/*
 * <p>ClassName: LPAgentComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPAgentComDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LPAgentComSchema implements Schema
{
    // @Field
    /** 上报申请号 */
    private String AppID;
    /** 上报项目 */
    private String AppItem;
    /** 代理机构 */
    private String AgentCom;
    /** 管理机构 */
    private String ManageCom;
    /** 地区类型 */
    private String AreaType;
    /** 渠道类型 */
    private String ChannelType;
    /** 上级代理机构 */
    private String UpAgentCom;
    /** 机构名称 */
    private String Name;
    /** 机构注册地址 */
    private String Address;
    /** 机构邮编 */
    private String ZipCode;
    /** 机构电话 */
    private String Phone;
    /** 机构传真 */
    private String Fax;
    /** Email */
    private String EMail;
    /** 网址 */
    private String WebAddress;
    /** 负责人 */
    private String LinkMan;
    /** 密码 */
    private String Password;
    /** 法人 */
    private String Corporation;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 行业分类 */
    private String BusinessType;
    /** 单位性质 */
    private String GrpNature;
    /** 中介机构类别 */
    private String ACType;
    /** 销售资格 */
    private String SellFlag;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 银行级别 */
    private String BankType;
    /** 是否统计网点合格率 */
    private String CalFlag;
    /** 工商执照编码 */
    private String BusiLicenseCode;
    /** 保险公司id */
    private String InsureID;
    /** 保险公司负责人 */
    private String InsurePrincipal;
    /** 主营业务 */
    private String ChiefBusiness;
    /** 营业地址 */
    private String BusiAddress;
    /** 签署人 */
    private String SubscribeMan;
    /** 签署人职务 */
    private String SubscribeManDuty;
    /** 许可证号码 */
    private String LicenseNo;
    /** 行政区划代码 */
    private String RegionalismCode;
    /** 上报代码 */
    private String AppAgentCom;
    /** 机构状态 */
    private String State;
    /** 转入保险公司id */
    private String AppInsureID;
    /** 转入机构 */
    private String AppManageCom;
    /** 相关说明 */
    private String Noti;
    /** 填表日期 */
    private Date FillDate;
    /** 表号 */
    private String TableNo;
    /** 行业代码 */
    private String BusinessCode;
    /** 许可证登记日期 */
    private Date LicenseStartDate;
    /** 许可证截至日期 */
    private Date LicenseEndDate;

    public static final int FIELDNUM = 49; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPAgentComSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "AppID";
        pk[1] = "AppItem";
        pk[2] = "AgentCom";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAppID()
    {
        if (AppID != null && !AppID.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppID = StrTool.unicodeToGBK(AppID);
        }
        return AppID;
    }

    public void setAppID(String aAppID)
    {
        AppID = aAppID;
    }

    public String getAppItem()
    {
        if (AppItem != null && !AppItem.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppItem = StrTool.unicodeToGBK(AppItem);
        }
        return AppItem;
    }

    public void setAppItem(String aAppItem)
    {
        AppItem = aAppItem;
    }

    public String getAgentCom()
    {
        if (AgentCom != null && !AgentCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAreaType()
    {
        if (AreaType != null && !AreaType.equals("") && SysConst.CHANGECHARSET == true)
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    public String getChannelType()
    {
        if (ChannelType != null && !ChannelType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ChannelType = StrTool.unicodeToGBK(ChannelType);
        }
        return ChannelType;
    }

    public void setChannelType(String aChannelType)
    {
        ChannelType = aChannelType;
    }

    public String getUpAgentCom()
    {
        if (UpAgentCom != null && !UpAgentCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UpAgentCom = StrTool.unicodeToGBK(UpAgentCom);
        }
        return UpAgentCom;
    }

    public void setUpAgentCom(String aUpAgentCom)
    {
        UpAgentCom = aUpAgentCom;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET == true)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getAddress()
    {
        if (Address != null && !Address.equals("") && SysConst.CHANGECHARSET == true)
        {
            Address = StrTool.unicodeToGBK(Address);
        }
        return Address;
    }

    public void setAddress(String aAddress)
    {
        Address = aAddress;
    }

    public String getZipCode()
    {
        if (ZipCode != null && !ZipCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ZipCode = StrTool.unicodeToGBK(ZipCode);
        }
        return ZipCode;
    }

    public void setZipCode(String aZipCode)
    {
        ZipCode = aZipCode;
    }

    public String getPhone()
    {
        if (Phone != null && !Phone.equals("") && SysConst.CHANGECHARSET == true)
        {
            Phone = StrTool.unicodeToGBK(Phone);
        }
        return Phone;
    }

    public void setPhone(String aPhone)
    {
        Phone = aPhone;
    }

    public String getFax()
    {
        if (Fax != null && !Fax.equals("") && SysConst.CHANGECHARSET == true)
        {
            Fax = StrTool.unicodeToGBK(Fax);
        }
        return Fax;
    }

    public void setFax(String aFax)
    {
        Fax = aFax;
    }

    public String getEMail()
    {
        if (EMail != null && !EMail.equals("") && SysConst.CHANGECHARSET == true)
        {
            EMail = StrTool.unicodeToGBK(EMail);
        }
        return EMail;
    }

    public void setEMail(String aEMail)
    {
        EMail = aEMail;
    }

    public String getWebAddress()
    {
        if (WebAddress != null && !WebAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            WebAddress = StrTool.unicodeToGBK(WebAddress);
        }
        return WebAddress;
    }

    public void setWebAddress(String aWebAddress)
    {
        WebAddress = aWebAddress;
    }

    public String getLinkMan()
    {
        if (LinkMan != null && !LinkMan.equals("") && SysConst.CHANGECHARSET == true)
        {
            LinkMan = StrTool.unicodeToGBK(LinkMan);
        }
        return LinkMan;
    }

    public void setLinkMan(String aLinkMan)
    {
        LinkMan = aLinkMan;
    }

    public String getPassword()
    {
        if (Password != null && !Password.equals("") && SysConst.CHANGECHARSET == true)
        {
            Password = StrTool.unicodeToGBK(Password);
        }
        return Password;
    }

    public void setPassword(String aPassword)
    {
        Password = aPassword;
    }

    public String getCorporation()
    {
        if (Corporation != null && !Corporation.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            Corporation = StrTool.unicodeToGBK(Corporation);
        }
        return Corporation;
    }

    public void setCorporation(String aCorporation)
    {
        Corporation = aCorporation;
    }

    public String getBankCode()
    {
        if (BankCode != null && !BankCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            BankCode = StrTool.unicodeToGBK(BankCode);
        }
        return BankCode;
    }

    public void setBankCode(String aBankCode)
    {
        BankCode = aBankCode;
    }

    public String getBankAccNo()
    {
        if (BankAccNo != null && !BankAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BankAccNo = StrTool.unicodeToGBK(BankAccNo);
        }
        return BankAccNo;
    }

    public void setBankAccNo(String aBankAccNo)
    {
        BankAccNo = aBankAccNo;
    }

    public String getBusinessType()
    {
        if (BusinessType != null && !BusinessType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BusinessType = StrTool.unicodeToGBK(BusinessType);
        }
        return BusinessType;
    }

    public void setBusinessType(String aBusinessType)
    {
        BusinessType = aBusinessType;
    }

    public String getGrpNature()
    {
        if (GrpNature != null && !GrpNature.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpNature = StrTool.unicodeToGBK(GrpNature);
        }
        return GrpNature;
    }

    public void setGrpNature(String aGrpNature)
    {
        GrpNature = aGrpNature;
    }

    public String getACType()
    {
        if (ACType != null && !ACType.equals("") && SysConst.CHANGECHARSET == true)
        {
            ACType = StrTool.unicodeToGBK(ACType);
        }
        return ACType;
    }

    public void setACType(String aACType)
    {
        ACType = aACType;
    }

    public String getSellFlag()
    {
        if (SellFlag != null && !SellFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            SellFlag = StrTool.unicodeToGBK(SellFlag);
        }
        return SellFlag;
    }

    public void setSellFlag(String aSellFlag)
    {
        SellFlag = aSellFlag;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getBankType()
    {
        if (BankType != null && !BankType.equals("") && SysConst.CHANGECHARSET == true)
        {
            BankType = StrTool.unicodeToGBK(BankType);
        }
        return BankType;
    }

    public void setBankType(String aBankType)
    {
        BankType = aBankType;
    }

    public String getCalFlag()
    {
        if (CalFlag != null && !CalFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalFlag = StrTool.unicodeToGBK(CalFlag);
        }
        return CalFlag;
    }

    public void setCalFlag(String aCalFlag)
    {
        CalFlag = aCalFlag;
    }

    public String getBusiLicenseCode()
    {
        if (BusiLicenseCode != null && !BusiLicenseCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BusiLicenseCode = StrTool.unicodeToGBK(BusiLicenseCode);
        }
        return BusiLicenseCode;
    }

    public void setBusiLicenseCode(String aBusiLicenseCode)
    {
        BusiLicenseCode = aBusiLicenseCode;
    }

    public String getInsureID()
    {
        if (InsureID != null && !InsureID.equals("") && SysConst.CHANGECHARSET == true)
        {
            InsureID = StrTool.unicodeToGBK(InsureID);
        }
        return InsureID;
    }

    public void setInsureID(String aInsureID)
    {
        InsureID = aInsureID;
    }

    public String getInsurePrincipal()
    {
        if (InsurePrincipal != null && !InsurePrincipal.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsurePrincipal = StrTool.unicodeToGBK(InsurePrincipal);
        }
        return InsurePrincipal;
    }

    public void setInsurePrincipal(String aInsurePrincipal)
    {
        InsurePrincipal = aInsurePrincipal;
    }

    public String getChiefBusiness()
    {
        if (ChiefBusiness != null && !ChiefBusiness.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ChiefBusiness = StrTool.unicodeToGBK(ChiefBusiness);
        }
        return ChiefBusiness;
    }

    public void setChiefBusiness(String aChiefBusiness)
    {
        ChiefBusiness = aChiefBusiness;
    }

    public String getBusiAddress()
    {
        if (BusiAddress != null && !BusiAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BusiAddress = StrTool.unicodeToGBK(BusiAddress);
        }
        return BusiAddress;
    }

    public void setBusiAddress(String aBusiAddress)
    {
        BusiAddress = aBusiAddress;
    }

    public String getSubscribeMan()
    {
        if (SubscribeMan != null && !SubscribeMan.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SubscribeMan = StrTool.unicodeToGBK(SubscribeMan);
        }
        return SubscribeMan;
    }

    public void setSubscribeMan(String aSubscribeMan)
    {
        SubscribeMan = aSubscribeMan;
    }

    public String getSubscribeManDuty()
    {
        if (SubscribeManDuty != null && !SubscribeManDuty.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SubscribeManDuty = StrTool.unicodeToGBK(SubscribeManDuty);
        }
        return SubscribeManDuty;
    }

    public void setSubscribeManDuty(String aSubscribeManDuty)
    {
        SubscribeManDuty = aSubscribeManDuty;
    }

    public String getLicenseNo()
    {
        if (LicenseNo != null && !LicenseNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            LicenseNo = StrTool.unicodeToGBK(LicenseNo);
        }
        return LicenseNo;
    }

    public void setLicenseNo(String aLicenseNo)
    {
        LicenseNo = aLicenseNo;
    }

    public String getRegionalismCode()
    {
        if (RegionalismCode != null && !RegionalismCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RegionalismCode = StrTool.unicodeToGBK(RegionalismCode);
        }
        return RegionalismCode;
    }

    public void setRegionalismCode(String aRegionalismCode)
    {
        RegionalismCode = aRegionalismCode;
    }

    public String getAppAgentCom()
    {
        if (AppAgentCom != null && !AppAgentCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppAgentCom = StrTool.unicodeToGBK(AppAgentCom);
        }
        return AppAgentCom;
    }

    public void setAppAgentCom(String aAppAgentCom)
    {
        AppAgentCom = aAppAgentCom;
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET == true)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getAppInsureID()
    {
        if (AppInsureID != null && !AppInsureID.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppInsureID = StrTool.unicodeToGBK(AppInsureID);
        }
        return AppInsureID;
    }

    public void setAppInsureID(String aAppInsureID)
    {
        AppInsureID = aAppInsureID;
    }

    public String getAppManageCom()
    {
        if (AppManageCom != null && !AppManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppManageCom = StrTool.unicodeToGBK(AppManageCom);
        }
        return AppManageCom;
    }

    public void setAppManageCom(String aAppManageCom)
    {
        AppManageCom = aAppManageCom;
    }

    public String getNoti()
    {
        if (Noti != null && !Noti.equals("") && SysConst.CHANGECHARSET == true)
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getFillDate()
    {
        if (FillDate != null)
        {
            return fDate.getString(FillDate);
        }
        else
        {
            return null;
        }
    }

    public void setFillDate(Date aFillDate)
    {
        FillDate = aFillDate;
    }

    public void setFillDate(String aFillDate)
    {
        if (aFillDate != null && !aFillDate.equals(""))
        {
            FillDate = fDate.getDate(aFillDate);
        }
        else
        {
            FillDate = null;
        }
    }

    public String getTableNo()
    {
        if (TableNo != null && !TableNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            TableNo = StrTool.unicodeToGBK(TableNo);
        }
        return TableNo;
    }

    public void setTableNo(String aTableNo)
    {
        TableNo = aTableNo;
    }

    public String getBusinessCode()
    {
        if (BusinessCode != null && !BusinessCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BusinessCode = StrTool.unicodeToGBK(BusinessCode);
        }
        return BusinessCode;
    }

    public void setBusinessCode(String aBusinessCode)
    {
        BusinessCode = aBusinessCode;
    }

    public String getLicenseStartDate()
    {
        if (LicenseStartDate != null)
        {
            return fDate.getString(LicenseStartDate);
        }
        else
        {
            return null;
        }
    }

    public void setLicenseStartDate(Date aLicenseStartDate)
    {
        LicenseStartDate = aLicenseStartDate;
    }

    public void setLicenseStartDate(String aLicenseStartDate)
    {
        if (aLicenseStartDate != null && !aLicenseStartDate.equals(""))
        {
            LicenseStartDate = fDate.getDate(aLicenseStartDate);
        }
        else
        {
            LicenseStartDate = null;
        }
    }

    public String getLicenseEndDate()
    {
        if (LicenseEndDate != null)
        {
            return fDate.getString(LicenseEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setLicenseEndDate(Date aLicenseEndDate)
    {
        LicenseEndDate = aLicenseEndDate;
    }

    public void setLicenseEndDate(String aLicenseEndDate)
    {
        if (aLicenseEndDate != null && !aLicenseEndDate.equals(""))
        {
            LicenseEndDate = fDate.getDate(aLicenseEndDate);
        }
        else
        {
            LicenseEndDate = null;
        }
    }


    /**
     * 使用另外一个 LPAgentComSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LPAgentComSchema aLPAgentComSchema)
    {
        this.AppID = aLPAgentComSchema.getAppID();
        this.AppItem = aLPAgentComSchema.getAppItem();
        this.AgentCom = aLPAgentComSchema.getAgentCom();
        this.ManageCom = aLPAgentComSchema.getManageCom();
        this.AreaType = aLPAgentComSchema.getAreaType();
        this.ChannelType = aLPAgentComSchema.getChannelType();
        this.UpAgentCom = aLPAgentComSchema.getUpAgentCom();
        this.Name = aLPAgentComSchema.getName();
        this.Address = aLPAgentComSchema.getAddress();
        this.ZipCode = aLPAgentComSchema.getZipCode();
        this.Phone = aLPAgentComSchema.getPhone();
        this.Fax = aLPAgentComSchema.getFax();
        this.EMail = aLPAgentComSchema.getEMail();
        this.WebAddress = aLPAgentComSchema.getWebAddress();
        this.LinkMan = aLPAgentComSchema.getLinkMan();
        this.Password = aLPAgentComSchema.getPassword();
        this.Corporation = aLPAgentComSchema.getCorporation();
        this.BankCode = aLPAgentComSchema.getBankCode();
        this.BankAccNo = aLPAgentComSchema.getBankAccNo();
        this.BusinessType = aLPAgentComSchema.getBusinessType();
        this.GrpNature = aLPAgentComSchema.getGrpNature();
        this.ACType = aLPAgentComSchema.getACType();
        this.SellFlag = aLPAgentComSchema.getSellFlag();
        this.Operator = aLPAgentComSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPAgentComSchema.getMakeDate());
        this.MakeTime = aLPAgentComSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPAgentComSchema.getModifyDate());
        this.ModifyTime = aLPAgentComSchema.getModifyTime();
        this.BankType = aLPAgentComSchema.getBankType();
        this.CalFlag = aLPAgentComSchema.getCalFlag();
        this.BusiLicenseCode = aLPAgentComSchema.getBusiLicenseCode();
        this.InsureID = aLPAgentComSchema.getInsureID();
        this.InsurePrincipal = aLPAgentComSchema.getInsurePrincipal();
        this.ChiefBusiness = aLPAgentComSchema.getChiefBusiness();
        this.BusiAddress = aLPAgentComSchema.getBusiAddress();
        this.SubscribeMan = aLPAgentComSchema.getSubscribeMan();
        this.SubscribeManDuty = aLPAgentComSchema.getSubscribeManDuty();
        this.LicenseNo = aLPAgentComSchema.getLicenseNo();
        this.RegionalismCode = aLPAgentComSchema.getRegionalismCode();
        this.AppAgentCom = aLPAgentComSchema.getAppAgentCom();
        this.State = aLPAgentComSchema.getState();
        this.AppInsureID = aLPAgentComSchema.getAppInsureID();
        this.AppManageCom = aLPAgentComSchema.getAppManageCom();
        this.Noti = aLPAgentComSchema.getNoti();
        this.FillDate = fDate.getDate(aLPAgentComSchema.getFillDate());
        this.TableNo = aLPAgentComSchema.getTableNo();
        this.BusinessCode = aLPAgentComSchema.getBusinessCode();
        this.LicenseStartDate = fDate.getDate(aLPAgentComSchema.
                                              getLicenseStartDate());
        this.LicenseEndDate = fDate.getDate(aLPAgentComSchema.getLicenseEndDate());
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AppID") == null)
            {
                this.AppID = null;
            }
            else
            {
                this.AppID = rs.getString("AppID").trim();
            }

            if (rs.getString("AppItem") == null)
            {
                this.AppItem = null;
            }
            else
            {
                this.AppItem = rs.getString("AppItem").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

            if (rs.getString("ChannelType") == null)
            {
                this.ChannelType = null;
            }
            else
            {
                this.ChannelType = rs.getString("ChannelType").trim();
            }

            if (rs.getString("UpAgentCom") == null)
            {
                this.UpAgentCom = null;
            }
            else
            {
                this.UpAgentCom = rs.getString("UpAgentCom").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("Address") == null)
            {
                this.Address = null;
            }
            else
            {
                this.Address = rs.getString("Address").trim();
            }

            if (rs.getString("ZipCode") == null)
            {
                this.ZipCode = null;
            }
            else
            {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null)
            {
                this.Phone = null;
            }
            else
            {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("Fax") == null)
            {
                this.Fax = null;
            }
            else
            {
                this.Fax = rs.getString("Fax").trim();
            }

            if (rs.getString("EMail") == null)
            {
                this.EMail = null;
            }
            else
            {
                this.EMail = rs.getString("EMail").trim();
            }

            if (rs.getString("WebAddress") == null)
            {
                this.WebAddress = null;
            }
            else
            {
                this.WebAddress = rs.getString("WebAddress").trim();
            }

            if (rs.getString("LinkMan") == null)
            {
                this.LinkMan = null;
            }
            else
            {
                this.LinkMan = rs.getString("LinkMan").trim();
            }

            if (rs.getString("Password") == null)
            {
                this.Password = null;
            }
            else
            {
                this.Password = rs.getString("Password").trim();
            }

            if (rs.getString("Corporation") == null)
            {
                this.Corporation = null;
            }
            else
            {
                this.Corporation = rs.getString("Corporation").trim();
            }

            if (rs.getString("BankCode") == null)
            {
                this.BankCode = null;
            }
            else
            {
                this.BankCode = rs.getString("BankCode").trim();
            }

            if (rs.getString("BankAccNo") == null)
            {
                this.BankAccNo = null;
            }
            else
            {
                this.BankAccNo = rs.getString("BankAccNo").trim();
            }

            if (rs.getString("BusinessType") == null)
            {
                this.BusinessType = null;
            }
            else
            {
                this.BusinessType = rs.getString("BusinessType").trim();
            }

            if (rs.getString("GrpNature") == null)
            {
                this.GrpNature = null;
            }
            else
            {
                this.GrpNature = rs.getString("GrpNature").trim();
            }

            if (rs.getString("ACType") == null)
            {
                this.ACType = null;
            }
            else
            {
                this.ACType = rs.getString("ACType").trim();
            }

            if (rs.getString("SellFlag") == null)
            {
                this.SellFlag = null;
            }
            else
            {
                this.SellFlag = rs.getString("SellFlag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BankType") == null)
            {
                this.BankType = null;
            }
            else
            {
                this.BankType = rs.getString("BankType").trim();
            }

            if (rs.getString("CalFlag") == null)
            {
                this.CalFlag = null;
            }
            else
            {
                this.CalFlag = rs.getString("CalFlag").trim();
            }

            if (rs.getString("BusiLicenseCode") == null)
            {
                this.BusiLicenseCode = null;
            }
            else
            {
                this.BusiLicenseCode = rs.getString("BusiLicenseCode").trim();
            }

            if (rs.getString("InsureID") == null)
            {
                this.InsureID = null;
            }
            else
            {
                this.InsureID = rs.getString("InsureID").trim();
            }

            if (rs.getString("InsurePrincipal") == null)
            {
                this.InsurePrincipal = null;
            }
            else
            {
                this.InsurePrincipal = rs.getString("InsurePrincipal").trim();
            }

            if (rs.getString("ChiefBusiness") == null)
            {
                this.ChiefBusiness = null;
            }
            else
            {
                this.ChiefBusiness = rs.getString("ChiefBusiness").trim();
            }

            if (rs.getString("BusiAddress") == null)
            {
                this.BusiAddress = null;
            }
            else
            {
                this.BusiAddress = rs.getString("BusiAddress").trim();
            }

            if (rs.getString("SubscribeMan") == null)
            {
                this.SubscribeMan = null;
            }
            else
            {
                this.SubscribeMan = rs.getString("SubscribeMan").trim();
            }

            if (rs.getString("SubscribeManDuty") == null)
            {
                this.SubscribeManDuty = null;
            }
            else
            {
                this.SubscribeManDuty = rs.getString("SubscribeManDuty").trim();
            }

            if (rs.getString("LicenseNo") == null)
            {
                this.LicenseNo = null;
            }
            else
            {
                this.LicenseNo = rs.getString("LicenseNo").trim();
            }

            if (rs.getString("RegionalismCode") == null)
            {
                this.RegionalismCode = null;
            }
            else
            {
                this.RegionalismCode = rs.getString("RegionalismCode").trim();
            }

            if (rs.getString("AppAgentCom") == null)
            {
                this.AppAgentCom = null;
            }
            else
            {
                this.AppAgentCom = rs.getString("AppAgentCom").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("AppInsureID") == null)
            {
                this.AppInsureID = null;
            }
            else
            {
                this.AppInsureID = rs.getString("AppInsureID").trim();
            }

            if (rs.getString("AppManageCom") == null)
            {
                this.AppManageCom = null;
            }
            else
            {
                this.AppManageCom = rs.getString("AppManageCom").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            this.FillDate = rs.getDate("FillDate");
            if (rs.getString("TableNo") == null)
            {
                this.TableNo = null;
            }
            else
            {
                this.TableNo = rs.getString("TableNo").trim();
            }

            if (rs.getString("BusinessCode") == null)
            {
                this.BusinessCode = null;
            }
            else
            {
                this.BusinessCode = rs.getString("BusinessCode").trim();
            }

            this.LicenseStartDate = rs.getDate("LicenseStartDate");
            this.LicenseEndDate = rs.getDate("LicenseEndDate");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPAgentComSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPAgentComSchema getSchema()
    {
        LPAgentComSchema aLPAgentComSchema = new LPAgentComSchema();
        aLPAgentComSchema.setSchema(this);
        return aLPAgentComSchema;
    }

    public LPAgentComDB getDB()
    {
        LPAgentComDB aDBOper = new LPAgentComDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPAgentCom描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AppID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppItem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AreaType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChannelType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpAgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Address)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Phone)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Fax)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EMail)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(WebAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(LinkMan)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Password)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Corporation)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BusinessType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpNature)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ACType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SellFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BusiLicenseCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsureID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsurePrincipal)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChiefBusiness)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BusiAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SubscribeMan)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SubscribeManDuty)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(LicenseNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RegionalismCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppAgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppInsureID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Noti)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(FillDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TableNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BusinessCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            LicenseStartDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            LicenseEndDate)));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPAgentCom>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AppID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            AppItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            ChannelType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            UpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                  SysConst.PACKAGESPILTER);
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                 SysConst.PACKAGESPILTER);
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                   SysConst.PACKAGESPILTER);
            WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            LinkMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                     SysConst.PACKAGESPILTER);
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            Corporation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                         SysConst.PACKAGESPILTER);
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                       SysConst.PACKAGESPILTER);
            BusinessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                          SysConst.PACKAGESPILTER);
            GrpNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                       SysConst.PACKAGESPILTER);
            ACType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                    SysConst.PACKAGESPILTER);
            SellFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 27, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
            BankType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                      SysConst.PACKAGESPILTER);
            CalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                     SysConst.PACKAGESPILTER);
            BusiLicenseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             31, SysConst.PACKAGESPILTER);
            InsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                      SysConst.PACKAGESPILTER);
            InsurePrincipal = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             33, SysConst.PACKAGESPILTER);
            ChiefBusiness = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                           SysConst.PACKAGESPILTER);
            BusiAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,
                                         SysConst.PACKAGESPILTER);
            SubscribeMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                          SysConst.PACKAGESPILTER);
            SubscribeManDuty = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              37, SysConst.PACKAGESPILTER);
            LicenseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,
                                       SysConst.PACKAGESPILTER);
            RegionalismCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             39, SysConst.PACKAGESPILTER);
            AppAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,
                                         SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,
                                   SysConst.PACKAGESPILTER);
            AppInsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,
                                         SysConst.PACKAGESPILTER);
            AppManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,
                                          SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,
                                  SysConst.PACKAGESPILTER);
            FillDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 45, SysConst.PACKAGESPILTER));
            TableNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46,
                                     SysConst.PACKAGESPILTER);
            BusinessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47,
                                          SysConst.PACKAGESPILTER);
            LicenseStartDate = fDate.getDate(StrTool.getStr(StrTool.
                    GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER));
            LicenseEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 49, SysConst.PACKAGESPILTER));
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPAgentComSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AppID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppID));
        }
        if (FCode.equals("AppItem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppItem));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCom));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AreaType));
        }
        if (FCode.equals("ChannelType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChannelType));
        }
        if (FCode.equals("UpAgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpAgentCom));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Name));
        }
        if (FCode.equals("Address"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Address));
        }
        if (FCode.equals("ZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ZipCode));
        }
        if (FCode.equals("Phone"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Phone));
        }
        if (FCode.equals("Fax"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Fax));
        }
        if (FCode.equals("EMail"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EMail));
        }
        if (FCode.equals("WebAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WebAddress));
        }
        if (FCode.equals("LinkMan"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LinkMan));
        }
        if (FCode.equals("Password"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Password));
        }
        if (FCode.equals("Corporation"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Corporation));
        }
        if (FCode.equals("BankCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankCode));
        }
        if (FCode.equals("BankAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankAccNo));
        }
        if (FCode.equals("BusinessType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BusinessType));
        }
        if (FCode.equals("GrpNature"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpNature));
        }
        if (FCode.equals("ACType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ACType));
        }
        if (FCode.equals("SellFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SellFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("BankType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankType));
        }
        if (FCode.equals("CalFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFlag));
        }
        if (FCode.equals("BusiLicenseCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BusiLicenseCode));
        }
        if (FCode.equals("InsureID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsureID));
        }
        if (FCode.equals("InsurePrincipal"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsurePrincipal));
        }
        if (FCode.equals("ChiefBusiness"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChiefBusiness));
        }
        if (FCode.equals("BusiAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BusiAddress));
        }
        if (FCode.equals("SubscribeMan"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SubscribeMan));
        }
        if (FCode.equals("SubscribeManDuty"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SubscribeManDuty));
        }
        if (FCode.equals("LicenseNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LicenseNo));
        }
        if (FCode.equals("RegionalismCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RegionalismCode));
        }
        if (FCode.equals("AppAgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppAgentCom));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(State));
        }
        if (FCode.equals("AppInsureID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppInsureID));
        }
        if (FCode.equals("AppManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppManageCom));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Noti));
        }
        if (FCode.equals("FillDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getFillDate()));
        }
        if (FCode.equals("TableNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TableNo));
        }
        if (FCode.equals("BusinessCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BusinessCode));
        }
        if (FCode.equals("LicenseStartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getLicenseStartDate()));
        }
        if (FCode.equals("LicenseEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getLicenseEndDate()));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AppID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AppItem);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ChannelType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(UpAgentCom);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Fax);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(WebAddress);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(LinkMan);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Corporation);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(BusinessType);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(GrpNature);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ACType);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(SellFlag);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(BankType);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(CalFlag);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(BusiLicenseCode);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(InsureID);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(InsurePrincipal);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ChiefBusiness);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(BusiAddress);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(SubscribeMan);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(SubscribeManDuty);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(LicenseNo);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(RegionalismCode);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(AppAgentCom);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(AppInsureID);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(AppManageCom);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFillDate()));
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(TableNo);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(BusinessCode);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLicenseStartDate()));
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLicenseEndDate()));
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AppID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppID = FValue.trim();
            }
            else
            {
                AppID = null;
            }
        }
        if (FCode.equals("AppItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppItem = FValue.trim();
            }
            else
            {
                AppItem = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        if (FCode.equals("ChannelType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChannelType = FValue.trim();
            }
            else
            {
                ChannelType = null;
            }
        }
        if (FCode.equals("UpAgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpAgentCom = FValue.trim();
            }
            else
            {
                UpAgentCom = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("Address"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
            {
                Address = null;
            }
        }
        if (FCode.equals("ZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
            {
                ZipCode = null;
            }
        }
        if (FCode.equals("Phone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
            {
                Phone = null;
            }
        }
        if (FCode.equals("Fax"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
            {
                Fax = null;
            }
        }
        if (FCode.equals("EMail"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
            {
                EMail = null;
            }
        }
        if (FCode.equals("WebAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WebAddress = FValue.trim();
            }
            else
            {
                WebAddress = null;
            }
        }
        if (FCode.equals("LinkMan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LinkMan = FValue.trim();
            }
            else
            {
                LinkMan = null;
            }
        }
        if (FCode.equals("Password"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
            {
                Password = null;
            }
        }
        if (FCode.equals("Corporation"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Corporation = FValue.trim();
            }
            else
            {
                Corporation = null;
            }
        }
        if (FCode.equals("BankCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
            {
                BankCode = null;
            }
        }
        if (FCode.equals("BankAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
            {
                BankAccNo = null;
            }
        }
        if (FCode.equals("BusinessType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BusinessType = FValue.trim();
            }
            else
            {
                BusinessType = null;
            }
        }
        if (FCode.equals("GrpNature"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpNature = FValue.trim();
            }
            else
            {
                GrpNature = null;
            }
        }
        if (FCode.equals("ACType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ACType = FValue.trim();
            }
            else
            {
                ACType = null;
            }
        }
        if (FCode.equals("SellFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SellFlag = FValue.trim();
            }
            else
            {
                SellFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("BankType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankType = FValue.trim();
            }
            else
            {
                BankType = null;
            }
        }
        if (FCode.equals("CalFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalFlag = FValue.trim();
            }
            else
            {
                CalFlag = null;
            }
        }
        if (FCode.equals("BusiLicenseCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BusiLicenseCode = FValue.trim();
            }
            else
            {
                BusiLicenseCode = null;
            }
        }
        if (FCode.equals("InsureID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsureID = FValue.trim();
            }
            else
            {
                InsureID = null;
            }
        }
        if (FCode.equals("InsurePrincipal"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsurePrincipal = FValue.trim();
            }
            else
            {
                InsurePrincipal = null;
            }
        }
        if (FCode.equals("ChiefBusiness"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChiefBusiness = FValue.trim();
            }
            else
            {
                ChiefBusiness = null;
            }
        }
        if (FCode.equals("BusiAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BusiAddress = FValue.trim();
            }
            else
            {
                BusiAddress = null;
            }
        }
        if (FCode.equals("SubscribeMan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubscribeMan = FValue.trim();
            }
            else
            {
                SubscribeMan = null;
            }
        }
        if (FCode.equals("SubscribeManDuty"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubscribeManDuty = FValue.trim();
            }
            else
            {
                SubscribeManDuty = null;
            }
        }
        if (FCode.equals("LicenseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LicenseNo = FValue.trim();
            }
            else
            {
                LicenseNo = null;
            }
        }
        if (FCode.equals("RegionalismCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RegionalismCode = FValue.trim();
            }
            else
            {
                RegionalismCode = null;
            }
        }
        if (FCode.equals("AppAgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppAgentCom = FValue.trim();
            }
            else
            {
                AppAgentCom = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("AppInsureID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppInsureID = FValue.trim();
            }
            else
            {
                AppInsureID = null;
            }
        }
        if (FCode.equals("AppManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppManageCom = FValue.trim();
            }
            else
            {
                AppManageCom = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("FillDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FillDate = fDate.getDate(FValue);
            }
            else
            {
                FillDate = null;
            }
        }
        if (FCode.equals("TableNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TableNo = FValue.trim();
            }
            else
            {
                TableNo = null;
            }
        }
        if (FCode.equals("BusinessCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BusinessCode = FValue.trim();
            }
            else
            {
                BusinessCode = null;
            }
        }
        if (FCode.equals("LicenseStartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LicenseStartDate = fDate.getDate(FValue);
            }
            else
            {
                LicenseStartDate = null;
            }
        }
        if (FCode.equals("LicenseEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LicenseEndDate = fDate.getDate(FValue);
            }
            else
            {
                LicenseEndDate = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPAgentComSchema other = (LPAgentComSchema) otherObject;
        return
                AppID.equals(other.getAppID())
                && AppItem.equals(other.getAppItem())
                && AgentCom.equals(other.getAgentCom())
                && ManageCom.equals(other.getManageCom())
                && AreaType.equals(other.getAreaType())
                && ChannelType.equals(other.getChannelType())
                && UpAgentCom.equals(other.getUpAgentCom())
                && Name.equals(other.getName())
                && Address.equals(other.getAddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && Fax.equals(other.getFax())
                && EMail.equals(other.getEMail())
                && WebAddress.equals(other.getWebAddress())
                && LinkMan.equals(other.getLinkMan())
                && Password.equals(other.getPassword())
                && Corporation.equals(other.getCorporation())
                && BankCode.equals(other.getBankCode())
                && BankAccNo.equals(other.getBankAccNo())
                && BusinessType.equals(other.getBusinessType())
                && GrpNature.equals(other.getGrpNature())
                && ACType.equals(other.getACType())
                && SellFlag.equals(other.getSellFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BankType.equals(other.getBankType())
                && CalFlag.equals(other.getCalFlag())
                && BusiLicenseCode.equals(other.getBusiLicenseCode())
                && InsureID.equals(other.getInsureID())
                && InsurePrincipal.equals(other.getInsurePrincipal())
                && ChiefBusiness.equals(other.getChiefBusiness())
                && BusiAddress.equals(other.getBusiAddress())
                && SubscribeMan.equals(other.getSubscribeMan())
                && SubscribeManDuty.equals(other.getSubscribeManDuty())
                && LicenseNo.equals(other.getLicenseNo())
                && RegionalismCode.equals(other.getRegionalismCode())
                && AppAgentCom.equals(other.getAppAgentCom())
                && State.equals(other.getState())
                && AppInsureID.equals(other.getAppInsureID())
                && AppManageCom.equals(other.getAppManageCom())
                && Noti.equals(other.getNoti())
                && fDate.getString(FillDate).equals(other.getFillDate())
                && TableNo.equals(other.getTableNo())
                && BusinessCode.equals(other.getBusinessCode())
                &&
                fDate.getString(LicenseStartDate).equals(other.
                getLicenseStartDate())
                &&
                fDate.getString(LicenseEndDate).equals(other.getLicenseEndDate());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return 0;
        }
        if (strFieldName.equals("AppItem"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 2;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 3;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 4;
        }
        if (strFieldName.equals("ChannelType"))
        {
            return 5;
        }
        if (strFieldName.equals("UpAgentCom"))
        {
            return 6;
        }
        if (strFieldName.equals("Name"))
        {
            return 7;
        }
        if (strFieldName.equals("Address"))
        {
            return 8;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return 9;
        }
        if (strFieldName.equals("Phone"))
        {
            return 10;
        }
        if (strFieldName.equals("Fax"))
        {
            return 11;
        }
        if (strFieldName.equals("EMail"))
        {
            return 12;
        }
        if (strFieldName.equals("WebAddress"))
        {
            return 13;
        }
        if (strFieldName.equals("LinkMan"))
        {
            return 14;
        }
        if (strFieldName.equals("Password"))
        {
            return 15;
        }
        if (strFieldName.equals("Corporation"))
        {
            return 16;
        }
        if (strFieldName.equals("BankCode"))
        {
            return 17;
        }
        if (strFieldName.equals("BankAccNo"))
        {
            return 18;
        }
        if (strFieldName.equals("BusinessType"))
        {
            return 19;
        }
        if (strFieldName.equals("GrpNature"))
        {
            return 20;
        }
        if (strFieldName.equals("ACType"))
        {
            return 21;
        }
        if (strFieldName.equals("SellFlag"))
        {
            return 22;
        }
        if (strFieldName.equals("Operator"))
        {
            return 23;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 24;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 25;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 26;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 27;
        }
        if (strFieldName.equals("BankType"))
        {
            return 28;
        }
        if (strFieldName.equals("CalFlag"))
        {
            return 29;
        }
        if (strFieldName.equals("BusiLicenseCode"))
        {
            return 30;
        }
        if (strFieldName.equals("InsureID"))
        {
            return 31;
        }
        if (strFieldName.equals("InsurePrincipal"))
        {
            return 32;
        }
        if (strFieldName.equals("ChiefBusiness"))
        {
            return 33;
        }
        if (strFieldName.equals("BusiAddress"))
        {
            return 34;
        }
        if (strFieldName.equals("SubscribeMan"))
        {
            return 35;
        }
        if (strFieldName.equals("SubscribeManDuty"))
        {
            return 36;
        }
        if (strFieldName.equals("LicenseNo"))
        {
            return 37;
        }
        if (strFieldName.equals("RegionalismCode"))
        {
            return 38;
        }
        if (strFieldName.equals("AppAgentCom"))
        {
            return 39;
        }
        if (strFieldName.equals("State"))
        {
            return 40;
        }
        if (strFieldName.equals("AppInsureID"))
        {
            return 41;
        }
        if (strFieldName.equals("AppManageCom"))
        {
            return 42;
        }
        if (strFieldName.equals("Noti"))
        {
            return 43;
        }
        if (strFieldName.equals("FillDate"))
        {
            return 44;
        }
        if (strFieldName.equals("TableNo"))
        {
            return 45;
        }
        if (strFieldName.equals("BusinessCode"))
        {
            return 46;
        }
        if (strFieldName.equals("LicenseStartDate"))
        {
            return 47;
        }
        if (strFieldName.equals("LicenseEndDate"))
        {
            return 48;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AppID";
                break;
            case 1:
                strFieldName = "AppItem";
                break;
            case 2:
                strFieldName = "AgentCom";
                break;
            case 3:
                strFieldName = "ManageCom";
                break;
            case 4:
                strFieldName = "AreaType";
                break;
            case 5:
                strFieldName = "ChannelType";
                break;
            case 6:
                strFieldName = "UpAgentCom";
                break;
            case 7:
                strFieldName = "Name";
                break;
            case 8:
                strFieldName = "Address";
                break;
            case 9:
                strFieldName = "ZipCode";
                break;
            case 10:
                strFieldName = "Phone";
                break;
            case 11:
                strFieldName = "Fax";
                break;
            case 12:
                strFieldName = "EMail";
                break;
            case 13:
                strFieldName = "WebAddress";
                break;
            case 14:
                strFieldName = "LinkMan";
                break;
            case 15:
                strFieldName = "Password";
                break;
            case 16:
                strFieldName = "Corporation";
                break;
            case 17:
                strFieldName = "BankCode";
                break;
            case 18:
                strFieldName = "BankAccNo";
                break;
            case 19:
                strFieldName = "BusinessType";
                break;
            case 20:
                strFieldName = "GrpNature";
                break;
            case 21:
                strFieldName = "ACType";
                break;
            case 22:
                strFieldName = "SellFlag";
                break;
            case 23:
                strFieldName = "Operator";
                break;
            case 24:
                strFieldName = "MakeDate";
                break;
            case 25:
                strFieldName = "MakeTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "ModifyTime";
                break;
            case 28:
                strFieldName = "BankType";
                break;
            case 29:
                strFieldName = "CalFlag";
                break;
            case 30:
                strFieldName = "BusiLicenseCode";
                break;
            case 31:
                strFieldName = "InsureID";
                break;
            case 32:
                strFieldName = "InsurePrincipal";
                break;
            case 33:
                strFieldName = "ChiefBusiness";
                break;
            case 34:
                strFieldName = "BusiAddress";
                break;
            case 35:
                strFieldName = "SubscribeMan";
                break;
            case 36:
                strFieldName = "SubscribeManDuty";
                break;
            case 37:
                strFieldName = "LicenseNo";
                break;
            case 38:
                strFieldName = "RegionalismCode";
                break;
            case 39:
                strFieldName = "AppAgentCom";
                break;
            case 40:
                strFieldName = "State";
                break;
            case 41:
                strFieldName = "AppInsureID";
                break;
            case 42:
                strFieldName = "AppManageCom";
                break;
            case 43:
                strFieldName = "Noti";
                break;
            case 44:
                strFieldName = "FillDate";
                break;
            case 45:
                strFieldName = "TableNo";
                break;
            case 46:
                strFieldName = "BusinessCode";
                break;
            case 47:
                strFieldName = "LicenseStartDate";
                break;
            case 48:
                strFieldName = "LicenseEndDate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChannelType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UpAgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Address"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Fax"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EMail"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WebAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LinkMan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Password"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Corporation"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BusinessType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpNature"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ACType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SellFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BusiLicenseCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsureID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsurePrincipal"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChiefBusiness"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BusiAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubscribeMan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubscribeManDuty"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LicenseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RegionalismCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppAgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppInsureID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FillDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("TableNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BusinessCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LicenseStartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("LicenseEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 34:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 35:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 36:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 37:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 38:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 39:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 40:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 41:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 42:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 43:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 44:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 45:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 46:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 47:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 48:
                nFieldType = Schema.TYPE_DATE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
