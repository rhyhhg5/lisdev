/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAPlanUnitDB;

/*
 * <p>ClassName: LAPlanUnitSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险计划表
 * @CreateDate：2005-12-14
 */
public class LAPlanUnitSchema implements Schema, Cloneable {
    // @Field
    /** 计划类型 */
    private String PlanType;
    /** 计划对象 */
    private String PlanObject;
    /** 计划时间单位 */
    private String PlanPeriodUnit;
    /** 计划起期年月 */
    private String PlanStartYM;
    /** 计划项目类别 */
    private String PlanItemType;
    /** 管理机构 */
    private String ManageCom;
    /** 计划标准 */
    private double PlanValue;
    /** 计划标准2 */
    private double PlanValue2;
    /** 计划起期 */
    private Date PlanStartDate;
    /** 计划止期 */
    private Date PlanEndDate;
    /** 展业类型 */
    private String BranchType;
    /** 渠道 */
    private String BranchType2;
    /** 备注 */
    private String PlanMark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 18; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAPlanUnitSchema() {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "PlanType";
        pk[1] = "PlanObject";
        pk[2] = "PlanPeriodUnit";
        pk[3] = "PlanStartYM";
        pk[4] = "PlanItemType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAPlanUnitSchema cloned = (LAPlanUnitSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getPlanType() {
        return PlanType;
    }

    public void setPlanType(String aPlanType) {
        PlanType = aPlanType;
    }

    public String getPlanObject() {
        return PlanObject;
    }

    public void setPlanObject(String aPlanObject) {
        PlanObject = aPlanObject;
    }

    public String getPlanPeriodUnit() {
        return PlanPeriodUnit;
    }

    public void setPlanPeriodUnit(String aPlanPeriodUnit) {
        PlanPeriodUnit = aPlanPeriodUnit;
    }

    public String getPlanStartYM() {
        return PlanStartYM;
    }

    public void setPlanStartYM(String aPlanStartYM) {
        PlanStartYM = aPlanStartYM;
    }

    public String getPlanItemType() {
        return PlanItemType;
    }

    public void setPlanItemType(String aPlanItemType) {
        PlanItemType = aPlanItemType;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public double getPlanValue() {
        return PlanValue;
    }

    public void setPlanValue(double aPlanValue) {
        PlanValue = Arith.round(aPlanValue, 2);
    }

    public void setPlanValue(String aPlanValue) {
        if (aPlanValue != null && !aPlanValue.equals("")) {
            Double tDouble = new Double(aPlanValue);
            double d = tDouble.doubleValue();
            PlanValue = Arith.round(d, 2);
        }
    }

    public double getPlanValue2() {
        return PlanValue2;
    }

    public void setPlanValue2(double aPlanValue2) {
        PlanValue2 = Arith.round(aPlanValue2, 2);
    }

    public void setPlanValue2(String aPlanValue2) {
        if (aPlanValue2 != null && !aPlanValue2.equals("")) {
            Double tDouble = new Double(aPlanValue2);
            double d = tDouble.doubleValue();
            PlanValue2 = Arith.round(d, 2);
        }
    }

    public String getPlanStartDate() {
        if (PlanStartDate != null) {
            return fDate.getString(PlanStartDate);
        } else {
            return null;
        }
    }

    public void setPlanStartDate(Date aPlanStartDate) {
        PlanStartDate = aPlanStartDate;
    }

    public void setPlanStartDate(String aPlanStartDate) {
        if (aPlanStartDate != null && !aPlanStartDate.equals("")) {
            PlanStartDate = fDate.getDate(aPlanStartDate);
        } else {
            PlanStartDate = null;
        }
    }

    public String getPlanEndDate() {
        if (PlanEndDate != null) {
            return fDate.getString(PlanEndDate);
        } else {
            return null;
        }
    }

    public void setPlanEndDate(Date aPlanEndDate) {
        PlanEndDate = aPlanEndDate;
    }

    public void setPlanEndDate(String aPlanEndDate) {
        if (aPlanEndDate != null && !aPlanEndDate.equals("")) {
            PlanEndDate = fDate.getDate(aPlanEndDate);
        } else {
            PlanEndDate = null;
        }
    }

    public String getBranchType() {
        return BranchType;
    }

    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }

    public String getBranchType2() {
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }

    public String getPlanMark() {
        return PlanMark;
    }

    public void setPlanMark(String aPlanMark) {
        PlanMark = aPlanMark;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAPlanUnitSchema 对象给 Schema 赋值
     * @param: aLAPlanUnitSchema LAPlanUnitSchema
     **/
    public void setSchema(LAPlanUnitSchema aLAPlanUnitSchema) {
        this.PlanType = aLAPlanUnitSchema.getPlanType();
        this.PlanObject = aLAPlanUnitSchema.getPlanObject();
        this.PlanPeriodUnit = aLAPlanUnitSchema.getPlanPeriodUnit();
        this.PlanStartYM = aLAPlanUnitSchema.getPlanStartYM();
        this.PlanItemType = aLAPlanUnitSchema.getPlanItemType();
        this.ManageCom = aLAPlanUnitSchema.getManageCom();
        this.PlanValue = aLAPlanUnitSchema.getPlanValue();
        this.PlanValue2 = aLAPlanUnitSchema.getPlanValue2();
        this.PlanStartDate = fDate.getDate(aLAPlanUnitSchema.getPlanStartDate());
        this.PlanEndDate = fDate.getDate(aLAPlanUnitSchema.getPlanEndDate());
        this.BranchType = aLAPlanUnitSchema.getBranchType();
        this.BranchType2 = aLAPlanUnitSchema.getBranchType2();
        this.PlanMark = aLAPlanUnitSchema.getPlanMark();
        this.Operator = aLAPlanUnitSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAPlanUnitSchema.getMakeDate());
        this.MakeTime = aLAPlanUnitSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAPlanUnitSchema.getModifyDate());
        this.ModifyTime = aLAPlanUnitSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PlanType") == null) {
                this.PlanType = null;
            } else {
                this.PlanType = rs.getString("PlanType").trim();
            }

            if (rs.getString("PlanObject") == null) {
                this.PlanObject = null;
            } else {
                this.PlanObject = rs.getString("PlanObject").trim();
            }

            if (rs.getString("PlanPeriodUnit") == null) {
                this.PlanPeriodUnit = null;
            } else {
                this.PlanPeriodUnit = rs.getString("PlanPeriodUnit").trim();
            }

            if (rs.getString("PlanStartYM") == null) {
                this.PlanStartYM = null;
            } else {
                this.PlanStartYM = rs.getString("PlanStartYM").trim();
            }

            if (rs.getString("PlanItemType") == null) {
                this.PlanItemType = null;
            } else {
                this.PlanItemType = rs.getString("PlanItemType").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.PlanValue = rs.getDouble("PlanValue");
            this.PlanValue2 = rs.getDouble("PlanValue2");
            this.PlanStartDate = rs.getDate("PlanStartDate");
            this.PlanEndDate = rs.getDate("PlanEndDate");
            if (rs.getString("BranchType") == null) {
                this.BranchType = null;
            } else {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("BranchType2") == null) {
                this.BranchType2 = null;
            } else {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            if (rs.getString("PlanMark") == null) {
                this.PlanMark = null;
            } else {
                this.PlanMark = rs.getString("PlanMark").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LAPlanUnit表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlanUnitSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAPlanUnitSchema getSchema() {
        LAPlanUnitSchema aLAPlanUnitSchema = new LAPlanUnitSchema();
        aLAPlanUnitSchema.setSchema(this);
        return aLAPlanUnitSchema;
    }

    public LAPlanUnitDB getDB() {
        LAPlanUnitDB aDBOper = new LAPlanUnitDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAPlanUnit描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PlanType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanObject));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanPeriodUnit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanStartYM));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanItemType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PlanValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PlanValue2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PlanStartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PlanEndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanMark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAPlanUnit>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            PlanType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            PlanObject = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            PlanPeriodUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
            PlanStartYM = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            PlanItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            PlanValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            PlanValue2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            PlanStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            PlanEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            PlanMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlanUnitSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("PlanType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanType));
        }
        if (FCode.equals("PlanObject")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanObject));
        }
        if (FCode.equals("PlanPeriodUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanPeriodUnit));
        }
        if (FCode.equals("PlanStartYM")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanStartYM));
        }
        if (FCode.equals("PlanItemType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanItemType));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("PlanValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanValue));
        }
        if (FCode.equals("PlanValue2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanValue2));
        }
        if (FCode.equals("PlanStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getPlanStartDate()));
        }
        if (FCode.equals("PlanEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPlanEndDate()));
        }
        if (FCode.equals("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("PlanMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanMark));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(PlanType);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(PlanObject);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(PlanPeriodUnit);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(PlanStartYM);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(PlanItemType);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 6:
            strFieldValue = String.valueOf(PlanValue);
            break;
        case 7:
            strFieldValue = String.valueOf(PlanValue2);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getPlanStartDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getPlanEndDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(BranchType);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(BranchType2);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(PlanMark);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("PlanType")) {
            if (FValue != null && !FValue.equals("")) {
                PlanType = FValue.trim();
            } else {
                PlanType = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanObject")) {
            if (FValue != null && !FValue.equals("")) {
                PlanObject = FValue.trim();
            } else {
                PlanObject = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanPeriodUnit")) {
            if (FValue != null && !FValue.equals("")) {
                PlanPeriodUnit = FValue.trim();
            } else {
                PlanPeriodUnit = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanStartYM")) {
            if (FValue != null && !FValue.equals("")) {
                PlanStartYM = FValue.trim();
            } else {
                PlanStartYM = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanItemType")) {
            if (FValue != null && !FValue.equals("")) {
                PlanItemType = FValue.trim();
            } else {
                PlanItemType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanValue")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PlanValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("PlanValue2")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PlanValue2 = d;
            }
        }
        if (FCode.equalsIgnoreCase("PlanStartDate")) {
            if (FValue != null && !FValue.equals("")) {
                PlanStartDate = fDate.getDate(FValue);
            } else {
                PlanStartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanEndDate")) {
            if (FValue != null && !FValue.equals("")) {
                PlanEndDate = fDate.getDate(FValue);
            } else {
                PlanEndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if (FValue != null && !FValue.equals("")) {
                BranchType = FValue.trim();
            } else {
                BranchType = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if (FValue != null && !FValue.equals("")) {
                BranchType2 = FValue.trim();
            } else {
                BranchType2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanMark")) {
            if (FValue != null && !FValue.equals("")) {
                PlanMark = FValue.trim();
            } else {
                PlanMark = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LAPlanUnitSchema other = (LAPlanUnitSchema) otherObject;
        return
                PlanType.equals(other.getPlanType())
                && PlanObject.equals(other.getPlanObject())
                && PlanPeriodUnit.equals(other.getPlanPeriodUnit())
                && PlanStartYM.equals(other.getPlanStartYM())
                && PlanItemType.equals(other.getPlanItemType())
                && ManageCom.equals(other.getManageCom())
                && PlanValue == other.getPlanValue()
                && PlanValue2 == other.getPlanValue2()
                && fDate.getString(PlanStartDate).equals(other.getPlanStartDate())
                && fDate.getString(PlanEndDate).equals(other.getPlanEndDate())
                && BranchType.equals(other.getBranchType())
                && BranchType2.equals(other.getBranchType2())
                && PlanMark.equals(other.getPlanMark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("PlanType")) {
            return 0;
        }
        if (strFieldName.equals("PlanObject")) {
            return 1;
        }
        if (strFieldName.equals("PlanPeriodUnit")) {
            return 2;
        }
        if (strFieldName.equals("PlanStartYM")) {
            return 3;
        }
        if (strFieldName.equals("PlanItemType")) {
            return 4;
        }
        if (strFieldName.equals("ManageCom")) {
            return 5;
        }
        if (strFieldName.equals("PlanValue")) {
            return 6;
        }
        if (strFieldName.equals("PlanValue2")) {
            return 7;
        }
        if (strFieldName.equals("PlanStartDate")) {
            return 8;
        }
        if (strFieldName.equals("PlanEndDate")) {
            return 9;
        }
        if (strFieldName.equals("BranchType")) {
            return 10;
        }
        if (strFieldName.equals("BranchType2")) {
            return 11;
        }
        if (strFieldName.equals("PlanMark")) {
            return 12;
        }
        if (strFieldName.equals("Operator")) {
            return 13;
        }
        if (strFieldName.equals("MakeDate")) {
            return 14;
        }
        if (strFieldName.equals("MakeTime")) {
            return 15;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 16;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "PlanType";
            break;
        case 1:
            strFieldName = "PlanObject";
            break;
        case 2:
            strFieldName = "PlanPeriodUnit";
            break;
        case 3:
            strFieldName = "PlanStartYM";
            break;
        case 4:
            strFieldName = "PlanItemType";
            break;
        case 5:
            strFieldName = "ManageCom";
            break;
        case 6:
            strFieldName = "PlanValue";
            break;
        case 7:
            strFieldName = "PlanValue2";
            break;
        case 8:
            strFieldName = "PlanStartDate";
            break;
        case 9:
            strFieldName = "PlanEndDate";
            break;
        case 10:
            strFieldName = "BranchType";
            break;
        case 11:
            strFieldName = "BranchType2";
            break;
        case 12:
            strFieldName = "PlanMark";
            break;
        case 13:
            strFieldName = "Operator";
            break;
        case 14:
            strFieldName = "MakeDate";
            break;
        case 15:
            strFieldName = "MakeTime";
            break;
        case 16:
            strFieldName = "ModifyDate";
            break;
        case 17:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("PlanType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanObject")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanPeriodUnit")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanStartYM")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanItemType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanValue")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PlanValue2")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PlanStartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PlanEndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BranchType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanMark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 7:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
