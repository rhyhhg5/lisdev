/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAComChargeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAComChargeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAComChargeSchema implements Schema
{
    // @Field
    /** 代理机构 */
    private String AgentCom;
    /** 手续费类型 */
    private String ChargeType;
    /** 统计起期 */
    private Date StartDate;
    /** 统计止期 */
    private Date StartEnd;
    /** 展业类型 */
    private String BranchType;
    /** 管理机构 */
    private String ManageCom;
    /** 实发日期 */
    private Date GetDate;
    /** 实发手续费金额 */
    private double SumMoney;
    /** 本期应发手续费金额 */
    private double CurrMoney;
    /** 备用1 */
    private double T01;
    /** 财务确认日期 */
    private Date ConfirmDate;
    /** 发放状态 */
    private String State;
    /** 发放方法 */
    private String PayMode;
    /** 操作员 */
    private String Operator;
    /** 复核人 */
    private String Operator2;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAComChargeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "AgentCom";
        pk[1] = "ChargeType";
        pk[2] = "StartDate";
        pk[3] = "StartEnd";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCom()
    {
        if (SysConst.CHANGECHARSET && AgentCom != null && !AgentCom.equals(""))
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getChargeType()
    {
        if (SysConst.CHANGECHARSET && ChargeType != null &&
            !ChargeType.equals(""))
        {
            ChargeType = StrTool.unicodeToGBK(ChargeType);
        }
        return ChargeType;
    }

    public void setChargeType(String aChargeType)
    {
        ChargeType = aChargeType;
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public String getStartEnd()
    {
        if (StartEnd != null)
        {
            return fDate.getString(StartEnd);
        }
        else
        {
            return null;
        }
    }

    public void setStartEnd(Date aStartEnd)
    {
        StartEnd = aStartEnd;
    }

    public void setStartEnd(String aStartEnd)
    {
        if (aStartEnd != null && !aStartEnd.equals(""))
        {
            StartEnd = fDate.getDate(aStartEnd);
        }
        else
        {
            StartEnd = null;
        }
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getGetDate()
    {
        if (GetDate != null)
        {
            return fDate.getString(GetDate);
        }
        else
        {
            return null;
        }
    }

    public void setGetDate(Date aGetDate)
    {
        GetDate = aGetDate;
    }

    public void setGetDate(String aGetDate)
    {
        if (aGetDate != null && !aGetDate.equals(""))
        {
            GetDate = fDate.getDate(aGetDate);
        }
        else
        {
            GetDate = null;
        }
    }

    public double getSumMoney()
    {
        return SumMoney;
    }

    public void setSumMoney(double aSumMoney)
    {
        SumMoney = aSumMoney;
    }

    public void setSumMoney(String aSumMoney)
    {
        if (aSumMoney != null && !aSumMoney.equals(""))
        {
            Double tDouble = new Double(aSumMoney);
            double d = tDouble.doubleValue();
            SumMoney = d;
        }
    }

    public double getCurrMoney()
    {
        return CurrMoney;
    }

    public void setCurrMoney(double aCurrMoney)
    {
        CurrMoney = aCurrMoney;
    }

    public void setCurrMoney(String aCurrMoney)
    {
        if (aCurrMoney != null && !aCurrMoney.equals(""))
        {
            Double tDouble = new Double(aCurrMoney);
            double d = tDouble.doubleValue();
            CurrMoney = d;
        }
    }

    public double getT01()
    {
        return T01;
    }

    public void setT01(double aT01)
    {
        T01 = aT01;
    }

    public void setT01(String aT01)
    {
        if (aT01 != null && !aT01.equals(""))
        {
            Double tDouble = new Double(aT01);
            double d = tDouble.doubleValue();
            T01 = d;
        }
    }

    public String getConfirmDate()
    {
        if (ConfirmDate != null)
        {
            return fDate.getString(ConfirmDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfirmDate(Date aConfirmDate)
    {
        ConfirmDate = aConfirmDate;
    }

    public void setConfirmDate(String aConfirmDate)
    {
        if (aConfirmDate != null && !aConfirmDate.equals(""))
        {
            ConfirmDate = fDate.getDate(aConfirmDate);
        }
        else
        {
            ConfirmDate = null;
        }
    }

    public String getState()
    {
        if (SysConst.CHANGECHARSET && State != null && !State.equals(""))
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getPayMode()
    {
        if (SysConst.CHANGECHARSET && PayMode != null && !PayMode.equals(""))
        {
            PayMode = StrTool.unicodeToGBK(PayMode);
        }
        return PayMode;
    }

    public void setPayMode(String aPayMode)
    {
        PayMode = aPayMode;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getOperator2()
    {
        if (SysConst.CHANGECHARSET && Operator2 != null && !Operator2.equals(""))
        {
            Operator2 = StrTool.unicodeToGBK(Operator2);
        }
        return Operator2;
    }

    public void setOperator2(String aOperator2)
    {
        Operator2 = aOperator2;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAComChargeSchema 对象给 Schema 赋值
     * @param: aLAComChargeSchema LAComChargeSchema
     **/
    public void setSchema(LAComChargeSchema aLAComChargeSchema)
    {
        this.AgentCom = aLAComChargeSchema.getAgentCom();
        this.ChargeType = aLAComChargeSchema.getChargeType();
        this.StartDate = fDate.getDate(aLAComChargeSchema.getStartDate());
        this.StartEnd = fDate.getDate(aLAComChargeSchema.getStartEnd());
        this.BranchType = aLAComChargeSchema.getBranchType();
        this.ManageCom = aLAComChargeSchema.getManageCom();
        this.GetDate = fDate.getDate(aLAComChargeSchema.getGetDate());
        this.SumMoney = aLAComChargeSchema.getSumMoney();
        this.CurrMoney = aLAComChargeSchema.getCurrMoney();
        this.T01 = aLAComChargeSchema.getT01();
        this.ConfirmDate = fDate.getDate(aLAComChargeSchema.getConfirmDate());
        this.State = aLAComChargeSchema.getState();
        this.PayMode = aLAComChargeSchema.getPayMode();
        this.Operator = aLAComChargeSchema.getOperator();
        this.Operator2 = aLAComChargeSchema.getOperator2();
        this.MakeDate = fDate.getDate(aLAComChargeSchema.getMakeDate());
        this.MakeTime = aLAComChargeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAComChargeSchema.getModifyDate());
        this.ModifyTime = aLAComChargeSchema.getModifyTime();
        this.BranchType2 = aLAComChargeSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("ChargeType") == null)
            {
                this.ChargeType = null;
            }
            else
            {
                this.ChargeType = rs.getString("ChargeType").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.StartEnd = rs.getDate("StartEnd");
            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.GetDate = rs.getDate("GetDate");
            this.SumMoney = rs.getDouble("SumMoney");
            this.CurrMoney = rs.getDouble("CurrMoney");
            this.T01 = rs.getDouble("T01");
            this.ConfirmDate = rs.getDate("ConfirmDate");
            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("PayMode") == null)
            {
                this.PayMode = null;
            }
            else
            {
                this.PayMode = rs.getString("PayMode").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("Operator2") == null)
            {
                this.Operator2 = null;
            }
            else
            {
                this.Operator2 = rs.getString("Operator2").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComChargeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAComChargeSchema getSchema()
    {
        LAComChargeSchema aLAComChargeSchema = new LAComChargeSchema();
        aLAComChargeSchema.setSchema(this);
        return aLAComChargeSchema;
    }

    public LAComChargeDB getDB()
    {
        LAComChargeDB aDBOper = new LAComChargeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAComCharge描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ChargeType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                StartDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                StartEnd))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                GetDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CurrMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T01));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ConfirmDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(State)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(PayMode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator2)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAComCharge>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ChargeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            StartEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            GetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            CurrMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            T01 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    10, SysConst.PACKAGESPILTER))).doubleValue();
            ConfirmDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                   SysConst.PACKAGESPILTER);
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            Operator2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComChargeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equals("ChargeType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeType));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("StartEnd"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartEnd()));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("GetDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getGetDate()));
        }
        if (FCode.equals("SumMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
        }
        if (FCode.equals("CurrMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CurrMoney));
        }
        if (FCode.equals("T01"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T01));
        }
        if (FCode.equals("ConfirmDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getConfirmDate()));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("PayMode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("Operator2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator2));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ChargeType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartEnd()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getGetDate()));
                break;
            case 7:
                strFieldValue = String.valueOf(SumMoney);
                break;
            case 8:
                strFieldValue = String.valueOf(CurrMoney);
                break;
            case 9:
                strFieldValue = String.valueOf(T01);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfirmDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Operator2);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("ChargeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChargeType = FValue.trim();
            }
            else
            {
                ChargeType = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("StartEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartEnd = fDate.getDate(FValue);
            }
            else
            {
                StartEnd = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("GetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDate = fDate.getDate(FValue);
            }
            else
            {
                GetDate = null;
            }
        }
        if (FCode.equals("SumMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumMoney = d;
            }
        }
        if (FCode.equals("CurrMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CurrMoney = d;
            }
        }
        if (FCode.equals("T01"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T01 = d;
            }
        }
        if (FCode.equals("ConfirmDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfirmDate = fDate.getDate(FValue);
            }
            else
            {
                ConfirmDate = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("PayMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
            {
                PayMode = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("Operator2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator2 = FValue.trim();
            }
            else
            {
                Operator2 = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAComChargeSchema other = (LAComChargeSchema) otherObject;
        return
                AgentCom.equals(other.getAgentCom())
                && ChargeType.equals(other.getChargeType())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(StartEnd).equals(other.getStartEnd())
                && BranchType.equals(other.getBranchType())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(GetDate).equals(other.getGetDate())
                && SumMoney == other.getSumMoney()
                && CurrMoney == other.getCurrMoney()
                && T01 == other.getT01()
                && fDate.getString(ConfirmDate).equals(other.getConfirmDate())
                && State.equals(other.getState())
                && PayMode.equals(other.getPayMode())
                && Operator.equals(other.getOperator())
                && Operator2.equals(other.getOperator2())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCom"))
        {
            return 0;
        }
        if (strFieldName.equals("ChargeType"))
        {
            return 1;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 2;
        }
        if (strFieldName.equals("StartEnd"))
        {
            return 3;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 4;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 5;
        }
        if (strFieldName.equals("GetDate"))
        {
            return 6;
        }
        if (strFieldName.equals("SumMoney"))
        {
            return 7;
        }
        if (strFieldName.equals("CurrMoney"))
        {
            return 8;
        }
        if (strFieldName.equals("T01"))
        {
            return 9;
        }
        if (strFieldName.equals("ConfirmDate"))
        {
            return 10;
        }
        if (strFieldName.equals("State"))
        {
            return 11;
        }
        if (strFieldName.equals("PayMode"))
        {
            return 12;
        }
        if (strFieldName.equals("Operator"))
        {
            return 13;
        }
        if (strFieldName.equals("Operator2"))
        {
            return 14;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 16;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 18;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCom";
                break;
            case 1:
                strFieldName = "ChargeType";
                break;
            case 2:
                strFieldName = "StartDate";
                break;
            case 3:
                strFieldName = "StartEnd";
                break;
            case 4:
                strFieldName = "BranchType";
                break;
            case 5:
                strFieldName = "ManageCom";
                break;
            case 6:
                strFieldName = "GetDate";
                break;
            case 7:
                strFieldName = "SumMoney";
                break;
            case 8:
                strFieldName = "CurrMoney";
                break;
            case 9:
                strFieldName = "T01";
                break;
            case 10:
                strFieldName = "ConfirmDate";
                break;
            case 11:
                strFieldName = "State";
                break;
            case 12:
                strFieldName = "PayMode";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "Operator2";
                break;
            case 15:
                strFieldName = "MakeDate";
                break;
            case 16:
                strFieldName = "MakeTime";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            case 19:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChargeType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("StartEnd"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SumMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CurrMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T01"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ConfirmDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
