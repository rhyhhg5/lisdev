/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.CIITC_CHECK_RESULTDB;

/*
 * <p>ClassName: CIITC_CHECK_RESULTSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-10-19
 */
public class CIITC_CHECK_RESULTSchema implements Schema, Cloneable
{
	// @Field
	/** Task_id */
	private String task_id;
	/** Businessno */
	private String businessNo;
	/** Businessserialno */
	private String businessSerialNo;
	/** Batchno */
	private String batchNo;
	/** Bankcode */
	private String bankCode;
	/** Subbankcode */
	private String subBankCode;
	/** Subinsurercode */
	private String subInsurerCode;
	/** Taskstatus */
	private String taskStatus;
	/** Ischeck */
	private String isCheck;
	/** Checkdate */
	private Date checkDate;
	/** Checkresult */
	private String checkResult;
	/** Extented */
	private String extented;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public CIITC_CHECK_RESULTSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "task_id";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		CIITC_CHECK_RESULTSchema cloned = (CIITC_CHECK_RESULTSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String gettask_id()
	{
		return task_id;
	}
	public void settask_id(String atask_id)
	{
		task_id = atask_id;
	}
	public String getbusinessNo()
	{
		return businessNo;
	}
	public void setbusinessNo(String abusinessNo)
	{
		businessNo = abusinessNo;
	}
	public String getbusinessSerialNo()
	{
		return businessSerialNo;
	}
	public void setbusinessSerialNo(String abusinessSerialNo)
	{
		businessSerialNo = abusinessSerialNo;
	}
	public String getbatchNo()
	{
		return batchNo;
	}
	public void setbatchNo(String abatchNo)
	{
		batchNo = abatchNo;
	}
	public String getbankCode()
	{
		return bankCode;
	}
	public void setbankCode(String abankCode)
	{
		bankCode = abankCode;
	}
	public String getsubBankCode()
	{
		return subBankCode;
	}
	public void setsubBankCode(String asubBankCode)
	{
		subBankCode = asubBankCode;
	}
	public String getsubInsurerCode()
	{
		return subInsurerCode;
	}
	public void setsubInsurerCode(String asubInsurerCode)
	{
		subInsurerCode = asubInsurerCode;
	}
	public String gettaskStatus()
	{
		return taskStatus;
	}
	public void settaskStatus(String ataskStatus)
	{
		taskStatus = ataskStatus;
	}
	public String getisCheck()
	{
		return isCheck;
	}
	public void setisCheck(String aisCheck)
	{
		isCheck = aisCheck;
	}
	public String getcheckDate()
	{
		if( checkDate != null )
			return fDate.getString(checkDate);
		else
			return null;
	}
	public void setcheckDate(Date acheckDate)
	{
		checkDate = acheckDate;
	}
	public void setcheckDate(String acheckDate)
	{
		if (acheckDate != null && !acheckDate.equals("") )
		{
			checkDate = fDate.getDate( acheckDate );
		}
		else
			checkDate = null;
	}

	public String getcheckResult()
	{
		return checkResult;
	}
	public void setcheckResult(String acheckResult)
	{
		checkResult = acheckResult;
	}
	public String getextented()
	{
		return extented;
	}
	public void setextented(String aextented)
	{
		extented = aextented;
	}

	/**
	* 使用另外一个 CIITC_CHECK_RESULTSchema 对象给 Schema 赋值
	* @param: aCIITC_CHECK_RESULTSchema CIITC_CHECK_RESULTSchema
	**/
	public void setSchema(CIITC_CHECK_RESULTSchema aCIITC_CHECK_RESULTSchema)
	{
		this.task_id = aCIITC_CHECK_RESULTSchema.gettask_id();
		this.businessNo = aCIITC_CHECK_RESULTSchema.getbusinessNo();
		this.businessSerialNo = aCIITC_CHECK_RESULTSchema.getbusinessSerialNo();
		this.batchNo = aCIITC_CHECK_RESULTSchema.getbatchNo();
		this.bankCode = aCIITC_CHECK_RESULTSchema.getbankCode();
		this.subBankCode = aCIITC_CHECK_RESULTSchema.getsubBankCode();
		this.subInsurerCode = aCIITC_CHECK_RESULTSchema.getsubInsurerCode();
		this.taskStatus = aCIITC_CHECK_RESULTSchema.gettaskStatus();
		this.isCheck = aCIITC_CHECK_RESULTSchema.getisCheck();
		this.checkDate = fDate.getDate( aCIITC_CHECK_RESULTSchema.getcheckDate());
		this.checkResult = aCIITC_CHECK_RESULTSchema.getcheckResult();
		this.extented = aCIITC_CHECK_RESULTSchema.getextented();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("task_id") == null )
				this.task_id = null;
			else
				this.task_id = rs.getString("task_id").trim();

			if( rs.getString("businessNo") == null )
				this.businessNo = null;
			else
				this.businessNo = rs.getString("businessNo").trim();

			if( rs.getString("businessSerialNo") == null )
				this.businessSerialNo = null;
			else
				this.businessSerialNo = rs.getString("businessSerialNo").trim();

			if( rs.getString("batchNo") == null )
				this.batchNo = null;
			else
				this.batchNo = rs.getString("batchNo").trim();

			if( rs.getString("bankCode") == null )
				this.bankCode = null;
			else
				this.bankCode = rs.getString("bankCode").trim();

			if( rs.getString("subBankCode") == null )
				this.subBankCode = null;
			else
				this.subBankCode = rs.getString("subBankCode").trim();

			if( rs.getString("subInsurerCode") == null )
				this.subInsurerCode = null;
			else
				this.subInsurerCode = rs.getString("subInsurerCode").trim();

			if( rs.getString("taskStatus") == null )
				this.taskStatus = null;
			else
				this.taskStatus = rs.getString("taskStatus").trim();

			if( rs.getString("isCheck") == null )
				this.isCheck = null;
			else
				this.isCheck = rs.getString("isCheck").trim();

			this.checkDate = rs.getDate("checkDate");
			if( rs.getString("checkResult") == null )
				this.checkResult = null;
			else
				this.checkResult = rs.getString("checkResult").trim();

			if( rs.getString("extented") == null )
				this.extented = null;
			else
				this.extented = rs.getString("extented").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的CIITC_CHECK_RESULT表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CIITC_CHECK_RESULTSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public CIITC_CHECK_RESULTSchema getSchema()
	{
		CIITC_CHECK_RESULTSchema aCIITC_CHECK_RESULTSchema = new CIITC_CHECK_RESULTSchema();
		aCIITC_CHECK_RESULTSchema.setSchema(this);
		return aCIITC_CHECK_RESULTSchema;
	}

	public CIITC_CHECK_RESULTDB getDB()
	{
		CIITC_CHECK_RESULTDB aDBOper = new CIITC_CHECK_RESULTDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCIITC_CHECK_RESULT描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(task_id)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(businessNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(businessSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(batchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(subBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(subInsurerCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(taskStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isCheck)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( checkDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(checkResult)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(extented));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCIITC_CHECK_RESULT>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			task_id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			businessNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			businessSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			batchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			bankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			subBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			subInsurerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			taskStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			isCheck = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			checkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			checkResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			extented = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CIITC_CHECK_RESULTSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("task_id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(task_id));
		}
		if (FCode.equals("businessNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(businessNo));
		}
		if (FCode.equals("businessSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(businessSerialNo));
		}
		if (FCode.equals("batchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(batchNo));
		}
		if (FCode.equals("bankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankCode));
		}
		if (FCode.equals("subBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(subBankCode));
		}
		if (FCode.equals("subInsurerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(subInsurerCode));
		}
		if (FCode.equals("taskStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(taskStatus));
		}
		if (FCode.equals("isCheck"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isCheck));
		}
		if (FCode.equals("checkDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getcheckDate()));
		}
		if (FCode.equals("checkResult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(checkResult));
		}
		if (FCode.equals("extented"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(extented));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(task_id);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(businessNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(businessSerialNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(batchNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(bankCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(subBankCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(subInsurerCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(taskStatus);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(isCheck);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getcheckDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(checkResult);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(extented);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("task_id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				task_id = FValue.trim();
			}
			else
				task_id = null;
		}
		if (FCode.equalsIgnoreCase("businessNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				businessNo = FValue.trim();
			}
			else
				businessNo = null;
		}
		if (FCode.equalsIgnoreCase("businessSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				businessSerialNo = FValue.trim();
			}
			else
				businessSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("batchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				batchNo = FValue.trim();
			}
			else
				batchNo = null;
		}
		if (FCode.equalsIgnoreCase("bankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankCode = FValue.trim();
			}
			else
				bankCode = null;
		}
		if (FCode.equalsIgnoreCase("subBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				subBankCode = FValue.trim();
			}
			else
				subBankCode = null;
		}
		if (FCode.equalsIgnoreCase("subInsurerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				subInsurerCode = FValue.trim();
			}
			else
				subInsurerCode = null;
		}
		if (FCode.equalsIgnoreCase("taskStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				taskStatus = FValue.trim();
			}
			else
				taskStatus = null;
		}
		if (FCode.equalsIgnoreCase("isCheck"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isCheck = FValue.trim();
			}
			else
				isCheck = null;
		}
		if (FCode.equalsIgnoreCase("checkDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				checkDate = fDate.getDate( FValue );
			}
			else
				checkDate = null;
		}
		if (FCode.equalsIgnoreCase("checkResult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				checkResult = FValue.trim();
			}
			else
				checkResult = null;
		}
		if (FCode.equalsIgnoreCase("extented"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				extented = FValue.trim();
			}
			else
				extented = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		CIITC_CHECK_RESULTSchema other = (CIITC_CHECK_RESULTSchema)otherObject;
		return
			(task_id == null ? other.gettask_id() == null : task_id.equals(other.gettask_id()))
			&& (businessNo == null ? other.getbusinessNo() == null : businessNo.equals(other.getbusinessNo()))
			&& (businessSerialNo == null ? other.getbusinessSerialNo() == null : businessSerialNo.equals(other.getbusinessSerialNo()))
			&& (batchNo == null ? other.getbatchNo() == null : batchNo.equals(other.getbatchNo()))
			&& (bankCode == null ? other.getbankCode() == null : bankCode.equals(other.getbankCode()))
			&& (subBankCode == null ? other.getsubBankCode() == null : subBankCode.equals(other.getsubBankCode()))
			&& (subInsurerCode == null ? other.getsubInsurerCode() == null : subInsurerCode.equals(other.getsubInsurerCode()))
			&& (taskStatus == null ? other.gettaskStatus() == null : taskStatus.equals(other.gettaskStatus()))
			&& (isCheck == null ? other.getisCheck() == null : isCheck.equals(other.getisCheck()))
			&& (checkDate == null ? other.getcheckDate() == null : fDate.getString(checkDate).equals(other.getcheckDate()))
			&& (checkResult == null ? other.getcheckResult() == null : checkResult.equals(other.getcheckResult()))
			&& (extented == null ? other.getextented() == null : extented.equals(other.getextented()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("task_id") ) {
			return 0;
		}
		if( strFieldName.equals("businessNo") ) {
			return 1;
		}
		if( strFieldName.equals("businessSerialNo") ) {
			return 2;
		}
		if( strFieldName.equals("batchNo") ) {
			return 3;
		}
		if( strFieldName.equals("bankCode") ) {
			return 4;
		}
		if( strFieldName.equals("subBankCode") ) {
			return 5;
		}
		if( strFieldName.equals("subInsurerCode") ) {
			return 6;
		}
		if( strFieldName.equals("taskStatus") ) {
			return 7;
		}
		if( strFieldName.equals("isCheck") ) {
			return 8;
		}
		if( strFieldName.equals("checkDate") ) {
			return 9;
		}
		if( strFieldName.equals("checkResult") ) {
			return 10;
		}
		if( strFieldName.equals("extented") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "task_id";
				break;
			case 1:
				strFieldName = "businessNo";
				break;
			case 2:
				strFieldName = "businessSerialNo";
				break;
			case 3:
				strFieldName = "batchNo";
				break;
			case 4:
				strFieldName = "bankCode";
				break;
			case 5:
				strFieldName = "subBankCode";
				break;
			case 6:
				strFieldName = "subInsurerCode";
				break;
			case 7:
				strFieldName = "taskStatus";
				break;
			case 8:
				strFieldName = "isCheck";
				break;
			case 9:
				strFieldName = "checkDate";
				break;
			case 10:
				strFieldName = "checkResult";
				break;
			case 11:
				strFieldName = "extented";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("task_id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("businessNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("businessSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("batchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("subBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("subInsurerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("taskStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isCheck") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("checkDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("checkResult") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("extented") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
