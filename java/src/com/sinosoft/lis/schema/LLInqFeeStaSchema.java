/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLInqFeeStaDB;

/*
 * <p>ClassName: LLInqFeeStaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-06-11
 */
public class LLInqFeeStaSchema implements Schema, Cloneable
{
	// @Field
	/** 对应号码 */
	private String OtherNo;
	/** 其他对应号码类型 */
	private String OtherNoType;
	/** 调查号 */
	private String SurveyNo;
	/** 调查机构 */
	private String InqDept;
	/** 调查次数 */
	private int ContSN;
	/** 录入人 */
	private String Inputer;
	/** 录入日期 */
	private Date InputDate;
	/** 审核人 */
	private String Confer;
	/** 审核日期 */
	private Date ConfDate;
	/** 领款人 */
	private String Payee;
	/** 领款方式 */
	private String PayeeType;
	/** 领取日期 */
	private Date PayGetDate;
	/** 备注 */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 直接调查费 */
	private double InqFee;
	/** 间接调查费 */
	private double IndirectFee;
	/** 案件调查费合计 */
	private double SurveyFee;
	/** 给付标记 */
	private String Payed;

	public static final int FIELDNUM = 22;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLInqFeeStaSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "SurveyNo";
		pk[1] = "InqDept";
		pk[2] = "ContSN";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LLInqFeeStaSchema cloned = (LLInqFeeStaSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getOtherNo()
	{
		return OtherNo;
	}
	public void setOtherNo(String aOtherNo)
	{
            OtherNo = aOtherNo;
	}
	public String getOtherNoType()
	{
		return OtherNoType;
	}
	public void setOtherNoType(String aOtherNoType)
	{
            OtherNoType = aOtherNoType;
	}
	public String getSurveyNo()
	{
		return SurveyNo;
	}
	public void setSurveyNo(String aSurveyNo)
	{
            SurveyNo = aSurveyNo;
	}
	public String getInqDept()
	{
		return InqDept;
	}
	public void setInqDept(String aInqDept)
	{
            InqDept = aInqDept;
	}
	public int getContSN()
	{
		return ContSN;
	}
	public void setContSN(int aContSN)
	{
            ContSN = aContSN;
	}
	public void setContSN(String aContSN)
	{
		if (aContSN != null && !aContSN.equals(""))
		{
			Integer tInteger = new Integer(aContSN);
			int i = tInteger.intValue();
			ContSN = i;
		}
	}

	public String getInputer()
	{
		return Inputer;
	}
	public void setInputer(String aInputer)
	{
            Inputer = aInputer;
	}
	public String getInputDate()
	{
		if( InputDate != null )
			return fDate.getString(InputDate);
		else
			return null;
	}
	public void setInputDate(Date aInputDate)
	{
            InputDate = aInputDate;
	}
	public void setInputDate(String aInputDate)
	{
		if (aInputDate != null && !aInputDate.equals("") )
		{
			InputDate = fDate.getDate( aInputDate );
		}
		else
			InputDate = null;
	}

	public String getConfer()
	{
		return Confer;
	}
	public void setConfer(String aConfer)
	{
            Confer = aConfer;
	}
	public String getConfDate()
	{
		if( ConfDate != null )
			return fDate.getString(ConfDate);
		else
			return null;
	}
	public void setConfDate(Date aConfDate)
	{
            ConfDate = aConfDate;
	}
	public void setConfDate(String aConfDate)
	{
		if (aConfDate != null && !aConfDate.equals("") )
		{
			ConfDate = fDate.getDate( aConfDate );
		}
		else
			ConfDate = null;
	}

	public String getPayee()
	{
		return Payee;
	}
	public void setPayee(String aPayee)
	{
            Payee = aPayee;
	}
	public String getPayeeType()
	{
		return PayeeType;
	}
	public void setPayeeType(String aPayeeType)
	{
            PayeeType = aPayeeType;
	}
	public String getPayGetDate()
	{
		if( PayGetDate != null )
			return fDate.getString(PayGetDate);
		else
			return null;
	}
	public void setPayGetDate(Date aPayGetDate)
	{
            PayGetDate = aPayGetDate;
	}
	public void setPayGetDate(String aPayGetDate)
	{
		if (aPayGetDate != null && !aPayGetDate.equals("") )
		{
			PayGetDate = fDate.getDate( aPayGetDate );
		}
		else
			PayGetDate = null;
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public double getInqFee()
	{
		return InqFee;
	}
	public void setInqFee(double aInqFee)
	{
            InqFee = Arith.round(aInqFee,2);
	}
	public void setInqFee(String aInqFee)
	{
		if (aInqFee != null && !aInqFee.equals(""))
		{
			Double tDouble = new Double(aInqFee);
			double d = tDouble.doubleValue();
                InqFee = Arith.round(d,2);
		}
	}

	public double getIndirectFee()
	{
		return IndirectFee;
	}
	public void setIndirectFee(double aIndirectFee)
	{
            IndirectFee = Arith.round(aIndirectFee,2);
	}
	public void setIndirectFee(String aIndirectFee)
	{
		if (aIndirectFee != null && !aIndirectFee.equals(""))
		{
			Double tDouble = new Double(aIndirectFee);
			double d = tDouble.doubleValue();
                IndirectFee = Arith.round(d,2);
		}
	}

	public double getSurveyFee()
	{
		return SurveyFee;
	}
	public void setSurveyFee(double aSurveyFee)
	{
            SurveyFee = Arith.round(aSurveyFee,2);
	}
	public void setSurveyFee(String aSurveyFee)
	{
		if (aSurveyFee != null && !aSurveyFee.equals(""))
		{
			Double tDouble = new Double(aSurveyFee);
			double d = tDouble.doubleValue();
                SurveyFee = Arith.round(d,2);
		}
	}

	public String getPayed()
	{
		return Payed;
	}
	public void setPayed(String aPayed)
	{
            Payed = aPayed;
	}

	/**
	* 使用另外一个 LLInqFeeStaSchema 对象给 Schema 赋值
	* @param: aLLInqFeeStaSchema LLInqFeeStaSchema
	**/
	public void setSchema(LLInqFeeStaSchema aLLInqFeeStaSchema)
	{
		this.OtherNo = aLLInqFeeStaSchema.getOtherNo();
		this.OtherNoType = aLLInqFeeStaSchema.getOtherNoType();
		this.SurveyNo = aLLInqFeeStaSchema.getSurveyNo();
		this.InqDept = aLLInqFeeStaSchema.getInqDept();
		this.ContSN = aLLInqFeeStaSchema.getContSN();
		this.Inputer = aLLInqFeeStaSchema.getInputer();
		this.InputDate = fDate.getDate( aLLInqFeeStaSchema.getInputDate());
		this.Confer = aLLInqFeeStaSchema.getConfer();
		this.ConfDate = fDate.getDate( aLLInqFeeStaSchema.getConfDate());
		this.Payee = aLLInqFeeStaSchema.getPayee();
		this.PayeeType = aLLInqFeeStaSchema.getPayeeType();
		this.PayGetDate = fDate.getDate( aLLInqFeeStaSchema.getPayGetDate());
		this.Remark = aLLInqFeeStaSchema.getRemark();
		this.Operator = aLLInqFeeStaSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLInqFeeStaSchema.getMakeDate());
		this.MakeTime = aLLInqFeeStaSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLInqFeeStaSchema.getModifyDate());
		this.ModifyTime = aLLInqFeeStaSchema.getModifyTime();
		this.InqFee = aLLInqFeeStaSchema.getInqFee();
		this.IndirectFee = aLLInqFeeStaSchema.getIndirectFee();
		this.SurveyFee = aLLInqFeeStaSchema.getSurveyFee();
		this.Payed = aLLInqFeeStaSchema.getPayed();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("OtherNo") == null )
				this.OtherNo = null;
			else
				this.OtherNo = rs.getString("OtherNo").trim();

			if( rs.getString("OtherNoType") == null )
				this.OtherNoType = null;
			else
				this.OtherNoType = rs.getString("OtherNoType").trim();

			if( rs.getString("SurveyNo") == null )
				this.SurveyNo = null;
			else
				this.SurveyNo = rs.getString("SurveyNo").trim();

			if( rs.getString("InqDept") == null )
				this.InqDept = null;
			else
				this.InqDept = rs.getString("InqDept").trim();

			this.ContSN = rs.getInt("ContSN");
			if( rs.getString("Inputer") == null )
				this.Inputer = null;
			else
				this.Inputer = rs.getString("Inputer").trim();

			this.InputDate = rs.getDate("InputDate");
			if( rs.getString("Confer") == null )
				this.Confer = null;
			else
				this.Confer = rs.getString("Confer").trim();

			this.ConfDate = rs.getDate("ConfDate");
			if( rs.getString("Payee") == null )
				this.Payee = null;
			else
				this.Payee = rs.getString("Payee").trim();

			if( rs.getString("PayeeType") == null )
				this.PayeeType = null;
			else
				this.PayeeType = rs.getString("PayeeType").trim();

			this.PayGetDate = rs.getDate("PayGetDate");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.InqFee = rs.getDouble("InqFee");
			this.IndirectFee = rs.getDouble("IndirectFee");
			this.SurveyFee = rs.getDouble("SurveyFee");
			if( rs.getString("Payed") == null )
				this.Payed = null;
			else
				this.Payed = rs.getString("Payed").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLInqFeeSta表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLInqFeeStaSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLInqFeeStaSchema getSchema()
	{
		LLInqFeeStaSchema aLLInqFeeStaSchema = new LLInqFeeStaSchema();
		aLLInqFeeStaSchema.setSchema(this);
		return aLLInqFeeStaSchema;
	}

	public LLInqFeeStaDB getDB()
	{
		LLInqFeeStaDB aDBOper = new LLInqFeeStaDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqFeeSta描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SurveyNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InqDept)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ContSN));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Inputer)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( InputDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Confer)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Payee)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayeeType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( PayGetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(InqFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IndirectFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SurveyFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Payed));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqFeeSta>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SurveyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InqDept = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContSN= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			Inputer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			Confer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			Payee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			PayeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			PayGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			InqFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			IndirectFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			SurveyFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			Payed = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLInqFeeStaSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("OtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
		}
		if (FCode.equals("OtherNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
		}
		if (FCode.equals("SurveyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyNo));
		}
		if (FCode.equals("InqDept"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InqDept));
		}
		if (FCode.equals("ContSN"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContSN));
		}
		if (FCode.equals("Inputer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Inputer));
		}
		if (FCode.equals("InputDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
		}
		if (FCode.equals("Confer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Confer));
		}
		if (FCode.equals("ConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
		}
		if (FCode.equals("Payee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Payee));
		}
		if (FCode.equals("PayeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayeeType));
		}
		if (FCode.equals("PayGetDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayGetDate()));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("InqFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InqFee));
		}
		if (FCode.equals("IndirectFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndirectFee));
		}
		if (FCode.equals("SurveyFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFee));
		}
		if (FCode.equals("Payed"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Payed));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(OtherNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(OtherNoType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SurveyNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(InqDept);
				break;
			case 4:
				strFieldValue = String.valueOf(ContSN);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Inputer);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Confer);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Payee);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(PayeeType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayGetDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 18:
				strFieldValue = String.valueOf(InqFee);
				break;
			case 19:
				strFieldValue = String.valueOf(IndirectFee);
				break;
			case 20:
				strFieldValue = String.valueOf(SurveyFee);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Payed);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("OtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo = FValue.trim();
			}
			else
				OtherNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNoType = FValue.trim();
			}
			else
				OtherNoType = null;
		}
		if (FCode.equalsIgnoreCase("SurveyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SurveyNo = FValue.trim();
			}
			else
				SurveyNo = null;
		}
		if (FCode.equalsIgnoreCase("InqDept"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InqDept = FValue.trim();
			}
			else
				InqDept = null;
		}
		if (FCode.equalsIgnoreCase("ContSN"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ContSN = i;
			}
		}
		if (FCode.equalsIgnoreCase("Inputer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Inputer = FValue.trim();
			}
			else
				Inputer = null;
		}
		if (FCode.equalsIgnoreCase("InputDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InputDate = fDate.getDate( FValue );
			}
			else
				InputDate = null;
		}
		if (FCode.equalsIgnoreCase("Confer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Confer = FValue.trim();
			}
			else
				Confer = null;
		}
		if (FCode.equalsIgnoreCase("ConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConfDate = fDate.getDate( FValue );
			}
			else
				ConfDate = null;
		}
		if (FCode.equalsIgnoreCase("Payee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Payee = FValue.trim();
			}
			else
				Payee = null;
		}
		if (FCode.equalsIgnoreCase("PayeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayeeType = FValue.trim();
			}
			else
				PayeeType = null;
		}
		if (FCode.equalsIgnoreCase("PayGetDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PayGetDate = fDate.getDate( FValue );
			}
			else
				PayGetDate = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("InqFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				InqFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("IndirectFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				IndirectFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SurveyFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SurveyFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Payed"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Payed = FValue.trim();
			}
			else
				Payed = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLInqFeeStaSchema other = (LLInqFeeStaSchema)otherObject;
		return
			OtherNo.equals(other.getOtherNo())
			&& OtherNoType.equals(other.getOtherNoType())
			&& SurveyNo.equals(other.getSurveyNo())
			&& InqDept.equals(other.getInqDept())
			&& ContSN == other.getContSN()
			&& Inputer.equals(other.getInputer())
			&& fDate.getString(InputDate).equals(other.getInputDate())
			&& Confer.equals(other.getConfer())
			&& fDate.getString(ConfDate).equals(other.getConfDate())
			&& Payee.equals(other.getPayee())
			&& PayeeType.equals(other.getPayeeType())
			&& fDate.getString(PayGetDate).equals(other.getPayGetDate())
			&& Remark.equals(other.getRemark())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& InqFee == other.getInqFee()
			&& IndirectFee == other.getIndirectFee()
			&& SurveyFee == other.getSurveyFee()
			&& Payed.equals(other.getPayed());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("OtherNo") ) {
			return 0;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return 1;
		}
		if( strFieldName.equals("SurveyNo") ) {
			return 2;
		}
		if( strFieldName.equals("InqDept") ) {
			return 3;
		}
		if( strFieldName.equals("ContSN") ) {
			return 4;
		}
		if( strFieldName.equals("Inputer") ) {
			return 5;
		}
		if( strFieldName.equals("InputDate") ) {
			return 6;
		}
		if( strFieldName.equals("Confer") ) {
			return 7;
		}
		if( strFieldName.equals("ConfDate") ) {
			return 8;
		}
		if( strFieldName.equals("Payee") ) {
			return 9;
		}
		if( strFieldName.equals("PayeeType") ) {
			return 10;
		}
		if( strFieldName.equals("PayGetDate") ) {
			return 11;
		}
		if( strFieldName.equals("Remark") ) {
			return 12;
		}
		if( strFieldName.equals("Operator") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 17;
		}
		if( strFieldName.equals("InqFee") ) {
			return 18;
		}
		if( strFieldName.equals("IndirectFee") ) {
			return 19;
		}
		if( strFieldName.equals("SurveyFee") ) {
			return 20;
		}
		if( strFieldName.equals("Payed") ) {
			return 21;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "OtherNo";
				break;
			case 1:
				strFieldName = "OtherNoType";
				break;
			case 2:
				strFieldName = "SurveyNo";
				break;
			case 3:
				strFieldName = "InqDept";
				break;
			case 4:
				strFieldName = "ContSN";
				break;
			case 5:
				strFieldName = "Inputer";
				break;
			case 6:
				strFieldName = "InputDate";
				break;
			case 7:
				strFieldName = "Confer";
				break;
			case 8:
				strFieldName = "ConfDate";
				break;
			case 9:
				strFieldName = "Payee";
				break;
			case 10:
				strFieldName = "PayeeType";
				break;
			case 11:
				strFieldName = "PayGetDate";
				break;
			case 12:
				strFieldName = "Remark";
				break;
			case 13:
				strFieldName = "Operator";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "ModifyDate";
				break;
			case 17:
				strFieldName = "ModifyTime";
				break;
			case 18:
				strFieldName = "InqFee";
				break;
			case 19:
				strFieldName = "IndirectFee";
				break;
			case 20:
				strFieldName = "SurveyFee";
				break;
			case 21:
				strFieldName = "Payed";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("OtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SurveyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InqDept") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContSN") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Inputer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Confer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ConfDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Payee") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayGetDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InqFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IndirectFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SurveyFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Payed") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
