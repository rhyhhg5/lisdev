/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LSTransOutInfoDB;

/*
 * <p>ClassName: LSTransOutInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-11-19
 */
public class LSTransOutInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 团单号 */
	private String GrpContNo;
	/** 保单号 */
	private String ContNo;
	/** 转出申请日期 */
	private Date ApplyDate;
	/** 平台转移编码 */
	private String PlatTransNo;
	/** 平台接收转移申请日期 */
	private Date PlatReceivDate;
	/** 转入保险公司 */
	private String InCompanyName;
	/** 对方开户行名称 */
	private String InBankName;
	/** 对方账号 */
	private String InBankAccNo;
	/** 对方联系人 */
	private String InContactName;
	/** 对方联系人电话 */
	private String InContactTele;
	/** 对方email */
	private String InContactEmail;
	/** 本保险公司名称 */
	private String CompanyName;
	/** 本公司转移联系人 */
	private String ContactName;
	/** 本公司联系人电话 */
	private String ContactTele;
	/** Email */
	private String ContactEmail;
	/** 转出登记日期 */
	private String RegDate;
	/** 预计终止日期 */
	private Date ExEndDate;
	/** 无法转出原因 */
	private String RejectReason;
	/** 转出状态 */
	private String TransOutState;
	/** 错误信息 */
	private String ErrorInfo;
	/** 登记上报标记 */
	private String RegFlag;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 最后修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 26;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LSTransOutInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ContNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LSTransOutInfoSchema cloned = (LSTransOutInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getApplyDate()
	{
		if( ApplyDate != null )
			return fDate.getString(ApplyDate);
		else
			return null;
	}
	public void setApplyDate(Date aApplyDate)
	{
		ApplyDate = aApplyDate;
	}
	public void setApplyDate(String aApplyDate)
	{
		if (aApplyDate != null && !aApplyDate.equals("") )
		{
			ApplyDate = fDate.getDate( aApplyDate );
		}
		else
			ApplyDate = null;
	}

	public String getPlatTransNo()
	{
		return PlatTransNo;
	}
	public void setPlatTransNo(String aPlatTransNo)
	{
		PlatTransNo = aPlatTransNo;
	}
	public String getPlatReceivDate()
	{
		if( PlatReceivDate != null )
			return fDate.getString(PlatReceivDate);
		else
			return null;
	}
	public void setPlatReceivDate(Date aPlatReceivDate)
	{
		PlatReceivDate = aPlatReceivDate;
	}
	public void setPlatReceivDate(String aPlatReceivDate)
	{
		if (aPlatReceivDate != null && !aPlatReceivDate.equals("") )
		{
			PlatReceivDate = fDate.getDate( aPlatReceivDate );
		}
		else
			PlatReceivDate = null;
	}

	public String getInCompanyName()
	{
		return InCompanyName;
	}
	public void setInCompanyName(String aInCompanyName)
	{
		InCompanyName = aInCompanyName;
	}
	public String getInBankName()
	{
		return InBankName;
	}
	public void setInBankName(String aInBankName)
	{
		InBankName = aInBankName;
	}
	public String getInBankAccNo()
	{
		return InBankAccNo;
	}
	public void setInBankAccNo(String aInBankAccNo)
	{
		InBankAccNo = aInBankAccNo;
	}
	public String getInContactName()
	{
		return InContactName;
	}
	public void setInContactName(String aInContactName)
	{
		InContactName = aInContactName;
	}
	public String getInContactTele()
	{
		return InContactTele;
	}
	public void setInContactTele(String aInContactTele)
	{
		InContactTele = aInContactTele;
	}
	public String getInContactEmail()
	{
		return InContactEmail;
	}
	public void setInContactEmail(String aInContactEmail)
	{
		InContactEmail = aInContactEmail;
	}
	public String getCompanyName()
	{
		return CompanyName;
	}
	public void setCompanyName(String aCompanyName)
	{
		CompanyName = aCompanyName;
	}
	public String getContactName()
	{
		return ContactName;
	}
	public void setContactName(String aContactName)
	{
		ContactName = aContactName;
	}
	public String getContactTele()
	{
		return ContactTele;
	}
	public void setContactTele(String aContactTele)
	{
		ContactTele = aContactTele;
	}
	public String getContactEmail()
	{
		return ContactEmail;
	}
	public void setContactEmail(String aContactEmail)
	{
		ContactEmail = aContactEmail;
	}
	public String getRegDate()
	{
		return RegDate;
	}
	public void setRegDate(String aRegDate)
	{
		RegDate = aRegDate;
	}
	public String getExEndDate()
	{
		if( ExEndDate != null )
			return fDate.getString(ExEndDate);
		else
			return null;
	}
	public void setExEndDate(Date aExEndDate)
	{
		ExEndDate = aExEndDate;
	}
	public void setExEndDate(String aExEndDate)
	{
		if (aExEndDate != null && !aExEndDate.equals("") )
		{
			ExEndDate = fDate.getDate( aExEndDate );
		}
		else
			ExEndDate = null;
	}

	public String getRejectReason()
	{
		return RejectReason;
	}
	public void setRejectReason(String aRejectReason)
	{
		RejectReason = aRejectReason;
	}
	public String getTransOutState()
	{
		return TransOutState;
	}
	public void setTransOutState(String aTransOutState)
	{
		TransOutState = aTransOutState;
	}
	public String getErrorInfo()
	{
		return ErrorInfo;
	}
	public void setErrorInfo(String aErrorInfo)
	{
		ErrorInfo = aErrorInfo;
	}
	public String getRegFlag()
	{
		return RegFlag;
	}
	public void setRegFlag(String aRegFlag)
	{
		RegFlag = aRegFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LSTransOutInfoSchema 对象给 Schema 赋值
	* @param: aLSTransOutInfoSchema LSTransOutInfoSchema
	**/
	public void setSchema(LSTransOutInfoSchema aLSTransOutInfoSchema)
	{
		this.GrpContNo = aLSTransOutInfoSchema.getGrpContNo();
		this.ContNo = aLSTransOutInfoSchema.getContNo();
		this.ApplyDate = fDate.getDate( aLSTransOutInfoSchema.getApplyDate());
		this.PlatTransNo = aLSTransOutInfoSchema.getPlatTransNo();
		this.PlatReceivDate = fDate.getDate( aLSTransOutInfoSchema.getPlatReceivDate());
		this.InCompanyName = aLSTransOutInfoSchema.getInCompanyName();
		this.InBankName = aLSTransOutInfoSchema.getInBankName();
		this.InBankAccNo = aLSTransOutInfoSchema.getInBankAccNo();
		this.InContactName = aLSTransOutInfoSchema.getInContactName();
		this.InContactTele = aLSTransOutInfoSchema.getInContactTele();
		this.InContactEmail = aLSTransOutInfoSchema.getInContactEmail();
		this.CompanyName = aLSTransOutInfoSchema.getCompanyName();
		this.ContactName = aLSTransOutInfoSchema.getContactName();
		this.ContactTele = aLSTransOutInfoSchema.getContactTele();
		this.ContactEmail = aLSTransOutInfoSchema.getContactEmail();
		this.RegDate = aLSTransOutInfoSchema.getRegDate();
		this.ExEndDate = fDate.getDate( aLSTransOutInfoSchema.getExEndDate());
		this.RejectReason = aLSTransOutInfoSchema.getRejectReason();
		this.TransOutState = aLSTransOutInfoSchema.getTransOutState();
		this.ErrorInfo = aLSTransOutInfoSchema.getErrorInfo();
		this.RegFlag = aLSTransOutInfoSchema.getRegFlag();
		this.Operator = aLSTransOutInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLSTransOutInfoSchema.getMakeDate());
		this.MakeTime = aLSTransOutInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLSTransOutInfoSchema.getModifyDate());
		this.ModifyTime = aLSTransOutInfoSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			this.ApplyDate = rs.getDate("ApplyDate");
			if( rs.getString("PlatTransNo") == null )
				this.PlatTransNo = null;
			else
				this.PlatTransNo = rs.getString("PlatTransNo").trim();

			this.PlatReceivDate = rs.getDate("PlatReceivDate");
			if( rs.getString("InCompanyName") == null )
				this.InCompanyName = null;
			else
				this.InCompanyName = rs.getString("InCompanyName").trim();

			if( rs.getString("InBankName") == null )
				this.InBankName = null;
			else
				this.InBankName = rs.getString("InBankName").trim();

			if( rs.getString("InBankAccNo") == null )
				this.InBankAccNo = null;
			else
				this.InBankAccNo = rs.getString("InBankAccNo").trim();

			if( rs.getString("InContactName") == null )
				this.InContactName = null;
			else
				this.InContactName = rs.getString("InContactName").trim();

			if( rs.getString("InContactTele") == null )
				this.InContactTele = null;
			else
				this.InContactTele = rs.getString("InContactTele").trim();

			if( rs.getString("InContactEmail") == null )
				this.InContactEmail = null;
			else
				this.InContactEmail = rs.getString("InContactEmail").trim();

			if( rs.getString("CompanyName") == null )
				this.CompanyName = null;
			else
				this.CompanyName = rs.getString("CompanyName").trim();

			if( rs.getString("ContactName") == null )
				this.ContactName = null;
			else
				this.ContactName = rs.getString("ContactName").trim();

			if( rs.getString("ContactTele") == null )
				this.ContactTele = null;
			else
				this.ContactTele = rs.getString("ContactTele").trim();

			if( rs.getString("ContactEmail") == null )
				this.ContactEmail = null;
			else
				this.ContactEmail = rs.getString("ContactEmail").trim();

			if( rs.getString("RegDate") == null )
				this.RegDate = null;
			else
				this.RegDate = rs.getString("RegDate").trim();

			this.ExEndDate = rs.getDate("ExEndDate");
			if( rs.getString("RejectReason") == null )
				this.RejectReason = null;
			else
				this.RejectReason = rs.getString("RejectReason").trim();

			if( rs.getString("TransOutState") == null )
				this.TransOutState = null;
			else
				this.TransOutState = rs.getString("TransOutState").trim();

			if( rs.getString("ErrorInfo") == null )
				this.ErrorInfo = null;
			else
				this.ErrorInfo = rs.getString("ErrorInfo").trim();

			if( rs.getString("RegFlag") == null )
				this.RegFlag = null;
			else
				this.RegFlag = rs.getString("RegFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LSTransOutInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LSTransOutInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LSTransOutInfoSchema getSchema()
	{
		LSTransOutInfoSchema aLSTransOutInfoSchema = new LSTransOutInfoSchema();
		aLSTransOutInfoSchema.setSchema(this);
		return aLSTransOutInfoSchema;
	}

	public LSTransOutInfoDB getDB()
	{
		LSTransOutInfoDB aDBOper = new LSTransOutInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLSTransOutInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PlatTransNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PlatReceivDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InCompanyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InBankName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InContactName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InContactTele)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InContactEmail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContactName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContactTele)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContactEmail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RegDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ExEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RejectReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransOutState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorInfo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RegFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLSTransOutInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			PlatTransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PlatReceivDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			InCompanyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			InBankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			InBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			InContactName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			InContactTele = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			InContactEmail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			CompanyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ContactName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ContactTele = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ContactEmail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			RegDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ExEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			RejectReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			TransOutState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ErrorInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			RegFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LSTransOutInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("ApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
		}
		if (FCode.equals("PlatTransNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlatTransNo));
		}
		if (FCode.equals("PlatReceivDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPlatReceivDate()));
		}
		if (FCode.equals("InCompanyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InCompanyName));
		}
		if (FCode.equals("InBankName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InBankName));
		}
		if (FCode.equals("InBankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InBankAccNo));
		}
		if (FCode.equals("InContactName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InContactName));
		}
		if (FCode.equals("InContactTele"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InContactTele));
		}
		if (FCode.equals("InContactEmail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InContactEmail));
		}
		if (FCode.equals("CompanyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyName));
		}
		if (FCode.equals("ContactName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContactName));
		}
		if (FCode.equals("ContactTele"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContactTele));
		}
		if (FCode.equals("ContactEmail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContactEmail));
		}
		if (FCode.equals("RegDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegDate));
		}
		if (FCode.equals("ExEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getExEndDate()));
		}
		if (FCode.equals("RejectReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RejectReason));
		}
		if (FCode.equals("TransOutState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransOutState));
		}
		if (FCode.equals("ErrorInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorInfo));
		}
		if (FCode.equals("RegFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PlatTransNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPlatReceivDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(InCompanyName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(InBankName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(InBankAccNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(InContactName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(InContactTele);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(InContactEmail);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(CompanyName);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ContactName);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ContactTele);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ContactEmail);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(RegDate);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getExEndDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(RejectReason);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(TransOutState);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ErrorInfo);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(RegFlag);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("ApplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ApplyDate = fDate.getDate( FValue );
			}
			else
				ApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("PlatTransNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PlatTransNo = FValue.trim();
			}
			else
				PlatTransNo = null;
		}
		if (FCode.equalsIgnoreCase("PlatReceivDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PlatReceivDate = fDate.getDate( FValue );
			}
			else
				PlatReceivDate = null;
		}
		if (FCode.equalsIgnoreCase("InCompanyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InCompanyName = FValue.trim();
			}
			else
				InCompanyName = null;
		}
		if (FCode.equalsIgnoreCase("InBankName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InBankName = FValue.trim();
			}
			else
				InBankName = null;
		}
		if (FCode.equalsIgnoreCase("InBankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InBankAccNo = FValue.trim();
			}
			else
				InBankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("InContactName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InContactName = FValue.trim();
			}
			else
				InContactName = null;
		}
		if (FCode.equalsIgnoreCase("InContactTele"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InContactTele = FValue.trim();
			}
			else
				InContactTele = null;
		}
		if (FCode.equalsIgnoreCase("InContactEmail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InContactEmail = FValue.trim();
			}
			else
				InContactEmail = null;
		}
		if (FCode.equalsIgnoreCase("CompanyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyName = FValue.trim();
			}
			else
				CompanyName = null;
		}
		if (FCode.equalsIgnoreCase("ContactName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContactName = FValue.trim();
			}
			else
				ContactName = null;
		}
		if (FCode.equalsIgnoreCase("ContactTele"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContactTele = FValue.trim();
			}
			else
				ContactTele = null;
		}
		if (FCode.equalsIgnoreCase("ContactEmail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContactEmail = FValue.trim();
			}
			else
				ContactEmail = null;
		}
		if (FCode.equalsIgnoreCase("RegDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RegDate = FValue.trim();
			}
			else
				RegDate = null;
		}
		if (FCode.equalsIgnoreCase("ExEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ExEndDate = fDate.getDate( FValue );
			}
			else
				ExEndDate = null;
		}
		if (FCode.equalsIgnoreCase("RejectReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RejectReason = FValue.trim();
			}
			else
				RejectReason = null;
		}
		if (FCode.equalsIgnoreCase("TransOutState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransOutState = FValue.trim();
			}
			else
				TransOutState = null;
		}
		if (FCode.equalsIgnoreCase("ErrorInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorInfo = FValue.trim();
			}
			else
				ErrorInfo = null;
		}
		if (FCode.equalsIgnoreCase("RegFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RegFlag = FValue.trim();
			}
			else
				RegFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LSTransOutInfoSchema other = (LSTransOutInfoSchema)otherObject;
		return
			(GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (ApplyDate == null ? other.getApplyDate() == null : fDate.getString(ApplyDate).equals(other.getApplyDate()))
			&& (PlatTransNo == null ? other.getPlatTransNo() == null : PlatTransNo.equals(other.getPlatTransNo()))
			&& (PlatReceivDate == null ? other.getPlatReceivDate() == null : fDate.getString(PlatReceivDate).equals(other.getPlatReceivDate()))
			&& (InCompanyName == null ? other.getInCompanyName() == null : InCompanyName.equals(other.getInCompanyName()))
			&& (InBankName == null ? other.getInBankName() == null : InBankName.equals(other.getInBankName()))
			&& (InBankAccNo == null ? other.getInBankAccNo() == null : InBankAccNo.equals(other.getInBankAccNo()))
			&& (InContactName == null ? other.getInContactName() == null : InContactName.equals(other.getInContactName()))
			&& (InContactTele == null ? other.getInContactTele() == null : InContactTele.equals(other.getInContactTele()))
			&& (InContactEmail == null ? other.getInContactEmail() == null : InContactEmail.equals(other.getInContactEmail()))
			&& (CompanyName == null ? other.getCompanyName() == null : CompanyName.equals(other.getCompanyName()))
			&& (ContactName == null ? other.getContactName() == null : ContactName.equals(other.getContactName()))
			&& (ContactTele == null ? other.getContactTele() == null : ContactTele.equals(other.getContactTele()))
			&& (ContactEmail == null ? other.getContactEmail() == null : ContactEmail.equals(other.getContactEmail()))
			&& (RegDate == null ? other.getRegDate() == null : RegDate.equals(other.getRegDate()))
			&& (ExEndDate == null ? other.getExEndDate() == null : fDate.getString(ExEndDate).equals(other.getExEndDate()))
			&& (RejectReason == null ? other.getRejectReason() == null : RejectReason.equals(other.getRejectReason()))
			&& (TransOutState == null ? other.getTransOutState() == null : TransOutState.equals(other.getTransOutState()))
			&& (ErrorInfo == null ? other.getErrorInfo() == null : ErrorInfo.equals(other.getErrorInfo()))
			&& (RegFlag == null ? other.getRegFlag() == null : RegFlag.equals(other.getRegFlag()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("ContNo") ) {
			return 1;
		}
		if( strFieldName.equals("ApplyDate") ) {
			return 2;
		}
		if( strFieldName.equals("PlatTransNo") ) {
			return 3;
		}
		if( strFieldName.equals("PlatReceivDate") ) {
			return 4;
		}
		if( strFieldName.equals("InCompanyName") ) {
			return 5;
		}
		if( strFieldName.equals("InBankName") ) {
			return 6;
		}
		if( strFieldName.equals("InBankAccNo") ) {
			return 7;
		}
		if( strFieldName.equals("InContactName") ) {
			return 8;
		}
		if( strFieldName.equals("InContactTele") ) {
			return 9;
		}
		if( strFieldName.equals("InContactEmail") ) {
			return 10;
		}
		if( strFieldName.equals("CompanyName") ) {
			return 11;
		}
		if( strFieldName.equals("ContactName") ) {
			return 12;
		}
		if( strFieldName.equals("ContactTele") ) {
			return 13;
		}
		if( strFieldName.equals("ContactEmail") ) {
			return 14;
		}
		if( strFieldName.equals("RegDate") ) {
			return 15;
		}
		if( strFieldName.equals("ExEndDate") ) {
			return 16;
		}
		if( strFieldName.equals("RejectReason") ) {
			return 17;
		}
		if( strFieldName.equals("TransOutState") ) {
			return 18;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return 19;
		}
		if( strFieldName.equals("RegFlag") ) {
			return 20;
		}
		if( strFieldName.equals("Operator") ) {
			return 21;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 25;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "ContNo";
				break;
			case 2:
				strFieldName = "ApplyDate";
				break;
			case 3:
				strFieldName = "PlatTransNo";
				break;
			case 4:
				strFieldName = "PlatReceivDate";
				break;
			case 5:
				strFieldName = "InCompanyName";
				break;
			case 6:
				strFieldName = "InBankName";
				break;
			case 7:
				strFieldName = "InBankAccNo";
				break;
			case 8:
				strFieldName = "InContactName";
				break;
			case 9:
				strFieldName = "InContactTele";
				break;
			case 10:
				strFieldName = "InContactEmail";
				break;
			case 11:
				strFieldName = "CompanyName";
				break;
			case 12:
				strFieldName = "ContactName";
				break;
			case 13:
				strFieldName = "ContactTele";
				break;
			case 14:
				strFieldName = "ContactEmail";
				break;
			case 15:
				strFieldName = "RegDate";
				break;
			case 16:
				strFieldName = "ExEndDate";
				break;
			case 17:
				strFieldName = "RejectReason";
				break;
			case 18:
				strFieldName = "TransOutState";
				break;
			case 19:
				strFieldName = "ErrorInfo";
				break;
			case 20:
				strFieldName = "RegFlag";
				break;
			case 21:
				strFieldName = "Operator";
				break;
			case 22:
				strFieldName = "MakeDate";
				break;
			case 23:
				strFieldName = "MakeTime";
				break;
			case 24:
				strFieldName = "ModifyDate";
				break;
			case 25:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PlatTransNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PlatReceivDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InCompanyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InBankName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InBankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InContactName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InContactTele") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InContactEmail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContactName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContactTele") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContactEmail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RejectReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransOutState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
