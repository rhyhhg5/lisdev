/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.WFInsuListDB;

/*
 * <p>ClassName: WFInsuListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 团险业务保险卡激活功能
 * @CreateDate：2019-07-31
 */
public class WFInsuListSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 保险卡号 */
	private String CardNo;
	/** 编号 */
	private String InsuNo;
	/** 与投保人关系 */
	private String ToAppntRela;
	/** 当前被保人是否连带 */
	private String RelationCode;
	/** 与主被保人关系 */
	private String RelationToInsured;
	/** 主被保人编号 */
	private String ReInsuNo;
	/** 被保人姓名 */
	private String Name;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private String Birthday;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 被保人职业类别 */
	private String OccupationType;
	/** 职业代码 */
	private String OccupationCode;
	/** 联系地址 */
	private String PostalAddress;
	/** 联系地址邮编 */
	private String ZipCode;
	/** 联系电话 */
	private String Phont;
	/** 手机 */
	private String Mobile;
	/** 电子邮箱 */
	private String Email;
	/** 单位名称 */
	private String GrpName;
	/** 单位电话 */
	private String CompanyPhone;
	/** 单位地址 */
	private String CompAddr;
	/** 单位地址邮编 */
	private String CompZipCode;
	/** 备用字段1 */
	private String Bak1;
	/** 备用字段2 */
	private String Bak2;
	/** 备用字段3 */
	private String Bak3;
	/** 备用字段4 */
	private String Bak4;
	/** 备用字段5 */
	private String Bak5;
	/** 备用字段6 */
	private String Bak6;
	/** 备用字段7 */
	private String Bak7;
	/** 备用字段8 */
	private String Bak8;
	/** 备用字段9 */
	private String Bak9;
	/** 备用字段10 */
	private String Bak10;
	/** 客户授权标识 */
	private String Authorization;
	/** 国籍 */
	private String NativePlace;
	/** 证件生效日期 */
	private Date IDStartDate;
	/** 证件失效日期 */
	private Date IDEndDate;
	/** 证件是否长期有效 */
	private String IDLongEffFlag;

	public static final int FIELDNUM = 38;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public WFInsuListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "BatchNo";
		pk[1] = "CardNo";
		pk[2] = "InsuNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		WFInsuListSchema cloned = (WFInsuListSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getCardNo()
	{
		return CardNo;
	}
	public void setCardNo(String aCardNo)
	{
		CardNo = aCardNo;
	}
	public String getInsuNo()
	{
		return InsuNo;
	}
	public void setInsuNo(String aInsuNo)
	{
		InsuNo = aInsuNo;
	}
	public String getToAppntRela()
	{
		return ToAppntRela;
	}
	public void setToAppntRela(String aToAppntRela)
	{
		ToAppntRela = aToAppntRela;
	}
	public String getRelationCode()
	{
		return RelationCode;
	}
	public void setRelationCode(String aRelationCode)
	{
		RelationCode = aRelationCode;
	}
	public String getRelationToInsured()
	{
		return RelationToInsured;
	}
	public void setRelationToInsured(String aRelationToInsured)
	{
		RelationToInsured = aRelationToInsured;
	}
	public String getReInsuNo()
	{
		return ReInsuNo;
	}
	public void setReInsuNo(String aReInsuNo)
	{
		ReInsuNo = aReInsuNo;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthday()
	{
		return Birthday;
	}
	public void setBirthday(String aBirthday)
	{
		Birthday = aBirthday;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public String getPostalAddress()
	{
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPhont()
	{
		return Phont;
	}
	public void setPhont(String aPhont)
	{
		Phont = aPhont;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getEmail()
	{
		return Email;
	}
	public void setEmail(String aEmail)
	{
		Email = aEmail;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getCompanyPhone()
	{
		return CompanyPhone;
	}
	public void setCompanyPhone(String aCompanyPhone)
	{
		CompanyPhone = aCompanyPhone;
	}
	public String getCompAddr()
	{
		return CompAddr;
	}
	public void setCompAddr(String aCompAddr)
	{
		CompAddr = aCompAddr;
	}
	public String getCompZipCode()
	{
		return CompZipCode;
	}
	public void setCompZipCode(String aCompZipCode)
	{
		CompZipCode = aCompZipCode;
	}
	public String getBak1()
	{
		return Bak1;
	}
	public void setBak1(String aBak1)
	{
		Bak1 = aBak1;
	}
	public String getBak2()
	{
		return Bak2;
	}
	public void setBak2(String aBak2)
	{
		Bak2 = aBak2;
	}
	public String getBak3()
	{
		return Bak3;
	}
	public void setBak3(String aBak3)
	{
		Bak3 = aBak3;
	}
	public String getBak4()
	{
		return Bak4;
	}
	public void setBak4(String aBak4)
	{
		Bak4 = aBak4;
	}
	public String getBak5()
	{
		return Bak5;
	}
	public void setBak5(String aBak5)
	{
		Bak5 = aBak5;
	}
	public String getBak6()
	{
		return Bak6;
	}
	public void setBak6(String aBak6)
	{
		Bak6 = aBak6;
	}
	public String getBak7()
	{
		return Bak7;
	}
	public void setBak7(String aBak7)
	{
		Bak7 = aBak7;
	}
	public String getBak8()
	{
		return Bak8;
	}
	public void setBak8(String aBak8)
	{
		Bak8 = aBak8;
	}
	public String getBak9()
	{
		return Bak9;
	}
	public void setBak9(String aBak9)
	{
		Bak9 = aBak9;
	}
	public String getBak10()
	{
		return Bak10;
	}
	public void setBak10(String aBak10)
	{
		Bak10 = aBak10;
	}
	public String getAuthorization()
	{
		return Authorization;
	}
	public void setAuthorization(String aAuthorization)
	{
		Authorization = aAuthorization;
	}
	public String getNativePlace()
	{
		return NativePlace;
	}
	public void setNativePlace(String aNativePlace)
	{
		NativePlace = aNativePlace;
	}
	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}

	public String getIDLongEffFlag()
	{
		return IDLongEffFlag;
	}
	public void setIDLongEffFlag(String aIDLongEffFlag)
	{
		IDLongEffFlag = aIDLongEffFlag;
	}

	/**
	* 使用另外一个 WFInsuListSchema 对象给 Schema 赋值
	* @param: aWFInsuListSchema WFInsuListSchema
	**/
	public void setSchema(WFInsuListSchema aWFInsuListSchema)
	{
		this.BatchNo = aWFInsuListSchema.getBatchNo();
		this.CardNo = aWFInsuListSchema.getCardNo();
		this.InsuNo = aWFInsuListSchema.getInsuNo();
		this.ToAppntRela = aWFInsuListSchema.getToAppntRela();
		this.RelationCode = aWFInsuListSchema.getRelationCode();
		this.RelationToInsured = aWFInsuListSchema.getRelationToInsured();
		this.ReInsuNo = aWFInsuListSchema.getReInsuNo();
		this.Name = aWFInsuListSchema.getName();
		this.Sex = aWFInsuListSchema.getSex();
		this.Birthday = aWFInsuListSchema.getBirthday();
		this.IDType = aWFInsuListSchema.getIDType();
		this.IDNo = aWFInsuListSchema.getIDNo();
		this.OccupationType = aWFInsuListSchema.getOccupationType();
		this.OccupationCode = aWFInsuListSchema.getOccupationCode();
		this.PostalAddress = aWFInsuListSchema.getPostalAddress();
		this.ZipCode = aWFInsuListSchema.getZipCode();
		this.Phont = aWFInsuListSchema.getPhont();
		this.Mobile = aWFInsuListSchema.getMobile();
		this.Email = aWFInsuListSchema.getEmail();
		this.GrpName = aWFInsuListSchema.getGrpName();
		this.CompanyPhone = aWFInsuListSchema.getCompanyPhone();
		this.CompAddr = aWFInsuListSchema.getCompAddr();
		this.CompZipCode = aWFInsuListSchema.getCompZipCode();
		this.Bak1 = aWFInsuListSchema.getBak1();
		this.Bak2 = aWFInsuListSchema.getBak2();
		this.Bak3 = aWFInsuListSchema.getBak3();
		this.Bak4 = aWFInsuListSchema.getBak4();
		this.Bak5 = aWFInsuListSchema.getBak5();
		this.Bak6 = aWFInsuListSchema.getBak6();
		this.Bak7 = aWFInsuListSchema.getBak7();
		this.Bak8 = aWFInsuListSchema.getBak8();
		this.Bak9 = aWFInsuListSchema.getBak9();
		this.Bak10 = aWFInsuListSchema.getBak10();
		this.Authorization = aWFInsuListSchema.getAuthorization();
		this.NativePlace = aWFInsuListSchema.getNativePlace();
		this.IDStartDate = fDate.getDate( aWFInsuListSchema.getIDStartDate());
		this.IDEndDate = fDate.getDate( aWFInsuListSchema.getIDEndDate());
		this.IDLongEffFlag = aWFInsuListSchema.getIDLongEffFlag();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("CardNo") == null )
				this.CardNo = null;
			else
				this.CardNo = rs.getString("CardNo").trim();

			if( rs.getString("InsuNo") == null )
				this.InsuNo = null;
			else
				this.InsuNo = rs.getString("InsuNo").trim();

			if( rs.getString("ToAppntRela") == null )
				this.ToAppntRela = null;
			else
				this.ToAppntRela = rs.getString("ToAppntRela").trim();

			if( rs.getString("RelationCode") == null )
				this.RelationCode = null;
			else
				this.RelationCode = rs.getString("RelationCode").trim();

			if( rs.getString("RelationToInsured") == null )
				this.RelationToInsured = null;
			else
				this.RelationToInsured = rs.getString("RelationToInsured").trim();

			if( rs.getString("ReInsuNo") == null )
				this.ReInsuNo = null;
			else
				this.ReInsuNo = rs.getString("ReInsuNo").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("Birthday") == null )
				this.Birthday = null;
			else
				this.Birthday = rs.getString("Birthday").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			if( rs.getString("PostalAddress") == null )
				this.PostalAddress = null;
			else
				this.PostalAddress = rs.getString("PostalAddress").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Phont") == null )
				this.Phont = null;
			else
				this.Phont = rs.getString("Phont").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("Email") == null )
				this.Email = null;
			else
				this.Email = rs.getString("Email").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("CompanyPhone") == null )
				this.CompanyPhone = null;
			else
				this.CompanyPhone = rs.getString("CompanyPhone").trim();

			if( rs.getString("CompAddr") == null )
				this.CompAddr = null;
			else
				this.CompAddr = rs.getString("CompAddr").trim();

			if( rs.getString("CompZipCode") == null )
				this.CompZipCode = null;
			else
				this.CompZipCode = rs.getString("CompZipCode").trim();

			if( rs.getString("Bak1") == null )
				this.Bak1 = null;
			else
				this.Bak1 = rs.getString("Bak1").trim();

			if( rs.getString("Bak2") == null )
				this.Bak2 = null;
			else
				this.Bak2 = rs.getString("Bak2").trim();

			if( rs.getString("Bak3") == null )
				this.Bak3 = null;
			else
				this.Bak3 = rs.getString("Bak3").trim();

			if( rs.getString("Bak4") == null )
				this.Bak4 = null;
			else
				this.Bak4 = rs.getString("Bak4").trim();

			if( rs.getString("Bak5") == null )
				this.Bak5 = null;
			else
				this.Bak5 = rs.getString("Bak5").trim();

			if( rs.getString("Bak6") == null )
				this.Bak6 = null;
			else
				this.Bak6 = rs.getString("Bak6").trim();

			if( rs.getString("Bak7") == null )
				this.Bak7 = null;
			else
				this.Bak7 = rs.getString("Bak7").trim();

			if( rs.getString("Bak8") == null )
				this.Bak8 = null;
			else
				this.Bak8 = rs.getString("Bak8").trim();

			if( rs.getString("Bak9") == null )
				this.Bak9 = null;
			else
				this.Bak9 = rs.getString("Bak9").trim();

			if( rs.getString("Bak10") == null )
				this.Bak10 = null;
			else
				this.Bak10 = rs.getString("Bak10").trim();

			if( rs.getString("Authorization") == null )
				this.Authorization = null;
			else
				this.Authorization = rs.getString("Authorization").trim();

			if( rs.getString("NativePlace") == null )
				this.NativePlace = null;
			else
				this.NativePlace = rs.getString("NativePlace").trim();

			this.IDStartDate = rs.getDate("IDStartDate");
			this.IDEndDate = rs.getDate("IDEndDate");
			if( rs.getString("IDLongEffFlag") == null )
				this.IDLongEffFlag = null;
			else
				this.IDLongEffFlag = rs.getString("IDLongEffFlag").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的WFInsuList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFInsuListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public WFInsuListSchema getSchema()
	{
		WFInsuListSchema aWFInsuListSchema = new WFInsuListSchema();
		aWFInsuListSchema.setSchema(this);
		return aWFInsuListSchema;
	}

	public WFInsuListDB getDB()
	{
		WFInsuListDB aDBOper = new WFInsuListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFInsuList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ToAppntRela)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationToInsured)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReInsuNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Birthday)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phont)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Email)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompAddr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak8)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak10)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Authorization)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDLongEffFlag));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFInsuList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			InsuNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ToAppntRela = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RelationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RelationToInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ReInsuNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Birthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Phont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			CompanyPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			CompAddr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			CompZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Bak6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Bak7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			Bak8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			Bak9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Bak10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Authorization = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,SysConst.PACKAGESPILTER));
			IDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
			IDLongEffFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFInsuListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("CardNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
		}
		if (FCode.equals("InsuNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuNo));
		}
		if (FCode.equals("ToAppntRela"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ToAppntRela));
		}
		if (FCode.equals("RelationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationCode));
		}
		if (FCode.equals("RelationToInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToInsured));
		}
		if (FCode.equals("ReInsuNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReInsuNo));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("PostalAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Phont"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phont));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("Email"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("CompanyPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyPhone));
		}
		if (FCode.equals("CompAddr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompAddr));
		}
		if (FCode.equals("CompZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompZipCode));
		}
		if (FCode.equals("Bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
		}
		if (FCode.equals("Bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
		}
		if (FCode.equals("Bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
		}
		if (FCode.equals("Bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak4));
		}
		if (FCode.equals("Bak5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak5));
		}
		if (FCode.equals("Bak6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak6));
		}
		if (FCode.equals("Bak7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak7));
		}
		if (FCode.equals("Bak8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak8));
		}
		if (FCode.equals("Bak9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak9));
		}
		if (FCode.equals("Bak10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak10));
		}
		if (FCode.equals("Authorization"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Authorization));
		}
		if (FCode.equals("NativePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
		}
		if (FCode.equals("IDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
		}
		if (FCode.equals("IDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
		}
		if (FCode.equals("IDLongEffFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDLongEffFlag));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CardNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(InsuNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ToAppntRela);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RelationCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RelationToInsured);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ReInsuNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Birthday);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(PostalAddress);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Phont);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Email);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(CompanyPhone);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(CompAddr);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(CompZipCode);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Bak1);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Bak2);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Bak3);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Bak4);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Bak5);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Bak6);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Bak7);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(Bak8);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(Bak9);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Bak10);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Authorization);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(NativePlace);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(IDLongEffFlag);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("CardNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardNo = FValue.trim();
			}
			else
				CardNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuNo = FValue.trim();
			}
			else
				InsuNo = null;
		}
		if (FCode.equalsIgnoreCase("ToAppntRela"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ToAppntRela = FValue.trim();
			}
			else
				ToAppntRela = null;
		}
		if (FCode.equalsIgnoreCase("RelationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationCode = FValue.trim();
			}
			else
				RelationCode = null;
		}
		if (FCode.equalsIgnoreCase("RelationToInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToInsured = FValue.trim();
			}
			else
				RelationToInsured = null;
		}
		if (FCode.equalsIgnoreCase("ReInsuNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReInsuNo = FValue.trim();
			}
			else
				ReInsuNo = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Birthday = FValue.trim();
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("PostalAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalAddress = FValue.trim();
			}
			else
				PostalAddress = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Phont"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phont = FValue.trim();
			}
			else
				Phont = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("Email"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Email = FValue.trim();
			}
			else
				Email = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("CompanyPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyPhone = FValue.trim();
			}
			else
				CompanyPhone = null;
		}
		if (FCode.equalsIgnoreCase("CompAddr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompAddr = FValue.trim();
			}
			else
				CompAddr = null;
		}
		if (FCode.equalsIgnoreCase("CompZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompZipCode = FValue.trim();
			}
			else
				CompZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak1 = FValue.trim();
			}
			else
				Bak1 = null;
		}
		if (FCode.equalsIgnoreCase("Bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak2 = FValue.trim();
			}
			else
				Bak2 = null;
		}
		if (FCode.equalsIgnoreCase("Bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak3 = FValue.trim();
			}
			else
				Bak3 = null;
		}
		if (FCode.equalsIgnoreCase("Bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak4 = FValue.trim();
			}
			else
				Bak4 = null;
		}
		if (FCode.equalsIgnoreCase("Bak5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak5 = FValue.trim();
			}
			else
				Bak5 = null;
		}
		if (FCode.equalsIgnoreCase("Bak6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak6 = FValue.trim();
			}
			else
				Bak6 = null;
		}
		if (FCode.equalsIgnoreCase("Bak7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak7 = FValue.trim();
			}
			else
				Bak7 = null;
		}
		if (FCode.equalsIgnoreCase("Bak8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak8 = FValue.trim();
			}
			else
				Bak8 = null;
		}
		if (FCode.equalsIgnoreCase("Bak9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak9 = FValue.trim();
			}
			else
				Bak9 = null;
		}
		if (FCode.equalsIgnoreCase("Bak10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak10 = FValue.trim();
			}
			else
				Bak10 = null;
		}
		if (FCode.equalsIgnoreCase("Authorization"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Authorization = FValue.trim();
			}
			else
				Authorization = null;
		}
		if (FCode.equalsIgnoreCase("NativePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativePlace = FValue.trim();
			}
			else
				NativePlace = null;
		}
		if (FCode.equalsIgnoreCase("IDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDStartDate = fDate.getDate( FValue );
			}
			else
				IDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("IDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDEndDate = fDate.getDate( FValue );
			}
			else
				IDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("IDLongEffFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDLongEffFlag = FValue.trim();
			}
			else
				IDLongEffFlag = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		WFInsuListSchema other = (WFInsuListSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (CardNo == null ? other.getCardNo() == null : CardNo.equals(other.getCardNo()))
			&& (InsuNo == null ? other.getInsuNo() == null : InsuNo.equals(other.getInsuNo()))
			&& (ToAppntRela == null ? other.getToAppntRela() == null : ToAppntRela.equals(other.getToAppntRela()))
			&& (RelationCode == null ? other.getRelationCode() == null : RelationCode.equals(other.getRelationCode()))
			&& (RelationToInsured == null ? other.getRelationToInsured() == null : RelationToInsured.equals(other.getRelationToInsured()))
			&& (ReInsuNo == null ? other.getReInsuNo() == null : ReInsuNo.equals(other.getReInsuNo()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Birthday == null ? other.getBirthday() == null : Birthday.equals(other.getBirthday()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& (PostalAddress == null ? other.getPostalAddress() == null : PostalAddress.equals(other.getPostalAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Phont == null ? other.getPhont() == null : Phont.equals(other.getPhont()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (Email == null ? other.getEmail() == null : Email.equals(other.getEmail()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (CompanyPhone == null ? other.getCompanyPhone() == null : CompanyPhone.equals(other.getCompanyPhone()))
			&& (CompAddr == null ? other.getCompAddr() == null : CompAddr.equals(other.getCompAddr()))
			&& (CompZipCode == null ? other.getCompZipCode() == null : CompZipCode.equals(other.getCompZipCode()))
			&& (Bak1 == null ? other.getBak1() == null : Bak1.equals(other.getBak1()))
			&& (Bak2 == null ? other.getBak2() == null : Bak2.equals(other.getBak2()))
			&& (Bak3 == null ? other.getBak3() == null : Bak3.equals(other.getBak3()))
			&& (Bak4 == null ? other.getBak4() == null : Bak4.equals(other.getBak4()))
			&& (Bak5 == null ? other.getBak5() == null : Bak5.equals(other.getBak5()))
			&& (Bak6 == null ? other.getBak6() == null : Bak6.equals(other.getBak6()))
			&& (Bak7 == null ? other.getBak7() == null : Bak7.equals(other.getBak7()))
			&& (Bak8 == null ? other.getBak8() == null : Bak8.equals(other.getBak8()))
			&& (Bak9 == null ? other.getBak9() == null : Bak9.equals(other.getBak9()))
			&& (Bak10 == null ? other.getBak10() == null : Bak10.equals(other.getBak10()))
			&& (Authorization == null ? other.getAuthorization() == null : Authorization.equals(other.getAuthorization()))
			&& (NativePlace == null ? other.getNativePlace() == null : NativePlace.equals(other.getNativePlace()))
			&& (IDStartDate == null ? other.getIDStartDate() == null : fDate.getString(IDStartDate).equals(other.getIDStartDate()))
			&& (IDEndDate == null ? other.getIDEndDate() == null : fDate.getString(IDEndDate).equals(other.getIDEndDate()))
			&& (IDLongEffFlag == null ? other.getIDLongEffFlag() == null : IDLongEffFlag.equals(other.getIDLongEffFlag()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("CardNo") ) {
			return 1;
		}
		if( strFieldName.equals("InsuNo") ) {
			return 2;
		}
		if( strFieldName.equals("ToAppntRela") ) {
			return 3;
		}
		if( strFieldName.equals("RelationCode") ) {
			return 4;
		}
		if( strFieldName.equals("RelationToInsured") ) {
			return 5;
		}
		if( strFieldName.equals("ReInsuNo") ) {
			return 6;
		}
		if( strFieldName.equals("Name") ) {
			return 7;
		}
		if( strFieldName.equals("Sex") ) {
			return 8;
		}
		if( strFieldName.equals("Birthday") ) {
			return 9;
		}
		if( strFieldName.equals("IDType") ) {
			return 10;
		}
		if( strFieldName.equals("IDNo") ) {
			return 11;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 12;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 13;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return 14;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 15;
		}
		if( strFieldName.equals("Phont") ) {
			return 16;
		}
		if( strFieldName.equals("Mobile") ) {
			return 17;
		}
		if( strFieldName.equals("Email") ) {
			return 18;
		}
		if( strFieldName.equals("GrpName") ) {
			return 19;
		}
		if( strFieldName.equals("CompanyPhone") ) {
			return 20;
		}
		if( strFieldName.equals("CompAddr") ) {
			return 21;
		}
		if( strFieldName.equals("CompZipCode") ) {
			return 22;
		}
		if( strFieldName.equals("Bak1") ) {
			return 23;
		}
		if( strFieldName.equals("Bak2") ) {
			return 24;
		}
		if( strFieldName.equals("Bak3") ) {
			return 25;
		}
		if( strFieldName.equals("Bak4") ) {
			return 26;
		}
		if( strFieldName.equals("Bak5") ) {
			return 27;
		}
		if( strFieldName.equals("Bak6") ) {
			return 28;
		}
		if( strFieldName.equals("Bak7") ) {
			return 29;
		}
		if( strFieldName.equals("Bak8") ) {
			return 30;
		}
		if( strFieldName.equals("Bak9") ) {
			return 31;
		}
		if( strFieldName.equals("Bak10") ) {
			return 32;
		}
		if( strFieldName.equals("Authorization") ) {
			return 33;
		}
		if( strFieldName.equals("NativePlace") ) {
			return 34;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return 35;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return 36;
		}
		if( strFieldName.equals("IDLongEffFlag") ) {
			return 37;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "CardNo";
				break;
			case 2:
				strFieldName = "InsuNo";
				break;
			case 3:
				strFieldName = "ToAppntRela";
				break;
			case 4:
				strFieldName = "RelationCode";
				break;
			case 5:
				strFieldName = "RelationToInsured";
				break;
			case 6:
				strFieldName = "ReInsuNo";
				break;
			case 7:
				strFieldName = "Name";
				break;
			case 8:
				strFieldName = "Sex";
				break;
			case 9:
				strFieldName = "Birthday";
				break;
			case 10:
				strFieldName = "IDType";
				break;
			case 11:
				strFieldName = "IDNo";
				break;
			case 12:
				strFieldName = "OccupationType";
				break;
			case 13:
				strFieldName = "OccupationCode";
				break;
			case 14:
				strFieldName = "PostalAddress";
				break;
			case 15:
				strFieldName = "ZipCode";
				break;
			case 16:
				strFieldName = "Phont";
				break;
			case 17:
				strFieldName = "Mobile";
				break;
			case 18:
				strFieldName = "Email";
				break;
			case 19:
				strFieldName = "GrpName";
				break;
			case 20:
				strFieldName = "CompanyPhone";
				break;
			case 21:
				strFieldName = "CompAddr";
				break;
			case 22:
				strFieldName = "CompZipCode";
				break;
			case 23:
				strFieldName = "Bak1";
				break;
			case 24:
				strFieldName = "Bak2";
				break;
			case 25:
				strFieldName = "Bak3";
				break;
			case 26:
				strFieldName = "Bak4";
				break;
			case 27:
				strFieldName = "Bak5";
				break;
			case 28:
				strFieldName = "Bak6";
				break;
			case 29:
				strFieldName = "Bak7";
				break;
			case 30:
				strFieldName = "Bak8";
				break;
			case 31:
				strFieldName = "Bak9";
				break;
			case 32:
				strFieldName = "Bak10";
				break;
			case 33:
				strFieldName = "Authorization";
				break;
			case 34:
				strFieldName = "NativePlace";
				break;
			case 35:
				strFieldName = "IDStartDate";
				break;
			case 36:
				strFieldName = "IDEndDate";
				break;
			case 37:
				strFieldName = "IDLongEffFlag";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ToAppntRela") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationToInsured") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReInsuNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phont") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Email") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompAddr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak10") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Authorization") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDLongEffFlag") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
