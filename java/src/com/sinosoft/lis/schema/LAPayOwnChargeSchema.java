/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAPayOwnChargeDB;

/*
 * <p>ClassName: LAPayOwnChargeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 互动2015佣金报送存储表
 * @CreateDate：2016-01-25
 */
public class LAPayOwnChargeSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String ContNo;
	/** 子公司代码 */
	private String CompCode;
	/** 子公司名称 */
	private String CompName;
	/** 业务活动日期 */
	private String CValiDate;
	/** 签单日期 */
	private String SignDate;
	/** 险种号码 */
	private String RiskCode;
	/** 险种名称 */
	private String RiskName;
	/** 业务单证号码 */
	private String ActNo;
	/** 佣金生成日期 */
	private String TmakeDate;
	/** 佣金类型代码 */
	private String FeePayTypeCode;
	/** 佣金类型名称 */
	private String FeePayTypeName;
	/** 应付业务佣金 */
	private double Charge;
	/** 应付业务佣金比例 */
	private double ChargeRate;
	/** 业务活动代码 */
	private String ActCode;
	/** 业务活动名称 */
	private String ActName;
	/** 委托方机构代码 */
	private String OrgBelgCode;
	/** 委托方机构名称 */
	private String OrgBelgName;
	/** 代理方机构代码 */
	private String OrgCrsCode;
	/** 代理方机构名称 */
	private String OrgCrsName;
	/** 代理方子公司代码 */
	private String OrgCrsCompCode;
	/** 委托方人员代码 */
	private String PolSalesCode;
	/** 委托方人员姓名 */
	private String PolSalesName;
	/** 代理方人员代码 */
	private String CrsSalesCode;
	/** 代理方人员姓名 */
	private String CrsSalesName;
	/** 客户名称 */
	private String CstName;
	/** 承保标的 */
	private String InsrTarg;
	/** 保费收入 */
	private double Prem;
	/** 报送时间 */
	private String DateSend;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 操作员代码 */
	private String Operator;
	/** 备用1 */
	private String F1;
	/** 备用2 */
	private String F2;
	/** 备用3 */
	private String F3;
	/** 备用4 */
	private String F4;
	/** 备用5 */
	private String F5;
	/** 备用6 */
	private Date F6;
	/** 备用7 */
	private Date F7;
	/** 备用8 */
	private Date F8;
	/** 备用9 */
	private String F9;
	/** 备用0 */
	private String F0;
	/** C1 */
	private double C1;
	/** C2 */
	private double C2;
	/** C3 */
	private double C3;
	/** R1 */
	private double R1;
	/** R2 */
	private double R2;
	/** R3 */
	private double R3;

	public static final int FIELDNUM = 49;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAPayOwnChargeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[8];
		pk[0] = "ContNo";
		pk[1] = "CompCode";
		pk[2] = "CValiDate";
		pk[3] = "SignDate";
		pk[4] = "RiskCode";
		pk[5] = "ActNo";
		pk[6] = "TmakeDate";
		pk[7] = "FeePayTypeCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAPayOwnChargeSchema cloned = (LAPayOwnChargeSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getCompCode()
	{
		return CompCode;
	}
	public void setCompCode(String aCompCode)
	{
		CompCode = aCompCode;
	}
	public String getCompName()
	{
		return CompName;
	}
	public void setCompName(String aCompName)
	{
		CompName = aCompName;
	}
	public String getCValiDate()
	{
		return CValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public String getSignDate()
	{
		return SignDate;
	}
	public void setSignDate(String aSignDate)
	{
		SignDate = aSignDate;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskName()
	{
		return RiskName;
	}
	public void setRiskName(String aRiskName)
	{
		RiskName = aRiskName;
	}
	public String getActNo()
	{
		return ActNo;
	}
	public void setActNo(String aActNo)
	{
		ActNo = aActNo;
	}
	public String getTmakeDate()
	{
		return TmakeDate;
	}
	public void setTmakeDate(String aTmakeDate)
	{
		TmakeDate = aTmakeDate;
	}
	public String getFeePayTypeCode()
	{
		return FeePayTypeCode;
	}
	public void setFeePayTypeCode(String aFeePayTypeCode)
	{
		FeePayTypeCode = aFeePayTypeCode;
	}
	public String getFeePayTypeName()
	{
		return FeePayTypeName;
	}
	public void setFeePayTypeName(String aFeePayTypeName)
	{
		FeePayTypeName = aFeePayTypeName;
	}
	public double getCharge()
	{
		return Charge;
	}
	public void setCharge(double aCharge)
	{
		Charge = Arith.round(aCharge,2);
	}
	public void setCharge(String aCharge)
	{
		if (aCharge != null && !aCharge.equals(""))
		{
			Double tDouble = new Double(aCharge);
			double d = tDouble.doubleValue();
                Charge = Arith.round(d,2);
		}
	}

	public double getChargeRate()
	{
		return ChargeRate;
	}
	public void setChargeRate(double aChargeRate)
	{
		ChargeRate = Arith.round(aChargeRate,4);
	}
	public void setChargeRate(String aChargeRate)
	{
		if (aChargeRate != null && !aChargeRate.equals(""))
		{
			Double tDouble = new Double(aChargeRate);
			double d = tDouble.doubleValue();
                ChargeRate = Arith.round(d,4);
		}
	}

	public String getActCode()
	{
		return ActCode;
	}
	public void setActCode(String aActCode)
	{
		ActCode = aActCode;
	}
	public String getActName()
	{
		return ActName;
	}
	public void setActName(String aActName)
	{
		ActName = aActName;
	}
	public String getOrgBelgCode()
	{
		return OrgBelgCode;
	}
	public void setOrgBelgCode(String aOrgBelgCode)
	{
		OrgBelgCode = aOrgBelgCode;
	}
	public String getOrgBelgName()
	{
		return OrgBelgName;
	}
	public void setOrgBelgName(String aOrgBelgName)
	{
		OrgBelgName = aOrgBelgName;
	}
	public String getOrgCrsCode()
	{
		return OrgCrsCode;
	}
	public void setOrgCrsCode(String aOrgCrsCode)
	{
		OrgCrsCode = aOrgCrsCode;
	}
	public String getOrgCrsName()
	{
		return OrgCrsName;
	}
	public void setOrgCrsName(String aOrgCrsName)
	{
		OrgCrsName = aOrgCrsName;
	}
	public String getOrgCrsCompCode()
	{
		return OrgCrsCompCode;
	}
	public void setOrgCrsCompCode(String aOrgCrsCompCode)
	{
		OrgCrsCompCode = aOrgCrsCompCode;
	}
	public String getPolSalesCode()
	{
		return PolSalesCode;
	}
	public void setPolSalesCode(String aPolSalesCode)
	{
		PolSalesCode = aPolSalesCode;
	}
	public String getPolSalesName()
	{
		return PolSalesName;
	}
	public void setPolSalesName(String aPolSalesName)
	{
		PolSalesName = aPolSalesName;
	}
	public String getCrsSalesCode()
	{
		return CrsSalesCode;
	}
	public void setCrsSalesCode(String aCrsSalesCode)
	{
		CrsSalesCode = aCrsSalesCode;
	}
	public String getCrsSalesName()
	{
		return CrsSalesName;
	}
	public void setCrsSalesName(String aCrsSalesName)
	{
		CrsSalesName = aCrsSalesName;
	}
	public String getCstName()
	{
		return CstName;
	}
	public void setCstName(String aCstName)
	{
		CstName = aCstName;
	}
	public String getInsrTarg()
	{
		return InsrTarg;
	}
	public void setInsrTarg(String aInsrTarg)
	{
		InsrTarg = aInsrTarg;
	}
	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public String getDateSend()
	{
		return DateSend;
	}
	public void setDateSend(String aDateSend)
	{
		DateSend = aDateSend;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getF1()
	{
		return F1;
	}
	public void setF1(String aF1)
	{
		F1 = aF1;
	}
	public String getF2()
	{
		return F2;
	}
	public void setF2(String aF2)
	{
		F2 = aF2;
	}
	public String getF3()
	{
		return F3;
	}
	public void setF3(String aF3)
	{
		F3 = aF3;
	}
	public String getF4()
	{
		return F4;
	}
	public void setF4(String aF4)
	{
		F4 = aF4;
	}
	public String getF5()
	{
		return F5;
	}
	public void setF5(String aF5)
	{
		F5 = aF5;
	}
	public String getF6()
	{
		if( F6 != null )
			return fDate.getString(F6);
		else
			return null;
	}
	public void setF6(Date aF6)
	{
		F6 = aF6;
	}
	public void setF6(String aF6)
	{
		if (aF6 != null && !aF6.equals("") )
		{
			F6 = fDate.getDate( aF6 );
		}
		else
			F6 = null;
	}

	public String getF7()
	{
		if( F7 != null )
			return fDate.getString(F7);
		else
			return null;
	}
	public void setF7(Date aF7)
	{
		F7 = aF7;
	}
	public void setF7(String aF7)
	{
		if (aF7 != null && !aF7.equals("") )
		{
			F7 = fDate.getDate( aF7 );
		}
		else
			F7 = null;
	}

	public String getF8()
	{
		if( F8 != null )
			return fDate.getString(F8);
		else
			return null;
	}
	public void setF8(Date aF8)
	{
		F8 = aF8;
	}
	public void setF8(String aF8)
	{
		if (aF8 != null && !aF8.equals("") )
		{
			F8 = fDate.getDate( aF8 );
		}
		else
			F8 = null;
	}

	public String getF9()
	{
		return F9;
	}
	public void setF9(String aF9)
	{
		F9 = aF9;
	}
	public String getF0()
	{
		return F0;
	}
	public void setF0(String aF0)
	{
		F0 = aF0;
	}
	public double getC1()
	{
		return C1;
	}
	public void setC1(double aC1)
	{
		C1 = Arith.round(aC1,2);
	}
	public void setC1(String aC1)
	{
		if (aC1 != null && !aC1.equals(""))
		{
			Double tDouble = new Double(aC1);
			double d = tDouble.doubleValue();
                C1 = Arith.round(d,2);
		}
	}

	public double getC2()
	{
		return C2;
	}
	public void setC2(double aC2)
	{
		C2 = Arith.round(aC2,2);
	}
	public void setC2(String aC2)
	{
		if (aC2 != null && !aC2.equals(""))
		{
			Double tDouble = new Double(aC2);
			double d = tDouble.doubleValue();
                C2 = Arith.round(d,2);
		}
	}

	public double getC3()
	{
		return C3;
	}
	public void setC3(double aC3)
	{
		C3 = Arith.round(aC3,2);
	}
	public void setC3(String aC3)
	{
		if (aC3 != null && !aC3.equals(""))
		{
			Double tDouble = new Double(aC3);
			double d = tDouble.doubleValue();
                C3 = Arith.round(d,2);
		}
	}

	public double getR1()
	{
		return R1;
	}
	public void setR1(double aR1)
	{
		R1 = Arith.round(aR1,4);
	}
	public void setR1(String aR1)
	{
		if (aR1 != null && !aR1.equals(""))
		{
			Double tDouble = new Double(aR1);
			double d = tDouble.doubleValue();
                R1 = Arith.round(d,4);
		}
	}

	public double getR2()
	{
		return R2;
	}
	public void setR2(double aR2)
	{
		R2 = Arith.round(aR2,4);
	}
	public void setR2(String aR2)
	{
		if (aR2 != null && !aR2.equals(""))
		{
			Double tDouble = new Double(aR2);
			double d = tDouble.doubleValue();
                R2 = Arith.round(d,4);
		}
	}

	public double getR3()
	{
		return R3;
	}
	public void setR3(double aR3)
	{
		R3 = Arith.round(aR3,4);
	}
	public void setR3(String aR3)
	{
		if (aR3 != null && !aR3.equals(""))
		{
			Double tDouble = new Double(aR3);
			double d = tDouble.doubleValue();
                R3 = Arith.round(d,4);
		}
	}


	/**
	* 使用另外一个 LAPayOwnChargeSchema 对象给 Schema 赋值
	* @param: aLAPayOwnChargeSchema LAPayOwnChargeSchema
	**/
	public void setSchema(LAPayOwnChargeSchema aLAPayOwnChargeSchema)
	{
		this.ContNo = aLAPayOwnChargeSchema.getContNo();
		this.CompCode = aLAPayOwnChargeSchema.getCompCode();
		this.CompName = aLAPayOwnChargeSchema.getCompName();
		this.CValiDate = aLAPayOwnChargeSchema.getCValiDate();
		this.SignDate = aLAPayOwnChargeSchema.getSignDate();
		this.RiskCode = aLAPayOwnChargeSchema.getRiskCode();
		this.RiskName = aLAPayOwnChargeSchema.getRiskName();
		this.ActNo = aLAPayOwnChargeSchema.getActNo();
		this.TmakeDate = aLAPayOwnChargeSchema.getTmakeDate();
		this.FeePayTypeCode = aLAPayOwnChargeSchema.getFeePayTypeCode();
		this.FeePayTypeName = aLAPayOwnChargeSchema.getFeePayTypeName();
		this.Charge = aLAPayOwnChargeSchema.getCharge();
		this.ChargeRate = aLAPayOwnChargeSchema.getChargeRate();
		this.ActCode = aLAPayOwnChargeSchema.getActCode();
		this.ActName = aLAPayOwnChargeSchema.getActName();
		this.OrgBelgCode = aLAPayOwnChargeSchema.getOrgBelgCode();
		this.OrgBelgName = aLAPayOwnChargeSchema.getOrgBelgName();
		this.OrgCrsCode = aLAPayOwnChargeSchema.getOrgCrsCode();
		this.OrgCrsName = aLAPayOwnChargeSchema.getOrgCrsName();
		this.OrgCrsCompCode = aLAPayOwnChargeSchema.getOrgCrsCompCode();
		this.PolSalesCode = aLAPayOwnChargeSchema.getPolSalesCode();
		this.PolSalesName = aLAPayOwnChargeSchema.getPolSalesName();
		this.CrsSalesCode = aLAPayOwnChargeSchema.getCrsSalesCode();
		this.CrsSalesName = aLAPayOwnChargeSchema.getCrsSalesName();
		this.CstName = aLAPayOwnChargeSchema.getCstName();
		this.InsrTarg = aLAPayOwnChargeSchema.getInsrTarg();
		this.Prem = aLAPayOwnChargeSchema.getPrem();
		this.DateSend = aLAPayOwnChargeSchema.getDateSend();
		this.MakeDate = fDate.getDate( aLAPayOwnChargeSchema.getMakeDate());
		this.MakeTime = aLAPayOwnChargeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAPayOwnChargeSchema.getModifyDate());
		this.ModifyTime = aLAPayOwnChargeSchema.getModifyTime();
		this.Operator = aLAPayOwnChargeSchema.getOperator();
		this.F1 = aLAPayOwnChargeSchema.getF1();
		this.F2 = aLAPayOwnChargeSchema.getF2();
		this.F3 = aLAPayOwnChargeSchema.getF3();
		this.F4 = aLAPayOwnChargeSchema.getF4();
		this.F5 = aLAPayOwnChargeSchema.getF5();
		this.F6 = fDate.getDate( aLAPayOwnChargeSchema.getF6());
		this.F7 = fDate.getDate( aLAPayOwnChargeSchema.getF7());
		this.F8 = fDate.getDate( aLAPayOwnChargeSchema.getF8());
		this.F9 = aLAPayOwnChargeSchema.getF9();
		this.F0 = aLAPayOwnChargeSchema.getF0();
		this.C1 = aLAPayOwnChargeSchema.getC1();
		this.C2 = aLAPayOwnChargeSchema.getC2();
		this.C3 = aLAPayOwnChargeSchema.getC3();
		this.R1 = aLAPayOwnChargeSchema.getR1();
		this.R2 = aLAPayOwnChargeSchema.getR2();
		this.R3 = aLAPayOwnChargeSchema.getR3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("CompCode") == null )
				this.CompCode = null;
			else
				this.CompCode = rs.getString("CompCode").trim();

			if( rs.getString("CompName") == null )
				this.CompName = null;
			else
				this.CompName = rs.getString("CompName").trim();

			if( rs.getString("CValiDate") == null )
				this.CValiDate = null;
			else
				this.CValiDate = rs.getString("CValiDate").trim();

			if( rs.getString("SignDate") == null )
				this.SignDate = null;
			else
				this.SignDate = rs.getString("SignDate").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskName") == null )
				this.RiskName = null;
			else
				this.RiskName = rs.getString("RiskName").trim();

			if( rs.getString("ActNo") == null )
				this.ActNo = null;
			else
				this.ActNo = rs.getString("ActNo").trim();

			if( rs.getString("TmakeDate") == null )
				this.TmakeDate = null;
			else
				this.TmakeDate = rs.getString("TmakeDate").trim();

			if( rs.getString("FeePayTypeCode") == null )
				this.FeePayTypeCode = null;
			else
				this.FeePayTypeCode = rs.getString("FeePayTypeCode").trim();

			if( rs.getString("FeePayTypeName") == null )
				this.FeePayTypeName = null;
			else
				this.FeePayTypeName = rs.getString("FeePayTypeName").trim();

			this.Charge = rs.getDouble("Charge");
			this.ChargeRate = rs.getDouble("ChargeRate");
			if( rs.getString("ActCode") == null )
				this.ActCode = null;
			else
				this.ActCode = rs.getString("ActCode").trim();

			if( rs.getString("ActName") == null )
				this.ActName = null;
			else
				this.ActName = rs.getString("ActName").trim();

			if( rs.getString("OrgBelgCode") == null )
				this.OrgBelgCode = null;
			else
				this.OrgBelgCode = rs.getString("OrgBelgCode").trim();

			if( rs.getString("OrgBelgName") == null )
				this.OrgBelgName = null;
			else
				this.OrgBelgName = rs.getString("OrgBelgName").trim();

			if( rs.getString("OrgCrsCode") == null )
				this.OrgCrsCode = null;
			else
				this.OrgCrsCode = rs.getString("OrgCrsCode").trim();

			if( rs.getString("OrgCrsName") == null )
				this.OrgCrsName = null;
			else
				this.OrgCrsName = rs.getString("OrgCrsName").trim();

			if( rs.getString("OrgCrsCompCode") == null )
				this.OrgCrsCompCode = null;
			else
				this.OrgCrsCompCode = rs.getString("OrgCrsCompCode").trim();

			if( rs.getString("PolSalesCode") == null )
				this.PolSalesCode = null;
			else
				this.PolSalesCode = rs.getString("PolSalesCode").trim();

			if( rs.getString("PolSalesName") == null )
				this.PolSalesName = null;
			else
				this.PolSalesName = rs.getString("PolSalesName").trim();

			if( rs.getString("CrsSalesCode") == null )
				this.CrsSalesCode = null;
			else
				this.CrsSalesCode = rs.getString("CrsSalesCode").trim();

			if( rs.getString("CrsSalesName") == null )
				this.CrsSalesName = null;
			else
				this.CrsSalesName = rs.getString("CrsSalesName").trim();

			if( rs.getString("CstName") == null )
				this.CstName = null;
			else
				this.CstName = rs.getString("CstName").trim();

			if( rs.getString("InsrTarg") == null )
				this.InsrTarg = null;
			else
				this.InsrTarg = rs.getString("InsrTarg").trim();

			this.Prem = rs.getDouble("Prem");
			if( rs.getString("DateSend") == null )
				this.DateSend = null;
			else
				this.DateSend = rs.getString("DateSend").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("F1") == null )
				this.F1 = null;
			else
				this.F1 = rs.getString("F1").trim();

			if( rs.getString("F2") == null )
				this.F2 = null;
			else
				this.F2 = rs.getString("F2").trim();

			if( rs.getString("F3") == null )
				this.F3 = null;
			else
				this.F3 = rs.getString("F3").trim();

			if( rs.getString("F4") == null )
				this.F4 = null;
			else
				this.F4 = rs.getString("F4").trim();

			if( rs.getString("F5") == null )
				this.F5 = null;
			else
				this.F5 = rs.getString("F5").trim();

			this.F6 = rs.getDate("F6");
			this.F7 = rs.getDate("F7");
			this.F8 = rs.getDate("F8");
			if( rs.getString("F9") == null )
				this.F9 = null;
			else
				this.F9 = rs.getString("F9").trim();

			if( rs.getString("F0") == null )
				this.F0 = null;
			else
				this.F0 = rs.getString("F0").trim();

			this.C1 = rs.getDouble("C1");
			this.C2 = rs.getDouble("C2");
			this.C3 = rs.getDouble("C3");
			this.R1 = rs.getDouble("R1");
			this.R2 = rs.getDouble("R2");
			this.R3 = rs.getDouble("R3");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAPayOwnCharge表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAPayOwnChargeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAPayOwnChargeSchema getSchema()
	{
		LAPayOwnChargeSchema aLAPayOwnChargeSchema = new LAPayOwnChargeSchema();
		aLAPayOwnChargeSchema.setSchema(this);
		return aLAPayOwnChargeSchema;
	}

	public LAPayOwnChargeDB getDB()
	{
		LAPayOwnChargeDB aDBOper = new LAPayOwnChargeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAPayOwnCharge描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TmakeDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeePayTypeCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeePayTypeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Charge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ChargeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgBelgCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgBelgName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgCrsCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgCrsName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgCrsCompCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolSalesCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolSalesName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CrsSalesCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CrsSalesName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsrTarg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DateSend)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F6 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F7 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F8 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F0)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAPayOwnCharge>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CompCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CompName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SignDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ActNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			TmakeDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FeePayTypeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			FeePayTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Charge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			ChargeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			ActCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ActName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			OrgBelgCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			OrgBelgName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			OrgCrsCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			OrgCrsName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			OrgCrsCompCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PolSalesCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			PolSalesName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			CrsSalesCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			CrsSalesName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			CstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			InsrTarg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			DateSend = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			F1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			F2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			F3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			F4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			F5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			F6 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			F7 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
			F8 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			F9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			F0 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			C1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,44,SysConst.PACKAGESPILTER))).doubleValue();
			C2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).doubleValue();
			C3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
			R1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).doubleValue();
			R2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).doubleValue();
			R3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAPayOwnChargeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("CompCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompCode));
		}
		if (FCode.equals("CompName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompName));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
		}
		if (FCode.equals("ActNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActNo));
		}
		if (FCode.equals("TmakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TmakeDate));
		}
		if (FCode.equals("FeePayTypeCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeePayTypeCode));
		}
		if (FCode.equals("FeePayTypeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeePayTypeName));
		}
		if (FCode.equals("Charge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Charge));
		}
		if (FCode.equals("ChargeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeRate));
		}
		if (FCode.equals("ActCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActCode));
		}
		if (FCode.equals("ActName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActName));
		}
		if (FCode.equals("OrgBelgCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgBelgCode));
		}
		if (FCode.equals("OrgBelgName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgBelgName));
		}
		if (FCode.equals("OrgCrsCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgCrsCode));
		}
		if (FCode.equals("OrgCrsName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgCrsName));
		}
		if (FCode.equals("OrgCrsCompCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgCrsCompCode));
		}
		if (FCode.equals("PolSalesCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolSalesCode));
		}
		if (FCode.equals("PolSalesName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolSalesName));
		}
		if (FCode.equals("CrsSalesCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CrsSalesCode));
		}
		if (FCode.equals("CrsSalesName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CrsSalesName));
		}
		if (FCode.equals("CstName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstName));
		}
		if (FCode.equals("InsrTarg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsrTarg));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("DateSend"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DateSend));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("F3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F3));
		}
		if (FCode.equals("F4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F4));
		}
		if (FCode.equals("F5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F5));
		}
		if (FCode.equals("F6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF6()));
		}
		if (FCode.equals("F7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF7()));
		}
		if (FCode.equals("F8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF8()));
		}
		if (FCode.equals("F9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F9));
		}
		if (FCode.equals("F0"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F0));
		}
		if (FCode.equals("C1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C1));
		}
		if (FCode.equals("C2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C2));
		}
		if (FCode.equals("C3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C3));
		}
		if (FCode.equals("R1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R1));
		}
		if (FCode.equals("R2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R2));
		}
		if (FCode.equals("R3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CompCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CompName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CValiDate);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SignDate);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(RiskName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ActNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(TmakeDate);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(FeePayTypeCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(FeePayTypeName);
				break;
			case 11:
				strFieldValue = String.valueOf(Charge);
				break;
			case 12:
				strFieldValue = String.valueOf(ChargeRate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ActCode);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ActName);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(OrgBelgCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(OrgBelgName);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(OrgCrsCode);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(OrgCrsName);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(OrgCrsCompCode);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PolSalesCode);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(PolSalesName);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(CrsSalesCode);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(CrsSalesName);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(CstName);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(InsrTarg);
				break;
			case 26:
				strFieldValue = String.valueOf(Prem);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(DateSend);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(F1);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(F2);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(F3);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(F4);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(F5);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF6()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF7()));
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF8()));
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(F9);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(F0);
				break;
			case 43:
				strFieldValue = String.valueOf(C1);
				break;
			case 44:
				strFieldValue = String.valueOf(C2);
				break;
			case 45:
				strFieldValue = String.valueOf(C3);
				break;
			case 46:
				strFieldValue = String.valueOf(R1);
				break;
			case 47:
				strFieldValue = String.valueOf(R2);
				break;
			case 48:
				strFieldValue = String.valueOf(R3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("CompCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompCode = FValue.trim();
			}
			else
				CompCode = null;
		}
		if (FCode.equalsIgnoreCase("CompName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompName = FValue.trim();
			}
			else
				CompName = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CValiDate = FValue.trim();
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignDate = FValue.trim();
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskName = FValue.trim();
			}
			else
				RiskName = null;
		}
		if (FCode.equalsIgnoreCase("ActNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActNo = FValue.trim();
			}
			else
				ActNo = null;
		}
		if (FCode.equalsIgnoreCase("TmakeDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TmakeDate = FValue.trim();
			}
			else
				TmakeDate = null;
		}
		if (FCode.equalsIgnoreCase("FeePayTypeCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeePayTypeCode = FValue.trim();
			}
			else
				FeePayTypeCode = null;
		}
		if (FCode.equalsIgnoreCase("FeePayTypeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeePayTypeName = FValue.trim();
			}
			else
				FeePayTypeName = null;
		}
		if (FCode.equalsIgnoreCase("Charge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Charge = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChargeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ChargeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ActCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActCode = FValue.trim();
			}
			else
				ActCode = null;
		}
		if (FCode.equalsIgnoreCase("ActName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActName = FValue.trim();
			}
			else
				ActName = null;
		}
		if (FCode.equalsIgnoreCase("OrgBelgCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgBelgCode = FValue.trim();
			}
			else
				OrgBelgCode = null;
		}
		if (FCode.equalsIgnoreCase("OrgBelgName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgBelgName = FValue.trim();
			}
			else
				OrgBelgName = null;
		}
		if (FCode.equalsIgnoreCase("OrgCrsCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgCrsCode = FValue.trim();
			}
			else
				OrgCrsCode = null;
		}
		if (FCode.equalsIgnoreCase("OrgCrsName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgCrsName = FValue.trim();
			}
			else
				OrgCrsName = null;
		}
		if (FCode.equalsIgnoreCase("OrgCrsCompCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgCrsCompCode = FValue.trim();
			}
			else
				OrgCrsCompCode = null;
		}
		if (FCode.equalsIgnoreCase("PolSalesCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolSalesCode = FValue.trim();
			}
			else
				PolSalesCode = null;
		}
		if (FCode.equalsIgnoreCase("PolSalesName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolSalesName = FValue.trim();
			}
			else
				PolSalesName = null;
		}
		if (FCode.equalsIgnoreCase("CrsSalesCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CrsSalesCode = FValue.trim();
			}
			else
				CrsSalesCode = null;
		}
		if (FCode.equalsIgnoreCase("CrsSalesName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CrsSalesName = FValue.trim();
			}
			else
				CrsSalesName = null;
		}
		if (FCode.equalsIgnoreCase("CstName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstName = FValue.trim();
			}
			else
				CstName = null;
		}
		if (FCode.equalsIgnoreCase("InsrTarg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsrTarg = FValue.trim();
			}
			else
				InsrTarg = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("DateSend"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DateSend = FValue.trim();
			}
			else
				DateSend = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F1 = FValue.trim();
			}
			else
				F1 = null;
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F2 = FValue.trim();
			}
			else
				F2 = null;
		}
		if (FCode.equalsIgnoreCase("F3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F3 = FValue.trim();
			}
			else
				F3 = null;
		}
		if (FCode.equalsIgnoreCase("F4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F4 = FValue.trim();
			}
			else
				F4 = null;
		}
		if (FCode.equalsIgnoreCase("F5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F5 = FValue.trim();
			}
			else
				F5 = null;
		}
		if (FCode.equalsIgnoreCase("F6"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F6 = fDate.getDate( FValue );
			}
			else
				F6 = null;
		}
		if (FCode.equalsIgnoreCase("F7"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F7 = fDate.getDate( FValue );
			}
			else
				F7 = null;
		}
		if (FCode.equalsIgnoreCase("F8"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F8 = fDate.getDate( FValue );
			}
			else
				F8 = null;
		}
		if (FCode.equalsIgnoreCase("F9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F9 = FValue.trim();
			}
			else
				F9 = null;
		}
		if (FCode.equalsIgnoreCase("F0"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F0 = FValue.trim();
			}
			else
				F0 = null;
		}
		if (FCode.equalsIgnoreCase("C1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("C2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("C3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R3 = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAPayOwnChargeSchema other = (LAPayOwnChargeSchema)otherObject;
		return
			(ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (CompCode == null ? other.getCompCode() == null : CompCode.equals(other.getCompCode()))
			&& (CompName == null ? other.getCompName() == null : CompName.equals(other.getCompName()))
			&& (CValiDate == null ? other.getCValiDate() == null : CValiDate.equals(other.getCValiDate()))
			&& (SignDate == null ? other.getSignDate() == null : SignDate.equals(other.getSignDate()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskName == null ? other.getRiskName() == null : RiskName.equals(other.getRiskName()))
			&& (ActNo == null ? other.getActNo() == null : ActNo.equals(other.getActNo()))
			&& (TmakeDate == null ? other.getTmakeDate() == null : TmakeDate.equals(other.getTmakeDate()))
			&& (FeePayTypeCode == null ? other.getFeePayTypeCode() == null : FeePayTypeCode.equals(other.getFeePayTypeCode()))
			&& (FeePayTypeName == null ? other.getFeePayTypeName() == null : FeePayTypeName.equals(other.getFeePayTypeName()))
			&& Charge == other.getCharge()
			&& ChargeRate == other.getChargeRate()
			&& (ActCode == null ? other.getActCode() == null : ActCode.equals(other.getActCode()))
			&& (ActName == null ? other.getActName() == null : ActName.equals(other.getActName()))
			&& (OrgBelgCode == null ? other.getOrgBelgCode() == null : OrgBelgCode.equals(other.getOrgBelgCode()))
			&& (OrgBelgName == null ? other.getOrgBelgName() == null : OrgBelgName.equals(other.getOrgBelgName()))
			&& (OrgCrsCode == null ? other.getOrgCrsCode() == null : OrgCrsCode.equals(other.getOrgCrsCode()))
			&& (OrgCrsName == null ? other.getOrgCrsName() == null : OrgCrsName.equals(other.getOrgCrsName()))
			&& (OrgCrsCompCode == null ? other.getOrgCrsCompCode() == null : OrgCrsCompCode.equals(other.getOrgCrsCompCode()))
			&& (PolSalesCode == null ? other.getPolSalesCode() == null : PolSalesCode.equals(other.getPolSalesCode()))
			&& (PolSalesName == null ? other.getPolSalesName() == null : PolSalesName.equals(other.getPolSalesName()))
			&& (CrsSalesCode == null ? other.getCrsSalesCode() == null : CrsSalesCode.equals(other.getCrsSalesCode()))
			&& (CrsSalesName == null ? other.getCrsSalesName() == null : CrsSalesName.equals(other.getCrsSalesName()))
			&& (CstName == null ? other.getCstName() == null : CstName.equals(other.getCstName()))
			&& (InsrTarg == null ? other.getInsrTarg() == null : InsrTarg.equals(other.getInsrTarg()))
			&& Prem == other.getPrem()
			&& (DateSend == null ? other.getDateSend() == null : DateSend.equals(other.getDateSend()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (F1 == null ? other.getF1() == null : F1.equals(other.getF1()))
			&& (F2 == null ? other.getF2() == null : F2.equals(other.getF2()))
			&& (F3 == null ? other.getF3() == null : F3.equals(other.getF3()))
			&& (F4 == null ? other.getF4() == null : F4.equals(other.getF4()))
			&& (F5 == null ? other.getF5() == null : F5.equals(other.getF5()))
			&& (F6 == null ? other.getF6() == null : fDate.getString(F6).equals(other.getF6()))
			&& (F7 == null ? other.getF7() == null : fDate.getString(F7).equals(other.getF7()))
			&& (F8 == null ? other.getF8() == null : fDate.getString(F8).equals(other.getF8()))
			&& (F9 == null ? other.getF9() == null : F9.equals(other.getF9()))
			&& (F0 == null ? other.getF0() == null : F0.equals(other.getF0()))
			&& C1 == other.getC1()
			&& C2 == other.getC2()
			&& C3 == other.getC3()
			&& R1 == other.getR1()
			&& R2 == other.getR2()
			&& R3 == other.getR3();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ContNo") ) {
			return 0;
		}
		if( strFieldName.equals("CompCode") ) {
			return 1;
		}
		if( strFieldName.equals("CompName") ) {
			return 2;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 3;
		}
		if( strFieldName.equals("SignDate") ) {
			return 4;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 5;
		}
		if( strFieldName.equals("RiskName") ) {
			return 6;
		}
		if( strFieldName.equals("ActNo") ) {
			return 7;
		}
		if( strFieldName.equals("TmakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("FeePayTypeCode") ) {
			return 9;
		}
		if( strFieldName.equals("FeePayTypeName") ) {
			return 10;
		}
		if( strFieldName.equals("Charge") ) {
			return 11;
		}
		if( strFieldName.equals("ChargeRate") ) {
			return 12;
		}
		if( strFieldName.equals("ActCode") ) {
			return 13;
		}
		if( strFieldName.equals("ActName") ) {
			return 14;
		}
		if( strFieldName.equals("OrgBelgCode") ) {
			return 15;
		}
		if( strFieldName.equals("OrgBelgName") ) {
			return 16;
		}
		if( strFieldName.equals("OrgCrsCode") ) {
			return 17;
		}
		if( strFieldName.equals("OrgCrsName") ) {
			return 18;
		}
		if( strFieldName.equals("OrgCrsCompCode") ) {
			return 19;
		}
		if( strFieldName.equals("PolSalesCode") ) {
			return 20;
		}
		if( strFieldName.equals("PolSalesName") ) {
			return 21;
		}
		if( strFieldName.equals("CrsSalesCode") ) {
			return 22;
		}
		if( strFieldName.equals("CrsSalesName") ) {
			return 23;
		}
		if( strFieldName.equals("CstName") ) {
			return 24;
		}
		if( strFieldName.equals("InsrTarg") ) {
			return 25;
		}
		if( strFieldName.equals("Prem") ) {
			return 26;
		}
		if( strFieldName.equals("DateSend") ) {
			return 27;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 28;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 29;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 30;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 31;
		}
		if( strFieldName.equals("Operator") ) {
			return 32;
		}
		if( strFieldName.equals("F1") ) {
			return 33;
		}
		if( strFieldName.equals("F2") ) {
			return 34;
		}
		if( strFieldName.equals("F3") ) {
			return 35;
		}
		if( strFieldName.equals("F4") ) {
			return 36;
		}
		if( strFieldName.equals("F5") ) {
			return 37;
		}
		if( strFieldName.equals("F6") ) {
			return 38;
		}
		if( strFieldName.equals("F7") ) {
			return 39;
		}
		if( strFieldName.equals("F8") ) {
			return 40;
		}
		if( strFieldName.equals("F9") ) {
			return 41;
		}
		if( strFieldName.equals("F0") ) {
			return 42;
		}
		if( strFieldName.equals("C1") ) {
			return 43;
		}
		if( strFieldName.equals("C2") ) {
			return 44;
		}
		if( strFieldName.equals("C3") ) {
			return 45;
		}
		if( strFieldName.equals("R1") ) {
			return 46;
		}
		if( strFieldName.equals("R2") ) {
			return 47;
		}
		if( strFieldName.equals("R3") ) {
			return 48;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ContNo";
				break;
			case 1:
				strFieldName = "CompCode";
				break;
			case 2:
				strFieldName = "CompName";
				break;
			case 3:
				strFieldName = "CValiDate";
				break;
			case 4:
				strFieldName = "SignDate";
				break;
			case 5:
				strFieldName = "RiskCode";
				break;
			case 6:
				strFieldName = "RiskName";
				break;
			case 7:
				strFieldName = "ActNo";
				break;
			case 8:
				strFieldName = "TmakeDate";
				break;
			case 9:
				strFieldName = "FeePayTypeCode";
				break;
			case 10:
				strFieldName = "FeePayTypeName";
				break;
			case 11:
				strFieldName = "Charge";
				break;
			case 12:
				strFieldName = "ChargeRate";
				break;
			case 13:
				strFieldName = "ActCode";
				break;
			case 14:
				strFieldName = "ActName";
				break;
			case 15:
				strFieldName = "OrgBelgCode";
				break;
			case 16:
				strFieldName = "OrgBelgName";
				break;
			case 17:
				strFieldName = "OrgCrsCode";
				break;
			case 18:
				strFieldName = "OrgCrsName";
				break;
			case 19:
				strFieldName = "OrgCrsCompCode";
				break;
			case 20:
				strFieldName = "PolSalesCode";
				break;
			case 21:
				strFieldName = "PolSalesName";
				break;
			case 22:
				strFieldName = "CrsSalesCode";
				break;
			case 23:
				strFieldName = "CrsSalesName";
				break;
			case 24:
				strFieldName = "CstName";
				break;
			case 25:
				strFieldName = "InsrTarg";
				break;
			case 26:
				strFieldName = "Prem";
				break;
			case 27:
				strFieldName = "DateSend";
				break;
			case 28:
				strFieldName = "MakeDate";
				break;
			case 29:
				strFieldName = "MakeTime";
				break;
			case 30:
				strFieldName = "ModifyDate";
				break;
			case 31:
				strFieldName = "ModifyTime";
				break;
			case 32:
				strFieldName = "Operator";
				break;
			case 33:
				strFieldName = "F1";
				break;
			case 34:
				strFieldName = "F2";
				break;
			case 35:
				strFieldName = "F3";
				break;
			case 36:
				strFieldName = "F4";
				break;
			case 37:
				strFieldName = "F5";
				break;
			case 38:
				strFieldName = "F6";
				break;
			case 39:
				strFieldName = "F7";
				break;
			case 40:
				strFieldName = "F8";
				break;
			case 41:
				strFieldName = "F9";
				break;
			case 42:
				strFieldName = "F0";
				break;
			case 43:
				strFieldName = "C1";
				break;
			case 44:
				strFieldName = "C2";
				break;
			case 45:
				strFieldName = "C3";
				break;
			case 46:
				strFieldName = "R1";
				break;
			case 47:
				strFieldName = "R2";
				break;
			case 48:
				strFieldName = "R3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TmakeDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeePayTypeCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeePayTypeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Charge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChargeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ActCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgBelgCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgBelgName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgCrsCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgCrsName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgCrsCompCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolSalesCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolSalesName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CrsSalesCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CrsSalesName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsrTarg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DateSend") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F6") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F7") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F8") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F0") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("C1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("C2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("C3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R3") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 45:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 47:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 48:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
