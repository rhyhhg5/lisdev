/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LSTransInfoDB;

/*
 * <p>ClassName: LSTransInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: SY
 * @CreateDate：2015-10-29
 */
public class LSTransInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 对应本方印刷号 */
	private String PrtNo;
	/** 转入保险公司 */
	private String CompanyNo;
	/** 转入保单类型 */
	private String ContType;
	/** 转入保单号 */
	private String TransGrpContNo;
	/** 转入分单号 */
	private String TransContNo;
	/** 联系人代码 */
	private String AgentCode;
	/** 转入联系人 */
	private String Name;
	/** 联系人电话 */
	private String Phone;
	/** 联系人email */
	private String Email;
	/** 本方银行编码 */
	private String BankCode;
	/** 本方银行名称 */
	private String BankName;
	/** 本方银行账户名 */
	private String AccName;
	/** 本方银行帐号 */
	private String AccNo;
	/** 转入状态 */
	private String StateFlag;
	/** 录入员 */
	private String InputOperator;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 申请转入日期 */
	private Date ApplyDate;
	/** 保单转移编码 */
	private String TransNo;
	/** 申请成功标识 */
	private String SuccFlag;
	/** 申请失败原因 */
	private String ErrorInfo;
	/** 预计终止时间 */
	private Date ExpectedDate;
	/** 无法转出的原因 */
	private String RejectReason;
	/** 平台接收日期 */
	private Date ReceivedDate;
	/** 转入余额 */
	private double TransAmount;

	public static final int FIELDNUM = 28;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LSTransInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LSTransInfoSchema cloned = (LSTransInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getCompanyNo()
	{
		return CompanyNo;
	}
	public void setCompanyNo(String aCompanyNo)
	{
		CompanyNo = aCompanyNo;
	}
	public String getContType()
	{
		return ContType;
	}
	public void setContType(String aContType)
	{
		ContType = aContType;
	}
	public String getTransGrpContNo()
	{
		return TransGrpContNo;
	}
	public void setTransGrpContNo(String aTransGrpContNo)
	{
		TransGrpContNo = aTransGrpContNo;
	}
	public String getTransContNo()
	{
		return TransContNo;
	}
	public void setTransContNo(String aTransContNo)
	{
		TransContNo = aTransContNo;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getEmail()
	{
		return Email;
	}
	public void setEmail(String aEmail)
	{
		Email = aEmail;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankName()
	{
		return BankName;
	}
	public void setBankName(String aBankName)
	{
		BankName = aBankName;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getAccNo()
	{
		return AccNo;
	}
	public void setAccNo(String aAccNo)
	{
		AccNo = aAccNo;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getInputOperator()
	{
		return InputOperator;
	}
	public void setInputOperator(String aInputOperator)
	{
		InputOperator = aInputOperator;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getApplyDate()
	{
		if( ApplyDate != null )
			return fDate.getString(ApplyDate);
		else
			return null;
	}
	public void setApplyDate(Date aApplyDate)
	{
		ApplyDate = aApplyDate;
	}
	public void setApplyDate(String aApplyDate)
	{
		if (aApplyDate != null && !aApplyDate.equals("") )
		{
			ApplyDate = fDate.getDate( aApplyDate );
		}
		else
			ApplyDate = null;
	}

	public String getTransNo()
	{
		return TransNo;
	}
	public void setTransNo(String aTransNo)
	{
		TransNo = aTransNo;
	}
	public String getSuccFlag()
	{
		return SuccFlag;
	}
	public void setSuccFlag(String aSuccFlag)
	{
		SuccFlag = aSuccFlag;
	}
	public String getErrorInfo()
	{
		return ErrorInfo;
	}
	public void setErrorInfo(String aErrorInfo)
	{
		ErrorInfo = aErrorInfo;
	}
	public String getExpectedDate()
	{
		if( ExpectedDate != null )
			return fDate.getString(ExpectedDate);
		else
			return null;
	}
	public void setExpectedDate(Date aExpectedDate)
	{
		ExpectedDate = aExpectedDate;
	}
	public void setExpectedDate(String aExpectedDate)
	{
		if (aExpectedDate != null && !aExpectedDate.equals("") )
		{
			ExpectedDate = fDate.getDate( aExpectedDate );
		}
		else
			ExpectedDate = null;
	}

	public String getRejectReason()
	{
		return RejectReason;
	}
	public void setRejectReason(String aRejectReason)
	{
		RejectReason = aRejectReason;
	}
	public String getReceivedDate()
	{
		if( ReceivedDate != null )
			return fDate.getString(ReceivedDate);
		else
			return null;
	}
	public void setReceivedDate(Date aReceivedDate)
	{
		ReceivedDate = aReceivedDate;
	}
	public void setReceivedDate(String aReceivedDate)
	{
		if (aReceivedDate != null && !aReceivedDate.equals("") )
		{
			ReceivedDate = fDate.getDate( aReceivedDate );
		}
		else
			ReceivedDate = null;
	}

	public double getTransAmount()
	{
		return TransAmount;
	}
	public void setTransAmount(double aTransAmount)
	{
		TransAmount = Arith.round(aTransAmount,2);
	}
	public void setTransAmount(String aTransAmount)
	{
		if (aTransAmount != null && !aTransAmount.equals(""))
		{
			Double tDouble = new Double(aTransAmount);
			double d = tDouble.doubleValue();
                TransAmount = Arith.round(d,2);
		}
	}


	/**
	* 使用另外一个 LSTransInfoSchema 对象给 Schema 赋值
	* @param: aLSTransInfoSchema LSTransInfoSchema
	**/
	public void setSchema(LSTransInfoSchema aLSTransInfoSchema)
	{
		this.PrtNo = aLSTransInfoSchema.getPrtNo();
		this.CompanyNo = aLSTransInfoSchema.getCompanyNo();
		this.ContType = aLSTransInfoSchema.getContType();
		this.TransGrpContNo = aLSTransInfoSchema.getTransGrpContNo();
		this.TransContNo = aLSTransInfoSchema.getTransContNo();
		this.AgentCode = aLSTransInfoSchema.getAgentCode();
		this.Name = aLSTransInfoSchema.getName();
		this.Phone = aLSTransInfoSchema.getPhone();
		this.Email = aLSTransInfoSchema.getEmail();
		this.BankCode = aLSTransInfoSchema.getBankCode();
		this.BankName = aLSTransInfoSchema.getBankName();
		this.AccName = aLSTransInfoSchema.getAccName();
		this.AccNo = aLSTransInfoSchema.getAccNo();
		this.StateFlag = aLSTransInfoSchema.getStateFlag();
		this.InputOperator = aLSTransInfoSchema.getInputOperator();
		this.Operator = aLSTransInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLSTransInfoSchema.getMakeDate());
		this.MakeTime = aLSTransInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLSTransInfoSchema.getModifyDate());
		this.ModifyTime = aLSTransInfoSchema.getModifyTime();
		this.ApplyDate = fDate.getDate( aLSTransInfoSchema.getApplyDate());
		this.TransNo = aLSTransInfoSchema.getTransNo();
		this.SuccFlag = aLSTransInfoSchema.getSuccFlag();
		this.ErrorInfo = aLSTransInfoSchema.getErrorInfo();
		this.ExpectedDate = fDate.getDate( aLSTransInfoSchema.getExpectedDate());
		this.RejectReason = aLSTransInfoSchema.getRejectReason();
		this.ReceivedDate = fDate.getDate( aLSTransInfoSchema.getReceivedDate());
		this.TransAmount = aLSTransInfoSchema.getTransAmount();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("CompanyNo") == null )
				this.CompanyNo = null;
			else
				this.CompanyNo = rs.getString("CompanyNo").trim();

			if( rs.getString("ContType") == null )
				this.ContType = null;
			else
				this.ContType = rs.getString("ContType").trim();

			if( rs.getString("TransGrpContNo") == null )
				this.TransGrpContNo = null;
			else
				this.TransGrpContNo = rs.getString("TransGrpContNo").trim();

			if( rs.getString("TransContNo") == null )
				this.TransContNo = null;
			else
				this.TransContNo = rs.getString("TransContNo").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("Email") == null )
				this.Email = null;
			else
				this.Email = rs.getString("Email").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankName") == null )
				this.BankName = null;
			else
				this.BankName = rs.getString("BankName").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("AccNo") == null )
				this.AccNo = null;
			else
				this.AccNo = rs.getString("AccNo").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("InputOperator") == null )
				this.InputOperator = null;
			else
				this.InputOperator = rs.getString("InputOperator").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.ApplyDate = rs.getDate("ApplyDate");
			if( rs.getString("TransNo") == null )
				this.TransNo = null;
			else
				this.TransNo = rs.getString("TransNo").trim();

			if( rs.getString("SuccFlag") == null )
				this.SuccFlag = null;
			else
				this.SuccFlag = rs.getString("SuccFlag").trim();

			if( rs.getString("ErrorInfo") == null )
				this.ErrorInfo = null;
			else
				this.ErrorInfo = rs.getString("ErrorInfo").trim();

			this.ExpectedDate = rs.getDate("ExpectedDate");
			if( rs.getString("RejectReason") == null )
				this.RejectReason = null;
			else
				this.RejectReason = rs.getString("RejectReason").trim();

			this.ReceivedDate = rs.getDate("ReceivedDate");
			this.TransAmount = rs.getDouble("TransAmount");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LSTransInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LSTransInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LSTransInfoSchema getSchema()
	{
		LSTransInfoSchema aLSTransInfoSchema = new LSTransInfoSchema();
		aLSTransInfoSchema.setSchema(this);
		return aLSTransInfoSchema;
	}

	public LSTransInfoDB getDB()
	{
		LSTransInfoDB aDBOper = new LSTransInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLSTransInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransGrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Email)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SuccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorInfo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ExpectedDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RejectReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ReceivedDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransAmount));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLSTransInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CompanyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TransGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TransContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			BankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			AccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			InputOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			SuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ErrorInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ExpectedDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			RejectReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ReceivedDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			TransAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LSTransInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("CompanyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyNo));
		}
		if (FCode.equals("ContType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
		}
		if (FCode.equals("TransGrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransGrpContNo));
		}
		if (FCode.equals("TransContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransContNo));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("Email"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankName));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("AccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccNo));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("InputOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
		}
		if (FCode.equals("TransNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
		}
		if (FCode.equals("SuccFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuccFlag));
		}
		if (FCode.equals("ErrorInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorInfo));
		}
		if (FCode.equals("ExpectedDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getExpectedDate()));
		}
		if (FCode.equals("RejectReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RejectReason));
		}
		if (FCode.equals("ReceivedDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReceivedDate()));
		}
		if (FCode.equals("TransAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransAmount));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CompanyNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ContType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TransGrpContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TransContNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Email);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(BankName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(AccNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(InputOperator);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(TransNo);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(SuccFlag);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ErrorInfo);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getExpectedDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(RejectReason);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReceivedDate()));
				break;
			case 27:
				strFieldValue = String.valueOf(TransAmount);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("CompanyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyNo = FValue.trim();
			}
			else
				CompanyNo = null;
		}
		if (FCode.equalsIgnoreCase("ContType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContType = FValue.trim();
			}
			else
				ContType = null;
		}
		if (FCode.equalsIgnoreCase("TransGrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransGrpContNo = FValue.trim();
			}
			else
				TransGrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("TransContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransContNo = FValue.trim();
			}
			else
				TransContNo = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("Email"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Email = FValue.trim();
			}
			else
				Email = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankName = FValue.trim();
			}
			else
				BankName = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("AccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccNo = FValue.trim();
			}
			else
				AccNo = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("InputOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputOperator = FValue.trim();
			}
			else
				InputOperator = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ApplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ApplyDate = fDate.getDate( FValue );
			}
			else
				ApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("TransNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransNo = FValue.trim();
			}
			else
				TransNo = null;
		}
		if (FCode.equalsIgnoreCase("SuccFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SuccFlag = FValue.trim();
			}
			else
				SuccFlag = null;
		}
		if (FCode.equalsIgnoreCase("ErrorInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorInfo = FValue.trim();
			}
			else
				ErrorInfo = null;
		}
		if (FCode.equalsIgnoreCase("ExpectedDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ExpectedDate = fDate.getDate( FValue );
			}
			else
				ExpectedDate = null;
		}
		if (FCode.equalsIgnoreCase("RejectReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RejectReason = FValue.trim();
			}
			else
				RejectReason = null;
		}
		if (FCode.equalsIgnoreCase("ReceivedDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReceivedDate = fDate.getDate( FValue );
			}
			else
				ReceivedDate = null;
		}
		if (FCode.equalsIgnoreCase("TransAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransAmount = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LSTransInfoSchema other = (LSTransInfoSchema)otherObject;
		return
			(PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (CompanyNo == null ? other.getCompanyNo() == null : CompanyNo.equals(other.getCompanyNo()))
			&& (ContType == null ? other.getContType() == null : ContType.equals(other.getContType()))
			&& (TransGrpContNo == null ? other.getTransGrpContNo() == null : TransGrpContNo.equals(other.getTransGrpContNo()))
			&& (TransContNo == null ? other.getTransContNo() == null : TransContNo.equals(other.getTransContNo()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (Email == null ? other.getEmail() == null : Email.equals(other.getEmail()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankName == null ? other.getBankName() == null : BankName.equals(other.getBankName()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (AccNo == null ? other.getAccNo() == null : AccNo.equals(other.getAccNo()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (InputOperator == null ? other.getInputOperator() == null : InputOperator.equals(other.getInputOperator()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (ApplyDate == null ? other.getApplyDate() == null : fDate.getString(ApplyDate).equals(other.getApplyDate()))
			&& (TransNo == null ? other.getTransNo() == null : TransNo.equals(other.getTransNo()))
			&& (SuccFlag == null ? other.getSuccFlag() == null : SuccFlag.equals(other.getSuccFlag()))
			&& (ErrorInfo == null ? other.getErrorInfo() == null : ErrorInfo.equals(other.getErrorInfo()))
			&& (ExpectedDate == null ? other.getExpectedDate() == null : fDate.getString(ExpectedDate).equals(other.getExpectedDate()))
			&& (RejectReason == null ? other.getRejectReason() == null : RejectReason.equals(other.getRejectReason()))
			&& (ReceivedDate == null ? other.getReceivedDate() == null : fDate.getString(ReceivedDate).equals(other.getReceivedDate()))
			&& TransAmount == other.getTransAmount();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return 0;
		}
		if( strFieldName.equals("CompanyNo") ) {
			return 1;
		}
		if( strFieldName.equals("ContType") ) {
			return 2;
		}
		if( strFieldName.equals("TransGrpContNo") ) {
			return 3;
		}
		if( strFieldName.equals("TransContNo") ) {
			return 4;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 5;
		}
		if( strFieldName.equals("Name") ) {
			return 6;
		}
		if( strFieldName.equals("Phone") ) {
			return 7;
		}
		if( strFieldName.equals("Email") ) {
			return 8;
		}
		if( strFieldName.equals("BankCode") ) {
			return 9;
		}
		if( strFieldName.equals("BankName") ) {
			return 10;
		}
		if( strFieldName.equals("AccName") ) {
			return 11;
		}
		if( strFieldName.equals("AccNo") ) {
			return 12;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 13;
		}
		if( strFieldName.equals("InputOperator") ) {
			return 14;
		}
		if( strFieldName.equals("Operator") ) {
			return 15;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 16;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 19;
		}
		if( strFieldName.equals("ApplyDate") ) {
			return 20;
		}
		if( strFieldName.equals("TransNo") ) {
			return 21;
		}
		if( strFieldName.equals("SuccFlag") ) {
			return 22;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return 23;
		}
		if( strFieldName.equals("ExpectedDate") ) {
			return 24;
		}
		if( strFieldName.equals("RejectReason") ) {
			return 25;
		}
		if( strFieldName.equals("ReceivedDate") ) {
			return 26;
		}
		if( strFieldName.equals("TransAmount") ) {
			return 27;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PrtNo";
				break;
			case 1:
				strFieldName = "CompanyNo";
				break;
			case 2:
				strFieldName = "ContType";
				break;
			case 3:
				strFieldName = "TransGrpContNo";
				break;
			case 4:
				strFieldName = "TransContNo";
				break;
			case 5:
				strFieldName = "AgentCode";
				break;
			case 6:
				strFieldName = "Name";
				break;
			case 7:
				strFieldName = "Phone";
				break;
			case 8:
				strFieldName = "Email";
				break;
			case 9:
				strFieldName = "BankCode";
				break;
			case 10:
				strFieldName = "BankName";
				break;
			case 11:
				strFieldName = "AccName";
				break;
			case 12:
				strFieldName = "AccNo";
				break;
			case 13:
				strFieldName = "StateFlag";
				break;
			case 14:
				strFieldName = "InputOperator";
				break;
			case 15:
				strFieldName = "Operator";
				break;
			case 16:
				strFieldName = "MakeDate";
				break;
			case 17:
				strFieldName = "MakeTime";
				break;
			case 18:
				strFieldName = "ModifyDate";
				break;
			case 19:
				strFieldName = "ModifyTime";
				break;
			case 20:
				strFieldName = "ApplyDate";
				break;
			case 21:
				strFieldName = "TransNo";
				break;
			case 22:
				strFieldName = "SuccFlag";
				break;
			case 23:
				strFieldName = "ErrorInfo";
				break;
			case 24:
				strFieldName = "ExpectedDate";
				break;
			case 25:
				strFieldName = "RejectReason";
				break;
			case 26:
				strFieldName = "ReceivedDate";
				break;
			case 27:
				strFieldName = "TransAmount";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransGrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Email") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TransNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SuccFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExpectedDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RejectReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceivedDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TransAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
