/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAPlanDB;

/*
 * <p>ClassName: LAPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-08-11
 */
public class LAPlanSchema implements Schema, Cloneable
{
    // @Field
    /** 计划编码 */
    private String PlanCode;
    /** 展业类型 */
    private String BranchType;
    /** 计划类型 */
    private String PlanType;
    /** 计划对象 */
    private String PlanObject;
    /** 计划时间单位 */
    private int PlanPeriodUnit;
    /** 计划期间 */
    private int PlanPeriod;
    /** 计划条件1 */
    private String PlanCond1;
    /** 计划条件2 */
    private String PlanCond2;
    /** 计划条件3 */
    private String PlanCond3;
    /** 计划标准 */
    private double PlanValue;
    /** 计划起期 */
    private Date PlanStartDate;
    /** 计划止期 */
    private Date PlanEndDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 渠道 */
    private String BranchType2;
    /** 标准保费 */
    private double TransStandMoney;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAPlanSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PlanCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LAPlanSchema cloned = (LAPlanSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPlanCode()
    {
        return PlanCode;
    }

    public void setPlanCode(String aPlanCode)
    {
        PlanCode = aPlanCode;
    }

    public String getBranchType()
    {
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getPlanType()
    {
        return PlanType;
    }

    public void setPlanType(String aPlanType)
    {
        PlanType = aPlanType;
    }

    public String getPlanObject()
    {
        return PlanObject;
    }

    public void setPlanObject(String aPlanObject)
    {
        PlanObject = aPlanObject;
    }

    public int getPlanPeriodUnit()
    {
        return PlanPeriodUnit;
    }

    public void setPlanPeriodUnit(int aPlanPeriodUnit)
    {
        PlanPeriodUnit = aPlanPeriodUnit;
    }

    public void setPlanPeriodUnit(String aPlanPeriodUnit)
    {
        if (aPlanPeriodUnit != null && !aPlanPeriodUnit.equals(""))
        {
            Integer tInteger = new Integer(aPlanPeriodUnit);
            int i = tInteger.intValue();
            PlanPeriodUnit = i;
        }
    }

    public int getPlanPeriod()
    {
        return PlanPeriod;
    }

    public void setPlanPeriod(int aPlanPeriod)
    {
        PlanPeriod = aPlanPeriod;
    }

    public void setPlanPeriod(String aPlanPeriod)
    {
        if (aPlanPeriod != null && !aPlanPeriod.equals(""))
        {
            Integer tInteger = new Integer(aPlanPeriod);
            int i = tInteger.intValue();
            PlanPeriod = i;
        }
    }

    public String getPlanCond1()
    {
        return PlanCond1;
    }

    public void setPlanCond1(String aPlanCond1)
    {
        PlanCond1 = aPlanCond1;
    }

    public String getPlanCond2()
    {
        return PlanCond2;
    }

    public void setPlanCond2(String aPlanCond2)
    {
        PlanCond2 = aPlanCond2;
    }

    public String getPlanCond3()
    {
        return PlanCond3;
    }

    public void setPlanCond3(String aPlanCond3)
    {
        PlanCond3 = aPlanCond3;
    }

    public double getPlanValue()
    {
        return PlanValue;
    }

    public void setPlanValue(double aPlanValue)
    {
        PlanValue = Arith.round(aPlanValue, 2);
    }

    public void setPlanValue(String aPlanValue)
    {
        if (aPlanValue != null && !aPlanValue.equals(""))
        {
            Double tDouble = new Double(aPlanValue);
            double d = tDouble.doubleValue();
            PlanValue = Arith.round(d, 2);
        }
    }

    public String getPlanStartDate()
    {
        if (PlanStartDate != null)
        {
            return fDate.getString(PlanStartDate);
        }
        else
        {
            return null;
        }
    }

    public void setPlanStartDate(Date aPlanStartDate)
    {
        PlanStartDate = aPlanStartDate;
    }

    public void setPlanStartDate(String aPlanStartDate)
    {
        if (aPlanStartDate != null && !aPlanStartDate.equals(""))
        {
            PlanStartDate = fDate.getDate(aPlanStartDate);
        }
        else
        {
            PlanStartDate = null;
        }
    }

    public String getPlanEndDate()
    {
        if (PlanEndDate != null)
        {
            return fDate.getString(PlanEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setPlanEndDate(Date aPlanEndDate)
    {
        PlanEndDate = aPlanEndDate;
    }

    public void setPlanEndDate(String aPlanEndDate)
    {
        if (aPlanEndDate != null && !aPlanEndDate.equals(""))
        {
            PlanEndDate = fDate.getDate(aPlanEndDate);
        }
        else
        {
            PlanEndDate = null;
        }
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getBranchType2()
    {
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    public double getTransStandMoney()
    {
        return TransStandMoney;
    }

    public void setTransStandMoney(double aTransStandMoney)
    {
        TransStandMoney = Arith.round(aTransStandMoney, 2);
    }

    public void setTransStandMoney(String aTransStandMoney)
    {
        if (aTransStandMoney != null && !aTransStandMoney.equals(""))
        {
            Double tDouble = new Double(aTransStandMoney);
            double d = tDouble.doubleValue();
            TransStandMoney = Arith.round(d, 2);
        }
    }


    /**
     * 使用另外一个 LAPlanSchema 对象给 Schema 赋值
     * @param: aLAPlanSchema LAPlanSchema
     **/
    public void setSchema(LAPlanSchema aLAPlanSchema)
    {
        this.PlanCode = aLAPlanSchema.getPlanCode();
        this.BranchType = aLAPlanSchema.getBranchType();
        this.PlanType = aLAPlanSchema.getPlanType();
        this.PlanObject = aLAPlanSchema.getPlanObject();
        this.PlanPeriodUnit = aLAPlanSchema.getPlanPeriodUnit();
        this.PlanPeriod = aLAPlanSchema.getPlanPeriod();
        this.PlanCond1 = aLAPlanSchema.getPlanCond1();
        this.PlanCond2 = aLAPlanSchema.getPlanCond2();
        this.PlanCond3 = aLAPlanSchema.getPlanCond3();
        this.PlanValue = aLAPlanSchema.getPlanValue();
        this.PlanStartDate = fDate.getDate(aLAPlanSchema.getPlanStartDate());
        this.PlanEndDate = fDate.getDate(aLAPlanSchema.getPlanEndDate());
        this.Operator = aLAPlanSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAPlanSchema.getMakeDate());
        this.MakeTime = aLAPlanSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAPlanSchema.getModifyDate());
        this.ModifyTime = aLAPlanSchema.getModifyTime();
        this.BranchType2 = aLAPlanSchema.getBranchType2();
        this.TransStandMoney = aLAPlanSchema.getTransStandMoney();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PlanCode") == null)
            {
                this.PlanCode = null;
            }
            else
            {
                this.PlanCode = rs.getString("PlanCode").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("PlanType") == null)
            {
                this.PlanType = null;
            }
            else
            {
                this.PlanType = rs.getString("PlanType").trim();
            }

            if (rs.getString("PlanObject") == null)
            {
                this.PlanObject = null;
            }
            else
            {
                this.PlanObject = rs.getString("PlanObject").trim();
            }

            this.PlanPeriodUnit = rs.getInt("PlanPeriodUnit");
            this.PlanPeriod = rs.getInt("PlanPeriod");
            if (rs.getString("PlanCond1") == null)
            {
                this.PlanCond1 = null;
            }
            else
            {
                this.PlanCond1 = rs.getString("PlanCond1").trim();
            }

            if (rs.getString("PlanCond2") == null)
            {
                this.PlanCond2 = null;
            }
            else
            {
                this.PlanCond2 = rs.getString("PlanCond2").trim();
            }

            if (rs.getString("PlanCond3") == null)
            {
                this.PlanCond3 = null;
            }
            else
            {
                this.PlanCond3 = rs.getString("PlanCond3").trim();
            }

            this.PlanValue = rs.getDouble("PlanValue");
            this.PlanStartDate = rs.getDate("PlanStartDate");
            this.PlanEndDate = rs.getDate("PlanEndDate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            this.TransStandMoney = rs.getDouble("TransStandMoney");
        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LAPlan表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAPlanSchema getSchema()
    {
        LAPlanSchema aLAPlanSchema = new LAPlanSchema();
        aLAPlanSchema.setSchema(this);
        return aLAPlanSchema;
    }

    public LAPlanDB getDB()
    {
        LAPlanDB aDBOper = new LAPlanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAPlan描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PlanCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanObject));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PlanPeriodUnit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PlanPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanCond1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanCond2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanCond3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PlanValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PlanStartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PlanEndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TransStandMoney));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAPlan>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            PlanType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            PlanObject = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            PlanPeriodUnit = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            PlanPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            PlanCond1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            PlanCond2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            PlanCond3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            PlanValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            PlanStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            PlanEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                         SysConst.PACKAGESPILTER);
            TransStandMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("PlanType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanType));
        }
        if (FCode.equals("PlanObject"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanObject));
        }
        if (FCode.equals("PlanPeriodUnit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanPeriodUnit));
        }
        if (FCode.equals("PlanPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanPeriod));
        }
        if (FCode.equals("PlanCond1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCond1));
        }
        if (FCode.equals("PlanCond2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCond2));
        }
        if (FCode.equals("PlanCond3"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCond3));
        }
        if (FCode.equals("PlanValue"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanValue));
        }
        if (FCode.equals("PlanStartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getPlanStartDate()));
        }
        if (FCode.equals("PlanEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPlanEndDate()));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("TransStandMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransStandMoney));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PlanCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PlanType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PlanObject);
                break;
            case 4:
                strFieldValue = String.valueOf(PlanPeriodUnit);
                break;
            case 5:
                strFieldValue = String.valueOf(PlanPeriod);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PlanCond1);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PlanCond2);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PlanCond3);
                break;
            case 9:
                strFieldValue = String.valueOf(PlanValue);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPlanStartDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPlanEndDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            case 18:
                strFieldValue = String.valueOf(TransStandMoney);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equalsIgnoreCase("PlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
            {
                PlanCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanType = FValue.trim();
            }
            else
            {
                PlanType = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanObject"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanObject = FValue.trim();
            }
            else
            {
                PlanObject = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanPeriodUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PlanPeriodUnit = i;
            }
        }
        if (FCode.equalsIgnoreCase("PlanPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PlanPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("PlanCond1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanCond1 = FValue.trim();
            }
            else
            {
                PlanCond1 = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanCond2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanCond2 = FValue.trim();
            }
            else
            {
                PlanCond2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanCond3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanCond3 = FValue.trim();
            }
            else
            {
                PlanCond3 = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PlanValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("PlanStartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanStartDate = fDate.getDate(FValue);
            }
            else
            {
                PlanStartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanEndDate = fDate.getDate(FValue);
            }
            else
            {
                PlanEndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("TransStandMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TransStandMoney = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAPlanSchema other = (LAPlanSchema) otherObject;
        return
                PlanCode.equals(other.getPlanCode())
                && BranchType.equals(other.getBranchType())
                && PlanType.equals(other.getPlanType())
                && PlanObject.equals(other.getPlanObject())
                && PlanPeriodUnit == other.getPlanPeriodUnit()
                && PlanPeriod == other.getPlanPeriod()
                && PlanCond1.equals(other.getPlanCond1())
                && PlanCond2.equals(other.getPlanCond2())
                && PlanCond3.equals(other.getPlanCond3())
                && PlanValue == other.getPlanValue()
                && fDate.getString(PlanStartDate).equals(other.getPlanStartDate())
                && fDate.getString(PlanEndDate).equals(other.getPlanEndDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BranchType2.equals(other.getBranchType2())
                && TransStandMoney == other.getTransStandMoney();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PlanCode"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 1;
        }
        if (strFieldName.equals("PlanType"))
        {
            return 2;
        }
        if (strFieldName.equals("PlanObject"))
        {
            return 3;
        }
        if (strFieldName.equals("PlanPeriodUnit"))
        {
            return 4;
        }
        if (strFieldName.equals("PlanPeriod"))
        {
            return 5;
        }
        if (strFieldName.equals("PlanCond1"))
        {
            return 6;
        }
        if (strFieldName.equals("PlanCond2"))
        {
            return 7;
        }
        if (strFieldName.equals("PlanCond3"))
        {
            return 8;
        }
        if (strFieldName.equals("PlanValue"))
        {
            return 9;
        }
        if (strFieldName.equals("PlanStartDate"))
        {
            return 10;
        }
        if (strFieldName.equals("PlanEndDate"))
        {
            return 11;
        }
        if (strFieldName.equals("Operator"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 16;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 17;
        }
        if (strFieldName.equals("TransStandMoney"))
        {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PlanCode";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "PlanType";
                break;
            case 3:
                strFieldName = "PlanObject";
                break;
            case 4:
                strFieldName = "PlanPeriodUnit";
                break;
            case 5:
                strFieldName = "PlanPeriod";
                break;
            case 6:
                strFieldName = "PlanCond1";
                break;
            case 7:
                strFieldName = "PlanCond2";
                break;
            case 8:
                strFieldName = "PlanCond3";
                break;
            case 9:
                strFieldName = "PlanValue";
                break;
            case 10:
                strFieldName = "PlanStartDate";
                break;
            case 11:
                strFieldName = "PlanEndDate";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "BranchType2";
                break;
            case 18:
                strFieldName = "TransStandMoney";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanObject"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanPeriodUnit"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PlanPeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PlanCond1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanCond2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanCond3"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanValue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PlanStartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PlanEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransStandMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
