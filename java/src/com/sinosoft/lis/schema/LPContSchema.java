/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPContDB;

/*
 * <p>ClassName: LPContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2012-05-24
 */
public class LPContSchema implements Schema, Cloneable
{
	// @Field
	/** 批单号 */
	private String EdorNo;
	/** 批改类型 */
	private String EdorType;
	/** 集体合同号码 */
	private String GrpContNo;
	/** 合同号码 */
	private String ContNo;
	/** 总单投保单号码 */
	private String ProposalContNo;
	/** 印刷号码 */
	private String PrtNo;
	/** 总单类型 */
	private String ContType;
	/** 家庭单类型 */
	private String FamilyType;
	/** 家庭保障号 */
	private String FamilyID;
	/** 保单类型标记 */
	private String PolType;
	/** 卡单标志 */
	private String CardFlag;
	/** 管理机构 */
	private String ManageCom;
	/** 处理机构 */
	private String ExecuteCom;
	/** 代理机构 */
	private String AgentCom;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 联合代理人代码 */
	private String AgentCode1;
	/** 代理机构内部分类 */
	private String AgentType;
	/** 销售渠道 */
	private String SaleChnl;
	/** 经办人 */
	private String Handler;
	/** 保单口令 */
	private String Password;
	/** 投保人客户号码 */
	private String AppntNo;
	/** 投保人名称 */
	private String AppntName;
	/** 投保人性别 */
	private String AppntSex;
	/** 投保人出生日期 */
	private Date AppntBirthday;
	/** 投保人证件类型 */
	private String AppntIDType;
	/** 投保人证件号码 */
	private String AppntIDNo;
	/** 被保人客户号 */
	private String InsuredNo;
	/** 被保人名称 */
	private String InsuredName;
	/** 被保人性别 */
	private String InsuredSex;
	/** 被保人出生日期 */
	private Date InsuredBirthday;
	/** 证件类型 */
	private String InsuredIDType;
	/** 证件号码 */
	private String InsuredIDNo;
	/** 交费间隔 */
	private int PayIntv;
	/** 交费方式 */
	private String PayMode;
	/** 交费位置 */
	private String PayLocation;
	/** 合同争议处理方式 */
	private String DisputedFlag;
	/** 溢交处理方式 */
	private String OutPayFlag;
	/** 保单送达方式 */
	private String GetPolMode;
	/** 签单机构 */
	private String SignCom;
	/** 签单日期 */
	private Date SignDate;
	/** 签单时间 */
	private String SignTime;
	/** 银行委托书号码 */
	private String ConsignNo;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 银行帐户名 */
	private String AccName;
	/** 保单打印次数 */
	private int PrintCount;
	/** 遗失补发次数 */
	private int LostTimes;
	/** 语种标记 */
	private String Lang;
	/** 币别 */
	private String Currency;
	/** 备注 */
	private String Remark;
	/** 人数 */
	private int Peoples;
	/** 档次 */
	private double Mult;
	/** 保费 */
	private double Prem;
	/** 保额 */
	private double Amnt;
	/** 累计保费 */
	private double SumPrem;
	/** 余额 */
	private double Dif;
	/** 交至日期 */
	private Date PaytoDate;
	/** 首期交费日期 */
	private Date FirstPayDate;
	/** 保单生效日期 */
	private Date CValiDate;
	/** 录单人 */
	private String InputOperator;
	/** 录单完成日期 */
	private Date InputDate;
	/** 录单完成时间 */
	private String InputTime;
	/** 复核状态 */
	private String ApproveFlag;
	/** 复核人编码 */
	private String ApproveCode;
	/** 复核日期 */
	private Date ApproveDate;
	/** 复核时间 */
	private String ApproveTime;
	/** 核保状态 */
	private String UWFlag;
	/** 核保人 */
	private String UWOperator;
	/** 核保完成日期 */
	private Date UWDate;
	/** 核保完成时间 */
	private String UWTime;
	/** 投保单/保单标志 */
	private String AppFlag;
	/** 投保单申请日期 */
	private Date PolApplyDate;
	/** 保单送达日期 */
	private Date GetPolDate;
	/** 保单送达时间 */
	private String GetPolTime;
	/** 保单回执客户签收日期 */
	private Date CustomGetPolDate;
	/** 状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 初审人 */
	private String FirstTrialOperator;
	/** 初审日期 */
	private Date FirstTrialDate;
	/** 初审时间 */
	private String FirstTrialTime;
	/** 收单人 */
	private String ReceiveOperator;
	/** 收单日期 */
	private Date ReceiveDate;
	/** 收单时间 */
	private String ReceiveTime;
	/** 暂收据号 */
	private String TempFeeNo;
	/** 投保书类型 */
	private String ProposalType;
	/** 销售渠道明细 */
	private String SaleChnlDetail;
	/** 合同打印位置标记 */
	private String ContPrintLoFlag;
	/** 保费收据号 */
	private String ContPremFeeNo;
	/** 客户回执号 */
	private String CustomerReceiptNo;
	/** 合同终止日期 */
	private Date CInValiDate;
	/** 份数 */
	private double Copys;
	/** 投保次数类型 */
	private String DegreeType;
	/** 投保单填写日期 */
	private Date HandlerDate;
	/** 投保人签章 */
	private String HandlerPrint;
	/** 保单状态 */
	private String StateFlag;
	/** 资金规模 */
	private double PremScope;
	/** 国际业务标志 */
	private String IntlFlag;
	/** 核保审核号 */
	private String UWConfirmNo;
	/** 支付者 */
	private String PayerType;
	/** 集团代理机构 */
	private String GrpAgentCom;
	/** 集团代理人 */
	private String GrpAgentCode;
	/** 集团代理人姓名 */
	private String GrpAgentName;
	/** 账户类型 */
	private String AccType;
	/** 集团交叉销售渠道 */
	private String Crs_SaleChnl;
	/** 集团销售业务类型 */
	private String Crs_BussType;
	/** 集团代理人身份证 */
	private String GrpAgentIDNo;
	/** 自动发送续期通知书标志 */
	private String DueFeeMsgFlag;
	/** 缴费模式 */
	private String PayMethod;
	/** 投保备注项 */
	private String ExiSpec;
	/** 扩展缴费方式 */
	private String ExPayMode;
	/** 代理销售业务员编码 */
	private String AgentSaleCode;

	public static final int FIELDNUM = 116;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LPContSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "EdorNo";
		pk[1] = "EdorType";
		pk[2] = "ContNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LPContSchema cloned = (LPContSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getEdorType()
	{
		return EdorType;
	}
	public void setEdorType(String aEdorType)
	{
		EdorType = aEdorType;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getProposalContNo()
	{
		return ProposalContNo;
	}
	public void setProposalContNo(String aProposalContNo)
	{
		ProposalContNo = aProposalContNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getContType()
	{
		return ContType;
	}
	public void setContType(String aContType)
	{
		ContType = aContType;
	}
	public String getFamilyType()
	{
		return FamilyType;
	}
	public void setFamilyType(String aFamilyType)
	{
		FamilyType = aFamilyType;
	}
	public String getFamilyID()
	{
		return FamilyID;
	}
	public void setFamilyID(String aFamilyID)
	{
		FamilyID = aFamilyID;
	}
	public String getPolType()
	{
		return PolType;
	}
	public void setPolType(String aPolType)
	{
		PolType = aPolType;
	}
	public String getCardFlag()
	{
		return CardFlag;
	}
	public void setCardFlag(String aCardFlag)
	{
		CardFlag = aCardFlag;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getExecuteCom()
	{
		return ExecuteCom;
	}
	public void setExecuteCom(String aExecuteCom)
	{
		ExecuteCom = aExecuteCom;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getAgentCode1()
	{
		return AgentCode1;
	}
	public void setAgentCode1(String aAgentCode1)
	{
		AgentCode1 = aAgentCode1;
	}
	public String getAgentType()
	{
		return AgentType;
	}
	public void setAgentType(String aAgentType)
	{
		AgentType = aAgentType;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getHandler()
	{
		return Handler;
	}
	public void setHandler(String aHandler)
	{
		Handler = aHandler;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getAppntNo()
	{
		return AppntNo;
	}
	public void setAppntNo(String aAppntNo)
	{
		AppntNo = aAppntNo;
	}
	public String getAppntName()
	{
		return AppntName;
	}
	public void setAppntName(String aAppntName)
	{
		AppntName = aAppntName;
	}
	public String getAppntSex()
	{
		return AppntSex;
	}
	public void setAppntSex(String aAppntSex)
	{
		AppntSex = aAppntSex;
	}
	public String getAppntBirthday()
	{
		if( AppntBirthday != null )
			return fDate.getString(AppntBirthday);
		else
			return null;
	}
	public void setAppntBirthday(Date aAppntBirthday)
	{
		AppntBirthday = aAppntBirthday;
	}
	public void setAppntBirthday(String aAppntBirthday)
	{
		if (aAppntBirthday != null && !aAppntBirthday.equals("") )
		{
			AppntBirthday = fDate.getDate( aAppntBirthday );
		}
		else
			AppntBirthday = null;
	}

	public String getAppntIDType()
	{
		return AppntIDType;
	}
	public void setAppntIDType(String aAppntIDType)
	{
		AppntIDType = aAppntIDType;
	}
	public String getAppntIDNo()
	{
		return AppntIDNo;
	}
	public void setAppntIDNo(String aAppntIDNo)
	{
		AppntIDNo = aAppntIDNo;
	}
	public String getInsuredNo()
	{
		return InsuredNo;
	}
	public void setInsuredNo(String aInsuredNo)
	{
		InsuredNo = aInsuredNo;
	}
	public String getInsuredName()
	{
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
		InsuredName = aInsuredName;
	}
	public String getInsuredSex()
	{
		return InsuredSex;
	}
	public void setInsuredSex(String aInsuredSex)
	{
		InsuredSex = aInsuredSex;
	}
	public String getInsuredBirthday()
	{
		if( InsuredBirthday != null )
			return fDate.getString(InsuredBirthday);
		else
			return null;
	}
	public void setInsuredBirthday(Date aInsuredBirthday)
	{
		InsuredBirthday = aInsuredBirthday;
	}
	public void setInsuredBirthday(String aInsuredBirthday)
	{
		if (aInsuredBirthday != null && !aInsuredBirthday.equals("") )
		{
			InsuredBirthday = fDate.getDate( aInsuredBirthday );
		}
		else
			InsuredBirthday = null;
	}

	public String getInsuredIDType()
	{
		return InsuredIDType;
	}
	public void setInsuredIDType(String aInsuredIDType)
	{
		InsuredIDType = aInsuredIDType;
	}
	public String getInsuredIDNo()
	{
		return InsuredIDNo;
	}
	public void setInsuredIDNo(String aInsuredIDNo)
	{
		InsuredIDNo = aInsuredIDNo;
	}
	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getPayLocation()
	{
		return PayLocation;
	}
	public void setPayLocation(String aPayLocation)
	{
		PayLocation = aPayLocation;
	}
	public String getDisputedFlag()
	{
		return DisputedFlag;
	}
	public void setDisputedFlag(String aDisputedFlag)
	{
		DisputedFlag = aDisputedFlag;
	}
	public String getOutPayFlag()
	{
		return OutPayFlag;
	}
	public void setOutPayFlag(String aOutPayFlag)
	{
		OutPayFlag = aOutPayFlag;
	}
	public String getGetPolMode()
	{
		return GetPolMode;
	}
	public void setGetPolMode(String aGetPolMode)
	{
		GetPolMode = aGetPolMode;
	}
	public String getSignCom()
	{
		return SignCom;
	}
	public void setSignCom(String aSignCom)
	{
		SignCom = aSignCom;
	}
	public String getSignDate()
	{
		if( SignDate != null )
			return fDate.getString(SignDate);
		else
			return null;
	}
	public void setSignDate(Date aSignDate)
	{
		SignDate = aSignDate;
	}
	public void setSignDate(String aSignDate)
	{
		if (aSignDate != null && !aSignDate.equals("") )
		{
			SignDate = fDate.getDate( aSignDate );
		}
		else
			SignDate = null;
	}

	public String getSignTime()
	{
		return SignTime;
	}
	public void setSignTime(String aSignTime)
	{
		SignTime = aSignTime;
	}
	public String getConsignNo()
	{
		return ConsignNo;
	}
	public void setConsignNo(String aConsignNo)
	{
		ConsignNo = aConsignNo;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public int getPrintCount()
	{
		return PrintCount;
	}
	public void setPrintCount(int aPrintCount)
	{
		PrintCount = aPrintCount;
	}
	public void setPrintCount(String aPrintCount)
	{
		if (aPrintCount != null && !aPrintCount.equals(""))
		{
			Integer tInteger = new Integer(aPrintCount);
			int i = tInteger.intValue();
			PrintCount = i;
		}
	}

	public int getLostTimes()
	{
		return LostTimes;
	}
	public void setLostTimes(int aLostTimes)
	{
		LostTimes = aLostTimes;
	}
	public void setLostTimes(String aLostTimes)
	{
		if (aLostTimes != null && !aLostTimes.equals(""))
		{
			Integer tInteger = new Integer(aLostTimes);
			int i = tInteger.intValue();
			LostTimes = i;
		}
	}

	public String getLang()
	{
		return Lang;
	}
	public void setLang(String aLang)
	{
		Lang = aLang;
	}
	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public int getPeoples()
	{
		return Peoples;
	}
	public void setPeoples(int aPeoples)
	{
		Peoples = aPeoples;
	}
	public void setPeoples(String aPeoples)
	{
		if (aPeoples != null && !aPeoples.equals(""))
		{
			Integer tInteger = new Integer(aPeoples);
			int i = tInteger.intValue();
			Peoples = i;
		}
	}

	public double getMult()
	{
		return Mult;
	}
	public void setMult(double aMult)
	{
		Mult = Arith.round(aMult,5);
	}
	public void setMult(String aMult)
	{
		if (aMult != null && !aMult.equals(""))
		{
			Double tDouble = new Double(aMult);
			double d = tDouble.doubleValue();
                Mult = Arith.round(d,5);
		}
	}

	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public double getSumPrem()
	{
		return SumPrem;
	}
	public void setSumPrem(double aSumPrem)
	{
		SumPrem = Arith.round(aSumPrem,2);
	}
	public void setSumPrem(String aSumPrem)
	{
		if (aSumPrem != null && !aSumPrem.equals(""))
		{
			Double tDouble = new Double(aSumPrem);
			double d = tDouble.doubleValue();
                SumPrem = Arith.round(d,2);
		}
	}

	public double getDif()
	{
		return Dif;
	}
	public void setDif(double aDif)
	{
		Dif = Arith.round(aDif,2);
	}
	public void setDif(String aDif)
	{
		if (aDif != null && !aDif.equals(""))
		{
			Double tDouble = new Double(aDif);
			double d = tDouble.doubleValue();
                Dif = Arith.round(d,2);
		}
	}

	public String getPaytoDate()
	{
		if( PaytoDate != null )
			return fDate.getString(PaytoDate);
		else
			return null;
	}
	public void setPaytoDate(Date aPaytoDate)
	{
		PaytoDate = aPaytoDate;
	}
	public void setPaytoDate(String aPaytoDate)
	{
		if (aPaytoDate != null && !aPaytoDate.equals("") )
		{
			PaytoDate = fDate.getDate( aPaytoDate );
		}
		else
			PaytoDate = null;
	}

	public String getFirstPayDate()
	{
		if( FirstPayDate != null )
			return fDate.getString(FirstPayDate);
		else
			return null;
	}
	public void setFirstPayDate(Date aFirstPayDate)
	{
		FirstPayDate = aFirstPayDate;
	}
	public void setFirstPayDate(String aFirstPayDate)
	{
		if (aFirstPayDate != null && !aFirstPayDate.equals("") )
		{
			FirstPayDate = fDate.getDate( aFirstPayDate );
		}
		else
			FirstPayDate = null;
	}

	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getInputOperator()
	{
		return InputOperator;
	}
	public void setInputOperator(String aInputOperator)
	{
		InputOperator = aInputOperator;
	}
	public String getInputDate()
	{
		if( InputDate != null )
			return fDate.getString(InputDate);
		else
			return null;
	}
	public void setInputDate(Date aInputDate)
	{
		InputDate = aInputDate;
	}
	public void setInputDate(String aInputDate)
	{
		if (aInputDate != null && !aInputDate.equals("") )
		{
			InputDate = fDate.getDate( aInputDate );
		}
		else
			InputDate = null;
	}

	public String getInputTime()
	{
		return InputTime;
	}
	public void setInputTime(String aInputTime)
	{
		InputTime = aInputTime;
	}
	public String getApproveFlag()
	{
		return ApproveFlag;
	}
	public void setApproveFlag(String aApproveFlag)
	{
		ApproveFlag = aApproveFlag;
	}
	public String getApproveCode()
	{
		return ApproveCode;
	}
	public void setApproveCode(String aApproveCode)
	{
		ApproveCode = aApproveCode;
	}
	public String getApproveDate()
	{
		if( ApproveDate != null )
			return fDate.getString(ApproveDate);
		else
			return null;
	}
	public void setApproveDate(Date aApproveDate)
	{
		ApproveDate = aApproveDate;
	}
	public void setApproveDate(String aApproveDate)
	{
		if (aApproveDate != null && !aApproveDate.equals("") )
		{
			ApproveDate = fDate.getDate( aApproveDate );
		}
		else
			ApproveDate = null;
	}

	public String getApproveTime()
	{
		return ApproveTime;
	}
	public void setApproveTime(String aApproveTime)
	{
		ApproveTime = aApproveTime;
	}
	public String getUWFlag()
	{
		return UWFlag;
	}
	public void setUWFlag(String aUWFlag)
	{
		UWFlag = aUWFlag;
	}
	public String getUWOperator()
	{
		return UWOperator;
	}
	public void setUWOperator(String aUWOperator)
	{
		UWOperator = aUWOperator;
	}
	public String getUWDate()
	{
		if( UWDate != null )
			return fDate.getString(UWDate);
		else
			return null;
	}
	public void setUWDate(Date aUWDate)
	{
		UWDate = aUWDate;
	}
	public void setUWDate(String aUWDate)
	{
		if (aUWDate != null && !aUWDate.equals("") )
		{
			UWDate = fDate.getDate( aUWDate );
		}
		else
			UWDate = null;
	}

	public String getUWTime()
	{
		return UWTime;
	}
	public void setUWTime(String aUWTime)
	{
		UWTime = aUWTime;
	}
	public String getAppFlag()
	{
		return AppFlag;
	}
	public void setAppFlag(String aAppFlag)
	{
		AppFlag = aAppFlag;
	}
	public String getPolApplyDate()
	{
		if( PolApplyDate != null )
			return fDate.getString(PolApplyDate);
		else
			return null;
	}
	public void setPolApplyDate(Date aPolApplyDate)
	{
		PolApplyDate = aPolApplyDate;
	}
	public void setPolApplyDate(String aPolApplyDate)
	{
		if (aPolApplyDate != null && !aPolApplyDate.equals("") )
		{
			PolApplyDate = fDate.getDate( aPolApplyDate );
		}
		else
			PolApplyDate = null;
	}

	public String getGetPolDate()
	{
		if( GetPolDate != null )
			return fDate.getString(GetPolDate);
		else
			return null;
	}
	public void setGetPolDate(Date aGetPolDate)
	{
		GetPolDate = aGetPolDate;
	}
	public void setGetPolDate(String aGetPolDate)
	{
		if (aGetPolDate != null && !aGetPolDate.equals("") )
		{
			GetPolDate = fDate.getDate( aGetPolDate );
		}
		else
			GetPolDate = null;
	}

	public String getGetPolTime()
	{
		return GetPolTime;
	}
	public void setGetPolTime(String aGetPolTime)
	{
		GetPolTime = aGetPolTime;
	}
	public String getCustomGetPolDate()
	{
		if( CustomGetPolDate != null )
			return fDate.getString(CustomGetPolDate);
		else
			return null;
	}
	public void setCustomGetPolDate(Date aCustomGetPolDate)
	{
		CustomGetPolDate = aCustomGetPolDate;
	}
	public void setCustomGetPolDate(String aCustomGetPolDate)
	{
		if (aCustomGetPolDate != null && !aCustomGetPolDate.equals("") )
		{
			CustomGetPolDate = fDate.getDate( aCustomGetPolDate );
		}
		else
			CustomGetPolDate = null;
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getFirstTrialOperator()
	{
		return FirstTrialOperator;
	}
	public void setFirstTrialOperator(String aFirstTrialOperator)
	{
		FirstTrialOperator = aFirstTrialOperator;
	}
	public String getFirstTrialDate()
	{
		if( FirstTrialDate != null )
			return fDate.getString(FirstTrialDate);
		else
			return null;
	}
	public void setFirstTrialDate(Date aFirstTrialDate)
	{
		FirstTrialDate = aFirstTrialDate;
	}
	public void setFirstTrialDate(String aFirstTrialDate)
	{
		if (aFirstTrialDate != null && !aFirstTrialDate.equals("") )
		{
			FirstTrialDate = fDate.getDate( aFirstTrialDate );
		}
		else
			FirstTrialDate = null;
	}

	public String getFirstTrialTime()
	{
		return FirstTrialTime;
	}
	public void setFirstTrialTime(String aFirstTrialTime)
	{
		FirstTrialTime = aFirstTrialTime;
	}
	public String getReceiveOperator()
	{
		return ReceiveOperator;
	}
	public void setReceiveOperator(String aReceiveOperator)
	{
		ReceiveOperator = aReceiveOperator;
	}
	public String getReceiveDate()
	{
		if( ReceiveDate != null )
			return fDate.getString(ReceiveDate);
		else
			return null;
	}
	public void setReceiveDate(Date aReceiveDate)
	{
		ReceiveDate = aReceiveDate;
	}
	public void setReceiveDate(String aReceiveDate)
	{
		if (aReceiveDate != null && !aReceiveDate.equals("") )
		{
			ReceiveDate = fDate.getDate( aReceiveDate );
		}
		else
			ReceiveDate = null;
	}

	public String getReceiveTime()
	{
		return ReceiveTime;
	}
	public void setReceiveTime(String aReceiveTime)
	{
		ReceiveTime = aReceiveTime;
	}
	public String getTempFeeNo()
	{
		return TempFeeNo;
	}
	public void setTempFeeNo(String aTempFeeNo)
	{
		TempFeeNo = aTempFeeNo;
	}
	public String getProposalType()
	{
		return ProposalType;
	}
	public void setProposalType(String aProposalType)
	{
		ProposalType = aProposalType;
	}
	public String getSaleChnlDetail()
	{
		return SaleChnlDetail;
	}
	public void setSaleChnlDetail(String aSaleChnlDetail)
	{
		SaleChnlDetail = aSaleChnlDetail;
	}
	public String getContPrintLoFlag()
	{
		return ContPrintLoFlag;
	}
	public void setContPrintLoFlag(String aContPrintLoFlag)
	{
		ContPrintLoFlag = aContPrintLoFlag;
	}
	public String getContPremFeeNo()
	{
		return ContPremFeeNo;
	}
	public void setContPremFeeNo(String aContPremFeeNo)
	{
		ContPremFeeNo = aContPremFeeNo;
	}
	public String getCustomerReceiptNo()
	{
		return CustomerReceiptNo;
	}
	public void setCustomerReceiptNo(String aCustomerReceiptNo)
	{
		CustomerReceiptNo = aCustomerReceiptNo;
	}
	public String getCInValiDate()
	{
		if( CInValiDate != null )
			return fDate.getString(CInValiDate);
		else
			return null;
	}
	public void setCInValiDate(Date aCInValiDate)
	{
		CInValiDate = aCInValiDate;
	}
	public void setCInValiDate(String aCInValiDate)
	{
		if (aCInValiDate != null && !aCInValiDate.equals("") )
		{
			CInValiDate = fDate.getDate( aCInValiDate );
		}
		else
			CInValiDate = null;
	}

	public double getCopys()
	{
		return Copys;
	}
	public void setCopys(double aCopys)
	{
		Copys = Arith.round(aCopys,5);
	}
	public void setCopys(String aCopys)
	{
		if (aCopys != null && !aCopys.equals(""))
		{
			Double tDouble = new Double(aCopys);
			double d = tDouble.doubleValue();
                Copys = Arith.round(d,5);
		}
	}

	public String getDegreeType()
	{
		return DegreeType;
	}
	public void setDegreeType(String aDegreeType)
	{
		DegreeType = aDegreeType;
	}
	public String getHandlerDate()
	{
		if( HandlerDate != null )
			return fDate.getString(HandlerDate);
		else
			return null;
	}
	public void setHandlerDate(Date aHandlerDate)
	{
		HandlerDate = aHandlerDate;
	}
	public void setHandlerDate(String aHandlerDate)
	{
		if (aHandlerDate != null && !aHandlerDate.equals("") )
		{
			HandlerDate = fDate.getDate( aHandlerDate );
		}
		else
			HandlerDate = null;
	}

	public String getHandlerPrint()
	{
		return HandlerPrint;
	}
	public void setHandlerPrint(String aHandlerPrint)
	{
		HandlerPrint = aHandlerPrint;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public double getPremScope()
	{
		return PremScope;
	}
	public void setPremScope(double aPremScope)
	{
		PremScope = Arith.round(aPremScope,2);
	}
	public void setPremScope(String aPremScope)
	{
		if (aPremScope != null && !aPremScope.equals(""))
		{
			Double tDouble = new Double(aPremScope);
			double d = tDouble.doubleValue();
                PremScope = Arith.round(d,2);
		}
	}

	public String getIntlFlag()
	{
		return IntlFlag;
	}
	public void setIntlFlag(String aIntlFlag)
	{
		IntlFlag = aIntlFlag;
	}
	public String getUWConfirmNo()
	{
		return UWConfirmNo;
	}
	public void setUWConfirmNo(String aUWConfirmNo)
	{
		UWConfirmNo = aUWConfirmNo;
	}
	public String getPayerType()
	{
		return PayerType;
	}
	public void setPayerType(String aPayerType)
	{
		PayerType = aPayerType;
	}
	public String getGrpAgentCom()
	{
		return GrpAgentCom;
	}
	public void setGrpAgentCom(String aGrpAgentCom)
	{
		GrpAgentCom = aGrpAgentCom;
	}
	public String getGrpAgentCode()
	{
		return GrpAgentCode;
	}
	public void setGrpAgentCode(String aGrpAgentCode)
	{
		GrpAgentCode = aGrpAgentCode;
	}
	public String getGrpAgentName()
	{
		return GrpAgentName;
	}
	public void setGrpAgentName(String aGrpAgentName)
	{
		GrpAgentName = aGrpAgentName;
	}
	public String getAccType()
	{
		return AccType;
	}
	public void setAccType(String aAccType)
	{
		AccType = aAccType;
	}
	public String getCrs_SaleChnl()
	{
		return Crs_SaleChnl;
	}
	public void setCrs_SaleChnl(String aCrs_SaleChnl)
	{
		Crs_SaleChnl = aCrs_SaleChnl;
	}
	public String getCrs_BussType()
	{
		return Crs_BussType;
	}
	public void setCrs_BussType(String aCrs_BussType)
	{
		Crs_BussType = aCrs_BussType;
	}
	public String getGrpAgentIDNo()
	{
		return GrpAgentIDNo;
	}
	public void setGrpAgentIDNo(String aGrpAgentIDNo)
	{
		GrpAgentIDNo = aGrpAgentIDNo;
	}
	public String getDueFeeMsgFlag()
	{
		return DueFeeMsgFlag;
	}
	public void setDueFeeMsgFlag(String aDueFeeMsgFlag)
	{
		DueFeeMsgFlag = aDueFeeMsgFlag;
	}
	public String getPayMethod()
	{
		return PayMethod;
	}
	public void setPayMethod(String aPayMethod)
	{
		PayMethod = aPayMethod;
	}
	public String getExiSpec()
	{
		return ExiSpec;
	}
	public void setExiSpec(String aExiSpec)
	{
		ExiSpec = aExiSpec;
	}
	public String getExPayMode()
	{
		return ExPayMode;
	}
	public void setExPayMode(String aExPayMode)
	{
		ExPayMode = aExPayMode;
	}
	public String getAgentSaleCode()
	{
		return AgentSaleCode;
	}
	public void setAgentSaleCode(String aAgentSaleCode)
	{
		AgentSaleCode = aAgentSaleCode;
	}

	/**
	* 使用另外一个 LPContSchema 对象给 Schema 赋值
	* @param: aLPContSchema LPContSchema
	**/
	public void setSchema(LPContSchema aLPContSchema)
	{
		this.EdorNo = aLPContSchema.getEdorNo();
		this.EdorType = aLPContSchema.getEdorType();
		this.GrpContNo = aLPContSchema.getGrpContNo();
		this.ContNo = aLPContSchema.getContNo();
		this.ProposalContNo = aLPContSchema.getProposalContNo();
		this.PrtNo = aLPContSchema.getPrtNo();
		this.ContType = aLPContSchema.getContType();
		this.FamilyType = aLPContSchema.getFamilyType();
		this.FamilyID = aLPContSchema.getFamilyID();
		this.PolType = aLPContSchema.getPolType();
		this.CardFlag = aLPContSchema.getCardFlag();
		this.ManageCom = aLPContSchema.getManageCom();
		this.ExecuteCom = aLPContSchema.getExecuteCom();
		this.AgentCom = aLPContSchema.getAgentCom();
		this.AgentCode = aLPContSchema.getAgentCode();
		this.AgentGroup = aLPContSchema.getAgentGroup();
		this.AgentCode1 = aLPContSchema.getAgentCode1();
		this.AgentType = aLPContSchema.getAgentType();
		this.SaleChnl = aLPContSchema.getSaleChnl();
		this.Handler = aLPContSchema.getHandler();
		this.Password = aLPContSchema.getPassword();
		this.AppntNo = aLPContSchema.getAppntNo();
		this.AppntName = aLPContSchema.getAppntName();
		this.AppntSex = aLPContSchema.getAppntSex();
		this.AppntBirthday = fDate.getDate( aLPContSchema.getAppntBirthday());
		this.AppntIDType = aLPContSchema.getAppntIDType();
		this.AppntIDNo = aLPContSchema.getAppntIDNo();
		this.InsuredNo = aLPContSchema.getInsuredNo();
		this.InsuredName = aLPContSchema.getInsuredName();
		this.InsuredSex = aLPContSchema.getInsuredSex();
		this.InsuredBirthday = fDate.getDate( aLPContSchema.getInsuredBirthday());
		this.InsuredIDType = aLPContSchema.getInsuredIDType();
		this.InsuredIDNo = aLPContSchema.getInsuredIDNo();
		this.PayIntv = aLPContSchema.getPayIntv();
		this.PayMode = aLPContSchema.getPayMode();
		this.PayLocation = aLPContSchema.getPayLocation();
		this.DisputedFlag = aLPContSchema.getDisputedFlag();
		this.OutPayFlag = aLPContSchema.getOutPayFlag();
		this.GetPolMode = aLPContSchema.getGetPolMode();
		this.SignCom = aLPContSchema.getSignCom();
		this.SignDate = fDate.getDate( aLPContSchema.getSignDate());
		this.SignTime = aLPContSchema.getSignTime();
		this.ConsignNo = aLPContSchema.getConsignNo();
		this.BankCode = aLPContSchema.getBankCode();
		this.BankAccNo = aLPContSchema.getBankAccNo();
		this.AccName = aLPContSchema.getAccName();
		this.PrintCount = aLPContSchema.getPrintCount();
		this.LostTimes = aLPContSchema.getLostTimes();
		this.Lang = aLPContSchema.getLang();
		this.Currency = aLPContSchema.getCurrency();
		this.Remark = aLPContSchema.getRemark();
		this.Peoples = aLPContSchema.getPeoples();
		this.Mult = aLPContSchema.getMult();
		this.Prem = aLPContSchema.getPrem();
		this.Amnt = aLPContSchema.getAmnt();
		this.SumPrem = aLPContSchema.getSumPrem();
		this.Dif = aLPContSchema.getDif();
		this.PaytoDate = fDate.getDate( aLPContSchema.getPaytoDate());
		this.FirstPayDate = fDate.getDate( aLPContSchema.getFirstPayDate());
		this.CValiDate = fDate.getDate( aLPContSchema.getCValiDate());
		this.InputOperator = aLPContSchema.getInputOperator();
		this.InputDate = fDate.getDate( aLPContSchema.getInputDate());
		this.InputTime = aLPContSchema.getInputTime();
		this.ApproveFlag = aLPContSchema.getApproveFlag();
		this.ApproveCode = aLPContSchema.getApproveCode();
		this.ApproveDate = fDate.getDate( aLPContSchema.getApproveDate());
		this.ApproveTime = aLPContSchema.getApproveTime();
		this.UWFlag = aLPContSchema.getUWFlag();
		this.UWOperator = aLPContSchema.getUWOperator();
		this.UWDate = fDate.getDate( aLPContSchema.getUWDate());
		this.UWTime = aLPContSchema.getUWTime();
		this.AppFlag = aLPContSchema.getAppFlag();
		this.PolApplyDate = fDate.getDate( aLPContSchema.getPolApplyDate());
		this.GetPolDate = fDate.getDate( aLPContSchema.getGetPolDate());
		this.GetPolTime = aLPContSchema.getGetPolTime();
		this.CustomGetPolDate = fDate.getDate( aLPContSchema.getCustomGetPolDate());
		this.State = aLPContSchema.getState();
		this.Operator = aLPContSchema.getOperator();
		this.MakeDate = fDate.getDate( aLPContSchema.getMakeDate());
		this.MakeTime = aLPContSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLPContSchema.getModifyDate());
		this.ModifyTime = aLPContSchema.getModifyTime();
		this.FirstTrialOperator = aLPContSchema.getFirstTrialOperator();
		this.FirstTrialDate = fDate.getDate( aLPContSchema.getFirstTrialDate());
		this.FirstTrialTime = aLPContSchema.getFirstTrialTime();
		this.ReceiveOperator = aLPContSchema.getReceiveOperator();
		this.ReceiveDate = fDate.getDate( aLPContSchema.getReceiveDate());
		this.ReceiveTime = aLPContSchema.getReceiveTime();
		this.TempFeeNo = aLPContSchema.getTempFeeNo();
		this.ProposalType = aLPContSchema.getProposalType();
		this.SaleChnlDetail = aLPContSchema.getSaleChnlDetail();
		this.ContPrintLoFlag = aLPContSchema.getContPrintLoFlag();
		this.ContPremFeeNo = aLPContSchema.getContPremFeeNo();
		this.CustomerReceiptNo = aLPContSchema.getCustomerReceiptNo();
		this.CInValiDate = fDate.getDate( aLPContSchema.getCInValiDate());
		this.Copys = aLPContSchema.getCopys();
		this.DegreeType = aLPContSchema.getDegreeType();
		this.HandlerDate = fDate.getDate( aLPContSchema.getHandlerDate());
		this.HandlerPrint = aLPContSchema.getHandlerPrint();
		this.StateFlag = aLPContSchema.getStateFlag();
		this.PremScope = aLPContSchema.getPremScope();
		this.IntlFlag = aLPContSchema.getIntlFlag();
		this.UWConfirmNo = aLPContSchema.getUWConfirmNo();
		this.PayerType = aLPContSchema.getPayerType();
		this.GrpAgentCom = aLPContSchema.getGrpAgentCom();
		this.GrpAgentCode = aLPContSchema.getGrpAgentCode();
		this.GrpAgentName = aLPContSchema.getGrpAgentName();
		this.AccType = aLPContSchema.getAccType();
		this.Crs_SaleChnl = aLPContSchema.getCrs_SaleChnl();
		this.Crs_BussType = aLPContSchema.getCrs_BussType();
		this.GrpAgentIDNo = aLPContSchema.getGrpAgentIDNo();
		this.DueFeeMsgFlag = aLPContSchema.getDueFeeMsgFlag();
		this.PayMethod = aLPContSchema.getPayMethod();
		this.ExiSpec = aLPContSchema.getExiSpec();
		this.ExPayMode = aLPContSchema.getExPayMode();
		this.AgentSaleCode = aLPContSchema.getAgentSaleCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("EdorType") == null )
				this.EdorType = null;
			else
				this.EdorType = rs.getString("EdorType").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("ProposalContNo") == null )
				this.ProposalContNo = null;
			else
				this.ProposalContNo = rs.getString("ProposalContNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("ContType") == null )
				this.ContType = null;
			else
				this.ContType = rs.getString("ContType").trim();

			if( rs.getString("FamilyType") == null )
				this.FamilyType = null;
			else
				this.FamilyType = rs.getString("FamilyType").trim();

			if( rs.getString("FamilyID") == null )
				this.FamilyID = null;
			else
				this.FamilyID = rs.getString("FamilyID").trim();

			if( rs.getString("PolType") == null )
				this.PolType = null;
			else
				this.PolType = rs.getString("PolType").trim();

			if( rs.getString("CardFlag") == null )
				this.CardFlag = null;
			else
				this.CardFlag = rs.getString("CardFlag").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ExecuteCom") == null )
				this.ExecuteCom = null;
			else
				this.ExecuteCom = rs.getString("ExecuteCom").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("AgentCode1") == null )
				this.AgentCode1 = null;
			else
				this.AgentCode1 = rs.getString("AgentCode1").trim();

			if( rs.getString("AgentType") == null )
				this.AgentType = null;
			else
				this.AgentType = rs.getString("AgentType").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("Handler") == null )
				this.Handler = null;
			else
				this.Handler = rs.getString("Handler").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("AppntNo") == null )
				this.AppntNo = null;
			else
				this.AppntNo = rs.getString("AppntNo").trim();

			if( rs.getString("AppntName") == null )
				this.AppntName = null;
			else
				this.AppntName = rs.getString("AppntName").trim();

			if( rs.getString("AppntSex") == null )
				this.AppntSex = null;
			else
				this.AppntSex = rs.getString("AppntSex").trim();

			this.AppntBirthday = rs.getDate("AppntBirthday");
			if( rs.getString("AppntIDType") == null )
				this.AppntIDType = null;
			else
				this.AppntIDType = rs.getString("AppntIDType").trim();

			if( rs.getString("AppntIDNo") == null )
				this.AppntIDNo = null;
			else
				this.AppntIDNo = rs.getString("AppntIDNo").trim();

			if( rs.getString("InsuredNo") == null )
				this.InsuredNo = null;
			else
				this.InsuredNo = rs.getString("InsuredNo").trim();

			if( rs.getString("InsuredName") == null )
				this.InsuredName = null;
			else
				this.InsuredName = rs.getString("InsuredName").trim();

			if( rs.getString("InsuredSex") == null )
				this.InsuredSex = null;
			else
				this.InsuredSex = rs.getString("InsuredSex").trim();

			this.InsuredBirthday = rs.getDate("InsuredBirthday");
			if( rs.getString("InsuredIDType") == null )
				this.InsuredIDType = null;
			else
				this.InsuredIDType = rs.getString("InsuredIDType").trim();

			if( rs.getString("InsuredIDNo") == null )
				this.InsuredIDNo = null;
			else
				this.InsuredIDNo = rs.getString("InsuredIDNo").trim();

			this.PayIntv = rs.getInt("PayIntv");
			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("PayLocation") == null )
				this.PayLocation = null;
			else
				this.PayLocation = rs.getString("PayLocation").trim();

			if( rs.getString("DisputedFlag") == null )
				this.DisputedFlag = null;
			else
				this.DisputedFlag = rs.getString("DisputedFlag").trim();

			if( rs.getString("OutPayFlag") == null )
				this.OutPayFlag = null;
			else
				this.OutPayFlag = rs.getString("OutPayFlag").trim();

			if( rs.getString("GetPolMode") == null )
				this.GetPolMode = null;
			else
				this.GetPolMode = rs.getString("GetPolMode").trim();

			if( rs.getString("SignCom") == null )
				this.SignCom = null;
			else
				this.SignCom = rs.getString("SignCom").trim();

			this.SignDate = rs.getDate("SignDate");
			if( rs.getString("SignTime") == null )
				this.SignTime = null;
			else
				this.SignTime = rs.getString("SignTime").trim();

			if( rs.getString("ConsignNo") == null )
				this.ConsignNo = null;
			else
				this.ConsignNo = rs.getString("ConsignNo").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			this.PrintCount = rs.getInt("PrintCount");
			this.LostTimes = rs.getInt("LostTimes");
			if( rs.getString("Lang") == null )
				this.Lang = null;
			else
				this.Lang = rs.getString("Lang").trim();

			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			this.Peoples = rs.getInt("Peoples");
			this.Mult = rs.getDouble("Mult");
			this.Prem = rs.getDouble("Prem");
			this.Amnt = rs.getDouble("Amnt");
			this.SumPrem = rs.getDouble("SumPrem");
			this.Dif = rs.getDouble("Dif");
			this.PaytoDate = rs.getDate("PaytoDate");
			this.FirstPayDate = rs.getDate("FirstPayDate");
			this.CValiDate = rs.getDate("CValiDate");
			if( rs.getString("InputOperator") == null )
				this.InputOperator = null;
			else
				this.InputOperator = rs.getString("InputOperator").trim();

			this.InputDate = rs.getDate("InputDate");
			if( rs.getString("InputTime") == null )
				this.InputTime = null;
			else
				this.InputTime = rs.getString("InputTime").trim();

			if( rs.getString("ApproveFlag") == null )
				this.ApproveFlag = null;
			else
				this.ApproveFlag = rs.getString("ApproveFlag").trim();

			if( rs.getString("ApproveCode") == null )
				this.ApproveCode = null;
			else
				this.ApproveCode = rs.getString("ApproveCode").trim();

			this.ApproveDate = rs.getDate("ApproveDate");
			if( rs.getString("ApproveTime") == null )
				this.ApproveTime = null;
			else
				this.ApproveTime = rs.getString("ApproveTime").trim();

			if( rs.getString("UWFlag") == null )
				this.UWFlag = null;
			else
				this.UWFlag = rs.getString("UWFlag").trim();

			if( rs.getString("UWOperator") == null )
				this.UWOperator = null;
			else
				this.UWOperator = rs.getString("UWOperator").trim();

			this.UWDate = rs.getDate("UWDate");
			if( rs.getString("UWTime") == null )
				this.UWTime = null;
			else
				this.UWTime = rs.getString("UWTime").trim();

			if( rs.getString("AppFlag") == null )
				this.AppFlag = null;
			else
				this.AppFlag = rs.getString("AppFlag").trim();

			this.PolApplyDate = rs.getDate("PolApplyDate");
			this.GetPolDate = rs.getDate("GetPolDate");
			if( rs.getString("GetPolTime") == null )
				this.GetPolTime = null;
			else
				this.GetPolTime = rs.getString("GetPolTime").trim();

			this.CustomGetPolDate = rs.getDate("CustomGetPolDate");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("FirstTrialOperator") == null )
				this.FirstTrialOperator = null;
			else
				this.FirstTrialOperator = rs.getString("FirstTrialOperator").trim();

			this.FirstTrialDate = rs.getDate("FirstTrialDate");
			if( rs.getString("FirstTrialTime") == null )
				this.FirstTrialTime = null;
			else
				this.FirstTrialTime = rs.getString("FirstTrialTime").trim();

			if( rs.getString("ReceiveOperator") == null )
				this.ReceiveOperator = null;
			else
				this.ReceiveOperator = rs.getString("ReceiveOperator").trim();

			this.ReceiveDate = rs.getDate("ReceiveDate");
			if( rs.getString("ReceiveTime") == null )
				this.ReceiveTime = null;
			else
				this.ReceiveTime = rs.getString("ReceiveTime").trim();

			if( rs.getString("TempFeeNo") == null )
				this.TempFeeNo = null;
			else
				this.TempFeeNo = rs.getString("TempFeeNo").trim();

			if( rs.getString("ProposalType") == null )
				this.ProposalType = null;
			else
				this.ProposalType = rs.getString("ProposalType").trim();

			if( rs.getString("SaleChnlDetail") == null )
				this.SaleChnlDetail = null;
			else
				this.SaleChnlDetail = rs.getString("SaleChnlDetail").trim();

			if( rs.getString("ContPrintLoFlag") == null )
				this.ContPrintLoFlag = null;
			else
				this.ContPrintLoFlag = rs.getString("ContPrintLoFlag").trim();

			if( rs.getString("ContPremFeeNo") == null )
				this.ContPremFeeNo = null;
			else
				this.ContPremFeeNo = rs.getString("ContPremFeeNo").trim();

			if( rs.getString("CustomerReceiptNo") == null )
				this.CustomerReceiptNo = null;
			else
				this.CustomerReceiptNo = rs.getString("CustomerReceiptNo").trim();

			this.CInValiDate = rs.getDate("CInValiDate");
			this.Copys = rs.getDouble("Copys");
			if( rs.getString("DegreeType") == null )
				this.DegreeType = null;
			else
				this.DegreeType = rs.getString("DegreeType").trim();

			this.HandlerDate = rs.getDate("HandlerDate");
			if( rs.getString("HandlerPrint") == null )
				this.HandlerPrint = null;
			else
				this.HandlerPrint = rs.getString("HandlerPrint").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			this.PremScope = rs.getDouble("PremScope");
			if( rs.getString("IntlFlag") == null )
				this.IntlFlag = null;
			else
				this.IntlFlag = rs.getString("IntlFlag").trim();

			if( rs.getString("UWConfirmNo") == null )
				this.UWConfirmNo = null;
			else
				this.UWConfirmNo = rs.getString("UWConfirmNo").trim();

			if( rs.getString("PayerType") == null )
				this.PayerType = null;
			else
				this.PayerType = rs.getString("PayerType").trim();

			if( rs.getString("GrpAgentCom") == null )
				this.GrpAgentCom = null;
			else
				this.GrpAgentCom = rs.getString("GrpAgentCom").trim();

			if( rs.getString("GrpAgentCode") == null )
				this.GrpAgentCode = null;
			else
				this.GrpAgentCode = rs.getString("GrpAgentCode").trim();

			if( rs.getString("GrpAgentName") == null )
				this.GrpAgentName = null;
			else
				this.GrpAgentName = rs.getString("GrpAgentName").trim();

			if( rs.getString("AccType") == null )
				this.AccType = null;
			else
				this.AccType = rs.getString("AccType").trim();

			if( rs.getString("Crs_SaleChnl") == null )
				this.Crs_SaleChnl = null;
			else
				this.Crs_SaleChnl = rs.getString("Crs_SaleChnl").trim();

			if( rs.getString("Crs_BussType") == null )
				this.Crs_BussType = null;
			else
				this.Crs_BussType = rs.getString("Crs_BussType").trim();

			if( rs.getString("GrpAgentIDNo") == null )
				this.GrpAgentIDNo = null;
			else
				this.GrpAgentIDNo = rs.getString("GrpAgentIDNo").trim();

			if( rs.getString("DueFeeMsgFlag") == null )
				this.DueFeeMsgFlag = null;
			else
				this.DueFeeMsgFlag = rs.getString("DueFeeMsgFlag").trim();

			if( rs.getString("PayMethod") == null )
				this.PayMethod = null;
			else
				this.PayMethod = rs.getString("PayMethod").trim();

			if( rs.getString("ExiSpec") == null )
				this.ExiSpec = null;
			else
				this.ExiSpec = rs.getString("ExiSpec").trim();

			if( rs.getString("ExPayMode") == null )
				this.ExPayMode = null;
			else
				this.ExPayMode = rs.getString("ExPayMode").trim();

			if( rs.getString("AgentSaleCode") == null )
				this.AgentSaleCode = null;
			else
				this.AgentSaleCode = rs.getString("AgentSaleCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LPCont表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPContSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LPContSchema getSchema()
	{
		LPContSchema aLPContSchema = new LPContSchema();
		aLPContSchema.setSchema(this);
		return aLPContSchema;
	}

	public LPContDB getDB()
	{
		LPContDB aDBOper = new LPContDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPCont描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FamilyType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FamilyID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AppntBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InsuredBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayLocation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DisputedFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetPolMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConsignNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PrintCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LostTimes));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Lang)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Dif));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PaytoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FirstPayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InputDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApproveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetPolTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CustomGetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTrialOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FirstTrialDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTrialTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnlDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPrintLoFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPremFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CInValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Copys));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DegreeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HandlerDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerPrint)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PremScope));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IntlFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWConfirmNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayerType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_BussType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DueFeeMsgFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMethod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExiSpec)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExPayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentSaleCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPCont>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			FamilyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			FamilyID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CardFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AgentCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			AppntSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			AppntBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			AppntIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			AppntIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			InsuredBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			InsuredIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			InsuredIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).intValue();
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			PayLocation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			DisputedFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			OutPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			GetPolMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			SignCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			SignTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			ConsignNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			PrintCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).intValue();
			LostTimes= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).intValue();
			Lang = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			Peoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,52,SysConst.PACKAGESPILTER))).intValue();
			Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,53,SysConst.PACKAGESPILTER))).doubleValue();
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,54,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,55,SysConst.PACKAGESPILTER))).doubleValue();
			SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).doubleValue();
			Dif = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,57,SysConst.PACKAGESPILTER))).doubleValue();
			PaytoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58,SysConst.PACKAGESPILTER));
			FirstPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59,SysConst.PACKAGESPILTER));
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60,SysConst.PACKAGESPILTER));
			InputOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62,SysConst.PACKAGESPILTER));
			InputTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			ApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66,SysConst.PACKAGESPILTER));
			ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
			UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
			UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70,SysConst.PACKAGESPILTER));
			UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
			AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
			PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73,SysConst.PACKAGESPILTER));
			GetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74,SysConst.PACKAGESPILTER));
			GetPolTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
			CustomGetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76,SysConst.PACKAGESPILTER));
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 82, SysConst.PACKAGESPILTER );
			FirstTrialOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 83, SysConst.PACKAGESPILTER );
			FirstTrialDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 84,SysConst.PACKAGESPILTER));
			FirstTrialTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 85, SysConst.PACKAGESPILTER );
			ReceiveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 86, SysConst.PACKAGESPILTER );
			ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 87,SysConst.PACKAGESPILTER));
			ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 88, SysConst.PACKAGESPILTER );
			TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 89, SysConst.PACKAGESPILTER );
			ProposalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 90, SysConst.PACKAGESPILTER );
			SaleChnlDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 91, SysConst.PACKAGESPILTER );
			ContPrintLoFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 92, SysConst.PACKAGESPILTER );
			ContPremFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 93, SysConst.PACKAGESPILTER );
			CustomerReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 94, SysConst.PACKAGESPILTER );
			CInValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 95,SysConst.PACKAGESPILTER));
			Copys = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,96,SysConst.PACKAGESPILTER))).doubleValue();
			DegreeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 97, SysConst.PACKAGESPILTER );
			HandlerDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 98,SysConst.PACKAGESPILTER));
			HandlerPrint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 99, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 100, SysConst.PACKAGESPILTER );
			PremScope = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,101,SysConst.PACKAGESPILTER))).doubleValue();
			IntlFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 102, SysConst.PACKAGESPILTER );
			UWConfirmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 103, SysConst.PACKAGESPILTER );
			PayerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 104, SysConst.PACKAGESPILTER );
			GrpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 105, SysConst.PACKAGESPILTER );
			GrpAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 106, SysConst.PACKAGESPILTER );
			GrpAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 107, SysConst.PACKAGESPILTER );
			AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 108, SysConst.PACKAGESPILTER );
			Crs_SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 109, SysConst.PACKAGESPILTER );
			Crs_BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 110, SysConst.PACKAGESPILTER );
			GrpAgentIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 111, SysConst.PACKAGESPILTER );
			DueFeeMsgFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 112, SysConst.PACKAGESPILTER );
			PayMethod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 113, SysConst.PACKAGESPILTER );
			ExiSpec = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 114, SysConst.PACKAGESPILTER );
			ExPayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 115, SysConst.PACKAGESPILTER );
			AgentSaleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 116, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPContSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("EdorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("ProposalContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("ContType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
		}
		if (FCode.equals("FamilyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyType));
		}
		if (FCode.equals("FamilyID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyID));
		}
		if (FCode.equals("PolType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
		}
		if (FCode.equals("CardFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardFlag));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ExecuteCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("AgentCode1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
		}
		if (FCode.equals("AgentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("Handler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("AppntNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
		}
		if (FCode.equals("AppntName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
		}
		if (FCode.equals("AppntSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
		}
		if (FCode.equals("AppntBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
		}
		if (FCode.equals("AppntIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDType));
		}
		if (FCode.equals("AppntIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDNo));
		}
		if (FCode.equals("InsuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
		}
		if (FCode.equals("InsuredName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
		}
		if (FCode.equals("InsuredSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
		}
		if (FCode.equals("InsuredBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
		}
		if (FCode.equals("InsuredIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
		}
		if (FCode.equals("InsuredIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNo));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("PayLocation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayLocation));
		}
		if (FCode.equals("DisputedFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
		}
		if (FCode.equals("OutPayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
		}
		if (FCode.equals("GetPolMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolMode));
		}
		if (FCode.equals("SignCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
		}
		if (FCode.equals("SignTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
		}
		if (FCode.equals("ConsignNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConsignNo));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("PrintCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
		}
		if (FCode.equals("LostTimes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LostTimes));
		}
		if (FCode.equals("Lang"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Lang));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Peoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
		}
		if (FCode.equals("Mult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("SumPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
		}
		if (FCode.equals("Dif"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
		}
		if (FCode.equals("PaytoDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
		}
		if (FCode.equals("FirstPayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstPayDate()));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("InputOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
		}
		if (FCode.equals("InputDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
		}
		if (FCode.equals("InputTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputTime));
		}
		if (FCode.equals("ApproveFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
		}
		if (FCode.equals("ApproveCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
		}
		if (FCode.equals("ApproveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
		}
		if (FCode.equals("ApproveTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
		}
		if (FCode.equals("UWFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
		}
		if (FCode.equals("UWOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
		}
		if (FCode.equals("UWDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
		}
		if (FCode.equals("UWTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
		}
		if (FCode.equals("AppFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
		}
		if (FCode.equals("PolApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
		}
		if (FCode.equals("GetPolDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
		}
		if (FCode.equals("GetPolTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolTime));
		}
		if (FCode.equals("CustomGetPolDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("FirstTrialOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
		}
		if (FCode.equals("FirstTrialDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
		}
		if (FCode.equals("FirstTrialTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
		}
		if (FCode.equals("ReceiveOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
		}
		if (FCode.equals("ReceiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
		}
		if (FCode.equals("ReceiveTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
		}
		if (FCode.equals("TempFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
		}
		if (FCode.equals("ProposalType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalType));
		}
		if (FCode.equals("SaleChnlDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlDetail));
		}
		if (FCode.equals("ContPrintLoFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPrintLoFlag));
		}
		if (FCode.equals("ContPremFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPremFeeNo));
		}
		if (FCode.equals("CustomerReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerReceiptNo));
		}
		if (FCode.equals("CInValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
		}
		if (FCode.equals("Copys"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Copys));
		}
		if (FCode.equals("DegreeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DegreeType));
		}
		if (FCode.equals("HandlerDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHandlerDate()));
		}
		if (FCode.equals("HandlerPrint"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerPrint));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("PremScope"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremScope));
		}
		if (FCode.equals("IntlFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IntlFlag));
		}
		if (FCode.equals("UWConfirmNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWConfirmNo));
		}
		if (FCode.equals("PayerType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayerType));
		}
		if (FCode.equals("GrpAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCom));
		}
		if (FCode.equals("GrpAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCode));
		}
		if (FCode.equals("GrpAgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentName));
		}
		if (FCode.equals("AccType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
		}
		if (FCode.equals("Crs_SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_SaleChnl));
		}
		if (FCode.equals("Crs_BussType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_BussType));
		}
		if (FCode.equals("GrpAgentIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentIDNo));
		}
		if (FCode.equals("DueFeeMsgFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DueFeeMsgFlag));
		}
		if (FCode.equals("PayMethod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMethod));
		}
		if (FCode.equals("ExiSpec"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExiSpec));
		}
		if (FCode.equals("ExPayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExPayMode));
		}
		if (FCode.equals("AgentSaleCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSaleCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ContType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(FamilyType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(FamilyID);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PolType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CardFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(AgentCode1);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(AgentType);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Handler);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(AppntNo);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(AppntName);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(AppntSex);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(AppntIDType);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(AppntIDNo);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(InsuredNo);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(InsuredName);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(InsuredSex);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(InsuredIDType);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(InsuredIDNo);
				break;
			case 33:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(PayLocation);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(DisputedFlag);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(OutPayFlag);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(GetPolMode);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(SignCom);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(SignTime);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(ConsignNo);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 46:
				strFieldValue = String.valueOf(PrintCount);
				break;
			case 47:
				strFieldValue = String.valueOf(LostTimes);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(Lang);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 51:
				strFieldValue = String.valueOf(Peoples);
				break;
			case 52:
				strFieldValue = String.valueOf(Mult);
				break;
			case 53:
				strFieldValue = String.valueOf(Prem);
				break;
			case 54:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 55:
				strFieldValue = String.valueOf(SumPrem);
				break;
			case 56:
				strFieldValue = String.valueOf(Dif);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstPayDate()));
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(InputOperator);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(InputTime);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(ApproveFlag);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(ApproveCode);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(ApproveTime);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(UWFlag);
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(UWOperator);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
				break;
			case 70:
				strFieldValue = StrTool.GBKToUnicode(UWTime);
				break;
			case 71:
				strFieldValue = StrTool.GBKToUnicode(AppFlag);
				break;
			case 72:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
				break;
			case 73:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
				break;
			case 74:
				strFieldValue = StrTool.GBKToUnicode(GetPolTime);
				break;
			case 75:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
				break;
			case 76:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 77:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 78:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 79:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 80:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 81:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 82:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialOperator);
				break;
			case 83:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
				break;
			case 84:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialTime);
				break;
			case 85:
				strFieldValue = StrTool.GBKToUnicode(ReceiveOperator);
				break;
			case 86:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
				break;
			case 87:
				strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
				break;
			case 88:
				strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
				break;
			case 89:
				strFieldValue = StrTool.GBKToUnicode(ProposalType);
				break;
			case 90:
				strFieldValue = StrTool.GBKToUnicode(SaleChnlDetail);
				break;
			case 91:
				strFieldValue = StrTool.GBKToUnicode(ContPrintLoFlag);
				break;
			case 92:
				strFieldValue = StrTool.GBKToUnicode(ContPremFeeNo);
				break;
			case 93:
				strFieldValue = StrTool.GBKToUnicode(CustomerReceiptNo);
				break;
			case 94:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
				break;
			case 95:
				strFieldValue = String.valueOf(Copys);
				break;
			case 96:
				strFieldValue = StrTool.GBKToUnicode(DegreeType);
				break;
			case 97:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHandlerDate()));
				break;
			case 98:
				strFieldValue = StrTool.GBKToUnicode(HandlerPrint);
				break;
			case 99:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 100:
				strFieldValue = String.valueOf(PremScope);
				break;
			case 101:
				strFieldValue = StrTool.GBKToUnicode(IntlFlag);
				break;
			case 102:
				strFieldValue = StrTool.GBKToUnicode(UWConfirmNo);
				break;
			case 103:
				strFieldValue = StrTool.GBKToUnicode(PayerType);
				break;
			case 104:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCom);
				break;
			case 105:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCode);
				break;
			case 106:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentName);
				break;
			case 107:
				strFieldValue = StrTool.GBKToUnicode(AccType);
				break;
			case 108:
				strFieldValue = StrTool.GBKToUnicode(Crs_SaleChnl);
				break;
			case 109:
				strFieldValue = StrTool.GBKToUnicode(Crs_BussType);
				break;
			case 110:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentIDNo);
				break;
			case 111:
				strFieldValue = StrTool.GBKToUnicode(DueFeeMsgFlag);
				break;
			case 112:
				strFieldValue = StrTool.GBKToUnicode(PayMethod);
				break;
			case 113:
				strFieldValue = StrTool.GBKToUnicode(ExiSpec);
				break;
			case 114:
				strFieldValue = StrTool.GBKToUnicode(ExPayMode);
				break;
			case 115:
				strFieldValue = StrTool.GBKToUnicode(AgentSaleCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorType = FValue.trim();
			}
			else
				EdorType = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalContNo = FValue.trim();
			}
			else
				ProposalContNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("ContType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContType = FValue.trim();
			}
			else
				ContType = null;
		}
		if (FCode.equalsIgnoreCase("FamilyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FamilyType = FValue.trim();
			}
			else
				FamilyType = null;
		}
		if (FCode.equalsIgnoreCase("FamilyID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FamilyID = FValue.trim();
			}
			else
				FamilyID = null;
		}
		if (FCode.equalsIgnoreCase("PolType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolType = FValue.trim();
			}
			else
				PolType = null;
		}
		if (FCode.equalsIgnoreCase("CardFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardFlag = FValue.trim();
			}
			else
				CardFlag = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ExecuteCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExecuteCom = FValue.trim();
			}
			else
				ExecuteCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode1 = FValue.trim();
			}
			else
				AgentCode1 = null;
		}
		if (FCode.equalsIgnoreCase("AgentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentType = FValue.trim();
			}
			else
				AgentType = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("Handler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler = FValue.trim();
			}
			else
				Handler = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("AppntNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntNo = FValue.trim();
			}
			else
				AppntNo = null;
		}
		if (FCode.equalsIgnoreCase("AppntName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntName = FValue.trim();
			}
			else
				AppntName = null;
		}
		if (FCode.equalsIgnoreCase("AppntSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntSex = FValue.trim();
			}
			else
				AppntSex = null;
		}
		if (FCode.equalsIgnoreCase("AppntBirthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AppntBirthday = fDate.getDate( FValue );
			}
			else
				AppntBirthday = null;
		}
		if (FCode.equalsIgnoreCase("AppntIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIDType = FValue.trim();
			}
			else
				AppntIDType = null;
		}
		if (FCode.equalsIgnoreCase("AppntIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIDNo = FValue.trim();
			}
			else
				AppntIDNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredNo = FValue.trim();
			}
			else
				InsuredNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredName = FValue.trim();
			}
			else
				InsuredName = null;
		}
		if (FCode.equalsIgnoreCase("InsuredSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredSex = FValue.trim();
			}
			else
				InsuredSex = null;
		}
		if (FCode.equalsIgnoreCase("InsuredBirthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InsuredBirthday = fDate.getDate( FValue );
			}
			else
				InsuredBirthday = null;
		}
		if (FCode.equalsIgnoreCase("InsuredIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredIDType = FValue.trim();
			}
			else
				InsuredIDType = null;
		}
		if (FCode.equalsIgnoreCase("InsuredIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredIDNo = FValue.trim();
			}
			else
				InsuredIDNo = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("PayLocation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayLocation = FValue.trim();
			}
			else
				PayLocation = null;
		}
		if (FCode.equalsIgnoreCase("DisputedFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DisputedFlag = FValue.trim();
			}
			else
				DisputedFlag = null;
		}
		if (FCode.equalsIgnoreCase("OutPayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutPayFlag = FValue.trim();
			}
			else
				OutPayFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetPolMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetPolMode = FValue.trim();
			}
			else
				GetPolMode = null;
		}
		if (FCode.equalsIgnoreCase("SignCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignCom = FValue.trim();
			}
			else
				SignCom = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SignDate = fDate.getDate( FValue );
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("SignTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignTime = FValue.trim();
			}
			else
				SignTime = null;
		}
		if (FCode.equalsIgnoreCase("ConsignNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConsignNo = FValue.trim();
			}
			else
				ConsignNo = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("PrintCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PrintCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("LostTimes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				LostTimes = i;
			}
		}
		if (FCode.equalsIgnoreCase("Lang"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Lang = FValue.trim();
			}
			else
				Lang = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Peoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("Mult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Mult = d;
			}
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Dif"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Dif = d;
			}
		}
		if (FCode.equalsIgnoreCase("PaytoDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PaytoDate = fDate.getDate( FValue );
			}
			else
				PaytoDate = null;
		}
		if (FCode.equalsIgnoreCase("FirstPayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FirstPayDate = fDate.getDate( FValue );
			}
			else
				FirstPayDate = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("InputOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputOperator = FValue.trim();
			}
			else
				InputOperator = null;
		}
		if (FCode.equalsIgnoreCase("InputDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InputDate = fDate.getDate( FValue );
			}
			else
				InputDate = null;
		}
		if (FCode.equalsIgnoreCase("InputTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputTime = FValue.trim();
			}
			else
				InputTime = null;
		}
		if (FCode.equalsIgnoreCase("ApproveFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApproveFlag = FValue.trim();
			}
			else
				ApproveFlag = null;
		}
		if (FCode.equalsIgnoreCase("ApproveCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApproveCode = FValue.trim();
			}
			else
				ApproveCode = null;
		}
		if (FCode.equalsIgnoreCase("ApproveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ApproveDate = fDate.getDate( FValue );
			}
			else
				ApproveDate = null;
		}
		if (FCode.equalsIgnoreCase("ApproveTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApproveTime = FValue.trim();
			}
			else
				ApproveTime = null;
		}
		if (FCode.equalsIgnoreCase("UWFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWFlag = FValue.trim();
			}
			else
				UWFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWOperator = FValue.trim();
			}
			else
				UWOperator = null;
		}
		if (FCode.equalsIgnoreCase("UWDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				UWDate = fDate.getDate( FValue );
			}
			else
				UWDate = null;
		}
		if (FCode.equalsIgnoreCase("UWTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWTime = FValue.trim();
			}
			else
				UWTime = null;
		}
		if (FCode.equalsIgnoreCase("AppFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppFlag = FValue.trim();
			}
			else
				AppFlag = null;
		}
		if (FCode.equalsIgnoreCase("PolApplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PolApplyDate = fDate.getDate( FValue );
			}
			else
				PolApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("GetPolDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GetPolDate = fDate.getDate( FValue );
			}
			else
				GetPolDate = null;
		}
		if (FCode.equalsIgnoreCase("GetPolTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetPolTime = FValue.trim();
			}
			else
				GetPolTime = null;
		}
		if (FCode.equalsIgnoreCase("CustomGetPolDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CustomGetPolDate = fDate.getDate( FValue );
			}
			else
				CustomGetPolDate = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialOperator = FValue.trim();
			}
			else
				FirstTrialOperator = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FirstTrialDate = fDate.getDate( FValue );
			}
			else
				FirstTrialDate = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialTime = FValue.trim();
			}
			else
				FirstTrialTime = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveOperator = FValue.trim();
			}
			else
				ReceiveOperator = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReceiveDate = fDate.getDate( FValue );
			}
			else
				ReceiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveTime = FValue.trim();
			}
			else
				ReceiveTime = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeNo = FValue.trim();
			}
			else
				TempFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalType = FValue.trim();
			}
			else
				ProposalType = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnlDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnlDetail = FValue.trim();
			}
			else
				SaleChnlDetail = null;
		}
		if (FCode.equalsIgnoreCase("ContPrintLoFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPrintLoFlag = FValue.trim();
			}
			else
				ContPrintLoFlag = null;
		}
		if (FCode.equalsIgnoreCase("ContPremFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPremFeeNo = FValue.trim();
			}
			else
				ContPremFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerReceiptNo = FValue.trim();
			}
			else
				CustomerReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("CInValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CInValiDate = fDate.getDate( FValue );
			}
			else
				CInValiDate = null;
		}
		if (FCode.equalsIgnoreCase("Copys"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Copys = d;
			}
		}
		if (FCode.equalsIgnoreCase("DegreeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DegreeType = FValue.trim();
			}
			else
				DegreeType = null;
		}
		if (FCode.equalsIgnoreCase("HandlerDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HandlerDate = fDate.getDate( FValue );
			}
			else
				HandlerDate = null;
		}
		if (FCode.equalsIgnoreCase("HandlerPrint"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerPrint = FValue.trim();
			}
			else
				HandlerPrint = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("PremScope"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PremScope = d;
			}
		}
		if (FCode.equalsIgnoreCase("IntlFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IntlFlag = FValue.trim();
			}
			else
				IntlFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWConfirmNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWConfirmNo = FValue.trim();
			}
			else
				UWConfirmNo = null;
		}
		if (FCode.equalsIgnoreCase("PayerType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayerType = FValue.trim();
			}
			else
				PayerType = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCom = FValue.trim();
			}
			else
				GrpAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCode = FValue.trim();
			}
			else
				GrpAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentName = FValue.trim();
			}
			else
				GrpAgentName = null;
		}
		if (FCode.equalsIgnoreCase("AccType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccType = FValue.trim();
			}
			else
				AccType = null;
		}
		if (FCode.equalsIgnoreCase("Crs_SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_SaleChnl = FValue.trim();
			}
			else
				Crs_SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("Crs_BussType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_BussType = FValue.trim();
			}
			else
				Crs_BussType = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentIDNo = FValue.trim();
			}
			else
				GrpAgentIDNo = null;
		}
		if (FCode.equalsIgnoreCase("DueFeeMsgFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DueFeeMsgFlag = FValue.trim();
			}
			else
				DueFeeMsgFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayMethod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMethod = FValue.trim();
			}
			else
				PayMethod = null;
		}
		if (FCode.equalsIgnoreCase("ExiSpec"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExiSpec = FValue.trim();
			}
			else
				ExiSpec = null;
		}
		if (FCode.equalsIgnoreCase("ExPayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExPayMode = FValue.trim();
			}
			else
				ExPayMode = null;
		}
		if (FCode.equalsIgnoreCase("AgentSaleCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentSaleCode = FValue.trim();
			}
			else
				AgentSaleCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LPContSchema other = (LPContSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (EdorType == null ? other.getEdorType() == null : EdorType.equals(other.getEdorType()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (ProposalContNo == null ? other.getProposalContNo() == null : ProposalContNo.equals(other.getProposalContNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (ContType == null ? other.getContType() == null : ContType.equals(other.getContType()))
			&& (FamilyType == null ? other.getFamilyType() == null : FamilyType.equals(other.getFamilyType()))
			&& (FamilyID == null ? other.getFamilyID() == null : FamilyID.equals(other.getFamilyID()))
			&& (PolType == null ? other.getPolType() == null : PolType.equals(other.getPolType()))
			&& (CardFlag == null ? other.getCardFlag() == null : CardFlag.equals(other.getCardFlag()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ExecuteCom == null ? other.getExecuteCom() == null : ExecuteCom.equals(other.getExecuteCom()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (AgentCode1 == null ? other.getAgentCode1() == null : AgentCode1.equals(other.getAgentCode1()))
			&& (AgentType == null ? other.getAgentType() == null : AgentType.equals(other.getAgentType()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (Handler == null ? other.getHandler() == null : Handler.equals(other.getHandler()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (AppntNo == null ? other.getAppntNo() == null : AppntNo.equals(other.getAppntNo()))
			&& (AppntName == null ? other.getAppntName() == null : AppntName.equals(other.getAppntName()))
			&& (AppntSex == null ? other.getAppntSex() == null : AppntSex.equals(other.getAppntSex()))
			&& (AppntBirthday == null ? other.getAppntBirthday() == null : fDate.getString(AppntBirthday).equals(other.getAppntBirthday()))
			&& (AppntIDType == null ? other.getAppntIDType() == null : AppntIDType.equals(other.getAppntIDType()))
			&& (AppntIDNo == null ? other.getAppntIDNo() == null : AppntIDNo.equals(other.getAppntIDNo()))
			&& (InsuredNo == null ? other.getInsuredNo() == null : InsuredNo.equals(other.getInsuredNo()))
			&& (InsuredName == null ? other.getInsuredName() == null : InsuredName.equals(other.getInsuredName()))
			&& (InsuredSex == null ? other.getInsuredSex() == null : InsuredSex.equals(other.getInsuredSex()))
			&& (InsuredBirthday == null ? other.getInsuredBirthday() == null : fDate.getString(InsuredBirthday).equals(other.getInsuredBirthday()))
			&& (InsuredIDType == null ? other.getInsuredIDType() == null : InsuredIDType.equals(other.getInsuredIDType()))
			&& (InsuredIDNo == null ? other.getInsuredIDNo() == null : InsuredIDNo.equals(other.getInsuredIDNo()))
			&& PayIntv == other.getPayIntv()
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (PayLocation == null ? other.getPayLocation() == null : PayLocation.equals(other.getPayLocation()))
			&& (DisputedFlag == null ? other.getDisputedFlag() == null : DisputedFlag.equals(other.getDisputedFlag()))
			&& (OutPayFlag == null ? other.getOutPayFlag() == null : OutPayFlag.equals(other.getOutPayFlag()))
			&& (GetPolMode == null ? other.getGetPolMode() == null : GetPolMode.equals(other.getGetPolMode()))
			&& (SignCom == null ? other.getSignCom() == null : SignCom.equals(other.getSignCom()))
			&& (SignDate == null ? other.getSignDate() == null : fDate.getString(SignDate).equals(other.getSignDate()))
			&& (SignTime == null ? other.getSignTime() == null : SignTime.equals(other.getSignTime()))
			&& (ConsignNo == null ? other.getConsignNo() == null : ConsignNo.equals(other.getConsignNo()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& PrintCount == other.getPrintCount()
			&& LostTimes == other.getLostTimes()
			&& (Lang == null ? other.getLang() == null : Lang.equals(other.getLang()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& Peoples == other.getPeoples()
			&& Mult == other.getMult()
			&& Prem == other.getPrem()
			&& Amnt == other.getAmnt()
			&& SumPrem == other.getSumPrem()
			&& Dif == other.getDif()
			&& (PaytoDate == null ? other.getPaytoDate() == null : fDate.getString(PaytoDate).equals(other.getPaytoDate()))
			&& (FirstPayDate == null ? other.getFirstPayDate() == null : fDate.getString(FirstPayDate).equals(other.getFirstPayDate()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& (InputOperator == null ? other.getInputOperator() == null : InputOperator.equals(other.getInputOperator()))
			&& (InputDate == null ? other.getInputDate() == null : fDate.getString(InputDate).equals(other.getInputDate()))
			&& (InputTime == null ? other.getInputTime() == null : InputTime.equals(other.getInputTime()))
			&& (ApproveFlag == null ? other.getApproveFlag() == null : ApproveFlag.equals(other.getApproveFlag()))
			&& (ApproveCode == null ? other.getApproveCode() == null : ApproveCode.equals(other.getApproveCode()))
			&& (ApproveDate == null ? other.getApproveDate() == null : fDate.getString(ApproveDate).equals(other.getApproveDate()))
			&& (ApproveTime == null ? other.getApproveTime() == null : ApproveTime.equals(other.getApproveTime()))
			&& (UWFlag == null ? other.getUWFlag() == null : UWFlag.equals(other.getUWFlag()))
			&& (UWOperator == null ? other.getUWOperator() == null : UWOperator.equals(other.getUWOperator()))
			&& (UWDate == null ? other.getUWDate() == null : fDate.getString(UWDate).equals(other.getUWDate()))
			&& (UWTime == null ? other.getUWTime() == null : UWTime.equals(other.getUWTime()))
			&& (AppFlag == null ? other.getAppFlag() == null : AppFlag.equals(other.getAppFlag()))
			&& (PolApplyDate == null ? other.getPolApplyDate() == null : fDate.getString(PolApplyDate).equals(other.getPolApplyDate()))
			&& (GetPolDate == null ? other.getGetPolDate() == null : fDate.getString(GetPolDate).equals(other.getGetPolDate()))
			&& (GetPolTime == null ? other.getGetPolTime() == null : GetPolTime.equals(other.getGetPolTime()))
			&& (CustomGetPolDate == null ? other.getCustomGetPolDate() == null : fDate.getString(CustomGetPolDate).equals(other.getCustomGetPolDate()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (FirstTrialOperator == null ? other.getFirstTrialOperator() == null : FirstTrialOperator.equals(other.getFirstTrialOperator()))
			&& (FirstTrialDate == null ? other.getFirstTrialDate() == null : fDate.getString(FirstTrialDate).equals(other.getFirstTrialDate()))
			&& (FirstTrialTime == null ? other.getFirstTrialTime() == null : FirstTrialTime.equals(other.getFirstTrialTime()))
			&& (ReceiveOperator == null ? other.getReceiveOperator() == null : ReceiveOperator.equals(other.getReceiveOperator()))
			&& (ReceiveDate == null ? other.getReceiveDate() == null : fDate.getString(ReceiveDate).equals(other.getReceiveDate()))
			&& (ReceiveTime == null ? other.getReceiveTime() == null : ReceiveTime.equals(other.getReceiveTime()))
			&& (TempFeeNo == null ? other.getTempFeeNo() == null : TempFeeNo.equals(other.getTempFeeNo()))
			&& (ProposalType == null ? other.getProposalType() == null : ProposalType.equals(other.getProposalType()))
			&& (SaleChnlDetail == null ? other.getSaleChnlDetail() == null : SaleChnlDetail.equals(other.getSaleChnlDetail()))
			&& (ContPrintLoFlag == null ? other.getContPrintLoFlag() == null : ContPrintLoFlag.equals(other.getContPrintLoFlag()))
			&& (ContPremFeeNo == null ? other.getContPremFeeNo() == null : ContPremFeeNo.equals(other.getContPremFeeNo()))
			&& (CustomerReceiptNo == null ? other.getCustomerReceiptNo() == null : CustomerReceiptNo.equals(other.getCustomerReceiptNo()))
			&& (CInValiDate == null ? other.getCInValiDate() == null : fDate.getString(CInValiDate).equals(other.getCInValiDate()))
			&& Copys == other.getCopys()
			&& (DegreeType == null ? other.getDegreeType() == null : DegreeType.equals(other.getDegreeType()))
			&& (HandlerDate == null ? other.getHandlerDate() == null : fDate.getString(HandlerDate).equals(other.getHandlerDate()))
			&& (HandlerPrint == null ? other.getHandlerPrint() == null : HandlerPrint.equals(other.getHandlerPrint()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& PremScope == other.getPremScope()
			&& (IntlFlag == null ? other.getIntlFlag() == null : IntlFlag.equals(other.getIntlFlag()))
			&& (UWConfirmNo == null ? other.getUWConfirmNo() == null : UWConfirmNo.equals(other.getUWConfirmNo()))
			&& (PayerType == null ? other.getPayerType() == null : PayerType.equals(other.getPayerType()))
			&& (GrpAgentCom == null ? other.getGrpAgentCom() == null : GrpAgentCom.equals(other.getGrpAgentCom()))
			&& (GrpAgentCode == null ? other.getGrpAgentCode() == null : GrpAgentCode.equals(other.getGrpAgentCode()))
			&& (GrpAgentName == null ? other.getGrpAgentName() == null : GrpAgentName.equals(other.getGrpAgentName()))
			&& (AccType == null ? other.getAccType() == null : AccType.equals(other.getAccType()))
			&& (Crs_SaleChnl == null ? other.getCrs_SaleChnl() == null : Crs_SaleChnl.equals(other.getCrs_SaleChnl()))
			&& (Crs_BussType == null ? other.getCrs_BussType() == null : Crs_BussType.equals(other.getCrs_BussType()))
			&& (GrpAgentIDNo == null ? other.getGrpAgentIDNo() == null : GrpAgentIDNo.equals(other.getGrpAgentIDNo()))
			&& (DueFeeMsgFlag == null ? other.getDueFeeMsgFlag() == null : DueFeeMsgFlag.equals(other.getDueFeeMsgFlag()))
			&& (PayMethod == null ? other.getPayMethod() == null : PayMethod.equals(other.getPayMethod()))
			&& (ExiSpec == null ? other.getExiSpec() == null : ExiSpec.equals(other.getExiSpec()))
			&& (ExPayMode == null ? other.getExPayMode() == null : ExPayMode.equals(other.getExPayMode()))
			&& (AgentSaleCode == null ? other.getAgentSaleCode() == null : AgentSaleCode.equals(other.getAgentSaleCode()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("EdorType") ) {
			return 1;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 2;
		}
		if( strFieldName.equals("ContNo") ) {
			return 3;
		}
		if( strFieldName.equals("ProposalContNo") ) {
			return 4;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 5;
		}
		if( strFieldName.equals("ContType") ) {
			return 6;
		}
		if( strFieldName.equals("FamilyType") ) {
			return 7;
		}
		if( strFieldName.equals("FamilyID") ) {
			return 8;
		}
		if( strFieldName.equals("PolType") ) {
			return 9;
		}
		if( strFieldName.equals("CardFlag") ) {
			return 10;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 11;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return 12;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 13;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 14;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 15;
		}
		if( strFieldName.equals("AgentCode1") ) {
			return 16;
		}
		if( strFieldName.equals("AgentType") ) {
			return 17;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 18;
		}
		if( strFieldName.equals("Handler") ) {
			return 19;
		}
		if( strFieldName.equals("Password") ) {
			return 20;
		}
		if( strFieldName.equals("AppntNo") ) {
			return 21;
		}
		if( strFieldName.equals("AppntName") ) {
			return 22;
		}
		if( strFieldName.equals("AppntSex") ) {
			return 23;
		}
		if( strFieldName.equals("AppntBirthday") ) {
			return 24;
		}
		if( strFieldName.equals("AppntIDType") ) {
			return 25;
		}
		if( strFieldName.equals("AppntIDNo") ) {
			return 26;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return 27;
		}
		if( strFieldName.equals("InsuredName") ) {
			return 28;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return 29;
		}
		if( strFieldName.equals("InsuredBirthday") ) {
			return 30;
		}
		if( strFieldName.equals("InsuredIDType") ) {
			return 31;
		}
		if( strFieldName.equals("InsuredIDNo") ) {
			return 32;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 33;
		}
		if( strFieldName.equals("PayMode") ) {
			return 34;
		}
		if( strFieldName.equals("PayLocation") ) {
			return 35;
		}
		if( strFieldName.equals("DisputedFlag") ) {
			return 36;
		}
		if( strFieldName.equals("OutPayFlag") ) {
			return 37;
		}
		if( strFieldName.equals("GetPolMode") ) {
			return 38;
		}
		if( strFieldName.equals("SignCom") ) {
			return 39;
		}
		if( strFieldName.equals("SignDate") ) {
			return 40;
		}
		if( strFieldName.equals("SignTime") ) {
			return 41;
		}
		if( strFieldName.equals("ConsignNo") ) {
			return 42;
		}
		if( strFieldName.equals("BankCode") ) {
			return 43;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 44;
		}
		if( strFieldName.equals("AccName") ) {
			return 45;
		}
		if( strFieldName.equals("PrintCount") ) {
			return 46;
		}
		if( strFieldName.equals("LostTimes") ) {
			return 47;
		}
		if( strFieldName.equals("Lang") ) {
			return 48;
		}
		if( strFieldName.equals("Currency") ) {
			return 49;
		}
		if( strFieldName.equals("Remark") ) {
			return 50;
		}
		if( strFieldName.equals("Peoples") ) {
			return 51;
		}
		if( strFieldName.equals("Mult") ) {
			return 52;
		}
		if( strFieldName.equals("Prem") ) {
			return 53;
		}
		if( strFieldName.equals("Amnt") ) {
			return 54;
		}
		if( strFieldName.equals("SumPrem") ) {
			return 55;
		}
		if( strFieldName.equals("Dif") ) {
			return 56;
		}
		if( strFieldName.equals("PaytoDate") ) {
			return 57;
		}
		if( strFieldName.equals("FirstPayDate") ) {
			return 58;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 59;
		}
		if( strFieldName.equals("InputOperator") ) {
			return 60;
		}
		if( strFieldName.equals("InputDate") ) {
			return 61;
		}
		if( strFieldName.equals("InputTime") ) {
			return 62;
		}
		if( strFieldName.equals("ApproveFlag") ) {
			return 63;
		}
		if( strFieldName.equals("ApproveCode") ) {
			return 64;
		}
		if( strFieldName.equals("ApproveDate") ) {
			return 65;
		}
		if( strFieldName.equals("ApproveTime") ) {
			return 66;
		}
		if( strFieldName.equals("UWFlag") ) {
			return 67;
		}
		if( strFieldName.equals("UWOperator") ) {
			return 68;
		}
		if( strFieldName.equals("UWDate") ) {
			return 69;
		}
		if( strFieldName.equals("UWTime") ) {
			return 70;
		}
		if( strFieldName.equals("AppFlag") ) {
			return 71;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return 72;
		}
		if( strFieldName.equals("GetPolDate") ) {
			return 73;
		}
		if( strFieldName.equals("GetPolTime") ) {
			return 74;
		}
		if( strFieldName.equals("CustomGetPolDate") ) {
			return 75;
		}
		if( strFieldName.equals("State") ) {
			return 76;
		}
		if( strFieldName.equals("Operator") ) {
			return 77;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 78;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 79;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 80;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 81;
		}
		if( strFieldName.equals("FirstTrialOperator") ) {
			return 82;
		}
		if( strFieldName.equals("FirstTrialDate") ) {
			return 83;
		}
		if( strFieldName.equals("FirstTrialTime") ) {
			return 84;
		}
		if( strFieldName.equals("ReceiveOperator") ) {
			return 85;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return 86;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return 87;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return 88;
		}
		if( strFieldName.equals("ProposalType") ) {
			return 89;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return 90;
		}
		if( strFieldName.equals("ContPrintLoFlag") ) {
			return 91;
		}
		if( strFieldName.equals("ContPremFeeNo") ) {
			return 92;
		}
		if( strFieldName.equals("CustomerReceiptNo") ) {
			return 93;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return 94;
		}
		if( strFieldName.equals("Copys") ) {
			return 95;
		}
		if( strFieldName.equals("DegreeType") ) {
			return 96;
		}
		if( strFieldName.equals("HandlerDate") ) {
			return 97;
		}
		if( strFieldName.equals("HandlerPrint") ) {
			return 98;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 99;
		}
		if( strFieldName.equals("PremScope") ) {
			return 100;
		}
		if( strFieldName.equals("IntlFlag") ) {
			return 101;
		}
		if( strFieldName.equals("UWConfirmNo") ) {
			return 102;
		}
		if( strFieldName.equals("PayerType") ) {
			return 103;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return 104;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return 105;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return 106;
		}
		if( strFieldName.equals("AccType") ) {
			return 107;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return 108;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return 109;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return 110;
		}
		if( strFieldName.equals("DueFeeMsgFlag") ) {
			return 111;
		}
		if( strFieldName.equals("PayMethod") ) {
			return 112;
		}
		if( strFieldName.equals("ExiSpec") ) {
			return 113;
		}
		if( strFieldName.equals("ExPayMode") ) {
			return 114;
		}
		if( strFieldName.equals("AgentSaleCode") ) {
			return 115;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "EdorType";
				break;
			case 2:
				strFieldName = "GrpContNo";
				break;
			case 3:
				strFieldName = "ContNo";
				break;
			case 4:
				strFieldName = "ProposalContNo";
				break;
			case 5:
				strFieldName = "PrtNo";
				break;
			case 6:
				strFieldName = "ContType";
				break;
			case 7:
				strFieldName = "FamilyType";
				break;
			case 8:
				strFieldName = "FamilyID";
				break;
			case 9:
				strFieldName = "PolType";
				break;
			case 10:
				strFieldName = "CardFlag";
				break;
			case 11:
				strFieldName = "ManageCom";
				break;
			case 12:
				strFieldName = "ExecuteCom";
				break;
			case 13:
				strFieldName = "AgentCom";
				break;
			case 14:
				strFieldName = "AgentCode";
				break;
			case 15:
				strFieldName = "AgentGroup";
				break;
			case 16:
				strFieldName = "AgentCode1";
				break;
			case 17:
				strFieldName = "AgentType";
				break;
			case 18:
				strFieldName = "SaleChnl";
				break;
			case 19:
				strFieldName = "Handler";
				break;
			case 20:
				strFieldName = "Password";
				break;
			case 21:
				strFieldName = "AppntNo";
				break;
			case 22:
				strFieldName = "AppntName";
				break;
			case 23:
				strFieldName = "AppntSex";
				break;
			case 24:
				strFieldName = "AppntBirthday";
				break;
			case 25:
				strFieldName = "AppntIDType";
				break;
			case 26:
				strFieldName = "AppntIDNo";
				break;
			case 27:
				strFieldName = "InsuredNo";
				break;
			case 28:
				strFieldName = "InsuredName";
				break;
			case 29:
				strFieldName = "InsuredSex";
				break;
			case 30:
				strFieldName = "InsuredBirthday";
				break;
			case 31:
				strFieldName = "InsuredIDType";
				break;
			case 32:
				strFieldName = "InsuredIDNo";
				break;
			case 33:
				strFieldName = "PayIntv";
				break;
			case 34:
				strFieldName = "PayMode";
				break;
			case 35:
				strFieldName = "PayLocation";
				break;
			case 36:
				strFieldName = "DisputedFlag";
				break;
			case 37:
				strFieldName = "OutPayFlag";
				break;
			case 38:
				strFieldName = "GetPolMode";
				break;
			case 39:
				strFieldName = "SignCom";
				break;
			case 40:
				strFieldName = "SignDate";
				break;
			case 41:
				strFieldName = "SignTime";
				break;
			case 42:
				strFieldName = "ConsignNo";
				break;
			case 43:
				strFieldName = "BankCode";
				break;
			case 44:
				strFieldName = "BankAccNo";
				break;
			case 45:
				strFieldName = "AccName";
				break;
			case 46:
				strFieldName = "PrintCount";
				break;
			case 47:
				strFieldName = "LostTimes";
				break;
			case 48:
				strFieldName = "Lang";
				break;
			case 49:
				strFieldName = "Currency";
				break;
			case 50:
				strFieldName = "Remark";
				break;
			case 51:
				strFieldName = "Peoples";
				break;
			case 52:
				strFieldName = "Mult";
				break;
			case 53:
				strFieldName = "Prem";
				break;
			case 54:
				strFieldName = "Amnt";
				break;
			case 55:
				strFieldName = "SumPrem";
				break;
			case 56:
				strFieldName = "Dif";
				break;
			case 57:
				strFieldName = "PaytoDate";
				break;
			case 58:
				strFieldName = "FirstPayDate";
				break;
			case 59:
				strFieldName = "CValiDate";
				break;
			case 60:
				strFieldName = "InputOperator";
				break;
			case 61:
				strFieldName = "InputDate";
				break;
			case 62:
				strFieldName = "InputTime";
				break;
			case 63:
				strFieldName = "ApproveFlag";
				break;
			case 64:
				strFieldName = "ApproveCode";
				break;
			case 65:
				strFieldName = "ApproveDate";
				break;
			case 66:
				strFieldName = "ApproveTime";
				break;
			case 67:
				strFieldName = "UWFlag";
				break;
			case 68:
				strFieldName = "UWOperator";
				break;
			case 69:
				strFieldName = "UWDate";
				break;
			case 70:
				strFieldName = "UWTime";
				break;
			case 71:
				strFieldName = "AppFlag";
				break;
			case 72:
				strFieldName = "PolApplyDate";
				break;
			case 73:
				strFieldName = "GetPolDate";
				break;
			case 74:
				strFieldName = "GetPolTime";
				break;
			case 75:
				strFieldName = "CustomGetPolDate";
				break;
			case 76:
				strFieldName = "State";
				break;
			case 77:
				strFieldName = "Operator";
				break;
			case 78:
				strFieldName = "MakeDate";
				break;
			case 79:
				strFieldName = "MakeTime";
				break;
			case 80:
				strFieldName = "ModifyDate";
				break;
			case 81:
				strFieldName = "ModifyTime";
				break;
			case 82:
				strFieldName = "FirstTrialOperator";
				break;
			case 83:
				strFieldName = "FirstTrialDate";
				break;
			case 84:
				strFieldName = "FirstTrialTime";
				break;
			case 85:
				strFieldName = "ReceiveOperator";
				break;
			case 86:
				strFieldName = "ReceiveDate";
				break;
			case 87:
				strFieldName = "ReceiveTime";
				break;
			case 88:
				strFieldName = "TempFeeNo";
				break;
			case 89:
				strFieldName = "ProposalType";
				break;
			case 90:
				strFieldName = "SaleChnlDetail";
				break;
			case 91:
				strFieldName = "ContPrintLoFlag";
				break;
			case 92:
				strFieldName = "ContPremFeeNo";
				break;
			case 93:
				strFieldName = "CustomerReceiptNo";
				break;
			case 94:
				strFieldName = "CInValiDate";
				break;
			case 95:
				strFieldName = "Copys";
				break;
			case 96:
				strFieldName = "DegreeType";
				break;
			case 97:
				strFieldName = "HandlerDate";
				break;
			case 98:
				strFieldName = "HandlerPrint";
				break;
			case 99:
				strFieldName = "StateFlag";
				break;
			case 100:
				strFieldName = "PremScope";
				break;
			case 101:
				strFieldName = "IntlFlag";
				break;
			case 102:
				strFieldName = "UWConfirmNo";
				break;
			case 103:
				strFieldName = "PayerType";
				break;
			case 104:
				strFieldName = "GrpAgentCom";
				break;
			case 105:
				strFieldName = "GrpAgentCode";
				break;
			case 106:
				strFieldName = "GrpAgentName";
				break;
			case 107:
				strFieldName = "AccType";
				break;
			case 108:
				strFieldName = "Crs_SaleChnl";
				break;
			case 109:
				strFieldName = "Crs_BussType";
				break;
			case 110:
				strFieldName = "GrpAgentIDNo";
				break;
			case 111:
				strFieldName = "DueFeeMsgFlag";
				break;
			case 112:
				strFieldName = "PayMethod";
				break;
			case 113:
				strFieldName = "ExiSpec";
				break;
			case 114:
				strFieldName = "ExPayMode";
				break;
			case 115:
				strFieldName = "AgentSaleCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FamilyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FamilyID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntBirthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AppntIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredBirthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InsuredIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayLocation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DisputedFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutPayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetPolMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SignTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ConsignNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrintCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("LostTimes") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Lang") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Peoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Mult") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Dif") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PaytoDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FirstPayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InputOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InputTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApproveFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApproveCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApproveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ApproveTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("UWTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GetPolDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GetPolTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomGetPolDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTrialOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTrialDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FirstTrialTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPrintLoFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPremFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Copys") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DegreeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HandlerPrint") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremScope") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IntlFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWConfirmNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayerType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DueFeeMsgFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMethod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExiSpec") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExPayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentSaleCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_INT;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_INT;
				break;
			case 47:
				nFieldType = Schema.TYPE_INT;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_INT;
				break;
			case 52:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 53:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 54:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 55:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 56:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 57:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 58:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 59:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 69:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 70:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 71:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 72:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 73:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 74:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 75:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 76:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 77:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 78:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 79:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 80:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 81:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 82:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 83:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 84:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 85:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 86:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 87:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 88:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 89:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 90:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 91:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 92:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 93:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 94:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 95:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 96:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 97:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 98:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 99:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 100:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 101:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 102:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 103:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 104:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 105:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 106:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 107:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 108:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 109:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 110:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 111:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 112:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 113:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 114:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 115:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
