/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHCustomTestDB;

/*
 * <p>ClassName: LHCustomTestSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-23
 */
public class LHCustomTestSchema implements Schema, Cloneable {
    // @Field
    /** 客户号码 */
    private String CustomerNo;
    /** 就诊及住院序号 */
    private String InHospitNo;
    /** 检查序号 */
    private String TestNo;
    /** 医疗服务项目代码 */
    private String MedicaItemCode;
    /** 结果单位数值 */
    private String ResultUnitNum;
    /** 检查结果 */
    private String TestResult;
    /** 检查日期 */
    private Date TestDate;
    /** 医师代码 */
    private String DoctNo;
    /** 检查费用 */
    private double TestFeeAmount;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 所选套餐 */
    private String TestGrpUsed;
    /** 是否异常 */
    private String IsNormal;
    /** 管理机构 */
    private String ManageCom;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHCustomTestSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "CustomerNo";
        pk[1] = "InHospitNo";
        pk[2] = "TestNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHCustomTestSchema cloned = (LHCustomTestSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getInHospitNo() {
        return InHospitNo;
    }

    public void setInHospitNo(String aInHospitNo) {
        InHospitNo = aInHospitNo;
    }

    public String getTestNo() {
        return TestNo;
    }

    public void setTestNo(String aTestNo) {
        TestNo = aTestNo;
    }

    public String getMedicaItemCode() {
        return MedicaItemCode;
    }

    public void setMedicaItemCode(String aMedicaItemCode) {
        MedicaItemCode = aMedicaItemCode;
    }

    public String getResultUnitNum() {
        return ResultUnitNum;
    }

    public void setResultUnitNum(String aResultUnitNum) {
        ResultUnitNum = aResultUnitNum;
    }

    public String getTestResult() {
        return TestResult;
    }

    public void setTestResult(String aTestResult) {
        TestResult = aTestResult;
    }

    public String getTestDate() {
        if (TestDate != null) {
            return fDate.getString(TestDate);
        } else {
            return null;
        }
    }

    public void setTestDate(Date aTestDate) {
        TestDate = aTestDate;
    }

    public void setTestDate(String aTestDate) {
        if (aTestDate != null && !aTestDate.equals("")) {
            TestDate = fDate.getDate(aTestDate);
        } else {
            TestDate = null;
        }
    }

    public String getDoctNo() {
        return DoctNo;
    }

    public void setDoctNo(String aDoctNo) {
        DoctNo = aDoctNo;
    }

    public double getTestFeeAmount() {
        return TestFeeAmount;
    }

    public void setTestFeeAmount(double aTestFeeAmount) {
        TestFeeAmount = Arith.round(aTestFeeAmount, 2);
    }

    public void setTestFeeAmount(String aTestFeeAmount) {
        if (aTestFeeAmount != null && !aTestFeeAmount.equals("")) {
            Double tDouble = new Double(aTestFeeAmount);
            double d = tDouble.doubleValue();
            TestFeeAmount = Arith.round(d, 2);
        }
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getTestGrpUsed() {
        return TestGrpUsed;
    }

    public void setTestGrpUsed(String aTestGrpUsed) {
        TestGrpUsed = aTestGrpUsed;
    }

    public String getIsNormal() {
        return IsNormal;
    }

    public void setIsNormal(String aIsNormal) {
        IsNormal = aIsNormal;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    /**
     * 使用另外一个 LHCustomTestSchema 对象给 Schema 赋值
     * @param: aLHCustomTestSchema LHCustomTestSchema
     **/
    public void setSchema(LHCustomTestSchema aLHCustomTestSchema) {
        this.CustomerNo = aLHCustomTestSchema.getCustomerNo();
        this.InHospitNo = aLHCustomTestSchema.getInHospitNo();
        this.TestNo = aLHCustomTestSchema.getTestNo();
        this.MedicaItemCode = aLHCustomTestSchema.getMedicaItemCode();
        this.ResultUnitNum = aLHCustomTestSchema.getResultUnitNum();
        this.TestResult = aLHCustomTestSchema.getTestResult();
        this.TestDate = fDate.getDate(aLHCustomTestSchema.getTestDate());
        this.DoctNo = aLHCustomTestSchema.getDoctNo();
        this.TestFeeAmount = aLHCustomTestSchema.getTestFeeAmount();
        this.Operator = aLHCustomTestSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHCustomTestSchema.getMakeDate());
        this.MakeTime = aLHCustomTestSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHCustomTestSchema.getModifyDate());
        this.ModifyTime = aLHCustomTestSchema.getModifyTime();
        this.TestGrpUsed = aLHCustomTestSchema.getTestGrpUsed();
        this.IsNormal = aLHCustomTestSchema.getIsNormal();
        this.ManageCom = aLHCustomTestSchema.getManageCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("InHospitNo") == null) {
                this.InHospitNo = null;
            } else {
                this.InHospitNo = rs.getString("InHospitNo").trim();
            }

            if (rs.getString("TestNo") == null) {
                this.TestNo = null;
            } else {
                this.TestNo = rs.getString("TestNo").trim();
            }

            if (rs.getString("MedicaItemCode") == null) {
                this.MedicaItemCode = null;
            } else {
                this.MedicaItemCode = rs.getString("MedicaItemCode").trim();
            }

            if (rs.getString("ResultUnitNum") == null) {
                this.ResultUnitNum = null;
            } else {
                this.ResultUnitNum = rs.getString("ResultUnitNum").trim();
            }

            if (rs.getString("TestResult") == null) {
                this.TestResult = null;
            } else {
                this.TestResult = rs.getString("TestResult").trim();
            }

            this.TestDate = rs.getDate("TestDate");
            if (rs.getString("DoctNo") == null) {
                this.DoctNo = null;
            } else {
                this.DoctNo = rs.getString("DoctNo").trim();
            }

            this.TestFeeAmount = rs.getDouble("TestFeeAmount");
            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("TestGrpUsed") == null) {
                this.TestGrpUsed = null;
            } else {
                this.TestGrpUsed = rs.getString("TestGrpUsed").trim();
            }

            if (rs.getString("IsNormal") == null) {
                this.IsNormal = null;
            } else {
                this.IsNormal = rs.getString("IsNormal").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHCustomTest表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCustomTestSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHCustomTestSchema getSchema() {
        LHCustomTestSchema aLHCustomTestSchema = new LHCustomTestSchema();
        aLHCustomTestSchema.setSchema(this);
        return aLHCustomTestSchema;
    }

    public LHCustomTestDB getDB() {
        LHCustomTestDB aDBOper = new LHCustomTestDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCustomTest描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InHospitNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MedicaItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ResultUnitNum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestResult));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(TestDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DoctNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TestFeeAmount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestGrpUsed));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsNormal));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCustomTest>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            InHospitNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            TestNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            MedicaItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
            ResultUnitNum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            TestResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            TestDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            DoctNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            TestFeeAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            TestGrpUsed = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            IsNormal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCustomTestSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("InHospitNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InHospitNo));
        }
        if (FCode.equals("TestNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestNo));
        }
        if (FCode.equals("MedicaItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemCode));
        }
        if (FCode.equals("ResultUnitNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultUnitNum));
        }
        if (FCode.equals("TestResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestResult));
        }
        if (FCode.equals("TestDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getTestDate()));
        }
        if (FCode.equals("DoctNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoctNo));
        }
        if (FCode.equals("TestFeeAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestFeeAmount));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("TestGrpUsed")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestGrpUsed));
        }
        if (FCode.equals("IsNormal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsNormal));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(InHospitNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(TestNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(MedicaItemCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ResultUnitNum);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(TestResult);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getTestDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(DoctNo);
            break;
        case 8:
            strFieldValue = String.valueOf(TestFeeAmount);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(TestGrpUsed);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(IsNormal);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("InHospitNo")) {
            if (FValue != null && !FValue.equals("")) {
                InHospitNo = FValue.trim();
            } else {
                InHospitNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestNo")) {
            if (FValue != null && !FValue.equals("")) {
                TestNo = FValue.trim();
            } else {
                TestNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("MedicaItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                MedicaItemCode = FValue.trim();
            } else {
                MedicaItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ResultUnitNum")) {
            if (FValue != null && !FValue.equals("")) {
                ResultUnitNum = FValue.trim();
            } else {
                ResultUnitNum = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestResult")) {
            if (FValue != null && !FValue.equals("")) {
                TestResult = FValue.trim();
            } else {
                TestResult = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestDate")) {
            if (FValue != null && !FValue.equals("")) {
                TestDate = fDate.getDate(FValue);
            } else {
                TestDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("DoctNo")) {
            if (FValue != null && !FValue.equals("")) {
                DoctNo = FValue.trim();
            } else {
                DoctNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestFeeAmount")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TestFeeAmount = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestGrpUsed")) {
            if (FValue != null && !FValue.equals("")) {
                TestGrpUsed = FValue.trim();
            } else {
                TestGrpUsed = null;
            }
        }
        if (FCode.equalsIgnoreCase("IsNormal")) {
            if (FValue != null && !FValue.equals("")) {
                IsNormal = FValue.trim();
            } else {
                IsNormal = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHCustomTestSchema other = (LHCustomTestSchema) otherObject;
        return
                CustomerNo.equals(other.getCustomerNo())
                && InHospitNo.equals(other.getInHospitNo())
                && TestNo.equals(other.getTestNo())
                && MedicaItemCode.equals(other.getMedicaItemCode())
                && ResultUnitNum.equals(other.getResultUnitNum())
                && TestResult.equals(other.getTestResult())
                && fDate.getString(TestDate).equals(other.getTestDate())
                && DoctNo.equals(other.getDoctNo())
                && TestFeeAmount == other.getTestFeeAmount()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && TestGrpUsed.equals(other.getTestGrpUsed())
                && IsNormal.equals(other.getIsNormal())
                && ManageCom.equals(other.getManageCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("CustomerNo")) {
            return 0;
        }
        if (strFieldName.equals("InHospitNo")) {
            return 1;
        }
        if (strFieldName.equals("TestNo")) {
            return 2;
        }
        if (strFieldName.equals("MedicaItemCode")) {
            return 3;
        }
        if (strFieldName.equals("ResultUnitNum")) {
            return 4;
        }
        if (strFieldName.equals("TestResult")) {
            return 5;
        }
        if (strFieldName.equals("TestDate")) {
            return 6;
        }
        if (strFieldName.equals("DoctNo")) {
            return 7;
        }
        if (strFieldName.equals("TestFeeAmount")) {
            return 8;
        }
        if (strFieldName.equals("Operator")) {
            return 9;
        }
        if (strFieldName.equals("MakeDate")) {
            return 10;
        }
        if (strFieldName.equals("MakeTime")) {
            return 11;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 12;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 13;
        }
        if (strFieldName.equals("TestGrpUsed")) {
            return 14;
        }
        if (strFieldName.equals("IsNormal")) {
            return 15;
        }
        if (strFieldName.equals("ManageCom")) {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "CustomerNo";
            break;
        case 1:
            strFieldName = "InHospitNo";
            break;
        case 2:
            strFieldName = "TestNo";
            break;
        case 3:
            strFieldName = "MedicaItemCode";
            break;
        case 4:
            strFieldName = "ResultUnitNum";
            break;
        case 5:
            strFieldName = "TestResult";
            break;
        case 6:
            strFieldName = "TestDate";
            break;
        case 7:
            strFieldName = "DoctNo";
            break;
        case 8:
            strFieldName = "TestFeeAmount";
            break;
        case 9:
            strFieldName = "Operator";
            break;
        case 10:
            strFieldName = "MakeDate";
            break;
        case 11:
            strFieldName = "MakeTime";
            break;
        case 12:
            strFieldName = "ModifyDate";
            break;
        case 13:
            strFieldName = "ModifyTime";
            break;
        case 14:
            strFieldName = "TestGrpUsed";
            break;
        case 15:
            strFieldName = "IsNormal";
            break;
        case 16:
            strFieldName = "ManageCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InHospitNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MedicaItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ResultUnitNum")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestResult")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("DoctNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestFeeAmount")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestGrpUsed")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IsNormal")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
