/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDICDDiseasTypeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDICDDiseasTypeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_3
 * @CreateDate：2005-03-03
 */
public class LDICDDiseasTypeSchema implements Schema
{
    // @Field
    /** 分类等级 */
    private String DiseasTypeLevel;
    /** 起始icd号 */
    private String ICDBeginCode;
    /** 终止icd号 */
    private String ICDEndCode;
    /** 疾病分类名称 */
    private String DiseasType;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDICDDiseasTypeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "DiseasTypeLevel";
        pk[1] = "ICDBeginCode";
        pk[2] = "ICDEndCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getDiseasTypeLevel()
    {
        if (DiseasTypeLevel != null && !DiseasTypeLevel.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DiseasTypeLevel = StrTool.unicodeToGBK(DiseasTypeLevel);
        }
        return DiseasTypeLevel;
    }

    public void setDiseasTypeLevel(String aDiseasTypeLevel)
    {
        DiseasTypeLevel = aDiseasTypeLevel;
    }

    public String getICDBeginCode()
    {
        if (ICDBeginCode != null && !ICDBeginCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ICDBeginCode = StrTool.unicodeToGBK(ICDBeginCode);
        }
        return ICDBeginCode;
    }

    public void setICDBeginCode(String aICDBeginCode)
    {
        ICDBeginCode = aICDBeginCode;
    }

    public String getICDEndCode()
    {
        if (ICDEndCode != null && !ICDEndCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ICDEndCode = StrTool.unicodeToGBK(ICDEndCode);
        }
        return ICDEndCode;
    }

    public void setICDEndCode(String aICDEndCode)
    {
        ICDEndCode = aICDEndCode;
    }

    public String getDiseasType()
    {
        if (DiseasType != null && !DiseasType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DiseasType = StrTool.unicodeToGBK(DiseasType);
        }
        return DiseasType;
    }

    public void setDiseasType(String aDiseasType)
    {
        DiseasType = aDiseasType;
    }

    /**
     * 使用另外一个 LDICDDiseasTypeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDICDDiseasTypeSchema aLDICDDiseasTypeSchema)
    {
        this.DiseasTypeLevel = aLDICDDiseasTypeSchema.getDiseasTypeLevel();
        this.ICDBeginCode = aLDICDDiseasTypeSchema.getICDBeginCode();
        this.ICDEndCode = aLDICDDiseasTypeSchema.getICDEndCode();
        this.DiseasType = aLDICDDiseasTypeSchema.getDiseasType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("DiseasTypeLevel") == null)
            {
                this.DiseasTypeLevel = null;
            }
            else
            {
                this.DiseasTypeLevel = rs.getString("DiseasTypeLevel").trim();
            }

            if (rs.getString("ICDBeginCode") == null)
            {
                this.ICDBeginCode = null;
            }
            else
            {
                this.ICDBeginCode = rs.getString("ICDBeginCode").trim();
            }

            if (rs.getString("ICDEndCode") == null)
            {
                this.ICDEndCode = null;
            }
            else
            {
                this.ICDEndCode = rs.getString("ICDEndCode").trim();
            }

            if (rs.getString("DiseasType") == null)
            {
                this.DiseasType = null;
            }
            else
            {
                this.DiseasType = rs.getString("DiseasType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDICDDiseasTypeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDICDDiseasTypeSchema getSchema()
    {
        LDICDDiseasTypeSchema aLDICDDiseasTypeSchema = new
                LDICDDiseasTypeSchema();
        aLDICDDiseasTypeSchema.setSchema(this);
        return aLDICDDiseasTypeSchema;
    }

    public LDICDDiseasTypeDB getDB()
    {
        LDICDDiseasTypeDB aDBOper = new LDICDDiseasTypeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDICDDiseasType描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(DiseasTypeLevel)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ICDBeginCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ICDEndCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DiseasType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDICDDiseasType>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            DiseasTypeLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             1, SysConst.PACKAGESPILTER);
            ICDBeginCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ICDEndCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            DiseasType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDICDDiseasTypeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("DiseasTypeLevel"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DiseasTypeLevel));
        }
        if (FCode.equals("ICDBeginCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ICDBeginCode));
        }
        if (FCode.equals("ICDEndCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ICDEndCode));
        }
        if (FCode.equals("DiseasType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DiseasType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(DiseasTypeLevel);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ICDBeginCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ICDEndCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DiseasType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("DiseasTypeLevel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DiseasTypeLevel = FValue.trim();
            }
            else
            {
                DiseasTypeLevel = null;
            }
        }
        if (FCode.equals("ICDBeginCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ICDBeginCode = FValue.trim();
            }
            else
            {
                ICDBeginCode = null;
            }
        }
        if (FCode.equals("ICDEndCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ICDEndCode = FValue.trim();
            }
            else
            {
                ICDEndCode = null;
            }
        }
        if (FCode.equals("DiseasType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DiseasType = FValue.trim();
            }
            else
            {
                DiseasType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDICDDiseasTypeSchema other = (LDICDDiseasTypeSchema) otherObject;
        return
                DiseasTypeLevel.equals(other.getDiseasTypeLevel())
                && ICDBeginCode.equals(other.getICDBeginCode())
                && ICDEndCode.equals(other.getICDEndCode())
                && DiseasType.equals(other.getDiseasType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("DiseasTypeLevel"))
        {
            return 0;
        }
        if (strFieldName.equals("ICDBeginCode"))
        {
            return 1;
        }
        if (strFieldName.equals("ICDEndCode"))
        {
            return 2;
        }
        if (strFieldName.equals("DiseasType"))
        {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "DiseasTypeLevel";
                break;
            case 1:
                strFieldName = "ICDBeginCode";
                break;
            case 2:
                strFieldName = "ICDEndCode";
                break;
            case 3:
                strFieldName = "DiseasType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("DiseasTypeLevel"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ICDBeginCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ICDEndCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DiseasType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
