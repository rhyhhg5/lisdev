/*
 * <p>ClassName: LOBCSpecSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 核心表变更
 * @CreateDate：2004-12-06
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBCSpecDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOBCSpecSchema implements Schema
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 打印流水号 */
    private String PrtSeq;
    /** 流水号 */
    private String SerialNo;
    /** 批单号码 */
    private String EndorsementNo;
    /** 特约类型 */
    private String SpecType;
    /** 特约编码 */
    private String SpecCode;
    /** 特约内容 */
    private String SpecContent;
    /** 打印标记 */
    private String PrtFlag;
    /** 备份类型 */
    private String BackupType;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBCSpecSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ContNo";
        pk[1] = "SerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getProposalContNo()
    {
        if (ProposalContNo != null && !ProposalContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProposalContNo = StrTool.unicodeToGBK(ProposalContNo);
        }
        return ProposalContNo;
    }

    public void setProposalContNo(String aProposalContNo)
    {
        ProposalContNo = aProposalContNo;
    }

    public String getPrtSeq()
    {
        if (PrtSeq != null && !PrtSeq.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtSeq = StrTool.unicodeToGBK(PrtSeq);
        }
        return PrtSeq;
    }

    public void setPrtSeq(String aPrtSeq)
    {
        PrtSeq = aPrtSeq;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getEndorsementNo()
    {
        if (EndorsementNo != null && !EndorsementNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EndorsementNo = StrTool.unicodeToGBK(EndorsementNo);
        }
        return EndorsementNo;
    }

    public void setEndorsementNo(String aEndorsementNo)
    {
        EndorsementNo = aEndorsementNo;
    }

    public String getSpecType()
    {
        if (SpecType != null && !SpecType.equals("") && SysConst.CHANGECHARSET == true)
        {
            SpecType = StrTool.unicodeToGBK(SpecType);
        }
        return SpecType;
    }

    public void setSpecType(String aSpecType)
    {
        SpecType = aSpecType;
    }

    public String getSpecCode()
    {
        if (SpecCode != null && !SpecCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            SpecCode = StrTool.unicodeToGBK(SpecCode);
        }
        return SpecCode;
    }

    public void setSpecCode(String aSpecCode)
    {
        SpecCode = aSpecCode;
    }

    public String getSpecContent()
    {
        if (SpecContent != null && !SpecContent.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SpecContent = StrTool.unicodeToGBK(SpecContent);
        }
        return SpecContent;
    }

    public void setSpecContent(String aSpecContent)
    {
        SpecContent = aSpecContent;
    }

    public String getPrtFlag()
    {
        if (PrtFlag != null && !PrtFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtFlag = StrTool.unicodeToGBK(PrtFlag);
        }
        return PrtFlag;
    }

    public void setPrtFlag(String aPrtFlag)
    {
        PrtFlag = aPrtFlag;
    }

    public String getBackupType()
    {
        if (BackupType != null && !BackupType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BackupType = StrTool.unicodeToGBK(BackupType);
        }
        return BackupType;
    }

    public void setBackupType(String aBackupType)
    {
        BackupType = aBackupType;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOBCSpecSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOBCSpecSchema aLOBCSpecSchema)
    {
        this.GrpContNo = aLOBCSpecSchema.getGrpContNo();
        this.ContNo = aLOBCSpecSchema.getContNo();
        this.ProposalContNo = aLOBCSpecSchema.getProposalContNo();
        this.PrtSeq = aLOBCSpecSchema.getPrtSeq();
        this.SerialNo = aLOBCSpecSchema.getSerialNo();
        this.EndorsementNo = aLOBCSpecSchema.getEndorsementNo();
        this.SpecType = aLOBCSpecSchema.getSpecType();
        this.SpecCode = aLOBCSpecSchema.getSpecCode();
        this.SpecContent = aLOBCSpecSchema.getSpecContent();
        this.PrtFlag = aLOBCSpecSchema.getPrtFlag();
        this.BackupType = aLOBCSpecSchema.getBackupType();
        this.Operator = aLOBCSpecSchema.getOperator();
        this.MakeDate = fDate.getDate(aLOBCSpecSchema.getMakeDate());
        this.MakeTime = aLOBCSpecSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBCSpecSchema.getModifyDate());
        this.ModifyTime = aLOBCSpecSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ProposalContNo") == null)
            {
                this.ProposalContNo = null;
            }
            else
            {
                this.ProposalContNo = rs.getString("ProposalContNo").trim();
            }

            if (rs.getString("PrtSeq") == null)
            {
                this.PrtSeq = null;
            }
            else
            {
                this.PrtSeq = rs.getString("PrtSeq").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("EndorsementNo") == null)
            {
                this.EndorsementNo = null;
            }
            else
            {
                this.EndorsementNo = rs.getString("EndorsementNo").trim();
            }

            if (rs.getString("SpecType") == null)
            {
                this.SpecType = null;
            }
            else
            {
                this.SpecType = rs.getString("SpecType").trim();
            }

            if (rs.getString("SpecCode") == null)
            {
                this.SpecCode = null;
            }
            else
            {
                this.SpecCode = rs.getString("SpecCode").trim();
            }

            if (rs.getString("SpecContent") == null)
            {
                this.SpecContent = null;
            }
            else
            {
                this.SpecContent = rs.getString("SpecContent").trim();
            }

            if (rs.getString("PrtFlag") == null)
            {
                this.PrtFlag = null;
            }
            else
            {
                this.PrtFlag = rs.getString("PrtFlag").trim();
            }

            if (rs.getString("BackupType") == null)
            {
                this.BackupType = null;
            }
            else
            {
                this.BackupType = rs.getString("BackupType").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBCSpecSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOBCSpecSchema getSchema()
    {
        LOBCSpecSchema aLOBCSpecSchema = new LOBCSpecSchema();
        aLOBCSpecSchema.setSchema(this);
        return aLOBCSpecSchema;
    }

    public LOBCSpecDB getDB()
    {
        LOBCSpecDB aDBOper = new LOBCSpecDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBCSpec描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProposalContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtSeq)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndorsementNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SpecType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SpecCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SpecContent)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BackupType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBCSpec>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            EndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                           SysConst.PACKAGESPILTER);
            SpecType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            SpecCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            SpecContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            PrtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            BackupType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBCSpecSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("ProposalContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProposalContNo));
        }
        if (FCode.equals("PrtSeq"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtSeq));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("EndorsementNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndorsementNo));
        }
        if (FCode.equals("SpecType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpecType));
        }
        if (FCode.equals("SpecCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpecCode));
        }
        if (FCode.equals("SpecContent"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpecContent));
        }
        if (FCode.equals("PrtFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtFlag));
        }
        if (FCode.equals("BackupType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BackupType));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PrtSeq);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(EndorsementNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(SpecType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(SpecCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(SpecContent);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(PrtFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(BackupType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("ProposalContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
            {
                ProposalContNo = null;
            }
        }
        if (FCode.equals("PrtSeq"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
            {
                PrtSeq = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("EndorsementNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndorsementNo = FValue.trim();
            }
            else
            {
                EndorsementNo = null;
            }
        }
        if (FCode.equals("SpecType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpecType = FValue.trim();
            }
            else
            {
                SpecType = null;
            }
        }
        if (FCode.equals("SpecCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpecCode = FValue.trim();
            }
            else
            {
                SpecCode = null;
            }
        }
        if (FCode.equals("SpecContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpecContent = FValue.trim();
            }
            else
            {
                SpecContent = null;
            }
        }
        if (FCode.equals("PrtFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtFlag = FValue.trim();
            }
            else
            {
                PrtFlag = null;
            }
        }
        if (FCode.equals("BackupType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BackupType = FValue.trim();
            }
            else
            {
                BackupType = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBCSpecSchema other = (LOBCSpecSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && ProposalContNo.equals(other.getProposalContNo())
                && PrtSeq.equals(other.getPrtSeq())
                && SerialNo.equals(other.getSerialNo())
                && EndorsementNo.equals(other.getEndorsementNo())
                && SpecType.equals(other.getSpecType())
                && SpecCode.equals(other.getSpecCode())
                && SpecContent.equals(other.getSpecContent())
                && PrtFlag.equals(other.getPrtFlag())
                && BackupType.equals(other.getBackupType())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return 3;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 4;
        }
        if (strFieldName.equals("EndorsementNo"))
        {
            return 5;
        }
        if (strFieldName.equals("SpecType"))
        {
            return 6;
        }
        if (strFieldName.equals("SpecCode"))
        {
            return 7;
        }
        if (strFieldName.equals("SpecContent"))
        {
            return 8;
        }
        if (strFieldName.equals("PrtFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("BackupType"))
        {
            return 10;
        }
        if (strFieldName.equals("Operator"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "ProposalContNo";
                break;
            case 3:
                strFieldName = "PrtSeq";
                break;
            case 4:
                strFieldName = "SerialNo";
                break;
            case 5:
                strFieldName = "EndorsementNo";
                break;
            case 6:
                strFieldName = "SpecType";
                break;
            case 7:
                strFieldName = "SpecCode";
                break;
            case 8:
                strFieldName = "SpecContent";
                break;
            case 9:
                strFieldName = "PrtFlag";
                break;
            case 10:
                strFieldName = "BackupType";
                break;
            case 11:
                strFieldName = "Operator";
                break;
            case 12:
                strFieldName = "MakeDate";
                break;
            case 13:
                strFieldName = "MakeTime";
                break;
            case 14:
                strFieldName = "ModifyDate";
                break;
            case 15:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndorsementNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpecType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpecCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpecContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BackupType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
