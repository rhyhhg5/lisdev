/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAAscriptionDB;

/*
 * <p>ClassName: LAAscriptionSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2008-04-22
 */
public class LAAscriptionSchema implements Schema, Cloneable
{
	// @Field
	/** 归属序列号 */
	private String AscripNo;
	/** 保单号码 */
	private String PolNo;
	/** 归属次数 */
	private int AscriptionCount;
	/** 挂靠代理人编码 */
	private String UnionAgent;
	/** 原代理人编码 */
	private String AgentOld;
	/** 新代理人编码 */
	private String AgentNew;
	/** 归属类别 */
	private String AClass;
	/** 最后一次有效标志 */
	private String ValidFlag;
	/** 归属日期 */
	private Date AscriptionDate;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 展业机构外部编码 */
	private String BranchAttr;
	/** 主险保单号 */
	private String MainPolNo;
	/** 备注 */
	private String Noti;
	/** 个单合同号码 */
	private String ContNo;
	/** 集体合同号码 */
	private String GrpContNo;
	/** 展业类型 */
	private String BranchType;
	/** 渠道 */
	private String BranchType2;
	/** 归属状态 */
	private String AscripState;
	/** 新的销售机构 */
	private String AgentGroup;
	/** 操作类别 */
	private String MakeType;
	/** 管理机构 */
	private String ManageCom;
	/** 原代理机构 */
	private String AgentComOld;
	/** 新代理机构 */
	private String AgentComNew;

	public static final int FIELDNUM = 27;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAAscriptionSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "AscripNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LAAscriptionSchema cloned = (LAAscriptionSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAscripNo()
	{
		return AscripNo;
	}
	public void setAscripNo(String aAscripNo)
	{
            AscripNo = aAscripNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
            PolNo = aPolNo;
	}
	public int getAscriptionCount()
	{
		return AscriptionCount;
	}
	public void setAscriptionCount(int aAscriptionCount)
	{
            AscriptionCount = aAscriptionCount;
	}
	public void setAscriptionCount(String aAscriptionCount)
	{
		if (aAscriptionCount != null && !aAscriptionCount.equals(""))
		{
			Integer tInteger = new Integer(aAscriptionCount);
			int i = tInteger.intValue();
			AscriptionCount = i;
		}
	}

	public String getUnionAgent()
	{
		return UnionAgent;
	}
	public void setUnionAgent(String aUnionAgent)
	{
            UnionAgent = aUnionAgent;
	}
	public String getAgentOld()
	{
		return AgentOld;
	}
	public void setAgentOld(String aAgentOld)
	{
            AgentOld = aAgentOld;
	}
	public String getAgentNew()
	{
		return AgentNew;
	}
	public void setAgentNew(String aAgentNew)
	{
            AgentNew = aAgentNew;
	}
	public String getAClass()
	{
		return AClass;
	}
	public void setAClass(String aAClass)
	{
            AClass = aAClass;
	}
	public String getValidFlag()
	{
		return ValidFlag;
	}
	public void setValidFlag(String aValidFlag)
	{
            ValidFlag = aValidFlag;
	}
	public String getAscriptionDate()
	{
		if( AscriptionDate != null )
			return fDate.getString(AscriptionDate);
		else
			return null;
	}
	public void setAscriptionDate(Date aAscriptionDate)
	{
            AscriptionDate = aAscriptionDate;
	}
	public void setAscriptionDate(String aAscriptionDate)
	{
		if (aAscriptionDate != null && !aAscriptionDate.equals("") )
		{
			AscriptionDate = fDate.getDate( aAscriptionDate );
		}
		else
			AscriptionDate = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getBranchAttr()
	{
		return BranchAttr;
	}
	public void setBranchAttr(String aBranchAttr)
	{
            BranchAttr = aBranchAttr;
	}
	public String getMainPolNo()
	{
		return MainPolNo;
	}
	public void setMainPolNo(String aMainPolNo)
	{
            MainPolNo = aMainPolNo;
	}
	public String getNoti()
	{
		return Noti;
	}
	public void setNoti(String aNoti)
	{
            Noti = aNoti;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
            ContNo = aContNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
            GrpContNo = aGrpContNo;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
            BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
            BranchType2 = aBranchType2;
	}
	public String getAscripState()
	{
		return AscripState;
	}
	public void setAscripState(String aAscripState)
	{
            AscripState = aAscripState;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
            AgentGroup = aAgentGroup;
	}
	public String getMakeType()
	{
		return MakeType;
	}
	public void setMakeType(String aMakeType)
	{
            MakeType = aMakeType;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getAgentComOld()
	{
		return AgentComOld;
	}
	public void setAgentComOld(String aAgentComOld)
	{
            AgentComOld = aAgentComOld;
	}
	public String getAgentComNew()
	{
		return AgentComNew;
	}
	public void setAgentComNew(String aAgentComNew)
	{
            AgentComNew = aAgentComNew;
	}

	/**
	* 使用另外一个 LAAscriptionSchema 对象给 Schema 赋值
	* @param: aLAAscriptionSchema LAAscriptionSchema
	**/
	public void setSchema(LAAscriptionSchema aLAAscriptionSchema)
	{
		this.AscripNo = aLAAscriptionSchema.getAscripNo();
		this.PolNo = aLAAscriptionSchema.getPolNo();
		this.AscriptionCount = aLAAscriptionSchema.getAscriptionCount();
		this.UnionAgent = aLAAscriptionSchema.getUnionAgent();
		this.AgentOld = aLAAscriptionSchema.getAgentOld();
		this.AgentNew = aLAAscriptionSchema.getAgentNew();
		this.AClass = aLAAscriptionSchema.getAClass();
		this.ValidFlag = aLAAscriptionSchema.getValidFlag();
		this.AscriptionDate = fDate.getDate( aLAAscriptionSchema.getAscriptionDate());
		this.Operator = aLAAscriptionSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAAscriptionSchema.getMakeDate());
		this.MakeTime = aLAAscriptionSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAAscriptionSchema.getModifyDate());
		this.ModifyTime = aLAAscriptionSchema.getModifyTime();
		this.BranchAttr = aLAAscriptionSchema.getBranchAttr();
		this.MainPolNo = aLAAscriptionSchema.getMainPolNo();
		this.Noti = aLAAscriptionSchema.getNoti();
		this.ContNo = aLAAscriptionSchema.getContNo();
		this.GrpContNo = aLAAscriptionSchema.getGrpContNo();
		this.BranchType = aLAAscriptionSchema.getBranchType();
		this.BranchType2 = aLAAscriptionSchema.getBranchType2();
		this.AscripState = aLAAscriptionSchema.getAscripState();
		this.AgentGroup = aLAAscriptionSchema.getAgentGroup();
		this.MakeType = aLAAscriptionSchema.getMakeType();
		this.ManageCom = aLAAscriptionSchema.getManageCom();
		this.AgentComOld = aLAAscriptionSchema.getAgentComOld();
		this.AgentComNew = aLAAscriptionSchema.getAgentComNew();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AscripNo") == null )
				this.AscripNo = null;
			else
				this.AscripNo = rs.getString("AscripNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			this.AscriptionCount = rs.getInt("AscriptionCount");
			if( rs.getString("UnionAgent") == null )
				this.UnionAgent = null;
			else
				this.UnionAgent = rs.getString("UnionAgent").trim();

			if( rs.getString("AgentOld") == null )
				this.AgentOld = null;
			else
				this.AgentOld = rs.getString("AgentOld").trim();

			if( rs.getString("AgentNew") == null )
				this.AgentNew = null;
			else
				this.AgentNew = rs.getString("AgentNew").trim();

			if( rs.getString("AClass") == null )
				this.AClass = null;
			else
				this.AClass = rs.getString("AClass").trim();

			if( rs.getString("ValidFlag") == null )
				this.ValidFlag = null;
			else
				this.ValidFlag = rs.getString("ValidFlag").trim();

			this.AscriptionDate = rs.getDate("AscriptionDate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BranchAttr") == null )
				this.BranchAttr = null;
			else
				this.BranchAttr = rs.getString("BranchAttr").trim();

			if( rs.getString("MainPolNo") == null )
				this.MainPolNo = null;
			else
				this.MainPolNo = rs.getString("MainPolNo").trim();

			if( rs.getString("Noti") == null )
				this.Noti = null;
			else
				this.Noti = rs.getString("Noti").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("AscripState") == null )
				this.AscripState = null;
			else
				this.AscripState = rs.getString("AscripState").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("MakeType") == null )
				this.MakeType = null;
			else
				this.MakeType = rs.getString("MakeType").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AgentComOld") == null )
				this.AgentComOld = null;
			else
				this.AgentComOld = rs.getString("AgentComOld").trim();

			if( rs.getString("AgentComNew") == null )
				this.AgentComNew = null;
			else
				this.AgentComNew = rs.getString("AgentComNew").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAAscription表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAscriptionSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAAscriptionSchema getSchema()
	{
		LAAscriptionSchema aLAAscriptionSchema = new LAAscriptionSchema();
		aLAAscriptionSchema.setSchema(this);
		return aLAAscriptionSchema;
	}

	public LAAscriptionDB getDB()
	{
		LAAscriptionDB aDBOper = new LAAscriptionDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAscription描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(AscripNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AscriptionCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(UnionAgent)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentOld)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentNew)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AClass)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ValidFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( AscriptionDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BranchAttr)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MainPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Noti)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AscripState)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentComOld)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentComNew));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAscription>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AscripNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AscriptionCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).intValue();
			UnionAgent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AgentOld = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentNew = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ValidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AscriptionDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MainPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			AscripState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			AgentComOld = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			AgentComNew = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAscriptionSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AscripNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AscripNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("AscriptionCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptionCount));
		}
		if (FCode.equals("UnionAgent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnionAgent));
		}
		if (FCode.equals("AgentOld"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentOld));
		}
		if (FCode.equals("AgentNew"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentNew));
		}
		if (FCode.equals("AClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AClass));
		}
		if (FCode.equals("ValidFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ValidFlag));
		}
		if (FCode.equals("AscriptionDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAscriptionDate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BranchAttr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
		}
		if (FCode.equals("MainPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolNo));
		}
		if (FCode.equals("Noti"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("AscripState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AscripState));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("MakeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeType));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AgentComOld"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentComOld));
		}
		if (FCode.equals("AgentComNew"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentComNew));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AscripNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 2:
				strFieldValue = String.valueOf(AscriptionCount);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(UnionAgent);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AgentOld);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentNew);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AClass);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ValidFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAscriptionDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(BranchAttr);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MainPolNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Noti);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(AscripState);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeType);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(AgentComOld);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(AgentComNew);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AscripNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AscripNo = FValue.trim();
			}
			else
				AscripNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("AscriptionCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AscriptionCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("UnionAgent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnionAgent = FValue.trim();
			}
			else
				UnionAgent = null;
		}
		if (FCode.equalsIgnoreCase("AgentOld"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentOld = FValue.trim();
			}
			else
				AgentOld = null;
		}
		if (FCode.equalsIgnoreCase("AgentNew"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentNew = FValue.trim();
			}
			else
				AgentNew = null;
		}
		if (FCode.equalsIgnoreCase("AClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AClass = FValue.trim();
			}
			else
				AClass = null;
		}
		if (FCode.equalsIgnoreCase("ValidFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ValidFlag = FValue.trim();
			}
			else
				ValidFlag = null;
		}
		if (FCode.equalsIgnoreCase("AscriptionDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AscriptionDate = fDate.getDate( FValue );
			}
			else
				AscriptionDate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchAttr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchAttr = FValue.trim();
			}
			else
				BranchAttr = null;
		}
		if (FCode.equalsIgnoreCase("MainPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainPolNo = FValue.trim();
			}
			else
				MainPolNo = null;
		}
		if (FCode.equalsIgnoreCase("Noti"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Noti = FValue.trim();
			}
			else
				Noti = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("AscripState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AscripState = FValue.trim();
			}
			else
				AscripState = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("MakeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeType = FValue.trim();
			}
			else
				MakeType = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentComOld"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentComOld = FValue.trim();
			}
			else
				AgentComOld = null;
		}
		if (FCode.equalsIgnoreCase("AgentComNew"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentComNew = FValue.trim();
			}
			else
				AgentComNew = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAAscriptionSchema other = (LAAscriptionSchema)otherObject;
		return
			AscripNo.equals(other.getAscripNo())
			&& PolNo.equals(other.getPolNo())
			&& AscriptionCount == other.getAscriptionCount()
			&& UnionAgent.equals(other.getUnionAgent())
			&& AgentOld.equals(other.getAgentOld())
			&& AgentNew.equals(other.getAgentNew())
			&& AClass.equals(other.getAClass())
			&& ValidFlag.equals(other.getValidFlag())
			&& fDate.getString(AscriptionDate).equals(other.getAscriptionDate())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& BranchAttr.equals(other.getBranchAttr())
			&& MainPolNo.equals(other.getMainPolNo())
			&& Noti.equals(other.getNoti())
			&& ContNo.equals(other.getContNo())
			&& GrpContNo.equals(other.getGrpContNo())
			&& BranchType.equals(other.getBranchType())
			&& BranchType2.equals(other.getBranchType2())
			&& AscripState.equals(other.getAscripState())
			&& AgentGroup.equals(other.getAgentGroup())
			&& MakeType.equals(other.getMakeType())
			&& ManageCom.equals(other.getManageCom())
			&& AgentComOld.equals(other.getAgentComOld())
			&& AgentComNew.equals(other.getAgentComNew());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AscripNo") ) {
			return 0;
		}
		if( strFieldName.equals("PolNo") ) {
			return 1;
		}
		if( strFieldName.equals("AscriptionCount") ) {
			return 2;
		}
		if( strFieldName.equals("UnionAgent") ) {
			return 3;
		}
		if( strFieldName.equals("AgentOld") ) {
			return 4;
		}
		if( strFieldName.equals("AgentNew") ) {
			return 5;
		}
		if( strFieldName.equals("AClass") ) {
			return 6;
		}
		if( strFieldName.equals("ValidFlag") ) {
			return 7;
		}
		if( strFieldName.equals("AscriptionDate") ) {
			return 8;
		}
		if( strFieldName.equals("Operator") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 13;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return 14;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return 15;
		}
		if( strFieldName.equals("Noti") ) {
			return 16;
		}
		if( strFieldName.equals("ContNo") ) {
			return 17;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 18;
		}
		if( strFieldName.equals("BranchType") ) {
			return 19;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 20;
		}
		if( strFieldName.equals("AscripState") ) {
			return 21;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 22;
		}
		if( strFieldName.equals("MakeType") ) {
			return 23;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 24;
		}
		if( strFieldName.equals("AgentComOld") ) {
			return 25;
		}
		if( strFieldName.equals("AgentComNew") ) {
			return 26;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AscripNo";
				break;
			case 1:
				strFieldName = "PolNo";
				break;
			case 2:
				strFieldName = "AscriptionCount";
				break;
			case 3:
				strFieldName = "UnionAgent";
				break;
			case 4:
				strFieldName = "AgentOld";
				break;
			case 5:
				strFieldName = "AgentNew";
				break;
			case 6:
				strFieldName = "AClass";
				break;
			case 7:
				strFieldName = "ValidFlag";
				break;
			case 8:
				strFieldName = "AscriptionDate";
				break;
			case 9:
				strFieldName = "Operator";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "ModifyDate";
				break;
			case 13:
				strFieldName = "ModifyTime";
				break;
			case 14:
				strFieldName = "BranchAttr";
				break;
			case 15:
				strFieldName = "MainPolNo";
				break;
			case 16:
				strFieldName = "Noti";
				break;
			case 17:
				strFieldName = "ContNo";
				break;
			case 18:
				strFieldName = "GrpContNo";
				break;
			case 19:
				strFieldName = "BranchType";
				break;
			case 20:
				strFieldName = "BranchType2";
				break;
			case 21:
				strFieldName = "AscripState";
				break;
			case 22:
				strFieldName = "AgentGroup";
				break;
			case 23:
				strFieldName = "MakeType";
				break;
			case 24:
				strFieldName = "ManageCom";
				break;
			case 25:
				strFieldName = "AgentComOld";
				break;
			case 26:
				strFieldName = "AgentComNew";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AscripNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AscriptionCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("UnionAgent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentOld") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentNew") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValidFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AscriptionDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Noti") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AscripState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentComOld") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentComNew") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_INT;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
