/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAhumandeveiBDB;

/*
 * <p>ClassName: LAhumandeveiBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-05-22
 */
public class LAhumandeveiBSchema implements Schema, Cloneable
{
	// @Field
	/** 序列号 */
	private String SeiralNo;
	/** 机构 */
	private String Mngcom;
	/** 结算年份 */
	private String Year;
	/** 结算月份 */
	private String Month;
	/** 方案编码 */
	private String PlanCode;
	/** 方案 */
	private String Plan;
	/** 档次 */
	private String Level;
	/** 本月有效人力 */
	private int TMonthHu;
	/** 上月有效人力 */
	private int LMonthHu;
	/** 月均人力 */
	private int AveHu;
	/** 获奖前提 */
	private int PreWin;
	/** 计划净增人力 */
	private int PlanHu;
	/** 实际净增人力 */
	private int RealHu;
	/** 合格人力 */
	private int QCHu;
	/** 月度fyc */
	private double MFYC;
	/** 上月在职人力 */
	private int TMonthHuW;
	/** 本月在职人力 */
	private int LMonthHuW;
	/** 备用字段1 */
	private String Remark1;
	/** 备用字段2 */
	private String Remark2;
	/** 备用字段3 */
	private String Remark3;
	/** 操作人 */
	private String Operator;
	/** 人机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 数据状态 */
	private String DataType;
	/** 累计有效人力 */
	private int SumHu;
	/** 基础人力中离职人数 */
	private int OutWorkHu;

	public static final int FIELDNUM = 28;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAhumandeveiBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SeiralNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAhumandeveiBSchema cloned = (LAhumandeveiBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSeiralNo()
	{
		return SeiralNo;
	}
	public void setSeiralNo(String aSeiralNo)
	{
		SeiralNo = aSeiralNo;
	}
	public String getMngcom()
	{
		return Mngcom;
	}
	public void setMngcom(String aMngcom)
	{
		Mngcom = aMngcom;
	}
	public String getYear()
	{
		return Year;
	}
	public void setYear(String aYear)
	{
		Year = aYear;
	}
	public String getMonth()
	{
		return Month;
	}
	public void setMonth(String aMonth)
	{
		Month = aMonth;
	}
	public String getPlanCode()
	{
		return PlanCode;
	}
	public void setPlanCode(String aPlanCode)
	{
		PlanCode = aPlanCode;
	}
	public String getPlan()
	{
		return Plan;
	}
	public void setPlan(String aPlan)
	{
		Plan = aPlan;
	}
	public String getLevel()
	{
		return Level;
	}
	public void setLevel(String aLevel)
	{
		Level = aLevel;
	}
	public int getTMonthHu()
	{
		return TMonthHu;
	}
	public void setTMonthHu(int aTMonthHu)
	{
		TMonthHu = aTMonthHu;
	}
	public void setTMonthHu(String aTMonthHu)
	{
		if (aTMonthHu != null && !aTMonthHu.equals(""))
		{
			Integer tInteger = new Integer(aTMonthHu);
			int i = tInteger.intValue();
			TMonthHu = i;
		}
	}

	public int getLMonthHu()
	{
		return LMonthHu;
	}
	public void setLMonthHu(int aLMonthHu)
	{
		LMonthHu = aLMonthHu;
	}
	public void setLMonthHu(String aLMonthHu)
	{
		if (aLMonthHu != null && !aLMonthHu.equals(""))
		{
			Integer tInteger = new Integer(aLMonthHu);
			int i = tInteger.intValue();
			LMonthHu = i;
		}
	}

	public int getAveHu()
	{
		return AveHu;
	}
	public void setAveHu(int aAveHu)
	{
		AveHu = aAveHu;
	}
	public void setAveHu(String aAveHu)
	{
		if (aAveHu != null && !aAveHu.equals(""))
		{
			Integer tInteger = new Integer(aAveHu);
			int i = tInteger.intValue();
			AveHu = i;
		}
	}

	public int getPreWin()
	{
		return PreWin;
	}
	public void setPreWin(int aPreWin)
	{
		PreWin = aPreWin;
	}
	public void setPreWin(String aPreWin)
	{
		if (aPreWin != null && !aPreWin.equals(""))
		{
			Integer tInteger = new Integer(aPreWin);
			int i = tInteger.intValue();
			PreWin = i;
		}
	}

	public int getPlanHu()
	{
		return PlanHu;
	}
	public void setPlanHu(int aPlanHu)
	{
		PlanHu = aPlanHu;
	}
	public void setPlanHu(String aPlanHu)
	{
		if (aPlanHu != null && !aPlanHu.equals(""))
		{
			Integer tInteger = new Integer(aPlanHu);
			int i = tInteger.intValue();
			PlanHu = i;
		}
	}

	public int getRealHu()
	{
		return RealHu;
	}
	public void setRealHu(int aRealHu)
	{
		RealHu = aRealHu;
	}
	public void setRealHu(String aRealHu)
	{
		if (aRealHu != null && !aRealHu.equals(""))
		{
			Integer tInteger = new Integer(aRealHu);
			int i = tInteger.intValue();
			RealHu = i;
		}
	}

	public int getQCHu()
	{
		return QCHu;
	}
	public void setQCHu(int aQCHu)
	{
		QCHu = aQCHu;
	}
	public void setQCHu(String aQCHu)
	{
		if (aQCHu != null && !aQCHu.equals(""))
		{
			Integer tInteger = new Integer(aQCHu);
			int i = tInteger.intValue();
			QCHu = i;
		}
	}

	public double getMFYC()
	{
		return MFYC;
	}
	public void setMFYC(double aMFYC)
	{
		MFYC = Arith.round(aMFYC,2);
	}
	public void setMFYC(String aMFYC)
	{
		if (aMFYC != null && !aMFYC.equals(""))
		{
			Double tDouble = new Double(aMFYC);
			double d = tDouble.doubleValue();
                MFYC = Arith.round(d,2);
		}
	}

	public int getTMonthHuW()
	{
		return TMonthHuW;
	}
	public void setTMonthHuW(int aTMonthHuW)
	{
		TMonthHuW = aTMonthHuW;
	}
	public void setTMonthHuW(String aTMonthHuW)
	{
		if (aTMonthHuW != null && !aTMonthHuW.equals(""))
		{
			Integer tInteger = new Integer(aTMonthHuW);
			int i = tInteger.intValue();
			TMonthHuW = i;
		}
	}

	public int getLMonthHuW()
	{
		return LMonthHuW;
	}
	public void setLMonthHuW(int aLMonthHuW)
	{
		LMonthHuW = aLMonthHuW;
	}
	public void setLMonthHuW(String aLMonthHuW)
	{
		if (aLMonthHuW != null && !aLMonthHuW.equals(""))
		{
			Integer tInteger = new Integer(aLMonthHuW);
			int i = tInteger.intValue();
			LMonthHuW = i;
		}
	}

	public String getRemark1()
	{
		return Remark1;
	}
	public void setRemark1(String aRemark1)
	{
		Remark1 = aRemark1;
	}
	public String getRemark2()
	{
		return Remark2;
	}
	public void setRemark2(String aRemark2)
	{
		Remark2 = aRemark2;
	}
	public String getRemark3()
	{
		return Remark3;
	}
	public void setRemark3(String aRemark3)
	{
		Remark3 = aRemark3;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getDataType()
	{
		return DataType;
	}
	public void setDataType(String aDataType)
	{
		DataType = aDataType;
	}
	public int getSumHu()
	{
		return SumHu;
	}
	public void setSumHu(int aSumHu)
	{
		SumHu = aSumHu;
	}
	public void setSumHu(String aSumHu)
	{
		if (aSumHu != null && !aSumHu.equals(""))
		{
			Integer tInteger = new Integer(aSumHu);
			int i = tInteger.intValue();
			SumHu = i;
		}
	}

	public int getOutWorkHu()
	{
		return OutWorkHu;
	}
	public void setOutWorkHu(int aOutWorkHu)
	{
		OutWorkHu = aOutWorkHu;
	}
	public void setOutWorkHu(String aOutWorkHu)
	{
		if (aOutWorkHu != null && !aOutWorkHu.equals(""))
		{
			Integer tInteger = new Integer(aOutWorkHu);
			int i = tInteger.intValue();
			OutWorkHu = i;
		}
	}


	/**
	* 使用另外一个 LAhumandeveiBSchema 对象给 Schema 赋值
	* @param: aLAhumandeveiBSchema LAhumandeveiBSchema
	**/
	public void setSchema(LAhumandeveiBSchema aLAhumandeveiBSchema)
	{
		this.SeiralNo = aLAhumandeveiBSchema.getSeiralNo();
		this.Mngcom = aLAhumandeveiBSchema.getMngcom();
		this.Year = aLAhumandeveiBSchema.getYear();
		this.Month = aLAhumandeveiBSchema.getMonth();
		this.PlanCode = aLAhumandeveiBSchema.getPlanCode();
		this.Plan = aLAhumandeveiBSchema.getPlan();
		this.Level = aLAhumandeveiBSchema.getLevel();
		this.TMonthHu = aLAhumandeveiBSchema.getTMonthHu();
		this.LMonthHu = aLAhumandeveiBSchema.getLMonthHu();
		this.AveHu = aLAhumandeveiBSchema.getAveHu();
		this.PreWin = aLAhumandeveiBSchema.getPreWin();
		this.PlanHu = aLAhumandeveiBSchema.getPlanHu();
		this.RealHu = aLAhumandeveiBSchema.getRealHu();
		this.QCHu = aLAhumandeveiBSchema.getQCHu();
		this.MFYC = aLAhumandeveiBSchema.getMFYC();
		this.TMonthHuW = aLAhumandeveiBSchema.getTMonthHuW();
		this.LMonthHuW = aLAhumandeveiBSchema.getLMonthHuW();
		this.Remark1 = aLAhumandeveiBSchema.getRemark1();
		this.Remark2 = aLAhumandeveiBSchema.getRemark2();
		this.Remark3 = aLAhumandeveiBSchema.getRemark3();
		this.Operator = aLAhumandeveiBSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAhumandeveiBSchema.getMakeDate());
		this.MakeTime = aLAhumandeveiBSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAhumandeveiBSchema.getModifyDate());
		this.ModifyTime = aLAhumandeveiBSchema.getModifyTime();
		this.DataType = aLAhumandeveiBSchema.getDataType();
		this.SumHu = aLAhumandeveiBSchema.getSumHu();
		this.OutWorkHu = aLAhumandeveiBSchema.getOutWorkHu();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SeiralNo") == null )
				this.SeiralNo = null;
			else
				this.SeiralNo = rs.getString("SeiralNo").trim();

			if( rs.getString("Mngcom") == null )
				this.Mngcom = null;
			else
				this.Mngcom = rs.getString("Mngcom").trim();

			if( rs.getString("Year") == null )
				this.Year = null;
			else
				this.Year = rs.getString("Year").trim();

			if( rs.getString("Month") == null )
				this.Month = null;
			else
				this.Month = rs.getString("Month").trim();

			if( rs.getString("PlanCode") == null )
				this.PlanCode = null;
			else
				this.PlanCode = rs.getString("PlanCode").trim();

			if( rs.getString("Plan") == null )
				this.Plan = null;
			else
				this.Plan = rs.getString("Plan").trim();

			if( rs.getString("Level") == null )
				this.Level = null;
			else
				this.Level = rs.getString("Level").trim();

			this.TMonthHu = rs.getInt("TMonthHu");
			this.LMonthHu = rs.getInt("LMonthHu");
			this.AveHu = rs.getInt("AveHu");
			this.PreWin = rs.getInt("PreWin");
			this.PlanHu = rs.getInt("PlanHu");
			this.RealHu = rs.getInt("RealHu");
			this.QCHu = rs.getInt("QCHu");
			this.MFYC = rs.getDouble("MFYC");
			this.TMonthHuW = rs.getInt("TMonthHuW");
			this.LMonthHuW = rs.getInt("LMonthHuW");
			if( rs.getString("Remark1") == null )
				this.Remark1 = null;
			else
				this.Remark1 = rs.getString("Remark1").trim();

			if( rs.getString("Remark2") == null )
				this.Remark2 = null;
			else
				this.Remark2 = rs.getString("Remark2").trim();

			if( rs.getString("Remark3") == null )
				this.Remark3 = null;
			else
				this.Remark3 = rs.getString("Remark3").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("DataType") == null )
				this.DataType = null;
			else
				this.DataType = rs.getString("DataType").trim();

			this.SumHu = rs.getInt("SumHu");
			this.OutWorkHu = rs.getInt("OutWorkHu");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAhumandeveiB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAhumandeveiBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAhumandeveiBSchema getSchema()
	{
		LAhumandeveiBSchema aLAhumandeveiBSchema = new LAhumandeveiBSchema();
		aLAhumandeveiBSchema.setSchema(this);
		return aLAhumandeveiBSchema;
	}

	public LAhumandeveiBDB getDB()
	{
		LAhumandeveiBDB aDBOper = new LAhumandeveiBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAhumandeveiB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SeiralNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mngcom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Year)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Month)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Plan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Level)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TMonthHu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LMonthHu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AveHu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PreWin));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PlanHu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RealHu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(QCHu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MFYC));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TMonthHuW));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LMonthHuW));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumHu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OutWorkHu));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAhumandeveiB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SeiralNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Mngcom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Year = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Month = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Plan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Level = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			TMonthHu= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).intValue();
			LMonthHu= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			AveHu= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			PreWin= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).intValue();
			PlanHu= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).intValue();
			RealHu= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).intValue();
			QCHu= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).intValue();
			MFYC = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			TMonthHuW= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).intValue();
			LMonthHuW= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).intValue();
			Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Remark3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			DataType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			SumHu= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			OutWorkHu= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).intValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAhumandeveiBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SeiralNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeiralNo));
		}
		if (FCode.equals("Mngcom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mngcom));
		}
		if (FCode.equals("Year"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Year));
		}
		if (FCode.equals("Month"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Month));
		}
		if (FCode.equals("PlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
		}
		if (FCode.equals("Plan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Plan));
		}
		if (FCode.equals("Level"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Level));
		}
		if (FCode.equals("TMonthHu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TMonthHu));
		}
		if (FCode.equals("LMonthHu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LMonthHu));
		}
		if (FCode.equals("AveHu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AveHu));
		}
		if (FCode.equals("PreWin"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PreWin));
		}
		if (FCode.equals("PlanHu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlanHu));
		}
		if (FCode.equals("RealHu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealHu));
		}
		if (FCode.equals("QCHu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(QCHu));
		}
		if (FCode.equals("MFYC"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MFYC));
		}
		if (FCode.equals("TMonthHuW"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TMonthHuW));
		}
		if (FCode.equals("LMonthHuW"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LMonthHuW));
		}
		if (FCode.equals("Remark1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
		}
		if (FCode.equals("Remark2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
		}
		if (FCode.equals("Remark3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark3));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("DataType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataType));
		}
		if (FCode.equals("SumHu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumHu));
		}
		if (FCode.equals("OutWorkHu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutWorkHu));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SeiralNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Mngcom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Year);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Month);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PlanCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Plan);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Level);
				break;
			case 7:
				strFieldValue = String.valueOf(TMonthHu);
				break;
			case 8:
				strFieldValue = String.valueOf(LMonthHu);
				break;
			case 9:
				strFieldValue = String.valueOf(AveHu);
				break;
			case 10:
				strFieldValue = String.valueOf(PreWin);
				break;
			case 11:
				strFieldValue = String.valueOf(PlanHu);
				break;
			case 12:
				strFieldValue = String.valueOf(RealHu);
				break;
			case 13:
				strFieldValue = String.valueOf(QCHu);
				break;
			case 14:
				strFieldValue = String.valueOf(MFYC);
				break;
			case 15:
				strFieldValue = String.valueOf(TMonthHuW);
				break;
			case 16:
				strFieldValue = String.valueOf(LMonthHuW);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Remark1);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Remark2);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Remark3);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(DataType);
				break;
			case 26:
				strFieldValue = String.valueOf(SumHu);
				break;
			case 27:
				strFieldValue = String.valueOf(OutWorkHu);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SeiralNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SeiralNo = FValue.trim();
			}
			else
				SeiralNo = null;
		}
		if (FCode.equalsIgnoreCase("Mngcom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mngcom = FValue.trim();
			}
			else
				Mngcom = null;
		}
		if (FCode.equalsIgnoreCase("Year"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Year = FValue.trim();
			}
			else
				Year = null;
		}
		if (FCode.equalsIgnoreCase("Month"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Month = FValue.trim();
			}
			else
				Month = null;
		}
		if (FCode.equalsIgnoreCase("PlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PlanCode = FValue.trim();
			}
			else
				PlanCode = null;
		}
		if (FCode.equalsIgnoreCase("Plan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Plan = FValue.trim();
			}
			else
				Plan = null;
		}
		if (FCode.equalsIgnoreCase("Level"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Level = FValue.trim();
			}
			else
				Level = null;
		}
		if (FCode.equalsIgnoreCase("TMonthHu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				TMonthHu = i;
			}
		}
		if (FCode.equalsIgnoreCase("LMonthHu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				LMonthHu = i;
			}
		}
		if (FCode.equalsIgnoreCase("AveHu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AveHu = i;
			}
		}
		if (FCode.equalsIgnoreCase("PreWin"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PreWin = i;
			}
		}
		if (FCode.equalsIgnoreCase("PlanHu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PlanHu = i;
			}
		}
		if (FCode.equalsIgnoreCase("RealHu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RealHu = i;
			}
		}
		if (FCode.equalsIgnoreCase("QCHu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				QCHu = i;
			}
		}
		if (FCode.equalsIgnoreCase("MFYC"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MFYC = d;
			}
		}
		if (FCode.equalsIgnoreCase("TMonthHuW"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				TMonthHuW = i;
			}
		}
		if (FCode.equalsIgnoreCase("LMonthHuW"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				LMonthHuW = i;
			}
		}
		if (FCode.equalsIgnoreCase("Remark1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark1 = FValue.trim();
			}
			else
				Remark1 = null;
		}
		if (FCode.equalsIgnoreCase("Remark2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark2 = FValue.trim();
			}
			else
				Remark2 = null;
		}
		if (FCode.equalsIgnoreCase("Remark3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark3 = FValue.trim();
			}
			else
				Remark3 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("DataType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataType = FValue.trim();
			}
			else
				DataType = null;
		}
		if (FCode.equalsIgnoreCase("SumHu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SumHu = i;
			}
		}
		if (FCode.equalsIgnoreCase("OutWorkHu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OutWorkHu = i;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAhumandeveiBSchema other = (LAhumandeveiBSchema)otherObject;
		return
			(SeiralNo == null ? other.getSeiralNo() == null : SeiralNo.equals(other.getSeiralNo()))
			&& (Mngcom == null ? other.getMngcom() == null : Mngcom.equals(other.getMngcom()))
			&& (Year == null ? other.getYear() == null : Year.equals(other.getYear()))
			&& (Month == null ? other.getMonth() == null : Month.equals(other.getMonth()))
			&& (PlanCode == null ? other.getPlanCode() == null : PlanCode.equals(other.getPlanCode()))
			&& (Plan == null ? other.getPlan() == null : Plan.equals(other.getPlan()))
			&& (Level == null ? other.getLevel() == null : Level.equals(other.getLevel()))
			&& TMonthHu == other.getTMonthHu()
			&& LMonthHu == other.getLMonthHu()
			&& AveHu == other.getAveHu()
			&& PreWin == other.getPreWin()
			&& PlanHu == other.getPlanHu()
			&& RealHu == other.getRealHu()
			&& QCHu == other.getQCHu()
			&& MFYC == other.getMFYC()
			&& TMonthHuW == other.getTMonthHuW()
			&& LMonthHuW == other.getLMonthHuW()
			&& (Remark1 == null ? other.getRemark1() == null : Remark1.equals(other.getRemark1()))
			&& (Remark2 == null ? other.getRemark2() == null : Remark2.equals(other.getRemark2()))
			&& (Remark3 == null ? other.getRemark3() == null : Remark3.equals(other.getRemark3()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (DataType == null ? other.getDataType() == null : DataType.equals(other.getDataType()))
			&& SumHu == other.getSumHu()
			&& OutWorkHu == other.getOutWorkHu();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SeiralNo") ) {
			return 0;
		}
		if( strFieldName.equals("Mngcom") ) {
			return 1;
		}
		if( strFieldName.equals("Year") ) {
			return 2;
		}
		if( strFieldName.equals("Month") ) {
			return 3;
		}
		if( strFieldName.equals("PlanCode") ) {
			return 4;
		}
		if( strFieldName.equals("Plan") ) {
			return 5;
		}
		if( strFieldName.equals("Level") ) {
			return 6;
		}
		if( strFieldName.equals("TMonthHu") ) {
			return 7;
		}
		if( strFieldName.equals("LMonthHu") ) {
			return 8;
		}
		if( strFieldName.equals("AveHu") ) {
			return 9;
		}
		if( strFieldName.equals("PreWin") ) {
			return 10;
		}
		if( strFieldName.equals("PlanHu") ) {
			return 11;
		}
		if( strFieldName.equals("RealHu") ) {
			return 12;
		}
		if( strFieldName.equals("QCHu") ) {
			return 13;
		}
		if( strFieldName.equals("MFYC") ) {
			return 14;
		}
		if( strFieldName.equals("TMonthHuW") ) {
			return 15;
		}
		if( strFieldName.equals("LMonthHuW") ) {
			return 16;
		}
		if( strFieldName.equals("Remark1") ) {
			return 17;
		}
		if( strFieldName.equals("Remark2") ) {
			return 18;
		}
		if( strFieldName.equals("Remark3") ) {
			return 19;
		}
		if( strFieldName.equals("Operator") ) {
			return 20;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 24;
		}
		if( strFieldName.equals("DataType") ) {
			return 25;
		}
		if( strFieldName.equals("SumHu") ) {
			return 26;
		}
		if( strFieldName.equals("OutWorkHu") ) {
			return 27;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SeiralNo";
				break;
			case 1:
				strFieldName = "Mngcom";
				break;
			case 2:
				strFieldName = "Year";
				break;
			case 3:
				strFieldName = "Month";
				break;
			case 4:
				strFieldName = "PlanCode";
				break;
			case 5:
				strFieldName = "Plan";
				break;
			case 6:
				strFieldName = "Level";
				break;
			case 7:
				strFieldName = "TMonthHu";
				break;
			case 8:
				strFieldName = "LMonthHu";
				break;
			case 9:
				strFieldName = "AveHu";
				break;
			case 10:
				strFieldName = "PreWin";
				break;
			case 11:
				strFieldName = "PlanHu";
				break;
			case 12:
				strFieldName = "RealHu";
				break;
			case 13:
				strFieldName = "QCHu";
				break;
			case 14:
				strFieldName = "MFYC";
				break;
			case 15:
				strFieldName = "TMonthHuW";
				break;
			case 16:
				strFieldName = "LMonthHuW";
				break;
			case 17:
				strFieldName = "Remark1";
				break;
			case 18:
				strFieldName = "Remark2";
				break;
			case 19:
				strFieldName = "Remark3";
				break;
			case 20:
				strFieldName = "Operator";
				break;
			case 21:
				strFieldName = "MakeDate";
				break;
			case 22:
				strFieldName = "MakeTime";
				break;
			case 23:
				strFieldName = "ModifyDate";
				break;
			case 24:
				strFieldName = "ModifyTime";
				break;
			case 25:
				strFieldName = "DataType";
				break;
			case 26:
				strFieldName = "SumHu";
				break;
			case 27:
				strFieldName = "OutWorkHu";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SeiralNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mngcom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Year") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Month") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PlanCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Plan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Level") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TMonthHu") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("LMonthHu") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AveHu") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PreWin") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PlanHu") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RealHu") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("QCHu") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MFYC") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TMonthHuW") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("LMonthHuW") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Remark1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumHu") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OutWorkHu") ) {
			return Schema.TYPE_INT;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_INT;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_INT;
				break;
			case 11:
				nFieldType = Schema.TYPE_INT;
				break;
			case 12:
				nFieldType = Schema.TYPE_INT;
				break;
			case 13:
				nFieldType = Schema.TYPE_INT;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_INT;
				break;
			case 16:
				nFieldType = Schema.TYPE_INT;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_INT;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
