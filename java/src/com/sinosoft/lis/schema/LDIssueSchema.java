/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDIssueDB;

/*
 * <p>ClassName: LDIssueSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-02-22
 */
public class LDIssueSchema implements Schema, Cloneable
{
	// @Field
	/** 问题件代码 */
	private String IssueCode;
	/** 问题件名称 */
	private String IssueName;
	/** 问题件描述 */
	private String IssueDescription;
	/** 问题件类型 */
	private String IssueType;
	/** 退回对象类型 */
	private String BackObjType;
	/** 退回对象 */
	private String BackObj;
	/** 问题件所在管理机构 */
	private String IsueManageCom;
	/** 问题件内容 */
	private String IssueCont;
	/** 问题件规则类型 */
	private String IssueRuleCode;

	public static final int FIELDNUM = 9;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDIssueSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "IssueCode";
		pk[1] = "IssueType";
		pk[2] = "IssueRuleCode";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LDIssueSchema cloned = (LDIssueSchema)super.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getIssueCode()
	{
		return IssueCode;
	}
	public void setIssueCode(String aIssueCode)
	{
            IssueCode = aIssueCode;
	}
	public String getIssueName()
	{
		return IssueName;
	}
	public void setIssueName(String aIssueName)
	{
            IssueName = aIssueName;
	}
	public String getIssueDescription()
	{
		return IssueDescription;
	}
	public void setIssueDescription(String aIssueDescription)
	{
            IssueDescription = aIssueDescription;
	}
	public String getIssueType()
	{
		return IssueType;
	}
	public void setIssueType(String aIssueType)
	{
            IssueType = aIssueType;
	}
	public String getBackObjType()
	{
		return BackObjType;
	}
	public void setBackObjType(String aBackObjType)
	{
            BackObjType = aBackObjType;
	}
	public String getBackObj()
	{
		return BackObj;
	}
	public void setBackObj(String aBackObj)
	{
            BackObj = aBackObj;
	}
	public String getIsueManageCom()
	{
		return IsueManageCom;
	}
	public void setIsueManageCom(String aIsueManageCom)
	{
            IsueManageCom = aIsueManageCom;
	}
	public String getIssueCont()
	{
		return IssueCont;
	}
	public void setIssueCont(String aIssueCont)
	{
            IssueCont = aIssueCont;
	}
	public String getIssueRuleCode()
	{
		return IssueRuleCode;
	}
	public void setIssueRuleCode(String aIssueRuleCode)
	{
            IssueRuleCode = aIssueRuleCode;
	}

	/**
	* 使用另外一个 LDIssueSchema 对象给 Schema 赋值
	* @param: aLDIssueSchema LDIssueSchema
	**/
	public void setSchema(LDIssueSchema aLDIssueSchema)
	{
		this.IssueCode = aLDIssueSchema.getIssueCode();
		this.IssueName = aLDIssueSchema.getIssueName();
		this.IssueDescription = aLDIssueSchema.getIssueDescription();
		this.IssueType = aLDIssueSchema.getIssueType();
		this.BackObjType = aLDIssueSchema.getBackObjType();
		this.BackObj = aLDIssueSchema.getBackObj();
		this.IsueManageCom = aLDIssueSchema.getIsueManageCom();
		this.IssueCont = aLDIssueSchema.getIssueCont();
		this.IssueRuleCode = aLDIssueSchema.getIssueRuleCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("IssueCode") == null )
				this.IssueCode = null;
			else
				this.IssueCode = rs.getString("IssueCode").trim();

			if( rs.getString("IssueName") == null )
				this.IssueName = null;
			else
				this.IssueName = rs.getString("IssueName").trim();

			if( rs.getString("IssueDescription") == null )
				this.IssueDescription = null;
			else
				this.IssueDescription = rs.getString("IssueDescription").trim();

			if( rs.getString("IssueType") == null )
				this.IssueType = null;
			else
				this.IssueType = rs.getString("IssueType").trim();

			if( rs.getString("BackObjType") == null )
				this.BackObjType = null;
			else
				this.BackObjType = rs.getString("BackObjType").trim();

			if( rs.getString("BackObj") == null )
				this.BackObj = null;
			else
				this.BackObj = rs.getString("BackObj").trim();

			if( rs.getString("IsueManageCom") == null )
				this.IsueManageCom = null;
			else
				this.IsueManageCom = rs.getString("IsueManageCom").trim();

			if( rs.getString("IssueCont") == null )
				this.IssueCont = null;
			else
				this.IssueCont = rs.getString("IssueCont").trim();

			if( rs.getString("IssueRuleCode") == null )
				this.IssueRuleCode = null;
			else
				this.IssueRuleCode = rs.getString("IssueRuleCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDIssue表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDIssueSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDIssueSchema getSchema()
	{
		LDIssueSchema aLDIssueSchema = new LDIssueSchema();
		aLDIssueSchema.setSchema(this);
		return aLDIssueSchema;
	}

	public LDIssueDB getDB()
	{
		LDIssueDB aDBOper = new LDIssueDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDIssue描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(IssueCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IssueName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IssueDescription)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IssueType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BackObjType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BackObj)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IsueManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IssueCont)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IssueRuleCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDIssue>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			IssueCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			IssueName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			IssueDescription = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			IssueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BackObjType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BackObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			IsueManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			IssueCont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			IssueRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDIssueSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("IssueCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueCode));
		}
		if (FCode.equals("IssueName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueName));
		}
		if (FCode.equals("IssueDescription"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueDescription));
		}
		if (FCode.equals("IssueType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueType));
		}
		if (FCode.equals("BackObjType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackObjType));
		}
		if (FCode.equals("BackObj"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackObj));
		}
		if (FCode.equals("IsueManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IsueManageCom));
		}
		if (FCode.equals("IssueCont"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueCont));
		}
		if (FCode.equals("IssueRuleCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueRuleCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(IssueCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(IssueName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(IssueDescription);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(IssueType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BackObjType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BackObj);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(IsueManageCom);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(IssueCont);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(IssueRuleCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("IssueCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueCode = FValue.trim();
			}
			else
				IssueCode = null;
		}
		if (FCode.equalsIgnoreCase("IssueName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueName = FValue.trim();
			}
			else
				IssueName = null;
		}
		if (FCode.equalsIgnoreCase("IssueDescription"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueDescription = FValue.trim();
			}
			else
				IssueDescription = null;
		}
		if (FCode.equalsIgnoreCase("IssueType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueType = FValue.trim();
			}
			else
				IssueType = null;
		}
		if (FCode.equalsIgnoreCase("BackObjType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackObjType = FValue.trim();
			}
			else
				BackObjType = null;
		}
		if (FCode.equalsIgnoreCase("BackObj"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackObj = FValue.trim();
			}
			else
				BackObj = null;
		}
		if (FCode.equalsIgnoreCase("IsueManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IsueManageCom = FValue.trim();
			}
			else
				IsueManageCom = null;
		}
		if (FCode.equalsIgnoreCase("IssueCont"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueCont = FValue.trim();
			}
			else
				IssueCont = null;
		}
		if (FCode.equalsIgnoreCase("IssueRuleCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueRuleCode = FValue.trim();
			}
			else
				IssueRuleCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDIssueSchema other = (LDIssueSchema)otherObject;
		return
			IssueCode.equals(other.getIssueCode())
			&& IssueName.equals(other.getIssueName())
			&& IssueDescription.equals(other.getIssueDescription())
			&& IssueType.equals(other.getIssueType())
			&& BackObjType.equals(other.getBackObjType())
			&& BackObj.equals(other.getBackObj())
			&& IsueManageCom.equals(other.getIsueManageCom())
			&& IssueCont.equals(other.getIssueCont())
			&& IssueRuleCode.equals(other.getIssueRuleCode());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("IssueCode") ) {
			return 0;
		}
		if( strFieldName.equals("IssueName") ) {
			return 1;
		}
		if( strFieldName.equals("IssueDescription") ) {
			return 2;
		}
		if( strFieldName.equals("IssueType") ) {
			return 3;
		}
		if( strFieldName.equals("BackObjType") ) {
			return 4;
		}
		if( strFieldName.equals("BackObj") ) {
			return 5;
		}
		if( strFieldName.equals("IsueManageCom") ) {
			return 6;
		}
		if( strFieldName.equals("IssueCont") ) {
			return 7;
		}
		if( strFieldName.equals("IssueRuleCode") ) {
			return 8;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "IssueCode";
				break;
			case 1:
				strFieldName = "IssueName";
				break;
			case 2:
				strFieldName = "IssueDescription";
				break;
			case 3:
				strFieldName = "IssueType";
				break;
			case 4:
				strFieldName = "BackObjType";
				break;
			case 5:
				strFieldName = "BackObj";
				break;
			case 6:
				strFieldName = "IsueManageCom";
				break;
			case 7:
				strFieldName = "IssueCont";
				break;
			case 8:
				strFieldName = "IssueRuleCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("IssueCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueDescription") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackObjType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackObj") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IsueManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueCont") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueRuleCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
