/*
 * <p>ClassName: LFItemFactorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2004-11-11
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LFItemFactorDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFItemFactorSchema implements Schema
{
    // @Field
    /** 内部科目编码 */
    private String ItemCode;
    /** 计算科目编码 */
    private String DownItemCode;
    /** 内部科目类型 */
    private String ItemType;
    /** 内部科目层级 */
    private int UpItemLevel;
    /** 运算符号 */
    private String Operator;
    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFItemFactorSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ItemCode";
        pk[1] = "DownItemCode";
        pk[2] = "ItemType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getItemCode()
    {
        if (ItemCode != null && !ItemCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemCode = StrTool.unicodeToGBK(ItemCode);
        }
        return ItemCode;
    }

    public void setItemCode(String aItemCode)
    {
        ItemCode = aItemCode;
    }

    public String getDownItemCode()
    {
        if (DownItemCode != null && !DownItemCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DownItemCode = StrTool.unicodeToGBK(DownItemCode);
        }
        return DownItemCode;
    }

    public void setDownItemCode(String aDownItemCode)
    {
        DownItemCode = aDownItemCode;
    }

    public String getItemType()
    {
        if (ItemType != null && !ItemType.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemType = StrTool.unicodeToGBK(ItemType);
        }
        return ItemType;
    }

    public void setItemType(String aItemType)
    {
        ItemType = aItemType;
    }

    public int getUpItemLevel()
    {
        return UpItemLevel;
    }

    public void setUpItemLevel(int aUpItemLevel)
    {
        UpItemLevel = aUpItemLevel;
    }

    public void setUpItemLevel(String aUpItemLevel)
    {
        if (aUpItemLevel != null && !aUpItemLevel.equals(""))
        {
            Integer tInteger = new Integer(aUpItemLevel);
            int i = tInteger.intValue();
            UpItemLevel = i;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 LFItemFactorSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFItemFactorSchema aLFItemFactorSchema)
    {
        this.ItemCode = aLFItemFactorSchema.getItemCode();
        this.DownItemCode = aLFItemFactorSchema.getDownItemCode();
        this.ItemType = aLFItemFactorSchema.getItemType();
        this.UpItemLevel = aLFItemFactorSchema.getUpItemLevel();
        this.Operator = aLFItemFactorSchema.getOperator();
        this.Remark = aLFItemFactorSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ItemCode") == null)
            {
                this.ItemCode = null;
            }
            else
            {
                this.ItemCode = rs.getString("ItemCode").trim();
            }

            if (rs.getString("DownItemCode") == null)
            {
                this.DownItemCode = null;
            }
            else
            {
                this.DownItemCode = rs.getString("DownItemCode").trim();
            }

            if (rs.getString("ItemType") == null)
            {
                this.ItemType = null;
            }
            else
            {
                this.ItemType = rs.getString("ItemType").trim();
            }

            this.UpItemLevel = rs.getInt("UpItemLevel");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFItemFactorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFItemFactorSchema getSchema()
    {
        LFItemFactorSchema aLFItemFactorSchema = new LFItemFactorSchema();
        aLFItemFactorSchema.setSchema(this);
        return aLFItemFactorSchema;
    }

    public LFItemFactorDB getDB()
    {
        LFItemFactorDB aDBOper = new LFItemFactorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFItemFactor描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DownItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(UpItemLevel) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFItemFactor>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            DownItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            UpItemLevel = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFItemFactorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemCode));
        }
        if (FCode.equals("DownItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DownItemCode));
        }
        if (FCode.equals("ItemType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemType));
        }
        if (FCode.equals("UpItemLevel"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpItemLevel));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ItemCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(DownItemCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ItemType);
                break;
            case 3:
                strFieldValue = String.valueOf(UpItemLevel);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemCode = FValue.trim();
            }
            else
            {
                ItemCode = null;
            }
        }
        if (FCode.equals("DownItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DownItemCode = FValue.trim();
            }
            else
            {
                DownItemCode = null;
            }
        }
        if (FCode.equals("ItemType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemType = FValue.trim();
            }
            else
            {
                ItemType = null;
            }
        }
        if (FCode.equals("UpItemLevel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                UpItemLevel = i;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFItemFactorSchema other = (LFItemFactorSchema) otherObject;
        return
                ItemCode.equals(other.getItemCode())
                && DownItemCode.equals(other.getDownItemCode())
                && ItemType.equals(other.getItemType())
                && UpItemLevel == other.getUpItemLevel()
                && Operator.equals(other.getOperator())
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ItemCode"))
        {
            return 0;
        }
        if (strFieldName.equals("DownItemCode"))
        {
            return 1;
        }
        if (strFieldName.equals("ItemType"))
        {
            return 2;
        }
        if (strFieldName.equals("UpItemLevel"))
        {
            return 3;
        }
        if (strFieldName.equals("Operator"))
        {
            return 4;
        }
        if (strFieldName.equals("Remark"))
        {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ItemCode";
                break;
            case 1:
                strFieldName = "DownItemCode";
                break;
            case 2:
                strFieldName = "ItemType";
                break;
            case 3:
                strFieldName = "UpItemLevel";
                break;
            case 4:
                strFieldName = "Operator";
                break;
            case 5:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DownItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UpItemLevel"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
