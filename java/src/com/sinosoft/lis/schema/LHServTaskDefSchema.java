/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHServTaskDefDB;

/*
 * <p>ClassName: LHServTaskDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表结构修改-服务实施管理-20060731
 * @CreateDate：2006-08-01
 */
public class LHServTaskDefSchema implements Schema, Cloneable {
    // @Field
    /** 服务任务代码 */
    private String ServTaskCode;
    /** 服务任务类型 */
    private String ServTaskType;
    /** 服务任务名称 */
    private String ServTaskName;
    /** 服务任务描述 */
    private String ServTaskDes;
    /** 服务任务关联模块 */
    private String ServTaskModule;
    /** 关联模块地址 */
    private String ModuleURL;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHServTaskDefSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ServTaskCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHServTaskDefSchema cloned = (LHServTaskDefSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getServTaskCode() {
        return ServTaskCode;
    }

    public void setServTaskCode(String aServTaskCode) {
        ServTaskCode = aServTaskCode;
    }

    public String getServTaskType() {
        return ServTaskType;
    }

    public void setServTaskType(String aServTaskType) {
        ServTaskType = aServTaskType;
    }

    public String getServTaskName() {
        return ServTaskName;
    }

    public void setServTaskName(String aServTaskName) {
        ServTaskName = aServTaskName;
    }

    public String getServTaskDes() {
        return ServTaskDes;
    }

    public void setServTaskDes(String aServTaskDes) {
        ServTaskDes = aServTaskDes;
    }

    public String getServTaskModule() {
        return ServTaskModule;
    }

    public void setServTaskModule(String aServTaskModule) {
        ServTaskModule = aServTaskModule;
    }

    public String getModuleURL() {
        return ModuleURL;
    }

    public void setModuleURL(String aModuleURL) {
        ModuleURL = aModuleURL;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHServTaskDefSchema 对象给 Schema 赋值
     * @param: aLHServTaskDefSchema LHServTaskDefSchema
     **/
    public void setSchema(LHServTaskDefSchema aLHServTaskDefSchema) {
        this.ServTaskCode = aLHServTaskDefSchema.getServTaskCode();
        this.ServTaskType = aLHServTaskDefSchema.getServTaskType();
        this.ServTaskName = aLHServTaskDefSchema.getServTaskName();
        this.ServTaskDes = aLHServTaskDefSchema.getServTaskDes();
        this.ServTaskModule = aLHServTaskDefSchema.getServTaskModule();
        this.ModuleURL = aLHServTaskDefSchema.getModuleURL();
        this.ManageCom = aLHServTaskDefSchema.getManageCom();
        this.Operator = aLHServTaskDefSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHServTaskDefSchema.getMakeDate());
        this.MakeTime = aLHServTaskDefSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHServTaskDefSchema.getModifyDate());
        this.ModifyTime = aLHServTaskDefSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ServTaskCode") == null) {
                this.ServTaskCode = null;
            } else {
                this.ServTaskCode = rs.getString("ServTaskCode").trim();
            }

            if (rs.getString("ServTaskType") == null) {
                this.ServTaskType = null;
            } else {
                this.ServTaskType = rs.getString("ServTaskType").trim();
            }

            if (rs.getString("ServTaskName") == null) {
                this.ServTaskName = null;
            } else {
                this.ServTaskName = rs.getString("ServTaskName").trim();
            }

            if (rs.getString("ServTaskDes") == null) {
                this.ServTaskDes = null;
            } else {
                this.ServTaskDes = rs.getString("ServTaskDes").trim();
            }

            if (rs.getString("ServTaskModule") == null) {
                this.ServTaskModule = null;
            } else {
                this.ServTaskModule = rs.getString("ServTaskModule").trim();
            }

            if (rs.getString("ModuleURL") == null) {
                this.ModuleURL = null;
            } else {
                this.ModuleURL = rs.getString("ModuleURL").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHServTaskDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskDefSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHServTaskDefSchema getSchema() {
        LHServTaskDefSchema aLHServTaskDefSchema = new LHServTaskDefSchema();
        aLHServTaskDefSchema.setSchema(this);
        return aLHServTaskDefSchema;
    }

    public LHServTaskDefDB getDB() {
        LHServTaskDefDB aDBOper = new LHServTaskDefDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServTaskDef描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ServTaskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskDes));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskModule));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModuleURL));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServTaskDef>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ServTaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ServTaskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ServTaskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ServTaskDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            ServTaskModule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                            SysConst.PACKAGESPILTER);
            ModuleURL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskDefSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ServTaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskCode));
        }
        if (FCode.equals("ServTaskType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskType));
        }
        if (FCode.equals("ServTaskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskName));
        }
        if (FCode.equals("ServTaskDes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskDes));
        }
        if (FCode.equals("ServTaskModule")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskModule));
        }
        if (FCode.equals("ModuleURL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModuleURL));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ServTaskCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServTaskType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ServTaskName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServTaskDes);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServTaskModule);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ModuleURL);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ServTaskCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskCode = FValue.trim();
            } else {
                ServTaskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskType")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskType = FValue.trim();
            } else {
                ServTaskType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskName")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskName = FValue.trim();
            } else {
                ServTaskName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskDes")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskDes = FValue.trim();
            } else {
                ServTaskDes = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskModule")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskModule = FValue.trim();
            } else {
                ServTaskModule = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModuleURL")) {
            if (FValue != null && !FValue.equals("")) {
                ModuleURL = FValue.trim();
            } else {
                ModuleURL = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHServTaskDefSchema other = (LHServTaskDefSchema) otherObject;
        return
                ServTaskCode.equals(other.getServTaskCode())
                && ServTaskType.equals(other.getServTaskType())
                && ServTaskName.equals(other.getServTaskName())
                && ServTaskDes.equals(other.getServTaskDes())
                && ServTaskModule.equals(other.getServTaskModule())
                && ModuleURL.equals(other.getModuleURL())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ServTaskCode")) {
            return 0;
        }
        if (strFieldName.equals("ServTaskType")) {
            return 1;
        }
        if (strFieldName.equals("ServTaskName")) {
            return 2;
        }
        if (strFieldName.equals("ServTaskDes")) {
            return 3;
        }
        if (strFieldName.equals("ServTaskModule")) {
            return 4;
        }
        if (strFieldName.equals("ModuleURL")) {
            return 5;
        }
        if (strFieldName.equals("ManageCom")) {
            return 6;
        }
        if (strFieldName.equals("Operator")) {
            return 7;
        }
        if (strFieldName.equals("MakeDate")) {
            return 8;
        }
        if (strFieldName.equals("MakeTime")) {
            return 9;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 10;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ServTaskCode";
            break;
        case 1:
            strFieldName = "ServTaskType";
            break;
        case 2:
            strFieldName = "ServTaskName";
            break;
        case 3:
            strFieldName = "ServTaskDes";
            break;
        case 4:
            strFieldName = "ServTaskModule";
            break;
        case 5:
            strFieldName = "ModuleURL";
            break;
        case 6:
            strFieldName = "ManageCom";
            break;
        case 7:
            strFieldName = "Operator";
            break;
        case 8:
            strFieldName = "MakeDate";
            break;
        case 9:
            strFieldName = "MakeTime";
            break;
        case 10:
            strFieldName = "ModifyDate";
            break;
        case 11:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ServTaskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskDes")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskModule")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModuleURL")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
