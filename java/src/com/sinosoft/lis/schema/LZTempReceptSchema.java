/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZTempReceptDB;

/*
 * <p>ClassName: LZTempReceptSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-06-26
 */
public class LZTempReceptSchema implements Schema, Cloneable {
    // @Field
    /** 台头（收据方名称） */
    private String ReceptHeading;
    /** 投保单号 */
    private String ContNo;
    /** 暂收据号 */
    private String TempFeeNo;
    /** 对应其它号码 */
    private String OtherNo;
    /** 金额 */
    private double PayMoney;
    /** 单证领用人 */
    private String AgentCode;
    /** 业务处理人 */
    private String Handler;
    /** 业务处理机构 */
    private String HandleCom;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String ManageCom;
    /** 状态 */
    private String State;
    /** 入机时间 */
    private String MakeTime;
    /** 入机日期 */
    private Date MakeDate;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 开票日期 */
    private Date OpenTime;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LZTempReceptSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ContNo";
        pk[1] = "TempFeeNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LZTempReceptSchema cloned = (LZTempReceptSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getReceptHeading() {
        return ReceptHeading;
    }

    public void setReceptHeading(String aReceptHeading) {
        ReceptHeading = aReceptHeading;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getTempFeeNo() {
        return TempFeeNo;
    }

    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }

    public String getOtherNo() {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }

    public double getPayMoney() {
        return PayMoney;
    }

    public void setPayMoney(double aPayMoney) {
        PayMoney = Arith.round(aPayMoney, 2);
    }

    public void setPayMoney(String aPayMoney) {
        if (aPayMoney != null && !aPayMoney.equals("")) {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = Arith.round(d, 2);
        }
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getHandler() {
        return Handler;
    }

    public void setHandler(String aHandler) {
        Handler = aHandler;
    }

    public String getHandleCom() {
        return HandleCom;
    }

    public void setHandleCom(String aHandleCom) {
        HandleCom = aHandleCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getOpenTime() {
        if (OpenTime != null) {
            return fDate.getString(OpenTime);
        } else {
            return null;
        }
    }

    public void setOpenTime(Date aOpenTime) {
        OpenTime = aOpenTime;
    }

    public void setOpenTime(String aOpenTime) {
        if (aOpenTime != null && !aOpenTime.equals("")) {
            OpenTime = fDate.getDate(aOpenTime);
        } else {
            OpenTime = null;
        }
    }


    /**
     * 使用另外一个 LZTempReceptSchema 对象给 Schema 赋值
     * @param: aLZTempReceptSchema LZTempReceptSchema
     **/
    public void setSchema(LZTempReceptSchema aLZTempReceptSchema) {
        this.ReceptHeading = aLZTempReceptSchema.getReceptHeading();
        this.ContNo = aLZTempReceptSchema.getContNo();
        this.TempFeeNo = aLZTempReceptSchema.getTempFeeNo();
        this.OtherNo = aLZTempReceptSchema.getOtherNo();
        this.PayMoney = aLZTempReceptSchema.getPayMoney();
        this.AgentCode = aLZTempReceptSchema.getAgentCode();
        this.Handler = aLZTempReceptSchema.getHandler();
        this.HandleCom = aLZTempReceptSchema.getHandleCom();
        this.Operator = aLZTempReceptSchema.getOperator();
        this.ManageCom = aLZTempReceptSchema.getManageCom();
        this.State = aLZTempReceptSchema.getState();
        this.MakeTime = aLZTempReceptSchema.getMakeTime();
        this.MakeDate = fDate.getDate(aLZTempReceptSchema.getMakeDate());
        this.ModifyDate = fDate.getDate(aLZTempReceptSchema.getModifyDate());
        this.ModifyTime = aLZTempReceptSchema.getModifyTime();
        this.OpenTime = fDate.getDate(aLZTempReceptSchema.getOpenTime());
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ReceptHeading") == null) {
                this.ReceptHeading = null;
            } else {
                this.ReceptHeading = rs.getString("ReceptHeading").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("TempFeeNo") == null) {
                this.TempFeeNo = null;
            } else {
                this.TempFeeNo = rs.getString("TempFeeNo").trim();
            }

            if (rs.getString("OtherNo") == null) {
                this.OtherNo = null;
            } else {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            this.PayMoney = rs.getDouble("PayMoney");
            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("Handler") == null) {
                this.Handler = null;
            } else {
                this.Handler = rs.getString("Handler").trim();
            }

            if (rs.getString("HandleCom") == null) {
                this.HandleCom = null;
            } else {
                this.HandleCom = rs.getString("HandleCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.OpenTime = rs.getDate("OpenTime");
        } catch (SQLException sqle) {
            System.out.println("数据库中的LZTempRecept表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZTempReceptSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LZTempReceptSchema getSchema() {
        LZTempReceptSchema aLZTempReceptSchema = new LZTempReceptSchema();
        aLZTempReceptSchema.setSchema(this);
        return aLZTempReceptSchema;
    }

    public LZTempReceptDB getDB() {
        LZTempReceptDB aDBOper = new LZTempReceptDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZTempRecept描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ReceptHeading));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TempFeeNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Handler));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HandleCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(OpenTime)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZTempRecept>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ReceptHeading = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            PayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            HandleCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            OpenTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZTempReceptSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ReceptHeading")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceptHeading));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equals("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("Handler")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
        }
        if (FCode.equals("HandleCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HandleCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("OpenTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getOpenTime()));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ReceptHeading);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(OtherNo);
            break;
        case 4:
            strFieldValue = String.valueOf(PayMoney);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Handler);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(HandleCom);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getOpenTime()));
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ReceptHeading")) {
            if (FValue != null && !FValue.equals("")) {
                ReceptHeading = FValue.trim();
            } else {
                ReceptHeading = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if (FValue != null && !FValue.equals("")) {
                TempFeeNo = FValue.trim();
            } else {
                TempFeeNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if (FValue != null && !FValue.equals("")) {
                OtherNo = FValue.trim();
            } else {
                OtherNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            if (FValue != null && !FValue.equals("")) {
                Handler = FValue.trim();
            } else {
                Handler = null;
            }
        }
        if (FCode.equalsIgnoreCase("HandleCom")) {
            if (FValue != null && !FValue.equals("")) {
                HandleCom = FValue.trim();
            } else {
                HandleCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("OpenTime")) {
            if (FValue != null && !FValue.equals("")) {
                OpenTime = fDate.getDate(FValue);
            } else {
                OpenTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LZTempReceptSchema other = (LZTempReceptSchema) otherObject;
        return
                ReceptHeading.equals(other.getReceptHeading())
                && ContNo.equals(other.getContNo())
                && TempFeeNo.equals(other.getTempFeeNo())
                && OtherNo.equals(other.getOtherNo())
                && PayMoney == other.getPayMoney()
                && AgentCode.equals(other.getAgentCode())
                && Handler.equals(other.getHandler())
                && HandleCom.equals(other.getHandleCom())
                && Operator.equals(other.getOperator())
                && ManageCom.equals(other.getManageCom())
                && State.equals(other.getState())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(OpenTime).equals(other.getOpenTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ReceptHeading")) {
            return 0;
        }
        if (strFieldName.equals("ContNo")) {
            return 1;
        }
        if (strFieldName.equals("TempFeeNo")) {
            return 2;
        }
        if (strFieldName.equals("OtherNo")) {
            return 3;
        }
        if (strFieldName.equals("PayMoney")) {
            return 4;
        }
        if (strFieldName.equals("AgentCode")) {
            return 5;
        }
        if (strFieldName.equals("Handler")) {
            return 6;
        }
        if (strFieldName.equals("HandleCom")) {
            return 7;
        }
        if (strFieldName.equals("Operator")) {
            return 8;
        }
        if (strFieldName.equals("ManageCom")) {
            return 9;
        }
        if (strFieldName.equals("State")) {
            return 10;
        }
        if (strFieldName.equals("MakeTime")) {
            return 11;
        }
        if (strFieldName.equals("MakeDate")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        if (strFieldName.equals("OpenTime")) {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ReceptHeading";
            break;
        case 1:
            strFieldName = "ContNo";
            break;
        case 2:
            strFieldName = "TempFeeNo";
            break;
        case 3:
            strFieldName = "OtherNo";
            break;
        case 4:
            strFieldName = "PayMoney";
            break;
        case 5:
            strFieldName = "AgentCode";
            break;
        case 6:
            strFieldName = "Handler";
            break;
        case 7:
            strFieldName = "HandleCom";
            break;
        case 8:
            strFieldName = "Operator";
            break;
        case 9:
            strFieldName = "ManageCom";
            break;
        case 10:
            strFieldName = "State";
            break;
        case 11:
            strFieldName = "MakeTime";
            break;
        case 12:
            strFieldName = "MakeDate";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        case 15:
            strFieldName = "OpenTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ReceptHeading")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TempFeeNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMoney")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Handler")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HandleCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OpenTime")) {
            return Schema.TYPE_DATE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
