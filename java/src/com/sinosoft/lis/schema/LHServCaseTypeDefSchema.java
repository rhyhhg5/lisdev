/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHServCaseTypeDefDB;

/*
 * <p>ClassName: LHServCaseTypeDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表修改-服务事件类型设置_20060704
 * @CreateDate：2006-07-04
 */
public class LHServCaseTypeDefSchema implements Schema, Cloneable {
    // @Field
    /** 标准服务项目代码 */
    private String ServItemCode;
    /** 机构属性标识 */
    private String ComID;
    /** 保单类型 */
    private String ContType;
    /** 服务事件类型 */
    private String ServCaseType;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHServCaseTypeDefSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ServItemCode";
        pk[1] = "ComID";
        pk[2] = "ContType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHServCaseTypeDefSchema cloned = (LHServCaseTypeDefSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getServItemCode() {
        return ServItemCode;
    }

    public void setServItemCode(String aServItemCode) {
        ServItemCode = aServItemCode;
    }

    public String getComID() {
        return ComID;
    }

    public void setComID(String aComID) {
        ComID = aComID;
    }

    public String getContType() {
        return ContType;
    }

    public void setContType(String aContType) {
        ContType = aContType;
    }

    public String getServCaseType() {
        return ServCaseType;
    }

    public void setServCaseType(String aServCaseType) {
        ServCaseType = aServCaseType;
    }

    /**
     * 使用另外一个 LHServCaseTypeDefSchema 对象给 Schema 赋值
     * @param: aLHServCaseTypeDefSchema LHServCaseTypeDefSchema
     **/
    public void setSchema(LHServCaseTypeDefSchema aLHServCaseTypeDefSchema) {
        this.ServItemCode = aLHServCaseTypeDefSchema.getServItemCode();
        this.ComID = aLHServCaseTypeDefSchema.getComID();
        this.ContType = aLHServCaseTypeDefSchema.getContType();
        this.ServCaseType = aLHServCaseTypeDefSchema.getServCaseType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ServItemCode") == null) {
                this.ServItemCode = null;
            } else {
                this.ServItemCode = rs.getString("ServItemCode").trim();
            }

            if (rs.getString("ComID") == null) {
                this.ComID = null;
            } else {
                this.ComID = rs.getString("ComID").trim();
            }

            if (rs.getString("ContType") == null) {
                this.ContType = null;
            } else {
                this.ContType = rs.getString("ContType").trim();
            }

            if (rs.getString("ServCaseType") == null) {
                this.ServCaseType = null;
            } else {
                this.ServCaseType = rs.getString("ServCaseType").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHServCaseTypeDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServCaseTypeDefSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHServCaseTypeDefSchema getSchema() {
        LHServCaseTypeDefSchema aLHServCaseTypeDefSchema = new
                LHServCaseTypeDefSchema();
        aLHServCaseTypeDefSchema.setSchema(this);
        return aLHServCaseTypeDefSchema;
    }

    public LHServCaseTypeDefDB getDB() {
        LHServCaseTypeDefDB aDBOper = new LHServCaseTypeDefDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServCaseTypeDef描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ServItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseType));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServCaseTypeDef>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ServItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ComID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            ServCaseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServCaseTypeDefSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ServItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemCode));
        }
        if (FCode.equals("ComID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComID));
        }
        if (FCode.equals("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equals("ServCaseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ServItemCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ComID);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ContType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServCaseType);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ServItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemCode = FValue.trim();
            } else {
                ServItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComID")) {
            if (FValue != null && !FValue.equals("")) {
                ComID = FValue.trim();
            } else {
                ComID = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if (FValue != null && !FValue.equals("")) {
                ContType = FValue.trim();
            } else {
                ContType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseType")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseType = FValue.trim();
            } else {
                ServCaseType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHServCaseTypeDefSchema other = (LHServCaseTypeDefSchema) otherObject;
        return
                ServItemCode.equals(other.getServItemCode())
                && ComID.equals(other.getComID())
                && ContType.equals(other.getContType())
                && ServCaseType.equals(other.getServCaseType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ServItemCode")) {
            return 0;
        }
        if (strFieldName.equals("ComID")) {
            return 1;
        }
        if (strFieldName.equals("ContType")) {
            return 2;
        }
        if (strFieldName.equals("ServCaseType")) {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ServItemCode";
            break;
        case 1:
            strFieldName = "ComID";
            break;
        case 2:
            strFieldName = "ContType";
            break;
        case 3:
            strFieldName = "ServCaseType";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ServItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseType")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
