/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAIndexVsAssessDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAIndexVsAssessSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAIndexVsAssessSchema implements Schema
{
    // @Field
    /** 代理人职级 */
    private String AgentGrade;
    /** 目标代理人职级 */
    private String DestAgentGrade;
    /** 考核代码 */
    private String AssessCode;
    /** 展业类型 */
    private String BranchType;
    /** 考核类型 */
    private String AssessType;
    /** 考核名称 */
    private String AssessName;
    /** Sql语句编码 */
    private String CalCode;
    /** 对应指标编码 */
    private String IndexCode;
    /** 考核指标对应表名 */
    private String ATableName;
    /** 考核指标对应字段名 */
    private String AColName;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAIndexVsAssessSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AgentGrade";
        pk[1] = "AssessCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getDestAgentGrade()
    {
        if (SysConst.CHANGECHARSET && DestAgentGrade != null &&
            !DestAgentGrade.equals(""))
        {
            DestAgentGrade = StrTool.unicodeToGBK(DestAgentGrade);
        }
        return DestAgentGrade;
    }

    public void setDestAgentGrade(String aDestAgentGrade)
    {
        DestAgentGrade = aDestAgentGrade;
    }

    public String getAssessCode()
    {
        if (SysConst.CHANGECHARSET && AssessCode != null &&
            !AssessCode.equals(""))
        {
            AssessCode = StrTool.unicodeToGBK(AssessCode);
        }
        return AssessCode;
    }

    public void setAssessCode(String aAssessCode)
    {
        AssessCode = aAssessCode;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getAssessType()
    {
        if (SysConst.CHANGECHARSET && AssessType != null &&
            !AssessType.equals(""))
        {
            AssessType = StrTool.unicodeToGBK(AssessType);
        }
        return AssessType;
    }

    public void setAssessType(String aAssessType)
    {
        AssessType = aAssessType;
    }

    public String getAssessName()
    {
        if (SysConst.CHANGECHARSET && AssessName != null &&
            !AssessName.equals(""))
        {
            AssessName = StrTool.unicodeToGBK(AssessName);
        }
        return AssessName;
    }

    public void setAssessName(String aAssessName)
    {
        AssessName = aAssessName;
    }

    public String getCalCode()
    {
        if (SysConst.CHANGECHARSET && CalCode != null && !CalCode.equals(""))
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getIndexCode()
    {
        if (SysConst.CHANGECHARSET && IndexCode != null && !IndexCode.equals(""))
        {
            IndexCode = StrTool.unicodeToGBK(IndexCode);
        }
        return IndexCode;
    }

    public void setIndexCode(String aIndexCode)
    {
        IndexCode = aIndexCode;
    }

    public String getATableName()
    {
        if (SysConst.CHANGECHARSET && ATableName != null &&
            !ATableName.equals(""))
        {
            ATableName = StrTool.unicodeToGBK(ATableName);
        }
        return ATableName;
    }

    public void setATableName(String aATableName)
    {
        ATableName = aATableName;
    }

    public String getAColName()
    {
        if (SysConst.CHANGECHARSET && AColName != null && !AColName.equals(""))
        {
            AColName = StrTool.unicodeToGBK(AColName);
        }
        return AColName;
    }

    public void setAColName(String aAColName)
    {
        AColName = aAColName;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAIndexVsAssessSchema 对象给 Schema 赋值
     * @param: aLAIndexVsAssessSchema LAIndexVsAssessSchema
     **/
    public void setSchema(LAIndexVsAssessSchema aLAIndexVsAssessSchema)
    {
        this.AgentGrade = aLAIndexVsAssessSchema.getAgentGrade();
        this.DestAgentGrade = aLAIndexVsAssessSchema.getDestAgentGrade();
        this.AssessCode = aLAIndexVsAssessSchema.getAssessCode();
        this.BranchType = aLAIndexVsAssessSchema.getBranchType();
        this.AssessType = aLAIndexVsAssessSchema.getAssessType();
        this.AssessName = aLAIndexVsAssessSchema.getAssessName();
        this.CalCode = aLAIndexVsAssessSchema.getCalCode();
        this.IndexCode = aLAIndexVsAssessSchema.getIndexCode();
        this.ATableName = aLAIndexVsAssessSchema.getATableName();
        this.AColName = aLAIndexVsAssessSchema.getAColName();
        this.BranchType2 = aLAIndexVsAssessSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("DestAgentGrade") == null)
            {
                this.DestAgentGrade = null;
            }
            else
            {
                this.DestAgentGrade = rs.getString("DestAgentGrade").trim();
            }

            if (rs.getString("AssessCode") == null)
            {
                this.AssessCode = null;
            }
            else
            {
                this.AssessCode = rs.getString("AssessCode").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("AssessType") == null)
            {
                this.AssessType = null;
            }
            else
            {
                this.AssessType = rs.getString("AssessType").trim();
            }

            if (rs.getString("AssessName") == null)
            {
                this.AssessName = null;
            }
            else
            {
                this.AssessName = rs.getString("AssessName").trim();
            }

            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("IndexCode") == null)
            {
                this.IndexCode = null;
            }
            else
            {
                this.IndexCode = rs.getString("IndexCode").trim();
            }

            if (rs.getString("ATableName") == null)
            {
                this.ATableName = null;
            }
            else
            {
                this.ATableName = rs.getString("ATableName").trim();
            }

            if (rs.getString("AColName") == null)
            {
                this.AColName = null;
            }
            else
            {
                this.AColName = rs.getString("AColName").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexVsAssessSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAIndexVsAssessSchema getSchema()
    {
        LAIndexVsAssessSchema aLAIndexVsAssessSchema = new
                LAIndexVsAssessSchema();
        aLAIndexVsAssessSchema.setSchema(this);
        return aLAIndexVsAssessSchema;
    }

    public LAIndexVsAssessDB getDB()
    {
        LAIndexVsAssessDB aDBOper = new LAIndexVsAssessDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAIndexVsAssess描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DestAgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CalCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IndexCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ATableName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AColName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAIndexVsAssess>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            DestAgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                            SysConst.PACKAGESPILTER);
            AssessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            AssessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            AssessName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            ATableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            AColName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexVsAssessSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("DestAgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestAgentGrade));
        }
        if (FCode.equals("AssessCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessCode));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("AssessType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessType));
        }
        if (FCode.equals("AssessName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessName));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equals("IndexCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
        }
        if (FCode.equals("ATableName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ATableName));
        }
        if (FCode.equals("AColName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AColName));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(DestAgentGrade);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AssessCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AssessType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AssessName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(IndexCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ATableName);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AColName);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("DestAgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestAgentGrade = FValue.trim();
            }
            else
            {
                DestAgentGrade = null;
            }
        }
        if (FCode.equals("AssessCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessCode = FValue.trim();
            }
            else
            {
                AssessCode = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("AssessType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessType = FValue.trim();
            }
            else
            {
                AssessType = null;
            }
        }
        if (FCode.equals("AssessName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessName = FValue.trim();
            }
            else
            {
                AssessName = null;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("IndexCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexCode = FValue.trim();
            }
            else
            {
                IndexCode = null;
            }
        }
        if (FCode.equals("ATableName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ATableName = FValue.trim();
            }
            else
            {
                ATableName = null;
            }
        }
        if (FCode.equals("AColName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AColName = FValue.trim();
            }
            else
            {
                AColName = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAIndexVsAssessSchema other = (LAIndexVsAssessSchema) otherObject;
        return
                AgentGrade.equals(other.getAgentGrade())
                && DestAgentGrade.equals(other.getDestAgentGrade())
                && AssessCode.equals(other.getAssessCode())
                && BranchType.equals(other.getBranchType())
                && AssessType.equals(other.getAssessType())
                && AssessName.equals(other.getAssessName())
                && CalCode.equals(other.getCalCode())
                && IndexCode.equals(other.getIndexCode())
                && ATableName.equals(other.getATableName())
                && AColName.equals(other.getAColName())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return 0;
        }
        if (strFieldName.equals("DestAgentGrade"))
        {
            return 1;
        }
        if (strFieldName.equals("AssessCode"))
        {
            return 2;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 3;
        }
        if (strFieldName.equals("AssessType"))
        {
            return 4;
        }
        if (strFieldName.equals("AssessName"))
        {
            return 5;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 6;
        }
        if (strFieldName.equals("IndexCode"))
        {
            return 7;
        }
        if (strFieldName.equals("ATableName"))
        {
            return 8;
        }
        if (strFieldName.equals("AColName"))
        {
            return 9;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentGrade";
                break;
            case 1:
                strFieldName = "DestAgentGrade";
                break;
            case 2:
                strFieldName = "AssessCode";
                break;
            case 3:
                strFieldName = "BranchType";
                break;
            case 4:
                strFieldName = "AssessType";
                break;
            case 5:
                strFieldName = "AssessName";
                break;
            case 6:
                strFieldName = "CalCode";
                break;
            case 7:
                strFieldName = "IndexCode";
                break;
            case 8:
                strFieldName = "ATableName";
                break;
            case 9:
                strFieldName = "AColName";
                break;
            case 10:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestAgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ATableName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AColName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
