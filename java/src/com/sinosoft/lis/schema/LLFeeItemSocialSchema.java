/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLFeeItemSocialDB;

/*
 * <p>ClassName: LLFeeItemSocialSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-12-10
 */
public class LLFeeItemSocialSchema implements Schema, Cloneable
{
	// @Field
	/** 账单明细流水号 */
	private String SocialFeeDetailNo;
	/** 账单项目流水号 */
	private String SocialFeeNo;
	/** 案件流水号 */
	private String SocialCaseNo;
	/** 批次流水号 */
	private String SocialRgtNo;
	/** 理赔信息序号 */
	private String TransacTionNo;
	/** 费用项目代码 */
	private String FeeItemCode;
	/** 费用金额 */
	private double Fee;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 13;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLFeeItemSocialSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SocialFeeDetailNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLFeeItemSocialSchema cloned = (LLFeeItemSocialSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSocialFeeDetailNo()
	{
		return SocialFeeDetailNo;
	}
	public void setSocialFeeDetailNo(String aSocialFeeDetailNo)
	{
		SocialFeeDetailNo = aSocialFeeDetailNo;
	}
	public String getSocialFeeNo()
	{
		return SocialFeeNo;
	}
	public void setSocialFeeNo(String aSocialFeeNo)
	{
		SocialFeeNo = aSocialFeeNo;
	}
	public String getSocialCaseNo()
	{
		return SocialCaseNo;
	}
	public void setSocialCaseNo(String aSocialCaseNo)
	{
		SocialCaseNo = aSocialCaseNo;
	}
	public String getSocialRgtNo()
	{
		return SocialRgtNo;
	}
	public void setSocialRgtNo(String aSocialRgtNo)
	{
		SocialRgtNo = aSocialRgtNo;
	}
	public String getTransacTionNo()
	{
		return TransacTionNo;
	}
	public void setTransacTionNo(String aTransacTionNo)
	{
		TransacTionNo = aTransacTionNo;
	}
	public String getFeeItemCode()
	{
		return FeeItemCode;
	}
	public void setFeeItemCode(String aFeeItemCode)
	{
		FeeItemCode = aFeeItemCode;
	}
	public double getFee()
	{
		return Fee;
	}
	public void setFee(double aFee)
	{
		Fee = Arith.round(aFee,2);
	}
	public void setFee(String aFee)
	{
		if (aFee != null && !aFee.equals(""))
		{
			Double tDouble = new Double(aFee);
			double d = tDouble.doubleValue();
                Fee = Arith.round(d,2);
		}
	}

	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLFeeItemSocialSchema 对象给 Schema 赋值
	* @param: aLLFeeItemSocialSchema LLFeeItemSocialSchema
	**/
	public void setSchema(LLFeeItemSocialSchema aLLFeeItemSocialSchema)
	{
		this.SocialFeeDetailNo = aLLFeeItemSocialSchema.getSocialFeeDetailNo();
		this.SocialFeeNo = aLLFeeItemSocialSchema.getSocialFeeNo();
		this.SocialCaseNo = aLLFeeItemSocialSchema.getSocialCaseNo();
		this.SocialRgtNo = aLLFeeItemSocialSchema.getSocialRgtNo();
		this.TransacTionNo = aLLFeeItemSocialSchema.getTransacTionNo();
		this.FeeItemCode = aLLFeeItemSocialSchema.getFeeItemCode();
		this.Fee = aLLFeeItemSocialSchema.getFee();
		this.MngCom = aLLFeeItemSocialSchema.getMngCom();
		this.Operator = aLLFeeItemSocialSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLFeeItemSocialSchema.getMakeDate());
		this.MakeTime = aLLFeeItemSocialSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLFeeItemSocialSchema.getModifyDate());
		this.ModifyTime = aLLFeeItemSocialSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SocialFeeDetailNo") == null )
				this.SocialFeeDetailNo = null;
			else
				this.SocialFeeDetailNo = rs.getString("SocialFeeDetailNo").trim();

			if( rs.getString("SocialFeeNo") == null )
				this.SocialFeeNo = null;
			else
				this.SocialFeeNo = rs.getString("SocialFeeNo").trim();

			if( rs.getString("SocialCaseNo") == null )
				this.SocialCaseNo = null;
			else
				this.SocialCaseNo = rs.getString("SocialCaseNo").trim();

			if( rs.getString("SocialRgtNo") == null )
				this.SocialRgtNo = null;
			else
				this.SocialRgtNo = rs.getString("SocialRgtNo").trim();

			if( rs.getString("TransacTionNo") == null )
				this.TransacTionNo = null;
			else
				this.TransacTionNo = rs.getString("TransacTionNo").trim();

			if( rs.getString("FeeItemCode") == null )
				this.FeeItemCode = null;
			else
				this.FeeItemCode = rs.getString("FeeItemCode").trim();

			this.Fee = rs.getDouble("Fee");
			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLFeeItemSocial表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLFeeItemSocialSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLFeeItemSocialSchema getSchema()
	{
		LLFeeItemSocialSchema aLLFeeItemSocialSchema = new LLFeeItemSocialSchema();
		aLLFeeItemSocialSchema.setSchema(this);
		return aLLFeeItemSocialSchema;
	}

	public LLFeeItemSocialDB getDB()
	{
		LLFeeItemSocialDB aDBOper = new LLFeeItemSocialDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLFeeItemSocial描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SocialFeeDetailNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialCaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialRgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransacTionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeItemCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Fee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLFeeItemSocial>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SocialFeeDetailNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SocialFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SocialCaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SocialRgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TransacTionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			FeeItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Fee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLFeeItemSocialSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SocialFeeDetailNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialFeeDetailNo));
		}
		if (FCode.equals("SocialFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialFeeNo));
		}
		if (FCode.equals("SocialCaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCaseNo));
		}
		if (FCode.equals("SocialRgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialRgtNo));
		}
		if (FCode.equals("TransacTionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransacTionNo));
		}
		if (FCode.equals("FeeItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeItemCode));
		}
		if (FCode.equals("Fee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fee));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SocialFeeDetailNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SocialFeeNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SocialCaseNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SocialRgtNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TransacTionNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(FeeItemCode);
				break;
			case 6:
				strFieldValue = String.valueOf(Fee);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SocialFeeDetailNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialFeeDetailNo = FValue.trim();
			}
			else
				SocialFeeDetailNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialFeeNo = FValue.trim();
			}
			else
				SocialFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialCaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCaseNo = FValue.trim();
			}
			else
				SocialCaseNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialRgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialRgtNo = FValue.trim();
			}
			else
				SocialRgtNo = null;
		}
		if (FCode.equalsIgnoreCase("TransacTionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransacTionNo = FValue.trim();
			}
			else
				TransacTionNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeItemCode = FValue.trim();
			}
			else
				FeeItemCode = null;
		}
		if (FCode.equalsIgnoreCase("Fee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Fee = d;
			}
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLFeeItemSocialSchema other = (LLFeeItemSocialSchema)otherObject;
		return
			(SocialFeeDetailNo == null ? other.getSocialFeeDetailNo() == null : SocialFeeDetailNo.equals(other.getSocialFeeDetailNo()))
			&& (SocialFeeNo == null ? other.getSocialFeeNo() == null : SocialFeeNo.equals(other.getSocialFeeNo()))
			&& (SocialCaseNo == null ? other.getSocialCaseNo() == null : SocialCaseNo.equals(other.getSocialCaseNo()))
			&& (SocialRgtNo == null ? other.getSocialRgtNo() == null : SocialRgtNo.equals(other.getSocialRgtNo()))
			&& (TransacTionNo == null ? other.getTransacTionNo() == null : TransacTionNo.equals(other.getTransacTionNo()))
			&& (FeeItemCode == null ? other.getFeeItemCode() == null : FeeItemCode.equals(other.getFeeItemCode()))
			&& Fee == other.getFee()
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SocialFeeDetailNo") ) {
			return 0;
		}
		if( strFieldName.equals("SocialFeeNo") ) {
			return 1;
		}
		if( strFieldName.equals("SocialCaseNo") ) {
			return 2;
		}
		if( strFieldName.equals("SocialRgtNo") ) {
			return 3;
		}
		if( strFieldName.equals("TransacTionNo") ) {
			return 4;
		}
		if( strFieldName.equals("FeeItemCode") ) {
			return 5;
		}
		if( strFieldName.equals("Fee") ) {
			return 6;
		}
		if( strFieldName.equals("MngCom") ) {
			return 7;
		}
		if( strFieldName.equals("Operator") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SocialFeeDetailNo";
				break;
			case 1:
				strFieldName = "SocialFeeNo";
				break;
			case 2:
				strFieldName = "SocialCaseNo";
				break;
			case 3:
				strFieldName = "SocialRgtNo";
				break;
			case 4:
				strFieldName = "TransacTionNo";
				break;
			case 5:
				strFieldName = "FeeItemCode";
				break;
			case 6:
				strFieldName = "Fee";
				break;
			case 7:
				strFieldName = "MngCom";
				break;
			case 8:
				strFieldName = "Operator";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SocialFeeDetailNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialRgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransacTionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeItemCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
