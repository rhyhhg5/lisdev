/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.lknorealtimerecordingDB;

/*
 * <p>ClassName: lknorealtimerecordingSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2019-06-26
 */
public class lknorealtimerecordingSchema implements Schema, Cloneable
{
	// @Field
	/** 保单流水号 */
	private String transno;
	/** 投保单号 */
	private String prtno;
	/** 地区编码 */
	private String zoneno;
	/** 网点编码 */
	private String banknode;
	/** 分行代码 */
	private String branchcode;
	/** 支行代码 */
	private String branchcode2;
	/** 客户号 */
	private String appntno;
	/** 投保人账号 */
	private String bankaccno;
	/** 投保人姓名 */
	private String appntname;
	/** 投保人证件类型 */
	private String appntidtype;
	/** 投保人证件号码 */
	private String appntidno;
	/** 公司代码 */
	private String compaycode;
	/** 被保人姓名 */
	private String insuredno;
	/** 被保人证件类型 */
	private String insuredidtype;
	/** 被保人证件号码 */
	private String insuredidno;
	/** 缴费方式 */
	private String paymode;
	/** 缴费年期类型 */
	private String payendyearflag;
	/** 缴费年期 */
	private int payendyear;
	/** 保障年期类型 */
	private String insuyearflag;
	/** 保障年期 */
	private int insuyear;
	/** 主险代码 */
	private String mainriskcode;
	/** 首期保费 */
	private double prem;
	/** 客户经理工号 */
	private String accmanagerno;
	/** 状态 */
	private String status;
	/** 创建日期 */
	private Date makedate;
	/** 创建时间 */
	private String maketime;
	/** 最后一次修改日期 */
	private Date modifydate;
	/** 最后一次修改时间 */
	private String modifytime;
	/** 备用字段1 */
	private String bak1;
	/** 备用字段2 */
	private String bak2;
	/** 备用字段3 */
	private String bak3;
	/** 备用字段4 */
	private String bak4;

	public static final int FIELDNUM = 32;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public lknorealtimerecordingSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "transno";
		pk[1] = "prtno";

		PK = pk;
	}

	/**
	 * Schema克隆
	 * @return Object
	 * @throws CloneNotSupportedException
	 */
	public Object clone()
			throws CloneNotSupportedException
	{
		lknorealtimerecordingSchema cloned = (lknorealtimerecordingSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String gettransno()
	{
		return transno;
	}
	public void settransno(String atransno)
	{
		transno = atransno;
	}
	public String getprtno()
	{
		return prtno;
	}
	public void setprtno(String aprtno)
	{
		prtno = aprtno;
	}
	public String getzoneno()
	{
		return zoneno;
	}
	public void setzoneno(String azoneno)
	{
		zoneno = azoneno;
	}
	public String getbanknode()
	{
		return banknode;
	}
	public void setbanknode(String abanknode)
	{
		banknode = abanknode;
	}
	public String getbranchcode()
	{
		return branchcode;
	}
	public void setbranchcode(String abranchcode)
	{
		branchcode = abranchcode;
	}
	public String getbranchcode2()
	{
		return branchcode2;
	}
	public void setbranchcode2(String abranchcode2)
	{
		branchcode2 = abranchcode2;
	}
	public String getappntno()
	{
		return appntno;
	}
	public void setappntno(String aappntno)
	{
		appntno = aappntno;
	}
	public String getbankaccno()
	{
		return bankaccno;
	}
	public void setbankaccno(String abankaccno)
	{
		bankaccno = abankaccno;
	}
	public String getappntname()
	{
		return appntname;
	}
	public void setappntname(String aappntname)
	{
		appntname = aappntname;
	}
	public String getappntidtype()
	{
		return appntidtype;
	}
	public void setappntidtype(String aappntidtype)
	{
		appntidtype = aappntidtype;
	}
	public String getappntidno()
	{
		return appntidno;
	}
	public void setappntidno(String aappntidno)
	{
		appntidno = aappntidno;
	}
	public String getcompaycode()
	{
		return compaycode;
	}
	public void setcompaycode(String acompaycode)
	{
		compaycode = acompaycode;
	}
	public String getinsuredno()
	{
		return insuredno;
	}
	public void setinsuredno(String ainsuredno)
	{
		insuredno = ainsuredno;
	}
	public String getinsuredidtype()
	{
		return insuredidtype;
	}
	public void setinsuredidtype(String ainsuredidtype)
	{
		insuredidtype = ainsuredidtype;
	}
	public String getinsuredidno()
	{
		return insuredidno;
	}
	public void setinsuredidno(String ainsuredidno)
	{
		insuredidno = ainsuredidno;
	}
	public String getpaymode()
	{
		return paymode;
	}
	public void setpaymode(String apaymode)
	{
		paymode = apaymode;
	}
	public String getpayendyearflag()
	{
		return payendyearflag;
	}
	public void setpayendyearflag(String apayendyearflag)
	{
		payendyearflag = apayendyearflag;
	}
	public int getpayendyear()
	{
		return payendyear;
	}
	public void setpayendyear(int apayendyear)
	{
		payendyear = apayendyear;
	}
	public void setpayendyear(String apayendyear)
	{
		if (apayendyear != null && !apayendyear.equals(""))
		{
			Integer tInteger = new Integer(apayendyear);
			int i = tInteger.intValue();
			payendyear = i;
		}
	}

	public String getinsuyearflag()
	{
		return insuyearflag;
	}
	public void setinsuyearflag(String ainsuyearflag)
	{
		insuyearflag = ainsuyearflag;
	}
	public int getinsuyear()
	{
		return insuyear;
	}
	public void setinsuyear(int ainsuyear)
	{
		insuyear = ainsuyear;
	}
	public void setinsuyear(String ainsuyear)
	{
		if (ainsuyear != null && !ainsuyear.equals(""))
		{
			Integer tInteger = new Integer(ainsuyear);
			int i = tInteger.intValue();
			insuyear = i;
		}
	}

	public String getmainriskcode()
	{
		return mainriskcode;
	}
	public void setmainriskcode(String amainriskcode)
	{
		mainriskcode = amainriskcode;
	}
	public double getprem()
	{
		return prem;
	}
	public void setprem(double aprem)
	{
		prem = Arith.round(aprem,2);
	}
	public void setprem(String aprem)
	{
		if (aprem != null && !aprem.equals(""))
		{
			Double tDouble = new Double(aprem);
			double d = tDouble.doubleValue();
			prem = Arith.round(d,2);
		}
	}

	public String getaccmanagerno()
	{
		return accmanagerno;
	}
	public void setaccmanagerno(String aaccmanagerno)
	{
		accmanagerno = aaccmanagerno;
	}
	public String getstatus()
	{
		return status;
	}
	public void setstatus(String astatus)
	{
		status = astatus;
	}
	public String getmakedate()
	{
		if( makedate != null )
			return fDate.getString(makedate);
		else
			return null;
	}
	public void setmakedate(Date amakedate)
	{
		makedate = amakedate;
	}
	public void setmakedate(String amakedate)
	{
		if (amakedate != null && !amakedate.equals("") )
		{
			makedate = fDate.getDate( amakedate );
		}
		else
			makedate = null;
	}

	public String getmaketime()
	{
		return maketime;
	}
	public void setmaketime(String amaketime)
	{
		maketime = amaketime;
	}
	public String getmodifydate()
	{
		if( modifydate != null )
			return fDate.getString(modifydate);
		else
			return null;
	}
	public void setmodifydate(Date amodifydate)
	{
		modifydate = amodifydate;
	}
	public void setmodifydate(String amodifydate)
	{
		if (amodifydate != null && !amodifydate.equals("") )
		{
			modifydate = fDate.getDate( amodifydate );
		}
		else
			modifydate = null;
	}

	public String getmodifytime()
	{
		return modifytime;
	}
	public void setmodifytime(String amodifytime)
	{
		modifytime = amodifytime;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}
	public String getbak4()
	{
		return bak4;
	}
	public void setbak4(String abak4)
	{
		bak4 = abak4;
	}

	/**
	 * 使用另外一个 lknorealtimerecordingSchema 对象给 Schema 赋值
	 * @param: alknorealtimerecordingSchema lknorealtimerecordingSchema
	 **/
	public void setSchema(lknorealtimerecordingSchema alknorealtimerecordingSchema)
	{
		this.transno = alknorealtimerecordingSchema.gettransno();
		this.prtno = alknorealtimerecordingSchema.getprtno();
		this.zoneno = alknorealtimerecordingSchema.getzoneno();
		this.banknode = alknorealtimerecordingSchema.getbanknode();
		this.branchcode = alknorealtimerecordingSchema.getbranchcode();
		this.branchcode2 = alknorealtimerecordingSchema.getbranchcode2();
		this.appntno = alknorealtimerecordingSchema.getappntno();
		this.bankaccno = alknorealtimerecordingSchema.getbankaccno();
		this.appntname = alknorealtimerecordingSchema.getappntname();
		this.appntidtype = alknorealtimerecordingSchema.getappntidtype();
		this.appntidno = alknorealtimerecordingSchema.getappntidno();
		this.compaycode = alknorealtimerecordingSchema.getcompaycode();
		this.insuredno = alknorealtimerecordingSchema.getinsuredno();
		this.insuredidtype = alknorealtimerecordingSchema.getinsuredidtype();
		this.insuredidno = alknorealtimerecordingSchema.getinsuredidno();
		this.paymode = alknorealtimerecordingSchema.getpaymode();
		this.payendyearflag = alknorealtimerecordingSchema.getpayendyearflag();
		this.payendyear = alknorealtimerecordingSchema.getpayendyear();
		this.insuyearflag = alknorealtimerecordingSchema.getinsuyearflag();
		this.insuyear = alknorealtimerecordingSchema.getinsuyear();
		this.mainriskcode = alknorealtimerecordingSchema.getmainriskcode();
		this.prem = alknorealtimerecordingSchema.getprem();
		this.accmanagerno = alknorealtimerecordingSchema.getaccmanagerno();
		this.status = alknorealtimerecordingSchema.getstatus();
		this.makedate = fDate.getDate( alknorealtimerecordingSchema.getmakedate());
		this.maketime = alknorealtimerecordingSchema.getmaketime();
		this.modifydate = fDate.getDate( alknorealtimerecordingSchema.getmodifydate());
		this.modifytime = alknorealtimerecordingSchema.getmodifytime();
		this.bak1 = alknorealtimerecordingSchema.getbak1();
		this.bak2 = alknorealtimerecordingSchema.getbak2();
		this.bak3 = alknorealtimerecordingSchema.getbak3();
		this.bak4 = alknorealtimerecordingSchema.getbak4();
	}

	/**
	 * 使用 ResultSet 中的第 i 行给 Schema 赋值
	 * @param: rs ResultSet
	 * @param: i int
	 * @return: boolean
	 **/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("transno") == null )
				this.transno = null;
			else
				this.transno = rs.getString("transno").trim();

			if( rs.getString("prtno") == null )
				this.prtno = null;
			else
				this.prtno = rs.getString("prtno").trim();

			if( rs.getString("zoneno") == null )
				this.zoneno = null;
			else
				this.zoneno = rs.getString("zoneno").trim();

			if( rs.getString("banknode") == null )
				this.banknode = null;
			else
				this.banknode = rs.getString("banknode").trim();

			if( rs.getString("branchcode") == null )
				this.branchcode = null;
			else
				this.branchcode = rs.getString("branchcode").trim();

			if( rs.getString("branchcode2") == null )
				this.branchcode2 = null;
			else
				this.branchcode2 = rs.getString("branchcode2").trim();

			if( rs.getString("appntno") == null )
				this.appntno = null;
			else
				this.appntno = rs.getString("appntno").trim();

			if( rs.getString("bankaccno") == null )
				this.bankaccno = null;
			else
				this.bankaccno = rs.getString("bankaccno").trim();

			if( rs.getString("appntname") == null )
				this.appntname = null;
			else
				this.appntname = rs.getString("appntname").trim();

			if( rs.getString("appntidtype") == null )
				this.appntidtype = null;
			else
				this.appntidtype = rs.getString("appntidtype").trim();

			if( rs.getString("appntidno") == null )
				this.appntidno = null;
			else
				this.appntidno = rs.getString("appntidno").trim();

			if( rs.getString("compaycode") == null )
				this.compaycode = null;
			else
				this.compaycode = rs.getString("compaycode").trim();

			if( rs.getString("insuredno") == null )
				this.insuredno = null;
			else
				this.insuredno = rs.getString("insuredno").trim();

			if( rs.getString("insuredidtype") == null )
				this.insuredidtype = null;
			else
				this.insuredidtype = rs.getString("insuredidtype").trim();

			if( rs.getString("insuredidno") == null )
				this.insuredidno = null;
			else
				this.insuredidno = rs.getString("insuredidno").trim();

			if( rs.getString("paymode") == null )
				this.paymode = null;
			else
				this.paymode = rs.getString("paymode").trim();

			if( rs.getString("payendyearflag") == null )
				this.payendyearflag = null;
			else
				this.payendyearflag = rs.getString("payendyearflag").trim();

			this.payendyear = rs.getInt("payendyear");
			if( rs.getString("insuyearflag") == null )
				this.insuyearflag = null;
			else
				this.insuyearflag = rs.getString("insuyearflag").trim();

			this.insuyear = rs.getInt("insuyear");
			if( rs.getString("mainriskcode") == null )
				this.mainriskcode = null;
			else
				this.mainriskcode = rs.getString("mainriskcode").trim();

			this.prem = rs.getDouble("prem");
			if( rs.getString("accmanagerno") == null )
				this.accmanagerno = null;
			else
				this.accmanagerno = rs.getString("accmanagerno").trim();

			if( rs.getString("status") == null )
				this.status = null;
			else
				this.status = rs.getString("status").trim();

			this.makedate = rs.getDate("makedate");
			if( rs.getString("maketime") == null )
				this.maketime = null;
			else
				this.maketime = rs.getString("maketime").trim();

			this.modifydate = rs.getDate("modifydate");
			if( rs.getString("modifytime") == null )
				this.modifytime = null;
			else
				this.modifytime = rs.getString("modifytime").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

			if( rs.getString("bak4") == null )
				this.bak4 = null;
			else
				this.bak4 = rs.getString("bak4").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的lknorealtimerecording表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "lknorealtimerecordingSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public lknorealtimerecordingSchema getSchema()
	{
		lknorealtimerecordingSchema alknorealtimerecordingSchema = new lknorealtimerecordingSchema();
		alknorealtimerecordingSchema.setSchema(this);
		return alknorealtimerecordingSchema;
	}

	public lknorealtimerecordingDB getDB()
	{
		lknorealtimerecordingDB aDBOper = new lknorealtimerecordingDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	 * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prplknorealtimerecording描述/A>表字段
	 * @return: String 返回打包后字符串
	 **/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(transno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(prtno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(zoneno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(banknode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(branchcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(branchcode2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(appntno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankaccno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(appntname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(appntidtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(appntidno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(compaycode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuredno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuredidtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuredidno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paymode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(payendyearflag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(payendyear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuyearflag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(insuyear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(mainriskcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(accmanagerno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifytime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak4));
		return strReturn.toString();
	}

	/**
	 * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prplknorealtimerecording>历史记账凭证主表信息</A>表字段
	 * @param: strMessage String 包含一条纪录数据的字符串
	 * @return: boolean
	 **/
	public boolean decode(String strMessage)
	{
		try
		{
			transno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			prtno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			zoneno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			banknode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			branchcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			branchcode2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			appntno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			bankaccno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			appntname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			appntidtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			appntidno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			compaycode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			insuredno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			insuredidtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			insuredidno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			paymode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			payendyearflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			payendyear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).intValue();
			insuyearflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			insuyear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).intValue();
			mainriskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			accmanagerno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "lknorealtimerecordingSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	 * 取得对应传入参数的String形式的字段值
	 * @param: FCode String 希望取得的字段名
	 * @return: String
	 * 如果没有对应的字段，返回""
	 * 如果字段值为空，返回"null"
	 **/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("transno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(transno));
		}
		if (FCode.equals("prtno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prtno));
		}
		if (FCode.equals("zoneno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(zoneno));
		}
		if (FCode.equals("banknode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(banknode));
		}
		if (FCode.equals("branchcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(branchcode));
		}
		if (FCode.equals("branchcode2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(branchcode2));
		}
		if (FCode.equals("appntno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(appntno));
		}
		if (FCode.equals("bankaccno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankaccno));
		}
		if (FCode.equals("appntname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(appntname));
		}
		if (FCode.equals("appntidtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(appntidtype));
		}
		if (FCode.equals("appntidno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(appntidno));
		}
		if (FCode.equals("compaycode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(compaycode));
		}
		if (FCode.equals("insuredno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuredno));
		}
		if (FCode.equals("insuredidtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuredidtype));
		}
		if (FCode.equals("insuredidno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuredidno));
		}
		if (FCode.equals("paymode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paymode));
		}
		if (FCode.equals("payendyearflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(payendyearflag));
		}
		if (FCode.equals("payendyear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(payendyear));
		}
		if (FCode.equals("insuyearflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuyearflag));
		}
		if (FCode.equals("insuyear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuyear));
		}
		if (FCode.equals("mainriskcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(mainriskcode));
		}
		if (FCode.equals("prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prem));
		}
		if (FCode.equals("accmanagerno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(accmanagerno));
		}
		if (FCode.equals("status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(status));
		}
		if (FCode.equals("makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakedate()));
		}
		if (FCode.equals("maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(maketime));
		}
		if (FCode.equals("modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmodifydate()));
		}
		if (FCode.equals("modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifytime));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (FCode.equals("bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	 * 取得Schema中指定索引值所对应的字段值
	 * @param: nFieldIndex int 指定的字段索引值
	 * @return: String
	 * 如果没有对应的字段，返回""
	 * 如果字段值为空，返回"null"
	 **/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(transno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(prtno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(zoneno);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(banknode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(branchcode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(branchcode2);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(appntno);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(bankaccno);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(appntname);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(appntidtype);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(appntidno);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(compaycode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(insuredno);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(insuredidtype);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(insuredidno);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(paymode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(payendyearflag);
				break;
			case 17:
				strFieldValue = String.valueOf(payendyear);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(insuyearflag);
				break;
			case 19:
				strFieldValue = String.valueOf(insuyear);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(mainriskcode);
				break;
			case 21:
				strFieldValue = String.valueOf(prem);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(accmanagerno);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(status);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakedate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(maketime);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmodifydate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(modifytime);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(bak4);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	 * 设置对应传入参数的String形式的字段值
	 * @param: FCode String 需要赋值的对象
	 * @param: FValue String 要赋的值
	 * @return: boolean
	 **/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("transno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				transno = FValue.trim();
			}
			else
				transno = null;
		}
		if (FCode.equalsIgnoreCase("prtno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				prtno = FValue.trim();
			}
			else
				prtno = null;
		}
		if (FCode.equalsIgnoreCase("zoneno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				zoneno = FValue.trim();
			}
			else
				zoneno = null;
		}
		if (FCode.equalsIgnoreCase("banknode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				banknode = FValue.trim();
			}
			else
				banknode = null;
		}
		if (FCode.equalsIgnoreCase("branchcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				branchcode = FValue.trim();
			}
			else
				branchcode = null;
		}
		if (FCode.equalsIgnoreCase("branchcode2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				branchcode2 = FValue.trim();
			}
			else
				branchcode2 = null;
		}
		if (FCode.equalsIgnoreCase("appntno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				appntno = FValue.trim();
			}
			else
				appntno = null;
		}
		if (FCode.equalsIgnoreCase("bankaccno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankaccno = FValue.trim();
			}
			else
				bankaccno = null;
		}
		if (FCode.equalsIgnoreCase("appntname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				appntname = FValue.trim();
			}
			else
				appntname = null;
		}
		if (FCode.equalsIgnoreCase("appntidtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				appntidtype = FValue.trim();
			}
			else
				appntidtype = null;
		}
		if (FCode.equalsIgnoreCase("appntidno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				appntidno = FValue.trim();
			}
			else
				appntidno = null;
		}
		if (FCode.equalsIgnoreCase("compaycode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				compaycode = FValue.trim();
			}
			else
				compaycode = null;
		}
		if (FCode.equalsIgnoreCase("insuredno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuredno = FValue.trim();
			}
			else
				insuredno = null;
		}
		if (FCode.equalsIgnoreCase("insuredidtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuredidtype = FValue.trim();
			}
			else
				insuredidtype = null;
		}
		if (FCode.equalsIgnoreCase("insuredidno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuredidno = FValue.trim();
			}
			else
				insuredidno = null;
		}
		if (FCode.equalsIgnoreCase("paymode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paymode = FValue.trim();
			}
			else
				paymode = null;
		}
		if (FCode.equalsIgnoreCase("payendyearflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				payendyearflag = FValue.trim();
			}
			else
				payendyearflag = null;
		}
		if (FCode.equalsIgnoreCase("payendyear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				payendyear = i;
			}
		}
		if (FCode.equalsIgnoreCase("insuyearflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuyearflag = FValue.trim();
			}
			else
				insuyearflag = null;
		}
		if (FCode.equalsIgnoreCase("insuyear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				insuyear = i;
			}
		}
		if (FCode.equalsIgnoreCase("mainriskcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				mainriskcode = FValue.trim();
			}
			else
				mainriskcode = null;
		}
		if (FCode.equalsIgnoreCase("prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("accmanagerno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				accmanagerno = FValue.trim();
			}
			else
				accmanagerno = null;
		}
		if (FCode.equalsIgnoreCase("status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				status = FValue.trim();
			}
			else
				status = null;
		}
		if (FCode.equalsIgnoreCase("makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makedate = fDate.getDate( FValue );
			}
			else
				makedate = null;
		}
		if (FCode.equalsIgnoreCase("maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				maketime = FValue.trim();
			}
			else
				maketime = null;
		}
		if (FCode.equalsIgnoreCase("modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				modifydate = fDate.getDate( FValue );
			}
			else
				modifydate = null;
		}
		if (FCode.equalsIgnoreCase("modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifytime = FValue.trim();
			}
			else
				modifytime = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		if (FCode.equalsIgnoreCase("bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak4 = FValue.trim();
			}
			else
				bak4 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		lknorealtimerecordingSchema other = (lknorealtimerecordingSchema)otherObject;
		return
				(transno == null ? other.gettransno() == null : transno.equals(other.gettransno()))
						&& (prtno == null ? other.getprtno() == null : prtno.equals(other.getprtno()))
						&& (zoneno == null ? other.getzoneno() == null : zoneno.equals(other.getzoneno()))
						&& (banknode == null ? other.getbanknode() == null : banknode.equals(other.getbanknode()))
						&& (branchcode == null ? other.getbranchcode() == null : branchcode.equals(other.getbranchcode()))
						&& (branchcode2 == null ? other.getbranchcode2() == null : branchcode2.equals(other.getbranchcode2()))
						&& (appntno == null ? other.getappntno() == null : appntno.equals(other.getappntno()))
						&& (bankaccno == null ? other.getbankaccno() == null : bankaccno.equals(other.getbankaccno()))
						&& (appntname == null ? other.getappntname() == null : appntname.equals(other.getappntname()))
						&& (appntidtype == null ? other.getappntidtype() == null : appntidtype.equals(other.getappntidtype()))
						&& (appntidno == null ? other.getappntidno() == null : appntidno.equals(other.getappntidno()))
						&& (compaycode == null ? other.getcompaycode() == null : compaycode.equals(other.getcompaycode()))
						&& (insuredno == null ? other.getinsuredno() == null : insuredno.equals(other.getinsuredno()))
						&& (insuredidtype == null ? other.getinsuredidtype() == null : insuredidtype.equals(other.getinsuredidtype()))
						&& (insuredidno == null ? other.getinsuredidno() == null : insuredidno.equals(other.getinsuredidno()))
						&& (paymode == null ? other.getpaymode() == null : paymode.equals(other.getpaymode()))
						&& (payendyearflag == null ? other.getpayendyearflag() == null : payendyearflag.equals(other.getpayendyearflag()))
						&& payendyear == other.getpayendyear()
						&& (insuyearflag == null ? other.getinsuyearflag() == null : insuyearflag.equals(other.getinsuyearflag()))
						&& insuyear == other.getinsuyear()
						&& (mainriskcode == null ? other.getmainriskcode() == null : mainriskcode.equals(other.getmainriskcode()))
						&& prem == other.getprem()
						&& (accmanagerno == null ? other.getaccmanagerno() == null : accmanagerno.equals(other.getaccmanagerno()))
						&& (status == null ? other.getstatus() == null : status.equals(other.getstatus()))
						&& (makedate == null ? other.getmakedate() == null : fDate.getString(makedate).equals(other.getmakedate()))
						&& (maketime == null ? other.getmaketime() == null : maketime.equals(other.getmaketime()))
						&& (modifydate == null ? other.getmodifydate() == null : fDate.getString(modifydate).equals(other.getmodifydate()))
						&& (modifytime == null ? other.getmodifytime() == null : modifytime.equals(other.getmodifytime()))
						&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
						&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
						&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()))
						&& (bak4 == null ? other.getbak4() == null : bak4.equals(other.getbak4()));
	}

	/**
	 * 取得Schema拥有字段的数量
	 * @return: int
	 **/
	public int getFieldCount()
	{
		return FIELDNUM;
	}

	/**
	 * 取得Schema中指定字段名所对应的索引值
	 * 如果没有对应的字段，返回-1
	 * @param: strFieldName String
	 * @return: int
	 **/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("transno") ) {
			return 0;
		}
		if( strFieldName.equals("prtno") ) {
			return 1;
		}
		if( strFieldName.equals("zoneno") ) {
			return 2;
		}
		if( strFieldName.equals("banknode") ) {
			return 3;
		}
		if( strFieldName.equals("branchcode") ) {
			return 4;
		}
		if( strFieldName.equals("branchcode2") ) {
			return 5;
		}
		if( strFieldName.equals("appntno") ) {
			return 6;
		}
		if( strFieldName.equals("bankaccno") ) {
			return 7;
		}
		if( strFieldName.equals("appntname") ) {
			return 8;
		}
		if( strFieldName.equals("appntidtype") ) {
			return 9;
		}
		if( strFieldName.equals("appntidno") ) {
			return 10;
		}
		if( strFieldName.equals("compaycode") ) {
			return 11;
		}
		if( strFieldName.equals("insuredno") ) {
			return 12;
		}
		if( strFieldName.equals("insuredidtype") ) {
			return 13;
		}
		if( strFieldName.equals("insuredidno") ) {
			return 14;
		}
		if( strFieldName.equals("paymode") ) {
			return 15;
		}
		if( strFieldName.equals("payendyearflag") ) {
			return 16;
		}
		if( strFieldName.equals("payendyear") ) {
			return 17;
		}
		if( strFieldName.equals("insuyearflag") ) {
			return 18;
		}
		if( strFieldName.equals("insuyear") ) {
			return 19;
		}
		if( strFieldName.equals("mainriskcode") ) {
			return 20;
		}
		if( strFieldName.equals("prem") ) {
			return 21;
		}
		if( strFieldName.equals("accmanagerno") ) {
			return 22;
		}
		if( strFieldName.equals("status") ) {
			return 23;
		}
		if( strFieldName.equals("makedate") ) {
			return 24;
		}
		if( strFieldName.equals("maketime") ) {
			return 25;
		}
		if( strFieldName.equals("modifydate") ) {
			return 26;
		}
		if( strFieldName.equals("modifytime") ) {
			return 27;
		}
		if( strFieldName.equals("bak1") ) {
			return 28;
		}
		if( strFieldName.equals("bak2") ) {
			return 29;
		}
		if( strFieldName.equals("bak3") ) {
			return 30;
		}
		if( strFieldName.equals("bak4") ) {
			return 31;
		}
		return -1;
	}

	/**
	 * 取得Schema中指定索引值所对应的字段名
	 * 如果没有对应的字段，返回""
	 * @param: nFieldIndex int
	 * @return: String
	 **/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "transno";
				break;
			case 1:
				strFieldName = "prtno";
				break;
			case 2:
				strFieldName = "zoneno";
				break;
			case 3:
				strFieldName = "banknode";
				break;
			case 4:
				strFieldName = "branchcode";
				break;
			case 5:
				strFieldName = "branchcode2";
				break;
			case 6:
				strFieldName = "appntno";
				break;
			case 7:
				strFieldName = "bankaccno";
				break;
			case 8:
				strFieldName = "appntname";
				break;
			case 9:
				strFieldName = "appntidtype";
				break;
			case 10:
				strFieldName = "appntidno";
				break;
			case 11:
				strFieldName = "compaycode";
				break;
			case 12:
				strFieldName = "insuredno";
				break;
			case 13:
				strFieldName = "insuredidtype";
				break;
			case 14:
				strFieldName = "insuredidno";
				break;
			case 15:
				strFieldName = "paymode";
				break;
			case 16:
				strFieldName = "payendyearflag";
				break;
			case 17:
				strFieldName = "payendyear";
				break;
			case 18:
				strFieldName = "insuyearflag";
				break;
			case 19:
				strFieldName = "insuyear";
				break;
			case 20:
				strFieldName = "mainriskcode";
				break;
			case 21:
				strFieldName = "prem";
				break;
			case 22:
				strFieldName = "accmanagerno";
				break;
			case 23:
				strFieldName = "status";
				break;
			case 24:
				strFieldName = "makedate";
				break;
			case 25:
				strFieldName = "maketime";
				break;
			case 26:
				strFieldName = "modifydate";
				break;
			case 27:
				strFieldName = "modifytime";
				break;
			case 28:
				strFieldName = "bak1";
				break;
			case 29:
				strFieldName = "bak2";
				break;
			case 30:
				strFieldName = "bak3";
				break;
			case 31:
				strFieldName = "bak4";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	 * 取得Schema中指定字段名所对应的字段类型
	 * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
	 * @param: strFieldName String
	 * @return: int
	 **/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("transno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prtno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("zoneno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("banknode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("branchcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("branchcode2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("appntno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bankaccno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("appntname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("appntidtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("appntidno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("compaycode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuredno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuredidtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuredidno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paymode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("payendyearflag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("payendyear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("insuyearflag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuyear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("mainriskcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("accmanagerno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("modifytime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak4") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	 * 取得Schema中指定索引值所对应的字段类型
	 * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
	 * @param: nFieldIndex int
	 * @return: int
	 **/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_INT;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_INT;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
