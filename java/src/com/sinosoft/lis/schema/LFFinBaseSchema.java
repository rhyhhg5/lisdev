/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LFFinBaseDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LFFinBaseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务基础表
 * @CreateDate：2005-03-08
 */
public class LFFinBaseSchema implements Schema
{
    // @Field
    /** 财务机构编码 */
    private String FinComCode;
    /** 保监会科目编码 */
    private String ItemCode;
    /** 报表日期 */
    private Date ReportDate;
    /** 上级内部科目编码 */
    private String UpItemCode;
    /** 内部机构编码 */
    private String ComCode;
    /** 层级 */
    private int Layer;
    /** 统计值 */
    private double StatValue;
    /** 备注 */
    private String Remark;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 财务时间 */
    private String FinDate;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFFinBaseSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "FinComCode";
        pk[1] = "ItemCode";
        pk[2] = "ReportDate";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getFinComCode()
    {
        if (FinComCode != null && !FinComCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FinComCode = StrTool.unicodeToGBK(FinComCode);
        }
        return FinComCode;
    }

    public void setFinComCode(String aFinComCode)
    {
        FinComCode = aFinComCode;
    }

    public String getItemCode()
    {
        if (ItemCode != null && !ItemCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemCode = StrTool.unicodeToGBK(ItemCode);
        }
        return ItemCode;
    }

    public void setItemCode(String aItemCode)
    {
        ItemCode = aItemCode;
    }

    public String getReportDate()
    {
        if (ReportDate != null)
        {
            return fDate.getString(ReportDate);
        }
        else
        {
            return null;
        }
    }

    public void setReportDate(Date aReportDate)
    {
        ReportDate = aReportDate;
    }

    public void setReportDate(String aReportDate)
    {
        if (aReportDate != null && !aReportDate.equals(""))
        {
            ReportDate = fDate.getDate(aReportDate);
        }
        else
        {
            ReportDate = null;
        }
    }

    public String getUpItemCode()
    {
        if (UpItemCode != null && !UpItemCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UpItemCode = StrTool.unicodeToGBK(UpItemCode);
        }
        return UpItemCode;
    }

    public void setUpItemCode(String aUpItemCode)
    {
        UpItemCode = aUpItemCode;
    }

    public String getComCode()
    {
        if (ComCode != null && !ComCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ComCode = StrTool.unicodeToGBK(ComCode);
        }
        return ComCode;
    }

    public void setComCode(String aComCode)
    {
        ComCode = aComCode;
    }

    public int getLayer()
    {
        return Layer;
    }

    public void setLayer(int aLayer)
    {
        Layer = aLayer;
    }

    public void setLayer(String aLayer)
    {
        if (aLayer != null && !aLayer.equals(""))
        {
            Integer tInteger = new Integer(aLayer);
            int i = tInteger.intValue();
            Layer = i;
        }
    }

    public double getStatValue()
    {
        return StatValue;
    }

    public void setStatValue(double aStatValue)
    {
        StatValue = aStatValue;
    }

    public void setStatValue(String aStatValue)
    {
        if (aStatValue != null && !aStatValue.equals(""))
        {
            Double tDouble = new Double(aStatValue);
            double d = tDouble.doubleValue();
            StatValue = d;
        }
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getFinDate()
    {
        if (FinDate != null && !FinDate.equals("") && SysConst.CHANGECHARSET == true)
        {
            FinDate = StrTool.unicodeToGBK(FinDate);
        }
        return FinDate;
    }

    public void setFinDate(String aFinDate)
    {
        FinDate = aFinDate;
    }

    /**
     * 使用另外一个 LFFinBaseSchema 对象给 Schema 赋值
     * @param: aLFFinBaseSchema LFFinBaseSchema
     **/
    public void setSchema(LFFinBaseSchema aLFFinBaseSchema)
    {
        this.FinComCode = aLFFinBaseSchema.getFinComCode();
        this.ItemCode = aLFFinBaseSchema.getItemCode();
        this.ReportDate = fDate.getDate(aLFFinBaseSchema.getReportDate());
        this.UpItemCode = aLFFinBaseSchema.getUpItemCode();
        this.ComCode = aLFFinBaseSchema.getComCode();
        this.Layer = aLFFinBaseSchema.getLayer();
        this.StatValue = aLFFinBaseSchema.getStatValue();
        this.Remark = aLFFinBaseSchema.getRemark();
        this.MakeDate = fDate.getDate(aLFFinBaseSchema.getMakeDate());
        this.MakeTime = aLFFinBaseSchema.getMakeTime();
        this.FinDate = aLFFinBaseSchema.getFinDate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("FinComCode") == null)
            {
                this.FinComCode = null;
            }
            else
            {
                this.FinComCode = rs.getString("FinComCode").trim();
            }

            if (rs.getString("ItemCode") == null)
            {
                this.ItemCode = null;
            }
            else
            {
                this.ItemCode = rs.getString("ItemCode").trim();
            }

            this.ReportDate = rs.getDate("ReportDate");
            if (rs.getString("UpItemCode") == null)
            {
                this.UpItemCode = null;
            }
            else
            {
                this.UpItemCode = rs.getString("UpItemCode").trim();
            }

            if (rs.getString("ComCode") == null)
            {
                this.ComCode = null;
            }
            else
            {
                this.ComCode = rs.getString("ComCode").trim();
            }

            this.Layer = rs.getInt("Layer");
            this.StatValue = rs.getDouble("StatValue");
            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            if (rs.getString("FinDate") == null)
            {
                this.FinDate = null;
            }
            else
            {
                this.FinDate = rs.getString("FinDate").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFFinBaseSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFFinBaseSchema getSchema()
    {
        LFFinBaseSchema aLFFinBaseSchema = new LFFinBaseSchema();
        aLFFinBaseSchema.setSchema(this);
        return aLFFinBaseSchema;
    }

    public LFFinBaseDB getDB()
    {
        LFFinBaseDB aDBOper = new LFFinBaseDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFFinBase描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(FinComCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ReportDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Layer) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatValue) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FinDate));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFFinBase>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            FinComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            ReportDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            UpItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            Layer = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    6, SysConst.PACKAGESPILTER))).intValue();
            StatValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            FinDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFFinBaseSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("FinComCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FinComCode));
        }
        if (FCode.equals("ItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemCode));
        }
        if (FCode.equals("ReportDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getReportDate()));
        }
        if (FCode.equals("UpItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpItemCode));
        }
        if (FCode.equals("ComCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCode));
        }
        if (FCode.equals("Layer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Layer));
        }
        if (FCode.equals("StatValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatValue));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("FinDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FinDate));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(FinComCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ItemCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReportDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(UpItemCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 5:
                strFieldValue = String.valueOf(Layer);
                break;
            case 6:
                strFieldValue = String.valueOf(StatValue);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(FinDate);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("FinComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FinComCode = FValue.trim();
            }
            else
            {
                FinComCode = null;
            }
        }
        if (FCode.equals("ItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemCode = FValue.trim();
            }
            else
            {
                ItemCode = null;
            }
        }
        if (FCode.equals("ReportDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReportDate = fDate.getDate(FValue);
            }
            else
            {
                ReportDate = null;
            }
        }
        if (FCode.equals("UpItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpItemCode = FValue.trim();
            }
            else
            {
                UpItemCode = null;
            }
        }
        if (FCode.equals("ComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
            {
                ComCode = null;
            }
        }
        if (FCode.equals("Layer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Layer = i;
            }
        }
        if (FCode.equals("StatValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StatValue = d;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("FinDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FinDate = FValue.trim();
            }
            else
            {
                FinDate = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFFinBaseSchema other = (LFFinBaseSchema) otherObject;
        return
                FinComCode.equals(other.getFinComCode())
                && ItemCode.equals(other.getItemCode())
                && fDate.getString(ReportDate).equals(other.getReportDate())
                && UpItemCode.equals(other.getUpItemCode())
                && ComCode.equals(other.getComCode())
                && Layer == other.getLayer()
                && StatValue == other.getStatValue()
                && Remark.equals(other.getRemark())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && FinDate.equals(other.getFinDate());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("FinComCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return 1;
        }
        if (strFieldName.equals("ReportDate"))
        {
            return 2;
        }
        if (strFieldName.equals("UpItemCode"))
        {
            return 3;
        }
        if (strFieldName.equals("ComCode"))
        {
            return 4;
        }
        if (strFieldName.equals("Layer"))
        {
            return 5;
        }
        if (strFieldName.equals("StatValue"))
        {
            return 6;
        }
        if (strFieldName.equals("Remark"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 9;
        }
        if (strFieldName.equals("FinDate"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "FinComCode";
                break;
            case 1:
                strFieldName = "ItemCode";
                break;
            case 2:
                strFieldName = "ReportDate";
                break;
            case 3:
                strFieldName = "UpItemCode";
                break;
            case 4:
                strFieldName = "ComCode";
                break;
            case 5:
                strFieldName = "Layer";
                break;
            case 6:
                strFieldName = "StatValue";
                break;
            case 7:
                strFieldName = "Remark";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "FinDate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("FinComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReportDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("UpItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Layer"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("StatValue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FinDate"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
