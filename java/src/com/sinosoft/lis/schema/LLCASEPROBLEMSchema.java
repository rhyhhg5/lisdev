/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCASEPROBLEMDB;

/*
 * <p>ClassName: LLCASEPROBLEMSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-07-27
 */
public class LLCASEPROBLEMSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SERIALNO;
	/** 分案号(个人理赔号) */
	private String CASENO;
	/** 案件状态 */
	private String RGTSTATE;
	/** 问题件状态 */
	private String STATE;
	/** 下发原因代码 */
	private String REASONCODE;
	/** 下发备份信息 */
	private String UPREMARK;
	/** 问题件回复信息 */
	private String REPLY;
	/** 机构代码 */
	private String MNGCOM;
	/** 操作员 */
	private String OPERATOR;
	/** 入机日期 */
	private Date MAKEDATE;
	/** 入机时间 */
	private String MAKETIME;
	/** 修改日期 */
	private Date MODIFYDATE;
	/** 修改时间 */
	private String MODIFYTIME;
	/** 备注 */
	private String REMARK;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCASEPROBLEMSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "SERIALNO";
		pk[1] = "CASENO";
		pk[2] = "RGTSTATE";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCASEPROBLEMSchema cloned = (LLCASEPROBLEMSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSERIALNO()
	{
		return SERIALNO;
	}
	public void setSERIALNO(String aSERIALNO)
	{
		SERIALNO = aSERIALNO;
	}
	public String getCASENO()
	{
		return CASENO;
	}
	public void setCASENO(String aCASENO)
	{
		CASENO = aCASENO;
	}
	public String getRGTSTATE()
	{
		return RGTSTATE;
	}
	public void setRGTSTATE(String aRGTSTATE)
	{
		RGTSTATE = aRGTSTATE;
	}
	public String getSTATE()
	{
		return STATE;
	}
	public void setSTATE(String aSTATE)
	{
		STATE = aSTATE;
	}
	public String getREASONCODE()
	{
		return REASONCODE;
	}
	public void setREASONCODE(String aREASONCODE)
	{
		REASONCODE = aREASONCODE;
	}
	public String getUPREMARK()
	{
		return UPREMARK;
	}
	public void setUPREMARK(String aUPREMARK)
	{
		UPREMARK = aUPREMARK;
	}
	public String getREPLY()
	{
		return REPLY;
	}
	public void setREPLY(String aREPLY)
	{
		REPLY = aREPLY;
	}
	public String getMNGCOM()
	{
		return MNGCOM;
	}
	public void setMNGCOM(String aMNGCOM)
	{
		MNGCOM = aMNGCOM;
	}
	public String getOPERATOR()
	{
		return OPERATOR;
	}
	public void setOPERATOR(String aOPERATOR)
	{
		OPERATOR = aOPERATOR;
	}
	public String getMAKEDATE()
	{
		if( MAKEDATE != null )
			return fDate.getString(MAKEDATE);
		else
			return null;
	}
	public void setMAKEDATE(Date aMAKEDATE)
	{
		MAKEDATE = aMAKEDATE;
	}
	public void setMAKEDATE(String aMAKEDATE)
	{
		if (aMAKEDATE != null && !aMAKEDATE.equals("") )
		{
			MAKEDATE = fDate.getDate( aMAKEDATE );
		}
		else
			MAKEDATE = null;
	}

	public String getMAKETIME()
	{
		return MAKETIME;
	}
	public void setMAKETIME(String aMAKETIME)
	{
		MAKETIME = aMAKETIME;
	}
	public String getMODIFYDATE()
	{
		if( MODIFYDATE != null )
			return fDate.getString(MODIFYDATE);
		else
			return null;
	}
	public void setMODIFYDATE(Date aMODIFYDATE)
	{
		MODIFYDATE = aMODIFYDATE;
	}
	public void setMODIFYDATE(String aMODIFYDATE)
	{
		if (aMODIFYDATE != null && !aMODIFYDATE.equals("") )
		{
			MODIFYDATE = fDate.getDate( aMODIFYDATE );
		}
		else
			MODIFYDATE = null;
	}

	public String getMODIFYTIME()
	{
		return MODIFYTIME;
	}
	public void setMODIFYTIME(String aMODIFYTIME)
	{
		MODIFYTIME = aMODIFYTIME;
	}
	public String getREMARK()
	{
		return REMARK;
	}
	public void setREMARK(String aREMARK)
	{
		REMARK = aREMARK;
	}

	/**
	* 使用另外一个 LLCASEPROBLEMSchema 对象给 Schema 赋值
	* @param: aLLCASEPROBLEMSchema LLCASEPROBLEMSchema
	**/
	public void setSchema(LLCASEPROBLEMSchema aLLCASEPROBLEMSchema)
	{
		this.SERIALNO = aLLCASEPROBLEMSchema.getSERIALNO();
		this.CASENO = aLLCASEPROBLEMSchema.getCASENO();
		this.RGTSTATE = aLLCASEPROBLEMSchema.getRGTSTATE();
		this.STATE = aLLCASEPROBLEMSchema.getSTATE();
		this.REASONCODE = aLLCASEPROBLEMSchema.getREASONCODE();
		this.UPREMARK = aLLCASEPROBLEMSchema.getUPREMARK();
		this.REPLY = aLLCASEPROBLEMSchema.getREPLY();
		this.MNGCOM = aLLCASEPROBLEMSchema.getMNGCOM();
		this.OPERATOR = aLLCASEPROBLEMSchema.getOPERATOR();
		this.MAKEDATE = fDate.getDate( aLLCASEPROBLEMSchema.getMAKEDATE());
		this.MAKETIME = aLLCASEPROBLEMSchema.getMAKETIME();
		this.MODIFYDATE = fDate.getDate( aLLCASEPROBLEMSchema.getMODIFYDATE());
		this.MODIFYTIME = aLLCASEPROBLEMSchema.getMODIFYTIME();
		this.REMARK = aLLCASEPROBLEMSchema.getREMARK();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SERIALNO") == null )
				this.SERIALNO = null;
			else
				this.SERIALNO = rs.getString("SERIALNO").trim();

			if( rs.getString("CASENO") == null )
				this.CASENO = null;
			else
				this.CASENO = rs.getString("CASENO").trim();

			if( rs.getString("RGTSTATE") == null )
				this.RGTSTATE = null;
			else
				this.RGTSTATE = rs.getString("RGTSTATE").trim();

			if( rs.getString("STATE") == null )
				this.STATE = null;
			else
				this.STATE = rs.getString("STATE").trim();

			if( rs.getString("REASONCODE") == null )
				this.REASONCODE = null;
			else
				this.REASONCODE = rs.getString("REASONCODE").trim();

			if( rs.getString("UPREMARK") == null )
				this.UPREMARK = null;
			else
				this.UPREMARK = rs.getString("UPREMARK").trim();

			if( rs.getString("REPLY") == null )
				this.REPLY = null;
			else
				this.REPLY = rs.getString("REPLY").trim();

			if( rs.getString("MNGCOM") == null )
				this.MNGCOM = null;
			else
				this.MNGCOM = rs.getString("MNGCOM").trim();

			if( rs.getString("OPERATOR") == null )
				this.OPERATOR = null;
			else
				this.OPERATOR = rs.getString("OPERATOR").trim();

			this.MAKEDATE = rs.getDate("MAKEDATE");
			if( rs.getString("MAKETIME") == null )
				this.MAKETIME = null;
			else
				this.MAKETIME = rs.getString("MAKETIME").trim();

			this.MODIFYDATE = rs.getDate("MODIFYDATE");
			if( rs.getString("MODIFYTIME") == null )
				this.MODIFYTIME = null;
			else
				this.MODIFYTIME = rs.getString("MODIFYTIME").trim();

			if( rs.getString("REMARK") == null )
				this.REMARK = null;
			else
				this.REMARK = rs.getString("REMARK").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCASEPROBLEM表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCASEPROBLEMSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCASEPROBLEMSchema getSchema()
	{
		LLCASEPROBLEMSchema aLLCASEPROBLEMSchema = new LLCASEPROBLEMSchema();
		aLLCASEPROBLEMSchema.setSchema(this);
		return aLLCASEPROBLEMSchema;
	}

	public LLCASEPROBLEMDB getDB()
	{
		LLCASEPROBLEMDB aDBOper = new LLCASEPROBLEMDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCASEPROBLEM描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SERIALNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CASENO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RGTSTATE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(STATE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(REASONCODE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UPREMARK)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(REPLY)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MNGCOM)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OPERATOR)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MAKEDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MAKETIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MODIFYDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MODIFYTIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(REMARK));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCASEPROBLEM>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SERIALNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CASENO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RGTSTATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			STATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			REASONCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			UPREMARK = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			REPLY = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MNGCOM = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			OPERATOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MAKEDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MODIFYDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MODIFYTIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			REMARK = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCASEPROBLEMSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SERIALNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SERIALNO));
		}
		if (FCode.equals("CASENO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CASENO));
		}
		if (FCode.equals("RGTSTATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RGTSTATE));
		}
		if (FCode.equals("STATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(STATE));
		}
		if (FCode.equals("REASONCODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(REASONCODE));
		}
		if (FCode.equals("UPREMARK"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UPREMARK));
		}
		if (FCode.equals("REPLY"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(REPLY));
		}
		if (FCode.equals("MNGCOM"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MNGCOM));
		}
		if (FCode.equals("OPERATOR"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OPERATOR));
		}
		if (FCode.equals("MAKEDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
		}
		if (FCode.equals("MAKETIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
		}
		if (FCode.equals("MODIFYDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
		}
		if (FCode.equals("MODIFYTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
		}
		if (FCode.equals("REMARK"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(REMARK));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SERIALNO);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CASENO);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RGTSTATE);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(STATE);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(REASONCODE);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(UPREMARK);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(REPLY);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MNGCOM);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(OPERATOR);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MAKETIME);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MODIFYTIME);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(REMARK);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SERIALNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SERIALNO = FValue.trim();
			}
			else
				SERIALNO = null;
		}
		if (FCode.equalsIgnoreCase("CASENO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CASENO = FValue.trim();
			}
			else
				CASENO = null;
		}
		if (FCode.equalsIgnoreCase("RGTSTATE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RGTSTATE = FValue.trim();
			}
			else
				RGTSTATE = null;
		}
		if (FCode.equalsIgnoreCase("STATE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				STATE = FValue.trim();
			}
			else
				STATE = null;
		}
		if (FCode.equalsIgnoreCase("REASONCODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				REASONCODE = FValue.trim();
			}
			else
				REASONCODE = null;
		}
		if (FCode.equalsIgnoreCase("UPREMARK"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UPREMARK = FValue.trim();
			}
			else
				UPREMARK = null;
		}
		if (FCode.equalsIgnoreCase("REPLY"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				REPLY = FValue.trim();
			}
			else
				REPLY = null;
		}
		if (FCode.equalsIgnoreCase("MNGCOM"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MNGCOM = FValue.trim();
			}
			else
				MNGCOM = null;
		}
		if (FCode.equalsIgnoreCase("OPERATOR"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OPERATOR = FValue.trim();
			}
			else
				OPERATOR = null;
		}
		if (FCode.equalsIgnoreCase("MAKEDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MAKEDATE = fDate.getDate( FValue );
			}
			else
				MAKEDATE = null;
		}
		if (FCode.equalsIgnoreCase("MAKETIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MAKETIME = FValue.trim();
			}
			else
				MAKETIME = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MODIFYDATE = fDate.getDate( FValue );
			}
			else
				MODIFYDATE = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYTIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MODIFYTIME = FValue.trim();
			}
			else
				MODIFYTIME = null;
		}
		if (FCode.equalsIgnoreCase("REMARK"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				REMARK = FValue.trim();
			}
			else
				REMARK = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCASEPROBLEMSchema other = (LLCASEPROBLEMSchema)otherObject;
		return
			(SERIALNO == null ? other.getSERIALNO() == null : SERIALNO.equals(other.getSERIALNO()))
			&& (CASENO == null ? other.getCASENO() == null : CASENO.equals(other.getCASENO()))
			&& (RGTSTATE == null ? other.getRGTSTATE() == null : RGTSTATE.equals(other.getRGTSTATE()))
			&& (STATE == null ? other.getSTATE() == null : STATE.equals(other.getSTATE()))
			&& (REASONCODE == null ? other.getREASONCODE() == null : REASONCODE.equals(other.getREASONCODE()))
			&& (UPREMARK == null ? other.getUPREMARK() == null : UPREMARK.equals(other.getUPREMARK()))
			&& (REPLY == null ? other.getREPLY() == null : REPLY.equals(other.getREPLY()))
			&& (MNGCOM == null ? other.getMNGCOM() == null : MNGCOM.equals(other.getMNGCOM()))
			&& (OPERATOR == null ? other.getOPERATOR() == null : OPERATOR.equals(other.getOPERATOR()))
			&& (MAKEDATE == null ? other.getMAKEDATE() == null : fDate.getString(MAKEDATE).equals(other.getMAKEDATE()))
			&& (MAKETIME == null ? other.getMAKETIME() == null : MAKETIME.equals(other.getMAKETIME()))
			&& (MODIFYDATE == null ? other.getMODIFYDATE() == null : fDate.getString(MODIFYDATE).equals(other.getMODIFYDATE()))
			&& (MODIFYTIME == null ? other.getMODIFYTIME() == null : MODIFYTIME.equals(other.getMODIFYTIME()))
			&& (REMARK == null ? other.getREMARK() == null : REMARK.equals(other.getREMARK()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SERIALNO") ) {
			return 0;
		}
		if( strFieldName.equals("CASENO") ) {
			return 1;
		}
		if( strFieldName.equals("RGTSTATE") ) {
			return 2;
		}
		if( strFieldName.equals("STATE") ) {
			return 3;
		}
		if( strFieldName.equals("REASONCODE") ) {
			return 4;
		}
		if( strFieldName.equals("UPREMARK") ) {
			return 5;
		}
		if( strFieldName.equals("REPLY") ) {
			return 6;
		}
		if( strFieldName.equals("MNGCOM") ) {
			return 7;
		}
		if( strFieldName.equals("OPERATOR") ) {
			return 8;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return 9;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return 10;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return 11;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return 12;
		}
		if( strFieldName.equals("REMARK") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SERIALNO";
				break;
			case 1:
				strFieldName = "CASENO";
				break;
			case 2:
				strFieldName = "RGTSTATE";
				break;
			case 3:
				strFieldName = "STATE";
				break;
			case 4:
				strFieldName = "REASONCODE";
				break;
			case 5:
				strFieldName = "UPREMARK";
				break;
			case 6:
				strFieldName = "REPLY";
				break;
			case 7:
				strFieldName = "MNGCOM";
				break;
			case 8:
				strFieldName = "OPERATOR";
				break;
			case 9:
				strFieldName = "MAKEDATE";
				break;
			case 10:
				strFieldName = "MAKETIME";
				break;
			case 11:
				strFieldName = "MODIFYDATE";
				break;
			case 12:
				strFieldName = "MODIFYTIME";
				break;
			case 13:
				strFieldName = "REMARK";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SERIALNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CASENO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RGTSTATE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("STATE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("REASONCODE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UPREMARK") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("REPLY") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MNGCOM") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OPERATOR") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("REMARK") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
