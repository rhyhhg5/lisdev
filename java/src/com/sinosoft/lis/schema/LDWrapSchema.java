/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDWrapDB;

/*
 * <p>ClassName: LDWrapSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LDWrap
 * @CreateDate：2010-09-30
 */
public class LDWrapSchema implements Schema, Cloneable
{
	// @Field
	/** 套餐编码 */
	private String RiskWrapCode;
	/** 套餐名称 */
	private String WrapName;
	/** 套餐简称 */
	private String WrapShortName;
	/** 套餐英文名称 */
	private String WrapEnName;
	/** 套餐英文简称 */
	private String WrapEnShortName;
	/** 套餐类型 */
	private String WrapType;
	/** 套餐性质 */
	private String WrapProp;
	/** 套餐所属机构 */
	private String WrapManageCom;
	/** 最大份数 */
	private String MaxCopys;
	/** 最高职业类别 */
	private String MaxOccuType;
	/** 特约 */
	private String Remark;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDWrapSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "RiskWrapCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDWrapSchema cloned = (LDWrapSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRiskWrapCode()
	{
		return RiskWrapCode;
	}
	public void setRiskWrapCode(String aRiskWrapCode)
	{
		RiskWrapCode = aRiskWrapCode;
	}
	public String getWrapName()
	{
		return WrapName;
	}
	public void setWrapName(String aWrapName)
	{
		WrapName = aWrapName;
	}
	public String getWrapShortName()
	{
		return WrapShortName;
	}
	public void setWrapShortName(String aWrapShortName)
	{
		WrapShortName = aWrapShortName;
	}
	public String getWrapEnName()
	{
		return WrapEnName;
	}
	public void setWrapEnName(String aWrapEnName)
	{
		WrapEnName = aWrapEnName;
	}
	public String getWrapEnShortName()
	{
		return WrapEnShortName;
	}
	public void setWrapEnShortName(String aWrapEnShortName)
	{
		WrapEnShortName = aWrapEnShortName;
	}
	public String getWrapType()
	{
		return WrapType;
	}
	public void setWrapType(String aWrapType)
	{
		WrapType = aWrapType;
	}
	public String getWrapProp()
	{
		return WrapProp;
	}
	public void setWrapProp(String aWrapProp)
	{
		WrapProp = aWrapProp;
	}
	public String getWrapManageCom()
	{
		return WrapManageCom;
	}
	public void setWrapManageCom(String aWrapManageCom)
	{
		WrapManageCom = aWrapManageCom;
	}
	public String getMaxCopys()
	{
		return MaxCopys;
	}
	public void setMaxCopys(String aMaxCopys)
	{
		MaxCopys = aMaxCopys;
	}
	public String getMaxOccuType()
	{
		return MaxOccuType;
	}
	public void setMaxOccuType(String aMaxOccuType)
	{
		MaxOccuType = aMaxOccuType;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}

	/**
	* 使用另外一个 LDWrapSchema 对象给 Schema 赋值
	* @param: aLDWrapSchema LDWrapSchema
	**/
	public void setSchema(LDWrapSchema aLDWrapSchema)
	{
		this.RiskWrapCode = aLDWrapSchema.getRiskWrapCode();
		this.WrapName = aLDWrapSchema.getWrapName();
		this.WrapShortName = aLDWrapSchema.getWrapShortName();
		this.WrapEnName = aLDWrapSchema.getWrapEnName();
		this.WrapEnShortName = aLDWrapSchema.getWrapEnShortName();
		this.WrapType = aLDWrapSchema.getWrapType();
		this.WrapProp = aLDWrapSchema.getWrapProp();
		this.WrapManageCom = aLDWrapSchema.getWrapManageCom();
		this.MaxCopys = aLDWrapSchema.getMaxCopys();
		this.MaxOccuType = aLDWrapSchema.getMaxOccuType();
		this.Remark = aLDWrapSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RiskWrapCode") == null )
				this.RiskWrapCode = null;
			else
				this.RiskWrapCode = rs.getString("RiskWrapCode").trim();

			if( rs.getString("WrapName") == null )
				this.WrapName = null;
			else
				this.WrapName = rs.getString("WrapName").trim();

			if( rs.getString("WrapShortName") == null )
				this.WrapShortName = null;
			else
				this.WrapShortName = rs.getString("WrapShortName").trim();

			if( rs.getString("WrapEnName") == null )
				this.WrapEnName = null;
			else
				this.WrapEnName = rs.getString("WrapEnName").trim();

			if( rs.getString("WrapEnShortName") == null )
				this.WrapEnShortName = null;
			else
				this.WrapEnShortName = rs.getString("WrapEnShortName").trim();

			if( rs.getString("WrapType") == null )
				this.WrapType = null;
			else
				this.WrapType = rs.getString("WrapType").trim();

			if( rs.getString("WrapProp") == null )
				this.WrapProp = null;
			else
				this.WrapProp = rs.getString("WrapProp").trim();

			if( rs.getString("WrapManageCom") == null )
				this.WrapManageCom = null;
			else
				this.WrapManageCom = rs.getString("WrapManageCom").trim();

			if( rs.getString("MaxCopys") == null )
				this.MaxCopys = null;
			else
				this.MaxCopys = rs.getString("MaxCopys").trim();

			if( rs.getString("MaxOccuType") == null )
				this.MaxOccuType = null;
			else
				this.MaxOccuType = rs.getString("MaxOccuType").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDWrap表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDWrapSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDWrapSchema getSchema()
	{
		LDWrapSchema aLDWrapSchema = new LDWrapSchema();
		aLDWrapSchema.setSchema(this);
		return aLDWrapSchema;
	}

	public LDWrapDB getDB()
	{
		LDWrapDB aDBOper = new LDWrapDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDWrap描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RiskWrapCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapShortName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapEnName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapEnShortName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapProp)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MaxCopys)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MaxOccuType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDWrap>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RiskWrapCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			WrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			WrapShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			WrapEnName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			WrapEnShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			WrapType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			WrapProp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			WrapManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MaxCopys = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MaxOccuType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDWrapSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RiskWrapCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskWrapCode));
		}
		if (FCode.equals("WrapName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapName));
		}
		if (FCode.equals("WrapShortName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapShortName));
		}
		if (FCode.equals("WrapEnName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapEnName));
		}
		if (FCode.equals("WrapEnShortName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapEnShortName));
		}
		if (FCode.equals("WrapType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapType));
		}
		if (FCode.equals("WrapProp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapProp));
		}
		if (FCode.equals("WrapManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapManageCom));
		}
		if (FCode.equals("MaxCopys"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxCopys));
		}
		if (FCode.equals("MaxOccuType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxOccuType));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RiskWrapCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(WrapName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(WrapShortName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(WrapEnName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(WrapEnShortName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(WrapType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(WrapProp);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(WrapManageCom);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(MaxCopys);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MaxOccuType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RiskWrapCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskWrapCode = FValue.trim();
			}
			else
				RiskWrapCode = null;
		}
		if (FCode.equalsIgnoreCase("WrapName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapName = FValue.trim();
			}
			else
				WrapName = null;
		}
		if (FCode.equalsIgnoreCase("WrapShortName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapShortName = FValue.trim();
			}
			else
				WrapShortName = null;
		}
		if (FCode.equalsIgnoreCase("WrapEnName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapEnName = FValue.trim();
			}
			else
				WrapEnName = null;
		}
		if (FCode.equalsIgnoreCase("WrapEnShortName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapEnShortName = FValue.trim();
			}
			else
				WrapEnShortName = null;
		}
		if (FCode.equalsIgnoreCase("WrapType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapType = FValue.trim();
			}
			else
				WrapType = null;
		}
		if (FCode.equalsIgnoreCase("WrapProp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapProp = FValue.trim();
			}
			else
				WrapProp = null;
		}
		if (FCode.equalsIgnoreCase("WrapManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapManageCom = FValue.trim();
			}
			else
				WrapManageCom = null;
		}
		if (FCode.equalsIgnoreCase("MaxCopys"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaxCopys = FValue.trim();
			}
			else
				MaxCopys = null;
		}
		if (FCode.equalsIgnoreCase("MaxOccuType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaxOccuType = FValue.trim();
			}
			else
				MaxOccuType = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDWrapSchema other = (LDWrapSchema)otherObject;
		return
			(RiskWrapCode == null ? other.getRiskWrapCode() == null : RiskWrapCode.equals(other.getRiskWrapCode()))
			&& (WrapName == null ? other.getWrapName() == null : WrapName.equals(other.getWrapName()))
			&& (WrapShortName == null ? other.getWrapShortName() == null : WrapShortName.equals(other.getWrapShortName()))
			&& (WrapEnName == null ? other.getWrapEnName() == null : WrapEnName.equals(other.getWrapEnName()))
			&& (WrapEnShortName == null ? other.getWrapEnShortName() == null : WrapEnShortName.equals(other.getWrapEnShortName()))
			&& (WrapType == null ? other.getWrapType() == null : WrapType.equals(other.getWrapType()))
			&& (WrapProp == null ? other.getWrapProp() == null : WrapProp.equals(other.getWrapProp()))
			&& (WrapManageCom == null ? other.getWrapManageCom() == null : WrapManageCom.equals(other.getWrapManageCom()))
			&& (MaxCopys == null ? other.getMaxCopys() == null : MaxCopys.equals(other.getMaxCopys()))
			&& (MaxOccuType == null ? other.getMaxOccuType() == null : MaxOccuType.equals(other.getMaxOccuType()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RiskWrapCode") ) {
			return 0;
		}
		if( strFieldName.equals("WrapName") ) {
			return 1;
		}
		if( strFieldName.equals("WrapShortName") ) {
			return 2;
		}
		if( strFieldName.equals("WrapEnName") ) {
			return 3;
		}
		if( strFieldName.equals("WrapEnShortName") ) {
			return 4;
		}
		if( strFieldName.equals("WrapType") ) {
			return 5;
		}
		if( strFieldName.equals("WrapProp") ) {
			return 6;
		}
		if( strFieldName.equals("WrapManageCom") ) {
			return 7;
		}
		if( strFieldName.equals("MaxCopys") ) {
			return 8;
		}
		if( strFieldName.equals("MaxOccuType") ) {
			return 9;
		}
		if( strFieldName.equals("Remark") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RiskWrapCode";
				break;
			case 1:
				strFieldName = "WrapName";
				break;
			case 2:
				strFieldName = "WrapShortName";
				break;
			case 3:
				strFieldName = "WrapEnName";
				break;
			case 4:
				strFieldName = "WrapEnShortName";
				break;
			case 5:
				strFieldName = "WrapType";
				break;
			case 6:
				strFieldName = "WrapProp";
				break;
			case 7:
				strFieldName = "WrapManageCom";
				break;
			case 8:
				strFieldName = "MaxCopys";
				break;
			case 9:
				strFieldName = "MaxOccuType";
				break;
			case 10:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RiskWrapCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapShortName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapEnName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapEnShortName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapProp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MaxCopys") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MaxOccuType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
