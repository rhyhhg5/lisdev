/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.HMSuggestSendTrackDB;

/*
 * <p>ClassName: HMSuggestSendTrackSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-18
 */
public class HMSuggestSendTrackSchema implements Schema, Cloneable
{
	// @Field
	/** 序列号 */
	private String SerialNo;
	/** 业务类型 */
	private String BusinessType;
	/** 业务代码 */
	private String BusinessCode;
	/** 客户号码 */
	private String CustomerNo;
	/** 发送类别 */
	private String SendType;
	/** 客户姓名 */
	private String Name;
	/** 发送状态 */
	private String SendStatus;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 客户地址号码 */
	private String AddressNo;
	/** 手机 */
	private String Mobile;
	/** E_mail */
	private String EMail;
	/** 家庭地址 */
	private String HomeAddress;
	/** 通讯地址 */
	private String PostalAddress;
	/** 通讯邮编 */
	private String ZipCode;
	/** 健康建议 */
	private String Suggest;
	/** 就诊方式 */
	private String VisType;
	/** 最近一次就诊结束时间 */
	private Date LastVisEndDate;
	/** 最近一年就诊次数 */
	private double LastYearVisCount;
	/** 最近一年医疗费用理赔额度 */
	private double LastYearMedRealPay;
	/** 备注 */
	private String Remark;
	/** 备用字段1 */
	private String StandbyField1;
	/** 备用字段2 */
	private String StandbyField2;
	/** 入机时间 */
	private String MakeTime;
	/** 入机日期 */
	private Date MakeDate;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员代码 */
	private String Operator;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 29;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public HMSuggestSendTrackSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		HMSuggestSendTrackSchema cloned = (HMSuggestSendTrackSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getBusinessType()
	{
		return BusinessType;
	}
	public void setBusinessType(String aBusinessType)
	{
		BusinessType = aBusinessType;
	}
	public String getBusinessCode()
	{
		return BusinessCode;
	}
	public void setBusinessCode(String aBusinessCode)
	{
		BusinessCode = aBusinessCode;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getSendType()
	{
		return SendType;
	}
	public void setSendType(String aSendType)
	{
		SendType = aSendType;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getSendStatus()
	{
		return SendStatus;
	}
	public void setSendStatus(String aSendStatus)
	{
		SendStatus = aSendStatus;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getAddressNo()
	{
		return AddressNo;
	}
	public void setAddressNo(String aAddressNo)
	{
		AddressNo = aAddressNo;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getEMail()
	{
		return EMail;
	}
	public void setEMail(String aEMail)
	{
		EMail = aEMail;
	}
	public String getHomeAddress()
	{
		return HomeAddress;
	}
	public void setHomeAddress(String aHomeAddress)
	{
		HomeAddress = aHomeAddress;
	}
	public String getPostalAddress()
	{
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getSuggest()
	{
		return Suggest;
	}
	public void setSuggest(String aSuggest)
	{
		Suggest = aSuggest;
	}
	public String getVisType()
	{
		return VisType;
	}
	public void setVisType(String aVisType)
	{
		VisType = aVisType;
	}
	public String getLastVisEndDate()
	{
		if( LastVisEndDate != null )
			return fDate.getString(LastVisEndDate);
		else
			return null;
	}
	public void setLastVisEndDate(Date aLastVisEndDate)
	{
		LastVisEndDate = aLastVisEndDate;
	}
	public void setLastVisEndDate(String aLastVisEndDate)
	{
		if (aLastVisEndDate != null && !aLastVisEndDate.equals("") )
		{
			LastVisEndDate = fDate.getDate( aLastVisEndDate );
		}
		else
			LastVisEndDate = null;
	}

	public double getLastYearVisCount()
	{
		return LastYearVisCount;
	}
	public void setLastYearVisCount(double aLastYearVisCount)
	{
		LastYearVisCount = Arith.round(aLastYearVisCount,0);
	}
	public void setLastYearVisCount(String aLastYearVisCount)
	{
		if (aLastYearVisCount != null && !aLastYearVisCount.equals(""))
		{
			Double tDouble = new Double(aLastYearVisCount);
			double d = tDouble.doubleValue();
                LastYearVisCount = Arith.round(d,0);
		}
	}

	public double getLastYearMedRealPay()
	{
		return LastYearMedRealPay;
	}
	public void setLastYearMedRealPay(double aLastYearMedRealPay)
	{
		LastYearMedRealPay = Arith.round(aLastYearMedRealPay,2);
	}
	public void setLastYearMedRealPay(String aLastYearMedRealPay)
	{
		if (aLastYearMedRealPay != null && !aLastYearMedRealPay.equals(""))
		{
			Double tDouble = new Double(aLastYearMedRealPay);
			double d = tDouble.doubleValue();
                LastYearMedRealPay = Arith.round(d,2);
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getStandbyField1()
	{
		return StandbyField1;
	}
	public void setStandbyField1(String aStandbyField1)
	{
		StandbyField1 = aStandbyField1;
	}
	public String getStandbyField2()
	{
		return StandbyField2;
	}
	public void setStandbyField2(String aStandbyField2)
	{
		StandbyField2 = aStandbyField2;
	}
	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 HMSuggestSendTrackSchema 对象给 Schema 赋值
	* @param: aHMSuggestSendTrackSchema HMSuggestSendTrackSchema
	**/
	public void setSchema(HMSuggestSendTrackSchema aHMSuggestSendTrackSchema)
	{
		this.SerialNo = aHMSuggestSendTrackSchema.getSerialNo();
		this.BusinessType = aHMSuggestSendTrackSchema.getBusinessType();
		this.BusinessCode = aHMSuggestSendTrackSchema.getBusinessCode();
		this.CustomerNo = aHMSuggestSendTrackSchema.getCustomerNo();
		this.SendType = aHMSuggestSendTrackSchema.getSendType();
		this.Name = aHMSuggestSendTrackSchema.getName();
		this.SendStatus = aHMSuggestSendTrackSchema.getSendStatus();
		this.IDType = aHMSuggestSendTrackSchema.getIDType();
		this.IDNo = aHMSuggestSendTrackSchema.getIDNo();
		this.AddressNo = aHMSuggestSendTrackSchema.getAddressNo();
		this.Mobile = aHMSuggestSendTrackSchema.getMobile();
		this.EMail = aHMSuggestSendTrackSchema.getEMail();
		this.HomeAddress = aHMSuggestSendTrackSchema.getHomeAddress();
		this.PostalAddress = aHMSuggestSendTrackSchema.getPostalAddress();
		this.ZipCode = aHMSuggestSendTrackSchema.getZipCode();
		this.Suggest = aHMSuggestSendTrackSchema.getSuggest();
		this.VisType = aHMSuggestSendTrackSchema.getVisType();
		this.LastVisEndDate = fDate.getDate( aHMSuggestSendTrackSchema.getLastVisEndDate());
		this.LastYearVisCount = aHMSuggestSendTrackSchema.getLastYearVisCount();
		this.LastYearMedRealPay = aHMSuggestSendTrackSchema.getLastYearMedRealPay();
		this.Remark = aHMSuggestSendTrackSchema.getRemark();
		this.StandbyField1 = aHMSuggestSendTrackSchema.getStandbyField1();
		this.StandbyField2 = aHMSuggestSendTrackSchema.getStandbyField2();
		this.MakeTime = aHMSuggestSendTrackSchema.getMakeTime();
		this.MakeDate = fDate.getDate( aHMSuggestSendTrackSchema.getMakeDate());
		this.ManageCom = aHMSuggestSendTrackSchema.getManageCom();
		this.Operator = aHMSuggestSendTrackSchema.getOperator();
		this.ModifyDate = fDate.getDate( aHMSuggestSendTrackSchema.getModifyDate());
		this.ModifyTime = aHMSuggestSendTrackSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("BusinessType") == null )
				this.BusinessType = null;
			else
				this.BusinessType = rs.getString("BusinessType").trim();

			if( rs.getString("BusinessCode") == null )
				this.BusinessCode = null;
			else
				this.BusinessCode = rs.getString("BusinessCode").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("SendType") == null )
				this.SendType = null;
			else
				this.SendType = rs.getString("SendType").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("SendStatus") == null )
				this.SendStatus = null;
			else
				this.SendStatus = rs.getString("SendStatus").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("AddressNo") == null )
				this.AddressNo = null;
			else
				this.AddressNo = rs.getString("AddressNo").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("EMail") == null )
				this.EMail = null;
			else
				this.EMail = rs.getString("EMail").trim();

			if( rs.getString("HomeAddress") == null )
				this.HomeAddress = null;
			else
				this.HomeAddress = rs.getString("HomeAddress").trim();

			if( rs.getString("PostalAddress") == null )
				this.PostalAddress = null;
			else
				this.PostalAddress = rs.getString("PostalAddress").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Suggest") == null )
				this.Suggest = null;
			else
				this.Suggest = rs.getString("Suggest").trim();

			if( rs.getString("VisType") == null )
				this.VisType = null;
			else
				this.VisType = rs.getString("VisType").trim();

			this.LastVisEndDate = rs.getDate("LastVisEndDate");
			this.LastYearVisCount = rs.getDouble("LastYearVisCount");
			this.LastYearMedRealPay = rs.getDouble("LastYearMedRealPay");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("StandbyField1") == null )
				this.StandbyField1 = null;
			else
				this.StandbyField1 = rs.getString("StandbyField1").trim();

			if( rs.getString("StandbyField2") == null )
				this.StandbyField2 = null;
			else
				this.StandbyField2 = rs.getString("StandbyField2").trim();

			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的HMSuggestSendTrack表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMSuggestSendTrackSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public HMSuggestSendTrackSchema getSchema()
	{
		HMSuggestSendTrackSchema aHMSuggestSendTrackSchema = new HMSuggestSendTrackSchema();
		aHMSuggestSendTrackSchema.setSchema(this);
		return aHMSuggestSendTrackSchema;
	}

	public HMSuggestSendTrackDB getDB()
	{
		HMSuggestSendTrackDB aDBOper = new HMSuggestSendTrackDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMSuggestSendTrack描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HomeAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Suggest)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VisType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastVisEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LastYearVisCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LastYearMedRealPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyField1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyField2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMSuggestSendTrack>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BusinessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BusinessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SendType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SendStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			HomeAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Suggest = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			VisType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			LastVisEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			LastYearVisCount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			LastYearMedRealPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			StandbyField1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			StandbyField2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMSuggestSendTrackSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("BusinessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
		}
		if (FCode.equals("BusinessCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessCode));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("SendType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendType));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("SendStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendStatus));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("AddressNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("EMail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
		}
		if (FCode.equals("HomeAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HomeAddress));
		}
		if (FCode.equals("PostalAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Suggest"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Suggest));
		}
		if (FCode.equals("VisType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VisType));
		}
		if (FCode.equals("LastVisEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastVisEndDate()));
		}
		if (FCode.equals("LastYearVisCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LastYearVisCount));
		}
		if (FCode.equals("LastYearMedRealPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LastYearMedRealPay));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("StandbyField1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyField1));
		}
		if (FCode.equals("StandbyField2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyField2));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BusinessType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BusinessCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SendType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SendStatus);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AddressNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(EMail);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(HomeAddress);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(PostalAddress);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Suggest);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(VisType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastVisEndDate()));
				break;
			case 18:
				strFieldValue = String.valueOf(LastYearVisCount);
				break;
			case 19:
				strFieldValue = String.valueOf(LastYearMedRealPay);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(StandbyField1);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(StandbyField2);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("BusinessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessType = FValue.trim();
			}
			else
				BusinessType = null;
		}
		if (FCode.equalsIgnoreCase("BusinessCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessCode = FValue.trim();
			}
			else
				BusinessCode = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("SendType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendType = FValue.trim();
			}
			else
				SendType = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("SendStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendStatus = FValue.trim();
			}
			else
				SendStatus = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("AddressNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AddressNo = FValue.trim();
			}
			else
				AddressNo = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("EMail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EMail = FValue.trim();
			}
			else
				EMail = null;
		}
		if (FCode.equalsIgnoreCase("HomeAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HomeAddress = FValue.trim();
			}
			else
				HomeAddress = null;
		}
		if (FCode.equalsIgnoreCase("PostalAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalAddress = FValue.trim();
			}
			else
				PostalAddress = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Suggest"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Suggest = FValue.trim();
			}
			else
				Suggest = null;
		}
		if (FCode.equalsIgnoreCase("VisType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VisType = FValue.trim();
			}
			else
				VisType = null;
		}
		if (FCode.equalsIgnoreCase("LastVisEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastVisEndDate = fDate.getDate( FValue );
			}
			else
				LastVisEndDate = null;
		}
		if (FCode.equalsIgnoreCase("LastYearVisCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LastYearVisCount = d;
			}
		}
		if (FCode.equalsIgnoreCase("LastYearMedRealPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LastYearMedRealPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("StandbyField1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyField1 = FValue.trim();
			}
			else
				StandbyField1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyField2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyField2 = FValue.trim();
			}
			else
				StandbyField2 = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		HMSuggestSendTrackSchema other = (HMSuggestSendTrackSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (BusinessType == null ? other.getBusinessType() == null : BusinessType.equals(other.getBusinessType()))
			&& (BusinessCode == null ? other.getBusinessCode() == null : BusinessCode.equals(other.getBusinessCode()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (SendType == null ? other.getSendType() == null : SendType.equals(other.getSendType()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (SendStatus == null ? other.getSendStatus() == null : SendStatus.equals(other.getSendStatus()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (AddressNo == null ? other.getAddressNo() == null : AddressNo.equals(other.getAddressNo()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (EMail == null ? other.getEMail() == null : EMail.equals(other.getEMail()))
			&& (HomeAddress == null ? other.getHomeAddress() == null : HomeAddress.equals(other.getHomeAddress()))
			&& (PostalAddress == null ? other.getPostalAddress() == null : PostalAddress.equals(other.getPostalAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Suggest == null ? other.getSuggest() == null : Suggest.equals(other.getSuggest()))
			&& (VisType == null ? other.getVisType() == null : VisType.equals(other.getVisType()))
			&& (LastVisEndDate == null ? other.getLastVisEndDate() == null : fDate.getString(LastVisEndDate).equals(other.getLastVisEndDate()))
			&& LastYearVisCount == other.getLastYearVisCount()
			&& LastYearMedRealPay == other.getLastYearMedRealPay()
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (StandbyField1 == null ? other.getStandbyField1() == null : StandbyField1.equals(other.getStandbyField1()))
			&& (StandbyField2 == null ? other.getStandbyField2() == null : StandbyField2.equals(other.getStandbyField2()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("BusinessType") ) {
			return 1;
		}
		if( strFieldName.equals("BusinessCode") ) {
			return 2;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 3;
		}
		if( strFieldName.equals("SendType") ) {
			return 4;
		}
		if( strFieldName.equals("Name") ) {
			return 5;
		}
		if( strFieldName.equals("SendStatus") ) {
			return 6;
		}
		if( strFieldName.equals("IDType") ) {
			return 7;
		}
		if( strFieldName.equals("IDNo") ) {
			return 8;
		}
		if( strFieldName.equals("AddressNo") ) {
			return 9;
		}
		if( strFieldName.equals("Mobile") ) {
			return 10;
		}
		if( strFieldName.equals("EMail") ) {
			return 11;
		}
		if( strFieldName.equals("HomeAddress") ) {
			return 12;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return 13;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 14;
		}
		if( strFieldName.equals("Suggest") ) {
			return 15;
		}
		if( strFieldName.equals("VisType") ) {
			return 16;
		}
		if( strFieldName.equals("LastVisEndDate") ) {
			return 17;
		}
		if( strFieldName.equals("LastYearVisCount") ) {
			return 18;
		}
		if( strFieldName.equals("LastYearMedRealPay") ) {
			return 19;
		}
		if( strFieldName.equals("Remark") ) {
			return 20;
		}
		if( strFieldName.equals("StandbyField1") ) {
			return 21;
		}
		if( strFieldName.equals("StandbyField2") ) {
			return 22;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 23;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 24;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 25;
		}
		if( strFieldName.equals("Operator") ) {
			return 26;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 28;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "BusinessType";
				break;
			case 2:
				strFieldName = "BusinessCode";
				break;
			case 3:
				strFieldName = "CustomerNo";
				break;
			case 4:
				strFieldName = "SendType";
				break;
			case 5:
				strFieldName = "Name";
				break;
			case 6:
				strFieldName = "SendStatus";
				break;
			case 7:
				strFieldName = "IDType";
				break;
			case 8:
				strFieldName = "IDNo";
				break;
			case 9:
				strFieldName = "AddressNo";
				break;
			case 10:
				strFieldName = "Mobile";
				break;
			case 11:
				strFieldName = "EMail";
				break;
			case 12:
				strFieldName = "HomeAddress";
				break;
			case 13:
				strFieldName = "PostalAddress";
				break;
			case 14:
				strFieldName = "ZipCode";
				break;
			case 15:
				strFieldName = "Suggest";
				break;
			case 16:
				strFieldName = "VisType";
				break;
			case 17:
				strFieldName = "LastVisEndDate";
				break;
			case 18:
				strFieldName = "LastYearVisCount";
				break;
			case 19:
				strFieldName = "LastYearMedRealPay";
				break;
			case 20:
				strFieldName = "Remark";
				break;
			case 21:
				strFieldName = "StandbyField1";
				break;
			case 22:
				strFieldName = "StandbyField2";
				break;
			case 23:
				strFieldName = "MakeTime";
				break;
			case 24:
				strFieldName = "MakeDate";
				break;
			case 25:
				strFieldName = "ManageCom";
				break;
			case 26:
				strFieldName = "Operator";
				break;
			case 27:
				strFieldName = "ModifyDate";
				break;
			case 28:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AddressNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EMail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HomeAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Suggest") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VisType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LastVisEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LastYearVisCount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("LastYearMedRealPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyField1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyField2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
