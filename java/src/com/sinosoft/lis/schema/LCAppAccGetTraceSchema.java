/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCAppAccGetTraceDB;

/*
 * <p>ClassName: LCAppAccGetTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: CustomerAccount
 * @CreateDate：2006-02-22
 */
public class LCAppAccGetTraceSchema implements Schema, Cloneable
{
	// @Field
	/** 客户号码 */
	private String CustomerNo;
	/** 流水号 */
	private String SerialNo;
	/** 通知书号 */
	private String NoticeNo;
	/** 申请日期 */
	private Date AppDate;
	/** 领款人姓名 */
	private String Name;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 领取方式 */
	private String AccGetMode;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 银行户名 */
	private String AccName;
	/** 转帐日期 */
	private Date TransferDate;
	/** 领取金额 */
	private double AccGetMoney;
	/** 账户现金余额 */
	private double AccBala;
	/** 领取日期 */
	private Date AccGetDate;
	/** 领取时间 */
	private String AccGetTime;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCAppAccGetTraceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "CustomerNo";
		pk[1] = "SerialNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LCAppAccGetTraceSchema cloned = (LCAppAccGetTraceSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
            CustomerNo = aCustomerNo;
	}
	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
            SerialNo = aSerialNo;
	}
	public String getNoticeNo()
	{
		return NoticeNo;
	}
	public void setNoticeNo(String aNoticeNo)
	{
            NoticeNo = aNoticeNo;
	}
	public String getAppDate()
	{
		if( AppDate != null )
			return fDate.getString(AppDate);
		else
			return null;
	}
	public void setAppDate(Date aAppDate)
	{
            AppDate = aAppDate;
	}
	public void setAppDate(String aAppDate)
	{
		if (aAppDate != null && !aAppDate.equals("") )
		{
			AppDate = fDate.getDate( aAppDate );
		}
		else
			AppDate = null;
	}

	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
            Name = aName;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
            IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
            IDNo = aIDNo;
	}
	public String getAccGetMode()
	{
		return AccGetMode;
	}
	public void setAccGetMode(String aAccGetMode)
	{
            AccGetMode = aAccGetMode;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
            BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
            BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
            AccName = aAccName;
	}
	public String getTransferDate()
	{
		if( TransferDate != null )
			return fDate.getString(TransferDate);
		else
			return null;
	}
	public void setTransferDate(Date aTransferDate)
	{
            TransferDate = aTransferDate;
	}
	public void setTransferDate(String aTransferDate)
	{
		if (aTransferDate != null && !aTransferDate.equals("") )
		{
			TransferDate = fDate.getDate( aTransferDate );
		}
		else
			TransferDate = null;
	}

	public double getAccGetMoney()
	{
		return AccGetMoney;
	}
	public void setAccGetMoney(double aAccGetMoney)
	{
            AccGetMoney = Arith.round(aAccGetMoney,2);
	}
	public void setAccGetMoney(String aAccGetMoney)
	{
		if (aAccGetMoney != null && !aAccGetMoney.equals(""))
		{
			Double tDouble = new Double(aAccGetMoney);
			double d = tDouble.doubleValue();
                AccGetMoney = Arith.round(d,2);
		}
	}

	public double getAccBala()
	{
		return AccBala;
	}
	public void setAccBala(double aAccBala)
	{
            AccBala = Arith.round(aAccBala,2);
	}
	public void setAccBala(String aAccBala)
	{
		if (aAccBala != null && !aAccBala.equals(""))
		{
			Double tDouble = new Double(aAccBala);
			double d = tDouble.doubleValue();
                AccBala = Arith.round(d,2);
		}
	}

	public String getAccGetDate()
	{
		if( AccGetDate != null )
			return fDate.getString(AccGetDate);
		else
			return null;
	}
	public void setAccGetDate(Date aAccGetDate)
	{
            AccGetDate = aAccGetDate;
	}
	public void setAccGetDate(String aAccGetDate)
	{
		if (aAccGetDate != null && !aAccGetDate.equals("") )
		{
			AccGetDate = fDate.getDate( aAccGetDate );
		}
		else
			AccGetDate = null;
	}

	public String getAccGetTime()
	{
		return AccGetTime;
	}
	public void setAccGetTime(String aAccGetTime)
	{
            AccGetTime = aAccGetTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCAppAccGetTraceSchema 对象给 Schema 赋值
	* @param: aLCAppAccGetTraceSchema LCAppAccGetTraceSchema
	**/
	public void setSchema(LCAppAccGetTraceSchema aLCAppAccGetTraceSchema)
	{
		this.CustomerNo = aLCAppAccGetTraceSchema.getCustomerNo();
		this.SerialNo = aLCAppAccGetTraceSchema.getSerialNo();
		this.NoticeNo = aLCAppAccGetTraceSchema.getNoticeNo();
		this.AppDate = fDate.getDate( aLCAppAccGetTraceSchema.getAppDate());
		this.Name = aLCAppAccGetTraceSchema.getName();
		this.IDType = aLCAppAccGetTraceSchema.getIDType();
		this.IDNo = aLCAppAccGetTraceSchema.getIDNo();
		this.AccGetMode = aLCAppAccGetTraceSchema.getAccGetMode();
		this.BankCode = aLCAppAccGetTraceSchema.getBankCode();
		this.BankAccNo = aLCAppAccGetTraceSchema.getBankAccNo();
		this.AccName = aLCAppAccGetTraceSchema.getAccName();
		this.TransferDate = fDate.getDate( aLCAppAccGetTraceSchema.getTransferDate());
		this.AccGetMoney = aLCAppAccGetTraceSchema.getAccGetMoney();
		this.AccBala = aLCAppAccGetTraceSchema.getAccBala();
		this.AccGetDate = fDate.getDate( aLCAppAccGetTraceSchema.getAccGetDate());
		this.AccGetTime = aLCAppAccGetTraceSchema.getAccGetTime();
		this.Operator = aLCAppAccGetTraceSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCAppAccGetTraceSchema.getMakeDate());
		this.MakeTime = aLCAppAccGetTraceSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCAppAccGetTraceSchema.getModifyDate());
		this.ModifyTime = aLCAppAccGetTraceSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("NoticeNo") == null )
				this.NoticeNo = null;
			else
				this.NoticeNo = rs.getString("NoticeNo").trim();

			this.AppDate = rs.getDate("AppDate");
			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("AccGetMode") == null )
				this.AccGetMode = null;
			else
				this.AccGetMode = rs.getString("AccGetMode").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			this.TransferDate = rs.getDate("TransferDate");
			this.AccGetMoney = rs.getDouble("AccGetMoney");
			this.AccBala = rs.getDouble("AccBala");
			this.AccGetDate = rs.getDate("AccGetDate");
			if( rs.getString("AccGetTime") == null )
				this.AccGetTime = null;
			else
				this.AccGetTime = rs.getString("AccGetTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCAppAccGetTrace表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCAppAccGetTraceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCAppAccGetTraceSchema getSchema()
	{
		LCAppAccGetTraceSchema aLCAppAccGetTraceSchema = new LCAppAccGetTraceSchema();
		aLCAppAccGetTraceSchema.setSchema(this);
		return aLCAppAccGetTraceSchema;
	}

	public LCAppAccGetTraceDB getDB()
	{
		LCAppAccGetTraceDB aDBOper = new LCAppAccGetTraceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAppAccGetTrace描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(NoticeNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( AppDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AccGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( TransferDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AccGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AccBala));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( AccGetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AccGetTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAppAccGetTrace>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			NoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AccGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			TransferDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			AccGetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			AccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			AccGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			AccGetTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCAppAccGetTraceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("NoticeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NoticeNo));
		}
		if (FCode.equals("AppDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppDate()));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("AccGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccGetMode));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("TransferDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTransferDate()));
		}
		if (FCode.equals("AccGetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccGetMoney));
		}
		if (FCode.equals("AccBala"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccBala));
		}
		if (FCode.equals("AccGetDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccGetDate()));
		}
		if (FCode.equals("AccGetTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccGetTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(NoticeNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AccGetMode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTransferDate()));
				break;
			case 12:
				strFieldValue = String.valueOf(AccGetMoney);
				break;
			case 13:
				strFieldValue = String.valueOf(AccBala);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccGetDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AccGetTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("NoticeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NoticeNo = FValue.trim();
			}
			else
				NoticeNo = null;
		}
		if (FCode.equalsIgnoreCase("AppDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AppDate = fDate.getDate( FValue );
			}
			else
				AppDate = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("AccGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccGetMode = FValue.trim();
			}
			else
				AccGetMode = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("TransferDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TransferDate = fDate.getDate( FValue );
			}
			else
				TransferDate = null;
		}
		if (FCode.equalsIgnoreCase("AccGetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccGetMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccBala"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccBala = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccGetDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccGetDate = fDate.getDate( FValue );
			}
			else
				AccGetDate = null;
		}
		if (FCode.equalsIgnoreCase("AccGetTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccGetTime = FValue.trim();
			}
			else
				AccGetTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCAppAccGetTraceSchema other = (LCAppAccGetTraceSchema)otherObject;
		return
			CustomerNo.equals(other.getCustomerNo())
			&& SerialNo.equals(other.getSerialNo())
			&& NoticeNo.equals(other.getNoticeNo())
			&& fDate.getString(AppDate).equals(other.getAppDate())
			&& Name.equals(other.getName())
			&& IDType.equals(other.getIDType())
			&& IDNo.equals(other.getIDNo())
			&& AccGetMode.equals(other.getAccGetMode())
			&& BankCode.equals(other.getBankCode())
			&& BankAccNo.equals(other.getBankAccNo())
			&& AccName.equals(other.getAccName())
			&& fDate.getString(TransferDate).equals(other.getTransferDate())
			&& AccGetMoney == other.getAccGetMoney()
			&& AccBala == other.getAccBala()
			&& fDate.getString(AccGetDate).equals(other.getAccGetDate())
			&& AccGetTime.equals(other.getAccGetTime())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CustomerNo") ) {
			return 0;
		}
		if( strFieldName.equals("SerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("NoticeNo") ) {
			return 2;
		}
		if( strFieldName.equals("AppDate") ) {
			return 3;
		}
		if( strFieldName.equals("Name") ) {
			return 4;
		}
		if( strFieldName.equals("IDType") ) {
			return 5;
		}
		if( strFieldName.equals("IDNo") ) {
			return 6;
		}
		if( strFieldName.equals("AccGetMode") ) {
			return 7;
		}
		if( strFieldName.equals("BankCode") ) {
			return 8;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 9;
		}
		if( strFieldName.equals("AccName") ) {
			return 10;
		}
		if( strFieldName.equals("TransferDate") ) {
			return 11;
		}
		if( strFieldName.equals("AccGetMoney") ) {
			return 12;
		}
		if( strFieldName.equals("AccBala") ) {
			return 13;
		}
		if( strFieldName.equals("AccGetDate") ) {
			return 14;
		}
		if( strFieldName.equals("AccGetTime") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CustomerNo";
				break;
			case 1:
				strFieldName = "SerialNo";
				break;
			case 2:
				strFieldName = "NoticeNo";
				break;
			case 3:
				strFieldName = "AppDate";
				break;
			case 4:
				strFieldName = "Name";
				break;
			case 5:
				strFieldName = "IDType";
				break;
			case 6:
				strFieldName = "IDNo";
				break;
			case 7:
				strFieldName = "AccGetMode";
				break;
			case 8:
				strFieldName = "BankCode";
				break;
			case 9:
				strFieldName = "BankAccNo";
				break;
			case 10:
				strFieldName = "AccName";
				break;
			case 11:
				strFieldName = "TransferDate";
				break;
			case 12:
				strFieldName = "AccGetMoney";
				break;
			case 13:
				strFieldName = "AccBala";
				break;
			case 14:
				strFieldName = "AccGetDate";
				break;
			case 15:
				strFieldName = "AccGetTime";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NoticeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransferDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccGetMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccBala") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccGetDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccGetTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
