/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LGWorkDB;

/*
 * <p>ClassName: LGWorkSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全代理人信息
 * @CreateDate：2015-03-25
 */
public class LGWorkSchema implements Schema, Cloneable
{
	// @Field
	/** 作业编号 */
	private String WorkNo;
	/** 受理编号 */
	private String AcceptNo;
	/** 当前结点编号 */
	private String NodeNo;
	/** 作业类型编号 */
	private String TypeNo;
	/** 作业状态编号 */
	private String StatusNo;
	/** 具体作业编号 */
	private String DetailWorkNo;
	/** 优先级别编号 */
	private String PriorityNo;
	/** 客户编号 */
	private String CustomerNo;
	/** 客户身份证号 */
	private String CustomerCardNo;
	/** 客户姓名 */
	private String CustomerName;
	/** 保单编号 */
	private String ContNo;
	/** 申请人类型编号 */
	private String ApplyTypeNo;
	/** 申请人姓名 */
	private String ApplyName;
	/** 受理途径编号 */
	private String AcceptWayNo;
	/** 受理机构编号 */
	private String AcceptCom;
	/** 受理人编号 */
	private String AcceptorNo;
	/** 受理日期 */
	private Date AcceptDate;
	/** 时限 */
	private String DateLimit;
	/** 备注 */
	private String Remark;
	/** 待办天数 */
	private String PauseNo;
	/** 待办日期 */
	private Date PauseDate;
	/** 唤醒日期 */
	private Date WakeDate;
	/** 唤醒时间 */
	private String WakeTime;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 合并到 */
	private String UniteTo;
	/** 有合并项 */
	private String Uniting;
	/** 当前用户正经办标识 */
	private String CurrDoing;
	/** 扫描件状态 */
	private String ScanFlag;
	/** 内部作业源 */
	private String InnerSource;
	/** 代理标记 */
	private String AgentFlag;
	/** 代理人姓名 */
	private String AgentName;
	/** 证件类型 */
	private String AgentIDType;
	/** 证件号码 */
	private String AgentIDNo;
	/** 身份证明文件生效日期 */
	private Date AgentIDStartDate;
	/** 身份证明文件失效日期 */
	private Date AgentIDEndDate;
	/** 代理人联系电话 */
	private String AgentPhone;

	public static final int FIELDNUM = 40;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LGWorkSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "WorkNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LGWorkSchema cloned = (LGWorkSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getWorkNo()
	{
		return WorkNo;
	}
	public void setWorkNo(String aWorkNo)
	{
		WorkNo = aWorkNo;
	}
	public String getAcceptNo()
	{
		return AcceptNo;
	}
	public void setAcceptNo(String aAcceptNo)
	{
		AcceptNo = aAcceptNo;
	}
	public String getNodeNo()
	{
		return NodeNo;
	}
	public void setNodeNo(String aNodeNo)
	{
		NodeNo = aNodeNo;
	}
	public String getTypeNo()
	{
		return TypeNo;
	}
	public void setTypeNo(String aTypeNo)
	{
		TypeNo = aTypeNo;
	}
	public String getStatusNo()
	{
		return StatusNo;
	}
	public void setStatusNo(String aStatusNo)
	{
		StatusNo = aStatusNo;
	}
	public String getDetailWorkNo()
	{
		return DetailWorkNo;
	}
	public void setDetailWorkNo(String aDetailWorkNo)
	{
		DetailWorkNo = aDetailWorkNo;
	}
	public String getPriorityNo()
	{
		return PriorityNo;
	}
	public void setPriorityNo(String aPriorityNo)
	{
		PriorityNo = aPriorityNo;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getCustomerCardNo()
	{
		return CustomerCardNo;
	}
	public void setCustomerCardNo(String aCustomerCardNo)
	{
		CustomerCardNo = aCustomerCardNo;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getApplyTypeNo()
	{
		return ApplyTypeNo;
	}
	public void setApplyTypeNo(String aApplyTypeNo)
	{
		ApplyTypeNo = aApplyTypeNo;
	}
	public String getApplyName()
	{
		return ApplyName;
	}
	public void setApplyName(String aApplyName)
	{
		ApplyName = aApplyName;
	}
	public String getAcceptWayNo()
	{
		return AcceptWayNo;
	}
	public void setAcceptWayNo(String aAcceptWayNo)
	{
		AcceptWayNo = aAcceptWayNo;
	}
	public String getAcceptCom()
	{
		return AcceptCom;
	}
	public void setAcceptCom(String aAcceptCom)
	{
		AcceptCom = aAcceptCom;
	}
	public String getAcceptorNo()
	{
		return AcceptorNo;
	}
	public void setAcceptorNo(String aAcceptorNo)
	{
		AcceptorNo = aAcceptorNo;
	}
	public String getAcceptDate()
	{
		if( AcceptDate != null )
			return fDate.getString(AcceptDate);
		else
			return null;
	}
	public void setAcceptDate(Date aAcceptDate)
	{
		AcceptDate = aAcceptDate;
	}
	public void setAcceptDate(String aAcceptDate)
	{
		if (aAcceptDate != null && !aAcceptDate.equals("") )
		{
			AcceptDate = fDate.getDate( aAcceptDate );
		}
		else
			AcceptDate = null;
	}

	public String getDateLimit()
	{
		return DateLimit;
	}
	public void setDateLimit(String aDateLimit)
	{
		DateLimit = aDateLimit;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getPauseNo()
	{
		return PauseNo;
	}
	public void setPauseNo(String aPauseNo)
	{
		PauseNo = aPauseNo;
	}
	public String getPauseDate()
	{
		if( PauseDate != null )
			return fDate.getString(PauseDate);
		else
			return null;
	}
	public void setPauseDate(Date aPauseDate)
	{
		PauseDate = aPauseDate;
	}
	public void setPauseDate(String aPauseDate)
	{
		if (aPauseDate != null && !aPauseDate.equals("") )
		{
			PauseDate = fDate.getDate( aPauseDate );
		}
		else
			PauseDate = null;
	}

	public String getWakeDate()
	{
		if( WakeDate != null )
			return fDate.getString(WakeDate);
		else
			return null;
	}
	public void setWakeDate(Date aWakeDate)
	{
		WakeDate = aWakeDate;
	}
	public void setWakeDate(String aWakeDate)
	{
		if (aWakeDate != null && !aWakeDate.equals("") )
		{
			WakeDate = fDate.getDate( aWakeDate );
		}
		else
			WakeDate = null;
	}

	public String getWakeTime()
	{
		return WakeTime;
	}
	public void setWakeTime(String aWakeTime)
	{
		WakeTime = aWakeTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getUniteTo()
	{
		return UniteTo;
	}
	public void setUniteTo(String aUniteTo)
	{
		UniteTo = aUniteTo;
	}
	public String getUniting()
	{
		return Uniting;
	}
	public void setUniting(String aUniting)
	{
		Uniting = aUniting;
	}
	public String getCurrDoing()
	{
		return CurrDoing;
	}
	public void setCurrDoing(String aCurrDoing)
	{
		CurrDoing = aCurrDoing;
	}
	public String getScanFlag()
	{
		return ScanFlag;
	}
	public void setScanFlag(String aScanFlag)
	{
		ScanFlag = aScanFlag;
	}
	public String getInnerSource()
	{
		return InnerSource;
	}
	public void setInnerSource(String aInnerSource)
	{
		InnerSource = aInnerSource;
	}
	public String getAgentFlag()
	{
		return AgentFlag;
	}
	public void setAgentFlag(String aAgentFlag)
	{
		AgentFlag = aAgentFlag;
	}
	public String getAgentName()
	{
		return AgentName;
	}
	public void setAgentName(String aAgentName)
	{
		AgentName = aAgentName;
	}
	public String getAgentIDType()
	{
		return AgentIDType;
	}
	public void setAgentIDType(String aAgentIDType)
	{
		AgentIDType = aAgentIDType;
	}
	public String getAgentIDNo()
	{
		return AgentIDNo;
	}
	public void setAgentIDNo(String aAgentIDNo)
	{
		AgentIDNo = aAgentIDNo;
	}
	public String getAgentIDStartDate()
	{
		if( AgentIDStartDate != null )
			return fDate.getString(AgentIDStartDate);
		else
			return null;
	}
	public void setAgentIDStartDate(Date aAgentIDStartDate)
	{
		AgentIDStartDate = aAgentIDStartDate;
	}
	public void setAgentIDStartDate(String aAgentIDStartDate)
	{
		if (aAgentIDStartDate != null && !aAgentIDStartDate.equals("") )
		{
			AgentIDStartDate = fDate.getDate( aAgentIDStartDate );
		}
		else
			AgentIDStartDate = null;
	}

	public String getAgentIDEndDate()
	{
		if( AgentIDEndDate != null )
			return fDate.getString(AgentIDEndDate);
		else
			return null;
	}
	public void setAgentIDEndDate(Date aAgentIDEndDate)
	{
		AgentIDEndDate = aAgentIDEndDate;
	}
	public void setAgentIDEndDate(String aAgentIDEndDate)
	{
		if (aAgentIDEndDate != null && !aAgentIDEndDate.equals("") )
		{
			AgentIDEndDate = fDate.getDate( aAgentIDEndDate );
		}
		else
			AgentIDEndDate = null;
	}

	public String getAgentPhone()
	{
		return AgentPhone;
	}
	public void setAgentPhone(String aAgentPhone)
	{
		AgentPhone = aAgentPhone;
	}

	/**
	* 使用另外一个 LGWorkSchema 对象给 Schema 赋值
	* @param: aLGWorkSchema LGWorkSchema
	**/
	public void setSchema(LGWorkSchema aLGWorkSchema)
	{
		this.WorkNo = aLGWorkSchema.getWorkNo();
		this.AcceptNo = aLGWorkSchema.getAcceptNo();
		this.NodeNo = aLGWorkSchema.getNodeNo();
		this.TypeNo = aLGWorkSchema.getTypeNo();
		this.StatusNo = aLGWorkSchema.getStatusNo();
		this.DetailWorkNo = aLGWorkSchema.getDetailWorkNo();
		this.PriorityNo = aLGWorkSchema.getPriorityNo();
		this.CustomerNo = aLGWorkSchema.getCustomerNo();
		this.CustomerCardNo = aLGWorkSchema.getCustomerCardNo();
		this.CustomerName = aLGWorkSchema.getCustomerName();
		this.ContNo = aLGWorkSchema.getContNo();
		this.ApplyTypeNo = aLGWorkSchema.getApplyTypeNo();
		this.ApplyName = aLGWorkSchema.getApplyName();
		this.AcceptWayNo = aLGWorkSchema.getAcceptWayNo();
		this.AcceptCom = aLGWorkSchema.getAcceptCom();
		this.AcceptorNo = aLGWorkSchema.getAcceptorNo();
		this.AcceptDate = fDate.getDate( aLGWorkSchema.getAcceptDate());
		this.DateLimit = aLGWorkSchema.getDateLimit();
		this.Remark = aLGWorkSchema.getRemark();
		this.PauseNo = aLGWorkSchema.getPauseNo();
		this.PauseDate = fDate.getDate( aLGWorkSchema.getPauseDate());
		this.WakeDate = fDate.getDate( aLGWorkSchema.getWakeDate());
		this.WakeTime = aLGWorkSchema.getWakeTime();
		this.Operator = aLGWorkSchema.getOperator();
		this.MakeDate = fDate.getDate( aLGWorkSchema.getMakeDate());
		this.MakeTime = aLGWorkSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLGWorkSchema.getModifyDate());
		this.ModifyTime = aLGWorkSchema.getModifyTime();
		this.UniteTo = aLGWorkSchema.getUniteTo();
		this.Uniting = aLGWorkSchema.getUniting();
		this.CurrDoing = aLGWorkSchema.getCurrDoing();
		this.ScanFlag = aLGWorkSchema.getScanFlag();
		this.InnerSource = aLGWorkSchema.getInnerSource();
		this.AgentFlag = aLGWorkSchema.getAgentFlag();
		this.AgentName = aLGWorkSchema.getAgentName();
		this.AgentIDType = aLGWorkSchema.getAgentIDType();
		this.AgentIDNo = aLGWorkSchema.getAgentIDNo();
		this.AgentIDStartDate = fDate.getDate( aLGWorkSchema.getAgentIDStartDate());
		this.AgentIDEndDate = fDate.getDate( aLGWorkSchema.getAgentIDEndDate());
		this.AgentPhone = aLGWorkSchema.getAgentPhone();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("WorkNo") == null )
				this.WorkNo = null;
			else
				this.WorkNo = rs.getString("WorkNo").trim();

			if( rs.getString("AcceptNo") == null )
				this.AcceptNo = null;
			else
				this.AcceptNo = rs.getString("AcceptNo").trim();

			if( rs.getString("NodeNo") == null )
				this.NodeNo = null;
			else
				this.NodeNo = rs.getString("NodeNo").trim();

			if( rs.getString("TypeNo") == null )
				this.TypeNo = null;
			else
				this.TypeNo = rs.getString("TypeNo").trim();

			if( rs.getString("StatusNo") == null )
				this.StatusNo = null;
			else
				this.StatusNo = rs.getString("StatusNo").trim();

			if( rs.getString("DetailWorkNo") == null )
				this.DetailWorkNo = null;
			else
				this.DetailWorkNo = rs.getString("DetailWorkNo").trim();

			if( rs.getString("PriorityNo") == null )
				this.PriorityNo = null;
			else
				this.PriorityNo = rs.getString("PriorityNo").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("CustomerCardNo") == null )
				this.CustomerCardNo = null;
			else
				this.CustomerCardNo = rs.getString("CustomerCardNo").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("ApplyTypeNo") == null )
				this.ApplyTypeNo = null;
			else
				this.ApplyTypeNo = rs.getString("ApplyTypeNo").trim();

			if( rs.getString("ApplyName") == null )
				this.ApplyName = null;
			else
				this.ApplyName = rs.getString("ApplyName").trim();

			if( rs.getString("AcceptWayNo") == null )
				this.AcceptWayNo = null;
			else
				this.AcceptWayNo = rs.getString("AcceptWayNo").trim();

			if( rs.getString("AcceptCom") == null )
				this.AcceptCom = null;
			else
				this.AcceptCom = rs.getString("AcceptCom").trim();

			if( rs.getString("AcceptorNo") == null )
				this.AcceptorNo = null;
			else
				this.AcceptorNo = rs.getString("AcceptorNo").trim();

			this.AcceptDate = rs.getDate("AcceptDate");
			if( rs.getString("DateLimit") == null )
				this.DateLimit = null;
			else
				this.DateLimit = rs.getString("DateLimit").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("PauseNo") == null )
				this.PauseNo = null;
			else
				this.PauseNo = rs.getString("PauseNo").trim();

			this.PauseDate = rs.getDate("PauseDate");
			this.WakeDate = rs.getDate("WakeDate");
			if( rs.getString("WakeTime") == null )
				this.WakeTime = null;
			else
				this.WakeTime = rs.getString("WakeTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("UniteTo") == null )
				this.UniteTo = null;
			else
				this.UniteTo = rs.getString("UniteTo").trim();

			if( rs.getString("Uniting") == null )
				this.Uniting = null;
			else
				this.Uniting = rs.getString("Uniting").trim();

			if( rs.getString("CurrDoing") == null )
				this.CurrDoing = null;
			else
				this.CurrDoing = rs.getString("CurrDoing").trim();

			if( rs.getString("ScanFlag") == null )
				this.ScanFlag = null;
			else
				this.ScanFlag = rs.getString("ScanFlag").trim();

			if( rs.getString("InnerSource") == null )
				this.InnerSource = null;
			else
				this.InnerSource = rs.getString("InnerSource").trim();

			if( rs.getString("AgentFlag") == null )
				this.AgentFlag = null;
			else
				this.AgentFlag = rs.getString("AgentFlag").trim();

			if( rs.getString("AgentName") == null )
				this.AgentName = null;
			else
				this.AgentName = rs.getString("AgentName").trim();

			if( rs.getString("AgentIDType") == null )
				this.AgentIDType = null;
			else
				this.AgentIDType = rs.getString("AgentIDType").trim();

			if( rs.getString("AgentIDNo") == null )
				this.AgentIDNo = null;
			else
				this.AgentIDNo = rs.getString("AgentIDNo").trim();

			this.AgentIDStartDate = rs.getDate("AgentIDStartDate");
			this.AgentIDEndDate = rs.getDate("AgentIDEndDate");
			if( rs.getString("AgentPhone") == null )
				this.AgentPhone = null;
			else
				this.AgentPhone = rs.getString("AgentPhone").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LGWork表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LGWorkSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LGWorkSchema getSchema()
	{
		LGWorkSchema aLGWorkSchema = new LGWorkSchema();
		aLGWorkSchema.setSchema(this);
		return aLGWorkSchema;
	}

	public LGWorkDB getDB()
	{
		LGWorkDB aDBOper = new LGWorkDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGWork描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(WorkNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AcceptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NodeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TypeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StatusNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DetailWorkNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PriorityNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerCardNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApplyTypeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApplyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AcceptWayNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AcceptCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AcceptorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AcceptDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DateLimit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PauseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PauseDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( WakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UniteTo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Uniting)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CurrDoing)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ScanFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InnerSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AgentIDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AgentIDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentPhone));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGWork>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			WorkNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			NodeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TypeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			StatusNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			DetailWorkNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			PriorityNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CustomerCardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ApplyTypeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ApplyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			AcceptWayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			AcceptCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			AcceptorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AcceptDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			DateLimit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PauseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PauseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			WakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			WakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			UniteTo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Uniting = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			CurrDoing = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			ScanFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			InnerSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			AgentFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			AgentIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			AgentIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			AgentIDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			AgentIDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			AgentPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LGWorkSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("WorkNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkNo));
		}
		if (FCode.equals("AcceptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptNo));
		}
		if (FCode.equals("NodeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NodeNo));
		}
		if (FCode.equals("TypeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TypeNo));
		}
		if (FCode.equals("StatusNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StatusNo));
		}
		if (FCode.equals("DetailWorkNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DetailWorkNo));
		}
		if (FCode.equals("PriorityNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PriorityNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("CustomerCardNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerCardNo));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("ApplyTypeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyTypeNo));
		}
		if (FCode.equals("ApplyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyName));
		}
		if (FCode.equals("AcceptWayNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptWayNo));
		}
		if (FCode.equals("AcceptCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptCom));
		}
		if (FCode.equals("AcceptorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AcceptorNo));
		}
		if (FCode.equals("AcceptDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAcceptDate()));
		}
		if (FCode.equals("DateLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DateLimit));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("PauseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PauseNo));
		}
		if (FCode.equals("PauseDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPauseDate()));
		}
		if (FCode.equals("WakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getWakeDate()));
		}
		if (FCode.equals("WakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WakeTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("UniteTo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UniteTo));
		}
		if (FCode.equals("Uniting"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Uniting));
		}
		if (FCode.equals("CurrDoing"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CurrDoing));
		}
		if (FCode.equals("ScanFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ScanFlag));
		}
		if (FCode.equals("InnerSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InnerSource));
		}
		if (FCode.equals("AgentFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentFlag));
		}
		if (FCode.equals("AgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
		}
		if (FCode.equals("AgentIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentIDType));
		}
		if (FCode.equals("AgentIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentIDNo));
		}
		if (FCode.equals("AgentIDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAgentIDStartDate()));
		}
		if (FCode.equals("AgentIDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAgentIDEndDate()));
		}
		if (FCode.equals("AgentPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPhone));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(WorkNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AcceptNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(NodeNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TypeNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(StatusNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(DetailWorkNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(PriorityNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CustomerCardNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ApplyTypeNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ApplyName);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(AcceptWayNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(AcceptCom);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AcceptorNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAcceptDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(DateLimit);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(PauseNo);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPauseDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getWakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(WakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(UniteTo);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Uniting);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(CurrDoing);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(ScanFlag);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(InnerSource);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(AgentFlag);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(AgentName);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(AgentIDType);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(AgentIDNo);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAgentIDStartDate()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAgentIDEndDate()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(AgentPhone);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("WorkNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkNo = FValue.trim();
			}
			else
				WorkNo = null;
		}
		if (FCode.equalsIgnoreCase("AcceptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AcceptNo = FValue.trim();
			}
			else
				AcceptNo = null;
		}
		if (FCode.equalsIgnoreCase("NodeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NodeNo = FValue.trim();
			}
			else
				NodeNo = null;
		}
		if (FCode.equalsIgnoreCase("TypeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TypeNo = FValue.trim();
			}
			else
				TypeNo = null;
		}
		if (FCode.equalsIgnoreCase("StatusNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StatusNo = FValue.trim();
			}
			else
				StatusNo = null;
		}
		if (FCode.equalsIgnoreCase("DetailWorkNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DetailWorkNo = FValue.trim();
			}
			else
				DetailWorkNo = null;
		}
		if (FCode.equalsIgnoreCase("PriorityNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PriorityNo = FValue.trim();
			}
			else
				PriorityNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerCardNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerCardNo = FValue.trim();
			}
			else
				CustomerCardNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("ApplyTypeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApplyTypeNo = FValue.trim();
			}
			else
				ApplyTypeNo = null;
		}
		if (FCode.equalsIgnoreCase("ApplyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApplyName = FValue.trim();
			}
			else
				ApplyName = null;
		}
		if (FCode.equalsIgnoreCase("AcceptWayNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AcceptWayNo = FValue.trim();
			}
			else
				AcceptWayNo = null;
		}
		if (FCode.equalsIgnoreCase("AcceptCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AcceptCom = FValue.trim();
			}
			else
				AcceptCom = null;
		}
		if (FCode.equalsIgnoreCase("AcceptorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AcceptorNo = FValue.trim();
			}
			else
				AcceptorNo = null;
		}
		if (FCode.equalsIgnoreCase("AcceptDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AcceptDate = fDate.getDate( FValue );
			}
			else
				AcceptDate = null;
		}
		if (FCode.equalsIgnoreCase("DateLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DateLimit = FValue.trim();
			}
			else
				DateLimit = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("PauseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PauseNo = FValue.trim();
			}
			else
				PauseNo = null;
		}
		if (FCode.equalsIgnoreCase("PauseDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PauseDate = fDate.getDate( FValue );
			}
			else
				PauseDate = null;
		}
		if (FCode.equalsIgnoreCase("WakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				WakeDate = fDate.getDate( FValue );
			}
			else
				WakeDate = null;
		}
		if (FCode.equalsIgnoreCase("WakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WakeTime = FValue.trim();
			}
			else
				WakeTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("UniteTo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UniteTo = FValue.trim();
			}
			else
				UniteTo = null;
		}
		if (FCode.equalsIgnoreCase("Uniting"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Uniting = FValue.trim();
			}
			else
				Uniting = null;
		}
		if (FCode.equalsIgnoreCase("CurrDoing"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CurrDoing = FValue.trim();
			}
			else
				CurrDoing = null;
		}
		if (FCode.equalsIgnoreCase("ScanFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ScanFlag = FValue.trim();
			}
			else
				ScanFlag = null;
		}
		if (FCode.equalsIgnoreCase("InnerSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InnerSource = FValue.trim();
			}
			else
				InnerSource = null;
		}
		if (FCode.equalsIgnoreCase("AgentFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentFlag = FValue.trim();
			}
			else
				AgentFlag = null;
		}
		if (FCode.equalsIgnoreCase("AgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentName = FValue.trim();
			}
			else
				AgentName = null;
		}
		if (FCode.equalsIgnoreCase("AgentIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentIDType = FValue.trim();
			}
			else
				AgentIDType = null;
		}
		if (FCode.equalsIgnoreCase("AgentIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentIDNo = FValue.trim();
			}
			else
				AgentIDNo = null;
		}
		if (FCode.equalsIgnoreCase("AgentIDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AgentIDStartDate = fDate.getDate( FValue );
			}
			else
				AgentIDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("AgentIDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AgentIDEndDate = fDate.getDate( FValue );
			}
			else
				AgentIDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("AgentPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentPhone = FValue.trim();
			}
			else
				AgentPhone = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LGWorkSchema other = (LGWorkSchema)otherObject;
		return
			(WorkNo == null ? other.getWorkNo() == null : WorkNo.equals(other.getWorkNo()))
			&& (AcceptNo == null ? other.getAcceptNo() == null : AcceptNo.equals(other.getAcceptNo()))
			&& (NodeNo == null ? other.getNodeNo() == null : NodeNo.equals(other.getNodeNo()))
			&& (TypeNo == null ? other.getTypeNo() == null : TypeNo.equals(other.getTypeNo()))
			&& (StatusNo == null ? other.getStatusNo() == null : StatusNo.equals(other.getStatusNo()))
			&& (DetailWorkNo == null ? other.getDetailWorkNo() == null : DetailWorkNo.equals(other.getDetailWorkNo()))
			&& (PriorityNo == null ? other.getPriorityNo() == null : PriorityNo.equals(other.getPriorityNo()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (CustomerCardNo == null ? other.getCustomerCardNo() == null : CustomerCardNo.equals(other.getCustomerCardNo()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (ApplyTypeNo == null ? other.getApplyTypeNo() == null : ApplyTypeNo.equals(other.getApplyTypeNo()))
			&& (ApplyName == null ? other.getApplyName() == null : ApplyName.equals(other.getApplyName()))
			&& (AcceptWayNo == null ? other.getAcceptWayNo() == null : AcceptWayNo.equals(other.getAcceptWayNo()))
			&& (AcceptCom == null ? other.getAcceptCom() == null : AcceptCom.equals(other.getAcceptCom()))
			&& (AcceptorNo == null ? other.getAcceptorNo() == null : AcceptorNo.equals(other.getAcceptorNo()))
			&& (AcceptDate == null ? other.getAcceptDate() == null : fDate.getString(AcceptDate).equals(other.getAcceptDate()))
			&& (DateLimit == null ? other.getDateLimit() == null : DateLimit.equals(other.getDateLimit()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (PauseNo == null ? other.getPauseNo() == null : PauseNo.equals(other.getPauseNo()))
			&& (PauseDate == null ? other.getPauseDate() == null : fDate.getString(PauseDate).equals(other.getPauseDate()))
			&& (WakeDate == null ? other.getWakeDate() == null : fDate.getString(WakeDate).equals(other.getWakeDate()))
			&& (WakeTime == null ? other.getWakeTime() == null : WakeTime.equals(other.getWakeTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (UniteTo == null ? other.getUniteTo() == null : UniteTo.equals(other.getUniteTo()))
			&& (Uniting == null ? other.getUniting() == null : Uniting.equals(other.getUniting()))
			&& (CurrDoing == null ? other.getCurrDoing() == null : CurrDoing.equals(other.getCurrDoing()))
			&& (ScanFlag == null ? other.getScanFlag() == null : ScanFlag.equals(other.getScanFlag()))
			&& (InnerSource == null ? other.getInnerSource() == null : InnerSource.equals(other.getInnerSource()))
			&& (AgentFlag == null ? other.getAgentFlag() == null : AgentFlag.equals(other.getAgentFlag()))
			&& (AgentName == null ? other.getAgentName() == null : AgentName.equals(other.getAgentName()))
			&& (AgentIDType == null ? other.getAgentIDType() == null : AgentIDType.equals(other.getAgentIDType()))
			&& (AgentIDNo == null ? other.getAgentIDNo() == null : AgentIDNo.equals(other.getAgentIDNo()))
			&& (AgentIDStartDate == null ? other.getAgentIDStartDate() == null : fDate.getString(AgentIDStartDate).equals(other.getAgentIDStartDate()))
			&& (AgentIDEndDate == null ? other.getAgentIDEndDate() == null : fDate.getString(AgentIDEndDate).equals(other.getAgentIDEndDate()))
			&& (AgentPhone == null ? other.getAgentPhone() == null : AgentPhone.equals(other.getAgentPhone()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("WorkNo") ) {
			return 0;
		}
		if( strFieldName.equals("AcceptNo") ) {
			return 1;
		}
		if( strFieldName.equals("NodeNo") ) {
			return 2;
		}
		if( strFieldName.equals("TypeNo") ) {
			return 3;
		}
		if( strFieldName.equals("StatusNo") ) {
			return 4;
		}
		if( strFieldName.equals("DetailWorkNo") ) {
			return 5;
		}
		if( strFieldName.equals("PriorityNo") ) {
			return 6;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 7;
		}
		if( strFieldName.equals("CustomerCardNo") ) {
			return 8;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 9;
		}
		if( strFieldName.equals("ContNo") ) {
			return 10;
		}
		if( strFieldName.equals("ApplyTypeNo") ) {
			return 11;
		}
		if( strFieldName.equals("ApplyName") ) {
			return 12;
		}
		if( strFieldName.equals("AcceptWayNo") ) {
			return 13;
		}
		if( strFieldName.equals("AcceptCom") ) {
			return 14;
		}
		if( strFieldName.equals("AcceptorNo") ) {
			return 15;
		}
		if( strFieldName.equals("AcceptDate") ) {
			return 16;
		}
		if( strFieldName.equals("DateLimit") ) {
			return 17;
		}
		if( strFieldName.equals("Remark") ) {
			return 18;
		}
		if( strFieldName.equals("PauseNo") ) {
			return 19;
		}
		if( strFieldName.equals("PauseDate") ) {
			return 20;
		}
		if( strFieldName.equals("WakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("WakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("Operator") ) {
			return 23;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 24;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 26;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 27;
		}
		if( strFieldName.equals("UniteTo") ) {
			return 28;
		}
		if( strFieldName.equals("Uniting") ) {
			return 29;
		}
		if( strFieldName.equals("CurrDoing") ) {
			return 30;
		}
		if( strFieldName.equals("ScanFlag") ) {
			return 31;
		}
		if( strFieldName.equals("InnerSource") ) {
			return 32;
		}
		if( strFieldName.equals("AgentFlag") ) {
			return 33;
		}
		if( strFieldName.equals("AgentName") ) {
			return 34;
		}
		if( strFieldName.equals("AgentIDType") ) {
			return 35;
		}
		if( strFieldName.equals("AgentIDNo") ) {
			return 36;
		}
		if( strFieldName.equals("AgentIDStartDate") ) {
			return 37;
		}
		if( strFieldName.equals("AgentIDEndDate") ) {
			return 38;
		}
		if( strFieldName.equals("AgentPhone") ) {
			return 39;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "WorkNo";
				break;
			case 1:
				strFieldName = "AcceptNo";
				break;
			case 2:
				strFieldName = "NodeNo";
				break;
			case 3:
				strFieldName = "TypeNo";
				break;
			case 4:
				strFieldName = "StatusNo";
				break;
			case 5:
				strFieldName = "DetailWorkNo";
				break;
			case 6:
				strFieldName = "PriorityNo";
				break;
			case 7:
				strFieldName = "CustomerNo";
				break;
			case 8:
				strFieldName = "CustomerCardNo";
				break;
			case 9:
				strFieldName = "CustomerName";
				break;
			case 10:
				strFieldName = "ContNo";
				break;
			case 11:
				strFieldName = "ApplyTypeNo";
				break;
			case 12:
				strFieldName = "ApplyName";
				break;
			case 13:
				strFieldName = "AcceptWayNo";
				break;
			case 14:
				strFieldName = "AcceptCom";
				break;
			case 15:
				strFieldName = "AcceptorNo";
				break;
			case 16:
				strFieldName = "AcceptDate";
				break;
			case 17:
				strFieldName = "DateLimit";
				break;
			case 18:
				strFieldName = "Remark";
				break;
			case 19:
				strFieldName = "PauseNo";
				break;
			case 20:
				strFieldName = "PauseDate";
				break;
			case 21:
				strFieldName = "WakeDate";
				break;
			case 22:
				strFieldName = "WakeTime";
				break;
			case 23:
				strFieldName = "Operator";
				break;
			case 24:
				strFieldName = "MakeDate";
				break;
			case 25:
				strFieldName = "MakeTime";
				break;
			case 26:
				strFieldName = "ModifyDate";
				break;
			case 27:
				strFieldName = "ModifyTime";
				break;
			case 28:
				strFieldName = "UniteTo";
				break;
			case 29:
				strFieldName = "Uniting";
				break;
			case 30:
				strFieldName = "CurrDoing";
				break;
			case 31:
				strFieldName = "ScanFlag";
				break;
			case 32:
				strFieldName = "InnerSource";
				break;
			case 33:
				strFieldName = "AgentFlag";
				break;
			case 34:
				strFieldName = "AgentName";
				break;
			case 35:
				strFieldName = "AgentIDType";
				break;
			case 36:
				strFieldName = "AgentIDNo";
				break;
			case 37:
				strFieldName = "AgentIDStartDate";
				break;
			case 38:
				strFieldName = "AgentIDEndDate";
				break;
			case 39:
				strFieldName = "AgentPhone";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("WorkNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AcceptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NodeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TypeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StatusNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DetailWorkNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PriorityNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerCardNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyTypeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AcceptWayNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AcceptCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AcceptorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AcceptDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DateLimit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PauseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PauseDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("WakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("WakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UniteTo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Uniting") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CurrDoing") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ScanFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InnerSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentIDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AgentIDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AgentPhone") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
