/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCHistoryImpartDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LCHistoryImpartSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_3
 * @CreateDate：2005-06-30
 */
public class LCHistoryImpartSchema implements Schema, Cloneable
{
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 印刷号 */
    private String PrtNo;
    /** 保障期间 */
    private int InsuYear;
    /** 保险期间标志 */
    private String InsuYearFlag;
    /** 保障内容 */
    private String InsuContent;
    /** 费率 */
    private double Rate;
    /** 保障 */
    private String EnsureContent;
    /** 参加人数 */
    private int Peoples;
    /** 报销人数 */
    private int RecompensePeoples;
    /** 发生金额 */
    private double OccurMoney;
    /** 报销金额 */
    private double RecompenseMoney;
    /** 未决金额 */
    private double PendingMoney;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 保障起始时间 */
    private Date InsuStartYear;
    /** 保障终止时间 */
    private Date InsuEndYear;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCHistoryImpartSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LCHistoryImpartSchema cloned = (LCHistoryImpartSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getGrpContNo()
    {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getPrtNo()
    {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo)
    {
        PrtNo = aPrtNo;
    }

    public int getInsuYear()
    {
        return InsuYear;
    }

    public void setInsuYear(int aInsuYear)
    {
        InsuYear = aInsuYear;
    }

    public void setInsuYear(String aInsuYear)
    {
        if (aInsuYear != null && !aInsuYear.equals(""))
        {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getInsuYearFlag()
    {
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String aInsuYearFlag)
    {
        InsuYearFlag = aInsuYearFlag;
    }

    public String getInsuContent()
    {
        return InsuContent;
    }

    public void setInsuContent(String aInsuContent)
    {
        InsuContent = aInsuContent;
    }

    public double getRate()
    {
        return Rate;
    }

    public void setRate(double aRate)
    {
        Rate = Arith.round(aRate, 2);
    }

    public void setRate(String aRate)
    {
        if (aRate != null && !aRate.equals(""))
        {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = Arith.round(d, 2);
        }
    }

    public String getEnsureContent()
    {
        return EnsureContent;
    }

    public void setEnsureContent(String aEnsureContent)
    {
        EnsureContent = aEnsureContent;
    }

    public int getPeoples()
    {
        return Peoples;
    }

    public void setPeoples(int aPeoples)
    {
        Peoples = aPeoples;
    }

    public void setPeoples(String aPeoples)
    {
        if (aPeoples != null && !aPeoples.equals(""))
        {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public int getRecompensePeoples()
    {
        return RecompensePeoples;
    }

    public void setRecompensePeoples(int aRecompensePeoples)
    {
        RecompensePeoples = aRecompensePeoples;
    }

    public void setRecompensePeoples(String aRecompensePeoples)
    {
        if (aRecompensePeoples != null && !aRecompensePeoples.equals(""))
        {
            Integer tInteger = new Integer(aRecompensePeoples);
            int i = tInteger.intValue();
            RecompensePeoples = i;
        }
    }

    public double getOccurMoney()
    {
        return OccurMoney;
    }

    public void setOccurMoney(double aOccurMoney)
    {
        OccurMoney = Arith.round(aOccurMoney, 2);
    }

    public void setOccurMoney(String aOccurMoney)
    {
        if (aOccurMoney != null && !aOccurMoney.equals(""))
        {
            Double tDouble = new Double(aOccurMoney);
            double d = tDouble.doubleValue();
            OccurMoney = Arith.round(d, 2);
        }
    }

    public double getRecompenseMoney()
    {
        return RecompenseMoney;
    }

    public void setRecompenseMoney(double aRecompenseMoney)
    {
        RecompenseMoney = Arith.round(aRecompenseMoney, 2);
    }

    public void setRecompenseMoney(String aRecompenseMoney)
    {
        if (aRecompenseMoney != null && !aRecompenseMoney.equals(""))
        {
            Double tDouble = new Double(aRecompenseMoney);
            double d = tDouble.doubleValue();
            RecompenseMoney = Arith.round(d, 2);
        }
    }

    public double getPendingMoney()
    {
        return PendingMoney;
    }

    public void setPendingMoney(double aPendingMoney)
    {
        PendingMoney = Arith.round(aPendingMoney, 2);
    }

    public void setPendingMoney(String aPendingMoney)
    {
        if (aPendingMoney != null && !aPendingMoney.equals(""))
        {
            Double tDouble = new Double(aPendingMoney);
            double d = tDouble.doubleValue();
            PendingMoney = Arith.round(d, 2);
        }
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getInsuStartYear()
    {
        if (InsuStartYear != null)
        {
            return fDate.getString(InsuStartYear);
        }
        else
        {
            return null;
        }
    }

    public void setInsuStartYear(Date aInsuStartYear)
    {
        InsuStartYear = aInsuStartYear;
    }

    public void setInsuStartYear(String aInsuStartYear)
    {
        if (aInsuStartYear != null && !aInsuStartYear.equals(""))
        {
            InsuStartYear = fDate.getDate(aInsuStartYear);
        }
        else
        {
            InsuStartYear = null;
        }
    }

    public String getInsuEndYear()
    {
        if (InsuEndYear != null)
        {
            return fDate.getString(InsuEndYear);
        }
        else
        {
            return null;
        }
    }

    public void setInsuEndYear(Date aInsuEndYear)
    {
        InsuEndYear = aInsuEndYear;
    }

    public void setInsuEndYear(String aInsuEndYear)
    {
        if (aInsuEndYear != null && !aInsuEndYear.equals(""))
        {
            InsuEndYear = fDate.getDate(aInsuEndYear);
        }
        else
        {
            InsuEndYear = null;
        }
    }


    /**
     * 使用另外一个 LCHistoryImpartSchema 对象给 Schema 赋值
     * @param: aLCHistoryImpartSchema LCHistoryImpartSchema
     **/
    public void setSchema(LCHistoryImpartSchema aLCHistoryImpartSchema)
    {
        this.SerialNo = aLCHistoryImpartSchema.getSerialNo();
        this.GrpContNo = aLCHistoryImpartSchema.getGrpContNo();
        this.PrtNo = aLCHistoryImpartSchema.getPrtNo();
        this.InsuYear = aLCHistoryImpartSchema.getInsuYear();
        this.InsuYearFlag = aLCHistoryImpartSchema.getInsuYearFlag();
        this.InsuContent = aLCHistoryImpartSchema.getInsuContent();
        this.Rate = aLCHistoryImpartSchema.getRate();
        this.EnsureContent = aLCHistoryImpartSchema.getEnsureContent();
        this.Peoples = aLCHistoryImpartSchema.getPeoples();
        this.RecompensePeoples = aLCHistoryImpartSchema.getRecompensePeoples();
        this.OccurMoney = aLCHistoryImpartSchema.getOccurMoney();
        this.RecompenseMoney = aLCHistoryImpartSchema.getRecompenseMoney();
        this.PendingMoney = aLCHistoryImpartSchema.getPendingMoney();
        this.MakeDate = fDate.getDate(aLCHistoryImpartSchema.getMakeDate());
        this.MakeTime = aLCHistoryImpartSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCHistoryImpartSchema.getModifyDate());
        this.ModifyTime = aLCHistoryImpartSchema.getModifyTime();
        this.InsuStartYear = fDate.getDate(aLCHistoryImpartSchema.
                                           getInsuStartYear());
        this.InsuEndYear = fDate.getDate(aLCHistoryImpartSchema.getInsuEndYear());
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("PrtNo") == null)
            {
                this.PrtNo = null;
            }
            else
            {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            this.InsuYear = rs.getInt("InsuYear");
            if (rs.getString("InsuYearFlag") == null)
            {
                this.InsuYearFlag = null;
            }
            else
            {
                this.InsuYearFlag = rs.getString("InsuYearFlag").trim();
            }

            if (rs.getString("InsuContent") == null)
            {
                this.InsuContent = null;
            }
            else
            {
                this.InsuContent = rs.getString("InsuContent").trim();
            }

            this.Rate = rs.getDouble("Rate");
            if (rs.getString("EnsureContent") == null)
            {
                this.EnsureContent = null;
            }
            else
            {
                this.EnsureContent = rs.getString("EnsureContent").trim();
            }

            this.Peoples = rs.getInt("Peoples");
            this.RecompensePeoples = rs.getInt("RecompensePeoples");
            this.OccurMoney = rs.getDouble("OccurMoney");
            this.RecompenseMoney = rs.getDouble("RecompenseMoney");
            this.PendingMoney = rs.getDouble("PendingMoney");
            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.InsuStartYear = rs.getDate("InsuStartYear");
            this.InsuEndYear = rs.getDate("InsuEndYear");
        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LCHistoryImpart表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCHistoryImpartSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCHistoryImpartSchema getSchema()
    {
        LCHistoryImpartSchema aLCHistoryImpartSchema = new
                LCHistoryImpartSchema();
        aLCHistoryImpartSchema.setSchema(this);
        return aLCHistoryImpartSchema;
    }

    public LCHistoryImpartDB getDB()
    {
        LCHistoryImpartDB aDBOper = new LCHistoryImpartDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCHistoryImpart描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuYear));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuYearFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuContent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Rate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EnsureContent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RecompensePeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OccurMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RecompenseMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PendingMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(InsuStartYear)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(InsuEndYear)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCHistoryImpart>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            InsuYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            InsuContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    7, SysConst.PACKAGESPILTER))).doubleValue();
            EnsureContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            Peoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            RecompensePeoples = new Integer(ChgData.chgNumericStr(StrTool.
                    getStr(strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            OccurMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            RecompenseMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            PendingMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            InsuStartYear = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            InsuEndYear = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCHistoryImpartSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("PrtNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("InsuYear"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equals("InsuYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equals("InsuContent"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuContent));
        }
        if (FCode.equals("Rate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
        }
        if (FCode.equals("EnsureContent"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnsureContent));
        }
        if (FCode.equals("Peoples"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equals("RecompensePeoples"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecompensePeoples));
        }
        if (FCode.equals("OccurMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccurMoney));
        }
        if (FCode.equals("RecompenseMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecompenseMoney));
        }
        if (FCode.equals("PendingMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PendingMoney));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("InsuStartYear"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getInsuStartYear()));
        }
        if (FCode.equals("InsuEndYear"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getInsuEndYear()));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 3:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(InsuContent);
                break;
            case 6:
                strFieldValue = String.valueOf(Rate);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(EnsureContent);
                break;
            case 8:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 9:
                strFieldValue = String.valueOf(RecompensePeoples);
                break;
            case 10:
                strFieldValue = String.valueOf(OccurMoney);
                break;
            case 11:
                strFieldValue = String.valueOf(RecompenseMoney);
                break;
            case 12:
                strFieldValue = String.valueOf(PendingMoney);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInsuStartYear()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInsuEndYear()));
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
            {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
            {
                InsuYearFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuContent = FValue.trim();
            }
            else
            {
                InsuContent = null;
            }
        }
        if (FCode.equalsIgnoreCase("Rate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equalsIgnoreCase("EnsureContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EnsureContent = FValue.trim();
            }
            else
            {
                EnsureContent = null;
            }
        }
        if (FCode.equalsIgnoreCase("Peoples"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RecompensePeoples"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RecompensePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OccurMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                OccurMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("RecompenseMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RecompenseMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("PendingMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PendingMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuStartYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuStartYear = fDate.getDate(FValue);
            }
            else
            {
                InsuStartYear = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuEndYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuEndYear = fDate.getDate(FValue);
            }
            else
            {
                InsuEndYear = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCHistoryImpartSchema other = (LCHistoryImpartSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && GrpContNo.equals(other.getGrpContNo())
                && PrtNo.equals(other.getPrtNo())
                && InsuYear == other.getInsuYear()
                && InsuYearFlag.equals(other.getInsuYearFlag())
                && InsuContent.equals(other.getInsuContent())
                && Rate == other.getRate()
                && EnsureContent.equals(other.getEnsureContent())
                && Peoples == other.getPeoples()
                && RecompensePeoples == other.getRecompensePeoples()
                && OccurMoney == other.getOccurMoney()
                && RecompenseMoney == other.getRecompenseMoney()
                && PendingMoney == other.getPendingMoney()
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(InsuStartYear).equals(other.getInsuStartYear())
                && fDate.getString(InsuEndYear).equals(other.getInsuEndYear());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return 2;
        }
        if (strFieldName.equals("InsuYear"))
        {
            return 3;
        }
        if (strFieldName.equals("InsuYearFlag"))
        {
            return 4;
        }
        if (strFieldName.equals("InsuContent"))
        {
            return 5;
        }
        if (strFieldName.equals("Rate"))
        {
            return 6;
        }
        if (strFieldName.equals("EnsureContent"))
        {
            return 7;
        }
        if (strFieldName.equals("Peoples"))
        {
            return 8;
        }
        if (strFieldName.equals("RecompensePeoples"))
        {
            return 9;
        }
        if (strFieldName.equals("OccurMoney"))
        {
            return 10;
        }
        if (strFieldName.equals("RecompenseMoney"))
        {
            return 11;
        }
        if (strFieldName.equals("PendingMoney"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 16;
        }
        if (strFieldName.equals("InsuStartYear"))
        {
            return 17;
        }
        if (strFieldName.equals("InsuEndYear"))
        {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "GrpContNo";
                break;
            case 2:
                strFieldName = "PrtNo";
                break;
            case 3:
                strFieldName = "InsuYear";
                break;
            case 4:
                strFieldName = "InsuYearFlag";
                break;
            case 5:
                strFieldName = "InsuContent";
                break;
            case 6:
                strFieldName = "Rate";
                break;
            case 7:
                strFieldName = "EnsureContent";
                break;
            case 8:
                strFieldName = "Peoples";
                break;
            case 9:
                strFieldName = "RecompensePeoples";
                break;
            case 10:
                strFieldName = "OccurMoney";
                break;
            case 11:
                strFieldName = "RecompenseMoney";
                break;
            case 12:
                strFieldName = "PendingMoney";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "InsuStartYear";
                break;
            case 18:
                strFieldName = "InsuEndYear";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("InsuYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Rate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EnsureContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Peoples"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RecompensePeoples"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("OccurMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RecompenseMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PendingMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuStartYear"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InsuEndYear"))
        {
            return Schema.TYPE_DATE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
