/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLLockDB;

/*
 * <p>ClassName: LLLockSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-05-11
 */
public class LLLockSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String GrpContNo;
	/** 批次号 */
	private String Rgtno;
	/** 项目号 */
	private String ProjectNo;
	/** 项目名称 */
	private String ProjectName;
	/** 保单状态 */
	private String State;
	/** 锁定/解锁日期 */
	private Date LockDate;
	/** 锁定/解锁时间 */
	private String LockTime;
	/** 保单生效日期 */
	private Date Cvalidate;
	/** 项目年度实收保费 */
	private double SumGetFee;
	/** 项目年度累计赔付 */
	private double SumPayFee;
	/** 本次赔款 */
	private double OnePayFee;
	/** 操作员 */
	private String Operator;
	/** 管理机构 */
	private String Managecom;
	/** 其他字段1 */
	private String Other1;
	/** 其他字段2 */
	private String Other2;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLLockSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "GrpContNo";
		pk[1] = "Rgtno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLLockSchema cloned = (LLLockSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getRgtno()
	{
		return Rgtno;
	}
	public void setRgtno(String aRgtno)
	{
		Rgtno = aRgtno;
	}
	public String getProjectNo()
	{
		return ProjectNo;
	}
	public void setProjectNo(String aProjectNo)
	{
		ProjectNo = aProjectNo;
	}
	public String getProjectName()
	{
		return ProjectName;
	}
	public void setProjectName(String aProjectName)
	{
		ProjectName = aProjectName;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getLockDate()
	{
		if( LockDate != null )
			return fDate.getString(LockDate);
		else
			return null;
	}
	public void setLockDate(Date aLockDate)
	{
		LockDate = aLockDate;
	}
	public void setLockDate(String aLockDate)
	{
		if (aLockDate != null && !aLockDate.equals("") )
		{
			LockDate = fDate.getDate( aLockDate );
		}
		else
			LockDate = null;
	}

	public String getLockTime()
	{
		return LockTime;
	}
	public void setLockTime(String aLockTime)
	{
		LockTime = aLockTime;
	}
	public String getCvalidate()
	{
		if( Cvalidate != null )
			return fDate.getString(Cvalidate);
		else
			return null;
	}
	public void setCvalidate(Date aCvalidate)
	{
		Cvalidate = aCvalidate;
	}
	public void setCvalidate(String aCvalidate)
	{
		if (aCvalidate != null && !aCvalidate.equals("") )
		{
			Cvalidate = fDate.getDate( aCvalidate );
		}
		else
			Cvalidate = null;
	}

	public double getSumGetFee()
	{
		return SumGetFee;
	}
	public void setSumGetFee(double aSumGetFee)
	{
		SumGetFee = Arith.round(aSumGetFee,2);
	}
	public void setSumGetFee(String aSumGetFee)
	{
		if (aSumGetFee != null && !aSumGetFee.equals(""))
		{
			Double tDouble = new Double(aSumGetFee);
			double d = tDouble.doubleValue();
                SumGetFee = Arith.round(d,2);
		}
	}

	public double getSumPayFee()
	{
		return SumPayFee;
	}
	public void setSumPayFee(double aSumPayFee)
	{
		SumPayFee = Arith.round(aSumPayFee,2);
	}
	public void setSumPayFee(String aSumPayFee)
	{
		if (aSumPayFee != null && !aSumPayFee.equals(""))
		{
			Double tDouble = new Double(aSumPayFee);
			double d = tDouble.doubleValue();
                SumPayFee = Arith.round(d,2);
		}
	}

	public double getOnePayFee()
	{
		return OnePayFee;
	}
	public void setOnePayFee(double aOnePayFee)
	{
		OnePayFee = Arith.round(aOnePayFee,2);
	}
	public void setOnePayFee(String aOnePayFee)
	{
		if (aOnePayFee != null && !aOnePayFee.equals(""))
		{
			Double tDouble = new Double(aOnePayFee);
			double d = tDouble.doubleValue();
                OnePayFee = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManagecom()
	{
		return Managecom;
	}
	public void setManagecom(String aManagecom)
	{
		Managecom = aManagecom;
	}
	public String getOther1()
	{
		return Other1;
	}
	public void setOther1(String aOther1)
	{
		Other1 = aOther1;
	}
	public String getOther2()
	{
		return Other2;
	}
	public void setOther2(String aOther2)
	{
		Other2 = aOther2;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLLockSchema 对象给 Schema 赋值
	* @param: aLLLockSchema LLLockSchema
	**/
	public void setSchema(LLLockSchema aLLLockSchema)
	{
		this.GrpContNo = aLLLockSchema.getGrpContNo();
		this.Rgtno = aLLLockSchema.getRgtno();
		this.ProjectNo = aLLLockSchema.getProjectNo();
		this.ProjectName = aLLLockSchema.getProjectName();
		this.State = aLLLockSchema.getState();
		this.LockDate = fDate.getDate( aLLLockSchema.getLockDate());
		this.LockTime = aLLLockSchema.getLockTime();
		this.Cvalidate = fDate.getDate( aLLLockSchema.getCvalidate());
		this.SumGetFee = aLLLockSchema.getSumGetFee();
		this.SumPayFee = aLLLockSchema.getSumPayFee();
		this.OnePayFee = aLLLockSchema.getOnePayFee();
		this.Operator = aLLLockSchema.getOperator();
		this.Managecom = aLLLockSchema.getManagecom();
		this.Other1 = aLLLockSchema.getOther1();
		this.Other2 = aLLLockSchema.getOther2();
		this.MakeDate = fDate.getDate( aLLLockSchema.getMakeDate());
		this.MakeTime = aLLLockSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLLockSchema.getModifyDate());
		this.ModifyTime = aLLLockSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("Rgtno") == null )
				this.Rgtno = null;
			else
				this.Rgtno = rs.getString("Rgtno").trim();

			if( rs.getString("ProjectNo") == null )
				this.ProjectNo = null;
			else
				this.ProjectNo = rs.getString("ProjectNo").trim();

			if( rs.getString("ProjectName") == null )
				this.ProjectName = null;
			else
				this.ProjectName = rs.getString("ProjectName").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			this.LockDate = rs.getDate("LockDate");
			if( rs.getString("LockTime") == null )
				this.LockTime = null;
			else
				this.LockTime = rs.getString("LockTime").trim();

			this.Cvalidate = rs.getDate("Cvalidate");
			this.SumGetFee = rs.getDouble("SumGetFee");
			this.SumPayFee = rs.getDouble("SumPayFee");
			this.OnePayFee = rs.getDouble("OnePayFee");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("Managecom") == null )
				this.Managecom = null;
			else
				this.Managecom = rs.getString("Managecom").trim();

			if( rs.getString("Other1") == null )
				this.Other1 = null;
			else
				this.Other1 = rs.getString("Other1").trim();

			if( rs.getString("Other2") == null )
				this.Other2 = null;
			else
				this.Other2 = rs.getString("Other2").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLLock表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLLockSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLLockSchema getSchema()
	{
		LLLockSchema aLLLockSchema = new LLLockSchema();
		aLLLockSchema.setSchema(this);
		return aLLLockSchema;
	}

	public LLLockDB getDB()
	{
		LLLockDB aDBOper = new LLLockDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLLock描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Rgtno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LockDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LockTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Cvalidate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumGetFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumPayFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OnePayFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLLock>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Rgtno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ProjectNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ProjectName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			LockDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			LockTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Cvalidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			SumGetFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			SumPayFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			OnePayFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Other1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Other2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLLockSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("Rgtno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rgtno));
		}
		if (FCode.equals("ProjectNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectNo));
		}
		if (FCode.equals("ProjectName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectName));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("LockDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLockDate()));
		}
		if (FCode.equals("LockTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LockTime));
		}
		if (FCode.equals("Cvalidate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCvalidate()));
		}
		if (FCode.equals("SumGetFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumGetFee));
		}
		if (FCode.equals("SumPayFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumPayFee));
		}
		if (FCode.equals("OnePayFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OnePayFee));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
		}
		if (FCode.equals("Other1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other1));
		}
		if (FCode.equals("Other2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other2));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Rgtno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ProjectNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ProjectName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLockDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(LockTime);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCvalidate()));
				break;
			case 8:
				strFieldValue = String.valueOf(SumGetFee);
				break;
			case 9:
				strFieldValue = String.valueOf(SumPayFee);
				break;
			case 10:
				strFieldValue = String.valueOf(OnePayFee);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Managecom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Other1);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Other2);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("Rgtno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Rgtno = FValue.trim();
			}
			else
				Rgtno = null;
		}
		if (FCode.equalsIgnoreCase("ProjectNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectNo = FValue.trim();
			}
			else
				ProjectNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectName = FValue.trim();
			}
			else
				ProjectName = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("LockDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LockDate = fDate.getDate( FValue );
			}
			else
				LockDate = null;
		}
		if (FCode.equalsIgnoreCase("LockTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LockTime = FValue.trim();
			}
			else
				LockTime = null;
		}
		if (FCode.equalsIgnoreCase("Cvalidate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Cvalidate = fDate.getDate( FValue );
			}
			else
				Cvalidate = null;
		}
		if (FCode.equalsIgnoreCase("SumGetFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumGetFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumPayFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumPayFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("OnePayFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OnePayFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Managecom = FValue.trim();
			}
			else
				Managecom = null;
		}
		if (FCode.equalsIgnoreCase("Other1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other1 = FValue.trim();
			}
			else
				Other1 = null;
		}
		if (FCode.equalsIgnoreCase("Other2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other2 = FValue.trim();
			}
			else
				Other2 = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLLockSchema other = (LLLockSchema)otherObject;
		return
			(GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (Rgtno == null ? other.getRgtno() == null : Rgtno.equals(other.getRgtno()))
			&& (ProjectNo == null ? other.getProjectNo() == null : ProjectNo.equals(other.getProjectNo()))
			&& (ProjectName == null ? other.getProjectName() == null : ProjectName.equals(other.getProjectName()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (LockDate == null ? other.getLockDate() == null : fDate.getString(LockDate).equals(other.getLockDate()))
			&& (LockTime == null ? other.getLockTime() == null : LockTime.equals(other.getLockTime()))
			&& (Cvalidate == null ? other.getCvalidate() == null : fDate.getString(Cvalidate).equals(other.getCvalidate()))
			&& SumGetFee == other.getSumGetFee()
			&& SumPayFee == other.getSumPayFee()
			&& OnePayFee == other.getOnePayFee()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Managecom == null ? other.getManagecom() == null : Managecom.equals(other.getManagecom()))
			&& (Other1 == null ? other.getOther1() == null : Other1.equals(other.getOther1()))
			&& (Other2 == null ? other.getOther2() == null : Other2.equals(other.getOther2()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("Rgtno") ) {
			return 1;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return 2;
		}
		if( strFieldName.equals("ProjectName") ) {
			return 3;
		}
		if( strFieldName.equals("State") ) {
			return 4;
		}
		if( strFieldName.equals("LockDate") ) {
			return 5;
		}
		if( strFieldName.equals("LockTime") ) {
			return 6;
		}
		if( strFieldName.equals("Cvalidate") ) {
			return 7;
		}
		if( strFieldName.equals("SumGetFee") ) {
			return 8;
		}
		if( strFieldName.equals("SumPayFee") ) {
			return 9;
		}
		if( strFieldName.equals("OnePayFee") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("Managecom") ) {
			return 12;
		}
		if( strFieldName.equals("Other1") ) {
			return 13;
		}
		if( strFieldName.equals("Other2") ) {
			return 14;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 15;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "Rgtno";
				break;
			case 2:
				strFieldName = "ProjectNo";
				break;
			case 3:
				strFieldName = "ProjectName";
				break;
			case 4:
				strFieldName = "State";
				break;
			case 5:
				strFieldName = "LockDate";
				break;
			case 6:
				strFieldName = "LockTime";
				break;
			case 7:
				strFieldName = "Cvalidate";
				break;
			case 8:
				strFieldName = "SumGetFee";
				break;
			case 9:
				strFieldName = "SumPayFee";
				break;
			case 10:
				strFieldName = "OnePayFee";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "Managecom";
				break;
			case 13:
				strFieldName = "Other1";
				break;
			case 14:
				strFieldName = "Other2";
				break;
			case 15:
				strFieldName = "MakeDate";
				break;
			case 16:
				strFieldName = "MakeTime";
				break;
			case 17:
				strFieldName = "ModifyDate";
				break;
			case 18:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rgtno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LockDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LockTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Cvalidate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SumGetFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumPayFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OnePayFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Other1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Other2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
