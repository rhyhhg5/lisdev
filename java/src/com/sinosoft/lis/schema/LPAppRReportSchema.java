/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPAppRReportDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LPAppRReportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-12
 */
public class LPAppRReportSchema implements Schema
{
    // @Field
    /** 保全申请号 */
    private String EdorAcceptNo;
    /** 批单号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 打印流水号 */
    private String PrtSeq;
    /** 投保人编码 */
    private String AppntNo;
    /** 投保人 */
    private String AppntName;
    /** 生调人编码 */
    private String CustomerNo;
    /** 生调人姓名 */
    private String Name;
    /** 管理机构 */
    private String ManageCom;
    /** 生调内容 */
    private String Content;
    /** 回复内容 */
    private String ReplyContent;
    /** 回复标记 */
    private String ReplyFlag;
    /** 操作员 */
    private String Operator;
    /** 回复人 */
    private String ReplyOperator;
    /** 回复日期 */
    private Date ReplyDate;
    /** 回复时间 */
    private String ReplyTime;
    /** 录入日期 */
    private Date MakeDate;
    /** 录入时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPAppRReportSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "EdorAcceptNo";
        pk[1] = "PrtSeq";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorAcceptNo()
    {
        if (SysConst.CHANGECHARSET && EdorAcceptNo != null &&
            !EdorAcceptNo.equals(""))
        {
            EdorAcceptNo = StrTool.unicodeToGBK(EdorAcceptNo);
        }
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String aEdorAcceptNo)
    {
        EdorAcceptNo = aEdorAcceptNo;
    }

    public String getEdorNo()
    {
        if (SysConst.CHANGECHARSET && EdorNo != null && !EdorNo.equals(""))
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getGrpContNo()
    {
        if (SysConst.CHANGECHARSET && GrpContNo != null && !GrpContNo.equals(""))
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getContNo()
    {
        if (SysConst.CHANGECHARSET && ContNo != null && !ContNo.equals(""))
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getProposalContNo()
    {
        if (SysConst.CHANGECHARSET && ProposalContNo != null &&
            !ProposalContNo.equals(""))
        {
            ProposalContNo = StrTool.unicodeToGBK(ProposalContNo);
        }
        return ProposalContNo;
    }

    public void setProposalContNo(String aProposalContNo)
    {
        ProposalContNo = aProposalContNo;
    }

    public String getPrtSeq()
    {
        if (SysConst.CHANGECHARSET && PrtSeq != null && !PrtSeq.equals(""))
        {
            PrtSeq = StrTool.unicodeToGBK(PrtSeq);
        }
        return PrtSeq;
    }

    public void setPrtSeq(String aPrtSeq)
    {
        PrtSeq = aPrtSeq;
    }

    public String getAppntNo()
    {
        if (SysConst.CHANGECHARSET && AppntNo != null && !AppntNo.equals(""))
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getAppntName()
    {
        if (SysConst.CHANGECHARSET && AppntName != null && !AppntName.equals(""))
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public String getCustomerNo()
    {
        if (SysConst.CHANGECHARSET && CustomerNo != null &&
            !CustomerNo.equals(""))
        {
            CustomerNo = StrTool.unicodeToGBK(CustomerNo);
        }
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getName()
    {
        if (SysConst.CHANGECHARSET && Name != null && !Name.equals(""))
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getContent()
    {
        if (SysConst.CHANGECHARSET && Content != null && !Content.equals(""))
        {
            Content = StrTool.unicodeToGBK(Content);
        }
        return Content;
    }

    public void setContent(String aContent)
    {
        Content = aContent;
    }

    public String getReplyContent()
    {
        if (SysConst.CHANGECHARSET && ReplyContent != null &&
            !ReplyContent.equals(""))
        {
            ReplyContent = StrTool.unicodeToGBK(ReplyContent);
        }
        return ReplyContent;
    }

    public void setReplyContent(String aReplyContent)
    {
        ReplyContent = aReplyContent;
    }

    public String getReplyFlag()
    {
        if (SysConst.CHANGECHARSET && ReplyFlag != null && !ReplyFlag.equals(""))
        {
            ReplyFlag = StrTool.unicodeToGBK(ReplyFlag);
        }
        return ReplyFlag;
    }

    public void setReplyFlag(String aReplyFlag)
    {
        ReplyFlag = aReplyFlag;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getReplyOperator()
    {
        if (SysConst.CHANGECHARSET && ReplyOperator != null &&
            !ReplyOperator.equals(""))
        {
            ReplyOperator = StrTool.unicodeToGBK(ReplyOperator);
        }
        return ReplyOperator;
    }

    public void setReplyOperator(String aReplyOperator)
    {
        ReplyOperator = aReplyOperator;
    }

    public String getReplyDate()
    {
        if (ReplyDate != null)
        {
            return fDate.getString(ReplyDate);
        }
        else
        {
            return null;
        }
    }

    public void setReplyDate(Date aReplyDate)
    {
        ReplyDate = aReplyDate;
    }

    public void setReplyDate(String aReplyDate)
    {
        if (aReplyDate != null && !aReplyDate.equals(""))
        {
            ReplyDate = fDate.getDate(aReplyDate);
        }
        else
        {
            ReplyDate = null;
        }
    }

    public String getReplyTime()
    {
        if (SysConst.CHANGECHARSET && ReplyTime != null && !ReplyTime.equals(""))
        {
            ReplyTime = StrTool.unicodeToGBK(ReplyTime);
        }
        return ReplyTime;
    }

    public void setReplyTime(String aReplyTime)
    {
        ReplyTime = aReplyTime;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LPAppRReportSchema 对象给 Schema 赋值
     * @param: aLPAppRReportSchema LPAppRReportSchema
     **/
    public void setSchema(LPAppRReportSchema aLPAppRReportSchema)
    {
        this.EdorAcceptNo = aLPAppRReportSchema.getEdorAcceptNo();
        this.EdorNo = aLPAppRReportSchema.getEdorNo();
        this.GrpContNo = aLPAppRReportSchema.getGrpContNo();
        this.ContNo = aLPAppRReportSchema.getContNo();
        this.ProposalContNo = aLPAppRReportSchema.getProposalContNo();
        this.PrtSeq = aLPAppRReportSchema.getPrtSeq();
        this.AppntNo = aLPAppRReportSchema.getAppntNo();
        this.AppntName = aLPAppRReportSchema.getAppntName();
        this.CustomerNo = aLPAppRReportSchema.getCustomerNo();
        this.Name = aLPAppRReportSchema.getName();
        this.ManageCom = aLPAppRReportSchema.getManageCom();
        this.Content = aLPAppRReportSchema.getContent();
        this.ReplyContent = aLPAppRReportSchema.getReplyContent();
        this.ReplyFlag = aLPAppRReportSchema.getReplyFlag();
        this.Operator = aLPAppRReportSchema.getOperator();
        this.ReplyOperator = aLPAppRReportSchema.getReplyOperator();
        this.ReplyDate = fDate.getDate(aLPAppRReportSchema.getReplyDate());
        this.ReplyTime = aLPAppRReportSchema.getReplyTime();
        this.MakeDate = fDate.getDate(aLPAppRReportSchema.getMakeDate());
        this.MakeTime = aLPAppRReportSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPAppRReportSchema.getModifyDate());
        this.ModifyTime = aLPAppRReportSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString(1) == null)
            {
                this.EdorAcceptNo = null;
            }
            else
            {
                this.EdorAcceptNo = rs.getString(1).trim();
            }

            if (rs.getString(2) == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString(2).trim();
            }

            if (rs.getString(3) == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString(3).trim();
            }

            if (rs.getString(4) == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString(4).trim();
            }

            if (rs.getString(5) == null)
            {
                this.ProposalContNo = null;
            }
            else
            {
                this.ProposalContNo = rs.getString(5).trim();
            }

            if (rs.getString(6) == null)
            {
                this.PrtSeq = null;
            }
            else
            {
                this.PrtSeq = rs.getString(6).trim();
            }

            if (rs.getString(7) == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString(7).trim();
            }

            if (rs.getString(8) == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString(8).trim();
            }

            if (rs.getString(9) == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString(9).trim();
            }

            if (rs.getString(10) == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString(10).trim();
            }

            if (rs.getString(11) == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString(11).trim();
            }

            if (rs.getString(12) == null)
            {
                this.Content = null;
            }
            else
            {
                this.Content = rs.getString(12).trim();
            }

            if (rs.getString(13) == null)
            {
                this.ReplyContent = null;
            }
            else
            {
                this.ReplyContent = rs.getString(13).trim();
            }

            if (rs.getString(14) == null)
            {
                this.ReplyFlag = null;
            }
            else
            {
                this.ReplyFlag = rs.getString(14).trim();
            }

            if (rs.getString(15) == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString(15).trim();
            }

            if (rs.getString(16) == null)
            {
                this.ReplyOperator = null;
            }
            else
            {
                this.ReplyOperator = rs.getString(16).trim();
            }

            this.ReplyDate = rs.getDate(17);
            if (rs.getString(18) == null)
            {
                this.ReplyTime = null;
            }
            else
            {
                this.ReplyTime = rs.getString(18).trim();
            }

            this.MakeDate = rs.getDate(19);
            if (rs.getString(20) == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString(20).trim();
            }

            this.ModifyDate = rs.getDate(21);
            if (rs.getString(22) == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString(22).trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPAppRReportSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPAppRReportSchema getSchema()
    {
        LPAppRReportSchema aLPAppRReportSchema = new LPAppRReportSchema();
        aLPAppRReportSchema.setSchema(this);
        return aLPAppRReportSchema;
    }

    public LPAppRReportDB getDB()
    {
        LPAppRReportDB aDBOper = new LPAppRReportDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPAppRReport描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(EdorAcceptNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ContNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ProposalContNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(PrtSeq)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AppntName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CustomerNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Name)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Content)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReplyContent)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReplyFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReplyOperator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ReplyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReplyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPAppRReport>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                            SysConst.PACKAGESPILTER);
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                  SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            Content = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                     SysConst.PACKAGESPILTER);
            ReplyContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            ReplyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ReplyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                           SysConst.PACKAGESPILTER);
            ReplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ReplyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPAppRReportSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorAcceptNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("ProposalContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equals("PrtSeq"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Content"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Content));
        }
        if (FCode.equals("ReplyContent"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyContent));
        }
        if (FCode.equals("ReplyFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("ReplyOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyOperator));
        }
        if (FCode.equals("ReplyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getReplyDate()));
        }
        if (FCode.equals("ReplyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyTime));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PrtSeq);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Content);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ReplyContent);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ReplyFlag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ReplyOperator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReplyDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ReplyTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorAcceptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
            {
                EdorAcceptNo = null;
            }
        }
        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("ProposalContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
            {
                ProposalContNo = null;
            }
        }
        if (FCode.equals("PrtSeq"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
            {
                PrtSeq = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("Content"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Content = FValue.trim();
            }
            else
            {
                Content = null;
            }
        }
        if (FCode.equals("ReplyContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyContent = FValue.trim();
            }
            else
            {
                ReplyContent = null;
            }
        }
        if (FCode.equals("ReplyFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyFlag = FValue.trim();
            }
            else
            {
                ReplyFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("ReplyOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyOperator = FValue.trim();
            }
            else
            {
                ReplyOperator = null;
            }
        }
        if (FCode.equals("ReplyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyDate = fDate.getDate(FValue);
            }
            else
            {
                ReplyDate = null;
            }
        }
        if (FCode.equals("ReplyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyTime = FValue.trim();
            }
            else
            {
                ReplyTime = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPAppRReportSchema other = (LPAppRReportSchema) otherObject;
        return
                EdorAcceptNo.equals(other.getEdorAcceptNo())
                && EdorNo.equals(other.getEdorNo())
                && GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && ProposalContNo.equals(other.getProposalContNo())
                && PrtSeq.equals(other.getPrtSeq())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && CustomerNo.equals(other.getCustomerNo())
                && Name.equals(other.getName())
                && ManageCom.equals(other.getManageCom())
                && Content.equals(other.getContent())
                && ReplyContent.equals(other.getReplyContent())
                && ReplyFlag.equals(other.getReplyFlag())
                && Operator.equals(other.getOperator())
                && ReplyOperator.equals(other.getReplyOperator())
                && fDate.getString(ReplyDate).equals(other.getReplyDate())
                && ReplyTime.equals(other.getReplyTime())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorAcceptNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return 1;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 3;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return 4;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return 5;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 6;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 7;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 8;
        }
        if (strFieldName.equals("Name"))
        {
            return 9;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 10;
        }
        if (strFieldName.equals("Content"))
        {
            return 11;
        }
        if (strFieldName.equals("ReplyContent"))
        {
            return 12;
        }
        if (strFieldName.equals("ReplyFlag"))
        {
            return 13;
        }
        if (strFieldName.equals("Operator"))
        {
            return 14;
        }
        if (strFieldName.equals("ReplyOperator"))
        {
            return 15;
        }
        if (strFieldName.equals("ReplyDate"))
        {
            return 16;
        }
        if (strFieldName.equals("ReplyTime"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorAcceptNo";
                break;
            case 1:
                strFieldName = "EdorNo";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "ProposalContNo";
                break;
            case 5:
                strFieldName = "PrtSeq";
                break;
            case 6:
                strFieldName = "AppntNo";
                break;
            case 7:
                strFieldName = "AppntName";
                break;
            case 8:
                strFieldName = "CustomerNo";
                break;
            case 9:
                strFieldName = "Name";
                break;
            case 10:
                strFieldName = "ManageCom";
                break;
            case 11:
                strFieldName = "Content";
                break;
            case 12:
                strFieldName = "ReplyContent";
                break;
            case 13:
                strFieldName = "ReplyFlag";
                break;
            case 14:
                strFieldName = "Operator";
                break;
            case 15:
                strFieldName = "ReplyOperator";
                break;
            case 16:
                strFieldName = "ReplyDate";
                break;
            case 17:
                strFieldName = "ReplyTime";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorAcceptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Content"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ReplyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
