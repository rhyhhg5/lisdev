/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.CUSTOMERRESULTDB;

/*
 * <p>ClassName: CUSTOMERRESULTSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 平台客户返回
 * @CreateDate：2011-12-27
 */
public class CUSTOMERRESULTSchema implements Schema, Cloneable
{
	// @Field
	/** 公司代码 */
	private String CompanyCode;
	/** 姓名 */
	private String Name;
	/** 性别 */
	private String Gender;
	/** 生日 */
	private String Birthday;
	/** 证件类别 */
	private String CritType;
	/** 证件号码 */
	private String CritCode;
	/** 客户号 */
	private String Customerno;
	/** 平台返回客户号 */
	private String Customer_sequence_no;
	/** 错误信息 */
	private String ErrorMessage;

	public static final int FIELDNUM = 9;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public CUSTOMERRESULTSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "CritCode";
		pk[1] = "Customerno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		CUSTOMERRESULTSchema cloned = (CUSTOMERRESULTSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCompanyCode()
	{
		return CompanyCode;
	}
	public void setCompanyCode(String aCompanyCode)
	{
		CompanyCode = aCompanyCode;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getGender()
	{
		return Gender;
	}
	public void setGender(String aGender)
	{
		Gender = aGender;
	}
	public String getBirthday()
	{
		return Birthday;
	}
	public void setBirthday(String aBirthday)
	{
		Birthday = aBirthday;
	}
	public String getCritType()
	{
		return CritType;
	}
	public void setCritType(String aCritType)
	{
		CritType = aCritType;
	}
	public String getCritCode()
	{
		return CritCode;
	}
	public void setCritCode(String aCritCode)
	{
		CritCode = aCritCode;
	}
	public String getCustomerno()
	{
		return Customerno;
	}
	public void setCustomerno(String aCustomerno)
	{
		Customerno = aCustomerno;
	}
	public String getCustomer_sequence_no()
	{
		return Customer_sequence_no;
	}
	public void setCustomer_sequence_no(String aCustomer_sequence_no)
	{
		Customer_sequence_no = aCustomer_sequence_no;
	}
	public String getErrorMessage()
	{
		return ErrorMessage;
	}
	public void setErrorMessage(String aErrorMessage)
	{
		ErrorMessage = aErrorMessage;
	}

	/**
	* 使用另外一个 CUSTOMERRESULTSchema 对象给 Schema 赋值
	* @param: aCUSTOMERRESULTSchema CUSTOMERRESULTSchema
	**/
	public void setSchema(CUSTOMERRESULTSchema aCUSTOMERRESULTSchema)
	{
		this.CompanyCode = aCUSTOMERRESULTSchema.getCompanyCode();
		this.Name = aCUSTOMERRESULTSchema.getName();
		this.Gender = aCUSTOMERRESULTSchema.getGender();
		this.Birthday = aCUSTOMERRESULTSchema.getBirthday();
		this.CritType = aCUSTOMERRESULTSchema.getCritType();
		this.CritCode = aCUSTOMERRESULTSchema.getCritCode();
		this.Customerno = aCUSTOMERRESULTSchema.getCustomerno();
		this.Customer_sequence_no = aCUSTOMERRESULTSchema.getCustomer_sequence_no();
		this.ErrorMessage = aCUSTOMERRESULTSchema.getErrorMessage();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CompanyCode") == null )
				this.CompanyCode = null;
			else
				this.CompanyCode = rs.getString("CompanyCode").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Gender") == null )
				this.Gender = null;
			else
				this.Gender = rs.getString("Gender").trim();

			if( rs.getString("Birthday") == null )
				this.Birthday = null;
			else
				this.Birthday = rs.getString("Birthday").trim();

			if( rs.getString("CritType") == null )
				this.CritType = null;
			else
				this.CritType = rs.getString("CritType").trim();

			if( rs.getString("CritCode") == null )
				this.CritCode = null;
			else
				this.CritCode = rs.getString("CritCode").trim();

			if( rs.getString("Customerno") == null )
				this.Customerno = null;
			else
				this.Customerno = rs.getString("Customerno").trim();

			if( rs.getString("Customer_sequence_no") == null )
				this.Customer_sequence_no = null;
			else
				this.Customer_sequence_no = rs.getString("Customer_sequence_no").trim();

			if( rs.getString("ErrorMessage") == null )
				this.ErrorMessage = null;
			else
				this.ErrorMessage = rs.getString("ErrorMessage").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的CUSTOMERRESULT表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CUSTOMERRESULTSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public CUSTOMERRESULTSchema getSchema()
	{
		CUSTOMERRESULTSchema aCUSTOMERRESULTSchema = new CUSTOMERRESULTSchema();
		aCUSTOMERRESULTSchema.setSchema(this);
		return aCUSTOMERRESULTSchema;
	}

	public CUSTOMERRESULTDB getDB()
	{
		CUSTOMERRESULTDB aDBOper = new CUSTOMERRESULTDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCUSTOMERRESULT描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CompanyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Gender)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Birthday)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CritType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CritCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Customerno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Customer_sequence_no)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorMessage));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCUSTOMERRESULT>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Gender = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Birthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CritType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CritCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Customerno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Customer_sequence_no = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ErrorMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CUSTOMERRESULTSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CompanyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyCode));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Gender"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Gender));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
		}
		if (FCode.equals("CritType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CritType));
		}
		if (FCode.equals("CritCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CritCode));
		}
		if (FCode.equals("Customerno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Customerno));
		}
		if (FCode.equals("Customer_sequence_no"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Customer_sequence_no));
		}
		if (FCode.equals("ErrorMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorMessage));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CompanyCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Gender);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Birthday);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CritType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CritCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Customerno);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Customer_sequence_no);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ErrorMessage);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CompanyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyCode = FValue.trim();
			}
			else
				CompanyCode = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Gender"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Gender = FValue.trim();
			}
			else
				Gender = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Birthday = FValue.trim();
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("CritType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CritType = FValue.trim();
			}
			else
				CritType = null;
		}
		if (FCode.equalsIgnoreCase("CritCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CritCode = FValue.trim();
			}
			else
				CritCode = null;
		}
		if (FCode.equalsIgnoreCase("Customerno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Customerno = FValue.trim();
			}
			else
				Customerno = null;
		}
		if (FCode.equalsIgnoreCase("Customer_sequence_no"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Customer_sequence_no = FValue.trim();
			}
			else
				Customer_sequence_no = null;
		}
		if (FCode.equalsIgnoreCase("ErrorMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorMessage = FValue.trim();
			}
			else
				ErrorMessage = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		CUSTOMERRESULTSchema other = (CUSTOMERRESULTSchema)otherObject;
		return
			(CompanyCode == null ? other.getCompanyCode() == null : CompanyCode.equals(other.getCompanyCode()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Gender == null ? other.getGender() == null : Gender.equals(other.getGender()))
			&& (Birthday == null ? other.getBirthday() == null : Birthday.equals(other.getBirthday()))
			&& (CritType == null ? other.getCritType() == null : CritType.equals(other.getCritType()))
			&& (CritCode == null ? other.getCritCode() == null : CritCode.equals(other.getCritCode()))
			&& (Customerno == null ? other.getCustomerno() == null : Customerno.equals(other.getCustomerno()))
			&& (Customer_sequence_no == null ? other.getCustomer_sequence_no() == null : Customer_sequence_no.equals(other.getCustomer_sequence_no()))
			&& (ErrorMessage == null ? other.getErrorMessage() == null : ErrorMessage.equals(other.getErrorMessage()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CompanyCode") ) {
			return 0;
		}
		if( strFieldName.equals("Name") ) {
			return 1;
		}
		if( strFieldName.equals("Gender") ) {
			return 2;
		}
		if( strFieldName.equals("Birthday") ) {
			return 3;
		}
		if( strFieldName.equals("CritType") ) {
			return 4;
		}
		if( strFieldName.equals("CritCode") ) {
			return 5;
		}
		if( strFieldName.equals("Customerno") ) {
			return 6;
		}
		if( strFieldName.equals("Customer_sequence_no") ) {
			return 7;
		}
		if( strFieldName.equals("ErrorMessage") ) {
			return 8;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CompanyCode";
				break;
			case 1:
				strFieldName = "Name";
				break;
			case 2:
				strFieldName = "Gender";
				break;
			case 3:
				strFieldName = "Birthday";
				break;
			case 4:
				strFieldName = "CritType";
				break;
			case 5:
				strFieldName = "CritCode";
				break;
			case 6:
				strFieldName = "Customerno";
				break;
			case 7:
				strFieldName = "Customer_sequence_no";
				break;
			case 8:
				strFieldName = "ErrorMessage";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CompanyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Gender") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CritType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CritCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Customerno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Customer_sequence_no") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorMessage") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
