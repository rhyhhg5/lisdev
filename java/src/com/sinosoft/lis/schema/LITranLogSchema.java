/*
 * <p>ClassName: LITranLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LITranLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LITranLogSchema implements Schema
{
    // @Field
    /** 批次号 */
    private String BatchNo;
    /** 业务量 */
    private int NumBuss;
    /** 起始日期 */
    private Date StartDate;
    /** 结束日期 */
    private Date EndDate;
    /** 文件名 */
    private String FileName;
    /** 文件路径 */
    private String FilePath;
    /** 标志 */
    private String Flag;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String ManageCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LITranLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "BatchNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getBatchNo()
    {
        if (BatchNo != null && !BatchNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            BatchNo = StrTool.unicodeToGBK(BatchNo);
        }
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo)
    {
        BatchNo = aBatchNo;
    }

    public int getNumBuss()
    {
        return NumBuss;
    }

    public void setNumBuss(int aNumBuss)
    {
        NumBuss = aNumBuss;
    }

    public void setNumBuss(String aNumBuss)
    {
        if (aNumBuss != null && !aNumBuss.equals(""))
        {
            Integer tInteger = new Integer(aNumBuss);
            int i = tInteger.intValue();
            NumBuss = i;
        }
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }

    public String getFileName()
    {
        if (FileName != null && !FileName.equals("") && SysConst.CHANGECHARSET == true)
        {
            FileName = StrTool.unicodeToGBK(FileName);
        }
        return FileName;
    }

    public void setFileName(String aFileName)
    {
        FileName = aFileName;
    }

    public String getFilePath()
    {
        if (FilePath != null && !FilePath.equals("") && SysConst.CHANGECHARSET == true)
        {
            FilePath = StrTool.unicodeToGBK(FilePath);
        }
        return FilePath;
    }

    public void setFilePath(String aFilePath)
    {
        FilePath = aFilePath;
    }

    public String getFlag()
    {
        if (Flag != null && !Flag.equals("") && SysConst.CHANGECHARSET == true)
        {
            Flag = StrTool.unicodeToGBK(Flag);
        }
        return Flag;
    }

    public void setFlag(String aFlag)
    {
        Flag = aFlag;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LITranLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LITranLogSchema aLITranLogSchema)
    {
        this.BatchNo = aLITranLogSchema.getBatchNo();
        this.NumBuss = aLITranLogSchema.getNumBuss();
        this.StartDate = fDate.getDate(aLITranLogSchema.getStartDate());
        this.EndDate = fDate.getDate(aLITranLogSchema.getEndDate());
        this.FileName = aLITranLogSchema.getFileName();
        this.FilePath = aLITranLogSchema.getFilePath();
        this.Flag = aLITranLogSchema.getFlag();
        this.Operator = aLITranLogSchema.getOperator();
        this.ManageCom = aLITranLogSchema.getManageCom();
        this.MakeDate = fDate.getDate(aLITranLogSchema.getMakeDate());
        this.MakeTime = aLITranLogSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("BatchNo") == null)
            {
                this.BatchNo = null;
            }
            else
            {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            this.NumBuss = rs.getInt("NumBuss");
            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("FileName") == null)
            {
                this.FileName = null;
            }
            else
            {
                this.FileName = rs.getString("FileName").trim();
            }

            if (rs.getString("FilePath") == null)
            {
                this.FilePath = null;
            }
            else
            {
                this.FilePath = rs.getString("FilePath").trim();
            }

            if (rs.getString("Flag") == null)
            {
                this.Flag = null;
            }
            else
            {
                this.Flag = rs.getString("Flag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LITranLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LITranLogSchema getSchema()
    {
        LITranLogSchema aLITranLogSchema = new LITranLogSchema();
        aLITranLogSchema.setSchema(this);
        return aLITranLogSchema;
    }

    public LITranLogDB getDB()
    {
        LITranLogDB aDBOper = new LITranLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLITranLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(BatchNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(NumBuss) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            StartDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(EndDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FileName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FilePath)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Flag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLITranLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            NumBuss = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            FilePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LITranLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("BatchNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BatchNo));
        }
        if (FCode.equals("NumBuss"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NumBuss));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getStartDate()));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getEndDate()));
        }
        if (FCode.equals("FileName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FileName));
        }
        if (FCode.equals("FilePath"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FilePath));
        }
        if (FCode.equals("Flag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Flag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(BatchNo);
                break;
            case 1:
                strFieldValue = String.valueOf(NumBuss);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(FileName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(FilePath);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Flag);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("BatchNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BatchNo = FValue.trim();
            }
            else
            {
                BatchNo = null;
            }
        }
        if (FCode.equals("NumBuss"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                NumBuss = i;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        if (FCode.equals("FileName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FileName = FValue.trim();
            }
            else
            {
                FileName = null;
            }
        }
        if (FCode.equals("FilePath"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FilePath = FValue.trim();
            }
            else
            {
                FilePath = null;
            }
        }
        if (FCode.equals("Flag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Flag = FValue.trim();
            }
            else
            {
                Flag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LITranLogSchema other = (LITranLogSchema) otherObject;
        return
                BatchNo.equals(other.getBatchNo())
                && NumBuss == other.getNumBuss()
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && FileName.equals(other.getFileName())
                && FilePath.equals(other.getFilePath())
                && Flag.equals(other.getFlag())
                && Operator.equals(other.getOperator())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("BatchNo"))
        {
            return 0;
        }
        if (strFieldName.equals("NumBuss"))
        {
            return 1;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 2;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 3;
        }
        if (strFieldName.equals("FileName"))
        {
            return 4;
        }
        if (strFieldName.equals("FilePath"))
        {
            return 5;
        }
        if (strFieldName.equals("Flag"))
        {
            return 6;
        }
        if (strFieldName.equals("Operator"))
        {
            return 7;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "BatchNo";
                break;
            case 1:
                strFieldName = "NumBuss";
                break;
            case 2:
                strFieldName = "StartDate";
                break;
            case 3:
                strFieldName = "EndDate";
                break;
            case 4:
                strFieldName = "FileName";
                break;
            case 5:
                strFieldName = "FilePath";
                break;
            case 6:
                strFieldName = "Flag";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            case 9:
                strFieldName = "MakeDate";
                break;
            case 10:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("BatchNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NumBuss"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FileName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FilePath"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Flag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
