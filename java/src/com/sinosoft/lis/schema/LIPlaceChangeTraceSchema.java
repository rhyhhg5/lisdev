/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIPlaceChangeTraceDB;

/*
 * <p>ClassName: LIPlaceChangeTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 职场管理
 * @CreateDate：2012-08-22
 */
public class LIPlaceChangeTraceSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String Serialno;
	/** 职场编码 */
	private String Placeno;
	/** 关联职场编码 */
	private String Placenorefer;
	/** 变更类型 */
	private String Changetype;
	/** 审批状态 */
	private String Changestate;
	/** 停用时间 */
	private Date Enddate;
	/** 应收已付款金额 */
	private double Shouldgetfee;
	/** 应收已付款收回日期 */
	private Date Shouldgetdate;
	/** 实际违约金 */
	private double Apenaltyfee;
	/** 实际违约金支付日期 */
	private Date Apenaltyfeegetdate;
	/** 保证金 */
	private double Marginfee;
	/** 保证金收回日期 */
	private Date Marginfeegetdate;
	/** 分公司审批日期 */
	private Date Fcheckdate;
	/** 分公司备注 */
	private String Fremark;
	/** 总公司审批日期 */
	private Date Zcheckdate;
	/** 总公司备注 */
	private String Zremark;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date Makedate;
	/** 入机时间 */
	private String Maketime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 备用字符串类型1 */
	private String Standbystring1;
	/** 备用字符串类型2 */
	private String Standbystring2;
	/** 备用数字类型1 */
	private double Standbynum1;
	/** 备用数字类型2 */
	private double Standbynum2;
	/** 备用日期类型1 */
	private Date Standbydate1;
	/** 备用日期类型2 */
	private Date Standbydate2;

	public static final int FIELDNUM = 27;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIPlaceChangeTraceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Serialno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LIPlaceChangeTraceSchema cloned = (LIPlaceChangeTraceSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialno()
	{
		return Serialno;
	}
	public void setSerialno(String aSerialno)
	{
		Serialno = aSerialno;
	}
	public String getPlaceno()
	{
		return Placeno;
	}
	public void setPlaceno(String aPlaceno)
	{
		Placeno = aPlaceno;
	}
	public String getPlacenorefer()
	{
		return Placenorefer;
	}
	public void setPlacenorefer(String aPlacenorefer)
	{
		Placenorefer = aPlacenorefer;
	}
	public String getChangetype()
	{
		return Changetype;
	}
	public void setChangetype(String aChangetype)
	{
		Changetype = aChangetype;
	}
	public String getChangestate()
	{
		return Changestate;
	}
	public void setChangestate(String aChangestate)
	{
		Changestate = aChangestate;
	}
	public String getEnddate()
	{
		if( Enddate != null )
			return fDate.getString(Enddate);
		else
			return null;
	}
	public void setEnddate(Date aEnddate)
	{
		Enddate = aEnddate;
	}
	public void setEnddate(String aEnddate)
	{
		if (aEnddate != null && !aEnddate.equals("") )
		{
			Enddate = fDate.getDate( aEnddate );
		}
		else
			Enddate = null;
	}

	public double getShouldgetfee()
	{
		return Shouldgetfee;
	}
	public void setShouldgetfee(double aShouldgetfee)
	{
		Shouldgetfee = Arith.round(aShouldgetfee,2);
	}
	public void setShouldgetfee(String aShouldgetfee)
	{
		if (aShouldgetfee != null && !aShouldgetfee.equals(""))
		{
			Double tDouble = new Double(aShouldgetfee);
			double d = tDouble.doubleValue();
                Shouldgetfee = Arith.round(d,2);
		}
	}

	public String getShouldgetdate()
	{
		if( Shouldgetdate != null )
			return fDate.getString(Shouldgetdate);
		else
			return null;
	}
	public void setShouldgetdate(Date aShouldgetdate)
	{
		Shouldgetdate = aShouldgetdate;
	}
	public void setShouldgetdate(String aShouldgetdate)
	{
		if (aShouldgetdate != null && !aShouldgetdate.equals("") )
		{
			Shouldgetdate = fDate.getDate( aShouldgetdate );
		}
		else
			Shouldgetdate = null;
	}

	public double getApenaltyfee()
	{
		return Apenaltyfee;
	}
	public void setApenaltyfee(double aApenaltyfee)
	{
		Apenaltyfee = Arith.round(aApenaltyfee,2);
	}
	public void setApenaltyfee(String aApenaltyfee)
	{
		if (aApenaltyfee != null && !aApenaltyfee.equals(""))
		{
			Double tDouble = new Double(aApenaltyfee);
			double d = tDouble.doubleValue();
                Apenaltyfee = Arith.round(d,2);
		}
	}

	public String getApenaltyfeegetdate()
	{
		if( Apenaltyfeegetdate != null )
			return fDate.getString(Apenaltyfeegetdate);
		else
			return null;
	}
	public void setApenaltyfeegetdate(Date aApenaltyfeegetdate)
	{
		Apenaltyfeegetdate = aApenaltyfeegetdate;
	}
	public void setApenaltyfeegetdate(String aApenaltyfeegetdate)
	{
		if (aApenaltyfeegetdate != null && !aApenaltyfeegetdate.equals("") )
		{
			Apenaltyfeegetdate = fDate.getDate( aApenaltyfeegetdate );
		}
		else
			Apenaltyfeegetdate = null;
	}

	public double getMarginfee()
	{
		return Marginfee;
	}
	public void setMarginfee(double aMarginfee)
	{
		Marginfee = Arith.round(aMarginfee,2);
	}
	public void setMarginfee(String aMarginfee)
	{
		if (aMarginfee != null && !aMarginfee.equals(""))
		{
			Double tDouble = new Double(aMarginfee);
			double d = tDouble.doubleValue();
                Marginfee = Arith.round(d,2);
		}
	}

	public String getMarginfeegetdate()
	{
		if( Marginfeegetdate != null )
			return fDate.getString(Marginfeegetdate);
		else
			return null;
	}
	public void setMarginfeegetdate(Date aMarginfeegetdate)
	{
		Marginfeegetdate = aMarginfeegetdate;
	}
	public void setMarginfeegetdate(String aMarginfeegetdate)
	{
		if (aMarginfeegetdate != null && !aMarginfeegetdate.equals("") )
		{
			Marginfeegetdate = fDate.getDate( aMarginfeegetdate );
		}
		else
			Marginfeegetdate = null;
	}

	public String getFcheckdate()
	{
		if( Fcheckdate != null )
			return fDate.getString(Fcheckdate);
		else
			return null;
	}
	public void setFcheckdate(Date aFcheckdate)
	{
		Fcheckdate = aFcheckdate;
	}
	public void setFcheckdate(String aFcheckdate)
	{
		if (aFcheckdate != null && !aFcheckdate.equals("") )
		{
			Fcheckdate = fDate.getDate( aFcheckdate );
		}
		else
			Fcheckdate = null;
	}

	public String getFremark()
	{
		return Fremark;
	}
	public void setFremark(String aFremark)
	{
		Fremark = aFremark;
	}
	public String getZcheckdate()
	{
		if( Zcheckdate != null )
			return fDate.getString(Zcheckdate);
		else
			return null;
	}
	public void setZcheckdate(Date aZcheckdate)
	{
		Zcheckdate = aZcheckdate;
	}
	public void setZcheckdate(String aZcheckdate)
	{
		if (aZcheckdate != null && !aZcheckdate.equals("") )
		{
			Zcheckdate = fDate.getDate( aZcheckdate );
		}
		else
			Zcheckdate = null;
	}

	public String getZremark()
	{
		return Zremark;
	}
	public void setZremark(String aZremark)
	{
		Zremark = aZremark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakedate()
	{
		if( Makedate != null )
			return fDate.getString(Makedate);
		else
			return null;
	}
	public void setMakedate(Date aMakedate)
	{
		Makedate = aMakedate;
	}
	public void setMakedate(String aMakedate)
	{
		if (aMakedate != null && !aMakedate.equals("") )
		{
			Makedate = fDate.getDate( aMakedate );
		}
		else
			Makedate = null;
	}

	public String getMaketime()
	{
		return Maketime;
	}
	public void setMaketime(String aMaketime)
	{
		Maketime = aMaketime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStandbystring1()
	{
		return Standbystring1;
	}
	public void setStandbystring1(String aStandbystring1)
	{
		Standbystring1 = aStandbystring1;
	}
	public String getStandbystring2()
	{
		return Standbystring2;
	}
	public void setStandbystring2(String aStandbystring2)
	{
		Standbystring2 = aStandbystring2;
	}
	public double getStandbynum1()
	{
		return Standbynum1;
	}
	public void setStandbynum1(double aStandbynum1)
	{
		Standbynum1 = Arith.round(aStandbynum1,2);
	}
	public void setStandbynum1(String aStandbynum1)
	{
		if (aStandbynum1 != null && !aStandbynum1.equals(""))
		{
			Double tDouble = new Double(aStandbynum1);
			double d = tDouble.doubleValue();
                Standbynum1 = Arith.round(d,2);
		}
	}

	public double getStandbynum2()
	{
		return Standbynum2;
	}
	public void setStandbynum2(double aStandbynum2)
	{
		Standbynum2 = Arith.round(aStandbynum2,2);
	}
	public void setStandbynum2(String aStandbynum2)
	{
		if (aStandbynum2 != null && !aStandbynum2.equals(""))
		{
			Double tDouble = new Double(aStandbynum2);
			double d = tDouble.doubleValue();
                Standbynum2 = Arith.round(d,2);
		}
	}

	public String getStandbydate1()
	{
		if( Standbydate1 != null )
			return fDate.getString(Standbydate1);
		else
			return null;
	}
	public void setStandbydate1(Date aStandbydate1)
	{
		Standbydate1 = aStandbydate1;
	}
	public void setStandbydate1(String aStandbydate1)
	{
		if (aStandbydate1 != null && !aStandbydate1.equals("") )
		{
			Standbydate1 = fDate.getDate( aStandbydate1 );
		}
		else
			Standbydate1 = null;
	}

	public String getStandbydate2()
	{
		if( Standbydate2 != null )
			return fDate.getString(Standbydate2);
		else
			return null;
	}
	public void setStandbydate2(Date aStandbydate2)
	{
		Standbydate2 = aStandbydate2;
	}
	public void setStandbydate2(String aStandbydate2)
	{
		if (aStandbydate2 != null && !aStandbydate2.equals("") )
		{
			Standbydate2 = fDate.getDate( aStandbydate2 );
		}
		else
			Standbydate2 = null;
	}


	/**
	* 使用另外一个 LIPlaceChangeTraceSchema 对象给 Schema 赋值
	* @param: aLIPlaceChangeTraceSchema LIPlaceChangeTraceSchema
	**/
	public void setSchema(LIPlaceChangeTraceSchema aLIPlaceChangeTraceSchema)
	{
		this.Serialno = aLIPlaceChangeTraceSchema.getSerialno();
		this.Placeno = aLIPlaceChangeTraceSchema.getPlaceno();
		this.Placenorefer = aLIPlaceChangeTraceSchema.getPlacenorefer();
		this.Changetype = aLIPlaceChangeTraceSchema.getChangetype();
		this.Changestate = aLIPlaceChangeTraceSchema.getChangestate();
		this.Enddate = fDate.getDate( aLIPlaceChangeTraceSchema.getEnddate());
		this.Shouldgetfee = aLIPlaceChangeTraceSchema.getShouldgetfee();
		this.Shouldgetdate = fDate.getDate( aLIPlaceChangeTraceSchema.getShouldgetdate());
		this.Apenaltyfee = aLIPlaceChangeTraceSchema.getApenaltyfee();
		this.Apenaltyfeegetdate = fDate.getDate( aLIPlaceChangeTraceSchema.getApenaltyfeegetdate());
		this.Marginfee = aLIPlaceChangeTraceSchema.getMarginfee();
		this.Marginfeegetdate = fDate.getDate( aLIPlaceChangeTraceSchema.getMarginfeegetdate());
		this.Fcheckdate = fDate.getDate( aLIPlaceChangeTraceSchema.getFcheckdate());
		this.Fremark = aLIPlaceChangeTraceSchema.getFremark();
		this.Zcheckdate = fDate.getDate( aLIPlaceChangeTraceSchema.getZcheckdate());
		this.Zremark = aLIPlaceChangeTraceSchema.getZremark();
		this.Operator = aLIPlaceChangeTraceSchema.getOperator();
		this.Makedate = fDate.getDate( aLIPlaceChangeTraceSchema.getMakedate());
		this.Maketime = aLIPlaceChangeTraceSchema.getMaketime();
		this.ModifyDate = fDate.getDate( aLIPlaceChangeTraceSchema.getModifyDate());
		this.ModifyTime = aLIPlaceChangeTraceSchema.getModifyTime();
		this.Standbystring1 = aLIPlaceChangeTraceSchema.getStandbystring1();
		this.Standbystring2 = aLIPlaceChangeTraceSchema.getStandbystring2();
		this.Standbynum1 = aLIPlaceChangeTraceSchema.getStandbynum1();
		this.Standbynum2 = aLIPlaceChangeTraceSchema.getStandbynum2();
		this.Standbydate1 = fDate.getDate( aLIPlaceChangeTraceSchema.getStandbydate1());
		this.Standbydate2 = fDate.getDate( aLIPlaceChangeTraceSchema.getStandbydate2());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Serialno") == null )
				this.Serialno = null;
			else
				this.Serialno = rs.getString("Serialno").trim();

			if( rs.getString("Placeno") == null )
				this.Placeno = null;
			else
				this.Placeno = rs.getString("Placeno").trim();

			if( rs.getString("Placenorefer") == null )
				this.Placenorefer = null;
			else
				this.Placenorefer = rs.getString("Placenorefer").trim();

			if( rs.getString("Changetype") == null )
				this.Changetype = null;
			else
				this.Changetype = rs.getString("Changetype").trim();

			if( rs.getString("Changestate") == null )
				this.Changestate = null;
			else
				this.Changestate = rs.getString("Changestate").trim();

			this.Enddate = rs.getDate("Enddate");
			this.Shouldgetfee = rs.getDouble("Shouldgetfee");
			this.Shouldgetdate = rs.getDate("Shouldgetdate");
			this.Apenaltyfee = rs.getDouble("Apenaltyfee");
			this.Apenaltyfeegetdate = rs.getDate("Apenaltyfeegetdate");
			this.Marginfee = rs.getDouble("Marginfee");
			this.Marginfeegetdate = rs.getDate("Marginfeegetdate");
			this.Fcheckdate = rs.getDate("Fcheckdate");
			if( rs.getString("Fremark") == null )
				this.Fremark = null;
			else
				this.Fremark = rs.getString("Fremark").trim();

			this.Zcheckdate = rs.getDate("Zcheckdate");
			if( rs.getString("Zremark") == null )
				this.Zremark = null;
			else
				this.Zremark = rs.getString("Zremark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Makedate = rs.getDate("Makedate");
			if( rs.getString("Maketime") == null )
				this.Maketime = null;
			else
				this.Maketime = rs.getString("Maketime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Standbystring1") == null )
				this.Standbystring1 = null;
			else
				this.Standbystring1 = rs.getString("Standbystring1").trim();

			if( rs.getString("Standbystring2") == null )
				this.Standbystring2 = null;
			else
				this.Standbystring2 = rs.getString("Standbystring2").trim();

			this.Standbynum1 = rs.getDouble("Standbynum1");
			this.Standbynum2 = rs.getDouble("Standbynum2");
			this.Standbydate1 = rs.getDate("Standbydate1");
			this.Standbydate2 = rs.getDate("Standbydate2");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIPlaceChangeTrace表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPlaceChangeTraceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIPlaceChangeTraceSchema getSchema()
	{
		LIPlaceChangeTraceSchema aLIPlaceChangeTraceSchema = new LIPlaceChangeTraceSchema();
		aLIPlaceChangeTraceSchema.setSchema(this);
		return aLIPlaceChangeTraceSchema;
	}

	public LIPlaceChangeTraceDB getDB()
	{
		LIPlaceChangeTraceDB aDBOper = new LIPlaceChangeTraceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPlaceChangeTrace描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Serialno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Placeno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Placenorefer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Changetype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Changestate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Enddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Shouldgetfee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Shouldgetdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Apenaltyfee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Apenaltyfeegetdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Marginfee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Marginfeegetdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Fcheckdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Fremark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Zcheckdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Zremark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate2 )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPlaceChangeTrace>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Serialno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Placeno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Placenorefer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Changetype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Changestate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Enddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			Shouldgetfee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			Shouldgetdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			Apenaltyfee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			Apenaltyfeegetdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			Marginfee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			Marginfeegetdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			Fcheckdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			Fremark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Zcheckdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			Zremark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			Maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Standbystring1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Standbystring2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Standbynum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			Standbynum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			Standbydate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			Standbydate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPlaceChangeTraceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Serialno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Serialno));
		}
		if (FCode.equals("Placeno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Placeno));
		}
		if (FCode.equals("Placenorefer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Placenorefer));
		}
		if (FCode.equals("Changetype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Changetype));
		}
		if (FCode.equals("Changestate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Changestate));
		}
		if (FCode.equals("Enddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnddate()));
		}
		if (FCode.equals("Shouldgetfee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Shouldgetfee));
		}
		if (FCode.equals("Shouldgetdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getShouldgetdate()));
		}
		if (FCode.equals("Apenaltyfee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Apenaltyfee));
		}
		if (FCode.equals("Apenaltyfeegetdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApenaltyfeegetdate()));
		}
		if (FCode.equals("Marginfee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Marginfee));
		}
		if (FCode.equals("Marginfeegetdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarginfeegetdate()));
		}
		if (FCode.equals("Fcheckdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFcheckdate()));
		}
		if (FCode.equals("Fremark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fremark));
		}
		if (FCode.equals("Zcheckdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getZcheckdate()));
		}
		if (FCode.equals("Zremark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Zremark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
		}
		if (FCode.equals("Maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Maketime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Standbystring1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring1));
		}
		if (FCode.equals("Standbystring2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring2));
		}
		if (FCode.equals("Standbynum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum1));
		}
		if (FCode.equals("Standbynum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum2));
		}
		if (FCode.equals("Standbydate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
		}
		if (FCode.equals("Standbydate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Serialno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Placeno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Placenorefer);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Changetype);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Changestate);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnddate()));
				break;
			case 6:
				strFieldValue = String.valueOf(Shouldgetfee);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getShouldgetdate()));
				break;
			case 8:
				strFieldValue = String.valueOf(Apenaltyfee);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApenaltyfeegetdate()));
				break;
			case 10:
				strFieldValue = String.valueOf(Marginfee);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarginfeegetdate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFcheckdate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Fremark);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getZcheckdate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Zremark);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Maketime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Standbystring1);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Standbystring2);
				break;
			case 23:
				strFieldValue = String.valueOf(Standbynum1);
				break;
			case 24:
				strFieldValue = String.valueOf(Standbynum2);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Serialno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Serialno = FValue.trim();
			}
			else
				Serialno = null;
		}
		if (FCode.equalsIgnoreCase("Placeno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Placeno = FValue.trim();
			}
			else
				Placeno = null;
		}
		if (FCode.equalsIgnoreCase("Placenorefer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Placenorefer = FValue.trim();
			}
			else
				Placenorefer = null;
		}
		if (FCode.equalsIgnoreCase("Changetype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Changetype = FValue.trim();
			}
			else
				Changetype = null;
		}
		if (FCode.equalsIgnoreCase("Changestate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Changestate = FValue.trim();
			}
			else
				Changestate = null;
		}
		if (FCode.equalsIgnoreCase("Enddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Enddate = fDate.getDate( FValue );
			}
			else
				Enddate = null;
		}
		if (FCode.equalsIgnoreCase("Shouldgetfee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Shouldgetfee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Shouldgetdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Shouldgetdate = fDate.getDate( FValue );
			}
			else
				Shouldgetdate = null;
		}
		if (FCode.equalsIgnoreCase("Apenaltyfee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Apenaltyfee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Apenaltyfeegetdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Apenaltyfeegetdate = fDate.getDate( FValue );
			}
			else
				Apenaltyfeegetdate = null;
		}
		if (FCode.equalsIgnoreCase("Marginfee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Marginfee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Marginfeegetdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Marginfeegetdate = fDate.getDate( FValue );
			}
			else
				Marginfeegetdate = null;
		}
		if (FCode.equalsIgnoreCase("Fcheckdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Fcheckdate = fDate.getDate( FValue );
			}
			else
				Fcheckdate = null;
		}
		if (FCode.equalsIgnoreCase("Fremark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Fremark = FValue.trim();
			}
			else
				Fremark = null;
		}
		if (FCode.equalsIgnoreCase("Zcheckdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Zcheckdate = fDate.getDate( FValue );
			}
			else
				Zcheckdate = null;
		}
		if (FCode.equalsIgnoreCase("Zremark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Zremark = FValue.trim();
			}
			else
				Zremark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Makedate = fDate.getDate( FValue );
			}
			else
				Makedate = null;
		}
		if (FCode.equalsIgnoreCase("Maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Maketime = FValue.trim();
			}
			else
				Maketime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring1 = FValue.trim();
			}
			else
				Standbystring1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring2 = FValue.trim();
			}
			else
				Standbystring2 = null;
		}
		if (FCode.equalsIgnoreCase("Standbynum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbynum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbydate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate1 = fDate.getDate( FValue );
			}
			else
				Standbydate1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbydate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate2 = fDate.getDate( FValue );
			}
			else
				Standbydate2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIPlaceChangeTraceSchema other = (LIPlaceChangeTraceSchema)otherObject;
		return
			(Serialno == null ? other.getSerialno() == null : Serialno.equals(other.getSerialno()))
			&& (Placeno == null ? other.getPlaceno() == null : Placeno.equals(other.getPlaceno()))
			&& (Placenorefer == null ? other.getPlacenorefer() == null : Placenorefer.equals(other.getPlacenorefer()))
			&& (Changetype == null ? other.getChangetype() == null : Changetype.equals(other.getChangetype()))
			&& (Changestate == null ? other.getChangestate() == null : Changestate.equals(other.getChangestate()))
			&& (Enddate == null ? other.getEnddate() == null : fDate.getString(Enddate).equals(other.getEnddate()))
			&& Shouldgetfee == other.getShouldgetfee()
			&& (Shouldgetdate == null ? other.getShouldgetdate() == null : fDate.getString(Shouldgetdate).equals(other.getShouldgetdate()))
			&& Apenaltyfee == other.getApenaltyfee()
			&& (Apenaltyfeegetdate == null ? other.getApenaltyfeegetdate() == null : fDate.getString(Apenaltyfeegetdate).equals(other.getApenaltyfeegetdate()))
			&& Marginfee == other.getMarginfee()
			&& (Marginfeegetdate == null ? other.getMarginfeegetdate() == null : fDate.getString(Marginfeegetdate).equals(other.getMarginfeegetdate()))
			&& (Fcheckdate == null ? other.getFcheckdate() == null : fDate.getString(Fcheckdate).equals(other.getFcheckdate()))
			&& (Fremark == null ? other.getFremark() == null : Fremark.equals(other.getFremark()))
			&& (Zcheckdate == null ? other.getZcheckdate() == null : fDate.getString(Zcheckdate).equals(other.getZcheckdate()))
			&& (Zremark == null ? other.getZremark() == null : Zremark.equals(other.getZremark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Makedate == null ? other.getMakedate() == null : fDate.getString(Makedate).equals(other.getMakedate()))
			&& (Maketime == null ? other.getMaketime() == null : Maketime.equals(other.getMaketime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Standbystring1 == null ? other.getStandbystring1() == null : Standbystring1.equals(other.getStandbystring1()))
			&& (Standbystring2 == null ? other.getStandbystring2() == null : Standbystring2.equals(other.getStandbystring2()))
			&& Standbynum1 == other.getStandbynum1()
			&& Standbynum2 == other.getStandbynum2()
			&& (Standbydate1 == null ? other.getStandbydate1() == null : fDate.getString(Standbydate1).equals(other.getStandbydate1()))
			&& (Standbydate2 == null ? other.getStandbydate2() == null : fDate.getString(Standbydate2).equals(other.getStandbydate2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Serialno") ) {
			return 0;
		}
		if( strFieldName.equals("Placeno") ) {
			return 1;
		}
		if( strFieldName.equals("Placenorefer") ) {
			return 2;
		}
		if( strFieldName.equals("Changetype") ) {
			return 3;
		}
		if( strFieldName.equals("Changestate") ) {
			return 4;
		}
		if( strFieldName.equals("Enddate") ) {
			return 5;
		}
		if( strFieldName.equals("Shouldgetfee") ) {
			return 6;
		}
		if( strFieldName.equals("Shouldgetdate") ) {
			return 7;
		}
		if( strFieldName.equals("Apenaltyfee") ) {
			return 8;
		}
		if( strFieldName.equals("Apenaltyfeegetdate") ) {
			return 9;
		}
		if( strFieldName.equals("Marginfee") ) {
			return 10;
		}
		if( strFieldName.equals("Marginfeegetdate") ) {
			return 11;
		}
		if( strFieldName.equals("Fcheckdate") ) {
			return 12;
		}
		if( strFieldName.equals("Fremark") ) {
			return 13;
		}
		if( strFieldName.equals("Zcheckdate") ) {
			return 14;
		}
		if( strFieldName.equals("Zremark") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("Makedate") ) {
			return 17;
		}
		if( strFieldName.equals("Maketime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return 21;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return 22;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return 23;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return 24;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return 25;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return 26;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Serialno";
				break;
			case 1:
				strFieldName = "Placeno";
				break;
			case 2:
				strFieldName = "Placenorefer";
				break;
			case 3:
				strFieldName = "Changetype";
				break;
			case 4:
				strFieldName = "Changestate";
				break;
			case 5:
				strFieldName = "Enddate";
				break;
			case 6:
				strFieldName = "Shouldgetfee";
				break;
			case 7:
				strFieldName = "Shouldgetdate";
				break;
			case 8:
				strFieldName = "Apenaltyfee";
				break;
			case 9:
				strFieldName = "Apenaltyfeegetdate";
				break;
			case 10:
				strFieldName = "Marginfee";
				break;
			case 11:
				strFieldName = "Marginfeegetdate";
				break;
			case 12:
				strFieldName = "Fcheckdate";
				break;
			case 13:
				strFieldName = "Fremark";
				break;
			case 14:
				strFieldName = "Zcheckdate";
				break;
			case 15:
				strFieldName = "Zremark";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "Makedate";
				break;
			case 18:
				strFieldName = "Maketime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			case 21:
				strFieldName = "Standbystring1";
				break;
			case 22:
				strFieldName = "Standbystring2";
				break;
			case 23:
				strFieldName = "Standbynum1";
				break;
			case 24:
				strFieldName = "Standbynum2";
				break;
			case 25:
				strFieldName = "Standbydate1";
				break;
			case 26:
				strFieldName = "Standbydate2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Serialno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Placeno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Placenorefer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Changetype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Changestate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Enddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Shouldgetfee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Shouldgetdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Apenaltyfee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Apenaltyfeegetdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Marginfee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Marginfeegetdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Fcheckdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Fremark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Zcheckdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Zremark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
