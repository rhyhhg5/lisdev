/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRProfitLossDB;

/*
 * <p>ClassName: LRProfitLossSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-04-10
 */
public class LRProfitLossSchema implements Schema, Cloneable
{
	// @Field
	/** 再保合同号 */
	private String ReContCode;
	/** 再保险公司 */
	private String ReComCode;
	/** 再保项目 */
	private String ReinsureItem;
	/** 盈余佣金算法 */
	private String CalType;
	/** 盈余佣金年度 */
	private int UWYear;
	/** 盈余佣金开始日期 */
	private Date StartDate;
	/** 盈余佣金结束日期 */
	private Date EndDate;
	/** 分保费 */
	private double WrittenPrem;
	/** 期初未期满风险准备金 */
	private double IncomingClmReserve;
	/** 期初已报未决准备金 */
	private double IncomingReserve1;
	/** 期初未报未决准备金 */
	private double IncomingReserve2;
	/** 收入小计 */
	private double SubTotalIncome;
	/** 理赔摊回 */
	private double ClaimBackFee;
	/** 再保佣金支出 */
	private double ReProcFee;
	/** 期末未期满风险准备金 */
	private double OutgoingClmReserve;
	/** 期末已报未决准备金 */
	private double OutgoingReserve1;
	/** 期末未报未决准备金 */
	private double OutgoingReserve2;
	/** 再保险管理费 */
	private double ManageFee;
	/** 上年度亏损 */
	private double PreYearLoss;
	/** 支出小计 */
	private double SubTotalOutgo;
	/** 本年利润 */
	private double UWProfit;
	/** 本年盈余佣金 */
	private double UWProfitComm;
	/** 各年度利润累计 */
	private double SumUWProfit;
	/** 年度盈余佣金累计 */
	private double SumUWProfitComm;
	/** 入机时间 */
	private String MakeTime;
	/** 入机日期 */
	private Date MakeDate;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 操作人员 */
	private String Operator;

	public static final int FIELDNUM = 29;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRProfitLossSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "ReContCode";
		pk[1] = "ReinsureItem";
		pk[2] = "CalType";
		pk[3] = "UWYear";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LRProfitLossSchema cloned = (LRProfitLossSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
            ReContCode = aReContCode;
	}
	public String getReComCode()
	{
		return ReComCode;
	}
	public void setReComCode(String aReComCode)
	{
            ReComCode = aReComCode;
	}
	public String getReinsureItem()
	{
		return ReinsureItem;
	}
	public void setReinsureItem(String aReinsureItem)
	{
            ReinsureItem = aReinsureItem;
	}
	public String getCalType()
	{
		return CalType;
	}
	public void setCalType(String aCalType)
	{
            CalType = aCalType;
	}
	public int getUWYear()
	{
		return UWYear;
	}
	public void setUWYear(int aUWYear)
	{
            UWYear = aUWYear;
	}
	public void setUWYear(String aUWYear)
	{
		if (aUWYear != null && !aUWYear.equals(""))
		{
			Integer tInteger = new Integer(aUWYear);
			int i = tInteger.intValue();
			UWYear = i;
		}
	}

	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
            StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
            EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public double getWrittenPrem()
	{
		return WrittenPrem;
	}
	public void setWrittenPrem(double aWrittenPrem)
	{
            WrittenPrem = Arith.round(aWrittenPrem,2);
	}
	public void setWrittenPrem(String aWrittenPrem)
	{
		if (aWrittenPrem != null && !aWrittenPrem.equals(""))
		{
			Double tDouble = new Double(aWrittenPrem);
			double d = tDouble.doubleValue();
                WrittenPrem = Arith.round(d,2);
		}
	}

	public double getIncomingClmReserve()
	{
		return IncomingClmReserve;
	}
	public void setIncomingClmReserve(double aIncomingClmReserve)
	{
            IncomingClmReserve = Arith.round(aIncomingClmReserve,2);
	}
	public void setIncomingClmReserve(String aIncomingClmReserve)
	{
		if (aIncomingClmReserve != null && !aIncomingClmReserve.equals(""))
		{
			Double tDouble = new Double(aIncomingClmReserve);
			double d = tDouble.doubleValue();
                IncomingClmReserve = Arith.round(d,2);
		}
	}

	public double getIncomingReserve1()
	{
		return IncomingReserve1;
	}
	public void setIncomingReserve1(double aIncomingReserve1)
	{
            IncomingReserve1 = Arith.round(aIncomingReserve1,2);
	}
	public void setIncomingReserve1(String aIncomingReserve1)
	{
		if (aIncomingReserve1 != null && !aIncomingReserve1.equals(""))
		{
			Double tDouble = new Double(aIncomingReserve1);
			double d = tDouble.doubleValue();
                IncomingReserve1 = Arith.round(d,2);
		}
	}

	public double getIncomingReserve2()
	{
		return IncomingReserve2;
	}
	public void setIncomingReserve2(double aIncomingReserve2)
	{
            IncomingReserve2 = Arith.round(aIncomingReserve2,2);
	}
	public void setIncomingReserve2(String aIncomingReserve2)
	{
		if (aIncomingReserve2 != null && !aIncomingReserve2.equals(""))
		{
			Double tDouble = new Double(aIncomingReserve2);
			double d = tDouble.doubleValue();
                IncomingReserve2 = Arith.round(d,2);
		}
	}

	public double getSubTotalIncome()
	{
		return SubTotalIncome;
	}
	public void setSubTotalIncome(double aSubTotalIncome)
	{
            SubTotalIncome = Arith.round(aSubTotalIncome,2);
	}
	public void setSubTotalIncome(String aSubTotalIncome)
	{
		if (aSubTotalIncome != null && !aSubTotalIncome.equals(""))
		{
			Double tDouble = new Double(aSubTotalIncome);
			double d = tDouble.doubleValue();
                SubTotalIncome = Arith.round(d,2);
		}
	}

	public double getClaimBackFee()
	{
		return ClaimBackFee;
	}
	public void setClaimBackFee(double aClaimBackFee)
	{
            ClaimBackFee = Arith.round(aClaimBackFee,2);
	}
	public void setClaimBackFee(String aClaimBackFee)
	{
		if (aClaimBackFee != null && !aClaimBackFee.equals(""))
		{
			Double tDouble = new Double(aClaimBackFee);
			double d = tDouble.doubleValue();
                ClaimBackFee = Arith.round(d,2);
		}
	}

	public double getReProcFee()
	{
		return ReProcFee;
	}
	public void setReProcFee(double aReProcFee)
	{
            ReProcFee = Arith.round(aReProcFee,2);
	}
	public void setReProcFee(String aReProcFee)
	{
		if (aReProcFee != null && !aReProcFee.equals(""))
		{
			Double tDouble = new Double(aReProcFee);
			double d = tDouble.doubleValue();
                ReProcFee = Arith.round(d,2);
		}
	}

	public double getOutgoingClmReserve()
	{
		return OutgoingClmReserve;
	}
	public void setOutgoingClmReserve(double aOutgoingClmReserve)
	{
            OutgoingClmReserve = Arith.round(aOutgoingClmReserve,2);
	}
	public void setOutgoingClmReserve(String aOutgoingClmReserve)
	{
		if (aOutgoingClmReserve != null && !aOutgoingClmReserve.equals(""))
		{
			Double tDouble = new Double(aOutgoingClmReserve);
			double d = tDouble.doubleValue();
                OutgoingClmReserve = Arith.round(d,2);
		}
	}

	public double getOutgoingReserve1()
	{
		return OutgoingReserve1;
	}
	public void setOutgoingReserve1(double aOutgoingReserve1)
	{
            OutgoingReserve1 = Arith.round(aOutgoingReserve1,2);
	}
	public void setOutgoingReserve1(String aOutgoingReserve1)
	{
		if (aOutgoingReserve1 != null && !aOutgoingReserve1.equals(""))
		{
			Double tDouble = new Double(aOutgoingReserve1);
			double d = tDouble.doubleValue();
                OutgoingReserve1 = Arith.round(d,2);
		}
	}

	public double getOutgoingReserve2()
	{
		return OutgoingReserve2;
	}
	public void setOutgoingReserve2(double aOutgoingReserve2)
	{
            OutgoingReserve2 = Arith.round(aOutgoingReserve2,2);
	}
	public void setOutgoingReserve2(String aOutgoingReserve2)
	{
		if (aOutgoingReserve2 != null && !aOutgoingReserve2.equals(""))
		{
			Double tDouble = new Double(aOutgoingReserve2);
			double d = tDouble.doubleValue();
                OutgoingReserve2 = Arith.round(d,2);
		}
	}

	public double getManageFee()
	{
		return ManageFee;
	}
	public void setManageFee(double aManageFee)
	{
            ManageFee = Arith.round(aManageFee,2);
	}
	public void setManageFee(String aManageFee)
	{
		if (aManageFee != null && !aManageFee.equals(""))
		{
			Double tDouble = new Double(aManageFee);
			double d = tDouble.doubleValue();
                ManageFee = Arith.round(d,2);
		}
	}

	public double getPreYearLoss()
	{
		return PreYearLoss;
	}
	public void setPreYearLoss(double aPreYearLoss)
	{
            PreYearLoss = Arith.round(aPreYearLoss,2);
	}
	public void setPreYearLoss(String aPreYearLoss)
	{
		if (aPreYearLoss != null && !aPreYearLoss.equals(""))
		{
			Double tDouble = new Double(aPreYearLoss);
			double d = tDouble.doubleValue();
                PreYearLoss = Arith.round(d,2);
		}
	}

	public double getSubTotalOutgo()
	{
		return SubTotalOutgo;
	}
	public void setSubTotalOutgo(double aSubTotalOutgo)
	{
            SubTotalOutgo = Arith.round(aSubTotalOutgo,2);
	}
	public void setSubTotalOutgo(String aSubTotalOutgo)
	{
		if (aSubTotalOutgo != null && !aSubTotalOutgo.equals(""))
		{
			Double tDouble = new Double(aSubTotalOutgo);
			double d = tDouble.doubleValue();
                SubTotalOutgo = Arith.round(d,2);
		}
	}

	public double getUWProfit()
	{
		return UWProfit;
	}
	public void setUWProfit(double aUWProfit)
	{
            UWProfit = Arith.round(aUWProfit,2);
	}
	public void setUWProfit(String aUWProfit)
	{
		if (aUWProfit != null && !aUWProfit.equals(""))
		{
			Double tDouble = new Double(aUWProfit);
			double d = tDouble.doubleValue();
                UWProfit = Arith.round(d,2);
		}
	}

	public double getUWProfitComm()
	{
		return UWProfitComm;
	}
	public void setUWProfitComm(double aUWProfitComm)
	{
            UWProfitComm = Arith.round(aUWProfitComm,2);
	}
	public void setUWProfitComm(String aUWProfitComm)
	{
		if (aUWProfitComm != null && !aUWProfitComm.equals(""))
		{
			Double tDouble = new Double(aUWProfitComm);
			double d = tDouble.doubleValue();
                UWProfitComm = Arith.round(d,2);
		}
	}

	public double getSumUWProfit()
	{
		return SumUWProfit;
	}
	public void setSumUWProfit(double aSumUWProfit)
	{
            SumUWProfit = Arith.round(aSumUWProfit,2);
	}
	public void setSumUWProfit(String aSumUWProfit)
	{
		if (aSumUWProfit != null && !aSumUWProfit.equals(""))
		{
			Double tDouble = new Double(aSumUWProfit);
			double d = tDouble.doubleValue();
                SumUWProfit = Arith.round(d,2);
		}
	}

	public double getSumUWProfitComm()
	{
		return SumUWProfitComm;
	}
	public void setSumUWProfitComm(double aSumUWProfitComm)
	{
            SumUWProfitComm = Arith.round(aSumUWProfitComm,2);
	}
	public void setSumUWProfitComm(String aSumUWProfitComm)
	{
		if (aSumUWProfitComm != null && !aSumUWProfitComm.equals(""))
		{
			Double tDouble = new Double(aSumUWProfitComm);
			double d = tDouble.doubleValue();
                SumUWProfitComm = Arith.round(d,2);
		}
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}

	/**
	* 使用另外一个 LRProfitLossSchema 对象给 Schema 赋值
	* @param: aLRProfitLossSchema LRProfitLossSchema
	**/
	public void setSchema(LRProfitLossSchema aLRProfitLossSchema)
	{
		this.ReContCode = aLRProfitLossSchema.getReContCode();
		this.ReComCode = aLRProfitLossSchema.getReComCode();
		this.ReinsureItem = aLRProfitLossSchema.getReinsureItem();
		this.CalType = aLRProfitLossSchema.getCalType();
		this.UWYear = aLRProfitLossSchema.getUWYear();
		this.StartDate = fDate.getDate( aLRProfitLossSchema.getStartDate());
		this.EndDate = fDate.getDate( aLRProfitLossSchema.getEndDate());
		this.WrittenPrem = aLRProfitLossSchema.getWrittenPrem();
		this.IncomingClmReserve = aLRProfitLossSchema.getIncomingClmReserve();
		this.IncomingReserve1 = aLRProfitLossSchema.getIncomingReserve1();
		this.IncomingReserve2 = aLRProfitLossSchema.getIncomingReserve2();
		this.SubTotalIncome = aLRProfitLossSchema.getSubTotalIncome();
		this.ClaimBackFee = aLRProfitLossSchema.getClaimBackFee();
		this.ReProcFee = aLRProfitLossSchema.getReProcFee();
		this.OutgoingClmReserve = aLRProfitLossSchema.getOutgoingClmReserve();
		this.OutgoingReserve1 = aLRProfitLossSchema.getOutgoingReserve1();
		this.OutgoingReserve2 = aLRProfitLossSchema.getOutgoingReserve2();
		this.ManageFee = aLRProfitLossSchema.getManageFee();
		this.PreYearLoss = aLRProfitLossSchema.getPreYearLoss();
		this.SubTotalOutgo = aLRProfitLossSchema.getSubTotalOutgo();
		this.UWProfit = aLRProfitLossSchema.getUWProfit();
		this.UWProfitComm = aLRProfitLossSchema.getUWProfitComm();
		this.SumUWProfit = aLRProfitLossSchema.getSumUWProfit();
		this.SumUWProfitComm = aLRProfitLossSchema.getSumUWProfitComm();
		this.MakeTime = aLRProfitLossSchema.getMakeTime();
		this.MakeDate = fDate.getDate( aLRProfitLossSchema.getMakeDate());
		this.ModifyDate = fDate.getDate( aLRProfitLossSchema.getModifyDate());
		this.ModifyTime = aLRProfitLossSchema.getModifyTime();
		this.Operator = aLRProfitLossSchema.getOperator();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("ReComCode") == null )
				this.ReComCode = null;
			else
				this.ReComCode = rs.getString("ReComCode").trim();

			if( rs.getString("ReinsureItem") == null )
				this.ReinsureItem = null;
			else
				this.ReinsureItem = rs.getString("ReinsureItem").trim();

			if( rs.getString("CalType") == null )
				this.CalType = null;
			else
				this.CalType = rs.getString("CalType").trim();

			this.UWYear = rs.getInt("UWYear");
			this.StartDate = rs.getDate("StartDate");
			this.EndDate = rs.getDate("EndDate");
			this.WrittenPrem = rs.getDouble("WrittenPrem");
			this.IncomingClmReserve = rs.getDouble("IncomingClmReserve");
			this.IncomingReserve1 = rs.getDouble("IncomingReserve1");
			this.IncomingReserve2 = rs.getDouble("IncomingReserve2");
			this.SubTotalIncome = rs.getDouble("SubTotalIncome");
			this.ClaimBackFee = rs.getDouble("ClaimBackFee");
			this.ReProcFee = rs.getDouble("ReProcFee");
			this.OutgoingClmReserve = rs.getDouble("OutgoingClmReserve");
			this.OutgoingReserve1 = rs.getDouble("OutgoingReserve1");
			this.OutgoingReserve2 = rs.getDouble("OutgoingReserve2");
			this.ManageFee = rs.getDouble("ManageFee");
			this.PreYearLoss = rs.getDouble("PreYearLoss");
			this.SubTotalOutgo = rs.getDouble("SubTotalOutgo");
			this.UWProfit = rs.getDouble("UWProfit");
			this.UWProfitComm = rs.getDouble("UWProfitComm");
			this.SumUWProfit = rs.getDouble("SumUWProfit");
			this.SumUWProfitComm = rs.getDouble("SumUWProfitComm");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.MakeDate = rs.getDate("MakeDate");
			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRProfitLoss表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRProfitLossSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRProfitLossSchema getSchema()
	{
		LRProfitLossSchema aLRProfitLossSchema = new LRProfitLossSchema();
		aLRProfitLossSchema.setSchema(this);
		return aLRProfitLossSchema;
	}

	public LRProfitLossDB getDB()
	{
		LRProfitLossDB aDBOper = new LRProfitLossDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRProfitLoss描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReComCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReinsureItem)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CalType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(UWYear));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(WrittenPrem));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IncomingClmReserve));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IncomingReserve1));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IncomingReserve2));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SubTotalIncome));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ClaimBackFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ReProcFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(OutgoingClmReserve));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(OutgoingReserve1));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(OutgoingReserve2));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ManageFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PreYearLoss));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SubTotalOutgo));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(UWProfit));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(UWProfitComm));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SumUWProfit));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SumUWProfitComm));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRProfitLoss>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ReinsureItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			UWYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			WrittenPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			IncomingClmReserve = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			IncomingReserve1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			IncomingReserve2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			SubTotalIncome = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			ReProcFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			OutgoingClmReserve = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			OutgoingReserve1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			OutgoingReserve2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			ManageFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			PreYearLoss = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			SubTotalOutgo = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			UWProfit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			UWProfitComm = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			SumUWProfit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			SumUWProfitComm = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRProfitLossSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("ReComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComCode));
		}
		if (FCode.equals("ReinsureItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsureItem));
		}
		if (FCode.equals("CalType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalType));
		}
		if (FCode.equals("UWYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWYear));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("WrittenPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrittenPrem));
		}
		if (FCode.equals("IncomingClmReserve"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IncomingClmReserve));
		}
		if (FCode.equals("IncomingReserve1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IncomingReserve1));
		}
		if (FCode.equals("IncomingReserve2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IncomingReserve2));
		}
		if (FCode.equals("SubTotalIncome"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubTotalIncome));
		}
		if (FCode.equals("ClaimBackFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBackFee));
		}
		if (FCode.equals("ReProcFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReProcFee));
		}
		if (FCode.equals("OutgoingClmReserve"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutgoingClmReserve));
		}
		if (FCode.equals("OutgoingReserve1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutgoingReserve1));
		}
		if (FCode.equals("OutgoingReserve2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutgoingReserve2));
		}
		if (FCode.equals("ManageFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFee));
		}
		if (FCode.equals("PreYearLoss"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PreYearLoss));
		}
		if (FCode.equals("SubTotalOutgo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubTotalOutgo));
		}
		if (FCode.equals("UWProfit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWProfit));
		}
		if (FCode.equals("UWProfitComm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWProfitComm));
		}
		if (FCode.equals("SumUWProfit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumUWProfit));
		}
		if (FCode.equals("SumUWProfitComm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumUWProfitComm));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ReComCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ReinsureItem);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CalType);
				break;
			case 4:
				strFieldValue = String.valueOf(UWYear);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 7:
				strFieldValue = String.valueOf(WrittenPrem);
				break;
			case 8:
				strFieldValue = String.valueOf(IncomingClmReserve);
				break;
			case 9:
				strFieldValue = String.valueOf(IncomingReserve1);
				break;
			case 10:
				strFieldValue = String.valueOf(IncomingReserve2);
				break;
			case 11:
				strFieldValue = String.valueOf(SubTotalIncome);
				break;
			case 12:
				strFieldValue = String.valueOf(ClaimBackFee);
				break;
			case 13:
				strFieldValue = String.valueOf(ReProcFee);
				break;
			case 14:
				strFieldValue = String.valueOf(OutgoingClmReserve);
				break;
			case 15:
				strFieldValue = String.valueOf(OutgoingReserve1);
				break;
			case 16:
				strFieldValue = String.valueOf(OutgoingReserve2);
				break;
			case 17:
				strFieldValue = String.valueOf(ManageFee);
				break;
			case 18:
				strFieldValue = String.valueOf(PreYearLoss);
				break;
			case 19:
				strFieldValue = String.valueOf(SubTotalOutgo);
				break;
			case 20:
				strFieldValue = String.valueOf(UWProfit);
				break;
			case 21:
				strFieldValue = String.valueOf(UWProfitComm);
				break;
			case 22:
				strFieldValue = String.valueOf(SumUWProfit);
				break;
			case 23:
				strFieldValue = String.valueOf(SumUWProfitComm);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("ReComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComCode = FValue.trim();
			}
			else
				ReComCode = null;
		}
		if (FCode.equalsIgnoreCase("ReinsureItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReinsureItem = FValue.trim();
			}
			else
				ReinsureItem = null;
		}
		if (FCode.equalsIgnoreCase("CalType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalType = FValue.trim();
			}
			else
				CalType = null;
		}
		if (FCode.equalsIgnoreCase("UWYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				UWYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("WrittenPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				WrittenPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("IncomingClmReserve"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				IncomingClmReserve = d;
			}
		}
		if (FCode.equalsIgnoreCase("IncomingReserve1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				IncomingReserve1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("IncomingReserve2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				IncomingReserve2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SubTotalIncome"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SubTotalIncome = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimBackFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimBackFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReProcFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReProcFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("OutgoingClmReserve"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OutgoingClmReserve = d;
			}
		}
		if (FCode.equalsIgnoreCase("OutgoingReserve1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OutgoingReserve1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("OutgoingReserve2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OutgoingReserve2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ManageFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ManageFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("PreYearLoss"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PreYearLoss = d;
			}
		}
		if (FCode.equalsIgnoreCase("SubTotalOutgo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SubTotalOutgo = d;
			}
		}
		if (FCode.equalsIgnoreCase("UWProfit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				UWProfit = d;
			}
		}
		if (FCode.equalsIgnoreCase("UWProfitComm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				UWProfitComm = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumUWProfit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumUWProfit = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumUWProfitComm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumUWProfitComm = d;
			}
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRProfitLossSchema other = (LRProfitLossSchema)otherObject;
		return
			ReContCode.equals(other.getReContCode())
			&& ReComCode.equals(other.getReComCode())
			&& ReinsureItem.equals(other.getReinsureItem())
			&& CalType.equals(other.getCalType())
			&& UWYear == other.getUWYear()
			&& fDate.getString(StartDate).equals(other.getStartDate())
			&& fDate.getString(EndDate).equals(other.getEndDate())
			&& WrittenPrem == other.getWrittenPrem()
			&& IncomingClmReserve == other.getIncomingClmReserve()
			&& IncomingReserve1 == other.getIncomingReserve1()
			&& IncomingReserve2 == other.getIncomingReserve2()
			&& SubTotalIncome == other.getSubTotalIncome()
			&& ClaimBackFee == other.getClaimBackFee()
			&& ReProcFee == other.getReProcFee()
			&& OutgoingClmReserve == other.getOutgoingClmReserve()
			&& OutgoingReserve1 == other.getOutgoingReserve1()
			&& OutgoingReserve2 == other.getOutgoingReserve2()
			&& ManageFee == other.getManageFee()
			&& PreYearLoss == other.getPreYearLoss()
			&& SubTotalOutgo == other.getSubTotalOutgo()
			&& UWProfit == other.getUWProfit()
			&& UWProfitComm == other.getUWProfitComm()
			&& SumUWProfit == other.getSumUWProfit()
			&& SumUWProfitComm == other.getSumUWProfitComm()
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& Operator.equals(other.getOperator());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ReContCode") ) {
			return 0;
		}
		if( strFieldName.equals("ReComCode") ) {
			return 1;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return 2;
		}
		if( strFieldName.equals("CalType") ) {
			return 3;
		}
		if( strFieldName.equals("UWYear") ) {
			return 4;
		}
		if( strFieldName.equals("StartDate") ) {
			return 5;
		}
		if( strFieldName.equals("EndDate") ) {
			return 6;
		}
		if( strFieldName.equals("WrittenPrem") ) {
			return 7;
		}
		if( strFieldName.equals("IncomingClmReserve") ) {
			return 8;
		}
		if( strFieldName.equals("IncomingReserve1") ) {
			return 9;
		}
		if( strFieldName.equals("IncomingReserve2") ) {
			return 10;
		}
		if( strFieldName.equals("SubTotalIncome") ) {
			return 11;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return 12;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return 13;
		}
		if( strFieldName.equals("OutgoingClmReserve") ) {
			return 14;
		}
		if( strFieldName.equals("OutgoingReserve1") ) {
			return 15;
		}
		if( strFieldName.equals("OutgoingReserve2") ) {
			return 16;
		}
		if( strFieldName.equals("ManageFee") ) {
			return 17;
		}
		if( strFieldName.equals("PreYearLoss") ) {
			return 18;
		}
		if( strFieldName.equals("SubTotalOutgo") ) {
			return 19;
		}
		if( strFieldName.equals("UWProfit") ) {
			return 20;
		}
		if( strFieldName.equals("UWProfitComm") ) {
			return 21;
		}
		if( strFieldName.equals("SumUWProfit") ) {
			return 22;
		}
		if( strFieldName.equals("SumUWProfitComm") ) {
			return 23;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 24;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 26;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 27;
		}
		if( strFieldName.equals("Operator") ) {
			return 28;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ReContCode";
				break;
			case 1:
				strFieldName = "ReComCode";
				break;
			case 2:
				strFieldName = "ReinsureItem";
				break;
			case 3:
				strFieldName = "CalType";
				break;
			case 4:
				strFieldName = "UWYear";
				break;
			case 5:
				strFieldName = "StartDate";
				break;
			case 6:
				strFieldName = "EndDate";
				break;
			case 7:
				strFieldName = "WrittenPrem";
				break;
			case 8:
				strFieldName = "IncomingClmReserve";
				break;
			case 9:
				strFieldName = "IncomingReserve1";
				break;
			case 10:
				strFieldName = "IncomingReserve2";
				break;
			case 11:
				strFieldName = "SubTotalIncome";
				break;
			case 12:
				strFieldName = "ClaimBackFee";
				break;
			case 13:
				strFieldName = "ReProcFee";
				break;
			case 14:
				strFieldName = "OutgoingClmReserve";
				break;
			case 15:
				strFieldName = "OutgoingReserve1";
				break;
			case 16:
				strFieldName = "OutgoingReserve2";
				break;
			case 17:
				strFieldName = "ManageFee";
				break;
			case 18:
				strFieldName = "PreYearLoss";
				break;
			case 19:
				strFieldName = "SubTotalOutgo";
				break;
			case 20:
				strFieldName = "UWProfit";
				break;
			case 21:
				strFieldName = "UWProfitComm";
				break;
			case 22:
				strFieldName = "SumUWProfit";
				break;
			case 23:
				strFieldName = "SumUWProfitComm";
				break;
			case 24:
				strFieldName = "MakeTime";
				break;
			case 25:
				strFieldName = "MakeDate";
				break;
			case 26:
				strFieldName = "ModifyDate";
				break;
			case 27:
				strFieldName = "ModifyTime";
				break;
			case 28:
				strFieldName = "Operator";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("WrittenPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IncomingClmReserve") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IncomingReserve1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IncomingReserve2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SubTotalIncome") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OutgoingClmReserve") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OutgoingReserve1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OutgoingReserve2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ManageFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PreYearLoss") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SubTotalOutgo") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("UWProfit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("UWProfitComm") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumUWProfit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumUWProfitComm") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
