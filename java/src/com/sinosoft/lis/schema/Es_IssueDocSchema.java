/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.Es_IssueDocDB;

/*
 * <p>ClassName: Es_IssueDocSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 扫描问题件
 * @CreateDate：2015-02-12
 */
public class Es_IssueDocSchema implements Schema, Cloneable
{
	// @Field
	/** 问题流水号 */
	private String IssueDocID;
	/** 业务号码 */
	private String BussNo;
	/** 业务号码类型 */
	private String BussNoType;
	/** 业务类型 */
	private String BussType;
	/** 单证细类 */
	private String SubType;
	/** 问题描述 */
	private String IssueDesc;
	/** 处理状态 */
	private String Status;
	/** 处理结果 */
	private String Result;
	/** 回复操作员 */
	private String ReplyOperator;
	/** 提出操作员 */
	private String PromptOperator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 审核状态 */
	private String StateFlag;
	/** 回复信息 */
	private String CheckContent;
	/** 审核人 */
	private String CheckOperator;
	/** 审核日期 */
	private Date CheckDate;
	/** 审核时间 */
	private String CheckTime;
	/** 问题类型 */
	private String IssueType;

	public static final int FIELDNUM = 20;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public Es_IssueDocSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "IssueDocID";
		pk[1] = "BussNo";
		pk[2] = "BussNoType";
		pk[3] = "SubType";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		Es_IssueDocSchema cloned = (Es_IssueDocSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getIssueDocID()
	{
		return IssueDocID;
	}
	public void setIssueDocID(String aIssueDocID)
	{
		IssueDocID = aIssueDocID;
	}
	public String getBussNo()
	{
		return BussNo;
	}
	public void setBussNo(String aBussNo)
	{
		BussNo = aBussNo;
	}
	public String getBussNoType()
	{
		return BussNoType;
	}
	public void setBussNoType(String aBussNoType)
	{
		BussNoType = aBussNoType;
	}
	public String getBussType()
	{
		return BussType;
	}
	public void setBussType(String aBussType)
	{
		BussType = aBussType;
	}
	public String getSubType()
	{
		return SubType;
	}
	public void setSubType(String aSubType)
	{
		SubType = aSubType;
	}
	public String getIssueDesc()
	{
		return IssueDesc;
	}
	public void setIssueDesc(String aIssueDesc)
	{
		IssueDesc = aIssueDesc;
	}
	public String getStatus()
	{
		return Status;
	}
	public void setStatus(String aStatus)
	{
		Status = aStatus;
	}
	public String getResult()
	{
		return Result;
	}
	public void setResult(String aResult)
	{
		Result = aResult;
	}
	public String getReplyOperator()
	{
		return ReplyOperator;
	}
	public void setReplyOperator(String aReplyOperator)
	{
		ReplyOperator = aReplyOperator;
	}
	public String getPromptOperator()
	{
		return PromptOperator;
	}
	public void setPromptOperator(String aPromptOperator)
	{
		PromptOperator = aPromptOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getCheckContent()
	{
		return CheckContent;
	}
	public void setCheckContent(String aCheckContent)
	{
		CheckContent = aCheckContent;
	}
	public String getCheckOperator()
	{
		return CheckOperator;
	}
	public void setCheckOperator(String aCheckOperator)
	{
		CheckOperator = aCheckOperator;
	}
	public String getCheckDate()
	{
		if( CheckDate != null )
			return fDate.getString(CheckDate);
		else
			return null;
	}
	public void setCheckDate(Date aCheckDate)
	{
		CheckDate = aCheckDate;
	}
	public void setCheckDate(String aCheckDate)
	{
		if (aCheckDate != null && !aCheckDate.equals("") )
		{
			CheckDate = fDate.getDate( aCheckDate );
		}
		else
			CheckDate = null;
	}

	public String getCheckTime()
	{
		return CheckTime;
	}
	public void setCheckTime(String aCheckTime)
	{
		CheckTime = aCheckTime;
	}
	public String getIssueType()
	{
		return IssueType;
	}
	public void setIssueType(String aIssueType)
	{
		IssueType = aIssueType;
	}

	/**
	* 使用另外一个 Es_IssueDocSchema 对象给 Schema 赋值
	* @param: aEs_IssueDocSchema Es_IssueDocSchema
	**/
	public void setSchema(Es_IssueDocSchema aEs_IssueDocSchema)
	{
		this.IssueDocID = aEs_IssueDocSchema.getIssueDocID();
		this.BussNo = aEs_IssueDocSchema.getBussNo();
		this.BussNoType = aEs_IssueDocSchema.getBussNoType();
		this.BussType = aEs_IssueDocSchema.getBussType();
		this.SubType = aEs_IssueDocSchema.getSubType();
		this.IssueDesc = aEs_IssueDocSchema.getIssueDesc();
		this.Status = aEs_IssueDocSchema.getStatus();
		this.Result = aEs_IssueDocSchema.getResult();
		this.ReplyOperator = aEs_IssueDocSchema.getReplyOperator();
		this.PromptOperator = aEs_IssueDocSchema.getPromptOperator();
		this.MakeDate = fDate.getDate( aEs_IssueDocSchema.getMakeDate());
		this.MakeTime = aEs_IssueDocSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aEs_IssueDocSchema.getModifyDate());
		this.ModifyTime = aEs_IssueDocSchema.getModifyTime();
		this.StateFlag = aEs_IssueDocSchema.getStateFlag();
		this.CheckContent = aEs_IssueDocSchema.getCheckContent();
		this.CheckOperator = aEs_IssueDocSchema.getCheckOperator();
		this.CheckDate = fDate.getDate( aEs_IssueDocSchema.getCheckDate());
		this.CheckTime = aEs_IssueDocSchema.getCheckTime();
		this.IssueType = aEs_IssueDocSchema.getIssueType();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("IssueDocID") == null )
				this.IssueDocID = null;
			else
				this.IssueDocID = rs.getString("IssueDocID").trim();

			if( rs.getString("BussNo") == null )
				this.BussNo = null;
			else
				this.BussNo = rs.getString("BussNo").trim();

			if( rs.getString("BussNoType") == null )
				this.BussNoType = null;
			else
				this.BussNoType = rs.getString("BussNoType").trim();

			if( rs.getString("BussType") == null )
				this.BussType = null;
			else
				this.BussType = rs.getString("BussType").trim();

			if( rs.getString("SubType") == null )
				this.SubType = null;
			else
				this.SubType = rs.getString("SubType").trim();

			if( rs.getString("IssueDesc") == null )
				this.IssueDesc = null;
			else
				this.IssueDesc = rs.getString("IssueDesc").trim();

			if( rs.getString("Status") == null )
				this.Status = null;
			else
				this.Status = rs.getString("Status").trim();

			if( rs.getString("Result") == null )
				this.Result = null;
			else
				this.Result = rs.getString("Result").trim();

			if( rs.getString("ReplyOperator") == null )
				this.ReplyOperator = null;
			else
				this.ReplyOperator = rs.getString("ReplyOperator").trim();

			if( rs.getString("PromptOperator") == null )
				this.PromptOperator = null;
			else
				this.PromptOperator = rs.getString("PromptOperator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("CheckContent") == null )
				this.CheckContent = null;
			else
				this.CheckContent = rs.getString("CheckContent").trim();

			if( rs.getString("CheckOperator") == null )
				this.CheckOperator = null;
			else
				this.CheckOperator = rs.getString("CheckOperator").trim();

			this.CheckDate = rs.getDate("CheckDate");
			if( rs.getString("CheckTime") == null )
				this.CheckTime = null;
			else
				this.CheckTime = rs.getString("CheckTime").trim();

			if( rs.getString("IssueType") == null )
				this.IssueType = null;
			else
				this.IssueType = rs.getString("IssueType").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的Es_IssueDoc表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Es_IssueDocSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public Es_IssueDocSchema getSchema()
	{
		Es_IssueDocSchema aEs_IssueDocSchema = new Es_IssueDocSchema();
		aEs_IssueDocSchema.setSchema(this);
		return aEs_IssueDocSchema;
	}

	public Es_IssueDocDB getDB()
	{
		Es_IssueDocDB aDBOper = new Es_IssueDocDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpEs_IssueDoc描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(IssueDocID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BussNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BussNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BussType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Result)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReplyOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PromptOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckContent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CheckDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueType));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpEs_IssueDoc>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			IssueDocID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BussNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BussNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SubType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IssueDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Result = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ReplyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PromptOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			CheckContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CheckOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CheckDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			CheckTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			IssueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Es_IssueDocSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("IssueDocID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueDocID));
		}
		if (FCode.equals("BussNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BussNo));
		}
		if (FCode.equals("BussNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BussNoType));
		}
		if (FCode.equals("BussType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BussType));
		}
		if (FCode.equals("SubType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubType));
		}
		if (FCode.equals("IssueDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueDesc));
		}
		if (FCode.equals("Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
		}
		if (FCode.equals("Result"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Result));
		}
		if (FCode.equals("ReplyOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyOperator));
		}
		if (FCode.equals("PromptOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PromptOperator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("CheckContent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckContent));
		}
		if (FCode.equals("CheckOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckOperator));
		}
		if (FCode.equals("CheckDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCheckDate()));
		}
		if (FCode.equals("CheckTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckTime));
		}
		if (FCode.equals("IssueType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueType));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(IssueDocID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BussNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BussNoType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BussType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SubType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IssueDesc);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Status);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Result);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ReplyOperator);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PromptOperator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(CheckContent);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CheckOperator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCheckDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(CheckTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(IssueType);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("IssueDocID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueDocID = FValue.trim();
			}
			else
				IssueDocID = null;
		}
		if (FCode.equalsIgnoreCase("BussNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BussNo = FValue.trim();
			}
			else
				BussNo = null;
		}
		if (FCode.equalsIgnoreCase("BussNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BussNoType = FValue.trim();
			}
			else
				BussNoType = null;
		}
		if (FCode.equalsIgnoreCase("BussType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BussType = FValue.trim();
			}
			else
				BussType = null;
		}
		if (FCode.equalsIgnoreCase("SubType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubType = FValue.trim();
			}
			else
				SubType = null;
		}
		if (FCode.equalsIgnoreCase("IssueDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueDesc = FValue.trim();
			}
			else
				IssueDesc = null;
		}
		if (FCode.equalsIgnoreCase("Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status = FValue.trim();
			}
			else
				Status = null;
		}
		if (FCode.equalsIgnoreCase("Result"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Result = FValue.trim();
			}
			else
				Result = null;
		}
		if (FCode.equalsIgnoreCase("ReplyOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReplyOperator = FValue.trim();
			}
			else
				ReplyOperator = null;
		}
		if (FCode.equalsIgnoreCase("PromptOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PromptOperator = FValue.trim();
			}
			else
				PromptOperator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("CheckContent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckContent = FValue.trim();
			}
			else
				CheckContent = null;
		}
		if (FCode.equalsIgnoreCase("CheckOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckOperator = FValue.trim();
			}
			else
				CheckOperator = null;
		}
		if (FCode.equalsIgnoreCase("CheckDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CheckDate = fDate.getDate( FValue );
			}
			else
				CheckDate = null;
		}
		if (FCode.equalsIgnoreCase("CheckTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckTime = FValue.trim();
			}
			else
				CheckTime = null;
		}
		if (FCode.equalsIgnoreCase("IssueType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueType = FValue.trim();
			}
			else
				IssueType = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		Es_IssueDocSchema other = (Es_IssueDocSchema)otherObject;
		return
			(IssueDocID == null ? other.getIssueDocID() == null : IssueDocID.equals(other.getIssueDocID()))
			&& (BussNo == null ? other.getBussNo() == null : BussNo.equals(other.getBussNo()))
			&& (BussNoType == null ? other.getBussNoType() == null : BussNoType.equals(other.getBussNoType()))
			&& (BussType == null ? other.getBussType() == null : BussType.equals(other.getBussType()))
			&& (SubType == null ? other.getSubType() == null : SubType.equals(other.getSubType()))
			&& (IssueDesc == null ? other.getIssueDesc() == null : IssueDesc.equals(other.getIssueDesc()))
			&& (Status == null ? other.getStatus() == null : Status.equals(other.getStatus()))
			&& (Result == null ? other.getResult() == null : Result.equals(other.getResult()))
			&& (ReplyOperator == null ? other.getReplyOperator() == null : ReplyOperator.equals(other.getReplyOperator()))
			&& (PromptOperator == null ? other.getPromptOperator() == null : PromptOperator.equals(other.getPromptOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (CheckContent == null ? other.getCheckContent() == null : CheckContent.equals(other.getCheckContent()))
			&& (CheckOperator == null ? other.getCheckOperator() == null : CheckOperator.equals(other.getCheckOperator()))
			&& (CheckDate == null ? other.getCheckDate() == null : fDate.getString(CheckDate).equals(other.getCheckDate()))
			&& (CheckTime == null ? other.getCheckTime() == null : CheckTime.equals(other.getCheckTime()))
			&& (IssueType == null ? other.getIssueType() == null : IssueType.equals(other.getIssueType()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("IssueDocID") ) {
			return 0;
		}
		if( strFieldName.equals("BussNo") ) {
			return 1;
		}
		if( strFieldName.equals("BussNoType") ) {
			return 2;
		}
		if( strFieldName.equals("BussType") ) {
			return 3;
		}
		if( strFieldName.equals("SubType") ) {
			return 4;
		}
		if( strFieldName.equals("IssueDesc") ) {
			return 5;
		}
		if( strFieldName.equals("Status") ) {
			return 6;
		}
		if( strFieldName.equals("Result") ) {
			return 7;
		}
		if( strFieldName.equals("ReplyOperator") ) {
			return 8;
		}
		if( strFieldName.equals("PromptOperator") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 13;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 14;
		}
		if( strFieldName.equals("CheckContent") ) {
			return 15;
		}
		if( strFieldName.equals("CheckOperator") ) {
			return 16;
		}
		if( strFieldName.equals("CheckDate") ) {
			return 17;
		}
		if( strFieldName.equals("CheckTime") ) {
			return 18;
		}
		if( strFieldName.equals("IssueType") ) {
			return 19;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "IssueDocID";
				break;
			case 1:
				strFieldName = "BussNo";
				break;
			case 2:
				strFieldName = "BussNoType";
				break;
			case 3:
				strFieldName = "BussType";
				break;
			case 4:
				strFieldName = "SubType";
				break;
			case 5:
				strFieldName = "IssueDesc";
				break;
			case 6:
				strFieldName = "Status";
				break;
			case 7:
				strFieldName = "Result";
				break;
			case 8:
				strFieldName = "ReplyOperator";
				break;
			case 9:
				strFieldName = "PromptOperator";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "ModifyDate";
				break;
			case 13:
				strFieldName = "ModifyTime";
				break;
			case 14:
				strFieldName = "StateFlag";
				break;
			case 15:
				strFieldName = "CheckContent";
				break;
			case 16:
				strFieldName = "CheckOperator";
				break;
			case 17:
				strFieldName = "CheckDate";
				break;
			case 18:
				strFieldName = "CheckTime";
				break;
			case 19:
				strFieldName = "IssueType";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("IssueDocID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Result") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReplyOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PromptOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckContent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CheckTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueType") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
