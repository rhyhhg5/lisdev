/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.StatConfigDB;

/*
 * <p>ClassName: StatConfigSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-09-07
 */
public class StatConfigSchema implements Schema, Cloneable {
    // @Field
    /** 模块代码 */
    private String ModuCode;
    /** 模块名称 */
    private String ModuName;
    /** 模块入口代码 */
    private String EntranceCode;
    /** 模块入口检索表 */
    private String EntranceTable;
    /** 模块检索sql */
    private String RunSql;
    /** 描述 */
    private String Describe;
    /** 是否启动 */
    private String State;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public StatConfigSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ModuCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        StatConfigSchema cloned = (StatConfigSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getModuCode() {
        return ModuCode;
    }

    public void setModuCode(String aModuCode) {
        ModuCode = aModuCode;
    }

    public String getModuName() {
        return ModuName;
    }

    public void setModuName(String aModuName) {
        ModuName = aModuName;
    }

    public String getEntranceCode() {
        return EntranceCode;
    }

    public void setEntranceCode(String aEntranceCode) {
        EntranceCode = aEntranceCode;
    }

    public String getEntranceTable() {
        return EntranceTable;
    }

    public void setEntranceTable(String aEntranceTable) {
        EntranceTable = aEntranceTable;
    }

    public String getRunSql() {
        return RunSql;
    }

    public void setRunSql(String aRunSql) {
        RunSql = aRunSql;
    }

    public String getDescribe() {
        return Describe;
    }

    public void setDescribe(String aDescribe) {
        Describe = aDescribe;
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    /**
     * 使用另外一个 StatConfigSchema 对象给 Schema 赋值
     * @param: aStatConfigSchema StatConfigSchema
     **/
    public void setSchema(StatConfigSchema aStatConfigSchema) {
        this.ModuCode = aStatConfigSchema.getModuCode();
        this.ModuName = aStatConfigSchema.getModuName();
        this.EntranceCode = aStatConfigSchema.getEntranceCode();
        this.EntranceTable = aStatConfigSchema.getEntranceTable();
        this.RunSql = aStatConfigSchema.getRunSql();
        this.Describe = aStatConfigSchema.getDescribe();
        this.State = aStatConfigSchema.getState();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ModuCode") == null) {
                this.ModuCode = null;
            } else {
                this.ModuCode = rs.getString("ModuCode").trim();
            }

            if (rs.getString("ModuName") == null) {
                this.ModuName = null;
            } else {
                this.ModuName = rs.getString("ModuName").trim();
            }

            if (rs.getString("EntranceCode") == null) {
                this.EntranceCode = null;
            } else {
                this.EntranceCode = rs.getString("EntranceCode").trim();
            }

            if (rs.getString("EntranceTable") == null) {
                this.EntranceTable = null;
            } else {
                this.EntranceTable = rs.getString("EntranceTable").trim();
            }

            if (rs.getString("RunSql") == null) {
                this.RunSql = null;
            } else {
                this.RunSql = rs.getString("RunSql").trim();
            }

            if (rs.getString("Describe") == null) {
                this.Describe = null;
            } else {
                this.Describe = rs.getString("Describe").trim();
            }

            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的StatConfig表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "StatConfigSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public StatConfigSchema getSchema() {
        StatConfigSchema aStatConfigSchema = new StatConfigSchema();
        aStatConfigSchema.setSchema(this);
        return aStatConfigSchema;
    }

    public StatConfigDB getDB() {
        StatConfigDB aDBOper = new StatConfigDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStatConfig描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ModuCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModuName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EntranceCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EntranceTable));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RunSql));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Describe));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStatConfig>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ModuCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ModuName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            EntranceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            EntranceTable = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            RunSql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            Describe = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "StatConfigSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ModuCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModuCode));
        }
        if (FCode.equals("ModuName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModuName));
        }
        if (FCode.equals("EntranceCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EntranceCode));
        }
        if (FCode.equals("EntranceTable")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EntranceTable));
        }
        if (FCode.equals("RunSql")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RunSql));
        }
        if (FCode.equals("Describe")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Describe));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ModuCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ModuName);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(EntranceCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(EntranceTable);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(RunSql);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Describe);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ModuCode")) {
            if (FValue != null && !FValue.equals("")) {
                ModuCode = FValue.trim();
            } else {
                ModuCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModuName")) {
            if (FValue != null && !FValue.equals("")) {
                ModuName = FValue.trim();
            } else {
                ModuName = null;
            }
        }
        if (FCode.equalsIgnoreCase("EntranceCode")) {
            if (FValue != null && !FValue.equals("")) {
                EntranceCode = FValue.trim();
            } else {
                EntranceCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("EntranceTable")) {
            if (FValue != null && !FValue.equals("")) {
                EntranceTable = FValue.trim();
            } else {
                EntranceTable = null;
            }
        }
        if (FCode.equalsIgnoreCase("RunSql")) {
            if (FValue != null && !FValue.equals("")) {
                RunSql = FValue.trim();
            } else {
                RunSql = null;
            }
        }
        if (FCode.equalsIgnoreCase("Describe")) {
            if (FValue != null && !FValue.equals("")) {
                Describe = FValue.trim();
            } else {
                Describe = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        StatConfigSchema other = (StatConfigSchema) otherObject;
        return
                ModuCode.equals(other.getModuCode())
                && ModuName.equals(other.getModuName())
                && EntranceCode.equals(other.getEntranceCode())
                && EntranceTable.equals(other.getEntranceTable())
                && RunSql.equals(other.getRunSql())
                && Describe.equals(other.getDescribe())
                && State.equals(other.getState());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ModuCode")) {
            return 0;
        }
        if (strFieldName.equals("ModuName")) {
            return 1;
        }
        if (strFieldName.equals("EntranceCode")) {
            return 2;
        }
        if (strFieldName.equals("EntranceTable")) {
            return 3;
        }
        if (strFieldName.equals("RunSql")) {
            return 4;
        }
        if (strFieldName.equals("Describe")) {
            return 5;
        }
        if (strFieldName.equals("State")) {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ModuCode";
            break;
        case 1:
            strFieldName = "ModuName";
            break;
        case 2:
            strFieldName = "EntranceCode";
            break;
        case 3:
            strFieldName = "EntranceTable";
            break;
        case 4:
            strFieldName = "RunSql";
            break;
        case 5:
            strFieldName = "Describe";
            break;
        case 6:
            strFieldName = "State";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ModuCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModuName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EntranceCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EntranceTable")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RunSql")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Describe")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
