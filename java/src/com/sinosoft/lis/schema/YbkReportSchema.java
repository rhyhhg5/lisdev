/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.YbkReportDB;

/*
 * <p>ClassName: YbkReportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新表
 * @CreateDate：2019-05-16
 */
public class YbkReportSchema implements Schema, Cloneable
{
	// @Field
	/** 咨询号码 */
	private String ConsultNo;
	/** 登记号码 */
	private String LogNo;
	/** 保单编码 */
	private String PolicySequenceNo;
	/** 理赔流水号 */
	private String ClaimFormerNo;
	/** 委托人 */
	private String AppointName;
	/** 报案人 */
	private String ReportName;
	/** 报案人与出险人关系 */
	private String RelationShip;
	/** 联系电话 */
	private String ReportPhone;
	/** 联系地址 */
	private String ReportAddr;
	/** 治疗医院 */
	private String Hospital;
	/** 入院诊断 */
	private String AdmissionDiagnosis;
	/** 出险日期 */
	private Date AccidentDate;
	/** 事故说明 */
	private String AccidentDes;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 案件号 */
	private String CaseNo;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public YbkReportSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ConsultNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		YbkReportSchema cloned = (YbkReportSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getConsultNo()
	{
		return ConsultNo;
	}
	public void setConsultNo(String aConsultNo)
	{
		ConsultNo = aConsultNo;
	}
	public String getLogNo()
	{
		return LogNo;
	}
	public void setLogNo(String aLogNo)
	{
		LogNo = aLogNo;
	}
	public String getPolicySequenceNo()
	{
		return PolicySequenceNo;
	}
	public void setPolicySequenceNo(String aPolicySequenceNo)
	{
		PolicySequenceNo = aPolicySequenceNo;
	}
	public String getClaimFormerNo()
	{
		return ClaimFormerNo;
	}
	public void setClaimFormerNo(String aClaimFormerNo)
	{
		ClaimFormerNo = aClaimFormerNo;
	}
	public String getAppointName()
	{
		return AppointName;
	}
	public void setAppointName(String aAppointName)
	{
		AppointName = aAppointName;
	}
	public String getReportName()
	{
		return ReportName;
	}
	public void setReportName(String aReportName)
	{
		ReportName = aReportName;
	}
	public String getRelationShip()
	{
		return RelationShip;
	}
	public void setRelationShip(String aRelationShip)
	{
		RelationShip = aRelationShip;
	}
	public String getReportPhone()
	{
		return ReportPhone;
	}
	public void setReportPhone(String aReportPhone)
	{
		ReportPhone = aReportPhone;
	}
	public String getReportAddr()
	{
		return ReportAddr;
	}
	public void setReportAddr(String aReportAddr)
	{
		ReportAddr = aReportAddr;
	}
	public String getHospital()
	{
		return Hospital;
	}
	public void setHospital(String aHospital)
	{
		Hospital = aHospital;
	}
	public String getAdmissionDiagnosis()
	{
		return AdmissionDiagnosis;
	}
	public void setAdmissionDiagnosis(String aAdmissionDiagnosis)
	{
		AdmissionDiagnosis = aAdmissionDiagnosis;
	}
	public String getAccidentDate()
	{
		if( AccidentDate != null )
			return fDate.getString(AccidentDate);
		else
			return null;
	}
	public void setAccidentDate(Date aAccidentDate)
	{
		AccidentDate = aAccidentDate;
	}
	public void setAccidentDate(String aAccidentDate)
	{
		if (aAccidentDate != null && !aAccidentDate.equals("") )
		{
			AccidentDate = fDate.getDate( aAccidentDate );
		}
		else
			AccidentDate = null;
	}

	public String getAccidentDes()
	{
		return AccidentDes;
	}
	public void setAccidentDes(String aAccidentDes)
	{
		AccidentDes = aAccidentDes;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}

	/**
	* 使用另外一个 YbkReportSchema 对象给 Schema 赋值
	* @param: aYbkReportSchema YbkReportSchema
	**/
	public void setSchema(YbkReportSchema aYbkReportSchema)
	{
		this.ConsultNo = aYbkReportSchema.getConsultNo();
		this.LogNo = aYbkReportSchema.getLogNo();
		this.PolicySequenceNo = aYbkReportSchema.getPolicySequenceNo();
		this.ClaimFormerNo = aYbkReportSchema.getClaimFormerNo();
		this.AppointName = aYbkReportSchema.getAppointName();
		this.ReportName = aYbkReportSchema.getReportName();
		this.RelationShip = aYbkReportSchema.getRelationShip();
		this.ReportPhone = aYbkReportSchema.getReportPhone();
		this.ReportAddr = aYbkReportSchema.getReportAddr();
		this.Hospital = aYbkReportSchema.getHospital();
		this.AdmissionDiagnosis = aYbkReportSchema.getAdmissionDiagnosis();
		this.AccidentDate = fDate.getDate( aYbkReportSchema.getAccidentDate());
		this.AccidentDes = aYbkReportSchema.getAccidentDes();
		this.MakeDate = fDate.getDate( aYbkReportSchema.getMakeDate());
		this.MakeTime = aYbkReportSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aYbkReportSchema.getModifyDate());
		this.ModifyTime = aYbkReportSchema.getModifyTime();
		this.CaseNo = aYbkReportSchema.getCaseNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ConsultNo") == null )
				this.ConsultNo = null;
			else
				this.ConsultNo = rs.getString("ConsultNo").trim();

			if( rs.getString("LogNo") == null )
				this.LogNo = null;
			else
				this.LogNo = rs.getString("LogNo").trim();

			if( rs.getString("PolicySequenceNo") == null )
				this.PolicySequenceNo = null;
			else
				this.PolicySequenceNo = rs.getString("PolicySequenceNo").trim();

			if( rs.getString("ClaimFormerNo") == null )
				this.ClaimFormerNo = null;
			else
				this.ClaimFormerNo = rs.getString("ClaimFormerNo").trim();

			if( rs.getString("AppointName") == null )
				this.AppointName = null;
			else
				this.AppointName = rs.getString("AppointName").trim();

			if( rs.getString("ReportName") == null )
				this.ReportName = null;
			else
				this.ReportName = rs.getString("ReportName").trim();

			if( rs.getString("RelationShip") == null )
				this.RelationShip = null;
			else
				this.RelationShip = rs.getString("RelationShip").trim();

			if( rs.getString("ReportPhone") == null )
				this.ReportPhone = null;
			else
				this.ReportPhone = rs.getString("ReportPhone").trim();

			if( rs.getString("ReportAddr") == null )
				this.ReportAddr = null;
			else
				this.ReportAddr = rs.getString("ReportAddr").trim();

			if( rs.getString("Hospital") == null )
				this.Hospital = null;
			else
				this.Hospital = rs.getString("Hospital").trim();

			if( rs.getString("AdmissionDiagnosis") == null )
				this.AdmissionDiagnosis = null;
			else
				this.AdmissionDiagnosis = rs.getString("AdmissionDiagnosis").trim();

			this.AccidentDate = rs.getDate("AccidentDate");
			if( rs.getString("AccidentDes") == null )
				this.AccidentDes = null;
			else
				this.AccidentDes = rs.getString("AccidentDes").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的YbkReport表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YbkReportSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public YbkReportSchema getSchema()
	{
		YbkReportSchema aYbkReportSchema = new YbkReportSchema();
		aYbkReportSchema.setSchema(this);
		return aYbkReportSchema;
	}

	public YbkReportDB getDB()
	{
		YbkReportDB aDBOper = new YbkReportDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYbkReport描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ConsultNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LogNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicySequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimFormerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppointName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationShip)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportAddr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Hospital)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AdmissionDiagnosis)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccidentDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccidentDes)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYbkReport>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ConsultNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			LogNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PolicySequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ClaimFormerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AppointName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ReportName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			RelationShip = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ReportPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ReportAddr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Hospital = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AdmissionDiagnosis = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AccidentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			AccidentDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YbkReportSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ConsultNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConsultNo));
		}
		if (FCode.equals("LogNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LogNo));
		}
		if (FCode.equals("PolicySequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicySequenceNo));
		}
		if (FCode.equals("ClaimFormerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimFormerNo));
		}
		if (FCode.equals("AppointName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppointName));
		}
		if (FCode.equals("ReportName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportName));
		}
		if (FCode.equals("RelationShip"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationShip));
		}
		if (FCode.equals("ReportPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportPhone));
		}
		if (FCode.equals("ReportAddr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportAddr));
		}
		if (FCode.equals("Hospital"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Hospital));
		}
		if (FCode.equals("AdmissionDiagnosis"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AdmissionDiagnosis));
		}
		if (FCode.equals("AccidentDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
		}
		if (FCode.equals("AccidentDes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentDes));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ConsultNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(LogNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(PolicySequenceNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ClaimFormerNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AppointName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ReportName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(RelationShip);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ReportPhone);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ReportAddr);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Hospital);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AdmissionDiagnosis);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(AccidentDes);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ConsultNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConsultNo = FValue.trim();
			}
			else
				ConsultNo = null;
		}
		if (FCode.equalsIgnoreCase("LogNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LogNo = FValue.trim();
			}
			else
				LogNo = null;
		}
		if (FCode.equalsIgnoreCase("PolicySequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicySequenceNo = FValue.trim();
			}
			else
				PolicySequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("ClaimFormerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimFormerNo = FValue.trim();
			}
			else
				ClaimFormerNo = null;
		}
		if (FCode.equalsIgnoreCase("AppointName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppointName = FValue.trim();
			}
			else
				AppointName = null;
		}
		if (FCode.equalsIgnoreCase("ReportName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportName = FValue.trim();
			}
			else
				ReportName = null;
		}
		if (FCode.equalsIgnoreCase("RelationShip"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationShip = FValue.trim();
			}
			else
				RelationShip = null;
		}
		if (FCode.equalsIgnoreCase("ReportPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportPhone = FValue.trim();
			}
			else
				ReportPhone = null;
		}
		if (FCode.equalsIgnoreCase("ReportAddr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportAddr = FValue.trim();
			}
			else
				ReportAddr = null;
		}
		if (FCode.equalsIgnoreCase("Hospital"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Hospital = FValue.trim();
			}
			else
				Hospital = null;
		}
		if (FCode.equalsIgnoreCase("AdmissionDiagnosis"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AdmissionDiagnosis = FValue.trim();
			}
			else
				AdmissionDiagnosis = null;
		}
		if (FCode.equalsIgnoreCase("AccidentDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccidentDate = fDate.getDate( FValue );
			}
			else
				AccidentDate = null;
		}
		if (FCode.equalsIgnoreCase("AccidentDes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccidentDes = FValue.trim();
			}
			else
				AccidentDes = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		YbkReportSchema other = (YbkReportSchema)otherObject;
		return
			(ConsultNo == null ? other.getConsultNo() == null : ConsultNo.equals(other.getConsultNo()))
			&& (LogNo == null ? other.getLogNo() == null : LogNo.equals(other.getLogNo()))
			&& (PolicySequenceNo == null ? other.getPolicySequenceNo() == null : PolicySequenceNo.equals(other.getPolicySequenceNo()))
			&& (ClaimFormerNo == null ? other.getClaimFormerNo() == null : ClaimFormerNo.equals(other.getClaimFormerNo()))
			&& (AppointName == null ? other.getAppointName() == null : AppointName.equals(other.getAppointName()))
			&& (ReportName == null ? other.getReportName() == null : ReportName.equals(other.getReportName()))
			&& (RelationShip == null ? other.getRelationShip() == null : RelationShip.equals(other.getRelationShip()))
			&& (ReportPhone == null ? other.getReportPhone() == null : ReportPhone.equals(other.getReportPhone()))
			&& (ReportAddr == null ? other.getReportAddr() == null : ReportAddr.equals(other.getReportAddr()))
			&& (Hospital == null ? other.getHospital() == null : Hospital.equals(other.getHospital()))
			&& (AdmissionDiagnosis == null ? other.getAdmissionDiagnosis() == null : AdmissionDiagnosis.equals(other.getAdmissionDiagnosis()))
			&& (AccidentDate == null ? other.getAccidentDate() == null : fDate.getString(AccidentDate).equals(other.getAccidentDate()))
			&& (AccidentDes == null ? other.getAccidentDes() == null : AccidentDes.equals(other.getAccidentDes()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ConsultNo") ) {
			return 0;
		}
		if( strFieldName.equals("LogNo") ) {
			return 1;
		}
		if( strFieldName.equals("PolicySequenceNo") ) {
			return 2;
		}
		if( strFieldName.equals("ClaimFormerNo") ) {
			return 3;
		}
		if( strFieldName.equals("AppointName") ) {
			return 4;
		}
		if( strFieldName.equals("ReportName") ) {
			return 5;
		}
		if( strFieldName.equals("RelationShip") ) {
			return 6;
		}
		if( strFieldName.equals("ReportPhone") ) {
			return 7;
		}
		if( strFieldName.equals("ReportAddr") ) {
			return 8;
		}
		if( strFieldName.equals("Hospital") ) {
			return 9;
		}
		if( strFieldName.equals("AdmissionDiagnosis") ) {
			return 10;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return 11;
		}
		if( strFieldName.equals("AccidentDes") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ConsultNo";
				break;
			case 1:
				strFieldName = "LogNo";
				break;
			case 2:
				strFieldName = "PolicySequenceNo";
				break;
			case 3:
				strFieldName = "ClaimFormerNo";
				break;
			case 4:
				strFieldName = "AppointName";
				break;
			case 5:
				strFieldName = "ReportName";
				break;
			case 6:
				strFieldName = "RelationShip";
				break;
			case 7:
				strFieldName = "ReportPhone";
				break;
			case 8:
				strFieldName = "ReportAddr";
				break;
			case 9:
				strFieldName = "Hospital";
				break;
			case 10:
				strFieldName = "AdmissionDiagnosis";
				break;
			case 11:
				strFieldName = "AccidentDate";
				break;
			case 12:
				strFieldName = "AccidentDes";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			case 17:
				strFieldName = "CaseNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ConsultNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LogNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicySequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimFormerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppointName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReportName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationShip") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReportPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReportAddr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Hospital") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AdmissionDiagnosis") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccidentDes") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
