/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAAuthorizeDB;

/*
 * <p>ClassName: LAAuthorizeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-12
 */
public class LAAuthorizeSchema implements Schema
{
	// @Field
	/** 险种编码 */
	private String RiskCode;
	/** 被授权对象 */
	private String AuthorObj;
	/** 授权粒度 */
	private String AuthorType;
	/** 授权日期 */
	private Date AuthorStartDate;
	/** 授权解除日期 */
	private Date AuthorEndDate;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 展业类型 */
	private String BranchType;
	/** 险类编码 */
	private String RiskType;
	/** 险类名称 */
	private String RiskTypeName;
	/** 展业机构外部编码 */
	private String BranchAttr;
	/** 代理人编码 */
	private String AgentCode;
	/** 渠道 */
	private String BranchType2;
        /** 最高保额 */
        private double MaxAmnt;
        /** 最低保额 */
        private double MinAmnt;
	/** 业务员授权范围 */
	private String AuthorizeArea;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAAuthorizeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "RiskCode";
		pk[1] = "AuthorObj";
		pk[2] = "AuthorType";

		PK = pk;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRiskCode()
	{
		if (SysConst.CHANGECHARSET && RiskCode != null && !RiskCode.equals(""))
		{
			RiskCode = StrTool.unicodeToGBK(RiskCode);
		}
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getAuthorObj()
	{
		if (SysConst.CHANGECHARSET && AuthorObj != null && !AuthorObj.equals(""))
		{
			AuthorObj = StrTool.unicodeToGBK(AuthorObj);
		}
		return AuthorObj;
	}
	public void setAuthorObj(String aAuthorObj)
	{
		AuthorObj = aAuthorObj;
	}
	public String getAuthorType()
	{
		if (SysConst.CHANGECHARSET && AuthorType != null && !AuthorType.equals(""))
		{
			AuthorType = StrTool.unicodeToGBK(AuthorType);
		}
		return AuthorType;
	}
	public void setAuthorType(String aAuthorType)
	{
		AuthorType = aAuthorType;
	}
	public String getAuthorStartDate()
	{
		if( AuthorStartDate != null )
			return fDate.getString(AuthorStartDate);
		else
			return null;
	}
	public void setAuthorStartDate(Date aAuthorStartDate)
	{
		AuthorStartDate = aAuthorStartDate;
	}
	public void setAuthorStartDate(String aAuthorStartDate)
	{
		if (aAuthorStartDate != null && !aAuthorStartDate.equals("") )
		{
			AuthorStartDate = fDate.getDate( aAuthorStartDate );
		}
		else
			AuthorStartDate = null;
	}

	public String getAuthorEndDate()
	{
		if( AuthorEndDate != null )
			return fDate.getString(AuthorEndDate);
		else
			return null;
	}
	public void setAuthorEndDate(Date aAuthorEndDate)
	{
		AuthorEndDate = aAuthorEndDate;
	}
	public void setAuthorEndDate(String aAuthorEndDate)
	{
		if (aAuthorEndDate != null && !aAuthorEndDate.equals("") )
		{
			AuthorEndDate = fDate.getDate( aAuthorEndDate );
		}
		else
			AuthorEndDate = null;
	}

	public String getOperator()
	{
		if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
		{
			Operator = StrTool.unicodeToGBK(Operator);
		}
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
		{
			MakeTime = StrTool.unicodeToGBK(MakeTime);
		}
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		if (SysConst.CHANGECHARSET && ModifyTime != null && !ModifyTime.equals(""))
		{
			ModifyTime = StrTool.unicodeToGBK(ModifyTime);
		}
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBranchType()
	{
		if (SysConst.CHANGECHARSET && BranchType != null && !BranchType.equals(""))
		{
			BranchType = StrTool.unicodeToGBK(BranchType);
		}
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getRiskType()
	{
		if (SysConst.CHANGECHARSET && RiskType != null && !RiskType.equals(""))
		{
			RiskType = StrTool.unicodeToGBK(RiskType);
		}
		return RiskType;
	}
	public void setRiskType(String aRiskType)
	{
		RiskType = aRiskType;
	}
	public String getRiskTypeName()
	{
		if (SysConst.CHANGECHARSET && RiskTypeName != null && !RiskTypeName.equals(""))
		{
			RiskTypeName = StrTool.unicodeToGBK(RiskTypeName);
		}
		return RiskTypeName;
	}
	public void setRiskTypeName(String aRiskTypeName)
	{
		RiskTypeName = aRiskTypeName;
	}
	public String getBranchAttr()
	{
		if (SysConst.CHANGECHARSET && BranchAttr != null && !BranchAttr.equals(""))
		{
			BranchAttr = StrTool.unicodeToGBK(BranchAttr);
		}
		return BranchAttr;
	}
	public void setBranchAttr(String aBranchAttr)
	{
		BranchAttr = aBranchAttr;
	}
	public String getAgentCode()
	{
		if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
		{
			AgentCode = StrTool.unicodeToGBK(AgentCode);
		}
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getBranchType2()
	{
		if (SysConst.CHANGECHARSET && BranchType2 != null && !BranchType2.equals(""))
		{
			BranchType2 = StrTool.unicodeToGBK(BranchType2);
		}
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
        public double getMaxAmnt()
        {
                return MaxAmnt;
        }
        public void setMaxAmnt(double aMaxAmnt)
        {
                MaxAmnt = aMaxAmnt;
        }
        public void setMaxAmnt(String aMaxAmnt)
        {
                if (aMaxAmnt != null && !aMaxAmnt.equals(""))
                {
                        Double tDouble = new Double(aMaxAmnt);
                        double d = tDouble.doubleValue();
                        MaxAmnt = d;
                }
        }
        public double getMinAmnt()
        {
                return MinAmnt;
        }
        public void setMinAmnt(double aMinAmnt)
        {
                MinAmnt = aMinAmnt;
        }
        public void setMinAmnt(String aMinAmnt)
        {
                if (aMinAmnt != null && !aMinAmnt.equals(""))
                {
                        Double tDouble = new Double(aMinAmnt);
                        double d = tDouble.doubleValue();
                        MinAmnt = d;
                }
        }


	public String getAuthorizeArea()
	{
		if (SysConst.CHANGECHARSET && AuthorizeArea != null && !AuthorizeArea.equals(""))
		{
			AuthorizeArea = StrTool.unicodeToGBK(AuthorizeArea);
		}
		return AuthorizeArea;
	}
	public void setAuthorizeArea(String aAuthorizeArea)
	{
		AuthorizeArea = aAuthorizeArea;
	}

	/**
	* 使用另外一个 LAAuthorizeSchema 对象给 Schema 赋值
	* @param: aLAAuthorizeSchema LAAuthorizeSchema
	**/
	public void setSchema(LAAuthorizeSchema aLAAuthorizeSchema)
	{
		this.RiskCode = aLAAuthorizeSchema.getRiskCode();
		this.AuthorObj = aLAAuthorizeSchema.getAuthorObj();
		this.AuthorType = aLAAuthorizeSchema.getAuthorType();
		this.AuthorStartDate = fDate.getDate( aLAAuthorizeSchema.getAuthorStartDate());
		this.AuthorEndDate = fDate.getDate( aLAAuthorizeSchema.getAuthorEndDate());
		this.Operator = aLAAuthorizeSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAAuthorizeSchema.getMakeDate());
		this.MakeTime = aLAAuthorizeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAAuthorizeSchema.getModifyDate());
		this.ModifyTime = aLAAuthorizeSchema.getModifyTime();
		this.BranchType = aLAAuthorizeSchema.getBranchType();
		this.RiskType = aLAAuthorizeSchema.getRiskType();
		this.RiskTypeName = aLAAuthorizeSchema.getRiskTypeName();
		this.BranchAttr = aLAAuthorizeSchema.getBranchAttr();
		this.AgentCode = aLAAuthorizeSchema.getAgentCode();
		this.BranchType2 = aLAAuthorizeSchema.getBranchType2();
                this.MaxAmnt = aLAAuthorizeSchema.getMaxAmnt() ;
                this.MinAmnt = aLAAuthorizeSchema.getMinAmnt() ;
		this.AuthorizeArea = aLAAuthorizeSchema.getAuthorizeArea();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("AuthorObj") == null )
				this.AuthorObj = null;
			else
				this.AuthorObj = rs.getString("AuthorObj").trim();

			if( rs.getString("AuthorType") == null )
				this.AuthorType = null;
			else
				this.AuthorType = rs.getString("AuthorType").trim();

			this.AuthorStartDate = rs.getDate("AuthorStartDate");
			this.AuthorEndDate = rs.getDate("AuthorEndDate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("RiskType") == null )
				this.RiskType = null;
			else
				this.RiskType = rs.getString("RiskType").trim();

			if( rs.getString("RiskTypeName") == null )
				this.RiskTypeName = null;
			else
				this.RiskTypeName = rs.getString("RiskTypeName").trim();

			if( rs.getString("BranchAttr") == null )
				this.BranchAttr = null;
			else
				this.BranchAttr = rs.getString("BranchAttr").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();
                        this.MaxAmnt = rs.getDouble("MaxAmnt");
                        this.MinAmnt = rs.getDouble("MinAmnt");
			if( rs.getString("AuthorizeArea") == null )
				this.AuthorizeArea = null;
			else
				this.AuthorizeArea = rs.getString("AuthorizeArea").trim();

		}
		catch(SQLException sqle)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAuthorizeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	public LAAuthorizeSchema getSchema()
	{
		LAAuthorizeSchema aLAAuthorizeSchema = new LAAuthorizeSchema();
		aLAAuthorizeSchema.setSchema(this);
		return aLAAuthorizeSchema;
	}

	public LAAuthorizeDB getDB()
	{
		LAAuthorizeDB aDBOper = new LAAuthorizeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAuthorize描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(RiskCode))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(AuthorObj))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(AuthorType))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( AuthorStartDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( AuthorEndDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(Operator))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( MakeDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(MakeTime))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( ModifyDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(ModifyTime))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BranchType))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(RiskType))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(RiskTypeName))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BranchAttr))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(AgentCode))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BranchType2))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MaxAmnt));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MinAmnt));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(AuthorizeArea)));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAuthorize>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AuthorObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AuthorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AuthorStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			AuthorEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			RiskTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
                        MaxAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
                        MinAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			AuthorizeArea = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAuthorizeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("AuthorObj"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorObj));
		}
		if (FCode.equals("AuthorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorType));
		}
		if (FCode.equals("AuthorStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAuthorStartDate()));
		}
		if (FCode.equals("AuthorEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAuthorEndDate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("RiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
		}
		if (FCode.equals("RiskTypeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskTypeName));
		}
		if (FCode.equals("BranchAttr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
                if (FCode.equals("MaxAmnt"))
                {
                        strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAmnt));
                }
                if (FCode.equals("MinAmnt"))
                {
                        strReturn = StrTool.GBKToUnicode(String.valueOf(MinAmnt));
                }
		if (FCode.equals("AuthorizeArea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorizeArea));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AuthorObj);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AuthorType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAuthorStartDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAuthorEndDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RiskType);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(RiskTypeName);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BranchAttr);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
                        case 16:
                                strFieldValue = String.valueOf(MaxAmnt);;
                                break;
                        case 17:
                                strFieldValue = String.valueOf(MinAmnt);;
                                break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(AuthorizeArea);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equals("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equals("AuthorObj"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AuthorObj = FValue.trim();
			}
			else
				AuthorObj = null;
		}
		if (FCode.equals("AuthorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AuthorType = FValue.trim();
			}
			else
				AuthorType = null;
		}
		if (FCode.equals("AuthorStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AuthorStartDate = fDate.getDate( FValue );
			}
			else
				AuthorStartDate = null;
		}
		if (FCode.equals("AuthorEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AuthorEndDate = fDate.getDate( FValue );
			}
			else
				AuthorEndDate = null;
		}
		if (FCode.equals("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equals("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equals("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equals("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equals("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equals("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equals("RiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType = FValue.trim();
			}
			else
				RiskType = null;
		}
		if (FCode.equals("RiskTypeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskTypeName = FValue.trim();
			}
			else
				RiskTypeName = null;
		}
		if (FCode.equals("BranchAttr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchAttr = FValue.trim();
			}
			else
				BranchAttr = null;
		}
		if (FCode.equals("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equals("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
                if (FCode.equals("MaxAmnt"))
                {
                        if( FValue != null && !FValue.equals(""))
                        {
                                Double tDouble = new Double( FValue );
                                double d = tDouble.doubleValue();
                                MaxAmnt = d;
                        }
                }
                if (FCode.equals("MinAmnt"))
                {
                        if( FValue != null && !FValue.equals(""))
                        {
                                Double tDouble = new Double( FValue );
                                double d = tDouble.doubleValue();
                                MinAmnt = d;
                        }
                }
		if (FCode.equals("AuthorizeArea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AuthorizeArea = FValue.trim();
			}
			else
				AuthorizeArea = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAAuthorizeSchema other = (LAAuthorizeSchema)otherObject;
		return
			RiskCode.equals(other.getRiskCode())
			&& AuthorObj.equals(other.getAuthorObj())
			&& AuthorType.equals(other.getAuthorType())
			&& fDate.getString(AuthorStartDate).equals(other.getAuthorStartDate())
			&& fDate.getString(AuthorEndDate).equals(other.getAuthorEndDate())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& BranchType.equals(other.getBranchType())
			&& RiskType.equals(other.getRiskType())
			&& RiskTypeName.equals(other.getRiskTypeName())
			&& BranchAttr.equals(other.getBranchAttr())
			&& AgentCode.equals(other.getAgentCode())
			&& BranchType2.equals(other.getBranchType2())
                        && MaxAmnt == other.getMaxAmnt()
                        && MinAmnt == other.getMinAmnt()
			&& AuthorizeArea.equals(other.getAuthorizeArea());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return 0;
		}
		if( strFieldName.equals("AuthorObj") ) {
			return 1;
		}
		if( strFieldName.equals("AuthorType") ) {
			return 2;
		}
		if( strFieldName.equals("AuthorStartDate") ) {
			return 3;
		}
		if( strFieldName.equals("AuthorEndDate") ) {
			return 4;
		}
		if( strFieldName.equals("Operator") ) {
			return 5;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 6;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 7;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 8;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 9;
		}
		if( strFieldName.equals("BranchType") ) {
			return 10;
		}
		if( strFieldName.equals("RiskType") ) {
			return 11;
		}
		if( strFieldName.equals("RiskTypeName") ) {
			return 12;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return 13;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 14;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 15;
		}
                if( strFieldName.equals("MaxAmnt") ) {
                        return 16;
                }
                if( strFieldName.equals("MinAmnt") ) {
                        return 17;
                }
		if( strFieldName.equals("AuthorizeArea") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RiskCode";
				break;
			case 1:
				strFieldName = "AuthorObj";
				break;
			case 2:
				strFieldName = "AuthorType";
				break;
			case 3:
				strFieldName = "AuthorStartDate";
				break;
			case 4:
				strFieldName = "AuthorEndDate";
				break;
			case 5:
				strFieldName = "Operator";
				break;
			case 6:
				strFieldName = "MakeDate";
				break;
			case 7:
				strFieldName = "MakeTime";
				break;
			case 8:
				strFieldName = "ModifyDate";
				break;
			case 9:
				strFieldName = "ModifyTime";
				break;
			case 10:
				strFieldName = "BranchType";
				break;
			case 11:
				strFieldName = "RiskType";
				break;
			case 12:
				strFieldName = "RiskTypeName";
				break;
			case 13:
				strFieldName = "BranchAttr";
				break;
			case 14:
				strFieldName = "AgentCode";
				break;
			case 15:
				strFieldName = "BranchType2";
				break;
                        case 16:
                                strFieldName = "MaxAmnt";
                                break;
                        case 17:
                                strFieldName = "MinAmnt";
                                break;
			case 18:
				strFieldName = "AuthorizeArea";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AuthorObj") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AuthorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AuthorStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AuthorEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskTypeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
                if( strFieldName.equals("MaxAmnt") ) {
                        return Schema.TYPE_DOUBLE;
                }
                if( strFieldName.equals("MinAmnt") ) {
                        return Schema.TYPE_DOUBLE;
                }
		if( strFieldName.equals("AuthorizeArea") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
                        case 16:
                                nFieldType = Schema.TYPE_DOUBLE;
                                break;
                        case 17:
                                nFieldType = Schema.TYPE_DOUBLE;
                                break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
