/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHServItemDB;

/*
 * <p>ClassName: LHServItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构修改提交单-20060704
 * @CreateDate：2006-07-04
 */
public class LHServItemSchema implements Schema, Cloneable {
    // @Field
    /** 团体服务计划号码 */
    private String GrpServPlanNo;
    /** 个人服务计划号码 */
    private String ServPlanNo;
    /** 团体服务项目号码 */
    private String GrpServItemNo;
    /** 个人服务项目号码 */
    private String ServItemNo;
    /** 标准服务项目代码 */
    private String ServItemCode;
    /** 个人客户号 */
    private String CustomerNo;
    /** 客户姓名 */
    private String Name;
    /** 个人保单号 */
    private String ContNo;
    /** 机构属性标识 */
    private String ComID;
    /** 服务项目属性 */
    private String ServItemType;
    /** 定价方式代码 */
    private String ServPriceCode;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 服务事件类型 */
    private String ServCaseType;

    public static final int FIELDNUM = 18; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHServItemSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ServItemNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHServItemSchema cloned = (LHServItemSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpServPlanNo() {
        return GrpServPlanNo;
    }

    public void setGrpServPlanNo(String aGrpServPlanNo) {
        GrpServPlanNo = aGrpServPlanNo;
    }

    public String getServPlanNo() {
        return ServPlanNo;
    }

    public void setServPlanNo(String aServPlanNo) {
        ServPlanNo = aServPlanNo;
    }

    public String getGrpServItemNo() {
        return GrpServItemNo;
    }

    public void setGrpServItemNo(String aGrpServItemNo) {
        GrpServItemNo = aGrpServItemNo;
    }

    public String getServItemNo() {
        return ServItemNo;
    }

    public void setServItemNo(String aServItemNo) {
        ServItemNo = aServItemNo;
    }

    public String getServItemCode() {
        return ServItemCode;
    }

    public void setServItemCode(String aServItemCode) {
        ServItemCode = aServItemCode;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String aName) {
        Name = aName;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getComID() {
        return ComID;
    }

    public void setComID(String aComID) {
        ComID = aComID;
    }

    public String getServItemType() {
        return ServItemType;
    }

    public void setServItemType(String aServItemType) {
        ServItemType = aServItemType;
    }

    public String getServPriceCode() {
        return ServPriceCode;
    }

    public void setServPriceCode(String aServPriceCode) {
        ServPriceCode = aServPriceCode;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getServCaseType() {
        return ServCaseType;
    }

    public void setServCaseType(String aServCaseType) {
        ServCaseType = aServCaseType;
    }

    /**
     * 使用另外一个 LHServItemSchema 对象给 Schema 赋值
     * @param: aLHServItemSchema LHServItemSchema
     **/
    public void setSchema(LHServItemSchema aLHServItemSchema) {
        this.GrpServPlanNo = aLHServItemSchema.getGrpServPlanNo();
        this.ServPlanNo = aLHServItemSchema.getServPlanNo();
        this.GrpServItemNo = aLHServItemSchema.getGrpServItemNo();
        this.ServItemNo = aLHServItemSchema.getServItemNo();
        this.ServItemCode = aLHServItemSchema.getServItemCode();
        this.CustomerNo = aLHServItemSchema.getCustomerNo();
        this.Name = aLHServItemSchema.getName();
        this.ContNo = aLHServItemSchema.getContNo();
        this.ComID = aLHServItemSchema.getComID();
        this.ServItemType = aLHServItemSchema.getServItemType();
        this.ServPriceCode = aLHServItemSchema.getServPriceCode();
        this.ManageCom = aLHServItemSchema.getManageCom();
        this.Operator = aLHServItemSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHServItemSchema.getMakeDate());
        this.MakeTime = aLHServItemSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHServItemSchema.getModifyDate());
        this.ModifyTime = aLHServItemSchema.getModifyTime();
        this.ServCaseType = aLHServItemSchema.getServCaseType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpServPlanNo") == null) {
                this.GrpServPlanNo = null;
            } else {
                this.GrpServPlanNo = rs.getString("GrpServPlanNo").trim();
            }

            if (rs.getString("ServPlanNo") == null) {
                this.ServPlanNo = null;
            } else {
                this.ServPlanNo = rs.getString("ServPlanNo").trim();
            }

            if (rs.getString("GrpServItemNo") == null) {
                this.GrpServItemNo = null;
            } else {
                this.GrpServItemNo = rs.getString("GrpServItemNo").trim();
            }

            if (rs.getString("ServItemNo") == null) {
                this.ServItemNo = null;
            } else {
                this.ServItemNo = rs.getString("ServItemNo").trim();
            }

            if (rs.getString("ServItemCode") == null) {
                this.ServItemCode = null;
            } else {
                this.ServItemCode = rs.getString("ServItemCode").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("Name") == null) {
                this.Name = null;
            } else {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ComID") == null) {
                this.ComID = null;
            } else {
                this.ComID = rs.getString("ComID").trim();
            }

            if (rs.getString("ServItemType") == null) {
                this.ServItemType = null;
            } else {
                this.ServItemType = rs.getString("ServItemType").trim();
            }

            if (rs.getString("ServPriceCode") == null) {
                this.ServPriceCode = null;
            } else {
                this.ServPriceCode = rs.getString("ServPriceCode").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ServCaseType") == null) {
                this.ServCaseType = null;
            } else {
                this.ServCaseType = rs.getString("ServCaseType").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHServItem表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServItemSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHServItemSchema getSchema() {
        LHServItemSchema aLHServItemSchema = new LHServItemSchema();
        aLHServItemSchema.setSchema(this);
        return aLHServItemSchema;
    }

    public LHServItemDB getDB() {
        LHServItemDB aDBOper = new LHServItemDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServItem描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpServPlanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPlanNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpServItemNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServPriceCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseType));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServItem>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            GrpServPlanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            ServPlanNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            GrpServItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            ServItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            ServItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            ComID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            ServItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            ServPriceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                           SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            ServCaseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                          SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServItemSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("GrpServPlanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpServPlanNo));
        }
        if (FCode.equals("ServPlanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPlanNo));
        }
        if (FCode.equals("GrpServItemNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpServItemNo));
        }
        if (FCode.equals("ServItemNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemNo));
        }
        if (FCode.equals("ServItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemCode));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("ComID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComID));
        }
        if (FCode.equals("ServItemType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemType));
        }
        if (FCode.equals("ServPriceCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServPriceCode));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ServCaseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(GrpServPlanNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServPlanNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(GrpServItemNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServItemNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServItemCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Name);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ComID);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ServItemType);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ServPriceCode);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(ServCaseType);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("GrpServPlanNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpServPlanNo = FValue.trim();
            } else {
                GrpServPlanNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPlanNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServPlanNo = FValue.trim();
            } else {
                ServPlanNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpServItemNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpServItemNo = FValue.trim();
            } else {
                GrpServItemNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemNo = FValue.trim();
            } else {
                ServItemNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemCode = FValue.trim();
            } else {
                ServItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if (FValue != null && !FValue.equals("")) {
                Name = FValue.trim();
            } else {
                Name = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComID")) {
            if (FValue != null && !FValue.equals("")) {
                ComID = FValue.trim();
            } else {
                ComID = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemType")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemType = FValue.trim();
            } else {
                ServItemType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServPriceCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServPriceCode = FValue.trim();
            } else {
                ServPriceCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseType")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseType = FValue.trim();
            } else {
                ServCaseType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHServItemSchema other = (LHServItemSchema) otherObject;
        return
                GrpServPlanNo.equals(other.getGrpServPlanNo())
                && ServPlanNo.equals(other.getServPlanNo())
                && GrpServItemNo.equals(other.getGrpServItemNo())
                && ServItemNo.equals(other.getServItemNo())
                && ServItemCode.equals(other.getServItemCode())
                && CustomerNo.equals(other.getCustomerNo())
                && Name.equals(other.getName())
                && ContNo.equals(other.getContNo())
                && ComID.equals(other.getComID())
                && ServItemType.equals(other.getServItemType())
                && ServPriceCode.equals(other.getServPriceCode())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ServCaseType.equals(other.getServCaseType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("GrpServPlanNo")) {
            return 0;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return 1;
        }
        if (strFieldName.equals("GrpServItemNo")) {
            return 2;
        }
        if (strFieldName.equals("ServItemNo")) {
            return 3;
        }
        if (strFieldName.equals("ServItemCode")) {
            return 4;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 5;
        }
        if (strFieldName.equals("Name")) {
            return 6;
        }
        if (strFieldName.equals("ContNo")) {
            return 7;
        }
        if (strFieldName.equals("ComID")) {
            return 8;
        }
        if (strFieldName.equals("ServItemType")) {
            return 9;
        }
        if (strFieldName.equals("ServPriceCode")) {
            return 10;
        }
        if (strFieldName.equals("ManageCom")) {
            return 11;
        }
        if (strFieldName.equals("Operator")) {
            return 12;
        }
        if (strFieldName.equals("MakeDate")) {
            return 13;
        }
        if (strFieldName.equals("MakeTime")) {
            return 14;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 15;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 16;
        }
        if (strFieldName.equals("ServCaseType")) {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "GrpServPlanNo";
            break;
        case 1:
            strFieldName = "ServPlanNo";
            break;
        case 2:
            strFieldName = "GrpServItemNo";
            break;
        case 3:
            strFieldName = "ServItemNo";
            break;
        case 4:
            strFieldName = "ServItemCode";
            break;
        case 5:
            strFieldName = "CustomerNo";
            break;
        case 6:
            strFieldName = "Name";
            break;
        case 7:
            strFieldName = "ContNo";
            break;
        case 8:
            strFieldName = "ComID";
            break;
        case 9:
            strFieldName = "ServItemType";
            break;
        case 10:
            strFieldName = "ServPriceCode";
            break;
        case 11:
            strFieldName = "ManageCom";
            break;
        case 12:
            strFieldName = "Operator";
            break;
        case 13:
            strFieldName = "MakeDate";
            break;
        case 14:
            strFieldName = "MakeTime";
            break;
        case 15:
            strFieldName = "ModifyDate";
            break;
        case 16:
            strFieldName = "ModifyTime";
            break;
        case 17:
            strFieldName = "ServCaseType";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("GrpServPlanNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPlanNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpServItemNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServPriceCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseType")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
