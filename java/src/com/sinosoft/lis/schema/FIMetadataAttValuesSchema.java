/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIMetadataAttValuesDB;

/*
 * <p>ClassName: FIMetadataAttValuesSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIMetadataAttValuesSchema implements Schema, Cloneable
{
	// @Field
	/** 元数据编号 */
	private String MetadataNo;
	/** 流水号 */
	private String SerialNo;
	/** 属性编码 */
	private String AttNo;
	/** 值域类型 */
	private String ValueType;
	/** 类型名称 */
	private String ValueTypeName;
	/** 匹配符 */
	private String PatternType;
	/** 主值域 */
	private String MainValues;
	/** 次值域 */
	private String SecondValues;
	/** 值域含义 */
	private String ValueMeaning;
	/** 说明 */
	private String Remark;

	public static final int FIELDNUM = 10;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIMetadataAttValuesSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "MetadataNo";
		pk[1] = "SerialNo";
		pk[2] = "AttNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIMetadataAttValuesSchema cloned = (FIMetadataAttValuesSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMetadataNo()
	{
		return MetadataNo;
	}
	public void setMetadataNo(String aMetadataNo)
	{
		MetadataNo = aMetadataNo;
	}
	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getAttNo()
	{
		return AttNo;
	}
	public void setAttNo(String aAttNo)
	{
		AttNo = aAttNo;
	}
	public String getValueType()
	{
		return ValueType;
	}
	public void setValueType(String aValueType)
	{
		ValueType = aValueType;
	}
	public String getValueTypeName()
	{
		return ValueTypeName;
	}
	public void setValueTypeName(String aValueTypeName)
	{
		ValueTypeName = aValueTypeName;
	}
	public String getPatternType()
	{
		return PatternType;
	}
	public void setPatternType(String aPatternType)
	{
		PatternType = aPatternType;
	}
	public String getMainValues()
	{
		return MainValues;
	}
	public void setMainValues(String aMainValues)
	{
		MainValues = aMainValues;
	}
	public String getSecondValues()
	{
		return SecondValues;
	}
	public void setSecondValues(String aSecondValues)
	{
		SecondValues = aSecondValues;
	}
	public String getValueMeaning()
	{
		return ValueMeaning;
	}
	public void setValueMeaning(String aValueMeaning)
	{
		ValueMeaning = aValueMeaning;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}

	/**
	* 使用另外一个 FIMetadataAttValuesSchema 对象给 Schema 赋值
	* @param: aFIMetadataAttValuesSchema FIMetadataAttValuesSchema
	**/
	public void setSchema(FIMetadataAttValuesSchema aFIMetadataAttValuesSchema)
	{
		this.MetadataNo = aFIMetadataAttValuesSchema.getMetadataNo();
		this.SerialNo = aFIMetadataAttValuesSchema.getSerialNo();
		this.AttNo = aFIMetadataAttValuesSchema.getAttNo();
		this.ValueType = aFIMetadataAttValuesSchema.getValueType();
		this.ValueTypeName = aFIMetadataAttValuesSchema.getValueTypeName();
		this.PatternType = aFIMetadataAttValuesSchema.getPatternType();
		this.MainValues = aFIMetadataAttValuesSchema.getMainValues();
		this.SecondValues = aFIMetadataAttValuesSchema.getSecondValues();
		this.ValueMeaning = aFIMetadataAttValuesSchema.getValueMeaning();
		this.Remark = aFIMetadataAttValuesSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MetadataNo") == null )
				this.MetadataNo = null;
			else
				this.MetadataNo = rs.getString("MetadataNo").trim();

			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("AttNo") == null )
				this.AttNo = null;
			else
				this.AttNo = rs.getString("AttNo").trim();

			if( rs.getString("ValueType") == null )
				this.ValueType = null;
			else
				this.ValueType = rs.getString("ValueType").trim();

			if( rs.getString("ValueTypeName") == null )
				this.ValueTypeName = null;
			else
				this.ValueTypeName = rs.getString("ValueTypeName").trim();

			if( rs.getString("PatternType") == null )
				this.PatternType = null;
			else
				this.PatternType = rs.getString("PatternType").trim();

			if( rs.getString("MainValues") == null )
				this.MainValues = null;
			else
				this.MainValues = rs.getString("MainValues").trim();

			if( rs.getString("SecondValues") == null )
				this.SecondValues = null;
			else
				this.SecondValues = rs.getString("SecondValues").trim();

			if( rs.getString("ValueMeaning") == null )
				this.ValueMeaning = null;
			else
				this.ValueMeaning = rs.getString("ValueMeaning").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIMetadataAttValues表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIMetadataAttValuesSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIMetadataAttValuesSchema getSchema()
	{
		FIMetadataAttValuesSchema aFIMetadataAttValuesSchema = new FIMetadataAttValuesSchema();
		aFIMetadataAttValuesSchema.setSchema(this);
		return aFIMetadataAttValuesSchema;
	}

	public FIMetadataAttValuesDB getDB()
	{
		FIMetadataAttValuesDB aDBOper = new FIMetadataAttValuesDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIMetadataAttValues描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MetadataNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AttNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ValueType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ValueTypeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PatternType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainValues)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecondValues)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ValueMeaning)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIMetadataAttValues>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MetadataNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AttNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ValueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ValueTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			PatternType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MainValues = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			SecondValues = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ValueMeaning = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIMetadataAttValuesSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MetadataNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MetadataNo));
		}
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("AttNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AttNo));
		}
		if (FCode.equals("ValueType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ValueType));
		}
		if (FCode.equals("ValueTypeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ValueTypeName));
		}
		if (FCode.equals("PatternType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PatternType));
		}
		if (FCode.equals("MainValues"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainValues));
		}
		if (FCode.equals("SecondValues"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecondValues));
		}
		if (FCode.equals("ValueMeaning"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ValueMeaning));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MetadataNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AttNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ValueType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ValueTypeName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(PatternType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(MainValues);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(SecondValues);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ValueMeaning);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MetadataNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MetadataNo = FValue.trim();
			}
			else
				MetadataNo = null;
		}
		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("AttNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AttNo = FValue.trim();
			}
			else
				AttNo = null;
		}
		if (FCode.equalsIgnoreCase("ValueType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ValueType = FValue.trim();
			}
			else
				ValueType = null;
		}
		if (FCode.equalsIgnoreCase("ValueTypeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ValueTypeName = FValue.trim();
			}
			else
				ValueTypeName = null;
		}
		if (FCode.equalsIgnoreCase("PatternType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PatternType = FValue.trim();
			}
			else
				PatternType = null;
		}
		if (FCode.equalsIgnoreCase("MainValues"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainValues = FValue.trim();
			}
			else
				MainValues = null;
		}
		if (FCode.equalsIgnoreCase("SecondValues"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecondValues = FValue.trim();
			}
			else
				SecondValues = null;
		}
		if (FCode.equalsIgnoreCase("ValueMeaning"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ValueMeaning = FValue.trim();
			}
			else
				ValueMeaning = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIMetadataAttValuesSchema other = (FIMetadataAttValuesSchema)otherObject;
		return
			(MetadataNo == null ? other.getMetadataNo() == null : MetadataNo.equals(other.getMetadataNo()))
			&& (SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (AttNo == null ? other.getAttNo() == null : AttNo.equals(other.getAttNo()))
			&& (ValueType == null ? other.getValueType() == null : ValueType.equals(other.getValueType()))
			&& (ValueTypeName == null ? other.getValueTypeName() == null : ValueTypeName.equals(other.getValueTypeName()))
			&& (PatternType == null ? other.getPatternType() == null : PatternType.equals(other.getPatternType()))
			&& (MainValues == null ? other.getMainValues() == null : MainValues.equals(other.getMainValues()))
			&& (SecondValues == null ? other.getSecondValues() == null : SecondValues.equals(other.getSecondValues()))
			&& (ValueMeaning == null ? other.getValueMeaning() == null : ValueMeaning.equals(other.getValueMeaning()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MetadataNo") ) {
			return 0;
		}
		if( strFieldName.equals("SerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("AttNo") ) {
			return 2;
		}
		if( strFieldName.equals("ValueType") ) {
			return 3;
		}
		if( strFieldName.equals("ValueTypeName") ) {
			return 4;
		}
		if( strFieldName.equals("PatternType") ) {
			return 5;
		}
		if( strFieldName.equals("MainValues") ) {
			return 6;
		}
		if( strFieldName.equals("SecondValues") ) {
			return 7;
		}
		if( strFieldName.equals("ValueMeaning") ) {
			return 8;
		}
		if( strFieldName.equals("Remark") ) {
			return 9;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MetadataNo";
				break;
			case 1:
				strFieldName = "SerialNo";
				break;
			case 2:
				strFieldName = "AttNo";
				break;
			case 3:
				strFieldName = "ValueType";
				break;
			case 4:
				strFieldName = "ValueTypeName";
				break;
			case 5:
				strFieldName = "PatternType";
				break;
			case 6:
				strFieldName = "MainValues";
				break;
			case 7:
				strFieldName = "SecondValues";
				break;
			case 8:
				strFieldName = "ValueMeaning";
				break;
			case 9:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MetadataNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AttNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValueType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValueTypeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PatternType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainValues") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecondValues") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValueMeaning") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
