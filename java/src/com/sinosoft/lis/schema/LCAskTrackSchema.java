/*
 * <p>ClassName: LCAskTrackSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 业务跟踪表
 * @CreateDate：2005-01-24
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCAskTrackDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCAskTrackSchema implements Schema
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 打印流水号 */
    private String PrtSeq;
    /** 投保人编码 */
    private String AppntNo;
    /** 投保人 */
    private String AppntName;
    /** 跟踪结果 */
    private String TrackResult;
    /** 跟踪结果说明 */
    private String TrackResultInfo;
    /** 流失原因 */
    private String LoseReason;
    /** 流失原因说明 */
    private String LoseReasonInfo;
    /** 备注 */
    private String Remark;
    /** 回复标记 */
    private String ReplyFlag;
    /** 回复人 */
    private String ReplyOperator;
    /** 回复日期 */
    private Date ReplyDate;
    /** 回复时间 */
    private String ReplyTime;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String ManageCom;
    /** 录入日期 */
    private Date MakeDate;
    /** 录入时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCAskTrackSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "GrpContNo";
        pk[1] = "PrtSeq";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getProposalContNo()
    {
        if (ProposalContNo != null && !ProposalContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProposalContNo = StrTool.unicodeToGBK(ProposalContNo);
        }
        return ProposalContNo;
    }

    public void setProposalContNo(String aProposalContNo)
    {
        ProposalContNo = aProposalContNo;
    }

    public String getPrtSeq()
    {
        if (PrtSeq != null && !PrtSeq.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtSeq = StrTool.unicodeToGBK(PrtSeq);
        }
        return PrtSeq;
    }

    public void setPrtSeq(String aPrtSeq)
    {
        PrtSeq = aPrtSeq;
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getAppntName()
    {
        if (AppntName != null && !AppntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public String getTrackResult()
    {
        if (TrackResult != null && !TrackResult.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TrackResult = StrTool.unicodeToGBK(TrackResult);
        }
        return TrackResult;
    }

    public void setTrackResult(String aTrackResult)
    {
        TrackResult = aTrackResult;
    }

    public String getTrackResultInfo()
    {
        if (TrackResultInfo != null && !TrackResultInfo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TrackResultInfo = StrTool.unicodeToGBK(TrackResultInfo);
        }
        return TrackResultInfo;
    }

    public void setTrackResultInfo(String aTrackResultInfo)
    {
        TrackResultInfo = aTrackResultInfo;
    }

    public String getLoseReason()
    {
        if (LoseReason != null && !LoseReason.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            LoseReason = StrTool.unicodeToGBK(LoseReason);
        }
        return LoseReason;
    }

    public void setLoseReason(String aLoseReason)
    {
        LoseReason = aLoseReason;
    }

    public String getLoseReasonInfo()
    {
        if (LoseReasonInfo != null && !LoseReasonInfo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            LoseReasonInfo = StrTool.unicodeToGBK(LoseReasonInfo);
        }
        return LoseReasonInfo;
    }

    public void setLoseReasonInfo(String aLoseReasonInfo)
    {
        LoseReasonInfo = aLoseReasonInfo;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getReplyFlag()
    {
        if (ReplyFlag != null && !ReplyFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReplyFlag = StrTool.unicodeToGBK(ReplyFlag);
        }
        return ReplyFlag;
    }

    public void setReplyFlag(String aReplyFlag)
    {
        ReplyFlag = aReplyFlag;
    }

    public String getReplyOperator()
    {
        if (ReplyOperator != null && !ReplyOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReplyOperator = StrTool.unicodeToGBK(ReplyOperator);
        }
        return ReplyOperator;
    }

    public void setReplyOperator(String aReplyOperator)
    {
        ReplyOperator = aReplyOperator;
    }

    public String getReplyDate()
    {
        if (ReplyDate != null)
        {
            return fDate.getString(ReplyDate);
        }
        else
        {
            return null;
        }
    }

    public void setReplyDate(Date aReplyDate)
    {
        ReplyDate = aReplyDate;
    }

    public void setReplyDate(String aReplyDate)
    {
        if (aReplyDate != null && !aReplyDate.equals(""))
        {
            ReplyDate = fDate.getDate(aReplyDate);
        }
        else
        {
            ReplyDate = null;
        }
    }

    public String getReplyTime()
    {
        if (ReplyTime != null && !ReplyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReplyTime = StrTool.unicodeToGBK(ReplyTime);
        }
        return ReplyTime;
    }

    public void setReplyTime(String aReplyTime)
    {
        ReplyTime = aReplyTime;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCAskTrackSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCAskTrackSchema aLCAskTrackSchema)
    {
        this.GrpContNo = aLCAskTrackSchema.getGrpContNo();
        this.ProposalContNo = aLCAskTrackSchema.getProposalContNo();
        this.PrtSeq = aLCAskTrackSchema.getPrtSeq();
        this.AppntNo = aLCAskTrackSchema.getAppntNo();
        this.AppntName = aLCAskTrackSchema.getAppntName();
        this.TrackResult = aLCAskTrackSchema.getTrackResult();
        this.TrackResultInfo = aLCAskTrackSchema.getTrackResultInfo();
        this.LoseReason = aLCAskTrackSchema.getLoseReason();
        this.LoseReasonInfo = aLCAskTrackSchema.getLoseReasonInfo();
        this.Remark = aLCAskTrackSchema.getRemark();
        this.ReplyFlag = aLCAskTrackSchema.getReplyFlag();
        this.ReplyOperator = aLCAskTrackSchema.getReplyOperator();
        this.ReplyDate = fDate.getDate(aLCAskTrackSchema.getReplyDate());
        this.ReplyTime = aLCAskTrackSchema.getReplyTime();
        this.AgentCode = aLCAskTrackSchema.getAgentCode();
        this.AgentGroup = aLCAskTrackSchema.getAgentGroup();
        this.Operator = aLCAskTrackSchema.getOperator();
        this.ManageCom = aLCAskTrackSchema.getManageCom();
        this.MakeDate = fDate.getDate(aLCAskTrackSchema.getMakeDate());
        this.MakeTime = aLCAskTrackSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCAskTrackSchema.getModifyDate());
        this.ModifyTime = aLCAskTrackSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ProposalContNo") == null)
            {
                this.ProposalContNo = null;
            }
            else
            {
                this.ProposalContNo = rs.getString("ProposalContNo").trim();
            }

            if (rs.getString("PrtSeq") == null)
            {
                this.PrtSeq = null;
            }
            else
            {
                this.PrtSeq = rs.getString("PrtSeq").trim();
            }

            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AppntName") == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString("AppntName").trim();
            }

            if (rs.getString("TrackResult") == null)
            {
                this.TrackResult = null;
            }
            else
            {
                this.TrackResult = rs.getString("TrackResult").trim();
            }

            if (rs.getString("TrackResultInfo") == null)
            {
                this.TrackResultInfo = null;
            }
            else
            {
                this.TrackResultInfo = rs.getString("TrackResultInfo").trim();
            }

            if (rs.getString("LoseReason") == null)
            {
                this.LoseReason = null;
            }
            else
            {
                this.LoseReason = rs.getString("LoseReason").trim();
            }

            if (rs.getString("LoseReasonInfo") == null)
            {
                this.LoseReasonInfo = null;
            }
            else
            {
                this.LoseReasonInfo = rs.getString("LoseReasonInfo").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("ReplyFlag") == null)
            {
                this.ReplyFlag = null;
            }
            else
            {
                this.ReplyFlag = rs.getString("ReplyFlag").trim();
            }

            if (rs.getString("ReplyOperator") == null)
            {
                this.ReplyOperator = null;
            }
            else
            {
                this.ReplyOperator = rs.getString("ReplyOperator").trim();
            }

            this.ReplyDate = rs.getDate("ReplyDate");
            if (rs.getString("ReplyTime") == null)
            {
                this.ReplyTime = null;
            }
            else
            {
                this.ReplyTime = rs.getString("ReplyTime").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskTrackSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCAskTrackSchema getSchema()
    {
        LCAskTrackSchema aLCAskTrackSchema = new LCAskTrackSchema();
        aLCAskTrackSchema.setSchema(this);
        return aLCAskTrackSchema;
    }

    public LCAskTrackDB getDB()
    {
        LCAskTrackDB aDBOper = new LCAskTrackDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAskTrack描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProposalContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtSeq)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TrackResult)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TrackResultInfo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(LoseReason)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(LoseReasonInfo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReplyFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReplyOperator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ReplyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReplyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAskTrack>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                            SysConst.PACKAGESPILTER);
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            TrackResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            TrackResultInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             7, SysConst.PACKAGESPILTER);
            LoseReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            LoseReasonInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                            SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            ReplyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            ReplyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                           SysConst.PACKAGESPILTER);
            ReplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ReplyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAskTrackSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("ProposalContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProposalContNo));
        }
        if (FCode.equals("PrtSeq"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtSeq));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntName));
        }
        if (FCode.equals("TrackResult"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TrackResult));
        }
        if (FCode.equals("TrackResultInfo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TrackResultInfo));
        }
        if (FCode.equals("LoseReason"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LoseReason));
        }
        if (FCode.equals("LoseReasonInfo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LoseReasonInfo));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("ReplyFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReplyFlag));
        }
        if (FCode.equals("ReplyOperator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReplyOperator));
        }
        if (FCode.equals("ReplyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getReplyDate()));
        }
        if (FCode.equals("ReplyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReplyTime));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PrtSeq);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(TrackResult);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(TrackResultInfo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(LoseReason);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(LoseReasonInfo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ReplyFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ReplyOperator);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReplyDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ReplyTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ProposalContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
            {
                ProposalContNo = null;
            }
        }
        if (FCode.equals("PrtSeq"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
            {
                PrtSeq = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("TrackResult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TrackResult = FValue.trim();
            }
            else
            {
                TrackResult = null;
            }
        }
        if (FCode.equals("TrackResultInfo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TrackResultInfo = FValue.trim();
            }
            else
            {
                TrackResultInfo = null;
            }
        }
        if (FCode.equals("LoseReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LoseReason = FValue.trim();
            }
            else
            {
                LoseReason = null;
            }
        }
        if (FCode.equals("LoseReasonInfo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LoseReasonInfo = FValue.trim();
            }
            else
            {
                LoseReasonInfo = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("ReplyFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyFlag = FValue.trim();
            }
            else
            {
                ReplyFlag = null;
            }
        }
        if (FCode.equals("ReplyOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyOperator = FValue.trim();
            }
            else
            {
                ReplyOperator = null;
            }
        }
        if (FCode.equals("ReplyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyDate = fDate.getDate(FValue);
            }
            else
            {
                ReplyDate = null;
            }
        }
        if (FCode.equals("ReplyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyTime = FValue.trim();
            }
            else
            {
                ReplyTime = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCAskTrackSchema other = (LCAskTrackSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && ProposalContNo.equals(other.getProposalContNo())
                && PrtSeq.equals(other.getPrtSeq())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && TrackResult.equals(other.getTrackResult())
                && TrackResultInfo.equals(other.getTrackResultInfo())
                && LoseReason.equals(other.getLoseReason())
                && LoseReasonInfo.equals(other.getLoseReasonInfo())
                && Remark.equals(other.getRemark())
                && ReplyFlag.equals(other.getReplyFlag())
                && ReplyOperator.equals(other.getReplyOperator())
                && fDate.getString(ReplyDate).equals(other.getReplyDate())
                && ReplyTime.equals(other.getReplyTime())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && Operator.equals(other.getOperator())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return 2;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 3;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 4;
        }
        if (strFieldName.equals("TrackResult"))
        {
            return 5;
        }
        if (strFieldName.equals("TrackResultInfo"))
        {
            return 6;
        }
        if (strFieldName.equals("LoseReason"))
        {
            return 7;
        }
        if (strFieldName.equals("LoseReasonInfo"))
        {
            return 8;
        }
        if (strFieldName.equals("Remark"))
        {
            return 9;
        }
        if (strFieldName.equals("ReplyFlag"))
        {
            return 10;
        }
        if (strFieldName.equals("ReplyOperator"))
        {
            return 11;
        }
        if (strFieldName.equals("ReplyDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ReplyTime"))
        {
            return 13;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 14;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 15;
        }
        if (strFieldName.equals("Operator"))
        {
            return 16;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalContNo";
                break;
            case 2:
                strFieldName = "PrtSeq";
                break;
            case 3:
                strFieldName = "AppntNo";
                break;
            case 4:
                strFieldName = "AppntName";
                break;
            case 5:
                strFieldName = "TrackResult";
                break;
            case 6:
                strFieldName = "TrackResultInfo";
                break;
            case 7:
                strFieldName = "LoseReason";
                break;
            case 8:
                strFieldName = "LoseReasonInfo";
                break;
            case 9:
                strFieldName = "Remark";
                break;
            case 10:
                strFieldName = "ReplyFlag";
                break;
            case 11:
                strFieldName = "ReplyOperator";
                break;
            case 12:
                strFieldName = "ReplyDate";
                break;
            case 13:
                strFieldName = "ReplyTime";
                break;
            case 14:
                strFieldName = "AgentCode";
                break;
            case 15:
                strFieldName = "AgentGroup";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "ManageCom";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TrackResult"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TrackResultInfo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LoseReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LoseReasonInfo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ReplyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
