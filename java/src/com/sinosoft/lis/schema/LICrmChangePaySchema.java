/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LICrmChangePayDB;

/*
 * <p>ClassName: LICrmChangePaySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-11-09
 */
public class LICrmChangePaySchema implements Schema, Cloneable {
    // @Field
    /** 批次号 */
    private String BatchNo;
    /** 客户号 */
    private String Appntno;
    /** 保全受理号 */
    private String Endorseno;
    /** 保单号码 */
    private String Contno;
    /** 回访结果 */
    private String Result;
    /** 应缴日期 */
    private Date Paydate;
    /** 费用 */
    private double Paymoney;
    /** 约定下次缴费日期 */
    private Date NextPaydate;
    /** 录入状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LICrmChangePaySchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "BatchNo";
        pk[1] = "Endorseno";
        pk[2] = "Contno";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LICrmChangePaySchema cloned = (LICrmChangePaySchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo) {
        BatchNo = aBatchNo;
    }

    public String getAppntno() {
        return Appntno;
    }

    public void setAppntno(String aAppntno) {
        Appntno = aAppntno;
    }

    public String getEndorseno() {
        return Endorseno;
    }

    public void setEndorseno(String aEndorseno) {
        Endorseno = aEndorseno;
    }

    public String getContno() {
        return Contno;
    }

    public void setContno(String aContno) {
        Contno = aContno;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String aResult) {
        Result = aResult;
    }

    public String getPaydate() {
        if (Paydate != null) {
            return fDate.getString(Paydate);
        } else {
            return null;
        }
    }

    public void setPaydate(Date aPaydate) {
        Paydate = aPaydate;
    }

    public void setPaydate(String aPaydate) {
        if (aPaydate != null && !aPaydate.equals("")) {
            Paydate = fDate.getDate(aPaydate);
        } else {
            Paydate = null;
        }
    }

    public double getPaymoney() {
        return Paymoney;
    }

    public void setPaymoney(double aPaymoney) {
        Paymoney = Arith.round(aPaymoney, 2);
    }

    public void setPaymoney(String aPaymoney) {
        if (aPaymoney != null && !aPaymoney.equals("")) {
            Double tDouble = new Double(aPaymoney);
            double d = tDouble.doubleValue();
            Paymoney = Arith.round(d, 2);
        }
    }

    public String getNextPaydate() {
        if (NextPaydate != null) {
            return fDate.getString(NextPaydate);
        } else {
            return null;
        }
    }

    public void setNextPaydate(Date aNextPaydate) {
        NextPaydate = aNextPaydate;
    }

    public void setNextPaydate(String aNextPaydate) {
        if (aNextPaydate != null && !aNextPaydate.equals("")) {
            NextPaydate = fDate.getDate(aNextPaydate);
        } else {
            NextPaydate = null;
        }
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LICrmChangePaySchema 对象给 Schema 赋值
     * @param: aLICrmChangePaySchema LICrmChangePaySchema
     **/
    public void setSchema(LICrmChangePaySchema aLICrmChangePaySchema) {
        this.BatchNo = aLICrmChangePaySchema.getBatchNo();
        this.Appntno = aLICrmChangePaySchema.getAppntno();
        this.Endorseno = aLICrmChangePaySchema.getEndorseno();
        this.Contno = aLICrmChangePaySchema.getContno();
        this.Result = aLICrmChangePaySchema.getResult();
        this.Paydate = fDate.getDate(aLICrmChangePaySchema.getPaydate());
        this.Paymoney = aLICrmChangePaySchema.getPaymoney();
        this.NextPaydate = fDate.getDate(aLICrmChangePaySchema.getNextPaydate());
        this.State = aLICrmChangePaySchema.getState();
        this.Operator = aLICrmChangePaySchema.getOperator();
        this.MakeDate = fDate.getDate(aLICrmChangePaySchema.getMakeDate());
        this.MakeTime = aLICrmChangePaySchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("BatchNo") == null) {
                this.BatchNo = null;
            } else {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            if (rs.getString("Appntno") == null) {
                this.Appntno = null;
            } else {
                this.Appntno = rs.getString("Appntno").trim();
            }

            if (rs.getString("Endorseno") == null) {
                this.Endorseno = null;
            } else {
                this.Endorseno = rs.getString("Endorseno").trim();
            }

            if (rs.getString("Contno") == null) {
                this.Contno = null;
            } else {
                this.Contno = rs.getString("Contno").trim();
            }

            if (rs.getString("Result") == null) {
                this.Result = null;
            } else {
                this.Result = rs.getString("Result").trim();
            }

            this.Paydate = rs.getDate("Paydate");
            this.Paymoney = rs.getDouble("Paymoney");
            this.NextPaydate = rs.getDate("NextPaydate");
            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LICrmChangePay表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LICrmChangePaySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LICrmChangePaySchema getSchema() {
        LICrmChangePaySchema aLICrmChangePaySchema = new LICrmChangePaySchema();
        aLICrmChangePaySchema.setSchema(this);
        return aLICrmChangePaySchema;
    }

    public LICrmChangePayDB getDB() {
        LICrmChangePayDB aDBOper = new LICrmChangePayDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICrmChangePay描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(BatchNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Appntno));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Endorseno));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Contno));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Result));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(Paydate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Paymoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(NextPaydate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICrmChangePay>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            Appntno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            Endorseno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            Contno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            Result = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            Paydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            Paymoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            NextPaydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LICrmChangePaySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
        }
        if (FCode.equals("Appntno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Appntno));
        }
        if (FCode.equals("Endorseno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Endorseno));
        }
        if (FCode.equals("Contno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Contno));
        }
        if (FCode.equals("Result")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Result));
        }
        if (FCode.equals("Paydate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPaydate()));
        }
        if (FCode.equals("Paymoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Paymoney));
        }
        if (FCode.equals("NextPaydate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getNextPaydate()));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(BatchNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(Appntno);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(Endorseno);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Contno);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Result);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getPaydate()));
            break;
        case 6:
            strFieldValue = String.valueOf(Paymoney);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getNextPaydate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("BatchNo")) {
            if (FValue != null && !FValue.equals("")) {
                BatchNo = FValue.trim();
            } else {
                BatchNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Appntno")) {
            if (FValue != null && !FValue.equals("")) {
                Appntno = FValue.trim();
            } else {
                Appntno = null;
            }
        }
        if (FCode.equalsIgnoreCase("Endorseno")) {
            if (FValue != null && !FValue.equals("")) {
                Endorseno = FValue.trim();
            } else {
                Endorseno = null;
            }
        }
        if (FCode.equalsIgnoreCase("Contno")) {
            if (FValue != null && !FValue.equals("")) {
                Contno = FValue.trim();
            } else {
                Contno = null;
            }
        }
        if (FCode.equalsIgnoreCase("Result")) {
            if (FValue != null && !FValue.equals("")) {
                Result = FValue.trim();
            } else {
                Result = null;
            }
        }
        if (FCode.equalsIgnoreCase("Paydate")) {
            if (FValue != null && !FValue.equals("")) {
                Paydate = fDate.getDate(FValue);
            } else {
                Paydate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Paymoney")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Paymoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("NextPaydate")) {
            if (FValue != null && !FValue.equals("")) {
                NextPaydate = fDate.getDate(FValue);
            } else {
                NextPaydate = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LICrmChangePaySchema other = (LICrmChangePaySchema) otherObject;
        return
                BatchNo.equals(other.getBatchNo())
                && Appntno.equals(other.getAppntno())
                && Endorseno.equals(other.getEndorseno())
                && Contno.equals(other.getContno())
                && Result.equals(other.getResult())
                && fDate.getString(Paydate).equals(other.getPaydate())
                && Paymoney == other.getPaymoney()
                && fDate.getString(NextPaydate).equals(other.getNextPaydate())
                && State.equals(other.getState())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("BatchNo")) {
            return 0;
        }
        if (strFieldName.equals("Appntno")) {
            return 1;
        }
        if (strFieldName.equals("Endorseno")) {
            return 2;
        }
        if (strFieldName.equals("Contno")) {
            return 3;
        }
        if (strFieldName.equals("Result")) {
            return 4;
        }
        if (strFieldName.equals("Paydate")) {
            return 5;
        }
        if (strFieldName.equals("Paymoney")) {
            return 6;
        }
        if (strFieldName.equals("NextPaydate")) {
            return 7;
        }
        if (strFieldName.equals("State")) {
            return 8;
        }
        if (strFieldName.equals("Operator")) {
            return 9;
        }
        if (strFieldName.equals("MakeDate")) {
            return 10;
        }
        if (strFieldName.equals("MakeTime")) {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "BatchNo";
            break;
        case 1:
            strFieldName = "Appntno";
            break;
        case 2:
            strFieldName = "Endorseno";
            break;
        case 3:
            strFieldName = "Contno";
            break;
        case 4:
            strFieldName = "Result";
            break;
        case 5:
            strFieldName = "Paydate";
            break;
        case 6:
            strFieldName = "Paymoney";
            break;
        case 7:
            strFieldName = "NextPaydate";
            break;
        case 8:
            strFieldName = "State";
            break;
        case 9:
            strFieldName = "Operator";
            break;
        case 10:
            strFieldName = "MakeDate";
            break;
        case 11:
            strFieldName = "MakeTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("BatchNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Appntno")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Endorseno")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Contno")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Result")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Paydate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Paymoney")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("NextPaydate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
