/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLAppClaimMicroDB;

/*
 * <p>ClassName: LLAppClaimMicroSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-06-26
 */
public class LLAppClaimMicroSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SERIALNO;
	/** 团体保单号 */
	private String GRPCONTNO;
	/** 机构代码 */
	private String MNGCOM;
	/** 机构名称 */
	private String MNGNAME;
	/** 分单免原件限额 */
	private double CASEAMNT;
	/** 分单收取原件限额 */
	private double COLLECTCASEAMNT;
	/** 分单申请次数限额 */
	private String APPCOUNT;
	/** 是否快赔业务 */
	private String BUSINESS;
	/** 操作员 */
	private String OPERATOR;
	/** 入机日期 */
	private Date MAKEDATE;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 13;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLAppClaimMicroSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "SERIALNO";
		pk[1] = "GRPCONTNO";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLAppClaimMicroSchema cloned = (LLAppClaimMicroSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSERIALNO()
	{
		return SERIALNO;
	}
	public void setSERIALNO(String aSERIALNO)
	{
		SERIALNO = aSERIALNO;
	}
	public String getGRPCONTNO()
	{
		return GRPCONTNO;
	}
	public void setGRPCONTNO(String aGRPCONTNO)
	{
		GRPCONTNO = aGRPCONTNO;
	}
	public String getMNGCOM()
	{
		return MNGCOM;
	}
	public void setMNGCOM(String aMNGCOM)
	{
		MNGCOM = aMNGCOM;
	}
	public String getMNGNAME()
	{
		return MNGNAME;
	}
	public void setMNGNAME(String aMNGNAME)
	{
		MNGNAME = aMNGNAME;
	}
	public double getCASEAMNT()
	{
		return CASEAMNT;
	}
	public void setCASEAMNT(double aCASEAMNT)
	{
		CASEAMNT = Arith.round(aCASEAMNT, 2);
	}
	public void setCASEAMNT(String aCASEAMNT)
	{
		if (aCASEAMNT != null && !aCASEAMNT.equals(""))
		{
			Double tDouble = new Double(aCASEAMNT);
			double d = tDouble.doubleValue();
                CASEAMNT = Arith.round(d, 2);
		}
	}

	public double getCOLLECTCASEAMNT()
	{
		return COLLECTCASEAMNT;
	}
	public void setCOLLECTCASEAMNT(double aCOLLECTCASEAMNT)
	{
		COLLECTCASEAMNT = Arith.round(aCOLLECTCASEAMNT, 2);
	}
	public void setCOLLECTCASEAMNT(String aCOLLECTCASEAMNT)
	{
		if (aCOLLECTCASEAMNT != null && !aCOLLECTCASEAMNT.equals(""))
		{
			Double tDouble = new Double(aCOLLECTCASEAMNT);
			double d = tDouble.doubleValue();
                COLLECTCASEAMNT = Arith.round(d, 2);
		}
	}

	public String getAPPCOUNT()
	{
		return APPCOUNT;
	}
	public void setAPPCOUNT(String aAPPCOUNT)
	{
		APPCOUNT = aAPPCOUNT;
	}
	public String getBUSINESS()
	{
		return BUSINESS;
	}
	public void setBUSINESS(String aBUSINESS)
	{
		BUSINESS = aBUSINESS;
	}
	public String getOPERATOR()
	{
		return OPERATOR;
	}
	public void setOPERATOR(String aOPERATOR)
	{
		OPERATOR = aOPERATOR;
	}
	public String getMAKEDATE()
	{
		if( MAKEDATE != null )
			return fDate.getString(MAKEDATE);
		else
			return null;
	}
	public void setMAKEDATE(Date aMAKEDATE)
	{
		MAKEDATE = aMAKEDATE;
	}
	public void setMAKEDATE(String aMAKEDATE)
	{
		if (aMAKEDATE != null && !aMAKEDATE.equals("") )
		{
			MAKEDATE = fDate.getDate( aMAKEDATE );
		}
		else
			MAKEDATE = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLAppClaimMicroSchema 对象给 Schema 赋值
	* @param: aLLAppClaimMicroSchema LLAppClaimMicroSchema
	**/
	public void setSchema(LLAppClaimMicroSchema aLLAppClaimMicroSchema)
	{
		this.SERIALNO = aLLAppClaimMicroSchema.getSERIALNO();
		this.GRPCONTNO = aLLAppClaimMicroSchema.getGRPCONTNO();
		this.MNGCOM = aLLAppClaimMicroSchema.getMNGCOM();
		this.MNGNAME = aLLAppClaimMicroSchema.getMNGNAME();
		this.CASEAMNT = aLLAppClaimMicroSchema.getCASEAMNT();
		this.COLLECTCASEAMNT = aLLAppClaimMicroSchema.getCOLLECTCASEAMNT();
		this.APPCOUNT = aLLAppClaimMicroSchema.getAPPCOUNT();
		this.BUSINESS = aLLAppClaimMicroSchema.getBUSINESS();
		this.OPERATOR = aLLAppClaimMicroSchema.getOPERATOR();
		this.MAKEDATE = fDate.getDate( aLLAppClaimMicroSchema.getMAKEDATE());
		this.MakeTime = aLLAppClaimMicroSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLAppClaimMicroSchema.getModifyDate());
		this.ModifyTime = aLLAppClaimMicroSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SERIALNO") == null )
				this.SERIALNO = null;
			else
				this.SERIALNO = rs.getString("SERIALNO").trim();

			if( rs.getString("GRPCONTNO") == null )
				this.GRPCONTNO = null;
			else
				this.GRPCONTNO = rs.getString("GRPCONTNO").trim();

			if( rs.getString("MNGCOM") == null )
				this.MNGCOM = null;
			else
				this.MNGCOM = rs.getString("MNGCOM").trim();

			if( rs.getString("MNGNAME") == null )
				this.MNGNAME = null;
			else
				this.MNGNAME = rs.getString("MNGNAME").trim();

			this.CASEAMNT = rs.getDouble("CASEAMNT");
			this.COLLECTCASEAMNT = rs.getDouble("COLLECTCASEAMNT");
			if( rs.getString("APPCOUNT") == null )
				this.APPCOUNT = null;
			else
				this.APPCOUNT = rs.getString("APPCOUNT").trim();

			if( rs.getString("BUSINESS") == null )
				this.BUSINESS = null;
			else
				this.BUSINESS = rs.getString("BUSINESS").trim();

			if( rs.getString("OPERATOR") == null )
				this.OPERATOR = null;
			else
				this.OPERATOR = rs.getString("OPERATOR").trim();

			this.MAKEDATE = rs.getDate("MAKEDATE");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLAppClaimMicro表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLAppClaimMicroSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLAppClaimMicroSchema getSchema()
	{
		LLAppClaimMicroSchema aLLAppClaimMicroSchema = new LLAppClaimMicroSchema();
		aLLAppClaimMicroSchema.setSchema(this);
		return aLLAppClaimMicroSchema;
	}

	public LLAppClaimMicroDB getDB()
	{
		LLAppClaimMicroDB aDBOper = new LLAppClaimMicroDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLAppClaimMicro描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SERIALNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GRPCONTNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MNGCOM)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MNGNAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CASEAMNT));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(COLLECTCASEAMNT));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(APPCOUNT)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BUSINESS)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OPERATOR)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MAKEDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLAppClaimMicro>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SERIALNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			GRPCONTNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			MNGCOM = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			MNGNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CASEAMNT = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			COLLECTCASEAMNT = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			APPCOUNT = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BUSINESS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			OPERATOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MAKEDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLAppClaimMicroSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SERIALNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SERIALNO));
		}
		if (FCode.equals("GRPCONTNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GRPCONTNO));
		}
		if (FCode.equals("MNGCOM"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MNGCOM));
		}
		if (FCode.equals("MNGNAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MNGNAME));
		}
		if (FCode.equals("CASEAMNT"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CASEAMNT));
		}
		if (FCode.equals("COLLECTCASEAMNT"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(COLLECTCASEAMNT));
		}
		if (FCode.equals("APPCOUNT"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(APPCOUNT));
		}
		if (FCode.equals("BUSINESS"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BUSINESS));
		}
		if (FCode.equals("OPERATOR"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OPERATOR));
		}
		if (FCode.equals("MAKEDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SERIALNO);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(GRPCONTNO);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(MNGCOM);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(MNGNAME);
				break;
			case 4:
				strFieldValue = String.valueOf(CASEAMNT);
				break;
			case 5:
				strFieldValue = String.valueOf(COLLECTCASEAMNT);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(APPCOUNT);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BUSINESS);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(OPERATOR);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SERIALNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SERIALNO = FValue.trim();
			}
			else
				SERIALNO = null;
		}
		if (FCode.equalsIgnoreCase("GRPCONTNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GRPCONTNO = FValue.trim();
			}
			else
				GRPCONTNO = null;
		}
		if (FCode.equalsIgnoreCase("MNGCOM"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MNGCOM = FValue.trim();
			}
			else
				MNGCOM = null;
		}
		if (FCode.equalsIgnoreCase("MNGNAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MNGNAME = FValue.trim();
			}
			else
				MNGNAME = null;
		}
		if (FCode.equalsIgnoreCase("CASEAMNT"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CASEAMNT = d;
			}
		}
		if (FCode.equalsIgnoreCase("COLLECTCASEAMNT"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				COLLECTCASEAMNT = d;
			}
		}
		if (FCode.equalsIgnoreCase("APPCOUNT"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				APPCOUNT = FValue.trim();
			}
			else
				APPCOUNT = null;
		}
		if (FCode.equalsIgnoreCase("BUSINESS"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BUSINESS = FValue.trim();
			}
			else
				BUSINESS = null;
		}
		if (FCode.equalsIgnoreCase("OPERATOR"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OPERATOR = FValue.trim();
			}
			else
				OPERATOR = null;
		}
		if (FCode.equalsIgnoreCase("MAKEDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MAKEDATE = fDate.getDate( FValue );
			}
			else
				MAKEDATE = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLAppClaimMicroSchema other = (LLAppClaimMicroSchema)otherObject;
		return
			(SERIALNO == null ? other.getSERIALNO() == null : SERIALNO.equals(other.getSERIALNO()))
			&& (GRPCONTNO == null ? other.getGRPCONTNO() == null : GRPCONTNO.equals(other.getGRPCONTNO()))
			&& (MNGCOM == null ? other.getMNGCOM() == null : MNGCOM.equals(other.getMNGCOM()))
			&& (MNGNAME == null ? other.getMNGNAME() == null : MNGNAME.equals(other.getMNGNAME()))
			&& CASEAMNT == other.getCASEAMNT()
			&& COLLECTCASEAMNT == other.getCOLLECTCASEAMNT()
			&& (APPCOUNT == null ? other.getAPPCOUNT() == null : APPCOUNT.equals(other.getAPPCOUNT()))
			&& (BUSINESS == null ? other.getBUSINESS() == null : BUSINESS.equals(other.getBUSINESS()))
			&& (OPERATOR == null ? other.getOPERATOR() == null : OPERATOR.equals(other.getOPERATOR()))
			&& (MAKEDATE == null ? other.getMAKEDATE() == null : fDate.getString(MAKEDATE).equals(other.getMAKEDATE()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SERIALNO") ) {
			return 0;
		}
		if( strFieldName.equals("GRPCONTNO") ) {
			return 1;
		}
		if( strFieldName.equals("MNGCOM") ) {
			return 2;
		}
		if( strFieldName.equals("MNGNAME") ) {
			return 3;
		}
		if( strFieldName.equals("CASEAMNT") ) {
			return 4;
		}
		if( strFieldName.equals("COLLECTCASEAMNT") ) {
			return 5;
		}
		if( strFieldName.equals("APPCOUNT") ) {
			return 6;
		}
		if( strFieldName.equals("BUSINESS") ) {
			return 7;
		}
		if( strFieldName.equals("OPERATOR") ) {
			return 8;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SERIALNO";
				break;
			case 1:
				strFieldName = "GRPCONTNO";
				break;
			case 2:
				strFieldName = "MNGCOM";
				break;
			case 3:
				strFieldName = "MNGNAME";
				break;
			case 4:
				strFieldName = "CASEAMNT";
				break;
			case 5:
				strFieldName = "COLLECTCASEAMNT";
				break;
			case 6:
				strFieldName = "APPCOUNT";
				break;
			case 7:
				strFieldName = "BUSINESS";
				break;
			case 8:
				strFieldName = "OPERATOR";
				break;
			case 9:
				strFieldName = "MAKEDATE";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SERIALNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GRPCONTNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MNGCOM") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MNGNAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CASEAMNT") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("COLLECTCASEAMNT") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("APPCOUNT") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BUSINESS") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OPERATOR") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
