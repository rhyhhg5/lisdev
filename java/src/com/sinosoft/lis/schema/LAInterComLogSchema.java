/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAInterComLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAInterComLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LAInterComLogSchema implements Schema
{
    // @Field
    /** 计算年月 */
    private String WageNo;
    /** 管理机构 */
    private String MgCom;
    /** 展业类型 */
    private String BrchType;
    /** 计算起期 */
    private int StartDate;
    /** 计算止期 */
    private int EndDate;
    /** 复核人员 */
    private String Confirmer;
    /** 提取状态 */
    private String State;
    /** 操作人员 */
    private String Operator;
    /** 入机日期 */
    private int MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 上一次修改日期 */
    private int ModifyDate;
    /** 上一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAInterComLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "WageNo";
        pk[1] = "MgCom";
        pk[2] = "BrchType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getWageNo()
    {
        if (WageNo != null && !WageNo.equals("") && SysConst.CHANGECHARSET)
        {
            WageNo = StrTool.unicodeToGBK(WageNo);
        }
        return WageNo;
    }

    public void setWageNo(String aWageNo)
    {
        WageNo = aWageNo;
    }

    public String getMgCom()
    {
        if (MgCom != null && !MgCom.equals("") && SysConst.CHANGECHARSET)
        {
            MgCom = StrTool.unicodeToGBK(MgCom);
        }
        return MgCom;
    }

    public void setMgCom(String aMgCom)
    {
        MgCom = aMgCom;
    }

    public String getBrchType()
    {
        if (BrchType != null && !BrchType.equals("") && SysConst.CHANGECHARSET)
        {
            BrchType = StrTool.unicodeToGBK(BrchType);
        }
        return BrchType;
    }

    public void setBrchType(String aBrchType)
    {
        BrchType = aBrchType;
    }

    public int getStartDate()
    {
        return StartDate;
    }

    public void setStartDate(int aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            Integer tInteger = new Integer(aStartDate);
            int i = tInteger.intValue();
            StartDate = i;
        }
    }

    public int getEndDate()
    {
        return EndDate;
    }

    public void setEndDate(int aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            Integer tInteger = new Integer(aEndDate);
            int i = tInteger.intValue();
            EndDate = i;
        }
    }

    public String getConfirmer()
    {
        if (Confirmer != null && !Confirmer.equals("") &&
            SysConst.CHANGECHARSET)
        {
            Confirmer = StrTool.unicodeToGBK(Confirmer);
        }
        return Confirmer;
    }

    public void setConfirmer(String aConfirmer)
    {
        Confirmer = aConfirmer;
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public int getMakeDate()
    {
        return MakeDate;
    }

    public void setMakeDate(int aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            Integer tInteger = new Integer(aMakeDate);
            int i = tInteger.intValue();
            MakeDate = i;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public int getModifyDate()
    {
        return ModifyDate;
    }

    public void setModifyDate(int aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            Integer tInteger = new Integer(aModifyDate);
            int i = tInteger.intValue();
            ModifyDate = i;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAInterComLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LAInterComLogSchema aLAInterComLogSchema)
    {
        this.WageNo = aLAInterComLogSchema.getWageNo();
        this.MgCom = aLAInterComLogSchema.getMgCom();
        this.BrchType = aLAInterComLogSchema.getBrchType();
        this.StartDate = aLAInterComLogSchema.getStartDate();
        this.EndDate = aLAInterComLogSchema.getEndDate();
        this.Confirmer = aLAInterComLogSchema.getConfirmer();
        this.State = aLAInterComLogSchema.getState();
        this.Operator = aLAInterComLogSchema.getOperator();
        this.MakeDate = aLAInterComLogSchema.getMakeDate();
        this.MakeTime = aLAInterComLogSchema.getMakeTime();
        this.ModifyDate = aLAInterComLogSchema.getModifyDate();
        this.ModifyTime = aLAInterComLogSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("WageNo") == null)
            {
                this.WageNo = null;
            }
            else
            {
                this.WageNo = rs.getString("WageNo").trim();
            }

            if (rs.getString("MgCom") == null)
            {
                this.MgCom = null;
            }
            else
            {
                this.MgCom = rs.getString("MgCom").trim();
            }

            if (rs.getString("BrchType") == null)
            {
                this.BrchType = null;
            }
            else
            {
                this.BrchType = rs.getString("BrchType").trim();
            }

            this.StartDate = rs.getInt("StartDate");
            this.EndDate = rs.getInt("EndDate");
            if (rs.getString("Confirmer") == null)
            {
                this.Confirmer = null;
            }
            else
            {
                this.Confirmer = rs.getString("Confirmer").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getInt("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getInt("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInterComLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAInterComLogSchema getSchema()
    {
        LAInterComLogSchema aLAInterComLogSchema = new LAInterComLogSchema();
        aLAInterComLogSchema.setSchema(this);
        return aLAInterComLogSchema;
    }

    public LAInterComLogDB getDB()
    {
        LAInterComLogDB aDBOper = new LAInterComLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterComLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(WageNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MgCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BrchType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StartDate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EndDate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Confirmer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(MakeDate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ModifyDate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterComLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            MgCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            BrchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            StartDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            EndDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            Confirmer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).intValue();
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInterComLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("WageNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
        }
        if (FCode.equals("MgCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MgCom));
        }
        if (FCode.equals("BrchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BrchType));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equals("Confirmer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Confirmer));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(WageNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(MgCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BrchType);
                break;
            case 3:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 4:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Confirmer);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("WageNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WageNo = FValue.trim();
            }
            else
            {
                WageNo = null;
            }
        }
        if (FCode.equals("MgCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MgCom = FValue.trim();
            }
            else
            {
                MgCom = null;
            }
        }
        if (FCode.equals("BrchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BrchType = FValue.trim();
            }
            else
            {
                BrchType = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StartDate = i;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                EndDate = i;
            }
        }
        if (FCode.equals("Confirmer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Confirmer = FValue.trim();
            }
            else
            {
                Confirmer = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MakeDate = i;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ModifyDate = i;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAInterComLogSchema other = (LAInterComLogSchema) otherObject;
        return
                WageNo.equals(other.getWageNo())
                && MgCom.equals(other.getMgCom())
                && BrchType.equals(other.getBrchType())
                && StartDate == other.getStartDate()
                && EndDate == other.getEndDate()
                && Confirmer.equals(other.getConfirmer())
                && State.equals(other.getState())
                && Operator.equals(other.getOperator())
                && MakeDate == other.getMakeDate()
                && MakeTime.equals(other.getMakeTime())
                && ModifyDate == other.getModifyDate()
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("WageNo"))
        {
            return 0;
        }
        if (strFieldName.equals("MgCom"))
        {
            return 1;
        }
        if (strFieldName.equals("BrchType"))
        {
            return 2;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 3;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 4;
        }
        if (strFieldName.equals("Confirmer"))
        {
            return 5;
        }
        if (strFieldName.equals("State"))
        {
            return 6;
        }
        if (strFieldName.equals("Operator"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "WageNo";
                break;
            case 1:
                strFieldName = "MgCom";
                break;
            case 2:
                strFieldName = "BrchType";
                break;
            case 3:
                strFieldName = "StartDate";
                break;
            case 4:
                strFieldName = "EndDate";
                break;
            case 5:
                strFieldName = "Confirmer";
                break;
            case 6:
                strFieldName = "State";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("WageNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MgCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BrchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Confirmer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_INT;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
