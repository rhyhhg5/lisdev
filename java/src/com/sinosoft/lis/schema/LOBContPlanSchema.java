/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBContPlanDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LOBContPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-06-27
 */
public class LOBContPlanSchema implements Schema, Cloneable
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体投保单号码 */
    private String ProposalGrpContNo;
    /** 保险计划编码 */
    private String ContPlanCode;
    /** 保险计划名称 */
    private String ContPlanName;
    /** 计划类别 */
    private String PlanType;
    /** 计划规则 */
    private String PlanRule;
    /** 计划规则sql */
    private String PlanSql;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 单位可投保人数 */
    private int Peoples3;
    /** 备注2 */
    private String Remark2;
    /** 参保人数 */
    private int Peoples2;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBContPlanSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "GrpContNo";
        pk[1] = "ContPlanCode";
        pk[2] = "PlanType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LOBContPlanSchema cloned = (LOBContPlanSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getProposalGrpContNo()
    {
        return ProposalGrpContNo;
    }

    public void setProposalGrpContNo(String aProposalGrpContNo)
    {
        ProposalGrpContNo = aProposalGrpContNo;
    }

    public String getContPlanCode()
    {
        return ContPlanCode;
    }

    public void setContPlanCode(String aContPlanCode)
    {
        ContPlanCode = aContPlanCode;
    }

    public String getContPlanName()
    {
        return ContPlanName;
    }

    public void setContPlanName(String aContPlanName)
    {
        ContPlanName = aContPlanName;
    }

    public String getPlanType()
    {
        return PlanType;
    }

    public void setPlanType(String aPlanType)
    {
        PlanType = aPlanType;
    }

    public String getPlanRule()
    {
        return PlanRule;
    }

    public void setPlanRule(String aPlanRule)
    {
        PlanRule = aPlanRule;
    }

    public String getPlanSql()
    {
        return PlanSql;
    }

    public void setPlanSql(String aPlanSql)
    {
        PlanSql = aPlanSql;
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public int getPeoples3()
    {
        return Peoples3;
    }

    public void setPeoples3(int aPeoples3)
    {
        Peoples3 = aPeoples3;
    }

    public void setPeoples3(String aPeoples3)
    {
        if (aPeoples3 != null && !aPeoples3.equals(""))
        {
            Integer tInteger = new Integer(aPeoples3);
            int i = tInteger.intValue();
            Peoples3 = i;
        }
    }

    public String getRemark2()
    {
        return Remark2;
    }

    public void setRemark2(String aRemark2)
    {
        Remark2 = aRemark2;
    }

    public int getPeoples2()
    {
        return Peoples2;
    }

    public void setPeoples2(int aPeoples2)
    {
        Peoples2 = aPeoples2;
    }

    public void setPeoples2(String aPeoples2)
    {
        if (aPeoples2 != null && !aPeoples2.equals(""))
        {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }


    /**
     * 使用另外一个 LOBContPlanSchema 对象给 Schema 赋值
     * @param: aLOBContPlanSchema LOBContPlanSchema
     **/
    public void setSchema(LOBContPlanSchema aLOBContPlanSchema)
    {
        this.GrpContNo = aLOBContPlanSchema.getGrpContNo();
        this.ProposalGrpContNo = aLOBContPlanSchema.getProposalGrpContNo();
        this.ContPlanCode = aLOBContPlanSchema.getContPlanCode();
        this.ContPlanName = aLOBContPlanSchema.getContPlanName();
        this.PlanType = aLOBContPlanSchema.getPlanType();
        this.PlanRule = aLOBContPlanSchema.getPlanRule();
        this.PlanSql = aLOBContPlanSchema.getPlanSql();
        this.Remark = aLOBContPlanSchema.getRemark();
        this.Operator = aLOBContPlanSchema.getOperator();
        this.MakeDate = fDate.getDate(aLOBContPlanSchema.getMakeDate());
        this.MakeTime = aLOBContPlanSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBContPlanSchema.getModifyDate());
        this.ModifyTime = aLOBContPlanSchema.getModifyTime();
        this.Peoples3 = aLOBContPlanSchema.getPeoples3();
        this.Remark2 = aLOBContPlanSchema.getRemark2();
        this.Peoples2 = aLOBContPlanSchema.getPeoples2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ProposalGrpContNo") == null)
            {
                this.ProposalGrpContNo = null;
            }
            else
            {
                this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();
            }

            if (rs.getString("ContPlanCode") == null)
            {
                this.ContPlanCode = null;
            }
            else
            {
                this.ContPlanCode = rs.getString("ContPlanCode").trim();
            }

            if (rs.getString("ContPlanName") == null)
            {
                this.ContPlanName = null;
            }
            else
            {
                this.ContPlanName = rs.getString("ContPlanName").trim();
            }

            if (rs.getString("PlanType") == null)
            {
                this.PlanType = null;
            }
            else
            {
                this.PlanType = rs.getString("PlanType").trim();
            }

            if (rs.getString("PlanRule") == null)
            {
                this.PlanRule = null;
            }
            else
            {
                this.PlanRule = rs.getString("PlanRule").trim();
            }

            if (rs.getString("PlanSql") == null)
            {
                this.PlanSql = null;
            }
            else
            {
                this.PlanSql = rs.getString("PlanSql").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.Peoples3 = rs.getInt("Peoples3");
            if (rs.getString("Remark2") == null)
            {
                this.Remark2 = null;
            }
            else
            {
                this.Remark2 = rs.getString("Remark2").trim();
            }

            this.Peoples2 = rs.getInt("Peoples2");
        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LOBContPlan表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBContPlanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LOBContPlanSchema getSchema()
    {
        LOBContPlanSchema aLOBContPlanSchema = new LOBContPlanSchema();
        aLOBContPlanSchema.setSchema(this);
        return aLOBContPlanSchema;
    }

    public LOBContPlanDB getDB()
    {
        LOBContPlanDB aDBOper = new LOBContPlanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBContPlan描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalGrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContPlanCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContPlanName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanRule));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanSql));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples2));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBContPlan>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               2, SysConst.PACKAGESPILTER);
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ContPlanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            PlanType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            PlanRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            PlanSql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            Peoples3 = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).intValue();
            Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                     SysConst.PACKAGESPILTER);
            Peoples2 = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).intValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBContPlanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ProposalGrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
        }
        if (FCode.equals("ContPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
        }
        if (FCode.equals("ContPlanName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanName));
        }
        if (FCode.equals("PlanType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanType));
        }
        if (FCode.equals("PlanRule"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanRule));
        }
        if (FCode.equals("PlanSql"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanSql));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Peoples3"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
        }
        if (FCode.equals("Remark2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (FCode.equals("Peoples2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContPlanName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PlanType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PlanRule);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PlanSql);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 13:
                strFieldValue = String.valueOf(Peoples3);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Remark2);
                break;
            case 15:
                strFieldValue = String.valueOf(Peoples2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ProposalGrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
            {
                ProposalGrpContNo = null;
            }
        }
        if (FCode.equals("ContPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
            {
                ContPlanCode = null;
            }
        }
        if (FCode.equals("ContPlanName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanName = FValue.trim();
            }
            else
            {
                ContPlanName = null;
            }
        }
        if (FCode.equals("PlanType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanType = FValue.trim();
            }
            else
            {
                PlanType = null;
            }
        }
        if (FCode.equals("PlanRule"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanRule = FValue.trim();
            }
            else
            {
                PlanRule = null;
            }
        }
        if (FCode.equals("PlanSql"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanSql = FValue.trim();
            }
            else
            {
                PlanSql = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Peoples3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Peoples3 = i;
            }
        }
        if (FCode.equals("Remark2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
            {
                Remark2 = null;
            }
        }
        if (FCode.equals("Peoples2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBContPlanSchema other = (LOBContPlanSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && ProposalGrpContNo.equals(other.getProposalGrpContNo())
                && ContPlanCode.equals(other.getContPlanCode())
                && ContPlanName.equals(other.getContPlanName())
                && PlanType.equals(other.getPlanType())
                && PlanRule.equals(other.getPlanRule())
                && PlanSql.equals(other.getPlanSql())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Peoples3 == other.getPeoples3()
                && Remark2.equals(other.getRemark2())
                && Peoples2 == other.getPeoples2();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ProposalGrpContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return 2;
        }
        if (strFieldName.equals("ContPlanName"))
        {
            return 3;
        }
        if (strFieldName.equals("PlanType"))
        {
            return 4;
        }
        if (strFieldName.equals("PlanRule"))
        {
            return 5;
        }
        if (strFieldName.equals("PlanSql"))
        {
            return 6;
        }
        if (strFieldName.equals("Remark"))
        {
            return 7;
        }
        if (strFieldName.equals("Operator"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 11;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 12;
        }
        if (strFieldName.equals("Peoples3"))
        {
            return 13;
        }
        if (strFieldName.equals("Remark2"))
        {
            return 14;
        }
        if (strFieldName.equals("Peoples2"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "ContPlanCode";
                break;
            case 3:
                strFieldName = "ContPlanName";
                break;
            case 4:
                strFieldName = "PlanType";
                break;
            case 5:
                strFieldName = "PlanRule";
                break;
            case 6:
                strFieldName = "PlanSql";
                break;
            case 7:
                strFieldName = "Remark";
                break;
            case 8:
                strFieldName = "Operator";
                break;
            case 9:
                strFieldName = "MakeDate";
                break;
            case 10:
                strFieldName = "MakeTime";
                break;
            case 11:
                strFieldName = "ModifyDate";
                break;
            case 12:
                strFieldName = "ModifyTime";
                break;
            case 13:
                strFieldName = "Peoples3";
                break;
            case 14:
                strFieldName = "Remark2";
                break;
            case 15:
                strFieldName = "Peoples2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalGrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanRule"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanSql"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Peoples3"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Remark2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Peoples2"))
        {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_INT;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_INT;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
