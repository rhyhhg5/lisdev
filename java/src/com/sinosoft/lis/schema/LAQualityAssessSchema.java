/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAQualityAssessDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAQualityAssessSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAQualityAssessSchema implements Schema
{
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 展业类型 */
    private String BranchType;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 纪录顺序号 */
    private int Idx;
    /** 管理机构 */
    private String ManageCom;
    /** 扣分类型 */
    private String MarkType;
    /** 扣分代码 */
    private String MarkCode;
    /** 扣分内容 */
    private String MarkContent;
    /** 所扣分数 */
    private double Mark;
    /** 扣分原因 */
    private String MarkRsn;
    /** 执行日期 */
    private Date DoneDate;
    /** 批注 */
    private String Noti;
    /** 处理标志 */
    private String DoneFlag;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 展业机构外部编码 */
    private String BranchAttr;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAQualityAssessSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AgentCode";
        pk[1] = "Idx";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCode()
    {
        if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getAgentGroup()
    {
        if (SysConst.CHANGECHARSET && AgentGroup != null &&
            !AgentGroup.equals(""))
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public int getIdx()
    {
        return Idx;
    }

    public void setIdx(int aIdx)
    {
        Idx = aIdx;
    }

    public void setIdx(String aIdx)
    {
        if (aIdx != null && !aIdx.equals(""))
        {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getMarkType()
    {
        if (SysConst.CHANGECHARSET && MarkType != null && !MarkType.equals(""))
        {
            MarkType = StrTool.unicodeToGBK(MarkType);
        }
        return MarkType;
    }

    public void setMarkType(String aMarkType)
    {
        MarkType = aMarkType;
    }

    public String getMarkCode()
    {
        if (SysConst.CHANGECHARSET && MarkCode != null && !MarkCode.equals(""))
        {
            MarkCode = StrTool.unicodeToGBK(MarkCode);
        }
        return MarkCode;
    }

    public void setMarkCode(String aMarkCode)
    {
        MarkCode = aMarkCode;
    }

    public String getMarkContent()
    {
        if (SysConst.CHANGECHARSET && MarkContent != null &&
            !MarkContent.equals(""))
        {
            MarkContent = StrTool.unicodeToGBK(MarkContent);
        }
        return MarkContent;
    }

    public void setMarkContent(String aMarkContent)
    {
        MarkContent = aMarkContent;
    }

    public double getMark()
    {
        return Mark;
    }

    public void setMark(double aMark)
    {
        Mark = aMark;
    }

    public void setMark(String aMark)
    {
        if (aMark != null && !aMark.equals(""))
        {
            Double tDouble = new Double(aMark);
            double d = tDouble.doubleValue();
            Mark = d;
        }
    }

    public String getMarkRsn()
    {
        if (SysConst.CHANGECHARSET && MarkRsn != null && !MarkRsn.equals(""))
        {
            MarkRsn = StrTool.unicodeToGBK(MarkRsn);
        }
        return MarkRsn;
    }

    public void setMarkRsn(String aMarkRsn)
    {
        MarkRsn = aMarkRsn;
    }

    public String getDoneDate()
    {
        if (DoneDate != null)
        {
            return fDate.getString(DoneDate);
        }
        else
        {
            return null;
        }
    }

    public void setDoneDate(Date aDoneDate)
    {
        DoneDate = aDoneDate;
    }

    public void setDoneDate(String aDoneDate)
    {
        if (aDoneDate != null && !aDoneDate.equals(""))
        {
            DoneDate = fDate.getDate(aDoneDate);
        }
        else
        {
            DoneDate = null;
        }
    }

    public String getNoti()
    {
        if (SysConst.CHANGECHARSET && Noti != null && !Noti.equals(""))
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getDoneFlag()
    {
        if (SysConst.CHANGECHARSET && DoneFlag != null && !DoneFlag.equals(""))
        {
            DoneFlag = StrTool.unicodeToGBK(DoneFlag);
        }
        return DoneFlag;
    }

    public void setDoneFlag(String aDoneFlag)
    {
        DoneFlag = aDoneFlag;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getBranchAttr()
    {
        if (SysConst.CHANGECHARSET && BranchAttr != null &&
            !BranchAttr.equals(""))
        {
            BranchAttr = StrTool.unicodeToGBK(BranchAttr);
        }
        return BranchAttr;
    }

    public void setBranchAttr(String aBranchAttr)
    {
        BranchAttr = aBranchAttr;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAQualityAssessSchema 对象给 Schema 赋值
     * @param: aLAQualityAssessSchema LAQualityAssessSchema
     **/
    public void setSchema(LAQualityAssessSchema aLAQualityAssessSchema)
    {
        this.AgentCode = aLAQualityAssessSchema.getAgentCode();
        this.BranchType = aLAQualityAssessSchema.getBranchType();
        this.AgentGroup = aLAQualityAssessSchema.getAgentGroup();
        this.Idx = aLAQualityAssessSchema.getIdx();
        this.ManageCom = aLAQualityAssessSchema.getManageCom();
        this.MarkType = aLAQualityAssessSchema.getMarkType();
        this.MarkCode = aLAQualityAssessSchema.getMarkCode();
        this.MarkContent = aLAQualityAssessSchema.getMarkContent();
        this.Mark = aLAQualityAssessSchema.getMark();
        this.MarkRsn = aLAQualityAssessSchema.getMarkRsn();
        this.DoneDate = fDate.getDate(aLAQualityAssessSchema.getDoneDate());
        this.Noti = aLAQualityAssessSchema.getNoti();
        this.DoneFlag = aLAQualityAssessSchema.getDoneFlag();
        this.Operator = aLAQualityAssessSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAQualityAssessSchema.getMakeDate());
        this.MakeTime = aLAQualityAssessSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAQualityAssessSchema.getModifyDate());
        this.ModifyTime = aLAQualityAssessSchema.getModifyTime();
        this.BranchAttr = aLAQualityAssessSchema.getBranchAttr();
        this.BranchType2 = aLAQualityAssessSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            this.Idx = rs.getInt("Idx");
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("MarkType") == null)
            {
                this.MarkType = null;
            }
            else
            {
                this.MarkType = rs.getString("MarkType").trim();
            }

            if (rs.getString("MarkCode") == null)
            {
                this.MarkCode = null;
            }
            else
            {
                this.MarkCode = rs.getString("MarkCode").trim();
            }

            if (rs.getString("MarkContent") == null)
            {
                this.MarkContent = null;
            }
            else
            {
                this.MarkContent = rs.getString("MarkContent").trim();
            }

            this.Mark = rs.getDouble("Mark");
            if (rs.getString("MarkRsn") == null)
            {
                this.MarkRsn = null;
            }
            else
            {
                this.MarkRsn = rs.getString("MarkRsn").trim();
            }

            this.DoneDate = rs.getDate("DoneDate");
            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("DoneFlag") == null)
            {
                this.DoneFlag = null;
            }
            else
            {
                this.DoneFlag = rs.getString("DoneFlag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BranchAttr") == null)
            {
                this.BranchAttr = null;
            }
            else
            {
                this.BranchAttr = rs.getString("BranchAttr").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAQualityAssessSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAQualityAssessSchema getSchema()
    {
        LAQualityAssessSchema aLAQualityAssessSchema = new
                LAQualityAssessSchema();
        aLAQualityAssessSchema.setSchema(this);
        return aLAQualityAssessSchema;
    }

    public LAQualityAssessDB getDB()
    {
        LAQualityAssessDB aDBOper = new LAQualityAssessDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAQualityAssess描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Idx));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MarkType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MarkCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MarkContent)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Mark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MarkRsn)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                DoneDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Noti)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DoneFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchAttr)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAQualityAssess>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            Idx = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).intValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            MarkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            MarkCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MarkContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            Mark = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    9, SysConst.PACKAGESPILTER))).doubleValue();
            MarkRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            DoneDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                  SysConst.PACKAGESPILTER);
            DoneFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAQualityAssessSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("Idx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("MarkType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarkType));
        }
        if (FCode.equals("MarkCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarkCode));
        }
        if (FCode.equals("MarkContent"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarkContent));
        }
        if (FCode.equals("Mark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mark));
        }
        if (FCode.equals("MarkRsn"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarkRsn));
        }
        if (FCode.equals("DoneDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getDoneDate()));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("DoneFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoneFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("BranchAttr"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 3:
                strFieldValue = String.valueOf(Idx);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MarkType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MarkCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MarkContent);
                break;
            case 8:
                strFieldValue = String.valueOf(Mark);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MarkRsn);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getDoneDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(DoneFlag);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(BranchAttr);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("Idx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("MarkType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MarkType = FValue.trim();
            }
            else
            {
                MarkType = null;
            }
        }
        if (FCode.equals("MarkCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MarkCode = FValue.trim();
            }
            else
            {
                MarkCode = null;
            }
        }
        if (FCode.equals("MarkContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MarkContent = FValue.trim();
            }
            else
            {
                MarkContent = null;
            }
        }
        if (FCode.equals("Mark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Mark = d;
            }
        }
        if (FCode.equals("MarkRsn"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MarkRsn = FValue.trim();
            }
            else
            {
                MarkRsn = null;
            }
        }
        if (FCode.equals("DoneDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DoneDate = fDate.getDate(FValue);
            }
            else
            {
                DoneDate = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("DoneFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DoneFlag = FValue.trim();
            }
            else
            {
                DoneFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("BranchAttr"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
            {
                BranchAttr = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAQualityAssessSchema other = (LAQualityAssessSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && BranchType.equals(other.getBranchType())
                && AgentGroup.equals(other.getAgentGroup())
                && Idx == other.getIdx()
                && ManageCom.equals(other.getManageCom())
                && MarkType.equals(other.getMarkType())
                && MarkCode.equals(other.getMarkCode())
                && MarkContent.equals(other.getMarkContent())
                && Mark == other.getMark()
                && MarkRsn.equals(other.getMarkRsn())
                && fDate.getString(DoneDate).equals(other.getDoneDate())
                && Noti.equals(other.getNoti())
                && DoneFlag.equals(other.getDoneFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BranchAttr.equals(other.getBranchAttr())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 2;
        }
        if (strFieldName.equals("Idx"))
        {
            return 3;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 4;
        }
        if (strFieldName.equals("MarkType"))
        {
            return 5;
        }
        if (strFieldName.equals("MarkCode"))
        {
            return 6;
        }
        if (strFieldName.equals("MarkContent"))
        {
            return 7;
        }
        if (strFieldName.equals("Mark"))
        {
            return 8;
        }
        if (strFieldName.equals("MarkRsn"))
        {
            return 9;
        }
        if (strFieldName.equals("DoneDate"))
        {
            return 10;
        }
        if (strFieldName.equals("Noti"))
        {
            return 11;
        }
        if (strFieldName.equals("DoneFlag"))
        {
            return 12;
        }
        if (strFieldName.equals("Operator"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 14;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 16;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 17;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return 18;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "AgentGroup";
                break;
            case 3:
                strFieldName = "Idx";
                break;
            case 4:
                strFieldName = "ManageCom";
                break;
            case 5:
                strFieldName = "MarkType";
                break;
            case 6:
                strFieldName = "MarkCode";
                break;
            case 7:
                strFieldName = "MarkContent";
                break;
            case 8:
                strFieldName = "Mark";
                break;
            case 9:
                strFieldName = "MarkRsn";
                break;
            case 10:
                strFieldName = "DoneDate";
                break;
            case 11:
                strFieldName = "Noti";
                break;
            case 12:
                strFieldName = "DoneFlag";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            case 18:
                strFieldName = "BranchAttr";
                break;
            case 19:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Idx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MarkType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MarkCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MarkContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Mark"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MarkRsn"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DoneDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DoneFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
