/*
 * <p>ClassName: LJAGetDrawSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 更新表
 * @CreateDate：2004-12-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LJAGetDrawDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LJAGetDrawSchema implements Schema
{
    // @Field
    /** 实付号码 */
    private String ActuGetNo;
    /** 保单号码 */
    private String PolNo;
    /** 责任编码 */
    private String DutyCode;
    /** 给付责任类型 */
    private String GetDutyKind;
    /** 给付责任编码 */
    private String GetDutyCode;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 合同号码 */
    private String ContNo;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 给付日期 */
    private Date GetDate;
    /** 给付金额 */
    private double GetMoney;
    /** 补/退费到帐日期 */
    private Date EnterAccDate;
    /** 补/退费财务确认日期 */
    private Date ConfDate;
    /** 补/退费业务类型 */
    private String FeeOperationType;
    /** 补/退费财务类型 */
    private String FeeFinaType;
    /** 险类编码 */
    private String KindCode;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 单位名称 */
    private String GrpName;
    /** 经办人 */
    private String Handler;
    /** 上次领至日期 */
    private Date LastGettoDate;
    /** 本次领至日期 */
    private Date CurGetToDate;
    /** 给付首期标志 */
    private String GetmFirstFlag;
    /** 给付渠道标志 */
    private String GetChannelFlag;
    /** 消户标记 */
    private String DestrayFlag;
    /** 保单类型 */
    private String PolType;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 给付通知书号码 */
    private String GetNoticeNo;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 42; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LJAGetDrawSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "ActuGetNo";
        pk[1] = "PolNo";
        pk[2] = "DutyCode";
        pk[3] = "GetDutyKind";
        pk[4] = "GetDutyCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getActuGetNo()
    {
        if (ActuGetNo != null && !ActuGetNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ActuGetNo = StrTool.unicodeToGBK(ActuGetNo);
        }
        return ActuGetNo;
    }

    public void setActuGetNo(String aActuGetNo)
    {
        ActuGetNo = aActuGetNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public String getGetDutyKind()
    {
        if (GetDutyKind != null && !GetDutyKind.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyKind = StrTool.unicodeToGBK(GetDutyKind);
        }
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind)
    {
        GetDutyKind = aGetDutyKind;
    }

    public String getGetDutyCode()
    {
        if (GetDutyCode != null && !GetDutyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyCode = StrTool.unicodeToGBK(GetDutyCode);
        }
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getGetDate()
    {
        if (GetDate != null)
        {
            return fDate.getString(GetDate);
        }
        else
        {
            return null;
        }
    }

    public void setGetDate(Date aGetDate)
    {
        GetDate = aGetDate;
    }

    public void setGetDate(String aGetDate)
    {
        if (aGetDate != null && !aGetDate.equals(""))
        {
            GetDate = fDate.getDate(aGetDate);
        }
        else
        {
            GetDate = null;
        }
    }

    public double getGetMoney()
    {
        return GetMoney;
    }

    public void setGetMoney(double aGetMoney)
    {
        GetMoney = aGetMoney;
    }

    public void setGetMoney(String aGetMoney)
    {
        if (aGetMoney != null && !aGetMoney.equals(""))
        {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public String getEnterAccDate()
    {
        if (EnterAccDate != null)
        {
            return fDate.getString(EnterAccDate);
        }
        else
        {
            return null;
        }
    }

    public void setEnterAccDate(Date aEnterAccDate)
    {
        EnterAccDate = aEnterAccDate;
    }

    public void setEnterAccDate(String aEnterAccDate)
    {
        if (aEnterAccDate != null && !aEnterAccDate.equals(""))
        {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        }
        else
        {
            EnterAccDate = null;
        }
    }

    public String getConfDate()
    {
        if (ConfDate != null)
        {
            return fDate.getString(ConfDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfDate(Date aConfDate)
    {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate)
    {
        if (aConfDate != null && !aConfDate.equals(""))
        {
            ConfDate = fDate.getDate(aConfDate);
        }
        else
        {
            ConfDate = null;
        }
    }

    public String getFeeOperationType()
    {
        if (FeeOperationType != null && !FeeOperationType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FeeOperationType = StrTool.unicodeToGBK(FeeOperationType);
        }
        return FeeOperationType;
    }

    public void setFeeOperationType(String aFeeOperationType)
    {
        FeeOperationType = aFeeOperationType;
    }

    public String getFeeFinaType()
    {
        if (FeeFinaType != null && !FeeFinaType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FeeFinaType = StrTool.unicodeToGBK(FeeFinaType);
        }
        return FeeFinaType;
    }

    public void setFeeFinaType(String aFeeFinaType)
    {
        FeeFinaType = aFeeFinaType;
    }

    public String getKindCode()
    {
        if (KindCode != null && !KindCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            KindCode = StrTool.unicodeToGBK(KindCode);
        }
        return KindCode;
    }

    public void setKindCode(String aKindCode)
    {
        KindCode = aKindCode;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentCom()
    {
        if (AgentCom != null && !AgentCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getAgentType()
    {
        if (AgentType != null && !AgentType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentType = StrTool.unicodeToGBK(AgentType);
        }
        return AgentType;
    }

    public void setAgentType(String aAgentType)
    {
        AgentType = aAgentType;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getGrpName()
    {
        if (GrpName != null && !GrpName.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpName = StrTool.unicodeToGBK(GrpName);
        }
        return GrpName;
    }

    public void setGrpName(String aGrpName)
    {
        GrpName = aGrpName;
    }

    public String getHandler()
    {
        if (Handler != null && !Handler.equals("") && SysConst.CHANGECHARSET == true)
        {
            Handler = StrTool.unicodeToGBK(Handler);
        }
        return Handler;
    }

    public void setHandler(String aHandler)
    {
        Handler = aHandler;
    }

    public String getLastGettoDate()
    {
        if (LastGettoDate != null)
        {
            return fDate.getString(LastGettoDate);
        }
        else
        {
            return null;
        }
    }

    public void setLastGettoDate(Date aLastGettoDate)
    {
        LastGettoDate = aLastGettoDate;
    }

    public void setLastGettoDate(String aLastGettoDate)
    {
        if (aLastGettoDate != null && !aLastGettoDate.equals(""))
        {
            LastGettoDate = fDate.getDate(aLastGettoDate);
        }
        else
        {
            LastGettoDate = null;
        }
    }

    public String getCurGetToDate()
    {
        if (CurGetToDate != null)
        {
            return fDate.getString(CurGetToDate);
        }
        else
        {
            return null;
        }
    }

    public void setCurGetToDate(Date aCurGetToDate)
    {
        CurGetToDate = aCurGetToDate;
    }

    public void setCurGetToDate(String aCurGetToDate)
    {
        if (aCurGetToDate != null && !aCurGetToDate.equals(""))
        {
            CurGetToDate = fDate.getDate(aCurGetToDate);
        }
        else
        {
            CurGetToDate = null;
        }
    }

    public String getGetmFirstFlag()
    {
        if (GetmFirstFlag != null && !GetmFirstFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetmFirstFlag = StrTool.unicodeToGBK(GetmFirstFlag);
        }
        return GetmFirstFlag;
    }

    public void setGetmFirstFlag(String aGetmFirstFlag)
    {
        GetmFirstFlag = aGetmFirstFlag;
    }

    public String getGetChannelFlag()
    {
        if (GetChannelFlag != null && !GetChannelFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetChannelFlag = StrTool.unicodeToGBK(GetChannelFlag);
        }
        return GetChannelFlag;
    }

    public void setGetChannelFlag(String aGetChannelFlag)
    {
        GetChannelFlag = aGetChannelFlag;
    }

    public String getDestrayFlag()
    {
        if (DestrayFlag != null && !DestrayFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DestrayFlag = StrTool.unicodeToGBK(DestrayFlag);
        }
        return DestrayFlag;
    }

    public void setDestrayFlag(String aDestrayFlag)
    {
        DestrayFlag = aDestrayFlag;
    }

    public String getPolType()
    {
        if (PolType != null && !PolType.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolType = StrTool.unicodeToGBK(PolType);
        }
        return PolType;
    }

    public void setPolType(String aPolType)
    {
        PolType = aPolType;
    }

    public String getApproveCode()
    {
        if (ApproveCode != null && !ApproveCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ApproveCode = StrTool.unicodeToGBK(ApproveCode);
        }
        return ApproveCode;
    }

    public void setApproveCode(String aApproveCode)
    {
        ApproveCode = aApproveCode;
    }

    public String getApproveDate()
    {
        if (ApproveDate != null)
        {
            return fDate.getString(ApproveDate);
        }
        else
        {
            return null;
        }
    }

    public void setApproveDate(Date aApproveDate)
    {
        ApproveDate = aApproveDate;
    }

    public void setApproveDate(String aApproveDate)
    {
        if (aApproveDate != null && !aApproveDate.equals(""))
        {
            ApproveDate = fDate.getDate(aApproveDate);
        }
        else
        {
            ApproveDate = null;
        }
    }

    public String getApproveTime()
    {
        if (ApproveTime != null && !ApproveTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ApproveTime = StrTool.unicodeToGBK(ApproveTime);
        }
        return ApproveTime;
    }

    public void setApproveTime(String aApproveTime)
    {
        ApproveTime = aApproveTime;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getGetNoticeNo()
    {
        if (GetNoticeNo != null && !GetNoticeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetNoticeNo = StrTool.unicodeToGBK(GetNoticeNo);
        }
        return GetNoticeNo;
    }

    public void setGetNoticeNo(String aGetNoticeNo)
    {
        GetNoticeNo = aGetNoticeNo;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LJAGetDrawSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LJAGetDrawSchema aLJAGetDrawSchema)
    {
        this.ActuGetNo = aLJAGetDrawSchema.getActuGetNo();
        this.PolNo = aLJAGetDrawSchema.getPolNo();
        this.DutyCode = aLJAGetDrawSchema.getDutyCode();
        this.GetDutyKind = aLJAGetDrawSchema.getGetDutyKind();
        this.GetDutyCode = aLJAGetDrawSchema.getGetDutyCode();
        this.GrpContNo = aLJAGetDrawSchema.getGrpContNo();
        this.GrpPolNo = aLJAGetDrawSchema.getGrpPolNo();
        this.ContNo = aLJAGetDrawSchema.getContNo();
        this.AppntNo = aLJAGetDrawSchema.getAppntNo();
        this.InsuredNo = aLJAGetDrawSchema.getInsuredNo();
        this.GetDate = fDate.getDate(aLJAGetDrawSchema.getGetDate());
        this.GetMoney = aLJAGetDrawSchema.getGetMoney();
        this.EnterAccDate = fDate.getDate(aLJAGetDrawSchema.getEnterAccDate());
        this.ConfDate = fDate.getDate(aLJAGetDrawSchema.getConfDate());
        this.FeeOperationType = aLJAGetDrawSchema.getFeeOperationType();
        this.FeeFinaType = aLJAGetDrawSchema.getFeeFinaType();
        this.KindCode = aLJAGetDrawSchema.getKindCode();
        this.RiskCode = aLJAGetDrawSchema.getRiskCode();
        this.RiskVersion = aLJAGetDrawSchema.getRiskVersion();
        this.ManageCom = aLJAGetDrawSchema.getManageCom();
        this.AgentCom = aLJAGetDrawSchema.getAgentCom();
        this.AgentType = aLJAGetDrawSchema.getAgentType();
        this.AgentCode = aLJAGetDrawSchema.getAgentCode();
        this.AgentGroup = aLJAGetDrawSchema.getAgentGroup();
        this.GrpName = aLJAGetDrawSchema.getGrpName();
        this.Handler = aLJAGetDrawSchema.getHandler();
        this.LastGettoDate = fDate.getDate(aLJAGetDrawSchema.getLastGettoDate());
        this.CurGetToDate = fDate.getDate(aLJAGetDrawSchema.getCurGetToDate());
        this.GetmFirstFlag = aLJAGetDrawSchema.getGetmFirstFlag();
        this.GetChannelFlag = aLJAGetDrawSchema.getGetChannelFlag();
        this.DestrayFlag = aLJAGetDrawSchema.getDestrayFlag();
        this.PolType = aLJAGetDrawSchema.getPolType();
        this.ApproveCode = aLJAGetDrawSchema.getApproveCode();
        this.ApproveDate = fDate.getDate(aLJAGetDrawSchema.getApproveDate());
        this.ApproveTime = aLJAGetDrawSchema.getApproveTime();
        this.SerialNo = aLJAGetDrawSchema.getSerialNo();
        this.Operator = aLJAGetDrawSchema.getOperator();
        this.MakeDate = fDate.getDate(aLJAGetDrawSchema.getMakeDate());
        this.MakeTime = aLJAGetDrawSchema.getMakeTime();
        this.GetNoticeNo = aLJAGetDrawSchema.getGetNoticeNo();
        this.ModifyDate = fDate.getDate(aLJAGetDrawSchema.getModifyDate());
        this.ModifyTime = aLJAGetDrawSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ActuGetNo") == null)
            {
                this.ActuGetNo = null;
            }
            else
            {
                this.ActuGetNo = rs.getString("ActuGetNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("GetDutyKind") == null)
            {
                this.GetDutyKind = null;
            }
            else
            {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            this.GetDate = rs.getDate("GetDate");
            this.GetMoney = rs.getDouble("GetMoney");
            this.EnterAccDate = rs.getDate("EnterAccDate");
            this.ConfDate = rs.getDate("ConfDate");
            if (rs.getString("FeeOperationType") == null)
            {
                this.FeeOperationType = null;
            }
            else
            {
                this.FeeOperationType = rs.getString("FeeOperationType").trim();
            }

            if (rs.getString("FeeFinaType") == null)
            {
                this.FeeFinaType = null;
            }
            else
            {
                this.FeeFinaType = rs.getString("FeeFinaType").trim();
            }

            if (rs.getString("KindCode") == null)
            {
                this.KindCode = null;
            }
            else
            {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("AgentType") == null)
            {
                this.AgentType = null;
            }
            else
            {
                this.AgentType = rs.getString("AgentType").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("GrpName") == null)
            {
                this.GrpName = null;
            }
            else
            {
                this.GrpName = rs.getString("GrpName").trim();
            }

            if (rs.getString("Handler") == null)
            {
                this.Handler = null;
            }
            else
            {
                this.Handler = rs.getString("Handler").trim();
            }

            this.LastGettoDate = rs.getDate("LastGettoDate");
            this.CurGetToDate = rs.getDate("CurGetToDate");
            if (rs.getString("GetmFirstFlag") == null)
            {
                this.GetmFirstFlag = null;
            }
            else
            {
                this.GetmFirstFlag = rs.getString("GetmFirstFlag").trim();
            }

            if (rs.getString("GetChannelFlag") == null)
            {
                this.GetChannelFlag = null;
            }
            else
            {
                this.GetChannelFlag = rs.getString("GetChannelFlag").trim();
            }

            if (rs.getString("DestrayFlag") == null)
            {
                this.DestrayFlag = null;
            }
            else
            {
                this.DestrayFlag = rs.getString("DestrayFlag").trim();
            }

            if (rs.getString("PolType") == null)
            {
                this.PolType = null;
            }
            else
            {
                this.PolType = rs.getString("PolType").trim();
            }

            if (rs.getString("ApproveCode") == null)
            {
                this.ApproveCode = null;
            }
            else
            {
                this.ApproveCode = rs.getString("ApproveCode").trim();
            }

            this.ApproveDate = rs.getDate("ApproveDate");
            if (rs.getString("ApproveTime") == null)
            {
                this.ApproveTime = null;
            }
            else
            {
                this.ApproveTime = rs.getString("ApproveTime").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            if (rs.getString("GetNoticeNo") == null)
            {
                this.GetNoticeNo = null;
            }
            else
            {
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDrawSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LJAGetDrawSchema getSchema()
    {
        LJAGetDrawSchema aLJAGetDrawSchema = new LJAGetDrawSchema();
        aLJAGetDrawSchema.setSchema(this);
        return aLJAGetDrawSchema;
    }

    public LJAGetDrawDB getDB()
    {
        LJAGetDrawDB aDBOper = new LJAGetDrawDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJAGetDraw描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ActuGetNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(GetDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetMoney) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EnterAccDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ConfDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FeeOperationType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FeeFinaType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(KindCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Handler)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            LastGettoDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            CurGetToDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetmFirstFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetChannelFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DestrayFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ApproveCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ApproveDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ApproveTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetNoticeNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJAGetDraw>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            GetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            FeeOperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              15, SysConst.PACKAGESPILTER);
            FeeFinaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                         SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                       SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                       SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                     SysConst.PACKAGESPILTER);
            Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                     SysConst.PACKAGESPILTER);
            LastGettoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 27, SysConst.PACKAGESPILTER));
            CurGetToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 28, SysConst.PACKAGESPILTER));
            GetmFirstFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                           SysConst.PACKAGESPILTER);
            GetChannelFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            30, SysConst.PACKAGESPILTER);
            DestrayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                         SysConst.PACKAGESPILTER);
            PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                     SysConst.PACKAGESPILTER);
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,
                                         SysConst.PACKAGESPILTER);
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 34, SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,
                                         SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 38, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,
                                      SysConst.PACKAGESPILTER);
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,
                                         SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 41, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAGetDrawSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ActuGetNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActuGetNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("GetDutyKind"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyKind));
        }
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyCode));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntNo));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredNo));
        }
        if (FCode.equals("GetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getGetDate()));
        }
        if (FCode.equals("GetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetMoney));
        }
        if (FCode.equals("EnterAccDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEnterAccDate()));
        }
        if (FCode.equals("ConfDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getConfDate()));
        }
        if (FCode.equals("FeeOperationType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FeeOperationType));
        }
        if (FCode.equals("FeeFinaType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FeeFinaType));
        }
        if (FCode.equals("KindCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(KindCode));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCom));
        }
        if (FCode.equals("AgentType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentType));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("GrpName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpName));
        }
        if (FCode.equals("Handler"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Handler));
        }
        if (FCode.equals("LastGettoDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getLastGettoDate()));
        }
        if (FCode.equals("CurGetToDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getCurGetToDate()));
        }
        if (FCode.equals("GetmFirstFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetmFirstFlag));
        }
        if (FCode.equals("GetChannelFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetChannelFlag));
        }
        if (FCode.equals("DestrayFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DestrayFlag));
        }
        if (FCode.equals("PolType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolType));
        }
        if (FCode.equals("ApproveCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ApproveCode));
        }
        if (FCode.equals("ApproveDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getApproveDate()));
        }
        if (FCode.equals("ApproveTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ApproveTime));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("GetNoticeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetNoticeNo));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getGetDate()));
                break;
            case 11:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEnterAccDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(FeeOperationType);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(FeeFinaType);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(GrpName);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Handler);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLastGettoDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCurGetToDate()));
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(GetmFirstFlag);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(GetChannelFlag);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(DestrayFlag);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(PolType);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getApproveDate()));
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ActuGetNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActuGetNo = FValue.trim();
            }
            else
            {
                ActuGetNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("GetDutyKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
            {
                GetDutyKind = null;
            }
        }
        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("GetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDate = fDate.getDate(FValue);
            }
            else
            {
                GetDate = null;
            }
        }
        if (FCode.equals("GetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equals("EnterAccDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EnterAccDate = fDate.getDate(FValue);
            }
            else
            {
                EnterAccDate = null;
            }
        }
        if (FCode.equals("ConfDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfDate = fDate.getDate(FValue);
            }
            else
            {
                ConfDate = null;
            }
        }
        if (FCode.equals("FeeOperationType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FeeOperationType = FValue.trim();
            }
            else
            {
                FeeOperationType = null;
            }
        }
        if (FCode.equals("FeeFinaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FeeFinaType = FValue.trim();
            }
            else
            {
                FeeFinaType = null;
            }
        }
        if (FCode.equals("KindCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
            {
                KindCode = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("AgentType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
            {
                AgentType = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("GrpName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
            {
                GrpName = null;
            }
        }
        if (FCode.equals("Handler"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Handler = FValue.trim();
            }
            else
            {
                Handler = null;
            }
        }
        if (FCode.equals("LastGettoDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LastGettoDate = fDate.getDate(FValue);
            }
            else
            {
                LastGettoDate = null;
            }
        }
        if (FCode.equals("CurGetToDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CurGetToDate = fDate.getDate(FValue);
            }
            else
            {
                CurGetToDate = null;
            }
        }
        if (FCode.equals("GetmFirstFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetmFirstFlag = FValue.trim();
            }
            else
            {
                GetmFirstFlag = null;
            }
        }
        if (FCode.equals("GetChannelFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetChannelFlag = FValue.trim();
            }
            else
            {
                GetChannelFlag = null;
            }
        }
        if (FCode.equals("DestrayFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestrayFlag = FValue.trim();
            }
            else
            {
                DestrayFlag = null;
            }
        }
        if (FCode.equals("PolType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolType = FValue.trim();
            }
            else
            {
                PolType = null;
            }
        }
        if (FCode.equals("ApproveCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
            {
                ApproveCode = null;
            }
        }
        if (FCode.equals("ApproveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveDate = fDate.getDate(FValue);
            }
            else
            {
                ApproveDate = null;
            }
        }
        if (FCode.equals("ApproveTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
            {
                ApproveTime = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("GetNoticeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
            {
                GetNoticeNo = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LJAGetDrawSchema other = (LJAGetDrawSchema) otherObject;
        return
                ActuGetNo.equals(other.getActuGetNo())
                && PolNo.equals(other.getPolNo())
                && DutyCode.equals(other.getDutyCode())
                && GetDutyKind.equals(other.getGetDutyKind())
                && GetDutyCode.equals(other.getGetDutyCode())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && ContNo.equals(other.getContNo())
                && AppntNo.equals(other.getAppntNo())
                && InsuredNo.equals(other.getInsuredNo())
                && fDate.getString(GetDate).equals(other.getGetDate())
                && GetMoney == other.getGetMoney()
                && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && FeeOperationType.equals(other.getFeeOperationType())
                && FeeFinaType.equals(other.getFeeFinaType())
                && KindCode.equals(other.getKindCode())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && ManageCom.equals(other.getManageCom())
                && AgentCom.equals(other.getAgentCom())
                && AgentType.equals(other.getAgentType())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && GrpName.equals(other.getGrpName())
                && Handler.equals(other.getHandler())
                && fDate.getString(LastGettoDate).equals(other.getLastGettoDate())
                && fDate.getString(CurGetToDate).equals(other.getCurGetToDate())
                && GetmFirstFlag.equals(other.getGetmFirstFlag())
                && GetChannelFlag.equals(other.getGetChannelFlag())
                && DestrayFlag.equals(other.getDestrayFlag())
                && PolType.equals(other.getPolType())
                && ApproveCode.equals(other.getApproveCode())
                && fDate.getString(ApproveDate).equals(other.getApproveDate())
                && ApproveTime.equals(other.getApproveTime())
                && SerialNo.equals(other.getSerialNo())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && GetNoticeNo.equals(other.getGetNoticeNo())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ActuGetNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return 2;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return 3;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return 4;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 5;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 6;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 7;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 8;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 9;
        }
        if (strFieldName.equals("GetDate"))
        {
            return 10;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return 11;
        }
        if (strFieldName.equals("EnterAccDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return 13;
        }
        if (strFieldName.equals("FeeOperationType"))
        {
            return 14;
        }
        if (strFieldName.equals("FeeFinaType"))
        {
            return 15;
        }
        if (strFieldName.equals("KindCode"))
        {
            return 16;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 17;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 18;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 19;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 20;
        }
        if (strFieldName.equals("AgentType"))
        {
            return 21;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 22;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 23;
        }
        if (strFieldName.equals("GrpName"))
        {
            return 24;
        }
        if (strFieldName.equals("Handler"))
        {
            return 25;
        }
        if (strFieldName.equals("LastGettoDate"))
        {
            return 26;
        }
        if (strFieldName.equals("CurGetToDate"))
        {
            return 27;
        }
        if (strFieldName.equals("GetmFirstFlag"))
        {
            return 28;
        }
        if (strFieldName.equals("GetChannelFlag"))
        {
            return 29;
        }
        if (strFieldName.equals("DestrayFlag"))
        {
            return 30;
        }
        if (strFieldName.equals("PolType"))
        {
            return 31;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return 32;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return 33;
        }
        if (strFieldName.equals("ApproveTime"))
        {
            return 34;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 35;
        }
        if (strFieldName.equals("Operator"))
        {
            return 36;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 37;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 38;
        }
        if (strFieldName.equals("GetNoticeNo"))
        {
            return 39;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 40;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 41;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ActuGetNo";
                break;
            case 1:
                strFieldName = "PolNo";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "GetDutyKind";
                break;
            case 4:
                strFieldName = "GetDutyCode";
                break;
            case 5:
                strFieldName = "GrpContNo";
                break;
            case 6:
                strFieldName = "GrpPolNo";
                break;
            case 7:
                strFieldName = "ContNo";
                break;
            case 8:
                strFieldName = "AppntNo";
                break;
            case 9:
                strFieldName = "InsuredNo";
                break;
            case 10:
                strFieldName = "GetDate";
                break;
            case 11:
                strFieldName = "GetMoney";
                break;
            case 12:
                strFieldName = "EnterAccDate";
                break;
            case 13:
                strFieldName = "ConfDate";
                break;
            case 14:
                strFieldName = "FeeOperationType";
                break;
            case 15:
                strFieldName = "FeeFinaType";
                break;
            case 16:
                strFieldName = "KindCode";
                break;
            case 17:
                strFieldName = "RiskCode";
                break;
            case 18:
                strFieldName = "RiskVersion";
                break;
            case 19:
                strFieldName = "ManageCom";
                break;
            case 20:
                strFieldName = "AgentCom";
                break;
            case 21:
                strFieldName = "AgentType";
                break;
            case 22:
                strFieldName = "AgentCode";
                break;
            case 23:
                strFieldName = "AgentGroup";
                break;
            case 24:
                strFieldName = "GrpName";
                break;
            case 25:
                strFieldName = "Handler";
                break;
            case 26:
                strFieldName = "LastGettoDate";
                break;
            case 27:
                strFieldName = "CurGetToDate";
                break;
            case 28:
                strFieldName = "GetmFirstFlag";
                break;
            case 29:
                strFieldName = "GetChannelFlag";
                break;
            case 30:
                strFieldName = "DestrayFlag";
                break;
            case 31:
                strFieldName = "PolType";
                break;
            case 32:
                strFieldName = "ApproveCode";
                break;
            case 33:
                strFieldName = "ApproveDate";
                break;
            case 34:
                strFieldName = "ApproveTime";
                break;
            case 35:
                strFieldName = "SerialNo";
                break;
            case 36:
                strFieldName = "Operator";
                break;
            case 37:
                strFieldName = "MakeDate";
                break;
            case 38:
                strFieldName = "MakeTime";
                break;
            case 39:
                strFieldName = "GetNoticeNo";
                break;
            case 40:
                strFieldName = "ModifyDate";
                break;
            case 41:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ActuGetNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EnterAccDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FeeOperationType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeFinaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Handler"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LastGettoDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CurGetToDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetmFirstFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetChannelFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestrayFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ApproveTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetNoticeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 27:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 33:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 34:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 35:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 36:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 37:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 38:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 39:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 40:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 41:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
