/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAMettingDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAMettingSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新表结构
 * @CreateDate：2005-07-06
 */
public class LAMettingSchema implements Schema, Cloneable
{
    // @Field
    /** 会议序号 */
    private String SeriesNo;
    /** 会议目的和诉求 */
    private String MettingAim;
    /** 主办机构 */
    private String AgentGroup;
    /** 会议形式 */
    private String MettingForm;
    /** 会议费用 */
    private double PayMoney;
    /** 会议的重要性 */
    private String Import;
    /** 会议时间 */
    private Date DateTime;
    /** 会议地点 */
    private String Address;
    /** 与会领导 */
    private String Leads;
    /** 与会人员 */
    private String Peoples;
    /** 会议议程 */
    private String Agenda;
    /** 会议规范 */
    private String Criterion;
    /** 会议决定 */
    private String Decide;
    /** 会议记录 */
    private String Register;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAMettingSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SeriesNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LAMettingSchema cloned = (LAMettingSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSeriesNo()
    {
        return SeriesNo;
    }

    public void setSeriesNo(String aSeriesNo)
    {
        SeriesNo = aSeriesNo;
    }

    public String getMettingAim()
    {
        return MettingAim;
    }

    public void setMettingAim(String aMettingAim)
    {
        MettingAim = aMettingAim;
    }

    public String getAgentGroup()
    {
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getMettingForm()
    {
        return MettingForm;
    }

    public void setMettingForm(String aMettingForm)
    {
        MettingForm = aMettingForm;
    }

    public double getPayMoney()
    {
        return PayMoney;
    }

    public void setPayMoney(double aPayMoney)
    {
        PayMoney = Arith.round(aPayMoney, 2);
    }

    public void setPayMoney(String aPayMoney)
    {
        if (aPayMoney != null && !aPayMoney.equals(""))
        {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = Arith.round(d, 2);
        }
    }

    public String getImport()
    {
        return Import;
    }

    public void setImport(String aImport)
    {
        Import = aImport;
    }

    public String getDateTime()
    {
        if (DateTime != null)
        {
            return fDate.getString(DateTime);
        }
        else
        {
            return null;
        }
    }

    public void setDateTime(Date aDateTime)
    {
        DateTime = aDateTime;
    }

    public void setDateTime(String aDateTime)
    {
        if (aDateTime != null && !aDateTime.equals(""))
        {
            DateTime = fDate.getDate(aDateTime);
        }
        else
        {
            DateTime = null;
        }
    }

    public String getAddress()
    {
        return Address;
    }

    public void setAddress(String aAddress)
    {
        Address = aAddress;
    }

    public String getLeads()
    {
        return Leads;
    }

    public void setLeads(String aLeads)
    {
        Leads = aLeads;
    }

    public String getPeoples()
    {
        return Peoples;
    }

    public void setPeoples(String aPeoples)
    {
        Peoples = aPeoples;
    }

    public String getAgenda()
    {
        return Agenda;
    }

    public void setAgenda(String aAgenda)
    {
        Agenda = aAgenda;
    }

    public String getCriterion()
    {
        return Criterion;
    }

    public void setCriterion(String aCriterion)
    {
        Criterion = aCriterion;
    }

    public String getDecide()
    {
        return Decide;
    }

    public void setDecide(String aDecide)
    {
        Decide = aDecide;
    }

    public String getRegister()
    {
        return Register;
    }

    public void setRegister(String aRegister)
    {
        Register = aRegister;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAMettingSchema 对象给 Schema 赋值
     * @param: aLAMettingSchema LAMettingSchema
     **/
    public void setSchema(LAMettingSchema aLAMettingSchema)
    {
        this.SeriesNo = aLAMettingSchema.getSeriesNo();
        this.MettingAim = aLAMettingSchema.getMettingAim();
        this.AgentGroup = aLAMettingSchema.getAgentGroup();
        this.MettingForm = aLAMettingSchema.getMettingForm();
        this.PayMoney = aLAMettingSchema.getPayMoney();
        this.Import = aLAMettingSchema.getImport();
        this.DateTime = fDate.getDate(aLAMettingSchema.getDateTime());
        this.Address = aLAMettingSchema.getAddress();
        this.Leads = aLAMettingSchema.getLeads();
        this.Peoples = aLAMettingSchema.getPeoples();
        this.Agenda = aLAMettingSchema.getAgenda();
        this.Criterion = aLAMettingSchema.getCriterion();
        this.Decide = aLAMettingSchema.getDecide();
        this.Register = aLAMettingSchema.getRegister();
        this.Operator = aLAMettingSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAMettingSchema.getMakeDate());
        this.MakeTime = aLAMettingSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAMettingSchema.getModifyDate());
        this.ModifyTime = aLAMettingSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SeriesNo") == null)
            {
                this.SeriesNo = null;
            }
            else
            {
                this.SeriesNo = rs.getString("SeriesNo").trim();
            }

            if (rs.getString("MettingAim") == null)
            {
                this.MettingAim = null;
            }
            else
            {
                this.MettingAim = rs.getString("MettingAim").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("MettingForm") == null)
            {
                this.MettingForm = null;
            }
            else
            {
                this.MettingForm = rs.getString("MettingForm").trim();
            }

            this.PayMoney = rs.getDouble("PayMoney");
            if (rs.getString("Import") == null)
            {
                this.Import = null;
            }
            else
            {
                this.Import = rs.getString("Import").trim();
            }

            this.DateTime = rs.getDate("DateTime");
            if (rs.getString("Address") == null)
            {
                this.Address = null;
            }
            else
            {
                this.Address = rs.getString("Address").trim();
            }

            if (rs.getString("Leads") == null)
            {
                this.Leads = null;
            }
            else
            {
                this.Leads = rs.getString("Leads").trim();
            }

            if (rs.getString("Peoples") == null)
            {
                this.Peoples = null;
            }
            else
            {
                this.Peoples = rs.getString("Peoples").trim();
            }

            if (rs.getString("Agenda") == null)
            {
                this.Agenda = null;
            }
            else
            {
                this.Agenda = rs.getString("Agenda").trim();
            }

            if (rs.getString("Criterion") == null)
            {
                this.Criterion = null;
            }
            else
            {
                this.Criterion = rs.getString("Criterion").trim();
            }

            if (rs.getString("Decide") == null)
            {
                this.Decide = null;
            }
            else
            {
                this.Decide = rs.getString("Decide").trim();
            }

            if (rs.getString("Register") == null)
            {
                this.Register = null;
            }
            else
            {
                this.Register = rs.getString("Register").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LAMetting表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAMettingSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAMettingSchema getSchema()
    {
        LAMettingSchema aLAMettingSchema = new LAMettingSchema();
        aLAMettingSchema.setSchema(this);
        return aLAMettingSchema;
    }

    public LAMettingDB getDB()
    {
        LAMettingDB aDBOper = new LAMettingDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAMetting描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SeriesNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MettingAim));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MettingForm));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Import));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(DateTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Address));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Leads));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Peoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Agenda));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Criterion));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Decide));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Register));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAMetting>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SeriesNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            MettingAim = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            MettingForm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            PayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            Import = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            DateTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            Leads = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            Peoples = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            Agenda = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
            Criterion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            Decide = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                    SysConst.PACKAGESPILTER);
            Register = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAMettingSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SeriesNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeriesNo));
        }
        if (FCode.equals("MettingAim"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MettingAim));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("MettingForm"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MettingForm));
        }
        if (FCode.equals("PayMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (FCode.equals("Import"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Import));
        }
        if (FCode.equals("DateTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getDateTime()));
        }
        if (FCode.equals("Address"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equals("Leads"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Leads));
        }
        if (FCode.equals("Peoples"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equals("Agenda"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Agenda));
        }
        if (FCode.equals("Criterion"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Criterion));
        }
        if (FCode.equals("Decide"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Decide));
        }
        if (FCode.equals("Register"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Register));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SeriesNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(MettingAim);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(MettingForm);
                break;
            case 4:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Import);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getDateTime()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Leads);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Peoples);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Agenda);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Criterion);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Decide);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Register);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SeriesNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SeriesNo = FValue.trim();
            }
            else
            {
                SeriesNo = null;
            }
        }
        if (FCode.equals("MettingAim"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MettingAim = FValue.trim();
            }
            else
            {
                MettingAim = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("MettingForm"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MettingForm = FValue.trim();
            }
            else
            {
                MettingForm = null;
            }
        }
        if (FCode.equals("PayMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equals("Import"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Import = FValue.trim();
            }
            else
            {
                Import = null;
            }
        }
        if (FCode.equals("DateTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DateTime = fDate.getDate(FValue);
            }
            else
            {
                DateTime = null;
            }
        }
        if (FCode.equals("Address"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
            {
                Address = null;
            }
        }
        if (FCode.equals("Leads"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Leads = FValue.trim();
            }
            else
            {
                Leads = null;
            }
        }
        if (FCode.equals("Peoples"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Peoples = FValue.trim();
            }
            else
            {
                Peoples = null;
            }
        }
        if (FCode.equals("Agenda"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Agenda = FValue.trim();
            }
            else
            {
                Agenda = null;
            }
        }
        if (FCode.equals("Criterion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Criterion = FValue.trim();
            }
            else
            {
                Criterion = null;
            }
        }
        if (FCode.equals("Decide"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Decide = FValue.trim();
            }
            else
            {
                Decide = null;
            }
        }
        if (FCode.equals("Register"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Register = FValue.trim();
            }
            else
            {
                Register = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAMettingSchema other = (LAMettingSchema) otherObject;
        return
                SeriesNo.equals(other.getSeriesNo())
                && MettingAim.equals(other.getMettingAim())
                && AgentGroup.equals(other.getAgentGroup())
                && MettingForm.equals(other.getMettingForm())
                && PayMoney == other.getPayMoney()
                && Import.equals(other.getImport())
                && fDate.getString(DateTime).equals(other.getDateTime())
                && Address.equals(other.getAddress())
                && Leads.equals(other.getLeads())
                && Peoples.equals(other.getPeoples())
                && Agenda.equals(other.getAgenda())
                && Criterion.equals(other.getCriterion())
                && Decide.equals(other.getDecide())
                && Register.equals(other.getRegister())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SeriesNo"))
        {
            return 0;
        }
        if (strFieldName.equals("MettingAim"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 2;
        }
        if (strFieldName.equals("MettingForm"))
        {
            return 3;
        }
        if (strFieldName.equals("PayMoney"))
        {
            return 4;
        }
        if (strFieldName.equals("Import"))
        {
            return 5;
        }
        if (strFieldName.equals("DateTime"))
        {
            return 6;
        }
        if (strFieldName.equals("Address"))
        {
            return 7;
        }
        if (strFieldName.equals("Leads"))
        {
            return 8;
        }
        if (strFieldName.equals("Peoples"))
        {
            return 9;
        }
        if (strFieldName.equals("Agenda"))
        {
            return 10;
        }
        if (strFieldName.equals("Criterion"))
        {
            return 11;
        }
        if (strFieldName.equals("Decide"))
        {
            return 12;
        }
        if (strFieldName.equals("Register"))
        {
            return 13;
        }
        if (strFieldName.equals("Operator"))
        {
            return 14;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 16;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SeriesNo";
                break;
            case 1:
                strFieldName = "MettingAim";
                break;
            case 2:
                strFieldName = "AgentGroup";
                break;
            case 3:
                strFieldName = "MettingForm";
                break;
            case 4:
                strFieldName = "PayMoney";
                break;
            case 5:
                strFieldName = "Import";
                break;
            case 6:
                strFieldName = "DateTime";
                break;
            case 7:
                strFieldName = "Address";
                break;
            case 8:
                strFieldName = "Leads";
                break;
            case 9:
                strFieldName = "Peoples";
                break;
            case 10:
                strFieldName = "Agenda";
                break;
            case 11:
                strFieldName = "Criterion";
                break;
            case 12:
                strFieldName = "Decide";
                break;
            case 13:
                strFieldName = "Register";
                break;
            case 14:
                strFieldName = "Operator";
                break;
            case 15:
                strFieldName = "MakeDate";
                break;
            case 16:
                strFieldName = "MakeTime";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SeriesNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MettingAim"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MettingForm"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Import"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DateTime"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Address"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Leads"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Peoples"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Agenda"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Criterion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Decide"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Register"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
