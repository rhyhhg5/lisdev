/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.XFYJInsuredDB;

/*
 * <p>ClassName: XFYJInsuredSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 幸福一家数据存储表
 * @CreateDate：2017-09-13
 */
public class XFYJInsuredSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String BusiNo;
	/** 印刷号 */
	private String PrtNo;
	/** 被保人序号 */
	private String No;
	/** 与投保人关系 */
	private String RelationToAppnt;
	/** 与主被保险人关系 */
	private String RelationToMainInsured;
	/** 被保人姓名 */
	private String InsuredName;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 性别 */
	private String InsuredSex;
	/** 证件生效日期 */
	private Date IDStartDate;
	/** 证件失效日期 */
	private Date IDEndDate;
	/** 出生日期 */
	private Date Birthday;
	/** 职业代码 */
	private String OccupationCode;
	/** 年收入(万元) */
	private double Salary;
	/** 国籍 */
	private String NativePlace;
	/** 工作单位 */
	private String WorkName;
	/** 岗位职务 */
	private String Position;
	/** 邮政编码 */
	private String ZipCode;
	/** 电子邮箱 */
	private String Email;
	/** 移动电话 */
	private String Mobile;
	/** 联系地址 */
	private String PostAddress;
	/** 备用 */
	private String Other;
	/** 创建时间 */
	private String MakeTime;
	/** 最后修改时间 */
	private String ModifyTime;
	/** 创建日期 */
	private Date MakeDate;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 联系省份 */
	private String PostalProvince;
	/** 联系市 */
	private String PostalCity;
	/** 联系县 */
	private String PostalCounty;
	/** 联系乡镇 */
	private String PostalStreet;
	/** 联系村（社区） */
	private String PostalCommunity;

	public static final int FIELDNUM = 31;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public XFYJInsuredSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BusiNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		XFYJInsuredSchema cloned = (XFYJInsuredSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBusiNo()
	{
		return BusiNo;
	}
	public void setBusiNo(String aBusiNo)
	{
		BusiNo = aBusiNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getNo()
	{
		return No;
	}
	public void setNo(String aNo)
	{
		No = aNo;
	}
	public String getRelationToAppnt()
	{
		return RelationToAppnt;
	}
	public void setRelationToAppnt(String aRelationToAppnt)
	{
		RelationToAppnt = aRelationToAppnt;
	}
	public String getRelationToMainInsured()
	{
		return RelationToMainInsured;
	}
	public void setRelationToMainInsured(String aRelationToMainInsured)
	{
		RelationToMainInsured = aRelationToMainInsured;
	}
	public String getInsuredName()
	{
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
		InsuredName = aInsuredName;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getInsuredSex()
	{
		return InsuredSex;
	}
	public void setInsuredSex(String aInsuredSex)
	{
		InsuredSex = aInsuredSex;
	}
	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}

	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public double getSalary()
	{
		return Salary;
	}
	public void setSalary(double aSalary)
	{
		Salary = Arith.round(aSalary,2);
	}
	public void setSalary(String aSalary)
	{
		if (aSalary != null && !aSalary.equals(""))
		{
			Double tDouble = new Double(aSalary);
			double d = tDouble.doubleValue();
                Salary = Arith.round(d,2);
		}
	}

	public String getNativePlace()
	{
		return NativePlace;
	}
	public void setNativePlace(String aNativePlace)
	{
		NativePlace = aNativePlace;
	}
	public String getWorkName()
	{
		return WorkName;
	}
	public void setWorkName(String aWorkName)
	{
		WorkName = aWorkName;
	}
	public String getPosition()
	{
		return Position;
	}
	public void setPosition(String aPosition)
	{
		Position = aPosition;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getEmail()
	{
		return Email;
	}
	public void setEmail(String aEmail)
	{
		Email = aEmail;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getPostAddress()
	{
		return PostAddress;
	}
	public void setPostAddress(String aPostAddress)
	{
		PostAddress = aPostAddress;
	}
	public String getOther()
	{
		return Other;
	}
	public void setOther(String aOther)
	{
		Other = aOther;
	}
	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getPostalProvince()
	{
		return PostalProvince;
	}
	public void setPostalProvince(String aPostalProvince)
	{
		PostalProvince = aPostalProvince;
	}
	public String getPostalCity()
	{
		return PostalCity;
	}
	public void setPostalCity(String aPostalCity)
	{
		PostalCity = aPostalCity;
	}
	public String getPostalCounty()
	{
		return PostalCounty;
	}
	public void setPostalCounty(String aPostalCounty)
	{
		PostalCounty = aPostalCounty;
	}
	public String getPostalStreet()
	{
		return PostalStreet;
	}
	public void setPostalStreet(String aPostalStreet)
	{
		PostalStreet = aPostalStreet;
	}
	public String getPostalCommunity()
	{
		return PostalCommunity;
	}
	public void setPostalCommunity(String aPostalCommunity)
	{
		PostalCommunity = aPostalCommunity;
	}

	/**
	* 使用另外一个 XFYJInsuredSchema 对象给 Schema 赋值
	* @param: aXFYJInsuredSchema XFYJInsuredSchema
	**/
	public void setSchema(XFYJInsuredSchema aXFYJInsuredSchema)
	{
		this.BusiNo = aXFYJInsuredSchema.getBusiNo();
		this.PrtNo = aXFYJInsuredSchema.getPrtNo();
		this.No = aXFYJInsuredSchema.getNo();
		this.RelationToAppnt = aXFYJInsuredSchema.getRelationToAppnt();
		this.RelationToMainInsured = aXFYJInsuredSchema.getRelationToMainInsured();
		this.InsuredName = aXFYJInsuredSchema.getInsuredName();
		this.IDType = aXFYJInsuredSchema.getIDType();
		this.IDNo = aXFYJInsuredSchema.getIDNo();
		this.InsuredSex = aXFYJInsuredSchema.getInsuredSex();
		this.IDStartDate = fDate.getDate( aXFYJInsuredSchema.getIDStartDate());
		this.IDEndDate = fDate.getDate( aXFYJInsuredSchema.getIDEndDate());
		this.Birthday = fDate.getDate( aXFYJInsuredSchema.getBirthday());
		this.OccupationCode = aXFYJInsuredSchema.getOccupationCode();
		this.Salary = aXFYJInsuredSchema.getSalary();
		this.NativePlace = aXFYJInsuredSchema.getNativePlace();
		this.WorkName = aXFYJInsuredSchema.getWorkName();
		this.Position = aXFYJInsuredSchema.getPosition();
		this.ZipCode = aXFYJInsuredSchema.getZipCode();
		this.Email = aXFYJInsuredSchema.getEmail();
		this.Mobile = aXFYJInsuredSchema.getMobile();
		this.PostAddress = aXFYJInsuredSchema.getPostAddress();
		this.Other = aXFYJInsuredSchema.getOther();
		this.MakeTime = aXFYJInsuredSchema.getMakeTime();
		this.ModifyTime = aXFYJInsuredSchema.getModifyTime();
		this.MakeDate = fDate.getDate( aXFYJInsuredSchema.getMakeDate());
		this.ModifyDate = fDate.getDate( aXFYJInsuredSchema.getModifyDate());
		this.PostalProvince = aXFYJInsuredSchema.getPostalProvince();
		this.PostalCity = aXFYJInsuredSchema.getPostalCity();
		this.PostalCounty = aXFYJInsuredSchema.getPostalCounty();
		this.PostalStreet = aXFYJInsuredSchema.getPostalStreet();
		this.PostalCommunity = aXFYJInsuredSchema.getPostalCommunity();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BusiNo") == null )
				this.BusiNo = null;
			else
				this.BusiNo = rs.getString("BusiNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("No") == null )
				this.No = null;
			else
				this.No = rs.getString("No").trim();

			if( rs.getString("RelationToAppnt") == null )
				this.RelationToAppnt = null;
			else
				this.RelationToAppnt = rs.getString("RelationToAppnt").trim();

			if( rs.getString("RelationToMainInsured") == null )
				this.RelationToMainInsured = null;
			else
				this.RelationToMainInsured = rs.getString("RelationToMainInsured").trim();

			if( rs.getString("InsuredName") == null )
				this.InsuredName = null;
			else
				this.InsuredName = rs.getString("InsuredName").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("InsuredSex") == null )
				this.InsuredSex = null;
			else
				this.InsuredSex = rs.getString("InsuredSex").trim();

			this.IDStartDate = rs.getDate("IDStartDate");
			this.IDEndDate = rs.getDate("IDEndDate");
			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			this.Salary = rs.getDouble("Salary");
			if( rs.getString("NativePlace") == null )
				this.NativePlace = null;
			else
				this.NativePlace = rs.getString("NativePlace").trim();

			if( rs.getString("WorkName") == null )
				this.WorkName = null;
			else
				this.WorkName = rs.getString("WorkName").trim();

			if( rs.getString("Position") == null )
				this.Position = null;
			else
				this.Position = rs.getString("Position").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Email") == null )
				this.Email = null;
			else
				this.Email = rs.getString("Email").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("PostAddress") == null )
				this.PostAddress = null;
			else
				this.PostAddress = rs.getString("PostAddress").trim();

			if( rs.getString("Other") == null )
				this.Other = null;
			else
				this.Other = rs.getString("Other").trim();

			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.MakeDate = rs.getDate("MakeDate");
			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("PostalProvince") == null )
				this.PostalProvince = null;
			else
				this.PostalProvince = rs.getString("PostalProvince").trim();

			if( rs.getString("PostalCity") == null )
				this.PostalCity = null;
			else
				this.PostalCity = rs.getString("PostalCity").trim();

			if( rs.getString("PostalCounty") == null )
				this.PostalCounty = null;
			else
				this.PostalCounty = rs.getString("PostalCounty").trim();

			if( rs.getString("PostalStreet") == null )
				this.PostalStreet = null;
			else
				this.PostalStreet = rs.getString("PostalStreet").trim();

			if( rs.getString("PostalCommunity") == null )
				this.PostalCommunity = null;
			else
				this.PostalCommunity = rs.getString("PostalCommunity").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的XFYJInsured表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "XFYJInsuredSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public XFYJInsuredSchema getSchema()
	{
		XFYJInsuredSchema aXFYJInsuredSchema = new XFYJInsuredSchema();
		aXFYJInsuredSchema.setSchema(this);
		return aXFYJInsuredSchema;
	}

	public XFYJInsuredDB getDB()
	{
		XFYJInsuredDB aDBOper = new XFYJInsuredDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpXFYJInsured描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BusiNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(No)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationToAppnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationToMainInsured)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Salary));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WorkName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Email)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalProvince)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCounty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalStreet)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCommunity));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpXFYJInsured>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BusiNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			No = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RelationToAppnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RelationToMainInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			IDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Salary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			WorkName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PostAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Other = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			PostalProvince = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			PostalCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			PostalCounty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			PostalStreet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			PostalCommunity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "XFYJInsuredSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BusiNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusiNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("No"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(No));
		}
		if (FCode.equals("RelationToAppnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
		}
		if (FCode.equals("RelationToMainInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToMainInsured));
		}
		if (FCode.equals("InsuredName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("InsuredSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
		}
		if (FCode.equals("IDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
		}
		if (FCode.equals("IDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("Salary"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
		}
		if (FCode.equals("NativePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
		}
		if (FCode.equals("WorkName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkName));
		}
		if (FCode.equals("Position"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Email"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("PostAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostAddress));
		}
		if (FCode.equals("Other"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("PostalProvince"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalProvince));
		}
		if (FCode.equals("PostalCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCity));
		}
		if (FCode.equals("PostalCounty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCounty));
		}
		if (FCode.equals("PostalStreet"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalStreet));
		}
		if (FCode.equals("PostalCommunity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCommunity));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BusiNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(No);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RelationToAppnt);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RelationToMainInsured);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(InsuredName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(InsuredSex);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 13:
				strFieldValue = String.valueOf(Salary);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(NativePlace);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(WorkName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Position);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Email);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PostAddress);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Other);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(PostalProvince);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(PostalCity);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(PostalCounty);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(PostalStreet);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(PostalCommunity);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BusiNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusiNo = FValue.trim();
			}
			else
				BusiNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("No"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				No = FValue.trim();
			}
			else
				No = null;
		}
		if (FCode.equalsIgnoreCase("RelationToAppnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToAppnt = FValue.trim();
			}
			else
				RelationToAppnt = null;
		}
		if (FCode.equalsIgnoreCase("RelationToMainInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToMainInsured = FValue.trim();
			}
			else
				RelationToMainInsured = null;
		}
		if (FCode.equalsIgnoreCase("InsuredName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredName = FValue.trim();
			}
			else
				InsuredName = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredSex = FValue.trim();
			}
			else
				InsuredSex = null;
		}
		if (FCode.equalsIgnoreCase("IDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDStartDate = fDate.getDate( FValue );
			}
			else
				IDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("IDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDEndDate = fDate.getDate( FValue );
			}
			else
				IDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("Salary"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Salary = d;
			}
		}
		if (FCode.equalsIgnoreCase("NativePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativePlace = FValue.trim();
			}
			else
				NativePlace = null;
		}
		if (FCode.equalsIgnoreCase("WorkName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkName = FValue.trim();
			}
			else
				WorkName = null;
		}
		if (FCode.equalsIgnoreCase("Position"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position = FValue.trim();
			}
			else
				Position = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Email"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Email = FValue.trim();
			}
			else
				Email = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("PostAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostAddress = FValue.trim();
			}
			else
				PostAddress = null;
		}
		if (FCode.equalsIgnoreCase("Other"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other = FValue.trim();
			}
			else
				Other = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("PostalProvince"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalProvince = FValue.trim();
			}
			else
				PostalProvince = null;
		}
		if (FCode.equalsIgnoreCase("PostalCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCity = FValue.trim();
			}
			else
				PostalCity = null;
		}
		if (FCode.equalsIgnoreCase("PostalCounty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCounty = FValue.trim();
			}
			else
				PostalCounty = null;
		}
		if (FCode.equalsIgnoreCase("PostalStreet"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalStreet = FValue.trim();
			}
			else
				PostalStreet = null;
		}
		if (FCode.equalsIgnoreCase("PostalCommunity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCommunity = FValue.trim();
			}
			else
				PostalCommunity = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		XFYJInsuredSchema other = (XFYJInsuredSchema)otherObject;
		return
			(BusiNo == null ? other.getBusiNo() == null : BusiNo.equals(other.getBusiNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (No == null ? other.getNo() == null : No.equals(other.getNo()))
			&& (RelationToAppnt == null ? other.getRelationToAppnt() == null : RelationToAppnt.equals(other.getRelationToAppnt()))
			&& (RelationToMainInsured == null ? other.getRelationToMainInsured() == null : RelationToMainInsured.equals(other.getRelationToMainInsured()))
			&& (InsuredName == null ? other.getInsuredName() == null : InsuredName.equals(other.getInsuredName()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (InsuredSex == null ? other.getInsuredSex() == null : InsuredSex.equals(other.getInsuredSex()))
			&& (IDStartDate == null ? other.getIDStartDate() == null : fDate.getString(IDStartDate).equals(other.getIDStartDate()))
			&& (IDEndDate == null ? other.getIDEndDate() == null : fDate.getString(IDEndDate).equals(other.getIDEndDate()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& Salary == other.getSalary()
			&& (NativePlace == null ? other.getNativePlace() == null : NativePlace.equals(other.getNativePlace()))
			&& (WorkName == null ? other.getWorkName() == null : WorkName.equals(other.getWorkName()))
			&& (Position == null ? other.getPosition() == null : Position.equals(other.getPosition()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Email == null ? other.getEmail() == null : Email.equals(other.getEmail()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (PostAddress == null ? other.getPostAddress() == null : PostAddress.equals(other.getPostAddress()))
			&& (Other == null ? other.getOther() == null : Other.equals(other.getOther()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (PostalProvince == null ? other.getPostalProvince() == null : PostalProvince.equals(other.getPostalProvince()))
			&& (PostalCity == null ? other.getPostalCity() == null : PostalCity.equals(other.getPostalCity()))
			&& (PostalCounty == null ? other.getPostalCounty() == null : PostalCounty.equals(other.getPostalCounty()))
			&& (PostalStreet == null ? other.getPostalStreet() == null : PostalStreet.equals(other.getPostalStreet()))
			&& (PostalCommunity == null ? other.getPostalCommunity() == null : PostalCommunity.equals(other.getPostalCommunity()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BusiNo") ) {
			return 0;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 1;
		}
		if( strFieldName.equals("No") ) {
			return 2;
		}
		if( strFieldName.equals("RelationToAppnt") ) {
			return 3;
		}
		if( strFieldName.equals("RelationToMainInsured") ) {
			return 4;
		}
		if( strFieldName.equals("InsuredName") ) {
			return 5;
		}
		if( strFieldName.equals("IDType") ) {
			return 6;
		}
		if( strFieldName.equals("IDNo") ) {
			return 7;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return 8;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return 9;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return 10;
		}
		if( strFieldName.equals("Birthday") ) {
			return 11;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 12;
		}
		if( strFieldName.equals("Salary") ) {
			return 13;
		}
		if( strFieldName.equals("NativePlace") ) {
			return 14;
		}
		if( strFieldName.equals("WorkName") ) {
			return 15;
		}
		if( strFieldName.equals("Position") ) {
			return 16;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 17;
		}
		if( strFieldName.equals("Email") ) {
			return 18;
		}
		if( strFieldName.equals("Mobile") ) {
			return 19;
		}
		if( strFieldName.equals("PostAddress") ) {
			return 20;
		}
		if( strFieldName.equals("Other") ) {
			return 21;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 23;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 25;
		}
		if( strFieldName.equals("PostalProvince") ) {
			return 26;
		}
		if( strFieldName.equals("PostalCity") ) {
			return 27;
		}
		if( strFieldName.equals("PostalCounty") ) {
			return 28;
		}
		if( strFieldName.equals("PostalStreet") ) {
			return 29;
		}
		if( strFieldName.equals("PostalCommunity") ) {
			return 30;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BusiNo";
				break;
			case 1:
				strFieldName = "PrtNo";
				break;
			case 2:
				strFieldName = "No";
				break;
			case 3:
				strFieldName = "RelationToAppnt";
				break;
			case 4:
				strFieldName = "RelationToMainInsured";
				break;
			case 5:
				strFieldName = "InsuredName";
				break;
			case 6:
				strFieldName = "IDType";
				break;
			case 7:
				strFieldName = "IDNo";
				break;
			case 8:
				strFieldName = "InsuredSex";
				break;
			case 9:
				strFieldName = "IDStartDate";
				break;
			case 10:
				strFieldName = "IDEndDate";
				break;
			case 11:
				strFieldName = "Birthday";
				break;
			case 12:
				strFieldName = "OccupationCode";
				break;
			case 13:
				strFieldName = "Salary";
				break;
			case 14:
				strFieldName = "NativePlace";
				break;
			case 15:
				strFieldName = "WorkName";
				break;
			case 16:
				strFieldName = "Position";
				break;
			case 17:
				strFieldName = "ZipCode";
				break;
			case 18:
				strFieldName = "Email";
				break;
			case 19:
				strFieldName = "Mobile";
				break;
			case 20:
				strFieldName = "PostAddress";
				break;
			case 21:
				strFieldName = "Other";
				break;
			case 22:
				strFieldName = "MakeTime";
				break;
			case 23:
				strFieldName = "ModifyTime";
				break;
			case 24:
				strFieldName = "MakeDate";
				break;
			case 25:
				strFieldName = "ModifyDate";
				break;
			case 26:
				strFieldName = "PostalProvince";
				break;
			case 27:
				strFieldName = "PostalCity";
				break;
			case 28:
				strFieldName = "PostalCounty";
				break;
			case 29:
				strFieldName = "PostalStreet";
				break;
			case 30:
				strFieldName = "PostalCommunity";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BusiNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("No") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationToAppnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationToMainInsured") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Salary") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("NativePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WorkName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Position") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Email") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Other") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PostalProvince") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCounty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalStreet") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCommunity") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
