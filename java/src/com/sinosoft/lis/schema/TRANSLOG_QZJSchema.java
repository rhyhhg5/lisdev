/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TRANSLOG_QZJDB;

/*
 * <p>ClassName: TRANSLOG_QZJSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-10-19
 */
public class TRANSLOG_QZJSchema implements Schema, Cloneable
{
	// @Field
	/** Id */
	private int ID;
	/** Batchno */
	private String BatchNo;
	/** Proposalno */
	private String ProposalNo;
	/** Filename */
	private String FileName;
	/** Clientip */
	private String ClientIp;
	/** Sourcechannel */
	private String SourceChannel;
	/** Priority */
	private String PrioRity;
	/** Filemd5 */
	private String FileMD5;
	/** Localfilepaths */
	private String LocalFilePaths;
	/** Remotefilepaths */
	private String RemoteFilePaths;
	/** Cookie */
	private String Cookie;
	/** Task_id */
	private String task_id;
	/** Ossdowurl */
	private String OssDowUrl;
	/** Transtype */
	private String TransType;
	/** Reqmessage */
	private InputStream ReqMessage;
	/** Flag */
	private String Flag;
	/** Status */
	private String Status;
	/** Transmsg */
	private String TransMsg;
	/** Filemakedate */
	private Date FileMakeDate;
	/** Makedate */
	private Date MakeDate;
	/** Modifydate */
	private Date ModifyDate;
	/** Incrementid */
	private int IncrementID;
	/** Bak1 */
	private String bak1;
	/** Bak2 */
	private String bak2;
	/** Bak3 */
	private String bak3;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TRANSLOG_QZJSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TRANSLOG_QZJSchema cloned = (TRANSLOG_QZJSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public int getID()
	{
		return ID;
	}
	public void setID(int aID)
	{
		ID = aID;
	}
	public void setID(String aID)
	{
		if (aID != null && !aID.equals(""))
		{
			Integer tInteger = new Integer(aID);
			int i = tInteger.intValue();
			ID = i;
		}
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getProposalNo()
	{
		return ProposalNo;
	}
	public void setProposalNo(String aProposalNo)
	{
		ProposalNo = aProposalNo;
	}
	public String getFileName()
	{
		return FileName;
	}
	public void setFileName(String aFileName)
	{
		FileName = aFileName;
	}
	public String getClientIp()
	{
		return ClientIp;
	}
	public void setClientIp(String aClientIp)
	{
		ClientIp = aClientIp;
	}
	public String getSourceChannel()
	{
		return SourceChannel;
	}
	public void setSourceChannel(String aSourceChannel)
	{
		SourceChannel = aSourceChannel;
	}
	public String getPrioRity()
	{
		return PrioRity;
	}
	public void setPrioRity(String aPrioRity)
	{
		PrioRity = aPrioRity;
	}
	public String getFileMD5()
	{
		return FileMD5;
	}
	public void setFileMD5(String aFileMD5)
	{
		FileMD5 = aFileMD5;
	}
	public String getLocalFilePaths()
	{
		return LocalFilePaths;
	}
	public void setLocalFilePaths(String aLocalFilePaths)
	{
		LocalFilePaths = aLocalFilePaths;
	}
	public String getRemoteFilePaths()
	{
		return RemoteFilePaths;
	}
	public void setRemoteFilePaths(String aRemoteFilePaths)
	{
		RemoteFilePaths = aRemoteFilePaths;
	}
	public String getCookie()
	{
		return Cookie;
	}
	public void setCookie(String aCookie)
	{
		Cookie = aCookie;
	}
	public String gettask_id()
	{
		return task_id;
	}
	public void settask_id(String atask_id)
	{
		task_id = atask_id;
	}
	public String getOssDowUrl()
	{
		return OssDowUrl;
	}
	public void setOssDowUrl(String aOssDowUrl)
	{
		OssDowUrl = aOssDowUrl;
	}
	public String getTransType()
	{
		return TransType;
	}
	public void setTransType(String aTransType)
	{
		TransType = aTransType;
	}
	public InputStream getReqMessage()
	{
		return ReqMessage;
	}
	public void setReqMessage(InputStream aReqMessage)
	{
		ReqMessage = aReqMessage;
	}
	public void setReqMessage(String aReqMessage)
	{
	}

	public String getFlag()
	{
		return Flag;
	}
	public void setFlag(String aFlag)
	{
		Flag = aFlag;
	}
	public String getStatus()
	{
		return Status;
	}
	public void setStatus(String aStatus)
	{
		Status = aStatus;
	}
	public String getTransMsg()
	{
		return TransMsg;
	}
	public void setTransMsg(String aTransMsg)
	{
		TransMsg = aTransMsg;
	}
	public String getFileMakeDate()
	{
		if( FileMakeDate != null )
			return fDate.getString(FileMakeDate);
		else
			return null;
	}
	public void setFileMakeDate(Date aFileMakeDate)
	{
		FileMakeDate = aFileMakeDate;
	}
	public void setFileMakeDate(String aFileMakeDate)
	{
		if (aFileMakeDate != null && !aFileMakeDate.equals("") )
		{
			FileMakeDate = fDate.getDate( aFileMakeDate );
		}
		else
			FileMakeDate = null;
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public int getIncrementID()
	{
		return IncrementID;
	}
	public void setIncrementID(int aIncrementID)
	{
		IncrementID = aIncrementID;
	}
	public void setIncrementID(String aIncrementID)
	{
		if (aIncrementID != null && !aIncrementID.equals(""))
		{
			Integer tInteger = new Integer(aIncrementID);
			int i = tInteger.intValue();
			IncrementID = i;
		}
	}

	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}

	/**
	* 使用另外一个 TRANSLOG_QZJSchema 对象给 Schema 赋值
	* @param: aTRANSLOG_QZJSchema TRANSLOG_QZJSchema
	**/
	public void setSchema(TRANSLOG_QZJSchema aTRANSLOG_QZJSchema)
	{
		this.ID = aTRANSLOG_QZJSchema.getID();
		this.BatchNo = aTRANSLOG_QZJSchema.getBatchNo();
		this.ProposalNo = aTRANSLOG_QZJSchema.getProposalNo();
		this.FileName = aTRANSLOG_QZJSchema.getFileName();
		this.ClientIp = aTRANSLOG_QZJSchema.getClientIp();
		this.SourceChannel = aTRANSLOG_QZJSchema.getSourceChannel();
		this.PrioRity = aTRANSLOG_QZJSchema.getPrioRity();
		this.FileMD5 = aTRANSLOG_QZJSchema.getFileMD5();
		this.LocalFilePaths = aTRANSLOG_QZJSchema.getLocalFilePaths();
		this.RemoteFilePaths = aTRANSLOG_QZJSchema.getRemoteFilePaths();
		this.Cookie = aTRANSLOG_QZJSchema.getCookie();
		this.task_id = aTRANSLOG_QZJSchema.gettask_id();
		this.OssDowUrl = aTRANSLOG_QZJSchema.getOssDowUrl();
		this.TransType = aTRANSLOG_QZJSchema.getTransType();
		this.ReqMessage = aTRANSLOG_QZJSchema.getReqMessage();
		this.Flag = aTRANSLOG_QZJSchema.getFlag();
		this.Status = aTRANSLOG_QZJSchema.getStatus();
		this.TransMsg = aTRANSLOG_QZJSchema.getTransMsg();
		this.FileMakeDate = fDate.getDate( aTRANSLOG_QZJSchema.getFileMakeDate());
		this.MakeDate = fDate.getDate( aTRANSLOG_QZJSchema.getMakeDate());
		this.ModifyDate = fDate.getDate( aTRANSLOG_QZJSchema.getModifyDate());
		this.IncrementID = aTRANSLOG_QZJSchema.getIncrementID();
		this.bak1 = aTRANSLOG_QZJSchema.getbak1();
		this.bak2 = aTRANSLOG_QZJSchema.getbak2();
		this.bak3 = aTRANSLOG_QZJSchema.getbak3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.ID = rs.getInt("ID");
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("ProposalNo") == null )
				this.ProposalNo = null;
			else
				this.ProposalNo = rs.getString("ProposalNo").trim();

			if( rs.getString("FileName") == null )
				this.FileName = null;
			else
				this.FileName = rs.getString("FileName").trim();

			if( rs.getString("ClientIp") == null )
				this.ClientIp = null;
			else
				this.ClientIp = rs.getString("ClientIp").trim();

			if( rs.getString("SourceChannel") == null )
				this.SourceChannel = null;
			else
				this.SourceChannel = rs.getString("SourceChannel").trim();

			if( rs.getString("PrioRity") == null )
				this.PrioRity = null;
			else
				this.PrioRity = rs.getString("PrioRity").trim();

			if( rs.getString("FileMD5") == null )
				this.FileMD5 = null;
			else
				this.FileMD5 = rs.getString("FileMD5").trim();

			if( rs.getString("LocalFilePaths") == null )
				this.LocalFilePaths = null;
			else
				this.LocalFilePaths = rs.getString("LocalFilePaths").trim();

			if( rs.getString("RemoteFilePaths") == null )
				this.RemoteFilePaths = null;
			else
				this.RemoteFilePaths = rs.getString("RemoteFilePaths").trim();

			if( rs.getString("Cookie") == null )
				this.Cookie = null;
			else
				this.Cookie = rs.getString("Cookie").trim();

			if( rs.getString("task_id") == null )
				this.task_id = null;
			else
				this.task_id = rs.getString("task_id").trim();

			if( rs.getString("OssDowUrl") == null )
				this.OssDowUrl = null;
			else
				this.OssDowUrl = rs.getString("OssDowUrl").trim();

			if( rs.getString("TransType") == null )
				this.TransType = null;
			else
				this.TransType = rs.getString("TransType").trim();

			this.ReqMessage = rs.getBinaryStream("ReqMessage");
			if( rs.getString("Flag") == null )
				this.Flag = null;
			else
				this.Flag = rs.getString("Flag").trim();

			if( rs.getString("Status") == null )
				this.Status = null;
			else
				this.Status = rs.getString("Status").trim();

			if( rs.getString("TransMsg") == null )
				this.TransMsg = null;
			else
				this.TransMsg = rs.getString("TransMsg").trim();

			this.FileMakeDate = rs.getDate("FileMakeDate");
			this.MakeDate = rs.getDate("MakeDate");
			this.ModifyDate = rs.getDate("ModifyDate");
			this.IncrementID = rs.getInt("IncrementID");
			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TRANSLOG_QZJ表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TRANSLOG_QZJSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TRANSLOG_QZJSchema getSchema()
	{
		TRANSLOG_QZJSchema aTRANSLOG_QZJSchema = new TRANSLOG_QZJSchema();
		aTRANSLOG_QZJSchema.setSchema(this);
		return aTRANSLOG_QZJSchema;
	}

	public TRANSLOG_QZJDB getDB()
	{
		TRANSLOG_QZJDB aDBOper = new TRANSLOG_QZJDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTRANSLOG_QZJ描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append( ChgData.chgData(ID));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClientIp)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SourceChannel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrioRity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileMD5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LocalFilePaths)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RemoteFilePaths)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Cookie)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(task_id)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OssDowUrl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( 1 );strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Flag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransMsg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FileMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(IncrementID));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTRANSLOG_QZJ>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ID= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).intValue();
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ClientIp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			SourceChannel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			PrioRity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			FileMD5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			LocalFilePaths = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RemoteFilePaths = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Cookie = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			task_id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			OssDowUrl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			
			Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			TransMsg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			FileMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			IncrementID= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TRANSLOG_QZJSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ID));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ProposalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
		}
		if (FCode.equals("FileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
		}
		if (FCode.equals("ClientIp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClientIp));
		}
		if (FCode.equals("SourceChannel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SourceChannel));
		}
		if (FCode.equals("PrioRity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrioRity));
		}
		if (FCode.equals("FileMD5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileMD5));
		}
		if (FCode.equals("LocalFilePaths"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LocalFilePaths));
		}
		if (FCode.equals("RemoteFilePaths"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemoteFilePaths));
		}
		if (FCode.equals("Cookie"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Cookie));
		}
		if (FCode.equals("task_id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(task_id));
		}
		if (FCode.equals("OssDowUrl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OssDowUrl));
		}
		if (FCode.equals("TransType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
		}
		if (FCode.equals("ReqMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReqMessage));
		}
		if (FCode.equals("Flag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
		}
		if (FCode.equals("Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
		}
		if (FCode.equals("TransMsg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMsg));
		}
		if (FCode.equals("FileMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFileMakeDate()));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("IncrementID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IncrementID));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(ID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ProposalNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(FileName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ClientIp);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(SourceChannel);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(PrioRity);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(FileMD5);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(LocalFilePaths);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RemoteFilePaths);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Cookie);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(task_id);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(OssDowUrl);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(TransType);
				break;
			case 14:
				strFieldValue = String.valueOf(ReqMessage);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Flag);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Status);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(TransMsg);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFileMakeDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 21:
				strFieldValue = String.valueOf(IncrementID);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ID = i;
			}
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalNo = FValue.trim();
			}
			else
				ProposalNo = null;
		}
		if (FCode.equalsIgnoreCase("FileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileName = FValue.trim();
			}
			else
				FileName = null;
		}
		if (FCode.equalsIgnoreCase("ClientIp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClientIp = FValue.trim();
			}
			else
				ClientIp = null;
		}
		if (FCode.equalsIgnoreCase("SourceChannel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SourceChannel = FValue.trim();
			}
			else
				SourceChannel = null;
		}
		if (FCode.equalsIgnoreCase("PrioRity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrioRity = FValue.trim();
			}
			else
				PrioRity = null;
		}
		if (FCode.equalsIgnoreCase("FileMD5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileMD5 = FValue.trim();
			}
			else
				FileMD5 = null;
		}
		if (FCode.equalsIgnoreCase("LocalFilePaths"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LocalFilePaths = FValue.trim();
			}
			else
				LocalFilePaths = null;
		}
		if (FCode.equalsIgnoreCase("RemoteFilePaths"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RemoteFilePaths = FValue.trim();
			}
			else
				RemoteFilePaths = null;
		}
		if (FCode.equalsIgnoreCase("Cookie"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Cookie = FValue.trim();
			}
			else
				Cookie = null;
		}
		if (FCode.equalsIgnoreCase("task_id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				task_id = FValue.trim();
			}
			else
				task_id = null;
		}
		if (FCode.equalsIgnoreCase("OssDowUrl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OssDowUrl = FValue.trim();
			}
			else
				OssDowUrl = null;
		}
		if (FCode.equalsIgnoreCase("TransType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransType = FValue.trim();
			}
			else
				TransType = null;
		}
		if (FCode.equalsIgnoreCase("ReqMessage"))
		{
		}
		if (FCode.equalsIgnoreCase("Flag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Flag = FValue.trim();
			}
			else
				Flag = null;
		}
		if (FCode.equalsIgnoreCase("Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status = FValue.trim();
			}
			else
				Status = null;
		}
		if (FCode.equalsIgnoreCase("TransMsg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransMsg = FValue.trim();
			}
			else
				TransMsg = null;
		}
		if (FCode.equalsIgnoreCase("FileMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FileMakeDate = fDate.getDate( FValue );
			}
			else
				FileMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("IncrementID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				IncrementID = i;
			}
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TRANSLOG_QZJSchema other = (TRANSLOG_QZJSchema)otherObject;
		return
			ID == other.getID()
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (ProposalNo == null ? other.getProposalNo() == null : ProposalNo.equals(other.getProposalNo()))
			&& (FileName == null ? other.getFileName() == null : FileName.equals(other.getFileName()))
			&& (ClientIp == null ? other.getClientIp() == null : ClientIp.equals(other.getClientIp()))
			&& (SourceChannel == null ? other.getSourceChannel() == null : SourceChannel.equals(other.getSourceChannel()))
			&& (PrioRity == null ? other.getPrioRity() == null : PrioRity.equals(other.getPrioRity()))
			&& (FileMD5 == null ? other.getFileMD5() == null : FileMD5.equals(other.getFileMD5()))
			&& (LocalFilePaths == null ? other.getLocalFilePaths() == null : LocalFilePaths.equals(other.getLocalFilePaths()))
			&& (RemoteFilePaths == null ? other.getRemoteFilePaths() == null : RemoteFilePaths.equals(other.getRemoteFilePaths()))
			&& (Cookie == null ? other.getCookie() == null : Cookie.equals(other.getCookie()))
			&& (task_id == null ? other.gettask_id() == null : task_id.equals(other.gettask_id()))
			&& (OssDowUrl == null ? other.getOssDowUrl() == null : OssDowUrl.equals(other.getOssDowUrl()))
			&& (TransType == null ? other.getTransType() == null : TransType.equals(other.getTransType()))
			
			&& (Flag == null ? other.getFlag() == null : Flag.equals(other.getFlag()))
			&& (Status == null ? other.getStatus() == null : Status.equals(other.getStatus()))
			&& (TransMsg == null ? other.getTransMsg() == null : TransMsg.equals(other.getTransMsg()))
			&& (FileMakeDate == null ? other.getFileMakeDate() == null : fDate.getString(FileMakeDate).equals(other.getFileMakeDate()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& IncrementID == other.getIncrementID()
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ID") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return 2;
		}
		if( strFieldName.equals("FileName") ) {
			return 3;
		}
		if( strFieldName.equals("ClientIp") ) {
			return 4;
		}
		if( strFieldName.equals("SourceChannel") ) {
			return 5;
		}
		if( strFieldName.equals("PrioRity") ) {
			return 6;
		}
		if( strFieldName.equals("FileMD5") ) {
			return 7;
		}
		if( strFieldName.equals("LocalFilePaths") ) {
			return 8;
		}
		if( strFieldName.equals("RemoteFilePaths") ) {
			return 9;
		}
		if( strFieldName.equals("Cookie") ) {
			return 10;
		}
		if( strFieldName.equals("task_id") ) {
			return 11;
		}
		if( strFieldName.equals("OssDowUrl") ) {
			return 12;
		}
		if( strFieldName.equals("TransType") ) {
			return 13;
		}
		if( strFieldName.equals("ReqMessage") ) {
			return 14;
		}
		if( strFieldName.equals("Flag") ) {
			return 15;
		}
		if( strFieldName.equals("Status") ) {
			return 16;
		}
		if( strFieldName.equals("TransMsg") ) {
			return 17;
		}
		if( strFieldName.equals("FileMakeDate") ) {
			return 18;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 20;
		}
		if( strFieldName.equals("IncrementID") ) {
			return 21;
		}
		if( strFieldName.equals("bak1") ) {
			return 22;
		}
		if( strFieldName.equals("bak2") ) {
			return 23;
		}
		if( strFieldName.equals("bak3") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ID";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "ProposalNo";
				break;
			case 3:
				strFieldName = "FileName";
				break;
			case 4:
				strFieldName = "ClientIp";
				break;
			case 5:
				strFieldName = "SourceChannel";
				break;
			case 6:
				strFieldName = "PrioRity";
				break;
			case 7:
				strFieldName = "FileMD5";
				break;
			case 8:
				strFieldName = "LocalFilePaths";
				break;
			case 9:
				strFieldName = "RemoteFilePaths";
				break;
			case 10:
				strFieldName = "Cookie";
				break;
			case 11:
				strFieldName = "task_id";
				break;
			case 12:
				strFieldName = "OssDowUrl";
				break;
			case 13:
				strFieldName = "TransType";
				break;
			case 14:
				strFieldName = "ReqMessage";
				break;
			case 15:
				strFieldName = "Flag";
				break;
			case 16:
				strFieldName = "Status";
				break;
			case 17:
				strFieldName = "TransMsg";
				break;
			case 18:
				strFieldName = "FileMakeDate";
				break;
			case 19:
				strFieldName = "MakeDate";
				break;
			case 20:
				strFieldName = "ModifyDate";
				break;
			case 21:
				strFieldName = "IncrementID";
				break;
			case 22:
				strFieldName = "bak1";
				break;
			case 23:
				strFieldName = "bak2";
				break;
			case 24:
				strFieldName = "bak3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ID") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClientIp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SourceChannel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrioRity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileMD5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LocalFilePaths") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RemoteFilePaths") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Cookie") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("task_id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OssDowUrl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReqMessage") ) {
			return Schema.TYPE_NOFOUND;
		}
		if( strFieldName.equals("Flag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransMsg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IncrementID") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_INT;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_NOFOUND;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
