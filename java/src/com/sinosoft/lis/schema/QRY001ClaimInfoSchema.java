/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.QRY001ClaimInfoDB;

/*
 * <p>ClassName: QRY001ClaimInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 税优客户概要信息
 * @CreateDate：2016-03-28
 */
public class QRY001ClaimInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerNo;
	/** 赔案编码 */
	private String ClaimNo;
	/** 出险日期 */
	private Date AccidentDate;
	/** 结案时间 */
	private Date EndcaseDate;
	/** 理赔结论代码 */
	private String ClaimConclusionCode;
	/** 警示标识 */
	private String WarningIndi;
	/** 警示原因描述 */
	private String WainingDesc;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public QRY001ClaimInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		QRY001ClaimInfoSchema cloned = (QRY001ClaimInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerNo()
	{
		return SerNo;
	}
	public void setSerNo(String aSerNo)
	{
		SerNo = aSerNo;
	}
	public String getClaimNo()
	{
		return ClaimNo;
	}
	public void setClaimNo(String aClaimNo)
	{
		ClaimNo = aClaimNo;
	}
	public String getAccidentDate()
	{
		if( AccidentDate != null )
			return fDate.getString(AccidentDate);
		else
			return null;
	}
	public void setAccidentDate(Date aAccidentDate)
	{
		AccidentDate = aAccidentDate;
	}
	public void setAccidentDate(String aAccidentDate)
	{
		if (aAccidentDate != null && !aAccidentDate.equals("") )
		{
			AccidentDate = fDate.getDate( aAccidentDate );
		}
		else
			AccidentDate = null;
	}

	public String getEndcaseDate()
	{
		if( EndcaseDate != null )
			return fDate.getString(EndcaseDate);
		else
			return null;
	}
	public void setEndcaseDate(Date aEndcaseDate)
	{
		EndcaseDate = aEndcaseDate;
	}
	public void setEndcaseDate(String aEndcaseDate)
	{
		if (aEndcaseDate != null && !aEndcaseDate.equals("") )
		{
			EndcaseDate = fDate.getDate( aEndcaseDate );
		}
		else
			EndcaseDate = null;
	}

	public String getClaimConclusionCode()
	{
		return ClaimConclusionCode;
	}
	public void setClaimConclusionCode(String aClaimConclusionCode)
	{
		ClaimConclusionCode = aClaimConclusionCode;
	}
	public String getWarningIndi()
	{
		return WarningIndi;
	}
	public void setWarningIndi(String aWarningIndi)
	{
		WarningIndi = aWarningIndi;
	}
	public String getWainingDesc()
	{
		return WainingDesc;
	}
	public void setWainingDesc(String aWainingDesc)
	{
		WainingDesc = aWainingDesc;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}

	/**
	* 使用另外一个 QRY001ClaimInfoSchema 对象给 Schema 赋值
	* @param: aQRY001ClaimInfoSchema QRY001ClaimInfoSchema
	**/
	public void setSchema(QRY001ClaimInfoSchema aQRY001ClaimInfoSchema)
	{
		this.SerNo = aQRY001ClaimInfoSchema.getSerNo();
		this.ClaimNo = aQRY001ClaimInfoSchema.getClaimNo();
		this.AccidentDate = fDate.getDate( aQRY001ClaimInfoSchema.getAccidentDate());
		this.EndcaseDate = fDate.getDate( aQRY001ClaimInfoSchema.getEndcaseDate());
		this.ClaimConclusionCode = aQRY001ClaimInfoSchema.getClaimConclusionCode();
		this.WarningIndi = aQRY001ClaimInfoSchema.getWarningIndi();
		this.WainingDesc = aQRY001ClaimInfoSchema.getWainingDesc();
		this.ModifyDate = fDate.getDate( aQRY001ClaimInfoSchema.getModifyDate());
		this.ModifyTime = aQRY001ClaimInfoSchema.getModifyTime();
		this.MakeDate = fDate.getDate( aQRY001ClaimInfoSchema.getMakeDate());
		this.MakeTime = aQRY001ClaimInfoSchema.getMakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerNo") == null )
				this.SerNo = null;
			else
				this.SerNo = rs.getString("SerNo").trim();

			if( rs.getString("ClaimNo") == null )
				this.ClaimNo = null;
			else
				this.ClaimNo = rs.getString("ClaimNo").trim();

			this.AccidentDate = rs.getDate("AccidentDate");
			this.EndcaseDate = rs.getDate("EndcaseDate");
			if( rs.getString("ClaimConclusionCode") == null )
				this.ClaimConclusionCode = null;
			else
				this.ClaimConclusionCode = rs.getString("ClaimConclusionCode").trim();

			if( rs.getString("WarningIndi") == null )
				this.WarningIndi = null;
			else
				this.WarningIndi = rs.getString("WarningIndi").trim();

			if( rs.getString("WainingDesc") == null )
				this.WainingDesc = null;
			else
				this.WainingDesc = rs.getString("WainingDesc").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的QRY001ClaimInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QRY001ClaimInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public QRY001ClaimInfoSchema getSchema()
	{
		QRY001ClaimInfoSchema aQRY001ClaimInfoSchema = new QRY001ClaimInfoSchema();
		aQRY001ClaimInfoSchema.setSchema(this);
		return aQRY001ClaimInfoSchema;
	}

	public QRY001ClaimInfoDB getDB()
	{
		QRY001ClaimInfoDB aDBOper = new QRY001ClaimInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpQRY001ClaimInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccidentDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndcaseDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimConclusionCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WarningIndi)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WainingDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpQRY001ClaimInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ClaimNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AccidentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			EndcaseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			ClaimConclusionCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			WarningIndi = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			WainingDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "QRY001ClaimInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerNo));
		}
		if (FCode.equals("ClaimNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimNo));
		}
		if (FCode.equals("AccidentDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
		}
		if (FCode.equals("EndcaseDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndcaseDate()));
		}
		if (FCode.equals("ClaimConclusionCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimConclusionCode));
		}
		if (FCode.equals("WarningIndi"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WarningIndi));
		}
		if (FCode.equals("WainingDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WainingDesc));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ClaimNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndcaseDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ClaimConclusionCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(WarningIndi);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(WainingDesc);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerNo = FValue.trim();
			}
			else
				SerNo = null;
		}
		if (FCode.equalsIgnoreCase("ClaimNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimNo = FValue.trim();
			}
			else
				ClaimNo = null;
		}
		if (FCode.equalsIgnoreCase("AccidentDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccidentDate = fDate.getDate( FValue );
			}
			else
				AccidentDate = null;
		}
		if (FCode.equalsIgnoreCase("EndcaseDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndcaseDate = fDate.getDate( FValue );
			}
			else
				EndcaseDate = null;
		}
		if (FCode.equalsIgnoreCase("ClaimConclusionCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimConclusionCode = FValue.trim();
			}
			else
				ClaimConclusionCode = null;
		}
		if (FCode.equalsIgnoreCase("WarningIndi"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WarningIndi = FValue.trim();
			}
			else
				WarningIndi = null;
		}
		if (FCode.equalsIgnoreCase("WainingDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WainingDesc = FValue.trim();
			}
			else
				WainingDesc = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		QRY001ClaimInfoSchema other = (QRY001ClaimInfoSchema)otherObject;
		return
			(SerNo == null ? other.getSerNo() == null : SerNo.equals(other.getSerNo()))
			&& (ClaimNo == null ? other.getClaimNo() == null : ClaimNo.equals(other.getClaimNo()))
			&& (AccidentDate == null ? other.getAccidentDate() == null : fDate.getString(AccidentDate).equals(other.getAccidentDate()))
			&& (EndcaseDate == null ? other.getEndcaseDate() == null : fDate.getString(EndcaseDate).equals(other.getEndcaseDate()))
			&& (ClaimConclusionCode == null ? other.getClaimConclusionCode() == null : ClaimConclusionCode.equals(other.getClaimConclusionCode()))
			&& (WarningIndi == null ? other.getWarningIndi() == null : WarningIndi.equals(other.getWarningIndi()))
			&& (WainingDesc == null ? other.getWainingDesc() == null : WainingDesc.equals(other.getWainingDesc()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return 0;
		}
		if( strFieldName.equals("ClaimNo") ) {
			return 1;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return 2;
		}
		if( strFieldName.equals("EndcaseDate") ) {
			return 3;
		}
		if( strFieldName.equals("ClaimConclusionCode") ) {
			return 4;
		}
		if( strFieldName.equals("WarningIndi") ) {
			return 5;
		}
		if( strFieldName.equals("WainingDesc") ) {
			return 6;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 7;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerNo";
				break;
			case 1:
				strFieldName = "ClaimNo";
				break;
			case 2:
				strFieldName = "AccidentDate";
				break;
			case 3:
				strFieldName = "EndcaseDate";
				break;
			case 4:
				strFieldName = "ClaimConclusionCode";
				break;
			case 5:
				strFieldName = "WarningIndi";
				break;
			case 6:
				strFieldName = "WainingDesc";
				break;
			case 7:
				strFieldName = "ModifyDate";
				break;
			case 8:
				strFieldName = "ModifyTime";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndcaseDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ClaimConclusionCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WarningIndi") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WainingDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
