/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHHmBespeakManageDB;

/*
 * <p>ClassName: LHHmBespeakManageSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-09-04
 */
public class LHHmBespeakManageSchema implements Schema, Cloneable {
    // @Field
    /** 服务任务编号 */
    private String ServTaskNo;
    /** 服务事件代码 */
    private String ServCaseCode;
    /** 服务任务代码 */
    private String ServTaskCode;
    /** 预约机构代码 */
    private String BespeakComID;
    /** 服务预约日期 */
    private Date ServBespeakDate;
    /** 服务预约时间 */
    private String ServBespeakTime;
    /** 体检套餐代码 */
    private String TestGrpCode;
    /** 结算方式 */
    private String BalanceManner;
    /** 服务预约描述 */
    private String ServBespeakDes;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHHmBespeakManageSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ServTaskNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHHmBespeakManageSchema cloned = (LHHmBespeakManageSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getServTaskNo() {
        return ServTaskNo;
    }

    public void setServTaskNo(String aServTaskNo) {
        ServTaskNo = aServTaskNo;
    }

    public String getServCaseCode() {
        return ServCaseCode;
    }

    public void setServCaseCode(String aServCaseCode) {
        ServCaseCode = aServCaseCode;
    }

    public String getServTaskCode() {
        return ServTaskCode;
    }

    public void setServTaskCode(String aServTaskCode) {
        ServTaskCode = aServTaskCode;
    }

    public String getBespeakComID() {
        return BespeakComID;
    }

    public void setBespeakComID(String aBespeakComID) {
        BespeakComID = aBespeakComID;
    }

    public String getServBespeakDate() {
        if (ServBespeakDate != null) {
            return fDate.getString(ServBespeakDate);
        } else {
            return null;
        }
    }

    public void setServBespeakDate(Date aServBespeakDate) {
        ServBespeakDate = aServBespeakDate;
    }

    public void setServBespeakDate(String aServBespeakDate) {
        if (aServBespeakDate != null && !aServBespeakDate.equals("")) {
            ServBespeakDate = fDate.getDate(aServBespeakDate);
        } else {
            ServBespeakDate = null;
        }
    }

    public String getServBespeakTime() {
        return ServBespeakTime;
    }

    public void setServBespeakTime(String aServBespeakTime) {
        ServBespeakTime = aServBespeakTime;
    }

    public String getTestGrpCode() {
        return TestGrpCode;
    }

    public void setTestGrpCode(String aTestGrpCode) {
        TestGrpCode = aTestGrpCode;
    }

    public String getBalanceManner() {
        return BalanceManner;
    }

    public void setBalanceManner(String aBalanceManner) {
        BalanceManner = aBalanceManner;
    }

    public String getServBespeakDes() {
        return ServBespeakDes;
    }

    public void setServBespeakDes(String aServBespeakDes) {
        ServBespeakDes = aServBespeakDes;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHHmBespeakManageSchema 对象给 Schema 赋值
     * @param: aLHHmBespeakManageSchema LHHmBespeakManageSchema
     **/
    public void setSchema(LHHmBespeakManageSchema aLHHmBespeakManageSchema) {
        this.ServTaskNo = aLHHmBespeakManageSchema.getServTaskNo();
        this.ServCaseCode = aLHHmBespeakManageSchema.getServCaseCode();
        this.ServTaskCode = aLHHmBespeakManageSchema.getServTaskCode();
        this.BespeakComID = aLHHmBespeakManageSchema.getBespeakComID();
        this.ServBespeakDate = fDate.getDate(aLHHmBespeakManageSchema.
                                             getServBespeakDate());
        this.ServBespeakTime = aLHHmBespeakManageSchema.getServBespeakTime();
        this.TestGrpCode = aLHHmBespeakManageSchema.getTestGrpCode();
        this.BalanceManner = aLHHmBespeakManageSchema.getBalanceManner();
        this.ServBespeakDes = aLHHmBespeakManageSchema.getServBespeakDes();
        this.ManageCom = aLHHmBespeakManageSchema.getManageCom();
        this.Operator = aLHHmBespeakManageSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHHmBespeakManageSchema.getMakeDate());
        this.MakeTime = aLHHmBespeakManageSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHHmBespeakManageSchema.getModifyDate());
        this.ModifyTime = aLHHmBespeakManageSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ServTaskNo") == null) {
                this.ServTaskNo = null;
            } else {
                this.ServTaskNo = rs.getString("ServTaskNo").trim();
            }

            if (rs.getString("ServCaseCode") == null) {
                this.ServCaseCode = null;
            } else {
                this.ServCaseCode = rs.getString("ServCaseCode").trim();
            }

            if (rs.getString("ServTaskCode") == null) {
                this.ServTaskCode = null;
            } else {
                this.ServTaskCode = rs.getString("ServTaskCode").trim();
            }

            if (rs.getString("BespeakComID") == null) {
                this.BespeakComID = null;
            } else {
                this.BespeakComID = rs.getString("BespeakComID").trim();
            }

            this.ServBespeakDate = rs.getDate("ServBespeakDate");
            if (rs.getString("ServBespeakTime") == null) {
                this.ServBespeakTime = null;
            } else {
                this.ServBespeakTime = rs.getString("ServBespeakTime").trim();
            }

            if (rs.getString("TestGrpCode") == null) {
                this.TestGrpCode = null;
            } else {
                this.TestGrpCode = rs.getString("TestGrpCode").trim();
            }

            if (rs.getString("BalanceManner") == null) {
                this.BalanceManner = null;
            } else {
                this.BalanceManner = rs.getString("BalanceManner").trim();
            }

            if (rs.getString("ServBespeakDes") == null) {
                this.ServBespeakDes = null;
            } else {
                this.ServBespeakDes = rs.getString("ServBespeakDes").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHHmBespeakManage表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHHmBespeakManageSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHHmBespeakManageSchema getSchema() {
        LHHmBespeakManageSchema aLHHmBespeakManageSchema = new
                LHHmBespeakManageSchema();
        aLHHmBespeakManageSchema.setSchema(this);
        return aLHHmBespeakManageSchema;
    }

    public LHHmBespeakManageDB getDB() {
        LHHmBespeakManageDB aDBOper = new LHHmBespeakManageDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHHmBespeakManage描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ServTaskNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeakComID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ServBespeakDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServBespeakTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestGrpCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceManner));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServBespeakDes));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHHmBespeakManage>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ServTaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            ServCaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ServTaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            BespeakComID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            ServBespeakDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            ServBespeakTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             6, SysConst.PACKAGESPILTER);
            TestGrpCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            BalanceManner = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            ServBespeakDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                            SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHHmBespeakManageSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ServTaskNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskNo));
        }
        if (FCode.equals("ServCaseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseCode));
        }
        if (FCode.equals("ServTaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskCode));
        }
        if (FCode.equals("BespeakComID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BespeakComID));
        }
        if (FCode.equals("ServBespeakDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getServBespeakDate()));
        }
        if (FCode.equals("ServBespeakTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServBespeakTime));
        }
        if (FCode.equals("TestGrpCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestGrpCode));
        }
        if (FCode.equals("BalanceManner")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceManner));
        }
        if (FCode.equals("ServBespeakDes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServBespeakDes));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ServTaskNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServCaseCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ServTaskCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(BespeakComID);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getServBespeakDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServBespeakTime);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(TestGrpCode);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(BalanceManner);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ServBespeakDes);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ServTaskNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskNo = FValue.trim();
            } else {
                ServTaskNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseCode = FValue.trim();
            } else {
                ServCaseCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskCode = FValue.trim();
            } else {
                ServTaskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakComID")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakComID = FValue.trim();
            } else {
                BespeakComID = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServBespeakDate")) {
            if (FValue != null && !FValue.equals("")) {
                ServBespeakDate = fDate.getDate(FValue);
            } else {
                ServBespeakDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServBespeakTime")) {
            if (FValue != null && !FValue.equals("")) {
                ServBespeakTime = FValue.trim();
            } else {
                ServBespeakTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestGrpCode")) {
            if (FValue != null && !FValue.equals("")) {
                TestGrpCode = FValue.trim();
            } else {
                TestGrpCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BalanceManner")) {
            if (FValue != null && !FValue.equals("")) {
                BalanceManner = FValue.trim();
            } else {
                BalanceManner = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServBespeakDes")) {
            if (FValue != null && !FValue.equals("")) {
                ServBespeakDes = FValue.trim();
            } else {
                ServBespeakDes = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHHmBespeakManageSchema other = (LHHmBespeakManageSchema) otherObject;
        return
                ServTaskNo.equals(other.getServTaskNo())
                && ServCaseCode.equals(other.getServCaseCode())
                && ServTaskCode.equals(other.getServTaskCode())
                && BespeakComID.equals(other.getBespeakComID())
                &&
                fDate.getString(ServBespeakDate).equals(other.getServBespeakDate())
                && ServBespeakTime.equals(other.getServBespeakTime())
                && TestGrpCode.equals(other.getTestGrpCode())
                && BalanceManner.equals(other.getBalanceManner())
                && ServBespeakDes.equals(other.getServBespeakDes())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ServTaskNo")) {
            return 0;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return 1;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return 2;
        }
        if (strFieldName.equals("BespeakComID")) {
            return 3;
        }
        if (strFieldName.equals("ServBespeakDate")) {
            return 4;
        }
        if (strFieldName.equals("ServBespeakTime")) {
            return 5;
        }
        if (strFieldName.equals("TestGrpCode")) {
            return 6;
        }
        if (strFieldName.equals("BalanceManner")) {
            return 7;
        }
        if (strFieldName.equals("ServBespeakDes")) {
            return 8;
        }
        if (strFieldName.equals("ManageCom")) {
            return 9;
        }
        if (strFieldName.equals("Operator")) {
            return 10;
        }
        if (strFieldName.equals("MakeDate")) {
            return 11;
        }
        if (strFieldName.equals("MakeTime")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ServTaskNo";
            break;
        case 1:
            strFieldName = "ServCaseCode";
            break;
        case 2:
            strFieldName = "ServTaskCode";
            break;
        case 3:
            strFieldName = "BespeakComID";
            break;
        case 4:
            strFieldName = "ServBespeakDate";
            break;
        case 5:
            strFieldName = "ServBespeakTime";
            break;
        case 6:
            strFieldName = "TestGrpCode";
            break;
        case 7:
            strFieldName = "BalanceManner";
            break;
        case 8:
            strFieldName = "ServBespeakDes";
            break;
        case 9:
            strFieldName = "ManageCom";
            break;
        case 10:
            strFieldName = "Operator";
            break;
        case 11:
            strFieldName = "MakeDate";
            break;
        case 12:
            strFieldName = "MakeTime";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ServTaskNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeakComID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServBespeakDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ServBespeakTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestGrpCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BalanceManner")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServBespeakDes")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
