/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LGWorkTraceDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LGWorkTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 作业轨迹表
 * @CreateDate：2005-05-17
 */
public class LGWorkTraceSchema implements Schema, Cloneable
{
    // @Field
    /** 作业编号 */
    private String WorkNo;
    /** 结点编号 */
    private String NodeNo;
    /** 信箱编号 */
    private String WorkBoxNo;
    /** 入箱途径编号 */
    private String InMethodNo;
    /** 入箱日期 */
    private Date InDate;
    /** 入箱时间 */
    private String InTime;
    /** 发件机构编号 */
    private String SendComNo;
    /** 发件人编号 */
    private String SendPersonNo;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LGWorkTraceSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "WorkNo";
        pk[1] = "NodeNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LGWorkTraceSchema cloned = (LGWorkTraceSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getWorkNo()
    {
        if (SysConst.CHANGECHARSET && WorkNo != null && !WorkNo.equals(""))
        {
            WorkNo = StrTool.unicodeToGBK(WorkNo);
        }
        return WorkNo;
    }

    public void setWorkNo(String aWorkNo)
    {
        WorkNo = aWorkNo;
    }

    public String getNodeNo()
    {
        if (SysConst.CHANGECHARSET && NodeNo != null && !NodeNo.equals(""))
        {
            NodeNo = StrTool.unicodeToGBK(NodeNo);
        }
        return NodeNo;
    }

    public void setNodeNo(String aNodeNo)
    {
        NodeNo = aNodeNo;
    }

    public String getWorkBoxNo()
    {
        if (SysConst.CHANGECHARSET && WorkBoxNo != null && !WorkBoxNo.equals(""))
        {
            WorkBoxNo = StrTool.unicodeToGBK(WorkBoxNo);
        }
        return WorkBoxNo;
    }

    public void setWorkBoxNo(String aWorkBoxNo)
    {
        WorkBoxNo = aWorkBoxNo;
    }

    public String getInMethodNo()
    {
        if (SysConst.CHANGECHARSET && InMethodNo != null &&
            !InMethodNo.equals(""))
        {
            InMethodNo = StrTool.unicodeToGBK(InMethodNo);
        }
        return InMethodNo;
    }

    public void setInMethodNo(String aInMethodNo)
    {
        InMethodNo = aInMethodNo;
    }

    public String getInDate()
    {
        if (InDate != null)
        {
            return fDate.getString(InDate);
        }
        else
        {
            return null;
        }
    }

    public void setInDate(Date aInDate)
    {
        InDate = aInDate;
    }

    public void setInDate(String aInDate)
    {
        if (aInDate != null && !aInDate.equals(""))
        {
            InDate = fDate.getDate(aInDate);
        }
        else
        {
            InDate = null;
        }
    }

    public String getInTime()
    {
        if (SysConst.CHANGECHARSET && InTime != null && !InTime.equals(""))
        {
            InTime = StrTool.unicodeToGBK(InTime);
        }
        return InTime;
    }

    public void setInTime(String aInTime)
    {
        InTime = aInTime;
    }

    public String getSendComNo()
    {
        if (SysConst.CHANGECHARSET && SendComNo != null && !SendComNo.equals(""))
        {
            SendComNo = StrTool.unicodeToGBK(SendComNo);
        }
        return SendComNo;
    }

    public void setSendComNo(String aSendComNo)
    {
        SendComNo = aSendComNo;
    }

    public String getSendPersonNo()
    {
        if (SysConst.CHANGECHARSET && SendPersonNo != null &&
            !SendPersonNo.equals(""))
        {
            SendPersonNo = StrTool.unicodeToGBK(SendPersonNo);
        }
        return SendPersonNo;
    }

    public void setSendPersonNo(String aSendPersonNo)
    {
        SendPersonNo = aSendPersonNo;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getRemark()
    {
        if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 LGWorkTraceSchema 对象给 Schema 赋值
     * @param: aLGWorkTraceSchema LGWorkTraceSchema
     **/
    public void setSchema(LGWorkTraceSchema aLGWorkTraceSchema)
    {
        this.WorkNo = aLGWorkTraceSchema.getWorkNo();
        this.NodeNo = aLGWorkTraceSchema.getNodeNo();
        this.WorkBoxNo = aLGWorkTraceSchema.getWorkBoxNo();
        this.InMethodNo = aLGWorkTraceSchema.getInMethodNo();
        this.InDate = fDate.getDate(aLGWorkTraceSchema.getInDate());
        this.InTime = aLGWorkTraceSchema.getInTime();
        this.SendComNo = aLGWorkTraceSchema.getSendComNo();
        this.SendPersonNo = aLGWorkTraceSchema.getSendPersonNo();
        this.Operator = aLGWorkTraceSchema.getOperator();
        this.MakeDate = fDate.getDate(aLGWorkTraceSchema.getMakeDate());
        this.MakeTime = aLGWorkTraceSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLGWorkTraceSchema.getModifyDate());
        this.ModifyTime = aLGWorkTraceSchema.getModifyTime();
        this.Remark = aLGWorkTraceSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("WorkNo") == null)
            {
                this.WorkNo = null;
            }
            else
            {
                this.WorkNo = rs.getString("WorkNo").trim();
            }

            if (rs.getString("NodeNo") == null)
            {
                this.NodeNo = null;
            }
            else
            {
                this.NodeNo = rs.getString("NodeNo").trim();
            }

            if (rs.getString("WorkBoxNo") == null)
            {
                this.WorkBoxNo = null;
            }
            else
            {
                this.WorkBoxNo = rs.getString("WorkBoxNo").trim();
            }

            if (rs.getString("InMethodNo") == null)
            {
                this.InMethodNo = null;
            }
            else
            {
                this.InMethodNo = rs.getString("InMethodNo").trim();
            }

            this.InDate = rs.getDate("InDate");
            if (rs.getString("InTime") == null)
            {
                this.InTime = null;
            }
            else
            {
                this.InTime = rs.getString("InTime").trim();
            }

            if (rs.getString("SendComNo") == null)
            {
                this.SendComNo = null;
            }
            else
            {
                this.SendComNo = rs.getString("SendComNo").trim();
            }

            if (rs.getString("SendPersonNo") == null)
            {
                this.SendPersonNo = null;
            }
            else
            {
                this.SendPersonNo = rs.getString("SendPersonNo").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LGWorkTrace表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGWorkTraceSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LGWorkTraceSchema getSchema()
    {
        LGWorkTraceSchema aLGWorkTraceSchema = new LGWorkTraceSchema();
        aLGWorkTraceSchema.setSchema(this);
        return aLGWorkTraceSchema;
    }

    public LGWorkTraceDB getDB()
    {
        LGWorkTraceDB aDBOper = new LGWorkTraceDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGWorkTrace描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WorkNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(NodeNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WorkBoxNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(InMethodNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                InDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(InTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SendComNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SendPersonNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Remark)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGWorkTrace>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            WorkNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            NodeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            WorkBoxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            InMethodNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            InDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            InTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            SendComNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            SendPersonNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGWorkTraceSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("WorkNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkNo));
        }
        if (FCode.equals("NodeNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NodeNo));
        }
        if (FCode.equals("WorkBoxNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkBoxNo));
        }
        if (FCode.equals("InMethodNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InMethodNo));
        }
        if (FCode.equals("InDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getInDate()));
        }
        if (FCode.equals("InTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InTime));
        }
        if (FCode.equals("SendComNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendComNo));
        }
        if (FCode.equals("SendPersonNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendPersonNo));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(WorkNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(NodeNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(WorkBoxNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InMethodNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(InTime);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(SendComNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(SendPersonNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("WorkNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkNo = FValue.trim();
            }
            else
            {
                WorkNo = null;
            }
        }
        if (FCode.equals("NodeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NodeNo = FValue.trim();
            }
            else
            {
                NodeNo = null;
            }
        }
        if (FCode.equals("WorkBoxNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkBoxNo = FValue.trim();
            }
            else
            {
                WorkBoxNo = null;
            }
        }
        if (FCode.equals("InMethodNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InMethodNo = FValue.trim();
            }
            else
            {
                InMethodNo = null;
            }
        }
        if (FCode.equals("InDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InDate = fDate.getDate(FValue);
            }
            else
            {
                InDate = null;
            }
        }
        if (FCode.equals("InTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InTime = FValue.trim();
            }
            else
            {
                InTime = null;
            }
        }
        if (FCode.equals("SendComNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SendComNo = FValue.trim();
            }
            else
            {
                SendComNo = null;
            }
        }
        if (FCode.equals("SendPersonNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SendPersonNo = FValue.trim();
            }
            else
            {
                SendPersonNo = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LGWorkTraceSchema other = (LGWorkTraceSchema) otherObject;
        return
                WorkNo.equals(other.getWorkNo())
                && NodeNo.equals(other.getNodeNo())
                && WorkBoxNo.equals(other.getWorkBoxNo())
                && InMethodNo.equals(other.getInMethodNo())
                && fDate.getString(InDate).equals(other.getInDate())
                && InTime.equals(other.getInTime())
                && SendComNo.equals(other.getSendComNo())
                && SendPersonNo.equals(other.getSendPersonNo())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("WorkNo"))
        {
            return 0;
        }
        if (strFieldName.equals("NodeNo"))
        {
            return 1;
        }
        if (strFieldName.equals("WorkBoxNo"))
        {
            return 2;
        }
        if (strFieldName.equals("InMethodNo"))
        {
            return 3;
        }
        if (strFieldName.equals("InDate"))
        {
            return 4;
        }
        if (strFieldName.equals("InTime"))
        {
            return 5;
        }
        if (strFieldName.equals("SendComNo"))
        {
            return 6;
        }
        if (strFieldName.equals("SendPersonNo"))
        {
            return 7;
        }
        if (strFieldName.equals("Operator"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 11;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 12;
        }
        if (strFieldName.equals("Remark"))
        {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "WorkNo";
                break;
            case 1:
                strFieldName = "NodeNo";
                break;
            case 2:
                strFieldName = "WorkBoxNo";
                break;
            case 3:
                strFieldName = "InMethodNo";
                break;
            case 4:
                strFieldName = "InDate";
                break;
            case 5:
                strFieldName = "InTime";
                break;
            case 6:
                strFieldName = "SendComNo";
                break;
            case 7:
                strFieldName = "SendPersonNo";
                break;
            case 8:
                strFieldName = "Operator";
                break;
            case 9:
                strFieldName = "MakeDate";
                break;
            case 10:
                strFieldName = "MakeTime";
                break;
            case 11:
                strFieldName = "ModifyDate";
                break;
            case 12:
                strFieldName = "ModifyTime";
                break;
            case 13:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("WorkNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NodeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WorkBoxNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InMethodNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SendComNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SendPersonNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
