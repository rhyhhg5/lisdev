/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHScanInfoDB;

/*
 * <p>ClassName: LHScanInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康管理-扫描件信息表-2006-01-10
 * @CreateDate：2006-01-18
 */
public class LHScanInfoSchema implements Schema, Cloneable {
    // @Field
    /** 扫描件流水号 */
    private String SerialNo;
    /** 其他号码 */
    private String OtherNo;
    /** 其他号码类型 */
    private String OtherNoType;
    /** 扫描日期 */
    private Date ScanDate;
    /** 扫描时间 */
    private String ScanTime;
    /** 上传类型 */
    private String ScanState;
    /** 录入状态 */
    private String InsertState;
    /** 备注 */
    private String Comment;
    /** 说明 */
    private String Content;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 批次号 */
    private String BatchNo;
    /** 关联号1 */
    private String RelationNo;
    /** 关联号2 */
    private String RelationNo2;
    /** 关联类型 */
    private String RelationType;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHScanInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHScanInfoSchema cloned = (LHScanInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getOtherNo() {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType() {
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }

    public String getScanDate() {
        if (ScanDate != null) {
            return fDate.getString(ScanDate);
        } else {
            return null;
        }
    }

    public void setScanDate(Date aScanDate) {
        ScanDate = aScanDate;
    }

    public void setScanDate(String aScanDate) {
        if (aScanDate != null && !aScanDate.equals("")) {
            ScanDate = fDate.getDate(aScanDate);
        } else {
            ScanDate = null;
        }
    }

    public String getScanTime() {
        return ScanTime;
    }

    public void setScanTime(String aScanTime) {
        ScanTime = aScanTime;
    }

    public String getScanState() {
        return ScanState;
    }

    public void setScanState(String aScanState) {
        ScanState = aScanState;
    }

    public String getInsertState() {
        return InsertState;
    }

    public void setInsertState(String aInsertState) {
        InsertState = aInsertState;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String aComment) {
        Comment = aComment;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String aContent) {
        Content = aContent;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo) {
        BatchNo = aBatchNo;
    }

    public String getRelationNo() {
        return RelationNo;
    }

    public void setRelationNo(String aRelationNo) {
        RelationNo = aRelationNo;
    }

    public String getRelationNo2() {
        return RelationNo2;
    }

    public void setRelationNo2(String aRelationNo2) {
        RelationNo2 = aRelationNo2;
    }

    public String getRelationType() {
        return RelationType;
    }

    public void setRelationType(String aRelationType) {
        RelationType = aRelationType;
    }

    /**
     * 使用另外一个 LHScanInfoSchema 对象给 Schema 赋值
     * @param: aLHScanInfoSchema LHScanInfoSchema
     **/
    public void setSchema(LHScanInfoSchema aLHScanInfoSchema) {
        this.SerialNo = aLHScanInfoSchema.getSerialNo();
        this.OtherNo = aLHScanInfoSchema.getOtherNo();
        this.OtherNoType = aLHScanInfoSchema.getOtherNoType();
        this.ScanDate = fDate.getDate(aLHScanInfoSchema.getScanDate());
        this.ScanTime = aLHScanInfoSchema.getScanTime();
        this.ScanState = aLHScanInfoSchema.getScanState();
        this.InsertState = aLHScanInfoSchema.getInsertState();
        this.Comment = aLHScanInfoSchema.getComment();
        this.Content = aLHScanInfoSchema.getContent();
        this.ManageCom = aLHScanInfoSchema.getManageCom();
        this.Operator = aLHScanInfoSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHScanInfoSchema.getMakeDate());
        this.MakeTime = aLHScanInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHScanInfoSchema.getModifyDate());
        this.ModifyTime = aLHScanInfoSchema.getModifyTime();
        this.BatchNo = aLHScanInfoSchema.getBatchNo();
        this.RelationNo = aLHScanInfoSchema.getRelationNo();
        this.RelationNo2 = aLHScanInfoSchema.getRelationNo2();
        this.RelationType = aLHScanInfoSchema.getRelationType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("OtherNo") == null) {
                this.OtherNo = null;
            } else {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null) {
                this.OtherNoType = null;
            } else {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            this.ScanDate = rs.getDate("ScanDate");
            if (rs.getString("ScanTime") == null) {
                this.ScanTime = null;
            } else {
                this.ScanTime = rs.getString("ScanTime").trim();
            }

            if (rs.getString("ScanState") == null) {
                this.ScanState = null;
            } else {
                this.ScanState = rs.getString("ScanState").trim();
            }

            if (rs.getString("InsertState") == null) {
                this.InsertState = null;
            } else {
                this.InsertState = rs.getString("InsertState").trim();
            }

            if (rs.getString("Comment") == null) {
                this.Comment = null;
            } else {
                this.Comment = rs.getString("Comment").trim();
            }

            if (rs.getString("Content") == null) {
                this.Content = null;
            } else {
                this.Content = rs.getString("Content").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BatchNo") == null) {
                this.BatchNo = null;
            } else {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            if (rs.getString("RelationNo") == null) {
                this.RelationNo = null;
            } else {
                this.RelationNo = rs.getString("RelationNo").trim();
            }

            if (rs.getString("RelationNo2") == null) {
                this.RelationNo2 = null;
            } else {
                this.RelationNo2 = rs.getString("RelationNo2").trim();
            }

            if (rs.getString("RelationType") == null) {
                this.RelationType = null;
            } else {
                this.RelationType = rs.getString("RelationType").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHScanInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHScanInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHScanInfoSchema getSchema() {
        LHScanInfoSchema aLHScanInfoSchema = new LHScanInfoSchema();
        aLHScanInfoSchema.setSchema(this);
        return aLHScanInfoSchema;
    }

    public LHScanInfoDB getDB() {
        LHScanInfoDB aDBOper = new LHScanInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHScanInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ScanDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ScanTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ScanState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsertState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Comment));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Content));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BatchNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationNo2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationType));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHScanInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            ScanDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            ScanTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            ScanState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            InsertState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            Comment = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            Content = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                     SysConst.PACKAGESPILTER);
            RelationNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            RelationNo2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                         SysConst.PACKAGESPILTER);
            RelationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                          SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHScanInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equals("ScanDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getScanDate()));
        }
        if (FCode.equals("ScanTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanTime));
        }
        if (FCode.equals("ScanState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanState));
        }
        if (FCode.equals("InsertState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsertState));
        }
        if (FCode.equals("Comment")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Comment));
        }
        if (FCode.equals("Content")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Content));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
        }
        if (FCode.equals("RelationNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationNo));
        }
        if (FCode.equals("RelationNo2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationNo2));
        }
        if (FCode.equals("RelationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(OtherNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(OtherNoType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getScanDate()));
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ScanTime);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ScanState);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(InsertState);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Comment);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Content);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(BatchNo);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(RelationNo);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(RelationNo2);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(RelationType);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if (FValue != null && !FValue.equals("")) {
                OtherNo = FValue.trim();
            } else {
                OtherNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if (FValue != null && !FValue.equals("")) {
                OtherNoType = FValue.trim();
            } else {
                OtherNoType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ScanDate")) {
            if (FValue != null && !FValue.equals("")) {
                ScanDate = fDate.getDate(FValue);
            } else {
                ScanDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ScanTime")) {
            if (FValue != null && !FValue.equals("")) {
                ScanTime = FValue.trim();
            } else {
                ScanTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ScanState")) {
            if (FValue != null && !FValue.equals("")) {
                ScanState = FValue.trim();
            } else {
                ScanState = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsertState")) {
            if (FValue != null && !FValue.equals("")) {
                InsertState = FValue.trim();
            } else {
                InsertState = null;
            }
        }
        if (FCode.equalsIgnoreCase("Comment")) {
            if (FValue != null && !FValue.equals("")) {
                Comment = FValue.trim();
            } else {
                Comment = null;
            }
        }
        if (FCode.equalsIgnoreCase("Content")) {
            if (FValue != null && !FValue.equals("")) {
                Content = FValue.trim();
            } else {
                Content = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            if (FValue != null && !FValue.equals("")) {
                BatchNo = FValue.trim();
            } else {
                BatchNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RelationNo")) {
            if (FValue != null && !FValue.equals("")) {
                RelationNo = FValue.trim();
            } else {
                RelationNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RelationNo2")) {
            if (FValue != null && !FValue.equals("")) {
                RelationNo2 = FValue.trim();
            } else {
                RelationNo2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("RelationType")) {
            if (FValue != null && !FValue.equals("")) {
                RelationType = FValue.trim();
            } else {
                RelationType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHScanInfoSchema other = (LHScanInfoSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && fDate.getString(ScanDate).equals(other.getScanDate())
                && ScanTime.equals(other.getScanTime())
                && ScanState.equals(other.getScanState())
                && InsertState.equals(other.getInsertState())
                && Comment.equals(other.getComment())
                && Content.equals(other.getContent())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BatchNo.equals(other.getBatchNo())
                && RelationNo.equals(other.getRelationNo())
                && RelationNo2.equals(other.getRelationNo2())
                && RelationType.equals(other.getRelationType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("OtherNo")) {
            return 1;
        }
        if (strFieldName.equals("OtherNoType")) {
            return 2;
        }
        if (strFieldName.equals("ScanDate")) {
            return 3;
        }
        if (strFieldName.equals("ScanTime")) {
            return 4;
        }
        if (strFieldName.equals("ScanState")) {
            return 5;
        }
        if (strFieldName.equals("InsertState")) {
            return 6;
        }
        if (strFieldName.equals("Comment")) {
            return 7;
        }
        if (strFieldName.equals("Content")) {
            return 8;
        }
        if (strFieldName.equals("ManageCom")) {
            return 9;
        }
        if (strFieldName.equals("Operator")) {
            return 10;
        }
        if (strFieldName.equals("MakeDate")) {
            return 11;
        }
        if (strFieldName.equals("MakeTime")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        if (strFieldName.equals("BatchNo")) {
            return 15;
        }
        if (strFieldName.equals("RelationNo")) {
            return 16;
        }
        if (strFieldName.equals("RelationNo2")) {
            return 17;
        }
        if (strFieldName.equals("RelationType")) {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "OtherNo";
            break;
        case 2:
            strFieldName = "OtherNoType";
            break;
        case 3:
            strFieldName = "ScanDate";
            break;
        case 4:
            strFieldName = "ScanTime";
            break;
        case 5:
            strFieldName = "ScanState";
            break;
        case 6:
            strFieldName = "InsertState";
            break;
        case 7:
            strFieldName = "Comment";
            break;
        case 8:
            strFieldName = "Content";
            break;
        case 9:
            strFieldName = "ManageCom";
            break;
        case 10:
            strFieldName = "Operator";
            break;
        case 11:
            strFieldName = "MakeDate";
            break;
        case 12:
            strFieldName = "MakeTime";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        case 15:
            strFieldName = "BatchNo";
            break;
        case 16:
            strFieldName = "RelationNo";
            break;
        case 17:
            strFieldName = "RelationNo2";
            break;
        case 18:
            strFieldName = "RelationType";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ScanDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ScanTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ScanState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsertState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Comment")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Content")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BatchNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelationNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelationNo2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelationType")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
