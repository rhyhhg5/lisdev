/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LARewardPunishDB;

/*
 * <p>ClassName: LARewardPunishSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2011-03-16
 */
public class LARewardPunishSchema implements Schema, Cloneable
{
	// @Field
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人展业机构代码 */
	private String AgentGroup;
	/** 管理机构 */
	private String ManageCom;
	/** 纪录顺序号 */
	private int Idx;
	/** 金额 */
	private double Money;
	/** 奖惩项目 */
	private String AClass;
	/** 审批状态 */
	private String SendGrp;
	/** 奖励称号 */
	private String AwardTitle;
	/** 惩罚原因 */
	private String PunishRsn;
	/** 执行日期 */
	private Date DoneDate;
	/** 批注 */
	private String Noti;
	/** 处理标志 */
	private String DoneFlag;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 展业类型 */
	private String BranchType;
	/** 展业机构外部编码 */
	private String BranchAttr;
	/** 违规类型 */
	private String ViolationType;
	/** 处罚类型 */
	private String PenaltyType;
	/** 渠道 */
	private String BranchType2;
	/** 迟到早退次数 */
	private int LateCount;
	/** 病假次数 */
	private int SickCount;
	/** 事假次数 */
	private int AffairCount;
	/** 旷工次数 */
	private int AbsentCount;
	/** 薪资年月 */
	private String WageNo;
	/** 审批人1 */
	private String Check1;
	/** 审批人2 */
	private String Check2;
	/** 审批人3 */
	private String Check3;
	/** 机构编码 */
	private String AgentCom;

	public static final int FIELDNUM = 31;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LARewardPunishSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "AgentCode";
		pk[1] = "Idx";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LARewardPunishSchema cloned = (LARewardPunishSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public int getIdx()
	{
		return Idx;
	}
	public void setIdx(int aIdx)
	{
		Idx = aIdx;
	}
	public void setIdx(String aIdx)
	{
		if (aIdx != null && !aIdx.equals(""))
		{
			Integer tInteger = new Integer(aIdx);
			int i = tInteger.intValue();
			Idx = i;
		}
	}

	public double getMoney()
	{
		return Money;
	}
	public void setMoney(double aMoney)
	{
		Money = Arith.round(aMoney,2);
	}
	public void setMoney(String aMoney)
	{
		if (aMoney != null && !aMoney.equals(""))
		{
			Double tDouble = new Double(aMoney);
			double d = tDouble.doubleValue();
                Money = Arith.round(d,2);
		}
	}

	public String getAClass()
	{
		return AClass;
	}
	public void setAClass(String aAClass)
	{
		AClass = aAClass;
	}
	public String getSendGrp()
	{
		return SendGrp;
	}
	public void setSendGrp(String aSendGrp)
	{
		SendGrp = aSendGrp;
	}
	public String getAwardTitle()
	{
		return AwardTitle;
	}
	public void setAwardTitle(String aAwardTitle)
	{
		AwardTitle = aAwardTitle;
	}
	public String getPunishRsn()
	{
		return PunishRsn;
	}
	public void setPunishRsn(String aPunishRsn)
	{
		PunishRsn = aPunishRsn;
	}
	public String getDoneDate()
	{
		if( DoneDate != null )
			return fDate.getString(DoneDate);
		else
			return null;
	}
	public void setDoneDate(Date aDoneDate)
	{
		DoneDate = aDoneDate;
	}
	public void setDoneDate(String aDoneDate)
	{
		if (aDoneDate != null && !aDoneDate.equals("") )
		{
			DoneDate = fDate.getDate( aDoneDate );
		}
		else
			DoneDate = null;
	}

	public String getNoti()
	{
		return Noti;
	}
	public void setNoti(String aNoti)
	{
		Noti = aNoti;
	}
	public String getDoneFlag()
	{
		return DoneFlag;
	}
	public void setDoneFlag(String aDoneFlag)
	{
		DoneFlag = aDoneFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchAttr()
	{
		return BranchAttr;
	}
	public void setBranchAttr(String aBranchAttr)
	{
		BranchAttr = aBranchAttr;
	}
	public String getViolationType()
	{
		return ViolationType;
	}
	public void setViolationType(String aViolationType)
	{
		ViolationType = aViolationType;
	}
	public String getPenaltyType()
	{
		return PenaltyType;
	}
	public void setPenaltyType(String aPenaltyType)
	{
		PenaltyType = aPenaltyType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public int getLateCount()
	{
		return LateCount;
	}
	public void setLateCount(int aLateCount)
	{
		LateCount = aLateCount;
	}
	public void setLateCount(String aLateCount)
	{
		if (aLateCount != null && !aLateCount.equals(""))
		{
			Integer tInteger = new Integer(aLateCount);
			int i = tInteger.intValue();
			LateCount = i;
		}
	}

	public int getSickCount()
	{
		return SickCount;
	}
	public void setSickCount(int aSickCount)
	{
		SickCount = aSickCount;
	}
	public void setSickCount(String aSickCount)
	{
		if (aSickCount != null && !aSickCount.equals(""))
		{
			Integer tInteger = new Integer(aSickCount);
			int i = tInteger.intValue();
			SickCount = i;
		}
	}

	public int getAffairCount()
	{
		return AffairCount;
	}
	public void setAffairCount(int aAffairCount)
	{
		AffairCount = aAffairCount;
	}
	public void setAffairCount(String aAffairCount)
	{
		if (aAffairCount != null && !aAffairCount.equals(""))
		{
			Integer tInteger = new Integer(aAffairCount);
			int i = tInteger.intValue();
			AffairCount = i;
		}
	}

	public int getAbsentCount()
	{
		return AbsentCount;
	}
	public void setAbsentCount(int aAbsentCount)
	{
		AbsentCount = aAbsentCount;
	}
	public void setAbsentCount(String aAbsentCount)
	{
		if (aAbsentCount != null && !aAbsentCount.equals(""))
		{
			Integer tInteger = new Integer(aAbsentCount);
			int i = tInteger.intValue();
			AbsentCount = i;
		}
	}

	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getCheck1()
	{
		return Check1;
	}
	public void setCheck1(String aCheck1)
	{
		Check1 = aCheck1;
	}
	public String getCheck2()
	{
		return Check2;
	}
	public void setCheck2(String aCheck2)
	{
		Check2 = aCheck2;
	}
	public String getCheck3()
	{
		return Check3;
	}
	public void setCheck3(String aCheck3)
	{
		Check3 = aCheck3;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}

	/**
	* 使用另外一个 LARewardPunishSchema 对象给 Schema 赋值
	* @param: aLARewardPunishSchema LARewardPunishSchema
	**/
	public void setSchema(LARewardPunishSchema aLARewardPunishSchema)
	{
		this.AgentCode = aLARewardPunishSchema.getAgentCode();
		this.AgentGroup = aLARewardPunishSchema.getAgentGroup();
		this.ManageCom = aLARewardPunishSchema.getManageCom();
		this.Idx = aLARewardPunishSchema.getIdx();
		this.Money = aLARewardPunishSchema.getMoney();
		this.AClass = aLARewardPunishSchema.getAClass();
		this.SendGrp = aLARewardPunishSchema.getSendGrp();
		this.AwardTitle = aLARewardPunishSchema.getAwardTitle();
		this.PunishRsn = aLARewardPunishSchema.getPunishRsn();
		this.DoneDate = fDate.getDate( aLARewardPunishSchema.getDoneDate());
		this.Noti = aLARewardPunishSchema.getNoti();
		this.DoneFlag = aLARewardPunishSchema.getDoneFlag();
		this.Operator = aLARewardPunishSchema.getOperator();
		this.MakeDate = fDate.getDate( aLARewardPunishSchema.getMakeDate());
		this.MakeTime = aLARewardPunishSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLARewardPunishSchema.getModifyDate());
		this.ModifyTime = aLARewardPunishSchema.getModifyTime();
		this.BranchType = aLARewardPunishSchema.getBranchType();
		this.BranchAttr = aLARewardPunishSchema.getBranchAttr();
		this.ViolationType = aLARewardPunishSchema.getViolationType();
		this.PenaltyType = aLARewardPunishSchema.getPenaltyType();
		this.BranchType2 = aLARewardPunishSchema.getBranchType2();
		this.LateCount = aLARewardPunishSchema.getLateCount();
		this.SickCount = aLARewardPunishSchema.getSickCount();
		this.AffairCount = aLARewardPunishSchema.getAffairCount();
		this.AbsentCount = aLARewardPunishSchema.getAbsentCount();
		this.WageNo = aLARewardPunishSchema.getWageNo();
		this.Check1 = aLARewardPunishSchema.getCheck1();
		this.Check2 = aLARewardPunishSchema.getCheck2();
		this.Check3 = aLARewardPunishSchema.getCheck3();
		this.AgentCom = aLARewardPunishSchema.getAgentCom();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.Idx = rs.getInt("Idx");
			this.Money = rs.getDouble("Money");
			if( rs.getString("AClass") == null )
				this.AClass = null;
			else
				this.AClass = rs.getString("AClass").trim();

			if( rs.getString("SendGrp") == null )
				this.SendGrp = null;
			else
				this.SendGrp = rs.getString("SendGrp").trim();

			if( rs.getString("AwardTitle") == null )
				this.AwardTitle = null;
			else
				this.AwardTitle = rs.getString("AwardTitle").trim();

			if( rs.getString("PunishRsn") == null )
				this.PunishRsn = null;
			else
				this.PunishRsn = rs.getString("PunishRsn").trim();

			this.DoneDate = rs.getDate("DoneDate");
			if( rs.getString("Noti") == null )
				this.Noti = null;
			else
				this.Noti = rs.getString("Noti").trim();

			if( rs.getString("DoneFlag") == null )
				this.DoneFlag = null;
			else
				this.DoneFlag = rs.getString("DoneFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchAttr") == null )
				this.BranchAttr = null;
			else
				this.BranchAttr = rs.getString("BranchAttr").trim();

			if( rs.getString("ViolationType") == null )
				this.ViolationType = null;
			else
				this.ViolationType = rs.getString("ViolationType").trim();

			if( rs.getString("PenaltyType") == null )
				this.PenaltyType = null;
			else
				this.PenaltyType = rs.getString("PenaltyType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			this.LateCount = rs.getInt("LateCount");
			this.SickCount = rs.getInt("SickCount");
			this.AffairCount = rs.getInt("AffairCount");
			this.AbsentCount = rs.getInt("AbsentCount");
			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			if( rs.getString("Check1") == null )
				this.Check1 = null;
			else
				this.Check1 = rs.getString("Check1").trim();

			if( rs.getString("Check2") == null )
				this.Check2 = null;
			else
				this.Check2 = rs.getString("Check2").trim();

			if( rs.getString("Check3") == null )
				this.Check3 = null;
			else
				this.Check3 = rs.getString("Check3").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LARewardPunish表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LARewardPunishSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LARewardPunishSchema getSchema()
	{
		LARewardPunishSchema aLARewardPunishSchema = new LARewardPunishSchema();
		aLARewardPunishSchema.setSchema(this);
		return aLARewardPunishSchema;
	}

	public LARewardPunishDB getDB()
	{
		LARewardPunishDB aDBOper = new LARewardPunishDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARewardPunish描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Idx));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Money));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendGrp)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AwardTitle)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PunishRsn)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DoneDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Noti)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DoneFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchAttr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ViolationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PenaltyType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LateCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SickCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AffairCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AbsentCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Check1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Check2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Check3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARewardPunish>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Idx= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			Money = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			AClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SendGrp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AwardTitle = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PunishRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			DoneDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			DoneFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ViolationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PenaltyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			LateCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).intValue();
			SickCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).intValue();
			AffairCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).intValue();
			AbsentCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).intValue();
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Check1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Check2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Check3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LARewardPunishSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Idx"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
		}
		if (FCode.equals("Money"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Money));
		}
		if (FCode.equals("AClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AClass));
		}
		if (FCode.equals("SendGrp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendGrp));
		}
		if (FCode.equals("AwardTitle"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AwardTitle));
		}
		if (FCode.equals("PunishRsn"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PunishRsn));
		}
		if (FCode.equals("DoneDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDoneDate()));
		}
		if (FCode.equals("Noti"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
		}
		if (FCode.equals("DoneFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoneFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchAttr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
		}
		if (FCode.equals("ViolationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ViolationType));
		}
		if (FCode.equals("PenaltyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PenaltyType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("LateCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LateCount));
		}
		if (FCode.equals("SickCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SickCount));
		}
		if (FCode.equals("AffairCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AffairCount));
		}
		if (FCode.equals("AbsentCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AbsentCount));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("Check1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Check1));
		}
		if (FCode.equals("Check2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Check2));
		}
		if (FCode.equals("Check3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Check3));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = String.valueOf(Idx);
				break;
			case 4:
				strFieldValue = String.valueOf(Money);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AClass);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SendGrp);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AwardTitle);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PunishRsn);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDoneDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Noti);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(DoneFlag);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BranchAttr);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ViolationType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PenaltyType);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 22:
				strFieldValue = String.valueOf(LateCount);
				break;
			case 23:
				strFieldValue = String.valueOf(SickCount);
				break;
			case 24:
				strFieldValue = String.valueOf(AffairCount);
				break;
			case 25:
				strFieldValue = String.valueOf(AbsentCount);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Check1);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Check2);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Check3);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Idx"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Idx = i;
			}
		}
		if (FCode.equalsIgnoreCase("Money"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Money = d;
			}
		}
		if (FCode.equalsIgnoreCase("AClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AClass = FValue.trim();
			}
			else
				AClass = null;
		}
		if (FCode.equalsIgnoreCase("SendGrp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendGrp = FValue.trim();
			}
			else
				SendGrp = null;
		}
		if (FCode.equalsIgnoreCase("AwardTitle"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AwardTitle = FValue.trim();
			}
			else
				AwardTitle = null;
		}
		if (FCode.equalsIgnoreCase("PunishRsn"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PunishRsn = FValue.trim();
			}
			else
				PunishRsn = null;
		}
		if (FCode.equalsIgnoreCase("DoneDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DoneDate = fDate.getDate( FValue );
			}
			else
				DoneDate = null;
		}
		if (FCode.equalsIgnoreCase("Noti"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Noti = FValue.trim();
			}
			else
				Noti = null;
		}
		if (FCode.equalsIgnoreCase("DoneFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoneFlag = FValue.trim();
			}
			else
				DoneFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchAttr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchAttr = FValue.trim();
			}
			else
				BranchAttr = null;
		}
		if (FCode.equalsIgnoreCase("ViolationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ViolationType = FValue.trim();
			}
			else
				ViolationType = null;
		}
		if (FCode.equalsIgnoreCase("PenaltyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PenaltyType = FValue.trim();
			}
			else
				PenaltyType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("LateCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				LateCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("SickCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SickCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("AffairCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AffairCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("AbsentCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AbsentCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("Check1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Check1 = FValue.trim();
			}
			else
				Check1 = null;
		}
		if (FCode.equalsIgnoreCase("Check2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Check2 = FValue.trim();
			}
			else
				Check2 = null;
		}
		if (FCode.equalsIgnoreCase("Check3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Check3 = FValue.trim();
			}
			else
				Check3 = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LARewardPunishSchema other = (LARewardPunishSchema)otherObject;
		return
			(AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& Idx == other.getIdx()
			&& Money == other.getMoney()
			&& (AClass == null ? other.getAClass() == null : AClass.equals(other.getAClass()))
			&& (SendGrp == null ? other.getSendGrp() == null : SendGrp.equals(other.getSendGrp()))
			&& (AwardTitle == null ? other.getAwardTitle() == null : AwardTitle.equals(other.getAwardTitle()))
			&& (PunishRsn == null ? other.getPunishRsn() == null : PunishRsn.equals(other.getPunishRsn()))
			&& (DoneDate == null ? other.getDoneDate() == null : fDate.getString(DoneDate).equals(other.getDoneDate()))
			&& (Noti == null ? other.getNoti() == null : Noti.equals(other.getNoti()))
			&& (DoneFlag == null ? other.getDoneFlag() == null : DoneFlag.equals(other.getDoneFlag()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchAttr == null ? other.getBranchAttr() == null : BranchAttr.equals(other.getBranchAttr()))
			&& (ViolationType == null ? other.getViolationType() == null : ViolationType.equals(other.getViolationType()))
			&& (PenaltyType == null ? other.getPenaltyType() == null : PenaltyType.equals(other.getPenaltyType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& LateCount == other.getLateCount()
			&& SickCount == other.getSickCount()
			&& AffairCount == other.getAffairCount()
			&& AbsentCount == other.getAbsentCount()
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (Check1 == null ? other.getCheck1() == null : Check1.equals(other.getCheck1()))
			&& (Check2 == null ? other.getCheck2() == null : Check2.equals(other.getCheck2()))
			&& (Check3 == null ? other.getCheck3() == null : Check3.equals(other.getCheck3()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return 0;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("Idx") ) {
			return 3;
		}
		if( strFieldName.equals("Money") ) {
			return 4;
		}
		if( strFieldName.equals("AClass") ) {
			return 5;
		}
		if( strFieldName.equals("SendGrp") ) {
			return 6;
		}
		if( strFieldName.equals("AwardTitle") ) {
			return 7;
		}
		if( strFieldName.equals("PunishRsn") ) {
			return 8;
		}
		if( strFieldName.equals("DoneDate") ) {
			return 9;
		}
		if( strFieldName.equals("Noti") ) {
			return 10;
		}
		if( strFieldName.equals("DoneFlag") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		if( strFieldName.equals("BranchType") ) {
			return 17;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return 18;
		}
		if( strFieldName.equals("ViolationType") ) {
			return 19;
		}
		if( strFieldName.equals("PenaltyType") ) {
			return 20;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 21;
		}
		if( strFieldName.equals("LateCount") ) {
			return 22;
		}
		if( strFieldName.equals("SickCount") ) {
			return 23;
		}
		if( strFieldName.equals("AffairCount") ) {
			return 24;
		}
		if( strFieldName.equals("AbsentCount") ) {
			return 25;
		}
		if( strFieldName.equals("WageNo") ) {
			return 26;
		}
		if( strFieldName.equals("Check1") ) {
			return 27;
		}
		if( strFieldName.equals("Check2") ) {
			return 28;
		}
		if( strFieldName.equals("Check3") ) {
			return 29;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 30;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AgentCode";
				break;
			case 1:
				strFieldName = "AgentGroup";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "Idx";
				break;
			case 4:
				strFieldName = "Money";
				break;
			case 5:
				strFieldName = "AClass";
				break;
			case 6:
				strFieldName = "SendGrp";
				break;
			case 7:
				strFieldName = "AwardTitle";
				break;
			case 8:
				strFieldName = "PunishRsn";
				break;
			case 9:
				strFieldName = "DoneDate";
				break;
			case 10:
				strFieldName = "Noti";
				break;
			case 11:
				strFieldName = "DoneFlag";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			case 17:
				strFieldName = "BranchType";
				break;
			case 18:
				strFieldName = "BranchAttr";
				break;
			case 19:
				strFieldName = "ViolationType";
				break;
			case 20:
				strFieldName = "PenaltyType";
				break;
			case 21:
				strFieldName = "BranchType2";
				break;
			case 22:
				strFieldName = "LateCount";
				break;
			case 23:
				strFieldName = "SickCount";
				break;
			case 24:
				strFieldName = "AffairCount";
				break;
			case 25:
				strFieldName = "AbsentCount";
				break;
			case 26:
				strFieldName = "WageNo";
				break;
			case 27:
				strFieldName = "Check1";
				break;
			case 28:
				strFieldName = "Check2";
				break;
			case 29:
				strFieldName = "Check3";
				break;
			case 30:
				strFieldName = "AgentCom";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Idx") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Money") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendGrp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AwardTitle") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PunishRsn") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoneDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Noti") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoneFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ViolationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PenaltyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LateCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SickCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AffairCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AbsentCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Check1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Check2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Check3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_INT;
				break;
			case 23:
				nFieldType = Schema.TYPE_INT;
				break;
			case 24:
				nFieldType = Schema.TYPE_INT;
				break;
			case 25:
				nFieldType = Schema.TYPE_INT;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
