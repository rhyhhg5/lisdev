/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPInsureAccClassDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LPInsureAccClassSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-23
 */
public class LPInsureAccClassSchema implements Schema
{
    // @Field
    /** 批单号 */
    private String EdorNo;
    /** 批改类型 */
    private String EdorType;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 合同号码 */
    private String ContNo;
    /** 管理机构 */
    private String ManageCom;
    /** 保单险种号码 */
    private String PolNo;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 对应其它号码 */
    private String OtherNo;
    /** 对应其它号码类型 */
    private String OtherType;
    /** 账户归属属性 */
    private String AccAscription;
    /** 险种编码 */
    private String RiskCode;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 账户类型 */
    private String AccType;
    /** 账户结算方式 */
    private String AccComputeFlag;
    /** 账户成立日期 */
    private Date AccFoundDate;
    /** 账户成立时间 */
    private String AccFoundTime;
    /** 结算日期 */
    private Date BalaDate;
    /** 结算时间 */
    private String BalaTime;
    /** 累计交费 */
    private double SumPay;
    /** 累计领取 */
    private double SumPaym;
    /** 期初账户现金余额 */
    private double LastAccBala;
    /** 期初账户单位数 */
    private double LastUnitCount;
    /** 期初账户单位价格 */
    private double LastUnitPrice;
    /** 保险帐户现金余额 */
    private double InsuAccBala;
    /** 保险帐户单位数 */
    private double UnitCount;
    /** 保险账户可领金额 */
    private double InsuAccGetMoney;
    /** 冻结金额 */
    private double FrozenMoney;
    /** 状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 36; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPInsureAccClassSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[8];
        pk[0] = "EdorNo";
        pk[1] = "EdorType";
        pk[2] = "GrpContNo";
        pk[3] = "PolNo";
        pk[4] = "InsuAccNo";
        pk[5] = "PayPlanCode";
        pk[6] = "OtherNo";
        pk[7] = "AccAscription";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getEdorType()
    {
        if (EdorType != null && !EdorType.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getInsuAccNo()
    {
        if (InsuAccNo != null && !InsuAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuAccNo = StrTool.unicodeToGBK(InsuAccNo);
        }
        return InsuAccNo;
    }

    public void setInsuAccNo(String aInsuAccNo)
    {
        InsuAccNo = aInsuAccNo;
    }

    public String getPayPlanCode()
    {
        if (PayPlanCode != null && !PayPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayPlanCode = StrTool.unicodeToGBK(PayPlanCode);
        }
        return PayPlanCode;
    }

    public void setPayPlanCode(String aPayPlanCode)
    {
        PayPlanCode = aPayPlanCode;
    }

    public String getOtherNo()
    {
        if (OtherNo != null && !OtherNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OtherNo = StrTool.unicodeToGBK(OtherNo);
        }
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherType()
    {
        if (OtherType != null && !OtherType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OtherType = StrTool.unicodeToGBK(OtherType);
        }
        return OtherType;
    }

    public void setOtherType(String aOtherType)
    {
        OtherType = aOtherType;
    }

    public String getAccAscription()
    {
        if (AccAscription != null && !AccAscription.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccAscription = StrTool.unicodeToGBK(AccAscription);
        }
        return AccAscription;
    }

    public void setAccAscription(String aAccAscription)
    {
        AccAscription = aAccAscription;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getAccType()
    {
        if (AccType != null && !AccType.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccType = StrTool.unicodeToGBK(AccType);
        }
        return AccType;
    }

    public void setAccType(String aAccType)
    {
        AccType = aAccType;
    }

    public String getAccComputeFlag()
    {
        if (AccComputeFlag != null && !AccComputeFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccComputeFlag = StrTool.unicodeToGBK(AccComputeFlag);
        }
        return AccComputeFlag;
    }

    public void setAccComputeFlag(String aAccComputeFlag)
    {
        AccComputeFlag = aAccComputeFlag;
    }

    public String getAccFoundDate()
    {
        if (AccFoundDate != null)
        {
            return fDate.getString(AccFoundDate);
        }
        else
        {
            return null;
        }
    }

    public void setAccFoundDate(Date aAccFoundDate)
    {
        AccFoundDate = aAccFoundDate;
    }

    public void setAccFoundDate(String aAccFoundDate)
    {
        if (aAccFoundDate != null && !aAccFoundDate.equals(""))
        {
            AccFoundDate = fDate.getDate(aAccFoundDate);
        }
        else
        {
            AccFoundDate = null;
        }
    }

    public String getAccFoundTime()
    {
        if (AccFoundTime != null && !AccFoundTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccFoundTime = StrTool.unicodeToGBK(AccFoundTime);
        }
        return AccFoundTime;
    }

    public void setAccFoundTime(String aAccFoundTime)
    {
        AccFoundTime = aAccFoundTime;
    }

    public String getBalaDate()
    {
        if (BalaDate != null)
        {
            return fDate.getString(BalaDate);
        }
        else
        {
            return null;
        }
    }

    public void setBalaDate(Date aBalaDate)
    {
        BalaDate = aBalaDate;
    }

    public void setBalaDate(String aBalaDate)
    {
        if (aBalaDate != null && !aBalaDate.equals(""))
        {
            BalaDate = fDate.getDate(aBalaDate);
        }
        else
        {
            BalaDate = null;
        }
    }

    public String getBalaTime()
    {
        if (BalaTime != null && !BalaTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            BalaTime = StrTool.unicodeToGBK(BalaTime);
        }
        return BalaTime;
    }

    public void setBalaTime(String aBalaTime)
    {
        BalaTime = aBalaTime;
    }

    public double getSumPay()
    {
        return SumPay;
    }

    public void setSumPay(double aSumPay)
    {
        SumPay = aSumPay;
    }

    public void setSumPay(String aSumPay)
    {
        if (aSumPay != null && !aSumPay.equals(""))
        {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = d;
        }
    }

    public double getSumPaym()
    {
        return SumPaym;
    }

    public void setSumPaym(double aSumPaym)
    {
        SumPaym = aSumPaym;
    }

    public void setSumPaym(String aSumPaym)
    {
        if (aSumPaym != null && !aSumPaym.equals(""))
        {
            Double tDouble = new Double(aSumPaym);
            double d = tDouble.doubleValue();
            SumPaym = d;
        }
    }

    public double getLastAccBala()
    {
        return LastAccBala;
    }

    public void setLastAccBala(double aLastAccBala)
    {
        LastAccBala = aLastAccBala;
    }

    public void setLastAccBala(String aLastAccBala)
    {
        if (aLastAccBala != null && !aLastAccBala.equals(""))
        {
            Double tDouble = new Double(aLastAccBala);
            double d = tDouble.doubleValue();
            LastAccBala = d;
        }
    }

    public double getLastUnitCount()
    {
        return LastUnitCount;
    }

    public void setLastUnitCount(double aLastUnitCount)
    {
        LastUnitCount = aLastUnitCount;
    }

    public void setLastUnitCount(String aLastUnitCount)
    {
        if (aLastUnitCount != null && !aLastUnitCount.equals(""))
        {
            Double tDouble = new Double(aLastUnitCount);
            double d = tDouble.doubleValue();
            LastUnitCount = d;
        }
    }

    public double getLastUnitPrice()
    {
        return LastUnitPrice;
    }

    public void setLastUnitPrice(double aLastUnitPrice)
    {
        LastUnitPrice = aLastUnitPrice;
    }

    public void setLastUnitPrice(String aLastUnitPrice)
    {
        if (aLastUnitPrice != null && !aLastUnitPrice.equals(""))
        {
            Double tDouble = new Double(aLastUnitPrice);
            double d = tDouble.doubleValue();
            LastUnitPrice = d;
        }
    }

    public double getInsuAccBala()
    {
        return InsuAccBala;
    }

    public void setInsuAccBala(double aInsuAccBala)
    {
        InsuAccBala = aInsuAccBala;
    }

    public void setInsuAccBala(String aInsuAccBala)
    {
        if (aInsuAccBala != null && !aInsuAccBala.equals(""))
        {
            Double tDouble = new Double(aInsuAccBala);
            double d = tDouble.doubleValue();
            InsuAccBala = d;
        }
    }

    public double getUnitCount()
    {
        return UnitCount;
    }

    public void setUnitCount(double aUnitCount)
    {
        UnitCount = aUnitCount;
    }

    public void setUnitCount(String aUnitCount)
    {
        if (aUnitCount != null && !aUnitCount.equals(""))
        {
            Double tDouble = new Double(aUnitCount);
            double d = tDouble.doubleValue();
            UnitCount = d;
        }
    }

    public double getInsuAccGetMoney()
    {
        return InsuAccGetMoney;
    }

    public void setInsuAccGetMoney(double aInsuAccGetMoney)
    {
        InsuAccGetMoney = aInsuAccGetMoney;
    }

    public void setInsuAccGetMoney(String aInsuAccGetMoney)
    {
        if (aInsuAccGetMoney != null && !aInsuAccGetMoney.equals(""))
        {
            Double tDouble = new Double(aInsuAccGetMoney);
            double d = tDouble.doubleValue();
            InsuAccGetMoney = d;
        }
    }

    public double getFrozenMoney()
    {
        return FrozenMoney;
    }

    public void setFrozenMoney(double aFrozenMoney)
    {
        FrozenMoney = aFrozenMoney;
    }

    public void setFrozenMoney(String aFrozenMoney)
    {
        if (aFrozenMoney != null && !aFrozenMoney.equals(""))
        {
            Double tDouble = new Double(aFrozenMoney);
            double d = tDouble.doubleValue();
            FrozenMoney = d;
        }
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET == true)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LPInsureAccClassSchema 对象给 Schema 赋值
     * @param: aLPInsureAccClassSchema LPInsureAccClassSchema
     **/
    public void setSchema(LPInsureAccClassSchema aLPInsureAccClassSchema)
    {
        this.EdorNo = aLPInsureAccClassSchema.getEdorNo();
        this.EdorType = aLPInsureAccClassSchema.getEdorType();
        this.GrpContNo = aLPInsureAccClassSchema.getGrpContNo();
        this.GrpPolNo = aLPInsureAccClassSchema.getGrpPolNo();
        this.ContNo = aLPInsureAccClassSchema.getContNo();
        this.ManageCom = aLPInsureAccClassSchema.getManageCom();
        this.PolNo = aLPInsureAccClassSchema.getPolNo();
        this.InsuAccNo = aLPInsureAccClassSchema.getInsuAccNo();
        this.PayPlanCode = aLPInsureAccClassSchema.getPayPlanCode();
        this.OtherNo = aLPInsureAccClassSchema.getOtherNo();
        this.OtherType = aLPInsureAccClassSchema.getOtherType();
        this.AccAscription = aLPInsureAccClassSchema.getAccAscription();
        this.RiskCode = aLPInsureAccClassSchema.getRiskCode();
        this.InsuredNo = aLPInsureAccClassSchema.getInsuredNo();
        this.AppntNo = aLPInsureAccClassSchema.getAppntNo();
        this.AccType = aLPInsureAccClassSchema.getAccType();
        this.AccComputeFlag = aLPInsureAccClassSchema.getAccComputeFlag();
        this.AccFoundDate = fDate.getDate(aLPInsureAccClassSchema.
                                          getAccFoundDate());
        this.AccFoundTime = aLPInsureAccClassSchema.getAccFoundTime();
        this.BalaDate = fDate.getDate(aLPInsureAccClassSchema.getBalaDate());
        this.BalaTime = aLPInsureAccClassSchema.getBalaTime();
        this.SumPay = aLPInsureAccClassSchema.getSumPay();
        this.SumPaym = aLPInsureAccClassSchema.getSumPaym();
        this.LastAccBala = aLPInsureAccClassSchema.getLastAccBala();
        this.LastUnitCount = aLPInsureAccClassSchema.getLastUnitCount();
        this.LastUnitPrice = aLPInsureAccClassSchema.getLastUnitPrice();
        this.InsuAccBala = aLPInsureAccClassSchema.getInsuAccBala();
        this.UnitCount = aLPInsureAccClassSchema.getUnitCount();
        this.InsuAccGetMoney = aLPInsureAccClassSchema.getInsuAccGetMoney();
        this.FrozenMoney = aLPInsureAccClassSchema.getFrozenMoney();
        this.State = aLPInsureAccClassSchema.getState();
        this.Operator = aLPInsureAccClassSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPInsureAccClassSchema.getMakeDate());
        this.MakeTime = aLPInsureAccClassSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPInsureAccClassSchema.getModifyDate());
        this.ModifyTime = aLPInsureAccClassSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("InsuAccNo") == null)
            {
                this.InsuAccNo = null;
            }
            else
            {
                this.InsuAccNo = rs.getString("InsuAccNo").trim();
            }

            if (rs.getString("PayPlanCode") == null)
            {
                this.PayPlanCode = null;
            }
            else
            {
                this.PayPlanCode = rs.getString("PayPlanCode").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherType") == null)
            {
                this.OtherType = null;
            }
            else
            {
                this.OtherType = rs.getString("OtherType").trim();
            }

            if (rs.getString("AccAscription") == null)
            {
                this.AccAscription = null;
            }
            else
            {
                this.AccAscription = rs.getString("AccAscription").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AccType") == null)
            {
                this.AccType = null;
            }
            else
            {
                this.AccType = rs.getString("AccType").trim();
            }

            if (rs.getString("AccComputeFlag") == null)
            {
                this.AccComputeFlag = null;
            }
            else
            {
                this.AccComputeFlag = rs.getString("AccComputeFlag").trim();
            }

            this.AccFoundDate = rs.getDate("AccFoundDate");
            if (rs.getString("AccFoundTime") == null)
            {
                this.AccFoundTime = null;
            }
            else
            {
                this.AccFoundTime = rs.getString("AccFoundTime").trim();
            }

            this.BalaDate = rs.getDate("BalaDate");
            if (rs.getString("BalaTime") == null)
            {
                this.BalaTime = null;
            }
            else
            {
                this.BalaTime = rs.getString("BalaTime").trim();
            }

            this.SumPay = rs.getDouble("SumPay");
            this.SumPaym = rs.getDouble("SumPaym");
            this.LastAccBala = rs.getDouble("LastAccBala");
            this.LastUnitCount = rs.getDouble("LastUnitCount");
            this.LastUnitPrice = rs.getDouble("LastUnitPrice");
            this.InsuAccBala = rs.getDouble("InsuAccBala");
            this.UnitCount = rs.getDouble("UnitCount");
            this.InsuAccGetMoney = rs.getDouble("InsuAccGetMoney");
            this.FrozenMoney = rs.getDouble("FrozenMoney");
            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPInsureAccClassSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPInsureAccClassSchema getSchema()
    {
        LPInsureAccClassSchema aLPInsureAccClassSchema = new
                LPInsureAccClassSchema();
        aLPInsureAccClassSchema.setSchema(this);
        return aLPInsureAccClassSchema;
    }

    public LPInsureAccClassDB getDB()
    {
        LPInsureAccClassDB aDBOper = new LPInsureAccClassDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPInsureAccClass描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccAscription)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccComputeFlag)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(AccFoundDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccFoundTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(BalaDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BalaTime)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SumPay) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(SumPaym) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(LastAccBala) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(LastUnitCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(LastUnitPrice) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(InsuAccBala) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(UnitCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(InsuAccGetMoney) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FrozenMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPInsureAccClass>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            OtherType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            AccAscription = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                           SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                     SysConst.PACKAGESPILTER);
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                     SysConst.PACKAGESPILTER);
            AccComputeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            17, SysConst.PACKAGESPILTER);
            AccFoundDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            AccFoundTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                          SysConst.PACKAGESPILTER);
            BalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            BalaTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            SumPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    22, SysConst.PACKAGESPILTER))).doubleValue();
            SumPaym = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 23, SysConst.PACKAGESPILTER))).doubleValue();
            LastAccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 24, SysConst.PACKAGESPILTER))).doubleValue();
            LastUnitCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 25, SysConst.PACKAGESPILTER))).doubleValue();
            LastUnitPrice = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 26, SysConst.PACKAGESPILTER))).doubleValue();
            InsuAccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).doubleValue();
            UnitCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 28, SysConst.PACKAGESPILTER))).doubleValue();
            InsuAccGetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 29, SysConst.PACKAGESPILTER))).doubleValue();
            FrozenMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 30, SysConst.PACKAGESPILTER))).doubleValue();
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 33, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 35, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPInsureAccClassSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("InsuAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equals("PayPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("OtherType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherType));
        }
        if (FCode.equals("AccAscription"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAscription));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equals("AccType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equals("AccComputeFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccComputeFlag));
        }
        if (FCode.equals("AccFoundDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getAccFoundDate()));
        }
        if (FCode.equals("AccFoundTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundTime));
        }
        if (FCode.equals("BalaDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBalaDate()));
        }
        if (FCode.equals("BalaTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaTime));
        }
        if (FCode.equals("SumPay"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equals("SumPaym"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPaym));
        }
        if (FCode.equals("LastAccBala"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastAccBala));
        }
        if (FCode.equals("LastUnitCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastUnitCount));
        }
        if (FCode.equals("LastUnitPrice"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastUnitPrice));
        }
        if (FCode.equals("InsuAccBala"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccBala));
        }
        if (FCode.equals("UnitCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitCount));
        }
        if (FCode.equals("InsuAccGetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccGetMoney));
        }
        if (FCode.equals("FrozenMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FrozenMoney));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(OtherType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AccAscription);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(AccComputeFlag);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getAccFoundDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(AccFoundTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBalaDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(BalaTime);
                break;
            case 21:
                strFieldValue = String.valueOf(SumPay);
                break;
            case 22:
                strFieldValue = String.valueOf(SumPaym);
                break;
            case 23:
                strFieldValue = String.valueOf(LastAccBala);
                break;
            case 24:
                strFieldValue = String.valueOf(LastUnitCount);
                break;
            case 25:
                strFieldValue = String.valueOf(LastUnitPrice);
                break;
            case 26:
                strFieldValue = String.valueOf(InsuAccBala);
                break;
            case 27:
                strFieldValue = String.valueOf(UnitCount);
                break;
            case 28:
                strFieldValue = String.valueOf(InsuAccGetMoney);
                break;
            case 29:
                strFieldValue = String.valueOf(FrozenMoney);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("InsuAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
            {
                InsuAccNo = null;
            }
        }
        if (FCode.equals("PayPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
            {
                PayPlanCode = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherType = FValue.trim();
            }
            else
            {
                OtherType = null;
            }
        }
        if (FCode.equals("AccAscription"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccAscription = FValue.trim();
            }
            else
            {
                AccAscription = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("AccType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
            {
                AccType = null;
            }
        }
        if (FCode.equals("AccComputeFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
            {
                AccComputeFlag = null;
            }
        }
        if (FCode.equals("AccFoundDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccFoundDate = fDate.getDate(FValue);
            }
            else
            {
                AccFoundDate = null;
            }
        }
        if (FCode.equals("AccFoundTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccFoundTime = FValue.trim();
            }
            else
            {
                AccFoundTime = null;
            }
        }
        if (FCode.equals("BalaDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BalaDate = fDate.getDate(FValue);
            }
            else
            {
                BalaDate = null;
            }
        }
        if (FCode.equals("BalaTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BalaTime = FValue.trim();
            }
            else
            {
                BalaTime = null;
            }
        }
        if (FCode.equals("SumPay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equals("SumPaym"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumPaym = d;
            }
        }
        if (FCode.equals("LastAccBala"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LastAccBala = d;
            }
        }
        if (FCode.equals("LastUnitCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LastUnitCount = d;
            }
        }
        if (FCode.equals("LastUnitPrice"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LastUnitPrice = d;
            }
        }
        if (FCode.equals("InsuAccBala"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                InsuAccBala = d;
            }
        }
        if (FCode.equals("UnitCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                UnitCount = d;
            }
        }
        if (FCode.equals("InsuAccGetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                InsuAccGetMoney = d;
            }
        }
        if (FCode.equals("FrozenMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FrozenMoney = d;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPInsureAccClassSchema other = (LPInsureAccClassSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && ContNo.equals(other.getContNo())
                && ManageCom.equals(other.getManageCom())
                && PolNo.equals(other.getPolNo())
                && InsuAccNo.equals(other.getInsuAccNo())
                && PayPlanCode.equals(other.getPayPlanCode())
                && OtherNo.equals(other.getOtherNo())
                && OtherType.equals(other.getOtherType())
                && AccAscription.equals(other.getAccAscription())
                && RiskCode.equals(other.getRiskCode())
                && InsuredNo.equals(other.getInsuredNo())
                && AppntNo.equals(other.getAppntNo())
                && AccType.equals(other.getAccType())
                && AccComputeFlag.equals(other.getAccComputeFlag())
                && fDate.getString(AccFoundDate).equals(other.getAccFoundDate())
                && AccFoundTime.equals(other.getAccFoundTime())
                && fDate.getString(BalaDate).equals(other.getBalaDate())
                && BalaTime.equals(other.getBalaTime())
                && SumPay == other.getSumPay()
                && SumPaym == other.getSumPaym()
                && LastAccBala == other.getLastAccBala()
                && LastUnitCount == other.getLastUnitCount()
                && LastUnitPrice == other.getLastUnitPrice()
                && InsuAccBala == other.getInsuAccBala()
                && UnitCount == other.getUnitCount()
                && InsuAccGetMoney == other.getInsuAccGetMoney()
                && FrozenMoney == other.getFrozenMoney()
                && State.equals(other.getState())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 1;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 3;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 4;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 5;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 6;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return 7;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return 8;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 9;
        }
        if (strFieldName.equals("OtherType"))
        {
            return 10;
        }
        if (strFieldName.equals("AccAscription"))
        {
            return 11;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 12;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 13;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 14;
        }
        if (strFieldName.equals("AccType"))
        {
            return 15;
        }
        if (strFieldName.equals("AccComputeFlag"))
        {
            return 16;
        }
        if (strFieldName.equals("AccFoundDate"))
        {
            return 17;
        }
        if (strFieldName.equals("AccFoundTime"))
        {
            return 18;
        }
        if (strFieldName.equals("BalaDate"))
        {
            return 19;
        }
        if (strFieldName.equals("BalaTime"))
        {
            return 20;
        }
        if (strFieldName.equals("SumPay"))
        {
            return 21;
        }
        if (strFieldName.equals("SumPaym"))
        {
            return 22;
        }
        if (strFieldName.equals("LastAccBala"))
        {
            return 23;
        }
        if (strFieldName.equals("LastUnitCount"))
        {
            return 24;
        }
        if (strFieldName.equals("LastUnitPrice"))
        {
            return 25;
        }
        if (strFieldName.equals("InsuAccBala"))
        {
            return 26;
        }
        if (strFieldName.equals("UnitCount"))
        {
            return 27;
        }
        if (strFieldName.equals("InsuAccGetMoney"))
        {
            return 28;
        }
        if (strFieldName.equals("FrozenMoney"))
        {
            return 29;
        }
        if (strFieldName.equals("State"))
        {
            return 30;
        }
        if (strFieldName.equals("Operator"))
        {
            return 31;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 32;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 33;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 34;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 35;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "EdorType";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "GrpPolNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "ManageCom";
                break;
            case 6:
                strFieldName = "PolNo";
                break;
            case 7:
                strFieldName = "InsuAccNo";
                break;
            case 8:
                strFieldName = "PayPlanCode";
                break;
            case 9:
                strFieldName = "OtherNo";
                break;
            case 10:
                strFieldName = "OtherType";
                break;
            case 11:
                strFieldName = "AccAscription";
                break;
            case 12:
                strFieldName = "RiskCode";
                break;
            case 13:
                strFieldName = "InsuredNo";
                break;
            case 14:
                strFieldName = "AppntNo";
                break;
            case 15:
                strFieldName = "AccType";
                break;
            case 16:
                strFieldName = "AccComputeFlag";
                break;
            case 17:
                strFieldName = "AccFoundDate";
                break;
            case 18:
                strFieldName = "AccFoundTime";
                break;
            case 19:
                strFieldName = "BalaDate";
                break;
            case 20:
                strFieldName = "BalaTime";
                break;
            case 21:
                strFieldName = "SumPay";
                break;
            case 22:
                strFieldName = "SumPaym";
                break;
            case 23:
                strFieldName = "LastAccBala";
                break;
            case 24:
                strFieldName = "LastUnitCount";
                break;
            case 25:
                strFieldName = "LastUnitPrice";
                break;
            case 26:
                strFieldName = "InsuAccBala";
                break;
            case 27:
                strFieldName = "UnitCount";
                break;
            case 28:
                strFieldName = "InsuAccGetMoney";
                break;
            case 29:
                strFieldName = "FrozenMoney";
                break;
            case 30:
                strFieldName = "State";
                break;
            case 31:
                strFieldName = "Operator";
                break;
            case 32:
                strFieldName = "MakeDate";
                break;
            case 33:
                strFieldName = "MakeTime";
                break;
            case 34:
                strFieldName = "ModifyDate";
                break;
            case 35:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccAscription"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccComputeFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccFoundDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AccFoundTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BalaDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BalaTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SumPay"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SumPaym"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("LastAccBala"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("LastUnitCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("LastUnitPrice"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("InsuAccBala"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("UnitCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("InsuAccGetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FrozenMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 22:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 23:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 24:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 25:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 26:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 27:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 28:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 29:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 34:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 35:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
