/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LYOutPayQueryDetailDB;

/*
 * <p>ClassName: LYOutPayQueryDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-06-27
 */
public class LYOutPayQueryDetailSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String BusilNo;
	/** 最大号 */
	private String BatchNo;
	/** 打印日期 */
	private Date printdate;
	/** 发票号码 */
	private String billnum;
	/** 发票状态 */
	private String vstatus;
	/** 是否打印 */
	private String isprint;
	/** 开票与生效孰后的状态 */
	private String cvalistate;
	/** 操作员 */
	private String Operator;
	/** 备注 */
	private String Remark;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 平台回写日期 */
	private String TS;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LYOutPayQueryDetailSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LYOutPayQueryDetailSchema cloned = (LYOutPayQueryDetailSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBusilNo()
	{
		return BusilNo;
	}
	public void setBusilNo(String aBusilNo)
	{
		BusilNo = aBusilNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getprintdate()
	{
		if( printdate != null )
			return fDate.getString(printdate);
		else
			return null;
	}
	public void setprintdate(Date aprintdate)
	{
		printdate = aprintdate;
	}
	public void setprintdate(String aprintdate)
	{
		if (aprintdate != null && !aprintdate.equals("") )
		{
			printdate = fDate.getDate( aprintdate );
		}
		else
			printdate = null;
	}

	public String getbillnum()
	{
		return billnum;
	}
	public void setbillnum(String abillnum)
	{
		billnum = abillnum;
	}
	public String getvstatus()
	{
		return vstatus;
	}
	public void setvstatus(String avstatus)
	{
		vstatus = avstatus;
	}
	public String getisprint()
	{
		return isprint;
	}
	public void setisprint(String aisprint)
	{
		isprint = aisprint;
	}
	public String getcvalistate()
	{
		return cvalistate;
	}
	public void setcvalistate(String acvalistate)
	{
		cvalistate = acvalistate;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getTS()
	{
		return TS;
	}
	public void setTS(String aTS)
	{
		TS = aTS;
	}

	/**
	* 使用另外一个 LYOutPayQueryDetailSchema 对象给 Schema 赋值
	* @param: aLYOutPayQueryDetailSchema LYOutPayQueryDetailSchema
	**/
	public void setSchema(LYOutPayQueryDetailSchema aLYOutPayQueryDetailSchema)
	{
		this.BusilNo = aLYOutPayQueryDetailSchema.getBusilNo();
		this.BatchNo = aLYOutPayQueryDetailSchema.getBatchNo();
		this.printdate = fDate.getDate( aLYOutPayQueryDetailSchema.getprintdate());
		this.billnum = aLYOutPayQueryDetailSchema.getbillnum();
		this.vstatus = aLYOutPayQueryDetailSchema.getvstatus();
		this.isprint = aLYOutPayQueryDetailSchema.getisprint();
		this.cvalistate = aLYOutPayQueryDetailSchema.getcvalistate();
		this.Operator = aLYOutPayQueryDetailSchema.getOperator();
		this.Remark = aLYOutPayQueryDetailSchema.getRemark();
		this.MakeDate = fDate.getDate( aLYOutPayQueryDetailSchema.getMakeDate());
		this.MakeTime = aLYOutPayQueryDetailSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLYOutPayQueryDetailSchema.getModifyDate());
		this.ModifyTime = aLYOutPayQueryDetailSchema.getModifyTime();
		this.TS = aLYOutPayQueryDetailSchema.getTS();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BusilNo") == null )
				this.BusilNo = null;
			else
				this.BusilNo = rs.getString("BusilNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			this.printdate = rs.getDate("printdate");
			if( rs.getString("billnum") == null )
				this.billnum = null;
			else
				this.billnum = rs.getString("billnum").trim();

			if( rs.getString("vstatus") == null )
				this.vstatus = null;
			else
				this.vstatus = rs.getString("vstatus").trim();

			if( rs.getString("isprint") == null )
				this.isprint = null;
			else
				this.isprint = rs.getString("isprint").trim();

			if( rs.getString("cvalistate") == null )
				this.cvalistate = null;
			else
				this.cvalistate = rs.getString("cvalistate").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("TS") == null )
				this.TS = null;
			else
				this.TS = rs.getString("TS").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LYOutPayQueryDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LYOutPayQueryDetailSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LYOutPayQueryDetailSchema getSchema()
	{
		LYOutPayQueryDetailSchema aLYOutPayQueryDetailSchema = new LYOutPayQueryDetailSchema();
		aLYOutPayQueryDetailSchema.setSchema(this);
		return aLYOutPayQueryDetailSchema;
	}

	public LYOutPayQueryDetailDB getDB()
	{
		LYOutPayQueryDetailDB aDBOper = new LYOutPayQueryDetailDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYOutPayQueryDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BusilNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( printdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(billnum)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vstatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isprint)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cvalistate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TS));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYOutPayQueryDetail>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BusilNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			printdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			billnum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			vstatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			isprint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			cvalistate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			TS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LYOutPayQueryDetailSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BusilNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusilNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("printdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getprintdate()));
		}
		if (FCode.equals("billnum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(billnum));
		}
		if (FCode.equals("vstatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vstatus));
		}
		if (FCode.equals("isprint"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isprint));
		}
		if (FCode.equals("cvalistate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cvalistate));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("TS"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TS));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BusilNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getprintdate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(billnum);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(vstatus);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(isprint);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(cvalistate);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(TS);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BusilNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusilNo = FValue.trim();
			}
			else
				BusilNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("printdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				printdate = fDate.getDate( FValue );
			}
			else
				printdate = null;
		}
		if (FCode.equalsIgnoreCase("billnum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				billnum = FValue.trim();
			}
			else
				billnum = null;
		}
		if (FCode.equalsIgnoreCase("vstatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vstatus = FValue.trim();
			}
			else
				vstatus = null;
		}
		if (FCode.equalsIgnoreCase("isprint"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isprint = FValue.trim();
			}
			else
				isprint = null;
		}
		if (FCode.equalsIgnoreCase("cvalistate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cvalistate = FValue.trim();
			}
			else
				cvalistate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("TS"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TS = FValue.trim();
			}
			else
				TS = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LYOutPayQueryDetailSchema other = (LYOutPayQueryDetailSchema)otherObject;
		return
			(BusilNo == null ? other.getBusilNo() == null : BusilNo.equals(other.getBusilNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (printdate == null ? other.getprintdate() == null : fDate.getString(printdate).equals(other.getprintdate()))
			&& (billnum == null ? other.getbillnum() == null : billnum.equals(other.getbillnum()))
			&& (vstatus == null ? other.getvstatus() == null : vstatus.equals(other.getvstatus()))
			&& (isprint == null ? other.getisprint() == null : isprint.equals(other.getisprint()))
			&& (cvalistate == null ? other.getcvalistate() == null : cvalistate.equals(other.getcvalistate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (TS == null ? other.getTS() == null : TS.equals(other.getTS()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BusilNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("printdate") ) {
			return 2;
		}
		if( strFieldName.equals("billnum") ) {
			return 3;
		}
		if( strFieldName.equals("vstatus") ) {
			return 4;
		}
		if( strFieldName.equals("isprint") ) {
			return 5;
		}
		if( strFieldName.equals("cvalistate") ) {
			return 6;
		}
		if( strFieldName.equals("Operator") ) {
			return 7;
		}
		if( strFieldName.equals("Remark") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		if( strFieldName.equals("TS") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BusilNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "printdate";
				break;
			case 3:
				strFieldName = "billnum";
				break;
			case 4:
				strFieldName = "vstatus";
				break;
			case 5:
				strFieldName = "isprint";
				break;
			case 6:
				strFieldName = "cvalistate";
				break;
			case 7:
				strFieldName = "Operator";
				break;
			case 8:
				strFieldName = "Remark";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			case 13:
				strFieldName = "TS";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BusilNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("printdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("billnum") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vstatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isprint") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cvalistate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TS") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
