/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRPolEdorResultDB;

/*
 * <p>ClassName: LRPolEdorResultSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2009-08-28
 */
public class LRPolEdorResultSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号码 */
	private String PolNo;
	/** 保全批单号 */
	private String EdorNo;
	/** 再保合同号 */
	private String ReContCode;
	/** 再保险公司 */
	private String ReComCode;
	/** 再保项目 */
	private String ReinsureItem;
	/** 险种计算分类 */
	private String RiskCalSort;
	/** 险种编码 */
	private String RiskCode;
	/** 险种版本 */
	private String RiskVersion;
	/** 分保计算开始日期 */
	private Date CessStartDate;
	/** 分保计算结束日期 */
	private Date CessEndDate;
	/** 保全退费摊回 */
	private double EdorBackFee;
	/** 保全手续费摊回 */
	private double EdorProcFee;
	/** 操作人 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 续保次数 */
	private int ReNewCount;
	/** 待缴期数 */
	private int RemainCount;
	/** 应退保费合计 */
	private double ShouldReturnPrem;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRPolEdorResultSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "PolNo";
		pk[1] = "EdorNo";
		pk[2] = "ReContCode";
		pk[3] = "ReinsureItem";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRPolEdorResultSchema cloned = (LRPolEdorResultSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
		ReContCode = aReContCode;
	}
	public String getReComCode()
	{
		return ReComCode;
	}
	public void setReComCode(String aReComCode)
	{
		ReComCode = aReComCode;
	}
	public String getReinsureItem()
	{
		return ReinsureItem;
	}
	public void setReinsureItem(String aReinsureItem)
	{
		ReinsureItem = aReinsureItem;
	}
	public String getRiskCalSort()
	{
		return RiskCalSort;
	}
	public void setRiskCalSort(String aRiskCalSort)
	{
		RiskCalSort = aRiskCalSort;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVersion()
	{
		return RiskVersion;
	}
	public void setRiskVersion(String aRiskVersion)
	{
		RiskVersion = aRiskVersion;
	}
	public String getCessStartDate()
	{
		if( CessStartDate != null )
			return fDate.getString(CessStartDate);
		else
			return null;
	}
	public void setCessStartDate(Date aCessStartDate)
	{
		CessStartDate = aCessStartDate;
	}
	public void setCessStartDate(String aCessStartDate)
	{
		if (aCessStartDate != null && !aCessStartDate.equals("") )
		{
			CessStartDate = fDate.getDate( aCessStartDate );
		}
		else
			CessStartDate = null;
	}

	public String getCessEndDate()
	{
		if( CessEndDate != null )
			return fDate.getString(CessEndDate);
		else
			return null;
	}
	public void setCessEndDate(Date aCessEndDate)
	{
		CessEndDate = aCessEndDate;
	}
	public void setCessEndDate(String aCessEndDate)
	{
		if (aCessEndDate != null && !aCessEndDate.equals("") )
		{
			CessEndDate = fDate.getDate( aCessEndDate );
		}
		else
			CessEndDate = null;
	}

	public double getEdorBackFee()
	{
		return EdorBackFee;
	}
	public void setEdorBackFee(double aEdorBackFee)
	{
		EdorBackFee = Arith.round(aEdorBackFee,2);
	}
	public void setEdorBackFee(String aEdorBackFee)
	{
		if (aEdorBackFee != null && !aEdorBackFee.equals(""))
		{
			Double tDouble = new Double(aEdorBackFee);
			double d = tDouble.doubleValue();
                EdorBackFee = Arith.round(d,2);
		}
	}

	public double getEdorProcFee()
	{
		return EdorProcFee;
	}
	public void setEdorProcFee(double aEdorProcFee)
	{
		EdorProcFee = Arith.round(aEdorProcFee,2);
	}
	public void setEdorProcFee(String aEdorProcFee)
	{
		if (aEdorProcFee != null && !aEdorProcFee.equals(""))
		{
			Double tDouble = new Double(aEdorProcFee);
			double d = tDouble.doubleValue();
                EdorProcFee = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public int getReNewCount()
	{
		return ReNewCount;
	}
	public void setReNewCount(int aReNewCount)
	{
		ReNewCount = aReNewCount;
	}
	public void setReNewCount(String aReNewCount)
	{
		if (aReNewCount != null && !aReNewCount.equals(""))
		{
			Integer tInteger = new Integer(aReNewCount);
			int i = tInteger.intValue();
			ReNewCount = i;
		}
	}

	public int getRemainCount()
	{
		return RemainCount;
	}
	public void setRemainCount(int aRemainCount)
	{
		RemainCount = aRemainCount;
	}
	public void setRemainCount(String aRemainCount)
	{
		if (aRemainCount != null && !aRemainCount.equals(""))
		{
			Integer tInteger = new Integer(aRemainCount);
			int i = tInteger.intValue();
			RemainCount = i;
		}
	}

	public double getShouldReturnPrem()
	{
		return ShouldReturnPrem;
	}
	public void setShouldReturnPrem(double aShouldReturnPrem)
	{
		ShouldReturnPrem = Arith.round(aShouldReturnPrem,2);
	}
	public void setShouldReturnPrem(String aShouldReturnPrem)
	{
		if (aShouldReturnPrem != null && !aShouldReturnPrem.equals(""))
		{
			Double tDouble = new Double(aShouldReturnPrem);
			double d = tDouble.doubleValue();
                ShouldReturnPrem = Arith.round(d,2);
		}
	}


	/**
	* 使用另外一个 LRPolEdorResultSchema 对象给 Schema 赋值
	* @param: aLRPolEdorResultSchema LRPolEdorResultSchema
	**/
	public void setSchema(LRPolEdorResultSchema aLRPolEdorResultSchema)
	{
		this.PolNo = aLRPolEdorResultSchema.getPolNo();
		this.EdorNo = aLRPolEdorResultSchema.getEdorNo();
		this.ReContCode = aLRPolEdorResultSchema.getReContCode();
		this.ReComCode = aLRPolEdorResultSchema.getReComCode();
		this.ReinsureItem = aLRPolEdorResultSchema.getReinsureItem();
		this.RiskCalSort = aLRPolEdorResultSchema.getRiskCalSort();
		this.RiskCode = aLRPolEdorResultSchema.getRiskCode();
		this.RiskVersion = aLRPolEdorResultSchema.getRiskVersion();
		this.CessStartDate = fDate.getDate( aLRPolEdorResultSchema.getCessStartDate());
		this.CessEndDate = fDate.getDate( aLRPolEdorResultSchema.getCessEndDate());
		this.EdorBackFee = aLRPolEdorResultSchema.getEdorBackFee();
		this.EdorProcFee = aLRPolEdorResultSchema.getEdorProcFee();
		this.Operator = aLRPolEdorResultSchema.getOperator();
		this.ManageCom = aLRPolEdorResultSchema.getManageCom();
		this.MakeDate = fDate.getDate( aLRPolEdorResultSchema.getMakeDate());
		this.MakeTime = aLRPolEdorResultSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRPolEdorResultSchema.getModifyDate());
		this.ModifyTime = aLRPolEdorResultSchema.getModifyTime();
		this.ReNewCount = aLRPolEdorResultSchema.getReNewCount();
		this.RemainCount = aLRPolEdorResultSchema.getRemainCount();
		this.ShouldReturnPrem = aLRPolEdorResultSchema.getShouldReturnPrem();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("ReComCode") == null )
				this.ReComCode = null;
			else
				this.ReComCode = rs.getString("ReComCode").trim();

			if( rs.getString("ReinsureItem") == null )
				this.ReinsureItem = null;
			else
				this.ReinsureItem = rs.getString("ReinsureItem").trim();

			if( rs.getString("RiskCalSort") == null )
				this.RiskCalSort = null;
			else
				this.RiskCalSort = rs.getString("RiskCalSort").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVersion") == null )
				this.RiskVersion = null;
			else
				this.RiskVersion = rs.getString("RiskVersion").trim();

			this.CessStartDate = rs.getDate("CessStartDate");
			this.CessEndDate = rs.getDate("CessEndDate");
			this.EdorBackFee = rs.getDouble("EdorBackFee");
			this.EdorProcFee = rs.getDouble("EdorProcFee");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.ReNewCount = rs.getInt("ReNewCount");
			this.RemainCount = rs.getInt("RemainCount");
			this.ShouldReturnPrem = rs.getDouble("ShouldReturnPrem");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRPolEdorResult表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolEdorResultSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRPolEdorResultSchema getSchema()
	{
		LRPolEdorResultSchema aLRPolEdorResultSchema = new LRPolEdorResultSchema();
		aLRPolEdorResultSchema.setSchema(this);
		return aLRPolEdorResultSchema;
	}

	public LRPolEdorResultDB getDB()
	{
		LRPolEdorResultDB aDBOper = new LRPolEdorResultDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPolEdorResult描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReinsureItem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCalSort)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CessStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CessEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(EdorBackFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(EdorProcFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReNewCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RemainCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ShouldReturnPrem));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPolEdorResult>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ReComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ReinsureItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RiskCalSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CessStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			CessEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			EdorBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			EdorProcFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ReNewCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).intValue();
			RemainCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).intValue();
			ShouldReturnPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolEdorResultSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("ReComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComCode));
		}
		if (FCode.equals("ReinsureItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsureItem));
		}
		if (FCode.equals("RiskCalSort"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCalSort));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
		}
		if (FCode.equals("CessStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCessStartDate()));
		}
		if (FCode.equals("CessEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCessEndDate()));
		}
		if (FCode.equals("EdorBackFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorBackFee));
		}
		if (FCode.equals("EdorProcFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorProcFee));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ReNewCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReNewCount));
		}
		if (FCode.equals("RemainCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemainCount));
		}
		if (FCode.equals("ShouldReturnPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShouldReturnPrem));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ReComCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ReinsureItem);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RiskCalSort);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RiskVersion);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCessStartDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCessEndDate()));
				break;
			case 10:
				strFieldValue = String.valueOf(EdorBackFee);
				break;
			case 11:
				strFieldValue = String.valueOf(EdorProcFee);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 18:
				strFieldValue = String.valueOf(ReNewCount);
				break;
			case 19:
				strFieldValue = String.valueOf(RemainCount);
				break;
			case 20:
				strFieldValue = String.valueOf(ShouldReturnPrem);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("ReComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComCode = FValue.trim();
			}
			else
				ReComCode = null;
		}
		if (FCode.equalsIgnoreCase("ReinsureItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReinsureItem = FValue.trim();
			}
			else
				ReinsureItem = null;
		}
		if (FCode.equalsIgnoreCase("RiskCalSort"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCalSort = FValue.trim();
			}
			else
				RiskCalSort = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVersion = FValue.trim();
			}
			else
				RiskVersion = null;
		}
		if (FCode.equalsIgnoreCase("CessStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CessStartDate = fDate.getDate( FValue );
			}
			else
				CessStartDate = null;
		}
		if (FCode.equalsIgnoreCase("CessEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CessEndDate = fDate.getDate( FValue );
			}
			else
				CessEndDate = null;
		}
		if (FCode.equalsIgnoreCase("EdorBackFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				EdorBackFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("EdorProcFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				EdorProcFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ReNewCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ReNewCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("RemainCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RemainCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("ShouldReturnPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ShouldReturnPrem = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRPolEdorResultSchema other = (LRPolEdorResultSchema)otherObject;
		return
			(PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (ReContCode == null ? other.getReContCode() == null : ReContCode.equals(other.getReContCode()))
			&& (ReComCode == null ? other.getReComCode() == null : ReComCode.equals(other.getReComCode()))
			&& (ReinsureItem == null ? other.getReinsureItem() == null : ReinsureItem.equals(other.getReinsureItem()))
			&& (RiskCalSort == null ? other.getRiskCalSort() == null : RiskCalSort.equals(other.getRiskCalSort()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskVersion == null ? other.getRiskVersion() == null : RiskVersion.equals(other.getRiskVersion()))
			&& (CessStartDate == null ? other.getCessStartDate() == null : fDate.getString(CessStartDate).equals(other.getCessStartDate()))
			&& (CessEndDate == null ? other.getCessEndDate() == null : fDate.getString(CessEndDate).equals(other.getCessEndDate()))
			&& EdorBackFee == other.getEdorBackFee()
			&& EdorProcFee == other.getEdorProcFee()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& ReNewCount == other.getReNewCount()
			&& RemainCount == other.getRemainCount()
			&& ShouldReturnPrem == other.getShouldReturnPrem();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PolNo") ) {
			return 0;
		}
		if( strFieldName.equals("EdorNo") ) {
			return 1;
		}
		if( strFieldName.equals("ReContCode") ) {
			return 2;
		}
		if( strFieldName.equals("ReComCode") ) {
			return 3;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return 4;
		}
		if( strFieldName.equals("RiskCalSort") ) {
			return 5;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 6;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return 7;
		}
		if( strFieldName.equals("CessStartDate") ) {
			return 8;
		}
		if( strFieldName.equals("CessEndDate") ) {
			return 9;
		}
		if( strFieldName.equals("EdorBackFee") ) {
			return 10;
		}
		if( strFieldName.equals("EdorProcFee") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 17;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return 18;
		}
		if( strFieldName.equals("RemainCount") ) {
			return 19;
		}
		if( strFieldName.equals("ShouldReturnPrem") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PolNo";
				break;
			case 1:
				strFieldName = "EdorNo";
				break;
			case 2:
				strFieldName = "ReContCode";
				break;
			case 3:
				strFieldName = "ReComCode";
				break;
			case 4:
				strFieldName = "ReinsureItem";
				break;
			case 5:
				strFieldName = "RiskCalSort";
				break;
			case 6:
				strFieldName = "RiskCode";
				break;
			case 7:
				strFieldName = "RiskVersion";
				break;
			case 8:
				strFieldName = "CessStartDate";
				break;
			case 9:
				strFieldName = "CessEndDate";
				break;
			case 10:
				strFieldName = "EdorBackFee";
				break;
			case 11:
				strFieldName = "EdorProcFee";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "ManageCom";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "ModifyDate";
				break;
			case 17:
				strFieldName = "ModifyTime";
				break;
			case 18:
				strFieldName = "ReNewCount";
				break;
			case 19:
				strFieldName = "RemainCount";
				break;
			case 20:
				strFieldName = "ShouldReturnPrem";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCalSort") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CessEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EdorBackFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("EdorProcFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RemainCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ShouldReturnPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_INT;
				break;
			case 19:
				nFieldType = Schema.TYPE_INT;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
