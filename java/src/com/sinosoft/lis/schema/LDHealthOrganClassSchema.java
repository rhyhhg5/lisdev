/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDHealthOrganClassDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDHealthOrganClassSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_3
 * @CreateDate：2005-03-03
 */
public class LDHealthOrganClassSchema implements Schema
{
    // @Field
    /** 卫生机构类别代码 */
    private String HealthOrganClass;
    /** 卫生机构类别名称 */
    private String HealthOrganClassName;
    /** 说明 */
    private String Explai;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDHealthOrganClassSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "HealthOrganClass";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getHealthOrganClass()
    {
        if (HealthOrganClass != null && !HealthOrganClass.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HealthOrganClass = StrTool.unicodeToGBK(HealthOrganClass);
        }
        return HealthOrganClass;
    }

    public void setHealthOrganClass(String aHealthOrganClass)
    {
        HealthOrganClass = aHealthOrganClass;
    }

    public String getHealthOrganClassName()
    {
        if (HealthOrganClassName != null && !HealthOrganClassName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HealthOrganClassName = StrTool.unicodeToGBK(HealthOrganClassName);
        }
        return HealthOrganClassName;
    }

    public void setHealthOrganClassName(String aHealthOrganClassName)
    {
        HealthOrganClassName = aHealthOrganClassName;
    }

    public String getExplai()
    {
        if (Explai != null && !Explai.equals("") && SysConst.CHANGECHARSET == true)
        {
            Explai = StrTool.unicodeToGBK(Explai);
        }
        return Explai;
    }

    public void setExplai(String aExplai)
    {
        Explai = aExplai;
    }

    /**
     * 使用另外一个 LDHealthOrganClassSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDHealthOrganClassSchema aLDHealthOrganClassSchema)
    {
        this.HealthOrganClass = aLDHealthOrganClassSchema.getHealthOrganClass();
        this.HealthOrganClassName = aLDHealthOrganClassSchema.
                                    getHealthOrganClassName();
        this.Explai = aLDHealthOrganClassSchema.getExplai();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("HealthOrganClass") == null)
            {
                this.HealthOrganClass = null;
            }
            else
            {
                this.HealthOrganClass = rs.getString("HealthOrganClass").trim();
            }

            if (rs.getString("HealthOrganClassName") == null)
            {
                this.HealthOrganClassName = null;
            }
            else
            {
                this.HealthOrganClassName = rs.getString("HealthOrganClassName").
                                            trim();
            }

            if (rs.getString("Explai") == null)
            {
                this.Explai = null;
            }
            else
            {
                this.Explai = rs.getString("Explai").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDHealthOrganClassSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDHealthOrganClassSchema getSchema()
    {
        LDHealthOrganClassSchema aLDHealthOrganClassSchema = new
                LDHealthOrganClassSchema();
        aLDHealthOrganClassSchema.setSchema(this);
        return aLDHealthOrganClassSchema;
    }

    public LDHealthOrganClassDB getDB()
    {
        LDHealthOrganClassDB aDBOper = new LDHealthOrganClassDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDHealthOrganClass描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(HealthOrganClass)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HealthOrganClassName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Explai));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDHealthOrganClass>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            HealthOrganClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              1, SysConst.PACKAGESPILTER);
            HealthOrganClassName = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER);
            Explai = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDHealthOrganClassSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("HealthOrganClass"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HealthOrganClass));
        }
        if (FCode.equals("HealthOrganClassName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    HealthOrganClassName));
        }
        if (FCode.equals("Explai"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Explai));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(HealthOrganClass);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(HealthOrganClassName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Explai);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("HealthOrganClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HealthOrganClass = FValue.trim();
            }
            else
            {
                HealthOrganClass = null;
            }
        }
        if (FCode.equals("HealthOrganClassName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HealthOrganClassName = FValue.trim();
            }
            else
            {
                HealthOrganClassName = null;
            }
        }
        if (FCode.equals("Explai"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Explai = FValue.trim();
            }
            else
            {
                Explai = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDHealthOrganClassSchema other = (LDHealthOrganClassSchema) otherObject;
        return
                HealthOrganClass.equals(other.getHealthOrganClass())
                && HealthOrganClassName.equals(other.getHealthOrganClassName())
                && Explai.equals(other.getExplai());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("HealthOrganClass"))
        {
            return 0;
        }
        if (strFieldName.equals("HealthOrganClassName"))
        {
            return 1;
        }
        if (strFieldName.equals("Explai"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "HealthOrganClass";
                break;
            case 1:
                strFieldName = "HealthOrganClassName";
                break;
            case 2:
                strFieldName = "Explai";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("HealthOrganClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HealthOrganClassName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Explai"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
