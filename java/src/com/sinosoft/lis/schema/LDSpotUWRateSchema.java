/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDSpotUWRateDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDSpotUWRateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 抽取
 * @CreateDate：2005-02-21
 */
public class LDSpotUWRateSchema implements Schema
{
    // @Field
    /** 核保师编码 */
    private String UserCode;
    /** 核保师名称 */
    private String UserName;
    /** 核保级别 */
    private String UWGrade;
    /** 核保分类 */
    private String UWType;
    /** 核保比例 */
    private int UWRate;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDSpotUWRateSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "UserCode";
        pk[1] = "UWType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getUserCode()
    {
        if (UserCode != null && !UserCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            UserCode = StrTool.unicodeToGBK(UserCode);
        }
        return UserCode;
    }

    public void setUserCode(String aUserCode)
    {
        UserCode = aUserCode;
    }

    public String getUserName()
    {
        if (UserName != null && !UserName.equals("") && SysConst.CHANGECHARSET == true)
        {
            UserName = StrTool.unicodeToGBK(UserName);
        }
        return UserName;
    }

    public void setUserName(String aUserName)
    {
        UserName = aUserName;
    }

    public String getUWGrade()
    {
        if (UWGrade != null && !UWGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWGrade = StrTool.unicodeToGBK(UWGrade);
        }
        return UWGrade;
    }

    public void setUWGrade(String aUWGrade)
    {
        UWGrade = aUWGrade;
    }

    public String getUWType()
    {
        if (UWType != null && !UWType.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWType = StrTool.unicodeToGBK(UWType);
        }
        return UWType;
    }

    public void setUWType(String aUWType)
    {
        UWType = aUWType;
    }

    public int getUWRate()
    {
        return UWRate;
    }

    public void setUWRate(int aUWRate)
    {
        UWRate = aUWRate;
    }

    public void setUWRate(String aUWRate)
    {
        if (aUWRate != null && !aUWRate.equals(""))
        {
            Integer tInteger = new Integer(aUWRate);
            int i = tInteger.intValue();
            UWRate = i;
        }
    }


    /**
     * 使用另外一个 LDSpotUWRateSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDSpotUWRateSchema aLDSpotUWRateSchema)
    {
        this.UserCode = aLDSpotUWRateSchema.getUserCode();
        this.UserName = aLDSpotUWRateSchema.getUserName();
        this.UWGrade = aLDSpotUWRateSchema.getUWGrade();
        this.UWType = aLDSpotUWRateSchema.getUWType();
        this.UWRate = aLDSpotUWRateSchema.getUWRate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("UserCode") == null)
            {
                this.UserCode = null;
            }
            else
            {
                this.UserCode = rs.getString("UserCode").trim();
            }

            if (rs.getString("UserName") == null)
            {
                this.UserName = null;
            }
            else
            {
                this.UserName = rs.getString("UserName").trim();
            }

            if (rs.getString("UWGrade") == null)
            {
                this.UWGrade = null;
            }
            else
            {
                this.UWGrade = rs.getString("UWGrade").trim();
            }

            if (rs.getString("UWType") == null)
            {
                this.UWType = null;
            }
            else
            {
                this.UWType = rs.getString("UWType").trim();
            }

            this.UWRate = rs.getInt("UWRate");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSpotUWRateSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDSpotUWRateSchema getSchema()
    {
        LDSpotUWRateSchema aLDSpotUWRateSchema = new LDSpotUWRateSchema();
        aLDSpotUWRateSchema.setSchema(this);
        return aLDSpotUWRateSchema;
    }

    public LDSpotUWRateDB getDB()
    {
        LDSpotUWRateDB aDBOper = new LDSpotUWRateDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSpotUWRate描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(UserCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UserName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(UWRate);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSpotUWRate>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            UserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            UWType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            UWRate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSpotUWRateSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("UserCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UserCode));
        }
        if (FCode.equals("UserName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UserName));
        }
        if (FCode.equals("UWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWGrade));
        }
        if (FCode.equals("UWType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWType));
        }
        if (FCode.equals("UWRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWRate));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(UserCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(UserName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(UWGrade);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(UWType);
                break;
            case 4:
                strFieldValue = String.valueOf(UWRate);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("UserCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UserCode = FValue.trim();
            }
            else
            {
                UserCode = null;
            }
        }
        if (FCode.equals("UserName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UserName = FValue.trim();
            }
            else
            {
                UserName = null;
            }
        }
        if (FCode.equals("UWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
            {
                UWGrade = null;
            }
        }
        if (FCode.equals("UWType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWType = FValue.trim();
            }
            else
            {
                UWType = null;
            }
        }
        if (FCode.equals("UWRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                UWRate = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDSpotUWRateSchema other = (LDSpotUWRateSchema) otherObject;
        return
                UserCode.equals(other.getUserCode())
                && UserName.equals(other.getUserName())
                && UWGrade.equals(other.getUWGrade())
                && UWType.equals(other.getUWType())
                && UWRate == other.getUWRate();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("UserCode"))
        {
            return 0;
        }
        if (strFieldName.equals("UserName"))
        {
            return 1;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return 2;
        }
        if (strFieldName.equals("UWType"))
        {
            return 3;
        }
        if (strFieldName.equals("UWRate"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "UserCode";
                break;
            case 1:
                strFieldName = "UserName";
                break;
            case 2:
                strFieldName = "UWGrade";
                break;
            case 3:
                strFieldName = "UWType";
                break;
            case 4:
                strFieldName = "UWRate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("UserCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UserName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWRate"))
        {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
