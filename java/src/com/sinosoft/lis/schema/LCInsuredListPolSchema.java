/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCInsuredListPolDB;

/*
 * <p>ClassName: LCInsuredListPolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-10-24
 */
public class LCInsuredListPolSchema implements Schema, Cloneable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 被保人序号 */
    private String InsuredID;
    /** 合同号码 */
    private String ContNo;
    /** 保单险种号码 */
    private String PolNo;
    /** 保险计划 */
    private String ContPlanCode;
    /** 险种号码 */
    private String RiskCode;
    /** 期交保费 */
    private String Prem;
    /** 保额 */
    private String Amnt;
    /** 未满期保费 */
    private String EdorPrem;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCInsuredListPolSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "GrpContNo";
        pk[1] = "InsuredID";
        pk[2] = "RiskCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCInsuredListPolSchema cloned = (LCInsuredListPolSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getInsuredID() {
        return InsuredID;
    }

    public void setInsuredID(String aInsuredID) {
        InsuredID = aInsuredID;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }

    public String getContPlanCode() {
        return ContPlanCode;
    }

    public void setContPlanCode(String aContPlanCode) {
        ContPlanCode = aContPlanCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String aPrem) {
        Prem = aPrem;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String aAmnt) {
        Amnt = aAmnt;
    }

    public String getEdorPrem() {
        return EdorPrem;
    }

    public void setEdorPrem(String aEdorPrem) {
        EdorPrem = aEdorPrem;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCInsuredListPolSchema 对象给 Schema 赋值
     * @param: aLCInsuredListPolSchema LCInsuredListPolSchema
     **/
    public void setSchema(LCInsuredListPolSchema aLCInsuredListPolSchema) {
        this.GrpContNo = aLCInsuredListPolSchema.getGrpContNo();
        this.InsuredID = aLCInsuredListPolSchema.getInsuredID();
        this.ContNo = aLCInsuredListPolSchema.getContNo();
        this.PolNo = aLCInsuredListPolSchema.getPolNo();
        this.ContPlanCode = aLCInsuredListPolSchema.getContPlanCode();
        this.RiskCode = aLCInsuredListPolSchema.getRiskCode();
        this.Prem = aLCInsuredListPolSchema.getPrem();
        this.Amnt = aLCInsuredListPolSchema.getAmnt();
        this.EdorPrem = aLCInsuredListPolSchema.getEdorPrem();
        this.Operator = aLCInsuredListPolSchema.getOperator();
        this.MakeDate = fDate.getDate(aLCInsuredListPolSchema.getMakeDate());
        this.MakeTime = aLCInsuredListPolSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCInsuredListPolSchema.getModifyDate());
        this.ModifyTime = aLCInsuredListPolSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("InsuredID") == null) {
                this.InsuredID = null;
            } else {
                this.InsuredID = rs.getString("InsuredID").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PolNo") == null) {
                this.PolNo = null;
            } else {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("ContPlanCode") == null) {
                this.ContPlanCode = null;
            } else {
                this.ContPlanCode = rs.getString("ContPlanCode").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("Prem") == null) {
                this.Prem = null;
            } else {
                this.Prem = rs.getString("Prem").trim();
            }

            if (rs.getString("Amnt") == null) {
                this.Amnt = null;
            } else {
                this.Amnt = rs.getString("Amnt").trim();
            }

            if (rs.getString("EdorPrem") == null) {
                this.EdorPrem = null;
            } else {
                this.EdorPrem = rs.getString("EdorPrem").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LCInsuredListPol表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInsuredListPolSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCInsuredListPolSchema getSchema() {
        LCInsuredListPolSchema aLCInsuredListPolSchema = new
                LCInsuredListPolSchema();
        aLCInsuredListPolSchema.setSchema(this);
        return aLCInsuredListPolSchema;
    }

    public LCInsuredListPolDB getDB() {
        LCInsuredListPolDB aDBOper = new LCInsuredListPolDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsuredListPol描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContPlanCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Prem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Amnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorPrem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsuredListPol>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            InsuredID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            Prem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            Amnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                  SysConst.PACKAGESPILTER);
            EdorPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInsuredListPolSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("InsuredID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredID));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("ContPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equals("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equals("EdorPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorPrem));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(InsuredID);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(PolNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Prem);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Amnt);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(EdorPrem);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredID")) {
            if (FValue != null && !FValue.equals("")) {
                InsuredID = FValue.trim();
            } else {
                InsuredID = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if (FValue != null && !FValue.equals("")) {
                PolNo = FValue.trim();
            } else {
                PolNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            if (FValue != null && !FValue.equals("")) {
                ContPlanCode = FValue.trim();
            } else {
                ContPlanCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if (FValue != null && !FValue.equals("")) {
                Prem = FValue.trim();
            } else {
                Prem = null;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if (FValue != null && !FValue.equals("")) {
                Amnt = FValue.trim();
            } else {
                Amnt = null;
            }
        }
        if (FCode.equalsIgnoreCase("EdorPrem")) {
            if (FValue != null && !FValue.equals("")) {
                EdorPrem = FValue.trim();
            } else {
                EdorPrem = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LCInsuredListPolSchema other = (LCInsuredListPolSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && InsuredID.equals(other.getInsuredID())
                && ContNo.equals(other.getContNo())
                && PolNo.equals(other.getPolNo())
                && ContPlanCode.equals(other.getContPlanCode())
                && RiskCode.equals(other.getRiskCode())
                && Prem.equals(other.getPrem())
                && Amnt.equals(other.getAmnt())
                && EdorPrem.equals(other.getEdorPrem())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("GrpContNo")) {
            return 0;
        }
        if (strFieldName.equals("InsuredID")) {
            return 1;
        }
        if (strFieldName.equals("ContNo")) {
            return 2;
        }
        if (strFieldName.equals("PolNo")) {
            return 3;
        }
        if (strFieldName.equals("ContPlanCode")) {
            return 4;
        }
        if (strFieldName.equals("RiskCode")) {
            return 5;
        }
        if (strFieldName.equals("Prem")) {
            return 6;
        }
        if (strFieldName.equals("Amnt")) {
            return 7;
        }
        if (strFieldName.equals("EdorPrem")) {
            return 8;
        }
        if (strFieldName.equals("Operator")) {
            return 9;
        }
        if (strFieldName.equals("MakeDate")) {
            return 10;
        }
        if (strFieldName.equals("MakeTime")) {
            return 11;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 12;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "GrpContNo";
            break;
        case 1:
            strFieldName = "InsuredID";
            break;
        case 2:
            strFieldName = "ContNo";
            break;
        case 3:
            strFieldName = "PolNo";
            break;
        case 4:
            strFieldName = "ContPlanCode";
            break;
        case 5:
            strFieldName = "RiskCode";
            break;
        case 6:
            strFieldName = "Prem";
            break;
        case 7:
            strFieldName = "Amnt";
            break;
        case 8:
            strFieldName = "EdorPrem";
            break;
        case 9:
            strFieldName = "Operator";
            break;
        case 10:
            strFieldName = "MakeDate";
            break;
        case 11:
            strFieldName = "MakeTime";
            break;
        case 12:
            strFieldName = "ModifyDate";
            break;
        case 13:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Prem")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Amnt")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorPrem")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
