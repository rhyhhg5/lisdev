/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOPRTInvoiceManagerDB;

/*
 * <p>ClassName: LOPRTInvoiceManagerSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 发票打印导出接口
 * @CreateDate：2007-11-16
 */
public class LOPRTInvoiceManagerSchema implements Schema, Cloneable
{
	// @Field
	/** 发票号 */
	private String InvoiceNo;
	/** 发票代码 */
	private String InvoiceCode;
	/** 发票扩展代码 */
	private String InvoiceCodeEx;
	/** 保单号 */
	private String ContNo;
	/** 批单号 */
	private String EndorNo;
	/** 金额 */
	private double XSumMoney;
	/** 作废标志 */
	private String StateFlag;
	/** 付款方纳税人识别号 */
	private String PayerCode;
	/** 付款方名称 */
	private String PayerName;
	/** 付款方地址 */
	private String PayerAddr;
	/** 收款方纳税人识别号 */
	private String PayeeCode;
	/** 收款方名称 */
	private String PayeeName;
	/** 收款人 */
	private String Payee;
	/** 开票人（操作员） */
	private String Operator;
	/** 开票日期 */
	private Date Opdate;
	/** 开票备注 */
	private String Memo;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 机构代码 */
	private String ComCode;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LOPRTInvoiceManagerSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "InvoiceNo";
		pk[1] = "InvoiceCode";
		pk[2] = "ComCode";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LOPRTInvoiceManagerSchema cloned = (LOPRTInvoiceManagerSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getInvoiceNo()
	{
		return InvoiceNo;
	}
	public void setInvoiceNo(String aInvoiceNo)
	{
            InvoiceNo = aInvoiceNo;
	}
	public String getInvoiceCode()
	{
		return InvoiceCode;
	}
	public void setInvoiceCode(String aInvoiceCode)
	{
            InvoiceCode = aInvoiceCode;
	}
	public String getInvoiceCodeEx()
	{
		return InvoiceCodeEx;
	}
	public void setInvoiceCodeEx(String aInvoiceCodeEx)
	{
            InvoiceCodeEx = aInvoiceCodeEx;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
            ContNo = aContNo;
	}
	public String getEndorNo()
	{
		return EndorNo;
	}
	public void setEndorNo(String aEndorNo)
	{
            EndorNo = aEndorNo;
	}
	public double getXSumMoney()
	{
		return XSumMoney;
	}
	public void setXSumMoney(double aXSumMoney)
	{
            XSumMoney = Arith.round(aXSumMoney,2);
	}
	public void setXSumMoney(String aXSumMoney)
	{
		if (aXSumMoney != null && !aXSumMoney.equals(""))
		{
			Double tDouble = new Double(aXSumMoney);
			double d = tDouble.doubleValue();
                XSumMoney = Arith.round(d,2);
		}
	}

	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
            StateFlag = aStateFlag;
	}
	public String getPayerCode()
	{
		return PayerCode;
	}
	public void setPayerCode(String aPayerCode)
	{
            PayerCode = aPayerCode;
	}
	public String getPayerName()
	{
		return PayerName;
	}
	public void setPayerName(String aPayerName)
	{
            PayerName = aPayerName;
	}
	public String getPayerAddr()
	{
		return PayerAddr;
	}
	public void setPayerAddr(String aPayerAddr)
	{
            PayerAddr = aPayerAddr;
	}
	public String getPayeeCode()
	{
		return PayeeCode;
	}
	public void setPayeeCode(String aPayeeCode)
	{
            PayeeCode = aPayeeCode;
	}
	public String getPayeeName()
	{
		return PayeeName;
	}
	public void setPayeeName(String aPayeeName)
	{
            PayeeName = aPayeeName;
	}
	public String getPayee()
	{
		return Payee;
	}
	public void setPayee(String aPayee)
	{
            Payee = aPayee;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getOpdate()
	{
		if( Opdate != null )
			return fDate.getString(Opdate);
		else
			return null;
	}
	public void setOpdate(Date aOpdate)
	{
            Opdate = aOpdate;
	}
	public void setOpdate(String aOpdate)
	{
		if (aOpdate != null && !aOpdate.equals("") )
		{
			Opdate = fDate.getDate( aOpdate );
		}
		else
			Opdate = null;
	}

	public String getMemo()
	{
		return Memo;
	}
	public void setMemo(String aMemo)
	{
            Memo = aMemo;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getComCode()
	{
		return ComCode;
	}
	public void setComCode(String aComCode)
	{
            ComCode = aComCode;
	}

	/**
	* 使用另外一个 LOPRTInvoiceManagerSchema 对象给 Schema 赋值
	* @param: aLOPRTInvoiceManagerSchema LOPRTInvoiceManagerSchema
	**/
	public void setSchema(LOPRTInvoiceManagerSchema aLOPRTInvoiceManagerSchema)
	{
		this.InvoiceNo = aLOPRTInvoiceManagerSchema.getInvoiceNo();
		this.InvoiceCode = aLOPRTInvoiceManagerSchema.getInvoiceCode();
		this.InvoiceCodeEx = aLOPRTInvoiceManagerSchema.getInvoiceCodeEx();
		this.ContNo = aLOPRTInvoiceManagerSchema.getContNo();
		this.EndorNo = aLOPRTInvoiceManagerSchema.getEndorNo();
		this.XSumMoney = aLOPRTInvoiceManagerSchema.getXSumMoney();
		this.StateFlag = aLOPRTInvoiceManagerSchema.getStateFlag();
		this.PayerCode = aLOPRTInvoiceManagerSchema.getPayerCode();
		this.PayerName = aLOPRTInvoiceManagerSchema.getPayerName();
		this.PayerAddr = aLOPRTInvoiceManagerSchema.getPayerAddr();
		this.PayeeCode = aLOPRTInvoiceManagerSchema.getPayeeCode();
		this.PayeeName = aLOPRTInvoiceManagerSchema.getPayeeName();
		this.Payee = aLOPRTInvoiceManagerSchema.getPayee();
		this.Operator = aLOPRTInvoiceManagerSchema.getOperator();
		this.Opdate = fDate.getDate( aLOPRTInvoiceManagerSchema.getOpdate());
		this.Memo = aLOPRTInvoiceManagerSchema.getMemo();
		this.MakeDate = fDate.getDate( aLOPRTInvoiceManagerSchema.getMakeDate());
		this.MakeTime = aLOPRTInvoiceManagerSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLOPRTInvoiceManagerSchema.getModifyDate());
		this.ModifyTime = aLOPRTInvoiceManagerSchema.getModifyTime();
		this.ComCode = aLOPRTInvoiceManagerSchema.getComCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("InvoiceNo") == null )
				this.InvoiceNo = null;
			else
				this.InvoiceNo = rs.getString("InvoiceNo").trim();

			if( rs.getString("InvoiceCode") == null )
				this.InvoiceCode = null;
			else
				this.InvoiceCode = rs.getString("InvoiceCode").trim();

			if( rs.getString("InvoiceCodeEx") == null )
				this.InvoiceCodeEx = null;
			else
				this.InvoiceCodeEx = rs.getString("InvoiceCodeEx").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("EndorNo") == null )
				this.EndorNo = null;
			else
				this.EndorNo = rs.getString("EndorNo").trim();

			this.XSumMoney = rs.getDouble("XSumMoney");
			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("PayerCode") == null )
				this.PayerCode = null;
			else
				this.PayerCode = rs.getString("PayerCode").trim();

			if( rs.getString("PayerName") == null )
				this.PayerName = null;
			else
				this.PayerName = rs.getString("PayerName").trim();

			if( rs.getString("PayerAddr") == null )
				this.PayerAddr = null;
			else
				this.PayerAddr = rs.getString("PayerAddr").trim();

			if( rs.getString("PayeeCode") == null )
				this.PayeeCode = null;
			else
				this.PayeeCode = rs.getString("PayeeCode").trim();

			if( rs.getString("PayeeName") == null )
				this.PayeeName = null;
			else
				this.PayeeName = rs.getString("PayeeName").trim();

			if( rs.getString("Payee") == null )
				this.Payee = null;
			else
				this.Payee = rs.getString("Payee").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Opdate = rs.getDate("Opdate");
			if( rs.getString("Memo") == null )
				this.Memo = null;
			else
				this.Memo = rs.getString("Memo").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("ComCode") == null )
				this.ComCode = null;
			else
				this.ComCode = rs.getString("ComCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LOPRTInvoiceManager表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOPRTInvoiceManagerSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LOPRTInvoiceManagerSchema getSchema()
	{
		LOPRTInvoiceManagerSchema aLOPRTInvoiceManagerSchema = new LOPRTInvoiceManagerSchema();
		aLOPRTInvoiceManagerSchema.setSchema(this);
		return aLOPRTInvoiceManagerSchema;
	}

	public LOPRTInvoiceManagerDB getDB()
	{
		LOPRTInvoiceManagerDB aDBOper = new LOPRTInvoiceManagerDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOPRTInvoiceManager描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(InvoiceNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InvoiceCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InvoiceCodeEx)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(EndorNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(XSumMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayerCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayerName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayerAddr)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayeeCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayeeName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Payee)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( Opdate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Memo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ComCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOPRTInvoiceManager>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			InvoiceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			InvoiceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			InvoiceCodeEx = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			EndorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			XSumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			PayerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PayerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PayerAddr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			PayeeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			PayeeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Payee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Opdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			Memo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOPRTInvoiceManagerSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("InvoiceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvoiceNo));
		}
		if (FCode.equals("InvoiceCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvoiceCode));
		}
		if (FCode.equals("InvoiceCodeEx"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvoiceCodeEx));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("EndorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndorNo));
		}
		if (FCode.equals("XSumMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(XSumMoney));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("PayerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayerCode));
		}
		if (FCode.equals("PayerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayerName));
		}
		if (FCode.equals("PayerAddr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayerAddr));
		}
		if (FCode.equals("PayeeCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayeeCode));
		}
		if (FCode.equals("PayeeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayeeName));
		}
		if (FCode.equals("Payee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Payee));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Opdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOpdate()));
		}
		if (FCode.equals("Memo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Memo));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(InvoiceNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(InvoiceCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(InvoiceCodeEx);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(EndorNo);
				break;
			case 5:
				strFieldValue = String.valueOf(XSumMoney);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(PayerCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PayerName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PayerAddr);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(PayeeCode);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(PayeeName);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Payee);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOpdate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Memo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ComCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("InvoiceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvoiceNo = FValue.trim();
			}
			else
				InvoiceNo = null;
		}
		if (FCode.equalsIgnoreCase("InvoiceCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvoiceCode = FValue.trim();
			}
			else
				InvoiceCode = null;
		}
		if (FCode.equalsIgnoreCase("InvoiceCodeEx"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvoiceCodeEx = FValue.trim();
			}
			else
				InvoiceCodeEx = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("EndorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndorNo = FValue.trim();
			}
			else
				EndorNo = null;
		}
		if (FCode.equalsIgnoreCase("XSumMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				XSumMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayerCode = FValue.trim();
			}
			else
				PayerCode = null;
		}
		if (FCode.equalsIgnoreCase("PayerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayerName = FValue.trim();
			}
			else
				PayerName = null;
		}
		if (FCode.equalsIgnoreCase("PayerAddr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayerAddr = FValue.trim();
			}
			else
				PayerAddr = null;
		}
		if (FCode.equalsIgnoreCase("PayeeCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayeeCode = FValue.trim();
			}
			else
				PayeeCode = null;
		}
		if (FCode.equalsIgnoreCase("PayeeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayeeName = FValue.trim();
			}
			else
				PayeeName = null;
		}
		if (FCode.equalsIgnoreCase("Payee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Payee = FValue.trim();
			}
			else
				Payee = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Opdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Opdate = fDate.getDate( FValue );
			}
			else
				Opdate = null;
		}
		if (FCode.equalsIgnoreCase("Memo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Memo = FValue.trim();
			}
			else
				Memo = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCode = FValue.trim();
			}
			else
				ComCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LOPRTInvoiceManagerSchema other = (LOPRTInvoiceManagerSchema)otherObject;
		return
			InvoiceNo.equals(other.getInvoiceNo())
			&& InvoiceCode.equals(other.getInvoiceCode())
			&& InvoiceCodeEx.equals(other.getInvoiceCodeEx())
			&& ContNo.equals(other.getContNo())
			&& EndorNo.equals(other.getEndorNo())
			&& XSumMoney == other.getXSumMoney()
			&& StateFlag.equals(other.getStateFlag())
			&& PayerCode.equals(other.getPayerCode())
			&& PayerName.equals(other.getPayerName())
			&& PayerAddr.equals(other.getPayerAddr())
			&& PayeeCode.equals(other.getPayeeCode())
			&& PayeeName.equals(other.getPayeeName())
			&& Payee.equals(other.getPayee())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(Opdate).equals(other.getOpdate())
			&& Memo.equals(other.getMemo())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& ComCode.equals(other.getComCode());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("InvoiceNo") ) {
			return 0;
		}
		if( strFieldName.equals("InvoiceCode") ) {
			return 1;
		}
		if( strFieldName.equals("InvoiceCodeEx") ) {
			return 2;
		}
		if( strFieldName.equals("ContNo") ) {
			return 3;
		}
		if( strFieldName.equals("EndorNo") ) {
			return 4;
		}
		if( strFieldName.equals("XSumMoney") ) {
			return 5;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 6;
		}
		if( strFieldName.equals("PayerCode") ) {
			return 7;
		}
		if( strFieldName.equals("PayerName") ) {
			return 8;
		}
		if( strFieldName.equals("PayerAddr") ) {
			return 9;
		}
		if( strFieldName.equals("PayeeCode") ) {
			return 10;
		}
		if( strFieldName.equals("PayeeName") ) {
			return 11;
		}
		if( strFieldName.equals("Payee") ) {
			return 12;
		}
		if( strFieldName.equals("Operator") ) {
			return 13;
		}
		if( strFieldName.equals("Opdate") ) {
			return 14;
		}
		if( strFieldName.equals("Memo") ) {
			return 15;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 16;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 19;
		}
		if( strFieldName.equals("ComCode") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "InvoiceNo";
				break;
			case 1:
				strFieldName = "InvoiceCode";
				break;
			case 2:
				strFieldName = "InvoiceCodeEx";
				break;
			case 3:
				strFieldName = "ContNo";
				break;
			case 4:
				strFieldName = "EndorNo";
				break;
			case 5:
				strFieldName = "XSumMoney";
				break;
			case 6:
				strFieldName = "StateFlag";
				break;
			case 7:
				strFieldName = "PayerCode";
				break;
			case 8:
				strFieldName = "PayerName";
				break;
			case 9:
				strFieldName = "PayerAddr";
				break;
			case 10:
				strFieldName = "PayeeCode";
				break;
			case 11:
				strFieldName = "PayeeName";
				break;
			case 12:
				strFieldName = "Payee";
				break;
			case 13:
				strFieldName = "Operator";
				break;
			case 14:
				strFieldName = "Opdate";
				break;
			case 15:
				strFieldName = "Memo";
				break;
			case 16:
				strFieldName = "MakeDate";
				break;
			case 17:
				strFieldName = "MakeTime";
				break;
			case 18:
				strFieldName = "ModifyDate";
				break;
			case 19:
				strFieldName = "ModifyTime";
				break;
			case 20:
				strFieldName = "ComCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("InvoiceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InvoiceCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InvoiceCodeEx") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("XSumMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayerAddr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayeeCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayeeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Payee") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Opdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Memo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
