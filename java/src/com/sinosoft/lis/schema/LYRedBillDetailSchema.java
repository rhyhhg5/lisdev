/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LYRedBillDetailDB;

/*
 * <p>ClassName: LYRedBillDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 营改增红蓝票关联表
 * @CreateDate：2018-11-14
 */
public class LYRedBillDetailSchema implements Schema, Cloneable
{
	// @Field
	/** 蓝字发票流水号 */
	private String BlueBusiNo;
	/** 开票方式 */
	private String OpenWay;
	/** 核保标识 */
	private String SrcSystem;
	/** 红字发票流水号 */
	private String RedBusiNo;
	/** 推送日期 */
	private Date CZOldDate;
	/** 推送状态 */
	private String State;
	/** 失败原因 */
	private String ErrorReason;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LYRedBillDetailSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BlueBusiNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LYRedBillDetailSchema cloned = (LYRedBillDetailSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBlueBusiNo()
	{
		return BlueBusiNo;
	}
	public void setBlueBusiNo(String aBlueBusiNo)
	{
		BlueBusiNo = aBlueBusiNo;
	}
	public String getOpenWay()
	{
		return OpenWay;
	}
	public void setOpenWay(String aOpenWay)
	{
		OpenWay = aOpenWay;
	}
	public String getSrcSystem()
	{
		return SrcSystem;
	}
	public void setSrcSystem(String aSrcSystem)
	{
		SrcSystem = aSrcSystem;
	}
	public String getRedBusiNo()
	{
		return RedBusiNo;
	}
	public void setRedBusiNo(String aRedBusiNo)
	{
		RedBusiNo = aRedBusiNo;
	}
	public String getCZOldDate()
	{
		if( CZOldDate != null )
			return fDate.getString(CZOldDate);
		else
			return null;
	}
	public void setCZOldDate(Date aCZOldDate)
	{
		CZOldDate = aCZOldDate;
	}
	public void setCZOldDate(String aCZOldDate)
	{
		if (aCZOldDate != null && !aCZOldDate.equals("") )
		{
			CZOldDate = fDate.getDate( aCZOldDate );
		}
		else
			CZOldDate = null;
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getErrorReason()
	{
		return ErrorReason;
	}
	public void setErrorReason(String aErrorReason)
	{
		ErrorReason = aErrorReason;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LYRedBillDetailSchema 对象给 Schema 赋值
	* @param: aLYRedBillDetailSchema LYRedBillDetailSchema
	**/
	public void setSchema(LYRedBillDetailSchema aLYRedBillDetailSchema)
	{
		this.BlueBusiNo = aLYRedBillDetailSchema.getBlueBusiNo();
		this.OpenWay = aLYRedBillDetailSchema.getOpenWay();
		this.SrcSystem = aLYRedBillDetailSchema.getSrcSystem();
		this.RedBusiNo = aLYRedBillDetailSchema.getRedBusiNo();
		this.CZOldDate = fDate.getDate( aLYRedBillDetailSchema.getCZOldDate());
		this.State = aLYRedBillDetailSchema.getState();
		this.ErrorReason = aLYRedBillDetailSchema.getErrorReason();
		this.MakeDate = fDate.getDate( aLYRedBillDetailSchema.getMakeDate());
		this.MakeTime = aLYRedBillDetailSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLYRedBillDetailSchema.getModifyDate());
		this.ModifyTime = aLYRedBillDetailSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BlueBusiNo") == null )
				this.BlueBusiNo = null;
			else
				this.BlueBusiNo = rs.getString("BlueBusiNo").trim();

			if( rs.getString("OpenWay") == null )
				this.OpenWay = null;
			else
				this.OpenWay = rs.getString("OpenWay").trim();

			if( rs.getString("SrcSystem") == null )
				this.SrcSystem = null;
			else
				this.SrcSystem = rs.getString("SrcSystem").trim();

			if( rs.getString("RedBusiNo") == null )
				this.RedBusiNo = null;
			else
				this.RedBusiNo = rs.getString("RedBusiNo").trim();

			this.CZOldDate = rs.getDate("CZOldDate");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("ErrorReason") == null )
				this.ErrorReason = null;
			else
				this.ErrorReason = rs.getString("ErrorReason").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LYRedBillDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LYRedBillDetailSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LYRedBillDetailSchema getSchema()
	{
		LYRedBillDetailSchema aLYRedBillDetailSchema = new LYRedBillDetailSchema();
		aLYRedBillDetailSchema.setSchema(this);
		return aLYRedBillDetailSchema;
	}

	public LYRedBillDetailDB getDB()
	{
		LYRedBillDetailDB aDBOper = new LYRedBillDetailDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYRedBillDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BlueBusiNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OpenWay)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SrcSystem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RedBusiNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CZOldDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYRedBillDetail>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BlueBusiNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			OpenWay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SrcSystem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RedBusiNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CZOldDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ErrorReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LYRedBillDetailSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BlueBusiNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlueBusiNo));
		}
		if (FCode.equals("OpenWay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OpenWay));
		}
		if (FCode.equals("SrcSystem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SrcSystem));
		}
		if (FCode.equals("RedBusiNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RedBusiNo));
		}
		if (FCode.equals("CZOldDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCZOldDate()));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ErrorReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorReason));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BlueBusiNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(OpenWay);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SrcSystem);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RedBusiNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCZOldDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ErrorReason);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BlueBusiNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlueBusiNo = FValue.trim();
			}
			else
				BlueBusiNo = null;
		}
		if (FCode.equalsIgnoreCase("OpenWay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OpenWay = FValue.trim();
			}
			else
				OpenWay = null;
		}
		if (FCode.equalsIgnoreCase("SrcSystem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SrcSystem = FValue.trim();
			}
			else
				SrcSystem = null;
		}
		if (FCode.equalsIgnoreCase("RedBusiNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RedBusiNo = FValue.trim();
			}
			else
				RedBusiNo = null;
		}
		if (FCode.equalsIgnoreCase("CZOldDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CZOldDate = fDate.getDate( FValue );
			}
			else
				CZOldDate = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ErrorReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorReason = FValue.trim();
			}
			else
				ErrorReason = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LYRedBillDetailSchema other = (LYRedBillDetailSchema)otherObject;
		return
			(BlueBusiNo == null ? other.getBlueBusiNo() == null : BlueBusiNo.equals(other.getBlueBusiNo()))
			&& (OpenWay == null ? other.getOpenWay() == null : OpenWay.equals(other.getOpenWay()))
			&& (SrcSystem == null ? other.getSrcSystem() == null : SrcSystem.equals(other.getSrcSystem()))
			&& (RedBusiNo == null ? other.getRedBusiNo() == null : RedBusiNo.equals(other.getRedBusiNo()))
			&& (CZOldDate == null ? other.getCZOldDate() == null : fDate.getString(CZOldDate).equals(other.getCZOldDate()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (ErrorReason == null ? other.getErrorReason() == null : ErrorReason.equals(other.getErrorReason()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BlueBusiNo") ) {
			return 0;
		}
		if( strFieldName.equals("OpenWay") ) {
			return 1;
		}
		if( strFieldName.equals("SrcSystem") ) {
			return 2;
		}
		if( strFieldName.equals("RedBusiNo") ) {
			return 3;
		}
		if( strFieldName.equals("CZOldDate") ) {
			return 4;
		}
		if( strFieldName.equals("State") ) {
			return 5;
		}
		if( strFieldName.equals("ErrorReason") ) {
			return 6;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 7;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 8;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BlueBusiNo";
				break;
			case 1:
				strFieldName = "OpenWay";
				break;
			case 2:
				strFieldName = "SrcSystem";
				break;
			case 3:
				strFieldName = "RedBusiNo";
				break;
			case 4:
				strFieldName = "CZOldDate";
				break;
			case 5:
				strFieldName = "State";
				break;
			case 6:
				strFieldName = "ErrorReason";
				break;
			case 7:
				strFieldName = "MakeDate";
				break;
			case 8:
				strFieldName = "MakeTime";
				break;
			case 9:
				strFieldName = "ModifyDate";
				break;
			case 10:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BlueBusiNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OpenWay") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SrcSystem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RedBusiNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CZOldDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
