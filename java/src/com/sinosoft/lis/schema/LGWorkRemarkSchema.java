/*
 * <p>ClassName: LGWorkRemarkSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工单管理修改请求
 * @CreateDate：2005-01-19
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LGWorkRemarkDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LGWorkRemarkSchema implements Schema
{
    // @Field
    /** 作业编号 */
    private String WorkNo;
    /** 结点编号 */
    private String NodeNo;
    /** 批注编号 */
    private String RemarkNo;
    /** 批注类型编号 */
    private String RemarkTypeNo;
    /** 批注日期 */
    private Date RemarkDate;
    /** 批注时间 */
    private String RemarkTime;
    /** 批注内容 */
    private String RemarkContent;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LGWorkRemarkSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "WorkNo";
        pk[1] = "NodeNo";
        pk[2] = "RemarkNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getWorkNo()
    {
        if (WorkNo != null && !WorkNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            WorkNo = StrTool.unicodeToGBK(WorkNo);
        }
        return WorkNo;
    }

    public void setWorkNo(String aWorkNo)
    {
        WorkNo = aWorkNo;
    }

    public String getNodeNo()
    {
        if (NodeNo != null && !NodeNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            NodeNo = StrTool.unicodeToGBK(NodeNo);
        }
        return NodeNo;
    }

    public void setNodeNo(String aNodeNo)
    {
        NodeNo = aNodeNo;
    }

    public String getRemarkNo()
    {
        if (RemarkNo != null && !RemarkNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            RemarkNo = StrTool.unicodeToGBK(RemarkNo);
        }
        return RemarkNo;
    }

    public void setRemarkNo(String aRemarkNo)
    {
        RemarkNo = aRemarkNo;
    }

    public String getRemarkTypeNo()
    {
        if (RemarkTypeNo != null && !RemarkTypeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RemarkTypeNo = StrTool.unicodeToGBK(RemarkTypeNo);
        }
        return RemarkTypeNo;
    }

    public void setRemarkTypeNo(String aRemarkTypeNo)
    {
        RemarkTypeNo = aRemarkTypeNo;
    }

    public String getRemarkDate()
    {
        if (RemarkDate != null)
        {
            return fDate.getString(RemarkDate);
        }
        else
        {
            return null;
        }
    }

    public void setRemarkDate(Date aRemarkDate)
    {
        RemarkDate = aRemarkDate;
    }

    public void setRemarkDate(String aRemarkDate)
    {
        if (aRemarkDate != null && !aRemarkDate.equals(""))
        {
            RemarkDate = fDate.getDate(aRemarkDate);
        }
        else
        {
            RemarkDate = null;
        }
    }

    public String getRemarkTime()
    {
        if (RemarkTime != null && !RemarkTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RemarkTime = StrTool.unicodeToGBK(RemarkTime);
        }
        return RemarkTime;
    }

    public void setRemarkTime(String aRemarkTime)
    {
        RemarkTime = aRemarkTime;
    }

    public String getRemarkContent()
    {
        if (RemarkContent != null && !RemarkContent.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RemarkContent = StrTool.unicodeToGBK(RemarkContent);
        }
        return RemarkContent;
    }

    public void setRemarkContent(String aRemarkContent)
    {
        RemarkContent = aRemarkContent;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LGWorkRemarkSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LGWorkRemarkSchema aLGWorkRemarkSchema)
    {
        this.WorkNo = aLGWorkRemarkSchema.getWorkNo();
        this.NodeNo = aLGWorkRemarkSchema.getNodeNo();
        this.RemarkNo = aLGWorkRemarkSchema.getRemarkNo();
        this.RemarkTypeNo = aLGWorkRemarkSchema.getRemarkTypeNo();
        this.RemarkDate = fDate.getDate(aLGWorkRemarkSchema.getRemarkDate());
        this.RemarkTime = aLGWorkRemarkSchema.getRemarkTime();
        this.RemarkContent = aLGWorkRemarkSchema.getRemarkContent();
        this.Operator = aLGWorkRemarkSchema.getOperator();
        this.MakeDate = fDate.getDate(aLGWorkRemarkSchema.getMakeDate());
        this.MakeTime = aLGWorkRemarkSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLGWorkRemarkSchema.getModifyDate());
        this.ModifyTime = aLGWorkRemarkSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("WorkNo") == null)
            {
                this.WorkNo = null;
            }
            else
            {
                this.WorkNo = rs.getString("WorkNo").trim();
            }

            if (rs.getString("NodeNo") == null)
            {
                this.NodeNo = null;
            }
            else
            {
                this.NodeNo = rs.getString("NodeNo").trim();
            }

            if (rs.getString("RemarkNo") == null)
            {
                this.RemarkNo = null;
            }
            else
            {
                this.RemarkNo = rs.getString("RemarkNo").trim();
            }

            if (rs.getString("RemarkTypeNo") == null)
            {
                this.RemarkTypeNo = null;
            }
            else
            {
                this.RemarkTypeNo = rs.getString("RemarkTypeNo").trim();
            }

            this.RemarkDate = rs.getDate("RemarkDate");
            if (rs.getString("RemarkTime") == null)
            {
                this.RemarkTime = null;
            }
            else
            {
                this.RemarkTime = rs.getString("RemarkTime").trim();
            }

            if (rs.getString("RemarkContent") == null)
            {
                this.RemarkContent = null;
            }
            else
            {
                this.RemarkContent = rs.getString("RemarkContent").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGWorkRemarkSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LGWorkRemarkSchema getSchema()
    {
        LGWorkRemarkSchema aLGWorkRemarkSchema = new LGWorkRemarkSchema();
        aLGWorkRemarkSchema.setSchema(this);
        return aLGWorkRemarkSchema;
    }

    public LGWorkRemarkDB getDB()
    {
        LGWorkRemarkDB aDBOper = new LGWorkRemarkDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGWorkRemark描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(WorkNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NodeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RemarkNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RemarkTypeNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            RemarkDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RemarkTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RemarkContent)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGWorkRemark>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            WorkNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            NodeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            RemarkNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RemarkTypeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            RemarkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            RemarkTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            RemarkContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGWorkRemarkSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("WorkNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WorkNo));
        }
        if (FCode.equals("NodeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NodeNo));
        }
        if (FCode.equals("RemarkNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RemarkNo));
        }
        if (FCode.equals("RemarkTypeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RemarkTypeNo));
        }
        if (FCode.equals("RemarkDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getRemarkDate()));
        }
        if (FCode.equals("RemarkTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RemarkTime));
        }
        if (FCode.equals("RemarkContent"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RemarkContent));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(WorkNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(NodeNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RemarkNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RemarkTypeNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getRemarkDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(RemarkTime);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RemarkContent);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("WorkNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkNo = FValue.trim();
            }
            else
            {
                WorkNo = null;
            }
        }
        if (FCode.equals("NodeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NodeNo = FValue.trim();
            }
            else
            {
                NodeNo = null;
            }
        }
        if (FCode.equals("RemarkNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RemarkNo = FValue.trim();
            }
            else
            {
                RemarkNo = null;
            }
        }
        if (FCode.equals("RemarkTypeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RemarkTypeNo = FValue.trim();
            }
            else
            {
                RemarkTypeNo = null;
            }
        }
        if (FCode.equals("RemarkDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RemarkDate = fDate.getDate(FValue);
            }
            else
            {
                RemarkDate = null;
            }
        }
        if (FCode.equals("RemarkTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RemarkTime = FValue.trim();
            }
            else
            {
                RemarkTime = null;
            }
        }
        if (FCode.equals("RemarkContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RemarkContent = FValue.trim();
            }
            else
            {
                RemarkContent = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LGWorkRemarkSchema other = (LGWorkRemarkSchema) otherObject;
        return
                WorkNo.equals(other.getWorkNo())
                && NodeNo.equals(other.getNodeNo())
                && RemarkNo.equals(other.getRemarkNo())
                && RemarkTypeNo.equals(other.getRemarkTypeNo())
                && fDate.getString(RemarkDate).equals(other.getRemarkDate())
                && RemarkTime.equals(other.getRemarkTime())
                && RemarkContent.equals(other.getRemarkContent())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("WorkNo"))
        {
            return 0;
        }
        if (strFieldName.equals("NodeNo"))
        {
            return 1;
        }
        if (strFieldName.equals("RemarkNo"))
        {
            return 2;
        }
        if (strFieldName.equals("RemarkTypeNo"))
        {
            return 3;
        }
        if (strFieldName.equals("RemarkDate"))
        {
            return 4;
        }
        if (strFieldName.equals("RemarkTime"))
        {
            return 5;
        }
        if (strFieldName.equals("RemarkContent"))
        {
            return 6;
        }
        if (strFieldName.equals("Operator"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "WorkNo";
                break;
            case 1:
                strFieldName = "NodeNo";
                break;
            case 2:
                strFieldName = "RemarkNo";
                break;
            case 3:
                strFieldName = "RemarkTypeNo";
                break;
            case 4:
                strFieldName = "RemarkDate";
                break;
            case 5:
                strFieldName = "RemarkTime";
                break;
            case 6:
                strFieldName = "RemarkContent";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("WorkNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NodeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RemarkNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RemarkTypeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RemarkDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RemarkTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RemarkContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
