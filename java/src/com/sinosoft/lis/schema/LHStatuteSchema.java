/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHStatuteDB;

/*
 * <p>ClassName: LHStatuteSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康管理表修改_2005-12-06
 * @CreateDate：2005-12-06
 */
public class LHStatuteSchema implements Schema, Cloneable {
    // @Field
    /** 法规代码 */
    private String StatuteNo;
    /** 法规标题 */
    private String StatuteTitle;
    /** 发文号 */
    private String DocumentNum;
    /** 内容分类代码 */
    private String ContTypeCode;
    /** 颁布单位 */
    private String IssueOrganCode;
    /** 颁布日期 */
    private Date IssueDate;
    /** 实施日期 */
    private Date StartDate;
    /** 终止日期 */
    private Date EndDate;
    /** 内容简介 */
    private String DocContent;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHStatuteSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "StatuteNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHStatuteSchema cloned = (LHStatuteSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getStatuteNo() {
        return StatuteNo;
    }

    public void setStatuteNo(String aStatuteNo) {
        StatuteNo = aStatuteNo;
    }

    public String getStatuteTitle() {
        return StatuteTitle;
    }

    public void setStatuteTitle(String aStatuteTitle) {
        StatuteTitle = aStatuteTitle;
    }

    public String getDocumentNum() {
        return DocumentNum;
    }

    public void setDocumentNum(String aDocumentNum) {
        DocumentNum = aDocumentNum;
    }

    public String getContTypeCode() {
        return ContTypeCode;
    }

    public void setContTypeCode(String aContTypeCode) {
        ContTypeCode = aContTypeCode;
    }

    public String getIssueOrganCode() {
        return IssueOrganCode;
    }

    public void setIssueOrganCode(String aIssueOrganCode) {
        IssueOrganCode = aIssueOrganCode;
    }

    public String getIssueDate() {
        if (IssueDate != null) {
            return fDate.getString(IssueDate);
        } else {
            return null;
        }
    }

    public void setIssueDate(Date aIssueDate) {
        IssueDate = aIssueDate;
    }

    public void setIssueDate(String aIssueDate) {
        if (aIssueDate != null && !aIssueDate.equals("")) {
            IssueDate = fDate.getDate(aIssueDate);
        } else {
            IssueDate = null;
        }
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public String getDocContent() {
        return DocContent;
    }

    public void setDocContent(String aDocContent) {
        DocContent = aDocContent;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    /**
     * 使用另外一个 LHStatuteSchema 对象给 Schema 赋值
     * @param: aLHStatuteSchema LHStatuteSchema
     **/
    public void setSchema(LHStatuteSchema aLHStatuteSchema) {
        this.StatuteNo = aLHStatuteSchema.getStatuteNo();
        this.StatuteTitle = aLHStatuteSchema.getStatuteTitle();
        this.DocumentNum = aLHStatuteSchema.getDocumentNum();
        this.ContTypeCode = aLHStatuteSchema.getContTypeCode();
        this.IssueOrganCode = aLHStatuteSchema.getIssueOrganCode();
        this.IssueDate = fDate.getDate(aLHStatuteSchema.getIssueDate());
        this.StartDate = fDate.getDate(aLHStatuteSchema.getStartDate());
        this.EndDate = fDate.getDate(aLHStatuteSchema.getEndDate());
        this.DocContent = aLHStatuteSchema.getDocContent();
        this.Operator = aLHStatuteSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHStatuteSchema.getMakeDate());
        this.MakeTime = aLHStatuteSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHStatuteSchema.getModifyDate());
        this.ModifyTime = aLHStatuteSchema.getModifyTime();
        this.ManageCom = aLHStatuteSchema.getManageCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("StatuteNo") == null) {
                this.StatuteNo = null;
            } else {
                this.StatuteNo = rs.getString("StatuteNo").trim();
            }

            if (rs.getString("StatuteTitle") == null) {
                this.StatuteTitle = null;
            } else {
                this.StatuteTitle = rs.getString("StatuteTitle").trim();
            }

            if (rs.getString("DocumentNum") == null) {
                this.DocumentNum = null;
            } else {
                this.DocumentNum = rs.getString("DocumentNum").trim();
            }

            if (rs.getString("ContTypeCode") == null) {
                this.ContTypeCode = null;
            } else {
                this.ContTypeCode = rs.getString("ContTypeCode").trim();
            }

            if (rs.getString("IssueOrganCode") == null) {
                this.IssueOrganCode = null;
            } else {
                this.IssueOrganCode = rs.getString("IssueOrganCode").trim();
            }

            this.IssueDate = rs.getDate("IssueDate");
            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("DocContent") == null) {
                this.DocContent = null;
            } else {
                this.DocContent = rs.getString("DocContent").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHStatute表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHStatuteSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHStatuteSchema getSchema() {
        LHStatuteSchema aLHStatuteSchema = new LHStatuteSchema();
        aLHStatuteSchema.setSchema(this);
        return aLHStatuteSchema;
    }

    public LHStatuteDB getDB() {
        LHStatuteDB aDBOper = new LHStatuteDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHStatute描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StatuteNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StatuteTitle));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DocumentNum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContTypeCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IssueOrganCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(IssueDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DocContent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHStatute>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            StatuteNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            StatuteTitle = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            DocumentNum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            ContTypeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            IssueOrganCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                            SysConst.PACKAGESPILTER);
            IssueDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            DocContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHStatuteSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("StatuteNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StatuteNo));
        }
        if (FCode.equals("StatuteTitle")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StatuteTitle));
        }
        if (FCode.equals("DocumentNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocumentNum));
        }
        if (FCode.equals("ContTypeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContTypeCode));
        }
        if (FCode.equals("IssueOrganCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IssueOrganCode));
        }
        if (FCode.equals("IssueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getIssueDate()));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("DocContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocContent));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(StatuteNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(StatuteTitle);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(DocumentNum);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ContTypeCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(IssueOrganCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getIssueDate()));
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(DocContent);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("StatuteNo")) {
            if (FValue != null && !FValue.equals("")) {
                StatuteNo = FValue.trim();
            } else {
                StatuteNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("StatuteTitle")) {
            if (FValue != null && !FValue.equals("")) {
                StatuteTitle = FValue.trim();
            } else {
                StatuteTitle = null;
            }
        }
        if (FCode.equalsIgnoreCase("DocumentNum")) {
            if (FValue != null && !FValue.equals("")) {
                DocumentNum = FValue.trim();
            } else {
                DocumentNum = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContTypeCode")) {
            if (FValue != null && !FValue.equals("")) {
                ContTypeCode = FValue.trim();
            } else {
                ContTypeCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("IssueOrganCode")) {
            if (FValue != null && !FValue.equals("")) {
                IssueOrganCode = FValue.trim();
            } else {
                IssueOrganCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("IssueDate")) {
            if (FValue != null && !FValue.equals("")) {
                IssueDate = fDate.getDate(FValue);
            } else {
                IssueDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("DocContent")) {
            if (FValue != null && !FValue.equals("")) {
                DocContent = FValue.trim();
            } else {
                DocContent = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHStatuteSchema other = (LHStatuteSchema) otherObject;
        return
                StatuteNo.equals(other.getStatuteNo())
                && StatuteTitle.equals(other.getStatuteTitle())
                && DocumentNum.equals(other.getDocumentNum())
                && ContTypeCode.equals(other.getContTypeCode())
                && IssueOrganCode.equals(other.getIssueOrganCode())
                && fDate.getString(IssueDate).equals(other.getIssueDate())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && DocContent.equals(other.getDocContent())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("StatuteNo")) {
            return 0;
        }
        if (strFieldName.equals("StatuteTitle")) {
            return 1;
        }
        if (strFieldName.equals("DocumentNum")) {
            return 2;
        }
        if (strFieldName.equals("ContTypeCode")) {
            return 3;
        }
        if (strFieldName.equals("IssueOrganCode")) {
            return 4;
        }
        if (strFieldName.equals("IssueDate")) {
            return 5;
        }
        if (strFieldName.equals("StartDate")) {
            return 6;
        }
        if (strFieldName.equals("EndDate")) {
            return 7;
        }
        if (strFieldName.equals("DocContent")) {
            return 8;
        }
        if (strFieldName.equals("Operator")) {
            return 9;
        }
        if (strFieldName.equals("MakeDate")) {
            return 10;
        }
        if (strFieldName.equals("MakeTime")) {
            return 11;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 12;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 13;
        }
        if (strFieldName.equals("ManageCom")) {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "StatuteNo";
            break;
        case 1:
            strFieldName = "StatuteTitle";
            break;
        case 2:
            strFieldName = "DocumentNum";
            break;
        case 3:
            strFieldName = "ContTypeCode";
            break;
        case 4:
            strFieldName = "IssueOrganCode";
            break;
        case 5:
            strFieldName = "IssueDate";
            break;
        case 6:
            strFieldName = "StartDate";
            break;
        case 7:
            strFieldName = "EndDate";
            break;
        case 8:
            strFieldName = "DocContent";
            break;
        case 9:
            strFieldName = "Operator";
            break;
        case 10:
            strFieldName = "MakeDate";
            break;
        case 11:
            strFieldName = "MakeTime";
            break;
        case 12:
            strFieldName = "ModifyDate";
            break;
        case 13:
            strFieldName = "ModifyTime";
            break;
        case 14:
            strFieldName = "ManageCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("StatuteNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StatuteTitle")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DocumentNum")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContTypeCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IssueOrganCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IssueDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("DocContent")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
