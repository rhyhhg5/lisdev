/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZOrderDetailDB;

/*
 * <p>ClassName: LZOrderDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 单证管理
 * @CreateDate：2006-07-21
 */
public class LZOrderDetailSchema implements Schema, Cloneable {
    // @Field
    /** 批次号 */
    private String SerialNo;
    /** 单证类型 */
    private String CertifyCode;
    /** 订单机构 */
    private String OrderCom;
    /** 追加次数 */
    private int AppendTime;
    /** 订单数量 */
    private int Amnt;
    /** 订单人 */
    private String OrderPerson;
    /** 征订状态 */
    private String OrderState;
    /** 操作人 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LZOrderDetailSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "SerialNo";
        pk[1] = "CertifyCode";
        pk[2] = "OrderCom";
        pk[3] = "AppendTime";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LZOrderDetailSchema cloned = (LZOrderDetailSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getCertifyCode() {
        return CertifyCode;
    }

    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }

    public String getOrderCom() {
        return OrderCom;
    }

    public void setOrderCom(String aOrderCom) {
        OrderCom = aOrderCom;
    }

    public int getAppendTime() {
        return AppendTime;
    }

    public void setAppendTime(int aAppendTime) {
        AppendTime = aAppendTime;
    }

    public void setAppendTime(String aAppendTime) {
        if (aAppendTime != null && !aAppendTime.equals("")) {
            Integer tInteger = new Integer(aAppendTime);
            int i = tInteger.intValue();
            AppendTime = i;
        }
    }

    public int getAmnt() {
        return Amnt;
    }

    public void setAmnt(int aAmnt) {
        Amnt = aAmnt;
    }

    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Integer tInteger = new Integer(aAmnt);
            int i = tInteger.intValue();
            Amnt = i;
        }
    }

    public String getOrderPerson() {
        return OrderPerson;
    }

    public void setOrderPerson(String aOrderPerson) {
        OrderPerson = aOrderPerson;
    }

    public String getOrderState() {
        return OrderState;
    }

    public void setOrderState(String aOrderState) {
        OrderState = aOrderState;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LZOrderDetailSchema 对象给 Schema 赋值
     * @param: aLZOrderDetailSchema LZOrderDetailSchema
     **/
    public void setSchema(LZOrderDetailSchema aLZOrderDetailSchema) {
        this.SerialNo = aLZOrderDetailSchema.getSerialNo();
        this.CertifyCode = aLZOrderDetailSchema.getCertifyCode();
        this.OrderCom = aLZOrderDetailSchema.getOrderCom();
        this.AppendTime = aLZOrderDetailSchema.getAppendTime();
        this.Amnt = aLZOrderDetailSchema.getAmnt();
        this.OrderPerson = aLZOrderDetailSchema.getOrderPerson();
        this.OrderState = aLZOrderDetailSchema.getOrderState();
        this.Operator = aLZOrderDetailSchema.getOperator();
        this.MakeDate = fDate.getDate(aLZOrderDetailSchema.getMakeDate());
        this.MakeTime = aLZOrderDetailSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLZOrderDetailSchema.getModifyDate());
        this.ModifyTime = aLZOrderDetailSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("CertifyCode") == null) {
                this.CertifyCode = null;
            } else {
                this.CertifyCode = rs.getString("CertifyCode").trim();
            }

            if (rs.getString("OrderCom") == null) {
                this.OrderCom = null;
            } else {
                this.OrderCom = rs.getString("OrderCom").trim();
            }

            this.AppendTime = rs.getInt("AppendTime");
            this.Amnt = rs.getInt("Amnt");
            if (rs.getString("OrderPerson") == null) {
                this.OrderPerson = null;
            } else {
                this.OrderPerson = rs.getString("OrderPerson").trim();
            }

            if (rs.getString("OrderState") == null) {
                this.OrderState = null;
            } else {
                this.OrderState = rs.getString("OrderState").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LZOrderDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZOrderDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LZOrderDetailSchema getSchema() {
        LZOrderDetailSchema aLZOrderDetailSchema = new LZOrderDetailSchema();
        aLZOrderDetailSchema.setSchema(this);
        return aLZOrderDetailSchema;
    }

    public LZOrderDetailDB getDB() {
        LZOrderDetailDB aDBOper = new LZOrderDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZOrderDetail描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CertifyCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OrderCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AppendTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Amnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OrderPerson));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OrderState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZOrderDetail>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            OrderCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            AppendTime = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            Amnt = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    5, SysConst.PACKAGESPILTER))).intValue();
            OrderPerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            OrderState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZOrderDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equals("OrderCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderCom));
        }
        if (FCode.equals("AppendTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppendTime));
        }
        if (FCode.equals("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equals("OrderPerson")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderPerson));
        }
        if (FCode.equals("OrderState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderState));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CertifyCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(OrderCom);
            break;
        case 3:
            strFieldValue = String.valueOf(AppendTime);
            break;
        case 4:
            strFieldValue = String.valueOf(Amnt);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(OrderPerson);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(OrderState);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if (FValue != null && !FValue.equals("")) {
                CertifyCode = FValue.trim();
            } else {
                CertifyCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("OrderCom")) {
            if (FValue != null && !FValue.equals("")) {
                OrderCom = FValue.trim();
            } else {
                OrderCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppendTime")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AppendTime = i;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Amnt = i;
            }
        }
        if (FCode.equalsIgnoreCase("OrderPerson")) {
            if (FValue != null && !FValue.equals("")) {
                OrderPerson = FValue.trim();
            } else {
                OrderPerson = null;
            }
        }
        if (FCode.equalsIgnoreCase("OrderState")) {
            if (FValue != null && !FValue.equals("")) {
                OrderState = FValue.trim();
            } else {
                OrderState = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LZOrderDetailSchema other = (LZOrderDetailSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && CertifyCode.equals(other.getCertifyCode())
                && OrderCom.equals(other.getOrderCom())
                && AppendTime == other.getAppendTime()
                && Amnt == other.getAmnt()
                && OrderPerson.equals(other.getOrderPerson())
                && OrderState.equals(other.getOrderState())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("CertifyCode")) {
            return 1;
        }
        if (strFieldName.equals("OrderCom")) {
            return 2;
        }
        if (strFieldName.equals("AppendTime")) {
            return 3;
        }
        if (strFieldName.equals("Amnt")) {
            return 4;
        }
        if (strFieldName.equals("OrderPerson")) {
            return 5;
        }
        if (strFieldName.equals("OrderState")) {
            return 6;
        }
        if (strFieldName.equals("Operator")) {
            return 7;
        }
        if (strFieldName.equals("MakeDate")) {
            return 8;
        }
        if (strFieldName.equals("MakeTime")) {
            return 9;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 10;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "CertifyCode";
            break;
        case 2:
            strFieldName = "OrderCom";
            break;
        case 3:
            strFieldName = "AppendTime";
            break;
        case 4:
            strFieldName = "Amnt";
            break;
        case 5:
            strFieldName = "OrderPerson";
            break;
        case 6:
            strFieldName = "OrderState";
            break;
        case 7:
            strFieldName = "Operator";
            break;
        case 8:
            strFieldName = "MakeDate";
            break;
        case 9:
            strFieldName = "MakeTime";
            break;
        case 10:
            strFieldName = "ModifyDate";
            break;
        case 11:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifyCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OrderCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppendTime")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Amnt")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("OrderPerson")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OrderState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_INT;
            break;
        case 4:
            nFieldType = Schema.TYPE_INT;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
