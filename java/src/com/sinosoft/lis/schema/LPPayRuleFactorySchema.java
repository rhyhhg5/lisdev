/*
 * <p>ClassName: LPPayRuleFactorySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 更新表
 * @CreateDate：2005-01-12
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPPayRuleFactoryDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LPPayRuleFactorySchema implements Schema
{
    // @Field
    /** 批单号 */
    private String EdorNo;
    /** 批改类型 */
    private String EdorType;
    /** 集体保单险种号码 */
    private String GrpPolNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 险种编码 */
    private String RiskCode;
    /** 缴费规则编码 */
    private String PayRuleCode;
    /** 缴费规则名称 */
    private String PayRuleName;
    /** 要素类型 */
    private String FactoryType;
    /** 要素类型对应号码 */
    private String OtherNo;
    /** 计算编码 */
    private String FactoryCode;
    /** 计算子编码 */
    private int FactorySubCode;
    /** 计算名称 */
    private String FactoryName;
    /** 内部流水号 */
    private String InerSerialNo;
    /** 计算sql */
    private String CalSql;
    /** 参数 */
    private String Params;
    /** 计算备注 */
    private String CalRemark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPPayRuleFactorySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[9];
        pk[0] = "EdorNo";
        pk[1] = "EdorType";
        pk[2] = "GrpPolNo";
        pk[3] = "PayRuleCode";
        pk[4] = "FactoryType";
        pk[5] = "OtherNo";
        pk[6] = "FactoryCode";
        pk[7] = "FactorySubCode";
        pk[8] = "InerSerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getEdorType()
    {
        if (EdorType != null && !EdorType.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getPayRuleCode()
    {
        if (PayRuleCode != null && !PayRuleCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayRuleCode = StrTool.unicodeToGBK(PayRuleCode);
        }
        return PayRuleCode;
    }

    public void setPayRuleCode(String aPayRuleCode)
    {
        PayRuleCode = aPayRuleCode;
    }

    public String getPayRuleName()
    {
        if (PayRuleName != null && !PayRuleName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayRuleName = StrTool.unicodeToGBK(PayRuleName);
        }
        return PayRuleName;
    }

    public void setPayRuleName(String aPayRuleName)
    {
        PayRuleName = aPayRuleName;
    }

    public String getFactoryType()
    {
        if (FactoryType != null && !FactoryType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FactoryType = StrTool.unicodeToGBK(FactoryType);
        }
        return FactoryType;
    }

    public void setFactoryType(String aFactoryType)
    {
        FactoryType = aFactoryType;
    }

    public String getOtherNo()
    {
        if (OtherNo != null && !OtherNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OtherNo = StrTool.unicodeToGBK(OtherNo);
        }
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getFactoryCode()
    {
        if (FactoryCode != null && !FactoryCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FactoryCode = StrTool.unicodeToGBK(FactoryCode);
        }
        return FactoryCode;
    }

    public void setFactoryCode(String aFactoryCode)
    {
        FactoryCode = aFactoryCode;
    }

    public int getFactorySubCode()
    {
        return FactorySubCode;
    }

    public void setFactorySubCode(int aFactorySubCode)
    {
        FactorySubCode = aFactorySubCode;
    }

    public void setFactorySubCode(String aFactorySubCode)
    {
        if (aFactorySubCode != null && !aFactorySubCode.equals(""))
        {
            Integer tInteger = new Integer(aFactorySubCode);
            int i = tInteger.intValue();
            FactorySubCode = i;
        }
    }

    public String getFactoryName()
    {
        if (FactoryName != null && !FactoryName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FactoryName = StrTool.unicodeToGBK(FactoryName);
        }
        return FactoryName;
    }

    public void setFactoryName(String aFactoryName)
    {
        FactoryName = aFactoryName;
    }

    public String getInerSerialNo()
    {
        if (InerSerialNo != null && !InerSerialNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InerSerialNo = StrTool.unicodeToGBK(InerSerialNo);
        }
        return InerSerialNo;
    }

    public void setInerSerialNo(String aInerSerialNo)
    {
        InerSerialNo = aInerSerialNo;
    }

    public String getCalSql()
    {
        if (CalSql != null && !CalSql.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalSql = StrTool.unicodeToGBK(CalSql);
        }
        return CalSql;
    }

    public void setCalSql(String aCalSql)
    {
        CalSql = aCalSql;
    }

    public String getParams()
    {
        if (Params != null && !Params.equals("") && SysConst.CHANGECHARSET == true)
        {
            Params = StrTool.unicodeToGBK(Params);
        }
        return Params;
    }

    public void setParams(String aParams)
    {
        Params = aParams;
    }

    public String getCalRemark()
    {
        if (CalRemark != null && !CalRemark.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalRemark = StrTool.unicodeToGBK(CalRemark);
        }
        return CalRemark;
    }

    public void setCalRemark(String aCalRemark)
    {
        CalRemark = aCalRemark;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LPPayRuleFactorySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LPPayRuleFactorySchema aLPPayRuleFactorySchema)
    {
        this.EdorNo = aLPPayRuleFactorySchema.getEdorNo();
        this.EdorType = aLPPayRuleFactorySchema.getEdorType();
        this.GrpPolNo = aLPPayRuleFactorySchema.getGrpPolNo();
        this.GrpContNo = aLPPayRuleFactorySchema.getGrpContNo();
        this.RiskCode = aLPPayRuleFactorySchema.getRiskCode();
        this.PayRuleCode = aLPPayRuleFactorySchema.getPayRuleCode();
        this.PayRuleName = aLPPayRuleFactorySchema.getPayRuleName();
        this.FactoryType = aLPPayRuleFactorySchema.getFactoryType();
        this.OtherNo = aLPPayRuleFactorySchema.getOtherNo();
        this.FactoryCode = aLPPayRuleFactorySchema.getFactoryCode();
        this.FactorySubCode = aLPPayRuleFactorySchema.getFactorySubCode();
        this.FactoryName = aLPPayRuleFactorySchema.getFactoryName();
        this.InerSerialNo = aLPPayRuleFactorySchema.getInerSerialNo();
        this.CalSql = aLPPayRuleFactorySchema.getCalSql();
        this.Params = aLPPayRuleFactorySchema.getParams();
        this.CalRemark = aLPPayRuleFactorySchema.getCalRemark();
        this.Operator = aLPPayRuleFactorySchema.getOperator();
        this.MakeDate = fDate.getDate(aLPPayRuleFactorySchema.getMakeDate());
        this.MakeTime = aLPPayRuleFactorySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPPayRuleFactorySchema.getModifyDate());
        this.ModifyTime = aLPPayRuleFactorySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("PayRuleCode") == null)
            {
                this.PayRuleCode = null;
            }
            else
            {
                this.PayRuleCode = rs.getString("PayRuleCode").trim();
            }

            if (rs.getString("PayRuleName") == null)
            {
                this.PayRuleName = null;
            }
            else
            {
                this.PayRuleName = rs.getString("PayRuleName").trim();
            }

            if (rs.getString("FactoryType") == null)
            {
                this.FactoryType = null;
            }
            else
            {
                this.FactoryType = rs.getString("FactoryType").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("FactoryCode") == null)
            {
                this.FactoryCode = null;
            }
            else
            {
                this.FactoryCode = rs.getString("FactoryCode").trim();
            }

            this.FactorySubCode = rs.getInt("FactorySubCode");
            if (rs.getString("FactoryName") == null)
            {
                this.FactoryName = null;
            }
            else
            {
                this.FactoryName = rs.getString("FactoryName").trim();
            }

            if (rs.getString("InerSerialNo") == null)
            {
                this.InerSerialNo = null;
            }
            else
            {
                this.InerSerialNo = rs.getString("InerSerialNo").trim();
            }

            if (rs.getString("CalSql") == null)
            {
                this.CalSql = null;
            }
            else
            {
                this.CalSql = rs.getString("CalSql").trim();
            }

            if (rs.getString("Params") == null)
            {
                this.Params = null;
            }
            else
            {
                this.Params = rs.getString("Params").trim();
            }

            if (rs.getString("CalRemark") == null)
            {
                this.CalRemark = null;
            }
            else
            {
                this.CalRemark = rs.getString("CalRemark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPPayRuleFactorySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPPayRuleFactorySchema getSchema()
    {
        LPPayRuleFactorySchema aLPPayRuleFactorySchema = new
                LPPayRuleFactorySchema();
        aLPPayRuleFactorySchema.setSchema(this);
        return aLPPayRuleFactorySchema;
    }

    public LPPayRuleFactoryDB getDB()
    {
        LPPayRuleFactoryDB aDBOper = new LPPayRuleFactoryDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPPayRuleFactory描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayRuleCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayRuleName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FactoryType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FactoryCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FactorySubCode) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FactoryName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InerSerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalSql)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Params)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalRemark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPPayRuleFactory>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            PayRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            PayRuleName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            FactoryType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            FactoryCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            FactorySubCode = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).intValue();
            FactoryName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            InerSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            CalSql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                    SysConst.PACKAGESPILTER);
            Params = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                    SysConst.PACKAGESPILTER);
            CalRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPPayRuleFactorySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorType));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("PayRuleCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayRuleCode));
        }
        if (FCode.equals("PayRuleName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayRuleName));
        }
        if (FCode.equals("FactoryType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FactoryType));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNo));
        }
        if (FCode.equals("FactoryCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FactoryCode));
        }
        if (FCode.equals("FactorySubCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FactorySubCode));
        }
        if (FCode.equals("FactoryName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FactoryName));
        }
        if (FCode.equals("InerSerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InerSerialNo));
        }
        if (FCode.equals("CalSql"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalSql));
        }
        if (FCode.equals("Params"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Params));
        }
        if (FCode.equals("CalRemark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalRemark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PayRuleCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PayRuleName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(FactoryType);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(FactoryCode);
                break;
            case 10:
                strFieldValue = String.valueOf(FactorySubCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(FactoryName);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(InerSerialNo);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(CalSql);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Params);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(CalRemark);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("PayRuleCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayRuleCode = FValue.trim();
            }
            else
            {
                PayRuleCode = null;
            }
        }
        if (FCode.equals("PayRuleName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayRuleName = FValue.trim();
            }
            else
            {
                PayRuleName = null;
            }
        }
        if (FCode.equals("FactoryType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FactoryType = FValue.trim();
            }
            else
            {
                FactoryType = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("FactoryCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FactoryCode = FValue.trim();
            }
            else
            {
                FactoryCode = null;
            }
        }
        if (FCode.equals("FactorySubCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                FactorySubCode = i;
            }
        }
        if (FCode.equals("FactoryName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FactoryName = FValue.trim();
            }
            else
            {
                FactoryName = null;
            }
        }
        if (FCode.equals("InerSerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InerSerialNo = FValue.trim();
            }
            else
            {
                InerSerialNo = null;
            }
        }
        if (FCode.equals("CalSql"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalSql = FValue.trim();
            }
            else
            {
                CalSql = null;
            }
        }
        if (FCode.equals("Params"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Params = FValue.trim();
            }
            else
            {
                Params = null;
            }
        }
        if (FCode.equals("CalRemark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalRemark = FValue.trim();
            }
            else
            {
                CalRemark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPPayRuleFactorySchema other = (LPPayRuleFactorySchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && GrpPolNo.equals(other.getGrpPolNo())
                && GrpContNo.equals(other.getGrpContNo())
                && RiskCode.equals(other.getRiskCode())
                && PayRuleCode.equals(other.getPayRuleCode())
                && PayRuleName.equals(other.getPayRuleName())
                && FactoryType.equals(other.getFactoryType())
                && OtherNo.equals(other.getOtherNo())
                && FactoryCode.equals(other.getFactoryCode())
                && FactorySubCode == other.getFactorySubCode()
                && FactoryName.equals(other.getFactoryName())
                && InerSerialNo.equals(other.getInerSerialNo())
                && CalSql.equals(other.getCalSql())
                && Params.equals(other.getParams())
                && CalRemark.equals(other.getCalRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 1;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 3;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 4;
        }
        if (strFieldName.equals("PayRuleCode"))
        {
            return 5;
        }
        if (strFieldName.equals("PayRuleName"))
        {
            return 6;
        }
        if (strFieldName.equals("FactoryType"))
        {
            return 7;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 8;
        }
        if (strFieldName.equals("FactoryCode"))
        {
            return 9;
        }
        if (strFieldName.equals("FactorySubCode"))
        {
            return 10;
        }
        if (strFieldName.equals("FactoryName"))
        {
            return 11;
        }
        if (strFieldName.equals("InerSerialNo"))
        {
            return 12;
        }
        if (strFieldName.equals("CalSql"))
        {
            return 13;
        }
        if (strFieldName.equals("Params"))
        {
            return 14;
        }
        if (strFieldName.equals("CalRemark"))
        {
            return 15;
        }
        if (strFieldName.equals("Operator"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "EdorType";
                break;
            case 2:
                strFieldName = "GrpPolNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "RiskCode";
                break;
            case 5:
                strFieldName = "PayRuleCode";
                break;
            case 6:
                strFieldName = "PayRuleName";
                break;
            case 7:
                strFieldName = "FactoryType";
                break;
            case 8:
                strFieldName = "OtherNo";
                break;
            case 9:
                strFieldName = "FactoryCode";
                break;
            case 10:
                strFieldName = "FactorySubCode";
                break;
            case 11:
                strFieldName = "FactoryName";
                break;
            case 12:
                strFieldName = "InerSerialNo";
                break;
            case 13:
                strFieldName = "CalSql";
                break;
            case 14:
                strFieldName = "Params";
                break;
            case 15:
                strFieldName = "CalRemark";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "ModifyDate";
                break;
            case 20:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayRuleCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayRuleName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactoryType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactoryCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorySubCode"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("FactoryName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InerSerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalSql"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Params"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalRemark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_INT;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
