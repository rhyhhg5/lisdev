/*
 * <p>ClassName: LDPageAccessSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDPageAccessDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDPageAccessSchema implements Schema
{
    // @Field
    /** 执行语句 */
    private String RunScript;
    /** 菜单节点说明 */
    private String NodeDescription;
    /** 菜单节点编码 */
    private String NodeCode;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDPageAccessSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RunScript";
        pk[1] = "NodeCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRunScript()
    {
        if (RunScript != null && !RunScript.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RunScript = StrTool.unicodeToGBK(RunScript);
        }
        return RunScript;
    }

    public void setRunScript(String aRunScript)
    {
        RunScript = aRunScript;
    }

    public String getNodeDescription()
    {
        if (NodeDescription != null && !NodeDescription.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            NodeDescription = StrTool.unicodeToGBK(NodeDescription);
        }
        return NodeDescription;
    }

    public void setNodeDescription(String aNodeDescription)
    {
        NodeDescription = aNodeDescription;
    }

    public String getNodeCode()
    {
        if (NodeCode != null && !NodeCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            NodeCode = StrTool.unicodeToGBK(NodeCode);
        }
        return NodeCode;
    }

    public void setNodeCode(String aNodeCode)
    {
        NodeCode = aNodeCode;
    }

    /**
     * 使用另外一个 LDPageAccessSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDPageAccessSchema aLDPageAccessSchema)
    {
        this.RunScript = aLDPageAccessSchema.getRunScript();
        this.NodeDescription = aLDPageAccessSchema.getNodeDescription();
        this.NodeCode = aLDPageAccessSchema.getNodeCode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RunScript") == null)
            {
                this.RunScript = null;
            }
            else
            {
                this.RunScript = rs.getString("RunScript").trim();
            }

            if (rs.getString("NodeDescription") == null)
            {
                this.NodeDescription = null;
            }
            else
            {
                this.NodeDescription = rs.getString("NodeDescription").trim();
            }

            if (rs.getString("NodeCode") == null)
            {
                this.NodeCode = null;
            }
            else
            {
                this.NodeCode = rs.getString("NodeCode").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPageAccessSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDPageAccessSchema getSchema()
    {
        LDPageAccessSchema aLDPageAccessSchema = new LDPageAccessSchema();
        aLDPageAccessSchema.setSchema(this);
        return aLDPageAccessSchema;
    }

    public LDPageAccessDB getDB()
    {
        LDPageAccessDB aDBOper = new LDPageAccessDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDPageAccess描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RunScript)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NodeDescription)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NodeCode));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDPageAccess>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RunScript = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            NodeDescription = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             2, SysConst.PACKAGESPILTER);
            NodeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPageAccessSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RunScript"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RunScript));
        }
        if (FCode.equals("NodeDescription"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NodeDescription));
        }
        if (FCode.equals("NodeCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NodeCode));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RunScript);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(NodeDescription);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(NodeCode);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RunScript"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RunScript = FValue.trim();
            }
            else
            {
                RunScript = null;
            }
        }
        if (FCode.equals("NodeDescription"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NodeDescription = FValue.trim();
            }
            else
            {
                NodeDescription = null;
            }
        }
        if (FCode.equals("NodeCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NodeCode = FValue.trim();
            }
            else
            {
                NodeCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDPageAccessSchema other = (LDPageAccessSchema) otherObject;
        return
                RunScript.equals(other.getRunScript())
                && NodeDescription.equals(other.getNodeDescription())
                && NodeCode.equals(other.getNodeCode());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RunScript"))
        {
            return 0;
        }
        if (strFieldName.equals("NodeDescription"))
        {
            return 1;
        }
        if (strFieldName.equals("NodeCode"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RunScript";
                break;
            case 1:
                strFieldName = "NodeDescription";
                break;
            case 2:
                strFieldName = "NodeCode";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RunScript"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NodeDescription"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NodeCode"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
