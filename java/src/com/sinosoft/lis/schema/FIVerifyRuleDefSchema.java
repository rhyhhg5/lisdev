/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIVerifyRuleDefDB;

/*
 * <p>ClassName: FIVerifyRuleDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIVerifyRuleDefSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 规则编码 */
	private String RuleDefID;
	/** 规则名称 */
	private String RuleName;
	/** 规则类型 */
	private String RuleType;
	/** 执行节点id */
	private String CallPointID;
	/** 优先级 */
	private String Priority;
	/** 规则描述 */
	private String ReturnRemark;
	/** 错误类型 */
	private String ErrType;
	/** 规则状态 */
	private String RuleState;

	public static final int FIELDNUM = 9;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIVerifyRuleDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "VersionNo";
		pk[1] = "RuleDefID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIVerifyRuleDefSchema cloned = (FIVerifyRuleDefSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getRuleDefID()
	{
		return RuleDefID;
	}
	public void setRuleDefID(String aRuleDefID)
	{
		RuleDefID = aRuleDefID;
	}
	public String getRuleName()
	{
		return RuleName;
	}
	public void setRuleName(String aRuleName)
	{
		RuleName = aRuleName;
	}
	public String getRuleType()
	{
		return RuleType;
	}
	public void setRuleType(String aRuleType)
	{
		RuleType = aRuleType;
	}
	public String getCallPointID()
	{
		return CallPointID;
	}
	public void setCallPointID(String aCallPointID)
	{
		CallPointID = aCallPointID;
	}
	public String getPriority()
	{
		return Priority;
	}
	public void setPriority(String aPriority)
	{
		Priority = aPriority;
	}
	public String getReturnRemark()
	{
		return ReturnRemark;
	}
	public void setReturnRemark(String aReturnRemark)
	{
		ReturnRemark = aReturnRemark;
	}
	public String getErrType()
	{
		return ErrType;
	}
	public void setErrType(String aErrType)
	{
		ErrType = aErrType;
	}
	public String getRuleState()
	{
		return RuleState;
	}
	public void setRuleState(String aRuleState)
	{
		RuleState = aRuleState;
	}

	/**
	* 使用另外一个 FIVerifyRuleDefSchema 对象给 Schema 赋值
	* @param: aFIVerifyRuleDefSchema FIVerifyRuleDefSchema
	**/
	public void setSchema(FIVerifyRuleDefSchema aFIVerifyRuleDefSchema)
	{
		this.VersionNo = aFIVerifyRuleDefSchema.getVersionNo();
		this.RuleDefID = aFIVerifyRuleDefSchema.getRuleDefID();
		this.RuleName = aFIVerifyRuleDefSchema.getRuleName();
		this.RuleType = aFIVerifyRuleDefSchema.getRuleType();
		this.CallPointID = aFIVerifyRuleDefSchema.getCallPointID();
		this.Priority = aFIVerifyRuleDefSchema.getPriority();
		this.ReturnRemark = aFIVerifyRuleDefSchema.getReturnRemark();
		this.ErrType = aFIVerifyRuleDefSchema.getErrType();
		this.RuleState = aFIVerifyRuleDefSchema.getRuleState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("RuleDefID") == null )
				this.RuleDefID = null;
			else
				this.RuleDefID = rs.getString("RuleDefID").trim();

			if( rs.getString("RuleName") == null )
				this.RuleName = null;
			else
				this.RuleName = rs.getString("RuleName").trim();

			if( rs.getString("RuleType") == null )
				this.RuleType = null;
			else
				this.RuleType = rs.getString("RuleType").trim();

			if( rs.getString("CallPointID") == null )
				this.CallPointID = null;
			else
				this.CallPointID = rs.getString("CallPointID").trim();

			if( rs.getString("Priority") == null )
				this.Priority = null;
			else
				this.Priority = rs.getString("Priority").trim();

			if( rs.getString("ReturnRemark") == null )
				this.ReturnRemark = null;
			else
				this.ReturnRemark = rs.getString("ReturnRemark").trim();

			if( rs.getString("ErrType") == null )
				this.ErrType = null;
			else
				this.ErrType = rs.getString("ErrType").trim();

			if( rs.getString("RuleState") == null )
				this.RuleState = null;
			else
				this.RuleState = rs.getString("RuleState").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIVerifyRuleDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVerifyRuleDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIVerifyRuleDefSchema getSchema()
	{
		FIVerifyRuleDefSchema aFIVerifyRuleDefSchema = new FIVerifyRuleDefSchema();
		aFIVerifyRuleDefSchema.setSchema(this);
		return aFIVerifyRuleDefSchema;
	}

	public FIVerifyRuleDefDB getDB()
	{
		FIVerifyRuleDefDB aDBOper = new FIVerifyRuleDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVerifyRuleDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleDefID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CallPointID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Priority)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleState));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVerifyRuleDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RuleDefID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RuleName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RuleType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CallPointID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Priority = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ReturnRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ErrType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RuleState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVerifyRuleDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("RuleDefID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleDefID));
		}
		if (FCode.equals("RuleName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleName));
		}
		if (FCode.equals("RuleType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleType));
		}
		if (FCode.equals("CallPointID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CallPointID));
		}
		if (FCode.equals("Priority"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Priority));
		}
		if (FCode.equals("ReturnRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnRemark));
		}
		if (FCode.equals("ErrType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrType));
		}
		if (FCode.equals("RuleState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleState));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RuleDefID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RuleName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RuleType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CallPointID);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Priority);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ReturnRemark);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ErrType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RuleState);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("RuleDefID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleDefID = FValue.trim();
			}
			else
				RuleDefID = null;
		}
		if (FCode.equalsIgnoreCase("RuleName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleName = FValue.trim();
			}
			else
				RuleName = null;
		}
		if (FCode.equalsIgnoreCase("RuleType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleType = FValue.trim();
			}
			else
				RuleType = null;
		}
		if (FCode.equalsIgnoreCase("CallPointID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CallPointID = FValue.trim();
			}
			else
				CallPointID = null;
		}
		if (FCode.equalsIgnoreCase("Priority"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Priority = FValue.trim();
			}
			else
				Priority = null;
		}
		if (FCode.equalsIgnoreCase("ReturnRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnRemark = FValue.trim();
			}
			else
				ReturnRemark = null;
		}
		if (FCode.equalsIgnoreCase("ErrType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrType = FValue.trim();
			}
			else
				ErrType = null;
		}
		if (FCode.equalsIgnoreCase("RuleState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleState = FValue.trim();
			}
			else
				RuleState = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIVerifyRuleDefSchema other = (FIVerifyRuleDefSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (RuleDefID == null ? other.getRuleDefID() == null : RuleDefID.equals(other.getRuleDefID()))
			&& (RuleName == null ? other.getRuleName() == null : RuleName.equals(other.getRuleName()))
			&& (RuleType == null ? other.getRuleType() == null : RuleType.equals(other.getRuleType()))
			&& (CallPointID == null ? other.getCallPointID() == null : CallPointID.equals(other.getCallPointID()))
			&& (Priority == null ? other.getPriority() == null : Priority.equals(other.getPriority()))
			&& (ReturnRemark == null ? other.getReturnRemark() == null : ReturnRemark.equals(other.getReturnRemark()))
			&& (ErrType == null ? other.getErrType() == null : ErrType.equals(other.getErrType()))
			&& (RuleState == null ? other.getRuleState() == null : RuleState.equals(other.getRuleState()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("RuleDefID") ) {
			return 1;
		}
		if( strFieldName.equals("RuleName") ) {
			return 2;
		}
		if( strFieldName.equals("RuleType") ) {
			return 3;
		}
		if( strFieldName.equals("CallPointID") ) {
			return 4;
		}
		if( strFieldName.equals("Priority") ) {
			return 5;
		}
		if( strFieldName.equals("ReturnRemark") ) {
			return 6;
		}
		if( strFieldName.equals("ErrType") ) {
			return 7;
		}
		if( strFieldName.equals("RuleState") ) {
			return 8;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "RuleDefID";
				break;
			case 2:
				strFieldName = "RuleName";
				break;
			case 3:
				strFieldName = "RuleType";
				break;
			case 4:
				strFieldName = "CallPointID";
				break;
			case 5:
				strFieldName = "Priority";
				break;
			case 6:
				strFieldName = "ReturnRemark";
				break;
			case 7:
				strFieldName = "ErrType";
				break;
			case 8:
				strFieldName = "RuleState";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleDefID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CallPointID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Priority") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleState") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
