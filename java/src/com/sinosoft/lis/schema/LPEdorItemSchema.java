/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LPEdorItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-06-13
 */
public class LPEdorItemSchema implements Schema, Cloneable
{
    // @Field
    /** 保全受理号 */
    private String EdorAcceptNo;
    /** 批单号 */
    private String EdorNo;
    /** 批改申请号 */
    private String EdorAppNo;
    /** 批改类型 */
    private String EdorType;
    /** 批改类型显示级别 */
    private String DisplayType;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 保单险种号码 */
    private String PolNo;
    /** 管理机构 */
    private String ManageCom;
    /** 批改生效日期 */
    private Date EdorValiDate;
    /** 批改申请日期 */
    private Date EdorAppDate;
    /** 批改状态 */
    private String EdorState;
    /** 核保状态 */
    private String UWFlag;
    /** 核保人 */
    private String UWOperator;
    /** 核保完成日期 */
    private Date UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 变动的保费 */
    private double ChgPrem;
    /** 变动的保额 */
    private double ChgAmnt;
    /** 补/退费金额 */
    private double GetMoney;
    /** 补/退费利息 */
    private double GetInterest;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 撤保原因编码 */
    private String EdorReasonNo;
    /** 撤保原因内容 */
    private String EdorReason;
    /** 撤销原因 */
    private String Reason;
    /** 原因编码 */
    private String ReasonCode;
    /** 审核级别 */
    private String ApproveGrade;
    /** 审核标志 */
    private String ApproveState;
    /** 审核人 */
    private String ApproveOperator;
    /** 审核日期 */
    private Date ApproveDate;
    /** 审核时间 */
    private String ApproveTime;

    public static final int FIELDNUM = 35; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPEdorItemSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "EdorAcceptNo";
        pk[1] = "EdorNo";
        pk[2] = "EdorType";
        pk[3] = "ContNo";
        pk[4] = "InsuredNo";
        pk[5] = "PolNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LPEdorItemSchema cloned = (LPEdorItemSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorAcceptNo()
    {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String aEdorAcceptNo)
    {
        EdorAcceptNo = aEdorAcceptNo;
    }

    public String getEdorNo()
    {
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getEdorAppNo()
    {
        return EdorAppNo;
    }

    public void setEdorAppNo(String aEdorAppNo)
    {
        EdorAppNo = aEdorAppNo;
    }

    public String getEdorType()
    {
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getDisplayType()
    {
        return DisplayType;
    }

    public void setDisplayType(String aDisplayType)
    {
        DisplayType = aDisplayType;
    }

    public String getGrpContNo()
    {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getContNo()
    {
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getInsuredNo()
    {
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getPolNo()
    {
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getManageCom()
    {
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getEdorValiDate()
    {
        if (EdorValiDate != null)
        {
            return fDate.getString(EdorValiDate);
        }
        else
        {
            return null;
        }
    }

    public void setEdorValiDate(Date aEdorValiDate)
    {
        EdorValiDate = aEdorValiDate;
    }

    public void setEdorValiDate(String aEdorValiDate)
    {
        if (aEdorValiDate != null && !aEdorValiDate.equals(""))
        {
            EdorValiDate = fDate.getDate(aEdorValiDate);
        }
        else
        {
            EdorValiDate = null;
        }
    }

    public String getEdorAppDate()
    {
        if (EdorAppDate != null)
        {
            return fDate.getString(EdorAppDate);
        }
        else
        {
            return null;
        }
    }

    public void setEdorAppDate(Date aEdorAppDate)
    {
        EdorAppDate = aEdorAppDate;
    }

    public void setEdorAppDate(String aEdorAppDate)
    {
        if (aEdorAppDate != null && !aEdorAppDate.equals(""))
        {
            EdorAppDate = fDate.getDate(aEdorAppDate);
        }
        else
        {
            EdorAppDate = null;
        }
    }

    public String getEdorState()
    {
        return EdorState;
    }

    public void setEdorState(String aEdorState)
    {
        EdorState = aEdorState;
    }

    public String getUWFlag()
    {
        return UWFlag;
    }

    public void setUWFlag(String aUWFlag)
    {
        UWFlag = aUWFlag;
    }

    public String getUWOperator()
    {
        return UWOperator;
    }

    public void setUWOperator(String aUWOperator)
    {
        UWOperator = aUWOperator;
    }

    public String getUWDate()
    {
        if (UWDate != null)
        {
            return fDate.getString(UWDate);
        }
        else
        {
            return null;
        }
    }

    public void setUWDate(Date aUWDate)
    {
        UWDate = aUWDate;
    }

    public void setUWDate(String aUWDate)
    {
        if (aUWDate != null && !aUWDate.equals(""))
        {
            UWDate = fDate.getDate(aUWDate);
        }
        else
        {
            UWDate = null;
        }
    }

    public String getUWTime()
    {
        return UWTime;
    }

    public void setUWTime(String aUWTime)
    {
        UWTime = aUWTime;
    }

    public double getChgPrem()
    {
        return ChgPrem;
    }

    public void setChgPrem(double aChgPrem)
    {
        ChgPrem = Arith.round(aChgPrem, 2);
    }

    public void setChgPrem(String aChgPrem)
    {
        if (aChgPrem != null && !aChgPrem.equals(""))
        {
            Double tDouble = new Double(aChgPrem);
            double d = tDouble.doubleValue();
            ChgPrem = Arith.round(d, 2);
        }
    }

    public double getChgAmnt()
    {
        return ChgAmnt;
    }

    public void setChgAmnt(double aChgAmnt)
    {
        ChgAmnt = Arith.round(aChgAmnt, 2);
    }

    public void setChgAmnt(String aChgAmnt)
    {
        if (aChgAmnt != null && !aChgAmnt.equals(""))
        {
            Double tDouble = new Double(aChgAmnt);
            double d = tDouble.doubleValue();
            ChgAmnt = Arith.round(d, 2);
        }
    }

    public double getGetMoney()
    {
        return GetMoney;
    }

    public void setGetMoney(double aGetMoney)
    {
        GetMoney = Arith.round(aGetMoney, 2);
    }

    public void setGetMoney(String aGetMoney)
    {
        if (aGetMoney != null && !aGetMoney.equals(""))
        {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = Arith.round(d, 2);
        }
    }

    public double getGetInterest()
    {
        return GetInterest;
    }

    public void setGetInterest(double aGetInterest)
    {
        GetInterest = Arith.round(aGetInterest, 2);
    }

    public void setGetInterest(String aGetInterest)
    {
        if (aGetInterest != null && !aGetInterest.equals(""))
        {
            Double tDouble = new Double(aGetInterest);
            double d = tDouble.doubleValue();
            GetInterest = Arith.round(d, 2);
        }
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getEdorReasonNo()
    {
        return EdorReasonNo;
    }

    public void setEdorReasonNo(String aEdorReasonNo)
    {
        EdorReasonNo = aEdorReasonNo;
    }

    public String getEdorReason()
    {
        return EdorReason;
    }

    public void setEdorReason(String aEdorReason)
    {
        EdorReason = aEdorReason;
    }

    public String getReason()
    {
        return Reason;
    }

    public void setReason(String aReason)
    {
        Reason = aReason;
    }

    public String getReasonCode()
    {
        return ReasonCode;
    }

    public void setReasonCode(String aReasonCode)
    {
        ReasonCode = aReasonCode;
    }

    public String getApproveGrade()
    {
        return ApproveGrade;
    }

    public void setApproveGrade(String aApproveGrade)
    {
        ApproveGrade = aApproveGrade;
    }

    public String getApproveState()
    {
        return ApproveState;
    }

    public void setApproveState(String aApproveState)
    {
        ApproveState = aApproveState;
    }

    public String getApproveOperator()
    {
        return ApproveOperator;
    }

    public void setApproveOperator(String aApproveOperator)
    {
        ApproveOperator = aApproveOperator;
    }

    public String getApproveDate()
    {
        if (ApproveDate != null)
        {
            return fDate.getString(ApproveDate);
        }
        else
        {
            return null;
        }
    }

    public void setApproveDate(Date aApproveDate)
    {
        ApproveDate = aApproveDate;
    }

    public void setApproveDate(String aApproveDate)
    {
        if (aApproveDate != null && !aApproveDate.equals(""))
        {
            ApproveDate = fDate.getDate(aApproveDate);
        }
        else
        {
            ApproveDate = null;
        }
    }

    public String getApproveTime()
    {
        return ApproveTime;
    }

    public void setApproveTime(String aApproveTime)
    {
        ApproveTime = aApproveTime;
    }

    /**
     * 使用另外一个 LPEdorItemSchema 对象给 Schema 赋值
     * @param: aLPEdorItemSchema LPEdorItemSchema
     **/
    public void setSchema(LPEdorItemSchema aLPEdorItemSchema)
    {
        this.EdorAcceptNo = aLPEdorItemSchema.getEdorAcceptNo();
        this.EdorNo = aLPEdorItemSchema.getEdorNo();
        this.EdorAppNo = aLPEdorItemSchema.getEdorAppNo();
        this.EdorType = aLPEdorItemSchema.getEdorType();
        this.DisplayType = aLPEdorItemSchema.getDisplayType();
        this.GrpContNo = aLPEdorItemSchema.getGrpContNo();
        this.ContNo = aLPEdorItemSchema.getContNo();
        this.InsuredNo = aLPEdorItemSchema.getInsuredNo();
        this.PolNo = aLPEdorItemSchema.getPolNo();
        this.ManageCom = aLPEdorItemSchema.getManageCom();
        this.EdorValiDate = fDate.getDate(aLPEdorItemSchema.getEdorValiDate());
        this.EdorAppDate = fDate.getDate(aLPEdorItemSchema.getEdorAppDate());
        this.EdorState = aLPEdorItemSchema.getEdorState();
        this.UWFlag = aLPEdorItemSchema.getUWFlag();
        this.UWOperator = aLPEdorItemSchema.getUWOperator();
        this.UWDate = fDate.getDate(aLPEdorItemSchema.getUWDate());
        this.UWTime = aLPEdorItemSchema.getUWTime();
        this.ChgPrem = aLPEdorItemSchema.getChgPrem();
        this.ChgAmnt = aLPEdorItemSchema.getChgAmnt();
        this.GetMoney = aLPEdorItemSchema.getGetMoney();
        this.GetInterest = aLPEdorItemSchema.getGetInterest();
        this.Operator = aLPEdorItemSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPEdorItemSchema.getMakeDate());
        this.MakeTime = aLPEdorItemSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPEdorItemSchema.getModifyDate());
        this.ModifyTime = aLPEdorItemSchema.getModifyTime();
        this.EdorReasonNo = aLPEdorItemSchema.getEdorReasonNo();
        this.EdorReason = aLPEdorItemSchema.getEdorReason();
        this.Reason = aLPEdorItemSchema.getReason();
        this.ReasonCode = aLPEdorItemSchema.getReasonCode();
        this.ApproveGrade = aLPEdorItemSchema.getApproveGrade();
        this.ApproveState = aLPEdorItemSchema.getApproveState();
        this.ApproveOperator = aLPEdorItemSchema.getApproveOperator();
        this.ApproveDate = fDate.getDate(aLPEdorItemSchema.getApproveDate());
        this.ApproveTime = aLPEdorItemSchema.getApproveTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorAcceptNo") == null)
            {
                this.EdorAcceptNo = null;
            }
            else
            {
                this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();
            }

            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorAppNo") == null)
            {
                this.EdorAppNo = null;
            }
            else
            {
                this.EdorAppNo = rs.getString("EdorAppNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("DisplayType") == null)
            {
                this.DisplayType = null;
            }
            else
            {
                this.DisplayType = rs.getString("DisplayType").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.EdorValiDate = rs.getDate("EdorValiDate");
            this.EdorAppDate = rs.getDate("EdorAppDate");
            if (rs.getString("EdorState") == null)
            {
                this.EdorState = null;
            }
            else
            {
                this.EdorState = rs.getString("EdorState").trim();
            }

            if (rs.getString("UWFlag") == null)
            {
                this.UWFlag = null;
            }
            else
            {
                this.UWFlag = rs.getString("UWFlag").trim();
            }

            if (rs.getString("UWOperator") == null)
            {
                this.UWOperator = null;
            }
            else
            {
                this.UWOperator = rs.getString("UWOperator").trim();
            }

            this.UWDate = rs.getDate("UWDate");
            if (rs.getString("UWTime") == null)
            {
                this.UWTime = null;
            }
            else
            {
                this.UWTime = rs.getString("UWTime").trim();
            }

            this.ChgPrem = rs.getDouble("ChgPrem");
            this.ChgAmnt = rs.getDouble("ChgAmnt");
            this.GetMoney = rs.getDouble("GetMoney");
            this.GetInterest = rs.getDouble("GetInterest");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("EdorReasonNo") == null)
            {
                this.EdorReasonNo = null;
            }
            else
            {
                this.EdorReasonNo = rs.getString("EdorReasonNo").trim();
            }

            if (rs.getString("EdorReason") == null)
            {
                this.EdorReason = null;
            }
            else
            {
                this.EdorReason = rs.getString("EdorReason").trim();
            }

            if (rs.getString("Reason") == null)
            {
                this.Reason = null;
            }
            else
            {
                this.Reason = rs.getString("Reason").trim();
            }

            if (rs.getString("ReasonCode") == null)
            {
                this.ReasonCode = null;
            }
            else
            {
                this.ReasonCode = rs.getString("ReasonCode").trim();
            }

            if (rs.getString("ApproveGrade") == null)
            {
                this.ApproveGrade = null;
            }
            else
            {
                this.ApproveGrade = rs.getString("ApproveGrade").trim();
            }

            if (rs.getString("ApproveState") == null)
            {
                this.ApproveState = null;
            }
            else
            {
                this.ApproveState = rs.getString("ApproveState").trim();
            }

            if (rs.getString("ApproveOperator") == null)
            {
                this.ApproveOperator = null;
            }
            else
            {
                this.ApproveOperator = rs.getString("ApproveOperator").trim();
            }

            this.ApproveDate = rs.getDate("ApproveDate");
            if (rs.getString("ApproveTime") == null)
            {
                this.ApproveTime = null;
            }
            else
            {
                this.ApproveTime = rs.getString("ApproveTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LPEdorItem表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPEdorItemSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LPEdorItemSchema getSchema()
    {
        LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
        aLPEdorItemSchema.setSchema(this);
        return aLPEdorItemSchema;
    }

    public LPEdorItemDB getDB()
    {
        LPEdorItemDB aDBOper = new LPEdorItemDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPEdorItem描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(EdorAcceptNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorAppNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisplayType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EdorValiDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EdorAppDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(UWDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ChgPrem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ChgAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetInterest));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorReasonNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorReason));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Reason));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReasonCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveGrade));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ApproveDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPEdorItem>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            EdorAppNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            DisplayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            EdorValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            EdorAppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            EdorState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                    SysConst.PACKAGESPILTER);
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                    SysConst.PACKAGESPILTER);
            ChgPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            ChgAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            GetInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                        SysConst.PACKAGESPILTER);
            EdorReasonNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                          SysConst.PACKAGESPILTER);
            EdorReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
            Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                    SysConst.PACKAGESPILTER);
            ReasonCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                        SysConst.PACKAGESPILTER);
            ApproveGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                          SysConst.PACKAGESPILTER);
            ApproveState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                          SysConst.PACKAGESPILTER);
            ApproveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             33, SysConst.PACKAGESPILTER);
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 34, SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPEdorItemSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorAcceptNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("EdorAppNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAppNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equals("DisplayType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisplayType));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("EdorValiDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getEdorValiDate()));
        }
        if (FCode.equals("EdorAppDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEdorAppDate()));
        }
        if (FCode.equals("EdorState"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorState));
        }
        if (FCode.equals("UWFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equals("UWOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equals("UWDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getUWDate()));
        }
        if (FCode.equals("UWTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equals("ChgPrem"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgPrem));
        }
        if (FCode.equals("ChgAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgAmnt));
        }
        if (FCode.equals("GetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMoney));
        }
        if (FCode.equals("GetInterest"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetInterest));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("EdorReasonNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorReasonNo));
        }
        if (FCode.equals("EdorReason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorReason));
        }
        if (FCode.equals("Reason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
        }
        if (FCode.equals("ReasonCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReasonCode));
        }
        if (FCode.equals("ApproveGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveGrade));
        }
        if (FCode.equals("ApproveState"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveState));
        }
        if (FCode.equals("ApproveOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOperator));
        }
        if (FCode.equals("ApproveDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getApproveDate()));
        }
        if (FCode.equals("ApproveTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorAppNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DisplayType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEdorValiDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEdorAppDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(EdorState);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(UWFlag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(UWOperator);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getUWDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(UWTime);
                break;
            case 17:
                strFieldValue = String.valueOf(ChgPrem);
                break;
            case 18:
                strFieldValue = String.valueOf(ChgAmnt);
                break;
            case 19:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 20:
                strFieldValue = String.valueOf(GetInterest);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(EdorReasonNo);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(EdorReason);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(Reason);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ReasonCode);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(ApproveGrade);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(ApproveState);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(ApproveOperator);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getApproveDate()));
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorAcceptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
            {
                EdorAcceptNo = null;
            }
        }
        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("EdorAppNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAppNo = FValue.trim();
            }
            else
            {
                EdorAppNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("DisplayType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DisplayType = FValue.trim();
            }
            else
            {
                DisplayType = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("EdorValiDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorValiDate = fDate.getDate(FValue);
            }
            else
            {
                EdorValiDate = null;
            }
        }
        if (FCode.equals("EdorAppDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAppDate = fDate.getDate(FValue);
            }
            else
            {
                EdorAppDate = null;
            }
        }
        if (FCode.equals("EdorState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorState = FValue.trim();
            }
            else
            {
                EdorState = null;
            }
        }
        if (FCode.equals("UWFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
            {
                UWFlag = null;
            }
        }
        if (FCode.equals("UWOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
            {
                UWOperator = null;
            }
        }
        if (FCode.equals("UWDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWDate = fDate.getDate(FValue);
            }
            else
            {
                UWDate = null;
            }
        }
        if (FCode.equals("UWTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
            {
                UWTime = null;
            }
        }
        if (FCode.equals("ChgPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChgPrem = d;
            }
        }
        if (FCode.equals("ChgAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChgAmnt = d;
            }
        }
        if (FCode.equals("GetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equals("GetInterest"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetInterest = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("EdorReasonNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorReasonNo = FValue.trim();
            }
            else
            {
                EdorReasonNo = null;
            }
        }
        if (FCode.equals("EdorReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorReason = FValue.trim();
            }
            else
            {
                EdorReason = null;
            }
        }
        if (FCode.equals("Reason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Reason = FValue.trim();
            }
            else
            {
                Reason = null;
            }
        }
        if (FCode.equals("ReasonCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReasonCode = FValue.trim();
            }
            else
            {
                ReasonCode = null;
            }
        }
        if (FCode.equals("ApproveGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveGrade = FValue.trim();
            }
            else
            {
                ApproveGrade = null;
            }
        }
        if (FCode.equals("ApproveState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveState = FValue.trim();
            }
            else
            {
                ApproveState = null;
            }
        }
        if (FCode.equals("ApproveOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveOperator = FValue.trim();
            }
            else
            {
                ApproveOperator = null;
            }
        }
        if (FCode.equals("ApproveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveDate = fDate.getDate(FValue);
            }
            else
            {
                ApproveDate = null;
            }
        }
        if (FCode.equals("ApproveTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
            {
                ApproveTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPEdorItemSchema other = (LPEdorItemSchema) otherObject;
        return
                EdorAcceptNo.equals(other.getEdorAcceptNo())
                && EdorNo.equals(other.getEdorNo())
                && EdorAppNo.equals(other.getEdorAppNo())
                && EdorType.equals(other.getEdorType())
                && DisplayType.equals(other.getDisplayType())
                && GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && InsuredNo.equals(other.getInsuredNo())
                && PolNo.equals(other.getPolNo())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(EdorValiDate).equals(other.getEdorValiDate())
                && fDate.getString(EdorAppDate).equals(other.getEdorAppDate())
                && EdorState.equals(other.getEdorState())
                && UWFlag.equals(other.getUWFlag())
                && UWOperator.equals(other.getUWOperator())
                && fDate.getString(UWDate).equals(other.getUWDate())
                && UWTime.equals(other.getUWTime())
                && ChgPrem == other.getChgPrem()
                && ChgAmnt == other.getChgAmnt()
                && GetMoney == other.getGetMoney()
                && GetInterest == other.getGetInterest()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && EdorReasonNo.equals(other.getEdorReasonNo())
                && EdorReason.equals(other.getEdorReason())
                && Reason.equals(other.getReason())
                && ReasonCode.equals(other.getReasonCode())
                && ApproveGrade.equals(other.getApproveGrade())
                && ApproveState.equals(other.getApproveState())
                && ApproveOperator.equals(other.getApproveOperator())
                && fDate.getString(ApproveDate).equals(other.getApproveDate())
                && ApproveTime.equals(other.getApproveTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorAcceptNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return 1;
        }
        if (strFieldName.equals("EdorAppNo"))
        {
            return 2;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 3;
        }
        if (strFieldName.equals("DisplayType"))
        {
            return 4;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 5;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 6;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 7;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 8;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 9;
        }
        if (strFieldName.equals("EdorValiDate"))
        {
            return 10;
        }
        if (strFieldName.equals("EdorAppDate"))
        {
            return 11;
        }
        if (strFieldName.equals("EdorState"))
        {
            return 12;
        }
        if (strFieldName.equals("UWFlag"))
        {
            return 13;
        }
        if (strFieldName.equals("UWOperator"))
        {
            return 14;
        }
        if (strFieldName.equals("UWDate"))
        {
            return 15;
        }
        if (strFieldName.equals("UWTime"))
        {
            return 16;
        }
        if (strFieldName.equals("ChgPrem"))
        {
            return 17;
        }
        if (strFieldName.equals("ChgAmnt"))
        {
            return 18;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return 19;
        }
        if (strFieldName.equals("GetInterest"))
        {
            return 20;
        }
        if (strFieldName.equals("Operator"))
        {
            return 21;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 22;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 23;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 24;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 25;
        }
        if (strFieldName.equals("EdorReasonNo"))
        {
            return 26;
        }
        if (strFieldName.equals("EdorReason"))
        {
            return 27;
        }
        if (strFieldName.equals("Reason"))
        {
            return 28;
        }
        if (strFieldName.equals("ReasonCode"))
        {
            return 29;
        }
        if (strFieldName.equals("ApproveGrade"))
        {
            return 30;
        }
        if (strFieldName.equals("ApproveState"))
        {
            return 31;
        }
        if (strFieldName.equals("ApproveOperator"))
        {
            return 32;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return 33;
        }
        if (strFieldName.equals("ApproveTime"))
        {
            return 34;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorAcceptNo";
                break;
            case 1:
                strFieldName = "EdorNo";
                break;
            case 2:
                strFieldName = "EdorAppNo";
                break;
            case 3:
                strFieldName = "EdorType";
                break;
            case 4:
                strFieldName = "DisplayType";
                break;
            case 5:
                strFieldName = "GrpContNo";
                break;
            case 6:
                strFieldName = "ContNo";
                break;
            case 7:
                strFieldName = "InsuredNo";
                break;
            case 8:
                strFieldName = "PolNo";
                break;
            case 9:
                strFieldName = "ManageCom";
                break;
            case 10:
                strFieldName = "EdorValiDate";
                break;
            case 11:
                strFieldName = "EdorAppDate";
                break;
            case 12:
                strFieldName = "EdorState";
                break;
            case 13:
                strFieldName = "UWFlag";
                break;
            case 14:
                strFieldName = "UWOperator";
                break;
            case 15:
                strFieldName = "UWDate";
                break;
            case 16:
                strFieldName = "UWTime";
                break;
            case 17:
                strFieldName = "ChgPrem";
                break;
            case 18:
                strFieldName = "ChgAmnt";
                break;
            case 19:
                strFieldName = "GetMoney";
                break;
            case 20:
                strFieldName = "GetInterest";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "EdorReasonNo";
                break;
            case 27:
                strFieldName = "EdorReason";
                break;
            case 28:
                strFieldName = "Reason";
                break;
            case 29:
                strFieldName = "ReasonCode";
                break;
            case 30:
                strFieldName = "ApproveGrade";
                break;
            case 31:
                strFieldName = "ApproveState";
                break;
            case 32:
                strFieldName = "ApproveOperator";
                break;
            case 33:
                strFieldName = "ApproveDate";
                break;
            case 34:
                strFieldName = "ApproveTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorAcceptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorAppNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DisplayType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorValiDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EdorAppDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EdorState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("UWTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChgPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ChgAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetInterest"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorReasonNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Reason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReasonCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ApproveTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 33:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 34:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
