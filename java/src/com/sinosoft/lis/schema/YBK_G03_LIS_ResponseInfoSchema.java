/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.YBK_G03_LIS_ResponseInfoDB;

/*
 * <p>ClassName: YBK_G03_LIS_ResponseInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-03-08
 */
public class YBK_G03_LIS_ResponseInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 投保流水号 */
	private String PolicyFormerNo;
	/** 医保扣款流水号 */
	private String YBKKSequenceNo;
	/** 医保验证扣款是否成功 */
	private String MedicalIfSucess;
	/** 医保扣款时间 */
	private String DecuctMoney;
	/** 扣款失败原因 */
	private String FailReason;
	/** 医保人员类型 */
	private String IfSpecialPerson;
	/** 医保实际扣款金额 */
	private double ActualDecuctPremium;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public YBK_G03_LIS_ResponseInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "PolicyFormerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		YBK_G03_LIS_ResponseInfoSchema cloned = (YBK_G03_LIS_ResponseInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPolicyFormerNo()
	{
		return PolicyFormerNo;
	}
	public void setPolicyFormerNo(String aPolicyFormerNo)
	{
		PolicyFormerNo = aPolicyFormerNo;
	}
	public String getYBKKSequenceNo()
	{
		return YBKKSequenceNo;
	}
	public void setYBKKSequenceNo(String aYBKKSequenceNo)
	{
		YBKKSequenceNo = aYBKKSequenceNo;
	}
	public String getMedicalIfSucess()
	{
		return MedicalIfSucess;
	}
	public void setMedicalIfSucess(String aMedicalIfSucess)
	{
		MedicalIfSucess = aMedicalIfSucess;
	}
	public String getDecuctMoney()
	{
		return DecuctMoney;
	}
	public void setDecuctMoney(String aDecuctMoney)
	{
		DecuctMoney = aDecuctMoney;
	}
	public String getFailReason()
	{
		return FailReason;
	}
	public void setFailReason(String aFailReason)
	{
		FailReason = aFailReason;
	}
	public String getIfSpecialPerson()
	{
		return IfSpecialPerson;
	}
	public void setIfSpecialPerson(String aIfSpecialPerson)
	{
		IfSpecialPerson = aIfSpecialPerson;
	}
	public double getActualDecuctPremium()
	{
		return ActualDecuctPremium;
	}
	public void setActualDecuctPremium(double aActualDecuctPremium)
	{
		ActualDecuctPremium = Arith.round(aActualDecuctPremium,2);
	}
	public void setActualDecuctPremium(String aActualDecuctPremium)
	{
		if (aActualDecuctPremium != null && !aActualDecuctPremium.equals(""))
		{
			Double tDouble = new Double(aActualDecuctPremium);
			double d = tDouble.doubleValue();
                ActualDecuctPremium = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 YBK_G03_LIS_ResponseInfoSchema 对象给 Schema 赋值
	* @param: aYBK_G03_LIS_ResponseInfoSchema YBK_G03_LIS_ResponseInfoSchema
	**/
	public void setSchema(YBK_G03_LIS_ResponseInfoSchema aYBK_G03_LIS_ResponseInfoSchema)
	{
		this.PolicyFormerNo = aYBK_G03_LIS_ResponseInfoSchema.getPolicyFormerNo();
		this.YBKKSequenceNo = aYBK_G03_LIS_ResponseInfoSchema.getYBKKSequenceNo();
		this.MedicalIfSucess = aYBK_G03_LIS_ResponseInfoSchema.getMedicalIfSucess();
		this.DecuctMoney = aYBK_G03_LIS_ResponseInfoSchema.getDecuctMoney();
		this.FailReason = aYBK_G03_LIS_ResponseInfoSchema.getFailReason();
		this.IfSpecialPerson = aYBK_G03_LIS_ResponseInfoSchema.getIfSpecialPerson();
		this.ActualDecuctPremium = aYBK_G03_LIS_ResponseInfoSchema.getActualDecuctPremium();
		this.Operator = aYBK_G03_LIS_ResponseInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aYBK_G03_LIS_ResponseInfoSchema.getMakeDate());
		this.MakeTime = aYBK_G03_LIS_ResponseInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aYBK_G03_LIS_ResponseInfoSchema.getModifyDate());
		this.ModifyTime = aYBK_G03_LIS_ResponseInfoSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PolicyFormerNo") == null )
				this.PolicyFormerNo = null;
			else
				this.PolicyFormerNo = rs.getString("PolicyFormerNo").trim();

			if( rs.getString("YBKKSequenceNo") == null )
				this.YBKKSequenceNo = null;
			else
				this.YBKKSequenceNo = rs.getString("YBKKSequenceNo").trim();

			if( rs.getString("MedicalIfSucess") == null )
				this.MedicalIfSucess = null;
			else
				this.MedicalIfSucess = rs.getString("MedicalIfSucess").trim();

			if( rs.getString("DecuctMoney") == null )
				this.DecuctMoney = null;
			else
				this.DecuctMoney = rs.getString("DecuctMoney").trim();

			if( rs.getString("FailReason") == null )
				this.FailReason = null;
			else
				this.FailReason = rs.getString("FailReason").trim();

			if( rs.getString("IfSpecialPerson") == null )
				this.IfSpecialPerson = null;
			else
				this.IfSpecialPerson = rs.getString("IfSpecialPerson").trim();

			this.ActualDecuctPremium = rs.getDouble("ActualDecuctPremium");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的YBK_G03_LIS_ResponseInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBK_G03_LIS_ResponseInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public YBK_G03_LIS_ResponseInfoSchema getSchema()
	{
		YBK_G03_LIS_ResponseInfoSchema aYBK_G03_LIS_ResponseInfoSchema = new YBK_G03_LIS_ResponseInfoSchema();
		aYBK_G03_LIS_ResponseInfoSchema.setSchema(this);
		return aYBK_G03_LIS_ResponseInfoSchema;
	}

	public YBK_G03_LIS_ResponseInfoDB getDB()
	{
		YBK_G03_LIS_ResponseInfoDB aDBOper = new YBK_G03_LIS_ResponseInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBK_G03_LIS_ResponseInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PolicyFormerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(YBKKSequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MedicalIfSucess)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DecuctMoney)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FailReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IfSpecialPerson)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ActualDecuctPremium));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBK_G03_LIS_ResponseInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PolicyFormerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			YBKKSequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			MedicalIfSucess = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DecuctMoney = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			FailReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IfSpecialPerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ActualDecuctPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBK_G03_LIS_ResponseInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PolicyFormerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyFormerNo));
		}
		if (FCode.equals("YBKKSequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YBKKSequenceNo));
		}
		if (FCode.equals("MedicalIfSucess"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicalIfSucess));
		}
		if (FCode.equals("DecuctMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DecuctMoney));
		}
		if (FCode.equals("FailReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FailReason));
		}
		if (FCode.equals("IfSpecialPerson"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IfSpecialPerson));
		}
		if (FCode.equals("ActualDecuctPremium"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActualDecuctPremium));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PolicyFormerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(YBKKSequenceNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(MedicalIfSucess);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DecuctMoney);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(FailReason);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IfSpecialPerson);
				break;
			case 6:
				strFieldValue = String.valueOf(ActualDecuctPremium);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PolicyFormerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyFormerNo = FValue.trim();
			}
			else
				PolicyFormerNo = null;
		}
		if (FCode.equalsIgnoreCase("YBKKSequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				YBKKSequenceNo = FValue.trim();
			}
			else
				YBKKSequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("MedicalIfSucess"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicalIfSucess = FValue.trim();
			}
			else
				MedicalIfSucess = null;
		}
		if (FCode.equalsIgnoreCase("DecuctMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DecuctMoney = FValue.trim();
			}
			else
				DecuctMoney = null;
		}
		if (FCode.equalsIgnoreCase("FailReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FailReason = FValue.trim();
			}
			else
				FailReason = null;
		}
		if (FCode.equalsIgnoreCase("IfSpecialPerson"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IfSpecialPerson = FValue.trim();
			}
			else
				IfSpecialPerson = null;
		}
		if (FCode.equalsIgnoreCase("ActualDecuctPremium"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ActualDecuctPremium = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		YBK_G03_LIS_ResponseInfoSchema other = (YBK_G03_LIS_ResponseInfoSchema)otherObject;
		return
			(PolicyFormerNo == null ? other.getPolicyFormerNo() == null : PolicyFormerNo.equals(other.getPolicyFormerNo()))
			&& (YBKKSequenceNo == null ? other.getYBKKSequenceNo() == null : YBKKSequenceNo.equals(other.getYBKKSequenceNo()))
			&& (MedicalIfSucess == null ? other.getMedicalIfSucess() == null : MedicalIfSucess.equals(other.getMedicalIfSucess()))
			&& (DecuctMoney == null ? other.getDecuctMoney() == null : DecuctMoney.equals(other.getDecuctMoney()))
			&& (FailReason == null ? other.getFailReason() == null : FailReason.equals(other.getFailReason()))
			&& (IfSpecialPerson == null ? other.getIfSpecialPerson() == null : IfSpecialPerson.equals(other.getIfSpecialPerson()))
			&& ActualDecuctPremium == other.getActualDecuctPremium()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PolicyFormerNo") ) {
			return 0;
		}
		if( strFieldName.equals("YBKKSequenceNo") ) {
			return 1;
		}
		if( strFieldName.equals("MedicalIfSucess") ) {
			return 2;
		}
		if( strFieldName.equals("DecuctMoney") ) {
			return 3;
		}
		if( strFieldName.equals("FailReason") ) {
			return 4;
		}
		if( strFieldName.equals("IfSpecialPerson") ) {
			return 5;
		}
		if( strFieldName.equals("ActualDecuctPremium") ) {
			return 6;
		}
		if( strFieldName.equals("Operator") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PolicyFormerNo";
				break;
			case 1:
				strFieldName = "YBKKSequenceNo";
				break;
			case 2:
				strFieldName = "MedicalIfSucess";
				break;
			case 3:
				strFieldName = "DecuctMoney";
				break;
			case 4:
				strFieldName = "FailReason";
				break;
			case 5:
				strFieldName = "IfSpecialPerson";
				break;
			case 6:
				strFieldName = "ActualDecuctPremium";
				break;
			case 7:
				strFieldName = "Operator";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PolicyFormerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("YBKKSequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MedicalIfSucess") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DecuctMoney") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FailReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IfSpecialPerson") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActualDecuctPremium") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
