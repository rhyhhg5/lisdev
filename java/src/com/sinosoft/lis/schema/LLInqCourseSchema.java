/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLInqCourseDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLInqCourseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 理赔调查相关表
 * @CreateDate：2005-06-02
 */
public class LLInqCourseSchema implements Schema, Cloneable
{
    // @Field
    /** 对应号码 */
    private String OtherNo;
    /** 其他对应号码类型 */
    private String OtherNoType;
    /** 调查号 */
    private String SurveyNo;
    /** 调查项目号 */
    private String InqNo;
    /** 过程序号 */
    private String CouNo;
    /** 调查日期 */
    private Date InqDate;
    /** 调查方式 */
    private String InqMode;
    /** 调查地点 */
    private String InqSite;
    /** 被调查人 */
    private String InqByPer;
    /** 调查过程 */
    private String InqCourse;
    /** 调查机构 */
    private String InqDept;
    /** 第一调查人 */
    private String InqPer1;
    /** 第二调查人 */
    private String InqPer2;
    /** 调查途径 */
    private String InqWay;
    /** 事实与证据 */
    private String InqEvidence;
    /** 调查结论 */
    private String InqConclusion;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLInqCourseSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "SurveyNo";
        pk[1] = "InqNo";
        pk[2] = "CouNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLInqCourseSchema cloned = (LLInqCourseSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getOtherNo()
    {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getSurveyNo()
    {
        return SurveyNo;
    }

    public void setSurveyNo(String aSurveyNo)
    {
        SurveyNo = aSurveyNo;
    }

    public String getInqNo()
    {
        return InqNo;
    }

    public void setInqNo(String aInqNo)
    {
        InqNo = aInqNo;
    }

    public String getCouNo()
    {
        return CouNo;
    }

    public void setCouNo(String aCouNo)
    {
        CouNo = aCouNo;
    }

    public String getInqDate()
    {
        if (InqDate != null)
        {
            return fDate.getString(InqDate);
        }
        else
        {
            return null;
        }
    }

    public void setInqDate(Date aInqDate)
    {
        InqDate = aInqDate;
    }

    public void setInqDate(String aInqDate)
    {
        if (aInqDate != null && !aInqDate.equals(""))
        {
            InqDate = fDate.getDate(aInqDate);
        }
        else
        {
            InqDate = null;
        }
    }

    public String getInqMode()
    {
        return InqMode;
    }

    public void setInqMode(String aInqMode)
    {
        InqMode = aInqMode;
    }

    public String getInqSite()
    {
        return InqSite;
    }

    public void setInqSite(String aInqSite)
    {
        InqSite = aInqSite;
    }

    public String getInqByPer()
    {
        return InqByPer;
    }

    public void setInqByPer(String aInqByPer)
    {
        InqByPer = aInqByPer;
    }

    public String getInqCourse()
    {
        return InqCourse;
    }

    public void setInqCourse(String aInqCourse)
    {
        InqCourse = aInqCourse;
    }

    public String getInqDept()
    {
        return InqDept;
    }

    public void setInqDept(String aInqDept)
    {
        InqDept = aInqDept;
    }

    public String getInqPer1()
    {
        return InqPer1;
    }

    public void setInqPer1(String aInqPer1)
    {
        InqPer1 = aInqPer1;
    }

    public String getInqPer2()
    {
        return InqPer2;
    }

    public void setInqPer2(String aInqPer2)
    {
        InqPer2 = aInqPer2;
    }

    public String getInqWay()
    {
        return InqWay;
    }

    public void setInqWay(String aInqWay)
    {
        InqWay = aInqWay;
    }

    public String getInqEvidence()
    {
        return InqEvidence;
    }

    public void setInqEvidence(String aInqEvidence)
    {
        InqEvidence = aInqEvidence;
    }

    public String getInqConclusion()
    {
        return InqConclusion;
    }

    public void setInqConclusion(String aInqConclusion)
    {
        InqConclusion = aInqConclusion;
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLInqCourseSchema 对象给 Schema 赋值
     * @param: aLLInqCourseSchema LLInqCourseSchema
     **/
    public void setSchema(LLInqCourseSchema aLLInqCourseSchema)
    {
        this.OtherNo = aLLInqCourseSchema.getOtherNo();
        this.OtherNoType = aLLInqCourseSchema.getOtherNoType();
        this.SurveyNo = aLLInqCourseSchema.getSurveyNo();
        this.InqNo = aLLInqCourseSchema.getInqNo();
        this.CouNo = aLLInqCourseSchema.getCouNo();
        this.InqDate = fDate.getDate(aLLInqCourseSchema.getInqDate());
        this.InqMode = aLLInqCourseSchema.getInqMode();
        this.InqSite = aLLInqCourseSchema.getInqSite();
        this.InqByPer = aLLInqCourseSchema.getInqByPer();
        this.InqCourse = aLLInqCourseSchema.getInqCourse();
        this.InqDept = aLLInqCourseSchema.getInqDept();
        this.InqPer1 = aLLInqCourseSchema.getInqPer1();
        this.InqPer2 = aLLInqCourseSchema.getInqPer2();
        this.InqWay = aLLInqCourseSchema.getInqWay();
        this.InqEvidence = aLLInqCourseSchema.getInqEvidence();
        this.InqConclusion = aLLInqCourseSchema.getInqConclusion();
        this.Remark = aLLInqCourseSchema.getRemark();
        this.Operator = aLLInqCourseSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLInqCourseSchema.getMakeDate());
        this.MakeTime = aLLInqCourseSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLInqCourseSchema.getModifyDate());
        this.ModifyTime = aLLInqCourseSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("SurveyNo") == null)
            {
                this.SurveyNo = null;
            }
            else
            {
                this.SurveyNo = rs.getString("SurveyNo").trim();
            }

            if (rs.getString("InqNo") == null)
            {
                this.InqNo = null;
            }
            else
            {
                this.InqNo = rs.getString("InqNo").trim();
            }

            if (rs.getString("CouNo") == null)
            {
                this.CouNo = null;
            }
            else
            {
                this.CouNo = rs.getString("CouNo").trim();
            }

            this.InqDate = rs.getDate("InqDate");
            if (rs.getString("InqMode") == null)
            {
                this.InqMode = null;
            }
            else
            {
                this.InqMode = rs.getString("InqMode").trim();
            }

            if (rs.getString("InqSite") == null)
            {
                this.InqSite = null;
            }
            else
            {
                this.InqSite = rs.getString("InqSite").trim();
            }

            if (rs.getString("InqByPer") == null)
            {
                this.InqByPer = null;
            }
            else
            {
                this.InqByPer = rs.getString("InqByPer").trim();
            }

            if (rs.getString("InqCourse") == null)
            {
                this.InqCourse = null;
            }
            else
            {
                this.InqCourse = rs.getString("InqCourse").trim();
            }

            if (rs.getString("InqDept") == null)
            {
                this.InqDept = null;
            }
            else
            {
                this.InqDept = rs.getString("InqDept").trim();
            }

            if (rs.getString("InqPer1") == null)
            {
                this.InqPer1 = null;
            }
            else
            {
                this.InqPer1 = rs.getString("InqPer1").trim();
            }

            if (rs.getString("InqPer2") == null)
            {
                this.InqPer2 = null;
            }
            else
            {
                this.InqPer2 = rs.getString("InqPer2").trim();
            }

            if (rs.getString("InqWay") == null)
            {
                this.InqWay = null;
            }
            else
            {
                this.InqWay = rs.getString("InqWay").trim();
            }

            if (rs.getString("InqEvidence") == null)
            {
                this.InqEvidence = null;
            }
            else
            {
                this.InqEvidence = rs.getString("InqEvidence").trim();
            }

            if (rs.getString("InqConclusion") == null)
            {
                this.InqConclusion = null;
            }
            else
            {
                this.InqConclusion = rs.getString("InqConclusion").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LLInqCourse表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqCourseSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLInqCourseSchema getSchema()
    {
        LLInqCourseSchema aLLInqCourseSchema = new LLInqCourseSchema();
        aLLInqCourseSchema.setSchema(this);
        return aLLInqCourseSchema;
    }

    public LLInqCourseDB getDB()
    {
        LLInqCourseDB aDBOper = new LLInqCourseDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqCourse描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CouNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(InqDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqMode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqSite));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqByPer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqCourse));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqDept));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqPer1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqPer2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqWay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqEvidence));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqConclusion));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqCourse>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            SurveyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            InqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            CouNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            InqDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            InqMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            InqSite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            InqByPer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            InqCourse = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            InqDept = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            InqPer1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                     SysConst.PACKAGESPILTER);
            InqPer2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            InqWay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                    SysConst.PACKAGESPILTER);
            InqEvidence = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            InqConclusion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                           SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqCourseSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equals("SurveyNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyNo));
        }
        if (FCode.equals("InqNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqNo));
        }
        if (FCode.equals("CouNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CouNo));
        }
        if (FCode.equals("InqDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getInqDate()));
        }
        if (FCode.equals("InqMode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqMode));
        }
        if (FCode.equals("InqSite"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqSite));
        }
        if (FCode.equals("InqByPer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqByPer));
        }
        if (FCode.equals("InqCourse"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqCourse));
        }
        if (FCode.equals("InqDept"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqDept));
        }
        if (FCode.equals("InqPer1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqPer1));
        }
        if (FCode.equals("InqPer2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqPer2));
        }
        if (FCode.equals("InqWay"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqWay));
        }
        if (FCode.equals("InqEvidence"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqEvidence));
        }
        if (FCode.equals("InqConclusion"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqConclusion));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SurveyNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InqNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CouNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInqDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(InqMode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(InqSite);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(InqByPer);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(InqCourse);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InqDept);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(InqPer1);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(InqPer2);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(InqWay);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(InqEvidence);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InqConclusion);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("SurveyNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyNo = FValue.trim();
            }
            else
            {
                SurveyNo = null;
            }
        }
        if (FCode.equals("InqNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqNo = FValue.trim();
            }
            else
            {
                InqNo = null;
            }
        }
        if (FCode.equals("CouNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CouNo = FValue.trim();
            }
            else
            {
                CouNo = null;
            }
        }
        if (FCode.equals("InqDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqDate = fDate.getDate(FValue);
            }
            else
            {
                InqDate = null;
            }
        }
        if (FCode.equals("InqMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqMode = FValue.trim();
            }
            else
            {
                InqMode = null;
            }
        }
        if (FCode.equals("InqSite"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqSite = FValue.trim();
            }
            else
            {
                InqSite = null;
            }
        }
        if (FCode.equals("InqByPer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqByPer = FValue.trim();
            }
            else
            {
                InqByPer = null;
            }
        }
        if (FCode.equals("InqCourse"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqCourse = FValue.trim();
            }
            else
            {
                InqCourse = null;
            }
        }
        if (FCode.equals("InqDept"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqDept = FValue.trim();
            }
            else
            {
                InqDept = null;
            }
        }
        if (FCode.equals("InqPer1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqPer1 = FValue.trim();
            }
            else
            {
                InqPer1 = null;
            }
        }
        if (FCode.equals("InqPer2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqPer2 = FValue.trim();
            }
            else
            {
                InqPer2 = null;
            }
        }
        if (FCode.equals("InqWay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqWay = FValue.trim();
            }
            else
            {
                InqWay = null;
            }
        }
        if (FCode.equals("InqEvidence"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqEvidence = FValue.trim();
            }
            else
            {
                InqEvidence = null;
            }
        }
        if (FCode.equals("InqConclusion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqConclusion = FValue.trim();
            }
            else
            {
                InqConclusion = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLInqCourseSchema other = (LLInqCourseSchema) otherObject;
        return
                OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && SurveyNo.equals(other.getSurveyNo())
                && InqNo.equals(other.getInqNo())
                && CouNo.equals(other.getCouNo())
                && fDate.getString(InqDate).equals(other.getInqDate())
                && InqMode.equals(other.getInqMode())
                && InqSite.equals(other.getInqSite())
                && InqByPer.equals(other.getInqByPer())
                && InqCourse.equals(other.getInqCourse())
                && InqDept.equals(other.getInqDept())
                && InqPer1.equals(other.getInqPer1())
                && InqPer2.equals(other.getInqPer2())
                && InqWay.equals(other.getInqWay())
                && InqEvidence.equals(other.getInqEvidence())
                && InqConclusion.equals(other.getInqConclusion())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("OtherNo"))
        {
            return 0;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 1;
        }
        if (strFieldName.equals("SurveyNo"))
        {
            return 2;
        }
        if (strFieldName.equals("InqNo"))
        {
            return 3;
        }
        if (strFieldName.equals("CouNo"))
        {
            return 4;
        }
        if (strFieldName.equals("InqDate"))
        {
            return 5;
        }
        if (strFieldName.equals("InqMode"))
        {
            return 6;
        }
        if (strFieldName.equals("InqSite"))
        {
            return 7;
        }
        if (strFieldName.equals("InqByPer"))
        {
            return 8;
        }
        if (strFieldName.equals("InqCourse"))
        {
            return 9;
        }
        if (strFieldName.equals("InqDept"))
        {
            return 10;
        }
        if (strFieldName.equals("InqPer1"))
        {
            return 11;
        }
        if (strFieldName.equals("InqPer2"))
        {
            return 12;
        }
        if (strFieldName.equals("InqWay"))
        {
            return 13;
        }
        if (strFieldName.equals("InqEvidence"))
        {
            return 14;
        }
        if (strFieldName.equals("InqConclusion"))
        {
            return 15;
        }
        if (strFieldName.equals("Remark"))
        {
            return 16;
        }
        if (strFieldName.equals("Operator"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "OtherNo";
                break;
            case 1:
                strFieldName = "OtherNoType";
                break;
            case 2:
                strFieldName = "SurveyNo";
                break;
            case 3:
                strFieldName = "InqNo";
                break;
            case 4:
                strFieldName = "CouNo";
                break;
            case 5:
                strFieldName = "InqDate";
                break;
            case 6:
                strFieldName = "InqMode";
                break;
            case 7:
                strFieldName = "InqSite";
                break;
            case 8:
                strFieldName = "InqByPer";
                break;
            case 9:
                strFieldName = "InqCourse";
                break;
            case 10:
                strFieldName = "InqDept";
                break;
            case 11:
                strFieldName = "InqPer1";
                break;
            case 12:
                strFieldName = "InqPer2";
                break;
            case 13:
                strFieldName = "InqWay";
                break;
            case 14:
                strFieldName = "InqEvidence";
                break;
            case 15:
                strFieldName = "InqConclusion";
                break;
            case 16:
                strFieldName = "Remark";
                break;
            case 17:
                strFieldName = "Operator";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CouNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InqMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqSite"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqByPer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqCourse"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqDept"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqPer1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqPer2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqWay"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqEvidence"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqConclusion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
