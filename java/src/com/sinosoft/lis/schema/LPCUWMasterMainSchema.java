/*
 * <p>ClassName: LPCUWMasterMainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPCUWMasterMainDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LPCUWMasterMainSchema implements Schema
{
    // @Field
    /** 批单号 */
    private String EdorNo;
    /** 核保顺序号 */
    private int UWNo;
    /** 总单/合同号码 */
    private String ContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 险种版本 */
    private String RiskVersion;
    /** 险种编码 */
    private String RiskCode;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 核保通过标志 */
    private String PassFlag;
    /** 核保级别 */
    private String UWGrade;
    /** 申请级别 */
    private String AppGrade;
    /** 延至日 */
    private String PostponeDay;
    /** 延至日期 */
    private Date PostponeDate;
    /** 是否自动核保 */
    private String AutoUWFlag;
    /** 状态 */
    private String State;
    /** 管理机构 */
    private String ManageCom;
    /** 核保意见 */
    private String UWIdea;
    /** 上报内容 */
    private String UpReportContent;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 是否体检件 */
    private String HealthFlag;
    /** 是否问题件 */
    private String QuesFlag;
    /** 是否条件承保 */
    private String SpecFlag;
    /** 加费原因 */
    private String AddPremReason;
    /** 生调标志 */
    private String ReportFlag;
    /** 核保通知书打印标记 */
    private String PrintFlag;
    /** 保险计划变更标记 */
    private String ChangePolFlag;
    /** 保险计划变更原因 */
    private String ChangePolReason;
    /** 特约原因 */
    private String SpecReason;

    public static final int FIELDNUM = 36; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPCUWMasterMainSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "EdorNo";
        pk[1] = "ContNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public int getUWNo()
    {
        return UWNo;
    }

    public void setUWNo(int aUWNo)
    {
        UWNo = aUWNo;
    }

    public void setUWNo(String aUWNo)
    {
        if (aUWNo != null && !aUWNo.equals(""))
        {
            Integer tInteger = new Integer(aUWNo);
            int i = tInteger.intValue();
            UWNo = i;
        }
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getInsuredName()
    {
        if (InsuredName != null && !InsuredName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredName = StrTool.unicodeToGBK(InsuredName);
        }
        return InsuredName;
    }

    public void setInsuredName(String aInsuredName)
    {
        InsuredName = aInsuredName;
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getAppntName()
    {
        if (AppntName != null && !AppntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getPassFlag()
    {
        if (PassFlag != null && !PassFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            PassFlag = StrTool.unicodeToGBK(PassFlag);
        }
        return PassFlag;
    }

    public void setPassFlag(String aPassFlag)
    {
        PassFlag = aPassFlag;
    }

    public String getUWGrade()
    {
        if (UWGrade != null && !UWGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWGrade = StrTool.unicodeToGBK(UWGrade);
        }
        return UWGrade;
    }

    public void setUWGrade(String aUWGrade)
    {
        UWGrade = aUWGrade;
    }

    public String getAppGrade()
    {
        if (AppGrade != null && !AppGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppGrade = StrTool.unicodeToGBK(AppGrade);
        }
        return AppGrade;
    }

    public void setAppGrade(String aAppGrade)
    {
        AppGrade = aAppGrade;
    }

    public String getPostponeDay()
    {
        if (PostponeDay != null && !PostponeDay.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PostponeDay = StrTool.unicodeToGBK(PostponeDay);
        }
        return PostponeDay;
    }

    public void setPostponeDay(String aPostponeDay)
    {
        PostponeDay = aPostponeDay;
    }

    public String getPostponeDate()
    {
        if (PostponeDate != null)
        {
            return fDate.getString(PostponeDate);
        }
        else
        {
            return null;
        }
    }

    public void setPostponeDate(Date aPostponeDate)
    {
        PostponeDate = aPostponeDate;
    }

    public void setPostponeDate(String aPostponeDate)
    {
        if (aPostponeDate != null && !aPostponeDate.equals(""))
        {
            PostponeDate = fDate.getDate(aPostponeDate);
        }
        else
        {
            PostponeDate = null;
        }
    }

    public String getAutoUWFlag()
    {
        if (AutoUWFlag != null && !AutoUWFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AutoUWFlag = StrTool.unicodeToGBK(AutoUWFlag);
        }
        return AutoUWFlag;
    }

    public void setAutoUWFlag(String aAutoUWFlag)
    {
        AutoUWFlag = aAutoUWFlag;
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET == true)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getUWIdea()
    {
        if (UWIdea != null && !UWIdea.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWIdea = StrTool.unicodeToGBK(UWIdea);
        }
        return UWIdea;
    }

    public void setUWIdea(String aUWIdea)
    {
        UWIdea = aUWIdea;
    }

    public String getUpReportContent()
    {
        if (UpReportContent != null && !UpReportContent.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UpReportContent = StrTool.unicodeToGBK(UpReportContent);
        }
        return UpReportContent;
    }

    public void setUpReportContent(String aUpReportContent)
    {
        UpReportContent = aUpReportContent;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getHealthFlag()
    {
        if (HealthFlag != null && !HealthFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HealthFlag = StrTool.unicodeToGBK(HealthFlag);
        }
        return HealthFlag;
    }

    public void setHealthFlag(String aHealthFlag)
    {
        HealthFlag = aHealthFlag;
    }

    public String getQuesFlag()
    {
        if (QuesFlag != null && !QuesFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            QuesFlag = StrTool.unicodeToGBK(QuesFlag);
        }
        return QuesFlag;
    }

    public void setQuesFlag(String aQuesFlag)
    {
        QuesFlag = aQuesFlag;
    }

    public String getSpecFlag()
    {
        if (SpecFlag != null && !SpecFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            SpecFlag = StrTool.unicodeToGBK(SpecFlag);
        }
        return SpecFlag;
    }

    public void setSpecFlag(String aSpecFlag)
    {
        SpecFlag = aSpecFlag;
    }

    public String getAddPremReason()
    {
        if (AddPremReason != null && !AddPremReason.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AddPremReason = StrTool.unicodeToGBK(AddPremReason);
        }
        return AddPremReason;
    }

    public void setAddPremReason(String aAddPremReason)
    {
        AddPremReason = aAddPremReason;
    }

    public String getReportFlag()
    {
        if (ReportFlag != null && !ReportFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReportFlag = StrTool.unicodeToGBK(ReportFlag);
        }
        return ReportFlag;
    }

    public void setReportFlag(String aReportFlag)
    {
        ReportFlag = aReportFlag;
    }

    public String getPrintFlag()
    {
        if (PrintFlag != null && !PrintFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PrintFlag = StrTool.unicodeToGBK(PrintFlag);
        }
        return PrintFlag;
    }

    public void setPrintFlag(String aPrintFlag)
    {
        PrintFlag = aPrintFlag;
    }

    public String getChangePolFlag()
    {
        if (ChangePolFlag != null && !ChangePolFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ChangePolFlag = StrTool.unicodeToGBK(ChangePolFlag);
        }
        return ChangePolFlag;
    }

    public void setChangePolFlag(String aChangePolFlag)
    {
        ChangePolFlag = aChangePolFlag;
    }

    public String getChangePolReason()
    {
        if (ChangePolReason != null && !ChangePolReason.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ChangePolReason = StrTool.unicodeToGBK(ChangePolReason);
        }
        return ChangePolReason;
    }

    public void setChangePolReason(String aChangePolReason)
    {
        ChangePolReason = aChangePolReason;
    }

    public String getSpecReason()
    {
        if (SpecReason != null && !SpecReason.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SpecReason = StrTool.unicodeToGBK(SpecReason);
        }
        return SpecReason;
    }

    public void setSpecReason(String aSpecReason)
    {
        SpecReason = aSpecReason;
    }

    /**
     * 使用另外一个 LPCUWMasterMainSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LPCUWMasterMainSchema aLPCUWMasterMainSchema)
    {
        this.EdorNo = aLPCUWMasterMainSchema.getEdorNo();
        this.UWNo = aLPCUWMasterMainSchema.getUWNo();
        this.ContNo = aLPCUWMasterMainSchema.getContNo();
        this.GrpPolNo = aLPCUWMasterMainSchema.getGrpPolNo();
        this.RiskVersion = aLPCUWMasterMainSchema.getRiskVersion();
        this.RiskCode = aLPCUWMasterMainSchema.getRiskCode();
        this.InsuredNo = aLPCUWMasterMainSchema.getInsuredNo();
        this.InsuredName = aLPCUWMasterMainSchema.getInsuredName();
        this.AppntNo = aLPCUWMasterMainSchema.getAppntNo();
        this.AppntName = aLPCUWMasterMainSchema.getAppntName();
        this.AgentCode = aLPCUWMasterMainSchema.getAgentCode();
        this.AgentGroup = aLPCUWMasterMainSchema.getAgentGroup();
        this.PassFlag = aLPCUWMasterMainSchema.getPassFlag();
        this.UWGrade = aLPCUWMasterMainSchema.getUWGrade();
        this.AppGrade = aLPCUWMasterMainSchema.getAppGrade();
        this.PostponeDay = aLPCUWMasterMainSchema.getPostponeDay();
        this.PostponeDate = fDate.getDate(aLPCUWMasterMainSchema.
                                          getPostponeDate());
        this.AutoUWFlag = aLPCUWMasterMainSchema.getAutoUWFlag();
        this.State = aLPCUWMasterMainSchema.getState();
        this.ManageCom = aLPCUWMasterMainSchema.getManageCom();
        this.UWIdea = aLPCUWMasterMainSchema.getUWIdea();
        this.UpReportContent = aLPCUWMasterMainSchema.getUpReportContent();
        this.Operator = aLPCUWMasterMainSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPCUWMasterMainSchema.getMakeDate());
        this.MakeTime = aLPCUWMasterMainSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPCUWMasterMainSchema.getModifyDate());
        this.ModifyTime = aLPCUWMasterMainSchema.getModifyTime();
        this.HealthFlag = aLPCUWMasterMainSchema.getHealthFlag();
        this.QuesFlag = aLPCUWMasterMainSchema.getQuesFlag();
        this.SpecFlag = aLPCUWMasterMainSchema.getSpecFlag();
        this.AddPremReason = aLPCUWMasterMainSchema.getAddPremReason();
        this.ReportFlag = aLPCUWMasterMainSchema.getReportFlag();
        this.PrintFlag = aLPCUWMasterMainSchema.getPrintFlag();
        this.ChangePolFlag = aLPCUWMasterMainSchema.getChangePolFlag();
        this.ChangePolReason = aLPCUWMasterMainSchema.getChangePolReason();
        this.SpecReason = aLPCUWMasterMainSchema.getSpecReason();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            this.UWNo = rs.getInt("UWNo");
            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("InsuredName") == null)
            {
                this.InsuredName = null;
            }
            else
            {
                this.InsuredName = rs.getString("InsuredName").trim();
            }

            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AppntName") == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString("AppntName").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("PassFlag") == null)
            {
                this.PassFlag = null;
            }
            else
            {
                this.PassFlag = rs.getString("PassFlag").trim();
            }

            if (rs.getString("UWGrade") == null)
            {
                this.UWGrade = null;
            }
            else
            {
                this.UWGrade = rs.getString("UWGrade").trim();
            }

            if (rs.getString("AppGrade") == null)
            {
                this.AppGrade = null;
            }
            else
            {
                this.AppGrade = rs.getString("AppGrade").trim();
            }

            if (rs.getString("PostponeDay") == null)
            {
                this.PostponeDay = null;
            }
            else
            {
                this.PostponeDay = rs.getString("PostponeDay").trim();
            }

            this.PostponeDate = rs.getDate("PostponeDate");
            if (rs.getString("AutoUWFlag") == null)
            {
                this.AutoUWFlag = null;
            }
            else
            {
                this.AutoUWFlag = rs.getString("AutoUWFlag").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("UWIdea") == null)
            {
                this.UWIdea = null;
            }
            else
            {
                this.UWIdea = rs.getString("UWIdea").trim();
            }

            if (rs.getString("UpReportContent") == null)
            {
                this.UpReportContent = null;
            }
            else
            {
                this.UpReportContent = rs.getString("UpReportContent").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("HealthFlag") == null)
            {
                this.HealthFlag = null;
            }
            else
            {
                this.HealthFlag = rs.getString("HealthFlag").trim();
            }

            if (rs.getString("QuesFlag") == null)
            {
                this.QuesFlag = null;
            }
            else
            {
                this.QuesFlag = rs.getString("QuesFlag").trim();
            }

            if (rs.getString("SpecFlag") == null)
            {
                this.SpecFlag = null;
            }
            else
            {
                this.SpecFlag = rs.getString("SpecFlag").trim();
            }

            if (rs.getString("AddPremReason") == null)
            {
                this.AddPremReason = null;
            }
            else
            {
                this.AddPremReason = rs.getString("AddPremReason").trim();
            }

            if (rs.getString("ReportFlag") == null)
            {
                this.ReportFlag = null;
            }
            else
            {
                this.ReportFlag = rs.getString("ReportFlag").trim();
            }

            if (rs.getString("PrintFlag") == null)
            {
                this.PrintFlag = null;
            }
            else
            {
                this.PrintFlag = rs.getString("PrintFlag").trim();
            }

            if (rs.getString("ChangePolFlag") == null)
            {
                this.ChangePolFlag = null;
            }
            else
            {
                this.ChangePolFlag = rs.getString("ChangePolFlag").trim();
            }

            if (rs.getString("ChangePolReason") == null)
            {
                this.ChangePolReason = null;
            }
            else
            {
                this.ChangePolReason = rs.getString("ChangePolReason").trim();
            }

            if (rs.getString("SpecReason") == null)
            {
                this.SpecReason = null;
            }
            else
            {
                this.SpecReason = rs.getString("SpecReason").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPCUWMasterMainSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPCUWMasterMainSchema getSchema()
    {
        LPCUWMasterMainSchema aLPCUWMasterMainSchema = new
                LPCUWMasterMainSchema();
        aLPCUWMasterMainSchema.setSchema(this);
        return aLPCUWMasterMainSchema;
    }

    public LPCUWMasterMainDB getDB()
    {
        LPCUWMasterMainDB aDBOper = new LPCUWMasterMainDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPCUWMasterMain描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(UWNo) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PassFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PostponeDay)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            PostponeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AutoUWFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWIdea)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpReportContent)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HealthFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(QuesFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SpecFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AddPremReason)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReportFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrintFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChangePolFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChangePolReason)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SpecReason));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPCUWMasterMain>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            UWNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    2, SysConst.PACKAGESPILTER))).intValue();
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            PassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                     SysConst.PACKAGESPILTER);
            AppGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            PostponeDay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            PostponeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            AutoUWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                   SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                       SysConst.PACKAGESPILTER);
            UWIdea = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                    SysConst.PACKAGESPILTER);
            UpReportContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             22, SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 24, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                        SysConst.PACKAGESPILTER);
            HealthFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
            QuesFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                      SysConst.PACKAGESPILTER);
            SpecFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                      SysConst.PACKAGESPILTER);
            AddPremReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                           SysConst.PACKAGESPILTER);
            ReportFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                        SysConst.PACKAGESPILTER);
            PrintFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,
                                       SysConst.PACKAGESPILTER);
            ChangePolFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                           SysConst.PACKAGESPILTER);
            ChangePolReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             35, SysConst.PACKAGESPILTER);
            SpecReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPCUWMasterMainSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorNo));
        }
        if (FCode.equals("UWNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredNo));
        }
        if (FCode.equals("InsuredName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredName));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntName));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("PassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PassFlag));
        }
        if (FCode.equals("UWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWGrade));
        }
        if (FCode.equals("AppGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppGrade));
        }
        if (FCode.equals("PostponeDay"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PostponeDay));
        }
        if (FCode.equals("PostponeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getPostponeDate()));
        }
        if (FCode.equals("AutoUWFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AutoUWFlag));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(State));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("UWIdea"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWIdea));
        }
        if (FCode.equals("UpReportContent"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpReportContent));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("HealthFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HealthFlag));
        }
        if (FCode.equals("QuesFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(QuesFlag));
        }
        if (FCode.equals("SpecFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpecFlag));
        }
        if (FCode.equals("AddPremReason"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AddPremReason));
        }
        if (FCode.equals("ReportFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReportFlag));
        }
        if (FCode.equals("PrintFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrintFlag));
        }
        if (FCode.equals("ChangePolFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChangePolFlag));
        }
        if (FCode.equals("ChangePolReason"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChangePolReason));
        }
        if (FCode.equals("SpecReason"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpecReason));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = String.valueOf(UWNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(PassFlag);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(UWGrade);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AppGrade);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(PostponeDay);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPostponeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AutoUWFlag);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(UWIdea);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(UpReportContent);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(HealthFlag);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(QuesFlag);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(SpecFlag);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(AddPremReason);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(ReportFlag);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(PrintFlag);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ChangePolFlag);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ChangePolReason);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(SpecReason);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("UWNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                UWNo = i;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("InsuredName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
            {
                InsuredName = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("PassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PassFlag = FValue.trim();
            }
            else
            {
                PassFlag = null;
            }
        }
        if (FCode.equals("UWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
            {
                UWGrade = null;
            }
        }
        if (FCode.equals("AppGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppGrade = FValue.trim();
            }
            else
            {
                AppGrade = null;
            }
        }
        if (FCode.equals("PostponeDay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostponeDay = FValue.trim();
            }
            else
            {
                PostponeDay = null;
            }
        }
        if (FCode.equals("PostponeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostponeDate = fDate.getDate(FValue);
            }
            else
            {
                PostponeDate = null;
            }
        }
        if (FCode.equals("AutoUWFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AutoUWFlag = FValue.trim();
            }
            else
            {
                AutoUWFlag = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("UWIdea"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWIdea = FValue.trim();
            }
            else
            {
                UWIdea = null;
            }
        }
        if (FCode.equals("UpReportContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpReportContent = FValue.trim();
            }
            else
            {
                UpReportContent = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("HealthFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HealthFlag = FValue.trim();
            }
            else
            {
                HealthFlag = null;
            }
        }
        if (FCode.equals("QuesFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                QuesFlag = FValue.trim();
            }
            else
            {
                QuesFlag = null;
            }
        }
        if (FCode.equals("SpecFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpecFlag = FValue.trim();
            }
            else
            {
                SpecFlag = null;
            }
        }
        if (FCode.equals("AddPremReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AddPremReason = FValue.trim();
            }
            else
            {
                AddPremReason = null;
            }
        }
        if (FCode.equals("ReportFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReportFlag = FValue.trim();
            }
            else
            {
                ReportFlag = null;
            }
        }
        if (FCode.equals("PrintFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
            {
                PrintFlag = null;
            }
        }
        if (FCode.equals("ChangePolFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChangePolFlag = FValue.trim();
            }
            else
            {
                ChangePolFlag = null;
            }
        }
        if (FCode.equals("ChangePolReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChangePolReason = FValue.trim();
            }
            else
            {
                ChangePolReason = null;
            }
        }
        if (FCode.equals("SpecReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpecReason = FValue.trim();
            }
            else
            {
                SpecReason = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPCUWMasterMainSchema other = (LPCUWMasterMainSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && UWNo == other.getUWNo()
                && ContNo.equals(other.getContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && RiskVersion.equals(other.getRiskVersion())
                && RiskCode.equals(other.getRiskCode())
                && InsuredNo.equals(other.getInsuredNo())
                && InsuredName.equals(other.getInsuredName())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && PassFlag.equals(other.getPassFlag())
                && UWGrade.equals(other.getUWGrade())
                && AppGrade.equals(other.getAppGrade())
                && PostponeDay.equals(other.getPostponeDay())
                && fDate.getString(PostponeDate).equals(other.getPostponeDate())
                && AutoUWFlag.equals(other.getAutoUWFlag())
                && State.equals(other.getState())
                && ManageCom.equals(other.getManageCom())
                && UWIdea.equals(other.getUWIdea())
                && UpReportContent.equals(other.getUpReportContent())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && HealthFlag.equals(other.getHealthFlag())
                && QuesFlag.equals(other.getQuesFlag())
                && SpecFlag.equals(other.getSpecFlag())
                && AddPremReason.equals(other.getAddPremReason())
                && ReportFlag.equals(other.getReportFlag())
                && PrintFlag.equals(other.getPrintFlag())
                && ChangePolFlag.equals(other.getChangePolFlag())
                && ChangePolReason.equals(other.getChangePolReason())
                && SpecReason.equals(other.getSpecReason());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("UWNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 3;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 4;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 5;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 6;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return 7;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 8;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 9;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 10;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 11;
        }
        if (strFieldName.equals("PassFlag"))
        {
            return 12;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return 13;
        }
        if (strFieldName.equals("AppGrade"))
        {
            return 14;
        }
        if (strFieldName.equals("PostponeDay"))
        {
            return 15;
        }
        if (strFieldName.equals("PostponeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("AutoUWFlag"))
        {
            return 17;
        }
        if (strFieldName.equals("State"))
        {
            return 18;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 19;
        }
        if (strFieldName.equals("UWIdea"))
        {
            return 20;
        }
        if (strFieldName.equals("UpReportContent"))
        {
            return 21;
        }
        if (strFieldName.equals("Operator"))
        {
            return 22;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 23;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 24;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 25;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 26;
        }
        if (strFieldName.equals("HealthFlag"))
        {
            return 27;
        }
        if (strFieldName.equals("QuesFlag"))
        {
            return 28;
        }
        if (strFieldName.equals("SpecFlag"))
        {
            return 29;
        }
        if (strFieldName.equals("AddPremReason"))
        {
            return 30;
        }
        if (strFieldName.equals("ReportFlag"))
        {
            return 31;
        }
        if (strFieldName.equals("PrintFlag"))
        {
            return 32;
        }
        if (strFieldName.equals("ChangePolFlag"))
        {
            return 33;
        }
        if (strFieldName.equals("ChangePolReason"))
        {
            return 34;
        }
        if (strFieldName.equals("SpecReason"))
        {
            return 35;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "UWNo";
                break;
            case 2:
                strFieldName = "ContNo";
                break;
            case 3:
                strFieldName = "GrpPolNo";
                break;
            case 4:
                strFieldName = "RiskVersion";
                break;
            case 5:
                strFieldName = "RiskCode";
                break;
            case 6:
                strFieldName = "InsuredNo";
                break;
            case 7:
                strFieldName = "InsuredName";
                break;
            case 8:
                strFieldName = "AppntNo";
                break;
            case 9:
                strFieldName = "AppntName";
                break;
            case 10:
                strFieldName = "AgentCode";
                break;
            case 11:
                strFieldName = "AgentGroup";
                break;
            case 12:
                strFieldName = "PassFlag";
                break;
            case 13:
                strFieldName = "UWGrade";
                break;
            case 14:
                strFieldName = "AppGrade";
                break;
            case 15:
                strFieldName = "PostponeDay";
                break;
            case 16:
                strFieldName = "PostponeDate";
                break;
            case 17:
                strFieldName = "AutoUWFlag";
                break;
            case 18:
                strFieldName = "State";
                break;
            case 19:
                strFieldName = "ManageCom";
                break;
            case 20:
                strFieldName = "UWIdea";
                break;
            case 21:
                strFieldName = "UpReportContent";
                break;
            case 22:
                strFieldName = "Operator";
                break;
            case 23:
                strFieldName = "MakeDate";
                break;
            case 24:
                strFieldName = "MakeTime";
                break;
            case 25:
                strFieldName = "ModifyDate";
                break;
            case 26:
                strFieldName = "ModifyTime";
                break;
            case 27:
                strFieldName = "HealthFlag";
                break;
            case 28:
                strFieldName = "QuesFlag";
                break;
            case 29:
                strFieldName = "SpecFlag";
                break;
            case 30:
                strFieldName = "AddPremReason";
                break;
            case 31:
                strFieldName = "ReportFlag";
                break;
            case 32:
                strFieldName = "PrintFlag";
                break;
            case 33:
                strFieldName = "ChangePolFlag";
                break;
            case 34:
                strFieldName = "ChangePolReason";
                break;
            case 35:
                strFieldName = "SpecReason";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWNo"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostponeDay"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostponeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AutoUWFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWIdea"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UpReportContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HealthFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuesFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpecFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AddPremReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReportFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChangePolFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChangePolReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpecReason"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 34:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 35:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
