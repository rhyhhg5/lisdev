/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLMedicalManagementDB;

/*
 * <p>ClassName: LLMedicalManagementSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 医保通
 * @CreateDate：2010-03-23
 */
public class LLMedicalManagementSchema implements Schema, Cloneable
{
	// @Field
	/** 业务号 */
	private String SerialNo;
	/** 医院编码 */
	private String HospitalCode;
	/** 医院名称 */
	private String HospitalName;
	/** 管理机构 */
	private String ManageCom;
	/** 责任人 */
	private String Charger;
	/** 责任人职业 */
	private String Professional;
	/** 责任人职业编码 */
	private String ProfessionalCode;
	/** 问题描述 */
	private String ProblemDetail;
	/** 采取动作及结果 */
	private String Action;
	/** 操作人 */
	private String Operator;
	/** 业务发生日期 */
	private Date BussinessDate;
	/** 信息渠道编码 */
	private String MsgChannelCode;
	/** 信息渠道 */
	private String MsgChannel;
	/** 发生日期 */
	private Date MakeDate;
	/** 发生时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改日间 */
	private String ModifyTime;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLMedicalManagementSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLMedicalManagementSchema cloned = (LLMedicalManagementSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
		HospitalCode = aHospitalCode;
	}
	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
		HospitalName = aHospitalName;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getCharger()
	{
		return Charger;
	}
	public void setCharger(String aCharger)
	{
		Charger = aCharger;
	}
	public String getProfessional()
	{
		return Professional;
	}
	public void setProfessional(String aProfessional)
	{
		Professional = aProfessional;
	}
	public String getProfessionalCode()
	{
		return ProfessionalCode;
	}
	public void setProfessionalCode(String aProfessionalCode)
	{
		ProfessionalCode = aProfessionalCode;
	}
	public String getProblemDetail()
	{
		return ProblemDetail;
	}
	public void setProblemDetail(String aProblemDetail)
	{
		ProblemDetail = aProblemDetail;
	}
	public String getAction()
	{
		return Action;
	}
	public void setAction(String aAction)
	{
		Action = aAction;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getBussinessDate()
	{
		if( BussinessDate != null )
			return fDate.getString(BussinessDate);
		else
			return null;
	}
	public void setBussinessDate(Date aBussinessDate)
	{
		BussinessDate = aBussinessDate;
	}
	public void setBussinessDate(String aBussinessDate)
	{
		if (aBussinessDate != null && !aBussinessDate.equals("") )
		{
			BussinessDate = fDate.getDate( aBussinessDate );
		}
		else
			BussinessDate = null;
	}

	public String getMsgChannelCode()
	{
		return MsgChannelCode;
	}
	public void setMsgChannelCode(String aMsgChannelCode)
	{
		MsgChannelCode = aMsgChannelCode;
	}
	public String getMsgChannel()
	{
		return MsgChannel;
	}
	public void setMsgChannel(String aMsgChannel)
	{
		MsgChannel = aMsgChannel;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLMedicalManagementSchema 对象给 Schema 赋值
	* @param: aLLMedicalManagementSchema LLMedicalManagementSchema
	**/
	public void setSchema(LLMedicalManagementSchema aLLMedicalManagementSchema)
	{
		this.SerialNo = aLLMedicalManagementSchema.getSerialNo();
		this.HospitalCode = aLLMedicalManagementSchema.getHospitalCode();
		this.HospitalName = aLLMedicalManagementSchema.getHospitalName();
		this.ManageCom = aLLMedicalManagementSchema.getManageCom();
		this.Charger = aLLMedicalManagementSchema.getCharger();
		this.Professional = aLLMedicalManagementSchema.getProfessional();
		this.ProfessionalCode = aLLMedicalManagementSchema.getProfessionalCode();
		this.ProblemDetail = aLLMedicalManagementSchema.getProblemDetail();
		this.Action = aLLMedicalManagementSchema.getAction();
		this.Operator = aLLMedicalManagementSchema.getOperator();
		this.BussinessDate = fDate.getDate( aLLMedicalManagementSchema.getBussinessDate());
		this.MsgChannelCode = aLLMedicalManagementSchema.getMsgChannelCode();
		this.MsgChannel = aLLMedicalManagementSchema.getMsgChannel();
		this.MakeDate = fDate.getDate( aLLMedicalManagementSchema.getMakeDate());
		this.MakeTime = aLLMedicalManagementSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLMedicalManagementSchema.getModifyDate());
		this.ModifyTime = aLLMedicalManagementSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Charger") == null )
				this.Charger = null;
			else
				this.Charger = rs.getString("Charger").trim();

			if( rs.getString("Professional") == null )
				this.Professional = null;
			else
				this.Professional = rs.getString("Professional").trim();

			if( rs.getString("ProfessionalCode") == null )
				this.ProfessionalCode = null;
			else
				this.ProfessionalCode = rs.getString("ProfessionalCode").trim();

			if( rs.getString("ProblemDetail") == null )
				this.ProblemDetail = null;
			else
				this.ProblemDetail = rs.getString("ProblemDetail").trim();

			if( rs.getString("Action") == null )
				this.Action = null;
			else
				this.Action = rs.getString("Action").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.BussinessDate = rs.getDate("BussinessDate");
			if( rs.getString("MsgChannelCode") == null )
				this.MsgChannelCode = null;
			else
				this.MsgChannelCode = rs.getString("MsgChannelCode").trim();

			if( rs.getString("MsgChannel") == null )
				this.MsgChannel = null;
			else
				this.MsgChannel = rs.getString("MsgChannel").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLMedicalManagement表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLMedicalManagementSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLMedicalManagementSchema getSchema()
	{
		LLMedicalManagementSchema aLLMedicalManagementSchema = new LLMedicalManagementSchema();
		aLLMedicalManagementSchema.setSchema(this);
		return aLLMedicalManagementSchema;
	}

	public LLMedicalManagementDB getDB()
	{
		LLMedicalManagementDB aDBOper = new LLMedicalManagementDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLMedicalManagement描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Charger)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Professional)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProfessionalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProblemDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Action)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BussinessDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MsgChannelCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MsgChannel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLMedicalManagement>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Charger = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Professional = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ProfessionalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ProblemDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Action = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			BussinessDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MsgChannelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MsgChannel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLMedicalManagementSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Charger"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Charger));
		}
		if (FCode.equals("Professional"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Professional));
		}
		if (FCode.equals("ProfessionalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProfessionalCode));
		}
		if (FCode.equals("ProblemDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProblemDetail));
		}
		if (FCode.equals("Action"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Action));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("BussinessDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBussinessDate()));
		}
		if (FCode.equals("MsgChannelCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgChannelCode));
		}
		if (FCode.equals("MsgChannel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgChannel));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Charger);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Professional);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ProfessionalCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ProblemDetail);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Action);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBussinessDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MsgChannelCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MsgChannel);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Charger"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Charger = FValue.trim();
			}
			else
				Charger = null;
		}
		if (FCode.equalsIgnoreCase("Professional"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Professional = FValue.trim();
			}
			else
				Professional = null;
		}
		if (FCode.equalsIgnoreCase("ProfessionalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProfessionalCode = FValue.trim();
			}
			else
				ProfessionalCode = null;
		}
		if (FCode.equalsIgnoreCase("ProblemDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProblemDetail = FValue.trim();
			}
			else
				ProblemDetail = null;
		}
		if (FCode.equalsIgnoreCase("Action"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Action = FValue.trim();
			}
			else
				Action = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("BussinessDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BussinessDate = fDate.getDate( FValue );
			}
			else
				BussinessDate = null;
		}
		if (FCode.equalsIgnoreCase("MsgChannelCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgChannelCode = FValue.trim();
			}
			else
				MsgChannelCode = null;
		}
		if (FCode.equalsIgnoreCase("MsgChannel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgChannel = FValue.trim();
			}
			else
				MsgChannel = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLMedicalManagementSchema other = (LLMedicalManagementSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (HospitalCode == null ? other.getHospitalCode() == null : HospitalCode.equals(other.getHospitalCode()))
			&& (HospitalName == null ? other.getHospitalName() == null : HospitalName.equals(other.getHospitalName()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Charger == null ? other.getCharger() == null : Charger.equals(other.getCharger()))
			&& (Professional == null ? other.getProfessional() == null : Professional.equals(other.getProfessional()))
			&& (ProfessionalCode == null ? other.getProfessionalCode() == null : ProfessionalCode.equals(other.getProfessionalCode()))
			&& (ProblemDetail == null ? other.getProblemDetail() == null : ProblemDetail.equals(other.getProblemDetail()))
			&& (Action == null ? other.getAction() == null : Action.equals(other.getAction()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (BussinessDate == null ? other.getBussinessDate() == null : fDate.getString(BussinessDate).equals(other.getBussinessDate()))
			&& (MsgChannelCode == null ? other.getMsgChannelCode() == null : MsgChannelCode.equals(other.getMsgChannelCode()))
			&& (MsgChannel == null ? other.getMsgChannel() == null : MsgChannel.equals(other.getMsgChannel()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 1;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 2;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 3;
		}
		if( strFieldName.equals("Charger") ) {
			return 4;
		}
		if( strFieldName.equals("Professional") ) {
			return 5;
		}
		if( strFieldName.equals("ProfessionalCode") ) {
			return 6;
		}
		if( strFieldName.equals("ProblemDetail") ) {
			return 7;
		}
		if( strFieldName.equals("Action") ) {
			return 8;
		}
		if( strFieldName.equals("Operator") ) {
			return 9;
		}
		if( strFieldName.equals("BussinessDate") ) {
			return 10;
		}
		if( strFieldName.equals("MsgChannelCode") ) {
			return 11;
		}
		if( strFieldName.equals("MsgChannel") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "HospitalCode";
				break;
			case 2:
				strFieldName = "HospitalName";
				break;
			case 3:
				strFieldName = "ManageCom";
				break;
			case 4:
				strFieldName = "Charger";
				break;
			case 5:
				strFieldName = "Professional";
				break;
			case 6:
				strFieldName = "ProfessionalCode";
				break;
			case 7:
				strFieldName = "ProblemDetail";
				break;
			case 8:
				strFieldName = "Action";
				break;
			case 9:
				strFieldName = "Operator";
				break;
			case 10:
				strFieldName = "BussinessDate";
				break;
			case 11:
				strFieldName = "MsgChannelCode";
				break;
			case 12:
				strFieldName = "MsgChannel";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Charger") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Professional") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProfessionalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProblemDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Action") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussinessDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MsgChannelCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MsgChannel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
