/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKNoRealTimeUWCheckDB;

/*
 * <p>ClassName: LKNoRealTimeUWCheckSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 应收保费
 * @CreateDate：2012-06-27
 */
public class LKNoRealTimeUWCheckSchema implements Schema, Cloneable
{
	// @Field
	/** 投保单号码 */
	private String ProposalContno;
	/** 银行编码 */
	private String BankCode;
	/** 交易日期 */
	private Date TransDate;
	/** 地区代码 */
	private String ZoneNo;
	/** 网点代码 */
	private String BankNode;
	/** 银行柜员代码 */
	private String BankOperator;
	/** 交易流水号 */
	private String TransNo;
	/** 销售渠道 */
	private String BranchType;
	/** 产品代码 */
	private String ProductType;
	/** 险种代码 */
	private String RiskCode;
	/** 投保人姓名 */
	private String AppntName;
	/** 投保人证件类型 */
	private String AppntIdType;
	/** 投保人证件号码 */
	private String AppntIdNo;
	/** 银行账号 */
	private String BankAccNo;
	/** 是否签单标志 */
	private String PassFlag;
	/** 受理状态 */
	private String TbDealStatus;
	/** 描述信息 */
	private String Descr;
	/** 备用字段1 */
	private String Temp1;
	/** 备用字段2 */
	private String Temp2;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 24;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKNoRealTimeUWCheckSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ProposalContno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LKNoRealTimeUWCheckSchema cloned = (LKNoRealTimeUWCheckSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getProposalContno()
	{
		return ProposalContno;
	}
	public void setProposalContno(String aProposalContno)
	{
		ProposalContno = aProposalContno;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getTransDate()
	{
		if( TransDate != null )
			return fDate.getString(TransDate);
		else
			return null;
	}
	public void setTransDate(Date aTransDate)
	{
		TransDate = aTransDate;
	}
	public void setTransDate(String aTransDate)
	{
		if (aTransDate != null && !aTransDate.equals("") )
		{
			TransDate = fDate.getDate( aTransDate );
		}
		else
			TransDate = null;
	}

	public String getZoneNo()
	{
		return ZoneNo;
	}
	public void setZoneNo(String aZoneNo)
	{
		ZoneNo = aZoneNo;
	}
	public String getBankNode()
	{
		return BankNode;
	}
	public void setBankNode(String aBankNode)
	{
		BankNode = aBankNode;
	}
	public String getBankOperator()
	{
		return BankOperator;
	}
	public void setBankOperator(String aBankOperator)
	{
		BankOperator = aBankOperator;
	}
	public String getTransNo()
	{
		return TransNo;
	}
	public void setTransNo(String aTransNo)
	{
		TransNo = aTransNo;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getProductType()
	{
		return ProductType;
	}
	public void setProductType(String aProductType)
	{
		ProductType = aProductType;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getAppntName()
	{
		return AppntName;
	}
	public void setAppntName(String aAppntName)
	{
		AppntName = aAppntName;
	}
	public String getAppntIdType()
	{
		return AppntIdType;
	}
	public void setAppntIdType(String aAppntIdType)
	{
		AppntIdType = aAppntIdType;
	}
	public String getAppntIdNo()
	{
		return AppntIdNo;
	}
	public void setAppntIdNo(String aAppntIdNo)
	{
		AppntIdNo = aAppntIdNo;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getPassFlag()
	{
		return PassFlag;
	}
	public void setPassFlag(String aPassFlag)
	{
		PassFlag = aPassFlag;
	}
	public String getTbDealStatus()
	{
		return TbDealStatus;
	}
	public void setTbDealStatus(String aTbDealStatus)
	{
		TbDealStatus = aTbDealStatus;
	}
	public String getDescr()
	{
		return Descr;
	}
	public void setDescr(String aDescr)
	{
		Descr = aDescr;
	}
	public String getTemp1()
	{
		return Temp1;
	}
	public void setTemp1(String aTemp1)
	{
		Temp1 = aTemp1;
	}
	public String getTemp2()
	{
		return Temp2;
	}
	public void setTemp2(String aTemp2)
	{
		Temp2 = aTemp2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LKNoRealTimeUWCheckSchema 对象给 Schema 赋值
	* @param: aLKNoRealTimeUWCheckSchema LKNoRealTimeUWCheckSchema
	**/
	public void setSchema(LKNoRealTimeUWCheckSchema aLKNoRealTimeUWCheckSchema)
	{
		this.ProposalContno = aLKNoRealTimeUWCheckSchema.getProposalContno();
		this.BankCode = aLKNoRealTimeUWCheckSchema.getBankCode();
		this.TransDate = fDate.getDate( aLKNoRealTimeUWCheckSchema.getTransDate());
		this.ZoneNo = aLKNoRealTimeUWCheckSchema.getZoneNo();
		this.BankNode = aLKNoRealTimeUWCheckSchema.getBankNode();
		this.BankOperator = aLKNoRealTimeUWCheckSchema.getBankOperator();
		this.TransNo = aLKNoRealTimeUWCheckSchema.getTransNo();
		this.BranchType = aLKNoRealTimeUWCheckSchema.getBranchType();
		this.ProductType = aLKNoRealTimeUWCheckSchema.getProductType();
		this.RiskCode = aLKNoRealTimeUWCheckSchema.getRiskCode();
		this.AppntName = aLKNoRealTimeUWCheckSchema.getAppntName();
		this.AppntIdType = aLKNoRealTimeUWCheckSchema.getAppntIdType();
		this.AppntIdNo = aLKNoRealTimeUWCheckSchema.getAppntIdNo();
		this.BankAccNo = aLKNoRealTimeUWCheckSchema.getBankAccNo();
		this.PassFlag = aLKNoRealTimeUWCheckSchema.getPassFlag();
		this.TbDealStatus = aLKNoRealTimeUWCheckSchema.getTbDealStatus();
		this.Descr = aLKNoRealTimeUWCheckSchema.getDescr();
		this.Temp1 = aLKNoRealTimeUWCheckSchema.getTemp1();
		this.Temp2 = aLKNoRealTimeUWCheckSchema.getTemp2();
		this.Operator = aLKNoRealTimeUWCheckSchema.getOperator();
		this.MakeDate = fDate.getDate( aLKNoRealTimeUWCheckSchema.getMakeDate());
		this.MakeTime = aLKNoRealTimeUWCheckSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLKNoRealTimeUWCheckSchema.getModifyDate());
		this.ModifyTime = aLKNoRealTimeUWCheckSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ProposalContno") == null )
				this.ProposalContno = null;
			else
				this.ProposalContno = rs.getString("ProposalContno").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			this.TransDate = rs.getDate("TransDate");
			if( rs.getString("ZoneNo") == null )
				this.ZoneNo = null;
			else
				this.ZoneNo = rs.getString("ZoneNo").trim();

			if( rs.getString("BankNode") == null )
				this.BankNode = null;
			else
				this.BankNode = rs.getString("BankNode").trim();

			if( rs.getString("BankOperator") == null )
				this.BankOperator = null;
			else
				this.BankOperator = rs.getString("BankOperator").trim();

			if( rs.getString("TransNo") == null )
				this.TransNo = null;
			else
				this.TransNo = rs.getString("TransNo").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("ProductType") == null )
				this.ProductType = null;
			else
				this.ProductType = rs.getString("ProductType").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("AppntName") == null )
				this.AppntName = null;
			else
				this.AppntName = rs.getString("AppntName").trim();

			if( rs.getString("AppntIdType") == null )
				this.AppntIdType = null;
			else
				this.AppntIdType = rs.getString("AppntIdType").trim();

			if( rs.getString("AppntIdNo") == null )
				this.AppntIdNo = null;
			else
				this.AppntIdNo = rs.getString("AppntIdNo").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("PassFlag") == null )
				this.PassFlag = null;
			else
				this.PassFlag = rs.getString("PassFlag").trim();

			if( rs.getString("TbDealStatus") == null )
				this.TbDealStatus = null;
			else
				this.TbDealStatus = rs.getString("TbDealStatus").trim();

			if( rs.getString("Descr") == null )
				this.Descr = null;
			else
				this.Descr = rs.getString("Descr").trim();

			if( rs.getString("Temp1") == null )
				this.Temp1 = null;
			else
				this.Temp1 = rs.getString("Temp1").trim();

			if( rs.getString("Temp2") == null )
				this.Temp2 = null;
			else
				this.Temp2 = rs.getString("Temp2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKNoRealTimeUWCheck表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKNoRealTimeUWCheckSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKNoRealTimeUWCheckSchema getSchema()
	{
		LKNoRealTimeUWCheckSchema aLKNoRealTimeUWCheckSchema = new LKNoRealTimeUWCheckSchema();
		aLKNoRealTimeUWCheckSchema.setSchema(this);
		return aLKNoRealTimeUWCheckSchema;
	}

	public LKNoRealTimeUWCheckDB getDB()
	{
		LKNoRealTimeUWCheckDB aDBOper = new LKNoRealTimeUWCheckDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKNoRealTimeUWCheck描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ProposalContno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TransDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZoneNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProductType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIdType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIdNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PassFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TbDealStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Descr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKNoRealTimeUWCheck>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ProposalContno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			TransDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			ZoneNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BankOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ProductType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AppntIdType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			AppntIdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			PassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			TbDealStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Descr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Temp1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Temp2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKNoRealTimeUWCheckSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ProposalContno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContno));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("TransDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
		}
		if (FCode.equals("ZoneNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZoneNo));
		}
		if (FCode.equals("BankNode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
		}
		if (FCode.equals("BankOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankOperator));
		}
		if (FCode.equals("TransNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("ProductType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProductType));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("AppntName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
		}
		if (FCode.equals("AppntIdType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIdType));
		}
		if (FCode.equals("AppntIdNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIdNo));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("PassFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PassFlag));
		}
		if (FCode.equals("TbDealStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TbDealStatus));
		}
		if (FCode.equals("Descr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Descr));
		}
		if (FCode.equals("Temp1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp1));
		}
		if (FCode.equals("Temp2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ProposalContno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ZoneNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BankNode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BankOperator);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(TransNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ProductType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AppntName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AppntIdType);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(AppntIdNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(PassFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(TbDealStatus);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Descr);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Temp1);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Temp2);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ProposalContno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalContno = FValue.trim();
			}
			else
				ProposalContno = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("TransDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TransDate = fDate.getDate( FValue );
			}
			else
				TransDate = null;
		}
		if (FCode.equalsIgnoreCase("ZoneNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZoneNo = FValue.trim();
			}
			else
				ZoneNo = null;
		}
		if (FCode.equalsIgnoreCase("BankNode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankNode = FValue.trim();
			}
			else
				BankNode = null;
		}
		if (FCode.equalsIgnoreCase("BankOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankOperator = FValue.trim();
			}
			else
				BankOperator = null;
		}
		if (FCode.equalsIgnoreCase("TransNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransNo = FValue.trim();
			}
			else
				TransNo = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("ProductType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProductType = FValue.trim();
			}
			else
				ProductType = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("AppntName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntName = FValue.trim();
			}
			else
				AppntName = null;
		}
		if (FCode.equalsIgnoreCase("AppntIdType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIdType = FValue.trim();
			}
			else
				AppntIdType = null;
		}
		if (FCode.equalsIgnoreCase("AppntIdNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIdNo = FValue.trim();
			}
			else
				AppntIdNo = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("PassFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PassFlag = FValue.trim();
			}
			else
				PassFlag = null;
		}
		if (FCode.equalsIgnoreCase("TbDealStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TbDealStatus = FValue.trim();
			}
			else
				TbDealStatus = null;
		}
		if (FCode.equalsIgnoreCase("Descr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Descr = FValue.trim();
			}
			else
				Descr = null;
		}
		if (FCode.equalsIgnoreCase("Temp1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp1 = FValue.trim();
			}
			else
				Temp1 = null;
		}
		if (FCode.equalsIgnoreCase("Temp2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp2 = FValue.trim();
			}
			else
				Temp2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKNoRealTimeUWCheckSchema other = (LKNoRealTimeUWCheckSchema)otherObject;
		return
			(ProposalContno == null ? other.getProposalContno() == null : ProposalContno.equals(other.getProposalContno()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (TransDate == null ? other.getTransDate() == null : fDate.getString(TransDate).equals(other.getTransDate()))
			&& (ZoneNo == null ? other.getZoneNo() == null : ZoneNo.equals(other.getZoneNo()))
			&& (BankNode == null ? other.getBankNode() == null : BankNode.equals(other.getBankNode()))
			&& (BankOperator == null ? other.getBankOperator() == null : BankOperator.equals(other.getBankOperator()))
			&& (TransNo == null ? other.getTransNo() == null : TransNo.equals(other.getTransNo()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (ProductType == null ? other.getProductType() == null : ProductType.equals(other.getProductType()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (AppntName == null ? other.getAppntName() == null : AppntName.equals(other.getAppntName()))
			&& (AppntIdType == null ? other.getAppntIdType() == null : AppntIdType.equals(other.getAppntIdType()))
			&& (AppntIdNo == null ? other.getAppntIdNo() == null : AppntIdNo.equals(other.getAppntIdNo()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (PassFlag == null ? other.getPassFlag() == null : PassFlag.equals(other.getPassFlag()))
			&& (TbDealStatus == null ? other.getTbDealStatus() == null : TbDealStatus.equals(other.getTbDealStatus()))
			&& (Descr == null ? other.getDescr() == null : Descr.equals(other.getDescr()))
			&& (Temp1 == null ? other.getTemp1() == null : Temp1.equals(other.getTemp1()))
			&& (Temp2 == null ? other.getTemp2() == null : Temp2.equals(other.getTemp2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ProposalContno") ) {
			return 0;
		}
		if( strFieldName.equals("BankCode") ) {
			return 1;
		}
		if( strFieldName.equals("TransDate") ) {
			return 2;
		}
		if( strFieldName.equals("ZoneNo") ) {
			return 3;
		}
		if( strFieldName.equals("BankNode") ) {
			return 4;
		}
		if( strFieldName.equals("BankOperator") ) {
			return 5;
		}
		if( strFieldName.equals("TransNo") ) {
			return 6;
		}
		if( strFieldName.equals("BranchType") ) {
			return 7;
		}
		if( strFieldName.equals("ProductType") ) {
			return 8;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 9;
		}
		if( strFieldName.equals("AppntName") ) {
			return 10;
		}
		if( strFieldName.equals("AppntIdType") ) {
			return 11;
		}
		if( strFieldName.equals("AppntIdNo") ) {
			return 12;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 13;
		}
		if( strFieldName.equals("PassFlag") ) {
			return 14;
		}
		if( strFieldName.equals("TbDealStatus") ) {
			return 15;
		}
		if( strFieldName.equals("Descr") ) {
			return 16;
		}
		if( strFieldName.equals("Temp1") ) {
			return 17;
		}
		if( strFieldName.equals("Temp2") ) {
			return 18;
		}
		if( strFieldName.equals("Operator") ) {
			return 19;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 20;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 23;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ProposalContno";
				break;
			case 1:
				strFieldName = "BankCode";
				break;
			case 2:
				strFieldName = "TransDate";
				break;
			case 3:
				strFieldName = "ZoneNo";
				break;
			case 4:
				strFieldName = "BankNode";
				break;
			case 5:
				strFieldName = "BankOperator";
				break;
			case 6:
				strFieldName = "TransNo";
				break;
			case 7:
				strFieldName = "BranchType";
				break;
			case 8:
				strFieldName = "ProductType";
				break;
			case 9:
				strFieldName = "RiskCode";
				break;
			case 10:
				strFieldName = "AppntName";
				break;
			case 11:
				strFieldName = "AppntIdType";
				break;
			case 12:
				strFieldName = "AppntIdNo";
				break;
			case 13:
				strFieldName = "BankAccNo";
				break;
			case 14:
				strFieldName = "PassFlag";
				break;
			case 15:
				strFieldName = "TbDealStatus";
				break;
			case 16:
				strFieldName = "Descr";
				break;
			case 17:
				strFieldName = "Temp1";
				break;
			case 18:
				strFieldName = "Temp2";
				break;
			case 19:
				strFieldName = "Operator";
				break;
			case 20:
				strFieldName = "MakeDate";
				break;
			case 21:
				strFieldName = "MakeTime";
				break;
			case 22:
				strFieldName = "ModifyDate";
				break;
			case 23:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ProposalContno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ZoneNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankNode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProductType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntIdType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntIdNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PassFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TbDealStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Descr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
