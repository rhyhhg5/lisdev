/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOBatchPrtLogDB;

/*
 * <p>ClassName: LOBatchPrtLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2006-01-19
 */
public class LOBatchPrtLogSchema implements Schema, Cloneable {
    // @Field
    /** 打印批次号 */
    private String Batchtimescode;
    /** 业务部门 */
    private String Department;
    /** 单证类型 */
    private String Billtype;
    /** 份数 */
    private int Copy;
    /** 预期打印日期 */
    private Date PreSendDate;
    /** 预期打印时间 */
    private String PreSendTime;
    /** 定时打印标志 */
    private String SetFlag;
    /** 是否已打印 */
    private String PrtFlag;
    /** 打印日期 */
    private Date PrtDate;
    /** 打印时间 */
    private String PrtTime;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBatchPrtLogSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "Batchtimescode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LOBatchPrtLogSchema cloned = (LOBatchPrtLogSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getBatchtimescode() {
        return Batchtimescode;
    }

    public void setBatchtimescode(String aBatchtimescode) {
        Batchtimescode = aBatchtimescode;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String aDepartment) {
        Department = aDepartment;
    }

    public String getBilltype() {
        return Billtype;
    }

    public void setBilltype(String aBilltype) {
        Billtype = aBilltype;
    }

    public int getCopy() {
        return Copy;
    }

    public void setCopy(int aCopy) {
        Copy = aCopy;
    }

    public void setCopy(String aCopy) {
        if (aCopy != null && !aCopy.equals("")) {
            Integer tInteger = new Integer(aCopy);
            int i = tInteger.intValue();
            Copy = i;
        }
    }

    public String getPreSendDate() {
        if (PreSendDate != null) {
            return fDate.getString(PreSendDate);
        } else {
            return null;
        }
    }

    public void setPreSendDate(Date aPreSendDate) {
        PreSendDate = aPreSendDate;
    }

    public void setPreSendDate(String aPreSendDate) {
        if (aPreSendDate != null && !aPreSendDate.equals("")) {
            PreSendDate = fDate.getDate(aPreSendDate);
        } else {
            PreSendDate = null;
        }
    }

    public String getPreSendTime() {
        return PreSendTime;
    }

    public void setPreSendTime(String aPreSendTime) {
        PreSendTime = aPreSendTime;
    }

    public String getSetFlag() {
        return SetFlag;
    }

    public void setSetFlag(String aSetFlag) {
        SetFlag = aSetFlag;
    }

    public String getPrtFlag() {
        return PrtFlag;
    }

    public void setPrtFlag(String aPrtFlag) {
        PrtFlag = aPrtFlag;
    }

    public String getPrtDate() {
        if (PrtDate != null) {
            return fDate.getString(PrtDate);
        } else {
            return null;
        }
    }

    public void setPrtDate(Date aPrtDate) {
        PrtDate = aPrtDate;
    }

    public void setPrtDate(String aPrtDate) {
        if (aPrtDate != null && !aPrtDate.equals("")) {
            PrtDate = fDate.getDate(aPrtDate);
        } else {
            PrtDate = null;
        }
    }

    public String getPrtTime() {
        return PrtTime;
    }

    public void setPrtTime(String aPrtTime) {
        PrtTime = aPrtTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOBatchPrtLogSchema 对象给 Schema 赋值
     * @param: aLOBatchPrtLogSchema LOBatchPrtLogSchema
     **/
    public void setSchema(LOBatchPrtLogSchema aLOBatchPrtLogSchema) {
        this.Batchtimescode = aLOBatchPrtLogSchema.getBatchtimescode();
        this.Department = aLOBatchPrtLogSchema.getDepartment();
        this.Billtype = aLOBatchPrtLogSchema.getBilltype();
        this.Copy = aLOBatchPrtLogSchema.getCopy();
        this.PreSendDate = fDate.getDate(aLOBatchPrtLogSchema.getPreSendDate());
        this.PreSendTime = aLOBatchPrtLogSchema.getPreSendTime();
        this.SetFlag = aLOBatchPrtLogSchema.getSetFlag();
        this.PrtFlag = aLOBatchPrtLogSchema.getPrtFlag();
        this.PrtDate = fDate.getDate(aLOBatchPrtLogSchema.getPrtDate());
        this.PrtTime = aLOBatchPrtLogSchema.getPrtTime();
        this.ManageCom = aLOBatchPrtLogSchema.getManageCom();
        this.Operator = aLOBatchPrtLogSchema.getOperator();
        this.MakeDate = fDate.getDate(aLOBatchPrtLogSchema.getMakeDate());
        this.MakeTime = aLOBatchPrtLogSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBatchPrtLogSchema.getModifyDate());
        this.ModifyTime = aLOBatchPrtLogSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("Batchtimescode") == null) {
                this.Batchtimescode = null;
            } else {
                this.Batchtimescode = rs.getString("Batchtimescode").trim();
            }

            if (rs.getString("Department") == null) {
                this.Department = null;
            } else {
                this.Department = rs.getString("Department").trim();
            }

            if (rs.getString("Billtype") == null) {
                this.Billtype = null;
            } else {
                this.Billtype = rs.getString("Billtype").trim();
            }

            this.Copy = rs.getInt("Copy");
            this.PreSendDate = rs.getDate("PreSendDate");
            if (rs.getString("PreSendTime") == null) {
                this.PreSendTime = null;
            } else {
                this.PreSendTime = rs.getString("PreSendTime").trim();
            }

            if (rs.getString("SetFlag") == null) {
                this.SetFlag = null;
            } else {
                this.SetFlag = rs.getString("SetFlag").trim();
            }

            if (rs.getString("PrtFlag") == null) {
                this.PrtFlag = null;
            } else {
                this.PrtFlag = rs.getString("PrtFlag").trim();
            }

            this.PrtDate = rs.getDate("PrtDate");
            if (rs.getString("PrtTime") == null) {
                this.PrtTime = null;
            } else {
                this.PrtTime = rs.getString("PrtTime").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LOBatchPrtLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBatchPrtLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LOBatchPrtLogSchema getSchema() {
        LOBatchPrtLogSchema aLOBatchPrtLogSchema = new LOBatchPrtLogSchema();
        aLOBatchPrtLogSchema.setSchema(this);
        return aLOBatchPrtLogSchema;
    }

    public LOBatchPrtLogDB getDB() {
        LOBatchPrtLogDB aDBOper = new LOBatchPrtLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBatchPrtLog描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(Batchtimescode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Department));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Billtype));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Copy));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PreSendDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PreSendTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SetFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PrtDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBatchPrtLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            Batchtimescode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                            SysConst.PACKAGESPILTER);
            Department = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            Billtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            Copy = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).intValue();
            PreSendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            PreSendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            SetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            PrtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            PrtDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            PrtTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBatchPrtLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("Batchtimescode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Batchtimescode));
        }
        if (FCode.equals("Department")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Department));
        }
        if (FCode.equals("Billtype")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Billtype));
        }
        if (FCode.equals("Copy")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Copy));
        }
        if (FCode.equals("PreSendDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPreSendDate()));
        }
        if (FCode.equals("PreSendTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PreSendTime));
        }
        if (FCode.equals("SetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SetFlag));
        }
        if (FCode.equals("PrtFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtFlag));
        }
        if (FCode.equals("PrtDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPrtDate()));
        }
        if (FCode.equals("PrtTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtTime));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(Batchtimescode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(Department);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(Billtype);
            break;
        case 3:
            strFieldValue = String.valueOf(Copy);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getPreSendDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(PreSendTime);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(SetFlag);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(PrtFlag);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getPrtDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(PrtTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("Batchtimescode")) {
            if (FValue != null && !FValue.equals("")) {
                Batchtimescode = FValue.trim();
            } else {
                Batchtimescode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Department")) {
            if (FValue != null && !FValue.equals("")) {
                Department = FValue.trim();
            } else {
                Department = null;
            }
        }
        if (FCode.equalsIgnoreCase("Billtype")) {
            if (FValue != null && !FValue.equals("")) {
                Billtype = FValue.trim();
            } else {
                Billtype = null;
            }
        }
        if (FCode.equalsIgnoreCase("Copy")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Copy = i;
            }
        }
        if (FCode.equalsIgnoreCase("PreSendDate")) {
            if (FValue != null && !FValue.equals("")) {
                PreSendDate = fDate.getDate(FValue);
            } else {
                PreSendDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PreSendTime")) {
            if (FValue != null && !FValue.equals("")) {
                PreSendTime = FValue.trim();
            } else {
                PreSendTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("SetFlag")) {
            if (FValue != null && !FValue.equals("")) {
                SetFlag = FValue.trim();
            } else {
                SetFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtFlag")) {
            if (FValue != null && !FValue.equals("")) {
                PrtFlag = FValue.trim();
            } else {
                PrtFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtDate")) {
            if (FValue != null && !FValue.equals("")) {
                PrtDate = fDate.getDate(FValue);
            } else {
                PrtDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtTime")) {
            if (FValue != null && !FValue.equals("")) {
                PrtTime = FValue.trim();
            } else {
                PrtTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LOBatchPrtLogSchema other = (LOBatchPrtLogSchema) otherObject;
        return
                Batchtimescode.equals(other.getBatchtimescode())
                && Department.equals(other.getDepartment())
                && Billtype.equals(other.getBilltype())
                && Copy == other.getCopy()
                && fDate.getString(PreSendDate).equals(other.getPreSendDate())
                && PreSendTime.equals(other.getPreSendTime())
                && SetFlag.equals(other.getSetFlag())
                && PrtFlag.equals(other.getPrtFlag())
                && fDate.getString(PrtDate).equals(other.getPrtDate())
                && PrtTime.equals(other.getPrtTime())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("Batchtimescode")) {
            return 0;
        }
        if (strFieldName.equals("Department")) {
            return 1;
        }
        if (strFieldName.equals("Billtype")) {
            return 2;
        }
        if (strFieldName.equals("Copy")) {
            return 3;
        }
        if (strFieldName.equals("PreSendDate")) {
            return 4;
        }
        if (strFieldName.equals("PreSendTime")) {
            return 5;
        }
        if (strFieldName.equals("SetFlag")) {
            return 6;
        }
        if (strFieldName.equals("PrtFlag")) {
            return 7;
        }
        if (strFieldName.equals("PrtDate")) {
            return 8;
        }
        if (strFieldName.equals("PrtTime")) {
            return 9;
        }
        if (strFieldName.equals("ManageCom")) {
            return 10;
        }
        if (strFieldName.equals("Operator")) {
            return 11;
        }
        if (strFieldName.equals("MakeDate")) {
            return 12;
        }
        if (strFieldName.equals("MakeTime")) {
            return 13;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 14;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "Batchtimescode";
            break;
        case 1:
            strFieldName = "Department";
            break;
        case 2:
            strFieldName = "Billtype";
            break;
        case 3:
            strFieldName = "Copy";
            break;
        case 4:
            strFieldName = "PreSendDate";
            break;
        case 5:
            strFieldName = "PreSendTime";
            break;
        case 6:
            strFieldName = "SetFlag";
            break;
        case 7:
            strFieldName = "PrtFlag";
            break;
        case 8:
            strFieldName = "PrtDate";
            break;
        case 9:
            strFieldName = "PrtTime";
            break;
        case 10:
            strFieldName = "ManageCom";
            break;
        case 11:
            strFieldName = "Operator";
            break;
        case 12:
            strFieldName = "MakeDate";
            break;
        case 13:
            strFieldName = "MakeTime";
            break;
        case 14:
            strFieldName = "ModifyDate";
            break;
        case 15:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("Batchtimescode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Department")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Billtype")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Copy")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PreSendDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PreSendTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SetFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PrtTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_INT;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
