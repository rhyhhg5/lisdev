/*
 * <p>ClassName: LMPayModeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMPayModeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMPayModeSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 终交年龄年期标志 */
    private String PayEndYearFlag;
    /** 终交年龄年期 */
    private int PayEndYear;
    /** 交费间隔 */
    private int PayIntv;
    /** 允许交至满期标志 */
    private String PayToExpiryFlag;
    /** 显示信息 */
    private String ShowInfo;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMPayModeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";
        pk[2] = "PayEndYearFlag";
        pk[3] = "PayEndYear";
        pk[4] = "PayIntv";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getPayEndYearFlag()
    {
        if (PayEndYearFlag != null && !PayEndYearFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayEndYearFlag = StrTool.unicodeToGBK(PayEndYearFlag);
        }
        return PayEndYearFlag;
    }

    public void setPayEndYearFlag(String aPayEndYearFlag)
    {
        PayEndYearFlag = aPayEndYearFlag;
    }

    public int getPayEndYear()
    {
        return PayEndYear;
    }

    public void setPayEndYear(int aPayEndYear)
    {
        PayEndYear = aPayEndYear;
    }

    public void setPayEndYear(String aPayEndYear)
    {
        if (aPayEndYear != null && !aPayEndYear.equals(""))
        {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public int getPayIntv()
    {
        return PayIntv;
    }

    public void setPayIntv(int aPayIntv)
    {
        PayIntv = aPayIntv;
    }

    public void setPayIntv(String aPayIntv)
    {
        if (aPayIntv != null && !aPayIntv.equals(""))
        {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayToExpiryFlag()
    {
        if (PayToExpiryFlag != null && !PayToExpiryFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayToExpiryFlag = StrTool.unicodeToGBK(PayToExpiryFlag);
        }
        return PayToExpiryFlag;
    }

    public void setPayToExpiryFlag(String aPayToExpiryFlag)
    {
        PayToExpiryFlag = aPayToExpiryFlag;
    }

    public String getShowInfo()
    {
        if (ShowInfo != null && !ShowInfo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ShowInfo = StrTool.unicodeToGBK(ShowInfo);
        }
        return ShowInfo;
    }

    public void setShowInfo(String aShowInfo)
    {
        ShowInfo = aShowInfo;
    }

    /**
     * 使用另外一个 LMPayModeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMPayModeSchema aLMPayModeSchema)
    {
        this.RiskCode = aLMPayModeSchema.getRiskCode();
        this.RiskVer = aLMPayModeSchema.getRiskVer();
        this.PayEndYearFlag = aLMPayModeSchema.getPayEndYearFlag();
        this.PayEndYear = aLMPayModeSchema.getPayEndYear();
        this.PayIntv = aLMPayModeSchema.getPayIntv();
        this.PayToExpiryFlag = aLMPayModeSchema.getPayToExpiryFlag();
        this.ShowInfo = aLMPayModeSchema.getShowInfo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("PayEndYearFlag") == null)
            {
                this.PayEndYearFlag = null;
            }
            else
            {
                this.PayEndYearFlag = rs.getString("PayEndYearFlag").trim();
            }

            this.PayEndYear = rs.getInt("PayEndYear");
            this.PayIntv = rs.getInt("PayIntv");
            if (rs.getString("PayToExpiryFlag") == null)
            {
                this.PayToExpiryFlag = null;
            }
            else
            {
                this.PayToExpiryFlag = rs.getString("PayToExpiryFlag").trim();
            }

            if (rs.getString("ShowInfo") == null)
            {
                this.ShowInfo = null;
            }
            else
            {
                this.ShowInfo = rs.getString("ShowInfo").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMPayModeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMPayModeSchema getSchema()
    {
        LMPayModeSchema aLMPayModeSchema = new LMPayModeSchema();
        aLMPayModeSchema.setSchema(this);
        return aLMPayModeSchema;
    }

    public LMPayModeDB getDB()
    {
        LMPayModeDB aDBOper = new LMPayModeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMPayMode描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayEndYearFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayEndYear) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayIntv) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayToExpiryFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ShowInfo));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMPayMode>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            PayEndYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
            PayEndYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            PayToExpiryFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             6, SysConst.PACKAGESPILTER);
            ShowInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMPayModeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("PayEndYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayEndYearFlag));
        }
        if (FCode.equals("PayEndYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayEndYear));
        }
        if (FCode.equals("PayIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayIntv));
        }
        if (FCode.equals("PayToExpiryFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayToExpiryFlag));
        }
        if (FCode.equals("ShowInfo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ShowInfo));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PayEndYearFlag);
                break;
            case 3:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 4:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PayToExpiryFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ShowInfo);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("PayEndYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
            {
                PayEndYearFlag = null;
            }
        }
        if (FCode.equals("PayEndYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equals("PayIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equals("PayToExpiryFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayToExpiryFlag = FValue.trim();
            }
            else
            {
                PayToExpiryFlag = null;
            }
        }
        if (FCode.equals("ShowInfo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ShowInfo = FValue.trim();
            }
            else
            {
                ShowInfo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMPayModeSchema other = (LMPayModeSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && PayEndYearFlag.equals(other.getPayEndYearFlag())
                && PayEndYear == other.getPayEndYear()
                && PayIntv == other.getPayIntv()
                && PayToExpiryFlag.equals(other.getPayToExpiryFlag())
                && ShowInfo.equals(other.getShowInfo());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("PayEndYearFlag"))
        {
            return 2;
        }
        if (strFieldName.equals("PayEndYear"))
        {
            return 3;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return 4;
        }
        if (strFieldName.equals("PayToExpiryFlag"))
        {
            return 5;
        }
        if (strFieldName.equals("ShowInfo"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "PayEndYearFlag";
                break;
            case 3:
                strFieldName = "PayEndYear";
                break;
            case 4:
                strFieldName = "PayIntv";
                break;
            case 5:
                strFieldName = "PayToExpiryFlag";
                break;
            case 6:
                strFieldName = "ShowInfo";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayEndYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayEndYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayToExpiryFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ShowInfo"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
