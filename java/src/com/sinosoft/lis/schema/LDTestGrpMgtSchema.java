/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDTestGrpMgtDB;

/*
 * <p>ClassName: LDTestGrpMgtSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康管理表修改_2005-12-06
 * @CreateDate：2005-12-06
 */
public class LDTestGrpMgtSchema implements Schema, Cloneable {
    // @Field
    /** 体检套餐代码 */
    private String TestGrpCode;
    /** 体检套餐名称 */
    private String TestGrpName;
    /** 体检套餐类型 */
    private String TestGrpType;
    /** 检查项目代码 */
    private String TestItemCode;
    /** 检查项目俗名 */
    private String TestItemCommonName;
    /** 其他标志 */
    private String OtherSign;
    /** 说明 */
    private String Explain;
    /** 操作员 */
    private String Operator;
    /** 创建日期 */
    private String MakeDate;
    /** 创建时间 */
    private String MakeTime;
    /** 修改日期 */
    private String ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDTestGrpMgtSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "TestGrpCode";
        pk[1] = "TestItemCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDTestGrpMgtSchema cloned = (LDTestGrpMgtSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getTestGrpCode() {
        return TestGrpCode;
    }

    public void setTestGrpCode(String aTestGrpCode) {
        TestGrpCode = aTestGrpCode;
    }

    public String getTestGrpName() {
        return TestGrpName;
    }

    public void setTestGrpName(String aTestGrpName) {
        TestGrpName = aTestGrpName;
    }

    public String getTestGrpType() {
        return TestGrpType;
    }

    public void setTestGrpType(String aTestGrpType) {
        TestGrpType = aTestGrpType;
    }

    public String getTestItemCode() {
        return TestItemCode;
    }

    public void setTestItemCode(String aTestItemCode) {
        TestItemCode = aTestItemCode;
    }

    public String getTestItemCommonName() {
        return TestItemCommonName;
    }

    public void setTestItemCommonName(String aTestItemCommonName) {
        TestItemCommonName = aTestItemCommonName;
    }

    public String getOtherSign() {
        return OtherSign;
    }

    public void setOtherSign(String aOtherSign) {
        OtherSign = aOtherSign;
    }

    public String getExplain() {
        return Explain;
    }

    public void setExplain(String aExplain) {
        Explain = aExplain;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    /**
     * 使用另外一个 LDTestGrpMgtSchema 对象给 Schema 赋值
     * @param: aLDTestGrpMgtSchema LDTestGrpMgtSchema
     **/
    public void setSchema(LDTestGrpMgtSchema aLDTestGrpMgtSchema) {
        this.TestGrpCode = aLDTestGrpMgtSchema.getTestGrpCode();
        this.TestGrpName = aLDTestGrpMgtSchema.getTestGrpName();
        this.TestGrpType = aLDTestGrpMgtSchema.getTestGrpType();
        this.TestItemCode = aLDTestGrpMgtSchema.getTestItemCode();
        this.TestItemCommonName = aLDTestGrpMgtSchema.getTestItemCommonName();
        this.OtherSign = aLDTestGrpMgtSchema.getOtherSign();
        this.Explain = aLDTestGrpMgtSchema.getExplain();
        this.Operator = aLDTestGrpMgtSchema.getOperator();
        this.MakeDate = aLDTestGrpMgtSchema.getMakeDate();
        this.MakeTime = aLDTestGrpMgtSchema.getMakeTime();
        this.ModifyDate = aLDTestGrpMgtSchema.getModifyDate();
        this.ModifyTime = aLDTestGrpMgtSchema.getModifyTime();
        this.ManageCom = aLDTestGrpMgtSchema.getManageCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TestGrpCode") == null) {
                this.TestGrpCode = null;
            } else {
                this.TestGrpCode = rs.getString("TestGrpCode").trim();
            }

            if (rs.getString("TestGrpName") == null) {
                this.TestGrpName = null;
            } else {
                this.TestGrpName = rs.getString("TestGrpName").trim();
            }

            if (rs.getString("TestGrpType") == null) {
                this.TestGrpType = null;
            } else {
                this.TestGrpType = rs.getString("TestGrpType").trim();
            }

            if (rs.getString("TestItemCode") == null) {
                this.TestItemCode = null;
            } else {
                this.TestItemCode = rs.getString("TestItemCode").trim();
            }

            if (rs.getString("TestItemCommonName") == null) {
                this.TestItemCommonName = null;
            } else {
                this.TestItemCommonName = rs.getString("TestItemCommonName").
                                          trim();
            }

            if (rs.getString("OtherSign") == null) {
                this.OtherSign = null;
            } else {
                this.OtherSign = rs.getString("OtherSign").trim();
            }

            if (rs.getString("Explain") == null) {
                this.Explain = null;
            } else {
                this.Explain = rs.getString("Explain").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("MakeDate") == null) {
                this.MakeDate = null;
            } else {
                this.MakeDate = rs.getString("MakeDate").trim();
            }

            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            if (rs.getString("ModifyDate") == null) {
                this.ModifyDate = null;
            } else {
                this.ModifyDate = rs.getString("ModifyDate").trim();
            }

            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDTestGrpMgt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTestGrpMgtSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDTestGrpMgtSchema getSchema() {
        LDTestGrpMgtSchema aLDTestGrpMgtSchema = new LDTestGrpMgtSchema();
        aLDTestGrpMgtSchema.setSchema(this);
        return aLDTestGrpMgtSchema;
    }

    public LDTestGrpMgtDB getDB() {
        LDTestGrpMgtDB aDBOper = new LDTestGrpMgtDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTestGrpMgt描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(TestGrpCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestGrpName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestGrpType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestItemCommonName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherSign));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Explain));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeDate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyDate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTestGrpMgt>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            TestGrpCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            TestGrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            TestGrpType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            TestItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            TestItemCommonName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                5, SysConst.PACKAGESPILTER);
            OtherSign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            Explain = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTestGrpMgtSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("TestGrpCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestGrpCode));
        }
        if (FCode.equals("TestGrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestGrpName));
        }
        if (FCode.equals("TestGrpType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestGrpType));
        }
        if (FCode.equals("TestItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestItemCode));
        }
        if (FCode.equals("TestItemCommonName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestItemCommonName));
        }
        if (FCode.equals("OtherSign")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherSign));
        }
        if (FCode.equals("Explain")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Explain));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(TestGrpCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(TestGrpName);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(TestGrpType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(TestItemCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(TestItemCommonName);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(OtherSign);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Explain);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(MakeDate);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ModifyDate);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("TestGrpCode")) {
            if (FValue != null && !FValue.equals("")) {
                TestGrpCode = FValue.trim();
            } else {
                TestGrpCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestGrpName")) {
            if (FValue != null && !FValue.equals("")) {
                TestGrpName = FValue.trim();
            } else {
                TestGrpName = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestGrpType")) {
            if (FValue != null && !FValue.equals("")) {
                TestGrpType = FValue.trim();
            } else {
                TestGrpType = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                TestItemCode = FValue.trim();
            } else {
                TestItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestItemCommonName")) {
            if (FValue != null && !FValue.equals("")) {
                TestItemCommonName = FValue.trim();
            } else {
                TestItemCommonName = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherSign")) {
            if (FValue != null && !FValue.equals("")) {
                OtherSign = FValue.trim();
            } else {
                OtherSign = null;
            }
        }
        if (FCode.equalsIgnoreCase("Explain")) {
            if (FValue != null && !FValue.equals("")) {
                Explain = FValue.trim();
            } else {
                Explain = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = FValue.trim();
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = FValue.trim();
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDTestGrpMgtSchema other = (LDTestGrpMgtSchema) otherObject;
        return
                TestGrpCode.equals(other.getTestGrpCode())
                && TestGrpName.equals(other.getTestGrpName())
                && TestGrpType.equals(other.getTestGrpType())
                && TestItemCode.equals(other.getTestItemCode())
                && TestItemCommonName.equals(other.getTestItemCommonName())
                && OtherSign.equals(other.getOtherSign())
                && Explain.equals(other.getExplain())
                && Operator.equals(other.getOperator())
                && MakeDate.equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && ModifyDate.equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("TestGrpCode")) {
            return 0;
        }
        if (strFieldName.equals("TestGrpName")) {
            return 1;
        }
        if (strFieldName.equals("TestGrpType")) {
            return 2;
        }
        if (strFieldName.equals("TestItemCode")) {
            return 3;
        }
        if (strFieldName.equals("TestItemCommonName")) {
            return 4;
        }
        if (strFieldName.equals("OtherSign")) {
            return 5;
        }
        if (strFieldName.equals("Explain")) {
            return 6;
        }
        if (strFieldName.equals("Operator")) {
            return 7;
        }
        if (strFieldName.equals("MakeDate")) {
            return 8;
        }
        if (strFieldName.equals("MakeTime")) {
            return 9;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 10;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 11;
        }
        if (strFieldName.equals("ManageCom")) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "TestGrpCode";
            break;
        case 1:
            strFieldName = "TestGrpName";
            break;
        case 2:
            strFieldName = "TestGrpType";
            break;
        case 3:
            strFieldName = "TestItemCode";
            break;
        case 4:
            strFieldName = "TestItemCommonName";
            break;
        case 5:
            strFieldName = "OtherSign";
            break;
        case 6:
            strFieldName = "Explain";
            break;
        case 7:
            strFieldName = "Operator";
            break;
        case 8:
            strFieldName = "MakeDate";
            break;
        case 9:
            strFieldName = "MakeTime";
            break;
        case 10:
            strFieldName = "ModifyDate";
            break;
        case 11:
            strFieldName = "ModifyTime";
            break;
        case 12:
            strFieldName = "ManageCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("TestGrpCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestGrpName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestGrpType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestItemCommonName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherSign")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Explain")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
