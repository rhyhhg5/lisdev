/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRTempCessContDB;

/*
 * <p>ClassName: LRTempCessContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-26
 */
public class LRTempCessContSchema implements Schema, Cloneable
{
	// @Field
	/** 临分协议编号 */
	private String TempContCode;
	/** 临分协议名称 */
	private String TempContName;
	/** 再保公司代码 */
	private String ComCode;
	/** 保单号 */
	private String Contno;
	/** 保单类型 */
	private String ContType;
	/** 分保规则 */
	private String ReType;
	/** 分保方式 */
	private String CessionMode;
	/** 险种类型 */
	private String DiskKind;
	/** 保费计算方式 */
	private String CessionFeeMode;
	/** 备用字段1 */
	private String Reserve1;
	/** 备用字段2 */
	private String Reserve2;
	/** 合同状态 */
	private String ReContState;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 操作人 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 保障计划号 */
	private String ContPlanCode;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRTempCessContSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "TempContCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRTempCessContSchema cloned = (LRTempCessContSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTempContCode()
	{
		return TempContCode;
	}
	public void setTempContCode(String aTempContCode)
	{
		TempContCode = aTempContCode;
	}
	public String getTempContName()
	{
		return TempContName;
	}
	public void setTempContName(String aTempContName)
	{
		TempContName = aTempContName;
	}
	public String getComCode()
	{
		return ComCode;
	}
	public void setComCode(String aComCode)
	{
		ComCode = aComCode;
	}
	public String getContno()
	{
		return Contno;
	}
	public void setContno(String aContno)
	{
		Contno = aContno;
	}
	public String getContType()
	{
		return ContType;
	}
	public void setContType(String aContType)
	{
		ContType = aContType;
	}
	public String getReType()
	{
		return ReType;
	}
	public void setReType(String aReType)
	{
		ReType = aReType;
	}
	public String getCessionMode()
	{
		return CessionMode;
	}
	public void setCessionMode(String aCessionMode)
	{
		CessionMode = aCessionMode;
	}
	public String getDiskKind()
	{
		return DiskKind;
	}
	public void setDiskKind(String aDiskKind)
	{
		DiskKind = aDiskKind;
	}
	public String getCessionFeeMode()
	{
		return CessionFeeMode;
	}
	public void setCessionFeeMode(String aCessionFeeMode)
	{
		CessionFeeMode = aCessionFeeMode;
	}
	public String getReserve1()
	{
		return Reserve1;
	}
	public void setReserve1(String aReserve1)
	{
		Reserve1 = aReserve1;
	}
	public String getReserve2()
	{
		return Reserve2;
	}
	public void setReserve2(String aReserve2)
	{
		Reserve2 = aReserve2;
	}
	public String getReContState()
	{
		return ReContState;
	}
	public void setReContState(String aReContState)
	{
		ReContState = aReContState;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getContPlanCode()
	{
		return ContPlanCode;
	}
	public void setContPlanCode(String aContPlanCode)
	{
		ContPlanCode = aContPlanCode;
	}

	/**
	* 使用另外一个 LRTempCessContSchema 对象给 Schema 赋值
	* @param: aLRTempCessContSchema LRTempCessContSchema
	**/
	public void setSchema(LRTempCessContSchema aLRTempCessContSchema)
	{
		this.TempContCode = aLRTempCessContSchema.getTempContCode();
		this.TempContName = aLRTempCessContSchema.getTempContName();
		this.ComCode = aLRTempCessContSchema.getComCode();
		this.Contno = aLRTempCessContSchema.getContno();
		this.ContType = aLRTempCessContSchema.getContType();
		this.ReType = aLRTempCessContSchema.getReType();
		this.CessionMode = aLRTempCessContSchema.getCessionMode();
		this.DiskKind = aLRTempCessContSchema.getDiskKind();
		this.CessionFeeMode = aLRTempCessContSchema.getCessionFeeMode();
		this.Reserve1 = aLRTempCessContSchema.getReserve1();
		this.Reserve2 = aLRTempCessContSchema.getReserve2();
		this.ReContState = aLRTempCessContSchema.getReContState();
		this.MakeDate = fDate.getDate( aLRTempCessContSchema.getMakeDate());
		this.MakeTime = aLRTempCessContSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRTempCessContSchema.getModifyDate());
		this.ModifyTime = aLRTempCessContSchema.getModifyTime();
		this.Operator = aLRTempCessContSchema.getOperator();
		this.ManageCom = aLRTempCessContSchema.getManageCom();
		this.ContPlanCode = aLRTempCessContSchema.getContPlanCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TempContCode") == null )
				this.TempContCode = null;
			else
				this.TempContCode = rs.getString("TempContCode").trim();

			if( rs.getString("TempContName") == null )
				this.TempContName = null;
			else
				this.TempContName = rs.getString("TempContName").trim();

			if( rs.getString("ComCode") == null )
				this.ComCode = null;
			else
				this.ComCode = rs.getString("ComCode").trim();

			if( rs.getString("Contno") == null )
				this.Contno = null;
			else
				this.Contno = rs.getString("Contno").trim();

			if( rs.getString("ContType") == null )
				this.ContType = null;
			else
				this.ContType = rs.getString("ContType").trim();

			if( rs.getString("ReType") == null )
				this.ReType = null;
			else
				this.ReType = rs.getString("ReType").trim();

			if( rs.getString("CessionMode") == null )
				this.CessionMode = null;
			else
				this.CessionMode = rs.getString("CessionMode").trim();

			if( rs.getString("DiskKind") == null )
				this.DiskKind = null;
			else
				this.DiskKind = rs.getString("DiskKind").trim();

			if( rs.getString("CessionFeeMode") == null )
				this.CessionFeeMode = null;
			else
				this.CessionFeeMode = rs.getString("CessionFeeMode").trim();

			if( rs.getString("Reserve1") == null )
				this.Reserve1 = null;
			else
				this.Reserve1 = rs.getString("Reserve1").trim();

			if( rs.getString("Reserve2") == null )
				this.Reserve2 = null;
			else
				this.Reserve2 = rs.getString("Reserve2").trim();

			if( rs.getString("ReContState") == null )
				this.ReContState = null;
			else
				this.ReContState = rs.getString("ReContState").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ContPlanCode") == null )
				this.ContPlanCode = null;
			else
				this.ContPlanCode = rs.getString("ContPlanCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRTempCessCont表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRTempCessContSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRTempCessContSchema getSchema()
	{
		LRTempCessContSchema aLRTempCessContSchema = new LRTempCessContSchema();
		aLRTempCessContSchema.setSchema(this);
		return aLRTempCessContSchema;
	}

	public LRTempCessContDB getDB()
	{
		LRTempCessContDB aDBOper = new LRTempCessContDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRTempCessCont描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TempContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempContName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Contno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CessionMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiskKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CessionFeeMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Reserve1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Reserve2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPlanCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRTempCessCont>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TempContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TempContName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Contno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ReType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CessionMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			DiskKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CessionFeeMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Reserve1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Reserve2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ReContState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRTempCessContSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TempContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempContCode));
		}
		if (FCode.equals("TempContName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempContName));
		}
		if (FCode.equals("ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
		}
		if (FCode.equals("Contno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Contno));
		}
		if (FCode.equals("ContType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
		}
		if (FCode.equals("ReType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReType));
		}
		if (FCode.equals("CessionMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionMode));
		}
		if (FCode.equals("DiskKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiskKind));
		}
		if (FCode.equals("CessionFeeMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionFeeMode));
		}
		if (FCode.equals("Reserve1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reserve1));
		}
		if (FCode.equals("Reserve2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reserve2));
		}
		if (FCode.equals("ReContState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContState));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ContPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TempContCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TempContName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ComCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Contno);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ContType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ReType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CessionMode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(DiskKind);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CessionFeeMode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Reserve1);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Reserve2);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ReContState);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TempContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempContCode = FValue.trim();
			}
			else
				TempContCode = null;
		}
		if (FCode.equalsIgnoreCase("TempContName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempContName = FValue.trim();
			}
			else
				TempContName = null;
		}
		if (FCode.equalsIgnoreCase("ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCode = FValue.trim();
			}
			else
				ComCode = null;
		}
		if (FCode.equalsIgnoreCase("Contno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Contno = FValue.trim();
			}
			else
				Contno = null;
		}
		if (FCode.equalsIgnoreCase("ContType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContType = FValue.trim();
			}
			else
				ContType = null;
		}
		if (FCode.equalsIgnoreCase("ReType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReType = FValue.trim();
			}
			else
				ReType = null;
		}
		if (FCode.equalsIgnoreCase("CessionMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CessionMode = FValue.trim();
			}
			else
				CessionMode = null;
		}
		if (FCode.equalsIgnoreCase("DiskKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiskKind = FValue.trim();
			}
			else
				DiskKind = null;
		}
		if (FCode.equalsIgnoreCase("CessionFeeMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CessionFeeMode = FValue.trim();
			}
			else
				CessionFeeMode = null;
		}
		if (FCode.equalsIgnoreCase("Reserve1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Reserve1 = FValue.trim();
			}
			else
				Reserve1 = null;
		}
		if (FCode.equalsIgnoreCase("Reserve2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Reserve2 = FValue.trim();
			}
			else
				Reserve2 = null;
		}
		if (FCode.equalsIgnoreCase("ReContState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContState = FValue.trim();
			}
			else
				ReContState = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ContPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPlanCode = FValue.trim();
			}
			else
				ContPlanCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRTempCessContSchema other = (LRTempCessContSchema)otherObject;
		return
			(TempContCode == null ? other.getTempContCode() == null : TempContCode.equals(other.getTempContCode()))
			&& (TempContName == null ? other.getTempContName() == null : TempContName.equals(other.getTempContName()))
			&& (ComCode == null ? other.getComCode() == null : ComCode.equals(other.getComCode()))
			&& (Contno == null ? other.getContno() == null : Contno.equals(other.getContno()))
			&& (ContType == null ? other.getContType() == null : ContType.equals(other.getContType()))
			&& (ReType == null ? other.getReType() == null : ReType.equals(other.getReType()))
			&& (CessionMode == null ? other.getCessionMode() == null : CessionMode.equals(other.getCessionMode()))
			&& (DiskKind == null ? other.getDiskKind() == null : DiskKind.equals(other.getDiskKind()))
			&& (CessionFeeMode == null ? other.getCessionFeeMode() == null : CessionFeeMode.equals(other.getCessionFeeMode()))
			&& (Reserve1 == null ? other.getReserve1() == null : Reserve1.equals(other.getReserve1()))
			&& (Reserve2 == null ? other.getReserve2() == null : Reserve2.equals(other.getReserve2()))
			&& (ReContState == null ? other.getReContState() == null : ReContState.equals(other.getReContState()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ContPlanCode == null ? other.getContPlanCode() == null : ContPlanCode.equals(other.getContPlanCode()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TempContCode") ) {
			return 0;
		}
		if( strFieldName.equals("TempContName") ) {
			return 1;
		}
		if( strFieldName.equals("ComCode") ) {
			return 2;
		}
		if( strFieldName.equals("Contno") ) {
			return 3;
		}
		if( strFieldName.equals("ContType") ) {
			return 4;
		}
		if( strFieldName.equals("ReType") ) {
			return 5;
		}
		if( strFieldName.equals("CessionMode") ) {
			return 6;
		}
		if( strFieldName.equals("DiskKind") ) {
			return 7;
		}
		if( strFieldName.equals("CessionFeeMode") ) {
			return 8;
		}
		if( strFieldName.equals("Reserve1") ) {
			return 9;
		}
		if( strFieldName.equals("Reserve2") ) {
			return 10;
		}
		if( strFieldName.equals("ReContState") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 17;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TempContCode";
				break;
			case 1:
				strFieldName = "TempContName";
				break;
			case 2:
				strFieldName = "ComCode";
				break;
			case 3:
				strFieldName = "Contno";
				break;
			case 4:
				strFieldName = "ContType";
				break;
			case 5:
				strFieldName = "ReType";
				break;
			case 6:
				strFieldName = "CessionMode";
				break;
			case 7:
				strFieldName = "DiskKind";
				break;
			case 8:
				strFieldName = "CessionFeeMode";
				break;
			case 9:
				strFieldName = "Reserve1";
				break;
			case 10:
				strFieldName = "Reserve2";
				break;
			case 11:
				strFieldName = "ReContState";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "ManageCom";
				break;
			case 18:
				strFieldName = "ContPlanCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TempContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempContName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Contno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessionMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiskKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessionFeeMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reserve1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reserve2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
