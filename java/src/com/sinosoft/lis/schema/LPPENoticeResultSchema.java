/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPPENoticeResultDB;

/*
 * <p>ClassName: LPPENoticeResultSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-04-21
 */
public class LPPENoticeResultSchema implements Schema, Cloneable {
    // @Field
    /** 保全受理号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 打印流水号 */
    private String PrtSeq;
    /** 体检客户号码 */
    private String CustomerNo;
    /** 体检客户姓名 */
    private String Name;
    /** 内部子序列号 */
    private String SerialNo;
    /** 疾病症状 */
    private String DisDesb;
    /** 疾病结论 */
    private String DisResult;
    /** Icd编码 */
    private String ICDCode;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 使用部门分类 */
    private String SortDepart;

    public static final int FIELDNUM = 18; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPPENoticeResultSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "EdorNo";
        pk[1] = "ProposalContNo";
        pk[2] = "PrtSeq";
        pk[3] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LPPENoticeResultSchema cloned = (LPPENoticeResultSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }

    public String getPrtSeq() {
        return PrtSeq;
    }

    public void setPrtSeq(String aPrtSeq) {
        PrtSeq = aPrtSeq;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String aName) {
        Name = aName;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getDisDesb() {
        return DisDesb;
    }

    public void setDisDesb(String aDisDesb) {
        DisDesb = aDisDesb;
    }

    public String getDisResult() {
        return DisResult;
    }

    public void setDisResult(String aDisResult) {
        DisResult = aDisResult;
    }

    public String getICDCode() {
        return ICDCode;
    }

    public void setICDCode(String aICDCode) {
        ICDCode = aICDCode;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getSortDepart() {
        return SortDepart;
    }

    public void setSortDepart(String aSortDepart) {
        SortDepart = aSortDepart;
    }

    /**
     * 使用另外一个 LPPENoticeResultSchema 对象给 Schema 赋值
     * @param: aLPPENoticeResultSchema LPPENoticeResultSchema
     **/
    public void setSchema(LPPENoticeResultSchema aLPPENoticeResultSchema) {
        this.EdorNo = aLPPENoticeResultSchema.getEdorNo();
        this.GrpContNo = aLPPENoticeResultSchema.getGrpContNo();
        this.ContNo = aLPPENoticeResultSchema.getContNo();
        this.ProposalContNo = aLPPENoticeResultSchema.getProposalContNo();
        this.PrtSeq = aLPPENoticeResultSchema.getPrtSeq();
        this.CustomerNo = aLPPENoticeResultSchema.getCustomerNo();
        this.Name = aLPPENoticeResultSchema.getName();
        this.SerialNo = aLPPENoticeResultSchema.getSerialNo();
        this.DisDesb = aLPPENoticeResultSchema.getDisDesb();
        this.DisResult = aLPPENoticeResultSchema.getDisResult();
        this.ICDCode = aLPPENoticeResultSchema.getICDCode();
        this.Remark = aLPPENoticeResultSchema.getRemark();
        this.Operator = aLPPENoticeResultSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPPENoticeResultSchema.getMakeDate());
        this.MakeTime = aLPPENoticeResultSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPPENoticeResultSchema.getModifyDate());
        this.ModifyTime = aLPPENoticeResultSchema.getModifyTime();
        this.SortDepart = aLPPENoticeResultSchema.getSortDepart();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null) {
                this.EdorNo = null;
            } else {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ProposalContNo") == null) {
                this.ProposalContNo = null;
            } else {
                this.ProposalContNo = rs.getString("ProposalContNo").trim();
            }

            if (rs.getString("PrtSeq") == null) {
                this.PrtSeq = null;
            } else {
                this.PrtSeq = rs.getString("PrtSeq").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("Name") == null) {
                this.Name = null;
            } else {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("DisDesb") == null) {
                this.DisDesb = null;
            } else {
                this.DisDesb = rs.getString("DisDesb").trim();
            }

            if (rs.getString("DisResult") == null) {
                this.DisResult = null;
            } else {
                this.DisResult = rs.getString("DisResult").trim();
            }

            if (rs.getString("ICDCode") == null) {
                this.ICDCode = null;
            } else {
                this.ICDCode = rs.getString("ICDCode").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("SortDepart") == null) {
                this.SortDepart = null;
            } else {
                this.SortDepart = rs.getString("SortDepart").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LPPENoticeResult表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPPENoticeResultSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LPPENoticeResultSchema getSchema() {
        LPPENoticeResultSchema aLPPENoticeResultSchema = new
                LPPENoticeResultSchema();
        aLPPENoticeResultSchema.setSchema(this);
        return aLPPENoticeResultSchema;
    }

    public LPPENoticeResultDB getDB() {
        LPPENoticeResultDB aDBOper = new LPPENoticeResultDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPPENoticeResult描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(EdorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtSeq));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisDesb));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisResult));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ICDCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SortDepart));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPPENoticeResult>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            DisDesb = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            DisResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            ICDCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            SortDepart = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPPENoticeResultSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equals("PrtSeq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("DisDesb")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisDesb));
        }
        if (FCode.equals("DisResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisResult));
        }
        if (FCode.equals("ICDCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ICDCode));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("SortDepart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SortDepart));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(EdorNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(PrtSeq);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Name);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(DisDesb);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(DisResult);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ICDCode);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(SortDepart);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("EdorNo")) {
            if (FValue != null && !FValue.equals("")) {
                EdorNo = FValue.trim();
            } else {
                EdorNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ProposalContNo = FValue.trim();
            } else {
                ProposalContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtSeq")) {
            if (FValue != null && !FValue.equals("")) {
                PrtSeq = FValue.trim();
            } else {
                PrtSeq = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if (FValue != null && !FValue.equals("")) {
                Name = FValue.trim();
            } else {
                Name = null;
            }
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("DisDesb")) {
            if (FValue != null && !FValue.equals("")) {
                DisDesb = FValue.trim();
            } else {
                DisDesb = null;
            }
        }
        if (FCode.equalsIgnoreCase("DisResult")) {
            if (FValue != null && !FValue.equals("")) {
                DisResult = FValue.trim();
            } else {
                DisResult = null;
            }
        }
        if (FCode.equalsIgnoreCase("ICDCode")) {
            if (FValue != null && !FValue.equals("")) {
                ICDCode = FValue.trim();
            } else {
                ICDCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("SortDepart")) {
            if (FValue != null && !FValue.equals("")) {
                SortDepart = FValue.trim();
            } else {
                SortDepart = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LPPENoticeResultSchema other = (LPPENoticeResultSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && ProposalContNo.equals(other.getProposalContNo())
                && PrtSeq.equals(other.getPrtSeq())
                && CustomerNo.equals(other.getCustomerNo())
                && Name.equals(other.getName())
                && SerialNo.equals(other.getSerialNo())
                && DisDesb.equals(other.getDisDesb())
                && DisResult.equals(other.getDisResult())
                && ICDCode.equals(other.getICDCode())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && SortDepart.equals(other.getSortDepart());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("EdorNo")) {
            return 0;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 1;
        }
        if (strFieldName.equals("ContNo")) {
            return 2;
        }
        if (strFieldName.equals("ProposalContNo")) {
            return 3;
        }
        if (strFieldName.equals("PrtSeq")) {
            return 4;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 5;
        }
        if (strFieldName.equals("Name")) {
            return 6;
        }
        if (strFieldName.equals("SerialNo")) {
            return 7;
        }
        if (strFieldName.equals("DisDesb")) {
            return 8;
        }
        if (strFieldName.equals("DisResult")) {
            return 9;
        }
        if (strFieldName.equals("ICDCode")) {
            return 10;
        }
        if (strFieldName.equals("Remark")) {
            return 11;
        }
        if (strFieldName.equals("Operator")) {
            return 12;
        }
        if (strFieldName.equals("MakeDate")) {
            return 13;
        }
        if (strFieldName.equals("MakeTime")) {
            return 14;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 15;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 16;
        }
        if (strFieldName.equals("SortDepart")) {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "EdorNo";
            break;
        case 1:
            strFieldName = "GrpContNo";
            break;
        case 2:
            strFieldName = "ContNo";
            break;
        case 3:
            strFieldName = "ProposalContNo";
            break;
        case 4:
            strFieldName = "PrtSeq";
            break;
        case 5:
            strFieldName = "CustomerNo";
            break;
        case 6:
            strFieldName = "Name";
            break;
        case 7:
            strFieldName = "SerialNo";
            break;
        case 8:
            strFieldName = "DisDesb";
            break;
        case 9:
            strFieldName = "DisResult";
            break;
        case 10:
            strFieldName = "ICDCode";
            break;
        case 11:
            strFieldName = "Remark";
            break;
        case 12:
            strFieldName = "Operator";
            break;
        case 13:
            strFieldName = "MakeDate";
            break;
        case 14:
            strFieldName = "MakeTime";
            break;
        case 15:
            strFieldName = "ModifyDate";
            break;
        case 16:
            strFieldName = "ModifyTime";
            break;
        case 17:
            strFieldName = "SortDepart";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("EdorNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtSeq")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DisDesb")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DisResult")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ICDCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SortDepart")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
