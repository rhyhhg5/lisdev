/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAInterChargeDB;

/*
 * <p>ClassName: LAInterChargeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 互动2015佣金报送存储表
 * @CreateDate：2019-01-11
 */
public class LAInterChargeSchema implements Schema, Cloneable
{
	// @Field
	/** 主键值 */
	private String InterActNo;
	/** 保单号 */
	private String PolicyNo;
	/** 销售人员工号 */
	private String Usercode;
	/** 销售人员姓名 */
	private String UserName;
	/** 销售人员归属省级机构 */
	private String UserComId;
	/** 保单互动专员代码 */
	private String CrossSellUserCode;
	/** 保单互动专员归属机构 */
	private String CrossSellComCode;
	/** 保单专员归属渠道 */
	private String CrossSellAgentCode;
	/** 互动专员有效性 */
	private String CrossSellStatus;
	/** 财险保单经办人 */
	private String HandlerCode;
	/** 财险保单经办人归属机构 */
	private String HandlerComCode;
	/** 财险保单归属人代码 */
	private String Handler1Code;
	/** 财险保单归属人归属机构 */
	private String Handler1ComCode;
	/** 财险保单出单人 */
	private String MakeCode;
	/** 财险保单出单人机构 */
	private String MakeComCode;
	/** 是否可支付佣金状态 */
	private String CommissionExtractionStatus;
	/** 保单应付佣金合计 */
	private double CommissionAmount;
	/** 佣金不可支付原因 */
	private String CommissionNonPaymentDesc;
	/** 公对公支付单号 */
	private String PubToPubPaymentNo;
	/** 保单归属省级机构代码 */
	private String PolicyComId;
	/** 保单保费 */
	private double PolicyPremium;
	/** 出单系统 */
	private String PolicySystem;
	/** 主险代码 */
	private String MainRiskCode;
	/** 主险名称 */
	private String MainRiskName;
	/** 主险保费 */
	private double MainPremium;
	/** 附加险编码 */
	private String SubRiskCode;
	/** 附加险名称 */
	private String SubRiskName;
	/** 附加险保费 */
	private double SubPremium;
	/** 备注 */
	private String Remark;
	/** 公对个人佣金比例 */
	private double CostRate;
	/** 实付佣金 */
	private double PaydocFee;
	/** 公对公佣金 */
	private double CostFee;
	/** 备用1 */
	private String F1;
	/** 备用2 */
	private String F2;
	/** 备用3 */
	private String F3;
	/** 备用4 */
	private String F4;
	/** 备用5 */
	private String F5;
	/** 备用6 */
	private Date F6;
	/** 备用7 */
	private Date F7;
	/** 备用8 */
	private Date F8;
	/** 备用9 */
	private String F9;
	/** 备用0 */
	private String F0;
	/** C1 */
	private double C1;
	/** C2 */
	private double C2;
	/** C3 */
	private double C3;
	/** R1 */
	private double R1;
	/** R2 */
	private double R2;
	/** R3 */
	private double R3;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 健康险内部工号 */
	private String AgentCode;
	/** 修改时间 */
	private String ModifyTime;
	/** 实时付手续费 */
	private double PerCharge;
	/** 剩余手续费 */
	private double ReMainCharge;
	/** 备用11 */
	private String F11;
	/** 备用12 */
	private String F12;
	/** 备用13 */
	private String F13;
	/** 备用14 */
	private String F14;
	/** 备用15 */
	private String F15;

	public static final int FIELDNUM = 60;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAInterChargeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "InterActNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAInterChargeSchema cloned = (LAInterChargeSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getInterActNo()
	{
		return InterActNo;
	}
	public void setInterActNo(String aInterActNo)
	{
		InterActNo = aInterActNo;
	}
	public String getPolicyNo()
	{
		return PolicyNo;
	}
	public void setPolicyNo(String aPolicyNo)
	{
		PolicyNo = aPolicyNo;
	}
	public String getUsercode()
	{
		return Usercode;
	}
	public void setUsercode(String aUsercode)
	{
		Usercode = aUsercode;
	}
	public String getUserName()
	{
		return UserName;
	}
	public void setUserName(String aUserName)
	{
		UserName = aUserName;
	}
	public String getUserComId()
	{
		return UserComId;
	}
	public void setUserComId(String aUserComId)
	{
		UserComId = aUserComId;
	}
	public String getCrossSellUserCode()
	{
		return CrossSellUserCode;
	}
	public void setCrossSellUserCode(String aCrossSellUserCode)
	{
		CrossSellUserCode = aCrossSellUserCode;
	}
	public String getCrossSellComCode()
	{
		return CrossSellComCode;
	}
	public void setCrossSellComCode(String aCrossSellComCode)
	{
		CrossSellComCode = aCrossSellComCode;
	}
	public String getCrossSellAgentCode()
	{
		return CrossSellAgentCode;
	}
	public void setCrossSellAgentCode(String aCrossSellAgentCode)
	{
		CrossSellAgentCode = aCrossSellAgentCode;
	}
	public String getCrossSellStatus()
	{
		return CrossSellStatus;
	}
	public void setCrossSellStatus(String aCrossSellStatus)
	{
		CrossSellStatus = aCrossSellStatus;
	}
	public String getHandlerCode()
	{
		return HandlerCode;
	}
	public void setHandlerCode(String aHandlerCode)
	{
		HandlerCode = aHandlerCode;
	}
	public String getHandlerComCode()
	{
		return HandlerComCode;
	}
	public void setHandlerComCode(String aHandlerComCode)
	{
		HandlerComCode = aHandlerComCode;
	}
	public String getHandler1Code()
	{
		return Handler1Code;
	}
	public void setHandler1Code(String aHandler1Code)
	{
		Handler1Code = aHandler1Code;
	}
	public String getHandler1ComCode()
	{
		return Handler1ComCode;
	}
	public void setHandler1ComCode(String aHandler1ComCode)
	{
		Handler1ComCode = aHandler1ComCode;
	}
	public String getMakeCode()
	{
		return MakeCode;
	}
	public void setMakeCode(String aMakeCode)
	{
		MakeCode = aMakeCode;
	}
	public String getMakeComCode()
	{
		return MakeComCode;
	}
	public void setMakeComCode(String aMakeComCode)
	{
		MakeComCode = aMakeComCode;
	}
	public String getCommissionExtractionStatus()
	{
		return CommissionExtractionStatus;
	}
	public void setCommissionExtractionStatus(String aCommissionExtractionStatus)
	{
		CommissionExtractionStatus = aCommissionExtractionStatus;
	}
	public double getCommissionAmount()
	{
		return CommissionAmount;
	}
	public void setCommissionAmount(double aCommissionAmount)
	{
		CommissionAmount = Arith.round(aCommissionAmount,2);
	}
	public void setCommissionAmount(String aCommissionAmount)
	{
		if (aCommissionAmount != null && !aCommissionAmount.equals(""))
		{
			Double tDouble = new Double(aCommissionAmount);
			double d = tDouble.doubleValue();
                CommissionAmount = Arith.round(d,2);
		}
	}

	public String getCommissionNonPaymentDesc()
	{
		return CommissionNonPaymentDesc;
	}
	public void setCommissionNonPaymentDesc(String aCommissionNonPaymentDesc)
	{
		CommissionNonPaymentDesc = aCommissionNonPaymentDesc;
	}
	public String getPubToPubPaymentNo()
	{
		return PubToPubPaymentNo;
	}
	public void setPubToPubPaymentNo(String aPubToPubPaymentNo)
	{
		PubToPubPaymentNo = aPubToPubPaymentNo;
	}
	public String getPolicyComId()
	{
		return PolicyComId;
	}
	public void setPolicyComId(String aPolicyComId)
	{
		PolicyComId = aPolicyComId;
	}
	public double getPolicyPremium()
	{
		return PolicyPremium;
	}
	public void setPolicyPremium(double aPolicyPremium)
	{
		PolicyPremium = Arith.round(aPolicyPremium,2);
	}
	public void setPolicyPremium(String aPolicyPremium)
	{
		if (aPolicyPremium != null && !aPolicyPremium.equals(""))
		{
			Double tDouble = new Double(aPolicyPremium);
			double d = tDouble.doubleValue();
                PolicyPremium = Arith.round(d,2);
		}
	}

	public String getPolicySystem()
	{
		return PolicySystem;
	}
	public void setPolicySystem(String aPolicySystem)
	{
		PolicySystem = aPolicySystem;
	}
	public String getMainRiskCode()
	{
		return MainRiskCode;
	}
	public void setMainRiskCode(String aMainRiskCode)
	{
		MainRiskCode = aMainRiskCode;
	}
	public String getMainRiskName()
	{
		return MainRiskName;
	}
	public void setMainRiskName(String aMainRiskName)
	{
		MainRiskName = aMainRiskName;
	}
	public double getMainPremium()
	{
		return MainPremium;
	}
	public void setMainPremium(double aMainPremium)
	{
		MainPremium = Arith.round(aMainPremium,2);
	}
	public void setMainPremium(String aMainPremium)
	{
		if (aMainPremium != null && !aMainPremium.equals(""))
		{
			Double tDouble = new Double(aMainPremium);
			double d = tDouble.doubleValue();
                MainPremium = Arith.round(d,2);
		}
	}

	public String getSubRiskCode()
	{
		return SubRiskCode;
	}
	public void setSubRiskCode(String aSubRiskCode)
	{
		SubRiskCode = aSubRiskCode;
	}
	public String getSubRiskName()
	{
		return SubRiskName;
	}
	public void setSubRiskName(String aSubRiskName)
	{
		SubRiskName = aSubRiskName;
	}
	public double getSubPremium()
	{
		return SubPremium;
	}
	public void setSubPremium(double aSubPremium)
	{
		SubPremium = Arith.round(aSubPremium,2);
	}
	public void setSubPremium(String aSubPremium)
	{
		if (aSubPremium != null && !aSubPremium.equals(""))
		{
			Double tDouble = new Double(aSubPremium);
			double d = tDouble.doubleValue();
                SubPremium = Arith.round(d,2);
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public double getCostRate()
	{
		return CostRate;
	}
	public void setCostRate(double aCostRate)
	{
		CostRate = Arith.round(aCostRate,6);
	}
	public void setCostRate(String aCostRate)
	{
		if (aCostRate != null && !aCostRate.equals(""))
		{
			Double tDouble = new Double(aCostRate);
			double d = tDouble.doubleValue();
                CostRate = Arith.round(d,6);
		}
	}

	public double getPaydocFee()
	{
		return PaydocFee;
	}
	public void setPaydocFee(double aPaydocFee)
	{
		PaydocFee = Arith.round(aPaydocFee,2);
	}
	public void setPaydocFee(String aPaydocFee)
	{
		if (aPaydocFee != null && !aPaydocFee.equals(""))
		{
			Double tDouble = new Double(aPaydocFee);
			double d = tDouble.doubleValue();
                PaydocFee = Arith.round(d,2);
		}
	}

	public double getCostFee()
	{
		return CostFee;
	}
	public void setCostFee(double aCostFee)
	{
		CostFee = Arith.round(aCostFee,2);
	}
	public void setCostFee(String aCostFee)
	{
		if (aCostFee != null && !aCostFee.equals(""))
		{
			Double tDouble = new Double(aCostFee);
			double d = tDouble.doubleValue();
                CostFee = Arith.round(d,2);
		}
	}

	public String getF1()
	{
		return F1;
	}
	public void setF1(String aF1)
	{
		F1 = aF1;
	}
	public String getF2()
	{
		return F2;
	}
	public void setF2(String aF2)
	{
		F2 = aF2;
	}
	public String getF3()
	{
		return F3;
	}
	public void setF3(String aF3)
	{
		F3 = aF3;
	}
	public String getF4()
	{
		return F4;
	}
	public void setF4(String aF4)
	{
		F4 = aF4;
	}
	public String getF5()
	{
		return F5;
	}
	public void setF5(String aF5)
	{
		F5 = aF5;
	}
	public String getF6()
	{
		if( F6 != null )
			return fDate.getString(F6);
		else
			return null;
	}
	public void setF6(Date aF6)
	{
		F6 = aF6;
	}
	public void setF6(String aF6)
	{
		if (aF6 != null && !aF6.equals("") )
		{
			F6 = fDate.getDate( aF6 );
		}
		else
			F6 = null;
	}

	public String getF7()
	{
		if( F7 != null )
			return fDate.getString(F7);
		else
			return null;
	}
	public void setF7(Date aF7)
	{
		F7 = aF7;
	}
	public void setF7(String aF7)
	{
		if (aF7 != null && !aF7.equals("") )
		{
			F7 = fDate.getDate( aF7 );
		}
		else
			F7 = null;
	}

	public String getF8()
	{
		if( F8 != null )
			return fDate.getString(F8);
		else
			return null;
	}
	public void setF8(Date aF8)
	{
		F8 = aF8;
	}
	public void setF8(String aF8)
	{
		if (aF8 != null && !aF8.equals("") )
		{
			F8 = fDate.getDate( aF8 );
		}
		else
			F8 = null;
	}

	public String getF9()
	{
		return F9;
	}
	public void setF9(String aF9)
	{
		F9 = aF9;
	}
	public String getF0()
	{
		return F0;
	}
	public void setF0(String aF0)
	{
		F0 = aF0;
	}
	public double getC1()
	{
		return C1;
	}
	public void setC1(double aC1)
	{
		C1 = Arith.round(aC1,2);
	}
	public void setC1(String aC1)
	{
		if (aC1 != null && !aC1.equals(""))
		{
			Double tDouble = new Double(aC1);
			double d = tDouble.doubleValue();
                C1 = Arith.round(d,2);
		}
	}

	public double getC2()
	{
		return C2;
	}
	public void setC2(double aC2)
	{
		C2 = Arith.round(aC2,2);
	}
	public void setC2(String aC2)
	{
		if (aC2 != null && !aC2.equals(""))
		{
			Double tDouble = new Double(aC2);
			double d = tDouble.doubleValue();
                C2 = Arith.round(d,2);
		}
	}

	public double getC3()
	{
		return C3;
	}
	public void setC3(double aC3)
	{
		C3 = Arith.round(aC3,2);
	}
	public void setC3(String aC3)
	{
		if (aC3 != null && !aC3.equals(""))
		{
			Double tDouble = new Double(aC3);
			double d = tDouble.doubleValue();
                C3 = Arith.round(d,2);
		}
	}

	public double getR1()
	{
		return R1;
	}
	public void setR1(double aR1)
	{
		R1 = Arith.round(aR1,6);
	}
	public void setR1(String aR1)
	{
		if (aR1 != null && !aR1.equals(""))
		{
			Double tDouble = new Double(aR1);
			double d = tDouble.doubleValue();
                R1 = Arith.round(d,6);
		}
	}

	public double getR2()
	{
		return R2;
	}
	public void setR2(double aR2)
	{
		R2 = Arith.round(aR2,6);
	}
	public void setR2(String aR2)
	{
		if (aR2 != null && !aR2.equals(""))
		{
			Double tDouble = new Double(aR2);
			double d = tDouble.doubleValue();
                R2 = Arith.round(d,6);
		}
	}

	public double getR3()
	{
		return R3;
	}
	public void setR3(double aR3)
	{
		R3 = Arith.round(aR3,2);
	}
	public void setR3(String aR3)
	{
		if (aR3 != null && !aR3.equals(""))
		{
			Double tDouble = new Double(aR3);
			double d = tDouble.doubleValue();
                R3 = Arith.round(d,2);
		}
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public double getPerCharge()
	{
		return PerCharge;
	}
	public void setPerCharge(double aPerCharge)
	{
		PerCharge = Arith.round(aPerCharge,2);
	}
	public void setPerCharge(String aPerCharge)
	{
		if (aPerCharge != null && !aPerCharge.equals(""))
		{
			Double tDouble = new Double(aPerCharge);
			double d = tDouble.doubleValue();
                PerCharge = Arith.round(d,2);
		}
	}

	public double getReMainCharge()
	{
		return ReMainCharge;
	}
	public void setReMainCharge(double aReMainCharge)
	{
		ReMainCharge = Arith.round(aReMainCharge,2);
	}
	public void setReMainCharge(String aReMainCharge)
	{
		if (aReMainCharge != null && !aReMainCharge.equals(""))
		{
			Double tDouble = new Double(aReMainCharge);
			double d = tDouble.doubleValue();
                ReMainCharge = Arith.round(d,2);
		}
	}

	public String getF11()
	{
		return F11;
	}
	public void setF11(String aF11)
	{
		F11 = aF11;
	}
	public String getF12()
	{
		return F12;
	}
	public void setF12(String aF12)
	{
		F12 = aF12;
	}
	public String getF13()
	{
		return F13;
	}
	public void setF13(String aF13)
	{
		F13 = aF13;
	}
	public String getF14()
	{
		return F14;
	}
	public void setF14(String aF14)
	{
		F14 = aF14;
	}
	public String getF15()
	{
		return F15;
	}
	public void setF15(String aF15)
	{
		F15 = aF15;
	}

	/**
	* 使用另外一个 LAInterChargeSchema 对象给 Schema 赋值
	* @param: aLAInterChargeSchema LAInterChargeSchema
	**/
	public void setSchema(LAInterChargeSchema aLAInterChargeSchema)
	{
		this.InterActNo = aLAInterChargeSchema.getInterActNo();
		this.PolicyNo = aLAInterChargeSchema.getPolicyNo();
		this.Usercode = aLAInterChargeSchema.getUsercode();
		this.UserName = aLAInterChargeSchema.getUserName();
		this.UserComId = aLAInterChargeSchema.getUserComId();
		this.CrossSellUserCode = aLAInterChargeSchema.getCrossSellUserCode();
		this.CrossSellComCode = aLAInterChargeSchema.getCrossSellComCode();
		this.CrossSellAgentCode = aLAInterChargeSchema.getCrossSellAgentCode();
		this.CrossSellStatus = aLAInterChargeSchema.getCrossSellStatus();
		this.HandlerCode = aLAInterChargeSchema.getHandlerCode();
		this.HandlerComCode = aLAInterChargeSchema.getHandlerComCode();
		this.Handler1Code = aLAInterChargeSchema.getHandler1Code();
		this.Handler1ComCode = aLAInterChargeSchema.getHandler1ComCode();
		this.MakeCode = aLAInterChargeSchema.getMakeCode();
		this.MakeComCode = aLAInterChargeSchema.getMakeComCode();
		this.CommissionExtractionStatus = aLAInterChargeSchema.getCommissionExtractionStatus();
		this.CommissionAmount = aLAInterChargeSchema.getCommissionAmount();
		this.CommissionNonPaymentDesc = aLAInterChargeSchema.getCommissionNonPaymentDesc();
		this.PubToPubPaymentNo = aLAInterChargeSchema.getPubToPubPaymentNo();
		this.PolicyComId = aLAInterChargeSchema.getPolicyComId();
		this.PolicyPremium = aLAInterChargeSchema.getPolicyPremium();
		this.PolicySystem = aLAInterChargeSchema.getPolicySystem();
		this.MainRiskCode = aLAInterChargeSchema.getMainRiskCode();
		this.MainRiskName = aLAInterChargeSchema.getMainRiskName();
		this.MainPremium = aLAInterChargeSchema.getMainPremium();
		this.SubRiskCode = aLAInterChargeSchema.getSubRiskCode();
		this.SubRiskName = aLAInterChargeSchema.getSubRiskName();
		this.SubPremium = aLAInterChargeSchema.getSubPremium();
		this.Remark = aLAInterChargeSchema.getRemark();
		this.CostRate = aLAInterChargeSchema.getCostRate();
		this.PaydocFee = aLAInterChargeSchema.getPaydocFee();
		this.CostFee = aLAInterChargeSchema.getCostFee();
		this.F1 = aLAInterChargeSchema.getF1();
		this.F2 = aLAInterChargeSchema.getF2();
		this.F3 = aLAInterChargeSchema.getF3();
		this.F4 = aLAInterChargeSchema.getF4();
		this.F5 = aLAInterChargeSchema.getF5();
		this.F6 = fDate.getDate( aLAInterChargeSchema.getF6());
		this.F7 = fDate.getDate( aLAInterChargeSchema.getF7());
		this.F8 = fDate.getDate( aLAInterChargeSchema.getF8());
		this.F9 = aLAInterChargeSchema.getF9();
		this.F0 = aLAInterChargeSchema.getF0();
		this.C1 = aLAInterChargeSchema.getC1();
		this.C2 = aLAInterChargeSchema.getC2();
		this.C3 = aLAInterChargeSchema.getC3();
		this.R1 = aLAInterChargeSchema.getR1();
		this.R2 = aLAInterChargeSchema.getR2();
		this.R3 = aLAInterChargeSchema.getR3();
		this.MakeDate = fDate.getDate( aLAInterChargeSchema.getMakeDate());
		this.MakeTime = aLAInterChargeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAInterChargeSchema.getModifyDate());
		this.AgentCode = aLAInterChargeSchema.getAgentCode();
		this.ModifyTime = aLAInterChargeSchema.getModifyTime();
		this.PerCharge = aLAInterChargeSchema.getPerCharge();
		this.ReMainCharge = aLAInterChargeSchema.getReMainCharge();
		this.F11 = aLAInterChargeSchema.getF11();
		this.F12 = aLAInterChargeSchema.getF12();
		this.F13 = aLAInterChargeSchema.getF13();
		this.F14 = aLAInterChargeSchema.getF14();
		this.F15 = aLAInterChargeSchema.getF15();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("InterActNo") == null )
				this.InterActNo = null;
			else
				this.InterActNo = rs.getString("InterActNo").trim();

			if( rs.getString("PolicyNo") == null )
				this.PolicyNo = null;
			else
				this.PolicyNo = rs.getString("PolicyNo").trim();

			if( rs.getString("Usercode") == null )
				this.Usercode = null;
			else
				this.Usercode = rs.getString("Usercode").trim();

			if( rs.getString("UserName") == null )
				this.UserName = null;
			else
				this.UserName = rs.getString("UserName").trim();

			if( rs.getString("UserComId") == null )
				this.UserComId = null;
			else
				this.UserComId = rs.getString("UserComId").trim();

			if( rs.getString("CrossSellUserCode") == null )
				this.CrossSellUserCode = null;
			else
				this.CrossSellUserCode = rs.getString("CrossSellUserCode").trim();

			if( rs.getString("CrossSellComCode") == null )
				this.CrossSellComCode = null;
			else
				this.CrossSellComCode = rs.getString("CrossSellComCode").trim();

			if( rs.getString("CrossSellAgentCode") == null )
				this.CrossSellAgentCode = null;
			else
				this.CrossSellAgentCode = rs.getString("CrossSellAgentCode").trim();

			if( rs.getString("CrossSellStatus") == null )
				this.CrossSellStatus = null;
			else
				this.CrossSellStatus = rs.getString("CrossSellStatus").trim();

			if( rs.getString("HandlerCode") == null )
				this.HandlerCode = null;
			else
				this.HandlerCode = rs.getString("HandlerCode").trim();

			if( rs.getString("HandlerComCode") == null )
				this.HandlerComCode = null;
			else
				this.HandlerComCode = rs.getString("HandlerComCode").trim();

			if( rs.getString("Handler1Code") == null )
				this.Handler1Code = null;
			else
				this.Handler1Code = rs.getString("Handler1Code").trim();

			if( rs.getString("Handler1ComCode") == null )
				this.Handler1ComCode = null;
			else
				this.Handler1ComCode = rs.getString("Handler1ComCode").trim();

			if( rs.getString("MakeCode") == null )
				this.MakeCode = null;
			else
				this.MakeCode = rs.getString("MakeCode").trim();

			if( rs.getString("MakeComCode") == null )
				this.MakeComCode = null;
			else
				this.MakeComCode = rs.getString("MakeComCode").trim();

			if( rs.getString("CommissionExtractionStatus") == null )
				this.CommissionExtractionStatus = null;
			else
				this.CommissionExtractionStatus = rs.getString("CommissionExtractionStatus").trim();

			this.CommissionAmount = rs.getDouble("CommissionAmount");
			if( rs.getString("CommissionNonPaymentDesc") == null )
				this.CommissionNonPaymentDesc = null;
			else
				this.CommissionNonPaymentDesc = rs.getString("CommissionNonPaymentDesc").trim();

			if( rs.getString("PubToPubPaymentNo") == null )
				this.PubToPubPaymentNo = null;
			else
				this.PubToPubPaymentNo = rs.getString("PubToPubPaymentNo").trim();

			if( rs.getString("PolicyComId") == null )
				this.PolicyComId = null;
			else
				this.PolicyComId = rs.getString("PolicyComId").trim();

			this.PolicyPremium = rs.getDouble("PolicyPremium");
			if( rs.getString("PolicySystem") == null )
				this.PolicySystem = null;
			else
				this.PolicySystem = rs.getString("PolicySystem").trim();

			if( rs.getString("MainRiskCode") == null )
				this.MainRiskCode = null;
			else
				this.MainRiskCode = rs.getString("MainRiskCode").trim();

			if( rs.getString("MainRiskName") == null )
				this.MainRiskName = null;
			else
				this.MainRiskName = rs.getString("MainRiskName").trim();

			this.MainPremium = rs.getDouble("MainPremium");
			if( rs.getString("SubRiskCode") == null )
				this.SubRiskCode = null;
			else
				this.SubRiskCode = rs.getString("SubRiskCode").trim();

			if( rs.getString("SubRiskName") == null )
				this.SubRiskName = null;
			else
				this.SubRiskName = rs.getString("SubRiskName").trim();

			this.SubPremium = rs.getDouble("SubPremium");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			this.CostRate = rs.getDouble("CostRate");
			this.PaydocFee = rs.getDouble("PaydocFee");
			this.CostFee = rs.getDouble("CostFee");
			if( rs.getString("F1") == null )
				this.F1 = null;
			else
				this.F1 = rs.getString("F1").trim();

			if( rs.getString("F2") == null )
				this.F2 = null;
			else
				this.F2 = rs.getString("F2").trim();

			if( rs.getString("F3") == null )
				this.F3 = null;
			else
				this.F3 = rs.getString("F3").trim();

			if( rs.getString("F4") == null )
				this.F4 = null;
			else
				this.F4 = rs.getString("F4").trim();

			if( rs.getString("F5") == null )
				this.F5 = null;
			else
				this.F5 = rs.getString("F5").trim();

			this.F6 = rs.getDate("F6");
			this.F7 = rs.getDate("F7");
			this.F8 = rs.getDate("F8");
			if( rs.getString("F9") == null )
				this.F9 = null;
			else
				this.F9 = rs.getString("F9").trim();

			if( rs.getString("F0") == null )
				this.F0 = null;
			else
				this.F0 = rs.getString("F0").trim();

			this.C1 = rs.getDouble("C1");
			this.C2 = rs.getDouble("C2");
			this.C3 = rs.getDouble("C3");
			this.R1 = rs.getDouble("R1");
			this.R2 = rs.getDouble("R2");
			this.R3 = rs.getDouble("R3");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.PerCharge = rs.getDouble("PerCharge");
			this.ReMainCharge = rs.getDouble("ReMainCharge");
			if( rs.getString("F11") == null )
				this.F11 = null;
			else
				this.F11 = rs.getString("F11").trim();

			if( rs.getString("F12") == null )
				this.F12 = null;
			else
				this.F12 = rs.getString("F12").trim();

			if( rs.getString("F13") == null )
				this.F13 = null;
			else
				this.F13 = rs.getString("F13").trim();

			if( rs.getString("F14") == null )
				this.F14 = null;
			else
				this.F14 = rs.getString("F14").trim();

			if( rs.getString("F15") == null )
				this.F15 = null;
			else
				this.F15 = rs.getString("F15").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAInterCharge表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAInterChargeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAInterChargeSchema getSchema()
	{
		LAInterChargeSchema aLAInterChargeSchema = new LAInterChargeSchema();
		aLAInterChargeSchema.setSchema(this);
		return aLAInterChargeSchema;
	}

	public LAInterChargeDB getDB()
	{
		LAInterChargeDB aDBOper = new LAInterChargeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterCharge描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(InterActNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Usercode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UserName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UserComId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CrossSellUserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CrossSellComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CrossSellAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CrossSellStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler1Code)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler1ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CommissionExtractionStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CommissionAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CommissionNonPaymentDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PubToPubPaymentNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicyComId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolicyPremium));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicySystem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainRiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainRiskName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MainPremium));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubRiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubRiskName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SubPremium));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CostRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PaydocFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CostFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F6 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F7 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( F8 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F0)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(C3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(R3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PerCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReMainCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F11)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F12)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F13)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F14)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F15));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterCharge>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			InterActNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Usercode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			UserComId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CrossSellUserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CrossSellComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CrossSellAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CrossSellStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			HandlerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			HandlerComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Handler1Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Handler1ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			CommissionExtractionStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CommissionAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			CommissionNonPaymentDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			PubToPubPaymentNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PolicyComId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PolicyPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			PolicySystem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			MainRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MainRiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			MainPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			SubRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			SubRiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			SubPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			CostRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			PaydocFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			CostFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			F1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			F2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			F3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			F4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			F5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			F6 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			F7 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			F8 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
			F9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			F0 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			C1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,43,SysConst.PACKAGESPILTER))).doubleValue();
			C2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,44,SysConst.PACKAGESPILTER))).doubleValue();
			C3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).doubleValue();
			R1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
			R2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).doubleValue();
			R3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).doubleValue();
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51,SysConst.PACKAGESPILTER));
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			PerCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,54,SysConst.PACKAGESPILTER))).doubleValue();
			ReMainCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,55,SysConst.PACKAGESPILTER))).doubleValue();
			F11 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			F12 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			F13 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			F14 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			F15 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAInterChargeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("InterActNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InterActNo));
		}
		if (FCode.equals("PolicyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
		}
		if (FCode.equals("Usercode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Usercode));
		}
		if (FCode.equals("UserName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserName));
		}
		if (FCode.equals("UserComId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserComId));
		}
		if (FCode.equals("CrossSellUserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CrossSellUserCode));
		}
		if (FCode.equals("CrossSellComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CrossSellComCode));
		}
		if (FCode.equals("CrossSellAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CrossSellAgentCode));
		}
		if (FCode.equals("CrossSellStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CrossSellStatus));
		}
		if (FCode.equals("HandlerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerCode));
		}
		if (FCode.equals("HandlerComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerComCode));
		}
		if (FCode.equals("Handler1Code"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler1Code));
		}
		if (FCode.equals("Handler1ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler1ComCode));
		}
		if (FCode.equals("MakeCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeCode));
		}
		if (FCode.equals("MakeComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeComCode));
		}
		if (FCode.equals("CommissionExtractionStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommissionExtractionStatus));
		}
		if (FCode.equals("CommissionAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommissionAmount));
		}
		if (FCode.equals("CommissionNonPaymentDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommissionNonPaymentDesc));
		}
		if (FCode.equals("PubToPubPaymentNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PubToPubPaymentNo));
		}
		if (FCode.equals("PolicyComId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyComId));
		}
		if (FCode.equals("PolicyPremium"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyPremium));
		}
		if (FCode.equals("PolicySystem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicySystem));
		}
		if (FCode.equals("MainRiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskCode));
		}
		if (FCode.equals("MainRiskName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskName));
		}
		if (FCode.equals("MainPremium"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainPremium));
		}
		if (FCode.equals("SubRiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubRiskCode));
		}
		if (FCode.equals("SubRiskName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubRiskName));
		}
		if (FCode.equals("SubPremium"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubPremium));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("CostRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostRate));
		}
		if (FCode.equals("PaydocFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PaydocFee));
		}
		if (FCode.equals("CostFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostFee));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("F3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F3));
		}
		if (FCode.equals("F4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F4));
		}
		if (FCode.equals("F5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F5));
		}
		if (FCode.equals("F6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF6()));
		}
		if (FCode.equals("F7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF7()));
		}
		if (FCode.equals("F8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getF8()));
		}
		if (FCode.equals("F9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F9));
		}
		if (FCode.equals("F0"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F0));
		}
		if (FCode.equals("C1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C1));
		}
		if (FCode.equals("C2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C2));
		}
		if (FCode.equals("C3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C3));
		}
		if (FCode.equals("R1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R1));
		}
		if (FCode.equals("R2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R2));
		}
		if (FCode.equals("R3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(R3));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("PerCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PerCharge));
		}
		if (FCode.equals("ReMainCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReMainCharge));
		}
		if (FCode.equals("F11"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F11));
		}
		if (FCode.equals("F12"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F12));
		}
		if (FCode.equals("F13"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F13));
		}
		if (FCode.equals("F14"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F14));
		}
		if (FCode.equals("F15"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F15));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(InterActNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PolicyNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Usercode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(UserName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(UserComId);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CrossSellUserCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CrossSellComCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CrossSellAgentCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CrossSellStatus);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(HandlerCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(HandlerComCode);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Handler1Code);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Handler1ComCode);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeCode);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeComCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(CommissionExtractionStatus);
				break;
			case 16:
				strFieldValue = String.valueOf(CommissionAmount);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(CommissionNonPaymentDesc);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(PubToPubPaymentNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(PolicyComId);
				break;
			case 20:
				strFieldValue = String.valueOf(PolicyPremium);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(PolicySystem);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MainRiskCode);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MainRiskName);
				break;
			case 24:
				strFieldValue = String.valueOf(MainPremium);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(SubRiskCode);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(SubRiskName);
				break;
			case 27:
				strFieldValue = String.valueOf(SubPremium);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 29:
				strFieldValue = String.valueOf(CostRate);
				break;
			case 30:
				strFieldValue = String.valueOf(PaydocFee);
				break;
			case 31:
				strFieldValue = String.valueOf(CostFee);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(F1);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(F2);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(F3);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(F4);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(F5);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF6()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF7()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getF8()));
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(F9);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(F0);
				break;
			case 42:
				strFieldValue = String.valueOf(C1);
				break;
			case 43:
				strFieldValue = String.valueOf(C2);
				break;
			case 44:
				strFieldValue = String.valueOf(C3);
				break;
			case 45:
				strFieldValue = String.valueOf(R1);
				break;
			case 46:
				strFieldValue = String.valueOf(R2);
				break;
			case 47:
				strFieldValue = String.valueOf(R3);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 53:
				strFieldValue = String.valueOf(PerCharge);
				break;
			case 54:
				strFieldValue = String.valueOf(ReMainCharge);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(F11);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(F12);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(F13);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(F14);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(F15);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("InterActNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InterActNo = FValue.trim();
			}
			else
				InterActNo = null;
		}
		if (FCode.equalsIgnoreCase("PolicyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyNo = FValue.trim();
			}
			else
				PolicyNo = null;
		}
		if (FCode.equalsIgnoreCase("Usercode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Usercode = FValue.trim();
			}
			else
				Usercode = null;
		}
		if (FCode.equalsIgnoreCase("UserName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserName = FValue.trim();
			}
			else
				UserName = null;
		}
		if (FCode.equalsIgnoreCase("UserComId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserComId = FValue.trim();
			}
			else
				UserComId = null;
		}
		if (FCode.equalsIgnoreCase("CrossSellUserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CrossSellUserCode = FValue.trim();
			}
			else
				CrossSellUserCode = null;
		}
		if (FCode.equalsIgnoreCase("CrossSellComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CrossSellComCode = FValue.trim();
			}
			else
				CrossSellComCode = null;
		}
		if (FCode.equalsIgnoreCase("CrossSellAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CrossSellAgentCode = FValue.trim();
			}
			else
				CrossSellAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("CrossSellStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CrossSellStatus = FValue.trim();
			}
			else
				CrossSellStatus = null;
		}
		if (FCode.equalsIgnoreCase("HandlerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerCode = FValue.trim();
			}
			else
				HandlerCode = null;
		}
		if (FCode.equalsIgnoreCase("HandlerComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerComCode = FValue.trim();
			}
			else
				HandlerComCode = null;
		}
		if (FCode.equalsIgnoreCase("Handler1Code"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler1Code = FValue.trim();
			}
			else
				Handler1Code = null;
		}
		if (FCode.equalsIgnoreCase("Handler1ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler1ComCode = FValue.trim();
			}
			else
				Handler1ComCode = null;
		}
		if (FCode.equalsIgnoreCase("MakeCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeCode = FValue.trim();
			}
			else
				MakeCode = null;
		}
		if (FCode.equalsIgnoreCase("MakeComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeComCode = FValue.trim();
			}
			else
				MakeComCode = null;
		}
		if (FCode.equalsIgnoreCase("CommissionExtractionStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommissionExtractionStatus = FValue.trim();
			}
			else
				CommissionExtractionStatus = null;
		}
		if (FCode.equalsIgnoreCase("CommissionAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CommissionAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("CommissionNonPaymentDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommissionNonPaymentDesc = FValue.trim();
			}
			else
				CommissionNonPaymentDesc = null;
		}
		if (FCode.equalsIgnoreCase("PubToPubPaymentNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PubToPubPaymentNo = FValue.trim();
			}
			else
				PubToPubPaymentNo = null;
		}
		if (FCode.equalsIgnoreCase("PolicyComId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyComId = FValue.trim();
			}
			else
				PolicyComId = null;
		}
		if (FCode.equalsIgnoreCase("PolicyPremium"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PolicyPremium = d;
			}
		}
		if (FCode.equalsIgnoreCase("PolicySystem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicySystem = FValue.trim();
			}
			else
				PolicySystem = null;
		}
		if (FCode.equalsIgnoreCase("MainRiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainRiskCode = FValue.trim();
			}
			else
				MainRiskCode = null;
		}
		if (FCode.equalsIgnoreCase("MainRiskName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainRiskName = FValue.trim();
			}
			else
				MainRiskName = null;
		}
		if (FCode.equalsIgnoreCase("MainPremium"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MainPremium = d;
			}
		}
		if (FCode.equalsIgnoreCase("SubRiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubRiskCode = FValue.trim();
			}
			else
				SubRiskCode = null;
		}
		if (FCode.equalsIgnoreCase("SubRiskName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubRiskName = FValue.trim();
			}
			else
				SubRiskName = null;
		}
		if (FCode.equalsIgnoreCase("SubPremium"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SubPremium = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("CostRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CostRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("PaydocFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PaydocFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("CostFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CostFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F1 = FValue.trim();
			}
			else
				F1 = null;
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F2 = FValue.trim();
			}
			else
				F2 = null;
		}
		if (FCode.equalsIgnoreCase("F3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F3 = FValue.trim();
			}
			else
				F3 = null;
		}
		if (FCode.equalsIgnoreCase("F4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F4 = FValue.trim();
			}
			else
				F4 = null;
		}
		if (FCode.equalsIgnoreCase("F5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F5 = FValue.trim();
			}
			else
				F5 = null;
		}
		if (FCode.equalsIgnoreCase("F6"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F6 = fDate.getDate( FValue );
			}
			else
				F6 = null;
		}
		if (FCode.equalsIgnoreCase("F7"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F7 = fDate.getDate( FValue );
			}
			else
				F7 = null;
		}
		if (FCode.equalsIgnoreCase("F8"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				F8 = fDate.getDate( FValue );
			}
			else
				F8 = null;
		}
		if (FCode.equalsIgnoreCase("F9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F9 = FValue.trim();
			}
			else
				F9 = null;
		}
		if (FCode.equalsIgnoreCase("F0"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F0 = FValue.trim();
			}
			else
				F0 = null;
		}
		if (FCode.equalsIgnoreCase("C1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("C2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("C3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				C3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("R3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				R3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("PerCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PerCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReMainCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReMainCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("F11"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F11 = FValue.trim();
			}
			else
				F11 = null;
		}
		if (FCode.equalsIgnoreCase("F12"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F12 = FValue.trim();
			}
			else
				F12 = null;
		}
		if (FCode.equalsIgnoreCase("F13"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F13 = FValue.trim();
			}
			else
				F13 = null;
		}
		if (FCode.equalsIgnoreCase("F14"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F14 = FValue.trim();
			}
			else
				F14 = null;
		}
		if (FCode.equalsIgnoreCase("F15"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F15 = FValue.trim();
			}
			else
				F15 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAInterChargeSchema other = (LAInterChargeSchema)otherObject;
		return
			(InterActNo == null ? other.getInterActNo() == null : InterActNo.equals(other.getInterActNo()))
			&& (PolicyNo == null ? other.getPolicyNo() == null : PolicyNo.equals(other.getPolicyNo()))
			&& (Usercode == null ? other.getUsercode() == null : Usercode.equals(other.getUsercode()))
			&& (UserName == null ? other.getUserName() == null : UserName.equals(other.getUserName()))
			&& (UserComId == null ? other.getUserComId() == null : UserComId.equals(other.getUserComId()))
			&& (CrossSellUserCode == null ? other.getCrossSellUserCode() == null : CrossSellUserCode.equals(other.getCrossSellUserCode()))
			&& (CrossSellComCode == null ? other.getCrossSellComCode() == null : CrossSellComCode.equals(other.getCrossSellComCode()))
			&& (CrossSellAgentCode == null ? other.getCrossSellAgentCode() == null : CrossSellAgentCode.equals(other.getCrossSellAgentCode()))
			&& (CrossSellStatus == null ? other.getCrossSellStatus() == null : CrossSellStatus.equals(other.getCrossSellStatus()))
			&& (HandlerCode == null ? other.getHandlerCode() == null : HandlerCode.equals(other.getHandlerCode()))
			&& (HandlerComCode == null ? other.getHandlerComCode() == null : HandlerComCode.equals(other.getHandlerComCode()))
			&& (Handler1Code == null ? other.getHandler1Code() == null : Handler1Code.equals(other.getHandler1Code()))
			&& (Handler1ComCode == null ? other.getHandler1ComCode() == null : Handler1ComCode.equals(other.getHandler1ComCode()))
			&& (MakeCode == null ? other.getMakeCode() == null : MakeCode.equals(other.getMakeCode()))
			&& (MakeComCode == null ? other.getMakeComCode() == null : MakeComCode.equals(other.getMakeComCode()))
			&& (CommissionExtractionStatus == null ? other.getCommissionExtractionStatus() == null : CommissionExtractionStatus.equals(other.getCommissionExtractionStatus()))
			&& CommissionAmount == other.getCommissionAmount()
			&& (CommissionNonPaymentDesc == null ? other.getCommissionNonPaymentDesc() == null : CommissionNonPaymentDesc.equals(other.getCommissionNonPaymentDesc()))
			&& (PubToPubPaymentNo == null ? other.getPubToPubPaymentNo() == null : PubToPubPaymentNo.equals(other.getPubToPubPaymentNo()))
			&& (PolicyComId == null ? other.getPolicyComId() == null : PolicyComId.equals(other.getPolicyComId()))
			&& PolicyPremium == other.getPolicyPremium()
			&& (PolicySystem == null ? other.getPolicySystem() == null : PolicySystem.equals(other.getPolicySystem()))
			&& (MainRiskCode == null ? other.getMainRiskCode() == null : MainRiskCode.equals(other.getMainRiskCode()))
			&& (MainRiskName == null ? other.getMainRiskName() == null : MainRiskName.equals(other.getMainRiskName()))
			&& MainPremium == other.getMainPremium()
			&& (SubRiskCode == null ? other.getSubRiskCode() == null : SubRiskCode.equals(other.getSubRiskCode()))
			&& (SubRiskName == null ? other.getSubRiskName() == null : SubRiskName.equals(other.getSubRiskName()))
			&& SubPremium == other.getSubPremium()
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& CostRate == other.getCostRate()
			&& PaydocFee == other.getPaydocFee()
			&& CostFee == other.getCostFee()
			&& (F1 == null ? other.getF1() == null : F1.equals(other.getF1()))
			&& (F2 == null ? other.getF2() == null : F2.equals(other.getF2()))
			&& (F3 == null ? other.getF3() == null : F3.equals(other.getF3()))
			&& (F4 == null ? other.getF4() == null : F4.equals(other.getF4()))
			&& (F5 == null ? other.getF5() == null : F5.equals(other.getF5()))
			&& (F6 == null ? other.getF6() == null : fDate.getString(F6).equals(other.getF6()))
			&& (F7 == null ? other.getF7() == null : fDate.getString(F7).equals(other.getF7()))
			&& (F8 == null ? other.getF8() == null : fDate.getString(F8).equals(other.getF8()))
			&& (F9 == null ? other.getF9() == null : F9.equals(other.getF9()))
			&& (F0 == null ? other.getF0() == null : F0.equals(other.getF0()))
			&& C1 == other.getC1()
			&& C2 == other.getC2()
			&& C3 == other.getC3()
			&& R1 == other.getR1()
			&& R2 == other.getR2()
			&& R3 == other.getR3()
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& PerCharge == other.getPerCharge()
			&& ReMainCharge == other.getReMainCharge()
			&& (F11 == null ? other.getF11() == null : F11.equals(other.getF11()))
			&& (F12 == null ? other.getF12() == null : F12.equals(other.getF12()))
			&& (F13 == null ? other.getF13() == null : F13.equals(other.getF13()))
			&& (F14 == null ? other.getF14() == null : F14.equals(other.getF14()))
			&& (F15 == null ? other.getF15() == null : F15.equals(other.getF15()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("InterActNo") ) {
			return 0;
		}
		if( strFieldName.equals("PolicyNo") ) {
			return 1;
		}
		if( strFieldName.equals("Usercode") ) {
			return 2;
		}
		if( strFieldName.equals("UserName") ) {
			return 3;
		}
		if( strFieldName.equals("UserComId") ) {
			return 4;
		}
		if( strFieldName.equals("CrossSellUserCode") ) {
			return 5;
		}
		if( strFieldName.equals("CrossSellComCode") ) {
			return 6;
		}
		if( strFieldName.equals("CrossSellAgentCode") ) {
			return 7;
		}
		if( strFieldName.equals("CrossSellStatus") ) {
			return 8;
		}
		if( strFieldName.equals("HandlerCode") ) {
			return 9;
		}
		if( strFieldName.equals("HandlerComCode") ) {
			return 10;
		}
		if( strFieldName.equals("Handler1Code") ) {
			return 11;
		}
		if( strFieldName.equals("Handler1ComCode") ) {
			return 12;
		}
		if( strFieldName.equals("MakeCode") ) {
			return 13;
		}
		if( strFieldName.equals("MakeComCode") ) {
			return 14;
		}
		if( strFieldName.equals("CommissionExtractionStatus") ) {
			return 15;
		}
		if( strFieldName.equals("CommissionAmount") ) {
			return 16;
		}
		if( strFieldName.equals("CommissionNonPaymentDesc") ) {
			return 17;
		}
		if( strFieldName.equals("PubToPubPaymentNo") ) {
			return 18;
		}
		if( strFieldName.equals("PolicyComId") ) {
			return 19;
		}
		if( strFieldName.equals("PolicyPremium") ) {
			return 20;
		}
		if( strFieldName.equals("PolicySystem") ) {
			return 21;
		}
		if( strFieldName.equals("MainRiskCode") ) {
			return 22;
		}
		if( strFieldName.equals("MainRiskName") ) {
			return 23;
		}
		if( strFieldName.equals("MainPremium") ) {
			return 24;
		}
		if( strFieldName.equals("SubRiskCode") ) {
			return 25;
		}
		if( strFieldName.equals("SubRiskName") ) {
			return 26;
		}
		if( strFieldName.equals("SubPremium") ) {
			return 27;
		}
		if( strFieldName.equals("Remark") ) {
			return 28;
		}
		if( strFieldName.equals("CostRate") ) {
			return 29;
		}
		if( strFieldName.equals("PaydocFee") ) {
			return 30;
		}
		if( strFieldName.equals("CostFee") ) {
			return 31;
		}
		if( strFieldName.equals("F1") ) {
			return 32;
		}
		if( strFieldName.equals("F2") ) {
			return 33;
		}
		if( strFieldName.equals("F3") ) {
			return 34;
		}
		if( strFieldName.equals("F4") ) {
			return 35;
		}
		if( strFieldName.equals("F5") ) {
			return 36;
		}
		if( strFieldName.equals("F6") ) {
			return 37;
		}
		if( strFieldName.equals("F7") ) {
			return 38;
		}
		if( strFieldName.equals("F8") ) {
			return 39;
		}
		if( strFieldName.equals("F9") ) {
			return 40;
		}
		if( strFieldName.equals("F0") ) {
			return 41;
		}
		if( strFieldName.equals("C1") ) {
			return 42;
		}
		if( strFieldName.equals("C2") ) {
			return 43;
		}
		if( strFieldName.equals("C3") ) {
			return 44;
		}
		if( strFieldName.equals("R1") ) {
			return 45;
		}
		if( strFieldName.equals("R2") ) {
			return 46;
		}
		if( strFieldName.equals("R3") ) {
			return 47;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 48;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 49;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 50;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 51;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 52;
		}
		if( strFieldName.equals("PerCharge") ) {
			return 53;
		}
		if( strFieldName.equals("ReMainCharge") ) {
			return 54;
		}
		if( strFieldName.equals("F11") ) {
			return 55;
		}
		if( strFieldName.equals("F12") ) {
			return 56;
		}
		if( strFieldName.equals("F13") ) {
			return 57;
		}
		if( strFieldName.equals("F14") ) {
			return 58;
		}
		if( strFieldName.equals("F15") ) {
			return 59;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "InterActNo";
				break;
			case 1:
				strFieldName = "PolicyNo";
				break;
			case 2:
				strFieldName = "Usercode";
				break;
			case 3:
				strFieldName = "UserName";
				break;
			case 4:
				strFieldName = "UserComId";
				break;
			case 5:
				strFieldName = "CrossSellUserCode";
				break;
			case 6:
				strFieldName = "CrossSellComCode";
				break;
			case 7:
				strFieldName = "CrossSellAgentCode";
				break;
			case 8:
				strFieldName = "CrossSellStatus";
				break;
			case 9:
				strFieldName = "HandlerCode";
				break;
			case 10:
				strFieldName = "HandlerComCode";
				break;
			case 11:
				strFieldName = "Handler1Code";
				break;
			case 12:
				strFieldName = "Handler1ComCode";
				break;
			case 13:
				strFieldName = "MakeCode";
				break;
			case 14:
				strFieldName = "MakeComCode";
				break;
			case 15:
				strFieldName = "CommissionExtractionStatus";
				break;
			case 16:
				strFieldName = "CommissionAmount";
				break;
			case 17:
				strFieldName = "CommissionNonPaymentDesc";
				break;
			case 18:
				strFieldName = "PubToPubPaymentNo";
				break;
			case 19:
				strFieldName = "PolicyComId";
				break;
			case 20:
				strFieldName = "PolicyPremium";
				break;
			case 21:
				strFieldName = "PolicySystem";
				break;
			case 22:
				strFieldName = "MainRiskCode";
				break;
			case 23:
				strFieldName = "MainRiskName";
				break;
			case 24:
				strFieldName = "MainPremium";
				break;
			case 25:
				strFieldName = "SubRiskCode";
				break;
			case 26:
				strFieldName = "SubRiskName";
				break;
			case 27:
				strFieldName = "SubPremium";
				break;
			case 28:
				strFieldName = "Remark";
				break;
			case 29:
				strFieldName = "CostRate";
				break;
			case 30:
				strFieldName = "PaydocFee";
				break;
			case 31:
				strFieldName = "CostFee";
				break;
			case 32:
				strFieldName = "F1";
				break;
			case 33:
				strFieldName = "F2";
				break;
			case 34:
				strFieldName = "F3";
				break;
			case 35:
				strFieldName = "F4";
				break;
			case 36:
				strFieldName = "F5";
				break;
			case 37:
				strFieldName = "F6";
				break;
			case 38:
				strFieldName = "F7";
				break;
			case 39:
				strFieldName = "F8";
				break;
			case 40:
				strFieldName = "F9";
				break;
			case 41:
				strFieldName = "F0";
				break;
			case 42:
				strFieldName = "C1";
				break;
			case 43:
				strFieldName = "C2";
				break;
			case 44:
				strFieldName = "C3";
				break;
			case 45:
				strFieldName = "R1";
				break;
			case 46:
				strFieldName = "R2";
				break;
			case 47:
				strFieldName = "R3";
				break;
			case 48:
				strFieldName = "MakeDate";
				break;
			case 49:
				strFieldName = "MakeTime";
				break;
			case 50:
				strFieldName = "ModifyDate";
				break;
			case 51:
				strFieldName = "AgentCode";
				break;
			case 52:
				strFieldName = "ModifyTime";
				break;
			case 53:
				strFieldName = "PerCharge";
				break;
			case 54:
				strFieldName = "ReMainCharge";
				break;
			case 55:
				strFieldName = "F11";
				break;
			case 56:
				strFieldName = "F12";
				break;
			case 57:
				strFieldName = "F13";
				break;
			case 58:
				strFieldName = "F14";
				break;
			case 59:
				strFieldName = "F15";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("InterActNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Usercode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UserName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UserComId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CrossSellUserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CrossSellComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CrossSellAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CrossSellStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler1Code") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler1ComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CommissionExtractionStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CommissionAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CommissionNonPaymentDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PubToPubPaymentNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyComId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyPremium") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PolicySystem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainRiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainRiskName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainPremium") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SubRiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubRiskName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubPremium") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PaydocFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CostFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F6") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F7") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F8") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("F9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F0") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("C1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("C2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("C3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("R3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PerCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReMainCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F11") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F12") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F13") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F14") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F15") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 43:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 45:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 47:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 48:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 54:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
