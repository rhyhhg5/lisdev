/*
 * <p>ClassName: LOBonusPolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBonusPolDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOBonusPolSchema implements Schema
{
    // @Field
    /** 保单号码 */
    private String PolNo;
    /** 会计年度 */
    private int FiscalYear;
    /** 红利分配组号 */
    private int GroupID;
    /** 总单/合同号码 */
    private String ContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 红利金额 */
    private double BonusMoney;
    /** 红利利息 */
    private double BonusInterest;
    /** 红利领取标志 */
    private String BonusFlag;
    /** 红利计算日期 */
    private Date BonusMakeDate;
    /** 红利应该分配日期 */
    private Date SGetDate;
    /** 红利实际分配日期 */
    private Date AGetDate;
    /** 该保单红利结算时刻的红利系数和 */
    private double BonusCoef;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBonusPolSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "PolNo";
        pk[1] = "FiscalYear";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public int getFiscalYear()
    {
        return FiscalYear;
    }

    public void setFiscalYear(int aFiscalYear)
    {
        FiscalYear = aFiscalYear;
    }

    public void setFiscalYear(String aFiscalYear)
    {
        if (aFiscalYear != null && !aFiscalYear.equals(""))
        {
            Integer tInteger = new Integer(aFiscalYear);
            int i = tInteger.intValue();
            FiscalYear = i;
        }
    }

    public int getGroupID()
    {
        return GroupID;
    }

    public void setGroupID(int aGroupID)
    {
        GroupID = aGroupID;
    }

    public void setGroupID(String aGroupID)
    {
        if (aGroupID != null && !aGroupID.equals(""))
        {
            Integer tInteger = new Integer(aGroupID);
            int i = tInteger.intValue();
            GroupID = i;
        }
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public double getBonusMoney()
    {
        return BonusMoney;
    }

    public void setBonusMoney(double aBonusMoney)
    {
        BonusMoney = aBonusMoney;
    }

    public void setBonusMoney(String aBonusMoney)
    {
        if (aBonusMoney != null && !aBonusMoney.equals(""))
        {
            Double tDouble = new Double(aBonusMoney);
            double d = tDouble.doubleValue();
            BonusMoney = d;
        }
    }

    public double getBonusInterest()
    {
        return BonusInterest;
    }

    public void setBonusInterest(double aBonusInterest)
    {
        BonusInterest = aBonusInterest;
    }

    public void setBonusInterest(String aBonusInterest)
    {
        if (aBonusInterest != null && !aBonusInterest.equals(""))
        {
            Double tDouble = new Double(aBonusInterest);
            double d = tDouble.doubleValue();
            BonusInterest = d;
        }
    }

    public String getBonusFlag()
    {
        if (BonusFlag != null && !BonusFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BonusFlag = StrTool.unicodeToGBK(BonusFlag);
        }
        return BonusFlag;
    }

    public void setBonusFlag(String aBonusFlag)
    {
        BonusFlag = aBonusFlag;
    }

    public String getBonusMakeDate()
    {
        if (BonusMakeDate != null)
        {
            return fDate.getString(BonusMakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setBonusMakeDate(Date aBonusMakeDate)
    {
        BonusMakeDate = aBonusMakeDate;
    }

    public void setBonusMakeDate(String aBonusMakeDate)
    {
        if (aBonusMakeDate != null && !aBonusMakeDate.equals(""))
        {
            BonusMakeDate = fDate.getDate(aBonusMakeDate);
        }
        else
        {
            BonusMakeDate = null;
        }
    }

    public String getSGetDate()
    {
        if (SGetDate != null)
        {
            return fDate.getString(SGetDate);
        }
        else
        {
            return null;
        }
    }

    public void setSGetDate(Date aSGetDate)
    {
        SGetDate = aSGetDate;
    }

    public void setSGetDate(String aSGetDate)
    {
        if (aSGetDate != null && !aSGetDate.equals(""))
        {
            SGetDate = fDate.getDate(aSGetDate);
        }
        else
        {
            SGetDate = null;
        }
    }

    public String getAGetDate()
    {
        if (AGetDate != null)
        {
            return fDate.getString(AGetDate);
        }
        else
        {
            return null;
        }
    }

    public void setAGetDate(Date aAGetDate)
    {
        AGetDate = aAGetDate;
    }

    public void setAGetDate(String aAGetDate)
    {
        if (aAGetDate != null && !aAGetDate.equals(""))
        {
            AGetDate = fDate.getDate(aAGetDate);
        }
        else
        {
            AGetDate = null;
        }
    }

    public double getBonusCoef()
    {
        return BonusCoef;
    }

    public void setBonusCoef(double aBonusCoef)
    {
        BonusCoef = aBonusCoef;
    }

    public void setBonusCoef(String aBonusCoef)
    {
        if (aBonusCoef != null && !aBonusCoef.equals(""))
        {
            Double tDouble = new Double(aBonusCoef);
            double d = tDouble.doubleValue();
            BonusCoef = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOBonusPolSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOBonusPolSchema aLOBonusPolSchema)
    {
        this.PolNo = aLOBonusPolSchema.getPolNo();
        this.FiscalYear = aLOBonusPolSchema.getFiscalYear();
        this.GroupID = aLOBonusPolSchema.getGroupID();
        this.ContNo = aLOBonusPolSchema.getContNo();
        this.GrpPolNo = aLOBonusPolSchema.getGrpPolNo();
        this.BonusMoney = aLOBonusPolSchema.getBonusMoney();
        this.BonusInterest = aLOBonusPolSchema.getBonusInterest();
        this.BonusFlag = aLOBonusPolSchema.getBonusFlag();
        this.BonusMakeDate = fDate.getDate(aLOBonusPolSchema.getBonusMakeDate());
        this.SGetDate = fDate.getDate(aLOBonusPolSchema.getSGetDate());
        this.AGetDate = fDate.getDate(aLOBonusPolSchema.getAGetDate());
        this.BonusCoef = aLOBonusPolSchema.getBonusCoef();
        this.Operator = aLOBonusPolSchema.getOperator();
        this.MakeDate = fDate.getDate(aLOBonusPolSchema.getMakeDate());
        this.MakeTime = aLOBonusPolSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBonusPolSchema.getModifyDate());
        this.ModifyTime = aLOBonusPolSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            this.FiscalYear = rs.getInt("FiscalYear");
            this.GroupID = rs.getInt("GroupID");
            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            this.BonusMoney = rs.getDouble("BonusMoney");
            this.BonusInterest = rs.getDouble("BonusInterest");
            if (rs.getString("BonusFlag") == null)
            {
                this.BonusFlag = null;
            }
            else
            {
                this.BonusFlag = rs.getString("BonusFlag").trim();
            }

            this.BonusMakeDate = rs.getDate("BonusMakeDate");
            this.SGetDate = rs.getDate("SGetDate");
            this.AGetDate = rs.getDate("AGetDate");
            this.BonusCoef = rs.getDouble("BonusCoef");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBonusPolSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOBonusPolSchema getSchema()
    {
        LOBonusPolSchema aLOBonusPolSchema = new LOBonusPolSchema();
        aLOBonusPolSchema.setSchema(this);
        return aLOBonusPolSchema;
    }

    public LOBonusPolDB getDB()
    {
        LOBonusPolDB aDBOper = new LOBonusPolDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBonusPol描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FiscalYear) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GroupID) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(BonusMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BonusInterest) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BonusFlag)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            BonusMakeDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(SGetDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(AGetDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(BonusCoef) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBonusPol>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            FiscalYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            GroupID = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            BonusMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            BonusInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            BonusFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            BonusMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            SGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            AGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            BonusCoef = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBonusPolSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("FiscalYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FiscalYear));
        }
        if (FCode.equals("GroupID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GroupID));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("BonusMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BonusMoney));
        }
        if (FCode.equals("BonusInterest"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BonusInterest));
        }
        if (FCode.equals("BonusFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BonusFlag));
        }
        if (FCode.equals("BonusMakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getBonusMakeDate()));
        }
        if (FCode.equals("SGetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getSGetDate()));
        }
        if (FCode.equals("AGetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getAGetDate()));
        }
        if (FCode.equals("BonusCoef"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BonusCoef));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 1:
                strFieldValue = String.valueOf(FiscalYear);
                break;
            case 2:
                strFieldValue = String.valueOf(GroupID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 5:
                strFieldValue = String.valueOf(BonusMoney);
                break;
            case 6:
                strFieldValue = String.valueOf(BonusInterest);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(BonusFlag);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBonusMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSGetDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getAGetDate()));
                break;
            case 11:
                strFieldValue = String.valueOf(BonusCoef);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("FiscalYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                FiscalYear = i;
            }
        }
        if (FCode.equals("GroupID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GroupID = i;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("BonusMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BonusMoney = d;
            }
        }
        if (FCode.equals("BonusInterest"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BonusInterest = d;
            }
        }
        if (FCode.equals("BonusFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BonusFlag = FValue.trim();
            }
            else
            {
                BonusFlag = null;
            }
        }
        if (FCode.equals("BonusMakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BonusMakeDate = fDate.getDate(FValue);
            }
            else
            {
                BonusMakeDate = null;
            }
        }
        if (FCode.equals("SGetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SGetDate = fDate.getDate(FValue);
            }
            else
            {
                SGetDate = null;
            }
        }
        if (FCode.equals("AGetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AGetDate = fDate.getDate(FValue);
            }
            else
            {
                AGetDate = null;
            }
        }
        if (FCode.equals("BonusCoef"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BonusCoef = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBonusPolSchema other = (LOBonusPolSchema) otherObject;
        return
                PolNo.equals(other.getPolNo())
                && FiscalYear == other.getFiscalYear()
                && GroupID == other.getGroupID()
                && ContNo.equals(other.getContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && BonusMoney == other.getBonusMoney()
                && BonusInterest == other.getBonusInterest()
                && BonusFlag.equals(other.getBonusFlag())
                && fDate.getString(BonusMakeDate).equals(other.getBonusMakeDate())
                && fDate.getString(SGetDate).equals(other.getSGetDate())
                && fDate.getString(AGetDate).equals(other.getAGetDate())
                && BonusCoef == other.getBonusCoef()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PolNo"))
        {
            return 0;
        }
        if (strFieldName.equals("FiscalYear"))
        {
            return 1;
        }
        if (strFieldName.equals("GroupID"))
        {
            return 2;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 3;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 4;
        }
        if (strFieldName.equals("BonusMoney"))
        {
            return 5;
        }
        if (strFieldName.equals("BonusInterest"))
        {
            return 6;
        }
        if (strFieldName.equals("BonusFlag"))
        {
            return 7;
        }
        if (strFieldName.equals("BonusMakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("SGetDate"))
        {
            return 9;
        }
        if (strFieldName.equals("AGetDate"))
        {
            return 10;
        }
        if (strFieldName.equals("BonusCoef"))
        {
            return 11;
        }
        if (strFieldName.equals("Operator"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PolNo";
                break;
            case 1:
                strFieldName = "FiscalYear";
                break;
            case 2:
                strFieldName = "GroupID";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "GrpPolNo";
                break;
            case 5:
                strFieldName = "BonusMoney";
                break;
            case 6:
                strFieldName = "BonusInterest";
                break;
            case 7:
                strFieldName = "BonusFlag";
                break;
            case 8:
                strFieldName = "BonusMakeDate";
                break;
            case 9:
                strFieldName = "SGetDate";
                break;
            case 10:
                strFieldName = "AGetDate";
                break;
            case 11:
                strFieldName = "BonusCoef";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FiscalYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GroupID"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BonusMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BonusInterest"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BonusFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BonusMakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SGetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AGetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BonusCoef"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
