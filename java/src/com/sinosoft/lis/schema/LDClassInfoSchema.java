/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDClassInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDClassInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 医疗服务机构信息
 * @CreateDate：2005-05-04
 */
public class LDClassInfoSchema implements Schema, Cloneable
{
    // @Field
    /** 分类信息代码 */
    private String ClassInfoCode;
    /** 类别等级 */
    private String ClassLevel;
    /** 类别代码 */
    private String ClassCode;
    /** 类别名称 */
    private String ClassName;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDClassInfoSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ClassCode";
        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LDClassInfoSchema cloned = (LDClassInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getClassInfoCode()
    {
        if (SysConst.CHANGECHARSET && ClassInfoCode != null &&
            !ClassInfoCode.equals(""))
        {
            ClassInfoCode = StrTool.unicodeToGBK(ClassInfoCode);
        }
        return ClassInfoCode;
    }

    public void setClassInfoCode(String aClassInfoCode)
    {
        ClassInfoCode = aClassInfoCode;
    }

    public String getClassLevel()
    {
        if (SysConst.CHANGECHARSET && ClassLevel != null &&
            !ClassLevel.equals(""))
        {
            ClassLevel = StrTool.unicodeToGBK(ClassLevel);
        }
        return ClassLevel;
    }

    public void setClassLevel(String aClassLevel)
    {
        ClassLevel = aClassLevel;
    }

    public String getClassCode()
    {
        if (SysConst.CHANGECHARSET && ClassCode != null && !ClassCode.equals(""))
        {
            ClassCode = StrTool.unicodeToGBK(ClassCode);
        }
        return ClassCode;
    }

    public void setClassCode(String aClassCode)
    {
        ClassCode = aClassCode;
    }

    public String getClassName()
    {
        if (SysConst.CHANGECHARSET && ClassName != null && !ClassName.equals(""))
        {
            ClassName = StrTool.unicodeToGBK(ClassName);
        }
        return ClassName;
    }

    public void setClassName(String aClassName)
    {
        ClassName = aClassName;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LDClassInfoSchema 对象给 Schema 赋值
     * @param: aLDClassInfoSchema LDClassInfoSchema
     **/
    public void setSchema(LDClassInfoSchema aLDClassInfoSchema)
    {
        this.ClassInfoCode = aLDClassInfoSchema.getClassInfoCode();
        this.ClassLevel = aLDClassInfoSchema.getClassLevel();
        this.ClassCode = aLDClassInfoSchema.getClassCode();
        this.ClassName = aLDClassInfoSchema.getClassName();
        this.Operator = aLDClassInfoSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDClassInfoSchema.getMakeDate());
        this.MakeTime = aLDClassInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDClassInfoSchema.getModifyDate());
        this.ModifyTime = aLDClassInfoSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ClassInfoCode") == null)
            {
                this.ClassInfoCode = null;
            }
            else
            {
                this.ClassInfoCode = rs.getString("ClassInfoCode").trim();
            }

            if (rs.getString("ClassLevel") == null)
            {
                this.ClassLevel = null;
            }
            else
            {
                this.ClassLevel = rs.getString("ClassLevel").trim();
            }

            if (rs.getString("ClassCode") == null)
            {
                this.ClassCode = null;
            }
            else
            {
                this.ClassCode = rs.getString("ClassCode").trim();
            }

            if (rs.getString("ClassName") == null)
            {
                this.ClassName = null;
            }
            else
            {
                this.ClassName = rs.getString("ClassName").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LDClassInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDClassInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDClassInfoSchema getSchema()
    {
        LDClassInfoSchema aLDClassInfoSchema = new LDClassInfoSchema();
        aLDClassInfoSchema.setSchema(this);
        return aLDClassInfoSchema;
    }

    public LDClassInfoDB getDB()
    {
        LDClassInfoDB aDBOper = new LDClassInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDClassInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClassInfoCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClassLevel)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClassCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClassName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDClassInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ClassInfoCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            ClassLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ClassCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            ClassName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDClassInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ClassInfoCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassInfoCode));
        }
        if (FCode.equals("ClassLevel"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassLevel));
        }
        if (FCode.equals("ClassCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassCode));
        }
        if (FCode.equals("ClassName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassName));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ClassInfoCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ClassLevel);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ClassCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ClassName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ClassInfoCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClassInfoCode = FValue.trim();
            }
            else
            {
                ClassInfoCode = null;
            }
        }
        if (FCode.equals("ClassLevel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClassLevel = FValue.trim();
            }
            else
            {
                ClassLevel = null;
            }
        }
        if (FCode.equals("ClassCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClassCode = FValue.trim();
            }
            else
            {
                ClassCode = null;
            }
        }
        if (FCode.equals("ClassName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClassName = FValue.trim();
            }
            else
            {
                ClassName = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDClassInfoSchema other = (LDClassInfoSchema) otherObject;
        return
                ClassInfoCode.equals(other.getClassInfoCode())
                && ClassLevel.equals(other.getClassLevel())
                && ClassCode.equals(other.getClassCode())
                && ClassName.equals(other.getClassName())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ClassInfoCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ClassLevel"))
        {
            return 1;
        }
        if (strFieldName.equals("ClassCode"))
        {
            return 2;
        }
        if (strFieldName.equals("ClassName"))
        {
            return 3;
        }
        if (strFieldName.equals("Operator"))
        {
            return 4;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 6;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ClassInfoCode";
                break;
            case 1:
                strFieldName = "ClassLevel";
                break;
            case 2:
                strFieldName = "ClassCode";
                break;
            case 3:
                strFieldName = "ClassName";
                break;
            case 4:
                strFieldName = "Operator";
                break;
            case 5:
                strFieldName = "MakeDate";
                break;
            case 6:
                strFieldName = "MakeTime";
                break;
            case 7:
                strFieldName = "ModifyDate";
                break;
            case 8:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ClassInfoCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClassLevel"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClassCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClassName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
