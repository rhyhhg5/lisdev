/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDServInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDServInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 服务描述表
 * @CreateDate：2005-03-22
 */
public class LDServInfoSchema implements Schema
{
    // @Field
    /** 业务类型 */
    private String KindCode;
    /** 服务类型 */
    private String ServKind;
    /** 服务明细 */
    private String ServDetail;
    /** 服务选择值 */
    private String ServChoose;
    /** 服务选择值说明 */
    private String ServChooseRemark;
    /** 服务备注 */
    private String ServRemark;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDServInfoSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "KindCode";
        pk[1] = "ServKind";
        pk[2] = "ServDetail";
        pk[3] = "ServChoose";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getKindCode()
    {
        if (KindCode != null && !KindCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            KindCode = StrTool.unicodeToGBK(KindCode);
        }
        return KindCode;
    }

    public void setKindCode(String aKindCode)
    {
        KindCode = aKindCode;
    }

    public String getServKind()
    {
        if (ServKind != null && !ServKind.equals("") && SysConst.CHANGECHARSET == true)
        {
            ServKind = StrTool.unicodeToGBK(ServKind);
        }
        return ServKind;
    }

    public void setServKind(String aServKind)
    {
        ServKind = aServKind;
    }

    public String getServDetail()
    {
        if (ServDetail != null && !ServDetail.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ServDetail = StrTool.unicodeToGBK(ServDetail);
        }
        return ServDetail;
    }

    public void setServDetail(String aServDetail)
    {
        ServDetail = aServDetail;
    }

    public String getServChoose()
    {
        if (ServChoose != null && !ServChoose.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ServChoose = StrTool.unicodeToGBK(ServChoose);
        }
        return ServChoose;
    }

    public void setServChoose(String aServChoose)
    {
        ServChoose = aServChoose;
    }

    public String getServChooseRemark()
    {
        if (ServChooseRemark != null && !ServChooseRemark.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ServChooseRemark = StrTool.unicodeToGBK(ServChooseRemark);
        }
        return ServChooseRemark;
    }

    public void setServChooseRemark(String aServChooseRemark)
    {
        ServChooseRemark = aServChooseRemark;
    }

    public String getServRemark()
    {
        if (ServRemark != null && !ServRemark.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ServRemark = StrTool.unicodeToGBK(ServRemark);
        }
        return ServRemark;
    }

    public void setServRemark(String aServRemark)
    {
        ServRemark = aServRemark;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LDServInfoSchema 对象给 Schema 赋值
     * @param: aLDServInfoSchema LDServInfoSchema
     **/
    public void setSchema(LDServInfoSchema aLDServInfoSchema)
    {
        this.KindCode = aLDServInfoSchema.getKindCode();
        this.ServKind = aLDServInfoSchema.getServKind();
        this.ServDetail = aLDServInfoSchema.getServDetail();
        this.ServChoose = aLDServInfoSchema.getServChoose();
        this.ServChooseRemark = aLDServInfoSchema.getServChooseRemark();
        this.ServRemark = aLDServInfoSchema.getServRemark();
        this.MakeDate = fDate.getDate(aLDServInfoSchema.getMakeDate());
        this.MakeTime = aLDServInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDServInfoSchema.getModifyDate());
        this.ModifyTime = aLDServInfoSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("KindCode") == null)
            {
                this.KindCode = null;
            }
            else
            {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("ServKind") == null)
            {
                this.ServKind = null;
            }
            else
            {
                this.ServKind = rs.getString("ServKind").trim();
            }

            if (rs.getString("ServDetail") == null)
            {
                this.ServDetail = null;
            }
            else
            {
                this.ServDetail = rs.getString("ServDetail").trim();
            }

            if (rs.getString("ServChoose") == null)
            {
                this.ServChoose = null;
            }
            else
            {
                this.ServChoose = rs.getString("ServChoose").trim();
            }

            if (rs.getString("ServChooseRemark") == null)
            {
                this.ServChooseRemark = null;
            }
            else
            {
                this.ServChooseRemark = rs.getString("ServChooseRemark").trim();
            }

            if (rs.getString("ServRemark") == null)
            {
                this.ServRemark = null;
            }
            else
            {
                this.ServRemark = rs.getString("ServRemark").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDServInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDServInfoSchema getSchema()
    {
        LDServInfoSchema aLDServInfoSchema = new LDServInfoSchema();
        aLDServInfoSchema.setSchema(this);
        return aLDServInfoSchema;
    }

    public LDServInfoDB getDB()
    {
        LDServInfoDB aDBOper = new LDServInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDServInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(KindCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ServKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ServDetail)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ServChoose)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ServChooseRemark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ServRemark)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDServInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ServKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            ServDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            ServChoose = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            ServChooseRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              5, SysConst.PACKAGESPILTER);
            ServRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDServInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("KindCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equals("ServKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServKind));
        }
        if (FCode.equals("ServDetail"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServDetail));
        }
        if (FCode.equals("ServChoose"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServChoose));
        }
        if (FCode.equals("ServChooseRemark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServChooseRemark));
        }
        if (FCode.equals("ServRemark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServRemark));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ServKind);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ServDetail);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ServChoose);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ServChooseRemark);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ServRemark);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("KindCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
            {
                KindCode = null;
            }
        }
        if (FCode.equals("ServKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ServKind = FValue.trim();
            }
            else
            {
                ServKind = null;
            }
        }
        if (FCode.equals("ServDetail"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ServDetail = FValue.trim();
            }
            else
            {
                ServDetail = null;
            }
        }
        if (FCode.equals("ServChoose"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ServChoose = FValue.trim();
            }
            else
            {
                ServChoose = null;
            }
        }
        if (FCode.equals("ServChooseRemark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ServChooseRemark = FValue.trim();
            }
            else
            {
                ServChooseRemark = null;
            }
        }
        if (FCode.equals("ServRemark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ServRemark = FValue.trim();
            }
            else
            {
                ServRemark = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDServInfoSchema other = (LDServInfoSchema) otherObject;
        return
                KindCode.equals(other.getKindCode())
                && ServKind.equals(other.getServKind())
                && ServDetail.equals(other.getServDetail())
                && ServChoose.equals(other.getServChoose())
                && ServChooseRemark.equals(other.getServChooseRemark())
                && ServRemark.equals(other.getServRemark())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("KindCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ServKind"))
        {
            return 1;
        }
        if (strFieldName.equals("ServDetail"))
        {
            return 2;
        }
        if (strFieldName.equals("ServChoose"))
        {
            return 3;
        }
        if (strFieldName.equals("ServChooseRemark"))
        {
            return 4;
        }
        if (strFieldName.equals("ServRemark"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "KindCode";
                break;
            case 1:
                strFieldName = "ServKind";
                break;
            case 2:
                strFieldName = "ServDetail";
                break;
            case 3:
                strFieldName = "ServChoose";
                break;
            case 4:
                strFieldName = "ServChooseRemark";
                break;
            case 5:
                strFieldName = "ServRemark";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("KindCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServDetail"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServChoose"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServChooseRemark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServRemark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
