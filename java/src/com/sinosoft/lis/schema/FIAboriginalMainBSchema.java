/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIAboriginalMainBDB;

/*
 * <p>ClassName: FIAboriginalMainBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIAboriginalMainBSchema implements Schema, Cloneable
{
	// @Field
	/** 数据回滚任务号 */
	private String TaskNo;
	/** 物理表 */
	private String PhysicalTable;
	/** 主键信息 */
	private String KeyUnionValue;
	/** 主业务数据流水号 */
	private String MSerialNo;
	/** 业务索引类型 */
	private String IndexCode;
	/** 业务索引号码 */
	private String IndexNo;
	/** 其它号码类型 */
	private String OtherNoType;
	/** 其它业务号码 */
	private String OtherNo;
	/** 客户号码类型 */
	private String CustomerNoType;
	/** 客户号码 */
	private String CustomerNo;
	/** 供应商号码 */
	private String SupplierNo;
	/** 票据号/报盘批次号 */
	private String BillNo;
	/** 通知书号码 */
	private String GetNoticeNo;
	/** 个团标记 */
	private String ListFlag;
	/** 团体保单号 */
	private String GrpContNo;
	/** 个人保单号 */
	private String ContNo;
	/** 收付费方式 */
	private String PayMode;
	/** 保单签单日 */
	private Date SignDate;
	/** 保单生效日 */
	private Date CValiDate;
	/** 交费类型 */
	private String DueFeeType;
	/** 总金额 */
	private double SumActuMoney;
	/** 客户账户余额 */
	private double AccBala;
	/** 帐户现金余额 */
	private double InsuAccBala;
	/** 期初账户现金余额 */
	private double LastAccBala;
	/** 管理机构 */
	private String ManageCom;
	/** 处理机构 */
	private String ExecuteCom;
	/** 成本中心 */
	private String CostCenter;
	/** 中介机构类别 */
	private String ACType;
	/** 代理机构 */
	private String AgentCom;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 交费间隔标志 */
	private String PayIntvFlag;
	/** 交费间隔 */
	private int PayIntv;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐户名 */
	private String AccName;
	/** 银行帐号 */
	private String BankAccNo;
	/** 保险年期 */
	private int Years;
	/** 销售渠道 */
	private String SaleChnl;
	/** 渠道明细 */
	private String SaleChnlDetail;
	/** 市场类型 */
	private String MarketType;
	/** 续保收费标记 */
	private String PayTypeFlag;
	/** 月结日期 */
	private Date BalanceDate;
	/** 业务日期 */
	private Date AccountDate;
	/** 最早收费日期 */
	private Date StartPayDate;
	/** 应付日期 */
	private Date ShouldDate;
	/** 财务到帐日期 */
	private Date EnterAccDate;
	/** 财务确认日期 */
	private Date ConfDate;
	/** 结算日期 */
	private Date BalaDate;
	/** 币别 */
	private String Currency;
	/** 执行状态 */
	private String State;
	/** 校检标记 */
	private String CheckFlag;
	/** 执行事件号 */
	private String EventNo;
	/** 操作员 */
	private String Operator;
	/** 提数日期 */
	private Date MakeDate;
	/** 提数时间 */
	private String MakeTime;

	public static final int FIELDNUM = 55;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIAboriginalMainBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "TaskNo";
		pk[1] = "PhysicalTable";
		pk[2] = "KeyUnionValue";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIAboriginalMainBSchema cloned = (FIAboriginalMainBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTaskNo()
	{
		return TaskNo;
	}
	public void setTaskNo(String aTaskNo)
	{
		TaskNo = aTaskNo;
	}
	public String getPhysicalTable()
	{
		return PhysicalTable;
	}
	public void setPhysicalTable(String aPhysicalTable)
	{
		PhysicalTable = aPhysicalTable;
	}
	public String getKeyUnionValue()
	{
		return KeyUnionValue;
	}
	public void setKeyUnionValue(String aKeyUnionValue)
	{
		KeyUnionValue = aKeyUnionValue;
	}
	public String getMSerialNo()
	{
		return MSerialNo;
	}
	public void setMSerialNo(String aMSerialNo)
	{
		MSerialNo = aMSerialNo;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getIndexNo()
	{
		return IndexNo;
	}
	public void setIndexNo(String aIndexNo)
	{
		IndexNo = aIndexNo;
	}
	public String getOtherNoType()
	{
		return OtherNoType;
	}
	public void setOtherNoType(String aOtherNoType)
	{
		OtherNoType = aOtherNoType;
	}
	public String getOtherNo()
	{
		return OtherNo;
	}
	public void setOtherNo(String aOtherNo)
	{
		OtherNo = aOtherNo;
	}
	public String getCustomerNoType()
	{
		return CustomerNoType;
	}
	public void setCustomerNoType(String aCustomerNoType)
	{
		CustomerNoType = aCustomerNoType;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getSupplierNo()
	{
		return SupplierNo;
	}
	public void setSupplierNo(String aSupplierNo)
	{
		SupplierNo = aSupplierNo;
	}
	public String getBillNo()
	{
		return BillNo;
	}
	public void setBillNo(String aBillNo)
	{
		BillNo = aBillNo;
	}
	public String getGetNoticeNo()
	{
		return GetNoticeNo;
	}
	public void setGetNoticeNo(String aGetNoticeNo)
	{
		GetNoticeNo = aGetNoticeNo;
	}
	public String getListFlag()
	{
		return ListFlag;
	}
	public void setListFlag(String aListFlag)
	{
		ListFlag = aListFlag;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getSignDate()
	{
		if( SignDate != null )
			return fDate.getString(SignDate);
		else
			return null;
	}
	public void setSignDate(Date aSignDate)
	{
		SignDate = aSignDate;
	}
	public void setSignDate(String aSignDate)
	{
		if (aSignDate != null && !aSignDate.equals("") )
		{
			SignDate = fDate.getDate( aSignDate );
		}
		else
			SignDate = null;
	}

	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getDueFeeType()
	{
		return DueFeeType;
	}
	public void setDueFeeType(String aDueFeeType)
	{
		DueFeeType = aDueFeeType;
	}
	public double getSumActuMoney()
	{
		return SumActuMoney;
	}
	public void setSumActuMoney(double aSumActuMoney)
	{
		SumActuMoney = Arith.round(aSumActuMoney,2);
	}
	public void setSumActuMoney(String aSumActuMoney)
	{
		if (aSumActuMoney != null && !aSumActuMoney.equals(""))
		{
			Double tDouble = new Double(aSumActuMoney);
			double d = tDouble.doubleValue();
                SumActuMoney = Arith.round(d,2);
		}
	}

	public double getAccBala()
	{
		return AccBala;
	}
	public void setAccBala(double aAccBala)
	{
		AccBala = Arith.round(aAccBala,2);
	}
	public void setAccBala(String aAccBala)
	{
		if (aAccBala != null && !aAccBala.equals(""))
		{
			Double tDouble = new Double(aAccBala);
			double d = tDouble.doubleValue();
                AccBala = Arith.round(d,2);
		}
	}

	public double getInsuAccBala()
	{
		return InsuAccBala;
	}
	public void setInsuAccBala(double aInsuAccBala)
	{
		InsuAccBala = Arith.round(aInsuAccBala,2);
	}
	public void setInsuAccBala(String aInsuAccBala)
	{
		if (aInsuAccBala != null && !aInsuAccBala.equals(""))
		{
			Double tDouble = new Double(aInsuAccBala);
			double d = tDouble.doubleValue();
                InsuAccBala = Arith.round(d,2);
		}
	}

	public double getLastAccBala()
	{
		return LastAccBala;
	}
	public void setLastAccBala(double aLastAccBala)
	{
		LastAccBala = Arith.round(aLastAccBala,2);
	}
	public void setLastAccBala(String aLastAccBala)
	{
		if (aLastAccBala != null && !aLastAccBala.equals(""))
		{
			Double tDouble = new Double(aLastAccBala);
			double d = tDouble.doubleValue();
                LastAccBala = Arith.round(d,2);
		}
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getExecuteCom()
	{
		return ExecuteCom;
	}
	public void setExecuteCom(String aExecuteCom)
	{
		ExecuteCom = aExecuteCom;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getACType()
	{
		return ACType;
	}
	public void setACType(String aACType)
	{
		ACType = aACType;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getPayIntvFlag()
	{
		return PayIntvFlag;
	}
	public void setPayIntvFlag(String aPayIntvFlag)
	{
		PayIntvFlag = aPayIntvFlag;
	}
	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getSaleChnlDetail()
	{
		return SaleChnlDetail;
	}
	public void setSaleChnlDetail(String aSaleChnlDetail)
	{
		SaleChnlDetail = aSaleChnlDetail;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getPayTypeFlag()
	{
		return PayTypeFlag;
	}
	public void setPayTypeFlag(String aPayTypeFlag)
	{
		PayTypeFlag = aPayTypeFlag;
	}
	public String getBalanceDate()
	{
		if( BalanceDate != null )
			return fDate.getString(BalanceDate);
		else
			return null;
	}
	public void setBalanceDate(Date aBalanceDate)
	{
		BalanceDate = aBalanceDate;
	}
	public void setBalanceDate(String aBalanceDate)
	{
		if (aBalanceDate != null && !aBalanceDate.equals("") )
		{
			BalanceDate = fDate.getDate( aBalanceDate );
		}
		else
			BalanceDate = null;
	}

	public String getAccountDate()
	{
		if( AccountDate != null )
			return fDate.getString(AccountDate);
		else
			return null;
	}
	public void setAccountDate(Date aAccountDate)
	{
		AccountDate = aAccountDate;
	}
	public void setAccountDate(String aAccountDate)
	{
		if (aAccountDate != null && !aAccountDate.equals("") )
		{
			AccountDate = fDate.getDate( aAccountDate );
		}
		else
			AccountDate = null;
	}

	public String getStartPayDate()
	{
		if( StartPayDate != null )
			return fDate.getString(StartPayDate);
		else
			return null;
	}
	public void setStartPayDate(Date aStartPayDate)
	{
		StartPayDate = aStartPayDate;
	}
	public void setStartPayDate(String aStartPayDate)
	{
		if (aStartPayDate != null && !aStartPayDate.equals("") )
		{
			StartPayDate = fDate.getDate( aStartPayDate );
		}
		else
			StartPayDate = null;
	}

	public String getShouldDate()
	{
		if( ShouldDate != null )
			return fDate.getString(ShouldDate);
		else
			return null;
	}
	public void setShouldDate(Date aShouldDate)
	{
		ShouldDate = aShouldDate;
	}
	public void setShouldDate(String aShouldDate)
	{
		if (aShouldDate != null && !aShouldDate.equals("") )
		{
			ShouldDate = fDate.getDate( aShouldDate );
		}
		else
			ShouldDate = null;
	}

	public String getEnterAccDate()
	{
		if( EnterAccDate != null )
			return fDate.getString(EnterAccDate);
		else
			return null;
	}
	public void setEnterAccDate(Date aEnterAccDate)
	{
		EnterAccDate = aEnterAccDate;
	}
	public void setEnterAccDate(String aEnterAccDate)
	{
		if (aEnterAccDate != null && !aEnterAccDate.equals("") )
		{
			EnterAccDate = fDate.getDate( aEnterAccDate );
		}
		else
			EnterAccDate = null;
	}

	public String getConfDate()
	{
		if( ConfDate != null )
			return fDate.getString(ConfDate);
		else
			return null;
	}
	public void setConfDate(Date aConfDate)
	{
		ConfDate = aConfDate;
	}
	public void setConfDate(String aConfDate)
	{
		if (aConfDate != null && !aConfDate.equals("") )
		{
			ConfDate = fDate.getDate( aConfDate );
		}
		else
			ConfDate = null;
	}

	public String getBalaDate()
	{
		if( BalaDate != null )
			return fDate.getString(BalaDate);
		else
			return null;
	}
	public void setBalaDate(Date aBalaDate)
	{
		BalaDate = aBalaDate;
	}
	public void setBalaDate(String aBalaDate)
	{
		if (aBalaDate != null && !aBalaDate.equals("") )
		{
			BalaDate = fDate.getDate( aBalaDate );
		}
		else
			BalaDate = null;
	}

	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getCheckFlag()
	{
		return CheckFlag;
	}
	public void setCheckFlag(String aCheckFlag)
	{
		CheckFlag = aCheckFlag;
	}
	public String getEventNo()
	{
		return EventNo;
	}
	public void setEventNo(String aEventNo)
	{
		EventNo = aEventNo;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}

	/**
	* 使用另外一个 FIAboriginalMainBSchema 对象给 Schema 赋值
	* @param: aFIAboriginalMainBSchema FIAboriginalMainBSchema
	**/
	public void setSchema(FIAboriginalMainBSchema aFIAboriginalMainBSchema)
	{
		this.TaskNo = aFIAboriginalMainBSchema.getTaskNo();
		this.PhysicalTable = aFIAboriginalMainBSchema.getPhysicalTable();
		this.KeyUnionValue = aFIAboriginalMainBSchema.getKeyUnionValue();
		this.MSerialNo = aFIAboriginalMainBSchema.getMSerialNo();
		this.IndexCode = aFIAboriginalMainBSchema.getIndexCode();
		this.IndexNo = aFIAboriginalMainBSchema.getIndexNo();
		this.OtherNoType = aFIAboriginalMainBSchema.getOtherNoType();
		this.OtherNo = aFIAboriginalMainBSchema.getOtherNo();
		this.CustomerNoType = aFIAboriginalMainBSchema.getCustomerNoType();
		this.CustomerNo = aFIAboriginalMainBSchema.getCustomerNo();
		this.SupplierNo = aFIAboriginalMainBSchema.getSupplierNo();
		this.BillNo = aFIAboriginalMainBSchema.getBillNo();
		this.GetNoticeNo = aFIAboriginalMainBSchema.getGetNoticeNo();
		this.ListFlag = aFIAboriginalMainBSchema.getListFlag();
		this.GrpContNo = aFIAboriginalMainBSchema.getGrpContNo();
		this.ContNo = aFIAboriginalMainBSchema.getContNo();
		this.PayMode = aFIAboriginalMainBSchema.getPayMode();
		this.SignDate = fDate.getDate( aFIAboriginalMainBSchema.getSignDate());
		this.CValiDate = fDate.getDate( aFIAboriginalMainBSchema.getCValiDate());
		this.DueFeeType = aFIAboriginalMainBSchema.getDueFeeType();
		this.SumActuMoney = aFIAboriginalMainBSchema.getSumActuMoney();
		this.AccBala = aFIAboriginalMainBSchema.getAccBala();
		this.InsuAccBala = aFIAboriginalMainBSchema.getInsuAccBala();
		this.LastAccBala = aFIAboriginalMainBSchema.getLastAccBala();
		this.ManageCom = aFIAboriginalMainBSchema.getManageCom();
		this.ExecuteCom = aFIAboriginalMainBSchema.getExecuteCom();
		this.CostCenter = aFIAboriginalMainBSchema.getCostCenter();
		this.ACType = aFIAboriginalMainBSchema.getACType();
		this.AgentCom = aFIAboriginalMainBSchema.getAgentCom();
		this.AgentCode = aFIAboriginalMainBSchema.getAgentCode();
		this.AgentGroup = aFIAboriginalMainBSchema.getAgentGroup();
		this.PayIntvFlag = aFIAboriginalMainBSchema.getPayIntvFlag();
		this.PayIntv = aFIAboriginalMainBSchema.getPayIntv();
		this.BankCode = aFIAboriginalMainBSchema.getBankCode();
		this.AccName = aFIAboriginalMainBSchema.getAccName();
		this.BankAccNo = aFIAboriginalMainBSchema.getBankAccNo();
		this.Years = aFIAboriginalMainBSchema.getYears();
		this.SaleChnl = aFIAboriginalMainBSchema.getSaleChnl();
		this.SaleChnlDetail = aFIAboriginalMainBSchema.getSaleChnlDetail();
		this.MarketType = aFIAboriginalMainBSchema.getMarketType();
		this.PayTypeFlag = aFIAboriginalMainBSchema.getPayTypeFlag();
		this.BalanceDate = fDate.getDate( aFIAboriginalMainBSchema.getBalanceDate());
		this.AccountDate = fDate.getDate( aFIAboriginalMainBSchema.getAccountDate());
		this.StartPayDate = fDate.getDate( aFIAboriginalMainBSchema.getStartPayDate());
		this.ShouldDate = fDate.getDate( aFIAboriginalMainBSchema.getShouldDate());
		this.EnterAccDate = fDate.getDate( aFIAboriginalMainBSchema.getEnterAccDate());
		this.ConfDate = fDate.getDate( aFIAboriginalMainBSchema.getConfDate());
		this.BalaDate = fDate.getDate( aFIAboriginalMainBSchema.getBalaDate());
		this.Currency = aFIAboriginalMainBSchema.getCurrency();
		this.State = aFIAboriginalMainBSchema.getState();
		this.CheckFlag = aFIAboriginalMainBSchema.getCheckFlag();
		this.EventNo = aFIAboriginalMainBSchema.getEventNo();
		this.Operator = aFIAboriginalMainBSchema.getOperator();
		this.MakeDate = fDate.getDate( aFIAboriginalMainBSchema.getMakeDate());
		this.MakeTime = aFIAboriginalMainBSchema.getMakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TaskNo") == null )
				this.TaskNo = null;
			else
				this.TaskNo = rs.getString("TaskNo").trim();

			if( rs.getString("PhysicalTable") == null )
				this.PhysicalTable = null;
			else
				this.PhysicalTable = rs.getString("PhysicalTable").trim();

			if( rs.getString("KeyUnionValue") == null )
				this.KeyUnionValue = null;
			else
				this.KeyUnionValue = rs.getString("KeyUnionValue").trim();

			if( rs.getString("MSerialNo") == null )
				this.MSerialNo = null;
			else
				this.MSerialNo = rs.getString("MSerialNo").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("IndexNo") == null )
				this.IndexNo = null;
			else
				this.IndexNo = rs.getString("IndexNo").trim();

			if( rs.getString("OtherNoType") == null )
				this.OtherNoType = null;
			else
				this.OtherNoType = rs.getString("OtherNoType").trim();

			if( rs.getString("OtherNo") == null )
				this.OtherNo = null;
			else
				this.OtherNo = rs.getString("OtherNo").trim();

			if( rs.getString("CustomerNoType") == null )
				this.CustomerNoType = null;
			else
				this.CustomerNoType = rs.getString("CustomerNoType").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("SupplierNo") == null )
				this.SupplierNo = null;
			else
				this.SupplierNo = rs.getString("SupplierNo").trim();

			if( rs.getString("BillNo") == null )
				this.BillNo = null;
			else
				this.BillNo = rs.getString("BillNo").trim();

			if( rs.getString("GetNoticeNo") == null )
				this.GetNoticeNo = null;
			else
				this.GetNoticeNo = rs.getString("GetNoticeNo").trim();

			if( rs.getString("ListFlag") == null )
				this.ListFlag = null;
			else
				this.ListFlag = rs.getString("ListFlag").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			this.SignDate = rs.getDate("SignDate");
			this.CValiDate = rs.getDate("CValiDate");
			if( rs.getString("DueFeeType") == null )
				this.DueFeeType = null;
			else
				this.DueFeeType = rs.getString("DueFeeType").trim();

			this.SumActuMoney = rs.getDouble("SumActuMoney");
			this.AccBala = rs.getDouble("AccBala");
			this.InsuAccBala = rs.getDouble("InsuAccBala");
			this.LastAccBala = rs.getDouble("LastAccBala");
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ExecuteCom") == null )
				this.ExecuteCom = null;
			else
				this.ExecuteCom = rs.getString("ExecuteCom").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("ACType") == null )
				this.ACType = null;
			else
				this.ACType = rs.getString("ACType").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("PayIntvFlag") == null )
				this.PayIntvFlag = null;
			else
				this.PayIntvFlag = rs.getString("PayIntvFlag").trim();

			this.PayIntv = rs.getInt("PayIntv");
			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			this.Years = rs.getInt("Years");
			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("SaleChnlDetail") == null )
				this.SaleChnlDetail = null;
			else
				this.SaleChnlDetail = rs.getString("SaleChnlDetail").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("PayTypeFlag") == null )
				this.PayTypeFlag = null;
			else
				this.PayTypeFlag = rs.getString("PayTypeFlag").trim();

			this.BalanceDate = rs.getDate("BalanceDate");
			this.AccountDate = rs.getDate("AccountDate");
			this.StartPayDate = rs.getDate("StartPayDate");
			this.ShouldDate = rs.getDate("ShouldDate");
			this.EnterAccDate = rs.getDate("EnterAccDate");
			this.ConfDate = rs.getDate("ConfDate");
			this.BalaDate = rs.getDate("BalaDate");
			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("CheckFlag") == null )
				this.CheckFlag = null;
			else
				this.CheckFlag = rs.getString("CheckFlag").trim();

			if( rs.getString("EventNo") == null )
				this.EventNo = null;
			else
				this.EventNo = rs.getString("EventNo").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIAboriginalMainB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIAboriginalMainBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIAboriginalMainBSchema getSchema()
	{
		FIAboriginalMainBSchema aFIAboriginalMainBSchema = new FIAboriginalMainBSchema();
		aFIAboriginalMainBSchema.setSchema(this);
		return aFIAboriginalMainBSchema;
	}

	public FIAboriginalMainBDB getDB()
	{
		FIAboriginalMainBDB aDBOper = new FIAboriginalMainBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIAboriginalMainB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TaskNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PhysicalTable)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(KeyUnionValue)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SupplierNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BillNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetNoticeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ListFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DueFeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumActuMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AccBala));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuAccBala));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LastAccBala));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ACType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayIntvFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnlDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayTypeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BalanceDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccountDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartPayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ShouldDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BalaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EventNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIAboriginalMainB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PhysicalTable = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			KeyUnionValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			MSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IndexNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CustomerNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			SupplierNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			BillNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ListFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			DueFeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			SumActuMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			AccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			InsuAccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			LastAccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ACType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			PayIntvFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).intValue();
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).intValue();
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			SaleChnlDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			PayTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			BalanceDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,SysConst.PACKAGESPILTER));
			AccountDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			StartPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			ShouldDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45,SysConst.PACKAGESPILTER));
			EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46,SysConst.PACKAGESPILTER));
			ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47,SysConst.PACKAGESPILTER));
			BalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48,SysConst.PACKAGESPILTER));
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			EventNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIAboriginalMainBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TaskNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaskNo));
		}
		if (FCode.equals("PhysicalTable"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PhysicalTable));
		}
		if (FCode.equals("KeyUnionValue"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KeyUnionValue));
		}
		if (FCode.equals("MSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MSerialNo));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("IndexNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexNo));
		}
		if (FCode.equals("OtherNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
		}
		if (FCode.equals("OtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
		}
		if (FCode.equals("CustomerNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNoType));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("SupplierNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupplierNo));
		}
		if (FCode.equals("BillNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BillNo));
		}
		if (FCode.equals("GetNoticeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
		}
		if (FCode.equals("ListFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ListFlag));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("DueFeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DueFeeType));
		}
		if (FCode.equals("SumActuMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuMoney));
		}
		if (FCode.equals("AccBala"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccBala));
		}
		if (FCode.equals("InsuAccBala"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccBala));
		}
		if (FCode.equals("LastAccBala"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LastAccBala));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ExecuteCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("ACType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ACType));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("PayIntvFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntvFlag));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("SaleChnlDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlDetail));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("PayTypeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayTypeFlag));
		}
		if (FCode.equals("BalanceDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalanceDate()));
		}
		if (FCode.equals("AccountDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
		}
		if (FCode.equals("StartPayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartPayDate()));
		}
		if (FCode.equals("ShouldDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getShouldDate()));
		}
		if (FCode.equals("EnterAccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
		}
		if (FCode.equals("ConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
		}
		if (FCode.equals("BalaDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("CheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
		}
		if (FCode.equals("EventNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EventNo));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TaskNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PhysicalTable);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(KeyUnionValue);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(MSerialNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IndexNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(OtherNoType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(OtherNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CustomerNoType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(SupplierNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(BillNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ListFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(DueFeeType);
				break;
			case 20:
				strFieldValue = String.valueOf(SumActuMoney);
				break;
			case 21:
				strFieldValue = String.valueOf(AccBala);
				break;
			case 22:
				strFieldValue = String.valueOf(InsuAccBala);
				break;
			case 23:
				strFieldValue = String.valueOf(LastAccBala);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(ACType);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(PayIntvFlag);
				break;
			case 32:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 36:
				strFieldValue = String.valueOf(Years);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(SaleChnlDetail);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(PayTypeFlag);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalanceDate()));
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartPayDate()));
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getShouldDate()));
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(CheckFlag);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(EventNo);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TaskNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaskNo = FValue.trim();
			}
			else
				TaskNo = null;
		}
		if (FCode.equalsIgnoreCase("PhysicalTable"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PhysicalTable = FValue.trim();
			}
			else
				PhysicalTable = null;
		}
		if (FCode.equalsIgnoreCase("KeyUnionValue"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				KeyUnionValue = FValue.trim();
			}
			else
				KeyUnionValue = null;
		}
		if (FCode.equalsIgnoreCase("MSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MSerialNo = FValue.trim();
			}
			else
				MSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexNo = FValue.trim();
			}
			else
				IndexNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNoType = FValue.trim();
			}
			else
				OtherNoType = null;
		}
		if (FCode.equalsIgnoreCase("OtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo = FValue.trim();
			}
			else
				OtherNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNoType = FValue.trim();
			}
			else
				CustomerNoType = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("SupplierNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SupplierNo = FValue.trim();
			}
			else
				SupplierNo = null;
		}
		if (FCode.equalsIgnoreCase("BillNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BillNo = FValue.trim();
			}
			else
				BillNo = null;
		}
		if (FCode.equalsIgnoreCase("GetNoticeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetNoticeNo = FValue.trim();
			}
			else
				GetNoticeNo = null;
		}
		if (FCode.equalsIgnoreCase("ListFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ListFlag = FValue.trim();
			}
			else
				ListFlag = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SignDate = fDate.getDate( FValue );
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("DueFeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DueFeeType = FValue.trim();
			}
			else
				DueFeeType = null;
		}
		if (FCode.equalsIgnoreCase("SumActuMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumActuMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccBala"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccBala = d;
			}
		}
		if (FCode.equalsIgnoreCase("InsuAccBala"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				InsuAccBala = d;
			}
		}
		if (FCode.equalsIgnoreCase("LastAccBala"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LastAccBala = d;
			}
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ExecuteCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExecuteCom = FValue.trim();
			}
			else
				ExecuteCom = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("ACType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ACType = FValue.trim();
			}
			else
				ACType = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("PayIntvFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayIntvFlag = FValue.trim();
			}
			else
				PayIntvFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnlDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnlDetail = FValue.trim();
			}
			else
				SaleChnlDetail = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("PayTypeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayTypeFlag = FValue.trim();
			}
			else
				PayTypeFlag = null;
		}
		if (FCode.equalsIgnoreCase("BalanceDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BalanceDate = fDate.getDate( FValue );
			}
			else
				BalanceDate = null;
		}
		if (FCode.equalsIgnoreCase("AccountDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccountDate = fDate.getDate( FValue );
			}
			else
				AccountDate = null;
		}
		if (FCode.equalsIgnoreCase("StartPayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartPayDate = fDate.getDate( FValue );
			}
			else
				StartPayDate = null;
		}
		if (FCode.equalsIgnoreCase("ShouldDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ShouldDate = fDate.getDate( FValue );
			}
			else
				ShouldDate = null;
		}
		if (FCode.equalsIgnoreCase("EnterAccDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EnterAccDate = fDate.getDate( FValue );
			}
			else
				EnterAccDate = null;
		}
		if (FCode.equalsIgnoreCase("ConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConfDate = fDate.getDate( FValue );
			}
			else
				ConfDate = null;
		}
		if (FCode.equalsIgnoreCase("BalaDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BalaDate = fDate.getDate( FValue );
			}
			else
				BalaDate = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("CheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckFlag = FValue.trim();
			}
			else
				CheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("EventNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EventNo = FValue.trim();
			}
			else
				EventNo = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIAboriginalMainBSchema other = (FIAboriginalMainBSchema)otherObject;
		return
			(TaskNo == null ? other.getTaskNo() == null : TaskNo.equals(other.getTaskNo()))
			&& (PhysicalTable == null ? other.getPhysicalTable() == null : PhysicalTable.equals(other.getPhysicalTable()))
			&& (KeyUnionValue == null ? other.getKeyUnionValue() == null : KeyUnionValue.equals(other.getKeyUnionValue()))
			&& (MSerialNo == null ? other.getMSerialNo() == null : MSerialNo.equals(other.getMSerialNo()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (IndexNo == null ? other.getIndexNo() == null : IndexNo.equals(other.getIndexNo()))
			&& (OtherNoType == null ? other.getOtherNoType() == null : OtherNoType.equals(other.getOtherNoType()))
			&& (OtherNo == null ? other.getOtherNo() == null : OtherNo.equals(other.getOtherNo()))
			&& (CustomerNoType == null ? other.getCustomerNoType() == null : CustomerNoType.equals(other.getCustomerNoType()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (SupplierNo == null ? other.getSupplierNo() == null : SupplierNo.equals(other.getSupplierNo()))
			&& (BillNo == null ? other.getBillNo() == null : BillNo.equals(other.getBillNo()))
			&& (GetNoticeNo == null ? other.getGetNoticeNo() == null : GetNoticeNo.equals(other.getGetNoticeNo()))
			&& (ListFlag == null ? other.getListFlag() == null : ListFlag.equals(other.getListFlag()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (SignDate == null ? other.getSignDate() == null : fDate.getString(SignDate).equals(other.getSignDate()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& (DueFeeType == null ? other.getDueFeeType() == null : DueFeeType.equals(other.getDueFeeType()))
			&& SumActuMoney == other.getSumActuMoney()
			&& AccBala == other.getAccBala()
			&& InsuAccBala == other.getInsuAccBala()
			&& LastAccBala == other.getLastAccBala()
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ExecuteCom == null ? other.getExecuteCom() == null : ExecuteCom.equals(other.getExecuteCom()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (ACType == null ? other.getACType() == null : ACType.equals(other.getACType()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (PayIntvFlag == null ? other.getPayIntvFlag() == null : PayIntvFlag.equals(other.getPayIntvFlag()))
			&& PayIntv == other.getPayIntv()
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& Years == other.getYears()
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (SaleChnlDetail == null ? other.getSaleChnlDetail() == null : SaleChnlDetail.equals(other.getSaleChnlDetail()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (PayTypeFlag == null ? other.getPayTypeFlag() == null : PayTypeFlag.equals(other.getPayTypeFlag()))
			&& (BalanceDate == null ? other.getBalanceDate() == null : fDate.getString(BalanceDate).equals(other.getBalanceDate()))
			&& (AccountDate == null ? other.getAccountDate() == null : fDate.getString(AccountDate).equals(other.getAccountDate()))
			&& (StartPayDate == null ? other.getStartPayDate() == null : fDate.getString(StartPayDate).equals(other.getStartPayDate()))
			&& (ShouldDate == null ? other.getShouldDate() == null : fDate.getString(ShouldDate).equals(other.getShouldDate()))
			&& (EnterAccDate == null ? other.getEnterAccDate() == null : fDate.getString(EnterAccDate).equals(other.getEnterAccDate()))
			&& (ConfDate == null ? other.getConfDate() == null : fDate.getString(ConfDate).equals(other.getConfDate()))
			&& (BalaDate == null ? other.getBalaDate() == null : fDate.getString(BalaDate).equals(other.getBalaDate()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (CheckFlag == null ? other.getCheckFlag() == null : CheckFlag.equals(other.getCheckFlag()))
			&& (EventNo == null ? other.getEventNo() == null : EventNo.equals(other.getEventNo()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TaskNo") ) {
			return 0;
		}
		if( strFieldName.equals("PhysicalTable") ) {
			return 1;
		}
		if( strFieldName.equals("KeyUnionValue") ) {
			return 2;
		}
		if( strFieldName.equals("MSerialNo") ) {
			return 3;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 4;
		}
		if( strFieldName.equals("IndexNo") ) {
			return 5;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return 6;
		}
		if( strFieldName.equals("OtherNo") ) {
			return 7;
		}
		if( strFieldName.equals("CustomerNoType") ) {
			return 8;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 9;
		}
		if( strFieldName.equals("SupplierNo") ) {
			return 10;
		}
		if( strFieldName.equals("BillNo") ) {
			return 11;
		}
		if( strFieldName.equals("GetNoticeNo") ) {
			return 12;
		}
		if( strFieldName.equals("ListFlag") ) {
			return 13;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 14;
		}
		if( strFieldName.equals("ContNo") ) {
			return 15;
		}
		if( strFieldName.equals("PayMode") ) {
			return 16;
		}
		if( strFieldName.equals("SignDate") ) {
			return 17;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 18;
		}
		if( strFieldName.equals("DueFeeType") ) {
			return 19;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return 20;
		}
		if( strFieldName.equals("AccBala") ) {
			return 21;
		}
		if( strFieldName.equals("InsuAccBala") ) {
			return 22;
		}
		if( strFieldName.equals("LastAccBala") ) {
			return 23;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 24;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return 25;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 26;
		}
		if( strFieldName.equals("ACType") ) {
			return 27;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 28;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 29;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 30;
		}
		if( strFieldName.equals("PayIntvFlag") ) {
			return 31;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 32;
		}
		if( strFieldName.equals("BankCode") ) {
			return 33;
		}
		if( strFieldName.equals("AccName") ) {
			return 34;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 35;
		}
		if( strFieldName.equals("Years") ) {
			return 36;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 37;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return 38;
		}
		if( strFieldName.equals("MarketType") ) {
			return 39;
		}
		if( strFieldName.equals("PayTypeFlag") ) {
			return 40;
		}
		if( strFieldName.equals("BalanceDate") ) {
			return 41;
		}
		if( strFieldName.equals("AccountDate") ) {
			return 42;
		}
		if( strFieldName.equals("StartPayDate") ) {
			return 43;
		}
		if( strFieldName.equals("ShouldDate") ) {
			return 44;
		}
		if( strFieldName.equals("EnterAccDate") ) {
			return 45;
		}
		if( strFieldName.equals("ConfDate") ) {
			return 46;
		}
		if( strFieldName.equals("BalaDate") ) {
			return 47;
		}
		if( strFieldName.equals("Currency") ) {
			return 48;
		}
		if( strFieldName.equals("State") ) {
			return 49;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return 50;
		}
		if( strFieldName.equals("EventNo") ) {
			return 51;
		}
		if( strFieldName.equals("Operator") ) {
			return 52;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 53;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 54;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TaskNo";
				break;
			case 1:
				strFieldName = "PhysicalTable";
				break;
			case 2:
				strFieldName = "KeyUnionValue";
				break;
			case 3:
				strFieldName = "MSerialNo";
				break;
			case 4:
				strFieldName = "IndexCode";
				break;
			case 5:
				strFieldName = "IndexNo";
				break;
			case 6:
				strFieldName = "OtherNoType";
				break;
			case 7:
				strFieldName = "OtherNo";
				break;
			case 8:
				strFieldName = "CustomerNoType";
				break;
			case 9:
				strFieldName = "CustomerNo";
				break;
			case 10:
				strFieldName = "SupplierNo";
				break;
			case 11:
				strFieldName = "BillNo";
				break;
			case 12:
				strFieldName = "GetNoticeNo";
				break;
			case 13:
				strFieldName = "ListFlag";
				break;
			case 14:
				strFieldName = "GrpContNo";
				break;
			case 15:
				strFieldName = "ContNo";
				break;
			case 16:
				strFieldName = "PayMode";
				break;
			case 17:
				strFieldName = "SignDate";
				break;
			case 18:
				strFieldName = "CValiDate";
				break;
			case 19:
				strFieldName = "DueFeeType";
				break;
			case 20:
				strFieldName = "SumActuMoney";
				break;
			case 21:
				strFieldName = "AccBala";
				break;
			case 22:
				strFieldName = "InsuAccBala";
				break;
			case 23:
				strFieldName = "LastAccBala";
				break;
			case 24:
				strFieldName = "ManageCom";
				break;
			case 25:
				strFieldName = "ExecuteCom";
				break;
			case 26:
				strFieldName = "CostCenter";
				break;
			case 27:
				strFieldName = "ACType";
				break;
			case 28:
				strFieldName = "AgentCom";
				break;
			case 29:
				strFieldName = "AgentCode";
				break;
			case 30:
				strFieldName = "AgentGroup";
				break;
			case 31:
				strFieldName = "PayIntvFlag";
				break;
			case 32:
				strFieldName = "PayIntv";
				break;
			case 33:
				strFieldName = "BankCode";
				break;
			case 34:
				strFieldName = "AccName";
				break;
			case 35:
				strFieldName = "BankAccNo";
				break;
			case 36:
				strFieldName = "Years";
				break;
			case 37:
				strFieldName = "SaleChnl";
				break;
			case 38:
				strFieldName = "SaleChnlDetail";
				break;
			case 39:
				strFieldName = "MarketType";
				break;
			case 40:
				strFieldName = "PayTypeFlag";
				break;
			case 41:
				strFieldName = "BalanceDate";
				break;
			case 42:
				strFieldName = "AccountDate";
				break;
			case 43:
				strFieldName = "StartPayDate";
				break;
			case 44:
				strFieldName = "ShouldDate";
				break;
			case 45:
				strFieldName = "EnterAccDate";
				break;
			case 46:
				strFieldName = "ConfDate";
				break;
			case 47:
				strFieldName = "BalaDate";
				break;
			case 48:
				strFieldName = "Currency";
				break;
			case 49:
				strFieldName = "State";
				break;
			case 50:
				strFieldName = "CheckFlag";
				break;
			case 51:
				strFieldName = "EventNo";
				break;
			case 52:
				strFieldName = "Operator";
				break;
			case 53:
				strFieldName = "MakeDate";
				break;
			case 54:
				strFieldName = "MakeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TaskNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PhysicalTable") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KeyUnionValue") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SupplierNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BillNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetNoticeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ListFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DueFeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccBala") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("InsuAccBala") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("LastAccBala") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ACType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntvFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayTypeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalanceDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccountDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StartPayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ShouldDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EnterAccDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ConfDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BalaDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EventNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_INT;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_INT;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 45:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 47:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
