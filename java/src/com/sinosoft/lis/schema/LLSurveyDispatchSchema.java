/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLSurveyDispatchDB;

/*
 * <p>ClassName: LLSurveyDispatchSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 理赔核心
 * @CreateDate：2010-08-20
 */
public class LLSurveyDispatchSchema implements Schema, Cloneable
{
	// @Field
	/** 分配轨迹号 */
	private String DispatchNo;
	/** 对应号码 */
	private String OtherNo;
	/** 对应号码类型 */
	private String OtherNoType;
	/** 调查号 */
	private String SurveyNo;
	/** 调查项目号 */
	private String InqNo;
	/** 分配人 */
	private String Dispatcher;
	/** 分配类型 */
	private String DispatchType;
	/** 原操作人 */
	private String OldOperator;
	/** 接收人 */
	private String Receiver;
	/** 备注 */
	private String Remark;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLSurveyDispatchSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "DispatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLSurveyDispatchSchema cloned = (LLSurveyDispatchSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getDispatchNo()
	{
		return DispatchNo;
	}
	public void setDispatchNo(String aDispatchNo)
	{
		DispatchNo = aDispatchNo;
	}
	public String getOtherNo()
	{
		return OtherNo;
	}
	public void setOtherNo(String aOtherNo)
	{
		OtherNo = aOtherNo;
	}
	public String getOtherNoType()
	{
		return OtherNoType;
	}
	public void setOtherNoType(String aOtherNoType)
	{
		OtherNoType = aOtherNoType;
	}
	public String getSurveyNo()
	{
		return SurveyNo;
	}
	public void setSurveyNo(String aSurveyNo)
	{
		SurveyNo = aSurveyNo;
	}
	public String getInqNo()
	{
		return InqNo;
	}
	public void setInqNo(String aInqNo)
	{
		InqNo = aInqNo;
	}
	public String getDispatcher()
	{
		return Dispatcher;
	}
	public void setDispatcher(String aDispatcher)
	{
		Dispatcher = aDispatcher;
	}
	public String getDispatchType()
	{
		return DispatchType;
	}
	public void setDispatchType(String aDispatchType)
	{
		DispatchType = aDispatchType;
	}
	public String getOldOperator()
	{
		return OldOperator;
	}
	public void setOldOperator(String aOldOperator)
	{
		OldOperator = aOldOperator;
	}
	public String getReceiver()
	{
		return Receiver;
	}
	public void setReceiver(String aReceiver)
	{
		Receiver = aReceiver;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLSurveyDispatchSchema 对象给 Schema 赋值
	* @param: aLLSurveyDispatchSchema LLSurveyDispatchSchema
	**/
	public void setSchema(LLSurveyDispatchSchema aLLSurveyDispatchSchema)
	{
		this.DispatchNo = aLLSurveyDispatchSchema.getDispatchNo();
		this.OtherNo = aLLSurveyDispatchSchema.getOtherNo();
		this.OtherNoType = aLLSurveyDispatchSchema.getOtherNoType();
		this.SurveyNo = aLLSurveyDispatchSchema.getSurveyNo();
		this.InqNo = aLLSurveyDispatchSchema.getInqNo();
		this.Dispatcher = aLLSurveyDispatchSchema.getDispatcher();
		this.DispatchType = aLLSurveyDispatchSchema.getDispatchType();
		this.OldOperator = aLLSurveyDispatchSchema.getOldOperator();
		this.Receiver = aLLSurveyDispatchSchema.getReceiver();
		this.Remark = aLLSurveyDispatchSchema.getRemark();
		this.ManageCom = aLLSurveyDispatchSchema.getManageCom();
		this.Operator = aLLSurveyDispatchSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLSurveyDispatchSchema.getMakeDate());
		this.MakeTime = aLLSurveyDispatchSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLSurveyDispatchSchema.getModifyDate());
		this.ModifyTime = aLLSurveyDispatchSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("DispatchNo") == null )
				this.DispatchNo = null;
			else
				this.DispatchNo = rs.getString("DispatchNo").trim();

			if( rs.getString("OtherNo") == null )
				this.OtherNo = null;
			else
				this.OtherNo = rs.getString("OtherNo").trim();

			if( rs.getString("OtherNoType") == null )
				this.OtherNoType = null;
			else
				this.OtherNoType = rs.getString("OtherNoType").trim();

			if( rs.getString("SurveyNo") == null )
				this.SurveyNo = null;
			else
				this.SurveyNo = rs.getString("SurveyNo").trim();

			if( rs.getString("InqNo") == null )
				this.InqNo = null;
			else
				this.InqNo = rs.getString("InqNo").trim();

			if( rs.getString("Dispatcher") == null )
				this.Dispatcher = null;
			else
				this.Dispatcher = rs.getString("Dispatcher").trim();

			if( rs.getString("DispatchType") == null )
				this.DispatchType = null;
			else
				this.DispatchType = rs.getString("DispatchType").trim();

			if( rs.getString("OldOperator") == null )
				this.OldOperator = null;
			else
				this.OldOperator = rs.getString("OldOperator").trim();

			if( rs.getString("Receiver") == null )
				this.Receiver = null;
			else
				this.Receiver = rs.getString("Receiver").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLSurveyDispatch表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSurveyDispatchSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLSurveyDispatchSchema getSchema()
	{
		LLSurveyDispatchSchema aLLSurveyDispatchSchema = new LLSurveyDispatchSchema();
		aLLSurveyDispatchSchema.setSchema(this);
		return aLLSurveyDispatchSchema;
	}

	public LLSurveyDispatchDB getDB()
	{
		LLSurveyDispatchDB aDBOper = new LLSurveyDispatchDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSurveyDispatch描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(DispatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SurveyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InqNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dispatcher)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DispatchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OldOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Receiver)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSurveyDispatch>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DispatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SurveyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			InqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Dispatcher = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DispatchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			OldOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Receiver = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSurveyDispatchSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DispatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DispatchNo));
		}
		if (FCode.equals("OtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
		}
		if (FCode.equals("OtherNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
		}
		if (FCode.equals("SurveyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyNo));
		}
		if (FCode.equals("InqNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InqNo));
		}
		if (FCode.equals("Dispatcher"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dispatcher));
		}
		if (FCode.equals("DispatchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DispatchType));
		}
		if (FCode.equals("OldOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OldOperator));
		}
		if (FCode.equals("Receiver"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Receiver));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(DispatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(OtherNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(OtherNoType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SurveyNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(InqNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Dispatcher);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DispatchType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(OldOperator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Receiver);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DispatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DispatchNo = FValue.trim();
			}
			else
				DispatchNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo = FValue.trim();
			}
			else
				OtherNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNoType = FValue.trim();
			}
			else
				OtherNoType = null;
		}
		if (FCode.equalsIgnoreCase("SurveyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SurveyNo = FValue.trim();
			}
			else
				SurveyNo = null;
		}
		if (FCode.equalsIgnoreCase("InqNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InqNo = FValue.trim();
			}
			else
				InqNo = null;
		}
		if (FCode.equalsIgnoreCase("Dispatcher"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dispatcher = FValue.trim();
			}
			else
				Dispatcher = null;
		}
		if (FCode.equalsIgnoreCase("DispatchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DispatchType = FValue.trim();
			}
			else
				DispatchType = null;
		}
		if (FCode.equalsIgnoreCase("OldOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OldOperator = FValue.trim();
			}
			else
				OldOperator = null;
		}
		if (FCode.equalsIgnoreCase("Receiver"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Receiver = FValue.trim();
			}
			else
				Receiver = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLSurveyDispatchSchema other = (LLSurveyDispatchSchema)otherObject;
		return
			(DispatchNo == null ? other.getDispatchNo() == null : DispatchNo.equals(other.getDispatchNo()))
			&& (OtherNo == null ? other.getOtherNo() == null : OtherNo.equals(other.getOtherNo()))
			&& (OtherNoType == null ? other.getOtherNoType() == null : OtherNoType.equals(other.getOtherNoType()))
			&& (SurveyNo == null ? other.getSurveyNo() == null : SurveyNo.equals(other.getSurveyNo()))
			&& (InqNo == null ? other.getInqNo() == null : InqNo.equals(other.getInqNo()))
			&& (Dispatcher == null ? other.getDispatcher() == null : Dispatcher.equals(other.getDispatcher()))
			&& (DispatchType == null ? other.getDispatchType() == null : DispatchType.equals(other.getDispatchType()))
			&& (OldOperator == null ? other.getOldOperator() == null : OldOperator.equals(other.getOldOperator()))
			&& (Receiver == null ? other.getReceiver() == null : Receiver.equals(other.getReceiver()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DispatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("OtherNo") ) {
			return 1;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return 2;
		}
		if( strFieldName.equals("SurveyNo") ) {
			return 3;
		}
		if( strFieldName.equals("InqNo") ) {
			return 4;
		}
		if( strFieldName.equals("Dispatcher") ) {
			return 5;
		}
		if( strFieldName.equals("DispatchType") ) {
			return 6;
		}
		if( strFieldName.equals("OldOperator") ) {
			return 7;
		}
		if( strFieldName.equals("Receiver") ) {
			return 8;
		}
		if( strFieldName.equals("Remark") ) {
			return 9;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DispatchNo";
				break;
			case 1:
				strFieldName = "OtherNo";
				break;
			case 2:
				strFieldName = "OtherNoType";
				break;
			case 3:
				strFieldName = "SurveyNo";
				break;
			case 4:
				strFieldName = "InqNo";
				break;
			case 5:
				strFieldName = "Dispatcher";
				break;
			case 6:
				strFieldName = "DispatchType";
				break;
			case 7:
				strFieldName = "OldOperator";
				break;
			case 8:
				strFieldName = "Receiver";
				break;
			case 9:
				strFieldName = "Remark";
				break;
			case 10:
				strFieldName = "ManageCom";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DispatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SurveyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InqNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dispatcher") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DispatchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OldOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Receiver") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
