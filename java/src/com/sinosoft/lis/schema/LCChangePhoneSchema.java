/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCChangePhoneDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LCChangePhoneSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-11-09
 */
public class LCChangePhoneSchema implements Schema, Cloneable {
    // @Field
    /** 合同号码 */
    private String ContNo;
    /** 更新日期 */
    private Date ChangePhoneDate;
    /** 通讯地址 */
    private String PostalAddress;
    /** 通讯邮编 */
    private String ZipCode;
    /** 通讯电话 */
    private String Phone;
    /** 通讯传真 */
    private String Fax;
    /** 家庭地址 */
    private String HomeAddress;
    /** 家庭邮编 */
    private String HomeZipCode;
    /** 家庭电话 */
    private String HomePhone;
    /** 家庭传真 */
    private String HomeFax;
    /** 单位地址 */
    private String CompanyAddress;
    /** 单位邮编 */
    private String CompanyZipCode;
    /** 单位电话 */
    private String CompanyPhone;
    /** 单位传真 */
    private String CompanyFax;
    /** 手机 */
    private String Mobile;
    /** E_mail */
    private String EMail;
    /** 传呼 */
    private String BP;
    /** 批次号 */
    private String BatchNo;
    /** 批数据的日期 */
    private Date BatchDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCChangePhoneSchema() {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCChangePhoneSchema cloned = (LCChangePhoneSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getChangePhoneDate() {
        if (ChangePhoneDate != null) {
            return fDate.getString(ChangePhoneDate);
        } else {
            return null;
        }
    }

    public void setChangePhoneDate(Date aChangePhoneDate) {
        ChangePhoneDate = aChangePhoneDate;
    }

    public void setChangePhoneDate(String aChangePhoneDate) {
        if (aChangePhoneDate != null && !aChangePhoneDate.equals("")) {
            ChangePhoneDate = fDate.getDate(aChangePhoneDate);
        } else {
            ChangePhoneDate = null;
        }
    }

    public String getPostalAddress() {
        return PostalAddress;
    }

    public void setPostalAddress(String aPostalAddress) {
        PostalAddress = aPostalAddress;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String aPhone) {
        Phone = aPhone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String aFax) {
        Fax = aFax;
    }

    public String getHomeAddress() {
        return HomeAddress;
    }

    public void setHomeAddress(String aHomeAddress) {
        HomeAddress = aHomeAddress;
    }

    public String getHomeZipCode() {
        return HomeZipCode;
    }

    public void setHomeZipCode(String aHomeZipCode) {
        HomeZipCode = aHomeZipCode;
    }

    public String getHomePhone() {
        return HomePhone;
    }

    public void setHomePhone(String aHomePhone) {
        HomePhone = aHomePhone;
    }

    public String getHomeFax() {
        return HomeFax;
    }

    public void setHomeFax(String aHomeFax) {
        HomeFax = aHomeFax;
    }

    public String getCompanyAddress() {
        return CompanyAddress;
    }

    public void setCompanyAddress(String aCompanyAddress) {
        CompanyAddress = aCompanyAddress;
    }

    public String getCompanyZipCode() {
        return CompanyZipCode;
    }

    public void setCompanyZipCode(String aCompanyZipCode) {
        CompanyZipCode = aCompanyZipCode;
    }

    public String getCompanyPhone() {
        return CompanyPhone;
    }

    public void setCompanyPhone(String aCompanyPhone) {
        CompanyPhone = aCompanyPhone;
    }

    public String getCompanyFax() {
        return CompanyFax;
    }

    public void setCompanyFax(String aCompanyFax) {
        CompanyFax = aCompanyFax;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String aEMail) {
        EMail = aEMail;
    }

    public String getBP() {
        return BP;
    }

    public void setBP(String aBP) {
        BP = aBP;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo) {
        BatchNo = aBatchNo;
    }

    public String getBatchDate() {
        if (BatchDate != null) {
            return fDate.getString(BatchDate);
        } else {
            return null;
        }
    }

    public void setBatchDate(Date aBatchDate) {
        BatchDate = aBatchDate;
    }

    public void setBatchDate(String aBatchDate) {
        if (aBatchDate != null && !aBatchDate.equals("")) {
            BatchDate = fDate.getDate(aBatchDate);
        } else {
            BatchDate = null;
        }
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LCChangePhoneSchema 对象给 Schema 赋值
     * @param: aLCChangePhoneSchema LCChangePhoneSchema
     **/
    public void setSchema(LCChangePhoneSchema aLCChangePhoneSchema) {
        this.ContNo = aLCChangePhoneSchema.getContNo();
        this.ChangePhoneDate = fDate.getDate(aLCChangePhoneSchema.
                                             getChangePhoneDate());
        this.PostalAddress = aLCChangePhoneSchema.getPostalAddress();
        this.ZipCode = aLCChangePhoneSchema.getZipCode();
        this.Phone = aLCChangePhoneSchema.getPhone();
        this.Fax = aLCChangePhoneSchema.getFax();
        this.HomeAddress = aLCChangePhoneSchema.getHomeAddress();
        this.HomeZipCode = aLCChangePhoneSchema.getHomeZipCode();
        this.HomePhone = aLCChangePhoneSchema.getHomePhone();
        this.HomeFax = aLCChangePhoneSchema.getHomeFax();
        this.CompanyAddress = aLCChangePhoneSchema.getCompanyAddress();
        this.CompanyZipCode = aLCChangePhoneSchema.getCompanyZipCode();
        this.CompanyPhone = aLCChangePhoneSchema.getCompanyPhone();
        this.CompanyFax = aLCChangePhoneSchema.getCompanyFax();
        this.Mobile = aLCChangePhoneSchema.getMobile();
        this.EMail = aLCChangePhoneSchema.getEMail();
        this.BP = aLCChangePhoneSchema.getBP();
        this.BatchNo = aLCChangePhoneSchema.getBatchNo();
        this.BatchDate = fDate.getDate(aLCChangePhoneSchema.getBatchDate());
        this.Operator = aLCChangePhoneSchema.getOperator();
        this.MakeDate = fDate.getDate(aLCChangePhoneSchema.getMakeDate());
        this.MakeTime = aLCChangePhoneSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            this.ChangePhoneDate = rs.getDate("ChangePhoneDate");
            if (rs.getString("PostalAddress") == null) {
                this.PostalAddress = null;
            } else {
                this.PostalAddress = rs.getString("PostalAddress").trim();
            }

            if (rs.getString("ZipCode") == null) {
                this.ZipCode = null;
            } else {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null) {
                this.Phone = null;
            } else {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("Fax") == null) {
                this.Fax = null;
            } else {
                this.Fax = rs.getString("Fax").trim();
            }

            if (rs.getString("HomeAddress") == null) {
                this.HomeAddress = null;
            } else {
                this.HomeAddress = rs.getString("HomeAddress").trim();
            }

            if (rs.getString("HomeZipCode") == null) {
                this.HomeZipCode = null;
            } else {
                this.HomeZipCode = rs.getString("HomeZipCode").trim();
            }

            if (rs.getString("HomePhone") == null) {
                this.HomePhone = null;
            } else {
                this.HomePhone = rs.getString("HomePhone").trim();
            }

            if (rs.getString("HomeFax") == null) {
                this.HomeFax = null;
            } else {
                this.HomeFax = rs.getString("HomeFax").trim();
            }

            if (rs.getString("CompanyAddress") == null) {
                this.CompanyAddress = null;
            } else {
                this.CompanyAddress = rs.getString("CompanyAddress").trim();
            }

            if (rs.getString("CompanyZipCode") == null) {
                this.CompanyZipCode = null;
            } else {
                this.CompanyZipCode = rs.getString("CompanyZipCode").trim();
            }

            if (rs.getString("CompanyPhone") == null) {
                this.CompanyPhone = null;
            } else {
                this.CompanyPhone = rs.getString("CompanyPhone").trim();
            }

            if (rs.getString("CompanyFax") == null) {
                this.CompanyFax = null;
            } else {
                this.CompanyFax = rs.getString("CompanyFax").trim();
            }

            if (rs.getString("Mobile") == null) {
                this.Mobile = null;
            } else {
                this.Mobile = rs.getString("Mobile").trim();
            }

            if (rs.getString("EMail") == null) {
                this.EMail = null;
            } else {
                this.EMail = rs.getString("EMail").trim();
            }

            if (rs.getString("BP") == null) {
                this.BP = null;
            } else {
                this.BP = rs.getString("BP").trim();
            }

            if (rs.getString("BatchNo") == null) {
                this.BatchNo = null;
            } else {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            this.BatchDate = rs.getDate("BatchDate");
            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LCChangePhone表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCChangePhoneSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCChangePhoneSchema getSchema() {
        LCChangePhoneSchema aLCChangePhoneSchema = new LCChangePhoneSchema();
        aLCChangePhoneSchema.setSchema(this);
        return aLCChangePhoneSchema;
    }

    public LCChangePhoneDB getDB() {
        LCChangePhoneDB aDBOper = new LCChangePhoneDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCChangePhone描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ChangePhoneDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PostalAddress));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZipCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HomeAddress));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HomeZipCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HomePhone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HomeFax));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyAddress));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyZipCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyPhone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyFax));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EMail));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BP));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BatchNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(BatchDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCChangePhone>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            ChangePhoneDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER));
            PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                 SysConst.PACKAGESPILTER);
            HomeAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            HomeZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            HomePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            HomeFax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            CompanyAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            11, SysConst.PACKAGESPILTER);
            CompanyZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            12, SysConst.PACKAGESPILTER);
            CompanyPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            CompanyFax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                    SysConst.PACKAGESPILTER);
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                   SysConst.PACKAGESPILTER);
            BP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                SysConst.PACKAGESPILTER);
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                     SysConst.PACKAGESPILTER);
            BatchDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCChangePhoneSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("ChangePhoneDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getChangePhoneDate()));
        }
        if (FCode.equals("PostalAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
        }
        if (FCode.equals("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equals("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equals("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equals("HomeAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeAddress));
        }
        if (FCode.equals("HomeZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeZipCode));
        }
        if (FCode.equals("HomePhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomePhone));
        }
        if (FCode.equals("HomeFax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HomeFax));
        }
        if (FCode.equals("CompanyAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyAddress));
        }
        if (FCode.equals("CompanyZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyZipCode));
        }
        if (FCode.equals("CompanyPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyPhone));
        }
        if (FCode.equals("CompanyFax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyFax));
        }
        if (FCode.equals("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equals("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equals("BP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BP));
        }
        if (FCode.equals("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
        }
        if (FCode.equals("BatchDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBatchDate()));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getChangePhoneDate()));
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(PostalAddress);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ZipCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Phone);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Fax);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(HomeAddress);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(HomeZipCode);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(HomePhone);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(HomeFax);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(CompanyAddress);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(CompanyZipCode);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(CompanyPhone);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(CompanyFax);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(Mobile);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(EMail);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(BP);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(BatchNo);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getBatchDate()));
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ChangePhoneDate")) {
            if (FValue != null && !FValue.equals("")) {
                ChangePhoneDate = fDate.getDate(FValue);
            } else {
                ChangePhoneDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            if (FValue != null && !FValue.equals("")) {
                PostalAddress = FValue.trim();
            } else {
                PostalAddress = null;
            }
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if (FValue != null && !FValue.equals("")) {
                ZipCode = FValue.trim();
            } else {
                ZipCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if (FValue != null && !FValue.equals("")) {
                Phone = FValue.trim();
            } else {
                Phone = null;
            }
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if (FValue != null && !FValue.equals("")) {
                Fax = FValue.trim();
            } else {
                Fax = null;
            }
        }
        if (FCode.equalsIgnoreCase("HomeAddress")) {
            if (FValue != null && !FValue.equals("")) {
                HomeAddress = FValue.trim();
            } else {
                HomeAddress = null;
            }
        }
        if (FCode.equalsIgnoreCase("HomeZipCode")) {
            if (FValue != null && !FValue.equals("")) {
                HomeZipCode = FValue.trim();
            } else {
                HomeZipCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("HomePhone")) {
            if (FValue != null && !FValue.equals("")) {
                HomePhone = FValue.trim();
            } else {
                HomePhone = null;
            }
        }
        if (FCode.equalsIgnoreCase("HomeFax")) {
            if (FValue != null && !FValue.equals("")) {
                HomeFax = FValue.trim();
            } else {
                HomeFax = null;
            }
        }
        if (FCode.equalsIgnoreCase("CompanyAddress")) {
            if (FValue != null && !FValue.equals("")) {
                CompanyAddress = FValue.trim();
            } else {
                CompanyAddress = null;
            }
        }
        if (FCode.equalsIgnoreCase("CompanyZipCode")) {
            if (FValue != null && !FValue.equals("")) {
                CompanyZipCode = FValue.trim();
            } else {
                CompanyZipCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("CompanyPhone")) {
            if (FValue != null && !FValue.equals("")) {
                CompanyPhone = FValue.trim();
            } else {
                CompanyPhone = null;
            }
        }
        if (FCode.equalsIgnoreCase("CompanyFax")) {
            if (FValue != null && !FValue.equals("")) {
                CompanyFax = FValue.trim();
            } else {
                CompanyFax = null;
            }
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if (FValue != null && !FValue.equals("")) {
                Mobile = FValue.trim();
            } else {
                Mobile = null;
            }
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if (FValue != null && !FValue.equals("")) {
                EMail = FValue.trim();
            } else {
                EMail = null;
            }
        }
        if (FCode.equalsIgnoreCase("BP")) {
            if (FValue != null && !FValue.equals("")) {
                BP = FValue.trim();
            } else {
                BP = null;
            }
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            if (FValue != null && !FValue.equals("")) {
                BatchNo = FValue.trim();
            } else {
                BatchNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("BatchDate")) {
            if (FValue != null && !FValue.equals("")) {
                BatchDate = fDate.getDate(FValue);
            } else {
                BatchDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LCChangePhoneSchema other = (LCChangePhoneSchema) otherObject;
        return
                ContNo.equals(other.getContNo())
                &&
                fDate.getString(ChangePhoneDate).equals(other.
                getChangePhoneDate())
                && PostalAddress.equals(other.getPostalAddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && Fax.equals(other.getFax())
                && HomeAddress.equals(other.getHomeAddress())
                && HomeZipCode.equals(other.getHomeZipCode())
                && HomePhone.equals(other.getHomePhone())
                && HomeFax.equals(other.getHomeFax())
                && CompanyAddress.equals(other.getCompanyAddress())
                && CompanyZipCode.equals(other.getCompanyZipCode())
                && CompanyPhone.equals(other.getCompanyPhone())
                && CompanyFax.equals(other.getCompanyFax())
                && Mobile.equals(other.getMobile())
                && EMail.equals(other.getEMail())
                && BP.equals(other.getBP())
                && BatchNo.equals(other.getBatchNo())
                && fDate.getString(BatchDate).equals(other.getBatchDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ContNo")) {
            return 0;
        }
        if (strFieldName.equals("ChangePhoneDate")) {
            return 1;
        }
        if (strFieldName.equals("PostalAddress")) {
            return 2;
        }
        if (strFieldName.equals("ZipCode")) {
            return 3;
        }
        if (strFieldName.equals("Phone")) {
            return 4;
        }
        if (strFieldName.equals("Fax")) {
            return 5;
        }
        if (strFieldName.equals("HomeAddress")) {
            return 6;
        }
        if (strFieldName.equals("HomeZipCode")) {
            return 7;
        }
        if (strFieldName.equals("HomePhone")) {
            return 8;
        }
        if (strFieldName.equals("HomeFax")) {
            return 9;
        }
        if (strFieldName.equals("CompanyAddress")) {
            return 10;
        }
        if (strFieldName.equals("CompanyZipCode")) {
            return 11;
        }
        if (strFieldName.equals("CompanyPhone")) {
            return 12;
        }
        if (strFieldName.equals("CompanyFax")) {
            return 13;
        }
        if (strFieldName.equals("Mobile")) {
            return 14;
        }
        if (strFieldName.equals("EMail")) {
            return 15;
        }
        if (strFieldName.equals("BP")) {
            return 16;
        }
        if (strFieldName.equals("BatchNo")) {
            return 17;
        }
        if (strFieldName.equals("BatchDate")) {
            return 18;
        }
        if (strFieldName.equals("Operator")) {
            return 19;
        }
        if (strFieldName.equals("MakeDate")) {
            return 20;
        }
        if (strFieldName.equals("MakeTime")) {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ContNo";
            break;
        case 1:
            strFieldName = "ChangePhoneDate";
            break;
        case 2:
            strFieldName = "PostalAddress";
            break;
        case 3:
            strFieldName = "ZipCode";
            break;
        case 4:
            strFieldName = "Phone";
            break;
        case 5:
            strFieldName = "Fax";
            break;
        case 6:
            strFieldName = "HomeAddress";
            break;
        case 7:
            strFieldName = "HomeZipCode";
            break;
        case 8:
            strFieldName = "HomePhone";
            break;
        case 9:
            strFieldName = "HomeFax";
            break;
        case 10:
            strFieldName = "CompanyAddress";
            break;
        case 11:
            strFieldName = "CompanyZipCode";
            break;
        case 12:
            strFieldName = "CompanyPhone";
            break;
        case 13:
            strFieldName = "CompanyFax";
            break;
        case 14:
            strFieldName = "Mobile";
            break;
        case 15:
            strFieldName = "EMail";
            break;
        case 16:
            strFieldName = "BP";
            break;
        case 17:
            strFieldName = "BatchNo";
            break;
        case 18:
            strFieldName = "BatchDate";
            break;
        case 19:
            strFieldName = "Operator";
            break;
        case 20:
            strFieldName = "MakeDate";
            break;
        case 21:
            strFieldName = "MakeTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChangePhoneDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PostalAddress")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Fax")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeAddress")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeZipCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomePhone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeFax")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CompanyAddress")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CompanyZipCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CompanyPhone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CompanyFax")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Mobile")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EMail")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BP")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BatchNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BatchDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
