/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.BPOIssueBDB;

/*
 * <p>ClassName: BPOIssueBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2014-03-04
 */
public class BPOIssueBSchema implements Schema, Cloneable
{
	// @Field
	/** 处理次数 */
	private int DealCount;
	/** 外包批次 */
	private String BPOBatchNo;
	/** 序号 */
	private int SequenceNo;
	/** 合同id */
	private String ContID;
	/** 错误对象 */
	private String IssueObject;
	/** 错误对象id */
	private String IssueObjectID;
	/** 错误所属模块 */
	private String Model;
	/** 错误类型 */
	private String IssueType;
	/** 问题件字段 */
	private String IssueField;
	/** 字段原始值 */
	private String OriginalInfo;
	/** 状态 */
	private String State;
	/** 备注 */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 印刷号 */
	private String PrtNo;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public BPOIssueBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "DealCount";
		pk[1] = "BPOBatchNo";
		pk[2] = "SequenceNo";
		pk[3] = "ContID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		BPOIssueBSchema cloned = (BPOIssueBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public int getDealCount()
	{
		return DealCount;
	}
	public void setDealCount(int aDealCount)
	{
		DealCount = aDealCount;
	}
	public void setDealCount(String aDealCount)
	{
		if (aDealCount != null && !aDealCount.equals(""))
		{
			Integer tInteger = new Integer(aDealCount);
			int i = tInteger.intValue();
			DealCount = i;
		}
	}

	public String getBPOBatchNo()
	{
		return BPOBatchNo;
	}
	public void setBPOBatchNo(String aBPOBatchNo)
	{
		BPOBatchNo = aBPOBatchNo;
	}
	public int getSequenceNo()
	{
		return SequenceNo;
	}
	public void setSequenceNo(int aSequenceNo)
	{
		SequenceNo = aSequenceNo;
	}
	public void setSequenceNo(String aSequenceNo)
	{
		if (aSequenceNo != null && !aSequenceNo.equals(""))
		{
			Integer tInteger = new Integer(aSequenceNo);
			int i = tInteger.intValue();
			SequenceNo = i;
		}
	}

	public String getContID()
	{
		return ContID;
	}
	public void setContID(String aContID)
	{
		ContID = aContID;
	}
	public String getIssueObject()
	{
		return IssueObject;
	}
	public void setIssueObject(String aIssueObject)
	{
		IssueObject = aIssueObject;
	}
	public String getIssueObjectID()
	{
		return IssueObjectID;
	}
	public void setIssueObjectID(String aIssueObjectID)
	{
		IssueObjectID = aIssueObjectID;
	}
	public String getModel()
	{
		return Model;
	}
	public void setModel(String aModel)
	{
		Model = aModel;
	}
	public String getIssueType()
	{
		return IssueType;
	}
	public void setIssueType(String aIssueType)
	{
		IssueType = aIssueType;
	}
	public String getIssueField()
	{
		return IssueField;
	}
	public void setIssueField(String aIssueField)
	{
		IssueField = aIssueField;
	}
	public String getOriginalInfo()
	{
		return OriginalInfo;
	}
	public void setOriginalInfo(String aOriginalInfo)
	{
		OriginalInfo = aOriginalInfo;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}

	/**
	* 使用另外一个 BPOIssueBSchema 对象给 Schema 赋值
	* @param: aBPOIssueBSchema BPOIssueBSchema
	**/
	public void setSchema(BPOIssueBSchema aBPOIssueBSchema)
	{
		this.DealCount = aBPOIssueBSchema.getDealCount();
		this.BPOBatchNo = aBPOIssueBSchema.getBPOBatchNo();
		this.SequenceNo = aBPOIssueBSchema.getSequenceNo();
		this.ContID = aBPOIssueBSchema.getContID();
		this.IssueObject = aBPOIssueBSchema.getIssueObject();
		this.IssueObjectID = aBPOIssueBSchema.getIssueObjectID();
		this.Model = aBPOIssueBSchema.getModel();
		this.IssueType = aBPOIssueBSchema.getIssueType();
		this.IssueField = aBPOIssueBSchema.getIssueField();
		this.OriginalInfo = aBPOIssueBSchema.getOriginalInfo();
		this.State = aBPOIssueBSchema.getState();
		this.Remark = aBPOIssueBSchema.getRemark();
		this.Operator = aBPOIssueBSchema.getOperator();
		this.MakeDate = fDate.getDate( aBPOIssueBSchema.getMakeDate());
		this.MakeTime = aBPOIssueBSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aBPOIssueBSchema.getModifyDate());
		this.ModifyTime = aBPOIssueBSchema.getModifyTime();
		this.PrtNo = aBPOIssueBSchema.getPrtNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.DealCount = rs.getInt("DealCount");
			if( rs.getString("BPOBatchNo") == null )
				this.BPOBatchNo = null;
			else
				this.BPOBatchNo = rs.getString("BPOBatchNo").trim();

			this.SequenceNo = rs.getInt("SequenceNo");
			if( rs.getString("ContID") == null )
				this.ContID = null;
			else
				this.ContID = rs.getString("ContID").trim();

			if( rs.getString("IssueObject") == null )
				this.IssueObject = null;
			else
				this.IssueObject = rs.getString("IssueObject").trim();

			if( rs.getString("IssueObjectID") == null )
				this.IssueObjectID = null;
			else
				this.IssueObjectID = rs.getString("IssueObjectID").trim();

			if( rs.getString("Model") == null )
				this.Model = null;
			else
				this.Model = rs.getString("Model").trim();

			if( rs.getString("IssueType") == null )
				this.IssueType = null;
			else
				this.IssueType = rs.getString("IssueType").trim();

			if( rs.getString("IssueField") == null )
				this.IssueField = null;
			else
				this.IssueField = rs.getString("IssueField").trim();

			if( rs.getString("OriginalInfo") == null )
				this.OriginalInfo = null;
			else
				this.OriginalInfo = rs.getString("OriginalInfo").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的BPOIssueB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOIssueBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public BPOIssueBSchema getSchema()
	{
		BPOIssueBSchema aBPOIssueBSchema = new BPOIssueBSchema();
		aBPOIssueBSchema.setSchema(this);
		return aBPOIssueBSchema;
	}

	public BPOIssueBDB getDB()
	{
		BPOIssueBDB aDBOper = new BPOIssueBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOIssueB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append( ChgData.chgData(DealCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BPOBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SequenceNo));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueObject)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueObjectID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Model)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueField)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OriginalInfo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOIssueB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DealCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).intValue();
			BPOBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SequenceNo= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).intValue();
			ContID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			IssueObject = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IssueObjectID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Model = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			IssueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			IssueField = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			OriginalInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOIssueBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DealCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealCount));
		}
		if (FCode.equals("BPOBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BPOBatchNo));
		}
		if (FCode.equals("SequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
		}
		if (FCode.equals("ContID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
		}
		if (FCode.equals("IssueObject"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueObject));
		}
		if (FCode.equals("IssueObjectID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueObjectID));
		}
		if (FCode.equals("Model"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Model));
		}
		if (FCode.equals("IssueType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueType));
		}
		if (FCode.equals("IssueField"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueField));
		}
		if (FCode.equals("OriginalInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OriginalInfo));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(DealCount);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BPOBatchNo);
				break;
			case 2:
				strFieldValue = String.valueOf(SequenceNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ContID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(IssueObject);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IssueObjectID);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Model);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(IssueType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(IssueField);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(OriginalInfo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DealCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DealCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("BPOBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BPOBatchNo = FValue.trim();
			}
			else
				BPOBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("SequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SequenceNo = i;
			}
		}
		if (FCode.equalsIgnoreCase("ContID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContID = FValue.trim();
			}
			else
				ContID = null;
		}
		if (FCode.equalsIgnoreCase("IssueObject"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueObject = FValue.trim();
			}
			else
				IssueObject = null;
		}
		if (FCode.equalsIgnoreCase("IssueObjectID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueObjectID = FValue.trim();
			}
			else
				IssueObjectID = null;
		}
		if (FCode.equalsIgnoreCase("Model"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Model = FValue.trim();
			}
			else
				Model = null;
		}
		if (FCode.equalsIgnoreCase("IssueType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueType = FValue.trim();
			}
			else
				IssueType = null;
		}
		if (FCode.equalsIgnoreCase("IssueField"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueField = FValue.trim();
			}
			else
				IssueField = null;
		}
		if (FCode.equalsIgnoreCase("OriginalInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OriginalInfo = FValue.trim();
			}
			else
				OriginalInfo = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		BPOIssueBSchema other = (BPOIssueBSchema)otherObject;
		return
			DealCount == other.getDealCount()
			&& (BPOBatchNo == null ? other.getBPOBatchNo() == null : BPOBatchNo.equals(other.getBPOBatchNo()))
			&& SequenceNo == other.getSequenceNo()
			&& (ContID == null ? other.getContID() == null : ContID.equals(other.getContID()))
			&& (IssueObject == null ? other.getIssueObject() == null : IssueObject.equals(other.getIssueObject()))
			&& (IssueObjectID == null ? other.getIssueObjectID() == null : IssueObjectID.equals(other.getIssueObjectID()))
			&& (Model == null ? other.getModel() == null : Model.equals(other.getModel()))
			&& (IssueType == null ? other.getIssueType() == null : IssueType.equals(other.getIssueType()))
			&& (IssueField == null ? other.getIssueField() == null : IssueField.equals(other.getIssueField()))
			&& (OriginalInfo == null ? other.getOriginalInfo() == null : OriginalInfo.equals(other.getOriginalInfo()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DealCount") ) {
			return 0;
		}
		if( strFieldName.equals("BPOBatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return 2;
		}
		if( strFieldName.equals("ContID") ) {
			return 3;
		}
		if( strFieldName.equals("IssueObject") ) {
			return 4;
		}
		if( strFieldName.equals("IssueObjectID") ) {
			return 5;
		}
		if( strFieldName.equals("Model") ) {
			return 6;
		}
		if( strFieldName.equals("IssueType") ) {
			return 7;
		}
		if( strFieldName.equals("IssueField") ) {
			return 8;
		}
		if( strFieldName.equals("OriginalInfo") ) {
			return 9;
		}
		if( strFieldName.equals("State") ) {
			return 10;
		}
		if( strFieldName.equals("Remark") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DealCount";
				break;
			case 1:
				strFieldName = "BPOBatchNo";
				break;
			case 2:
				strFieldName = "SequenceNo";
				break;
			case 3:
				strFieldName = "ContID";
				break;
			case 4:
				strFieldName = "IssueObject";
				break;
			case 5:
				strFieldName = "IssueObjectID";
				break;
			case 6:
				strFieldName = "Model";
				break;
			case 7:
				strFieldName = "IssueType";
				break;
			case 8:
				strFieldName = "IssueField";
				break;
			case 9:
				strFieldName = "OriginalInfo";
				break;
			case 10:
				strFieldName = "State";
				break;
			case 11:
				strFieldName = "Remark";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			case 17:
				strFieldName = "PrtNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DealCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BPOBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ContID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueObject") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueObjectID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Model") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueField") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OriginalInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_INT;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_INT;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
