/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseImportDB;

/*
 * <p>ClassName: LLCaseImportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LLCaseImport
 * @CreateDate：2008-01-30
 */
public class LLCaseImportSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String RgtNo;
	/** 案件号 */
	private String CaseNo;
	/** 姓名 */
	private String CustomerName;
	/** 身份证号 */
	private String IDNo;
	/** 医保号 */
	private String SecurityNo;
	/** 人员类别 */
	private String InsuredStat;
	/** 性别 */
	private String Sex;
	/** 医院名称 */
	private String HospitalName;
	/** 医院编码 */
	private String HospitalCode;
	/** 就诊类别 */
	private String FeeType;
	/** 账单号码 */
	private String ReceiptNo;
	/** 账单日期 */
	private Date FeeDate;
	/** 入院日期 */
	private Date HospStartDate;
	/** 出院日期 */
	private Date HospEndDate;
	/** 住院天数 */
	private int RealHospDate;
	/** 费用总额 */
	private double ApplyAmnt;
	/** 起付线 */
	private double GetLimit;
	/** 帐户支付 */
	private double AccountPay;
	/** 统筹支付 */
	private double PlanFee;
	/** 统筹自付 */
	private double MidAmnt;
	/** 大额救助支付 */
	private double SupInHosFee;
	/** 大额救助自付 */
	private double HighAmnt1;
	/** 个人自付 */
	private double SelfPay1;
	/** 部分自付 */
	private double SelfPay2;
	/** 全自费 */
	private double SelfAmnt;
	/** 第三方支付 */
	private double OtherOrganAmnt;
	/** 疾病诊断 */
	private String DiseaseName;
	/** 疾病编码 */
	private String DiseaseCode;
	/** 发生日期 */
	private Date AccDate;
	/** 实赔金额 */
	private double AffixNo;
	/** 特需险标志 */
	private String OriginFlag;
	/** 导入状态编码 */
	private String ImportState;
	/** 导入机构 */
	private String ManageCom;
	/** 导入人员 */
	private String Operator;
	/** 导入日期 */
	private Date MakeDate;
	/** 导入时间 */
	private String MakeTime;

	public static final int FIELDNUM = 36;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCaseImportSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "RgtNo";
		pk[1] = "CaseNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LLCaseImportSchema cloned = (LLCaseImportSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
            RgtNo = aRgtNo;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
            CaseNo = aCaseNo;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
            CustomerName = aCustomerName;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
            IDNo = aIDNo;
	}
	public String getSecurityNo()
	{
		return SecurityNo;
	}
	public void setSecurityNo(String aSecurityNo)
	{
            SecurityNo = aSecurityNo;
	}
	public String getInsuredStat()
	{
		return InsuredStat;
	}
	public void setInsuredStat(String aInsuredStat)
	{
            InsuredStat = aInsuredStat;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
            Sex = aSex;
	}
	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
            HospitalName = aHospitalName;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
            HospitalCode = aHospitalCode;
	}
	public String getFeeType()
	{
		return FeeType;
	}
	public void setFeeType(String aFeeType)
	{
            FeeType = aFeeType;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
            ReceiptNo = aReceiptNo;
	}
	public String getFeeDate()
	{
		if( FeeDate != null )
			return fDate.getString(FeeDate);
		else
			return null;
	}
	public void setFeeDate(Date aFeeDate)
	{
            FeeDate = aFeeDate;
	}
	public void setFeeDate(String aFeeDate)
	{
		if (aFeeDate != null && !aFeeDate.equals("") )
		{
			FeeDate = fDate.getDate( aFeeDate );
		}
		else
			FeeDate = null;
	}

	public String getHospStartDate()
	{
		if( HospStartDate != null )
			return fDate.getString(HospStartDate);
		else
			return null;
	}
	public void setHospStartDate(Date aHospStartDate)
	{
            HospStartDate = aHospStartDate;
	}
	public void setHospStartDate(String aHospStartDate)
	{
		if (aHospStartDate != null && !aHospStartDate.equals("") )
		{
			HospStartDate = fDate.getDate( aHospStartDate );
		}
		else
			HospStartDate = null;
	}

	public String getHospEndDate()
	{
		if( HospEndDate != null )
			return fDate.getString(HospEndDate);
		else
			return null;
	}
	public void setHospEndDate(Date aHospEndDate)
	{
            HospEndDate = aHospEndDate;
	}
	public void setHospEndDate(String aHospEndDate)
	{
		if (aHospEndDate != null && !aHospEndDate.equals("") )
		{
			HospEndDate = fDate.getDate( aHospEndDate );
		}
		else
			HospEndDate = null;
	}

	public int getRealHospDate()
	{
		return RealHospDate;
	}
	public void setRealHospDate(int aRealHospDate)
	{
            RealHospDate = aRealHospDate;
	}
	public void setRealHospDate(String aRealHospDate)
	{
		if (aRealHospDate != null && !aRealHospDate.equals(""))
		{
			Integer tInteger = new Integer(aRealHospDate);
			int i = tInteger.intValue();
			RealHospDate = i;
		}
	}

	public double getApplyAmnt()
	{
		return ApplyAmnt;
	}
	public void setApplyAmnt(double aApplyAmnt)
	{
            ApplyAmnt = Arith.round(aApplyAmnt,2);
	}
	public void setApplyAmnt(String aApplyAmnt)
	{
		if (aApplyAmnt != null && !aApplyAmnt.equals(""))
		{
			Double tDouble = new Double(aApplyAmnt);
			double d = tDouble.doubleValue();
                ApplyAmnt = Arith.round(d,2);
		}
	}

	public double getGetLimit()
	{
		return GetLimit;
	}
	public void setGetLimit(double aGetLimit)
	{
            GetLimit = Arith.round(aGetLimit,2);
	}
	public void setGetLimit(String aGetLimit)
	{
		if (aGetLimit != null && !aGetLimit.equals(""))
		{
			Double tDouble = new Double(aGetLimit);
			double d = tDouble.doubleValue();
                GetLimit = Arith.round(d,2);
		}
	}

	public double getAccountPay()
	{
		return AccountPay;
	}
	public void setAccountPay(double aAccountPay)
	{
            AccountPay = Arith.round(aAccountPay,2);
	}
	public void setAccountPay(String aAccountPay)
	{
		if (aAccountPay != null && !aAccountPay.equals(""))
		{
			Double tDouble = new Double(aAccountPay);
			double d = tDouble.doubleValue();
                AccountPay = Arith.round(d,2);
		}
	}

	public double getPlanFee()
	{
		return PlanFee;
	}
	public void setPlanFee(double aPlanFee)
	{
            PlanFee = Arith.round(aPlanFee,2);
	}
	public void setPlanFee(String aPlanFee)
	{
		if (aPlanFee != null && !aPlanFee.equals(""))
		{
			Double tDouble = new Double(aPlanFee);
			double d = tDouble.doubleValue();
                PlanFee = Arith.round(d,2);
		}
	}

	public double getMidAmnt()
	{
		return MidAmnt;
	}
	public void setMidAmnt(double aMidAmnt)
	{
            MidAmnt = Arith.round(aMidAmnt,2);
	}
	public void setMidAmnt(String aMidAmnt)
	{
		if (aMidAmnt != null && !aMidAmnt.equals(""))
		{
			Double tDouble = new Double(aMidAmnt);
			double d = tDouble.doubleValue();
                MidAmnt = Arith.round(d,2);
		}
	}

	public double getSupInHosFee()
	{
		return SupInHosFee;
	}
	public void setSupInHosFee(double aSupInHosFee)
	{
            SupInHosFee = Arith.round(aSupInHosFee,2);
	}
	public void setSupInHosFee(String aSupInHosFee)
	{
		if (aSupInHosFee != null && !aSupInHosFee.equals(""))
		{
			Double tDouble = new Double(aSupInHosFee);
			double d = tDouble.doubleValue();
                SupInHosFee = Arith.round(d,2);
		}
	}

	public double getHighAmnt1()
	{
		return HighAmnt1;
	}
	public void setHighAmnt1(double aHighAmnt1)
	{
            HighAmnt1 = Arith.round(aHighAmnt1,2);
	}
	public void setHighAmnt1(String aHighAmnt1)
	{
		if (aHighAmnt1 != null && !aHighAmnt1.equals(""))
		{
			Double tDouble = new Double(aHighAmnt1);
			double d = tDouble.doubleValue();
                HighAmnt1 = Arith.round(d,2);
		}
	}

	public double getSelfPay1()
	{
		return SelfPay1;
	}
	public void setSelfPay1(double aSelfPay1)
	{
            SelfPay1 = Arith.round(aSelfPay1,2);
	}
	public void setSelfPay1(String aSelfPay1)
	{
		if (aSelfPay1 != null && !aSelfPay1.equals(""))
		{
			Double tDouble = new Double(aSelfPay1);
			double d = tDouble.doubleValue();
                SelfPay1 = Arith.round(d,2);
		}
	}

	public double getSelfPay2()
	{
		return SelfPay2;
	}
	public void setSelfPay2(double aSelfPay2)
	{
            SelfPay2 = Arith.round(aSelfPay2,2);
	}
	public void setSelfPay2(String aSelfPay2)
	{
		if (aSelfPay2 != null && !aSelfPay2.equals(""))
		{
			Double tDouble = new Double(aSelfPay2);
			double d = tDouble.doubleValue();
                SelfPay2 = Arith.round(d,2);
		}
	}

	public double getSelfAmnt()
	{
		return SelfAmnt;
	}
	public void setSelfAmnt(double aSelfAmnt)
	{
            SelfAmnt = Arith.round(aSelfAmnt,2);
	}
	public void setSelfAmnt(String aSelfAmnt)
	{
		if (aSelfAmnt != null && !aSelfAmnt.equals(""))
		{
			Double tDouble = new Double(aSelfAmnt);
			double d = tDouble.doubleValue();
                SelfAmnt = Arith.round(d,2);
		}
	}

	public double getOtherOrganAmnt()
	{
		return OtherOrganAmnt;
	}
	public void setOtherOrganAmnt(double aOtherOrganAmnt)
	{
            OtherOrganAmnt = Arith.round(aOtherOrganAmnt,2);
	}
	public void setOtherOrganAmnt(String aOtherOrganAmnt)
	{
		if (aOtherOrganAmnt != null && !aOtherOrganAmnt.equals(""))
		{
			Double tDouble = new Double(aOtherOrganAmnt);
			double d = tDouble.doubleValue();
                OtherOrganAmnt = Arith.round(d,2);
		}
	}

	public String getDiseaseName()
	{
		return DiseaseName;
	}
	public void setDiseaseName(String aDiseaseName)
	{
            DiseaseName = aDiseaseName;
	}
	public String getDiseaseCode()
	{
		return DiseaseCode;
	}
	public void setDiseaseCode(String aDiseaseCode)
	{
            DiseaseCode = aDiseaseCode;
	}
	public String getAccDate()
	{
		if( AccDate != null )
			return fDate.getString(AccDate);
		else
			return null;
	}
	public void setAccDate(Date aAccDate)
	{
            AccDate = aAccDate;
	}
	public void setAccDate(String aAccDate)
	{
		if (aAccDate != null && !aAccDate.equals("") )
		{
			AccDate = fDate.getDate( aAccDate );
		}
		else
			AccDate = null;
	}

	public double getAffixNo()
	{
		return AffixNo;
	}
	public void setAffixNo(double aAffixNo)
	{
            AffixNo = Arith.round(aAffixNo,2);
	}
	public void setAffixNo(String aAffixNo)
	{
		if (aAffixNo != null && !aAffixNo.equals(""))
		{
			Double tDouble = new Double(aAffixNo);
			double d = tDouble.doubleValue();
                AffixNo = Arith.round(d,2);
		}
	}

	public String getOriginFlag()
	{
		return OriginFlag;
	}
	public void setOriginFlag(String aOriginFlag)
	{
            OriginFlag = aOriginFlag;
	}
	public String getImportState()
	{
		return ImportState;
	}
	public void setImportState(String aImportState)
	{
            ImportState = aImportState;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}

	/**
	* 使用另外一个 LLCaseImportSchema 对象给 Schema 赋值
	* @param: aLLCaseImportSchema LLCaseImportSchema
	**/
	public void setSchema(LLCaseImportSchema aLLCaseImportSchema)
	{
		this.RgtNo = aLLCaseImportSchema.getRgtNo();
		this.CaseNo = aLLCaseImportSchema.getCaseNo();
		this.CustomerName = aLLCaseImportSchema.getCustomerName();
		this.IDNo = aLLCaseImportSchema.getIDNo();
		this.SecurityNo = aLLCaseImportSchema.getSecurityNo();
		this.InsuredStat = aLLCaseImportSchema.getInsuredStat();
		this.Sex = aLLCaseImportSchema.getSex();
		this.HospitalName = aLLCaseImportSchema.getHospitalName();
		this.HospitalCode = aLLCaseImportSchema.getHospitalCode();
		this.FeeType = aLLCaseImportSchema.getFeeType();
		this.ReceiptNo = aLLCaseImportSchema.getReceiptNo();
		this.FeeDate = fDate.getDate( aLLCaseImportSchema.getFeeDate());
		this.HospStartDate = fDate.getDate( aLLCaseImportSchema.getHospStartDate());
		this.HospEndDate = fDate.getDate( aLLCaseImportSchema.getHospEndDate());
		this.RealHospDate = aLLCaseImportSchema.getRealHospDate();
		this.ApplyAmnt = aLLCaseImportSchema.getApplyAmnt();
		this.GetLimit = aLLCaseImportSchema.getGetLimit();
		this.AccountPay = aLLCaseImportSchema.getAccountPay();
		this.PlanFee = aLLCaseImportSchema.getPlanFee();
		this.MidAmnt = aLLCaseImportSchema.getMidAmnt();
		this.SupInHosFee = aLLCaseImportSchema.getSupInHosFee();
		this.HighAmnt1 = aLLCaseImportSchema.getHighAmnt1();
		this.SelfPay1 = aLLCaseImportSchema.getSelfPay1();
		this.SelfPay2 = aLLCaseImportSchema.getSelfPay2();
		this.SelfAmnt = aLLCaseImportSchema.getSelfAmnt();
		this.OtherOrganAmnt = aLLCaseImportSchema.getOtherOrganAmnt();
		this.DiseaseName = aLLCaseImportSchema.getDiseaseName();
		this.DiseaseCode = aLLCaseImportSchema.getDiseaseCode();
		this.AccDate = fDate.getDate( aLLCaseImportSchema.getAccDate());
		this.AffixNo = aLLCaseImportSchema.getAffixNo();
		this.OriginFlag = aLLCaseImportSchema.getOriginFlag();
		this.ImportState = aLLCaseImportSchema.getImportState();
		this.ManageCom = aLLCaseImportSchema.getManageCom();
		this.Operator = aLLCaseImportSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLCaseImportSchema.getMakeDate());
		this.MakeTime = aLLCaseImportSchema.getMakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("SecurityNo") == null )
				this.SecurityNo = null;
			else
				this.SecurityNo = rs.getString("SecurityNo").trim();

			if( rs.getString("InsuredStat") == null )
				this.InsuredStat = null;
			else
				this.InsuredStat = rs.getString("InsuredStat").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			if( rs.getString("FeeType") == null )
				this.FeeType = null;
			else
				this.FeeType = rs.getString("FeeType").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			this.FeeDate = rs.getDate("FeeDate");
			this.HospStartDate = rs.getDate("HospStartDate");
			this.HospEndDate = rs.getDate("HospEndDate");
			this.RealHospDate = rs.getInt("RealHospDate");
			this.ApplyAmnt = rs.getDouble("ApplyAmnt");
			this.GetLimit = rs.getDouble("GetLimit");
			this.AccountPay = rs.getDouble("AccountPay");
			this.PlanFee = rs.getDouble("PlanFee");
			this.MidAmnt = rs.getDouble("MidAmnt");
			this.SupInHosFee = rs.getDouble("SupInHosFee");
			this.HighAmnt1 = rs.getDouble("HighAmnt1");
			this.SelfPay1 = rs.getDouble("SelfPay1");
			this.SelfPay2 = rs.getDouble("SelfPay2");
			this.SelfAmnt = rs.getDouble("SelfAmnt");
			this.OtherOrganAmnt = rs.getDouble("OtherOrganAmnt");
			if( rs.getString("DiseaseName") == null )
				this.DiseaseName = null;
			else
				this.DiseaseName = rs.getString("DiseaseName").trim();

			if( rs.getString("DiseaseCode") == null )
				this.DiseaseCode = null;
			else
				this.DiseaseCode = rs.getString("DiseaseCode").trim();

			this.AccDate = rs.getDate("AccDate");
			this.AffixNo = rs.getDouble("AffixNo");
			if( rs.getString("OriginFlag") == null )
				this.OriginFlag = null;
			else
				this.OriginFlag = rs.getString("OriginFlag").trim();

			if( rs.getString("ImportState") == null )
				this.ImportState = null;
			else
				this.ImportState = rs.getString("ImportState").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCaseImport表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseImportSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCaseImportSchema getSchema()
	{
		LLCaseImportSchema aLLCaseImportSchema = new LLCaseImportSchema();
		aLLCaseImportSchema.setSchema(this);
		return aLLCaseImportSchema;
	}

	public LLCaseImportDB getDB()
	{
		LLCaseImportDB aDBOper = new LLCaseImportDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseImport描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SecurityNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InsuredStat)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FeeType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( FeeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( HospStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( HospEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RealHospDate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ApplyAmnt));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(GetLimit));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AccountPay));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PlanFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MidAmnt));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SupInHosFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(HighAmnt1));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SelfPay1));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SelfPay2));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SelfAmnt));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(OtherOrganAmnt));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DiseaseName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DiseaseCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( AccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AffixNo));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OriginFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ImportState)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseImport>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SecurityNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			InsuredStat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			FeeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			HospStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			HospEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			RealHospDate= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).intValue();
			ApplyAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			AccountPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			PlanFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			MidAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			SupInHosFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			HighAmnt1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			SelfPay1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			SelfPay2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			SelfAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			OtherOrganAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			DiseaseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			DiseaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			AccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			AffixNo = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			OriginFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			ImportState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseImportSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("SecurityNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecurityNo));
		}
		if (FCode.equals("InsuredStat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredStat));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("FeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("FeeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
		}
		if (FCode.equals("HospStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
		}
		if (FCode.equals("HospEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
		}
		if (FCode.equals("RealHospDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealHospDate));
		}
		if (FCode.equals("ApplyAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyAmnt));
		}
		if (FCode.equals("GetLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
		}
		if (FCode.equals("AccountPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountPay));
		}
		if (FCode.equals("PlanFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlanFee));
		}
		if (FCode.equals("MidAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MidAmnt));
		}
		if (FCode.equals("SupInHosFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupInHosFee));
		}
		if (FCode.equals("HighAmnt1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HighAmnt1));
		}
		if (FCode.equals("SelfPay1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay1));
		}
		if (FCode.equals("SelfPay2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay2));
		}
		if (FCode.equals("SelfAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfAmnt));
		}
		if (FCode.equals("OtherOrganAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherOrganAmnt));
		}
		if (FCode.equals("DiseaseName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseName));
		}
		if (FCode.equals("DiseaseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseCode));
		}
		if (FCode.equals("AccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccDate()));
		}
		if (FCode.equals("AffixNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AffixNo));
		}
		if (FCode.equals("OriginFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OriginFlag));
		}
		if (FCode.equals("ImportState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportState));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SecurityNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(InsuredStat);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(FeeType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
				break;
			case 14:
				strFieldValue = String.valueOf(RealHospDate);
				break;
			case 15:
				strFieldValue = String.valueOf(ApplyAmnt);
				break;
			case 16:
				strFieldValue = String.valueOf(GetLimit);
				break;
			case 17:
				strFieldValue = String.valueOf(AccountPay);
				break;
			case 18:
				strFieldValue = String.valueOf(PlanFee);
				break;
			case 19:
				strFieldValue = String.valueOf(MidAmnt);
				break;
			case 20:
				strFieldValue = String.valueOf(SupInHosFee);
				break;
			case 21:
				strFieldValue = String.valueOf(HighAmnt1);
				break;
			case 22:
				strFieldValue = String.valueOf(SelfPay1);
				break;
			case 23:
				strFieldValue = String.valueOf(SelfPay2);
				break;
			case 24:
				strFieldValue = String.valueOf(SelfAmnt);
				break;
			case 25:
				strFieldValue = String.valueOf(OtherOrganAmnt);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(DiseaseName);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(DiseaseCode);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccDate()));
				break;
			case 29:
				strFieldValue = String.valueOf(AffixNo);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(OriginFlag);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(ImportState);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("SecurityNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecurityNo = FValue.trim();
			}
			else
				SecurityNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredStat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredStat = FValue.trim();
			}
			else
				InsuredStat = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("FeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeType = FValue.trim();
			}
			else
				FeeType = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FeeDate = fDate.getDate( FValue );
			}
			else
				FeeDate = null;
		}
		if (FCode.equalsIgnoreCase("HospStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospStartDate = fDate.getDate( FValue );
			}
			else
				HospStartDate = null;
		}
		if (FCode.equalsIgnoreCase("HospEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospEndDate = fDate.getDate( FValue );
			}
			else
				HospEndDate = null;
		}
		if (FCode.equalsIgnoreCase("RealHospDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RealHospDate = i;
			}
		}
		if (FCode.equalsIgnoreCase("ApplyAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ApplyAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GetLimit = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccountPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccountPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("PlanFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PlanFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("MidAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MidAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SupInHosFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SupInHosFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("HighAmnt1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HighAmnt1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfPay1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfPay1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfPay2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfPay2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherOrganAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherOrganAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("DiseaseName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaseName = FValue.trim();
			}
			else
				DiseaseName = null;
		}
		if (FCode.equalsIgnoreCase("DiseaseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaseCode = FValue.trim();
			}
			else
				DiseaseCode = null;
		}
		if (FCode.equalsIgnoreCase("AccDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccDate = fDate.getDate( FValue );
			}
			else
				AccDate = null;
		}
		if (FCode.equalsIgnoreCase("AffixNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AffixNo = d;
			}
		}
		if (FCode.equalsIgnoreCase("OriginFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OriginFlag = FValue.trim();
			}
			else
				OriginFlag = null;
		}
		if (FCode.equalsIgnoreCase("ImportState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ImportState = FValue.trim();
			}
			else
				ImportState = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCaseImportSchema other = (LLCaseImportSchema)otherObject;
		return
			RgtNo.equals(other.getRgtNo())
			&& CaseNo.equals(other.getCaseNo())
			&& CustomerName.equals(other.getCustomerName())
			&& IDNo.equals(other.getIDNo())
			&& SecurityNo.equals(other.getSecurityNo())
			&& InsuredStat.equals(other.getInsuredStat())
			&& Sex.equals(other.getSex())
			&& HospitalName.equals(other.getHospitalName())
			&& HospitalCode.equals(other.getHospitalCode())
			&& FeeType.equals(other.getFeeType())
			&& ReceiptNo.equals(other.getReceiptNo())
			&& fDate.getString(FeeDate).equals(other.getFeeDate())
			&& fDate.getString(HospStartDate).equals(other.getHospStartDate())
			&& fDate.getString(HospEndDate).equals(other.getHospEndDate())
			&& RealHospDate == other.getRealHospDate()
			&& ApplyAmnt == other.getApplyAmnt()
			&& GetLimit == other.getGetLimit()
			&& AccountPay == other.getAccountPay()
			&& PlanFee == other.getPlanFee()
			&& MidAmnt == other.getMidAmnt()
			&& SupInHosFee == other.getSupInHosFee()
			&& HighAmnt1 == other.getHighAmnt1()
			&& SelfPay1 == other.getSelfPay1()
			&& SelfPay2 == other.getSelfPay2()
			&& SelfAmnt == other.getSelfAmnt()
			&& OtherOrganAmnt == other.getOtherOrganAmnt()
			&& DiseaseName.equals(other.getDiseaseName())
			&& DiseaseCode.equals(other.getDiseaseCode())
			&& fDate.getString(AccDate).equals(other.getAccDate())
			&& AffixNo == other.getAffixNo()
			&& OriginFlag.equals(other.getOriginFlag())
			&& ImportState.equals(other.getImportState())
			&& ManageCom.equals(other.getManageCom())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RgtNo") ) {
			return 0;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 1;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 2;
		}
		if( strFieldName.equals("IDNo") ) {
			return 3;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return 4;
		}
		if( strFieldName.equals("InsuredStat") ) {
			return 5;
		}
		if( strFieldName.equals("Sex") ) {
			return 6;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 7;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 8;
		}
		if( strFieldName.equals("FeeType") ) {
			return 9;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 10;
		}
		if( strFieldName.equals("FeeDate") ) {
			return 11;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return 12;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return 13;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return 14;
		}
		if( strFieldName.equals("ApplyAmnt") ) {
			return 15;
		}
		if( strFieldName.equals("GetLimit") ) {
			return 16;
		}
		if( strFieldName.equals("AccountPay") ) {
			return 17;
		}
		if( strFieldName.equals("PlanFee") ) {
			return 18;
		}
		if( strFieldName.equals("MidAmnt") ) {
			return 19;
		}
		if( strFieldName.equals("SupInHosFee") ) {
			return 20;
		}
		if( strFieldName.equals("HighAmnt1") ) {
			return 21;
		}
		if( strFieldName.equals("SelfPay1") ) {
			return 22;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return 23;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return 24;
		}
		if( strFieldName.equals("OtherOrganAmnt") ) {
			return 25;
		}
		if( strFieldName.equals("DiseaseName") ) {
			return 26;
		}
		if( strFieldName.equals("DiseaseCode") ) {
			return 27;
		}
		if( strFieldName.equals("AccDate") ) {
			return 28;
		}
		if( strFieldName.equals("AffixNo") ) {
			return 29;
		}
		if( strFieldName.equals("OriginFlag") ) {
			return 30;
		}
		if( strFieldName.equals("ImportState") ) {
			return 31;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 32;
		}
		if( strFieldName.equals("Operator") ) {
			return 33;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 34;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 35;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RgtNo";
				break;
			case 1:
				strFieldName = "CaseNo";
				break;
			case 2:
				strFieldName = "CustomerName";
				break;
			case 3:
				strFieldName = "IDNo";
				break;
			case 4:
				strFieldName = "SecurityNo";
				break;
			case 5:
				strFieldName = "InsuredStat";
				break;
			case 6:
				strFieldName = "Sex";
				break;
			case 7:
				strFieldName = "HospitalName";
				break;
			case 8:
				strFieldName = "HospitalCode";
				break;
			case 9:
				strFieldName = "FeeType";
				break;
			case 10:
				strFieldName = "ReceiptNo";
				break;
			case 11:
				strFieldName = "FeeDate";
				break;
			case 12:
				strFieldName = "HospStartDate";
				break;
			case 13:
				strFieldName = "HospEndDate";
				break;
			case 14:
				strFieldName = "RealHospDate";
				break;
			case 15:
				strFieldName = "ApplyAmnt";
				break;
			case 16:
				strFieldName = "GetLimit";
				break;
			case 17:
				strFieldName = "AccountPay";
				break;
			case 18:
				strFieldName = "PlanFee";
				break;
			case 19:
				strFieldName = "MidAmnt";
				break;
			case 20:
				strFieldName = "SupInHosFee";
				break;
			case 21:
				strFieldName = "HighAmnt1";
				break;
			case 22:
				strFieldName = "SelfPay1";
				break;
			case 23:
				strFieldName = "SelfPay2";
				break;
			case 24:
				strFieldName = "SelfAmnt";
				break;
			case 25:
				strFieldName = "OtherOrganAmnt";
				break;
			case 26:
				strFieldName = "DiseaseName";
				break;
			case 27:
				strFieldName = "DiseaseCode";
				break;
			case 28:
				strFieldName = "AccDate";
				break;
			case 29:
				strFieldName = "AffixNo";
				break;
			case 30:
				strFieldName = "OriginFlag";
				break;
			case 31:
				strFieldName = "ImportState";
				break;
			case 32:
				strFieldName = "ManageCom";
				break;
			case 33:
				strFieldName = "Operator";
				break;
			case 34:
				strFieldName = "MakeDate";
				break;
			case 35:
				strFieldName = "MakeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredStat") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RealHospDate") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ApplyAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccountPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PlanFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MidAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SupInHosFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HighAmnt1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfPay1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherOrganAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DiseaseName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiseaseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AffixNo") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OriginFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_INT;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
