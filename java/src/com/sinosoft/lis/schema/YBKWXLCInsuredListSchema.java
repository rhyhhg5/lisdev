/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
/*
import com.sinosoft.lis.db.HISLCInsuredListDB;*/

/*
 * <p>ClassName: HISLCInsuredListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: Physical Data _1
 * @CreateDate：2015-08-15
 */
public class YBKWXLCInsuredListSchema 
{
	// @Field
	/** 流水号 */
	private String SerNo;
	/** 团体编码 */
	private String GrpNo;
	/** 单证流水号 */
	private String PrtNo;
	/** 批次号 */
	private String BatchNo;
	/** 保单号码 */
	private String ContNo;
	/** 客户号码 */
	private String InsuredNo;
	/** 状态 */
	private String InsuredState;
	/** 平台客户号 */
	private String CustomerNo;
	/** 姓名 */
	private String InsuredName;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private Date BirthDay;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 证件有效起期 */
	private Date IDStartDate;
	/** 证件有效止期 */
	private Date IDEndDate;
	/** 手机 */
	private String Mobile;
	/** 投保申请日期 */
	private Date ApplyDate;
	/** 职业大类 */
	private String OccupationType;
	/** 职业代码 */
	private String OccupationCode;
	/** 职业名称 */
	private String OccupationName;
	/** 续期账号开户行 */
	private String BankCode;
	/** 续期账号 */
	private String BankAccNo;
	/** 续期账号开户名 */
	private String AccName;
	/** 交费方式 */
	private String PayIntv;
	/** 首期保费 */
	private String Prem;
	/** 国籍 */
	private String NativePlace;
	/** 投保人与被保人关系 */
	private String Relation;
	/** 联系电话 */
	private String PHONE;
	/** 传真 */
	private String Fax;
	/** 电子邮箱 */
	private String Email;
	/** 单位名称 */
	private String GrpName;
	/** 所属部门 */
	private String Department;
	/** 是否社保 */
	private String SocialSecurityFlag;
	/** 是否纳税 */
	private String TaxFlag;
	/** 争议处理方式 */
	private String DisputeMode;
	/** 邮编 */
	private String ZipCode;
	/** 省份 */
	private String PostalProvince;
	/** 城市 */
	private String PostalCity;
	/** 区县 */
	private String PostalCounty;
	/** 详细地址 */
	private String PostalAddress;
	/** 销售渠道 */
	private String SaleChnl;
	/** 保险公司客户经理工号 */
	private String AgentCode;
	/** 代理机构代码 */
	private String AgentCom;
	/** 代理机构名称 */
	private String AgentComName;
	/** 代理机构客户经理工号 */
	private String AgentCodeTemp;
	/** 代理机构客户经理 */
	private String AgentCodeTempName;
	/** 询问项1 */
	private String InquiryItem1;
	/** 询问项2 */
	private String InquiryItem2;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 修改日期 */
	private Date ModifyDate;
	/** 错误信息 */
	private String ErrorInfo;
	//ADD JL 
	/** 起保日期*/
    private String EFFECTIVE_DATE;
    /** 满期日期*/
    private String EXPIRE_DATE;
    /** 婚姻与否*/
    private String marriage;
    /** 身高*/
    private String stature; 
    /** 体重*/
    private String avoirdupois;
    /** 职位*/
    private String position;
    /** 保险年龄年期 */
    private String InsuYear;
    /** 意外年龄年期标志 */
    private String InsuYearFlag;
    /** 终交年龄年期 */
    private String PayEndYear;
    /** 终交年龄年期标志 */
    private String PayEndYearFlag;
    /** 档次*/
    private String Mult;
    /** 健康告知明细*/
	private String QUESTION_ONE;
	private String QUESTION_TWO;
	private String QUESTION_TWO_SICK;
	private String QUESTION_THREE;
	private String QUESTION_FOUR;
	private String QUESTION_FIVE;
	private String QUESTION_SIX;
	private String QUESTION_SEVEN;
    
    
    
	

	public static final int FIELDNUM = 52;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息



	public String getSerNo()
	{
		if (SysConst.CHANGECHARSET && SerNo != null && !SerNo.equals(""))
		{
			SerNo = StrTool.unicodeToGBK(SerNo);
		}
		return SerNo;
	}
	public void setSerNo(String aSerNo)
	{
		SerNo = aSerNo;
	}
	public String getGrpNo()
	{
		if (SysConst.CHANGECHARSET && GrpNo != null && !GrpNo.equals(""))
		{
			GrpNo = StrTool.unicodeToGBK(GrpNo);
		}
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
		GrpNo = aGrpNo;
	}
	public String getPrtNo()
	{
		if (SysConst.CHANGECHARSET && PrtNo != null && !PrtNo.equals(""))
		{
			PrtNo = StrTool.unicodeToGBK(PrtNo);
		}
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getBatchNo()
	{
		if (SysConst.CHANGECHARSET && BatchNo != null && !BatchNo.equals(""))
		{
			BatchNo = StrTool.unicodeToGBK(BatchNo);
		}
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getContNo()
	{
		if (SysConst.CHANGECHARSET && ContNo != null && !ContNo.equals(""))
		{
			ContNo = StrTool.unicodeToGBK(ContNo);
		}
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getInsuredNo()
	{
		if (SysConst.CHANGECHARSET && InsuredNo != null && !InsuredNo.equals(""))
		{
			InsuredNo = StrTool.unicodeToGBK(InsuredNo);
		}
		return InsuredNo;
	}
	public void setInsuredNo(String aInsuredNo)
	{
		InsuredNo = aInsuredNo;
	}
	public String getInsuredState()
	{
		if (SysConst.CHANGECHARSET && InsuredState != null && !InsuredState.equals(""))
		{
			InsuredState = StrTool.unicodeToGBK(InsuredState);
		}
		return InsuredState;
	}
	public void setInsuredState(String aInsuredState)
	{
		InsuredState = aInsuredState;
	}
	public String getCustomerNo()
	{
		if (SysConst.CHANGECHARSET && CustomerNo != null && !CustomerNo.equals(""))
		{
			CustomerNo = StrTool.unicodeToGBK(CustomerNo);
		}
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getInsuredName()
	{
		if (SysConst.CHANGECHARSET && InsuredName != null && !InsuredName.equals(""))
		{
			InsuredName = StrTool.unicodeToGBK(InsuredName);
		}
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
		InsuredName = aInsuredName;
	}
	public String getSex()
	{
		if (SysConst.CHANGECHARSET && Sex != null && !Sex.equals(""))
		{
			Sex = StrTool.unicodeToGBK(Sex);
		}
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthDay()
	{
		if( BirthDay != null )
			return fDate.getString(BirthDay);
		else
			return null;
	}
	public void setBirthDay(Date aBirthDay)
	{
		BirthDay = aBirthDay;
	}
	public void setBirthDay(String aBirthDay)
	{
		if (aBirthDay != null && !aBirthDay.equals("") )
		{
			BirthDay = fDate.getDate( aBirthDay );
		}
		else
			BirthDay = null;
	}

	public String getIDType()
	{
		if (SysConst.CHANGECHARSET && IDType != null && !IDType.equals(""))
		{
			IDType = StrTool.unicodeToGBK(IDType);
		}
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		if (SysConst.CHANGECHARSET && IDNo != null && !IDNo.equals(""))
		{
			IDNo = StrTool.unicodeToGBK(IDNo);
		}
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}

	public String getMobile()
	{
		if (SysConst.CHANGECHARSET && Mobile != null && !Mobile.equals(""))
		{
			Mobile = StrTool.unicodeToGBK(Mobile);
		}
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getApplyDate()
	{
		if( ApplyDate != null )
			return fDate.getString(ApplyDate);
		else
			return null;
	}
	public void setApplyDate(Date aApplyDate)
	{
		ApplyDate = aApplyDate;
	}
	public void setApplyDate(String aApplyDate)
	{
		if (aApplyDate != null && !aApplyDate.equals("") )
		{
			ApplyDate = fDate.getDate( aApplyDate );
		}
		else
			ApplyDate = null;
	}

	public String getOccupationType()
	{
		if (SysConst.CHANGECHARSET && OccupationType != null && !OccupationType.equals(""))
		{
			OccupationType = StrTool.unicodeToGBK(OccupationType);
		}
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public String getOccupationCode()
	{
		if (SysConst.CHANGECHARSET && OccupationCode != null && !OccupationCode.equals(""))
		{
			OccupationCode = StrTool.unicodeToGBK(OccupationCode);
		}
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public String getOccupationName()
	{
		if (SysConst.CHANGECHARSET && OccupationName != null && !OccupationName.equals(""))
		{
			OccupationName = StrTool.unicodeToGBK(OccupationName);
		}
		return OccupationName;
	}
	public void setOccupationName(String aOccupationName)
	{
		OccupationName = aOccupationName;
	}
	public String getBankCode()
	{
		if (SysConst.CHANGECHARSET && BankCode != null && !BankCode.equals(""))
		{
			BankCode = StrTool.unicodeToGBK(BankCode);
		}
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		if (SysConst.CHANGECHARSET && BankAccNo != null && !BankAccNo.equals(""))
		{
			BankAccNo = StrTool.unicodeToGBK(BankAccNo);
		}
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		if (SysConst.CHANGECHARSET && AccName != null && !AccName.equals(""))
		{
			AccName = StrTool.unicodeToGBK(AccName);
		}
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getPayIntv()
	{
		if (SysConst.CHANGECHARSET && PayIntv != null && !PayIntv.equals(""))
		{
			PayIntv = StrTool.unicodeToGBK(PayIntv);
		}
		return PayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public String getPrem()
	{
		if (SysConst.CHANGECHARSET && Prem != null && !Prem.equals(""))
		{
			Prem = StrTool.unicodeToGBK(Prem);
		}
		return Prem;
	}
	public void setPrem(String aPrem)
	{
		Prem = aPrem;
	}
	public String getNativePlace()
	{
		if (SysConst.CHANGECHARSET && NativePlace != null && !NativePlace.equals(""))
		{
			NativePlace = StrTool.unicodeToGBK(NativePlace);
		}
		return NativePlace;
	}
	public void setNativePlace(String aNativePlace)
	{
		NativePlace = aNativePlace;
	}
	public String getRelation()
	{
		if (SysConst.CHANGECHARSET && Relation != null && !Relation.equals(""))
		{
			Relation = StrTool.unicodeToGBK(Relation);
		}
		return Relation;
	}
	public void setRelation(String aRelation)
	{
		Relation = aRelation;
	}
	public String getPHONE()
	{
		if (SysConst.CHANGECHARSET && PHONE != null && !PHONE.equals(""))
		{
			PHONE = StrTool.unicodeToGBK(PHONE);
		}
		return PHONE;
	}
	public void setPHONE(String aPHONE)
	{
		PHONE = aPHONE;
	}
	public String getFax()
	{
		if (SysConst.CHANGECHARSET && Fax != null && !Fax.equals(""))
		{
			Fax = StrTool.unicodeToGBK(Fax);
		}
		return Fax;
	}
	public void setFax(String aFax)
	{
		Fax = aFax;
	}
	public String getEmail()
	{
		if (SysConst.CHANGECHARSET && Email != null && !Email.equals(""))
		{
			Email = StrTool.unicodeToGBK(Email);
		}
		return Email;
	}
	public void setEmail(String aEmail)
	{
		Email = aEmail;
	}
	public String getGrpName()
	{
		if (SysConst.CHANGECHARSET && GrpName != null && !GrpName.equals(""))
		{
			GrpName = StrTool.unicodeToGBK(GrpName);
		}
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getDepartment()
	{
		if (SysConst.CHANGECHARSET && Department != null && !Department.equals(""))
		{
			Department = StrTool.unicodeToGBK(Department);
		}
		return Department;
	}
	public void setDepartment(String aDepartment)
	{
		Department = aDepartment;
	}
	public String getSocialSecurityFlag()
	{
		if (SysConst.CHANGECHARSET && SocialSecurityFlag != null && !SocialSecurityFlag.equals(""))
		{
			SocialSecurityFlag = StrTool.unicodeToGBK(SocialSecurityFlag);
		}
		return SocialSecurityFlag;
	}
	public void setSocialSecurityFlag(String aSocialSecurityFlag)
	{
		SocialSecurityFlag = aSocialSecurityFlag;
	}
	public String getTaxFlag()
	{
		if (SysConst.CHANGECHARSET && TaxFlag != null && !TaxFlag.equals(""))
		{
			TaxFlag = StrTool.unicodeToGBK(TaxFlag);
		}
		return TaxFlag;
	}
	public void setTaxFlag(String aTaxFlag)
	{
		TaxFlag = aTaxFlag;
	}
	public String getDisputeMode()
	{
		if (SysConst.CHANGECHARSET && DisputeMode != null && !DisputeMode.equals(""))
		{
			DisputeMode = StrTool.unicodeToGBK(DisputeMode);
		}
		return DisputeMode;
	}
	public void setDisputeMode(String aDisputeMode)
	{
		DisputeMode = aDisputeMode;
	}
	public String getZipCode()
	{
		if (SysConst.CHANGECHARSET && ZipCode != null && !ZipCode.equals(""))
		{
			ZipCode = StrTool.unicodeToGBK(ZipCode);
		}
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPostalProvince()
	{
		if (SysConst.CHANGECHARSET && PostalProvince != null && !PostalProvince.equals(""))
		{
			PostalProvince = StrTool.unicodeToGBK(PostalProvince);
		}
		return PostalProvince;
	}
	public void setPostalProvince(String aPostalProvince)
	{
		PostalProvince = aPostalProvince;
	}
	public String getPostalCity()
	{
		if (SysConst.CHANGECHARSET && PostalCity != null && !PostalCity.equals(""))
		{
			PostalCity = StrTool.unicodeToGBK(PostalCity);
		}
		return PostalCity;
	}
	public void setPostalCity(String aPostalCity)
	{
		PostalCity = aPostalCity;
	}
	public String getPostalCounty()
	{
		if (SysConst.CHANGECHARSET && PostalCounty != null && !PostalCounty.equals(""))
		{
			PostalCounty = StrTool.unicodeToGBK(PostalCounty);
		}
		return PostalCounty;
	}
	public void setPostalCounty(String aPostalCounty)
	{
		PostalCounty = aPostalCounty;
	}
	public String getPostalAddress()
	{
		if (SysConst.CHANGECHARSET && PostalAddress != null && !PostalAddress.equals(""))
		{
			PostalAddress = StrTool.unicodeToGBK(PostalAddress);
		}
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getSaleChnl()
	{
		if (SysConst.CHANGECHARSET && SaleChnl != null && !SaleChnl.equals(""))
		{
			SaleChnl = StrTool.unicodeToGBK(SaleChnl);
		}
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getAgentCode()
	{
		if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
		{
			AgentCode = StrTool.unicodeToGBK(AgentCode);
		}
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentCom()
	{
		if (SysConst.CHANGECHARSET && AgentCom != null && !AgentCom.equals(""))
		{
			AgentCom = StrTool.unicodeToGBK(AgentCom);
		}
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentComName()
	{
		if (SysConst.CHANGECHARSET && AgentComName != null && !AgentComName.equals(""))
		{
			AgentComName = StrTool.unicodeToGBK(AgentComName);
		}
		return AgentComName;
	}
	public void setAgentComName(String aAgentComName)
	{
		AgentComName = aAgentComName;
	}
	public String getAgentCodeTemp()
	{
		if (SysConst.CHANGECHARSET && AgentCodeTemp != null && !AgentCodeTemp.equals(""))
		{
			AgentCodeTemp = StrTool.unicodeToGBK(AgentCodeTemp);
		}
		return AgentCodeTemp;
	}
	public void setAgentCodeTemp(String aAgentCodeTemp)
	{
		AgentCodeTemp = aAgentCodeTemp;
	}
	public String getAgentCodeTempName()
	{
		if (SysConst.CHANGECHARSET && AgentCodeTempName != null && !AgentCodeTempName.equals(""))
		{
			AgentCodeTempName = StrTool.unicodeToGBK(AgentCodeTempName);
		}
		return AgentCodeTempName;
	}
	public void setAgentCodeTempName(String aAgentCodeTempName)
	{
		AgentCodeTempName = aAgentCodeTempName;
	}
	public String getInquiryItem1()
	{
		if (SysConst.CHANGECHARSET && InquiryItem1 != null && !InquiryItem1.equals(""))
		{
			InquiryItem1 = StrTool.unicodeToGBK(InquiryItem1);
		}
		return InquiryItem1;
	}
	public void setInquiryItem1(String aInquiryItem1)
	{
		InquiryItem1 = aInquiryItem1;
	}
	public String getInquiryItem2()
	{
		if (SysConst.CHANGECHARSET && InquiryItem2 != null && !InquiryItem2.equals(""))
		{
			InquiryItem2 = StrTool.unicodeToGBK(InquiryItem2);
		}
		return InquiryItem2;
	}
	public void setInquiryItem2(String aInquiryItem2)
	{
		InquiryItem2 = aInquiryItem2;
	}
	public String getOperator()
	{
		if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
		{
			Operator = StrTool.unicodeToGBK(Operator);
		}
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getErrorInfo()
	{
		if (SysConst.CHANGECHARSET && ErrorInfo != null && !ErrorInfo.equals(""))
		{
			ErrorInfo = StrTool.unicodeToGBK(ErrorInfo);
		}
		return ErrorInfo;
	}
	public void setErrorInfo(String aErrorInfo)
	{
		ErrorInfo = aErrorInfo;
	}
	
	//TODO
	public String getEFFECTIVE_DATE()
	{
		if (SysConst.CHANGECHARSET && EFFECTIVE_DATE != null && !EFFECTIVE_DATE.equals(""))
		{
			EFFECTIVE_DATE = StrTool.unicodeToGBK(EFFECTIVE_DATE);
		}
		return EFFECTIVE_DATE;
	}
	public void setEFFECTIVE_DATE(String aEFFECTIVE_DATE)
	{
		EFFECTIVE_DATE = aEFFECTIVE_DATE;
	}
	
	public String getEXPIRE_DATE()
	{
		if (SysConst.CHANGECHARSET && EXPIRE_DATE != null && !EXPIRE_DATE.equals(""))
		{
			EXPIRE_DATE = StrTool.unicodeToGBK(EXPIRE_DATE);
		}
		return EXPIRE_DATE;
	}
	public void setEXPIRE_DATE(String aEXPIRE_DATE)
	{
		EXPIRE_DATE = aEXPIRE_DATE;
	}
	
	public String getmarriage()
	{
		if (SysConst.CHANGECHARSET && marriage != null && !marriage.equals(""))
		{
			marriage = StrTool.unicodeToGBK(marriage);
		}
		return marriage;
	}
	public void setmarriage(String amarriage)
	{
		marriage = amarriage;
	}
	public String getstature()
	{
		if (SysConst.CHANGECHARSET && stature != null && !stature.equals(""))
		{
			stature = StrTool.unicodeToGBK(stature);
		}
		return stature;
	}
	public void setstature(String astature)
	{
		stature = astature;
	}
	public String getavoirdupois()
	{
		if (SysConst.CHANGECHARSET && avoirdupois != null && !avoirdupois.equals(""))
		{
			avoirdupois = StrTool.unicodeToGBK(avoirdupois);
		}
		return avoirdupois;
	}
	public void setavoirdupois(String aavoirdupois)
	{
		avoirdupois = aavoirdupois;
	}
	public String getposition()
	{
		if (SysConst.CHANGECHARSET && position != null && !position.equals(""))
		{
			position = StrTool.unicodeToGBK(position);
		}
		return position;
	}
	public void setposition(String aposition)
	{
		position = aposition;
	}
	
	public String getInsuYear()
	{
		if (SysConst.CHANGECHARSET && InsuYear != null && !InsuYear.equals(""))
		{
			InsuYear = StrTool.unicodeToGBK(InsuYear);
		}
		return InsuYear;
	}
	public void setInsuYear(String aInsuYear)
	{
		InsuYear = aInsuYear;
	}
	
	public String getInsuYearFlag()
	{
		if (SysConst.CHANGECHARSET && InsuYearFlag != null && !InsuYearFlag.equals(""))
		{
			InsuYearFlag = StrTool.unicodeToGBK(InsuYearFlag);
		}
		return InsuYearFlag;
	}
	public void setInsuYearFlag(String aInsuYearFlag)
	{
		InsuYearFlag = aInsuYearFlag;
	}
	
	public String getPayEndYear()
	{
		if (SysConst.CHANGECHARSET && PayEndYear != null && !PayEndYear.equals(""))
		{
			PayEndYear = StrTool.unicodeToGBK(PayEndYear);
		}
		return PayEndYear;
	}
	public void setPayEndYear(String aPayEndYear)
	{
		PayEndYear = aPayEndYear;
	}
	
	public String getPayEndYearFlag()
	{
		if (SysConst.CHANGECHARSET && PayEndYear != null && !PayEndYearFlag.equals(""))
		{
			PayEndYearFlag = StrTool.unicodeToGBK(PayEndYearFlag);
		}
		return PayEndYearFlag;
	}
	public void setPayEndYearFlag(String aPayEndYearFlag)
	{
		PayEndYearFlag = aPayEndYearFlag;
	}
	
	public String getMult() {
		return Mult;
	}

	public void setMult(String mult) {
		Mult = mult;
	}
	public String getMarriage() {
		return marriage;
	}
	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}
	public String getStature() {
		return stature;
	}
	public void setStature(String stature) {
		this.stature = stature;
	}
	public String getAvoirdupois() {
		return avoirdupois;
	}
	public void setAvoirdupois(String avoirdupois) {
		this.avoirdupois = avoirdupois;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getQUESTION_ONE() {
		return QUESTION_ONE;
	}
	public void setQUESTION_ONE(String qUESTION_ONE) {
		QUESTION_ONE = qUESTION_ONE;
	}
	public String getQUESTION_TWO() {
		return QUESTION_TWO;
	}
	public void setQUESTION_TWO(String qUESTION_TWO) {
		QUESTION_TWO = qUESTION_TWO;
	}
	public String getQUESTION_TWO_SICK() {
		return QUESTION_TWO_SICK;
	}
	public void setQUESTION_TWO_SICK(String qUESTION_TWO_SICK) {
		QUESTION_TWO_SICK = qUESTION_TWO_SICK;
	}
	public String getQUESTION_THREE() {
		return QUESTION_THREE;
	}
	public void setQUESTION_THREE(String qUESTION_THREE) {
		QUESTION_THREE = qUESTION_THREE;
	}
	public String getQUESTION_FOUR() {
		return QUESTION_FOUR;
	}
	public void setQUESTION_FOUR(String qUESTION_FOUR) {
		QUESTION_FOUR = qUESTION_FOUR;
	}
	public String getQUESTION_FIVE() {
		return QUESTION_FIVE;
	}
	public void setQUESTION_FIVE(String qUESTION_FIVE) {
		QUESTION_FIVE = qUESTION_FIVE;
	}
	public String getQUESTION_SIX() {
		return QUESTION_SIX;
	}
	public void setQUESTION_SIX(String qUESTION_SIX) {
		QUESTION_SIX = qUESTION_SIX;
	}
	public String getQUESTION_SEVEN() {
		return QUESTION_SEVEN;
	}
	public void setQUESTION_SEVEN(String qUESTION_SEVEN) {
		QUESTION_SEVEN = qUESTION_SEVEN;
	}
	
	
	
	
	
}
