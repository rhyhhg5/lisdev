/*
 * <p>ClassName: LFComISCSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LFComISCDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFComISCSchema implements Schema
{
    // @Field
    /** 保监会机构编码 */
    private String ComCodeISC;
    /** 机构名称 */
    private String Name;
    /** 上级机构编码 */
    private String ParentComCodeISC;
    /** 机构层次 */
    private int ComLevel;
    /** 是否是叶子结点 */
    private String IsLeaf;
    /** 是否上报标志 */
    private String OutputFlag;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFComISCSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ComCodeISC";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getComCodeISC()
    {
        if (ComCodeISC != null && !ComCodeISC.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ComCodeISC = StrTool.unicodeToGBK(ComCodeISC);
        }
        return ComCodeISC;
    }

    public void setComCodeISC(String aComCodeISC)
    {
        ComCodeISC = aComCodeISC;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET == true)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getParentComCodeISC()
    {
        if (ParentComCodeISC != null && !ParentComCodeISC.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ParentComCodeISC = StrTool.unicodeToGBK(ParentComCodeISC);
        }
        return ParentComCodeISC;
    }

    public void setParentComCodeISC(String aParentComCodeISC)
    {
        ParentComCodeISC = aParentComCodeISC;
    }

    public int getComLevel()
    {
        return ComLevel;
    }

    public void setComLevel(int aComLevel)
    {
        ComLevel = aComLevel;
    }

    public void setComLevel(String aComLevel)
    {
        if (aComLevel != null && !aComLevel.equals(""))
        {
            Integer tInteger = new Integer(aComLevel);
            int i = tInteger.intValue();
            ComLevel = i;
        }
    }

    public String getIsLeaf()
    {
        if (IsLeaf != null && !IsLeaf.equals("") && SysConst.CHANGECHARSET == true)
        {
            IsLeaf = StrTool.unicodeToGBK(IsLeaf);
        }
        return IsLeaf;
    }

    public void setIsLeaf(String aIsLeaf)
    {
        IsLeaf = aIsLeaf;
    }

    public String getOutputFlag()
    {
        if (OutputFlag != null && !OutputFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OutputFlag = StrTool.unicodeToGBK(OutputFlag);
        }
        return OutputFlag;
    }

    public void setOutputFlag(String aOutputFlag)
    {
        OutputFlag = aOutputFlag;
    }

    /**
     * 使用另外一个 LFComISCSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFComISCSchema aLFComISCSchema)
    {
        this.ComCodeISC = aLFComISCSchema.getComCodeISC();
        this.Name = aLFComISCSchema.getName();
        this.ParentComCodeISC = aLFComISCSchema.getParentComCodeISC();
        this.ComLevel = aLFComISCSchema.getComLevel();
        this.IsLeaf = aLFComISCSchema.getIsLeaf();
        this.OutputFlag = aLFComISCSchema.getOutputFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ComCodeISC") == null)
            {
                this.ComCodeISC = null;
            }
            else
            {
                this.ComCodeISC = rs.getString("ComCodeISC").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("ParentComCodeISC") == null)
            {
                this.ParentComCodeISC = null;
            }
            else
            {
                this.ParentComCodeISC = rs.getString("ParentComCodeISC").trim();
            }

            this.ComLevel = rs.getInt("ComLevel");
            if (rs.getString("IsLeaf") == null)
            {
                this.IsLeaf = null;
            }
            else
            {
                this.IsLeaf = rs.getString("IsLeaf").trim();
            }

            if (rs.getString("OutputFlag") == null)
            {
                this.OutputFlag = null;
            }
            else
            {
                this.OutputFlag = rs.getString("OutputFlag").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFComISCSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFComISCSchema getSchema()
    {
        LFComISCSchema aLFComISCSchema = new LFComISCSchema();
        aLFComISCSchema.setSchema(this);
        return aLFComISCSchema;
    }

    public LFComISCDB getDB()
    {
        LFComISCDB aDBOper = new LFComISCDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFComISC描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ComCodeISC)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ParentComCodeISC)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ComLevel) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IsLeaf)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OutputFlag));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFComISC>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ComCodeISC = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                  SysConst.PACKAGESPILTER);
            ParentComCodeISC = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              3, SysConst.PACKAGESPILTER);
            ComLevel = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            IsLeaf = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            OutputFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFComISCSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ComCodeISC"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCodeISC));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Name));
        }
        if (FCode.equals("ParentComCodeISC"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ParentComCodeISC));
        }
        if (FCode.equals("ComLevel"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComLevel));
        }
        if (FCode.equals("IsLeaf"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IsLeaf));
        }
        if (FCode.equals("OutputFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OutputFlag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ComCodeISC);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ParentComCodeISC);
                break;
            case 3:
                strFieldValue = String.valueOf(ComLevel);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(IsLeaf);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(OutputFlag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ComCodeISC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCodeISC = FValue.trim();
            }
            else
            {
                ComCodeISC = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("ParentComCodeISC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ParentComCodeISC = FValue.trim();
            }
            else
            {
                ParentComCodeISC = null;
            }
        }
        if (FCode.equals("ComLevel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ComLevel = i;
            }
        }
        if (FCode.equals("IsLeaf"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IsLeaf = FValue.trim();
            }
            else
            {
                IsLeaf = null;
            }
        }
        if (FCode.equals("OutputFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutputFlag = FValue.trim();
            }
            else
            {
                OutputFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFComISCSchema other = (LFComISCSchema) otherObject;
        return
                ComCodeISC.equals(other.getComCodeISC())
                && Name.equals(other.getName())
                && ParentComCodeISC.equals(other.getParentComCodeISC())
                && ComLevel == other.getComLevel()
                && IsLeaf.equals(other.getIsLeaf())
                && OutputFlag.equals(other.getOutputFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ComCodeISC"))
        {
            return 0;
        }
        if (strFieldName.equals("Name"))
        {
            return 1;
        }
        if (strFieldName.equals("ParentComCodeISC"))
        {
            return 2;
        }
        if (strFieldName.equals("ComLevel"))
        {
            return 3;
        }
        if (strFieldName.equals("IsLeaf"))
        {
            return 4;
        }
        if (strFieldName.equals("OutputFlag"))
        {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ComCodeISC";
                break;
            case 1:
                strFieldName = "Name";
                break;
            case 2:
                strFieldName = "ParentComCodeISC";
                break;
            case 3:
                strFieldName = "ComLevel";
                break;
            case 4:
                strFieldName = "IsLeaf";
                break;
            case 5:
                strFieldName = "OutputFlag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ComCodeISC"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParentComCodeISC"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComLevel"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("IsLeaf"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OutputFlag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
