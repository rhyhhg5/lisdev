/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LHMedicalItemGroupDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;


/*
 * <p>ClassName: LHMedicalItemGroupSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康管理
 * @CreateDate：2005-04-08
 */
public class LHMedicalItemGroupSchema implements Schema
{
    // @Field
    /** 套餐代码 */
    private String MedicaItemGroup;

    /** 医疗服务项目代码 */
    private String MedicaItemCode;

    public static final int FIELDNUM = 2; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息


    // @Constructor
    public LHMedicalItemGroupSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "MedicaItemGroup";
        pk[1] = "MedicaItemCode";

        PK = pk;
    }


    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getMedicaItemGroup()
    {
        if (SysConst.CHANGECHARSET && MedicaItemGroup != null &&
            !MedicaItemGroup.equals(""))
        {
            MedicaItemGroup = StrTool.unicodeToGBK(MedicaItemGroup);
        }
        return MedicaItemGroup;
    }

    public void setMedicaItemGroup(String aMedicaItemGroup)
    {
        MedicaItemGroup = aMedicaItemGroup;
    }

    public String getMedicaItemCode()
    {
        if (SysConst.CHANGECHARSET && MedicaItemCode != null &&
            !MedicaItemCode.equals(""))
        {
            MedicaItemCode = StrTool.unicodeToGBK(MedicaItemCode);
        }
        return MedicaItemCode;
    }

    public void setMedicaItemCode(String aMedicaItemCode)
    {
        MedicaItemCode = aMedicaItemCode;
    }


    /**
     * 使用另外一个 LHMedicalItemGroupSchema 对象给 Schema 赋值
     * @param: aLHMedicalItemGroupSchema LHMedicalItemGroupSchema
     **/
    public void setSchema(LHMedicalItemGroupSchema aLHMedicalItemGroupSchema)
    {
        this.MedicaItemGroup = aLHMedicalItemGroupSchema.getMedicaItemGroup();
        this.MedicaItemCode = aLHMedicalItemGroupSchema.getMedicaItemCode();
    }


    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("MedicaItemGroup") == null)
            {
                this.MedicaItemGroup = null;
            }
            else
            {
                this.MedicaItemGroup = rs.getString("MedicaItemGroup").trim();
            }

            if (rs.getString("MedicaItemCode") == null)
            {
                this.MedicaItemCode = null;
            }
            else
            {
                this.MedicaItemCode = rs.getString("MedicaItemCode").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHMedicalItemGroupSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LHMedicalItemGroupSchema getSchema()
    {
        LHMedicalItemGroupSchema aLHMedicalItemGroupSchema = new
                LHMedicalItemGroupSchema();
        aLHMedicalItemGroupSchema.setSchema(this);
        return aLHMedicalItemGroupSchema;
    }

    public LHMedicalItemGroupDB getDB()
    {
        LHMedicalItemGroupDB aDBOper = new LHMedicalItemGroupDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHMedicalItemGroup描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MedicaItemGroup)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MedicaItemCode)));
        return strReturn.toString();
    }


    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHMedicalItemGroup>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            MedicaItemGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             1, SysConst.PACKAGESPILTER);
            MedicaItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                            SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHMedicalItemGroupSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }


    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("MedicaItemGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemGroup));
        }
        if (FCode.equals("MedicaItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemCode));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(MedicaItemGroup);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(MedicaItemCode);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }


    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("MedicaItemGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MedicaItemGroup = FValue.trim();
            }
            else
            {
                MedicaItemGroup = null;
            }
        }
        if (FCode.equals("MedicaItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MedicaItemCode = FValue.trim();
            }
            else
            {
                MedicaItemCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LHMedicalItemGroupSchema other = (LHMedicalItemGroupSchema) otherObject;
        return
                MedicaItemGroup.equals(other.getMedicaItemGroup())
                && MedicaItemCode.equals(other.getMedicaItemCode());
    }


    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }


    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("MedicaItemGroup"))
        {
            return 0;
        }
        if (strFieldName.equals("MedicaItemCode"))
        {
            return 1;
        }
        return -1;
    }


    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "MedicaItemGroup";
                break;
            case 1:
                strFieldName = "MedicaItemCode";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }


    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("MedicaItemGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MedicaItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }


    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
