/*
 * <p>ClassName: LCAppntIndSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCAppntIndDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCAppntIndSchema implements Schema
{
    // @Field
    /** 保单号码 */
    private String PolNo;
    /** 客户号码 */
    private String CustomerNo;
    /** 客户内部号码 */
    private String SequenceNo;
    /** 投保人级别 */
    private String AppntGrade;
    /** 被保人与投保人关系 */
    private String RelationToInsured;
    /** 密码 */
    private String Password;
    /** 客户姓名 */
    private String Name;
    /** 客户性别 */
    private String Sex;
    /** 客户出生日期 */
    private Date Birthday;
    /** 籍贯 */
    private String NativePlace;
    /** 民族 */
    private String Nationality;
    /** 婚姻状况 */
    private String Marriage;
    /** 结婚日期 */
    private Date MarriageDate;
    /** 职业类别 */
    private String OccupationType;
    /** 开始工作日期 */
    private Date StartWorkDate;
    /** 工资 */
    private double Salary;
    /** 健康状况 */
    private String Health;
    /** 身高 */
    private double Stature;
    /** 体重 */
    private double Avoirdupois;
    /** 信用等级 */
    private String CreditGrade;
    /** 证件类型 */
    private String IDType;
    /** 属性 */
    private String Proterty;
    /** 证件号码 */
    private String IDNo;
    /** 其它证件类型 */
    private String OthIDType;
    /** 其它证件号码 */
    private String OthIDNo;
    /** Ic卡号 */
    private String ICNo;
    /** 家庭地址编码 */
    private String HomeAddressCode;
    /** 家庭地址 */
    private String HomeAddress;
    /** 通讯地址 */
    private String PostalAddress;
    /** 邮政编码 */
    private String ZipCode;
    /** 电话 */
    private String Phone;
    /** 传呼 */
    private String BP;
    /** 手机 */
    private String Mobile;
    /** E_mail */
    private String EMail;
    /** 入司日期 */
    private Date JoinCompanyDate;
    /** 职位 */
    private String Position;
    /** 单位编码 */
    private String GrpNo;
    /** 单位名称 */
    private String GrpName;
    /** 单位电话 */
    private String GrpPhone;
    /** 单位地址编码 */
    private String GrpAddressCode;
    /** 单位地址 */
    private String GrpAddress;
    /** 死亡日期 */
    private Date DeathDate;
    /** 备注 */
    private String Remark;
    /** 状态 */
    private String State;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 职业（工种） */
    private String WorkType;
    /** 兼职（工种） */
    private String PluralityType;
    /** 学历 */
    private String Degree;
    /** 职业代码 */
    private String OccupationCode;
    /** 单位邮编 */
    private String GrpZipCode;
    /** 是否吸烟标志 */
    private String SmokeFlag;
    /** 户口所在地 */
    private String RgtAddress;
    /** 家庭地址邮政编码 */
    private String HomeZipCode;
    /** 联系电话2 */
    private String Phone2;

    public static final int FIELDNUM = 55; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCAppntIndSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "PolNo";
        pk[1] = "CustomerNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getCustomerNo()
    {
        if (CustomerNo != null && !CustomerNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerNo = StrTool.unicodeToGBK(CustomerNo);
        }
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getSequenceNo()
    {
        if (SequenceNo != null && !SequenceNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SequenceNo = StrTool.unicodeToGBK(SequenceNo);
        }
        return SequenceNo;
    }

    public void setSequenceNo(String aSequenceNo)
    {
        SequenceNo = aSequenceNo;
    }

    public String getAppntGrade()
    {
        if (AppntGrade != null && !AppntGrade.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntGrade = StrTool.unicodeToGBK(AppntGrade);
        }
        return AppntGrade;
    }

    public void setAppntGrade(String aAppntGrade)
    {
        AppntGrade = aAppntGrade;
    }

    public String getRelationToInsured()
    {
        if (RelationToInsured != null && !RelationToInsured.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RelationToInsured = StrTool.unicodeToGBK(RelationToInsured);
        }
        return RelationToInsured;
    }

    public void setRelationToInsured(String aRelationToInsured)
    {
        RelationToInsured = aRelationToInsured;
    }

    public String getPassword()
    {
        if (Password != null && !Password.equals("") && SysConst.CHANGECHARSET == true)
        {
            Password = StrTool.unicodeToGBK(Password);
        }
        return Password;
    }

    public void setPassword(String aPassword)
    {
        Password = aPassword;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET == true)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getSex()
    {
        if (Sex != null && !Sex.equals("") && SysConst.CHANGECHARSET == true)
        {
            Sex = StrTool.unicodeToGBK(Sex);
        }
        return Sex;
    }

    public void setSex(String aSex)
    {
        Sex = aSex;
    }

    public String getBirthday()
    {
        if (Birthday != null)
        {
            return fDate.getString(Birthday);
        }
        else
        {
            return null;
        }
    }

    public void setBirthday(Date aBirthday)
    {
        Birthday = aBirthday;
    }

    public void setBirthday(String aBirthday)
    {
        if (aBirthday != null && !aBirthday.equals(""))
        {
            Birthday = fDate.getDate(aBirthday);
        }
        else
        {
            Birthday = null;
        }
    }

    public String getNativePlace()
    {
        if (NativePlace != null && !NativePlace.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            NativePlace = StrTool.unicodeToGBK(NativePlace);
        }
        return NativePlace;
    }

    public void setNativePlace(String aNativePlace)
    {
        NativePlace = aNativePlace;
    }

    public String getNationality()
    {
        if (Nationality != null && !Nationality.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            Nationality = StrTool.unicodeToGBK(Nationality);
        }
        return Nationality;
    }

    public void setNationality(String aNationality)
    {
        Nationality = aNationality;
    }

    public String getMarriage()
    {
        if (Marriage != null && !Marriage.equals("") && SysConst.CHANGECHARSET == true)
        {
            Marriage = StrTool.unicodeToGBK(Marriage);
        }
        return Marriage;
    }

    public void setMarriage(String aMarriage)
    {
        Marriage = aMarriage;
    }

    public String getMarriageDate()
    {
        if (MarriageDate != null)
        {
            return fDate.getString(MarriageDate);
        }
        else
        {
            return null;
        }
    }

    public void setMarriageDate(Date aMarriageDate)
    {
        MarriageDate = aMarriageDate;
    }

    public void setMarriageDate(String aMarriageDate)
    {
        if (aMarriageDate != null && !aMarriageDate.equals(""))
        {
            MarriageDate = fDate.getDate(aMarriageDate);
        }
        else
        {
            MarriageDate = null;
        }
    }

    public String getOccupationType()
    {
        if (OccupationType != null && !OccupationType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OccupationType = StrTool.unicodeToGBK(OccupationType);
        }
        return OccupationType;
    }

    public void setOccupationType(String aOccupationType)
    {
        OccupationType = aOccupationType;
    }

    public String getStartWorkDate()
    {
        if (StartWorkDate != null)
        {
            return fDate.getString(StartWorkDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartWorkDate(Date aStartWorkDate)
    {
        StartWorkDate = aStartWorkDate;
    }

    public void setStartWorkDate(String aStartWorkDate)
    {
        if (aStartWorkDate != null && !aStartWorkDate.equals(""))
        {
            StartWorkDate = fDate.getDate(aStartWorkDate);
        }
        else
        {
            StartWorkDate = null;
        }
    }

    public double getSalary()
    {
        return Salary;
    }

    public void setSalary(double aSalary)
    {
        Salary = aSalary;
    }

    public void setSalary(String aSalary)
    {
        if (aSalary != null && !aSalary.equals(""))
        {
            Double tDouble = new Double(aSalary);
            double d = tDouble.doubleValue();
            Salary = d;
        }
    }

    public String getHealth()
    {
        if (Health != null && !Health.equals("") && SysConst.CHANGECHARSET == true)
        {
            Health = StrTool.unicodeToGBK(Health);
        }
        return Health;
    }

    public void setHealth(String aHealth)
    {
        Health = aHealth;
    }

    public double getStature()
    {
        return Stature;
    }

    public void setStature(double aStature)
    {
        Stature = aStature;
    }

    public void setStature(String aStature)
    {
        if (aStature != null && !aStature.equals(""))
        {
            Double tDouble = new Double(aStature);
            double d = tDouble.doubleValue();
            Stature = d;
        }
    }

    public double getAvoirdupois()
    {
        return Avoirdupois;
    }

    public void setAvoirdupois(double aAvoirdupois)
    {
        Avoirdupois = aAvoirdupois;
    }

    public void setAvoirdupois(String aAvoirdupois)
    {
        if (aAvoirdupois != null && !aAvoirdupois.equals(""))
        {
            Double tDouble = new Double(aAvoirdupois);
            double d = tDouble.doubleValue();
            Avoirdupois = d;
        }
    }

    public String getCreditGrade()
    {
        if (CreditGrade != null && !CreditGrade.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CreditGrade = StrTool.unicodeToGBK(CreditGrade);
        }
        return CreditGrade;
    }

    public void setCreditGrade(String aCreditGrade)
    {
        CreditGrade = aCreditGrade;
    }

    public String getIDType()
    {
        if (IDType != null && !IDType.equals("") && SysConst.CHANGECHARSET == true)
        {
            IDType = StrTool.unicodeToGBK(IDType);
        }
        return IDType;
    }

    public void setIDType(String aIDType)
    {
        IDType = aIDType;
    }

    public String getProterty()
    {
        if (Proterty != null && !Proterty.equals("") && SysConst.CHANGECHARSET == true)
        {
            Proterty = StrTool.unicodeToGBK(Proterty);
        }
        return Proterty;
    }

    public void setProterty(String aProterty)
    {
        Proterty = aProterty;
    }

    public String getIDNo()
    {
        if (IDNo != null && !IDNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            IDNo = StrTool.unicodeToGBK(IDNo);
        }
        return IDNo;
    }

    public void setIDNo(String aIDNo)
    {
        IDNo = aIDNo;
    }

    public String getOthIDType()
    {
        if (OthIDType != null && !OthIDType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OthIDType = StrTool.unicodeToGBK(OthIDType);
        }
        return OthIDType;
    }

    public void setOthIDType(String aOthIDType)
    {
        OthIDType = aOthIDType;
    }

    public String getOthIDNo()
    {
        if (OthIDNo != null && !OthIDNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OthIDNo = StrTool.unicodeToGBK(OthIDNo);
        }
        return OthIDNo;
    }

    public void setOthIDNo(String aOthIDNo)
    {
        OthIDNo = aOthIDNo;
    }

    public String getICNo()
    {
        if (ICNo != null && !ICNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ICNo = StrTool.unicodeToGBK(ICNo);
        }
        return ICNo;
    }

    public void setICNo(String aICNo)
    {
        ICNo = aICNo;
    }

    public String getHomeAddressCode()
    {
        if (HomeAddressCode != null && !HomeAddressCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HomeAddressCode = StrTool.unicodeToGBK(HomeAddressCode);
        }
        return HomeAddressCode;
    }

    public void setHomeAddressCode(String aHomeAddressCode)
    {
        HomeAddressCode = aHomeAddressCode;
    }

    public String getHomeAddress()
    {
        if (HomeAddress != null && !HomeAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HomeAddress = StrTool.unicodeToGBK(HomeAddress);
        }
        return HomeAddress;
    }

    public void setHomeAddress(String aHomeAddress)
    {
        HomeAddress = aHomeAddress;
    }

    public String getPostalAddress()
    {
        if (PostalAddress != null && !PostalAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PostalAddress = StrTool.unicodeToGBK(PostalAddress);
        }
        return PostalAddress;
    }

    public void setPostalAddress(String aPostalAddress)
    {
        PostalAddress = aPostalAddress;
    }

    public String getZipCode()
    {
        if (ZipCode != null && !ZipCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ZipCode = StrTool.unicodeToGBK(ZipCode);
        }
        return ZipCode;
    }

    public void setZipCode(String aZipCode)
    {
        ZipCode = aZipCode;
    }

    public String getPhone()
    {
        if (Phone != null && !Phone.equals("") && SysConst.CHANGECHARSET == true)
        {
            Phone = StrTool.unicodeToGBK(Phone);
        }
        return Phone;
    }

    public void setPhone(String aPhone)
    {
        Phone = aPhone;
    }

    public String getBP()
    {
        if (BP != null && !BP.equals("") && SysConst.CHANGECHARSET == true)
        {
            BP = StrTool.unicodeToGBK(BP);
        }
        return BP;
    }

    public void setBP(String aBP)
    {
        BP = aBP;
    }

    public String getMobile()
    {
        if (Mobile != null && !Mobile.equals("") && SysConst.CHANGECHARSET == true)
        {
            Mobile = StrTool.unicodeToGBK(Mobile);
        }
        return Mobile;
    }

    public void setMobile(String aMobile)
    {
        Mobile = aMobile;
    }

    public String getEMail()
    {
        if (EMail != null && !EMail.equals("") && SysConst.CHANGECHARSET == true)
        {
            EMail = StrTool.unicodeToGBK(EMail);
        }
        return EMail;
    }

    public void setEMail(String aEMail)
    {
        EMail = aEMail;
    }

    public String getJoinCompanyDate()
    {
        if (JoinCompanyDate != null)
        {
            return fDate.getString(JoinCompanyDate);
        }
        else
        {
            return null;
        }
    }

    public void setJoinCompanyDate(Date aJoinCompanyDate)
    {
        JoinCompanyDate = aJoinCompanyDate;
    }

    public void setJoinCompanyDate(String aJoinCompanyDate)
    {
        if (aJoinCompanyDate != null && !aJoinCompanyDate.equals(""))
        {
            JoinCompanyDate = fDate.getDate(aJoinCompanyDate);
        }
        else
        {
            JoinCompanyDate = null;
        }
    }

    public String getPosition()
    {
        if (Position != null && !Position.equals("") && SysConst.CHANGECHARSET == true)
        {
            Position = StrTool.unicodeToGBK(Position);
        }
        return Position;
    }

    public void setPosition(String aPosition)
    {
        Position = aPosition;
    }

    public String getGrpNo()
    {
        if (GrpNo != null && !GrpNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpNo = StrTool.unicodeToGBK(GrpNo);
        }
        return GrpNo;
    }

    public void setGrpNo(String aGrpNo)
    {
        GrpNo = aGrpNo;
    }

    public String getGrpName()
    {
        if (GrpName != null && !GrpName.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpName = StrTool.unicodeToGBK(GrpName);
        }
        return GrpName;
    }

    public void setGrpName(String aGrpName)
    {
        GrpName = aGrpName;
    }

    public String getGrpPhone()
    {
        if (GrpPhone != null && !GrpPhone.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPhone = StrTool.unicodeToGBK(GrpPhone);
        }
        return GrpPhone;
    }

    public void setGrpPhone(String aGrpPhone)
    {
        GrpPhone = aGrpPhone;
    }

    public String getGrpAddressCode()
    {
        if (GrpAddressCode != null && !GrpAddressCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpAddressCode = StrTool.unicodeToGBK(GrpAddressCode);
        }
        return GrpAddressCode;
    }

    public void setGrpAddressCode(String aGrpAddressCode)
    {
        GrpAddressCode = aGrpAddressCode;
    }

    public String getGrpAddress()
    {
        if (GrpAddress != null && !GrpAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpAddress = StrTool.unicodeToGBK(GrpAddress);
        }
        return GrpAddress;
    }

    public void setGrpAddress(String aGrpAddress)
    {
        GrpAddress = aGrpAddress;
    }

    public String getDeathDate()
    {
        if (DeathDate != null)
        {
            return fDate.getString(DeathDate);
        }
        else
        {
            return null;
        }
    }

    public void setDeathDate(Date aDeathDate)
    {
        DeathDate = aDeathDate;
    }

    public void setDeathDate(String aDeathDate)
    {
        if (aDeathDate != null && !aDeathDate.equals(""))
        {
            DeathDate = fDate.getDate(aDeathDate);
        }
        else
        {
            DeathDate = null;
        }
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET == true)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getWorkType()
    {
        if (WorkType != null && !WorkType.equals("") && SysConst.CHANGECHARSET == true)
        {
            WorkType = StrTool.unicodeToGBK(WorkType);
        }
        return WorkType;
    }

    public void setWorkType(String aWorkType)
    {
        WorkType = aWorkType;
    }

    public String getPluralityType()
    {
        if (PluralityType != null && !PluralityType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PluralityType = StrTool.unicodeToGBK(PluralityType);
        }
        return PluralityType;
    }

    public void setPluralityType(String aPluralityType)
    {
        PluralityType = aPluralityType;
    }

    public String getDegree()
    {
        if (Degree != null && !Degree.equals("") && SysConst.CHANGECHARSET == true)
        {
            Degree = StrTool.unicodeToGBK(Degree);
        }
        return Degree;
    }

    public void setDegree(String aDegree)
    {
        Degree = aDegree;
    }

    public String getOccupationCode()
    {
        if (OccupationCode != null && !OccupationCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OccupationCode = StrTool.unicodeToGBK(OccupationCode);
        }
        return OccupationCode;
    }

    public void setOccupationCode(String aOccupationCode)
    {
        OccupationCode = aOccupationCode;
    }

    public String getGrpZipCode()
    {
        if (GrpZipCode != null && !GrpZipCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpZipCode = StrTool.unicodeToGBK(GrpZipCode);
        }
        return GrpZipCode;
    }

    public void setGrpZipCode(String aGrpZipCode)
    {
        GrpZipCode = aGrpZipCode;
    }

    public String getSmokeFlag()
    {
        if (SmokeFlag != null && !SmokeFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SmokeFlag = StrTool.unicodeToGBK(SmokeFlag);
        }
        return SmokeFlag;
    }

    public void setSmokeFlag(String aSmokeFlag)
    {
        SmokeFlag = aSmokeFlag;
    }

    public String getRgtAddress()
    {
        if (RgtAddress != null && !RgtAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RgtAddress = StrTool.unicodeToGBK(RgtAddress);
        }
        return RgtAddress;
    }

    public void setRgtAddress(String aRgtAddress)
    {
        RgtAddress = aRgtAddress;
    }

    public String getHomeZipCode()
    {
        if (HomeZipCode != null && !HomeZipCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HomeZipCode = StrTool.unicodeToGBK(HomeZipCode);
        }
        return HomeZipCode;
    }

    public void setHomeZipCode(String aHomeZipCode)
    {
        HomeZipCode = aHomeZipCode;
    }

    public String getPhone2()
    {
        if (Phone2 != null && !Phone2.equals("") && SysConst.CHANGECHARSET == true)
        {
            Phone2 = StrTool.unicodeToGBK(Phone2);
        }
        return Phone2;
    }

    public void setPhone2(String aPhone2)
    {
        Phone2 = aPhone2;
    }

    /**
     * 使用另外一个 LCAppntIndSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCAppntIndSchema aLCAppntIndSchema)
    {
        this.PolNo = aLCAppntIndSchema.getPolNo();
        this.CustomerNo = aLCAppntIndSchema.getCustomerNo();
        this.SequenceNo = aLCAppntIndSchema.getSequenceNo();
        this.AppntGrade = aLCAppntIndSchema.getAppntGrade();
        this.RelationToInsured = aLCAppntIndSchema.getRelationToInsured();
        this.Password = aLCAppntIndSchema.getPassword();
        this.Name = aLCAppntIndSchema.getName();
        this.Sex = aLCAppntIndSchema.getSex();
        this.Birthday = fDate.getDate(aLCAppntIndSchema.getBirthday());
        this.NativePlace = aLCAppntIndSchema.getNativePlace();
        this.Nationality = aLCAppntIndSchema.getNationality();
        this.Marriage = aLCAppntIndSchema.getMarriage();
        this.MarriageDate = fDate.getDate(aLCAppntIndSchema.getMarriageDate());
        this.OccupationType = aLCAppntIndSchema.getOccupationType();
        this.StartWorkDate = fDate.getDate(aLCAppntIndSchema.getStartWorkDate());
        this.Salary = aLCAppntIndSchema.getSalary();
        this.Health = aLCAppntIndSchema.getHealth();
        this.Stature = aLCAppntIndSchema.getStature();
        this.Avoirdupois = aLCAppntIndSchema.getAvoirdupois();
        this.CreditGrade = aLCAppntIndSchema.getCreditGrade();
        this.IDType = aLCAppntIndSchema.getIDType();
        this.Proterty = aLCAppntIndSchema.getProterty();
        this.IDNo = aLCAppntIndSchema.getIDNo();
        this.OthIDType = aLCAppntIndSchema.getOthIDType();
        this.OthIDNo = aLCAppntIndSchema.getOthIDNo();
        this.ICNo = aLCAppntIndSchema.getICNo();
        this.HomeAddressCode = aLCAppntIndSchema.getHomeAddressCode();
        this.HomeAddress = aLCAppntIndSchema.getHomeAddress();
        this.PostalAddress = aLCAppntIndSchema.getPostalAddress();
        this.ZipCode = aLCAppntIndSchema.getZipCode();
        this.Phone = aLCAppntIndSchema.getPhone();
        this.BP = aLCAppntIndSchema.getBP();
        this.Mobile = aLCAppntIndSchema.getMobile();
        this.EMail = aLCAppntIndSchema.getEMail();
        this.JoinCompanyDate = fDate.getDate(aLCAppntIndSchema.
                                             getJoinCompanyDate());
        this.Position = aLCAppntIndSchema.getPosition();
        this.GrpNo = aLCAppntIndSchema.getGrpNo();
        this.GrpName = aLCAppntIndSchema.getGrpName();
        this.GrpPhone = aLCAppntIndSchema.getGrpPhone();
        this.GrpAddressCode = aLCAppntIndSchema.getGrpAddressCode();
        this.GrpAddress = aLCAppntIndSchema.getGrpAddress();
        this.DeathDate = fDate.getDate(aLCAppntIndSchema.getDeathDate());
        this.Remark = aLCAppntIndSchema.getRemark();
        this.State = aLCAppntIndSchema.getState();
        this.ModifyDate = fDate.getDate(aLCAppntIndSchema.getModifyDate());
        this.ModifyTime = aLCAppntIndSchema.getModifyTime();
        this.WorkType = aLCAppntIndSchema.getWorkType();
        this.PluralityType = aLCAppntIndSchema.getPluralityType();
        this.Degree = aLCAppntIndSchema.getDegree();
        this.OccupationCode = aLCAppntIndSchema.getOccupationCode();
        this.GrpZipCode = aLCAppntIndSchema.getGrpZipCode();
        this.SmokeFlag = aLCAppntIndSchema.getSmokeFlag();
        this.RgtAddress = aLCAppntIndSchema.getRgtAddress();
        this.HomeZipCode = aLCAppntIndSchema.getHomeZipCode();
        this.Phone2 = aLCAppntIndSchema.getPhone2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("CustomerNo") == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("SequenceNo") == null)
            {
                this.SequenceNo = null;
            }
            else
            {
                this.SequenceNo = rs.getString("SequenceNo").trim();
            }

            if (rs.getString("AppntGrade") == null)
            {
                this.AppntGrade = null;
            }
            else
            {
                this.AppntGrade = rs.getString("AppntGrade").trim();
            }

            if (rs.getString("RelationToInsured") == null)
            {
                this.RelationToInsured = null;
            }
            else
            {
                this.RelationToInsured = rs.getString("RelationToInsured").trim();
            }

            if (rs.getString("Password") == null)
            {
                this.Password = null;
            }
            else
            {
                this.Password = rs.getString("Password").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("Sex") == null)
            {
                this.Sex = null;
            }
            else
            {
                this.Sex = rs.getString("Sex").trim();
            }

            this.Birthday = rs.getDate("Birthday");
            if (rs.getString("NativePlace") == null)
            {
                this.NativePlace = null;
            }
            else
            {
                this.NativePlace = rs.getString("NativePlace").trim();
            }

            if (rs.getString("Nationality") == null)
            {
                this.Nationality = null;
            }
            else
            {
                this.Nationality = rs.getString("Nationality").trim();
            }

            if (rs.getString("Marriage") == null)
            {
                this.Marriage = null;
            }
            else
            {
                this.Marriage = rs.getString("Marriage").trim();
            }

            this.MarriageDate = rs.getDate("MarriageDate");
            if (rs.getString("OccupationType") == null)
            {
                this.OccupationType = null;
            }
            else
            {
                this.OccupationType = rs.getString("OccupationType").trim();
            }

            this.StartWorkDate = rs.getDate("StartWorkDate");
            this.Salary = rs.getDouble("Salary");
            if (rs.getString("Health") == null)
            {
                this.Health = null;
            }
            else
            {
                this.Health = rs.getString("Health").trim();
            }

            this.Stature = rs.getDouble("Stature");
            this.Avoirdupois = rs.getDouble("Avoirdupois");
            if (rs.getString("CreditGrade") == null)
            {
                this.CreditGrade = null;
            }
            else
            {
                this.CreditGrade = rs.getString("CreditGrade").trim();
            }

            if (rs.getString("IDType") == null)
            {
                this.IDType = null;
            }
            else
            {
                this.IDType = rs.getString("IDType").trim();
            }

            if (rs.getString("Proterty") == null)
            {
                this.Proterty = null;
            }
            else
            {
                this.Proterty = rs.getString("Proterty").trim();
            }

            if (rs.getString("IDNo") == null)
            {
                this.IDNo = null;
            }
            else
            {
                this.IDNo = rs.getString("IDNo").trim();
            }

            if (rs.getString("OthIDType") == null)
            {
                this.OthIDType = null;
            }
            else
            {
                this.OthIDType = rs.getString("OthIDType").trim();
            }

            if (rs.getString("OthIDNo") == null)
            {
                this.OthIDNo = null;
            }
            else
            {
                this.OthIDNo = rs.getString("OthIDNo").trim();
            }

            if (rs.getString("ICNo") == null)
            {
                this.ICNo = null;
            }
            else
            {
                this.ICNo = rs.getString("ICNo").trim();
            }

            if (rs.getString("HomeAddressCode") == null)
            {
                this.HomeAddressCode = null;
            }
            else
            {
                this.HomeAddressCode = rs.getString("HomeAddressCode").trim();
            }

            if (rs.getString("HomeAddress") == null)
            {
                this.HomeAddress = null;
            }
            else
            {
                this.HomeAddress = rs.getString("HomeAddress").trim();
            }

            if (rs.getString("PostalAddress") == null)
            {
                this.PostalAddress = null;
            }
            else
            {
                this.PostalAddress = rs.getString("PostalAddress").trim();
            }

            if (rs.getString("ZipCode") == null)
            {
                this.ZipCode = null;
            }
            else
            {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null)
            {
                this.Phone = null;
            }
            else
            {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("BP") == null)
            {
                this.BP = null;
            }
            else
            {
                this.BP = rs.getString("BP").trim();
            }

            if (rs.getString("Mobile") == null)
            {
                this.Mobile = null;
            }
            else
            {
                this.Mobile = rs.getString("Mobile").trim();
            }

            if (rs.getString("EMail") == null)
            {
                this.EMail = null;
            }
            else
            {
                this.EMail = rs.getString("EMail").trim();
            }

            this.JoinCompanyDate = rs.getDate("JoinCompanyDate");
            if (rs.getString("Position") == null)
            {
                this.Position = null;
            }
            else
            {
                this.Position = rs.getString("Position").trim();
            }

            if (rs.getString("GrpNo") == null)
            {
                this.GrpNo = null;
            }
            else
            {
                this.GrpNo = rs.getString("GrpNo").trim();
            }

            if (rs.getString("GrpName") == null)
            {
                this.GrpName = null;
            }
            else
            {
                this.GrpName = rs.getString("GrpName").trim();
            }

            if (rs.getString("GrpPhone") == null)
            {
                this.GrpPhone = null;
            }
            else
            {
                this.GrpPhone = rs.getString("GrpPhone").trim();
            }

            if (rs.getString("GrpAddressCode") == null)
            {
                this.GrpAddressCode = null;
            }
            else
            {
                this.GrpAddressCode = rs.getString("GrpAddressCode").trim();
            }

            if (rs.getString("GrpAddress") == null)
            {
                this.GrpAddress = null;
            }
            else
            {
                this.GrpAddress = rs.getString("GrpAddress").trim();
            }

            this.DeathDate = rs.getDate("DeathDate");
            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("WorkType") == null)
            {
                this.WorkType = null;
            }
            else
            {
                this.WorkType = rs.getString("WorkType").trim();
            }

            if (rs.getString("PluralityType") == null)
            {
                this.PluralityType = null;
            }
            else
            {
                this.PluralityType = rs.getString("PluralityType").trim();
            }

            if (rs.getString("Degree") == null)
            {
                this.Degree = null;
            }
            else
            {
                this.Degree = rs.getString("Degree").trim();
            }

            if (rs.getString("OccupationCode") == null)
            {
                this.OccupationCode = null;
            }
            else
            {
                this.OccupationCode = rs.getString("OccupationCode").trim();
            }

            if (rs.getString("GrpZipCode") == null)
            {
                this.GrpZipCode = null;
            }
            else
            {
                this.GrpZipCode = rs.getString("GrpZipCode").trim();
            }

            if (rs.getString("SmokeFlag") == null)
            {
                this.SmokeFlag = null;
            }
            else
            {
                this.SmokeFlag = rs.getString("SmokeFlag").trim();
            }

            if (rs.getString("RgtAddress") == null)
            {
                this.RgtAddress = null;
            }
            else
            {
                this.RgtAddress = rs.getString("RgtAddress").trim();
            }

            if (rs.getString("HomeZipCode") == null)
            {
                this.HomeZipCode = null;
            }
            else
            {
                this.HomeZipCode = rs.getString("HomeZipCode").trim();
            }

            if (rs.getString("Phone2") == null)
            {
                this.Phone2 = null;
            }
            else
            {
                this.Phone2 = rs.getString("Phone2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntIndSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCAppntIndSchema getSchema()
    {
        LCAppntIndSchema aLCAppntIndSchema = new LCAppntIndSchema();
        aLCAppntIndSchema.setSchema(this);
        return aLCAppntIndSchema;
    }

    public LCAppntIndDB getDB()
    {
        LCAppntIndDB aDBOper = new LCAppntIndDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAppntInd描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SequenceNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RelationToInsured)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Password)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Sex)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(Birthday))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NativePlace)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Nationality)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Marriage)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            MarriageDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OccupationType)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            StartWorkDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Salary) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Health)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Stature) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Avoirdupois) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CreditGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IDType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Proterty)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IDNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OthIDType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OthIDNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ICNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HomeAddressCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HomeAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PostalAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Phone)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BP)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Mobile)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EMail)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            JoinCompanyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Position)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPhone)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpAddressCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpAddress)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            DeathDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(WorkType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PluralityType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Degree)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OccupationCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SmokeFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RgtAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HomeZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Phone2));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAppntInd>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            SequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            AppntGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            RelationToInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               5, SysConst.PACKAGESPILTER);
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                 SysConst.PACKAGESPILTER);
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
            Marriage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MarriageDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            14, SysConst.PACKAGESPILTER);
            StartWorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            Salary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    16, SysConst.PACKAGESPILTER))).doubleValue();
            Health = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                    SysConst.PACKAGESPILTER);
            Stature = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            Avoirdupois = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            CreditGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                         SysConst.PACKAGESPILTER);
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                    SysConst.PACKAGESPILTER);
            Proterty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                  SysConst.PACKAGESPILTER);
            OthIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                       SysConst.PACKAGESPILTER);
            OthIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                     SysConst.PACKAGESPILTER);
            ICNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                  SysConst.PACKAGESPILTER);
            HomeAddressCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             27, SysConst.PACKAGESPILTER);
            HomeAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                         SysConst.PACKAGESPILTER);
            PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                           SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                   SysConst.PACKAGESPILTER);
            BP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                SysConst.PACKAGESPILTER);
            Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,
                                    SysConst.PACKAGESPILTER);
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                   SysConst.PACKAGESPILTER);
            JoinCompanyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 35, SysConst.PACKAGESPILTER));
            Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                      SysConst.PACKAGESPILTER);
            GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,
                                   SysConst.PACKAGESPILTER);
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,
                                     SysConst.PACKAGESPILTER);
            GrpPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,
                                      SysConst.PACKAGESPILTER);
            GrpAddressCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            40, SysConst.PACKAGESPILTER);
            GrpAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,
                                        SysConst.PACKAGESPILTER);
            DeathDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 42, SysConst.PACKAGESPILTER));
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,
                                    SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,
                                   SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 45, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46,
                                        SysConst.PACKAGESPILTER);
            WorkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47,
                                      SysConst.PACKAGESPILTER);
            PluralityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48,
                                           SysConst.PACKAGESPILTER);
            Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49,
                                    SysConst.PACKAGESPILTER);
            OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            50, SysConst.PACKAGESPILTER);
            GrpZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51,
                                        SysConst.PACKAGESPILTER);
            SmokeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52,
                                       SysConst.PACKAGESPILTER);
            RgtAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53,
                                        SysConst.PACKAGESPILTER);
            HomeZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,
                                         SysConst.PACKAGESPILTER);
            Phone2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntIndSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerNo));
        }
        if (FCode.equals("SequenceNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SequenceNo));
        }
        if (FCode.equals("AppntGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntGrade));
        }
        if (FCode.equals("RelationToInsured"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    RelationToInsured));
        }
        if (FCode.equals("Password"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Password));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Name));
        }
        if (FCode.equals("Sex"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Sex));
        }
        if (FCode.equals("Birthday"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getBirthday()));
        }
        if (FCode.equals("NativePlace"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NativePlace));
        }
        if (FCode.equals("Nationality"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Nationality));
        }
        if (FCode.equals("Marriage"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Marriage));
        }
        if (FCode.equals("MarriageDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getMarriageDate()));
        }
        if (FCode.equals("OccupationType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OccupationType));
        }
        if (FCode.equals("StartWorkDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getStartWorkDate()));
        }
        if (FCode.equals("Salary"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Salary));
        }
        if (FCode.equals("Health"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Health));
        }
        if (FCode.equals("Stature"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Stature));
        }
        if (FCode.equals("Avoirdupois"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Avoirdupois));
        }
        if (FCode.equals("CreditGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CreditGrade));
        }
        if (FCode.equals("IDType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IDType));
        }
        if (FCode.equals("Proterty"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Proterty));
        }
        if (FCode.equals("IDNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IDNo));
        }
        if (FCode.equals("OthIDType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OthIDType));
        }
        if (FCode.equals("OthIDNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OthIDNo));
        }
        if (FCode.equals("ICNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ICNo));
        }
        if (FCode.equals("HomeAddressCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HomeAddressCode));
        }
        if (FCode.equals("HomeAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HomeAddress));
        }
        if (FCode.equals("PostalAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PostalAddress));
        }
        if (FCode.equals("ZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ZipCode));
        }
        if (FCode.equals("Phone"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Phone));
        }
        if (FCode.equals("BP"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BP));
        }
        if (FCode.equals("Mobile"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Mobile));
        }
        if (FCode.equals("EMail"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EMail));
        }
        if (FCode.equals("JoinCompanyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getJoinCompanyDate()));
        }
        if (FCode.equals("Position"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Position));
        }
        if (FCode.equals("GrpNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpNo));
        }
        if (FCode.equals("GrpName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpName));
        }
        if (FCode.equals("GrpPhone"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPhone));
        }
        if (FCode.equals("GrpAddressCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpAddressCode));
        }
        if (FCode.equals("GrpAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpAddress));
        }
        if (FCode.equals("DeathDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getDeathDate()));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(State));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("WorkType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WorkType));
        }
        if (FCode.equals("PluralityType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PluralityType));
        }
        if (FCode.equals("Degree"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Degree));
        }
        if (FCode.equals("OccupationCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OccupationCode));
        }
        if (FCode.equals("GrpZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpZipCode));
        }
        if (FCode.equals("SmokeFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SmokeFlag));
        }
        if (FCode.equals("RgtAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RgtAddress));
        }
        if (FCode.equals("HomeZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HomeZipCode));
        }
        if (FCode.equals("Phone2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Phone2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SequenceNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AppntGrade);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RelationToInsured);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBirthday()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(NativePlace);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Nationality);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Marriage);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMarriageDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(OccupationType);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartWorkDate()));
                break;
            case 15:
                strFieldValue = String.valueOf(Salary);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Health);
                break;
            case 17:
                strFieldValue = String.valueOf(Stature);
                break;
            case 18:
                strFieldValue = String.valueOf(Avoirdupois);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(CreditGrade);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Proterty);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(OthIDType);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(OthIDNo);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ICNo);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(HomeAddressCode);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(HomeAddress);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(PostalAddress);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(BP);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(Mobile);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getJoinCompanyDate()));
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(Position);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(GrpNo);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(GrpName);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(GrpPhone);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(GrpAddressCode);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(GrpAddress);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getDeathDate()));
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(WorkType);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(PluralityType);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(Degree);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(OccupationCode);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(GrpZipCode);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(SmokeFlag);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(RgtAddress);
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(HomeZipCode);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(Phone2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("SequenceNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SequenceNo = FValue.trim();
            }
            else
            {
                SequenceNo = null;
            }
        }
        if (FCode.equals("AppntGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntGrade = FValue.trim();
            }
            else
            {
                AppntGrade = null;
            }
        }
        if (FCode.equals("RelationToInsured"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelationToInsured = FValue.trim();
            }
            else
            {
                RelationToInsured = null;
            }
        }
        if (FCode.equals("Password"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
            {
                Password = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("Sex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
            {
                Sex = null;
            }
        }
        if (FCode.equals("Birthday"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Birthday = fDate.getDate(FValue);
            }
            else
            {
                Birthday = null;
            }
        }
        if (FCode.equals("NativePlace"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
            {
                NativePlace = null;
            }
        }
        if (FCode.equals("Nationality"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
            {
                Nationality = null;
            }
        }
        if (FCode.equals("Marriage"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
            {
                Marriage = null;
            }
        }
        if (FCode.equals("MarriageDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MarriageDate = fDate.getDate(FValue);
            }
            else
            {
                MarriageDate = null;
            }
        }
        if (FCode.equals("OccupationType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
            {
                OccupationType = null;
            }
        }
        if (FCode.equals("StartWorkDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartWorkDate = fDate.getDate(FValue);
            }
            else
            {
                StartWorkDate = null;
            }
        }
        if (FCode.equals("Salary"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Salary = d;
            }
        }
        if (FCode.equals("Health"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Health = FValue.trim();
            }
            else
            {
                Health = null;
            }
        }
        if (FCode.equals("Stature"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Stature = d;
            }
        }
        if (FCode.equals("Avoirdupois"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Avoirdupois = d;
            }
        }
        if (FCode.equals("CreditGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
            {
                CreditGrade = null;
            }
        }
        if (FCode.equals("IDType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
            {
                IDType = null;
            }
        }
        if (FCode.equals("Proterty"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Proterty = FValue.trim();
            }
            else
            {
                Proterty = null;
            }
        }
        if (FCode.equals("IDNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
            {
                IDNo = null;
            }
        }
        if (FCode.equals("OthIDType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OthIDType = FValue.trim();
            }
            else
            {
                OthIDType = null;
            }
        }
        if (FCode.equals("OthIDNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OthIDNo = FValue.trim();
            }
            else
            {
                OthIDNo = null;
            }
        }
        if (FCode.equals("ICNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ICNo = FValue.trim();
            }
            else
            {
                ICNo = null;
            }
        }
        if (FCode.equals("HomeAddressCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HomeAddressCode = FValue.trim();
            }
            else
            {
                HomeAddressCode = null;
            }
        }
        if (FCode.equals("HomeAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HomeAddress = FValue.trim();
            }
            else
            {
                HomeAddress = null;
            }
        }
        if (FCode.equals("PostalAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
            {
                PostalAddress = null;
            }
        }
        if (FCode.equals("ZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
            {
                ZipCode = null;
            }
        }
        if (FCode.equals("Phone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
            {
                Phone = null;
            }
        }
        if (FCode.equals("BP"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BP = FValue.trim();
            }
            else
            {
                BP = null;
            }
        }
        if (FCode.equals("Mobile"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
            {
                Mobile = null;
            }
        }
        if (FCode.equals("EMail"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
            {
                EMail = null;
            }
        }
        if (FCode.equals("JoinCompanyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                JoinCompanyDate = fDate.getDate(FValue);
            }
            else
            {
                JoinCompanyDate = null;
            }
        }
        if (FCode.equals("Position"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Position = FValue.trim();
            }
            else
            {
                Position = null;
            }
        }
        if (FCode.equals("GrpNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpNo = FValue.trim();
            }
            else
            {
                GrpNo = null;
            }
        }
        if (FCode.equals("GrpName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
            {
                GrpName = null;
            }
        }
        if (FCode.equals("GrpPhone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPhone = FValue.trim();
            }
            else
            {
                GrpPhone = null;
            }
        }
        if (FCode.equals("GrpAddressCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpAddressCode = FValue.trim();
            }
            else
            {
                GrpAddressCode = null;
            }
        }
        if (FCode.equals("GrpAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpAddress = FValue.trim();
            }
            else
            {
                GrpAddress = null;
            }
        }
        if (FCode.equals("DeathDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeathDate = fDate.getDate(FValue);
            }
            else
            {
                DeathDate = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("WorkType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkType = FValue.trim();
            }
            else
            {
                WorkType = null;
            }
        }
        if (FCode.equals("PluralityType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
            {
                PluralityType = null;
            }
        }
        if (FCode.equals("Degree"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
            {
                Degree = null;
            }
        }
        if (FCode.equals("OccupationCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
            {
                OccupationCode = null;
            }
        }
        if (FCode.equals("GrpZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpZipCode = FValue.trim();
            }
            else
            {
                GrpZipCode = null;
            }
        }
        if (FCode.equals("SmokeFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
            {
                SmokeFlag = null;
            }
        }
        if (FCode.equals("RgtAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
            {
                RgtAddress = null;
            }
        }
        if (FCode.equals("HomeZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HomeZipCode = FValue.trim();
            }
            else
            {
                HomeZipCode = null;
            }
        }
        if (FCode.equals("Phone2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Phone2 = FValue.trim();
            }
            else
            {
                Phone2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCAppntIndSchema other = (LCAppntIndSchema) otherObject;
        return
                PolNo.equals(other.getPolNo())
                && CustomerNo.equals(other.getCustomerNo())
                && SequenceNo.equals(other.getSequenceNo())
                && AppntGrade.equals(other.getAppntGrade())
                && RelationToInsured.equals(other.getRelationToInsured())
                && Password.equals(other.getPassword())
                && Name.equals(other.getName())
                && Sex.equals(other.getSex())
                && fDate.getString(Birthday).equals(other.getBirthday())
                && NativePlace.equals(other.getNativePlace())
                && Nationality.equals(other.getNationality())
                && Marriage.equals(other.getMarriage())
                && fDate.getString(MarriageDate).equals(other.getMarriageDate())
                && OccupationType.equals(other.getOccupationType())
                && fDate.getString(StartWorkDate).equals(other.getStartWorkDate())
                && Salary == other.getSalary()
                && Health.equals(other.getHealth())
                && Stature == other.getStature()
                && Avoirdupois == other.getAvoirdupois()
                && CreditGrade.equals(other.getCreditGrade())
                && IDType.equals(other.getIDType())
                && Proterty.equals(other.getProterty())
                && IDNo.equals(other.getIDNo())
                && OthIDType.equals(other.getOthIDType())
                && OthIDNo.equals(other.getOthIDNo())
                && ICNo.equals(other.getICNo())
                && HomeAddressCode.equals(other.getHomeAddressCode())
                && HomeAddress.equals(other.getHomeAddress())
                && PostalAddress.equals(other.getPostalAddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && BP.equals(other.getBP())
                && Mobile.equals(other.getMobile())
                && EMail.equals(other.getEMail())
                &&
                fDate.getString(JoinCompanyDate).equals(other.
                getJoinCompanyDate())
                && Position.equals(other.getPosition())
                && GrpNo.equals(other.getGrpNo())
                && GrpName.equals(other.getGrpName())
                && GrpPhone.equals(other.getGrpPhone())
                && GrpAddressCode.equals(other.getGrpAddressCode())
                && GrpAddress.equals(other.getGrpAddress())
                && fDate.getString(DeathDate).equals(other.getDeathDate())
                && Remark.equals(other.getRemark())
                && State.equals(other.getState())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && WorkType.equals(other.getWorkType())
                && PluralityType.equals(other.getPluralityType())
                && Degree.equals(other.getDegree())
                && OccupationCode.equals(other.getOccupationCode())
                && GrpZipCode.equals(other.getGrpZipCode())
                && SmokeFlag.equals(other.getSmokeFlag())
                && RgtAddress.equals(other.getRgtAddress())
                && HomeZipCode.equals(other.getHomeZipCode())
                && Phone2.equals(other.getPhone2());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PolNo"))
        {
            return 0;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 1;
        }
        if (strFieldName.equals("SequenceNo"))
        {
            return 2;
        }
        if (strFieldName.equals("AppntGrade"))
        {
            return 3;
        }
        if (strFieldName.equals("RelationToInsured"))
        {
            return 4;
        }
        if (strFieldName.equals("Password"))
        {
            return 5;
        }
        if (strFieldName.equals("Name"))
        {
            return 6;
        }
        if (strFieldName.equals("Sex"))
        {
            return 7;
        }
        if (strFieldName.equals("Birthday"))
        {
            return 8;
        }
        if (strFieldName.equals("NativePlace"))
        {
            return 9;
        }
        if (strFieldName.equals("Nationality"))
        {
            return 10;
        }
        if (strFieldName.equals("Marriage"))
        {
            return 11;
        }
        if (strFieldName.equals("MarriageDate"))
        {
            return 12;
        }
        if (strFieldName.equals("OccupationType"))
        {
            return 13;
        }
        if (strFieldName.equals("StartWorkDate"))
        {
            return 14;
        }
        if (strFieldName.equals("Salary"))
        {
            return 15;
        }
        if (strFieldName.equals("Health"))
        {
            return 16;
        }
        if (strFieldName.equals("Stature"))
        {
            return 17;
        }
        if (strFieldName.equals("Avoirdupois"))
        {
            return 18;
        }
        if (strFieldName.equals("CreditGrade"))
        {
            return 19;
        }
        if (strFieldName.equals("IDType"))
        {
            return 20;
        }
        if (strFieldName.equals("Proterty"))
        {
            return 21;
        }
        if (strFieldName.equals("IDNo"))
        {
            return 22;
        }
        if (strFieldName.equals("OthIDType"))
        {
            return 23;
        }
        if (strFieldName.equals("OthIDNo"))
        {
            return 24;
        }
        if (strFieldName.equals("ICNo"))
        {
            return 25;
        }
        if (strFieldName.equals("HomeAddressCode"))
        {
            return 26;
        }
        if (strFieldName.equals("HomeAddress"))
        {
            return 27;
        }
        if (strFieldName.equals("PostalAddress"))
        {
            return 28;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return 29;
        }
        if (strFieldName.equals("Phone"))
        {
            return 30;
        }
        if (strFieldName.equals("BP"))
        {
            return 31;
        }
        if (strFieldName.equals("Mobile"))
        {
            return 32;
        }
        if (strFieldName.equals("EMail"))
        {
            return 33;
        }
        if (strFieldName.equals("JoinCompanyDate"))
        {
            return 34;
        }
        if (strFieldName.equals("Position"))
        {
            return 35;
        }
        if (strFieldName.equals("GrpNo"))
        {
            return 36;
        }
        if (strFieldName.equals("GrpName"))
        {
            return 37;
        }
        if (strFieldName.equals("GrpPhone"))
        {
            return 38;
        }
        if (strFieldName.equals("GrpAddressCode"))
        {
            return 39;
        }
        if (strFieldName.equals("GrpAddress"))
        {
            return 40;
        }
        if (strFieldName.equals("DeathDate"))
        {
            return 41;
        }
        if (strFieldName.equals("Remark"))
        {
            return 42;
        }
        if (strFieldName.equals("State"))
        {
            return 43;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 44;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 45;
        }
        if (strFieldName.equals("WorkType"))
        {
            return 46;
        }
        if (strFieldName.equals("PluralityType"))
        {
            return 47;
        }
        if (strFieldName.equals("Degree"))
        {
            return 48;
        }
        if (strFieldName.equals("OccupationCode"))
        {
            return 49;
        }
        if (strFieldName.equals("GrpZipCode"))
        {
            return 50;
        }
        if (strFieldName.equals("SmokeFlag"))
        {
            return 51;
        }
        if (strFieldName.equals("RgtAddress"))
        {
            return 52;
        }
        if (strFieldName.equals("HomeZipCode"))
        {
            return 53;
        }
        if (strFieldName.equals("Phone2"))
        {
            return 54;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PolNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "SequenceNo";
                break;
            case 3:
                strFieldName = "AppntGrade";
                break;
            case 4:
                strFieldName = "RelationToInsured";
                break;
            case 5:
                strFieldName = "Password";
                break;
            case 6:
                strFieldName = "Name";
                break;
            case 7:
                strFieldName = "Sex";
                break;
            case 8:
                strFieldName = "Birthday";
                break;
            case 9:
                strFieldName = "NativePlace";
                break;
            case 10:
                strFieldName = "Nationality";
                break;
            case 11:
                strFieldName = "Marriage";
                break;
            case 12:
                strFieldName = "MarriageDate";
                break;
            case 13:
                strFieldName = "OccupationType";
                break;
            case 14:
                strFieldName = "StartWorkDate";
                break;
            case 15:
                strFieldName = "Salary";
                break;
            case 16:
                strFieldName = "Health";
                break;
            case 17:
                strFieldName = "Stature";
                break;
            case 18:
                strFieldName = "Avoirdupois";
                break;
            case 19:
                strFieldName = "CreditGrade";
                break;
            case 20:
                strFieldName = "IDType";
                break;
            case 21:
                strFieldName = "Proterty";
                break;
            case 22:
                strFieldName = "IDNo";
                break;
            case 23:
                strFieldName = "OthIDType";
                break;
            case 24:
                strFieldName = "OthIDNo";
                break;
            case 25:
                strFieldName = "ICNo";
                break;
            case 26:
                strFieldName = "HomeAddressCode";
                break;
            case 27:
                strFieldName = "HomeAddress";
                break;
            case 28:
                strFieldName = "PostalAddress";
                break;
            case 29:
                strFieldName = "ZipCode";
                break;
            case 30:
                strFieldName = "Phone";
                break;
            case 31:
                strFieldName = "BP";
                break;
            case 32:
                strFieldName = "Mobile";
                break;
            case 33:
                strFieldName = "EMail";
                break;
            case 34:
                strFieldName = "JoinCompanyDate";
                break;
            case 35:
                strFieldName = "Position";
                break;
            case 36:
                strFieldName = "GrpNo";
                break;
            case 37:
                strFieldName = "GrpName";
                break;
            case 38:
                strFieldName = "GrpPhone";
                break;
            case 39:
                strFieldName = "GrpAddressCode";
                break;
            case 40:
                strFieldName = "GrpAddress";
                break;
            case 41:
                strFieldName = "DeathDate";
                break;
            case 42:
                strFieldName = "Remark";
                break;
            case 43:
                strFieldName = "State";
                break;
            case 44:
                strFieldName = "ModifyDate";
                break;
            case 45:
                strFieldName = "ModifyTime";
                break;
            case 46:
                strFieldName = "WorkType";
                break;
            case 47:
                strFieldName = "PluralityType";
                break;
            case 48:
                strFieldName = "Degree";
                break;
            case 49:
                strFieldName = "OccupationCode";
                break;
            case 50:
                strFieldName = "GrpZipCode";
                break;
            case 51:
                strFieldName = "SmokeFlag";
                break;
            case 52:
                strFieldName = "RgtAddress";
                break;
            case 53:
                strFieldName = "HomeZipCode";
                break;
            case 54:
                strFieldName = "Phone2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SequenceNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelationToInsured"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Password"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Sex"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Birthday"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("NativePlace"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Nationality"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Marriage"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MarriageDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OccupationType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartWorkDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Salary"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Health"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Stature"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Avoirdupois"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CreditGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IDType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Proterty"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IDNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OthIDType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OthIDNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ICNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeAddressCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostalAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BP"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Mobile"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EMail"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("JoinCompanyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Position"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPhone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpAddressCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeathDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WorkType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PluralityType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Degree"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OccupationCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SmokeFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 34:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 35:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 36:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 37:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 38:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 39:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 40:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 41:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 42:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 43:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 44:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 45:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 46:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 47:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 48:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 49:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 50:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 51:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 52:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 53:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 54:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
