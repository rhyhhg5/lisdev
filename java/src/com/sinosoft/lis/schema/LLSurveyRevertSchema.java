/*
 * <p>ClassName: LLSurveyRevertSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 理赔新流程图
 * @CreateDate：2005-01-12
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLSurveyRevertDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LLSurveyRevertSchema implements Schema
{
    // @Field
    /** 调查流水号 */
    private String SerialNo;
    /** 回复次序号码 */
    private int RevertCount;
    /** 案件号 */
    private String ClmCaseNo;
    /** 调查回复人 */
    private String SurveyOperator;
    /** 调查回复人名称 */
    private String SurveyName;
    /** 调查地点 */
    private String SurveySite;
    /** 调查结论 */
    private String result;
    /** 调查开始日期 */
    private Date SurveyStartDate;
    /** 调查结束日期 */
    private Date SurveyEndDate;
    /** 管理机构 */
    private String MngCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 接收人 */
    private String ReceiveMan;
    /** 回复状态 */
    private String RevertState;
    /** 类型 */
    private String Flag;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLSurveyRevertSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SerialNo";
        pk[1] = "RevertCount";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public int getRevertCount()
    {
        return RevertCount;
    }

    public void setRevertCount(int aRevertCount)
    {
        RevertCount = aRevertCount;
    }

    public void setRevertCount(String aRevertCount)
    {
        if (aRevertCount != null && !aRevertCount.equals(""))
        {
            Integer tInteger = new Integer(aRevertCount);
            int i = tInteger.intValue();
            RevertCount = i;
        }
    }

    public String getClmCaseNo()
    {
        if (ClmCaseNo != null && !ClmCaseNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmCaseNo = StrTool.unicodeToGBK(ClmCaseNo);
        }
        return ClmCaseNo;
    }

    public void setClmCaseNo(String aClmCaseNo)
    {
        ClmCaseNo = aClmCaseNo;
    }

    public String getSurveyOperator()
    {
        if (SurveyOperator != null && !SurveyOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SurveyOperator = StrTool.unicodeToGBK(SurveyOperator);
        }
        return SurveyOperator;
    }

    public void setSurveyOperator(String aSurveyOperator)
    {
        SurveyOperator = aSurveyOperator;
    }

    public String getSurveyName()
    {
        if (SurveyName != null && !SurveyName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SurveyName = StrTool.unicodeToGBK(SurveyName);
        }
        return SurveyName;
    }

    public void setSurveyName(String aSurveyName)
    {
        SurveyName = aSurveyName;
    }

    public String getSurveySite()
    {
        if (SurveySite != null && !SurveySite.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SurveySite = StrTool.unicodeToGBK(SurveySite);
        }
        return SurveySite;
    }

    public void setSurveySite(String aSurveySite)
    {
        SurveySite = aSurveySite;
    }

    public String getresult()
    {
        if (result != null && !result.equals("") && SysConst.CHANGECHARSET == true)
        {
            result = StrTool.unicodeToGBK(result);
        }
        return result;
    }

    public void setresult(String aresult)
    {
        result = aresult;
    }

    public String getSurveyStartDate()
    {
        if (SurveyStartDate != null)
        {
            return fDate.getString(SurveyStartDate);
        }
        else
        {
            return null;
        }
    }

    public void setSurveyStartDate(Date aSurveyStartDate)
    {
        SurveyStartDate = aSurveyStartDate;
    }

    public void setSurveyStartDate(String aSurveyStartDate)
    {
        if (aSurveyStartDate != null && !aSurveyStartDate.equals(""))
        {
            SurveyStartDate = fDate.getDate(aSurveyStartDate);
        }
        else
        {
            SurveyStartDate = null;
        }
    }

    public String getSurveyEndDate()
    {
        if (SurveyEndDate != null)
        {
            return fDate.getString(SurveyEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setSurveyEndDate(Date aSurveyEndDate)
    {
        SurveyEndDate = aSurveyEndDate;
    }

    public void setSurveyEndDate(String aSurveyEndDate)
    {
        if (aSurveyEndDate != null && !aSurveyEndDate.equals(""))
        {
            SurveyEndDate = fDate.getDate(aSurveyEndDate);
        }
        else
        {
            SurveyEndDate = null;
        }
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getReceiveMan()
    {
        if (ReceiveMan != null && !ReceiveMan.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReceiveMan = StrTool.unicodeToGBK(ReceiveMan);
        }
        return ReceiveMan;
    }

    public void setReceiveMan(String aReceiveMan)
    {
        ReceiveMan = aReceiveMan;
    }

    public String getRevertState()
    {
        if (RevertState != null && !RevertState.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RevertState = StrTool.unicodeToGBK(RevertState);
        }
        return RevertState;
    }

    public void setRevertState(String aRevertState)
    {
        RevertState = aRevertState;
    }

    public String getFlag()
    {
        if (Flag != null && !Flag.equals("") && SysConst.CHANGECHARSET == true)
        {
            Flag = StrTool.unicodeToGBK(Flag);
        }
        return Flag;
    }

    public void setFlag(String aFlag)
    {
        Flag = aFlag;
    }

    /**
     * 使用另外一个 LLSurveyRevertSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLSurveyRevertSchema aLLSurveyRevertSchema)
    {
        this.SerialNo = aLLSurveyRevertSchema.getSerialNo();
        this.RevertCount = aLLSurveyRevertSchema.getRevertCount();
        this.ClmCaseNo = aLLSurveyRevertSchema.getClmCaseNo();
        this.SurveyOperator = aLLSurveyRevertSchema.getSurveyOperator();
        this.SurveyName = aLLSurveyRevertSchema.getSurveyName();
        this.SurveySite = aLLSurveyRevertSchema.getSurveySite();
        this.result = aLLSurveyRevertSchema.getresult();
        this.SurveyStartDate = fDate.getDate(aLLSurveyRevertSchema.
                                             getSurveyStartDate());
        this.SurveyEndDate = fDate.getDate(aLLSurveyRevertSchema.
                                           getSurveyEndDate());
        this.MngCom = aLLSurveyRevertSchema.getMngCom();
        this.MakeDate = fDate.getDate(aLLSurveyRevertSchema.getMakeDate());
        this.MakeTime = aLLSurveyRevertSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLSurveyRevertSchema.getModifyDate());
        this.ModifyTime = aLLSurveyRevertSchema.getModifyTime();
        this.ReceiveMan = aLLSurveyRevertSchema.getReceiveMan();
        this.RevertState = aLLSurveyRevertSchema.getRevertState();
        this.Flag = aLLSurveyRevertSchema.getFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            this.RevertCount = rs.getInt("RevertCount");
            if (rs.getString("ClmCaseNo") == null)
            {
                this.ClmCaseNo = null;
            }
            else
            {
                this.ClmCaseNo = rs.getString("ClmCaseNo").trim();
            }

            if (rs.getString("SurveyOperator") == null)
            {
                this.SurveyOperator = null;
            }
            else
            {
                this.SurveyOperator = rs.getString("SurveyOperator").trim();
            }

            if (rs.getString("SurveyName") == null)
            {
                this.SurveyName = null;
            }
            else
            {
                this.SurveyName = rs.getString("SurveyName").trim();
            }

            if (rs.getString("SurveySite") == null)
            {
                this.SurveySite = null;
            }
            else
            {
                this.SurveySite = rs.getString("SurveySite").trim();
            }

            if (rs.getString("result") == null)
            {
                this.result = null;
            }
            else
            {
                this.result = rs.getString("result").trim();
            }

            this.SurveyStartDate = rs.getDate("SurveyStartDate");
            this.SurveyEndDate = rs.getDate("SurveyEndDate");
            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ReceiveMan") == null)
            {
                this.ReceiveMan = null;
            }
            else
            {
                this.ReceiveMan = rs.getString("ReceiveMan").trim();
            }

            if (rs.getString("RevertState") == null)
            {
                this.RevertState = null;
            }
            else
            {
                this.RevertState = rs.getString("RevertState").trim();
            }

            if (rs.getString("Flag") == null)
            {
                this.Flag = null;
            }
            else
            {
                this.Flag = rs.getString("Flag").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSurveyRevertSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLSurveyRevertSchema getSchema()
    {
        LLSurveyRevertSchema aLLSurveyRevertSchema = new LLSurveyRevertSchema();
        aLLSurveyRevertSchema.setSchema(this);
        return aLLSurveyRevertSchema;
    }

    public LLSurveyRevertDB getDB()
    {
        LLSurveyRevertDB aDBOper = new LLSurveyRevertDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSurveyRevert描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(RevertCount) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmCaseNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SurveyOperator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SurveyName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SurveySite)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(result)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            SurveyStartDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            SurveyEndDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReceiveMan)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RevertState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Flag));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSurveyRevert>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RevertCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            ClmCaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            SurveyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
            SurveyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            SurveySite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            result = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            SurveyStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            SurveyEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            ReceiveMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            RevertState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                  SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLSurveyRevertSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("RevertCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RevertCount));
        }
        if (FCode.equals("ClmCaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmCaseNo));
        }
        if (FCode.equals("SurveyOperator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SurveyOperator));
        }
        if (FCode.equals("SurveyName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SurveyName));
        }
        if (FCode.equals("SurveySite"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SurveySite));
        }
        if (FCode.equals("result"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(result));
        }
        if (FCode.equals("SurveyStartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getSurveyStartDate()));
        }
        if (FCode.equals("SurveyEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getSurveyEndDate()));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MngCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("ReceiveMan"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReceiveMan));
        }
        if (FCode.equals("RevertState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RevertState));
        }
        if (FCode.equals("Flag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Flag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(RevertCount);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ClmCaseNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SurveyOperator);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SurveyName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(SurveySite);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(result);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSurveyStartDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSurveyEndDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ReceiveMan);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(RevertState);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Flag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("RevertCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RevertCount = i;
            }
        }
        if (FCode.equals("ClmCaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmCaseNo = FValue.trim();
            }
            else
            {
                ClmCaseNo = null;
            }
        }
        if (FCode.equals("SurveyOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyOperator = FValue.trim();
            }
            else
            {
                SurveyOperator = null;
            }
        }
        if (FCode.equals("SurveyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyName = FValue.trim();
            }
            else
            {
                SurveyName = null;
            }
        }
        if (FCode.equals("SurveySite"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveySite = FValue.trim();
            }
            else
            {
                SurveySite = null;
            }
        }
        if (FCode.equals("result"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                result = FValue.trim();
            }
            else
            {
                result = null;
            }
        }
        if (FCode.equals("SurveyStartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyStartDate = fDate.getDate(FValue);
            }
            else
            {
                SurveyStartDate = null;
            }
        }
        if (FCode.equals("SurveyEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyEndDate = fDate.getDate(FValue);
            }
            else
            {
                SurveyEndDate = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("ReceiveMan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReceiveMan = FValue.trim();
            }
            else
            {
                ReceiveMan = null;
            }
        }
        if (FCode.equals("RevertState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RevertState = FValue.trim();
            }
            else
            {
                RevertState = null;
            }
        }
        if (FCode.equals("Flag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Flag = FValue.trim();
            }
            else
            {
                Flag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLSurveyRevertSchema other = (LLSurveyRevertSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && RevertCount == other.getRevertCount()
                && ClmCaseNo.equals(other.getClmCaseNo())
                && SurveyOperator.equals(other.getSurveyOperator())
                && SurveyName.equals(other.getSurveyName())
                && SurveySite.equals(other.getSurveySite())
                && result.equals(other.getresult())
                &&
                fDate.getString(SurveyStartDate).equals(other.
                getSurveyStartDate())
                && fDate.getString(SurveyEndDate).equals(other.getSurveyEndDate())
                && MngCom.equals(other.getMngCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ReceiveMan.equals(other.getReceiveMan())
                && RevertState.equals(other.getRevertState())
                && Flag.equals(other.getFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("RevertCount"))
        {
            return 1;
        }
        if (strFieldName.equals("ClmCaseNo"))
        {
            return 2;
        }
        if (strFieldName.equals("SurveyOperator"))
        {
            return 3;
        }
        if (strFieldName.equals("SurveyName"))
        {
            return 4;
        }
        if (strFieldName.equals("SurveySite"))
        {
            return 5;
        }
        if (strFieldName.equals("result"))
        {
            return 6;
        }
        if (strFieldName.equals("SurveyStartDate"))
        {
            return 7;
        }
        if (strFieldName.equals("SurveyEndDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 11;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 13;
        }
        if (strFieldName.equals("ReceiveMan"))
        {
            return 14;
        }
        if (strFieldName.equals("RevertState"))
        {
            return 15;
        }
        if (strFieldName.equals("Flag"))
        {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "RevertCount";
                break;
            case 2:
                strFieldName = "ClmCaseNo";
                break;
            case 3:
                strFieldName = "SurveyOperator";
                break;
            case 4:
                strFieldName = "SurveyName";
                break;
            case 5:
                strFieldName = "SurveySite";
                break;
            case 6:
                strFieldName = "result";
                break;
            case 7:
                strFieldName = "SurveyStartDate";
                break;
            case 8:
                strFieldName = "SurveyEndDate";
                break;
            case 9:
                strFieldName = "MngCom";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "ModifyDate";
                break;
            case 13:
                strFieldName = "ModifyTime";
                break;
            case 14:
                strFieldName = "ReceiveMan";
                break;
            case 15:
                strFieldName = "RevertState";
                break;
            case 16:
                strFieldName = "Flag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RevertCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ClmCaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveySite"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("result"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyStartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SurveyEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiveMan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RevertState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Flag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
