/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.HISTORYDB;

/*
 * <p>ClassName: HISTORYSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2014-04-23
 */
public class HISTORYSchema implements Schema, Cloneable
{
	// @Field
	/** Customernno */
	private String CUSTOMERNNO;
	/** Hospitalname */
	private String HOSPITALNAME;
	/** Illname */
	private String ILLNAME;
	/** Inputdate */
	private Date INPUTDATE;
	/** Outputdate */
	private Date OUTPUTDATE;
	/** Payid */
	private String PAYID;
	/** Dimen2name */
	private String DIMEN2NAME;
	/** Price */
	private double PRICE;
	/** Unit */
	private double UNIT;
	/** Total */
	private double TOTAL;
	/** Pay */
	private double PAY;
	/** Etltime */
	private Date ETLTIME;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public HISTORYSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		HISTORYSchema cloned = (HISTORYSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCUSTOMERNNO()
	{
		return CUSTOMERNNO;
	}
	public void setCUSTOMERNNO(String aCUSTOMERNNO)
	{
		CUSTOMERNNO = aCUSTOMERNNO;
	}
	public String getHOSPITALNAME()
	{
		return HOSPITALNAME;
	}
	public void setHOSPITALNAME(String aHOSPITALNAME)
	{
		HOSPITALNAME = aHOSPITALNAME;
	}
	public String getILLNAME()
	{
		return ILLNAME;
	}
	public void setILLNAME(String aILLNAME)
	{
		ILLNAME = aILLNAME;
	}
	public String getINPUTDATE()
	{
		if( INPUTDATE != null )
			return fDate.getString(INPUTDATE);
		else
			return null;
	}
	public void setINPUTDATE(Date aINPUTDATE)
	{
		INPUTDATE = aINPUTDATE;
	}
	public void setINPUTDATE(String aINPUTDATE)
	{
		if (aINPUTDATE != null && !aINPUTDATE.equals("") )
		{
			INPUTDATE = fDate.getDate( aINPUTDATE );
		}
		else
			INPUTDATE = null;
	}

	public String getOUTPUTDATE()
	{
		if( OUTPUTDATE != null )
			return fDate.getString(OUTPUTDATE);
		else
			return null;
	}
	public void setOUTPUTDATE(Date aOUTPUTDATE)
	{
		OUTPUTDATE = aOUTPUTDATE;
	}
	public void setOUTPUTDATE(String aOUTPUTDATE)
	{
		if (aOUTPUTDATE != null && !aOUTPUTDATE.equals("") )
		{
			OUTPUTDATE = fDate.getDate( aOUTPUTDATE );
		}
		else
			OUTPUTDATE = null;
	}

	public String getPAYID()
	{
		return PAYID;
	}
	public void setPAYID(String aPAYID)
	{
		PAYID = aPAYID;
	}
	public String getDIMEN2NAME()
	{
		return DIMEN2NAME;
	}
	public void setDIMEN2NAME(String aDIMEN2NAME)
	{
		DIMEN2NAME = aDIMEN2NAME;
	}
	public double getPRICE()
	{
		return PRICE;
	}
	public void setPRICE(double aPRICE)
	{
		PRICE = Arith.round(aPRICE,5);
	}
	public void setPRICE(String aPRICE)
	{
		if (aPRICE != null && !aPRICE.equals(""))
		{
			Double tDouble = new Double(aPRICE);
			double d = tDouble.doubleValue();
                PRICE = Arith.round(d,5);
		}
	}

	public double getUNIT()
	{
		return UNIT;
	}
	public void setUNIT(double aUNIT)
	{
		UNIT = Arith.round(aUNIT,5);
	}
	public void setUNIT(String aUNIT)
	{
		if (aUNIT != null && !aUNIT.equals(""))
		{
			Double tDouble = new Double(aUNIT);
			double d = tDouble.doubleValue();
                UNIT = Arith.round(d,5);
		}
	}

	public double getTOTAL()
	{
		return TOTAL;
	}
	public void setTOTAL(double aTOTAL)
	{
		TOTAL = Arith.round(aTOTAL,5);
	}
	public void setTOTAL(String aTOTAL)
	{
		if (aTOTAL != null && !aTOTAL.equals(""))
		{
			Double tDouble = new Double(aTOTAL);
			double d = tDouble.doubleValue();
                TOTAL = Arith.round(d,5);
		}
	}

	public double getPAY()
	{
		return PAY;
	}
	public void setPAY(double aPAY)
	{
		PAY = Arith.round(aPAY,5);
	}
	public void setPAY(String aPAY)
	{
		if (aPAY != null && !aPAY.equals(""))
		{
			Double tDouble = new Double(aPAY);
			double d = tDouble.doubleValue();
                PAY = Arith.round(d,5);
		}
	}

	public String getETLTIME()
	{
		if( ETLTIME != null )
			return fDate.getString(ETLTIME);
		else
			return null;
	}
	public void setETLTIME(Date aETLTIME)
	{
		ETLTIME = aETLTIME;
	}
	public void setETLTIME(String aETLTIME)
	{
		if (aETLTIME != null && !aETLTIME.equals("") )
		{
			ETLTIME = fDate.getDate( aETLTIME );
		}
		else
			ETLTIME = null;
	}


	/**
	* 使用另外一个 HISTORYSchema 对象给 Schema 赋值
	* @param: aHISTORYSchema HISTORYSchema
	**/
	public void setSchema(HISTORYSchema aHISTORYSchema)
	{
		this.CUSTOMERNNO = aHISTORYSchema.getCUSTOMERNNO();
		this.HOSPITALNAME = aHISTORYSchema.getHOSPITALNAME();
		this.ILLNAME = aHISTORYSchema.getILLNAME();
		this.INPUTDATE = fDate.getDate( aHISTORYSchema.getINPUTDATE());
		this.OUTPUTDATE = fDate.getDate( aHISTORYSchema.getOUTPUTDATE());
		this.PAYID = aHISTORYSchema.getPAYID();
		this.DIMEN2NAME = aHISTORYSchema.getDIMEN2NAME();
		this.PRICE = aHISTORYSchema.getPRICE();
		this.UNIT = aHISTORYSchema.getUNIT();
		this.TOTAL = aHISTORYSchema.getTOTAL();
		this.PAY = aHISTORYSchema.getPAY();
		this.ETLTIME = fDate.getDate( aHISTORYSchema.getETLTIME());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CUSTOMERNNO") == null )
				this.CUSTOMERNNO = null;
			else
				this.CUSTOMERNNO = rs.getString("CUSTOMERNNO").trim();

			if( rs.getString("HOSPITALNAME") == null )
				this.HOSPITALNAME = null;
			else
				this.HOSPITALNAME = rs.getString("HOSPITALNAME").trim();

			if( rs.getString("ILLNAME") == null )
				this.ILLNAME = null;
			else
				this.ILLNAME = rs.getString("ILLNAME").trim();

			this.INPUTDATE = rs.getDate("INPUTDATE");
			this.OUTPUTDATE = rs.getDate("OUTPUTDATE");
			if( rs.getString("PAYID") == null )
				this.PAYID = null;
			else
				this.PAYID = rs.getString("PAYID").trim();

			if( rs.getString("DIMEN2NAME") == null )
				this.DIMEN2NAME = null;
			else
				this.DIMEN2NAME = rs.getString("DIMEN2NAME").trim();

			this.PRICE = rs.getDouble("PRICE");
			this.UNIT = rs.getDouble("UNIT");
			this.TOTAL = rs.getDouble("TOTAL");
			this.PAY = rs.getDouble("PAY");
			this.ETLTIME = rs.getDate("ETLTIME");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的HISTORY表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HISTORYSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public HISTORYSchema getSchema()
	{
		HISTORYSchema aHISTORYSchema = new HISTORYSchema();
		aHISTORYSchema.setSchema(this);
		return aHISTORYSchema;
	}

	public HISTORYDB getDB()
	{
		HISTORYDB aDBOper = new HISTORYDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHISTORY描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CUSTOMERNNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HOSPITALNAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ILLNAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( INPUTDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OUTPUTDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PAYID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DIMEN2NAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PRICE));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(UNIT));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TOTAL));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PAY));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ETLTIME )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHISTORY>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CUSTOMERNNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			HOSPITALNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ILLNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			INPUTDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			OUTPUTDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			PAYID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DIMEN2NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			PRICE = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			UNIT = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			TOTAL = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			PAY = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			ETLTIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HISTORYSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CUSTOMERNNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CUSTOMERNNO));
		}
		if (FCode.equals("HOSPITALNAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HOSPITALNAME));
		}
		if (FCode.equals("ILLNAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ILLNAME));
		}
		if (FCode.equals("INPUTDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getINPUTDATE()));
		}
		if (FCode.equals("OUTPUTDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOUTPUTDATE()));
		}
		if (FCode.equals("PAYID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PAYID));
		}
		if (FCode.equals("DIMEN2NAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DIMEN2NAME));
		}
		if (FCode.equals("PRICE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PRICE));
		}
		if (FCode.equals("UNIT"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UNIT));
		}
		if (FCode.equals("TOTAL"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TOTAL));
		}
		if (FCode.equals("PAY"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PAY));
		}
		if (FCode.equals("ETLTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getETLTIME()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CUSTOMERNNO);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(HOSPITALNAME);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ILLNAME);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getINPUTDATE()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOUTPUTDATE()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(PAYID);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DIMEN2NAME);
				break;
			case 7:
				strFieldValue = String.valueOf(PRICE);
				break;
			case 8:
				strFieldValue = String.valueOf(UNIT);
				break;
			case 9:
				strFieldValue = String.valueOf(TOTAL);
				break;
			case 10:
				strFieldValue = String.valueOf(PAY);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getETLTIME()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CUSTOMERNNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CUSTOMERNNO = FValue.trim();
			}
			else
				CUSTOMERNNO = null;
		}
		if (FCode.equalsIgnoreCase("HOSPITALNAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HOSPITALNAME = FValue.trim();
			}
			else
				HOSPITALNAME = null;
		}
		if (FCode.equalsIgnoreCase("ILLNAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ILLNAME = FValue.trim();
			}
			else
				ILLNAME = null;
		}
		if (FCode.equalsIgnoreCase("INPUTDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				INPUTDATE = fDate.getDate( FValue );
			}
			else
				INPUTDATE = null;
		}
		if (FCode.equalsIgnoreCase("OUTPUTDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OUTPUTDATE = fDate.getDate( FValue );
			}
			else
				OUTPUTDATE = null;
		}
		if (FCode.equalsIgnoreCase("PAYID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PAYID = FValue.trim();
			}
			else
				PAYID = null;
		}
		if (FCode.equalsIgnoreCase("DIMEN2NAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DIMEN2NAME = FValue.trim();
			}
			else
				DIMEN2NAME = null;
		}
		if (FCode.equalsIgnoreCase("PRICE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PRICE = d;
			}
		}
		if (FCode.equalsIgnoreCase("UNIT"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				UNIT = d;
			}
		}
		if (FCode.equalsIgnoreCase("TOTAL"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TOTAL = d;
			}
		}
		if (FCode.equalsIgnoreCase("PAY"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PAY = d;
			}
		}
		if (FCode.equalsIgnoreCase("ETLTIME"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ETLTIME = fDate.getDate( FValue );
			}
			else
				ETLTIME = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		HISTORYSchema other = (HISTORYSchema)otherObject;
		return
			(CUSTOMERNNO == null ? other.getCUSTOMERNNO() == null : CUSTOMERNNO.equals(other.getCUSTOMERNNO()))
			&& (HOSPITALNAME == null ? other.getHOSPITALNAME() == null : HOSPITALNAME.equals(other.getHOSPITALNAME()))
			&& (ILLNAME == null ? other.getILLNAME() == null : ILLNAME.equals(other.getILLNAME()))
			&& (INPUTDATE == null ? other.getINPUTDATE() == null : fDate.getString(INPUTDATE).equals(other.getINPUTDATE()))
			&& (OUTPUTDATE == null ? other.getOUTPUTDATE() == null : fDate.getString(OUTPUTDATE).equals(other.getOUTPUTDATE()))
			&& (PAYID == null ? other.getPAYID() == null : PAYID.equals(other.getPAYID()))
			&& (DIMEN2NAME == null ? other.getDIMEN2NAME() == null : DIMEN2NAME.equals(other.getDIMEN2NAME()))
			&& PRICE == other.getPRICE()
			&& UNIT == other.getUNIT()
			&& TOTAL == other.getTOTAL()
			&& PAY == other.getPAY()
			&& (ETLTIME == null ? other.getETLTIME() == null : fDate.getString(ETLTIME).equals(other.getETLTIME()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CUSTOMERNNO") ) {
			return 0;
		}
		if( strFieldName.equals("HOSPITALNAME") ) {
			return 1;
		}
		if( strFieldName.equals("ILLNAME") ) {
			return 2;
		}
		if( strFieldName.equals("INPUTDATE") ) {
			return 3;
		}
		if( strFieldName.equals("OUTPUTDATE") ) {
			return 4;
		}
		if( strFieldName.equals("PAYID") ) {
			return 5;
		}
		if( strFieldName.equals("DIMEN2NAME") ) {
			return 6;
		}
		if( strFieldName.equals("PRICE") ) {
			return 7;
		}
		if( strFieldName.equals("UNIT") ) {
			return 8;
		}
		if( strFieldName.equals("TOTAL") ) {
			return 9;
		}
		if( strFieldName.equals("PAY") ) {
			return 10;
		}
		if( strFieldName.equals("ETLTIME") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CUSTOMERNNO";
				break;
			case 1:
				strFieldName = "HOSPITALNAME";
				break;
			case 2:
				strFieldName = "ILLNAME";
				break;
			case 3:
				strFieldName = "INPUTDATE";
				break;
			case 4:
				strFieldName = "OUTPUTDATE";
				break;
			case 5:
				strFieldName = "PAYID";
				break;
			case 6:
				strFieldName = "DIMEN2NAME";
				break;
			case 7:
				strFieldName = "PRICE";
				break;
			case 8:
				strFieldName = "UNIT";
				break;
			case 9:
				strFieldName = "TOTAL";
				break;
			case 10:
				strFieldName = "PAY";
				break;
			case 11:
				strFieldName = "ETLTIME";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CUSTOMERNNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HOSPITALNAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ILLNAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("INPUTDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OUTPUTDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PAYID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DIMEN2NAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PRICE") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("UNIT") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TOTAL") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PAY") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ETLTIME") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
