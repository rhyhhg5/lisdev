/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LADimissionSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-11-15
 */
public class LADimissionSchema implements Schema, Cloneable {
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 离职次数 */
    private int DepartTimes;
    /** 离职时间 */
    private Date DepartDate;
    /** 离职原因 */
    private String DepartRsn;
    /** 《保险代理合同书》回收标志 */
    private String ContractFlag;
    /** 工作证回收标志 */
    private String WorkFlag;
    /** 展业证回收标志 */
    private String PbcFlag;
    /** 保险费暂收据回收标志 */
    private String ReceiptFlag;
    /** 丢失标志 */
    private String LostFlag;
    /** 结清标志 */
    private String CheckFlag;
    /** 停止佣金发放标志 */
    private String WageFlag;
    /** 业务人员转任内勤人员标志 */
    private String TurnFlag;
    /** 备注 */
    private String Noti;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 展业类型 */
    private String BranchType;
    /** 展业机构外部编码 */
    private String BranchAttr;
    /** 离职状态 */
    private String DepartState;
    /** 押金回退标记 */
    private String ReturnFlag;
    /** 有价单证回销标记 */
    private String DestoryFlag;
    /** 保单送达标记 */
    private String ArriveFlag;
    /** 离职申请日期 */
    private Date ApplyDate;
    /** 渠道 */
    private String BranchType2;
    /** 办公用品提交标志 */
    private String OfficeFlag;
    /** 工作移交标记 */
    private String HandOverFlag;
    /** 活动手册提交标记 */
    private String ActiveFlag;
    /** 客户资料提交标记 */
    private String ClientFlag;
    /** 取消用户标志 */
    private String UserInvalidFlag;
    /** 所借图书归还标志 */
    private String ReturnBookFlag;
    /** 劳动合同解除通知发放标志 */
    private String InformFlag;
    /** 黑名单标记 */
    private String BlackFlag;
    /** 黑名单原因 */
    private String BlackListRsn;
    /** 孤儿单分配日期 */
    private Date OrphanDate;
    /** 孤儿单分配数量 */
    private int OrphanSum;
    /** 养老金是否领取 */
    private String AnnuityFlag;
    /** 离司回访是否通过 */
    private String VisitFlag;
    /** 资格证销毁标记 */
    private String QualityDestFlag;

    public static final int FIELDNUM = 40; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LADimissionSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AgentCode";
        pk[1] = "DepartTimes";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LADimissionSchema cloned = (LADimissionSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public int getDepartTimes() {
        return DepartTimes;
    }

    public void setDepartTimes(int aDepartTimes) {
        DepartTimes = aDepartTimes;
    }

    public void setDepartTimes(String aDepartTimes) {
        if (aDepartTimes != null && !aDepartTimes.equals("")) {
            Integer tInteger = new Integer(aDepartTimes);
            int i = tInteger.intValue();
            DepartTimes = i;
        }
    }

    public String getDepartDate() {
        if (DepartDate != null) {
            return fDate.getString(DepartDate);
        } else {
            return null;
        }
    }

    public void setDepartDate(Date aDepartDate) {
        DepartDate = aDepartDate;
    }

    public void setDepartDate(String aDepartDate) {
        if (aDepartDate != null && !aDepartDate.equals("")) {
            DepartDate = fDate.getDate(aDepartDate);
        } else {
            DepartDate = null;
        }
    }

    public String getDepartRsn() {
        return DepartRsn;
    }

    public void setDepartRsn(String aDepartRsn) {
        DepartRsn = aDepartRsn;
    }

    public String getContractFlag() {
        return ContractFlag;
    }

    public void setContractFlag(String aContractFlag) {
        ContractFlag = aContractFlag;
    }

    public String getWorkFlag() {
        return WorkFlag;
    }

    public void setWorkFlag(String aWorkFlag) {
        WorkFlag = aWorkFlag;
    }

    public String getPbcFlag() {
        return PbcFlag;
    }

    public void setPbcFlag(String aPbcFlag) {
        PbcFlag = aPbcFlag;
    }

    public String getReceiptFlag() {
        return ReceiptFlag;
    }

    public void setReceiptFlag(String aReceiptFlag) {
        ReceiptFlag = aReceiptFlag;
    }

    public String getLostFlag() {
        return LostFlag;
    }

    public void setLostFlag(String aLostFlag) {
        LostFlag = aLostFlag;
    }

    public String getCheckFlag() {
        return CheckFlag;
    }

    public void setCheckFlag(String aCheckFlag) {
        CheckFlag = aCheckFlag;
    }

    public String getWageFlag() {
        return WageFlag;
    }

    public void setWageFlag(String aWageFlag) {
        WageFlag = aWageFlag;
    }

    public String getTurnFlag() {
        return TurnFlag;
    }

    public void setTurnFlag(String aTurnFlag) {
        TurnFlag = aTurnFlag;
    }

    public String getNoti() {
        return Noti;
    }

    public void setNoti(String aNoti) {
        Noti = aNoti;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getBranchType() {
        return BranchType;
    }

    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }

    public String getBranchAttr() {
        return BranchAttr;
    }

    public void setBranchAttr(String aBranchAttr) {
        BranchAttr = aBranchAttr;
    }

    public String getDepartState() {
        return DepartState;
    }

    public void setDepartState(String aDepartState) {
        DepartState = aDepartState;
    }

    public String getReturnFlag() {
        return ReturnFlag;
    }

    public void setReturnFlag(String aReturnFlag) {
        ReturnFlag = aReturnFlag;
    }

    public String getDestoryFlag() {
        return DestoryFlag;
    }

    public void setDestoryFlag(String aDestoryFlag) {
        DestoryFlag = aDestoryFlag;
    }

    public String getArriveFlag() {
        return ArriveFlag;
    }

    public void setArriveFlag(String aArriveFlag) {
        ArriveFlag = aArriveFlag;
    }

    public String getApplyDate() {
        if (ApplyDate != null) {
            return fDate.getString(ApplyDate);
        } else {
            return null;
        }
    }

    public void setApplyDate(Date aApplyDate) {
        ApplyDate = aApplyDate;
    }

    public void setApplyDate(String aApplyDate) {
        if (aApplyDate != null && !aApplyDate.equals("")) {
            ApplyDate = fDate.getDate(aApplyDate);
        } else {
            ApplyDate = null;
        }
    }

    public String getBranchType2() {
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }

    public String getOfficeFlag() {
        return OfficeFlag;
    }

    public void setOfficeFlag(String aOfficeFlag) {
        OfficeFlag = aOfficeFlag;
    }

    public String getHandOverFlag() {
        return HandOverFlag;
    }

    public void setHandOverFlag(String aHandOverFlag) {
        HandOverFlag = aHandOverFlag;
    }

    public String getActiveFlag() {
        return ActiveFlag;
    }

    public void setActiveFlag(String aActiveFlag) {
        ActiveFlag = aActiveFlag;
    }

    public String getClientFlag() {
        return ClientFlag;
    }

    public void setClientFlag(String aClientFlag) {
        ClientFlag = aClientFlag;
    }

    public String getUserInvalidFlag() {
        return UserInvalidFlag;
    }

    public void setUserInvalidFlag(String aUserInvalidFlag) {
        UserInvalidFlag = aUserInvalidFlag;
    }

    public String getReturnBookFlag() {
        return ReturnBookFlag;
    }

    public void setReturnBookFlag(String aReturnBookFlag) {
        ReturnBookFlag = aReturnBookFlag;
    }

    public String getInformFlag() {
        return InformFlag;
    }

    public void setInformFlag(String aInformFlag) {
        InformFlag = aInformFlag;
    }

    public String getBlackFlag() {
        return BlackFlag;
    }

    public void setBlackFlag(String aBlackFlag) {
        BlackFlag = aBlackFlag;
    }

    public String getBlackListRsn() {
        return BlackListRsn;
    }

    public void setBlackListRsn(String aBlackListRsn) {
        BlackListRsn = aBlackListRsn;
    }

    public String getOrphanDate() {
        if (OrphanDate != null) {
            return fDate.getString(OrphanDate);
        } else {
            return null;
        }
    }

    public void setOrphanDate(Date aOrphanDate) {
        OrphanDate = aOrphanDate;
    }

    public void setOrphanDate(String aOrphanDate) {
        if (aOrphanDate != null && !aOrphanDate.equals("")) {
            OrphanDate = fDate.getDate(aOrphanDate);
        } else {
            OrphanDate = null;
        }
    }

    public int getOrphanSum() {
        return OrphanSum;
    }

    public void setOrphanSum(int aOrphanSum) {
        OrphanSum = aOrphanSum;
    }

    public void setOrphanSum(String aOrphanSum) {
        if (aOrphanSum != null && !aOrphanSum.equals("")) {
            Integer tInteger = new Integer(aOrphanSum);
            int i = tInteger.intValue();
            OrphanSum = i;
        }
    }

    public String getAnnuityFlag() {
        return AnnuityFlag;
    }

    public void setAnnuityFlag(String aAnnuityFlag) {
        AnnuityFlag = aAnnuityFlag;
    }

    public String getVisitFlag() {
        return VisitFlag;
    }

    public void setVisitFlag(String aVisitFlag) {
        VisitFlag = aVisitFlag;
    }

    public String getQualityDestFlag() {
        return QualityDestFlag;
    }

    public void setQualityDestFlag(String aQualityDestFlag) {
        QualityDestFlag = aQualityDestFlag;
    }

    /**
     * 使用另外一个 LADimissionSchema 对象给 Schema 赋值
     * @param: aLADimissionSchema LADimissionSchema
     **/
    public void setSchema(LADimissionSchema aLADimissionSchema) {
        this.AgentCode = aLADimissionSchema.getAgentCode();
        this.DepartTimes = aLADimissionSchema.getDepartTimes();
        this.DepartDate = fDate.getDate(aLADimissionSchema.getDepartDate());
        this.DepartRsn = aLADimissionSchema.getDepartRsn();
        this.ContractFlag = aLADimissionSchema.getContractFlag();
        this.WorkFlag = aLADimissionSchema.getWorkFlag();
        this.PbcFlag = aLADimissionSchema.getPbcFlag();
        this.ReceiptFlag = aLADimissionSchema.getReceiptFlag();
        this.LostFlag = aLADimissionSchema.getLostFlag();
        this.CheckFlag = aLADimissionSchema.getCheckFlag();
        this.WageFlag = aLADimissionSchema.getWageFlag();
        this.TurnFlag = aLADimissionSchema.getTurnFlag();
        this.Noti = aLADimissionSchema.getNoti();
        this.Operator = aLADimissionSchema.getOperator();
        this.MakeDate = fDate.getDate(aLADimissionSchema.getMakeDate());
        this.MakeTime = aLADimissionSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLADimissionSchema.getModifyDate());
        this.ModifyTime = aLADimissionSchema.getModifyTime();
        this.BranchType = aLADimissionSchema.getBranchType();
        this.BranchAttr = aLADimissionSchema.getBranchAttr();
        this.DepartState = aLADimissionSchema.getDepartState();
        this.ReturnFlag = aLADimissionSchema.getReturnFlag();
        this.DestoryFlag = aLADimissionSchema.getDestoryFlag();
        this.ArriveFlag = aLADimissionSchema.getArriveFlag();
        this.ApplyDate = fDate.getDate(aLADimissionSchema.getApplyDate());
        this.BranchType2 = aLADimissionSchema.getBranchType2();
        this.OfficeFlag = aLADimissionSchema.getOfficeFlag();
        this.HandOverFlag = aLADimissionSchema.getHandOverFlag();
        this.ActiveFlag = aLADimissionSchema.getActiveFlag();
        this.ClientFlag = aLADimissionSchema.getClientFlag();
        this.UserInvalidFlag = aLADimissionSchema.getUserInvalidFlag();
        this.ReturnBookFlag = aLADimissionSchema.getReturnBookFlag();
        this.InformFlag = aLADimissionSchema.getInformFlag();
        this.BlackFlag = aLADimissionSchema.getBlackFlag();
        this.BlackListRsn = aLADimissionSchema.getBlackListRsn();
        this.OrphanDate = fDate.getDate(aLADimissionSchema.getOrphanDate());
        this.OrphanSum = aLADimissionSchema.getOrphanSum();
        this.AnnuityFlag = aLADimissionSchema.getAnnuityFlag();
        this.VisitFlag = aLADimissionSchema.getVisitFlag();
        this.QualityDestFlag = aLADimissionSchema.getQualityDestFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            this.DepartTimes = rs.getInt("DepartTimes");
            this.DepartDate = rs.getDate("DepartDate");
            if (rs.getString("DepartRsn") == null) {
                this.DepartRsn = null;
            } else {
                this.DepartRsn = rs.getString("DepartRsn").trim();
            }

            if (rs.getString("ContractFlag") == null) {
                this.ContractFlag = null;
            } else {
                this.ContractFlag = rs.getString("ContractFlag").trim();
            }

            if (rs.getString("WorkFlag") == null) {
                this.WorkFlag = null;
            } else {
                this.WorkFlag = rs.getString("WorkFlag").trim();
            }

            if (rs.getString("PbcFlag") == null) {
                this.PbcFlag = null;
            } else {
                this.PbcFlag = rs.getString("PbcFlag").trim();
            }

            if (rs.getString("ReceiptFlag") == null) {
                this.ReceiptFlag = null;
            } else {
                this.ReceiptFlag = rs.getString("ReceiptFlag").trim();
            }

            if (rs.getString("LostFlag") == null) {
                this.LostFlag = null;
            } else {
                this.LostFlag = rs.getString("LostFlag").trim();
            }

            if (rs.getString("CheckFlag") == null) {
                this.CheckFlag = null;
            } else {
                this.CheckFlag = rs.getString("CheckFlag").trim();
            }

            if (rs.getString("WageFlag") == null) {
                this.WageFlag = null;
            } else {
                this.WageFlag = rs.getString("WageFlag").trim();
            }

            if (rs.getString("TurnFlag") == null) {
                this.TurnFlag = null;
            } else {
                this.TurnFlag = rs.getString("TurnFlag").trim();
            }

            if (rs.getString("Noti") == null) {
                this.Noti = null;
            } else {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BranchType") == null) {
                this.BranchType = null;
            } else {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("BranchAttr") == null) {
                this.BranchAttr = null;
            } else {
                this.BranchAttr = rs.getString("BranchAttr").trim();
            }

            if (rs.getString("DepartState") == null) {
                this.DepartState = null;
            } else {
                this.DepartState = rs.getString("DepartState").trim();
            }

            if (rs.getString("ReturnFlag") == null) {
                this.ReturnFlag = null;
            } else {
                this.ReturnFlag = rs.getString("ReturnFlag").trim();
            }

            if (rs.getString("DestoryFlag") == null) {
                this.DestoryFlag = null;
            } else {
                this.DestoryFlag = rs.getString("DestoryFlag").trim();
            }

            if (rs.getString("ArriveFlag") == null) {
                this.ArriveFlag = null;
            } else {
                this.ArriveFlag = rs.getString("ArriveFlag").trim();
            }

            this.ApplyDate = rs.getDate("ApplyDate");
            if (rs.getString("BranchType2") == null) {
                this.BranchType2 = null;
            } else {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            if (rs.getString("OfficeFlag") == null) {
                this.OfficeFlag = null;
            } else {
                this.OfficeFlag = rs.getString("OfficeFlag").trim();
            }

            if (rs.getString("HandOverFlag") == null) {
                this.HandOverFlag = null;
            } else {
                this.HandOverFlag = rs.getString("HandOverFlag").trim();
            }

            if (rs.getString("ActiveFlag") == null) {
                this.ActiveFlag = null;
            } else {
                this.ActiveFlag = rs.getString("ActiveFlag").trim();
            }

            if (rs.getString("ClientFlag") == null) {
                this.ClientFlag = null;
            } else {
                this.ClientFlag = rs.getString("ClientFlag").trim();
            }

            if (rs.getString("UserInvalidFlag") == null) {
                this.UserInvalidFlag = null;
            } else {
                this.UserInvalidFlag = rs.getString("UserInvalidFlag").trim();
            }

            if (rs.getString("ReturnBookFlag") == null) {
                this.ReturnBookFlag = null;
            } else {
                this.ReturnBookFlag = rs.getString("ReturnBookFlag").trim();
            }

            if (rs.getString("InformFlag") == null) {
                this.InformFlag = null;
            } else {
                this.InformFlag = rs.getString("InformFlag").trim();
            }

            if (rs.getString("BlackFlag") == null) {
                this.BlackFlag = null;
            } else {
                this.BlackFlag = rs.getString("BlackFlag").trim();
            }

            if (rs.getString("BlackListRsn") == null) {
                this.BlackListRsn = null;
            } else {
                this.BlackListRsn = rs.getString("BlackListRsn").trim();
            }

            this.OrphanDate = rs.getDate("OrphanDate");
            this.OrphanSum = rs.getInt("OrphanSum");
            if (rs.getString("AnnuityFlag") == null) {
                this.AnnuityFlag = null;
            } else {
                this.AnnuityFlag = rs.getString("AnnuityFlag").trim();
            }

            if (rs.getString("VisitFlag") == null) {
                this.VisitFlag = null;
            } else {
                this.VisitFlag = rs.getString("VisitFlag").trim();
            }

            if (rs.getString("QualityDestFlag") == null) {
                this.QualityDestFlag = null;
            } else {
                this.QualityDestFlag = rs.getString("QualityDestFlag").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LADimission表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADimissionSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LADimissionSchema getSchema() {
        LADimissionSchema aLADimissionSchema = new LADimissionSchema();
        aLADimissionSchema.setSchema(this);
        return aLADimissionSchema;
    }

    public LADimissionDB getDB() {
        LADimissionDB aDBOper = new LADimissionDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLADimission描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DepartTimes));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(DepartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DepartRsn));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContractFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WorkFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PbcFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiptFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LostFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CheckFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WageFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TurnFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Noti));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchAttr));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DepartState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReturnFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DestoryFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ArriveFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ApplyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OfficeFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HandOverFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ActiveFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClientFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UserInvalidFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReturnBookFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InformFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlackFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlackListRsn));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(OrphanDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OrphanSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AnnuityFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VisitFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QualityDestFlag));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLADimission>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            DepartTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            DepartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            DepartRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            ContractFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            WorkFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            PbcFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            ReceiptFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            LostFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            WageFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            TurnFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                  SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
            DepartState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                         SysConst.PACKAGESPILTER);
            ReturnFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
            DestoryFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                         SysConst.PACKAGESPILTER);
            ArriveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            ApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                         SysConst.PACKAGESPILTER);
            OfficeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                        SysConst.PACKAGESPILTER);
            HandOverFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                          SysConst.PACKAGESPILTER);
            ActiveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                        SysConst.PACKAGESPILTER);
            ClientFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                        SysConst.PACKAGESPILTER);
            UserInvalidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             31, SysConst.PACKAGESPILTER);
            ReturnBookFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            32, SysConst.PACKAGESPILTER);
            InformFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,
                                        SysConst.PACKAGESPILTER);
            BlackFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                       SysConst.PACKAGESPILTER);
            BlackListRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,
                                          SysConst.PACKAGESPILTER);
            OrphanDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 36, SysConst.PACKAGESPILTER));
            OrphanSum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 37, SysConst.PACKAGESPILTER))).intValue();
            AnnuityFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,
                                         SysConst.PACKAGESPILTER);
            VisitFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,
                                       SysConst.PACKAGESPILTER);
            QualityDestFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             40, SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADimissionSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("DepartTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DepartTimes));
        }
        if (FCode.equals("DepartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getDepartDate()));
        }
        if (FCode.equals("DepartRsn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DepartRsn));
        }
        if (FCode.equals("ContractFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContractFlag));
        }
        if (FCode.equals("WorkFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkFlag));
        }
        if (FCode.equals("PbcFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PbcFlag));
        }
        if (FCode.equals("ReceiptFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptFlag));
        }
        if (FCode.equals("LostFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LostFlag));
        }
        if (FCode.equals("CheckFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
        }
        if (FCode.equals("WageFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageFlag));
        }
        if (FCode.equals("TurnFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TurnFlag));
        }
        if (FCode.equals("Noti")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("BranchAttr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equals("DepartState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DepartState));
        }
        if (FCode.equals("ReturnFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnFlag));
        }
        if (FCode.equals("DestoryFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestoryFlag));
        }
        if (FCode.equals("ArriveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArriveFlag));
        }
        if (FCode.equals("ApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getApplyDate()));
        }
        if (FCode.equals("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("OfficeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OfficeFlag));
        }
        if (FCode.equals("HandOverFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HandOverFlag));
        }
        if (FCode.equals("ActiveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActiveFlag));
        }
        if (FCode.equals("ClientFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClientFlag));
        }
        if (FCode.equals("UserInvalidFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UserInvalidFlag));
        }
        if (FCode.equals("ReturnBookFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnBookFlag));
        }
        if (FCode.equals("InformFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InformFlag));
        }
        if (FCode.equals("BlackFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackFlag));
        }
        if (FCode.equals("BlackListRsn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListRsn));
        }
        if (FCode.equals("OrphanDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getOrphanDate()));
        }
        if (FCode.equals("OrphanSum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrphanSum));
        }
        if (FCode.equals("AnnuityFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AnnuityFlag));
        }
        if (FCode.equals("VisitFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VisitFlag));
        }
        if (FCode.equals("QualityDestFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualityDestFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 1:
            strFieldValue = String.valueOf(DepartTimes);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getDepartDate()));
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(DepartRsn);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ContractFlag);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(WorkFlag);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(PbcFlag);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ReceiptFlag);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(LostFlag);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(CheckFlag);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(WageFlag);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(TurnFlag);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(Noti);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(BranchType);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(BranchAttr);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(DepartState);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(ReturnFlag);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(DestoryFlag);
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(ArriveFlag);
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getApplyDate()));
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(BranchType2);
            break;
        case 26:
            strFieldValue = StrTool.GBKToUnicode(OfficeFlag);
            break;
        case 27:
            strFieldValue = StrTool.GBKToUnicode(HandOverFlag);
            break;
        case 28:
            strFieldValue = StrTool.GBKToUnicode(ActiveFlag);
            break;
        case 29:
            strFieldValue = StrTool.GBKToUnicode(ClientFlag);
            break;
        case 30:
            strFieldValue = StrTool.GBKToUnicode(UserInvalidFlag);
            break;
        case 31:
            strFieldValue = StrTool.GBKToUnicode(ReturnBookFlag);
            break;
        case 32:
            strFieldValue = StrTool.GBKToUnicode(InformFlag);
            break;
        case 33:
            strFieldValue = StrTool.GBKToUnicode(BlackFlag);
            break;
        case 34:
            strFieldValue = StrTool.GBKToUnicode(BlackListRsn);
            break;
        case 35:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getOrphanDate()));
            break;
        case 36:
            strFieldValue = String.valueOf(OrphanSum);
            break;
        case 37:
            strFieldValue = StrTool.GBKToUnicode(AnnuityFlag);
            break;
        case 38:
            strFieldValue = StrTool.GBKToUnicode(VisitFlag);
            break;
        case 39:
            strFieldValue = StrTool.GBKToUnicode(QualityDestFlag);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("DepartTimes")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DepartTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("DepartDate")) {
            if (FValue != null && !FValue.equals("")) {
                DepartDate = fDate.getDate(FValue);
            } else {
                DepartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("DepartRsn")) {
            if (FValue != null && !FValue.equals("")) {
                DepartRsn = FValue.trim();
            } else {
                DepartRsn = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContractFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ContractFlag = FValue.trim();
            } else {
                ContractFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("WorkFlag")) {
            if (FValue != null && !FValue.equals("")) {
                WorkFlag = FValue.trim();
            } else {
                WorkFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("PbcFlag")) {
            if (FValue != null && !FValue.equals("")) {
                PbcFlag = FValue.trim();
            } else {
                PbcFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiptFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiptFlag = FValue.trim();
            } else {
                ReceiptFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("LostFlag")) {
            if (FValue != null && !FValue.equals("")) {
                LostFlag = FValue.trim();
            } else {
                LostFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("CheckFlag")) {
            if (FValue != null && !FValue.equals("")) {
                CheckFlag = FValue.trim();
            } else {
                CheckFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("WageFlag")) {
            if (FValue != null && !FValue.equals("")) {
                WageFlag = FValue.trim();
            } else {
                WageFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("TurnFlag")) {
            if (FValue != null && !FValue.equals("")) {
                TurnFlag = FValue.trim();
            } else {
                TurnFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("Noti")) {
            if (FValue != null && !FValue.equals("")) {
                Noti = FValue.trim();
            } else {
                Noti = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if (FValue != null && !FValue.equals("")) {
                BranchType = FValue.trim();
            } else {
                BranchType = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            if (FValue != null && !FValue.equals("")) {
                BranchAttr = FValue.trim();
            } else {
                BranchAttr = null;
            }
        }
        if (FCode.equalsIgnoreCase("DepartState")) {
            if (FValue != null && !FValue.equals("")) {
                DepartState = FValue.trim();
            } else {
                DepartState = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReturnFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ReturnFlag = FValue.trim();
            } else {
                ReturnFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("DestoryFlag")) {
            if (FValue != null && !FValue.equals("")) {
                DestoryFlag = FValue.trim();
            } else {
                DestoryFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ArriveFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ArriveFlag = FValue.trim();
            } else {
                ArriveFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ApplyDate = fDate.getDate(FValue);
            } else {
                ApplyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if (FValue != null && !FValue.equals("")) {
                BranchType2 = FValue.trim();
            } else {
                BranchType2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("OfficeFlag")) {
            if (FValue != null && !FValue.equals("")) {
                OfficeFlag = FValue.trim();
            } else {
                OfficeFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("HandOverFlag")) {
            if (FValue != null && !FValue.equals("")) {
                HandOverFlag = FValue.trim();
            } else {
                HandOverFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ActiveFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ActiveFlag = FValue.trim();
            } else {
                ActiveFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ClientFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ClientFlag = FValue.trim();
            } else {
                ClientFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("UserInvalidFlag")) {
            if (FValue != null && !FValue.equals("")) {
                UserInvalidFlag = FValue.trim();
            } else {
                UserInvalidFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReturnBookFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ReturnBookFlag = FValue.trim();
            } else {
                ReturnBookFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("InformFlag")) {
            if (FValue != null && !FValue.equals("")) {
                InformFlag = FValue.trim();
            } else {
                InformFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("BlackFlag")) {
            if (FValue != null && !FValue.equals("")) {
                BlackFlag = FValue.trim();
            } else {
                BlackFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("BlackListRsn")) {
            if (FValue != null && !FValue.equals("")) {
                BlackListRsn = FValue.trim();
            } else {
                BlackListRsn = null;
            }
        }
        if (FCode.equalsIgnoreCase("OrphanDate")) {
            if (FValue != null && !FValue.equals("")) {
                OrphanDate = fDate.getDate(FValue);
            } else {
                OrphanDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("OrphanSum")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                OrphanSum = i;
            }
        }
        if (FCode.equalsIgnoreCase("AnnuityFlag")) {
            if (FValue != null && !FValue.equals("")) {
                AnnuityFlag = FValue.trim();
            } else {
                AnnuityFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("VisitFlag")) {
            if (FValue != null && !FValue.equals("")) {
                VisitFlag = FValue.trim();
            } else {
                VisitFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("QualityDestFlag")) {
            if (FValue != null && !FValue.equals("")) {
                QualityDestFlag = FValue.trim();
            } else {
                QualityDestFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LADimissionSchema other = (LADimissionSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && DepartTimes == other.getDepartTimes()
                && fDate.getString(DepartDate).equals(other.getDepartDate())
                && DepartRsn.equals(other.getDepartRsn())
                && ContractFlag.equals(other.getContractFlag())
                && WorkFlag.equals(other.getWorkFlag())
                && PbcFlag.equals(other.getPbcFlag())
                && ReceiptFlag.equals(other.getReceiptFlag())
                && LostFlag.equals(other.getLostFlag())
                && CheckFlag.equals(other.getCheckFlag())
                && WageFlag.equals(other.getWageFlag())
                && TurnFlag.equals(other.getTurnFlag())
                && Noti.equals(other.getNoti())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BranchType.equals(other.getBranchType())
                && BranchAttr.equals(other.getBranchAttr())
                && DepartState.equals(other.getDepartState())
                && ReturnFlag.equals(other.getReturnFlag())
                && DestoryFlag.equals(other.getDestoryFlag())
                && ArriveFlag.equals(other.getArriveFlag())
                && fDate.getString(ApplyDate).equals(other.getApplyDate())
                && BranchType2.equals(other.getBranchType2())
                && OfficeFlag.equals(other.getOfficeFlag())
                && HandOverFlag.equals(other.getHandOverFlag())
                && ActiveFlag.equals(other.getActiveFlag())
                && ClientFlag.equals(other.getClientFlag())
                && UserInvalidFlag.equals(other.getUserInvalidFlag())
                && ReturnBookFlag.equals(other.getReturnBookFlag())
                && InformFlag.equals(other.getInformFlag())
                && BlackFlag.equals(other.getBlackFlag())
                && BlackListRsn.equals(other.getBlackListRsn())
                && fDate.getString(OrphanDate).equals(other.getOrphanDate())
                && OrphanSum == other.getOrphanSum()
                && AnnuityFlag.equals(other.getAnnuityFlag())
                && VisitFlag.equals(other.getVisitFlag())
                && QualityDestFlag.equals(other.getQualityDestFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("AgentCode")) {
            return 0;
        }
        if (strFieldName.equals("DepartTimes")) {
            return 1;
        }
        if (strFieldName.equals("DepartDate")) {
            return 2;
        }
        if (strFieldName.equals("DepartRsn")) {
            return 3;
        }
        if (strFieldName.equals("ContractFlag")) {
            return 4;
        }
        if (strFieldName.equals("WorkFlag")) {
            return 5;
        }
        if (strFieldName.equals("PbcFlag")) {
            return 6;
        }
        if (strFieldName.equals("ReceiptFlag")) {
            return 7;
        }
        if (strFieldName.equals("LostFlag")) {
            return 8;
        }
        if (strFieldName.equals("CheckFlag")) {
            return 9;
        }
        if (strFieldName.equals("WageFlag")) {
            return 10;
        }
        if (strFieldName.equals("TurnFlag")) {
            return 11;
        }
        if (strFieldName.equals("Noti")) {
            return 12;
        }
        if (strFieldName.equals("Operator")) {
            return 13;
        }
        if (strFieldName.equals("MakeDate")) {
            return 14;
        }
        if (strFieldName.equals("MakeTime")) {
            return 15;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 16;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 17;
        }
        if (strFieldName.equals("BranchType")) {
            return 18;
        }
        if (strFieldName.equals("BranchAttr")) {
            return 19;
        }
        if (strFieldName.equals("DepartState")) {
            return 20;
        }
        if (strFieldName.equals("ReturnFlag")) {
            return 21;
        }
        if (strFieldName.equals("DestoryFlag")) {
            return 22;
        }
        if (strFieldName.equals("ArriveFlag")) {
            return 23;
        }
        if (strFieldName.equals("ApplyDate")) {
            return 24;
        }
        if (strFieldName.equals("BranchType2")) {
            return 25;
        }
        if (strFieldName.equals("OfficeFlag")) {
            return 26;
        }
        if (strFieldName.equals("HandOverFlag")) {
            return 27;
        }
        if (strFieldName.equals("ActiveFlag")) {
            return 28;
        }
        if (strFieldName.equals("ClientFlag")) {
            return 29;
        }
        if (strFieldName.equals("UserInvalidFlag")) {
            return 30;
        }
        if (strFieldName.equals("ReturnBookFlag")) {
            return 31;
        }
        if (strFieldName.equals("InformFlag")) {
            return 32;
        }
        if (strFieldName.equals("BlackFlag")) {
            return 33;
        }
        if (strFieldName.equals("BlackListRsn")) {
            return 34;
        }
        if (strFieldName.equals("OrphanDate")) {
            return 35;
        }
        if (strFieldName.equals("OrphanSum")) {
            return 36;
        }
        if (strFieldName.equals("AnnuityFlag")) {
            return 37;
        }
        if (strFieldName.equals("VisitFlag")) {
            return 38;
        }
        if (strFieldName.equals("QualityDestFlag")) {
            return 39;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "AgentCode";
            break;
        case 1:
            strFieldName = "DepartTimes";
            break;
        case 2:
            strFieldName = "DepartDate";
            break;
        case 3:
            strFieldName = "DepartRsn";
            break;
        case 4:
            strFieldName = "ContractFlag";
            break;
        case 5:
            strFieldName = "WorkFlag";
            break;
        case 6:
            strFieldName = "PbcFlag";
            break;
        case 7:
            strFieldName = "ReceiptFlag";
            break;
        case 8:
            strFieldName = "LostFlag";
            break;
        case 9:
            strFieldName = "CheckFlag";
            break;
        case 10:
            strFieldName = "WageFlag";
            break;
        case 11:
            strFieldName = "TurnFlag";
            break;
        case 12:
            strFieldName = "Noti";
            break;
        case 13:
            strFieldName = "Operator";
            break;
        case 14:
            strFieldName = "MakeDate";
            break;
        case 15:
            strFieldName = "MakeTime";
            break;
        case 16:
            strFieldName = "ModifyDate";
            break;
        case 17:
            strFieldName = "ModifyTime";
            break;
        case 18:
            strFieldName = "BranchType";
            break;
        case 19:
            strFieldName = "BranchAttr";
            break;
        case 20:
            strFieldName = "DepartState";
            break;
        case 21:
            strFieldName = "ReturnFlag";
            break;
        case 22:
            strFieldName = "DestoryFlag";
            break;
        case 23:
            strFieldName = "ArriveFlag";
            break;
        case 24:
            strFieldName = "ApplyDate";
            break;
        case 25:
            strFieldName = "BranchType2";
            break;
        case 26:
            strFieldName = "OfficeFlag";
            break;
        case 27:
            strFieldName = "HandOverFlag";
            break;
        case 28:
            strFieldName = "ActiveFlag";
            break;
        case 29:
            strFieldName = "ClientFlag";
            break;
        case 30:
            strFieldName = "UserInvalidFlag";
            break;
        case 31:
            strFieldName = "ReturnBookFlag";
            break;
        case 32:
            strFieldName = "InformFlag";
            break;
        case 33:
            strFieldName = "BlackFlag";
            break;
        case 34:
            strFieldName = "BlackListRsn";
            break;
        case 35:
            strFieldName = "OrphanDate";
            break;
        case 36:
            strFieldName = "OrphanSum";
            break;
        case 37:
            strFieldName = "AnnuityFlag";
            break;
        case 38:
            strFieldName = "VisitFlag";
            break;
        case 39:
            strFieldName = "QualityDestFlag";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DepartTimes")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DepartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("DepartRsn")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContractFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WorkFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PbcFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiptFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LostFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CheckFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WageFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TurnFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchAttr")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DepartState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReturnFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestoryFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ArriveFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApplyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BranchType2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OfficeFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HandOverFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActiveFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClientFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UserInvalidFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReturnBookFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InformFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BlackListRsn")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OrphanDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OrphanSum")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AnnuityFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("VisitFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QualityDestFlag")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_INT;
            break;
        case 2:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 24:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 25:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 26:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 27:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 28:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 29:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 30:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 31:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 32:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 33:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 34:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 35:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 36:
            nFieldType = Schema.TYPE_INT;
            break;
        case 37:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 38:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 39:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
