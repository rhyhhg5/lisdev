/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.YBTRiskSwtichDB;

/*
 * <p>ClassName: YBTRiskSwtichSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2018-03-02
 */
public class YBTRiskSwtichSchema implements Schema, Cloneable
{
	// @Field
	/** 主键 */
	private int id;
	/** 银行代码 */
	private String bankcode;
	/** 地区代码 */
	private String comcode;
	/** 险种代码 */
	private String riskcode;
	/** 主险代码 */
	private String mainriskcode;
	/** 数据类别 */
	private String datatype;
	/** 判断类型 */
	private String swtype;
	/** 判断值 */
	private String swvalue;
	/** 状态 */
	private String status;
	/** 渠道类型 */
	private String channel;
	/** 操作员 */
	private String operator;
	/** 操作日期 */
	private String changedate;
	/** 执行日期 */
	private String makedate;
	/** 备用1 */
	private String bak1;
	/** 备用2 */
	private String bak2;
	/** 备用3 */
	private String bak3;
	/** 备用4 */
	private String bak4;
	/** 备用5 */
	private String bak5;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public YBTRiskSwtichSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "id";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		YBTRiskSwtichSchema cloned = (YBTRiskSwtichSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public int getid()
	{
		return id;
	}
	public void setid(int aid)
	{
		id = aid;
	}
	public void setid(String aid)
	{
		if (aid != null && !aid.equals(""))
		{
			Integer tInteger = new Integer(aid);
			int i = tInteger.intValue();
			id = i;
		}
	}

	public String getbankcode()
	{
		return bankcode;
	}
	public void setbankcode(String abankcode)
	{
		bankcode = abankcode;
	}
	public String getcomcode()
	{
		return comcode;
	}
	public void setcomcode(String acomcode)
	{
		comcode = acomcode;
	}
	public String getriskcode()
	{
		return riskcode;
	}
	public void setriskcode(String ariskcode)
	{
		riskcode = ariskcode;
	}
	public String getmainriskcode()
	{
		return mainriskcode;
	}
	public void setmainriskcode(String amainriskcode)
	{
		mainriskcode = amainriskcode;
	}
	public String getdatatype()
	{
		return datatype;
	}
	public void setdatatype(String adatatype)
	{
		datatype = adatatype;
	}
	public String getswtype()
	{
		return swtype;
	}
	public void setswtype(String aswtype)
	{
		swtype = aswtype;
	}
	public String getswvalue()
	{
		return swvalue;
	}
	public void setswvalue(String aswvalue)
	{
		swvalue = aswvalue;
	}
	public String getstatus()
	{
		return status;
	}
	public void setstatus(String astatus)
	{
		status = astatus;
	}
	public String getchannel()
	{
		return channel;
	}
	public void setchannel(String achannel)
	{
		channel = achannel;
	}
	public String getoperator()
	{
		return operator;
	}
	public void setoperator(String aoperator)
	{
		operator = aoperator;
	}
	public String getchangedate()
	{
		return changedate;
	}
	public void setchangedate(String achangedate)
	{
		changedate = achangedate;
	}
	public String getmakedate()
	{
		return makedate;
	}
	public void setmakedate(String amakedate)
	{
		makedate = amakedate;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}
	public String getbak4()
	{
		return bak4;
	}
	public void setbak4(String abak4)
	{
		bak4 = abak4;
	}
	public String getbak5()
	{
		return bak5;
	}
	public void setbak5(String abak5)
	{
		bak5 = abak5;
	}

	/**
	* 使用另外一个 YBTRiskSwtichSchema 对象给 Schema 赋值
	* @param: aYBTRiskSwtichSchema YBTRiskSwtichSchema
	**/
	public void setSchema(YBTRiskSwtichSchema aYBTRiskSwtichSchema)
	{
		this.id = aYBTRiskSwtichSchema.getid();
		this.bankcode = aYBTRiskSwtichSchema.getbankcode();
		this.comcode = aYBTRiskSwtichSchema.getcomcode();
		this.riskcode = aYBTRiskSwtichSchema.getriskcode();
		this.mainriskcode = aYBTRiskSwtichSchema.getmainriskcode();
		this.datatype = aYBTRiskSwtichSchema.getdatatype();
		this.swtype = aYBTRiskSwtichSchema.getswtype();
		this.swvalue = aYBTRiskSwtichSchema.getswvalue();
		this.status = aYBTRiskSwtichSchema.getstatus();
		this.channel = aYBTRiskSwtichSchema.getchannel();
		this.operator = aYBTRiskSwtichSchema.getoperator();
		this.changedate = aYBTRiskSwtichSchema.getchangedate();
		this.makedate = aYBTRiskSwtichSchema.getmakedate();
		this.bak1 = aYBTRiskSwtichSchema.getbak1();
		this.bak2 = aYBTRiskSwtichSchema.getbak2();
		this.bak3 = aYBTRiskSwtichSchema.getbak3();
		this.bak4 = aYBTRiskSwtichSchema.getbak4();
		this.bak5 = aYBTRiskSwtichSchema.getbak5();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.id = rs.getInt("id");
			if( rs.getString("bankcode") == null )
				this.bankcode = null;
			else
				this.bankcode = rs.getString("bankcode").trim();

			if( rs.getString("comcode") == null )
				this.comcode = null;
			else
				this.comcode = rs.getString("comcode").trim();

			if( rs.getString("riskcode") == null )
				this.riskcode = null;
			else
				this.riskcode = rs.getString("riskcode").trim();

			if( rs.getString("mainriskcode") == null )
				this.mainriskcode = null;
			else
				this.mainriskcode = rs.getString("mainriskcode").trim();

			if( rs.getString("datatype") == null )
				this.datatype = null;
			else
				this.datatype = rs.getString("datatype").trim();

			if( rs.getString("swtype") == null )
				this.swtype = null;
			else
				this.swtype = rs.getString("swtype").trim();

			if( rs.getString("swvalue") == null )
				this.swvalue = null;
			else
				this.swvalue = rs.getString("swvalue").trim();

			if( rs.getString("status") == null )
				this.status = null;
			else
				this.status = rs.getString("status").trim();

			if( rs.getString("channel") == null )
				this.channel = null;
			else
				this.channel = rs.getString("channel").trim();

			if( rs.getString("operator") == null )
				this.operator = null;
			else
				this.operator = rs.getString("operator").trim();

			if( rs.getString("changedate") == null )
				this.changedate = null;
			else
				this.changedate = rs.getString("changedate").trim();

			if( rs.getString("makedate") == null )
				this.makedate = null;
			else
				this.makedate = rs.getString("makedate").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

			if( rs.getString("bak4") == null )
				this.bak4 = null;
			else
				this.bak4 = rs.getString("bak4").trim();

			if( rs.getString("bak5") == null )
				this.bak5 = null;
			else
				this.bak5 = rs.getString("bak5").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的YBTRiskSwtich表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBTRiskSwtichSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public YBTRiskSwtichSchema getSchema()
	{
		YBTRiskSwtichSchema aYBTRiskSwtichSchema = new YBTRiskSwtichSchema();
		aYBTRiskSwtichSchema.setSchema(this);
		return aYBTRiskSwtichSchema;
	}

	public YBTRiskSwtichDB getDB()
	{
		YBTRiskSwtichDB aDBOper = new YBTRiskSwtichDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBTRiskSwtich描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append( ChgData.chgData(id));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(riskcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(mainriskcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(datatype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(swtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(swvalue)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(channel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(changedate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makedate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak5));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBTRiskSwtich>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			id= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).intValue();
			bankcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			comcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			riskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			mainriskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			datatype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			swtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			swvalue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			channel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			changedate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			makedate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBTRiskSwtichSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(id));
		}
		if (FCode.equals("bankcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankcode));
		}
		if (FCode.equals("comcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comcode));
		}
		if (FCode.equals("riskcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(riskcode));
		}
		if (FCode.equals("mainriskcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(mainriskcode));
		}
		if (FCode.equals("datatype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(datatype));
		}
		if (FCode.equals("swtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(swtype));
		}
		if (FCode.equals("swvalue"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(swvalue));
		}
		if (FCode.equals("status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(status));
		}
		if (FCode.equals("channel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(channel));
		}
		if (FCode.equals("operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(operator));
		}
		if (FCode.equals("changedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(changedate));
		}
		if (FCode.equals("makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makedate));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (FCode.equals("bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
		}
		if (FCode.equals("bak5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak5));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(id);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(bankcode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(comcode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(riskcode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(mainriskcode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(datatype);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(swtype);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(swvalue);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(status);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(channel);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(operator);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(changedate);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(makedate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(bak4);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(bak5);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				id = i;
			}
		}
		if (FCode.equalsIgnoreCase("bankcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankcode = FValue.trim();
			}
			else
				bankcode = null;
		}
		if (FCode.equalsIgnoreCase("comcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comcode = FValue.trim();
			}
			else
				comcode = null;
		}
		if (FCode.equalsIgnoreCase("riskcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				riskcode = FValue.trim();
			}
			else
				riskcode = null;
		}
		if (FCode.equalsIgnoreCase("mainriskcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				mainriskcode = FValue.trim();
			}
			else
				mainriskcode = null;
		}
		if (FCode.equalsIgnoreCase("datatype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				datatype = FValue.trim();
			}
			else
				datatype = null;
		}
		if (FCode.equalsIgnoreCase("swtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				swtype = FValue.trim();
			}
			else
				swtype = null;
		}
		if (FCode.equalsIgnoreCase("swvalue"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				swvalue = FValue.trim();
			}
			else
				swvalue = null;
		}
		if (FCode.equalsIgnoreCase("status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				status = FValue.trim();
			}
			else
				status = null;
		}
		if (FCode.equalsIgnoreCase("channel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				channel = FValue.trim();
			}
			else
				channel = null;
		}
		if (FCode.equalsIgnoreCase("operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				operator = FValue.trim();
			}
			else
				operator = null;
		}
		if (FCode.equalsIgnoreCase("changedate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				changedate = FValue.trim();
			}
			else
				changedate = null;
		}
		if (FCode.equalsIgnoreCase("makedate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makedate = FValue.trim();
			}
			else
				makedate = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		if (FCode.equalsIgnoreCase("bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak4 = FValue.trim();
			}
			else
				bak4 = null;
		}
		if (FCode.equalsIgnoreCase("bak5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak5 = FValue.trim();
			}
			else
				bak5 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		YBTRiskSwtichSchema other = (YBTRiskSwtichSchema)otherObject;
		return
			id == other.getid()
			&& (bankcode == null ? other.getbankcode() == null : bankcode.equals(other.getbankcode()))
			&& (comcode == null ? other.getcomcode() == null : comcode.equals(other.getcomcode()))
			&& (riskcode == null ? other.getriskcode() == null : riskcode.equals(other.getriskcode()))
			&& (mainriskcode == null ? other.getmainriskcode() == null : mainriskcode.equals(other.getmainriskcode()))
			&& (datatype == null ? other.getdatatype() == null : datatype.equals(other.getdatatype()))
			&& (swtype == null ? other.getswtype() == null : swtype.equals(other.getswtype()))
			&& (swvalue == null ? other.getswvalue() == null : swvalue.equals(other.getswvalue()))
			&& (status == null ? other.getstatus() == null : status.equals(other.getstatus()))
			&& (channel == null ? other.getchannel() == null : channel.equals(other.getchannel()))
			&& (operator == null ? other.getoperator() == null : operator.equals(other.getoperator()))
			&& (changedate == null ? other.getchangedate() == null : changedate.equals(other.getchangedate()))
			&& (makedate == null ? other.getmakedate() == null : makedate.equals(other.getmakedate()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()))
			&& (bak4 == null ? other.getbak4() == null : bak4.equals(other.getbak4()))
			&& (bak5 == null ? other.getbak5() == null : bak5.equals(other.getbak5()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("id") ) {
			return 0;
		}
		if( strFieldName.equals("bankcode") ) {
			return 1;
		}
		if( strFieldName.equals("comcode") ) {
			return 2;
		}
		if( strFieldName.equals("riskcode") ) {
			return 3;
		}
		if( strFieldName.equals("mainriskcode") ) {
			return 4;
		}
		if( strFieldName.equals("datatype") ) {
			return 5;
		}
		if( strFieldName.equals("swtype") ) {
			return 6;
		}
		if( strFieldName.equals("swvalue") ) {
			return 7;
		}
		if( strFieldName.equals("status") ) {
			return 8;
		}
		if( strFieldName.equals("channel") ) {
			return 9;
		}
		if( strFieldName.equals("operator") ) {
			return 10;
		}
		if( strFieldName.equals("changedate") ) {
			return 11;
		}
		if( strFieldName.equals("makedate") ) {
			return 12;
		}
		if( strFieldName.equals("bak1") ) {
			return 13;
		}
		if( strFieldName.equals("bak2") ) {
			return 14;
		}
		if( strFieldName.equals("bak3") ) {
			return 15;
		}
		if( strFieldName.equals("bak4") ) {
			return 16;
		}
		if( strFieldName.equals("bak5") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "id";
				break;
			case 1:
				strFieldName = "bankcode";
				break;
			case 2:
				strFieldName = "comcode";
				break;
			case 3:
				strFieldName = "riskcode";
				break;
			case 4:
				strFieldName = "mainriskcode";
				break;
			case 5:
				strFieldName = "datatype";
				break;
			case 6:
				strFieldName = "swtype";
				break;
			case 7:
				strFieldName = "swvalue";
				break;
			case 8:
				strFieldName = "status";
				break;
			case 9:
				strFieldName = "channel";
				break;
			case 10:
				strFieldName = "operator";
				break;
			case 11:
				strFieldName = "changedate";
				break;
			case 12:
				strFieldName = "makedate";
				break;
			case 13:
				strFieldName = "bak1";
				break;
			case 14:
				strFieldName = "bak2";
				break;
			case 15:
				strFieldName = "bak3";
				break;
			case 16:
				strFieldName = "bak4";
				break;
			case 17:
				strFieldName = "bak5";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("id") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("bankcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("riskcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("mainriskcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("datatype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("swtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("swvalue") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("channel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("changedate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makedate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak5") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_INT;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
