/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.DutyItemDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: DutyItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-20
 */
public class DutyItemSchema implements Schema, Cloneable
{
    // @Field
    /** 责任项目编号 */
    private String DutyItemCode;
    /** 责任项目类型代码 */
    private String DutyTypeCode;
    /** 责任项目名称 */
    private String DutyItemName;
    /** 责任内容描述 */
    private String DutyContDescri;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public DutyItemSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "DutyItemCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        DutyItemSchema cloned = (DutyItemSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getDutyItemCode()
    {
        if (SysConst.CHANGECHARSET && DutyItemCode != null &&
            !DutyItemCode.equals(""))
        {
            DutyItemCode = StrTool.unicodeToGBK(DutyItemCode);
        }
        return DutyItemCode;
    }

    public void setDutyItemCode(String aDutyItemCode)
    {
        DutyItemCode = aDutyItemCode;
    }

    public String getDutyTypeCode()
    {
        if (SysConst.CHANGECHARSET && DutyTypeCode != null &&
            !DutyTypeCode.equals(""))
        {
            DutyTypeCode = StrTool.unicodeToGBK(DutyTypeCode);
        }
        return DutyTypeCode;
    }

    public void setDutyTypeCode(String aDutyTypeCode)
    {
        DutyTypeCode = aDutyTypeCode;
    }

    public String getDutyItemName()
    {
        if (SysConst.CHANGECHARSET && DutyItemName != null &&
            !DutyItemName.equals(""))
        {
            DutyItemName = StrTool.unicodeToGBK(DutyItemName);
        }
        return DutyItemName;
    }

    public void setDutyItemName(String aDutyItemName)
    {
        DutyItemName = aDutyItemName;
    }

    public String getDutyContDescri()
    {
        if (SysConst.CHANGECHARSET && DutyContDescri != null &&
            !DutyContDescri.equals(""))
        {
            DutyContDescri = StrTool.unicodeToGBK(DutyContDescri);
        }
        return DutyContDescri;
    }

    public void setDutyContDescri(String aDutyContDescri)
    {
        DutyContDescri = aDutyContDescri;
    }

    /**
     * 使用另外一个 DutyItemSchema 对象给 Schema 赋值
     * @param: aDutyItemSchema DutyItemSchema
     **/
    public void setSchema(DutyItemSchema aDutyItemSchema)
    {
        this.DutyItemCode = aDutyItemSchema.getDutyItemCode();
        this.DutyTypeCode = aDutyItemSchema.getDutyTypeCode();
        this.DutyItemName = aDutyItemSchema.getDutyItemName();
        this.DutyContDescri = aDutyItemSchema.getDutyContDescri();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString(1) == null)
            {
                this.DutyItemCode = null;
            }
            else
            {
                this.DutyItemCode = rs.getString(1).trim();
            }

            if (rs.getString(2) == null)
            {
                this.DutyTypeCode = null;
            }
            else
            {
                this.DutyTypeCode = rs.getString(2).trim();
            }

            if (rs.getString(3) == null)
            {
                this.DutyItemName = null;
            }
            else
            {
                this.DutyItemName = rs.getString(3).trim();
            }

            if (rs.getString(4) == null)
            {
                this.DutyContDescri = null;
            }
            else
            {
                this.DutyContDescri = rs.getString(4).trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DutyItemSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public DutyItemSchema getSchema()
    {
        DutyItemSchema aDutyItemSchema = new DutyItemSchema();
        aDutyItemSchema.setSchema(this);
        return aDutyItemSchema;
    }

    public DutyItemDB getDB()
    {
        DutyItemDB aDBOper = new DutyItemDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpDutyItem描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DutyItemCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DutyTypeCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DutyItemName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DutyContDescri)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpDutyItem>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            DutyItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            DutyTypeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            DutyItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            DutyContDescri = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DutyItemSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("DutyItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyItemCode));
        }
        if (FCode.equals("DutyTypeCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyTypeCode));
        }
        if (FCode.equals("DutyItemName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyItemName));
        }
        if (FCode.equals("DutyContDescri"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyContDescri));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(DutyItemCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(DutyTypeCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DutyItemName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DutyContDescri);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("DutyItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyItemCode = FValue.trim();
            }
            else
            {
                DutyItemCode = null;
            }
        }
        if (FCode.equals("DutyTypeCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyTypeCode = FValue.trim();
            }
            else
            {
                DutyTypeCode = null;
            }
        }
        if (FCode.equals("DutyItemName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyItemName = FValue.trim();
            }
            else
            {
                DutyItemName = null;
            }
        }
        if (FCode.equals("DutyContDescri"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyContDescri = FValue.trim();
            }
            else
            {
                DutyContDescri = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        DutyItemSchema other = (DutyItemSchema) otherObject;
        return
                DutyItemCode.equals(other.getDutyItemCode())
                && DutyTypeCode.equals(other.getDutyTypeCode())
                && DutyItemName.equals(other.getDutyItemName())
                && DutyContDescri.equals(other.getDutyContDescri());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("DutyItemCode"))
        {
            return 0;
        }
        if (strFieldName.equals("DutyTypeCode"))
        {
            return 1;
        }
        if (strFieldName.equals("DutyItemName"))
        {
            return 2;
        }
        if (strFieldName.equals("DutyContDescri"))
        {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "DutyItemCode";
                break;
            case 1:
                strFieldName = "DutyTypeCode";
                break;
            case 2:
                strFieldName = "DutyItemName";
                break;
            case 3:
                strFieldName = "DutyContDescri";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("DutyItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyTypeCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyItemName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyContDescri"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
