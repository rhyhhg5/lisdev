/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LHServCaseDefDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LHServCaseDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表修改-服务事件设置_20060703
 * @CreateDate：2006-07-03
 */
public class LHServCaseDefSchema implements Schema, Cloneable {
    // @Field
    /** 服务事件类型 */
    private String ServCaseType;
    /** 服务事件号码 */
    private String ServCaseCode;
    /** 服务事件名称 */
    private String ServCaseName;
    /** 服务事件状态 */
    private String ServCaseState;
    /** 事件启动状态 */
    private String CaseLaunchState;
    /** 服务事件描述 */
    private String ServCaseDes;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHServCaseDefSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ServCaseCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHServCaseDefSchema cloned = (LHServCaseDefSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getServCaseType() {
        return ServCaseType;
    }

    public void setServCaseType(String aServCaseType) {
        ServCaseType = aServCaseType;
    }

    public String getServCaseCode() {
        return ServCaseCode;
    }

    public void setServCaseCode(String aServCaseCode) {
        ServCaseCode = aServCaseCode;
    }

    public String getServCaseName() {
        return ServCaseName;
    }

    public void setServCaseName(String aServCaseName) {
        ServCaseName = aServCaseName;
    }

    public String getServCaseState() {
        return ServCaseState;
    }

    public void setServCaseState(String aServCaseState) {
        ServCaseState = aServCaseState;
    }

    public String getCaseLaunchState() {
        return CaseLaunchState;
    }

    public void setCaseLaunchState(String aCaseLaunchState) {
        CaseLaunchState = aCaseLaunchState;
    }

    public String getServCaseDes() {
        return ServCaseDes;
    }

    public void setServCaseDes(String aServCaseDes) {
        ServCaseDes = aServCaseDes;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHServCaseDefSchema 对象给 Schema 赋值
     * @param: aLHServCaseDefSchema LHServCaseDefSchema
     **/
    public void setSchema(LHServCaseDefSchema aLHServCaseDefSchema) {
        this.ServCaseType = aLHServCaseDefSchema.getServCaseType();
        this.ServCaseCode = aLHServCaseDefSchema.getServCaseCode();
        this.ServCaseName = aLHServCaseDefSchema.getServCaseName();
        this.ServCaseState = aLHServCaseDefSchema.getServCaseState();
        this.CaseLaunchState = aLHServCaseDefSchema.getCaseLaunchState();
        this.ServCaseDes = aLHServCaseDefSchema.getServCaseDes();
        this.ManageCom = aLHServCaseDefSchema.getManageCom();
        this.Operator = aLHServCaseDefSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHServCaseDefSchema.getMakeDate());
        this.MakeTime = aLHServCaseDefSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHServCaseDefSchema.getModifyDate());
        this.ModifyTime = aLHServCaseDefSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ServCaseType") == null) {
                this.ServCaseType = null;
            } else {
                this.ServCaseType = rs.getString("ServCaseType").trim();
            }

            if (rs.getString("ServCaseCode") == null) {
                this.ServCaseCode = null;
            } else {
                this.ServCaseCode = rs.getString("ServCaseCode").trim();
            }

            if (rs.getString("ServCaseName") == null) {
                this.ServCaseName = null;
            } else {
                this.ServCaseName = rs.getString("ServCaseName").trim();
            }

            if (rs.getString("ServCaseState") == null) {
                this.ServCaseState = null;
            } else {
                this.ServCaseState = rs.getString("ServCaseState").trim();
            }

            if (rs.getString("CaseLaunchState") == null) {
                this.CaseLaunchState = null;
            } else {
                this.CaseLaunchState = rs.getString("CaseLaunchState").trim();
            }

            if (rs.getString("ServCaseDes") == null) {
                this.ServCaseDes = null;
            } else {
                this.ServCaseDes = rs.getString("ServCaseDes").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHServCaseDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServCaseDefSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHServCaseDefSchema getSchema() {
        LHServCaseDefSchema aLHServCaseDefSchema = new LHServCaseDefSchema();
        aLHServCaseDefSchema.setSchema(this);
        return aLHServCaseDefSchema;
    }

    public LHServCaseDefDB getDB() {
        LHServCaseDefDB aDBOper = new LHServCaseDefDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServCaseDef描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ServCaseType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CaseLaunchState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseDes));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServCaseDef>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ServCaseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ServCaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ServCaseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ServCaseState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            CaseLaunchState = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             5, SysConst.PACKAGESPILTER);
            ServCaseDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServCaseDefSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ServCaseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseType));
        }
        if (FCode.equals("ServCaseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseCode));
        }
        if (FCode.equals("ServCaseName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseName));
        }
        if (FCode.equals("ServCaseState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseState));
        }
        if (FCode.equals("CaseLaunchState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseLaunchState));
        }
        if (FCode.equals("ServCaseDes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseDes));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ServCaseType);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServCaseCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ServCaseName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServCaseState);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(CaseLaunchState);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServCaseDes);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ServCaseType")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseType = FValue.trim();
            } else {
                ServCaseType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseCode = FValue.trim();
            } else {
                ServCaseCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseName")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseName = FValue.trim();
            } else {
                ServCaseName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseState")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseState = FValue.trim();
            } else {
                ServCaseState = null;
            }
        }
        if (FCode.equalsIgnoreCase("CaseLaunchState")) {
            if (FValue != null && !FValue.equals("")) {
                CaseLaunchState = FValue.trim();
            } else {
                CaseLaunchState = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseDes")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseDes = FValue.trim();
            } else {
                ServCaseDes = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHServCaseDefSchema other = (LHServCaseDefSchema) otherObject;
        return
                ServCaseType.equals(other.getServCaseType())
                && ServCaseCode.equals(other.getServCaseCode())
                && ServCaseName.equals(other.getServCaseName())
                && ServCaseState.equals(other.getServCaseState())
                && CaseLaunchState.equals(other.getCaseLaunchState())
                && ServCaseDes.equals(other.getServCaseDes())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ServCaseType")) {
            return 0;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return 1;
        }
        if (strFieldName.equals("ServCaseName")) {
            return 2;
        }
        if (strFieldName.equals("ServCaseState")) {
            return 3;
        }
        if (strFieldName.equals("CaseLaunchState")) {
            return 4;
        }
        if (strFieldName.equals("ServCaseDes")) {
            return 5;
        }
        if (strFieldName.equals("ManageCom")) {
            return 6;
        }
        if (strFieldName.equals("Operator")) {
            return 7;
        }
        if (strFieldName.equals("MakeDate")) {
            return 8;
        }
        if (strFieldName.equals("MakeTime")) {
            return 9;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 10;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ServCaseType";
            break;
        case 1:
            strFieldName = "ServCaseCode";
            break;
        case 2:
            strFieldName = "ServCaseName";
            break;
        case 3:
            strFieldName = "ServCaseState";
            break;
        case 4:
            strFieldName = "CaseLaunchState";
            break;
        case 5:
            strFieldName = "ServCaseDes";
            break;
        case 6:
            strFieldName = "ManageCom";
            break;
        case 7:
            strFieldName = "Operator";
            break;
        case 8:
            strFieldName = "MakeDate";
            break;
        case 9:
            strFieldName = "MakeTime";
            break;
        case 10:
            strFieldName = "ModifyDate";
            break;
        case 11:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ServCaseType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseLaunchState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseDes")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
