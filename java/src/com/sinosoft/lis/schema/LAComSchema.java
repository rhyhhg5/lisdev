/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAComDB;

/*
 * <p>ClassName: LAComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-05-22
 */
public class LAComSchema implements Schema, Cloneable
{
	// @Field
	/** 代理机构 */
	private String AgentCom;
	/** 管理机构 */
	private String ManageCom;
	/** 地区类型 */
	private String AreaType;
	/** 渠道类型 */
	private String ChannelType;
	/** 上级代理机构 */
	private String UpAgentCom;
	/** 机构名称 */
	private String Name;
	/** 机构注册地址 */
	private String Address;
	/** 机构邮编 */
	private String ZipCode;
	/** 机构电话 */
	private String Phone;
	/** 机构传真 */
	private String Fax;
	/** Email */
	private String EMail;
	/** 网址 */
	private String WebAddress;
	/** 负责人 */
	private String LinkMan;
	/** 密码 */
	private String Password;
	/** 法人 */
	private String Corporation;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 行业分类 */
	private String BusinessType;
	/** 单位性质 */
	private String GrpNature;
	/** 中介机构类别 */
	private String ACType;
	/** 销售资格 */
	private String SellFlag;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 银行级别 */
	private String BankType;
	/** 是否统计网点合格率 */
	private String CalFlag;
	/** 工商执照编码 */
	private String BusiLicenseCode;
	/** 保险公司id */
	private String InsureID;
	/** 保险公司负责人 */
	private String InsurePrincipal;
	/** 主营业务 */
	private String ChiefBusiness;
	/** 营业地址 */
	private String BusiAddress;
	/** 签署人 */
	private String SubscribeMan;
	/** 签署人职务 */
	private String SubscribeManDuty;
	/** 许可证号码 */
	private String LicenseNo;
	/** 行政区划代码 */
	private String RegionalismCode;
	/** 上报代码 */
	private String AppAgentCom;
	/** 机构状态 */
	private String State;
	/** 相关说明 */
	private String Noti;
	/** 行业代码 */
	private String BusinessCode;
	/** 许可证登记日期 */
	private Date LicenseStartDate;
	/** 许可证截至日期 */
	private Date LicenseEndDate;
	/** 展业类型 */
	private String BranchType;
	/** 渠道 */
	private String BranchType2;
	/** 资产 */
	private double Assets;
	/** 营业收入 */
	private double Income;
	/** 营业利润 */
	private double Profits;
	/** 机构人数 */
	private int PersonnalSum;
	/** 合同编码 */
	private String ProtocalNo;
	/** 停业标志 */
	private String EndFlag;
	/** 停业日期 */
	private Date EndDate;
	/** 集团审核标志字段 */
	private String Crs_Check_Status;
	/** 账户名 */
	private String BankAccName;
	/** 账户开户行 */
	private String BankAccOpen;
	/** 是否县域网点 */
	private String IsDotFlag;
	/** 中介组织机构代码 */
	private String AgentOrganCode;
	/** 兼业类型 */
	private String PartAcType;
	/** F1 */
	private String F1;

	public static final int FIELDNUM = 59;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAComSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "AgentCom";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAComSchema cloned = (LAComSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getAreaType()
	{
		return AreaType;
	}
	public void setAreaType(String aAreaType)
	{
		AreaType = aAreaType;
	}
	public String getChannelType()
	{
		return ChannelType;
	}
	public void setChannelType(String aChannelType)
	{
		ChannelType = aChannelType;
	}
	public String getUpAgentCom()
	{
		return UpAgentCom;
	}
	public void setUpAgentCom(String aUpAgentCom)
	{
		UpAgentCom = aUpAgentCom;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getAddress()
	{
		return Address;
	}
	public void setAddress(String aAddress)
	{
		Address = aAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getFax()
	{
		return Fax;
	}
	public void setFax(String aFax)
	{
		Fax = aFax;
	}
	public String getEMail()
	{
		return EMail;
	}
	public void setEMail(String aEMail)
	{
		EMail = aEMail;
	}
	public String getWebAddress()
	{
		return WebAddress;
	}
	public void setWebAddress(String aWebAddress)
	{
		WebAddress = aWebAddress;
	}
	public String getLinkMan()
	{
		return LinkMan;
	}
	public void setLinkMan(String aLinkMan)
	{
		LinkMan = aLinkMan;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getCorporation()
	{
		return Corporation;
	}
	public void setCorporation(String aCorporation)
	{
		Corporation = aCorporation;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getBusinessType()
	{
		return BusinessType;
	}
	public void setBusinessType(String aBusinessType)
	{
		BusinessType = aBusinessType;
	}
	public String getGrpNature()
	{
		return GrpNature;
	}
	public void setGrpNature(String aGrpNature)
	{
		GrpNature = aGrpNature;
	}
	public String getACType()
	{
		return ACType;
	}
	public void setACType(String aACType)
	{
		ACType = aACType;
	}
	public String getSellFlag()
	{
		return SellFlag;
	}
	public void setSellFlag(String aSellFlag)
	{
		SellFlag = aSellFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBankType()
	{
		return BankType;
	}
	public void setBankType(String aBankType)
	{
		BankType = aBankType;
	}
	public String getCalFlag()
	{
		return CalFlag;
	}
	public void setCalFlag(String aCalFlag)
	{
		CalFlag = aCalFlag;
	}
	public String getBusiLicenseCode()
	{
		return BusiLicenseCode;
	}
	public void setBusiLicenseCode(String aBusiLicenseCode)
	{
		BusiLicenseCode = aBusiLicenseCode;
	}
	public String getInsureID()
	{
		return InsureID;
	}
	public void setInsureID(String aInsureID)
	{
		InsureID = aInsureID;
	}
	public String getInsurePrincipal()
	{
		return InsurePrincipal;
	}
	public void setInsurePrincipal(String aInsurePrincipal)
	{
		InsurePrincipal = aInsurePrincipal;
	}
	public String getChiefBusiness()
	{
		return ChiefBusiness;
	}
	public void setChiefBusiness(String aChiefBusiness)
	{
		ChiefBusiness = aChiefBusiness;
	}
	public String getBusiAddress()
	{
		return BusiAddress;
	}
	public void setBusiAddress(String aBusiAddress)
	{
		BusiAddress = aBusiAddress;
	}
	public String getSubscribeMan()
	{
		return SubscribeMan;
	}
	public void setSubscribeMan(String aSubscribeMan)
	{
		SubscribeMan = aSubscribeMan;
	}
	public String getSubscribeManDuty()
	{
		return SubscribeManDuty;
	}
	public void setSubscribeManDuty(String aSubscribeManDuty)
	{
		SubscribeManDuty = aSubscribeManDuty;
	}
	public String getLicenseNo()
	{
		return LicenseNo;
	}
	public void setLicenseNo(String aLicenseNo)
	{
		LicenseNo = aLicenseNo;
	}
	public String getRegionalismCode()
	{
		return RegionalismCode;
	}
	public void setRegionalismCode(String aRegionalismCode)
	{
		RegionalismCode = aRegionalismCode;
	}
	public String getAppAgentCom()
	{
		return AppAgentCom;
	}
	public void setAppAgentCom(String aAppAgentCom)
	{
		AppAgentCom = aAppAgentCom;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getNoti()
	{
		return Noti;
	}
	public void setNoti(String aNoti)
	{
		Noti = aNoti;
	}
	public String getBusinessCode()
	{
		return BusinessCode;
	}
	public void setBusinessCode(String aBusinessCode)
	{
		BusinessCode = aBusinessCode;
	}
	public String getLicenseStartDate()
	{
		if( LicenseStartDate != null )
			return fDate.getString(LicenseStartDate);
		else
			return null;
	}
	public void setLicenseStartDate(Date aLicenseStartDate)
	{
		LicenseStartDate = aLicenseStartDate;
	}
	public void setLicenseStartDate(String aLicenseStartDate)
	{
		if (aLicenseStartDate != null && !aLicenseStartDate.equals("") )
		{
			LicenseStartDate = fDate.getDate( aLicenseStartDate );
		}
		else
			LicenseStartDate = null;
	}

	public String getLicenseEndDate()
	{
		if( LicenseEndDate != null )
			return fDate.getString(LicenseEndDate);
		else
			return null;
	}
	public void setLicenseEndDate(Date aLicenseEndDate)
	{
		LicenseEndDate = aLicenseEndDate;
	}
	public void setLicenseEndDate(String aLicenseEndDate)
	{
		if (aLicenseEndDate != null && !aLicenseEndDate.equals("") )
		{
			LicenseEndDate = fDate.getDate( aLicenseEndDate );
		}
		else
			LicenseEndDate = null;
	}

	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public double getAssets()
	{
		return Assets;
	}
	public void setAssets(double aAssets)
	{
		Assets = Arith.round(aAssets,2);
	}
	public void setAssets(String aAssets)
	{
		if (aAssets != null && !aAssets.equals(""))
		{
			Double tDouble = new Double(aAssets);
			double d = tDouble.doubleValue();
                Assets = Arith.round(d,2);
		}
	}

	public double getIncome()
	{
		return Income;
	}
	public void setIncome(double aIncome)
	{
		Income = Arith.round(aIncome,2);
	}
	public void setIncome(String aIncome)
	{
		if (aIncome != null && !aIncome.equals(""))
		{
			Double tDouble = new Double(aIncome);
			double d = tDouble.doubleValue();
                Income = Arith.round(d,2);
		}
	}

	public double getProfits()
	{
		return Profits;
	}
	public void setProfits(double aProfits)
	{
		Profits = Arith.round(aProfits,2);
	}
	public void setProfits(String aProfits)
	{
		if (aProfits != null && !aProfits.equals(""))
		{
			Double tDouble = new Double(aProfits);
			double d = tDouble.doubleValue();
                Profits = Arith.round(d,2);
		}
	}

	public int getPersonnalSum()
	{
		return PersonnalSum;
	}
	public void setPersonnalSum(int aPersonnalSum)
	{
		PersonnalSum = aPersonnalSum;
	}
	public void setPersonnalSum(String aPersonnalSum)
	{
		if (aPersonnalSum != null && !aPersonnalSum.equals(""))
		{
			Integer tInteger = new Integer(aPersonnalSum);
			int i = tInteger.intValue();
			PersonnalSum = i;
		}
	}

	public String getProtocalNo()
	{
		return ProtocalNo;
	}
	public void setProtocalNo(String aProtocalNo)
	{
		ProtocalNo = aProtocalNo;
	}
	public String getEndFlag()
	{
		return EndFlag;
	}
	public void setEndFlag(String aEndFlag)
	{
		EndFlag = aEndFlag;
	}
	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
		EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public String getCrs_Check_Status()
	{
		return Crs_Check_Status;
	}
	public void setCrs_Check_Status(String aCrs_Check_Status)
	{
		Crs_Check_Status = aCrs_Check_Status;
	}
	public String getBankAccName()
	{
		return BankAccName;
	}
	public void setBankAccName(String aBankAccName)
	{
		BankAccName = aBankAccName;
	}
	public String getBankAccOpen()
	{
		return BankAccOpen;
	}
	public void setBankAccOpen(String aBankAccOpen)
	{
		BankAccOpen = aBankAccOpen;
	}
	public String getIsDotFlag()
	{
		return IsDotFlag;
	}
	public void setIsDotFlag(String aIsDotFlag)
	{
		IsDotFlag = aIsDotFlag;
	}
	public String getAgentOrganCode()
	{
		return AgentOrganCode;
	}
	public void setAgentOrganCode(String aAgentOrganCode)
	{
		AgentOrganCode = aAgentOrganCode;
	}
	public String getPartAcType()
	{
		return PartAcType;
	}
	public void setPartAcType(String aPartAcType)
	{
		PartAcType = aPartAcType;
	}
	public String getF1()
	{
		return F1;
	}
	public void setF1(String aF1)
	{
		F1 = aF1;
	}

	/**
	* 使用另外一个 LAComSchema 对象给 Schema 赋值
	* @param: aLAComSchema LAComSchema
	**/
	public void setSchema(LAComSchema aLAComSchema)
	{
		this.AgentCom = aLAComSchema.getAgentCom();
		this.ManageCom = aLAComSchema.getManageCom();
		this.AreaType = aLAComSchema.getAreaType();
		this.ChannelType = aLAComSchema.getChannelType();
		this.UpAgentCom = aLAComSchema.getUpAgentCom();
		this.Name = aLAComSchema.getName();
		this.Address = aLAComSchema.getAddress();
		this.ZipCode = aLAComSchema.getZipCode();
		this.Phone = aLAComSchema.getPhone();
		this.Fax = aLAComSchema.getFax();
		this.EMail = aLAComSchema.getEMail();
		this.WebAddress = aLAComSchema.getWebAddress();
		this.LinkMan = aLAComSchema.getLinkMan();
		this.Password = aLAComSchema.getPassword();
		this.Corporation = aLAComSchema.getCorporation();
		this.BankCode = aLAComSchema.getBankCode();
		this.BankAccNo = aLAComSchema.getBankAccNo();
		this.BusinessType = aLAComSchema.getBusinessType();
		this.GrpNature = aLAComSchema.getGrpNature();
		this.ACType = aLAComSchema.getACType();
		this.SellFlag = aLAComSchema.getSellFlag();
		this.Operator = aLAComSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAComSchema.getMakeDate());
		this.MakeTime = aLAComSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAComSchema.getModifyDate());
		this.ModifyTime = aLAComSchema.getModifyTime();
		this.BankType = aLAComSchema.getBankType();
		this.CalFlag = aLAComSchema.getCalFlag();
		this.BusiLicenseCode = aLAComSchema.getBusiLicenseCode();
		this.InsureID = aLAComSchema.getInsureID();
		this.InsurePrincipal = aLAComSchema.getInsurePrincipal();
		this.ChiefBusiness = aLAComSchema.getChiefBusiness();
		this.BusiAddress = aLAComSchema.getBusiAddress();
		this.SubscribeMan = aLAComSchema.getSubscribeMan();
		this.SubscribeManDuty = aLAComSchema.getSubscribeManDuty();
		this.LicenseNo = aLAComSchema.getLicenseNo();
		this.RegionalismCode = aLAComSchema.getRegionalismCode();
		this.AppAgentCom = aLAComSchema.getAppAgentCom();
		this.State = aLAComSchema.getState();
		this.Noti = aLAComSchema.getNoti();
		this.BusinessCode = aLAComSchema.getBusinessCode();
		this.LicenseStartDate = fDate.getDate( aLAComSchema.getLicenseStartDate());
		this.LicenseEndDate = fDate.getDate( aLAComSchema.getLicenseEndDate());
		this.BranchType = aLAComSchema.getBranchType();
		this.BranchType2 = aLAComSchema.getBranchType2();
		this.Assets = aLAComSchema.getAssets();
		this.Income = aLAComSchema.getIncome();
		this.Profits = aLAComSchema.getProfits();
		this.PersonnalSum = aLAComSchema.getPersonnalSum();
		this.ProtocalNo = aLAComSchema.getProtocalNo();
		this.EndFlag = aLAComSchema.getEndFlag();
		this.EndDate = fDate.getDate( aLAComSchema.getEndDate());
		this.Crs_Check_Status = aLAComSchema.getCrs_Check_Status();
		this.BankAccName = aLAComSchema.getBankAccName();
		this.BankAccOpen = aLAComSchema.getBankAccOpen();
		this.IsDotFlag = aLAComSchema.getIsDotFlag();
		this.AgentOrganCode = aLAComSchema.getAgentOrganCode();
		this.PartAcType = aLAComSchema.getPartAcType();
		this.F1 = aLAComSchema.getF1();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AreaType") == null )
				this.AreaType = null;
			else
				this.AreaType = rs.getString("AreaType").trim();

			if( rs.getString("ChannelType") == null )
				this.ChannelType = null;
			else
				this.ChannelType = rs.getString("ChannelType").trim();

			if( rs.getString("UpAgentCom") == null )
				this.UpAgentCom = null;
			else
				this.UpAgentCom = rs.getString("UpAgentCom").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Address") == null )
				this.Address = null;
			else
				this.Address = rs.getString("Address").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("Fax") == null )
				this.Fax = null;
			else
				this.Fax = rs.getString("Fax").trim();

			if( rs.getString("EMail") == null )
				this.EMail = null;
			else
				this.EMail = rs.getString("EMail").trim();

			if( rs.getString("WebAddress") == null )
				this.WebAddress = null;
			else
				this.WebAddress = rs.getString("WebAddress").trim();

			if( rs.getString("LinkMan") == null )
				this.LinkMan = null;
			else
				this.LinkMan = rs.getString("LinkMan").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("Corporation") == null )
				this.Corporation = null;
			else
				this.Corporation = rs.getString("Corporation").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("BusinessType") == null )
				this.BusinessType = null;
			else
				this.BusinessType = rs.getString("BusinessType").trim();

			if( rs.getString("GrpNature") == null )
				this.GrpNature = null;
			else
				this.GrpNature = rs.getString("GrpNature").trim();

			if( rs.getString("ACType") == null )
				this.ACType = null;
			else
				this.ACType = rs.getString("ACType").trim();

			if( rs.getString("SellFlag") == null )
				this.SellFlag = null;
			else
				this.SellFlag = rs.getString("SellFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BankType") == null )
				this.BankType = null;
			else
				this.BankType = rs.getString("BankType").trim();

			if( rs.getString("CalFlag") == null )
				this.CalFlag = null;
			else
				this.CalFlag = rs.getString("CalFlag").trim();

			if( rs.getString("BusiLicenseCode") == null )
				this.BusiLicenseCode = null;
			else
				this.BusiLicenseCode = rs.getString("BusiLicenseCode").trim();

			if( rs.getString("InsureID") == null )
				this.InsureID = null;
			else
				this.InsureID = rs.getString("InsureID").trim();

			if( rs.getString("InsurePrincipal") == null )
				this.InsurePrincipal = null;
			else
				this.InsurePrincipal = rs.getString("InsurePrincipal").trim();

			if( rs.getString("ChiefBusiness") == null )
				this.ChiefBusiness = null;
			else
				this.ChiefBusiness = rs.getString("ChiefBusiness").trim();

			if( rs.getString("BusiAddress") == null )
				this.BusiAddress = null;
			else
				this.BusiAddress = rs.getString("BusiAddress").trim();

			if( rs.getString("SubscribeMan") == null )
				this.SubscribeMan = null;
			else
				this.SubscribeMan = rs.getString("SubscribeMan").trim();

			if( rs.getString("SubscribeManDuty") == null )
				this.SubscribeManDuty = null;
			else
				this.SubscribeManDuty = rs.getString("SubscribeManDuty").trim();

			if( rs.getString("LicenseNo") == null )
				this.LicenseNo = null;
			else
				this.LicenseNo = rs.getString("LicenseNo").trim();

			if( rs.getString("RegionalismCode") == null )
				this.RegionalismCode = null;
			else
				this.RegionalismCode = rs.getString("RegionalismCode").trim();

			if( rs.getString("AppAgentCom") == null )
				this.AppAgentCom = null;
			else
				this.AppAgentCom = rs.getString("AppAgentCom").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Noti") == null )
				this.Noti = null;
			else
				this.Noti = rs.getString("Noti").trim();

			if( rs.getString("BusinessCode") == null )
				this.BusinessCode = null;
			else
				this.BusinessCode = rs.getString("BusinessCode").trim();

			this.LicenseStartDate = rs.getDate("LicenseStartDate");
			this.LicenseEndDate = rs.getDate("LicenseEndDate");
			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			this.Assets = rs.getDouble("Assets");
			this.Income = rs.getDouble("Income");
			this.Profits = rs.getDouble("Profits");
			this.PersonnalSum = rs.getInt("PersonnalSum");
			if( rs.getString("ProtocalNo") == null )
				this.ProtocalNo = null;
			else
				this.ProtocalNo = rs.getString("ProtocalNo").trim();

			if( rs.getString("EndFlag") == null )
				this.EndFlag = null;
			else
				this.EndFlag = rs.getString("EndFlag").trim();

			this.EndDate = rs.getDate("EndDate");
			if( rs.getString("Crs_Check_Status") == null )
				this.Crs_Check_Status = null;
			else
				this.Crs_Check_Status = rs.getString("Crs_Check_Status").trim();

			if( rs.getString("BankAccName") == null )
				this.BankAccName = null;
			else
				this.BankAccName = rs.getString("BankAccName").trim();

			if( rs.getString("BankAccOpen") == null )
				this.BankAccOpen = null;
			else
				this.BankAccOpen = rs.getString("BankAccOpen").trim();

			if( rs.getString("IsDotFlag") == null )
				this.IsDotFlag = null;
			else
				this.IsDotFlag = rs.getString("IsDotFlag").trim();

			if( rs.getString("AgentOrganCode") == null )
				this.AgentOrganCode = null;
			else
				this.AgentOrganCode = rs.getString("AgentOrganCode").trim();

			if( rs.getString("PartAcType") == null )
				this.PartAcType = null;
			else
				this.PartAcType = rs.getString("PartAcType").trim();

			if( rs.getString("F1") == null )
				this.F1 = null;
			else
				this.F1 = rs.getString("F1").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LACom表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAComSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAComSchema getSchema()
	{
		LAComSchema aLAComSchema = new LAComSchema();
		aLAComSchema.setSchema(this);
		return aLAComSchema;
	}

	public LAComDB getDB()
	{
		LAComDB aDBOper = new LAComDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACom描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChannelType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UpAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WebAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LinkMan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Corporation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNature)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ACType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SellFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusiLicenseCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsureID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsurePrincipal)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChiefBusiness)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusiAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubscribeMan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubscribeManDuty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LicenseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RegionalismCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Noti)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LicenseStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LicenseEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Assets));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Income));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Profits));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersonnalSum));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProtocalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_Check_Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccOpen)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IsDotFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentOrganCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PartAcType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F1));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACom>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ChannelType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			UpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			LinkMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Corporation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BusinessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			GrpNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ACType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			SellFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			BankType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			CalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			BusiLicenseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			InsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			InsurePrincipal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			ChiefBusiness = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			BusiAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			SubscribeMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			SubscribeManDuty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			LicenseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			RegionalismCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			AppAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			BusinessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			LicenseStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,SysConst.PACKAGESPILTER));
			LicenseEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			Assets = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
			Income = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).doubleValue();
			Profits = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).doubleValue();
			PersonnalSum= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).intValue();
			ProtocalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			EndFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52,SysConst.PACKAGESPILTER));
			Crs_Check_Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			BankAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			BankAccOpen = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			IsDotFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			AgentOrganCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			PartAcType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			F1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAComSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AreaType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
		}
		if (FCode.equals("ChannelType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelType));
		}
		if (FCode.equals("UpAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UpAgentCom));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Address"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("Fax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
		}
		if (FCode.equals("EMail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
		}
		if (FCode.equals("WebAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
		}
		if (FCode.equals("LinkMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("Corporation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Corporation));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("BusinessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
		}
		if (FCode.equals("GrpNature"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
		}
		if (FCode.equals("ACType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ACType));
		}
		if (FCode.equals("SellFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SellFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BankType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankType));
		}
		if (FCode.equals("CalFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
		}
		if (FCode.equals("BusiLicenseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusiLicenseCode));
		}
		if (FCode.equals("InsureID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsureID));
		}
		if (FCode.equals("InsurePrincipal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsurePrincipal));
		}
		if (FCode.equals("ChiefBusiness"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChiefBusiness));
		}
		if (FCode.equals("BusiAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusiAddress));
		}
		if (FCode.equals("SubscribeMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubscribeMan));
		}
		if (FCode.equals("SubscribeManDuty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubscribeManDuty));
		}
		if (FCode.equals("LicenseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseNo));
		}
		if (FCode.equals("RegionalismCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegionalismCode));
		}
		if (FCode.equals("AppAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppAgentCom));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Noti"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
		}
		if (FCode.equals("BusinessCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessCode));
		}
		if (FCode.equals("LicenseStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLicenseStartDate()));
		}
		if (FCode.equals("LicenseEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLicenseEndDate()));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("Assets"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Assets));
		}
		if (FCode.equals("Income"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Income));
		}
		if (FCode.equals("Profits"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Profits));
		}
		if (FCode.equals("PersonnalSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonnalSum));
		}
		if (FCode.equals("ProtocalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProtocalNo));
		}
		if (FCode.equals("EndFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndFlag));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("Crs_Check_Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_Check_Status));
		}
		if (FCode.equals("BankAccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccName));
		}
		if (FCode.equals("BankAccOpen"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccOpen));
		}
		if (FCode.equals("IsDotFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IsDotFlag));
		}
		if (FCode.equals("AgentOrganCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentOrganCode));
		}
		if (FCode.equals("PartAcType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PartAcType));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AreaType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ChannelType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(UpAgentCom);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Address);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Fax);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(EMail);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(WebAddress);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(LinkMan);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Corporation);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BusinessType);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(GrpNature);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ACType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(SellFlag);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(BankType);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(CalFlag);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(BusiLicenseCode);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(InsureID);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(InsurePrincipal);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(ChiefBusiness);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(BusiAddress);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(SubscribeMan);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(SubscribeManDuty);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(LicenseNo);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(RegionalismCode);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(AppAgentCom);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(Noti);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(BusinessCode);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLicenseStartDate()));
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLicenseEndDate()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 45:
				strFieldValue = String.valueOf(Assets);
				break;
			case 46:
				strFieldValue = String.valueOf(Income);
				break;
			case 47:
				strFieldValue = String.valueOf(Profits);
				break;
			case 48:
				strFieldValue = String.valueOf(PersonnalSum);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(ProtocalNo);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(EndFlag);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(Crs_Check_Status);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(BankAccName);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(BankAccOpen);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(IsDotFlag);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(AgentOrganCode);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(PartAcType);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(F1);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AreaType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaType = FValue.trim();
			}
			else
				AreaType = null;
		}
		if (FCode.equalsIgnoreCase("ChannelType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChannelType = FValue.trim();
			}
			else
				ChannelType = null;
		}
		if (FCode.equalsIgnoreCase("UpAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UpAgentCom = FValue.trim();
			}
			else
				UpAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Address"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Address = FValue.trim();
			}
			else
				Address = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("Fax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Fax = FValue.trim();
			}
			else
				Fax = null;
		}
		if (FCode.equalsIgnoreCase("EMail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EMail = FValue.trim();
			}
			else
				EMail = null;
		}
		if (FCode.equalsIgnoreCase("WebAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WebAddress = FValue.trim();
			}
			else
				WebAddress = null;
		}
		if (FCode.equalsIgnoreCase("LinkMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LinkMan = FValue.trim();
			}
			else
				LinkMan = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("Corporation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Corporation = FValue.trim();
			}
			else
				Corporation = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("BusinessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessType = FValue.trim();
			}
			else
				BusinessType = null;
		}
		if (FCode.equalsIgnoreCase("GrpNature"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNature = FValue.trim();
			}
			else
				GrpNature = null;
		}
		if (FCode.equalsIgnoreCase("ACType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ACType = FValue.trim();
			}
			else
				ACType = null;
		}
		if (FCode.equalsIgnoreCase("SellFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SellFlag = FValue.trim();
			}
			else
				SellFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BankType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankType = FValue.trim();
			}
			else
				BankType = null;
		}
		if (FCode.equalsIgnoreCase("CalFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalFlag = FValue.trim();
			}
			else
				CalFlag = null;
		}
		if (FCode.equalsIgnoreCase("BusiLicenseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusiLicenseCode = FValue.trim();
			}
			else
				BusiLicenseCode = null;
		}
		if (FCode.equalsIgnoreCase("InsureID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsureID = FValue.trim();
			}
			else
				InsureID = null;
		}
		if (FCode.equalsIgnoreCase("InsurePrincipal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsurePrincipal = FValue.trim();
			}
			else
				InsurePrincipal = null;
		}
		if (FCode.equalsIgnoreCase("ChiefBusiness"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChiefBusiness = FValue.trim();
			}
			else
				ChiefBusiness = null;
		}
		if (FCode.equalsIgnoreCase("BusiAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusiAddress = FValue.trim();
			}
			else
				BusiAddress = null;
		}
		if (FCode.equalsIgnoreCase("SubscribeMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubscribeMan = FValue.trim();
			}
			else
				SubscribeMan = null;
		}
		if (FCode.equalsIgnoreCase("SubscribeManDuty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubscribeManDuty = FValue.trim();
			}
			else
				SubscribeManDuty = null;
		}
		if (FCode.equalsIgnoreCase("LicenseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LicenseNo = FValue.trim();
			}
			else
				LicenseNo = null;
		}
		if (FCode.equalsIgnoreCase("RegionalismCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RegionalismCode = FValue.trim();
			}
			else
				RegionalismCode = null;
		}
		if (FCode.equalsIgnoreCase("AppAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppAgentCom = FValue.trim();
			}
			else
				AppAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Noti"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Noti = FValue.trim();
			}
			else
				Noti = null;
		}
		if (FCode.equalsIgnoreCase("BusinessCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessCode = FValue.trim();
			}
			else
				BusinessCode = null;
		}
		if (FCode.equalsIgnoreCase("LicenseStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LicenseStartDate = fDate.getDate( FValue );
			}
			else
				LicenseStartDate = null;
		}
		if (FCode.equalsIgnoreCase("LicenseEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LicenseEndDate = fDate.getDate( FValue );
			}
			else
				LicenseEndDate = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("Assets"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Assets = d;
			}
		}
		if (FCode.equalsIgnoreCase("Income"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Income = d;
			}
		}
		if (FCode.equalsIgnoreCase("Profits"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Profits = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersonnalSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PersonnalSum = i;
			}
		}
		if (FCode.equalsIgnoreCase("ProtocalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProtocalNo = FValue.trim();
			}
			else
				ProtocalNo = null;
		}
		if (FCode.equalsIgnoreCase("EndFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndFlag = FValue.trim();
			}
			else
				EndFlag = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("Crs_Check_Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_Check_Status = FValue.trim();
			}
			else
				Crs_Check_Status = null;
		}
		if (FCode.equalsIgnoreCase("BankAccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccName = FValue.trim();
			}
			else
				BankAccName = null;
		}
		if (FCode.equalsIgnoreCase("BankAccOpen"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccOpen = FValue.trim();
			}
			else
				BankAccOpen = null;
		}
		if (FCode.equalsIgnoreCase("IsDotFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IsDotFlag = FValue.trim();
			}
			else
				IsDotFlag = null;
		}
		if (FCode.equalsIgnoreCase("AgentOrganCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentOrganCode = FValue.trim();
			}
			else
				AgentOrganCode = null;
		}
		if (FCode.equalsIgnoreCase("PartAcType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PartAcType = FValue.trim();
			}
			else
				PartAcType = null;
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F1 = FValue.trim();
			}
			else
				F1 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAComSchema other = (LAComSchema)otherObject;
		return
			(AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (AreaType == null ? other.getAreaType() == null : AreaType.equals(other.getAreaType()))
			&& (ChannelType == null ? other.getChannelType() == null : ChannelType.equals(other.getChannelType()))
			&& (UpAgentCom == null ? other.getUpAgentCom() == null : UpAgentCom.equals(other.getUpAgentCom()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Address == null ? other.getAddress() == null : Address.equals(other.getAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (Fax == null ? other.getFax() == null : Fax.equals(other.getFax()))
			&& (EMail == null ? other.getEMail() == null : EMail.equals(other.getEMail()))
			&& (WebAddress == null ? other.getWebAddress() == null : WebAddress.equals(other.getWebAddress()))
			&& (LinkMan == null ? other.getLinkMan() == null : LinkMan.equals(other.getLinkMan()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (Corporation == null ? other.getCorporation() == null : Corporation.equals(other.getCorporation()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (BusinessType == null ? other.getBusinessType() == null : BusinessType.equals(other.getBusinessType()))
			&& (GrpNature == null ? other.getGrpNature() == null : GrpNature.equals(other.getGrpNature()))
			&& (ACType == null ? other.getACType() == null : ACType.equals(other.getACType()))
			&& (SellFlag == null ? other.getSellFlag() == null : SellFlag.equals(other.getSellFlag()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BankType == null ? other.getBankType() == null : BankType.equals(other.getBankType()))
			&& (CalFlag == null ? other.getCalFlag() == null : CalFlag.equals(other.getCalFlag()))
			&& (BusiLicenseCode == null ? other.getBusiLicenseCode() == null : BusiLicenseCode.equals(other.getBusiLicenseCode()))
			&& (InsureID == null ? other.getInsureID() == null : InsureID.equals(other.getInsureID()))
			&& (InsurePrincipal == null ? other.getInsurePrincipal() == null : InsurePrincipal.equals(other.getInsurePrincipal()))
			&& (ChiefBusiness == null ? other.getChiefBusiness() == null : ChiefBusiness.equals(other.getChiefBusiness()))
			&& (BusiAddress == null ? other.getBusiAddress() == null : BusiAddress.equals(other.getBusiAddress()))
			&& (SubscribeMan == null ? other.getSubscribeMan() == null : SubscribeMan.equals(other.getSubscribeMan()))
			&& (SubscribeManDuty == null ? other.getSubscribeManDuty() == null : SubscribeManDuty.equals(other.getSubscribeManDuty()))
			&& (LicenseNo == null ? other.getLicenseNo() == null : LicenseNo.equals(other.getLicenseNo()))
			&& (RegionalismCode == null ? other.getRegionalismCode() == null : RegionalismCode.equals(other.getRegionalismCode()))
			&& (AppAgentCom == null ? other.getAppAgentCom() == null : AppAgentCom.equals(other.getAppAgentCom()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Noti == null ? other.getNoti() == null : Noti.equals(other.getNoti()))
			&& (BusinessCode == null ? other.getBusinessCode() == null : BusinessCode.equals(other.getBusinessCode()))
			&& (LicenseStartDate == null ? other.getLicenseStartDate() == null : fDate.getString(LicenseStartDate).equals(other.getLicenseStartDate()))
			&& (LicenseEndDate == null ? other.getLicenseEndDate() == null : fDate.getString(LicenseEndDate).equals(other.getLicenseEndDate()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& Assets == other.getAssets()
			&& Income == other.getIncome()
			&& Profits == other.getProfits()
			&& PersonnalSum == other.getPersonnalSum()
			&& (ProtocalNo == null ? other.getProtocalNo() == null : ProtocalNo.equals(other.getProtocalNo()))
			&& (EndFlag == null ? other.getEndFlag() == null : EndFlag.equals(other.getEndFlag()))
			&& (EndDate == null ? other.getEndDate() == null : fDate.getString(EndDate).equals(other.getEndDate()))
			&& (Crs_Check_Status == null ? other.getCrs_Check_Status() == null : Crs_Check_Status.equals(other.getCrs_Check_Status()))
			&& (BankAccName == null ? other.getBankAccName() == null : BankAccName.equals(other.getBankAccName()))
			&& (BankAccOpen == null ? other.getBankAccOpen() == null : BankAccOpen.equals(other.getBankAccOpen()))
			&& (IsDotFlag == null ? other.getIsDotFlag() == null : IsDotFlag.equals(other.getIsDotFlag()))
			&& (AgentOrganCode == null ? other.getAgentOrganCode() == null : AgentOrganCode.equals(other.getAgentOrganCode()))
			&& (PartAcType == null ? other.getPartAcType() == null : PartAcType.equals(other.getPartAcType()))
			&& (F1 == null ? other.getF1() == null : F1.equals(other.getF1()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AgentCom") ) {
			return 0;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 1;
		}
		if( strFieldName.equals("AreaType") ) {
			return 2;
		}
		if( strFieldName.equals("ChannelType") ) {
			return 3;
		}
		if( strFieldName.equals("UpAgentCom") ) {
			return 4;
		}
		if( strFieldName.equals("Name") ) {
			return 5;
		}
		if( strFieldName.equals("Address") ) {
			return 6;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 7;
		}
		if( strFieldName.equals("Phone") ) {
			return 8;
		}
		if( strFieldName.equals("Fax") ) {
			return 9;
		}
		if( strFieldName.equals("EMail") ) {
			return 10;
		}
		if( strFieldName.equals("WebAddress") ) {
			return 11;
		}
		if( strFieldName.equals("LinkMan") ) {
			return 12;
		}
		if( strFieldName.equals("Password") ) {
			return 13;
		}
		if( strFieldName.equals("Corporation") ) {
			return 14;
		}
		if( strFieldName.equals("BankCode") ) {
			return 15;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 16;
		}
		if( strFieldName.equals("BusinessType") ) {
			return 17;
		}
		if( strFieldName.equals("GrpNature") ) {
			return 18;
		}
		if( strFieldName.equals("ACType") ) {
			return 19;
		}
		if( strFieldName.equals("SellFlag") ) {
			return 20;
		}
		if( strFieldName.equals("Operator") ) {
			return 21;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 25;
		}
		if( strFieldName.equals("BankType") ) {
			return 26;
		}
		if( strFieldName.equals("CalFlag") ) {
			return 27;
		}
		if( strFieldName.equals("BusiLicenseCode") ) {
			return 28;
		}
		if( strFieldName.equals("InsureID") ) {
			return 29;
		}
		if( strFieldName.equals("InsurePrincipal") ) {
			return 30;
		}
		if( strFieldName.equals("ChiefBusiness") ) {
			return 31;
		}
		if( strFieldName.equals("BusiAddress") ) {
			return 32;
		}
		if( strFieldName.equals("SubscribeMan") ) {
			return 33;
		}
		if( strFieldName.equals("SubscribeManDuty") ) {
			return 34;
		}
		if( strFieldName.equals("LicenseNo") ) {
			return 35;
		}
		if( strFieldName.equals("RegionalismCode") ) {
			return 36;
		}
		if( strFieldName.equals("AppAgentCom") ) {
			return 37;
		}
		if( strFieldName.equals("State") ) {
			return 38;
		}
		if( strFieldName.equals("Noti") ) {
			return 39;
		}
		if( strFieldName.equals("BusinessCode") ) {
			return 40;
		}
		if( strFieldName.equals("LicenseStartDate") ) {
			return 41;
		}
		if( strFieldName.equals("LicenseEndDate") ) {
			return 42;
		}
		if( strFieldName.equals("BranchType") ) {
			return 43;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 44;
		}
		if( strFieldName.equals("Assets") ) {
			return 45;
		}
		if( strFieldName.equals("Income") ) {
			return 46;
		}
		if( strFieldName.equals("Profits") ) {
			return 47;
		}
		if( strFieldName.equals("PersonnalSum") ) {
			return 48;
		}
		if( strFieldName.equals("ProtocalNo") ) {
			return 49;
		}
		if( strFieldName.equals("EndFlag") ) {
			return 50;
		}
		if( strFieldName.equals("EndDate") ) {
			return 51;
		}
		if( strFieldName.equals("Crs_Check_Status") ) {
			return 52;
		}
		if( strFieldName.equals("BankAccName") ) {
			return 53;
		}
		if( strFieldName.equals("BankAccOpen") ) {
			return 54;
		}
		if( strFieldName.equals("IsDotFlag") ) {
			return 55;
		}
		if( strFieldName.equals("AgentOrganCode") ) {
			return 56;
		}
		if( strFieldName.equals("PartAcType") ) {
			return 57;
		}
		if( strFieldName.equals("F1") ) {
			return 58;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AgentCom";
				break;
			case 1:
				strFieldName = "ManageCom";
				break;
			case 2:
				strFieldName = "AreaType";
				break;
			case 3:
				strFieldName = "ChannelType";
				break;
			case 4:
				strFieldName = "UpAgentCom";
				break;
			case 5:
				strFieldName = "Name";
				break;
			case 6:
				strFieldName = "Address";
				break;
			case 7:
				strFieldName = "ZipCode";
				break;
			case 8:
				strFieldName = "Phone";
				break;
			case 9:
				strFieldName = "Fax";
				break;
			case 10:
				strFieldName = "EMail";
				break;
			case 11:
				strFieldName = "WebAddress";
				break;
			case 12:
				strFieldName = "LinkMan";
				break;
			case 13:
				strFieldName = "Password";
				break;
			case 14:
				strFieldName = "Corporation";
				break;
			case 15:
				strFieldName = "BankCode";
				break;
			case 16:
				strFieldName = "BankAccNo";
				break;
			case 17:
				strFieldName = "BusinessType";
				break;
			case 18:
				strFieldName = "GrpNature";
				break;
			case 19:
				strFieldName = "ACType";
				break;
			case 20:
				strFieldName = "SellFlag";
				break;
			case 21:
				strFieldName = "Operator";
				break;
			case 22:
				strFieldName = "MakeDate";
				break;
			case 23:
				strFieldName = "MakeTime";
				break;
			case 24:
				strFieldName = "ModifyDate";
				break;
			case 25:
				strFieldName = "ModifyTime";
				break;
			case 26:
				strFieldName = "BankType";
				break;
			case 27:
				strFieldName = "CalFlag";
				break;
			case 28:
				strFieldName = "BusiLicenseCode";
				break;
			case 29:
				strFieldName = "InsureID";
				break;
			case 30:
				strFieldName = "InsurePrincipal";
				break;
			case 31:
				strFieldName = "ChiefBusiness";
				break;
			case 32:
				strFieldName = "BusiAddress";
				break;
			case 33:
				strFieldName = "SubscribeMan";
				break;
			case 34:
				strFieldName = "SubscribeManDuty";
				break;
			case 35:
				strFieldName = "LicenseNo";
				break;
			case 36:
				strFieldName = "RegionalismCode";
				break;
			case 37:
				strFieldName = "AppAgentCom";
				break;
			case 38:
				strFieldName = "State";
				break;
			case 39:
				strFieldName = "Noti";
				break;
			case 40:
				strFieldName = "BusinessCode";
				break;
			case 41:
				strFieldName = "LicenseStartDate";
				break;
			case 42:
				strFieldName = "LicenseEndDate";
				break;
			case 43:
				strFieldName = "BranchType";
				break;
			case 44:
				strFieldName = "BranchType2";
				break;
			case 45:
				strFieldName = "Assets";
				break;
			case 46:
				strFieldName = "Income";
				break;
			case 47:
				strFieldName = "Profits";
				break;
			case 48:
				strFieldName = "PersonnalSum";
				break;
			case 49:
				strFieldName = "ProtocalNo";
				break;
			case 50:
				strFieldName = "EndFlag";
				break;
			case 51:
				strFieldName = "EndDate";
				break;
			case 52:
				strFieldName = "Crs_Check_Status";
				break;
			case 53:
				strFieldName = "BankAccName";
				break;
			case 54:
				strFieldName = "BankAccOpen";
				break;
			case 55:
				strFieldName = "IsDotFlag";
				break;
			case 56:
				strFieldName = "AgentOrganCode";
				break;
			case 57:
				strFieldName = "PartAcType";
				break;
			case 58:
				strFieldName = "F1";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChannelType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UpAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Address") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EMail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WebAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LinkMan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Corporation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNature") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ACType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SellFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusiLicenseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsureID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsurePrincipal") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChiefBusiness") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusiAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubscribeMan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubscribeManDuty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LicenseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegionalismCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Noti") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LicenseStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LicenseEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Assets") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Income") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Profits") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersonnalSum") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ProtocalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Crs_Check_Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccOpen") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IsDotFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentOrganCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PartAcType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 47:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 48:
				nFieldType = Schema.TYPE_INT;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
