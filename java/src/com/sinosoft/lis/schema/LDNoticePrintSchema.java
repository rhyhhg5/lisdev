/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDNoticePrintDB;

/*
 * <p>ClassName: LDNoticePrintSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-02-07
 */
public class LDNoticePrintSchema implements Schema, Cloneable
{
	// @Field
	/** 套餐编码 */
	private String RiskWrapCode;
	/** 告知名称 */
	private String ItemName;
	/** 告知类型 */
	private String ItemType;
	/** 文档名称 */
	private String FileName;

	public static final int FIELDNUM = 4;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDNoticePrintSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "RiskWrapCode";
		pk[1] = "ItemType";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDNoticePrintSchema cloned = (LDNoticePrintSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRiskWrapCode()
	{
		return RiskWrapCode;
	}
	public void setRiskWrapCode(String aRiskWrapCode)
	{
		RiskWrapCode = aRiskWrapCode;
	}
	public String getItemName()
	{
		return ItemName;
	}
	public void setItemName(String aItemName)
	{
		ItemName = aItemName;
	}
	public String getItemType()
	{
		return ItemType;
	}
	public void setItemType(String aItemType)
	{
		ItemType = aItemType;
	}
	public String getFileName()
	{
		return FileName;
	}
	public void setFileName(String aFileName)
	{
		FileName = aFileName;
	}

	/**
	* 使用另外一个 LDNoticePrintSchema 对象给 Schema 赋值
	* @param: aLDNoticePrintSchema LDNoticePrintSchema
	**/
	public void setSchema(LDNoticePrintSchema aLDNoticePrintSchema)
	{
		this.RiskWrapCode = aLDNoticePrintSchema.getRiskWrapCode();
		this.ItemName = aLDNoticePrintSchema.getItemName();
		this.ItemType = aLDNoticePrintSchema.getItemType();
		this.FileName = aLDNoticePrintSchema.getFileName();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RiskWrapCode") == null )
				this.RiskWrapCode = null;
			else
				this.RiskWrapCode = rs.getString("RiskWrapCode").trim();

			if( rs.getString("ItemName") == null )
				this.ItemName = null;
			else
				this.ItemName = rs.getString("ItemName").trim();

			if( rs.getString("ItemType") == null )
				this.ItemType = null;
			else
				this.ItemType = rs.getString("ItemType").trim();

			if( rs.getString("FileName") == null )
				this.FileName = null;
			else
				this.FileName = rs.getString("FileName").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDNoticePrint表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDNoticePrintSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDNoticePrintSchema getSchema()
	{
		LDNoticePrintSchema aLDNoticePrintSchema = new LDNoticePrintSchema();
		aLDNoticePrintSchema.setSchema(this);
		return aLDNoticePrintSchema;
	}

	public LDNoticePrintDB getDB()
	{
		LDNoticePrintDB aDBOper = new LDNoticePrintDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDNoticePrint描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RiskWrapCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ItemName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ItemType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FileName));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDNoticePrint>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RiskWrapCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDNoticePrintSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RiskWrapCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskWrapCode));
		}
		if (FCode.equals("ItemName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemName));
		}
		if (FCode.equals("ItemType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemType));
		}
		if (FCode.equals("FileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RiskWrapCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ItemName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ItemType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(FileName);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RiskWrapCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskWrapCode = FValue.trim();
			}
			else
				RiskWrapCode = null;
		}
		if (FCode.equalsIgnoreCase("ItemName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemName = FValue.trim();
			}
			else
				ItemName = null;
		}
		if (FCode.equalsIgnoreCase("ItemType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemType = FValue.trim();
			}
			else
				ItemType = null;
		}
		if (FCode.equalsIgnoreCase("FileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileName = FValue.trim();
			}
			else
				FileName = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDNoticePrintSchema other = (LDNoticePrintSchema)otherObject;
		return
			(RiskWrapCode == null ? other.getRiskWrapCode() == null : RiskWrapCode.equals(other.getRiskWrapCode()))
			&& (ItemName == null ? other.getItemName() == null : ItemName.equals(other.getItemName()))
			&& (ItemType == null ? other.getItemType() == null : ItemType.equals(other.getItemType()))
			&& (FileName == null ? other.getFileName() == null : FileName.equals(other.getFileName()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RiskWrapCode") ) {
			return 0;
		}
		if( strFieldName.equals("ItemName") ) {
			return 1;
		}
		if( strFieldName.equals("ItemType") ) {
			return 2;
		}
		if( strFieldName.equals("FileName") ) {
			return 3;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RiskWrapCode";
				break;
			case 1:
				strFieldName = "ItemName";
				break;
			case 2:
				strFieldName = "ItemType";
				break;
			case 3:
				strFieldName = "FileName";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RiskWrapCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FileName") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
