/*
 * <p>ClassName: LPComBlacklistSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPComBlacklistDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LPComBlacklistSchema implements Schema
{
    // @Field
    /** 上报申请号 */
    private String AppID;
    /** 上报项目 */
    private String AppItem;
    /** 代理机构 */
    private String AgentCom;
    /** 黑名单次数 */
    private int Times;
    /** 管理机构 */
    private String ManageCom;
    /** 保险公司id */
    private String InsureID;
    /** 上报代码 */
    private String AppAgentCom;
    /** 黑名单日期 */
    private Date StartDate;
    /** 黑名单期限 */
    private int Period;
    /** 黑名单原因 */
    private String Reason;
    /** 黑名单状态 */
    private String State;
    /** 备注 */
    private String Noti;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 填表日期 */
    private Date FillDate;
    /** 表号 */
    private String TableNo;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPComBlacklistSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "AppID";
        pk[1] = "AppItem";
        pk[2] = "AgentCom";
        pk[3] = "Times";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAppID()
    {
        if (AppID != null && !AppID.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppID = StrTool.unicodeToGBK(AppID);
        }
        return AppID;
    }

    public void setAppID(String aAppID)
    {
        AppID = aAppID;
    }

    public String getAppItem()
    {
        if (AppItem != null && !AppItem.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppItem = StrTool.unicodeToGBK(AppItem);
        }
        return AppItem;
    }

    public void setAppItem(String aAppItem)
    {
        AppItem = aAppItem;
    }

    public String getAgentCom()
    {
        if (AgentCom != null && !AgentCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public int getTimes()
    {
        return Times;
    }

    public void setTimes(int aTimes)
    {
        Times = aTimes;
    }

    public void setTimes(String aTimes)
    {
        if (aTimes != null && !aTimes.equals(""))
        {
            Integer tInteger = new Integer(aTimes);
            int i = tInteger.intValue();
            Times = i;
        }
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getInsureID()
    {
        if (InsureID != null && !InsureID.equals("") && SysConst.CHANGECHARSET == true)
        {
            InsureID = StrTool.unicodeToGBK(InsureID);
        }
        return InsureID;
    }

    public void setInsureID(String aInsureID)
    {
        InsureID = aInsureID;
    }

    public String getAppAgentCom()
    {
        if (AppAgentCom != null && !AppAgentCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppAgentCom = StrTool.unicodeToGBK(AppAgentCom);
        }
        return AppAgentCom;
    }

    public void setAppAgentCom(String aAppAgentCom)
    {
        AppAgentCom = aAppAgentCom;
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public int getPeriod()
    {
        return Period;
    }

    public void setPeriod(int aPeriod)
    {
        Period = aPeriod;
    }

    public void setPeriod(String aPeriod)
    {
        if (aPeriod != null && !aPeriod.equals(""))
        {
            Integer tInteger = new Integer(aPeriod);
            int i = tInteger.intValue();
            Period = i;
        }
    }

    public String getReason()
    {
        if (Reason != null && !Reason.equals("") && SysConst.CHANGECHARSET == true)
        {
            Reason = StrTool.unicodeToGBK(Reason);
        }
        return Reason;
    }

    public void setReason(String aReason)
    {
        Reason = aReason;
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET == true)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getNoti()
    {
        if (Noti != null && !Noti.equals("") && SysConst.CHANGECHARSET == true)
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getFillDate()
    {
        if (FillDate != null)
        {
            return fDate.getString(FillDate);
        }
        else
        {
            return null;
        }
    }

    public void setFillDate(Date aFillDate)
    {
        FillDate = aFillDate;
    }

    public void setFillDate(String aFillDate)
    {
        if (aFillDate != null && !aFillDate.equals(""))
        {
            FillDate = fDate.getDate(aFillDate);
        }
        else
        {
            FillDate = null;
        }
    }

    public String getTableNo()
    {
        if (TableNo != null && !TableNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            TableNo = StrTool.unicodeToGBK(TableNo);
        }
        return TableNo;
    }

    public void setTableNo(String aTableNo)
    {
        TableNo = aTableNo;
    }

    /**
     * 使用另外一个 LPComBlacklistSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LPComBlacklistSchema aLPComBlacklistSchema)
    {
        this.AppID = aLPComBlacklistSchema.getAppID();
        this.AppItem = aLPComBlacklistSchema.getAppItem();
        this.AgentCom = aLPComBlacklistSchema.getAgentCom();
        this.Times = aLPComBlacklistSchema.getTimes();
        this.ManageCom = aLPComBlacklistSchema.getManageCom();
        this.InsureID = aLPComBlacklistSchema.getInsureID();
        this.AppAgentCom = aLPComBlacklistSchema.getAppAgentCom();
        this.StartDate = fDate.getDate(aLPComBlacklistSchema.getStartDate());
        this.Period = aLPComBlacklistSchema.getPeriod();
        this.Reason = aLPComBlacklistSchema.getReason();
        this.State = aLPComBlacklistSchema.getState();
        this.Noti = aLPComBlacklistSchema.getNoti();
        this.Operator = aLPComBlacklistSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPComBlacklistSchema.getMakeDate());
        this.MakeTime = aLPComBlacklistSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPComBlacklistSchema.getModifyDate());
        this.ModifyTime = aLPComBlacklistSchema.getModifyTime();
        this.FillDate = fDate.getDate(aLPComBlacklistSchema.getFillDate());
        this.TableNo = aLPComBlacklistSchema.getTableNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AppID") == null)
            {
                this.AppID = null;
            }
            else
            {
                this.AppID = rs.getString("AppID").trim();
            }

            if (rs.getString("AppItem") == null)
            {
                this.AppItem = null;
            }
            else
            {
                this.AppItem = rs.getString("AppItem").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            this.Times = rs.getInt("Times");
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("InsureID") == null)
            {
                this.InsureID = null;
            }
            else
            {
                this.InsureID = rs.getString("InsureID").trim();
            }

            if (rs.getString("AppAgentCom") == null)
            {
                this.AppAgentCom = null;
            }
            else
            {
                this.AppAgentCom = rs.getString("AppAgentCom").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.Period = rs.getInt("Period");
            if (rs.getString("Reason") == null)
            {
                this.Reason = null;
            }
            else
            {
                this.Reason = rs.getString("Reason").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.FillDate = rs.getDate("FillDate");
            if (rs.getString("TableNo") == null)
            {
                this.TableNo = null;
            }
            else
            {
                this.TableNo = rs.getString("TableNo").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPComBlacklistSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPComBlacklistSchema getSchema()
    {
        LPComBlacklistSchema aLPComBlacklistSchema = new LPComBlacklistSchema();
        aLPComBlacklistSchema.setSchema(this);
        return aLPComBlacklistSchema;
    }

    public LPComBlacklistDB getDB()
    {
        LPComBlacklistDB aDBOper = new LPComBlacklistDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPComBlacklist描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AppID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppItem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Times) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsureID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppAgentCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            StartDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Period) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Reason)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Noti)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(FillDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TableNo));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPComBlacklist>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AppID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            AppItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            Times = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).intValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            InsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            AppAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            Period = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                  SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            FillDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            TableNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPComBlacklistSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AppID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppID));
        }
        if (FCode.equals("AppItem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppItem));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCom));
        }
        if (FCode.equals("Times"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Times));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("InsureID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsureID));
        }
        if (FCode.equals("AppAgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppAgentCom));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getStartDate()));
        }
        if (FCode.equals("Period"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Period));
        }
        if (FCode.equals("Reason"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Reason));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(State));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Noti));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("FillDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getFillDate()));
        }
        if (FCode.equals("TableNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TableNo));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AppID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AppItem);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 3:
                strFieldValue = String.valueOf(Times);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(InsureID);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AppAgentCom);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 8:
                strFieldValue = String.valueOf(Period);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Reason);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFillDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(TableNo);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AppID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppID = FValue.trim();
            }
            else
            {
                AppID = null;
            }
        }
        if (FCode.equals("AppItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppItem = FValue.trim();
            }
            else
            {
                AppItem = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("Times"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Times = i;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("InsureID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsureID = FValue.trim();
            }
            else
            {
                InsureID = null;
            }
        }
        if (FCode.equals("AppAgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppAgentCom = FValue.trim();
            }
            else
            {
                AppAgentCom = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("Period"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Period = i;
            }
        }
        if (FCode.equals("Reason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Reason = FValue.trim();
            }
            else
            {
                Reason = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("FillDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FillDate = fDate.getDate(FValue);
            }
            else
            {
                FillDate = null;
            }
        }
        if (FCode.equals("TableNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TableNo = FValue.trim();
            }
            else
            {
                TableNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPComBlacklistSchema other = (LPComBlacklistSchema) otherObject;
        return
                AppID.equals(other.getAppID())
                && AppItem.equals(other.getAppItem())
                && AgentCom.equals(other.getAgentCom())
                && Times == other.getTimes()
                && ManageCom.equals(other.getManageCom())
                && InsureID.equals(other.getInsureID())
                && AppAgentCom.equals(other.getAppAgentCom())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && Period == other.getPeriod()
                && Reason.equals(other.getReason())
                && State.equals(other.getState())
                && Noti.equals(other.getNoti())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(FillDate).equals(other.getFillDate())
                && TableNo.equals(other.getTableNo());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return 0;
        }
        if (strFieldName.equals("AppItem"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 2;
        }
        if (strFieldName.equals("Times"))
        {
            return 3;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 4;
        }
        if (strFieldName.equals("InsureID"))
        {
            return 5;
        }
        if (strFieldName.equals("AppAgentCom"))
        {
            return 6;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 7;
        }
        if (strFieldName.equals("Period"))
        {
            return 8;
        }
        if (strFieldName.equals("Reason"))
        {
            return 9;
        }
        if (strFieldName.equals("State"))
        {
            return 10;
        }
        if (strFieldName.equals("Noti"))
        {
            return 11;
        }
        if (strFieldName.equals("Operator"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 16;
        }
        if (strFieldName.equals("FillDate"))
        {
            return 17;
        }
        if (strFieldName.equals("TableNo"))
        {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AppID";
                break;
            case 1:
                strFieldName = "AppItem";
                break;
            case 2:
                strFieldName = "AgentCom";
                break;
            case 3:
                strFieldName = "Times";
                break;
            case 4:
                strFieldName = "ManageCom";
                break;
            case 5:
                strFieldName = "InsureID";
                break;
            case 6:
                strFieldName = "AppAgentCom";
                break;
            case 7:
                strFieldName = "StartDate";
                break;
            case 8:
                strFieldName = "Period";
                break;
            case 9:
                strFieldName = "Reason";
                break;
            case 10:
                strFieldName = "State";
                break;
            case 11:
                strFieldName = "Noti";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "FillDate";
                break;
            case 18:
                strFieldName = "TableNo";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Times"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsureID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppAgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Period"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Reason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FillDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("TableNo"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
