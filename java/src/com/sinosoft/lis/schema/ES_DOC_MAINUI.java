package com.sinosoft.lis.schema;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ES_DOC_MAINUI {
	
	public CErrors mErrors = new CErrors();
	
	private ES_DOC_MAINBL pdBL;
	
	public ES_DOC_MAINUI() {
		this.pdBL = new ES_DOC_MAINBL();
	}
	
	public boolean submiteData(VData cInputData) {
		if(!pdBL.submitData(cInputData))
        {
            mErrors.copyAllErrors(pdBL.mErrors);
            return false;
        }
        return true;
	}
	

}
