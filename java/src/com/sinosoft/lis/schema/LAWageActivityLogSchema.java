/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAWageActivityLogDB;

/*
 * <p>ClassName: LAWageActivityLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-09-18
 */
public class LAWageActivityLogSchema implements Schema, Cloneable {
    // @Field
    /** 流水号 */
    private int SerialNo;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 日志类型 */
    private String WageLogType;
    /** 薪资计算年月代码 */
    private String WageNo;
    /** 团体合同号 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 集体险种号码 */
    private String GrpPolNo;
    /** 个人险种号码 */
    private String PolNo;
    /** 行为描述 */
    private String Describe;
    /** 险种编码 */
    private String RiskCode;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 其它号码 */
    private String OtherNo;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAWageActivityLogSchema() {
        mErrors = new CErrors();

        String[] pk = new String[8];
        pk[0] = "SerialNo";
        pk[1] = "AgentCode";
        pk[2] = "AgentGroup";
        pk[3] = "WageLogType";
        pk[4] = "GrpContNo";
        pk[5] = "ContNo";
        pk[6] = "GrpPolNo";
        pk[7] = "PolNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAWageActivityLogSchema cloned = (LAWageActivityLogSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public int getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(int aSerialNo) {
        SerialNo = aSerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        if (aSerialNo != null && !aSerialNo.equals("")) {
            Integer tInteger = new Integer(aSerialNo);
            int i = tInteger.intValue();
            SerialNo = i;
        }
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }

    public String getWageLogType() {
        return WageLogType;
    }

    public void setWageLogType(String aWageLogType) {
        WageLogType = aWageLogType;
    }

    public String getWageNo() {
        return WageNo;
    }

    public void setWageNo(String aWageNo) {
        WageNo = aWageNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getGrpPolNo() {
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }

    public String getDescribe() {
        return Describe;
    }

    public void setDescribe(String aDescribe) {
        Describe = aDescribe;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getOtherNoType() {
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }

    public String getOtherNo() {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LAWageActivityLogSchema 对象给 Schema 赋值
     * @param: aLAWageActivityLogSchema LAWageActivityLogSchema
     **/
    public void setSchema(LAWageActivityLogSchema aLAWageActivityLogSchema) {
        this.SerialNo = aLAWageActivityLogSchema.getSerialNo();
        this.AgentCode = aLAWageActivityLogSchema.getAgentCode();
        this.AgentGroup = aLAWageActivityLogSchema.getAgentGroup();
        this.WageLogType = aLAWageActivityLogSchema.getWageLogType();
        this.WageNo = aLAWageActivityLogSchema.getWageNo();
        this.GrpContNo = aLAWageActivityLogSchema.getGrpContNo();
        this.ContNo = aLAWageActivityLogSchema.getContNo();
        this.GrpPolNo = aLAWageActivityLogSchema.getGrpPolNo();
        this.PolNo = aLAWageActivityLogSchema.getPolNo();
        this.Describe = aLAWageActivityLogSchema.getDescribe();
        this.RiskCode = aLAWageActivityLogSchema.getRiskCode();
        this.OtherNoType = aLAWageActivityLogSchema.getOtherNoType();
        this.OtherNo = aLAWageActivityLogSchema.getOtherNo();
        this.ManageCom = aLAWageActivityLogSchema.getManageCom();
        this.Operator = aLAWageActivityLogSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAWageActivityLogSchema.getMakeDate());
        this.MakeTime = aLAWageActivityLogSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            this.SerialNo = rs.getInt("SerialNo");
            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null) {
                this.AgentGroup = null;
            } else {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("WageLogType") == null) {
                this.WageLogType = null;
            } else {
                this.WageLogType = rs.getString("WageLogType").trim();
            }

            if (rs.getString("WageNo") == null) {
                this.WageNo = null;
            } else {
                this.WageNo = rs.getString("WageNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null) {
                this.GrpPolNo = null;
            } else {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("PolNo") == null) {
                this.PolNo = null;
            } else {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("Describe") == null) {
                this.Describe = null;
            } else {
                this.Describe = rs.getString("Describe").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("OtherNoType") == null) {
                this.OtherNoType = null;
            } else {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("OtherNo") == null) {
                this.OtherNo = null;
            } else {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LAWageActivityLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageActivityLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAWageActivityLogSchema getSchema() {
        LAWageActivityLogSchema aLAWageActivityLogSchema = new
                LAWageActivityLogSchema();
        aLAWageActivityLogSchema.setSchema(this);
        return aLAWageActivityLogSchema;
    }

    public LAWageActivityLogDB getDB() {
        LAWageActivityLogDB aDBOper = new LAWageActivityLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageActivityLog描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WageLogType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WageNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpPolNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Describe));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageActivityLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 1, SysConst.PACKAGESPILTER))).intValue();
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            WageLogType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            Describe = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageActivityLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("WageLogType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageLogType));
        }
        if (FCode.equals("WageNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("Describe")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Describe));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equals("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = String.valueOf(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(AgentGroup);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(WageLogType);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(WageNo);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(PolNo);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(Describe);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(OtherNoType);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(OtherNo);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SerialNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if (FValue != null && !FValue.equals("")) {
                AgentGroup = FValue.trim();
            } else {
                AgentGroup = null;
            }
        }
        if (FCode.equalsIgnoreCase("WageLogType")) {
            if (FValue != null && !FValue.equals("")) {
                WageLogType = FValue.trim();
            } else {
                WageLogType = null;
            }
        }
        if (FCode.equalsIgnoreCase("WageNo")) {
            if (FValue != null && !FValue.equals("")) {
                WageNo = FValue.trim();
            } else {
                WageNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpPolNo = FValue.trim();
            } else {
                GrpPolNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if (FValue != null && !FValue.equals("")) {
                PolNo = FValue.trim();
            } else {
                PolNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Describe")) {
            if (FValue != null && !FValue.equals("")) {
                Describe = FValue.trim();
            } else {
                Describe = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if (FValue != null && !FValue.equals("")) {
                OtherNoType = FValue.trim();
            } else {
                OtherNoType = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if (FValue != null && !FValue.equals("")) {
                OtherNo = FValue.trim();
            } else {
                OtherNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LAWageActivityLogSchema other = (LAWageActivityLogSchema) otherObject;
        return
                SerialNo == other.getSerialNo()
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && WageLogType.equals(other.getWageLogType())
                && WageNo.equals(other.getWageNo())
                && GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && PolNo.equals(other.getPolNo())
                && Describe.equals(other.getDescribe())
                && RiskCode.equals(other.getRiskCode())
                && OtherNoType.equals(other.getOtherNoType())
                && OtherNo.equals(other.getOtherNo())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("AgentCode")) {
            return 1;
        }
        if (strFieldName.equals("AgentGroup")) {
            return 2;
        }
        if (strFieldName.equals("WageLogType")) {
            return 3;
        }
        if (strFieldName.equals("WageNo")) {
            return 4;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 5;
        }
        if (strFieldName.equals("ContNo")) {
            return 6;
        }
        if (strFieldName.equals("GrpPolNo")) {
            return 7;
        }
        if (strFieldName.equals("PolNo")) {
            return 8;
        }
        if (strFieldName.equals("Describe")) {
            return 9;
        }
        if (strFieldName.equals("RiskCode")) {
            return 10;
        }
        if (strFieldName.equals("OtherNoType")) {
            return 11;
        }
        if (strFieldName.equals("OtherNo")) {
            return 12;
        }
        if (strFieldName.equals("ManageCom")) {
            return 13;
        }
        if (strFieldName.equals("Operator")) {
            return 14;
        }
        if (strFieldName.equals("MakeDate")) {
            return 15;
        }
        if (strFieldName.equals("MakeTime")) {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "AgentCode";
            break;
        case 2:
            strFieldName = "AgentGroup";
            break;
        case 3:
            strFieldName = "WageLogType";
            break;
        case 4:
            strFieldName = "WageNo";
            break;
        case 5:
            strFieldName = "GrpContNo";
            break;
        case 6:
            strFieldName = "ContNo";
            break;
        case 7:
            strFieldName = "GrpPolNo";
            break;
        case 8:
            strFieldName = "PolNo";
            break;
        case 9:
            strFieldName = "Describe";
            break;
        case 10:
            strFieldName = "RiskCode";
            break;
        case 11:
            strFieldName = "OtherNoType";
            break;
        case 12:
            strFieldName = "OtherNo";
            break;
        case 13:
            strFieldName = "ManageCom";
            break;
        case 14:
            strFieldName = "Operator";
            break;
        case 15:
            strFieldName = "MakeDate";
            break;
        case 16:
            strFieldName = "MakeTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WageLogType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WageNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Describe")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_INT;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
