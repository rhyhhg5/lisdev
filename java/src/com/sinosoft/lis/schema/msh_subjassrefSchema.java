/*
 * <p>ClassName: msh_subjassrefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.msh_subjassrefDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class msh_subjassrefSchema implements Schema
{
    // @Field
    /** 公司编码 */
    private String unitcode;
    /** 公司名称 */
    private String unitname;
    /** 年度 */
    private String year;
    /** 会计月份 */
    private String month;
    /** 科目编码 */
    private String subjcode;
    /** 科目名称 */
    private String subjname;
    /** 项目1类别编码 */
    private String bdcode1;
    /** 项目1类别名称 */
    private String bdname1;
    /** 项目2类别编码 */
    private String bdcode2;
    /** 项目2类别名称 */
    private String bdname2;
    /** 项目3类别编码 */
    private String bdcode3;
    /** 项目3类别名称 */
    private String bdname3;
    /** 项目4类别编码 */
    private String bdcode4;
    /** 项目4类别名称 */
    private String bdname4;
    /** 项目5类别编码 */
    private String bdcode5;
    /** 项目5类别名称 */
    private String bdname5;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public msh_subjassrefSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getunitcode()
    {
        if (unitcode != null && !unitcode.equals("") && SysConst.CHANGECHARSET == true)
        {
            unitcode = StrTool.unicodeToGBK(unitcode);
        }
        return unitcode;
    }

    public void setunitcode(String aunitcode)
    {
        unitcode = aunitcode;
    }

    public String getunitname()
    {
        if (unitname != null && !unitname.equals("") && SysConst.CHANGECHARSET == true)
        {
            unitname = StrTool.unicodeToGBK(unitname);
        }
        return unitname;
    }

    public void setunitname(String aunitname)
    {
        unitname = aunitname;
    }

    public String getyear()
    {
        if (year != null && !year.equals("") && SysConst.CHANGECHARSET == true)
        {
            year = StrTool.unicodeToGBK(year);
        }
        return year;
    }

    public void setyear(String ayear)
    {
        year = ayear;
    }

    public String getmonth()
    {
        if (month != null && !month.equals("") && SysConst.CHANGECHARSET == true)
        {
            month = StrTool.unicodeToGBK(month);
        }
        return month;
    }

    public void setmonth(String amonth)
    {
        month = amonth;
    }

    public String getsubjcode()
    {
        if (subjcode != null && !subjcode.equals("") && SysConst.CHANGECHARSET == true)
        {
            subjcode = StrTool.unicodeToGBK(subjcode);
        }
        return subjcode;
    }

    public void setsubjcode(String asubjcode)
    {
        subjcode = asubjcode;
    }

    public String getsubjname()
    {
        if (subjname != null && !subjname.equals("") && SysConst.CHANGECHARSET == true)
        {
            subjname = StrTool.unicodeToGBK(subjname);
        }
        return subjname;
    }

    public void setsubjname(String asubjname)
    {
        subjname = asubjname;
    }

    public String getbdcode1()
    {
        if (bdcode1 != null && !bdcode1.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdcode1 = StrTool.unicodeToGBK(bdcode1);
        }
        return bdcode1;
    }

    public void setbdcode1(String abdcode1)
    {
        bdcode1 = abdcode1;
    }

    public String getbdname1()
    {
        if (bdname1 != null && !bdname1.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdname1 = StrTool.unicodeToGBK(bdname1);
        }
        return bdname1;
    }

    public void setbdname1(String abdname1)
    {
        bdname1 = abdname1;
    }

    public String getbdcode2()
    {
        if (bdcode2 != null && !bdcode2.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdcode2 = StrTool.unicodeToGBK(bdcode2);
        }
        return bdcode2;
    }

    public void setbdcode2(String abdcode2)
    {
        bdcode2 = abdcode2;
    }

    public String getbdname2()
    {
        if (bdname2 != null && !bdname2.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdname2 = StrTool.unicodeToGBK(bdname2);
        }
        return bdname2;
    }

    public void setbdname2(String abdname2)
    {
        bdname2 = abdname2;
    }

    public String getbdcode3()
    {
        if (bdcode3 != null && !bdcode3.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdcode3 = StrTool.unicodeToGBK(bdcode3);
        }
        return bdcode3;
    }

    public void setbdcode3(String abdcode3)
    {
        bdcode3 = abdcode3;
    }

    public String getbdname3()
    {
        if (bdname3 != null && !bdname3.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdname3 = StrTool.unicodeToGBK(bdname3);
        }
        return bdname3;
    }

    public void setbdname3(String abdname3)
    {
        bdname3 = abdname3;
    }

    public String getbdcode4()
    {
        if (bdcode4 != null && !bdcode4.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdcode4 = StrTool.unicodeToGBK(bdcode4);
        }
        return bdcode4;
    }

    public void setbdcode4(String abdcode4)
    {
        bdcode4 = abdcode4;
    }

    public String getbdname4()
    {
        if (bdname4 != null && !bdname4.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdname4 = StrTool.unicodeToGBK(bdname4);
        }
        return bdname4;
    }

    public void setbdname4(String abdname4)
    {
        bdname4 = abdname4;
    }

    public String getbdcode5()
    {
        if (bdcode5 != null && !bdcode5.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdcode5 = StrTool.unicodeToGBK(bdcode5);
        }
        return bdcode5;
    }

    public void setbdcode5(String abdcode5)
    {
        bdcode5 = abdcode5;
    }

    public String getbdname5()
    {
        if (bdname5 != null && !bdname5.equals("") && SysConst.CHANGECHARSET == true)
        {
            bdname5 = StrTool.unicodeToGBK(bdname5);
        }
        return bdname5;
    }

    public void setbdname5(String abdname5)
    {
        bdname5 = abdname5;
    }

    /**
     * 使用另外一个 msh_subjassrefSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(msh_subjassrefSchema amsh_subjassrefSchema)
    {
        this.unitcode = amsh_subjassrefSchema.getunitcode();
        this.unitname = amsh_subjassrefSchema.getunitname();
        this.year = amsh_subjassrefSchema.getyear();
        this.month = amsh_subjassrefSchema.getmonth();
        this.subjcode = amsh_subjassrefSchema.getsubjcode();
        this.subjname = amsh_subjassrefSchema.getsubjname();
        this.bdcode1 = amsh_subjassrefSchema.getbdcode1();
        this.bdname1 = amsh_subjassrefSchema.getbdname1();
        this.bdcode2 = amsh_subjassrefSchema.getbdcode2();
        this.bdname2 = amsh_subjassrefSchema.getbdname2();
        this.bdcode3 = amsh_subjassrefSchema.getbdcode3();
        this.bdname3 = amsh_subjassrefSchema.getbdname3();
        this.bdcode4 = amsh_subjassrefSchema.getbdcode4();
        this.bdname4 = amsh_subjassrefSchema.getbdname4();
        this.bdcode5 = amsh_subjassrefSchema.getbdcode5();
        this.bdname5 = amsh_subjassrefSchema.getbdname5();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("unitcode") == null)
            {
                this.unitcode = null;
            }
            else
            {
                this.unitcode = rs.getString("unitcode").trim();
            }

            if (rs.getString("unitname") == null)
            {
                this.unitname = null;
            }
            else
            {
                this.unitname = rs.getString("unitname").trim();
            }

            if (rs.getString("year") == null)
            {
                this.year = null;
            }
            else
            {
                this.year = rs.getString("year").trim();
            }

            if (rs.getString("month") == null)
            {
                this.month = null;
            }
            else
            {
                this.month = rs.getString("month").trim();
            }

            if (rs.getString("subjcode") == null)
            {
                this.subjcode = null;
            }
            else
            {
                this.subjcode = rs.getString("subjcode").trim();
            }

            if (rs.getString("subjname") == null)
            {
                this.subjname = null;
            }
            else
            {
                this.subjname = rs.getString("subjname").trim();
            }

            if (rs.getString("bdcode1") == null)
            {
                this.bdcode1 = null;
            }
            else
            {
                this.bdcode1 = rs.getString("bdcode1").trim();
            }

            if (rs.getString("bdname1") == null)
            {
                this.bdname1 = null;
            }
            else
            {
                this.bdname1 = rs.getString("bdname1").trim();
            }

            if (rs.getString("bdcode2") == null)
            {
                this.bdcode2 = null;
            }
            else
            {
                this.bdcode2 = rs.getString("bdcode2").trim();
            }

            if (rs.getString("bdname2") == null)
            {
                this.bdname2 = null;
            }
            else
            {
                this.bdname2 = rs.getString("bdname2").trim();
            }

            if (rs.getString("bdcode3") == null)
            {
                this.bdcode3 = null;
            }
            else
            {
                this.bdcode3 = rs.getString("bdcode3").trim();
            }

            if (rs.getString("bdname3") == null)
            {
                this.bdname3 = null;
            }
            else
            {
                this.bdname3 = rs.getString("bdname3").trim();
            }

            if (rs.getString("bdcode4") == null)
            {
                this.bdcode4 = null;
            }
            else
            {
                this.bdcode4 = rs.getString("bdcode4").trim();
            }

            if (rs.getString("bdname4") == null)
            {
                this.bdname4 = null;
            }
            else
            {
                this.bdname4 = rs.getString("bdname4").trim();
            }

            if (rs.getString("bdcode5") == null)
            {
                this.bdcode5 = null;
            }
            else
            {
                this.bdcode5 = rs.getString("bdcode5").trim();
            }

            if (rs.getString("bdname5") == null)
            {
                this.bdname5 = null;
            }
            else
            {
                this.bdname5 = rs.getString("bdname5").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "msh_subjassrefSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public msh_subjassrefSchema getSchema()
    {
        msh_subjassrefSchema amsh_subjassrefSchema = new msh_subjassrefSchema();
        amsh_subjassrefSchema.setSchema(this);
        return amsh_subjassrefSchema;
    }

    public msh_subjassrefDB getDB()
    {
        msh_subjassrefDB aDBOper = new msh_subjassrefDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_subjassref描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(unitcode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(unitname)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(year)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(month)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(subjcode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(subjname)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdcode1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdname1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdcode2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdname2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdcode3)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdname3)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdcode4)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdname4)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdcode5)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(bdname5));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_subjassref>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            unitcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            unitname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            year = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                  SysConst.PACKAGESPILTER);
            month = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            subjcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            subjname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            bdcode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            bdname1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            bdcode2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            bdname2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            bdcode3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            bdname3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                     SysConst.PACKAGESPILTER);
            bdcode4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            bdname4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                     SysConst.PACKAGESPILTER);
            bdcode5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                     SysConst.PACKAGESPILTER);
            bdname5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "msh_subjassrefSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("unitcode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(unitcode));
        }
        if (FCode.equals("unitname"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(unitname));
        }
        if (FCode.equals("year"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(year));
        }
        if (FCode.equals("month"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(month));
        }
        if (FCode.equals("subjcode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(subjcode));
        }
        if (FCode.equals("subjname"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(subjname));
        }
        if (FCode.equals("bdcode1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdcode1));
        }
        if (FCode.equals("bdname1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdname1));
        }
        if (FCode.equals("bdcode2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdcode2));
        }
        if (FCode.equals("bdname2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdname2));
        }
        if (FCode.equals("bdcode3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdcode3));
        }
        if (FCode.equals("bdname3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdname3));
        }
        if (FCode.equals("bdcode4"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdcode4));
        }
        if (FCode.equals("bdname4"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdname4));
        }
        if (FCode.equals("bdcode5"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdcode5));
        }
        if (FCode.equals("bdname5"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(bdname5));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(unitcode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(unitname);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(year);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(month);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(subjcode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(subjname);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(bdcode1);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(bdname1);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(bdcode2);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(bdname2);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(bdcode3);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(bdname3);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(bdcode4);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(bdname4);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(bdcode5);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(bdname5);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("unitcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                unitcode = FValue.trim();
            }
            else
            {
                unitcode = null;
            }
        }
        if (FCode.equals("unitname"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                unitname = FValue.trim();
            }
            else
            {
                unitname = null;
            }
        }
        if (FCode.equals("year"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                year = FValue.trim();
            }
            else
            {
                year = null;
            }
        }
        if (FCode.equals("month"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                month = FValue.trim();
            }
            else
            {
                month = null;
            }
        }
        if (FCode.equals("subjcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                subjcode = FValue.trim();
            }
            else
            {
                subjcode = null;
            }
        }
        if (FCode.equals("subjname"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                subjname = FValue.trim();
            }
            else
            {
                subjname = null;
            }
        }
        if (FCode.equals("bdcode1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdcode1 = FValue.trim();
            }
            else
            {
                bdcode1 = null;
            }
        }
        if (FCode.equals("bdname1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdname1 = FValue.trim();
            }
            else
            {
                bdname1 = null;
            }
        }
        if (FCode.equals("bdcode2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdcode2 = FValue.trim();
            }
            else
            {
                bdcode2 = null;
            }
        }
        if (FCode.equals("bdname2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdname2 = FValue.trim();
            }
            else
            {
                bdname2 = null;
            }
        }
        if (FCode.equals("bdcode3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdcode3 = FValue.trim();
            }
            else
            {
                bdcode3 = null;
            }
        }
        if (FCode.equals("bdname3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdname3 = FValue.trim();
            }
            else
            {
                bdname3 = null;
            }
        }
        if (FCode.equals("bdcode4"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdcode4 = FValue.trim();
            }
            else
            {
                bdcode4 = null;
            }
        }
        if (FCode.equals("bdname4"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdname4 = FValue.trim();
            }
            else
            {
                bdname4 = null;
            }
        }
        if (FCode.equals("bdcode5"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdcode5 = FValue.trim();
            }
            else
            {
                bdcode5 = null;
            }
        }
        if (FCode.equals("bdname5"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                bdname5 = FValue.trim();
            }
            else
            {
                bdname5 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        msh_subjassrefSchema other = (msh_subjassrefSchema) otherObject;
        return
                unitcode.equals(other.getunitcode())
                && unitname.equals(other.getunitname())
                && year.equals(other.getyear())
                && month.equals(other.getmonth())
                && subjcode.equals(other.getsubjcode())
                && subjname.equals(other.getsubjname())
                && bdcode1.equals(other.getbdcode1())
                && bdname1.equals(other.getbdname1())
                && bdcode2.equals(other.getbdcode2())
                && bdname2.equals(other.getbdname2())
                && bdcode3.equals(other.getbdcode3())
                && bdname3.equals(other.getbdname3())
                && bdcode4.equals(other.getbdcode4())
                && bdname4.equals(other.getbdname4())
                && bdcode5.equals(other.getbdcode5())
                && bdname5.equals(other.getbdname5());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("unitcode"))
        {
            return 0;
        }
        if (strFieldName.equals("unitname"))
        {
            return 1;
        }
        if (strFieldName.equals("year"))
        {
            return 2;
        }
        if (strFieldName.equals("month"))
        {
            return 3;
        }
        if (strFieldName.equals("subjcode"))
        {
            return 4;
        }
        if (strFieldName.equals("subjname"))
        {
            return 5;
        }
        if (strFieldName.equals("bdcode1"))
        {
            return 6;
        }
        if (strFieldName.equals("bdname1"))
        {
            return 7;
        }
        if (strFieldName.equals("bdcode2"))
        {
            return 8;
        }
        if (strFieldName.equals("bdname2"))
        {
            return 9;
        }
        if (strFieldName.equals("bdcode3"))
        {
            return 10;
        }
        if (strFieldName.equals("bdname3"))
        {
            return 11;
        }
        if (strFieldName.equals("bdcode4"))
        {
            return 12;
        }
        if (strFieldName.equals("bdname4"))
        {
            return 13;
        }
        if (strFieldName.equals("bdcode5"))
        {
            return 14;
        }
        if (strFieldName.equals("bdname5"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "unitcode";
                break;
            case 1:
                strFieldName = "unitname";
                break;
            case 2:
                strFieldName = "year";
                break;
            case 3:
                strFieldName = "month";
                break;
            case 4:
                strFieldName = "subjcode";
                break;
            case 5:
                strFieldName = "subjname";
                break;
            case 6:
                strFieldName = "bdcode1";
                break;
            case 7:
                strFieldName = "bdname1";
                break;
            case 8:
                strFieldName = "bdcode2";
                break;
            case 9:
                strFieldName = "bdname2";
                break;
            case 10:
                strFieldName = "bdcode3";
                break;
            case 11:
                strFieldName = "bdname3";
                break;
            case 12:
                strFieldName = "bdcode4";
                break;
            case 13:
                strFieldName = "bdname4";
                break;
            case 14:
                strFieldName = "bdcode5";
                break;
            case 15:
                strFieldName = "bdname5";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("unitcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("unitname"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("year"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("month"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("subjcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("subjname"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdcode1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdname1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdcode2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdname2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdcode3"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdname3"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdcode4"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdname4"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdcode5"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bdname5"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
