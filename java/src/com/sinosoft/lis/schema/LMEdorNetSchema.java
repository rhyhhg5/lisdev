/*
 * <p>ClassName: LMEdorNetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMEdorNetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMEdorNetSchema implements Schema
{
    // @Field
    /** 保单起始年度 */
    private double StartYear;
    /** 保单终止年度 */
    private double EndYear;
    /** 折算比例 */
    private double NetRate;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMEdorNetSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "StartYear";
        pk[1] = "EndYear";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public double getStartYear()
    {
        return StartYear;
    }

    public void setStartYear(double aStartYear)
    {
        StartYear = aStartYear;
    }

    public void setStartYear(String aStartYear)
    {
        if (aStartYear != null && !aStartYear.equals(""))
        {
            Double tDouble = new Double(aStartYear);
            double d = tDouble.doubleValue();
            StartYear = d;
        }
    }

    public double getEndYear()
    {
        return EndYear;
    }

    public void setEndYear(double aEndYear)
    {
        EndYear = aEndYear;
    }

    public void setEndYear(String aEndYear)
    {
        if (aEndYear != null && !aEndYear.equals(""))
        {
            Double tDouble = new Double(aEndYear);
            double d = tDouble.doubleValue();
            EndYear = d;
        }
    }

    public double getNetRate()
    {
        return NetRate;
    }

    public void setNetRate(double aNetRate)
    {
        NetRate = aNetRate;
    }

    public void setNetRate(String aNetRate)
    {
        if (aNetRate != null && !aNetRate.equals(""))
        {
            Double tDouble = new Double(aNetRate);
            double d = tDouble.doubleValue();
            NetRate = d;
        }
    }


    /**
     * 使用另外一个 LMEdorNetSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMEdorNetSchema aLMEdorNetSchema)
    {
        this.StartYear = aLMEdorNetSchema.getStartYear();
        this.EndYear = aLMEdorNetSchema.getEndYear();
        this.NetRate = aLMEdorNetSchema.getNetRate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            this.StartYear = rs.getDouble("StartYear");
            this.EndYear = rs.getDouble("EndYear");
            this.NetRate = rs.getDouble("NetRate");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorNetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMEdorNetSchema getSchema()
    {
        LMEdorNetSchema aLMEdorNetSchema = new LMEdorNetSchema();
        aLMEdorNetSchema.setSchema(this);
        return aLMEdorNetSchema;
    }

    public LMEdorNetDB getDB()
    {
        LMEdorNetDB aDBOper = new LMEdorNetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorNet描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = ChgData.chgData(StartYear) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EndYear) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(NetRate);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorNet>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            StartYear = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 1, SysConst.PACKAGESPILTER))).doubleValue();
            EndYear = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).doubleValue();
            NetRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorNetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("StartYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartYear));
        }
        if (FCode.equals("EndYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndYear));
        }
        if (FCode.equals("NetRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NetRate));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = String.valueOf(StartYear);
                break;
            case 1:
                strFieldValue = String.valueOf(EndYear);
                break;
            case 2:
                strFieldValue = String.valueOf(NetRate);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("StartYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StartYear = d;
            }
        }
        if (FCode.equals("EndYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                EndYear = d;
            }
        }
        if (FCode.equals("NetRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                NetRate = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMEdorNetSchema other = (LMEdorNetSchema) otherObject;
        return
                StartYear == other.getStartYear()
                && EndYear == other.getEndYear()
                && NetRate == other.getNetRate();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("StartYear"))
        {
            return 0;
        }
        if (strFieldName.equals("EndYear"))
        {
            return 1;
        }
        if (strFieldName.equals("NetRate"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "StartYear";
                break;
            case 1:
                strFieldName = "EndYear";
                break;
            case 2:
                strFieldName = "NetRate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("StartYear"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EndYear"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("NetRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 1:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 2:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
