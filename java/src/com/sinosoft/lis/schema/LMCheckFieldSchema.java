/*
 * <p>ClassName: LMCheckFieldSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMCheckFieldSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 控制字段名称 */
    private String FieldName;
    /** 序号 */
    private String SerialNo;
    /** 算法 */
    private String CalCode;
    /** 页面位置 */
    private String PageLocation;
    /** 事件位置 */
    private String Location;
    /** 提示信息 */
    private String Msg;
    /** 提示标记 */
    private String MsgFlag;
    /** 修改变量标记 */
    private String UpdFlag;
    /** 有效结果标记 */
    private String ValiFlag;
    /** 返回值有效标记 */
    private String ReturnValiFlag;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMCheckFieldSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";
        pk[2] = "FieldName";
        pk[3] = "SerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getFieldName()
    {
        if (FieldName != null && !FieldName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FieldName = StrTool.unicodeToGBK(FieldName);
        }
        return FieldName;
    }

    public void setFieldName(String aFieldName)
    {
        FieldName = aFieldName;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getPageLocation()
    {
        if (PageLocation != null && !PageLocation.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PageLocation = StrTool.unicodeToGBK(PageLocation);
        }
        return PageLocation;
    }

    public void setPageLocation(String aPageLocation)
    {
        PageLocation = aPageLocation;
    }

    public String getLocation()
    {
        if (Location != null && !Location.equals("") && SysConst.CHANGECHARSET == true)
        {
            Location = StrTool.unicodeToGBK(Location);
        }
        return Location;
    }

    public void setLocation(String aLocation)
    {
        Location = aLocation;
    }

    public String getMsg()
    {
        if (Msg != null && !Msg.equals("") && SysConst.CHANGECHARSET == true)
        {
            Msg = StrTool.unicodeToGBK(Msg);
        }
        return Msg;
    }

    public void setMsg(String aMsg)
    {
        Msg = aMsg;
    }

    public String getMsgFlag()
    {
        if (MsgFlag != null && !MsgFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            MsgFlag = StrTool.unicodeToGBK(MsgFlag);
        }
        return MsgFlag;
    }

    public void setMsgFlag(String aMsgFlag)
    {
        MsgFlag = aMsgFlag;
    }

    public String getUpdFlag()
    {
        if (UpdFlag != null && !UpdFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            UpdFlag = StrTool.unicodeToGBK(UpdFlag);
        }
        return UpdFlag;
    }

    public void setUpdFlag(String aUpdFlag)
    {
        UpdFlag = aUpdFlag;
    }

    public String getValiFlag()
    {
        if (ValiFlag != null && !ValiFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            ValiFlag = StrTool.unicodeToGBK(ValiFlag);
        }
        return ValiFlag;
    }

    public void setValiFlag(String aValiFlag)
    {
        ValiFlag = aValiFlag;
    }

    public String getReturnValiFlag()
    {
        if (ReturnValiFlag != null && !ReturnValiFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReturnValiFlag = StrTool.unicodeToGBK(ReturnValiFlag);
        }
        return ReturnValiFlag;
    }

    public void setReturnValiFlag(String aReturnValiFlag)
    {
        ReturnValiFlag = aReturnValiFlag;
    }

    /**
     * 使用另外一个 LMCheckFieldSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMCheckFieldSchema aLMCheckFieldSchema)
    {
        this.RiskCode = aLMCheckFieldSchema.getRiskCode();
        this.RiskVer = aLMCheckFieldSchema.getRiskVer();
        this.FieldName = aLMCheckFieldSchema.getFieldName();
        this.SerialNo = aLMCheckFieldSchema.getSerialNo();
        this.CalCode = aLMCheckFieldSchema.getCalCode();
        this.PageLocation = aLMCheckFieldSchema.getPageLocation();
        this.Location = aLMCheckFieldSchema.getLocation();
        this.Msg = aLMCheckFieldSchema.getMsg();
        this.MsgFlag = aLMCheckFieldSchema.getMsgFlag();
        this.UpdFlag = aLMCheckFieldSchema.getUpdFlag();
        this.ValiFlag = aLMCheckFieldSchema.getValiFlag();
        this.ReturnValiFlag = aLMCheckFieldSchema.getReturnValiFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("FieldName") == null)
            {
                this.FieldName = null;
            }
            else
            {
                this.FieldName = rs.getString("FieldName").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("PageLocation") == null)
            {
                this.PageLocation = null;
            }
            else
            {
                this.PageLocation = rs.getString("PageLocation").trim();
            }

            if (rs.getString("Location") == null)
            {
                this.Location = null;
            }
            else
            {
                this.Location = rs.getString("Location").trim();
            }

            if (rs.getString("Msg") == null)
            {
                this.Msg = null;
            }
            else
            {
                this.Msg = rs.getString("Msg").trim();
            }

            if (rs.getString("MsgFlag") == null)
            {
                this.MsgFlag = null;
            }
            else
            {
                this.MsgFlag = rs.getString("MsgFlag").trim();
            }

            if (rs.getString("UpdFlag") == null)
            {
                this.UpdFlag = null;
            }
            else
            {
                this.UpdFlag = rs.getString("UpdFlag").trim();
            }

            if (rs.getString("ValiFlag") == null)
            {
                this.ValiFlag = null;
            }
            else
            {
                this.ValiFlag = rs.getString("ValiFlag").trim();
            }

            if (rs.getString("ReturnValiFlag") == null)
            {
                this.ReturnValiFlag = null;
            }
            else
            {
                this.ReturnValiFlag = rs.getString("ReturnValiFlag").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCheckFieldSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMCheckFieldSchema getSchema()
    {
        LMCheckFieldSchema aLMCheckFieldSchema = new LMCheckFieldSchema();
        aLMCheckFieldSchema.setSchema(this);
        return aLMCheckFieldSchema;
    }

    public LMCheckFieldDB getDB()
    {
        LMCheckFieldDB aDBOper = new LMCheckFieldDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCheckField描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FieldName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PageLocation)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Location)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Msg)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MsgFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpdFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ValiFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReturnValiFlag));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCheckField>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            FieldName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            PageLocation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            Location = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            Msg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                 SysConst.PACKAGESPILTER);
            MsgFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            UpdFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            ValiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ReturnValiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            12, SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCheckFieldSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("FieldName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FieldName));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("PageLocation"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PageLocation));
        }
        if (FCode.equals("Location"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Location));
        }
        if (FCode.equals("Msg"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Msg));
        }
        if (FCode.equals("MsgFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MsgFlag));
        }
        if (FCode.equals("UpdFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpdFlag));
        }
        if (FCode.equals("ValiFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ValiFlag));
        }
        if (FCode.equals("ReturnValiFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReturnValiFlag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(FieldName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PageLocation);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Location);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Msg);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MsgFlag);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(UpdFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ValiFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ReturnValiFlag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("FieldName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FieldName = FValue.trim();
            }
            else
            {
                FieldName = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("PageLocation"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PageLocation = FValue.trim();
            }
            else
            {
                PageLocation = null;
            }
        }
        if (FCode.equals("Location"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Location = FValue.trim();
            }
            else
            {
                Location = null;
            }
        }
        if (FCode.equals("Msg"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Msg = FValue.trim();
            }
            else
            {
                Msg = null;
            }
        }
        if (FCode.equals("MsgFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MsgFlag = FValue.trim();
            }
            else
            {
                MsgFlag = null;
            }
        }
        if (FCode.equals("UpdFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpdFlag = FValue.trim();
            }
            else
            {
                UpdFlag = null;
            }
        }
        if (FCode.equals("ValiFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ValiFlag = FValue.trim();
            }
            else
            {
                ValiFlag = null;
            }
        }
        if (FCode.equals("ReturnValiFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReturnValiFlag = FValue.trim();
            }
            else
            {
                ReturnValiFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMCheckFieldSchema other = (LMCheckFieldSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && FieldName.equals(other.getFieldName())
                && SerialNo.equals(other.getSerialNo())
                && CalCode.equals(other.getCalCode())
                && PageLocation.equals(other.getPageLocation())
                && Location.equals(other.getLocation())
                && Msg.equals(other.getMsg())
                && MsgFlag.equals(other.getMsgFlag())
                && UpdFlag.equals(other.getUpdFlag())
                && ValiFlag.equals(other.getValiFlag())
                && ReturnValiFlag.equals(other.getReturnValiFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("FieldName"))
        {
            return 2;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 3;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 4;
        }
        if (strFieldName.equals("PageLocation"))
        {
            return 5;
        }
        if (strFieldName.equals("Location"))
        {
            return 6;
        }
        if (strFieldName.equals("Msg"))
        {
            return 7;
        }
        if (strFieldName.equals("MsgFlag"))
        {
            return 8;
        }
        if (strFieldName.equals("UpdFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("ValiFlag"))
        {
            return 10;
        }
        if (strFieldName.equals("ReturnValiFlag"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "FieldName";
                break;
            case 3:
                strFieldName = "SerialNo";
                break;
            case 4:
                strFieldName = "CalCode";
                break;
            case 5:
                strFieldName = "PageLocation";
                break;
            case 6:
                strFieldName = "Location";
                break;
            case 7:
                strFieldName = "Msg";
                break;
            case 8:
                strFieldName = "MsgFlag";
                break;
            case 9:
                strFieldName = "UpdFlag";
                break;
            case 10:
                strFieldName = "ValiFlag";
                break;
            case 11:
                strFieldName = "ReturnValiFlag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FieldName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PageLocation"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Location"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Msg"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MsgFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UpdFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ValiFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReturnValiFlag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
