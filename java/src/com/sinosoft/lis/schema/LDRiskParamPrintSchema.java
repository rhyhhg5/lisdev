/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDRiskParamPrintDB;

/*
 * <p>ClassName: LDRiskParamPrintSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-27
 */
public class LDRiskParamPrintSchema implements Schema, Cloneable {
    // @Field
    /** 序号 */
    private int SeqNo;
    /** 险种编码 */
    private String RiskCode;
    /** 参数类型1 */
    private String ParamType1;
    /** 参数值1 */
    private String ParamValue1;
    /** 参数类型2 */
    private String ParamType2;
    /** 参数值2 */
    private String ParamValue2;
    /** 参数类型3 */
    private String ParamType3;
    /** 参数值3 */
    private String ParamValue3;
    /** 要素名称 */
    private String FactorName;
    /** 要素值 */
    private String FactorValue;
    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDRiskParamPrintSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SeqNo";
        pk[1] = "RiskCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDRiskParamPrintSchema cloned = (LDRiskParamPrintSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public int getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(int aSeqNo) {
        SeqNo = aSeqNo;
    }

    public void setSeqNo(String aSeqNo) {
        if (aSeqNo != null && !aSeqNo.equals("")) {
            Integer tInteger = new Integer(aSeqNo);
            int i = tInteger.intValue();
            SeqNo = i;
        }
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getParamType1() {
        return ParamType1;
    }

    public void setParamType1(String aParamType1) {
        ParamType1 = aParamType1;
    }

    public String getParamValue1() {
        return ParamValue1;
    }

    public void setParamValue1(String aParamValue1) {
        ParamValue1 = aParamValue1;
    }

    public String getParamType2() {
        return ParamType2;
    }

    public void setParamType2(String aParamType2) {
        ParamType2 = aParamType2;
    }

    public String getParamValue2() {
        return ParamValue2;
    }

    public void setParamValue2(String aParamValue2) {
        ParamValue2 = aParamValue2;
    }

    public String getParamType3() {
        return ParamType3;
    }

    public void setParamType3(String aParamType3) {
        ParamType3 = aParamType3;
    }

    public String getParamValue3() {
        return ParamValue3;
    }

    public void setParamValue3(String aParamValue3) {
        ParamValue3 = aParamValue3;
    }

    public String getFactorName() {
        return FactorName;
    }

    public void setFactorName(String aFactorName) {
        FactorName = aFactorName;
    }

    public String getFactorValue() {
        return FactorValue;
    }

    public void setFactorValue(String aFactorValue) {
        FactorValue = aFactorValue;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 LDRiskParamPrintSchema 对象给 Schema 赋值
     * @param: aLDRiskParamPrintSchema LDRiskParamPrintSchema
     **/
    public void setSchema(LDRiskParamPrintSchema aLDRiskParamPrintSchema) {
        this.SeqNo = aLDRiskParamPrintSchema.getSeqNo();
        this.RiskCode = aLDRiskParamPrintSchema.getRiskCode();
        this.ParamType1 = aLDRiskParamPrintSchema.getParamType1();
        this.ParamValue1 = aLDRiskParamPrintSchema.getParamValue1();
        this.ParamType2 = aLDRiskParamPrintSchema.getParamType2();
        this.ParamValue2 = aLDRiskParamPrintSchema.getParamValue2();
        this.ParamType3 = aLDRiskParamPrintSchema.getParamType3();
        this.ParamValue3 = aLDRiskParamPrintSchema.getParamValue3();
        this.FactorName = aLDRiskParamPrintSchema.getFactorName();
        this.FactorValue = aLDRiskParamPrintSchema.getFactorValue();
        this.Remark = aLDRiskParamPrintSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            this.SeqNo = rs.getInt("SeqNo");
            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("ParamType1") == null) {
                this.ParamType1 = null;
            } else {
                this.ParamType1 = rs.getString("ParamType1").trim();
            }

            if (rs.getString("ParamValue1") == null) {
                this.ParamValue1 = null;
            } else {
                this.ParamValue1 = rs.getString("ParamValue1").trim();
            }

            if (rs.getString("ParamType2") == null) {
                this.ParamType2 = null;
            } else {
                this.ParamType2 = rs.getString("ParamType2").trim();
            }

            if (rs.getString("ParamValue2") == null) {
                this.ParamValue2 = null;
            } else {
                this.ParamValue2 = rs.getString("ParamValue2").trim();
            }

            if (rs.getString("ParamType3") == null) {
                this.ParamType3 = null;
            } else {
                this.ParamType3 = rs.getString("ParamType3").trim();
            }

            if (rs.getString("ParamValue3") == null) {
                this.ParamValue3 = null;
            } else {
                this.ParamValue3 = rs.getString("ParamValue3").trim();
            }

            if (rs.getString("FactorName") == null) {
                this.FactorName = null;
            } else {
                this.FactorName = rs.getString("FactorName").trim();
            }

            if (rs.getString("FactorValue") == null) {
                this.FactorValue = null;
            } else {
                this.FactorValue = rs.getString("FactorValue").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDRiskParamPrint表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskParamPrintSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDRiskParamPrintSchema getSchema() {
        LDRiskParamPrintSchema aLDRiskParamPrintSchema = new
                LDRiskParamPrintSchema();
        aLDRiskParamPrintSchema.setSchema(this);
        return aLDRiskParamPrintSchema;
    }

    public LDRiskParamPrintDB getDB() {
        LDRiskParamPrintDB aDBOper = new LDRiskParamPrintDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskParamPrint描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(SeqNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ParamType1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ParamValue1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ParamType2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ParamValue2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ParamType3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ParamValue3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FactorName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FactorValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskParamPrint>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SeqNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    1, SysConst.PACKAGESPILTER))).intValue();
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            ParamType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            ParamValue1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            ParamType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            ParamValue2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            ParamType3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            ParamValue3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            FactorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            FactorValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskParamPrintSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeqNo));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("ParamType1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamType1));
        }
        if (FCode.equals("ParamValue1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamValue1));
        }
        if (FCode.equals("ParamType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamType2));
        }
        if (FCode.equals("ParamValue2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamValue2));
        }
        if (FCode.equals("ParamType3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamType3));
        }
        if (FCode.equals("ParamValue3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamValue3));
        }
        if (FCode.equals("FactorName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorName));
        }
        if (FCode.equals("FactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorValue));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = String.valueOf(SeqNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ParamType1);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ParamValue1);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ParamType2);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ParamValue2);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ParamType3);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ParamValue3);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(FactorName);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(FactorValue);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SeqNo")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SeqNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ParamType1")) {
            if (FValue != null && !FValue.equals("")) {
                ParamType1 = FValue.trim();
            } else {
                ParamType1 = null;
            }
        }
        if (FCode.equalsIgnoreCase("ParamValue1")) {
            if (FValue != null && !FValue.equals("")) {
                ParamValue1 = FValue.trim();
            } else {
                ParamValue1 = null;
            }
        }
        if (FCode.equalsIgnoreCase("ParamType2")) {
            if (FValue != null && !FValue.equals("")) {
                ParamType2 = FValue.trim();
            } else {
                ParamType2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("ParamValue2")) {
            if (FValue != null && !FValue.equals("")) {
                ParamValue2 = FValue.trim();
            } else {
                ParamValue2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("ParamType3")) {
            if (FValue != null && !FValue.equals("")) {
                ParamType3 = FValue.trim();
            } else {
                ParamType3 = null;
            }
        }
        if (FCode.equalsIgnoreCase("ParamValue3")) {
            if (FValue != null && !FValue.equals("")) {
                ParamValue3 = FValue.trim();
            } else {
                ParamValue3 = null;
            }
        }
        if (FCode.equalsIgnoreCase("FactorName")) {
            if (FValue != null && !FValue.equals("")) {
                FactorName = FValue.trim();
            } else {
                FactorName = null;
            }
        }
        if (FCode.equalsIgnoreCase("FactorValue")) {
            if (FValue != null && !FValue.equals("")) {
                FactorValue = FValue.trim();
            } else {
                FactorValue = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDRiskParamPrintSchema other = (LDRiskParamPrintSchema) otherObject;
        return
                SeqNo == other.getSeqNo()
                && RiskCode.equals(other.getRiskCode())
                && ParamType1.equals(other.getParamType1())
                && ParamValue1.equals(other.getParamValue1())
                && ParamType2.equals(other.getParamType2())
                && ParamValue2.equals(other.getParamValue2())
                && ParamType3.equals(other.getParamType3())
                && ParamValue3.equals(other.getParamValue3())
                && FactorName.equals(other.getFactorName())
                && FactorValue.equals(other.getFactorValue())
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SeqNo")) {
            return 0;
        }
        if (strFieldName.equals("RiskCode")) {
            return 1;
        }
        if (strFieldName.equals("ParamType1")) {
            return 2;
        }
        if (strFieldName.equals("ParamValue1")) {
            return 3;
        }
        if (strFieldName.equals("ParamType2")) {
            return 4;
        }
        if (strFieldName.equals("ParamValue2")) {
            return 5;
        }
        if (strFieldName.equals("ParamType3")) {
            return 6;
        }
        if (strFieldName.equals("ParamValue3")) {
            return 7;
        }
        if (strFieldName.equals("FactorName")) {
            return 8;
        }
        if (strFieldName.equals("FactorValue")) {
            return 9;
        }
        if (strFieldName.equals("Remark")) {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SeqNo";
            break;
        case 1:
            strFieldName = "RiskCode";
            break;
        case 2:
            strFieldName = "ParamType1";
            break;
        case 3:
            strFieldName = "ParamValue1";
            break;
        case 4:
            strFieldName = "ParamType2";
            break;
        case 5:
            strFieldName = "ParamValue2";
            break;
        case 6:
            strFieldName = "ParamType3";
            break;
        case 7:
            strFieldName = "ParamValue3";
            break;
        case 8:
            strFieldName = "FactorName";
            break;
        case 9:
            strFieldName = "FactorValue";
            break;
        case 10:
            strFieldName = "Remark";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SeqNo")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamType1")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamValue1")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamType2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamValue2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamType3")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamValue3")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorValue")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_INT;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
