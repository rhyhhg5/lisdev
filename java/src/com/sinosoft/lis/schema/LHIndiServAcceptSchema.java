/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHIndiServAcceptDB;

/*
 * <p>ClassName: LHIndiServAcceptSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构修改提交单-20060826
 * @CreateDate：2006-08-28
 */
public class LHIndiServAcceptSchema implements Schema, Cloneable {
    // @Field
    /** 服务受理编号 */
    private String ServAccepNo;
    /** 服务受理渠道 */
    private String ServAccepChannel;
    /** 服务受理时间 */
    private Date ServAceptDate;
    /** 客户号码 */
    private String CustomerNo;
    /** 保单号 */
    private String ContNo;
    /** 服务项目代码 */
    private String ServItemCode;
    /** 服务事件编号 */
    private String ServCaseCode;
    /** 任务模块类型 */
    private String TaskModelType;
    /** 模块类型流水号 */
    private String ModelTypeNo;
    /** 预约服务机构 */
    private String BespeakComID;
    /** 预约受理类型 */
    private String BespeakAcceptType;
    /** 预约时间要求 */
    private String BespeakDateRequest;
    /** 客户服务需求 */
    private String CusServRequest;
    /** 服务任务编号 */
    private String ServTaskCode;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHIndiServAcceptSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ServAccepNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHIndiServAcceptSchema cloned = (LHIndiServAcceptSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getServAccepNo() {
        return ServAccepNo;
    }

    public void setServAccepNo(String aServAccepNo) {
        ServAccepNo = aServAccepNo;
    }

    public String getServAccepChannel() {
        return ServAccepChannel;
    }

    public void setServAccepChannel(String aServAccepChannel) {
        ServAccepChannel = aServAccepChannel;
    }

    public String getServAceptDate() {
        if (ServAceptDate != null) {
            return fDate.getString(ServAceptDate);
        } else {
            return null;
        }
    }

    public void setServAceptDate(Date aServAceptDate) {
        ServAceptDate = aServAceptDate;
    }

    public void setServAceptDate(String aServAceptDate) {
        if (aServAceptDate != null && !aServAceptDate.equals("")) {
            ServAceptDate = fDate.getDate(aServAceptDate);
        } else {
            ServAceptDate = null;
        }
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getServItemCode() {
        return ServItemCode;
    }

    public void setServItemCode(String aServItemCode) {
        ServItemCode = aServItemCode;
    }

    public String getServCaseCode() {
        return ServCaseCode;
    }

    public void setServCaseCode(String aServCaseCode) {
        ServCaseCode = aServCaseCode;
    }

    public String getTaskModelType() {
        return TaskModelType;
    }

    public void setTaskModelType(String aTaskModelType) {
        TaskModelType = aTaskModelType;
    }

    public String getModelTypeNo() {
        return ModelTypeNo;
    }

    public void setModelTypeNo(String aModelTypeNo) {
        ModelTypeNo = aModelTypeNo;
    }

    public String getBespeakComID() {
        return BespeakComID;
    }

    public void setBespeakComID(String aBespeakComID) {
        BespeakComID = aBespeakComID;
    }

    public String getBespeakAcceptType() {
        return BespeakAcceptType;
    }

    public void setBespeakAcceptType(String aBespeakAcceptType) {
        BespeakAcceptType = aBespeakAcceptType;
    }

    public String getBespeakDateRequest() {
        return BespeakDateRequest;
    }

    public void setBespeakDateRequest(String aBespeakDateRequest) {
        BespeakDateRequest = aBespeakDateRequest;
    }

    public String getCusServRequest() {
        return CusServRequest;
    }

    public void setCusServRequest(String aCusServRequest) {
        CusServRequest = aCusServRequest;
    }

    public String getServTaskCode() {
        return ServTaskCode;
    }

    public void setServTaskCode(String aServTaskCode) {
        ServTaskCode = aServTaskCode;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHIndiServAcceptSchema 对象给 Schema 赋值
     * @param: aLHIndiServAcceptSchema LHIndiServAcceptSchema
     **/
    public void setSchema(LHIndiServAcceptSchema aLHIndiServAcceptSchema) {
        this.ServAccepNo = aLHIndiServAcceptSchema.getServAccepNo();
        this.ServAccepChannel = aLHIndiServAcceptSchema.getServAccepChannel();
        this.ServAceptDate = fDate.getDate(aLHIndiServAcceptSchema.
                                           getServAceptDate());
        this.CustomerNo = aLHIndiServAcceptSchema.getCustomerNo();
        this.ContNo = aLHIndiServAcceptSchema.getContNo();
        this.ServItemCode = aLHIndiServAcceptSchema.getServItemCode();
        this.ServCaseCode = aLHIndiServAcceptSchema.getServCaseCode();
        this.TaskModelType = aLHIndiServAcceptSchema.getTaskModelType();
        this.ModelTypeNo = aLHIndiServAcceptSchema.getModelTypeNo();
        this.BespeakComID = aLHIndiServAcceptSchema.getBespeakComID();
        this.BespeakAcceptType = aLHIndiServAcceptSchema.getBespeakAcceptType();
        this.BespeakDateRequest = aLHIndiServAcceptSchema.getBespeakDateRequest();
        this.CusServRequest = aLHIndiServAcceptSchema.getCusServRequest();
        this.ServTaskCode = aLHIndiServAcceptSchema.getServTaskCode();
        this.ManageCom = aLHIndiServAcceptSchema.getManageCom();
        this.Operator = aLHIndiServAcceptSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHIndiServAcceptSchema.getMakeDate());
        this.MakeTime = aLHIndiServAcceptSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHIndiServAcceptSchema.getModifyDate());
        this.ModifyTime = aLHIndiServAcceptSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ServAccepNo") == null) {
                this.ServAccepNo = null;
            } else {
                this.ServAccepNo = rs.getString("ServAccepNo").trim();
            }

            if (rs.getString("ServAccepChannel") == null) {
                this.ServAccepChannel = null;
            } else {
                this.ServAccepChannel = rs.getString("ServAccepChannel").trim();
            }

            this.ServAceptDate = rs.getDate("ServAceptDate");
            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ServItemCode") == null) {
                this.ServItemCode = null;
            } else {
                this.ServItemCode = rs.getString("ServItemCode").trim();
            }

            if (rs.getString("ServCaseCode") == null) {
                this.ServCaseCode = null;
            } else {
                this.ServCaseCode = rs.getString("ServCaseCode").trim();
            }

            if (rs.getString("TaskModelType") == null) {
                this.TaskModelType = null;
            } else {
                this.TaskModelType = rs.getString("TaskModelType").trim();
            }

            if (rs.getString("ModelTypeNo") == null) {
                this.ModelTypeNo = null;
            } else {
                this.ModelTypeNo = rs.getString("ModelTypeNo").trim();
            }

            if (rs.getString("BespeakComID") == null) {
                this.BespeakComID = null;
            } else {
                this.BespeakComID = rs.getString("BespeakComID").trim();
            }

            if (rs.getString("BespeakAcceptType") == null) {
                this.BespeakAcceptType = null;
            } else {
                this.BespeakAcceptType = rs.getString("BespeakAcceptType").trim();
            }

            if (rs.getString("BespeakDateRequest") == null) {
                this.BespeakDateRequest = null;
            } else {
                this.BespeakDateRequest = rs.getString("BespeakDateRequest").
                                          trim();
            }

            if (rs.getString("CusServRequest") == null) {
                this.CusServRequest = null;
            } else {
                this.CusServRequest = rs.getString("CusServRequest").trim();
            }

            if (rs.getString("ServTaskCode") == null) {
                this.ServTaskCode = null;
            } else {
                this.ServTaskCode = rs.getString("ServTaskCode").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHIndiServAccept表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHIndiServAcceptSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHIndiServAcceptSchema getSchema() {
        LHIndiServAcceptSchema aLHIndiServAcceptSchema = new
                LHIndiServAcceptSchema();
        aLHIndiServAcceptSchema.setSchema(this);
        return aLHIndiServAcceptSchema;
    }

    public LHIndiServAcceptDB getDB() {
        LHIndiServAcceptDB aDBOper = new LHIndiServAcceptDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHIndiServAccept描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ServAccepNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServAccepChannel));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ServAceptDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaskModelType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModelTypeNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeakComID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeakAcceptType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeakDateRequest));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CusServRequest));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHIndiServAccept>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ServAccepNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            ServAccepChannel = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              2, SysConst.PACKAGESPILTER);
            ServAceptDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            ServItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            ServCaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            TaskModelType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            ModelTypeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            BespeakComID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            BespeakAcceptType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               11, SysConst.PACKAGESPILTER);
            BespeakDateRequest = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                12, SysConst.PACKAGESPILTER);
            CusServRequest = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            13, SysConst.PACKAGESPILTER);
            ServTaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                          SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHIndiServAcceptSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ServAccepNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServAccepNo));
        }
        if (FCode.equals("ServAccepChannel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServAccepChannel));
        }
        if (FCode.equals("ServAceptDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getServAceptDate()));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("ServItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemCode));
        }
        if (FCode.equals("ServCaseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseCode));
        }
        if (FCode.equals("TaskModelType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskModelType));
        }
        if (FCode.equals("ModelTypeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModelTypeNo));
        }
        if (FCode.equals("BespeakComID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BespeakComID));
        }
        if (FCode.equals("BespeakAcceptType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BespeakAcceptType));
        }
        if (FCode.equals("BespeakDateRequest")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BespeakDateRequest));
        }
        if (FCode.equals("CusServRequest")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CusServRequest));
        }
        if (FCode.equals("ServTaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskCode));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ServAccepNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServAccepChannel);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getServAceptDate()));
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServItemCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ServCaseCode);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(TaskModelType);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ModelTypeNo);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(BespeakComID);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(BespeakAcceptType);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(BespeakDateRequest);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(CusServRequest);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ServTaskCode);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ServAccepNo")) {
            if (FValue != null && !FValue.equals("")) {
                ServAccepNo = FValue.trim();
            } else {
                ServAccepNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServAccepChannel")) {
            if (FValue != null && !FValue.equals("")) {
                ServAccepChannel = FValue.trim();
            } else {
                ServAccepChannel = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServAceptDate")) {
            if (FValue != null && !FValue.equals("")) {
                ServAceptDate = fDate.getDate(FValue);
            } else {
                ServAceptDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemCode = FValue.trim();
            } else {
                ServItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseCode = FValue.trim();
            } else {
                ServCaseCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("TaskModelType")) {
            if (FValue != null && !FValue.equals("")) {
                TaskModelType = FValue.trim();
            } else {
                TaskModelType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModelTypeNo")) {
            if (FValue != null && !FValue.equals("")) {
                ModelTypeNo = FValue.trim();
            } else {
                ModelTypeNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakComID")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakComID = FValue.trim();
            } else {
                BespeakComID = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakAcceptType")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakAcceptType = FValue.trim();
            } else {
                BespeakAcceptType = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakDateRequest")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakDateRequest = FValue.trim();
            } else {
                BespeakDateRequest = null;
            }
        }
        if (FCode.equalsIgnoreCase("CusServRequest")) {
            if (FValue != null && !FValue.equals("")) {
                CusServRequest = FValue.trim();
            } else {
                CusServRequest = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskCode = FValue.trim();
            } else {
                ServTaskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHIndiServAcceptSchema other = (LHIndiServAcceptSchema) otherObject;
        return
                ServAccepNo.equals(other.getServAccepNo())
                && ServAccepChannel.equals(other.getServAccepChannel())
                && fDate.getString(ServAceptDate).equals(other.getServAceptDate())
                && CustomerNo.equals(other.getCustomerNo())
                && ContNo.equals(other.getContNo())
                && ServItemCode.equals(other.getServItemCode())
                && ServCaseCode.equals(other.getServCaseCode())
                && TaskModelType.equals(other.getTaskModelType())
                && ModelTypeNo.equals(other.getModelTypeNo())
                && BespeakComID.equals(other.getBespeakComID())
                && BespeakAcceptType.equals(other.getBespeakAcceptType())
                && BespeakDateRequest.equals(other.getBespeakDateRequest())
                && CusServRequest.equals(other.getCusServRequest())
                && ServTaskCode.equals(other.getServTaskCode())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ServAccepNo")) {
            return 0;
        }
        if (strFieldName.equals("ServAccepChannel")) {
            return 1;
        }
        if (strFieldName.equals("ServAceptDate")) {
            return 2;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 3;
        }
        if (strFieldName.equals("ContNo")) {
            return 4;
        }
        if (strFieldName.equals("ServItemCode")) {
            return 5;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return 6;
        }
        if (strFieldName.equals("TaskModelType")) {
            return 7;
        }
        if (strFieldName.equals("ModelTypeNo")) {
            return 8;
        }
        if (strFieldName.equals("BespeakComID")) {
            return 9;
        }
        if (strFieldName.equals("BespeakAcceptType")) {
            return 10;
        }
        if (strFieldName.equals("BespeakDateRequest")) {
            return 11;
        }
        if (strFieldName.equals("CusServRequest")) {
            return 12;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return 13;
        }
        if (strFieldName.equals("ManageCom")) {
            return 14;
        }
        if (strFieldName.equals("Operator")) {
            return 15;
        }
        if (strFieldName.equals("MakeDate")) {
            return 16;
        }
        if (strFieldName.equals("MakeTime")) {
            return 17;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 18;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ServAccepNo";
            break;
        case 1:
            strFieldName = "ServAccepChannel";
            break;
        case 2:
            strFieldName = "ServAceptDate";
            break;
        case 3:
            strFieldName = "CustomerNo";
            break;
        case 4:
            strFieldName = "ContNo";
            break;
        case 5:
            strFieldName = "ServItemCode";
            break;
        case 6:
            strFieldName = "ServCaseCode";
            break;
        case 7:
            strFieldName = "TaskModelType";
            break;
        case 8:
            strFieldName = "ModelTypeNo";
            break;
        case 9:
            strFieldName = "BespeakComID";
            break;
        case 10:
            strFieldName = "BespeakAcceptType";
            break;
        case 11:
            strFieldName = "BespeakDateRequest";
            break;
        case 12:
            strFieldName = "CusServRequest";
            break;
        case 13:
            strFieldName = "ServTaskCode";
            break;
        case 14:
            strFieldName = "ManageCom";
            break;
        case 15:
            strFieldName = "Operator";
            break;
        case 16:
            strFieldName = "MakeDate";
            break;
        case 17:
            strFieldName = "MakeTime";
            break;
        case 18:
            strFieldName = "ModifyDate";
            break;
        case 19:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ServAccepNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServAccepChannel")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServAceptDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaskModelType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModelTypeNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeakComID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeakAcceptType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeakDateRequest")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CusServRequest")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
