/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCUnionPayDetailDB;

/*
 * <p>ClassName: LCUnionPayDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 银联数据存储表
 * @CreateDate：2016-07-14
 */
public class LCUnionPayDetailSchema implements Schema, Cloneable
{
	// @Field
	/** 文件编号 */
	private String FileNo;
	/** 投保编号 */
	private String BatchNo;
	/** 投保类型 */
	private String ContType;
	/** 投保人姓名 */
	private String AppntName;
	/** 投保人证件类型 */
	private String AppntIDType;
	/** 投保人证件号码 */
	private String AppntIDNo;
	/** 投保人出生日期 */
	private String AppntBirthday;
	/** 投保人性别 */
	private String AppntSex;
	/** 投保人卡号 */
	private String AppntBankAccNo;
	/** 投保人手机号 */
	private String AppntPhone;
	/** 投保人与持卡人关系 */
	private String RelationToAppnt;
	/** 被保人姓名 */
	private String InsuredName;
	/** 被保人证件类型 */
	private String InsuredIDType;
	/** 被保人证件号码 */
	private String InsuredIDNo;
	/** 被保人出生日期 */
	private String InsuredBirthday;
	/** 被保人性别 */
	private String InsuredSex;
	/** 保险等级 */
	private String CardLevel;
	/** 保额 */
	private String Amnt;
	/** 保单生效时间 */
	private String CValiDate;
	/** 保单失效时间 */
	private String CInValiDate;
	/** 保单编号 */
	private String CardNo;
	/** 套餐编码 */
	private String WrapCode;
	/** 单证编码 */
	private String CertifyCode;
	/** 投保结果 */
	private String Result;
	/** 失败原因码 */
	private String FalseCode;
	/** 状态 */
	private String State;
	/** 失败原因 */
	private String FalseReason;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 31;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCUnionPayDetailSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCUnionPayDetailSchema cloned = (LCUnionPayDetailSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getFileNo()
	{
		return FileNo;
	}
	public void setFileNo(String aFileNo)
	{
		FileNo = aFileNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getContType()
	{
		return ContType;
	}
	public void setContType(String aContType)
	{
		ContType = aContType;
	}
	public String getAppntName()
	{
		return AppntName;
	}
	public void setAppntName(String aAppntName)
	{
		AppntName = aAppntName;
	}
	public String getAppntIDType()
	{
		return AppntIDType;
	}
	public void setAppntIDType(String aAppntIDType)
	{
		AppntIDType = aAppntIDType;
	}
	public String getAppntIDNo()
	{
		return AppntIDNo;
	}
	public void setAppntIDNo(String aAppntIDNo)
	{
		AppntIDNo = aAppntIDNo;
	}
	public String getAppntBirthday()
	{
		return AppntBirthday;
	}
	public void setAppntBirthday(String aAppntBirthday)
	{
		AppntBirthday = aAppntBirthday;
	}
	public String getAppntSex()
	{
		return AppntSex;
	}
	public void setAppntSex(String aAppntSex)
	{
		AppntSex = aAppntSex;
	}
	public String getAppntBankAccNo()
	{
		return AppntBankAccNo;
	}
	public void setAppntBankAccNo(String aAppntBankAccNo)
	{
		AppntBankAccNo = aAppntBankAccNo;
	}
	public String getAppntPhone()
	{
		return AppntPhone;
	}
	public void setAppntPhone(String aAppntPhone)
	{
		AppntPhone = aAppntPhone;
	}
	public String getRelationToAppnt()
	{
		return RelationToAppnt;
	}
	public void setRelationToAppnt(String aRelationToAppnt)
	{
		RelationToAppnt = aRelationToAppnt;
	}
	public String getInsuredName()
	{
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
		InsuredName = aInsuredName;
	}
	public String getInsuredIDType()
	{
		return InsuredIDType;
	}
	public void setInsuredIDType(String aInsuredIDType)
	{
		InsuredIDType = aInsuredIDType;
	}
	public String getInsuredIDNo()
	{
		return InsuredIDNo;
	}
	public void setInsuredIDNo(String aInsuredIDNo)
	{
		InsuredIDNo = aInsuredIDNo;
	}
	public String getInsuredBirthday()
	{
		return InsuredBirthday;
	}
	public void setInsuredBirthday(String aInsuredBirthday)
	{
		InsuredBirthday = aInsuredBirthday;
	}
	public String getInsuredSex()
	{
		return InsuredSex;
	}
	public void setInsuredSex(String aInsuredSex)
	{
		InsuredSex = aInsuredSex;
	}
	public String getCardLevel()
	{
		return CardLevel;
	}
	public void setCardLevel(String aCardLevel)
	{
		CardLevel = aCardLevel;
	}
	public String getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(String aAmnt)
	{
		Amnt = aAmnt;
	}
	public String getCValiDate()
	{
		return CValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public String getCInValiDate()
	{
		return CInValiDate;
	}
	public void setCInValiDate(String aCInValiDate)
	{
		CInValiDate = aCInValiDate;
	}
	public String getCardNo()
	{
		return CardNo;
	}
	public void setCardNo(String aCardNo)
	{
		CardNo = aCardNo;
	}
	public String getWrapCode()
	{
		return WrapCode;
	}
	public void setWrapCode(String aWrapCode)
	{
		WrapCode = aWrapCode;
	}
	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
		CertifyCode = aCertifyCode;
	}
	public String getResult()
	{
		return Result;
	}
	public void setResult(String aResult)
	{
		Result = aResult;
	}
	public String getFalseCode()
	{
		return FalseCode;
	}
	public void setFalseCode(String aFalseCode)
	{
		FalseCode = aFalseCode;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getFalseReason()
	{
		return FalseReason;
	}
	public void setFalseReason(String aFalseReason)
	{
		FalseReason = aFalseReason;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCUnionPayDetailSchema 对象给 Schema 赋值
	* @param: aLCUnionPayDetailSchema LCUnionPayDetailSchema
	**/
	public void setSchema(LCUnionPayDetailSchema aLCUnionPayDetailSchema)
	{
		this.FileNo = aLCUnionPayDetailSchema.getFileNo();
		this.BatchNo = aLCUnionPayDetailSchema.getBatchNo();
		this.ContType = aLCUnionPayDetailSchema.getContType();
		this.AppntName = aLCUnionPayDetailSchema.getAppntName();
		this.AppntIDType = aLCUnionPayDetailSchema.getAppntIDType();
		this.AppntIDNo = aLCUnionPayDetailSchema.getAppntIDNo();
		this.AppntBirthday = aLCUnionPayDetailSchema.getAppntBirthday();
		this.AppntSex = aLCUnionPayDetailSchema.getAppntSex();
		this.AppntBankAccNo = aLCUnionPayDetailSchema.getAppntBankAccNo();
		this.AppntPhone = aLCUnionPayDetailSchema.getAppntPhone();
		this.RelationToAppnt = aLCUnionPayDetailSchema.getRelationToAppnt();
		this.InsuredName = aLCUnionPayDetailSchema.getInsuredName();
		this.InsuredIDType = aLCUnionPayDetailSchema.getInsuredIDType();
		this.InsuredIDNo = aLCUnionPayDetailSchema.getInsuredIDNo();
		this.InsuredBirthday = aLCUnionPayDetailSchema.getInsuredBirthday();
		this.InsuredSex = aLCUnionPayDetailSchema.getInsuredSex();
		this.CardLevel = aLCUnionPayDetailSchema.getCardLevel();
		this.Amnt = aLCUnionPayDetailSchema.getAmnt();
		this.CValiDate = aLCUnionPayDetailSchema.getCValiDate();
		this.CInValiDate = aLCUnionPayDetailSchema.getCInValiDate();
		this.CardNo = aLCUnionPayDetailSchema.getCardNo();
		this.WrapCode = aLCUnionPayDetailSchema.getWrapCode();
		this.CertifyCode = aLCUnionPayDetailSchema.getCertifyCode();
		this.Result = aLCUnionPayDetailSchema.getResult();
		this.FalseCode = aLCUnionPayDetailSchema.getFalseCode();
		this.State = aLCUnionPayDetailSchema.getState();
		this.FalseReason = aLCUnionPayDetailSchema.getFalseReason();
		this.MakeDate = fDate.getDate( aLCUnionPayDetailSchema.getMakeDate());
		this.MakeTime = aLCUnionPayDetailSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCUnionPayDetailSchema.getModifyDate());
		this.ModifyTime = aLCUnionPayDetailSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("FileNo") == null )
				this.FileNo = null;
			else
				this.FileNo = rs.getString("FileNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("ContType") == null )
				this.ContType = null;
			else
				this.ContType = rs.getString("ContType").trim();

			if( rs.getString("AppntName") == null )
				this.AppntName = null;
			else
				this.AppntName = rs.getString("AppntName").trim();

			if( rs.getString("AppntIDType") == null )
				this.AppntIDType = null;
			else
				this.AppntIDType = rs.getString("AppntIDType").trim();

			if( rs.getString("AppntIDNo") == null )
				this.AppntIDNo = null;
			else
				this.AppntIDNo = rs.getString("AppntIDNo").trim();

			if( rs.getString("AppntBirthday") == null )
				this.AppntBirthday = null;
			else
				this.AppntBirthday = rs.getString("AppntBirthday").trim();

			if( rs.getString("AppntSex") == null )
				this.AppntSex = null;
			else
				this.AppntSex = rs.getString("AppntSex").trim();

			if( rs.getString("AppntBankAccNo") == null )
				this.AppntBankAccNo = null;
			else
				this.AppntBankAccNo = rs.getString("AppntBankAccNo").trim();

			if( rs.getString("AppntPhone") == null )
				this.AppntPhone = null;
			else
				this.AppntPhone = rs.getString("AppntPhone").trim();

			if( rs.getString("RelationToAppnt") == null )
				this.RelationToAppnt = null;
			else
				this.RelationToAppnt = rs.getString("RelationToAppnt").trim();

			if( rs.getString("InsuredName") == null )
				this.InsuredName = null;
			else
				this.InsuredName = rs.getString("InsuredName").trim();

			if( rs.getString("InsuredIDType") == null )
				this.InsuredIDType = null;
			else
				this.InsuredIDType = rs.getString("InsuredIDType").trim();

			if( rs.getString("InsuredIDNo") == null )
				this.InsuredIDNo = null;
			else
				this.InsuredIDNo = rs.getString("InsuredIDNo").trim();

			if( rs.getString("InsuredBirthday") == null )
				this.InsuredBirthday = null;
			else
				this.InsuredBirthday = rs.getString("InsuredBirthday").trim();

			if( rs.getString("InsuredSex") == null )
				this.InsuredSex = null;
			else
				this.InsuredSex = rs.getString("InsuredSex").trim();

			if( rs.getString("CardLevel") == null )
				this.CardLevel = null;
			else
				this.CardLevel = rs.getString("CardLevel").trim();

			if( rs.getString("Amnt") == null )
				this.Amnt = null;
			else
				this.Amnt = rs.getString("Amnt").trim();

			if( rs.getString("CValiDate") == null )
				this.CValiDate = null;
			else
				this.CValiDate = rs.getString("CValiDate").trim();

			if( rs.getString("CInValiDate") == null )
				this.CInValiDate = null;
			else
				this.CInValiDate = rs.getString("CInValiDate").trim();

			if( rs.getString("CardNo") == null )
				this.CardNo = null;
			else
				this.CardNo = rs.getString("CardNo").trim();

			if( rs.getString("WrapCode") == null )
				this.WrapCode = null;
			else
				this.WrapCode = rs.getString("WrapCode").trim();

			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			if( rs.getString("Result") == null )
				this.Result = null;
			else
				this.Result = rs.getString("Result").trim();

			if( rs.getString("FalseCode") == null )
				this.FalseCode = null;
			else
				this.FalseCode = rs.getString("FalseCode").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("FalseReason") == null )
				this.FalseReason = null;
			else
				this.FalseReason = rs.getString("FalseReason").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCUnionPayDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCUnionPayDetailSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCUnionPayDetailSchema getSchema()
	{
		LCUnionPayDetailSchema aLCUnionPayDetailSchema = new LCUnionPayDetailSchema();
		aLCUnionPayDetailSchema.setSchema(this);
		return aLCUnionPayDetailSchema;
	}

	public LCUnionPayDetailDB getDB()
	{
		LCUnionPayDetailDB aDBOper = new LCUnionPayDetailDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCUnionPayDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(FileNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntBirthday)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationToAppnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredBirthday)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardLevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Amnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CInValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Result)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FalseCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FalseReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCUnionPayDetail>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			FileNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AppntIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AppntIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AppntBirthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AppntSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AppntBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AppntPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RelationToAppnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			InsuredIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			InsuredIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			InsuredBirthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CardLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Amnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			CValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			CInValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			CardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			WrapCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Result = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			FalseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			FalseReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCUnionPayDetailSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("FileNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ContType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
		}
		if (FCode.equals("AppntName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
		}
		if (FCode.equals("AppntIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDType));
		}
		if (FCode.equals("AppntIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDNo));
		}
		if (FCode.equals("AppntBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntBirthday));
		}
		if (FCode.equals("AppntSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
		}
		if (FCode.equals("AppntBankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntBankAccNo));
		}
		if (FCode.equals("AppntPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntPhone));
		}
		if (FCode.equals("RelationToAppnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
		}
		if (FCode.equals("InsuredName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
		}
		if (FCode.equals("InsuredIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
		}
		if (FCode.equals("InsuredIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNo));
		}
		if (FCode.equals("InsuredBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBirthday));
		}
		if (FCode.equals("InsuredSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
		}
		if (FCode.equals("CardLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardLevel));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
		}
		if (FCode.equals("CInValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CInValiDate));
		}
		if (FCode.equals("CardNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
		}
		if (FCode.equals("WrapCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapCode));
		}
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("Result"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Result));
		}
		if (FCode.equals("FalseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FalseCode));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("FalseReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FalseReason));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(FileNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ContType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AppntName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AppntIDType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AppntIDNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AppntBirthday);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AppntSex);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AppntBankAccNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AppntPhone);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(RelationToAppnt);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(InsuredName);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(InsuredIDType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(InsuredIDNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(InsuredBirthday);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(InsuredSex);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CardLevel);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Amnt);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(CValiDate);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(CInValiDate);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(CardNo);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(WrapCode);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Result);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(FalseCode);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(FalseReason);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("FileNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileNo = FValue.trim();
			}
			else
				FileNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("ContType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContType = FValue.trim();
			}
			else
				ContType = null;
		}
		if (FCode.equalsIgnoreCase("AppntName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntName = FValue.trim();
			}
			else
				AppntName = null;
		}
		if (FCode.equalsIgnoreCase("AppntIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIDType = FValue.trim();
			}
			else
				AppntIDType = null;
		}
		if (FCode.equalsIgnoreCase("AppntIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntIDNo = FValue.trim();
			}
			else
				AppntIDNo = null;
		}
		if (FCode.equalsIgnoreCase("AppntBirthday"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntBirthday = FValue.trim();
			}
			else
				AppntBirthday = null;
		}
		if (FCode.equalsIgnoreCase("AppntSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntSex = FValue.trim();
			}
			else
				AppntSex = null;
		}
		if (FCode.equalsIgnoreCase("AppntBankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntBankAccNo = FValue.trim();
			}
			else
				AppntBankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AppntPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntPhone = FValue.trim();
			}
			else
				AppntPhone = null;
		}
		if (FCode.equalsIgnoreCase("RelationToAppnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToAppnt = FValue.trim();
			}
			else
				RelationToAppnt = null;
		}
		if (FCode.equalsIgnoreCase("InsuredName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredName = FValue.trim();
			}
			else
				InsuredName = null;
		}
		if (FCode.equalsIgnoreCase("InsuredIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredIDType = FValue.trim();
			}
			else
				InsuredIDType = null;
		}
		if (FCode.equalsIgnoreCase("InsuredIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredIDNo = FValue.trim();
			}
			else
				InsuredIDNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredBirthday"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredBirthday = FValue.trim();
			}
			else
				InsuredBirthday = null;
		}
		if (FCode.equalsIgnoreCase("InsuredSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredSex = FValue.trim();
			}
			else
				InsuredSex = null;
		}
		if (FCode.equalsIgnoreCase("CardLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardLevel = FValue.trim();
			}
			else
				CardLevel = null;
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Amnt = FValue.trim();
			}
			else
				Amnt = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CValiDate = FValue.trim();
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("CInValiDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CInValiDate = FValue.trim();
			}
			else
				CInValiDate = null;
		}
		if (FCode.equalsIgnoreCase("CardNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardNo = FValue.trim();
			}
			else
				CardNo = null;
		}
		if (FCode.equalsIgnoreCase("WrapCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapCode = FValue.trim();
			}
			else
				WrapCode = null;
		}
		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("Result"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Result = FValue.trim();
			}
			else
				Result = null;
		}
		if (FCode.equalsIgnoreCase("FalseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FalseCode = FValue.trim();
			}
			else
				FalseCode = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("FalseReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FalseReason = FValue.trim();
			}
			else
				FalseReason = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCUnionPayDetailSchema other = (LCUnionPayDetailSchema)otherObject;
		return
			(FileNo == null ? other.getFileNo() == null : FileNo.equals(other.getFileNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (ContType == null ? other.getContType() == null : ContType.equals(other.getContType()))
			&& (AppntName == null ? other.getAppntName() == null : AppntName.equals(other.getAppntName()))
			&& (AppntIDType == null ? other.getAppntIDType() == null : AppntIDType.equals(other.getAppntIDType()))
			&& (AppntIDNo == null ? other.getAppntIDNo() == null : AppntIDNo.equals(other.getAppntIDNo()))
			&& (AppntBirthday == null ? other.getAppntBirthday() == null : AppntBirthday.equals(other.getAppntBirthday()))
			&& (AppntSex == null ? other.getAppntSex() == null : AppntSex.equals(other.getAppntSex()))
			&& (AppntBankAccNo == null ? other.getAppntBankAccNo() == null : AppntBankAccNo.equals(other.getAppntBankAccNo()))
			&& (AppntPhone == null ? other.getAppntPhone() == null : AppntPhone.equals(other.getAppntPhone()))
			&& (RelationToAppnt == null ? other.getRelationToAppnt() == null : RelationToAppnt.equals(other.getRelationToAppnt()))
			&& (InsuredName == null ? other.getInsuredName() == null : InsuredName.equals(other.getInsuredName()))
			&& (InsuredIDType == null ? other.getInsuredIDType() == null : InsuredIDType.equals(other.getInsuredIDType()))
			&& (InsuredIDNo == null ? other.getInsuredIDNo() == null : InsuredIDNo.equals(other.getInsuredIDNo()))
			&& (InsuredBirthday == null ? other.getInsuredBirthday() == null : InsuredBirthday.equals(other.getInsuredBirthday()))
			&& (InsuredSex == null ? other.getInsuredSex() == null : InsuredSex.equals(other.getInsuredSex()))
			&& (CardLevel == null ? other.getCardLevel() == null : CardLevel.equals(other.getCardLevel()))
			&& (Amnt == null ? other.getAmnt() == null : Amnt.equals(other.getAmnt()))
			&& (CValiDate == null ? other.getCValiDate() == null : CValiDate.equals(other.getCValiDate()))
			&& (CInValiDate == null ? other.getCInValiDate() == null : CInValiDate.equals(other.getCInValiDate()))
			&& (CardNo == null ? other.getCardNo() == null : CardNo.equals(other.getCardNo()))
			&& (WrapCode == null ? other.getWrapCode() == null : WrapCode.equals(other.getWrapCode()))
			&& (CertifyCode == null ? other.getCertifyCode() == null : CertifyCode.equals(other.getCertifyCode()))
			&& (Result == null ? other.getResult() == null : Result.equals(other.getResult()))
			&& (FalseCode == null ? other.getFalseCode() == null : FalseCode.equals(other.getFalseCode()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (FalseReason == null ? other.getFalseReason() == null : FalseReason.equals(other.getFalseReason()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("FileNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("ContType") ) {
			return 2;
		}
		if( strFieldName.equals("AppntName") ) {
			return 3;
		}
		if( strFieldName.equals("AppntIDType") ) {
			return 4;
		}
		if( strFieldName.equals("AppntIDNo") ) {
			return 5;
		}
		if( strFieldName.equals("AppntBirthday") ) {
			return 6;
		}
		if( strFieldName.equals("AppntSex") ) {
			return 7;
		}
		if( strFieldName.equals("AppntBankAccNo") ) {
			return 8;
		}
		if( strFieldName.equals("AppntPhone") ) {
			return 9;
		}
		if( strFieldName.equals("RelationToAppnt") ) {
			return 10;
		}
		if( strFieldName.equals("InsuredName") ) {
			return 11;
		}
		if( strFieldName.equals("InsuredIDType") ) {
			return 12;
		}
		if( strFieldName.equals("InsuredIDNo") ) {
			return 13;
		}
		if( strFieldName.equals("InsuredBirthday") ) {
			return 14;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return 15;
		}
		if( strFieldName.equals("CardLevel") ) {
			return 16;
		}
		if( strFieldName.equals("Amnt") ) {
			return 17;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 18;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return 19;
		}
		if( strFieldName.equals("CardNo") ) {
			return 20;
		}
		if( strFieldName.equals("WrapCode") ) {
			return 21;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return 22;
		}
		if( strFieldName.equals("Result") ) {
			return 23;
		}
		if( strFieldName.equals("FalseCode") ) {
			return 24;
		}
		if( strFieldName.equals("State") ) {
			return 25;
		}
		if( strFieldName.equals("FalseReason") ) {
			return 26;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 27;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 28;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 29;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 30;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "FileNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "ContType";
				break;
			case 3:
				strFieldName = "AppntName";
				break;
			case 4:
				strFieldName = "AppntIDType";
				break;
			case 5:
				strFieldName = "AppntIDNo";
				break;
			case 6:
				strFieldName = "AppntBirthday";
				break;
			case 7:
				strFieldName = "AppntSex";
				break;
			case 8:
				strFieldName = "AppntBankAccNo";
				break;
			case 9:
				strFieldName = "AppntPhone";
				break;
			case 10:
				strFieldName = "RelationToAppnt";
				break;
			case 11:
				strFieldName = "InsuredName";
				break;
			case 12:
				strFieldName = "InsuredIDType";
				break;
			case 13:
				strFieldName = "InsuredIDNo";
				break;
			case 14:
				strFieldName = "InsuredBirthday";
				break;
			case 15:
				strFieldName = "InsuredSex";
				break;
			case 16:
				strFieldName = "CardLevel";
				break;
			case 17:
				strFieldName = "Amnt";
				break;
			case 18:
				strFieldName = "CValiDate";
				break;
			case 19:
				strFieldName = "CInValiDate";
				break;
			case 20:
				strFieldName = "CardNo";
				break;
			case 21:
				strFieldName = "WrapCode";
				break;
			case 22:
				strFieldName = "CertifyCode";
				break;
			case 23:
				strFieldName = "Result";
				break;
			case 24:
				strFieldName = "FalseCode";
				break;
			case 25:
				strFieldName = "State";
				break;
			case 26:
				strFieldName = "FalseReason";
				break;
			case 27:
				strFieldName = "MakeDate";
				break;
			case 28:
				strFieldName = "MakeTime";
				break;
			case 29:
				strFieldName = "ModifyDate";
				break;
			case 30:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("FileNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntBirthday") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntBankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationToAppnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredBirthday") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Result") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FalseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FalseReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
