/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAtrainerIndexVsCommDB;

/*
 * <p>ClassName: LAtrainerIndexVsCommSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险培训组训信息表
 * @CreateDate：2018-05-03
 */
public class LAtrainerIndexVsCommSchema implements Schema, Cloneable
{
	// @Field
	/** 组训级别 */
	private String TrainerGrade;
	/** 佣金代码 */
	private String WageCode;
	/** 佣金名称 */
	private String WageName;
	/** 展业类型 */
	private String BranchType;
	/** 计算编码 */
	private String CalCode;
	/** 指标编码 */
	private String IndexCode;
	/** 佣金对应表名 */
	private String CTableName;
	/** 佣金对应字段名 */
	private String CColName;
	/** 打印顺序 */
	private int WageOrder;
	/** 打印标志 */
	private String PrintFlag;
	/** 佣金类型 */
	private String WageType;
	/** 渠道 */
	private String BranchType2;
	/** 管理机构 */
	private String ManageCom;
	/** 薪资版本 */
	private String WageVersion;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAtrainerIndexVsCommSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "TrainerGrade";
		pk[1] = "WageCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAtrainerIndexVsCommSchema cloned = (LAtrainerIndexVsCommSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTrainerGrade()
	{
		return TrainerGrade;
	}
	public void setTrainerGrade(String aTrainerGrade)
	{
		TrainerGrade = aTrainerGrade;
	}
	public String getWageCode()
	{
		return WageCode;
	}
	public void setWageCode(String aWageCode)
	{
		WageCode = aWageCode;
	}
	public String getWageName()
	{
		return WageName;
	}
	public void setWageName(String aWageName)
	{
		WageName = aWageName;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getCalCode()
	{
		return CalCode;
	}
	public void setCalCode(String aCalCode)
	{
		CalCode = aCalCode;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getCTableName()
	{
		return CTableName;
	}
	public void setCTableName(String aCTableName)
	{
		CTableName = aCTableName;
	}
	public String getCColName()
	{
		return CColName;
	}
	public void setCColName(String aCColName)
	{
		CColName = aCColName;
	}
	public int getWageOrder()
	{
		return WageOrder;
	}
	public void setWageOrder(int aWageOrder)
	{
		WageOrder = aWageOrder;
	}
	public void setWageOrder(String aWageOrder)
	{
		if (aWageOrder != null && !aWageOrder.equals(""))
		{
			Integer tInteger = new Integer(aWageOrder);
			int i = tInteger.intValue();
			WageOrder = i;
		}
	}

	public String getPrintFlag()
	{
		return PrintFlag;
	}
	public void setPrintFlag(String aPrintFlag)
	{
		PrintFlag = aPrintFlag;
	}
	public String getWageType()
	{
		return WageType;
	}
	public void setWageType(String aWageType)
	{
		WageType = aWageType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getWageVersion()
	{
		return WageVersion;
	}
	public void setWageVersion(String aWageVersion)
	{
		WageVersion = aWageVersion;
	}

	/**
	* 使用另外一个 LAtrainerIndexVsCommSchema 对象给 Schema 赋值
	* @param: aLAtrainerIndexVsCommSchema LAtrainerIndexVsCommSchema
	**/
	public void setSchema(LAtrainerIndexVsCommSchema aLAtrainerIndexVsCommSchema)
	{
		this.TrainerGrade = aLAtrainerIndexVsCommSchema.getTrainerGrade();
		this.WageCode = aLAtrainerIndexVsCommSchema.getWageCode();
		this.WageName = aLAtrainerIndexVsCommSchema.getWageName();
		this.BranchType = aLAtrainerIndexVsCommSchema.getBranchType();
		this.CalCode = aLAtrainerIndexVsCommSchema.getCalCode();
		this.IndexCode = aLAtrainerIndexVsCommSchema.getIndexCode();
		this.CTableName = aLAtrainerIndexVsCommSchema.getCTableName();
		this.CColName = aLAtrainerIndexVsCommSchema.getCColName();
		this.WageOrder = aLAtrainerIndexVsCommSchema.getWageOrder();
		this.PrintFlag = aLAtrainerIndexVsCommSchema.getPrintFlag();
		this.WageType = aLAtrainerIndexVsCommSchema.getWageType();
		this.BranchType2 = aLAtrainerIndexVsCommSchema.getBranchType2();
		this.ManageCom = aLAtrainerIndexVsCommSchema.getManageCom();
		this.WageVersion = aLAtrainerIndexVsCommSchema.getWageVersion();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TrainerGrade") == null )
				this.TrainerGrade = null;
			else
				this.TrainerGrade = rs.getString("TrainerGrade").trim();

			if( rs.getString("WageCode") == null )
				this.WageCode = null;
			else
				this.WageCode = rs.getString("WageCode").trim();

			if( rs.getString("WageName") == null )
				this.WageName = null;
			else
				this.WageName = rs.getString("WageName").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("CalCode") == null )
				this.CalCode = null;
			else
				this.CalCode = rs.getString("CalCode").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("CTableName") == null )
				this.CTableName = null;
			else
				this.CTableName = rs.getString("CTableName").trim();

			if( rs.getString("CColName") == null )
				this.CColName = null;
			else
				this.CColName = rs.getString("CColName").trim();

			this.WageOrder = rs.getInt("WageOrder");
			if( rs.getString("PrintFlag") == null )
				this.PrintFlag = null;
			else
				this.PrintFlag = rs.getString("PrintFlag").trim();

			if( rs.getString("WageType") == null )
				this.WageType = null;
			else
				this.WageType = rs.getString("WageType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("WageVersion") == null )
				this.WageVersion = null;
			else
				this.WageVersion = rs.getString("WageVersion").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAtrainerIndexVsComm表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAtrainerIndexVsCommSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAtrainerIndexVsCommSchema getSchema()
	{
		LAtrainerIndexVsCommSchema aLAtrainerIndexVsCommSchema = new LAtrainerIndexVsCommSchema();
		aLAtrainerIndexVsCommSchema.setSchema(this);
		return aLAtrainerIndexVsCommSchema;
	}

	public LAtrainerIndexVsCommDB getDB()
	{
		LAtrainerIndexVsCommDB aDBOper = new LAtrainerIndexVsCommDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAtrainerIndexVsComm描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TrainerGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CTableName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CColName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(WageOrder));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrintFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageVersion));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAtrainerIndexVsComm>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TrainerGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			WageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			WageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CTableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CColName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			WageOrder= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			PrintFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			WageType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			WageVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAtrainerIndexVsCommSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TrainerGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerGrade));
		}
		if (FCode.equals("WageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageCode));
		}
		if (FCode.equals("WageName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageName));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("CalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("CTableName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CTableName));
		}
		if (FCode.equals("CColName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CColName));
		}
		if (FCode.equals("WageOrder"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageOrder));
		}
		if (FCode.equals("PrintFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
		}
		if (FCode.equals("WageType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("WageVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageVersion));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TrainerGrade);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(WageCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(WageName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CalCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CTableName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CColName);
				break;
			case 8:
				strFieldValue = String.valueOf(WageOrder);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PrintFlag);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(WageType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(WageVersion);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TrainerGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerGrade = FValue.trim();
			}
			else
				TrainerGrade = null;
		}
		if (FCode.equalsIgnoreCase("WageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageCode = FValue.trim();
			}
			else
				WageCode = null;
		}
		if (FCode.equalsIgnoreCase("WageName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageName = FValue.trim();
			}
			else
				WageName = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("CalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalCode = FValue.trim();
			}
			else
				CalCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("CTableName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CTableName = FValue.trim();
			}
			else
				CTableName = null;
		}
		if (FCode.equalsIgnoreCase("CColName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CColName = FValue.trim();
			}
			else
				CColName = null;
		}
		if (FCode.equalsIgnoreCase("WageOrder"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				WageOrder = i;
			}
		}
		if (FCode.equalsIgnoreCase("PrintFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrintFlag = FValue.trim();
			}
			else
				PrintFlag = null;
		}
		if (FCode.equalsIgnoreCase("WageType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageType = FValue.trim();
			}
			else
				WageType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("WageVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageVersion = FValue.trim();
			}
			else
				WageVersion = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAtrainerIndexVsCommSchema other = (LAtrainerIndexVsCommSchema)otherObject;
		return
			(TrainerGrade == null ? other.getTrainerGrade() == null : TrainerGrade.equals(other.getTrainerGrade()))
			&& (WageCode == null ? other.getWageCode() == null : WageCode.equals(other.getWageCode()))
			&& (WageName == null ? other.getWageName() == null : WageName.equals(other.getWageName()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (CalCode == null ? other.getCalCode() == null : CalCode.equals(other.getCalCode()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (CTableName == null ? other.getCTableName() == null : CTableName.equals(other.getCTableName()))
			&& (CColName == null ? other.getCColName() == null : CColName.equals(other.getCColName()))
			&& WageOrder == other.getWageOrder()
			&& (PrintFlag == null ? other.getPrintFlag() == null : PrintFlag.equals(other.getPrintFlag()))
			&& (WageType == null ? other.getWageType() == null : WageType.equals(other.getWageType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (WageVersion == null ? other.getWageVersion() == null : WageVersion.equals(other.getWageVersion()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TrainerGrade") ) {
			return 0;
		}
		if( strFieldName.equals("WageCode") ) {
			return 1;
		}
		if( strFieldName.equals("WageName") ) {
			return 2;
		}
		if( strFieldName.equals("BranchType") ) {
			return 3;
		}
		if( strFieldName.equals("CalCode") ) {
			return 4;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 5;
		}
		if( strFieldName.equals("CTableName") ) {
			return 6;
		}
		if( strFieldName.equals("CColName") ) {
			return 7;
		}
		if( strFieldName.equals("WageOrder") ) {
			return 8;
		}
		if( strFieldName.equals("PrintFlag") ) {
			return 9;
		}
		if( strFieldName.equals("WageType") ) {
			return 10;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 11;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 12;
		}
		if( strFieldName.equals("WageVersion") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TrainerGrade";
				break;
			case 1:
				strFieldName = "WageCode";
				break;
			case 2:
				strFieldName = "WageName";
				break;
			case 3:
				strFieldName = "BranchType";
				break;
			case 4:
				strFieldName = "CalCode";
				break;
			case 5:
				strFieldName = "IndexCode";
				break;
			case 6:
				strFieldName = "CTableName";
				break;
			case 7:
				strFieldName = "CColName";
				break;
			case 8:
				strFieldName = "WageOrder";
				break;
			case 9:
				strFieldName = "PrintFlag";
				break;
			case 10:
				strFieldName = "WageType";
				break;
			case 11:
				strFieldName = "BranchType2";
				break;
			case 12:
				strFieldName = "ManageCom";
				break;
			case 13:
				strFieldName = "WageVersion";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TrainerGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CTableName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CColName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageOrder") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PrintFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageVersion") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
