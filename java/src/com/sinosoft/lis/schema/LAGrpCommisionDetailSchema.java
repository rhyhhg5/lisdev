/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAGrpCommisionDetailDB;

/*
 * <p>ClassName: LAGrpCommisionDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-03-08
 */
public class LAGrpCommisionDetailSchema implements Schema, Cloneable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 险种编码 */
    private String RiskCode;
    /** 代理人编码 */
    private String AgentCode;
    /** 是否服务人 */
    private String ServFlag;
    /** 业务百分比 */
    private double BusiRate;
    /** 是否分配客户数标记 */
    private String AppAllotMark;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 客户数比例 */
    private double AppntRate;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAGrpCommisionDetailSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "GrpContNo";
        pk[1] = "RiskCode";
        pk[2] = "AgentCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAGrpCommisionDetailSchema cloned = (LAGrpCommisionDetailSchema)super.
                                            clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getServFlag() {
        return ServFlag;
    }

    public void setServFlag(String aServFlag) {
        ServFlag = aServFlag;
    }

    public double getBusiRate() {
        return BusiRate;
    }

    public void setBusiRate(double aBusiRate) {
        BusiRate = Arith.round(aBusiRate, 2);
    }

    public void setBusiRate(String aBusiRate) {
        if (aBusiRate != null && !aBusiRate.equals("")) {
            Double tDouble = new Double(aBusiRate);
            double d = tDouble.doubleValue();
            BusiRate = Arith.round(d, 2);
        }
    }

    public String getAppAllotMark() {
        return AppAllotMark;
    }

    public void setAppAllotMark(String aAppAllotMark) {
        AppAllotMark = aAppAllotMark;
    }

    public String getMngCom() {
        return MngCom;
    }

    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public double getAppntRate() {
        return AppntRate;
    }

    public void setAppntRate(double aAppntRate) {
        AppntRate = Arith.round(aAppntRate, 2);
    }

    public void setAppntRate(String aAppntRate) {
        if (aAppntRate != null && !aAppntRate.equals("")) {
            Double tDouble = new Double(aAppntRate);
            double d = tDouble.doubleValue();
            AppntRate = Arith.round(d, 2);
        }
    }


    /**
     * 使用另外一个 LAGrpCommisionDetailSchema 对象给 Schema 赋值
     * @param: aLAGrpCommisionDetailSchema LAGrpCommisionDetailSchema
     **/
    public void setSchema(LAGrpCommisionDetailSchema
                          aLAGrpCommisionDetailSchema) {
        this.GrpContNo = aLAGrpCommisionDetailSchema.getGrpContNo();
        this.RiskCode = aLAGrpCommisionDetailSchema.getRiskCode();
        this.AgentCode = aLAGrpCommisionDetailSchema.getAgentCode();
        this.ServFlag = aLAGrpCommisionDetailSchema.getServFlag();
        this.BusiRate = aLAGrpCommisionDetailSchema.getBusiRate();
        this.AppAllotMark = aLAGrpCommisionDetailSchema.getAppAllotMark();
        this.MngCom = aLAGrpCommisionDetailSchema.getMngCom();
        this.Operator = aLAGrpCommisionDetailSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAGrpCommisionDetailSchema.getMakeDate());
        this.MakeTime = aLAGrpCommisionDetailSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAGrpCommisionDetailSchema.
                                        getModifyDate());
        this.ModifyTime = aLAGrpCommisionDetailSchema.getModifyTime();
        this.AppntRate = aLAGrpCommisionDetailSchema.getAppntRate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("ServFlag") == null) {
                this.ServFlag = null;
            } else {
                this.ServFlag = rs.getString("ServFlag").trim();
            }

            this.BusiRate = rs.getDouble("BusiRate");
            if (rs.getString("AppAllotMark") == null) {
                this.AppAllotMark = null;
            } else {
                this.AppAllotMark = rs.getString("AppAllotMark").trim();
            }

            if (rs.getString("MngCom") == null) {
                this.MngCom = null;
            } else {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.AppntRate = rs.getDouble("AppntRate");
        } catch (SQLException sqle) {
            System.out.println("数据库中的LAGrpCommisionDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpCommisionDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAGrpCommisionDetailSchema getSchema() {
        LAGrpCommisionDetailSchema aLAGrpCommisionDetailSchema = new
                LAGrpCommisionDetailSchema();
        aLAGrpCommisionDetailSchema.setSchema(this);
        return aLAGrpCommisionDetailSchema;
    }

    public LAGrpCommisionDetailDB getDB() {
        LAGrpCommisionDetailDB aDBOper = new LAGrpCommisionDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAGrpCommisionDetail描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BusiRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppAllotMark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AppntRate));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAGrpCommisionDetail>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            ServFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            BusiRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            AppAllotMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            AppntRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpCommisionDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("ServFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServFlag));
        }
        if (FCode.equals("BusiRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusiRate));
        }
        if (FCode.equals("AppAllotMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppAllotMark));
        }
        if (FCode.equals("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("AppntRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntRate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServFlag);
            break;
        case 4:
            strFieldValue = String.valueOf(BusiRate);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(AppAllotMark);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(MngCom);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 12:
            strFieldValue = String.valueOf(AppntRate);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ServFlag = FValue.trim();
            } else {
                ServFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("BusiRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BusiRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("AppAllotMark")) {
            if (FValue != null && !FValue.equals("")) {
                AppAllotMark = FValue.trim();
            } else {
                AppAllotMark = null;
            }
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if (FValue != null && !FValue.equals("")) {
                MngCom = FValue.trim();
            } else {
                MngCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppntRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AppntRate = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LAGrpCommisionDetailSchema other = (LAGrpCommisionDetailSchema)
                                           otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && RiskCode.equals(other.getRiskCode())
                && AgentCode.equals(other.getAgentCode())
                && ServFlag.equals(other.getServFlag())
                && BusiRate == other.getBusiRate()
                && AppAllotMark.equals(other.getAppAllotMark())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && AppntRate == other.getAppntRate();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("GrpContNo")) {
            return 0;
        }
        if (strFieldName.equals("RiskCode")) {
            return 1;
        }
        if (strFieldName.equals("AgentCode")) {
            return 2;
        }
        if (strFieldName.equals("ServFlag")) {
            return 3;
        }
        if (strFieldName.equals("BusiRate")) {
            return 4;
        }
        if (strFieldName.equals("AppAllotMark")) {
            return 5;
        }
        if (strFieldName.equals("MngCom")) {
            return 6;
        }
        if (strFieldName.equals("Operator")) {
            return 7;
        }
        if (strFieldName.equals("MakeDate")) {
            return 8;
        }
        if (strFieldName.equals("MakeTime")) {
            return 9;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 10;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 11;
        }
        if (strFieldName.equals("AppntRate")) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "GrpContNo";
            break;
        case 1:
            strFieldName = "RiskCode";
            break;
        case 2:
            strFieldName = "AgentCode";
            break;
        case 3:
            strFieldName = "ServFlag";
            break;
        case 4:
            strFieldName = "BusiRate";
            break;
        case 5:
            strFieldName = "AppAllotMark";
            break;
        case 6:
            strFieldName = "MngCom";
            break;
        case 7:
            strFieldName = "Operator";
            break;
        case 8:
            strFieldName = "MakeDate";
            break;
        case 9:
            strFieldName = "MakeTime";
            break;
        case 10:
            strFieldName = "ModifyDate";
            break;
        case 11:
            strFieldName = "ModifyTime";
            break;
        case 12:
            strFieldName = "AppntRate";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BusiRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AppAllotMark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntRate")) {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
