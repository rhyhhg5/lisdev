/*
 * <p>ClassName: LOPRTManagerSubSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LOPRTManagerSubDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOPRTManagerSubSchema implements Schema
{
    // @Field
    /** 印刷流水号 */
    private String PrtSeq;
    /** 缴费通知书号 */
    private String GetNoticeNo;
    /** 对应其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 险种编码 */
    private String RiskCode;
    /** 应收金额 */
    private double DuePayMoney;
    /** 投保人名称 */
    private String AppntName;
    /** 类型 */
    private String TypeFlag;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOPRTManagerSubSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "PrtSeq";
        pk[1] = "GetNoticeNo";
        pk[2] = "RiskCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPrtSeq()
    {
        if (PrtSeq != null && !PrtSeq.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtSeq = StrTool.unicodeToGBK(PrtSeq);
        }
        return PrtSeq;
    }

    public void setPrtSeq(String aPrtSeq)
    {
        PrtSeq = aPrtSeq;
    }

    public String getGetNoticeNo()
    {
        if (GetNoticeNo != null && !GetNoticeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetNoticeNo = StrTool.unicodeToGBK(GetNoticeNo);
        }
        return GetNoticeNo;
    }

    public void setGetNoticeNo(String aGetNoticeNo)
    {
        GetNoticeNo = aGetNoticeNo;
    }

    public String getOtherNo()
    {
        if (OtherNo != null && !OtherNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OtherNo = StrTool.unicodeToGBK(OtherNo);
        }
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        if (OtherNoType != null && !OtherNoType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OtherNoType = StrTool.unicodeToGBK(OtherNoType);
        }
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public double getDuePayMoney()
    {
        return DuePayMoney;
    }

    public void setDuePayMoney(double aDuePayMoney)
    {
        DuePayMoney = aDuePayMoney;
    }

    public void setDuePayMoney(String aDuePayMoney)
    {
        if (aDuePayMoney != null && !aDuePayMoney.equals(""))
        {
            Double tDouble = new Double(aDuePayMoney);
            double d = tDouble.doubleValue();
            DuePayMoney = d;
        }
    }

    public String getAppntName()
    {
        if (AppntName != null && !AppntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public String getTypeFlag()
    {
        if (TypeFlag != null && !TypeFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            TypeFlag = StrTool.unicodeToGBK(TypeFlag);
        }
        return TypeFlag;
    }

    public void setTypeFlag(String aTypeFlag)
    {
        TypeFlag = aTypeFlag;
    }

    /**
     * 使用另外一个 LOPRTManagerSubSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOPRTManagerSubSchema aLOPRTManagerSubSchema)
    {
        this.PrtSeq = aLOPRTManagerSubSchema.getPrtSeq();
        this.GetNoticeNo = aLOPRTManagerSubSchema.getGetNoticeNo();
        this.OtherNo = aLOPRTManagerSubSchema.getOtherNo();
        this.OtherNoType = aLOPRTManagerSubSchema.getOtherNoType();
        this.RiskCode = aLOPRTManagerSubSchema.getRiskCode();
        this.DuePayMoney = aLOPRTManagerSubSchema.getDuePayMoney();
        this.AppntName = aLOPRTManagerSubSchema.getAppntName();
        this.TypeFlag = aLOPRTManagerSubSchema.getTypeFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PrtSeq") == null)
            {
                this.PrtSeq = null;
            }
            else
            {
                this.PrtSeq = rs.getString("PrtSeq").trim();
            }

            if (rs.getString("GetNoticeNo") == null)
            {
                this.GetNoticeNo = null;
            }
            else
            {
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            this.DuePayMoney = rs.getDouble("DuePayMoney");
            if (rs.getString("AppntName") == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString("AppntName").trim();
            }

            if (rs.getString("TypeFlag") == null)
            {
                this.TypeFlag = null;
            }
            else
            {
                this.TypeFlag = rs.getString("TypeFlag").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOPRTManagerSubSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOPRTManagerSubSchema getSchema()
    {
        LOPRTManagerSubSchema aLOPRTManagerSubSchema = new
                LOPRTManagerSubSchema();
        aLOPRTManagerSubSchema.setSchema(this);
        return aLOPRTManagerSubSchema;
    }

    public LOPRTManagerSubDB getDB()
    {
        LOPRTManagerSubDB aDBOper = new LOPRTManagerSubDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOPRTManagerSub描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(PrtSeq)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetNoticeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNoType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(DuePayMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TypeFlag));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOPRTManagerSub>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            DuePayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            TypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOPRTManagerSubSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PrtSeq"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtSeq));
        }
        if (FCode.equals("GetNoticeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetNoticeNo));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNoType));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("DuePayMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DuePayMoney));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntName));
        }
        if (FCode.equals("TypeFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TypeFlag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PrtSeq);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 5:
                strFieldValue = String.valueOf(DuePayMoney);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(TypeFlag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("PrtSeq"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
            {
                PrtSeq = null;
            }
        }
        if (FCode.equals("GetNoticeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
            {
                GetNoticeNo = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("DuePayMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DuePayMoney = d;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("TypeFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TypeFlag = FValue.trim();
            }
            else
            {
                TypeFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOPRTManagerSubSchema other = (LOPRTManagerSubSchema) otherObject;
        return
                PrtSeq.equals(other.getPrtSeq())
                && GetNoticeNo.equals(other.getGetNoticeNo())
                && OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && RiskCode.equals(other.getRiskCode())
                && DuePayMoney == other.getDuePayMoney()
                && AppntName.equals(other.getAppntName())
                && TypeFlag.equals(other.getTypeFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PrtSeq"))
        {
            return 0;
        }
        if (strFieldName.equals("GetNoticeNo"))
        {
            return 1;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 2;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 3;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 4;
        }
        if (strFieldName.equals("DuePayMoney"))
        {
            return 5;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 6;
        }
        if (strFieldName.equals("TypeFlag"))
        {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PrtSeq";
                break;
            case 1:
                strFieldName = "GetNoticeNo";
                break;
            case 2:
                strFieldName = "OtherNo";
                break;
            case 3:
                strFieldName = "OtherNoType";
                break;
            case 4:
                strFieldName = "RiskCode";
                break;
            case 5:
                strFieldName = "DuePayMoney";
                break;
            case 6:
                strFieldName = "AppntName";
                break;
            case 7:
                strFieldName = "TypeFlag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PrtSeq"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetNoticeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DuePayMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TypeFlag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
