/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.Rate5201DB;

/*
 * <p>ClassName: Rate5201Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-09-04
 */
public class Rate5201Schema implements Schema, Cloneable {
    // @Field
    /** 管理机构 */
    private String ManageCom;
    /** 起始日期 */
    private Date StartDate;
    /** 职业类别 */
    private String OccType;
    /** 净再保费率 */
    private double ReFeeRate;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public Rate5201Schema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ManageCom";
        pk[1] = "StartDate";
        pk[2] = "OccType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        Rate5201Schema cloned = (Rate5201Schema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getOccType() {
        return OccType;
    }

    public void setOccType(String aOccType) {
        OccType = aOccType;
    }

    public double getReFeeRate() {
        return ReFeeRate;
    }

    public void setReFeeRate(double aReFeeRate) {
        ReFeeRate = Arith.round(aReFeeRate, 2);
    }

    public void setReFeeRate(String aReFeeRate) {
        if (aReFeeRate != null && !aReFeeRate.equals("")) {
            Double tDouble = new Double(aReFeeRate);
            double d = tDouble.doubleValue();
            ReFeeRate = Arith.round(d, 2);
        }
    }


    /**
     * 使用另外一个 Rate5201Schema 对象给 Schema 赋值
     * @param: aRate5201Schema Rate5201Schema
     **/
    public void setSchema(Rate5201Schema aRate5201Schema) {
        this.ManageCom = aRate5201Schema.getManageCom();
        this.StartDate = fDate.getDate(aRate5201Schema.getStartDate());
        this.OccType = aRate5201Schema.getOccType();
        this.ReFeeRate = aRate5201Schema.getReFeeRate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            if (rs.getString("OccType") == null) {
                this.OccType = null;
            } else {
                this.OccType = rs.getString("OccType").trim();
            }

            this.ReFeeRate = rs.getDouble("ReFeeRate");
        } catch (SQLException sqle) {
            System.out.println("数据库中的Rate5201表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Rate5201Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public Rate5201Schema getSchema() {
        Rate5201Schema aRate5201Schema = new Rate5201Schema();
        aRate5201Schema.setSchema(this);
        return aRate5201Schema;
    }

    public Rate5201DB getDB() {
        Rate5201DB aDBOper = new Rate5201DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpRate5201描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ReFeeRate));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpRate5201>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER));
            OccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            ReFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Rate5201Schema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("OccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccType));
        }
        if (FCode.equals("ReFeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReFeeRate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(OccType);
            break;
        case 3:
            strFieldValue = String.valueOf(ReFeeRate);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("OccType")) {
            if (FValue != null && !FValue.equals("")) {
                OccType = FValue.trim();
            } else {
                OccType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReFeeRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ReFeeRate = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        Rate5201Schema other = (Rate5201Schema) otherObject;
        return
                ManageCom.equals(other.getManageCom())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && OccType.equals(other.getOccType())
                && ReFeeRate == other.getReFeeRate();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ManageCom")) {
            return 0;
        }
        if (strFieldName.equals("StartDate")) {
            return 1;
        }
        if (strFieldName.equals("OccType")) {
            return 2;
        }
        if (strFieldName.equals("ReFeeRate")) {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ManageCom";
            break;
        case 1:
            strFieldName = "StartDate";
            break;
        case 2:
            strFieldName = "OccType";
            break;
        case 3:
            strFieldName = "ReFeeRate";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OccType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReFeeRate")) {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
