/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseCureDB;

/*
 * <p>ClassName: LLCaseCureSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-06-14
 */
public class LLCaseCureSchema implements Schema, Cloneable
{
	// @Field
	/** 序号 */
	private String SerialNo;
	/** 分案号 */
	private String CaseNo;
	/** 受理事故号 */
	private String CaseRelaNo;
	/** 医院代码 */
	private String HospitalCode;
	/** 医院名称 */
	private String HospitalName;
	/** 诊断医生号码 */
	private String DoctorNo;
	/** 诊断医生名称 */
	private String DoctorName;
	/** 出险人客户号 */
	private String CustomerNo;
	/** 出险人名称 */
	private String CustomerName;
	/** 出险类型 */
	private String AccidentType;
	/** 门诊/住院号码 */
	private String CureNo;
	/** 住院收据号码 */
	private String ReceiptNo;
	/** 入院日期 */
	private Date InHospitalDate;
	/** 出院日期 */
	private Date OutHospitalDate;
	/** 实际住院天数 */
	private int InHospitalDays;
	/** 重疾标记 */
	private String SeriousFlag;
	/** 疾病代码 */
	private String DiseaseCode;
	/** 疾病名称 */
	private String DiseaseName;
	/** 确诊日期 */
	private Date DiagnoseDate;
	/** 确诊依据 */
	private String DiagnoseDesc;
	/** 临床诊断 */
	private String Diagnose;
	/** 鉴定单位 */
	private String JudgeOrgan;
	/** 鉴定单位名称 */
	private String JudgeOrganName;
	/** 鉴定时间 */
	private Date JudgeDate;
	/** 鉴定依据 */
	private String JudgeDepend;
	/** 主要疾病标志 */
	private String MainDiseaseFlag;
	/** 赔付标志 */
	private String ClmFlag;
	/** 申请赔付金额 */
	private double ApplyFee;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 有无手术标志 */
	private String OperationFlag;
	/** 重疾责任标记 */
	private String SeriousDutyFlag;
	/** 风险等级 */
	private String RiskLevel;
	/** 风险存续期 */
	private String RiskContinueFlag;
	/** 肿瘤形态学代码 */
	private String LDCODE;
	/** 肿瘤形态学名称 */
	private String LDNAME;

	public static final int FIELDNUM = 40;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCaseCureSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "SerialNo";
		pk[1] = "CaseNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCaseCureSchema cloned = (LLCaseCureSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getCaseRelaNo()
	{
		return CaseRelaNo;
	}
	public void setCaseRelaNo(String aCaseRelaNo)
	{
		CaseRelaNo = aCaseRelaNo;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
		HospitalCode = aHospitalCode;
	}
	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
		HospitalName = aHospitalName;
	}
	public String getDoctorNo()
	{
		return DoctorNo;
	}
	public void setDoctorNo(String aDoctorNo)
	{
		DoctorNo = aDoctorNo;
	}
	public String getDoctorName()
	{
		return DoctorName;
	}
	public void setDoctorName(String aDoctorName)
	{
		DoctorName = aDoctorName;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getAccidentType()
	{
		return AccidentType;
	}
	public void setAccidentType(String aAccidentType)
	{
		AccidentType = aAccidentType;
	}
	public String getCureNo()
	{
		return CureNo;
	}
	public void setCureNo(String aCureNo)
	{
		CureNo = aCureNo;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getInHospitalDate()
	{
		if( InHospitalDate != null )
			return fDate.getString(InHospitalDate);
		else
			return null;
	}
	public void setInHospitalDate(Date aInHospitalDate)
	{
		InHospitalDate = aInHospitalDate;
	}
	public void setInHospitalDate(String aInHospitalDate)
	{
		if (aInHospitalDate != null && !aInHospitalDate.equals("") )
		{
			InHospitalDate = fDate.getDate( aInHospitalDate );
		}
		else
			InHospitalDate = null;
	}

	public String getOutHospitalDate()
	{
		if( OutHospitalDate != null )
			return fDate.getString(OutHospitalDate);
		else
			return null;
	}
	public void setOutHospitalDate(Date aOutHospitalDate)
	{
		OutHospitalDate = aOutHospitalDate;
	}
	public void setOutHospitalDate(String aOutHospitalDate)
	{
		if (aOutHospitalDate != null && !aOutHospitalDate.equals("") )
		{
			OutHospitalDate = fDate.getDate( aOutHospitalDate );
		}
		else
			OutHospitalDate = null;
	}

	public int getInHospitalDays()
	{
		return InHospitalDays;
	}
	public void setInHospitalDays(int aInHospitalDays)
	{
		InHospitalDays = aInHospitalDays;
	}
	public void setInHospitalDays(String aInHospitalDays)
	{
		if (aInHospitalDays != null && !aInHospitalDays.equals(""))
		{
			Integer tInteger = new Integer(aInHospitalDays);
			int i = tInteger.intValue();
			InHospitalDays = i;
		}
	}

	public String getSeriousFlag()
	{
		return SeriousFlag;
	}
	public void setSeriousFlag(String aSeriousFlag)
	{
		SeriousFlag = aSeriousFlag;
	}
	public String getDiseaseCode()
	{
		return DiseaseCode;
	}
	public void setDiseaseCode(String aDiseaseCode)
	{
		DiseaseCode = aDiseaseCode;
	}
	public String getDiseaseName()
	{
		return DiseaseName;
	}
	public void setDiseaseName(String aDiseaseName)
	{
		DiseaseName = aDiseaseName;
	}
	public String getDiagnoseDate()
	{
		if( DiagnoseDate != null )
			return fDate.getString(DiagnoseDate);
		else
			return null;
	}
	public void setDiagnoseDate(Date aDiagnoseDate)
	{
		DiagnoseDate = aDiagnoseDate;
	}
	public void setDiagnoseDate(String aDiagnoseDate)
	{
		if (aDiagnoseDate != null && !aDiagnoseDate.equals("") )
		{
			DiagnoseDate = fDate.getDate( aDiagnoseDate );
		}
		else
			DiagnoseDate = null;
	}

	public String getDiagnoseDesc()
	{
		return DiagnoseDesc;
	}
	public void setDiagnoseDesc(String aDiagnoseDesc)
	{
		DiagnoseDesc = aDiagnoseDesc;
	}
	public String getDiagnose()
	{
		return Diagnose;
	}
	public void setDiagnose(String aDiagnose)
	{
		Diagnose = aDiagnose;
	}
	public String getJudgeOrgan()
	{
		return JudgeOrgan;
	}
	public void setJudgeOrgan(String aJudgeOrgan)
	{
		JudgeOrgan = aJudgeOrgan;
	}
	public String getJudgeOrganName()
	{
		return JudgeOrganName;
	}
	public void setJudgeOrganName(String aJudgeOrganName)
	{
		JudgeOrganName = aJudgeOrganName;
	}
	public String getJudgeDate()
	{
		if( JudgeDate != null )
			return fDate.getString(JudgeDate);
		else
			return null;
	}
	public void setJudgeDate(Date aJudgeDate)
	{
		JudgeDate = aJudgeDate;
	}
	public void setJudgeDate(String aJudgeDate)
	{
		if (aJudgeDate != null && !aJudgeDate.equals("") )
		{
			JudgeDate = fDate.getDate( aJudgeDate );
		}
		else
			JudgeDate = null;
	}

	public String getJudgeDepend()
	{
		return JudgeDepend;
	}
	public void setJudgeDepend(String aJudgeDepend)
	{
		JudgeDepend = aJudgeDepend;
	}
	public String getMainDiseaseFlag()
	{
		return MainDiseaseFlag;
	}
	public void setMainDiseaseFlag(String aMainDiseaseFlag)
	{
		MainDiseaseFlag = aMainDiseaseFlag;
	}
	public String getClmFlag()
	{
		return ClmFlag;
	}
	public void setClmFlag(String aClmFlag)
	{
		ClmFlag = aClmFlag;
	}
	public double getApplyFee()
	{
		return ApplyFee;
	}
	public void setApplyFee(double aApplyFee)
	{
		ApplyFee = Arith.round(aApplyFee,2);
	}
	public void setApplyFee(String aApplyFee)
	{
		if (aApplyFee != null && !aApplyFee.equals(""))
		{
			Double tDouble = new Double(aApplyFee);
			double d = tDouble.doubleValue();
                ApplyFee = Arith.round(d,2);
		}
	}

	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperationFlag()
	{
		return OperationFlag;
	}
	public void setOperationFlag(String aOperationFlag)
	{
		OperationFlag = aOperationFlag;
	}
	public String getSeriousDutyFlag()
	{
		return SeriousDutyFlag;
	}
	public void setSeriousDutyFlag(String aSeriousDutyFlag)
	{
		SeriousDutyFlag = aSeriousDutyFlag;
	}
	public String getRiskLevel()
	{
		return RiskLevel;
	}
	public void setRiskLevel(String aRiskLevel)
	{
		RiskLevel = aRiskLevel;
	}
	public String getRiskContinueFlag()
	{
		return RiskContinueFlag;
	}
	public void setRiskContinueFlag(String aRiskContinueFlag)
	{
		RiskContinueFlag = aRiskContinueFlag;
	}
	public String getLDCODE()
	{
		return LDCODE;
	}
	public void setLDCODE(String aLDCODE)
	{
		LDCODE = aLDCODE;
	}
	public String getLDNAME()
	{
		return LDNAME;
	}
	public void setLDNAME(String aLDNAME)
	{
		LDNAME = aLDNAME;
	}

	/**
	* 使用另外一个 LLCaseCureSchema 对象给 Schema 赋值
	* @param: aLLCaseCureSchema LLCaseCureSchema
	**/
	public void setSchema(LLCaseCureSchema aLLCaseCureSchema)
	{
		this.SerialNo = aLLCaseCureSchema.getSerialNo();
		this.CaseNo = aLLCaseCureSchema.getCaseNo();
		this.CaseRelaNo = aLLCaseCureSchema.getCaseRelaNo();
		this.HospitalCode = aLLCaseCureSchema.getHospitalCode();
		this.HospitalName = aLLCaseCureSchema.getHospitalName();
		this.DoctorNo = aLLCaseCureSchema.getDoctorNo();
		this.DoctorName = aLLCaseCureSchema.getDoctorName();
		this.CustomerNo = aLLCaseCureSchema.getCustomerNo();
		this.CustomerName = aLLCaseCureSchema.getCustomerName();
		this.AccidentType = aLLCaseCureSchema.getAccidentType();
		this.CureNo = aLLCaseCureSchema.getCureNo();
		this.ReceiptNo = aLLCaseCureSchema.getReceiptNo();
		this.InHospitalDate = fDate.getDate( aLLCaseCureSchema.getInHospitalDate());
		this.OutHospitalDate = fDate.getDate( aLLCaseCureSchema.getOutHospitalDate());
		this.InHospitalDays = aLLCaseCureSchema.getInHospitalDays();
		this.SeriousFlag = aLLCaseCureSchema.getSeriousFlag();
		this.DiseaseCode = aLLCaseCureSchema.getDiseaseCode();
		this.DiseaseName = aLLCaseCureSchema.getDiseaseName();
		this.DiagnoseDate = fDate.getDate( aLLCaseCureSchema.getDiagnoseDate());
		this.DiagnoseDesc = aLLCaseCureSchema.getDiagnoseDesc();
		this.Diagnose = aLLCaseCureSchema.getDiagnose();
		this.JudgeOrgan = aLLCaseCureSchema.getJudgeOrgan();
		this.JudgeOrganName = aLLCaseCureSchema.getJudgeOrganName();
		this.JudgeDate = fDate.getDate( aLLCaseCureSchema.getJudgeDate());
		this.JudgeDepend = aLLCaseCureSchema.getJudgeDepend();
		this.MainDiseaseFlag = aLLCaseCureSchema.getMainDiseaseFlag();
		this.ClmFlag = aLLCaseCureSchema.getClmFlag();
		this.ApplyFee = aLLCaseCureSchema.getApplyFee();
		this.MngCom = aLLCaseCureSchema.getMngCom();
		this.Operator = aLLCaseCureSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLCaseCureSchema.getMakeDate());
		this.MakeTime = aLLCaseCureSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLCaseCureSchema.getModifyDate());
		this.ModifyTime = aLLCaseCureSchema.getModifyTime();
		this.OperationFlag = aLLCaseCureSchema.getOperationFlag();
		this.SeriousDutyFlag = aLLCaseCureSchema.getSeriousDutyFlag();
		this.RiskLevel = aLLCaseCureSchema.getRiskLevel();
		this.RiskContinueFlag = aLLCaseCureSchema.getRiskContinueFlag();
		this.LDCODE = aLLCaseCureSchema.getLDCODE();
		this.LDNAME = aLLCaseCureSchema.getLDNAME();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("CaseRelaNo") == null )
				this.CaseRelaNo = null;
			else
				this.CaseRelaNo = rs.getString("CaseRelaNo").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			if( rs.getString("DoctorNo") == null )
				this.DoctorNo = null;
			else
				this.DoctorNo = rs.getString("DoctorNo").trim();

			if( rs.getString("DoctorName") == null )
				this.DoctorName = null;
			else
				this.DoctorName = rs.getString("DoctorName").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("AccidentType") == null )
				this.AccidentType = null;
			else
				this.AccidentType = rs.getString("AccidentType").trim();

			if( rs.getString("CureNo") == null )
				this.CureNo = null;
			else
				this.CureNo = rs.getString("CureNo").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			this.InHospitalDate = rs.getDate("InHospitalDate");
			this.OutHospitalDate = rs.getDate("OutHospitalDate");
			this.InHospitalDays = rs.getInt("InHospitalDays");
			if( rs.getString("SeriousFlag") == null )
				this.SeriousFlag = null;
			else
				this.SeriousFlag = rs.getString("SeriousFlag").trim();

			if( rs.getString("DiseaseCode") == null )
				this.DiseaseCode = null;
			else
				this.DiseaseCode = rs.getString("DiseaseCode").trim();

			if( rs.getString("DiseaseName") == null )
				this.DiseaseName = null;
			else
				this.DiseaseName = rs.getString("DiseaseName").trim();

			this.DiagnoseDate = rs.getDate("DiagnoseDate");
			if( rs.getString("DiagnoseDesc") == null )
				this.DiagnoseDesc = null;
			else
				this.DiagnoseDesc = rs.getString("DiagnoseDesc").trim();

			if( rs.getString("Diagnose") == null )
				this.Diagnose = null;
			else
				this.Diagnose = rs.getString("Diagnose").trim();

			if( rs.getString("JudgeOrgan") == null )
				this.JudgeOrgan = null;
			else
				this.JudgeOrgan = rs.getString("JudgeOrgan").trim();

			if( rs.getString("JudgeOrganName") == null )
				this.JudgeOrganName = null;
			else
				this.JudgeOrganName = rs.getString("JudgeOrganName").trim();

			this.JudgeDate = rs.getDate("JudgeDate");
			if( rs.getString("JudgeDepend") == null )
				this.JudgeDepend = null;
			else
				this.JudgeDepend = rs.getString("JudgeDepend").trim();

			if( rs.getString("MainDiseaseFlag") == null )
				this.MainDiseaseFlag = null;
			else
				this.MainDiseaseFlag = rs.getString("MainDiseaseFlag").trim();

			if( rs.getString("ClmFlag") == null )
				this.ClmFlag = null;
			else
				this.ClmFlag = rs.getString("ClmFlag").trim();

			this.ApplyFee = rs.getDouble("ApplyFee");
			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("OperationFlag") == null )
				this.OperationFlag = null;
			else
				this.OperationFlag = rs.getString("OperationFlag").trim();

			if( rs.getString("SeriousDutyFlag") == null )
				this.SeriousDutyFlag = null;
			else
				this.SeriousDutyFlag = rs.getString("SeriousDutyFlag").trim();

			if( rs.getString("RiskLevel") == null )
				this.RiskLevel = null;
			else
				this.RiskLevel = rs.getString("RiskLevel").trim();

			if( rs.getString("RiskContinueFlag") == null )
				this.RiskContinueFlag = null;
			else
				this.RiskContinueFlag = rs.getString("RiskContinueFlag").trim();

			if( rs.getString("LDCODE") == null )
				this.LDCODE = null;
			else
				this.LDCODE = rs.getString("LDCODE").trim();

			if( rs.getString("LDNAME") == null )
				this.LDNAME = null;
			else
				this.LDNAME = rs.getString("LDNAME").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCaseCure表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseCureSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCaseCureSchema getSchema()
	{
		LLCaseCureSchema aLLCaseCureSchema = new LLCaseCureSchema();
		aLLCaseCureSchema.setSchema(this);
		return aLLCaseCureSchema;
	}

	public LLCaseCureDB getDB()
	{
		LLCaseCureDB aDBOper = new LLCaseCureDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseCure描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseRelaNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DoctorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DoctorName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccidentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CureNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InHospitalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OutHospitalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InHospitalDays));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SeriousFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiseaseCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiseaseName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DiagnoseDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiagnoseDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Diagnose)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(JudgeOrgan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(JudgeOrganName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( JudgeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(JudgeDepend)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainDiseaseFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClmFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ApplyFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OperationFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SeriousDutyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskLevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskContinueFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LDCODE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LDNAME));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseCure>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			DoctorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DoctorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AccidentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CureNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			InHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			OutHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			InHospitalDays= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).intValue();
			SeriousFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			DiseaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			DiseaseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			DiagnoseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			DiagnoseDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Diagnose = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			JudgeOrgan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			JudgeOrganName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			JudgeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			JudgeDepend = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			MainDiseaseFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ClmFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ApplyFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			OperationFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			SeriousDutyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			RiskLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			RiskContinueFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			LDCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			LDNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseCureSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("CaseRelaNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseRelaNo));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("DoctorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctorNo));
		}
		if (FCode.equals("DoctorName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctorName));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("AccidentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentType));
		}
		if (FCode.equals("CureNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CureNo));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("InHospitalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInHospitalDate()));
		}
		if (FCode.equals("OutHospitalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOutHospitalDate()));
		}
		if (FCode.equals("InHospitalDays"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InHospitalDays));
		}
		if (FCode.equals("SeriousFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeriousFlag));
		}
		if (FCode.equals("DiseaseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseCode));
		}
		if (FCode.equals("DiseaseName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseName));
		}
		if (FCode.equals("DiagnoseDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDiagnoseDate()));
		}
		if (FCode.equals("DiagnoseDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiagnoseDesc));
		}
		if (FCode.equals("Diagnose"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Diagnose));
		}
		if (FCode.equals("JudgeOrgan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JudgeOrgan));
		}
		if (FCode.equals("JudgeOrganName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JudgeOrganName));
		}
		if (FCode.equals("JudgeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getJudgeDate()));
		}
		if (FCode.equals("JudgeDepend"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JudgeDepend));
		}
		if (FCode.equals("MainDiseaseFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainDiseaseFlag));
		}
		if (FCode.equals("ClmFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFlag));
		}
		if (FCode.equals("ApplyFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyFee));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("OperationFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperationFlag));
		}
		if (FCode.equals("SeriousDutyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeriousDutyFlag));
		}
		if (FCode.equals("RiskLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskLevel));
		}
		if (FCode.equals("RiskContinueFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskContinueFlag));
		}
		if (FCode.equals("LDCODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LDCODE));
		}
		if (FCode.equals("LDNAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LDNAME));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(DoctorNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DoctorName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AccidentType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CureNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInHospitalDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOutHospitalDate()));
				break;
			case 14:
				strFieldValue = String.valueOf(InHospitalDays);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(SeriousFlag);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(DiseaseCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(DiseaseName);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDiagnoseDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(DiagnoseDesc);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Diagnose);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(JudgeOrgan);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(JudgeOrganName);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getJudgeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(JudgeDepend);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(MainDiseaseFlag);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ClmFlag);
				break;
			case 27:
				strFieldValue = String.valueOf(ApplyFee);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(OperationFlag);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(SeriousDutyFlag);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(RiskLevel);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(RiskContinueFlag);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(LDCODE);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(LDNAME);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseRelaNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseRelaNo = FValue.trim();
			}
			else
				CaseRelaNo = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("DoctorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctorNo = FValue.trim();
			}
			else
				DoctorNo = null;
		}
		if (FCode.equalsIgnoreCase("DoctorName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctorName = FValue.trim();
			}
			else
				DoctorName = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("AccidentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccidentType = FValue.trim();
			}
			else
				AccidentType = null;
		}
		if (FCode.equalsIgnoreCase("CureNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CureNo = FValue.trim();
			}
			else
				CureNo = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("InHospitalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InHospitalDate = fDate.getDate( FValue );
			}
			else
				InHospitalDate = null;
		}
		if (FCode.equalsIgnoreCase("OutHospitalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OutHospitalDate = fDate.getDate( FValue );
			}
			else
				OutHospitalDate = null;
		}
		if (FCode.equalsIgnoreCase("InHospitalDays"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InHospitalDays = i;
			}
		}
		if (FCode.equalsIgnoreCase("SeriousFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SeriousFlag = FValue.trim();
			}
			else
				SeriousFlag = null;
		}
		if (FCode.equalsIgnoreCase("DiseaseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaseCode = FValue.trim();
			}
			else
				DiseaseCode = null;
		}
		if (FCode.equalsIgnoreCase("DiseaseName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaseName = FValue.trim();
			}
			else
				DiseaseName = null;
		}
		if (FCode.equalsIgnoreCase("DiagnoseDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DiagnoseDate = fDate.getDate( FValue );
			}
			else
				DiagnoseDate = null;
		}
		if (FCode.equalsIgnoreCase("DiagnoseDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiagnoseDesc = FValue.trim();
			}
			else
				DiagnoseDesc = null;
		}
		if (FCode.equalsIgnoreCase("Diagnose"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Diagnose = FValue.trim();
			}
			else
				Diagnose = null;
		}
		if (FCode.equalsIgnoreCase("JudgeOrgan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				JudgeOrgan = FValue.trim();
			}
			else
				JudgeOrgan = null;
		}
		if (FCode.equalsIgnoreCase("JudgeOrganName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				JudgeOrganName = FValue.trim();
			}
			else
				JudgeOrganName = null;
		}
		if (FCode.equalsIgnoreCase("JudgeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				JudgeDate = fDate.getDate( FValue );
			}
			else
				JudgeDate = null;
		}
		if (FCode.equalsIgnoreCase("JudgeDepend"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				JudgeDepend = FValue.trim();
			}
			else
				JudgeDepend = null;
		}
		if (FCode.equalsIgnoreCase("MainDiseaseFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainDiseaseFlag = FValue.trim();
			}
			else
				MainDiseaseFlag = null;
		}
		if (FCode.equalsIgnoreCase("ClmFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClmFlag = FValue.trim();
			}
			else
				ClmFlag = null;
		}
		if (FCode.equalsIgnoreCase("ApplyFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ApplyFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("OperationFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperationFlag = FValue.trim();
			}
			else
				OperationFlag = null;
		}
		if (FCode.equalsIgnoreCase("SeriousDutyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SeriousDutyFlag = FValue.trim();
			}
			else
				SeriousDutyFlag = null;
		}
		if (FCode.equalsIgnoreCase("RiskLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskLevel = FValue.trim();
			}
			else
				RiskLevel = null;
		}
		if (FCode.equalsIgnoreCase("RiskContinueFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskContinueFlag = FValue.trim();
			}
			else
				RiskContinueFlag = null;
		}
		if (FCode.equalsIgnoreCase("LDCODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LDCODE = FValue.trim();
			}
			else
				LDCODE = null;
		}
		if (FCode.equalsIgnoreCase("LDNAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LDNAME = FValue.trim();
			}
			else
				LDNAME = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCaseCureSchema other = (LLCaseCureSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (CaseRelaNo == null ? other.getCaseRelaNo() == null : CaseRelaNo.equals(other.getCaseRelaNo()))
			&& (HospitalCode == null ? other.getHospitalCode() == null : HospitalCode.equals(other.getHospitalCode()))
			&& (HospitalName == null ? other.getHospitalName() == null : HospitalName.equals(other.getHospitalName()))
			&& (DoctorNo == null ? other.getDoctorNo() == null : DoctorNo.equals(other.getDoctorNo()))
			&& (DoctorName == null ? other.getDoctorName() == null : DoctorName.equals(other.getDoctorName()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (AccidentType == null ? other.getAccidentType() == null : AccidentType.equals(other.getAccidentType()))
			&& (CureNo == null ? other.getCureNo() == null : CureNo.equals(other.getCureNo()))
			&& (ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (InHospitalDate == null ? other.getInHospitalDate() == null : fDate.getString(InHospitalDate).equals(other.getInHospitalDate()))
			&& (OutHospitalDate == null ? other.getOutHospitalDate() == null : fDate.getString(OutHospitalDate).equals(other.getOutHospitalDate()))
			&& InHospitalDays == other.getInHospitalDays()
			&& (SeriousFlag == null ? other.getSeriousFlag() == null : SeriousFlag.equals(other.getSeriousFlag()))
			&& (DiseaseCode == null ? other.getDiseaseCode() == null : DiseaseCode.equals(other.getDiseaseCode()))
			&& (DiseaseName == null ? other.getDiseaseName() == null : DiseaseName.equals(other.getDiseaseName()))
			&& (DiagnoseDate == null ? other.getDiagnoseDate() == null : fDate.getString(DiagnoseDate).equals(other.getDiagnoseDate()))
			&& (DiagnoseDesc == null ? other.getDiagnoseDesc() == null : DiagnoseDesc.equals(other.getDiagnoseDesc()))
			&& (Diagnose == null ? other.getDiagnose() == null : Diagnose.equals(other.getDiagnose()))
			&& (JudgeOrgan == null ? other.getJudgeOrgan() == null : JudgeOrgan.equals(other.getJudgeOrgan()))
			&& (JudgeOrganName == null ? other.getJudgeOrganName() == null : JudgeOrganName.equals(other.getJudgeOrganName()))
			&& (JudgeDate == null ? other.getJudgeDate() == null : fDate.getString(JudgeDate).equals(other.getJudgeDate()))
			&& (JudgeDepend == null ? other.getJudgeDepend() == null : JudgeDepend.equals(other.getJudgeDepend()))
			&& (MainDiseaseFlag == null ? other.getMainDiseaseFlag() == null : MainDiseaseFlag.equals(other.getMainDiseaseFlag()))
			&& (ClmFlag == null ? other.getClmFlag() == null : ClmFlag.equals(other.getClmFlag()))
			&& ApplyFee == other.getApplyFee()
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (OperationFlag == null ? other.getOperationFlag() == null : OperationFlag.equals(other.getOperationFlag()))
			&& (SeriousDutyFlag == null ? other.getSeriousDutyFlag() == null : SeriousDutyFlag.equals(other.getSeriousDutyFlag()))
			&& (RiskLevel == null ? other.getRiskLevel() == null : RiskLevel.equals(other.getRiskLevel()))
			&& (RiskContinueFlag == null ? other.getRiskContinueFlag() == null : RiskContinueFlag.equals(other.getRiskContinueFlag()))
			&& (LDCODE == null ? other.getLDCODE() == null : LDCODE.equals(other.getLDCODE()))
			&& (LDNAME == null ? other.getLDNAME() == null : LDNAME.equals(other.getLDNAME()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 1;
		}
		if( strFieldName.equals("CaseRelaNo") ) {
			return 2;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 3;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 4;
		}
		if( strFieldName.equals("DoctorNo") ) {
			return 5;
		}
		if( strFieldName.equals("DoctorName") ) {
			return 6;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 7;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 8;
		}
		if( strFieldName.equals("AccidentType") ) {
			return 9;
		}
		if( strFieldName.equals("CureNo") ) {
			return 10;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 11;
		}
		if( strFieldName.equals("InHospitalDate") ) {
			return 12;
		}
		if( strFieldName.equals("OutHospitalDate") ) {
			return 13;
		}
		if( strFieldName.equals("InHospitalDays") ) {
			return 14;
		}
		if( strFieldName.equals("SeriousFlag") ) {
			return 15;
		}
		if( strFieldName.equals("DiseaseCode") ) {
			return 16;
		}
		if( strFieldName.equals("DiseaseName") ) {
			return 17;
		}
		if( strFieldName.equals("DiagnoseDate") ) {
			return 18;
		}
		if( strFieldName.equals("DiagnoseDesc") ) {
			return 19;
		}
		if( strFieldName.equals("Diagnose") ) {
			return 20;
		}
		if( strFieldName.equals("JudgeOrgan") ) {
			return 21;
		}
		if( strFieldName.equals("JudgeOrganName") ) {
			return 22;
		}
		if( strFieldName.equals("JudgeDate") ) {
			return 23;
		}
		if( strFieldName.equals("JudgeDepend") ) {
			return 24;
		}
		if( strFieldName.equals("MainDiseaseFlag") ) {
			return 25;
		}
		if( strFieldName.equals("ClmFlag") ) {
			return 26;
		}
		if( strFieldName.equals("ApplyFee") ) {
			return 27;
		}
		if( strFieldName.equals("MngCom") ) {
			return 28;
		}
		if( strFieldName.equals("Operator") ) {
			return 29;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 30;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 31;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 32;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 33;
		}
		if( strFieldName.equals("OperationFlag") ) {
			return 34;
		}
		if( strFieldName.equals("SeriousDutyFlag") ) {
			return 35;
		}
		if( strFieldName.equals("RiskLevel") ) {
			return 36;
		}
		if( strFieldName.equals("RiskContinueFlag") ) {
			return 37;
		}
		if( strFieldName.equals("LDCODE") ) {
			return 38;
		}
		if( strFieldName.equals("LDNAME") ) {
			return 39;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "CaseNo";
				break;
			case 2:
				strFieldName = "CaseRelaNo";
				break;
			case 3:
				strFieldName = "HospitalCode";
				break;
			case 4:
				strFieldName = "HospitalName";
				break;
			case 5:
				strFieldName = "DoctorNo";
				break;
			case 6:
				strFieldName = "DoctorName";
				break;
			case 7:
				strFieldName = "CustomerNo";
				break;
			case 8:
				strFieldName = "CustomerName";
				break;
			case 9:
				strFieldName = "AccidentType";
				break;
			case 10:
				strFieldName = "CureNo";
				break;
			case 11:
				strFieldName = "ReceiptNo";
				break;
			case 12:
				strFieldName = "InHospitalDate";
				break;
			case 13:
				strFieldName = "OutHospitalDate";
				break;
			case 14:
				strFieldName = "InHospitalDays";
				break;
			case 15:
				strFieldName = "SeriousFlag";
				break;
			case 16:
				strFieldName = "DiseaseCode";
				break;
			case 17:
				strFieldName = "DiseaseName";
				break;
			case 18:
				strFieldName = "DiagnoseDate";
				break;
			case 19:
				strFieldName = "DiagnoseDesc";
				break;
			case 20:
				strFieldName = "Diagnose";
				break;
			case 21:
				strFieldName = "JudgeOrgan";
				break;
			case 22:
				strFieldName = "JudgeOrganName";
				break;
			case 23:
				strFieldName = "JudgeDate";
				break;
			case 24:
				strFieldName = "JudgeDepend";
				break;
			case 25:
				strFieldName = "MainDiseaseFlag";
				break;
			case 26:
				strFieldName = "ClmFlag";
				break;
			case 27:
				strFieldName = "ApplyFee";
				break;
			case 28:
				strFieldName = "MngCom";
				break;
			case 29:
				strFieldName = "Operator";
				break;
			case 30:
				strFieldName = "MakeDate";
				break;
			case 31:
				strFieldName = "MakeTime";
				break;
			case 32:
				strFieldName = "ModifyDate";
				break;
			case 33:
				strFieldName = "ModifyTime";
				break;
			case 34:
				strFieldName = "OperationFlag";
				break;
			case 35:
				strFieldName = "SeriousDutyFlag";
				break;
			case 36:
				strFieldName = "RiskLevel";
				break;
			case 37:
				strFieldName = "RiskContinueFlag";
				break;
			case 38:
				strFieldName = "LDCODE";
				break;
			case 39:
				strFieldName = "LDNAME";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseRelaNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctorName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccidentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CureNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InHospitalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OutHospitalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InHospitalDays") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SeriousFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiseaseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiseaseName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiagnoseDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DiagnoseDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Diagnose") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JudgeOrgan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JudgeOrganName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JudgeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("JudgeDepend") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainDiseaseFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClmFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OperationFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SeriousDutyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskContinueFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LDCODE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LDNAME") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_INT;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
