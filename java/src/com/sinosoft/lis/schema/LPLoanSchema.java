/*
 * <p>ClassName: LPLoanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全管理
 * @CreateDate：2004-12-21
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPLoanDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LPLoanSchema implements Schema
{
    // @Field
    /** 保单号码 */
    private String PolNo;
    /** 批改类型 */
    private String EdorType;
    /** 流水号码 */
    private String SerialNo;
    /** 实付号码 */
    private String ActuGetNo;
    /** 业务类型 */
    private String LoanType;
    /** 顺序号码 */
    private String OrderNo;
    /** 借款日期 */
    private Date LoanDate;
    /** 还清日期 */
    private Date PayOffDate;
    /** 总金额 */
    private double SumMoney;
    /** 按录入还是描述计算 */
    private String InputFlag;
    /** 借款利息方式 */
    private String InterestType;
    /** 默认借款利率 */
    private double InterestRate;
    /** 利率类型 */
    private String InterestMode;
    /** 计算利率类型 */
    private String RateCalType;
    /** 计算利率编码 */
    private String RateCalCode;
    /** 是否按固定利率计算 */
    private String SpecifyRate;
    /** 余额 */
    private double LeaveMoney;
    /** 还清标志 */
    private String PayOffFlag;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 批单号 */
    private String EdorNo;

    public static final int FIELDNUM = 24; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPLoanSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "PolNo";
        pk[1] = "EdorType";
        pk[2] = "EdorNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getEdorType()
    {
        if (EdorType != null && !EdorType.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getActuGetNo()
    {
        if (ActuGetNo != null && !ActuGetNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ActuGetNo = StrTool.unicodeToGBK(ActuGetNo);
        }
        return ActuGetNo;
    }

    public void setActuGetNo(String aActuGetNo)
    {
        ActuGetNo = aActuGetNo;
    }

    public String getLoanType()
    {
        if (LoanType != null && !LoanType.equals("") && SysConst.CHANGECHARSET == true)
        {
            LoanType = StrTool.unicodeToGBK(LoanType);
        }
        return LoanType;
    }

    public void setLoanType(String aLoanType)
    {
        LoanType = aLoanType;
    }

    public String getOrderNo()
    {
        if (OrderNo != null && !OrderNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OrderNo = StrTool.unicodeToGBK(OrderNo);
        }
        return OrderNo;
    }

    public void setOrderNo(String aOrderNo)
    {
        OrderNo = aOrderNo;
    }

    public String getLoanDate()
    {
        if (LoanDate != null)
        {
            return fDate.getString(LoanDate);
        }
        else
        {
            return null;
        }
    }

    public void setLoanDate(Date aLoanDate)
    {
        LoanDate = aLoanDate;
    }

    public void setLoanDate(String aLoanDate)
    {
        if (aLoanDate != null && !aLoanDate.equals(""))
        {
            LoanDate = fDate.getDate(aLoanDate);
        }
        else
        {
            LoanDate = null;
        }
    }

    public String getPayOffDate()
    {
        if (PayOffDate != null)
        {
            return fDate.getString(PayOffDate);
        }
        else
        {
            return null;
        }
    }

    public void setPayOffDate(Date aPayOffDate)
    {
        PayOffDate = aPayOffDate;
    }

    public void setPayOffDate(String aPayOffDate)
    {
        if (aPayOffDate != null && !aPayOffDate.equals(""))
        {
            PayOffDate = fDate.getDate(aPayOffDate);
        }
        else
        {
            PayOffDate = null;
        }
    }

    public double getSumMoney()
    {
        return SumMoney;
    }

    public void setSumMoney(double aSumMoney)
    {
        SumMoney = aSumMoney;
    }

    public void setSumMoney(String aSumMoney)
    {
        if (aSumMoney != null && !aSumMoney.equals(""))
        {
            Double tDouble = new Double(aSumMoney);
            double d = tDouble.doubleValue();
            SumMoney = d;
        }
    }

    public String getInputFlag()
    {
        if (InputFlag != null && !InputFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InputFlag = StrTool.unicodeToGBK(InputFlag);
        }
        return InputFlag;
    }

    public void setInputFlag(String aInputFlag)
    {
        InputFlag = aInputFlag;
    }

    public String getInterestType()
    {
        if (InterestType != null && !InterestType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InterestType = StrTool.unicodeToGBK(InterestType);
        }
        return InterestType;
    }

    public void setInterestType(String aInterestType)
    {
        InterestType = aInterestType;
    }

    public double getInterestRate()
    {
        return InterestRate;
    }

    public void setInterestRate(double aInterestRate)
    {
        InterestRate = aInterestRate;
    }

    public void setInterestRate(String aInterestRate)
    {
        if (aInterestRate != null && !aInterestRate.equals(""))
        {
            Double tDouble = new Double(aInterestRate);
            double d = tDouble.doubleValue();
            InterestRate = d;
        }
    }

    public String getInterestMode()
    {
        if (InterestMode != null && !InterestMode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InterestMode = StrTool.unicodeToGBK(InterestMode);
        }
        return InterestMode;
    }

    public void setInterestMode(String aInterestMode)
    {
        InterestMode = aInterestMode;
    }

    public String getRateCalType()
    {
        if (RateCalType != null && !RateCalType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RateCalType = StrTool.unicodeToGBK(RateCalType);
        }
        return RateCalType;
    }

    public void setRateCalType(String aRateCalType)
    {
        RateCalType = aRateCalType;
    }

    public String getRateCalCode()
    {
        if (RateCalCode != null && !RateCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RateCalCode = StrTool.unicodeToGBK(RateCalCode);
        }
        return RateCalCode;
    }

    public void setRateCalCode(String aRateCalCode)
    {
        RateCalCode = aRateCalCode;
    }

    public String getSpecifyRate()
    {
        if (SpecifyRate != null && !SpecifyRate.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SpecifyRate = StrTool.unicodeToGBK(SpecifyRate);
        }
        return SpecifyRate;
    }

    public void setSpecifyRate(String aSpecifyRate)
    {
        SpecifyRate = aSpecifyRate;
    }

    public double getLeaveMoney()
    {
        return LeaveMoney;
    }

    public void setLeaveMoney(double aLeaveMoney)
    {
        LeaveMoney = aLeaveMoney;
    }

    public void setLeaveMoney(String aLeaveMoney)
    {
        if (aLeaveMoney != null && !aLeaveMoney.equals(""))
        {
            Double tDouble = new Double(aLeaveMoney);
            double d = tDouble.doubleValue();
            LeaveMoney = d;
        }
    }

    public String getPayOffFlag()
    {
        if (PayOffFlag != null && !PayOffFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayOffFlag = StrTool.unicodeToGBK(PayOffFlag);
        }
        return PayOffFlag;
    }

    public void setPayOffFlag(String aPayOffFlag)
    {
        PayOffFlag = aPayOffFlag;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    /**
     * 使用另外一个 LPLoanSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LPLoanSchema aLPLoanSchema)
    {
        this.PolNo = aLPLoanSchema.getPolNo();
        this.EdorType = aLPLoanSchema.getEdorType();
        this.SerialNo = aLPLoanSchema.getSerialNo();
        this.ActuGetNo = aLPLoanSchema.getActuGetNo();
        this.LoanType = aLPLoanSchema.getLoanType();
        this.OrderNo = aLPLoanSchema.getOrderNo();
        this.LoanDate = fDate.getDate(aLPLoanSchema.getLoanDate());
        this.PayOffDate = fDate.getDate(aLPLoanSchema.getPayOffDate());
        this.SumMoney = aLPLoanSchema.getSumMoney();
        this.InputFlag = aLPLoanSchema.getInputFlag();
        this.InterestType = aLPLoanSchema.getInterestType();
        this.InterestRate = aLPLoanSchema.getInterestRate();
        this.InterestMode = aLPLoanSchema.getInterestMode();
        this.RateCalType = aLPLoanSchema.getRateCalType();
        this.RateCalCode = aLPLoanSchema.getRateCalCode();
        this.SpecifyRate = aLPLoanSchema.getSpecifyRate();
        this.LeaveMoney = aLPLoanSchema.getLeaveMoney();
        this.PayOffFlag = aLPLoanSchema.getPayOffFlag();
        this.Operator = aLPLoanSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPLoanSchema.getMakeDate());
        this.MakeTime = aLPLoanSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPLoanSchema.getModifyDate());
        this.ModifyTime = aLPLoanSchema.getModifyTime();
        this.EdorNo = aLPLoanSchema.getEdorNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("ActuGetNo") == null)
            {
                this.ActuGetNo = null;
            }
            else
            {
                this.ActuGetNo = rs.getString("ActuGetNo").trim();
            }

            if (rs.getString("LoanType") == null)
            {
                this.LoanType = null;
            }
            else
            {
                this.LoanType = rs.getString("LoanType").trim();
            }

            if (rs.getString("OrderNo") == null)
            {
                this.OrderNo = null;
            }
            else
            {
                this.OrderNo = rs.getString("OrderNo").trim();
            }

            this.LoanDate = rs.getDate("LoanDate");
            this.PayOffDate = rs.getDate("PayOffDate");
            this.SumMoney = rs.getDouble("SumMoney");
            if (rs.getString("InputFlag") == null)
            {
                this.InputFlag = null;
            }
            else
            {
                this.InputFlag = rs.getString("InputFlag").trim();
            }

            if (rs.getString("InterestType") == null)
            {
                this.InterestType = null;
            }
            else
            {
                this.InterestType = rs.getString("InterestType").trim();
            }

            this.InterestRate = rs.getDouble("InterestRate");
            if (rs.getString("InterestMode") == null)
            {
                this.InterestMode = null;
            }
            else
            {
                this.InterestMode = rs.getString("InterestMode").trim();
            }

            if (rs.getString("RateCalType") == null)
            {
                this.RateCalType = null;
            }
            else
            {
                this.RateCalType = rs.getString("RateCalType").trim();
            }

            if (rs.getString("RateCalCode") == null)
            {
                this.RateCalCode = null;
            }
            else
            {
                this.RateCalCode = rs.getString("RateCalCode").trim();
            }

            if (rs.getString("SpecifyRate") == null)
            {
                this.SpecifyRate = null;
            }
            else
            {
                this.SpecifyRate = rs.getString("SpecifyRate").trim();
            }

            this.LeaveMoney = rs.getDouble("LeaveMoney");
            if (rs.getString("PayOffFlag") == null)
            {
                this.PayOffFlag = null;
            }
            else
            {
                this.PayOffFlag = rs.getString("PayOffFlag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPLoanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPLoanSchema getSchema()
    {
        LPLoanSchema aLPLoanSchema = new LPLoanSchema();
        aLPLoanSchema.setSchema(this);
        return aLPLoanSchema;
    }

    public LPLoanDB getDB()
    {
        LPLoanDB aDBOper = new LPLoanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPLoan描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ActuGetNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(LoanType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OrderNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(LoanDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            PayOffDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SumMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InputFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InterestType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(InterestRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InterestMode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RateCalType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RateCalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SpecifyRate)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(LeaveMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayOffFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorNo));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPLoan>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            LoanType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            OrderNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            LoanDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            PayOffDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            InputFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            InterestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            InterestRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            InterestMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            RateCalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
            RateCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            SpecifyRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            LeaveMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            PayOffFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                        SysConst.PACKAGESPILTER);
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPLoanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorType));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("ActuGetNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActuGetNo));
        }
        if (FCode.equals("LoanType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LoanType));
        }
        if (FCode.equals("OrderNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OrderNo));
        }
        if (FCode.equals("LoanDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getLoanDate()));
        }
        if (FCode.equals("PayOffDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getPayOffDate()));
        }
        if (FCode.equals("SumMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SumMoney));
        }
        if (FCode.equals("InputFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InputFlag));
        }
        if (FCode.equals("InterestType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InterestType));
        }
        if (FCode.equals("InterestRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InterestRate));
        }
        if (FCode.equals("InterestMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InterestMode));
        }
        if (FCode.equals("RateCalType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RateCalType));
        }
        if (FCode.equals("RateCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RateCalCode));
        }
        if (FCode.equals("SpecifyRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SpecifyRate));
        }
        if (FCode.equals("LeaveMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LeaveMoney));
        }
        if (FCode.equals("PayOffFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayOffFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorNo));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(LoanType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(OrderNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLoanDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPayOffDate()));
                break;
            case 8:
                strFieldValue = String.valueOf(SumMoney);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(InputFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InterestType);
                break;
            case 11:
                strFieldValue = String.valueOf(InterestRate);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(InterestMode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RateCalType);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(RateCalCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(SpecifyRate);
                break;
            case 16:
                strFieldValue = String.valueOf(LeaveMoney);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(PayOffFlag);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("ActuGetNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActuGetNo = FValue.trim();
            }
            else
            {
                ActuGetNo = null;
            }
        }
        if (FCode.equals("LoanType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LoanType = FValue.trim();
            }
            else
            {
                LoanType = null;
            }
        }
        if (FCode.equals("OrderNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OrderNo = FValue.trim();
            }
            else
            {
                OrderNo = null;
            }
        }
        if (FCode.equals("LoanDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LoanDate = fDate.getDate(FValue);
            }
            else
            {
                LoanDate = null;
            }
        }
        if (FCode.equals("PayOffDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayOffDate = fDate.getDate(FValue);
            }
            else
            {
                PayOffDate = null;
            }
        }
        if (FCode.equals("SumMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumMoney = d;
            }
        }
        if (FCode.equals("InputFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InputFlag = FValue.trim();
            }
            else
            {
                InputFlag = null;
            }
        }
        if (FCode.equals("InterestType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InterestType = FValue.trim();
            }
            else
            {
                InterestType = null;
            }
        }
        if (FCode.equals("InterestRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                InterestRate = d;
            }
        }
        if (FCode.equals("InterestMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InterestMode = FValue.trim();
            }
            else
            {
                InterestMode = null;
            }
        }
        if (FCode.equals("RateCalType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RateCalType = FValue.trim();
            }
            else
            {
                RateCalType = null;
            }
        }
        if (FCode.equals("RateCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RateCalCode = FValue.trim();
            }
            else
            {
                RateCalCode = null;
            }
        }
        if (FCode.equals("SpecifyRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SpecifyRate = FValue.trim();
            }
            else
            {
                SpecifyRate = null;
            }
        }
        if (FCode.equals("LeaveMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LeaveMoney = d;
            }
        }
        if (FCode.equals("PayOffFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayOffFlag = FValue.trim();
            }
            else
            {
                PayOffFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPLoanSchema other = (LPLoanSchema) otherObject;
        return
                PolNo.equals(other.getPolNo())
                && EdorType.equals(other.getEdorType())
                && SerialNo.equals(other.getSerialNo())
                && ActuGetNo.equals(other.getActuGetNo())
                && LoanType.equals(other.getLoanType())
                && OrderNo.equals(other.getOrderNo())
                && fDate.getString(LoanDate).equals(other.getLoanDate())
                && fDate.getString(PayOffDate).equals(other.getPayOffDate())
                && SumMoney == other.getSumMoney()
                && InputFlag.equals(other.getInputFlag())
                && InterestType.equals(other.getInterestType())
                && InterestRate == other.getInterestRate()
                && InterestMode.equals(other.getInterestMode())
                && RateCalType.equals(other.getRateCalType())
                && RateCalCode.equals(other.getRateCalCode())
                && SpecifyRate.equals(other.getSpecifyRate())
                && LeaveMoney == other.getLeaveMoney()
                && PayOffFlag.equals(other.getPayOffFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && EdorNo.equals(other.getEdorNo());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PolNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 1;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 2;
        }
        if (strFieldName.equals("ActuGetNo"))
        {
            return 3;
        }
        if (strFieldName.equals("LoanType"))
        {
            return 4;
        }
        if (strFieldName.equals("OrderNo"))
        {
            return 5;
        }
        if (strFieldName.equals("LoanDate"))
        {
            return 6;
        }
        if (strFieldName.equals("PayOffDate"))
        {
            return 7;
        }
        if (strFieldName.equals("SumMoney"))
        {
            return 8;
        }
        if (strFieldName.equals("InputFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("InterestType"))
        {
            return 10;
        }
        if (strFieldName.equals("InterestRate"))
        {
            return 11;
        }
        if (strFieldName.equals("InterestMode"))
        {
            return 12;
        }
        if (strFieldName.equals("RateCalType"))
        {
            return 13;
        }
        if (strFieldName.equals("RateCalCode"))
        {
            return 14;
        }
        if (strFieldName.equals("SpecifyRate"))
        {
            return 15;
        }
        if (strFieldName.equals("LeaveMoney"))
        {
            return 16;
        }
        if (strFieldName.equals("PayOffFlag"))
        {
            return 17;
        }
        if (strFieldName.equals("Operator"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 19;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 21;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 22;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return 23;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PolNo";
                break;
            case 1:
                strFieldName = "EdorType";
                break;
            case 2:
                strFieldName = "SerialNo";
                break;
            case 3:
                strFieldName = "ActuGetNo";
                break;
            case 4:
                strFieldName = "LoanType";
                break;
            case 5:
                strFieldName = "OrderNo";
                break;
            case 6:
                strFieldName = "LoanDate";
                break;
            case 7:
                strFieldName = "PayOffDate";
                break;
            case 8:
                strFieldName = "SumMoney";
                break;
            case 9:
                strFieldName = "InputFlag";
                break;
            case 10:
                strFieldName = "InterestType";
                break;
            case 11:
                strFieldName = "InterestRate";
                break;
            case 12:
                strFieldName = "InterestMode";
                break;
            case 13:
                strFieldName = "RateCalType";
                break;
            case 14:
                strFieldName = "RateCalCode";
                break;
            case 15:
                strFieldName = "SpecifyRate";
                break;
            case 16:
                strFieldName = "LeaveMoney";
                break;
            case 17:
                strFieldName = "PayOffFlag";
                break;
            case 18:
                strFieldName = "Operator";
                break;
            case 19:
                strFieldName = "MakeDate";
                break;
            case 20:
                strFieldName = "MakeTime";
                break;
            case 21:
                strFieldName = "ModifyDate";
                break;
            case 22:
                strFieldName = "ModifyTime";
                break;
            case 23:
                strFieldName = "EdorNo";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActuGetNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LoanType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OrderNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LoanDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PayOffDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SumMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("InputFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("InterestMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RateCalType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RateCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SpecifyRate"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LeaveMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PayOffFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
