/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TPH_LPCoverageDB;

/*
 * <p>ClassName: TPH_LPCoverageSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 税优平台表机构变更
 * @CreateDate：2015-10-14
 */
public class TPH_LPCoverageSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String policyNo;
	/** 保全类型 */
	private String endorsementType;
	/** 保全批单号 */
	private String endorsementNo;
	/** 分单号 */
	private String sequenceNo;
	/** 被保人客户编码 */
	private String customerNo;
	/** 公司产品组代码 */
	private String coveragePackageCode;
	/** 公司险种代码 */
	private String comCoverageCode;
	/** 公司险种名称 */
	private String comCoverageName;
	/** 险种保费 */
	private double coveragePremium;
	/** 投保年龄 */
	private int ageOfInception;
	/** 险种生效日期 */
	private Date coverageEffectiveDate;
	/** 险种满期日 */
	private Date coverageExpirationDate;
	/** 险种终止日期 */
	private Date terminationDate;
	/** 险种中止日期 */
	private Date suspendDate;
	/** 险种效力恢复日期 */
	private Date recoverDate;
	/** 主附险标志 */
	private String coverageType;
	/** 险种保额 */
	private double sa;
	/** 险种终身保额 */
	private double wholeLifeSa;
	/** 下期应缴日 */
	private Date payDueDay;
	/** 是否允许自动续保 */
	private String renewIndi;
	/** 险种状态 */
	private String coverageStatus;
	/** 险种终止原因 */
	private String terminationReason;
	/** 险种退保金 */
	private double surrenderAmount;
	/** 处理日期 */
	private Date makeDate;
	/** 处理时间 */
	private String makeTime;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TPH_LPCoverageSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[6];
		pk[0] = "policyNo";
		pk[1] = "endorsementType";
		pk[2] = "endorsementNo";
		pk[3] = "sequenceNo";
		pk[4] = "customerNo";
		pk[5] = "comCoverageCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TPH_LPCoverageSchema cloned = (TPH_LPCoverageSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getpolicyNo()
	{
		return policyNo;
	}
	public void setpolicyNo(String apolicyNo)
	{
		policyNo = apolicyNo;
	}
	public String getendorsementType()
	{
		return endorsementType;
	}
	public void setendorsementType(String aendorsementType)
	{
		endorsementType = aendorsementType;
	}
	public String getendorsementNo()
	{
		return endorsementNo;
	}
	public void setendorsementNo(String aendorsementNo)
	{
		endorsementNo = aendorsementNo;
	}
	public String getsequenceNo()
	{
		return sequenceNo;
	}
	public void setsequenceNo(String asequenceNo)
	{
		sequenceNo = asequenceNo;
	}
	public String getcustomerNo()
	{
		return customerNo;
	}
	public void setcustomerNo(String acustomerNo)
	{
		customerNo = acustomerNo;
	}
	public String getcoveragePackageCode()
	{
		return coveragePackageCode;
	}
	public void setcoveragePackageCode(String acoveragePackageCode)
	{
		coveragePackageCode = acoveragePackageCode;
	}
	public String getcomCoverageCode()
	{
		return comCoverageCode;
	}
	public void setcomCoverageCode(String acomCoverageCode)
	{
		comCoverageCode = acomCoverageCode;
	}
	public String getcomCoverageName()
	{
		return comCoverageName;
	}
	public void setcomCoverageName(String acomCoverageName)
	{
		comCoverageName = acomCoverageName;
	}
	public double getcoveragePremium()
	{
		return coveragePremium;
	}
	public void setcoveragePremium(double acoveragePremium)
	{
		coveragePremium = Arith.round(acoveragePremium,2);
	}
	public void setcoveragePremium(String acoveragePremium)
	{
		if (acoveragePremium != null && !acoveragePremium.equals(""))
		{
			Double tDouble = new Double(acoveragePremium);
			double d = tDouble.doubleValue();
                coveragePremium = Arith.round(d,2);
		}
	}

	public int getageOfInception()
	{
		return ageOfInception;
	}
	public void setageOfInception(int aageOfInception)
	{
		ageOfInception = aageOfInception;
	}
	public void setageOfInception(String aageOfInception)
	{
		if (aageOfInception != null && !aageOfInception.equals(""))
		{
			Integer tInteger = new Integer(aageOfInception);
			int i = tInteger.intValue();
			ageOfInception = i;
		}
	}

	public String getcoverageEffectiveDate()
	{
		if( coverageEffectiveDate != null )
			return fDate.getString(coverageEffectiveDate);
		else
			return null;
	}
	public void setcoverageEffectiveDate(Date acoverageEffectiveDate)
	{
		coverageEffectiveDate = acoverageEffectiveDate;
	}
	public void setcoverageEffectiveDate(String acoverageEffectiveDate)
	{
		if (acoverageEffectiveDate != null && !acoverageEffectiveDate.equals("") )
		{
			coverageEffectiveDate = fDate.getDate( acoverageEffectiveDate );
		}
		else
			coverageEffectiveDate = null;
	}

	public String getcoverageExpirationDate()
	{
		if( coverageExpirationDate != null )
			return fDate.getString(coverageExpirationDate);
		else
			return null;
	}
	public void setcoverageExpirationDate(Date acoverageExpirationDate)
	{
		coverageExpirationDate = acoverageExpirationDate;
	}
	public void setcoverageExpirationDate(String acoverageExpirationDate)
	{
		if (acoverageExpirationDate != null && !acoverageExpirationDate.equals("") )
		{
			coverageExpirationDate = fDate.getDate( acoverageExpirationDate );
		}
		else
			coverageExpirationDate = null;
	}

	public String getterminationDate()
	{
		if( terminationDate != null )
			return fDate.getString(terminationDate);
		else
			return null;
	}
	public void setterminationDate(Date aterminationDate)
	{
		terminationDate = aterminationDate;
	}
	public void setterminationDate(String aterminationDate)
	{
		if (aterminationDate != null && !aterminationDate.equals("") )
		{
			terminationDate = fDate.getDate( aterminationDate );
		}
		else
			terminationDate = null;
	}

	public String getsuspendDate()
	{
		if( suspendDate != null )
			return fDate.getString(suspendDate);
		else
			return null;
	}
	public void setsuspendDate(Date asuspendDate)
	{
		suspendDate = asuspendDate;
	}
	public void setsuspendDate(String asuspendDate)
	{
		if (asuspendDate != null && !asuspendDate.equals("") )
		{
			suspendDate = fDate.getDate( asuspendDate );
		}
		else
			suspendDate = null;
	}

	public String getrecoverDate()
	{
		if( recoverDate != null )
			return fDate.getString(recoverDate);
		else
			return null;
	}
	public void setrecoverDate(Date arecoverDate)
	{
		recoverDate = arecoverDate;
	}
	public void setrecoverDate(String arecoverDate)
	{
		if (arecoverDate != null && !arecoverDate.equals("") )
		{
			recoverDate = fDate.getDate( arecoverDate );
		}
		else
			recoverDate = null;
	}

	public String getcoverageType()
	{
		return coverageType;
	}
	public void setcoverageType(String acoverageType)
	{
		coverageType = acoverageType;
	}
	public double getsa()
	{
		return sa;
	}
	public void setsa(double asa)
	{
		sa = Arith.round(asa,2);
	}
	public void setsa(String asa)
	{
		if (asa != null && !asa.equals(""))
		{
			Double tDouble = new Double(asa);
			double d = tDouble.doubleValue();
                sa = Arith.round(d,2);
		}
	}

	public double getwholeLifeSa()
	{
		return wholeLifeSa;
	}
	public void setwholeLifeSa(double awholeLifeSa)
	{
		wholeLifeSa = Arith.round(awholeLifeSa,2);
	}
	public void setwholeLifeSa(String awholeLifeSa)
	{
		if (awholeLifeSa != null && !awholeLifeSa.equals(""))
		{
			Double tDouble = new Double(awholeLifeSa);
			double d = tDouble.doubleValue();
                wholeLifeSa = Arith.round(d,2);
		}
	}

	public String getpayDueDay()
	{
		if( payDueDay != null )
			return fDate.getString(payDueDay);
		else
			return null;
	}
	public void setpayDueDay(Date apayDueDay)
	{
		payDueDay = apayDueDay;
	}
	public void setpayDueDay(String apayDueDay)
	{
		if (apayDueDay != null && !apayDueDay.equals("") )
		{
			payDueDay = fDate.getDate( apayDueDay );
		}
		else
			payDueDay = null;
	}

	public String getrenewIndi()
	{
		return renewIndi;
	}
	public void setrenewIndi(String arenewIndi)
	{
		renewIndi = arenewIndi;
	}
	public String getcoverageStatus()
	{
		return coverageStatus;
	}
	public void setcoverageStatus(String acoverageStatus)
	{
		coverageStatus = acoverageStatus;
	}
	public String getterminationReason()
	{
		return terminationReason;
	}
	public void setterminationReason(String aterminationReason)
	{
		terminationReason = aterminationReason;
	}
	public double getsurrenderAmount()
	{
		return surrenderAmount;
	}
	public void setsurrenderAmount(double asurrenderAmount)
	{
		surrenderAmount = Arith.round(asurrenderAmount,2);
	}
	public void setsurrenderAmount(String asurrenderAmount)
	{
		if (asurrenderAmount != null && !asurrenderAmount.equals(""))
		{
			Double tDouble = new Double(asurrenderAmount);
			double d = tDouble.doubleValue();
                surrenderAmount = Arith.round(d,2);
		}
	}

	public String getmakeDate()
	{
		if( makeDate != null )
			return fDate.getString(makeDate);
		else
			return null;
	}
	public void setmakeDate(Date amakeDate)
	{
		makeDate = amakeDate;
	}
	public void setmakeDate(String amakeDate)
	{
		if (amakeDate != null && !amakeDate.equals("") )
		{
			makeDate = fDate.getDate( amakeDate );
		}
		else
			makeDate = null;
	}

	public String getmakeTime()
	{
		return makeTime;
	}
	public void setmakeTime(String amakeTime)
	{
		makeTime = amakeTime;
	}

	/**
	* 使用另外一个 TPH_LPCoverageSchema 对象给 Schema 赋值
	* @param: aTPH_LPCoverageSchema TPH_LPCoverageSchema
	**/
	public void setSchema(TPH_LPCoverageSchema aTPH_LPCoverageSchema)
	{
		this.policyNo = aTPH_LPCoverageSchema.getpolicyNo();
		this.endorsementType = aTPH_LPCoverageSchema.getendorsementType();
		this.endorsementNo = aTPH_LPCoverageSchema.getendorsementNo();
		this.sequenceNo = aTPH_LPCoverageSchema.getsequenceNo();
		this.customerNo = aTPH_LPCoverageSchema.getcustomerNo();
		this.coveragePackageCode = aTPH_LPCoverageSchema.getcoveragePackageCode();
		this.comCoverageCode = aTPH_LPCoverageSchema.getcomCoverageCode();
		this.comCoverageName = aTPH_LPCoverageSchema.getcomCoverageName();
		this.coveragePremium = aTPH_LPCoverageSchema.getcoveragePremium();
		this.ageOfInception = aTPH_LPCoverageSchema.getageOfInception();
		this.coverageEffectiveDate = fDate.getDate( aTPH_LPCoverageSchema.getcoverageEffectiveDate());
		this.coverageExpirationDate = fDate.getDate( aTPH_LPCoverageSchema.getcoverageExpirationDate());
		this.terminationDate = fDate.getDate( aTPH_LPCoverageSchema.getterminationDate());
		this.suspendDate = fDate.getDate( aTPH_LPCoverageSchema.getsuspendDate());
		this.recoverDate = fDate.getDate( aTPH_LPCoverageSchema.getrecoverDate());
		this.coverageType = aTPH_LPCoverageSchema.getcoverageType();
		this.sa = aTPH_LPCoverageSchema.getsa();
		this.wholeLifeSa = aTPH_LPCoverageSchema.getwholeLifeSa();
		this.payDueDay = fDate.getDate( aTPH_LPCoverageSchema.getpayDueDay());
		this.renewIndi = aTPH_LPCoverageSchema.getrenewIndi();
		this.coverageStatus = aTPH_LPCoverageSchema.getcoverageStatus();
		this.terminationReason = aTPH_LPCoverageSchema.getterminationReason();
		this.surrenderAmount = aTPH_LPCoverageSchema.getsurrenderAmount();
		this.makeDate = fDate.getDate( aTPH_LPCoverageSchema.getmakeDate());
		this.makeTime = aTPH_LPCoverageSchema.getmakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("policyNo") == null )
				this.policyNo = null;
			else
				this.policyNo = rs.getString("policyNo").trim();

			if( rs.getString("endorsementType") == null )
				this.endorsementType = null;
			else
				this.endorsementType = rs.getString("endorsementType").trim();

			if( rs.getString("endorsementNo") == null )
				this.endorsementNo = null;
			else
				this.endorsementNo = rs.getString("endorsementNo").trim();

			if( rs.getString("sequenceNo") == null )
				this.sequenceNo = null;
			else
				this.sequenceNo = rs.getString("sequenceNo").trim();

			if( rs.getString("customerNo") == null )
				this.customerNo = null;
			else
				this.customerNo = rs.getString("customerNo").trim();

			if( rs.getString("coveragePackageCode") == null )
				this.coveragePackageCode = null;
			else
				this.coveragePackageCode = rs.getString("coveragePackageCode").trim();

			if( rs.getString("comCoverageCode") == null )
				this.comCoverageCode = null;
			else
				this.comCoverageCode = rs.getString("comCoverageCode").trim();

			if( rs.getString("comCoverageName") == null )
				this.comCoverageName = null;
			else
				this.comCoverageName = rs.getString("comCoverageName").trim();

			this.coveragePremium = rs.getDouble("coveragePremium");
			this.ageOfInception = rs.getInt("ageOfInception");
			this.coverageEffectiveDate = rs.getDate("coverageEffectiveDate");
			this.coverageExpirationDate = rs.getDate("coverageExpirationDate");
			this.terminationDate = rs.getDate("terminationDate");
			this.suspendDate = rs.getDate("suspendDate");
			this.recoverDate = rs.getDate("recoverDate");
			if( rs.getString("coverageType") == null )
				this.coverageType = null;
			else
				this.coverageType = rs.getString("coverageType").trim();

			this.sa = rs.getDouble("sa");
			this.wholeLifeSa = rs.getDouble("wholeLifeSa");
			this.payDueDay = rs.getDate("payDueDay");
			if( rs.getString("renewIndi") == null )
				this.renewIndi = null;
			else
				this.renewIndi = rs.getString("renewIndi").trim();

			if( rs.getString("coverageStatus") == null )
				this.coverageStatus = null;
			else
				this.coverageStatus = rs.getString("coverageStatus").trim();

			if( rs.getString("terminationReason") == null )
				this.terminationReason = null;
			else
				this.terminationReason = rs.getString("terminationReason").trim();

			this.surrenderAmount = rs.getDouble("surrenderAmount");
			this.makeDate = rs.getDate("makeDate");
			if( rs.getString("makeTime") == null )
				this.makeTime = null;
			else
				this.makeTime = rs.getString("makeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TPH_LPCoverage表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPCoverageSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TPH_LPCoverageSchema getSchema()
	{
		TPH_LPCoverageSchema aTPH_LPCoverageSchema = new TPH_LPCoverageSchema();
		aTPH_LPCoverageSchema.setSchema(this);
		return aTPH_LPCoverageSchema;
	}

	public TPH_LPCoverageDB getDB()
	{
		TPH_LPCoverageDB aDBOper = new TPH_LPCoverageDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPCoverage描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorsementType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(customerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(coveragePackageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comCoverageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comCoverageName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(coveragePremium));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ageOfInception));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( coverageEffectiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( coverageExpirationDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( terminationDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( suspendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( recoverDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(coverageType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(sa));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(wholeLifeSa));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( payDueDay ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(renewIndi)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(coverageStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(terminationReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(surrenderAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPCoverage>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			endorsementType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			endorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			sequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			customerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			coveragePackageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			comCoverageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			comCoverageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			coveragePremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			ageOfInception= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			coverageEffectiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			coverageExpirationDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			terminationDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			suspendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			recoverDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			coverageType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			sa = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			wholeLifeSa = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			payDueDay = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			renewIndi = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			coverageStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			terminationReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			surrenderAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			makeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			makeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPCoverageSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("policyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
		}
		if (FCode.equals("endorsementType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorsementType));
		}
		if (FCode.equals("endorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorsementNo));
		}
		if (FCode.equals("sequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sequenceNo));
		}
		if (FCode.equals("customerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(customerNo));
		}
		if (FCode.equals("coveragePackageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(coveragePackageCode));
		}
		if (FCode.equals("comCoverageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comCoverageCode));
		}
		if (FCode.equals("comCoverageName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comCoverageName));
		}
		if (FCode.equals("coveragePremium"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(coveragePremium));
		}
		if (FCode.equals("ageOfInception"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ageOfInception));
		}
		if (FCode.equals("coverageEffectiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getcoverageEffectiveDate()));
		}
		if (FCode.equals("coverageExpirationDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getcoverageExpirationDate()));
		}
		if (FCode.equals("terminationDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getterminationDate()));
		}
		if (FCode.equals("suspendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getsuspendDate()));
		}
		if (FCode.equals("recoverDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getrecoverDate()));
		}
		if (FCode.equals("coverageType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(coverageType));
		}
		if (FCode.equals("sa"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sa));
		}
		if (FCode.equals("wholeLifeSa"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(wholeLifeSa));
		}
		if (FCode.equals("payDueDay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getpayDueDay()));
		}
		if (FCode.equals("renewIndi"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(renewIndi));
		}
		if (FCode.equals("coverageStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(coverageStatus));
		}
		if (FCode.equals("terminationReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(terminationReason));
		}
		if (FCode.equals("surrenderAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(surrenderAmount));
		}
		if (FCode.equals("makeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
		}
		if (FCode.equals("makeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(policyNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(endorsementType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(endorsementNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(sequenceNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(customerNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(coveragePackageCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(comCoverageCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(comCoverageName);
				break;
			case 8:
				strFieldValue = String.valueOf(coveragePremium);
				break;
			case 9:
				strFieldValue = String.valueOf(ageOfInception);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getcoverageEffectiveDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getcoverageExpirationDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getterminationDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getsuspendDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getrecoverDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(coverageType);
				break;
			case 16:
				strFieldValue = String.valueOf(sa);
				break;
			case 17:
				strFieldValue = String.valueOf(wholeLifeSa);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getpayDueDay()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(renewIndi);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(coverageStatus);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(terminationReason);
				break;
			case 22:
				strFieldValue = String.valueOf(surrenderAmount);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(makeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("policyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyNo = FValue.trim();
			}
			else
				policyNo = null;
		}
		if (FCode.equalsIgnoreCase("endorsementType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorsementType = FValue.trim();
			}
			else
				endorsementType = null;
		}
		if (FCode.equalsIgnoreCase("endorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorsementNo = FValue.trim();
			}
			else
				endorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("sequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sequenceNo = FValue.trim();
			}
			else
				sequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("customerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				customerNo = FValue.trim();
			}
			else
				customerNo = null;
		}
		if (FCode.equalsIgnoreCase("coveragePackageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				coveragePackageCode = FValue.trim();
			}
			else
				coveragePackageCode = null;
		}
		if (FCode.equalsIgnoreCase("comCoverageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comCoverageCode = FValue.trim();
			}
			else
				comCoverageCode = null;
		}
		if (FCode.equalsIgnoreCase("comCoverageName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comCoverageName = FValue.trim();
			}
			else
				comCoverageName = null;
		}
		if (FCode.equalsIgnoreCase("coveragePremium"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				coveragePremium = d;
			}
		}
		if (FCode.equalsIgnoreCase("ageOfInception"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ageOfInception = i;
			}
		}
		if (FCode.equalsIgnoreCase("coverageEffectiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				coverageEffectiveDate = fDate.getDate( FValue );
			}
			else
				coverageEffectiveDate = null;
		}
		if (FCode.equalsIgnoreCase("coverageExpirationDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				coverageExpirationDate = fDate.getDate( FValue );
			}
			else
				coverageExpirationDate = null;
		}
		if (FCode.equalsIgnoreCase("terminationDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				terminationDate = fDate.getDate( FValue );
			}
			else
				terminationDate = null;
		}
		if (FCode.equalsIgnoreCase("suspendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				suspendDate = fDate.getDate( FValue );
			}
			else
				suspendDate = null;
		}
		if (FCode.equalsIgnoreCase("recoverDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				recoverDate = fDate.getDate( FValue );
			}
			else
				recoverDate = null;
		}
		if (FCode.equalsIgnoreCase("coverageType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				coverageType = FValue.trim();
			}
			else
				coverageType = null;
		}
		if (FCode.equalsIgnoreCase("sa"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				sa = d;
			}
		}
		if (FCode.equalsIgnoreCase("wholeLifeSa"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				wholeLifeSa = d;
			}
		}
		if (FCode.equalsIgnoreCase("payDueDay"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				payDueDay = fDate.getDate( FValue );
			}
			else
				payDueDay = null;
		}
		if (FCode.equalsIgnoreCase("renewIndi"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				renewIndi = FValue.trim();
			}
			else
				renewIndi = null;
		}
		if (FCode.equalsIgnoreCase("coverageStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				coverageStatus = FValue.trim();
			}
			else
				coverageStatus = null;
		}
		if (FCode.equalsIgnoreCase("terminationReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				terminationReason = FValue.trim();
			}
			else
				terminationReason = null;
		}
		if (FCode.equalsIgnoreCase("surrenderAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				surrenderAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("makeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makeDate = fDate.getDate( FValue );
			}
			else
				makeDate = null;
		}
		if (FCode.equalsIgnoreCase("makeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makeTime = FValue.trim();
			}
			else
				makeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TPH_LPCoverageSchema other = (TPH_LPCoverageSchema)otherObject;
		return
			(policyNo == null ? other.getpolicyNo() == null : policyNo.equals(other.getpolicyNo()))
			&& (endorsementType == null ? other.getendorsementType() == null : endorsementType.equals(other.getendorsementType()))
			&& (endorsementNo == null ? other.getendorsementNo() == null : endorsementNo.equals(other.getendorsementNo()))
			&& (sequenceNo == null ? other.getsequenceNo() == null : sequenceNo.equals(other.getsequenceNo()))
			&& (customerNo == null ? other.getcustomerNo() == null : customerNo.equals(other.getcustomerNo()))
			&& (coveragePackageCode == null ? other.getcoveragePackageCode() == null : coveragePackageCode.equals(other.getcoveragePackageCode()))
			&& (comCoverageCode == null ? other.getcomCoverageCode() == null : comCoverageCode.equals(other.getcomCoverageCode()))
			&& (comCoverageName == null ? other.getcomCoverageName() == null : comCoverageName.equals(other.getcomCoverageName()))
			&& coveragePremium == other.getcoveragePremium()
			&& ageOfInception == other.getageOfInception()
			&& (coverageEffectiveDate == null ? other.getcoverageEffectiveDate() == null : fDate.getString(coverageEffectiveDate).equals(other.getcoverageEffectiveDate()))
			&& (coverageExpirationDate == null ? other.getcoverageExpirationDate() == null : fDate.getString(coverageExpirationDate).equals(other.getcoverageExpirationDate()))
			&& (terminationDate == null ? other.getterminationDate() == null : fDate.getString(terminationDate).equals(other.getterminationDate()))
			&& (suspendDate == null ? other.getsuspendDate() == null : fDate.getString(suspendDate).equals(other.getsuspendDate()))
			&& (recoverDate == null ? other.getrecoverDate() == null : fDate.getString(recoverDate).equals(other.getrecoverDate()))
			&& (coverageType == null ? other.getcoverageType() == null : coverageType.equals(other.getcoverageType()))
			&& sa == other.getsa()
			&& wholeLifeSa == other.getwholeLifeSa()
			&& (payDueDay == null ? other.getpayDueDay() == null : fDate.getString(payDueDay).equals(other.getpayDueDay()))
			&& (renewIndi == null ? other.getrenewIndi() == null : renewIndi.equals(other.getrenewIndi()))
			&& (coverageStatus == null ? other.getcoverageStatus() == null : coverageStatus.equals(other.getcoverageStatus()))
			&& (terminationReason == null ? other.getterminationReason() == null : terminationReason.equals(other.getterminationReason()))
			&& surrenderAmount == other.getsurrenderAmount()
			&& (makeDate == null ? other.getmakeDate() == null : fDate.getString(makeDate).equals(other.getmakeDate()))
			&& (makeTime == null ? other.getmakeTime() == null : makeTime.equals(other.getmakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return 0;
		}
		if( strFieldName.equals("endorsementType") ) {
			return 1;
		}
		if( strFieldName.equals("endorsementNo") ) {
			return 2;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return 3;
		}
		if( strFieldName.equals("customerNo") ) {
			return 4;
		}
		if( strFieldName.equals("coveragePackageCode") ) {
			return 5;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return 6;
		}
		if( strFieldName.equals("comCoverageName") ) {
			return 7;
		}
		if( strFieldName.equals("coveragePremium") ) {
			return 8;
		}
		if( strFieldName.equals("ageOfInception") ) {
			return 9;
		}
		if( strFieldName.equals("coverageEffectiveDate") ) {
			return 10;
		}
		if( strFieldName.equals("coverageExpirationDate") ) {
			return 11;
		}
		if( strFieldName.equals("terminationDate") ) {
			return 12;
		}
		if( strFieldName.equals("suspendDate") ) {
			return 13;
		}
		if( strFieldName.equals("recoverDate") ) {
			return 14;
		}
		if( strFieldName.equals("coverageType") ) {
			return 15;
		}
		if( strFieldName.equals("sa") ) {
			return 16;
		}
		if( strFieldName.equals("wholeLifeSa") ) {
			return 17;
		}
		if( strFieldName.equals("payDueDay") ) {
			return 18;
		}
		if( strFieldName.equals("renewIndi") ) {
			return 19;
		}
		if( strFieldName.equals("coverageStatus") ) {
			return 20;
		}
		if( strFieldName.equals("terminationReason") ) {
			return 21;
		}
		if( strFieldName.equals("surrenderAmount") ) {
			return 22;
		}
		if( strFieldName.equals("makeDate") ) {
			return 23;
		}
		if( strFieldName.equals("makeTime") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "policyNo";
				break;
			case 1:
				strFieldName = "endorsementType";
				break;
			case 2:
				strFieldName = "endorsementNo";
				break;
			case 3:
				strFieldName = "sequenceNo";
				break;
			case 4:
				strFieldName = "customerNo";
				break;
			case 5:
				strFieldName = "coveragePackageCode";
				break;
			case 6:
				strFieldName = "comCoverageCode";
				break;
			case 7:
				strFieldName = "comCoverageName";
				break;
			case 8:
				strFieldName = "coveragePremium";
				break;
			case 9:
				strFieldName = "ageOfInception";
				break;
			case 10:
				strFieldName = "coverageEffectiveDate";
				break;
			case 11:
				strFieldName = "coverageExpirationDate";
				break;
			case 12:
				strFieldName = "terminationDate";
				break;
			case 13:
				strFieldName = "suspendDate";
				break;
			case 14:
				strFieldName = "recoverDate";
				break;
			case 15:
				strFieldName = "coverageType";
				break;
			case 16:
				strFieldName = "sa";
				break;
			case 17:
				strFieldName = "wholeLifeSa";
				break;
			case 18:
				strFieldName = "payDueDay";
				break;
			case 19:
				strFieldName = "renewIndi";
				break;
			case 20:
				strFieldName = "coverageStatus";
				break;
			case 21:
				strFieldName = "terminationReason";
				break;
			case 22:
				strFieldName = "surrenderAmount";
				break;
			case 23:
				strFieldName = "makeDate";
				break;
			case 24:
				strFieldName = "makeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("customerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("coveragePackageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comCoverageName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("coveragePremium") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ageOfInception") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("coverageEffectiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("coverageExpirationDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("terminationDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("suspendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("recoverDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("coverageType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sa") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("wholeLifeSa") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("payDueDay") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("renewIndi") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("coverageStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("terminationReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("surrenderAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("makeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("makeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
