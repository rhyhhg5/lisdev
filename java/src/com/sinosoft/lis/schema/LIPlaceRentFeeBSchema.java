/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIPlaceRentFeeBDB;

/*
 * <p>ClassName: LIPlaceRentFeeBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 职场管理
 * @CreateDate：2012-08-22
 */
public class LIPlaceRentFeeBSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String Serialno;
	/** 职场编码 */
	private String Placeno;
	/** 费用类型 */
	private String Feetype;
	/** 序列号 */
	private String Order;
	/** 支付日期 */
	private Date PayDate;
	/** 支付金额 */
	private double Money;
	/** 支付租金摊销起期 */
	private Date Paybegindate;
	/** 支付租金摊销止期 */
	private Date Payenddate;
	/** 关联职场编码 */
	private String Placenorefer;
	/** 操作类型 */
	private String Dealtype;
	/** 操作状态 */
	private String Dealstate;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date Makedate;
	/** 入机时间 */
	private String Maketime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 备用字符串类型1 */
	private String Standbystring1;
	/** 备用字符串类型2 */
	private String Standbystring2;
	/** 备用数字类型1 */
	private double Standbynum1;
	/** 备用数字类型2 */
	private double Standbynum2;
	/** 备用日期类型1 */
	private Date Standbydate1;
	/** 备用日期类型2 */
	private Date Standbydate2;

	public static final int FIELDNUM = 22;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIPlaceRentFeeBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Serialno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LIPlaceRentFeeBSchema cloned = (LIPlaceRentFeeBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialno()
	{
		return Serialno;
	}
	public void setSerialno(String aSerialno)
	{
		Serialno = aSerialno;
	}
	public String getPlaceno()
	{
		return Placeno;
	}
	public void setPlaceno(String aPlaceno)
	{
		Placeno = aPlaceno;
	}
	public String getFeetype()
	{
		return Feetype;
	}
	public void setFeetype(String aFeetype)
	{
		Feetype = aFeetype;
	}
	public String getOrder()
	{
		return Order;
	}
	public void setOrder(String aOrder)
	{
		Order = aOrder;
	}
	public String getPayDate()
	{
		if( PayDate != null )
			return fDate.getString(PayDate);
		else
			return null;
	}
	public void setPayDate(Date aPayDate)
	{
		PayDate = aPayDate;
	}
	public void setPayDate(String aPayDate)
	{
		if (aPayDate != null && !aPayDate.equals("") )
		{
			PayDate = fDate.getDate( aPayDate );
		}
		else
			PayDate = null;
	}

	public double getMoney()
	{
		return Money;
	}
	public void setMoney(double aMoney)
	{
		Money = Arith.round(aMoney,2);
	}
	public void setMoney(String aMoney)
	{
		if (aMoney != null && !aMoney.equals(""))
		{
			Double tDouble = new Double(aMoney);
			double d = tDouble.doubleValue();
                Money = Arith.round(d,2);
		}
	}

	public String getPaybegindate()
	{
		if( Paybegindate != null )
			return fDate.getString(Paybegindate);
		else
			return null;
	}
	public void setPaybegindate(Date aPaybegindate)
	{
		Paybegindate = aPaybegindate;
	}
	public void setPaybegindate(String aPaybegindate)
	{
		if (aPaybegindate != null && !aPaybegindate.equals("") )
		{
			Paybegindate = fDate.getDate( aPaybegindate );
		}
		else
			Paybegindate = null;
	}

	public String getPayenddate()
	{
		if( Payenddate != null )
			return fDate.getString(Payenddate);
		else
			return null;
	}
	public void setPayenddate(Date aPayenddate)
	{
		Payenddate = aPayenddate;
	}
	public void setPayenddate(String aPayenddate)
	{
		if (aPayenddate != null && !aPayenddate.equals("") )
		{
			Payenddate = fDate.getDate( aPayenddate );
		}
		else
			Payenddate = null;
	}

	public String getPlacenorefer()
	{
		return Placenorefer;
	}
	public void setPlacenorefer(String aPlacenorefer)
	{
		Placenorefer = aPlacenorefer;
	}
	public String getDealtype()
	{
		return Dealtype;
	}
	public void setDealtype(String aDealtype)
	{
		Dealtype = aDealtype;
	}
	public String getDealstate()
	{
		return Dealstate;
	}
	public void setDealstate(String aDealstate)
	{
		Dealstate = aDealstate;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakedate()
	{
		if( Makedate != null )
			return fDate.getString(Makedate);
		else
			return null;
	}
	public void setMakedate(Date aMakedate)
	{
		Makedate = aMakedate;
	}
	public void setMakedate(String aMakedate)
	{
		if (aMakedate != null && !aMakedate.equals("") )
		{
			Makedate = fDate.getDate( aMakedate );
		}
		else
			Makedate = null;
	}

	public String getMaketime()
	{
		return Maketime;
	}
	public void setMaketime(String aMaketime)
	{
		Maketime = aMaketime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStandbystring1()
	{
		return Standbystring1;
	}
	public void setStandbystring1(String aStandbystring1)
	{
		Standbystring1 = aStandbystring1;
	}
	public String getStandbystring2()
	{
		return Standbystring2;
	}
	public void setStandbystring2(String aStandbystring2)
	{
		Standbystring2 = aStandbystring2;
	}
	public double getStandbynum1()
	{
		return Standbynum1;
	}
	public void setStandbynum1(double aStandbynum1)
	{
		Standbynum1 = Arith.round(aStandbynum1,2);
	}
	public void setStandbynum1(String aStandbynum1)
	{
		if (aStandbynum1 != null && !aStandbynum1.equals(""))
		{
			Double tDouble = new Double(aStandbynum1);
			double d = tDouble.doubleValue();
                Standbynum1 = Arith.round(d,2);
		}
	}

	public double getStandbynum2()
	{
		return Standbynum2;
	}
	public void setStandbynum2(double aStandbynum2)
	{
		Standbynum2 = Arith.round(aStandbynum2,2);
	}
	public void setStandbynum2(String aStandbynum2)
	{
		if (aStandbynum2 != null && !aStandbynum2.equals(""))
		{
			Double tDouble = new Double(aStandbynum2);
			double d = tDouble.doubleValue();
                Standbynum2 = Arith.round(d,2);
		}
	}

	public String getStandbydate1()
	{
		if( Standbydate1 != null )
			return fDate.getString(Standbydate1);
		else
			return null;
	}
	public void setStandbydate1(Date aStandbydate1)
	{
		Standbydate1 = aStandbydate1;
	}
	public void setStandbydate1(String aStandbydate1)
	{
		if (aStandbydate1 != null && !aStandbydate1.equals("") )
		{
			Standbydate1 = fDate.getDate( aStandbydate1 );
		}
		else
			Standbydate1 = null;
	}

	public String getStandbydate2()
	{
		if( Standbydate2 != null )
			return fDate.getString(Standbydate2);
		else
			return null;
	}
	public void setStandbydate2(Date aStandbydate2)
	{
		Standbydate2 = aStandbydate2;
	}
	public void setStandbydate2(String aStandbydate2)
	{
		if (aStandbydate2 != null && !aStandbydate2.equals("") )
		{
			Standbydate2 = fDate.getDate( aStandbydate2 );
		}
		else
			Standbydate2 = null;
	}


	/**
	* 使用另外一个 LIPlaceRentFeeBSchema 对象给 Schema 赋值
	* @param: aLIPlaceRentFeeBSchema LIPlaceRentFeeBSchema
	**/
	public void setSchema(LIPlaceRentFeeBSchema aLIPlaceRentFeeBSchema)
	{
		this.Serialno = aLIPlaceRentFeeBSchema.getSerialno();
		this.Placeno = aLIPlaceRentFeeBSchema.getPlaceno();
		this.Feetype = aLIPlaceRentFeeBSchema.getFeetype();
		this.Order = aLIPlaceRentFeeBSchema.getOrder();
		this.PayDate = fDate.getDate( aLIPlaceRentFeeBSchema.getPayDate());
		this.Money = aLIPlaceRentFeeBSchema.getMoney();
		this.Paybegindate = fDate.getDate( aLIPlaceRentFeeBSchema.getPaybegindate());
		this.Payenddate = fDate.getDate( aLIPlaceRentFeeBSchema.getPayenddate());
		this.Placenorefer = aLIPlaceRentFeeBSchema.getPlacenorefer();
		this.Dealtype = aLIPlaceRentFeeBSchema.getDealtype();
		this.Dealstate = aLIPlaceRentFeeBSchema.getDealstate();
		this.Operator = aLIPlaceRentFeeBSchema.getOperator();
		this.Makedate = fDate.getDate( aLIPlaceRentFeeBSchema.getMakedate());
		this.Maketime = aLIPlaceRentFeeBSchema.getMaketime();
		this.ModifyDate = fDate.getDate( aLIPlaceRentFeeBSchema.getModifyDate());
		this.ModifyTime = aLIPlaceRentFeeBSchema.getModifyTime();
		this.Standbystring1 = aLIPlaceRentFeeBSchema.getStandbystring1();
		this.Standbystring2 = aLIPlaceRentFeeBSchema.getStandbystring2();
		this.Standbynum1 = aLIPlaceRentFeeBSchema.getStandbynum1();
		this.Standbynum2 = aLIPlaceRentFeeBSchema.getStandbynum2();
		this.Standbydate1 = fDate.getDate( aLIPlaceRentFeeBSchema.getStandbydate1());
		this.Standbydate2 = fDate.getDate( aLIPlaceRentFeeBSchema.getStandbydate2());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Serialno") == null )
				this.Serialno = null;
			else
				this.Serialno = rs.getString("Serialno").trim();

			if( rs.getString("Placeno") == null )
				this.Placeno = null;
			else
				this.Placeno = rs.getString("Placeno").trim();

			if( rs.getString("Feetype") == null )
				this.Feetype = null;
			else
				this.Feetype = rs.getString("Feetype").trim();

			if( rs.getString("Order") == null )
				this.Order = null;
			else
				this.Order = rs.getString("Order").trim();

			this.PayDate = rs.getDate("PayDate");
			this.Money = rs.getDouble("Money");
			this.Paybegindate = rs.getDate("Paybegindate");
			this.Payenddate = rs.getDate("Payenddate");
			if( rs.getString("Placenorefer") == null )
				this.Placenorefer = null;
			else
				this.Placenorefer = rs.getString("Placenorefer").trim();

			if( rs.getString("Dealtype") == null )
				this.Dealtype = null;
			else
				this.Dealtype = rs.getString("Dealtype").trim();

			if( rs.getString("Dealstate") == null )
				this.Dealstate = null;
			else
				this.Dealstate = rs.getString("Dealstate").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Makedate = rs.getDate("Makedate");
			if( rs.getString("Maketime") == null )
				this.Maketime = null;
			else
				this.Maketime = rs.getString("Maketime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Standbystring1") == null )
				this.Standbystring1 = null;
			else
				this.Standbystring1 = rs.getString("Standbystring1").trim();

			if( rs.getString("Standbystring2") == null )
				this.Standbystring2 = null;
			else
				this.Standbystring2 = rs.getString("Standbystring2").trim();

			this.Standbynum1 = rs.getDouble("Standbynum1");
			this.Standbynum2 = rs.getDouble("Standbynum2");
			this.Standbydate1 = rs.getDate("Standbydate1");
			this.Standbydate2 = rs.getDate("Standbydate2");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIPlaceRentFeeB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPlaceRentFeeBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIPlaceRentFeeBSchema getSchema()
	{
		LIPlaceRentFeeBSchema aLIPlaceRentFeeBSchema = new LIPlaceRentFeeBSchema();
		aLIPlaceRentFeeBSchema.setSchema(this);
		return aLIPlaceRentFeeBSchema;
	}

	public LIPlaceRentFeeBDB getDB()
	{
		LIPlaceRentFeeBDB aDBOper = new LIPlaceRentFeeBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPlaceRentFeeB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Serialno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Placeno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Feetype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Order)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Money));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Paybegindate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Payenddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Placenorefer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dealtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dealstate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate2 )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPlaceRentFeeB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Serialno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Placeno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Feetype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Order = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			Money = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			Paybegindate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			Payenddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			Placenorefer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Dealtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Dealstate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			Maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Standbystring1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Standbystring2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Standbynum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			Standbynum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			Standbydate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			Standbydate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPlaceRentFeeBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Serialno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Serialno));
		}
		if (FCode.equals("Placeno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Placeno));
		}
		if (FCode.equals("Feetype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Feetype));
		}
		if (FCode.equals("Order"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Order));
		}
		if (FCode.equals("PayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
		}
		if (FCode.equals("Money"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Money));
		}
		if (FCode.equals("Paybegindate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPaybegindate()));
		}
		if (FCode.equals("Payenddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayenddate()));
		}
		if (FCode.equals("Placenorefer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Placenorefer));
		}
		if (FCode.equals("Dealtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dealtype));
		}
		if (FCode.equals("Dealstate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dealstate));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
		}
		if (FCode.equals("Maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Maketime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Standbystring1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring1));
		}
		if (FCode.equals("Standbystring2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring2));
		}
		if (FCode.equals("Standbynum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum1));
		}
		if (FCode.equals("Standbynum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum2));
		}
		if (FCode.equals("Standbydate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
		}
		if (FCode.equals("Standbydate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Serialno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Placeno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Feetype);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Order);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
				break;
			case 5:
				strFieldValue = String.valueOf(Money);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPaybegindate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayenddate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Placenorefer);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Dealtype);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Dealstate);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Maketime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Standbystring1);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Standbystring2);
				break;
			case 18:
				strFieldValue = String.valueOf(Standbynum1);
				break;
			case 19:
				strFieldValue = String.valueOf(Standbynum2);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Serialno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Serialno = FValue.trim();
			}
			else
				Serialno = null;
		}
		if (FCode.equalsIgnoreCase("Placeno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Placeno = FValue.trim();
			}
			else
				Placeno = null;
		}
		if (FCode.equalsIgnoreCase("Feetype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Feetype = FValue.trim();
			}
			else
				Feetype = null;
		}
		if (FCode.equalsIgnoreCase("Order"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Order = FValue.trim();
			}
			else
				Order = null;
		}
		if (FCode.equalsIgnoreCase("PayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PayDate = fDate.getDate( FValue );
			}
			else
				PayDate = null;
		}
		if (FCode.equalsIgnoreCase("Money"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Money = d;
			}
		}
		if (FCode.equalsIgnoreCase("Paybegindate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Paybegindate = fDate.getDate( FValue );
			}
			else
				Paybegindate = null;
		}
		if (FCode.equalsIgnoreCase("Payenddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Payenddate = fDate.getDate( FValue );
			}
			else
				Payenddate = null;
		}
		if (FCode.equalsIgnoreCase("Placenorefer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Placenorefer = FValue.trim();
			}
			else
				Placenorefer = null;
		}
		if (FCode.equalsIgnoreCase("Dealtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dealtype = FValue.trim();
			}
			else
				Dealtype = null;
		}
		if (FCode.equalsIgnoreCase("Dealstate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dealstate = FValue.trim();
			}
			else
				Dealstate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Makedate = fDate.getDate( FValue );
			}
			else
				Makedate = null;
		}
		if (FCode.equalsIgnoreCase("Maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Maketime = FValue.trim();
			}
			else
				Maketime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring1 = FValue.trim();
			}
			else
				Standbystring1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring2 = FValue.trim();
			}
			else
				Standbystring2 = null;
		}
		if (FCode.equalsIgnoreCase("Standbynum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbynum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbydate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate1 = fDate.getDate( FValue );
			}
			else
				Standbydate1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbydate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate2 = fDate.getDate( FValue );
			}
			else
				Standbydate2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIPlaceRentFeeBSchema other = (LIPlaceRentFeeBSchema)otherObject;
		return
			(Serialno == null ? other.getSerialno() == null : Serialno.equals(other.getSerialno()))
			&& (Placeno == null ? other.getPlaceno() == null : Placeno.equals(other.getPlaceno()))
			&& (Feetype == null ? other.getFeetype() == null : Feetype.equals(other.getFeetype()))
			&& (Order == null ? other.getOrder() == null : Order.equals(other.getOrder()))
			&& (PayDate == null ? other.getPayDate() == null : fDate.getString(PayDate).equals(other.getPayDate()))
			&& Money == other.getMoney()
			&& (Paybegindate == null ? other.getPaybegindate() == null : fDate.getString(Paybegindate).equals(other.getPaybegindate()))
			&& (Payenddate == null ? other.getPayenddate() == null : fDate.getString(Payenddate).equals(other.getPayenddate()))
			&& (Placenorefer == null ? other.getPlacenorefer() == null : Placenorefer.equals(other.getPlacenorefer()))
			&& (Dealtype == null ? other.getDealtype() == null : Dealtype.equals(other.getDealtype()))
			&& (Dealstate == null ? other.getDealstate() == null : Dealstate.equals(other.getDealstate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Makedate == null ? other.getMakedate() == null : fDate.getString(Makedate).equals(other.getMakedate()))
			&& (Maketime == null ? other.getMaketime() == null : Maketime.equals(other.getMaketime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Standbystring1 == null ? other.getStandbystring1() == null : Standbystring1.equals(other.getStandbystring1()))
			&& (Standbystring2 == null ? other.getStandbystring2() == null : Standbystring2.equals(other.getStandbystring2()))
			&& Standbynum1 == other.getStandbynum1()
			&& Standbynum2 == other.getStandbynum2()
			&& (Standbydate1 == null ? other.getStandbydate1() == null : fDate.getString(Standbydate1).equals(other.getStandbydate1()))
			&& (Standbydate2 == null ? other.getStandbydate2() == null : fDate.getString(Standbydate2).equals(other.getStandbydate2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Serialno") ) {
			return 0;
		}
		if( strFieldName.equals("Placeno") ) {
			return 1;
		}
		if( strFieldName.equals("Feetype") ) {
			return 2;
		}
		if( strFieldName.equals("Order") ) {
			return 3;
		}
		if( strFieldName.equals("PayDate") ) {
			return 4;
		}
		if( strFieldName.equals("Money") ) {
			return 5;
		}
		if( strFieldName.equals("Paybegindate") ) {
			return 6;
		}
		if( strFieldName.equals("Payenddate") ) {
			return 7;
		}
		if( strFieldName.equals("Placenorefer") ) {
			return 8;
		}
		if( strFieldName.equals("Dealtype") ) {
			return 9;
		}
		if( strFieldName.equals("Dealstate") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("Makedate") ) {
			return 12;
		}
		if( strFieldName.equals("Maketime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return 16;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return 17;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return 18;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return 19;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return 20;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return 21;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Serialno";
				break;
			case 1:
				strFieldName = "Placeno";
				break;
			case 2:
				strFieldName = "Feetype";
				break;
			case 3:
				strFieldName = "Order";
				break;
			case 4:
				strFieldName = "PayDate";
				break;
			case 5:
				strFieldName = "Money";
				break;
			case 6:
				strFieldName = "Paybegindate";
				break;
			case 7:
				strFieldName = "Payenddate";
				break;
			case 8:
				strFieldName = "Placenorefer";
				break;
			case 9:
				strFieldName = "Dealtype";
				break;
			case 10:
				strFieldName = "Dealstate";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "Makedate";
				break;
			case 13:
				strFieldName = "Maketime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			case 16:
				strFieldName = "Standbystring1";
				break;
			case 17:
				strFieldName = "Standbystring2";
				break;
			case 18:
				strFieldName = "Standbynum1";
				break;
			case 19:
				strFieldName = "Standbynum2";
				break;
			case 20:
				strFieldName = "Standbydate1";
				break;
			case 21:
				strFieldName = "Standbydate2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Serialno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Placeno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Feetype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Order") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Money") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Paybegindate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Payenddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Placenorefer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dealtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dealstate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
