/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHCustomInHospitalDB;

/*
 * <p>ClassName: LHCustomInHospitalSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表修改-就诊信息-20060509
 * @CreateDate：2006-05-10
 */
public class LHCustomInHospitalSchema implements Schema, Cloneable
{
	// @Field
	/** 客户号码 */
	private String CustomerNo;
	/** 就诊及住院序号 */
	private String InHospitNo;
	/** 是否为体检 */
	private String IsPhysicalExam;
	/** 住院天数 */
	private int InHospitalDays;
	/** 就诊方式 */
	private String InHospitMode;
	/** 就诊日期 */
	private Date InHospitDate;
	/** 医疗机构代码 */
	private String HospitCode;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 管理机构 */
	private String ManageCom;
	/** 主诉 */
	private String MainItem;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LHCustomInHospitalSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "CustomerNo";
		pk[1] = "InHospitNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LHCustomInHospitalSchema cloned = (LHCustomInHospitalSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
            CustomerNo = aCustomerNo;
	}
	public String getInHospitNo()
	{
		return InHospitNo;
	}
	public void setInHospitNo(String aInHospitNo)
	{
            InHospitNo = aInHospitNo;
	}
	public String getIsPhysicalExam()
	{
		return IsPhysicalExam;
	}
	public void setIsPhysicalExam(String aIsPhysicalExam)
	{
            IsPhysicalExam = aIsPhysicalExam;
	}
	public int getInHospitalDays()
	{
		return InHospitalDays;
	}
	public void setInHospitalDays(int aInHospitalDays)
	{
            InHospitalDays = aInHospitalDays;
	}
	public void setInHospitalDays(String aInHospitalDays)
	{
		if (aInHospitalDays != null && !aInHospitalDays.equals(""))
		{
			Integer tInteger = new Integer(aInHospitalDays);
			int i = tInteger.intValue();
			InHospitalDays = i;
		}
	}

	public String getInHospitMode()
	{
		return InHospitMode;
	}
	public void setInHospitMode(String aInHospitMode)
	{
            InHospitMode = aInHospitMode;
	}
	public String getInHospitDate()
	{
		if( InHospitDate != null )
			return fDate.getString(InHospitDate);
		else
			return null;
	}
	public void setInHospitDate(Date aInHospitDate)
	{
            InHospitDate = aInHospitDate;
	}
	public void setInHospitDate(String aInHospitDate)
	{
		if (aInHospitDate != null && !aInHospitDate.equals("") )
		{
			InHospitDate = fDate.getDate( aInHospitDate );
		}
		else
			InHospitDate = null;
	}

	public String getHospitCode()
	{
		return HospitCode;
	}
	public void setHospitCode(String aHospitCode)
	{
            HospitCode = aHospitCode;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getMainItem()
	{
		return MainItem;
	}
	public void setMainItem(String aMainItem)
	{
            MainItem = aMainItem;
	}

	/**
	* 使用另外一个 LHCustomInHospitalSchema 对象给 Schema 赋值
	* @param: aLHCustomInHospitalSchema LHCustomInHospitalSchema
	**/
	public void setSchema(LHCustomInHospitalSchema aLHCustomInHospitalSchema)
	{
		this.CustomerNo = aLHCustomInHospitalSchema.getCustomerNo();
		this.InHospitNo = aLHCustomInHospitalSchema.getInHospitNo();
		this.IsPhysicalExam = aLHCustomInHospitalSchema.getIsPhysicalExam();
		this.InHospitalDays = aLHCustomInHospitalSchema.getInHospitalDays();
		this.InHospitMode = aLHCustomInHospitalSchema.getInHospitMode();
		this.InHospitDate = fDate.getDate( aLHCustomInHospitalSchema.getInHospitDate());
		this.HospitCode = aLHCustomInHospitalSchema.getHospitCode();
		this.Operator = aLHCustomInHospitalSchema.getOperator();
		this.MakeDate = fDate.getDate( aLHCustomInHospitalSchema.getMakeDate());
		this.MakeTime = aLHCustomInHospitalSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLHCustomInHospitalSchema.getModifyDate());
		this.ModifyTime = aLHCustomInHospitalSchema.getModifyTime();
		this.ManageCom = aLHCustomInHospitalSchema.getManageCom();
		this.MainItem = aLHCustomInHospitalSchema.getMainItem();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("InHospitNo") == null )
				this.InHospitNo = null;
			else
				this.InHospitNo = rs.getString("InHospitNo").trim();

			if( rs.getString("IsPhysicalExam") == null )
				this.IsPhysicalExam = null;
			else
				this.IsPhysicalExam = rs.getString("IsPhysicalExam").trim();

			this.InHospitalDays = rs.getInt("InHospitalDays");
			if( rs.getString("InHospitMode") == null )
				this.InHospitMode = null;
			else
				this.InHospitMode = rs.getString("InHospitMode").trim();

			this.InHospitDate = rs.getDate("InHospitDate");
			if( rs.getString("HospitCode") == null )
				this.HospitCode = null;
			else
				this.HospitCode = rs.getString("HospitCode").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("MainItem") == null )
				this.MainItem = null;
			else
				this.MainItem = rs.getString("MainItem").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LHCustomInHospital表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHCustomInHospitalSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LHCustomInHospitalSchema getSchema()
	{
		LHCustomInHospitalSchema aLHCustomInHospitalSchema = new LHCustomInHospitalSchema();
		aLHCustomInHospitalSchema.setSchema(this);
		return aLHCustomInHospitalSchema;
	}

	public LHCustomInHospitalDB getDB()
	{
		LHCustomInHospitalDB aDBOper = new LHCustomInHospitalDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCustomInHospital描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InHospitNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IsPhysicalExam)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(InHospitalDays));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InHospitMode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( InHospitDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(HospitCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MainItem));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCustomInHospital>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			InHospitNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			IsPhysicalExam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InHospitalDays= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			InHospitMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			InHospitDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MainItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHCustomInHospitalSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("InHospitNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InHospitNo));
		}
		if (FCode.equals("IsPhysicalExam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IsPhysicalExam));
		}
		if (FCode.equals("InHospitalDays"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InHospitalDays));
		}
		if (FCode.equals("InHospitMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InHospitMode));
		}
		if (FCode.equals("InHospitDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInHospitDate()));
		}
		if (FCode.equals("HospitCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("MainItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainItem));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(InHospitNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(IsPhysicalExam);
				break;
			case 3:
				strFieldValue = String.valueOf(InHospitalDays);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(InHospitMode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInHospitDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(HospitCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MainItem);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("InHospitNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InHospitNo = FValue.trim();
			}
			else
				InHospitNo = null;
		}
		if (FCode.equalsIgnoreCase("IsPhysicalExam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IsPhysicalExam = FValue.trim();
			}
			else
				IsPhysicalExam = null;
		}
		if (FCode.equalsIgnoreCase("InHospitalDays"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InHospitalDays = i;
			}
		}
		if (FCode.equalsIgnoreCase("InHospitMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InHospitMode = FValue.trim();
			}
			else
				InHospitMode = null;
		}
		if (FCode.equalsIgnoreCase("InHospitDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InHospitDate = fDate.getDate( FValue );
			}
			else
				InHospitDate = null;
		}
		if (FCode.equalsIgnoreCase("HospitCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitCode = FValue.trim();
			}
			else
				HospitCode = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("MainItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainItem = FValue.trim();
			}
			else
				MainItem = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LHCustomInHospitalSchema other = (LHCustomInHospitalSchema)otherObject;
		return
			CustomerNo.equals(other.getCustomerNo())
			&& InHospitNo.equals(other.getInHospitNo())
			&& IsPhysicalExam.equals(other.getIsPhysicalExam())
			&& InHospitalDays == other.getInHospitalDays()
			&& InHospitMode.equals(other.getInHospitMode())
			&& fDate.getString(InHospitDate).equals(other.getInHospitDate())
			&& HospitCode.equals(other.getHospitCode())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& ManageCom.equals(other.getManageCom())
			&& MainItem.equals(other.getMainItem());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CustomerNo") ) {
			return 0;
		}
		if( strFieldName.equals("InHospitNo") ) {
			return 1;
		}
		if( strFieldName.equals("IsPhysicalExam") ) {
			return 2;
		}
		if( strFieldName.equals("InHospitalDays") ) {
			return 3;
		}
		if( strFieldName.equals("InHospitMode") ) {
			return 4;
		}
		if( strFieldName.equals("InHospitDate") ) {
			return 5;
		}
		if( strFieldName.equals("HospitCode") ) {
			return 6;
		}
		if( strFieldName.equals("Operator") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 12;
		}
		if( strFieldName.equals("MainItem") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CustomerNo";
				break;
			case 1:
				strFieldName = "InHospitNo";
				break;
			case 2:
				strFieldName = "IsPhysicalExam";
				break;
			case 3:
				strFieldName = "InHospitalDays";
				break;
			case 4:
				strFieldName = "InHospitMode";
				break;
			case 5:
				strFieldName = "InHospitDate";
				break;
			case 6:
				strFieldName = "HospitCode";
				break;
			case 7:
				strFieldName = "Operator";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			case 12:
				strFieldName = "ManageCom";
				break;
			case 13:
				strFieldName = "MainItem";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InHospitNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IsPhysicalExam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InHospitalDays") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InHospitMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InHospitDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospitCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainItem") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
