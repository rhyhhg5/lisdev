/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseDrugDB;

/*
 * <p>ClassName: LLCaseDrugSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2009-12-07
 */
public class LLCaseDrugSchema implements Schema, Cloneable
{
	// @Field
	/** 账单药品明细 */
	private String DrugDetailNo;
	/** 账单号 */
	private String MainFeeNo;
	/** 立案号(申请登记号) */
	private String RgtNo;
	/** 分案号 */
	private String CaseNo;
	/** 药品类型 */
	private String DrugType;
	/** 药品代码 */
	private String DrugCode;
	/** 药品名称 */
	private String DrugName;
	/** 费用金额 */
	private double Fee;
	/** 统筹支付 */
	private double SecuFee;
	/** 自负2 */
	private double SelfPay2;
	/** 自费 */
	private double SelfFee;
	/** 支付比例 */
	private double PayRate;
	/** 支付限额 */
	private double PayLimit;
	/** 备注 */
	private String Remark;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 不合理费用 */
	private double UnReasonableFee;
	/** 处方编号 */
	private String PrescriptionNo;
	/** 录入类型 */
	private String InputType;

	public static final int FIELDNUM = 23;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCaseDrugSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "DrugDetailNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCaseDrugSchema cloned = (LLCaseDrugSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getDrugDetailNo()
	{
		return DrugDetailNo;
	}
	public void setDrugDetailNo(String aDrugDetailNo)
	{
		DrugDetailNo = aDrugDetailNo;
	}
	public String getMainFeeNo()
	{
		return MainFeeNo;
	}
	public void setMainFeeNo(String aMainFeeNo)
	{
		MainFeeNo = aMainFeeNo;
	}
	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getDrugType()
	{
		return DrugType;
	}
	public void setDrugType(String aDrugType)
	{
		DrugType = aDrugType;
	}
	public String getDrugCode()
	{
		return DrugCode;
	}
	public void setDrugCode(String aDrugCode)
	{
		DrugCode = aDrugCode;
	}
	public String getDrugName()
	{
		return DrugName;
	}
	public void setDrugName(String aDrugName)
	{
		DrugName = aDrugName;
	}
	public double getFee()
	{
		return Fee;
	}
	public void setFee(double aFee)
	{
		Fee = Arith.round(aFee,2);
	}
	public void setFee(String aFee)
	{
		if (aFee != null && !aFee.equals(""))
		{
			Double tDouble = new Double(aFee);
			double d = tDouble.doubleValue();
                Fee = Arith.round(d,2);
		}
	}

	public double getSecuFee()
	{
		return SecuFee;
	}
	public void setSecuFee(double aSecuFee)
	{
		SecuFee = Arith.round(aSecuFee,2);
	}
	public void setSecuFee(String aSecuFee)
	{
		if (aSecuFee != null && !aSecuFee.equals(""))
		{
			Double tDouble = new Double(aSecuFee);
			double d = tDouble.doubleValue();
                SecuFee = Arith.round(d,2);
		}
	}

	public double getSelfPay2()
	{
		return SelfPay2;
	}
	public void setSelfPay2(double aSelfPay2)
	{
		SelfPay2 = Arith.round(aSelfPay2,2);
	}
	public void setSelfPay2(String aSelfPay2)
	{
		if (aSelfPay2 != null && !aSelfPay2.equals(""))
		{
			Double tDouble = new Double(aSelfPay2);
			double d = tDouble.doubleValue();
                SelfPay2 = Arith.round(d,2);
		}
	}

	public double getSelfFee()
	{
		return SelfFee;
	}
	public void setSelfFee(double aSelfFee)
	{
		SelfFee = Arith.round(aSelfFee,2);
	}
	public void setSelfFee(String aSelfFee)
	{
		if (aSelfFee != null && !aSelfFee.equals(""))
		{
			Double tDouble = new Double(aSelfFee);
			double d = tDouble.doubleValue();
                SelfFee = Arith.round(d,2);
		}
	}

	public double getPayRate()
	{
		return PayRate;
	}
	public void setPayRate(double aPayRate)
	{
		PayRate = Arith.round(aPayRate,2);
	}
	public void setPayRate(String aPayRate)
	{
		if (aPayRate != null && !aPayRate.equals(""))
		{
			Double tDouble = new Double(aPayRate);
			double d = tDouble.doubleValue();
                PayRate = Arith.round(d,2);
		}
	}

	public double getPayLimit()
	{
		return PayLimit;
	}
	public void setPayLimit(double aPayLimit)
	{
		PayLimit = Arith.round(aPayLimit,2);
	}
	public void setPayLimit(String aPayLimit)
	{
		if (aPayLimit != null && !aPayLimit.equals(""))
		{
			Double tDouble = new Double(aPayLimit);
			double d = tDouble.doubleValue();
                PayLimit = Arith.round(d,2);
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public double getUnReasonableFee()
	{
		return UnReasonableFee;
	}
	public void setUnReasonableFee(double aUnReasonableFee)
	{
		UnReasonableFee = Arith.round(aUnReasonableFee,2);
	}
	public void setUnReasonableFee(String aUnReasonableFee)
	{
		if (aUnReasonableFee != null && !aUnReasonableFee.equals(""))
		{
			Double tDouble = new Double(aUnReasonableFee);
			double d = tDouble.doubleValue();
                UnReasonableFee = Arith.round(d,2);
		}
	}

	public String getPrescriptionNo()
	{
		return PrescriptionNo;
	}
	public void setPrescriptionNo(String aPrescriptionNo)
	{
		PrescriptionNo = aPrescriptionNo;
	}
	public String getInputType()
	{
		return InputType;
	}
	public void setInputType(String aInputType)
	{
		InputType = aInputType;
	}

	/**
	* 使用另外一个 LLCaseDrugSchema 对象给 Schema 赋值
	* @param: aLLCaseDrugSchema LLCaseDrugSchema
	**/
	public void setSchema(LLCaseDrugSchema aLLCaseDrugSchema)
	{
		this.DrugDetailNo = aLLCaseDrugSchema.getDrugDetailNo();
		this.MainFeeNo = aLLCaseDrugSchema.getMainFeeNo();
		this.RgtNo = aLLCaseDrugSchema.getRgtNo();
		this.CaseNo = aLLCaseDrugSchema.getCaseNo();
		this.DrugType = aLLCaseDrugSchema.getDrugType();
		this.DrugCode = aLLCaseDrugSchema.getDrugCode();
		this.DrugName = aLLCaseDrugSchema.getDrugName();
		this.Fee = aLLCaseDrugSchema.getFee();
		this.SecuFee = aLLCaseDrugSchema.getSecuFee();
		this.SelfPay2 = aLLCaseDrugSchema.getSelfPay2();
		this.SelfFee = aLLCaseDrugSchema.getSelfFee();
		this.PayRate = aLLCaseDrugSchema.getPayRate();
		this.PayLimit = aLLCaseDrugSchema.getPayLimit();
		this.Remark = aLLCaseDrugSchema.getRemark();
		this.MngCom = aLLCaseDrugSchema.getMngCom();
		this.Operator = aLLCaseDrugSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLCaseDrugSchema.getMakeDate());
		this.MakeTime = aLLCaseDrugSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLCaseDrugSchema.getModifyDate());
		this.ModifyTime = aLLCaseDrugSchema.getModifyTime();
		this.UnReasonableFee = aLLCaseDrugSchema.getUnReasonableFee();
		this.PrescriptionNo = aLLCaseDrugSchema.getPrescriptionNo();
		this.InputType = aLLCaseDrugSchema.getInputType();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("DrugDetailNo") == null )
				this.DrugDetailNo = null;
			else
				this.DrugDetailNo = rs.getString("DrugDetailNo").trim();

			if( rs.getString("MainFeeNo") == null )
				this.MainFeeNo = null;
			else
				this.MainFeeNo = rs.getString("MainFeeNo").trim();

			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("DrugType") == null )
				this.DrugType = null;
			else
				this.DrugType = rs.getString("DrugType").trim();

			if( rs.getString("DrugCode") == null )
				this.DrugCode = null;
			else
				this.DrugCode = rs.getString("DrugCode").trim();

			if( rs.getString("DrugName") == null )
				this.DrugName = null;
			else
				this.DrugName = rs.getString("DrugName").trim();

			this.Fee = rs.getDouble("Fee");
			this.SecuFee = rs.getDouble("SecuFee");
			this.SelfPay2 = rs.getDouble("SelfPay2");
			this.SelfFee = rs.getDouble("SelfFee");
			this.PayRate = rs.getDouble("PayRate");
			this.PayLimit = rs.getDouble("PayLimit");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.UnReasonableFee = rs.getDouble("UnReasonableFee");
			if( rs.getString("PrescriptionNo") == null )
				this.PrescriptionNo = null;
			else
				this.PrescriptionNo = rs.getString("PrescriptionNo").trim();

			if( rs.getString("InputType") == null )
				this.InputType = null;
			else
				this.InputType = rs.getString("InputType").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCaseDrug表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseDrugSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCaseDrugSchema getSchema()
	{
		LLCaseDrugSchema aLLCaseDrugSchema = new LLCaseDrugSchema();
		aLLCaseDrugSchema.setSchema(this);
		return aLLCaseDrugSchema;
	}

	public LLCaseDrugDB getDB()
	{
		LLCaseDrugDB aDBOper = new LLCaseDrugDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseDrug描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(DrugDetailNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Fee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SecuFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfPay2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayLimit));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(UnReasonableFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrescriptionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputType));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseDrug>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DrugDetailNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			MainFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DrugType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			DrugCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DrugName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Fee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			SecuFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			SelfPay2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			SelfFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			PayRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			PayLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			UnReasonableFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			PrescriptionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			InputType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseDrugSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DrugDetailNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugDetailNo));
		}
		if (FCode.equals("MainFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainFeeNo));
		}
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("DrugType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugType));
		}
		if (FCode.equals("DrugCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugCode));
		}
		if (FCode.equals("DrugName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugName));
		}
		if (FCode.equals("Fee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fee));
		}
		if (FCode.equals("SecuFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecuFee));
		}
		if (FCode.equals("SelfPay2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay2));
		}
		if (FCode.equals("SelfFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfFee));
		}
		if (FCode.equals("PayRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayRate));
		}
		if (FCode.equals("PayLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayLimit));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("UnReasonableFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnReasonableFee));
		}
		if (FCode.equals("PrescriptionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrescriptionNo));
		}
		if (FCode.equals("InputType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputType));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(DrugDetailNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(MainFeeNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DrugType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(DrugCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DrugName);
				break;
			case 7:
				strFieldValue = String.valueOf(Fee);
				break;
			case 8:
				strFieldValue = String.valueOf(SecuFee);
				break;
			case 9:
				strFieldValue = String.valueOf(SelfPay2);
				break;
			case 10:
				strFieldValue = String.valueOf(SelfFee);
				break;
			case 11:
				strFieldValue = String.valueOf(PayRate);
				break;
			case 12:
				strFieldValue = String.valueOf(PayLimit);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 20:
				strFieldValue = String.valueOf(UnReasonableFee);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(PrescriptionNo);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(InputType);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DrugDetailNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugDetailNo = FValue.trim();
			}
			else
				DrugDetailNo = null;
		}
		if (FCode.equalsIgnoreCase("MainFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainFeeNo = FValue.trim();
			}
			else
				MainFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("DrugType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugType = FValue.trim();
			}
			else
				DrugType = null;
		}
		if (FCode.equalsIgnoreCase("DrugCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugCode = FValue.trim();
			}
			else
				DrugCode = null;
		}
		if (FCode.equalsIgnoreCase("DrugName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugName = FValue.trim();
			}
			else
				DrugName = null;
		}
		if (FCode.equalsIgnoreCase("Fee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Fee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SecuFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SecuFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfPay2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfPay2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("PayRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PayRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("PayLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PayLimit = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("UnReasonableFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				UnReasonableFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("PrescriptionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrescriptionNo = FValue.trim();
			}
			else
				PrescriptionNo = null;
		}
		if (FCode.equalsIgnoreCase("InputType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputType = FValue.trim();
			}
			else
				InputType = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCaseDrugSchema other = (LLCaseDrugSchema)otherObject;
		return
			(DrugDetailNo == null ? other.getDrugDetailNo() == null : DrugDetailNo.equals(other.getDrugDetailNo()))
			&& (MainFeeNo == null ? other.getMainFeeNo() == null : MainFeeNo.equals(other.getMainFeeNo()))
			&& (RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (DrugType == null ? other.getDrugType() == null : DrugType.equals(other.getDrugType()))
			&& (DrugCode == null ? other.getDrugCode() == null : DrugCode.equals(other.getDrugCode()))
			&& (DrugName == null ? other.getDrugName() == null : DrugName.equals(other.getDrugName()))
			&& Fee == other.getFee()
			&& SecuFee == other.getSecuFee()
			&& SelfPay2 == other.getSelfPay2()
			&& SelfFee == other.getSelfFee()
			&& PayRate == other.getPayRate()
			&& PayLimit == other.getPayLimit()
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& UnReasonableFee == other.getUnReasonableFee()
			&& (PrescriptionNo == null ? other.getPrescriptionNo() == null : PrescriptionNo.equals(other.getPrescriptionNo()))
			&& (InputType == null ? other.getInputType() == null : InputType.equals(other.getInputType()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DrugDetailNo") ) {
			return 0;
		}
		if( strFieldName.equals("MainFeeNo") ) {
			return 1;
		}
		if( strFieldName.equals("RgtNo") ) {
			return 2;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 3;
		}
		if( strFieldName.equals("DrugType") ) {
			return 4;
		}
		if( strFieldName.equals("DrugCode") ) {
			return 5;
		}
		if( strFieldName.equals("DrugName") ) {
			return 6;
		}
		if( strFieldName.equals("Fee") ) {
			return 7;
		}
		if( strFieldName.equals("SecuFee") ) {
			return 8;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return 9;
		}
		if( strFieldName.equals("SelfFee") ) {
			return 10;
		}
		if( strFieldName.equals("PayRate") ) {
			return 11;
		}
		if( strFieldName.equals("PayLimit") ) {
			return 12;
		}
		if( strFieldName.equals("Remark") ) {
			return 13;
		}
		if( strFieldName.equals("MngCom") ) {
			return 14;
		}
		if( strFieldName.equals("Operator") ) {
			return 15;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 16;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 19;
		}
		if( strFieldName.equals("UnReasonableFee") ) {
			return 20;
		}
		if( strFieldName.equals("PrescriptionNo") ) {
			return 21;
		}
		if( strFieldName.equals("InputType") ) {
			return 22;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DrugDetailNo";
				break;
			case 1:
				strFieldName = "MainFeeNo";
				break;
			case 2:
				strFieldName = "RgtNo";
				break;
			case 3:
				strFieldName = "CaseNo";
				break;
			case 4:
				strFieldName = "DrugType";
				break;
			case 5:
				strFieldName = "DrugCode";
				break;
			case 6:
				strFieldName = "DrugName";
				break;
			case 7:
				strFieldName = "Fee";
				break;
			case 8:
				strFieldName = "SecuFee";
				break;
			case 9:
				strFieldName = "SelfPay2";
				break;
			case 10:
				strFieldName = "SelfFee";
				break;
			case 11:
				strFieldName = "PayRate";
				break;
			case 12:
				strFieldName = "PayLimit";
				break;
			case 13:
				strFieldName = "Remark";
				break;
			case 14:
				strFieldName = "MngCom";
				break;
			case 15:
				strFieldName = "Operator";
				break;
			case 16:
				strFieldName = "MakeDate";
				break;
			case 17:
				strFieldName = "MakeTime";
				break;
			case 18:
				strFieldName = "ModifyDate";
				break;
			case 19:
				strFieldName = "ModifyTime";
				break;
			case 20:
				strFieldName = "UnReasonableFee";
				break;
			case 21:
				strFieldName = "PrescriptionNo";
				break;
			case 22:
				strFieldName = "InputType";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DrugDetailNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SecuFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfPay2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PayRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PayLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnReasonableFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PrescriptionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputType") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
