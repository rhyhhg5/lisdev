/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKBankDescDB;

/*
 * <p>ClassName: LKBankDescSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-06-16
 */
public class LKBankDescSchema implements Schema, Cloneable
{
	// @Field
	/** Bankcode */
	private String BankCode;
	/** Bankname */
	private String BankName;
	/** Infotype */
	private String InfoType;
	/** Infocontent */
	private String InfoContent;

	public static final int FIELDNUM = 4;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKBankDescSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "BankCode";
		pk[1] = "InfoType";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LKBankDescSchema cloned = (LKBankDescSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBankCode()
	{
		if (SysConst.CHANGECHARSET && BankCode != null && !BankCode.equals(""))
		{
			BankCode = StrTool.unicodeToGBK(BankCode);
		}
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankName()
	{
		if (SysConst.CHANGECHARSET && BankName != null && !BankName.equals(""))
		{
			BankName = StrTool.unicodeToGBK(BankName);
		}
		return BankName;
	}
	public void setBankName(String aBankName)
	{
		BankName = aBankName;
	}
	public String getInfoType()
	{
		if (SysConst.CHANGECHARSET && InfoType != null && !InfoType.equals(""))
		{
			InfoType = StrTool.unicodeToGBK(InfoType);
		}
		return InfoType;
	}
	public void setInfoType(String aInfoType)
	{
		InfoType = aInfoType;
	}
	public String getInfoContent()
	{
		if (SysConst.CHANGECHARSET && InfoContent != null && !InfoContent.equals(""))
		{
			InfoContent = StrTool.unicodeToGBK(InfoContent);
		}
		return InfoContent;
	}
	public void setInfoContent(String aInfoContent)
	{
		InfoContent = aInfoContent;
	}

	/**
	* 使用另外一个 LKBankDescSchema 对象给 Schema 赋值
	* @param: aLKBankDescSchema LKBankDescSchema
	**/
	public void setSchema(LKBankDescSchema aLKBankDescSchema)
	{
		this.BankCode = aLKBankDescSchema.getBankCode();
		this.BankName = aLKBankDescSchema.getBankName();
		this.InfoType = aLKBankDescSchema.getInfoType();
		this.InfoContent = aLKBankDescSchema.getInfoContent();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankName") == null )
				this.BankName = null;
			else
				this.BankName = rs.getString("BankName").trim();

			if( rs.getString("InfoType") == null )
				this.InfoType = null;
			else
				this.InfoType = rs.getString("InfoType").trim();

			if( rs.getString("InfoContent") == null )
				this.InfoContent = null;
			else
				this.InfoContent = rs.getString("InfoContent").trim();

		}
		catch(SQLException sqle)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKBankDescSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	public LKBankDescSchema getSchema()
	{
		LKBankDescSchema aLKBankDescSchema = new LKBankDescSchema();
		aLKBankDescSchema.setSchema(this);
		return aLKBankDescSchema;
	}

	public LKBankDescDB getDB()
	{
		LKBankDescDB aDBOper = new LKBankDescDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKBankDesc描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BankCode))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BankName))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(InfoType))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(InfoContent)));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKBankDesc>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			InfoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InfoContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKBankDescSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankName));
		}
		if (FCode.equals("InfoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InfoType));
		}
		if (FCode.equals("InfoContent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InfoContent));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BankName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(InfoType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(InfoContent);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equals("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equals("BankName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankName = FValue.trim();
			}
			else
				BankName = null;
		}
		if (FCode.equals("InfoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InfoType = FValue.trim();
			}
			else
				InfoType = null;
		}
		if (FCode.equals("InfoContent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InfoContent = FValue.trim();
			}
			else
				InfoContent = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKBankDescSchema other = (LKBankDescSchema)otherObject;
		return
			BankCode.equals(other.getBankCode())
			&& BankName.equals(other.getBankName())
			&& InfoType.equals(other.getInfoType())
			&& InfoContent.equals(other.getInfoContent());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BankCode") ) {
			return 0;
		}
		if( strFieldName.equals("BankName") ) {
			return 1;
		}
		if( strFieldName.equals("InfoType") ) {
			return 2;
		}
		if( strFieldName.equals("InfoContent") ) {
			return 3;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BankCode";
				break;
			case 1:
				strFieldName = "BankName";
				break;
			case 2:
				strFieldName = "InfoType";
				break;
			case 3:
				strFieldName = "InfoContent";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InfoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InfoContent") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
