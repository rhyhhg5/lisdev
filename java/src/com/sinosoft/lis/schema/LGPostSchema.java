/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LGPostDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LGPostSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全自动审批设置
 * @CreateDate：2005-06-23
 */
public class LGPostSchema implements Schema, Cloneable
{
    // @Field
    /** 岗位编码 */
    private String PostNo;
    /** 岗位名称 */
    private String PostName;
    /** 所属小组编号 */
    private String GroupNo;
    /** 业务上级编号 */
    private String SuperPostNo;
    /** 岗位职责 */
    private String PostDuty;
    /** 备注 */
    private String Remark;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LGPostSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PostNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LGPostSchema cloned = (LGPostSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPostNo()
    {
        return PostNo;
    }

    public void setPostNo(String aPostNo)
    {
        PostNo = aPostNo;
    }

    public String getPostName()
    {
        return PostName;
    }

    public void setPostName(String aPostName)
    {
        PostName = aPostName;
    }

    public String getGroupNo()
    {
        return GroupNo;
    }

    public void setGroupNo(String aGroupNo)
    {
        GroupNo = aGroupNo;
    }

    public String getSuperPostNo()
    {
        return SuperPostNo;
    }

    public void setSuperPostNo(String aSuperPostNo)
    {
        SuperPostNo = aSuperPostNo;
    }

    public String getPostDuty()
    {
        return PostDuty;
    }

    public void setPostDuty(String aPostDuty)
    {
        PostDuty = aPostDuty;
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LGPostSchema 对象给 Schema 赋值
     * @param: aLGPostSchema LGPostSchema
     **/
    public void setSchema(LGPostSchema aLGPostSchema)
    {
        this.PostNo = aLGPostSchema.getPostNo();
        this.PostName = aLGPostSchema.getPostName();
        this.GroupNo = aLGPostSchema.getGroupNo();
        this.SuperPostNo = aLGPostSchema.getSuperPostNo();
        this.PostDuty = aLGPostSchema.getPostDuty();
        this.Remark = aLGPostSchema.getRemark();
        this.Operator = aLGPostSchema.getOperator();
        this.MakeDate = fDate.getDate(aLGPostSchema.getMakeDate());
        this.MakeTime = aLGPostSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLGPostSchema.getModifyDate());
        this.ModifyTime = aLGPostSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PostNo") == null)
            {
                this.PostNo = null;
            }
            else
            {
                this.PostNo = rs.getString("PostNo").trim();
            }

            if (rs.getString("PostName") == null)
            {
                this.PostName = null;
            }
            else
            {
                this.PostName = rs.getString("PostName").trim();
            }

            if (rs.getString("GroupNo") == null)
            {
                this.GroupNo = null;
            }
            else
            {
                this.GroupNo = rs.getString("GroupNo").trim();
            }

            if (rs.getString("SuperPostNo") == null)
            {
                this.SuperPostNo = null;
            }
            else
            {
                this.SuperPostNo = rs.getString("SuperPostNo").trim();
            }

            if (rs.getString("PostDuty") == null)
            {
                this.PostDuty = null;
            }
            else
            {
                this.PostDuty = rs.getString("PostDuty").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LGPost表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGPostSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LGPostSchema getSchema()
    {
        LGPostSchema aLGPostSchema = new LGPostSchema();
        aLGPostSchema.setSchema(this);
        return aLGPostSchema;
    }

    public LGPostDB getDB()
    {
        LGPostDB aDBOper = new LGPostDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGPost描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PostNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PostName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GroupNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SuperPostNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PostDuty));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGPost>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PostNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            PostName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            GroupNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            SuperPostNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            PostDuty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGPostSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PostNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostNo));
        }
        if (FCode.equals("PostName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostName));
        }
        if (FCode.equals("GroupNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GroupNo));
        }
        if (FCode.equals("SuperPostNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SuperPostNo));
        }
        if (FCode.equals("PostDuty"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostDuty));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PostNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PostName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GroupNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SuperPostNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PostDuty);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("PostNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostNo = FValue.trim();
            }
            else
            {
                PostNo = null;
            }
        }
        if (FCode.equals("PostName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostName = FValue.trim();
            }
            else
            {
                PostName = null;
            }
        }
        if (FCode.equals("GroupNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GroupNo = FValue.trim();
            }
            else
            {
                GroupNo = null;
            }
        }
        if (FCode.equals("SuperPostNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SuperPostNo = FValue.trim();
            }
            else
            {
                SuperPostNo = null;
            }
        }
        if (FCode.equals("PostDuty"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostDuty = FValue.trim();
            }
            else
            {
                PostDuty = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LGPostSchema other = (LGPostSchema) otherObject;
        return
                PostNo.equals(other.getPostNo())
                && PostName.equals(other.getPostName())
                && GroupNo.equals(other.getGroupNo())
                && SuperPostNo.equals(other.getSuperPostNo())
                && PostDuty.equals(other.getPostDuty())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PostNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PostName"))
        {
            return 1;
        }
        if (strFieldName.equals("GroupNo"))
        {
            return 2;
        }
        if (strFieldName.equals("SuperPostNo"))
        {
            return 3;
        }
        if (strFieldName.equals("PostDuty"))
        {
            return 4;
        }
        if (strFieldName.equals("Remark"))
        {
            return 5;
        }
        if (strFieldName.equals("Operator"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PostNo";
                break;
            case 1:
                strFieldName = "PostName";
                break;
            case 2:
                strFieldName = "GroupNo";
                break;
            case 3:
                strFieldName = "SuperPostNo";
                break;
            case 4:
                strFieldName = "PostDuty";
                break;
            case 5:
                strFieldName = "Remark";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PostNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GroupNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SuperPostNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostDuty"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
