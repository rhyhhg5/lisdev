/*
 * <p>ClassName: LFComToFinComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LFComToFinComDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFComToFinComSchema implements Schema
{
    // @Field
    /** 机构编码 */
    private String ComCode;
    /** 财务系统机构编码 */
    private String ComCodeFin;
    /** 财务系统机构名称 */
    private String comcodefinname;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFComToFinComSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ComCode";
        pk[1] = "ComCodeFin";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getComCode()
    {
        if (ComCode != null && !ComCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ComCode = StrTool.unicodeToGBK(ComCode);
        }
        return ComCode;
    }

    public void setComCode(String aComCode)
    {
        ComCode = aComCode;
    }

    public String getComCodeFin()
    {
        if (ComCodeFin != null && !ComCodeFin.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ComCodeFin = StrTool.unicodeToGBK(ComCodeFin);
        }
        return ComCodeFin;
    }

    public void setComCodeFin(String aComCodeFin)
    {
        ComCodeFin = aComCodeFin;
    }

    public String getcomcodefinname()
    {
        if (comcodefinname != null && !comcodefinname.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            comcodefinname = StrTool.unicodeToGBK(comcodefinname);
        }
        return comcodefinname;
    }

    public void setcomcodefinname(String acomcodefinname)
    {
        comcodefinname = acomcodefinname;
    }

    /**
     * 使用另外一个 LFComToFinComSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFComToFinComSchema aLFComToFinComSchema)
    {
        this.ComCode = aLFComToFinComSchema.getComCode();
        this.ComCodeFin = aLFComToFinComSchema.getComCodeFin();
        this.comcodefinname = aLFComToFinComSchema.getcomcodefinname();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ComCode") == null)
            {
                this.ComCode = null;
            }
            else
            {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("ComCodeFin") == null)
            {
                this.ComCodeFin = null;
            }
            else
            {
                this.ComCodeFin = rs.getString("ComCodeFin").trim();
            }

            if (rs.getString("comcodefinname") == null)
            {
                this.comcodefinname = null;
            }
            else
            {
                this.comcodefinname = rs.getString("comcodefinname").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFComToFinComSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFComToFinComSchema getSchema()
    {
        LFComToFinComSchema aLFComToFinComSchema = new LFComToFinComSchema();
        aLFComToFinComSchema.setSchema(this);
        return aLFComToFinComSchema;
    }

    public LFComToFinComDB getDB()
    {
        LFComToFinComDB aDBOper = new LFComToFinComDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFComToFinCom描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ComCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComCodeFin)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(comcodefinname));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFComToFinCom>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            ComCodeFin = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            comcodefinname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFComToFinComSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ComCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCode));
        }
        if (FCode.equals("ComCodeFin"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCodeFin));
        }
        if (FCode.equals("comcodefinname"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(comcodefinname));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ComCodeFin);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(comcodefinname);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
            {
                ComCode = null;
            }
        }
        if (FCode.equals("ComCodeFin"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCodeFin = FValue.trim();
            }
            else
            {
                ComCodeFin = null;
            }
        }
        if (FCode.equals("comcodefinname"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                comcodefinname = FValue.trim();
            }
            else
            {
                comcodefinname = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFComToFinComSchema other = (LFComToFinComSchema) otherObject;
        return
                ComCode.equals(other.getComCode())
                && ComCodeFin.equals(other.getComCodeFin())
                && comcodefinname.equals(other.getcomcodefinname());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ComCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ComCodeFin"))
        {
            return 1;
        }
        if (strFieldName.equals("comcodefinname"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ComCode";
                break;
            case 1:
                strFieldName = "ComCodeFin";
                break;
            case 2:
                strFieldName = "comcodefinname";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCodeFin"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("comcodefinname"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
