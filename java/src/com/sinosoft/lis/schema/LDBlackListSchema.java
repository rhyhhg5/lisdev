/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDBlackListDB;

/*
 * <p>ClassName: LDBlackListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-03-30
 */
public class LDBlackListSchema implements Schema, Cloneable
{
	// @Field
	/** 黑名单流水号 */
	private String BlackListSerialNo;
	/** 黑名单编码 */
	private String BlackListNo;
	/** 黑名单类型 */
	private String BlackListType;
	/** 黑名单名称 */
	private String BlackName;
	/** 黑名单录入员 */
	private String BlackListOperator;
	/** 黑名单日期 */
	private Date BlackListMakeDate;
	/** 黑名单时间 */
	private String BlackListMakeTime;
	/** 黑名单原因 */
	private String BlackListReason;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 录入来源 */
	private String BlackListSource;
	/** 录入原因 */
	private String BlackListReasonKind;
	/** 黑名单级别 */
	private String BlackListGrade;
	/** 黑名单类别 */
	private String BlackListKind;
	/** 相关合同号 */
	private String RelaContNo;
	/** 相关保单号 */
	private String RelaPolNo;
	/** 其它相关号 */
	private String RelaOtherNo;
	/** 其它相关号类型 */
	private String RelaOtherNoType;
	/** 黑名单录入员类别 */
	private String BlackListOperatorType;
	/** 身份证号 */
	private String IDNo;

	public static final int FIELDNUM = 22;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDBlackListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BlackListSerialNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LDBlackListSchema cloned = (LDBlackListSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBlackListSerialNo()
	{
		return BlackListSerialNo;
	}
	public void setBlackListSerialNo(String aBlackListSerialNo)
	{
            BlackListSerialNo = aBlackListSerialNo;
	}
	public String getBlackListNo()
	{
		return BlackListNo;
	}
	public void setBlackListNo(String aBlackListNo)
	{
            BlackListNo = aBlackListNo;
	}
	public String getBlackListType()
	{
		return BlackListType;
	}
	public void setBlackListType(String aBlackListType)
	{
            BlackListType = aBlackListType;
	}
	public String getBlackName()
	{
		return BlackName;
	}
	public void setBlackName(String aBlackName)
	{
            BlackName = aBlackName;
	}
	public String getBlackListOperator()
	{
		return BlackListOperator;
	}
	public void setBlackListOperator(String aBlackListOperator)
	{
            BlackListOperator = aBlackListOperator;
	}
	public String getBlackListMakeDate()
	{
		if( BlackListMakeDate != null )
			return fDate.getString(BlackListMakeDate);
		else
			return null;
	}
	public void setBlackListMakeDate(Date aBlackListMakeDate)
	{
            BlackListMakeDate = aBlackListMakeDate;
	}
	public void setBlackListMakeDate(String aBlackListMakeDate)
	{
		if (aBlackListMakeDate != null && !aBlackListMakeDate.equals("") )
		{
			BlackListMakeDate = fDate.getDate( aBlackListMakeDate );
		}
		else
			BlackListMakeDate = null;
	}

	public String getBlackListMakeTime()
	{
		return BlackListMakeTime;
	}
	public void setBlackListMakeTime(String aBlackListMakeTime)
	{
            BlackListMakeTime = aBlackListMakeTime;
	}
	public String getBlackListReason()
	{
		return BlackListReason;
	}
	public void setBlackListReason(String aBlackListReason)
	{
            BlackListReason = aBlackListReason;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getBlackListSource()
	{
		return BlackListSource;
	}
	public void setBlackListSource(String aBlackListSource)
	{
            BlackListSource = aBlackListSource;
	}
	public String getBlackListReasonKind()
	{
		return BlackListReasonKind;
	}
	public void setBlackListReasonKind(String aBlackListReasonKind)
	{
            BlackListReasonKind = aBlackListReasonKind;
	}
	public String getBlackListGrade()
	{
		return BlackListGrade;
	}
	public void setBlackListGrade(String aBlackListGrade)
	{
            BlackListGrade = aBlackListGrade;
	}
	public String getBlackListKind()
	{
		return BlackListKind;
	}
	public void setBlackListKind(String aBlackListKind)
	{
            BlackListKind = aBlackListKind;
	}
	public String getRelaContNo()
	{
		return RelaContNo;
	}
	public void setRelaContNo(String aRelaContNo)
	{
            RelaContNo = aRelaContNo;
	}
	public String getRelaPolNo()
	{
		return RelaPolNo;
	}
	public void setRelaPolNo(String aRelaPolNo)
	{
            RelaPolNo = aRelaPolNo;
	}
	public String getRelaOtherNo()
	{
		return RelaOtherNo;
	}
	public void setRelaOtherNo(String aRelaOtherNo)
	{
            RelaOtherNo = aRelaOtherNo;
	}
	public String getRelaOtherNoType()
	{
		return RelaOtherNoType;
	}
	public void setRelaOtherNoType(String aRelaOtherNoType)
	{
            RelaOtherNoType = aRelaOtherNoType;
	}
	public String getBlackListOperatorType()
	{
		return BlackListOperatorType;
	}
	public void setBlackListOperatorType(String aBlackListOperatorType)
	{
            BlackListOperatorType = aBlackListOperatorType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
            IDNo = aIDNo;
	}

	/**
	* 使用另外一个 LDBlackListSchema 对象给 Schema 赋值
	* @param: aLDBlackListSchema LDBlackListSchema
	**/
	public void setSchema(LDBlackListSchema aLDBlackListSchema)
	{
		this.BlackListSerialNo = aLDBlackListSchema.getBlackListSerialNo();
		this.BlackListNo = aLDBlackListSchema.getBlackListNo();
		this.BlackListType = aLDBlackListSchema.getBlackListType();
		this.BlackName = aLDBlackListSchema.getBlackName();
		this.BlackListOperator = aLDBlackListSchema.getBlackListOperator();
		this.BlackListMakeDate = fDate.getDate( aLDBlackListSchema.getBlackListMakeDate());
		this.BlackListMakeTime = aLDBlackListSchema.getBlackListMakeTime();
		this.BlackListReason = aLDBlackListSchema.getBlackListReason();
		this.MakeDate = fDate.getDate( aLDBlackListSchema.getMakeDate());
		this.MakeTime = aLDBlackListSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDBlackListSchema.getModifyDate());
		this.ModifyTime = aLDBlackListSchema.getModifyTime();
		this.BlackListSource = aLDBlackListSchema.getBlackListSource();
		this.BlackListReasonKind = aLDBlackListSchema.getBlackListReasonKind();
		this.BlackListGrade = aLDBlackListSchema.getBlackListGrade();
		this.BlackListKind = aLDBlackListSchema.getBlackListKind();
		this.RelaContNo = aLDBlackListSchema.getRelaContNo();
		this.RelaPolNo = aLDBlackListSchema.getRelaPolNo();
		this.RelaOtherNo = aLDBlackListSchema.getRelaOtherNo();
		this.RelaOtherNoType = aLDBlackListSchema.getRelaOtherNoType();
		this.BlackListOperatorType = aLDBlackListSchema.getBlackListOperatorType();
		this.IDNo = aLDBlackListSchema.getIDNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BlackListSerialNo") == null )
				this.BlackListSerialNo = null;
			else
				this.BlackListSerialNo = rs.getString("BlackListSerialNo").trim();

			if( rs.getString("BlackListNo") == null )
				this.BlackListNo = null;
			else
				this.BlackListNo = rs.getString("BlackListNo").trim();

			if( rs.getString("BlackListType") == null )
				this.BlackListType = null;
			else
				this.BlackListType = rs.getString("BlackListType").trim();

			if( rs.getString("BlackName") == null )
				this.BlackName = null;
			else
				this.BlackName = rs.getString("BlackName").trim();

			if( rs.getString("BlackListOperator") == null )
				this.BlackListOperator = null;
			else
				this.BlackListOperator = rs.getString("BlackListOperator").trim();

			this.BlackListMakeDate = rs.getDate("BlackListMakeDate");
			if( rs.getString("BlackListMakeTime") == null )
				this.BlackListMakeTime = null;
			else
				this.BlackListMakeTime = rs.getString("BlackListMakeTime").trim();

			if( rs.getString("BlackListReason") == null )
				this.BlackListReason = null;
			else
				this.BlackListReason = rs.getString("BlackListReason").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BlackListSource") == null )
				this.BlackListSource = null;
			else
				this.BlackListSource = rs.getString("BlackListSource").trim();

			if( rs.getString("BlackListReasonKind") == null )
				this.BlackListReasonKind = null;
			else
				this.BlackListReasonKind = rs.getString("BlackListReasonKind").trim();

			if( rs.getString("BlackListGrade") == null )
				this.BlackListGrade = null;
			else
				this.BlackListGrade = rs.getString("BlackListGrade").trim();

			if( rs.getString("BlackListKind") == null )
				this.BlackListKind = null;
			else
				this.BlackListKind = rs.getString("BlackListKind").trim();

			if( rs.getString("RelaContNo") == null )
				this.RelaContNo = null;
			else
				this.RelaContNo = rs.getString("RelaContNo").trim();

			if( rs.getString("RelaPolNo") == null )
				this.RelaPolNo = null;
			else
				this.RelaPolNo = rs.getString("RelaPolNo").trim();

			if( rs.getString("RelaOtherNo") == null )
				this.RelaOtherNo = null;
			else
				this.RelaOtherNo = rs.getString("RelaOtherNo").trim();

			if( rs.getString("RelaOtherNoType") == null )
				this.RelaOtherNoType = null;
			else
				this.RelaOtherNoType = rs.getString("RelaOtherNoType").trim();

			if( rs.getString("BlackListOperatorType") == null )
				this.BlackListOperatorType = null;
			else
				this.BlackListOperatorType = rs.getString("BlackListOperatorType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDBlackList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDBlackListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDBlackListSchema getSchema()
	{
		LDBlackListSchema aLDBlackListSchema = new LDBlackListSchema();
		aLDBlackListSchema.setSchema(this);
		return aLDBlackListSchema;
	}

	public LDBlackListDB getDB()
	{
		LDBlackListDB aDBOper = new LDBlackListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBlackList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(BlackListSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListOperator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( BlackListMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListMakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListReason)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListSource)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListReasonKind)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListGrade)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListKind)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RelaContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RelaPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RelaOtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RelaOtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BlackListOperatorType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IDNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBlackList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BlackListSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BlackListNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BlackListType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BlackName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BlackListOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BlackListMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			BlackListMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BlackListReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			BlackListSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BlackListReasonKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			BlackListGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			BlackListKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			RelaContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			RelaPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			RelaOtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			RelaOtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			BlackListOperatorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDBlackListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BlackListSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListSerialNo));
		}
		if (FCode.equals("BlackListNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListNo));
		}
		if (FCode.equals("BlackListType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListType));
		}
		if (FCode.equals("BlackName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackName));
		}
		if (FCode.equals("BlackListOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListOperator));
		}
		if (FCode.equals("BlackListMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBlackListMakeDate()));
		}
		if (FCode.equals("BlackListMakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListMakeTime));
		}
		if (FCode.equals("BlackListReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListReason));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BlackListSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListSource));
		}
		if (FCode.equals("BlackListReasonKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListReasonKind));
		}
		if (FCode.equals("BlackListGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListGrade));
		}
		if (FCode.equals("BlackListKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListKind));
		}
		if (FCode.equals("RelaContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaContNo));
		}
		if (FCode.equals("RelaPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPolNo));
		}
		if (FCode.equals("RelaOtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherNo));
		}
		if (FCode.equals("RelaOtherNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherNoType));
		}
		if (FCode.equals("BlackListOperatorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListOperatorType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BlackListSerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BlackListNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BlackListType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BlackName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BlackListOperator);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBlackListMakeDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(BlackListMakeTime);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BlackListReason);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(BlackListSource);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BlackListReasonKind);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(BlackListGrade);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(BlackListKind);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(RelaContNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(RelaPolNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(RelaOtherNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(RelaOtherNoType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(BlackListOperatorType);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BlackListSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListSerialNo = FValue.trim();
			}
			else
				BlackListSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("BlackListNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListNo = FValue.trim();
			}
			else
				BlackListNo = null;
		}
		if (FCode.equalsIgnoreCase("BlackListType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListType = FValue.trim();
			}
			else
				BlackListType = null;
		}
		if (FCode.equalsIgnoreCase("BlackName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackName = FValue.trim();
			}
			else
				BlackName = null;
		}
		if (FCode.equalsIgnoreCase("BlackListOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListOperator = FValue.trim();
			}
			else
				BlackListOperator = null;
		}
		if (FCode.equalsIgnoreCase("BlackListMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BlackListMakeDate = fDate.getDate( FValue );
			}
			else
				BlackListMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("BlackListMakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListMakeTime = FValue.trim();
			}
			else
				BlackListMakeTime = null;
		}
		if (FCode.equalsIgnoreCase("BlackListReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListReason = FValue.trim();
			}
			else
				BlackListReason = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BlackListSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListSource = FValue.trim();
			}
			else
				BlackListSource = null;
		}
		if (FCode.equalsIgnoreCase("BlackListReasonKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListReasonKind = FValue.trim();
			}
			else
				BlackListReasonKind = null;
		}
		if (FCode.equalsIgnoreCase("BlackListGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListGrade = FValue.trim();
			}
			else
				BlackListGrade = null;
		}
		if (FCode.equalsIgnoreCase("BlackListKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListKind = FValue.trim();
			}
			else
				BlackListKind = null;
		}
		if (FCode.equalsIgnoreCase("RelaContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaContNo = FValue.trim();
			}
			else
				RelaContNo = null;
		}
		if (FCode.equalsIgnoreCase("RelaPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaPolNo = FValue.trim();
			}
			else
				RelaPolNo = null;
		}
		if (FCode.equalsIgnoreCase("RelaOtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaOtherNo = FValue.trim();
			}
			else
				RelaOtherNo = null;
		}
		if (FCode.equalsIgnoreCase("RelaOtherNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaOtherNoType = FValue.trim();
			}
			else
				RelaOtherNoType = null;
		}
		if (FCode.equalsIgnoreCase("BlackListOperatorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlackListOperatorType = FValue.trim();
			}
			else
				BlackListOperatorType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDBlackListSchema other = (LDBlackListSchema)otherObject;
		return
			BlackListSerialNo.equals(other.getBlackListSerialNo())
			&& BlackListNo.equals(other.getBlackListNo())
			&& BlackListType.equals(other.getBlackListType())
			&& BlackName.equals(other.getBlackName())
			&& BlackListOperator.equals(other.getBlackListOperator())
			&& fDate.getString(BlackListMakeDate).equals(other.getBlackListMakeDate())
			&& BlackListMakeTime.equals(other.getBlackListMakeTime())
			&& BlackListReason.equals(other.getBlackListReason())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& BlackListSource.equals(other.getBlackListSource())
			&& BlackListReasonKind.equals(other.getBlackListReasonKind())
			&& BlackListGrade.equals(other.getBlackListGrade())
			&& BlackListKind.equals(other.getBlackListKind())
			&& RelaContNo.equals(other.getRelaContNo())
			&& RelaPolNo.equals(other.getRelaPolNo())
			&& RelaOtherNo.equals(other.getRelaOtherNo())
			&& RelaOtherNoType.equals(other.getRelaOtherNoType())
			&& BlackListOperatorType.equals(other.getBlackListOperatorType())
			&& IDNo.equals(other.getIDNo());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BlackListSerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("BlackListNo") ) {
			return 1;
		}
		if( strFieldName.equals("BlackListType") ) {
			return 2;
		}
		if( strFieldName.equals("BlackName") ) {
			return 3;
		}
		if( strFieldName.equals("BlackListOperator") ) {
			return 4;
		}
		if( strFieldName.equals("BlackListMakeDate") ) {
			return 5;
		}
		if( strFieldName.equals("BlackListMakeTime") ) {
			return 6;
		}
		if( strFieldName.equals("BlackListReason") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		if( strFieldName.equals("BlackListSource") ) {
			return 12;
		}
		if( strFieldName.equals("BlackListReasonKind") ) {
			return 13;
		}
		if( strFieldName.equals("BlackListGrade") ) {
			return 14;
		}
		if( strFieldName.equals("BlackListKind") ) {
			return 15;
		}
		if( strFieldName.equals("RelaContNo") ) {
			return 16;
		}
		if( strFieldName.equals("RelaPolNo") ) {
			return 17;
		}
		if( strFieldName.equals("RelaOtherNo") ) {
			return 18;
		}
		if( strFieldName.equals("RelaOtherNoType") ) {
			return 19;
		}
		if( strFieldName.equals("BlackListOperatorType") ) {
			return 20;
		}
		if( strFieldName.equals("IDNo") ) {
			return 21;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BlackListSerialNo";
				break;
			case 1:
				strFieldName = "BlackListNo";
				break;
			case 2:
				strFieldName = "BlackListType";
				break;
			case 3:
				strFieldName = "BlackName";
				break;
			case 4:
				strFieldName = "BlackListOperator";
				break;
			case 5:
				strFieldName = "BlackListMakeDate";
				break;
			case 6:
				strFieldName = "BlackListMakeTime";
				break;
			case 7:
				strFieldName = "BlackListReason";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			case 12:
				strFieldName = "BlackListSource";
				break;
			case 13:
				strFieldName = "BlackListReasonKind";
				break;
			case 14:
				strFieldName = "BlackListGrade";
				break;
			case 15:
				strFieldName = "BlackListKind";
				break;
			case 16:
				strFieldName = "RelaContNo";
				break;
			case 17:
				strFieldName = "RelaPolNo";
				break;
			case 18:
				strFieldName = "RelaOtherNo";
				break;
			case 19:
				strFieldName = "RelaOtherNoType";
				break;
			case 20:
				strFieldName = "BlackListOperatorType";
				break;
			case 21:
				strFieldName = "IDNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BlackListSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BlackListMakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListReasonKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelaContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelaPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelaOtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelaOtherNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlackListOperatorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
