/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLSubReportDB;

/*
 * <p>ClassName: LLSubReportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-09-04
 */
public class LLSubReportSchema implements Schema, Cloneable
{
	// @Field
	/** 分报案号(事件号) */
	private String SubRptNo;
	/** 关联客户号码 */
	private String CustomerNo;
	/** 关联客户的名称 */
	private String CustomerName;
	/** 关联客户类型 */
	private String CustomerType;
	/** 事件主题 */
	private String AccSubject;
	/** 事故类型 */
	private String AccidentType;
	/** 发生日期 */
	private Date AccDate;
	/** 终止日期 */
	private Date AccEndDate;
	/** 事故描述 */
	private String AccDesc;
	/** 事故者状况 */
	private String CustSituation;
	/** 事故地点 */
	private String AccPlace;
	/** 医院代码 */
	private String HospitalCode;
	/** 医院名称 */
	private String HospitalName;
	/** 入院日期 */
	private Date InHospitalDate;
	/** 出院日期 */
	private Date OutHospitalDate;
	/** 备注 */
	private String Remark;
	/** 重大事件标志 */
	private String SeriousGrade;
	/** 调查报告标志 */
	private String SurveyFlag;
	/** 操作员 */
	private String Operator;
	/** 管理机构 */
	private String MngCom;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 事故者死亡日期 */
	private Date DieDate;
	/** 首诊日 */
	private Date FirstDiaDate;
	/** 医院级别 */
	private String HosGrade;
	/** 所在地 */
	private String LocalPlace;
	/** 医院联系电话 */
	private String HosTel;
	/** 发生地点省 */
	private String AccProvinceCode;
	/** 发生地点市 */
	private String AccCityCode;
	/** 发生地点县 */
	private String AccCountyCode;

	public static final int FIELDNUM = 32;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLSubReportSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SubRptNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLSubReportSchema cloned = (LLSubReportSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSubRptNo()
	{
		return SubRptNo;
	}
	public void setSubRptNo(String aSubRptNo)
	{
		SubRptNo = aSubRptNo;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getCustomerType()
	{
		return CustomerType;
	}
	public void setCustomerType(String aCustomerType)
	{
		CustomerType = aCustomerType;
	}
	public String getAccSubject()
	{
		return AccSubject;
	}
	public void setAccSubject(String aAccSubject)
	{
		AccSubject = aAccSubject;
	}
	public String getAccidentType()
	{
		return AccidentType;
	}
	public void setAccidentType(String aAccidentType)
	{
		AccidentType = aAccidentType;
	}
	public String getAccDate()
	{
		if( AccDate != null )
			return fDate.getString(AccDate);
		else
			return null;
	}
	public void setAccDate(Date aAccDate)
	{
		AccDate = aAccDate;
	}
	public void setAccDate(String aAccDate)
	{
		if (aAccDate != null && !aAccDate.equals("") )
		{
			AccDate = fDate.getDate( aAccDate );
		}
		else
			AccDate = null;
	}

	public String getAccEndDate()
	{
		if( AccEndDate != null )
			return fDate.getString(AccEndDate);
		else
			return null;
	}
	public void setAccEndDate(Date aAccEndDate)
	{
		AccEndDate = aAccEndDate;
	}
	public void setAccEndDate(String aAccEndDate)
	{
		if (aAccEndDate != null && !aAccEndDate.equals("") )
		{
			AccEndDate = fDate.getDate( aAccEndDate );
		}
		else
			AccEndDate = null;
	}

	public String getAccDesc()
	{
		return AccDesc;
	}
	public void setAccDesc(String aAccDesc)
	{
		AccDesc = aAccDesc;
	}
	public String getCustSituation()
	{
		return CustSituation;
	}
	public void setCustSituation(String aCustSituation)
	{
		CustSituation = aCustSituation;
	}
	public String getAccPlace()
	{
		return AccPlace;
	}
	public void setAccPlace(String aAccPlace)
	{
		AccPlace = aAccPlace;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
		HospitalCode = aHospitalCode;
	}
	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
		HospitalName = aHospitalName;
	}
	public String getInHospitalDate()
	{
		if( InHospitalDate != null )
			return fDate.getString(InHospitalDate);
		else
			return null;
	}
	public void setInHospitalDate(Date aInHospitalDate)
	{
		InHospitalDate = aInHospitalDate;
	}
	public void setInHospitalDate(String aInHospitalDate)
	{
		if (aInHospitalDate != null && !aInHospitalDate.equals("") )
		{
			InHospitalDate = fDate.getDate( aInHospitalDate );
		}
		else
			InHospitalDate = null;
	}

	public String getOutHospitalDate()
	{
		if( OutHospitalDate != null )
			return fDate.getString(OutHospitalDate);
		else
			return null;
	}
	public void setOutHospitalDate(Date aOutHospitalDate)
	{
		OutHospitalDate = aOutHospitalDate;
	}
	public void setOutHospitalDate(String aOutHospitalDate)
	{
		if (aOutHospitalDate != null && !aOutHospitalDate.equals("") )
		{
			OutHospitalDate = fDate.getDate( aOutHospitalDate );
		}
		else
			OutHospitalDate = null;
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getSeriousGrade()
	{
		return SeriousGrade;
	}
	public void setSeriousGrade(String aSeriousGrade)
	{
		SeriousGrade = aSeriousGrade;
	}
	public String getSurveyFlag()
	{
		return SurveyFlag;
	}
	public void setSurveyFlag(String aSurveyFlag)
	{
		SurveyFlag = aSurveyFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getDieDate()
	{
		if( DieDate != null )
			return fDate.getString(DieDate);
		else
			return null;
	}
	public void setDieDate(Date aDieDate)
	{
		DieDate = aDieDate;
	}
	public void setDieDate(String aDieDate)
	{
		if (aDieDate != null && !aDieDate.equals("") )
		{
			DieDate = fDate.getDate( aDieDate );
		}
		else
			DieDate = null;
	}

	public String getFirstDiaDate()
	{
		if( FirstDiaDate != null )
			return fDate.getString(FirstDiaDate);
		else
			return null;
	}
	public void setFirstDiaDate(Date aFirstDiaDate)
	{
		FirstDiaDate = aFirstDiaDate;
	}
	public void setFirstDiaDate(String aFirstDiaDate)
	{
		if (aFirstDiaDate != null && !aFirstDiaDate.equals("") )
		{
			FirstDiaDate = fDate.getDate( aFirstDiaDate );
		}
		else
			FirstDiaDate = null;
	}

	public String getHosGrade()
	{
		return HosGrade;
	}
	public void setHosGrade(String aHosGrade)
	{
		HosGrade = aHosGrade;
	}
	public String getLocalPlace()
	{
		return LocalPlace;
	}
	public void setLocalPlace(String aLocalPlace)
	{
		LocalPlace = aLocalPlace;
	}
	public String getHosTel()
	{
		return HosTel;
	}
	public void setHosTel(String aHosTel)
	{
		HosTel = aHosTel;
	}
	public String getAccProvinceCode()
	{
		return AccProvinceCode;
	}
	public void setAccProvinceCode(String aAccProvinceCode)
	{
		AccProvinceCode = aAccProvinceCode;
	}
	public String getAccCityCode()
	{
		return AccCityCode;
	}
	public void setAccCityCode(String aAccCityCode)
	{
		AccCityCode = aAccCityCode;
	}
	public String getAccCountyCode()
	{
		return AccCountyCode;
	}
	public void setAccCountyCode(String aAccCountyCode)
	{
		AccCountyCode = aAccCountyCode;
	}

	/**
	* 使用另外一个 LLSubReportSchema 对象给 Schema 赋值
	* @param: aLLSubReportSchema LLSubReportSchema
	**/
	public void setSchema(LLSubReportSchema aLLSubReportSchema)
	{
		this.SubRptNo = aLLSubReportSchema.getSubRptNo();
		this.CustomerNo = aLLSubReportSchema.getCustomerNo();
		this.CustomerName = aLLSubReportSchema.getCustomerName();
		this.CustomerType = aLLSubReportSchema.getCustomerType();
		this.AccSubject = aLLSubReportSchema.getAccSubject();
		this.AccidentType = aLLSubReportSchema.getAccidentType();
		this.AccDate = fDate.getDate( aLLSubReportSchema.getAccDate());
		this.AccEndDate = fDate.getDate( aLLSubReportSchema.getAccEndDate());
		this.AccDesc = aLLSubReportSchema.getAccDesc();
		this.CustSituation = aLLSubReportSchema.getCustSituation();
		this.AccPlace = aLLSubReportSchema.getAccPlace();
		this.HospitalCode = aLLSubReportSchema.getHospitalCode();
		this.HospitalName = aLLSubReportSchema.getHospitalName();
		this.InHospitalDate = fDate.getDate( aLLSubReportSchema.getInHospitalDate());
		this.OutHospitalDate = fDate.getDate( aLLSubReportSchema.getOutHospitalDate());
		this.Remark = aLLSubReportSchema.getRemark();
		this.SeriousGrade = aLLSubReportSchema.getSeriousGrade();
		this.SurveyFlag = aLLSubReportSchema.getSurveyFlag();
		this.Operator = aLLSubReportSchema.getOperator();
		this.MngCom = aLLSubReportSchema.getMngCom();
		this.MakeDate = fDate.getDate( aLLSubReportSchema.getMakeDate());
		this.MakeTime = aLLSubReportSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLSubReportSchema.getModifyDate());
		this.ModifyTime = aLLSubReportSchema.getModifyTime();
		this.DieDate = fDate.getDate( aLLSubReportSchema.getDieDate());
		this.FirstDiaDate = fDate.getDate( aLLSubReportSchema.getFirstDiaDate());
		this.HosGrade = aLLSubReportSchema.getHosGrade();
		this.LocalPlace = aLLSubReportSchema.getLocalPlace();
		this.HosTel = aLLSubReportSchema.getHosTel();
		this.AccProvinceCode = aLLSubReportSchema.getAccProvinceCode();
		this.AccCityCode = aLLSubReportSchema.getAccCityCode();
		this.AccCountyCode = aLLSubReportSchema.getAccCountyCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SubRptNo") == null )
				this.SubRptNo = null;
			else
				this.SubRptNo = rs.getString("SubRptNo").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("CustomerType") == null )
				this.CustomerType = null;
			else
				this.CustomerType = rs.getString("CustomerType").trim();

			if( rs.getString("AccSubject") == null )
				this.AccSubject = null;
			else
				this.AccSubject = rs.getString("AccSubject").trim();

			if( rs.getString("AccidentType") == null )
				this.AccidentType = null;
			else
				this.AccidentType = rs.getString("AccidentType").trim();

			this.AccDate = rs.getDate("AccDate");
			this.AccEndDate = rs.getDate("AccEndDate");
			if( rs.getString("AccDesc") == null )
				this.AccDesc = null;
			else
				this.AccDesc = rs.getString("AccDesc").trim();

			if( rs.getString("CustSituation") == null )
				this.CustSituation = null;
			else
				this.CustSituation = rs.getString("CustSituation").trim();

			if( rs.getString("AccPlace") == null )
				this.AccPlace = null;
			else
				this.AccPlace = rs.getString("AccPlace").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			this.InHospitalDate = rs.getDate("InHospitalDate");
			this.OutHospitalDate = rs.getDate("OutHospitalDate");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("SeriousGrade") == null )
				this.SeriousGrade = null;
			else
				this.SeriousGrade = rs.getString("SeriousGrade").trim();

			if( rs.getString("SurveyFlag") == null )
				this.SurveyFlag = null;
			else
				this.SurveyFlag = rs.getString("SurveyFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.DieDate = rs.getDate("DieDate");
			this.FirstDiaDate = rs.getDate("FirstDiaDate");
			if( rs.getString("HosGrade") == null )
				this.HosGrade = null;
			else
				this.HosGrade = rs.getString("HosGrade").trim();

			if( rs.getString("LocalPlace") == null )
				this.LocalPlace = null;
			else
				this.LocalPlace = rs.getString("LocalPlace").trim();

			if( rs.getString("HosTel") == null )
				this.HosTel = null;
			else
				this.HosTel = rs.getString("HosTel").trim();

			if( rs.getString("AccProvinceCode") == null )
				this.AccProvinceCode = null;
			else
				this.AccProvinceCode = rs.getString("AccProvinceCode").trim();

			if( rs.getString("AccCityCode") == null )
				this.AccCityCode = null;
			else
				this.AccCityCode = rs.getString("AccCityCode").trim();

			if( rs.getString("AccCountyCode") == null )
				this.AccCountyCode = null;
			else
				this.AccCountyCode = rs.getString("AccCountyCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLSubReport表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSubReportSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLSubReportSchema getSchema()
	{
		LLSubReportSchema aLLSubReportSchema = new LLSubReportSchema();
		aLLSubReportSchema.setSchema(this);
		return aLLSubReportSchema;
	}

	public LLSubReportDB getDB()
	{
		LLSubReportDB aDBOper = new LLSubReportDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSubReport描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SubRptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccSubject)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccidentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustSituation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccPlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InHospitalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OutHospitalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SeriousGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SurveyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DieDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FirstDiaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HosGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LocalPlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HosTel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccProvinceCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccCityCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccCountyCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLSubReport>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SubRptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CustomerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AccSubject = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AccidentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			AccEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			AccDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CustSituation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AccPlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			InHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			OutHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			SeriousGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			DieDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			FirstDiaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			HosGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			LocalPlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			HosTel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			AccProvinceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			AccCityCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			AccCountyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLSubReportSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SubRptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubRptNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("CustomerType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerType));
		}
		if (FCode.equals("AccSubject"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccSubject));
		}
		if (FCode.equals("AccidentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentType));
		}
		if (FCode.equals("AccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccDate()));
		}
		if (FCode.equals("AccEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccEndDate()));
		}
		if (FCode.equals("AccDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccDesc));
		}
		if (FCode.equals("CustSituation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustSituation));
		}
		if (FCode.equals("AccPlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccPlace));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("InHospitalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInHospitalDate()));
		}
		if (FCode.equals("OutHospitalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOutHospitalDate()));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("SeriousGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeriousGrade));
		}
		if (FCode.equals("SurveyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("DieDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDieDate()));
		}
		if (FCode.equals("FirstDiaDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstDiaDate()));
		}
		if (FCode.equals("HosGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosGrade));
		}
		if (FCode.equals("LocalPlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LocalPlace));
		}
		if (FCode.equals("HosTel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosTel));
		}
		if (FCode.equals("AccProvinceCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccProvinceCode));
		}
		if (FCode.equals("AccCityCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccCityCode));
		}
		if (FCode.equals("AccCountyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccCountyCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SubRptNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CustomerType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AccSubject);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AccidentType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccEndDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AccDesc);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CustSituation);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AccPlace);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInHospitalDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOutHospitalDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(SeriousGrade);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDieDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstDiaDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(HosGrade);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(LocalPlace);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(HosTel);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(AccProvinceCode);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(AccCityCode);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(AccCountyCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SubRptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubRptNo = FValue.trim();
			}
			else
				SubRptNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("CustomerType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerType = FValue.trim();
			}
			else
				CustomerType = null;
		}
		if (FCode.equalsIgnoreCase("AccSubject"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccSubject = FValue.trim();
			}
			else
				AccSubject = null;
		}
		if (FCode.equalsIgnoreCase("AccidentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccidentType = FValue.trim();
			}
			else
				AccidentType = null;
		}
		if (FCode.equalsIgnoreCase("AccDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccDate = fDate.getDate( FValue );
			}
			else
				AccDate = null;
		}
		if (FCode.equalsIgnoreCase("AccEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccEndDate = fDate.getDate( FValue );
			}
			else
				AccEndDate = null;
		}
		if (FCode.equalsIgnoreCase("AccDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccDesc = FValue.trim();
			}
			else
				AccDesc = null;
		}
		if (FCode.equalsIgnoreCase("CustSituation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustSituation = FValue.trim();
			}
			else
				CustSituation = null;
		}
		if (FCode.equalsIgnoreCase("AccPlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccPlace = FValue.trim();
			}
			else
				AccPlace = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("InHospitalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InHospitalDate = fDate.getDate( FValue );
			}
			else
				InHospitalDate = null;
		}
		if (FCode.equalsIgnoreCase("OutHospitalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OutHospitalDate = fDate.getDate( FValue );
			}
			else
				OutHospitalDate = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("SeriousGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SeriousGrade = FValue.trim();
			}
			else
				SeriousGrade = null;
		}
		if (FCode.equalsIgnoreCase("SurveyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SurveyFlag = FValue.trim();
			}
			else
				SurveyFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("DieDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DieDate = fDate.getDate( FValue );
			}
			else
				DieDate = null;
		}
		if (FCode.equalsIgnoreCase("FirstDiaDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FirstDiaDate = fDate.getDate( FValue );
			}
			else
				FirstDiaDate = null;
		}
		if (FCode.equalsIgnoreCase("HosGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosGrade = FValue.trim();
			}
			else
				HosGrade = null;
		}
		if (FCode.equalsIgnoreCase("LocalPlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LocalPlace = FValue.trim();
			}
			else
				LocalPlace = null;
		}
		if (FCode.equalsIgnoreCase("HosTel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosTel = FValue.trim();
			}
			else
				HosTel = null;
		}
		if (FCode.equalsIgnoreCase("AccProvinceCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccProvinceCode = FValue.trim();
			}
			else
				AccProvinceCode = null;
		}
		if (FCode.equalsIgnoreCase("AccCityCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccCityCode = FValue.trim();
			}
			else
				AccCityCode = null;
		}
		if (FCode.equalsIgnoreCase("AccCountyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccCountyCode = FValue.trim();
			}
			else
				AccCountyCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLSubReportSchema other = (LLSubReportSchema)otherObject;
		return
			(SubRptNo == null ? other.getSubRptNo() == null : SubRptNo.equals(other.getSubRptNo()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (CustomerType == null ? other.getCustomerType() == null : CustomerType.equals(other.getCustomerType()))
			&& (AccSubject == null ? other.getAccSubject() == null : AccSubject.equals(other.getAccSubject()))
			&& (AccidentType == null ? other.getAccidentType() == null : AccidentType.equals(other.getAccidentType()))
			&& (AccDate == null ? other.getAccDate() == null : fDate.getString(AccDate).equals(other.getAccDate()))
			&& (AccEndDate == null ? other.getAccEndDate() == null : fDate.getString(AccEndDate).equals(other.getAccEndDate()))
			&& (AccDesc == null ? other.getAccDesc() == null : AccDesc.equals(other.getAccDesc()))
			&& (CustSituation == null ? other.getCustSituation() == null : CustSituation.equals(other.getCustSituation()))
			&& (AccPlace == null ? other.getAccPlace() == null : AccPlace.equals(other.getAccPlace()))
			&& (HospitalCode == null ? other.getHospitalCode() == null : HospitalCode.equals(other.getHospitalCode()))
			&& (HospitalName == null ? other.getHospitalName() == null : HospitalName.equals(other.getHospitalName()))
			&& (InHospitalDate == null ? other.getInHospitalDate() == null : fDate.getString(InHospitalDate).equals(other.getInHospitalDate()))
			&& (OutHospitalDate == null ? other.getOutHospitalDate() == null : fDate.getString(OutHospitalDate).equals(other.getOutHospitalDate()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (SeriousGrade == null ? other.getSeriousGrade() == null : SeriousGrade.equals(other.getSeriousGrade()))
			&& (SurveyFlag == null ? other.getSurveyFlag() == null : SurveyFlag.equals(other.getSurveyFlag()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (DieDate == null ? other.getDieDate() == null : fDate.getString(DieDate).equals(other.getDieDate()))
			&& (FirstDiaDate == null ? other.getFirstDiaDate() == null : fDate.getString(FirstDiaDate).equals(other.getFirstDiaDate()))
			&& (HosGrade == null ? other.getHosGrade() == null : HosGrade.equals(other.getHosGrade()))
			&& (LocalPlace == null ? other.getLocalPlace() == null : LocalPlace.equals(other.getLocalPlace()))
			&& (HosTel == null ? other.getHosTel() == null : HosTel.equals(other.getHosTel()))
			&& (AccProvinceCode == null ? other.getAccProvinceCode() == null : AccProvinceCode.equals(other.getAccProvinceCode()))
			&& (AccCityCode == null ? other.getAccCityCode() == null : AccCityCode.equals(other.getAccCityCode()))
			&& (AccCountyCode == null ? other.getAccCountyCode() == null : AccCountyCode.equals(other.getAccCountyCode()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SubRptNo") ) {
			return 0;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 1;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 2;
		}
		if( strFieldName.equals("CustomerType") ) {
			return 3;
		}
		if( strFieldName.equals("AccSubject") ) {
			return 4;
		}
		if( strFieldName.equals("AccidentType") ) {
			return 5;
		}
		if( strFieldName.equals("AccDate") ) {
			return 6;
		}
		if( strFieldName.equals("AccEndDate") ) {
			return 7;
		}
		if( strFieldName.equals("AccDesc") ) {
			return 8;
		}
		if( strFieldName.equals("CustSituation") ) {
			return 9;
		}
		if( strFieldName.equals("AccPlace") ) {
			return 10;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 11;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 12;
		}
		if( strFieldName.equals("InHospitalDate") ) {
			return 13;
		}
		if( strFieldName.equals("OutHospitalDate") ) {
			return 14;
		}
		if( strFieldName.equals("Remark") ) {
			return 15;
		}
		if( strFieldName.equals("SeriousGrade") ) {
			return 16;
		}
		if( strFieldName.equals("SurveyFlag") ) {
			return 17;
		}
		if( strFieldName.equals("Operator") ) {
			return 18;
		}
		if( strFieldName.equals("MngCom") ) {
			return 19;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 20;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 23;
		}
		if( strFieldName.equals("DieDate") ) {
			return 24;
		}
		if( strFieldName.equals("FirstDiaDate") ) {
			return 25;
		}
		if( strFieldName.equals("HosGrade") ) {
			return 26;
		}
		if( strFieldName.equals("LocalPlace") ) {
			return 27;
		}
		if( strFieldName.equals("HosTel") ) {
			return 28;
		}
		if( strFieldName.equals("AccProvinceCode") ) {
			return 29;
		}
		if( strFieldName.equals("AccCityCode") ) {
			return 30;
		}
		if( strFieldName.equals("AccCountyCode") ) {
			return 31;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SubRptNo";
				break;
			case 1:
				strFieldName = "CustomerNo";
				break;
			case 2:
				strFieldName = "CustomerName";
				break;
			case 3:
				strFieldName = "CustomerType";
				break;
			case 4:
				strFieldName = "AccSubject";
				break;
			case 5:
				strFieldName = "AccidentType";
				break;
			case 6:
				strFieldName = "AccDate";
				break;
			case 7:
				strFieldName = "AccEndDate";
				break;
			case 8:
				strFieldName = "AccDesc";
				break;
			case 9:
				strFieldName = "CustSituation";
				break;
			case 10:
				strFieldName = "AccPlace";
				break;
			case 11:
				strFieldName = "HospitalCode";
				break;
			case 12:
				strFieldName = "HospitalName";
				break;
			case 13:
				strFieldName = "InHospitalDate";
				break;
			case 14:
				strFieldName = "OutHospitalDate";
				break;
			case 15:
				strFieldName = "Remark";
				break;
			case 16:
				strFieldName = "SeriousGrade";
				break;
			case 17:
				strFieldName = "SurveyFlag";
				break;
			case 18:
				strFieldName = "Operator";
				break;
			case 19:
				strFieldName = "MngCom";
				break;
			case 20:
				strFieldName = "MakeDate";
				break;
			case 21:
				strFieldName = "MakeTime";
				break;
			case 22:
				strFieldName = "ModifyDate";
				break;
			case 23:
				strFieldName = "ModifyTime";
				break;
			case 24:
				strFieldName = "DieDate";
				break;
			case 25:
				strFieldName = "FirstDiaDate";
				break;
			case 26:
				strFieldName = "HosGrade";
				break;
			case 27:
				strFieldName = "LocalPlace";
				break;
			case 28:
				strFieldName = "HosTel";
				break;
			case 29:
				strFieldName = "AccProvinceCode";
				break;
			case 30:
				strFieldName = "AccCityCode";
				break;
			case 31:
				strFieldName = "AccCountyCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SubRptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccSubject") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccidentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustSituation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccPlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InHospitalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OutHospitalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SeriousGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SurveyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DieDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FirstDiaDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HosGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LocalPlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HosTel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccProvinceCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccCityCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccCountyCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
