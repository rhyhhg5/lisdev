/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.Crs_CheckResultLogDB;

/*
 * <p>ClassName: Crs_CheckResultLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 卡式业务
 * @CreateDate：2010-07-09
 */
public class Crs_CheckResultLogSchema implements Schema, Cloneable
{
	// @Field
	/** 批次名称 */
	private String MsgBatchNo;
	/** 信息唯一标识 */
	private String info_id;
	/** 校验方式 */
	private String check_type;
	/** 校验描述 */
	private String check_account;
	/** 校验时间 */
	private String check_date;
	/** 子公司报送时间 */
	private String date_send;
	/** 状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public Crs_CheckResultLogSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "MsgBatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		Crs_CheckResultLogSchema cloned = (Crs_CheckResultLogSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMsgBatchNo()
	{
		return MsgBatchNo;
	}
	public void setMsgBatchNo(String aMsgBatchNo)
	{
		MsgBatchNo = aMsgBatchNo;
	}
	public String getinfo_id()
	{
		return info_id;
	}
	public void setinfo_id(String ainfo_id)
	{
		info_id = ainfo_id;
	}
	public String getcheck_type()
	{
		return check_type;
	}
	public void setcheck_type(String acheck_type)
	{
		check_type = acheck_type;
	}
	public String getcheck_account()
	{
		return check_account;
	}
	public void setcheck_account(String acheck_account)
	{
		check_account = acheck_account;
	}
	public String getcheck_date()
	{
		return check_date;
	}
	public void setcheck_date(String acheck_date)
	{
		check_date = acheck_date;
	}
	public String getdate_send()
	{
		return date_send;
	}
	public void setdate_send(String adate_send)
	{
		date_send = adate_send;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 Crs_CheckResultLogSchema 对象给 Schema 赋值
	* @param: aCrs_CheckResultLogSchema Crs_CheckResultLogSchema
	**/
	public void setSchema(Crs_CheckResultLogSchema aCrs_CheckResultLogSchema)
	{
		this.MsgBatchNo = aCrs_CheckResultLogSchema.getMsgBatchNo();
		this.info_id = aCrs_CheckResultLogSchema.getinfo_id();
		this.check_type = aCrs_CheckResultLogSchema.getcheck_type();
		this.check_account = aCrs_CheckResultLogSchema.getcheck_account();
		this.check_date = aCrs_CheckResultLogSchema.getcheck_date();
		this.date_send = aCrs_CheckResultLogSchema.getdate_send();
		this.State = aCrs_CheckResultLogSchema.getState();
		this.Operator = aCrs_CheckResultLogSchema.getOperator();
		this.MakeDate = fDate.getDate( aCrs_CheckResultLogSchema.getMakeDate());
		this.MakeTime = aCrs_CheckResultLogSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aCrs_CheckResultLogSchema.getModifyDate());
		this.ModifyTime = aCrs_CheckResultLogSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MsgBatchNo") == null )
				this.MsgBatchNo = null;
			else
				this.MsgBatchNo = rs.getString("MsgBatchNo").trim();

			if( rs.getString("info_id") == null )
				this.info_id = null;
			else
				this.info_id = rs.getString("info_id").trim();

			if( rs.getString("check_type") == null )
				this.check_type = null;
			else
				this.check_type = rs.getString("check_type").trim();

			if( rs.getString("check_account") == null )
				this.check_account = null;
			else
				this.check_account = rs.getString("check_account").trim();

			if( rs.getString("check_date") == null )
				this.check_date = null;
			else
				this.check_date = rs.getString("check_date").trim();

			if( rs.getString("date_send") == null )
				this.date_send = null;
			else
				this.date_send = rs.getString("date_send").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的Crs_CheckResultLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Crs_CheckResultLogSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public Crs_CheckResultLogSchema getSchema()
	{
		Crs_CheckResultLogSchema aCrs_CheckResultLogSchema = new Crs_CheckResultLogSchema();
		aCrs_CheckResultLogSchema.setSchema(this);
		return aCrs_CheckResultLogSchema;
	}

	public Crs_CheckResultLogDB getDB()
	{
		Crs_CheckResultLogDB aDBOper = new Crs_CheckResultLogDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCrs_CheckResultLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MsgBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(info_id)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(check_type)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(check_account)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(check_date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(date_send)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCrs_CheckResultLog>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MsgBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			info_id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			check_type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			check_account = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			check_date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			date_send = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Crs_CheckResultLogSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MsgBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgBatchNo));
		}
		if (FCode.equals("info_id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(info_id));
		}
		if (FCode.equals("check_type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(check_type));
		}
		if (FCode.equals("check_account"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(check_account));
		}
		if (FCode.equals("check_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(check_date));
		}
		if (FCode.equals("date_send"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(date_send));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MsgBatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(info_id);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(check_type);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(check_account);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(check_date);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(date_send);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MsgBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgBatchNo = FValue.trim();
			}
			else
				MsgBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("info_id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				info_id = FValue.trim();
			}
			else
				info_id = null;
		}
		if (FCode.equalsIgnoreCase("check_type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				check_type = FValue.trim();
			}
			else
				check_type = null;
		}
		if (FCode.equalsIgnoreCase("check_account"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				check_account = FValue.trim();
			}
			else
				check_account = null;
		}
		if (FCode.equalsIgnoreCase("check_date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				check_date = FValue.trim();
			}
			else
				check_date = null;
		}
		if (FCode.equalsIgnoreCase("date_send"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				date_send = FValue.trim();
			}
			else
				date_send = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		Crs_CheckResultLogSchema other = (Crs_CheckResultLogSchema)otherObject;
		return
			(MsgBatchNo == null ? other.getMsgBatchNo() == null : MsgBatchNo.equals(other.getMsgBatchNo()))
			&& (info_id == null ? other.getinfo_id() == null : info_id.equals(other.getinfo_id()))
			&& (check_type == null ? other.getcheck_type() == null : check_type.equals(other.getcheck_type()))
			&& (check_account == null ? other.getcheck_account() == null : check_account.equals(other.getcheck_account()))
			&& (check_date == null ? other.getcheck_date() == null : check_date.equals(other.getcheck_date()))
			&& (date_send == null ? other.getdate_send() == null : date_send.equals(other.getdate_send()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MsgBatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("info_id") ) {
			return 1;
		}
		if( strFieldName.equals("check_type") ) {
			return 2;
		}
		if( strFieldName.equals("check_account") ) {
			return 3;
		}
		if( strFieldName.equals("check_date") ) {
			return 4;
		}
		if( strFieldName.equals("date_send") ) {
			return 5;
		}
		if( strFieldName.equals("State") ) {
			return 6;
		}
		if( strFieldName.equals("Operator") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MsgBatchNo";
				break;
			case 1:
				strFieldName = "info_id";
				break;
			case 2:
				strFieldName = "check_type";
				break;
			case 3:
				strFieldName = "check_account";
				break;
			case 4:
				strFieldName = "check_date";
				break;
			case 5:
				strFieldName = "date_send";
				break;
			case 6:
				strFieldName = "State";
				break;
			case 7:
				strFieldName = "Operator";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MsgBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("info_id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("check_type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("check_account") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("check_date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("date_send") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
