/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDUWGradeDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDUWGradeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-16
 */
public class LDUWGradeSchema implements Schema, Cloneable {
    // @Field
    /** 核保师级别 */
    private String UWGrade;
    /** 核保师级别名称 */
    private String UWGradeName;
    /** 业务类型 */
    private String ContFlag;
    /** 险种类型 */
    private String RiskType;
    /** 协议业务标记 */
    private String AgreementFlag;
    /** 加费比例 */
    private double AddPrem;
    /** 免责项目个数 */
    private int SpecItemNum;
    /** 权限类型 */
    private String PopedomType;
    /** 权限方向 */
    private String PopedomDirect;
    /** 权限值 */
    private double PopedomValue;
    /** 备用字段1 */
    private String StandbyFlag1;
    /** 备用字段2 */
    private String StandbyFlag2;
    /** 险种编码 */
    private String RiskCode;
    /** 管理机构 */
    private String ManageCom;
    /** 权限方向2 */
    private String PopedomDirect2;
    /** 权限值2 */
    private double PopedomValue2;
    /** 权限方向类型 */
    private String PopedomDirectType;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDUWGradeSchema() {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "UWGrade";
        pk[1] = "ContFlag";
        pk[2] = "RiskType";
        pk[3] = "PopedomType";
        pk[4] = "RiskCode";
        pk[5] = "ManageCom";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDUWGradeSchema cloned = (LDUWGradeSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getUWGrade() {
        return UWGrade;
    }

    public void setUWGrade(String aUWGrade) {
        UWGrade = aUWGrade;
    }

    public String getUWGradeName() {
        return UWGradeName;
    }

    public void setUWGradeName(String aUWGradeName) {
        UWGradeName = aUWGradeName;
    }

    public String getContFlag() {
        return ContFlag;
    }

    public void setContFlag(String aContFlag) {
        ContFlag = aContFlag;
    }

    public String getRiskType() {
        return RiskType;
    }

    public void setRiskType(String aRiskType) {
        RiskType = aRiskType;
    }

    public String getAgreementFlag() {
        return AgreementFlag;
    }

    public void setAgreementFlag(String aAgreementFlag) {
        AgreementFlag = aAgreementFlag;
    }

    public double getAddPrem() {
        return AddPrem;
    }

    public void setAddPrem(double aAddPrem) {
        AddPrem = Arith.round(aAddPrem, 2);
    }

    public void setAddPrem(String aAddPrem) {
        if (aAddPrem != null && !aAddPrem.equals("")) {
            Double tDouble = new Double(aAddPrem);
            double d = tDouble.doubleValue();
            AddPrem = Arith.round(d, 2);
        }
    }

    public int getSpecItemNum() {
        return SpecItemNum;
    }

    public void setSpecItemNum(int aSpecItemNum) {
        SpecItemNum = aSpecItemNum;
    }

    public void setSpecItemNum(String aSpecItemNum) {
        if (aSpecItemNum != null && !aSpecItemNum.equals("")) {
            Integer tInteger = new Integer(aSpecItemNum);
            int i = tInteger.intValue();
            SpecItemNum = i;
        }
    }

    public String getPopedomType() {
        return PopedomType;
    }

    public void setPopedomType(String aPopedomType) {
        PopedomType = aPopedomType;
    }

    public String getPopedomDirect() {
        return PopedomDirect;
    }

    public void setPopedomDirect(String aPopedomDirect) {
        PopedomDirect = aPopedomDirect;
    }

    public double getPopedomValue() {
        return PopedomValue;
    }

    public void setPopedomValue(double aPopedomValue) {
        PopedomValue = Arith.round(aPopedomValue, 2);
    }

    public void setPopedomValue(String aPopedomValue) {
        if (aPopedomValue != null && !aPopedomValue.equals("")) {
            Double tDouble = new Double(aPopedomValue);
            double d = tDouble.doubleValue();
            PopedomValue = Arith.round(d, 2);
        }
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }

    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }

    public String getStandbyFlag2() {
        return StandbyFlag2;
    }

    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getPopedomDirect2() {
        return PopedomDirect2;
    }

    public void setPopedomDirect2(String aPopedomDirect2) {
        PopedomDirect2 = aPopedomDirect2;
    }

    public double getPopedomValue2() {
        return PopedomValue2;
    }

    public void setPopedomValue2(double aPopedomValue2) {
        PopedomValue2 = Arith.round(aPopedomValue2, 2);
    }

    public void setPopedomValue2(String aPopedomValue2) {
        if (aPopedomValue2 != null && !aPopedomValue2.equals("")) {
            Double tDouble = new Double(aPopedomValue2);
            double d = tDouble.doubleValue();
            PopedomValue2 = Arith.round(d, 2);
        }
    }

    public String getPopedomDirectType() {
        return PopedomDirectType;
    }

    public void setPopedomDirectType(String aPopedomDirectType) {
        PopedomDirectType = aPopedomDirectType;
    }

    /**
     * 使用另外一个 LDUWGradeSchema 对象给 Schema 赋值
     * @param: aLDUWGradeSchema LDUWGradeSchema
     **/
    public void setSchema(LDUWGradeSchema aLDUWGradeSchema) {
        this.UWGrade = aLDUWGradeSchema.getUWGrade();
        this.UWGradeName = aLDUWGradeSchema.getUWGradeName();
        this.ContFlag = aLDUWGradeSchema.getContFlag();
        this.RiskType = aLDUWGradeSchema.getRiskType();
        this.AgreementFlag = aLDUWGradeSchema.getAgreementFlag();
        this.AddPrem = aLDUWGradeSchema.getAddPrem();
        this.SpecItemNum = aLDUWGradeSchema.getSpecItemNum();
        this.PopedomType = aLDUWGradeSchema.getPopedomType();
        this.PopedomDirect = aLDUWGradeSchema.getPopedomDirect();
        this.PopedomValue = aLDUWGradeSchema.getPopedomValue();
        this.StandbyFlag1 = aLDUWGradeSchema.getStandbyFlag1();
        this.StandbyFlag2 = aLDUWGradeSchema.getStandbyFlag2();
        this.RiskCode = aLDUWGradeSchema.getRiskCode();
        this.ManageCom = aLDUWGradeSchema.getManageCom();
        this.PopedomDirect2 = aLDUWGradeSchema.getPopedomDirect2();
        this.PopedomValue2 = aLDUWGradeSchema.getPopedomValue2();
        this.PopedomDirectType = aLDUWGradeSchema.getPopedomDirectType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("UWGrade") == null) {
                this.UWGrade = null;
            } else {
                this.UWGrade = rs.getString("UWGrade").trim();
            }

            if (rs.getString("UWGradeName") == null) {
                this.UWGradeName = null;
            } else {
                this.UWGradeName = rs.getString("UWGradeName").trim();
            }

            if (rs.getString("ContFlag") == null) {
                this.ContFlag = null;
            } else {
                this.ContFlag = rs.getString("ContFlag").trim();
            }

            if (rs.getString("RiskType") == null) {
                this.RiskType = null;
            } else {
                this.RiskType = rs.getString("RiskType").trim();
            }

            if (rs.getString("AgreementFlag") == null) {
                this.AgreementFlag = null;
            } else {
                this.AgreementFlag = rs.getString("AgreementFlag").trim();
            }

            this.AddPrem = rs.getDouble("AddPrem");
            this.SpecItemNum = rs.getInt("SpecItemNum");
            if (rs.getString("PopedomType") == null) {
                this.PopedomType = null;
            } else {
                this.PopedomType = rs.getString("PopedomType").trim();
            }

            if (rs.getString("PopedomDirect") == null) {
                this.PopedomDirect = null;
            } else {
                this.PopedomDirect = rs.getString("PopedomDirect").trim();
            }

            this.PopedomValue = rs.getDouble("PopedomValue");
            if (rs.getString("StandbyFlag1") == null) {
                this.StandbyFlag1 = null;
            } else {
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();
            }

            if (rs.getString("StandbyFlag2") == null) {
                this.StandbyFlag2 = null;
            } else {
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("PopedomDirect2") == null) {
                this.PopedomDirect2 = null;
            } else {
                this.PopedomDirect2 = rs.getString("PopedomDirect2").trim();
            }

            this.PopedomValue2 = rs.getDouble("PopedomValue2");
            if (rs.getString("PopedomDirectType") == null) {
                this.PopedomDirectType = null;
            } else {
                this.PopedomDirectType = rs.getString("PopedomDirectType").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDUWGrade表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUWGradeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDUWGradeSchema getSchema() {
        LDUWGradeSchema aLDUWGradeSchema = new LDUWGradeSchema();
        aLDUWGradeSchema.setSchema(this);
        return aLDUWGradeSchema;
    }

    public LDUWGradeDB getDB() {
        LDUWGradeDB aDBOper = new LDUWGradeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDUWGrade描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(UWGrade));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWGradeName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgreementFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AddPrem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SpecItemNum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PopedomType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PopedomDirect));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PopedomValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PopedomDirect2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PopedomValue2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PopedomDirectType));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDUWGrade>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            UWGradeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            ContFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            AgreementFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            AddPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            SpecItemNum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            PopedomType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            PopedomDirect = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                           SysConst.PACKAGESPILTER);
            PopedomValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            PopedomDirect2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            15, SysConst.PACKAGESPILTER);
            PopedomValue2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).doubleValue();
            PopedomDirectType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               17, SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUWGradeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("UWGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWGrade));
        }
        if (FCode.equals("UWGradeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWGradeName));
        }
        if (FCode.equals("ContFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContFlag));
        }
        if (FCode.equals("RiskType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
        }
        if (FCode.equals("AgreementFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgreementFlag));
        }
        if (FCode.equals("AddPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddPrem));
        }
        if (FCode.equals("SpecItemNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecItemNum));
        }
        if (FCode.equals("PopedomType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PopedomType));
        }
        if (FCode.equals("PopedomDirect")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PopedomDirect));
        }
        if (FCode.equals("PopedomValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PopedomValue));
        }
        if (FCode.equals("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equals("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("PopedomDirect2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PopedomDirect2));
        }
        if (FCode.equals("PopedomValue2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PopedomValue2));
        }
        if (FCode.equals("PopedomDirectType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PopedomDirectType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(UWGrade);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(UWGradeName);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ContFlag);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(RiskType);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(AgreementFlag);
            break;
        case 5:
            strFieldValue = String.valueOf(AddPrem);
            break;
        case 6:
            strFieldValue = String.valueOf(SpecItemNum);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(PopedomType);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(PopedomDirect);
            break;
        case 9:
            strFieldValue = String.valueOf(PopedomValue);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(PopedomDirect2);
            break;
        case 15:
            strFieldValue = String.valueOf(PopedomValue2);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(PopedomDirectType);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("UWGrade")) {
            if (FValue != null && !FValue.equals("")) {
                UWGrade = FValue.trim();
            } else {
                UWGrade = null;
            }
        }
        if (FCode.equalsIgnoreCase("UWGradeName")) {
            if (FValue != null && !FValue.equals("")) {
                UWGradeName = FValue.trim();
            } else {
                UWGradeName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ContFlag = FValue.trim();
            } else {
                ContFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            if (FValue != null && !FValue.equals("")) {
                RiskType = FValue.trim();
            } else {
                RiskType = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgreementFlag")) {
            if (FValue != null && !FValue.equals("")) {
                AgreementFlag = FValue.trim();
            } else {
                AgreementFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("AddPrem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AddPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SpecItemNum")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SpecItemNum = i;
            }
        }
        if (FCode.equalsIgnoreCase("PopedomType")) {
            if (FValue != null && !FValue.equals("")) {
                PopedomType = FValue.trim();
            } else {
                PopedomType = null;
            }
        }
        if (FCode.equalsIgnoreCase("PopedomDirect")) {
            if (FValue != null && !FValue.equals("")) {
                PopedomDirect = FValue.trim();
            } else {
                PopedomDirect = null;
            }
        }
        if (FCode.equalsIgnoreCase("PopedomValue")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PopedomValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if (FValue != null && !FValue.equals("")) {
                StandbyFlag1 = FValue.trim();
            } else {
                StandbyFlag1 = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if (FValue != null && !FValue.equals("")) {
                StandbyFlag2 = FValue.trim();
            } else {
                StandbyFlag2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("PopedomDirect2")) {
            if (FValue != null && !FValue.equals("")) {
                PopedomDirect2 = FValue.trim();
            } else {
                PopedomDirect2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("PopedomValue2")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PopedomValue2 = d;
            }
        }
        if (FCode.equalsIgnoreCase("PopedomDirectType")) {
            if (FValue != null && !FValue.equals("")) {
                PopedomDirectType = FValue.trim();
            } else {
                PopedomDirectType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDUWGradeSchema other = (LDUWGradeSchema) otherObject;
        return
                UWGrade.equals(other.getUWGrade())
                && UWGradeName.equals(other.getUWGradeName())
                && ContFlag.equals(other.getContFlag())
                && RiskType.equals(other.getRiskType())
                && AgreementFlag.equals(other.getAgreementFlag())
                && AddPrem == other.getAddPrem()
                && SpecItemNum == other.getSpecItemNum()
                && PopedomType.equals(other.getPopedomType())
                && PopedomDirect.equals(other.getPopedomDirect())
                && PopedomValue == other.getPopedomValue()
                && StandbyFlag1.equals(other.getStandbyFlag1())
                && StandbyFlag2.equals(other.getStandbyFlag2())
                && RiskCode.equals(other.getRiskCode())
                && ManageCom.equals(other.getManageCom())
                && PopedomDirect2.equals(other.getPopedomDirect2())
                && PopedomValue2 == other.getPopedomValue2()
                && PopedomDirectType.equals(other.getPopedomDirectType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("UWGrade")) {
            return 0;
        }
        if (strFieldName.equals("UWGradeName")) {
            return 1;
        }
        if (strFieldName.equals("ContFlag")) {
            return 2;
        }
        if (strFieldName.equals("RiskType")) {
            return 3;
        }
        if (strFieldName.equals("AgreementFlag")) {
            return 4;
        }
        if (strFieldName.equals("AddPrem")) {
            return 5;
        }
        if (strFieldName.equals("SpecItemNum")) {
            return 6;
        }
        if (strFieldName.equals("PopedomType")) {
            return 7;
        }
        if (strFieldName.equals("PopedomDirect")) {
            return 8;
        }
        if (strFieldName.equals("PopedomValue")) {
            return 9;
        }
        if (strFieldName.equals("StandbyFlag1")) {
            return 10;
        }
        if (strFieldName.equals("StandbyFlag2")) {
            return 11;
        }
        if (strFieldName.equals("RiskCode")) {
            return 12;
        }
        if (strFieldName.equals("ManageCom")) {
            return 13;
        }
        if (strFieldName.equals("PopedomDirect2")) {
            return 14;
        }
        if (strFieldName.equals("PopedomValue2")) {
            return 15;
        }
        if (strFieldName.equals("PopedomDirectType")) {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "UWGrade";
            break;
        case 1:
            strFieldName = "UWGradeName";
            break;
        case 2:
            strFieldName = "ContFlag";
            break;
        case 3:
            strFieldName = "RiskType";
            break;
        case 4:
            strFieldName = "AgreementFlag";
            break;
        case 5:
            strFieldName = "AddPrem";
            break;
        case 6:
            strFieldName = "SpecItemNum";
            break;
        case 7:
            strFieldName = "PopedomType";
            break;
        case 8:
            strFieldName = "PopedomDirect";
            break;
        case 9:
            strFieldName = "PopedomValue";
            break;
        case 10:
            strFieldName = "StandbyFlag1";
            break;
        case 11:
            strFieldName = "StandbyFlag2";
            break;
        case 12:
            strFieldName = "RiskCode";
            break;
        case 13:
            strFieldName = "ManageCom";
            break;
        case 14:
            strFieldName = "PopedomDirect2";
            break;
        case 15:
            strFieldName = "PopedomValue2";
            break;
        case 16:
            strFieldName = "PopedomDirectType";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("UWGrade")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWGradeName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgreementFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AddPrem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SpecItemNum")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PopedomType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PopedomDirect")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PopedomValue")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StandbyFlag1")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PopedomDirect2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PopedomValue2")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PopedomDirectType")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 6:
            nFieldType = Schema.TYPE_INT;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
