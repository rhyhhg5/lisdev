/*
 * <p>ClassName: LJSGetClaimSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 更新表
 * @CreateDate：2004-12-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LJSGetClaimDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LJSGetClaimSchema implements Schema
{
    // @Field
    /** 给付通知书号码 */
    private String GetNoticeNo;
    /** 补/退费财务类型 */
    private String FeeFinaType;
    /** 补/退费业务类型 */
    private String FeeOperationType;
    /** 其它号码 */
    private String OtherNo;
    /** 其它号码类型 */
    private String OtherNoType;
    /** 销售渠道 */
    private String SaleChnl;
    /** 给付责任编码 */
    private String GetDutyCode;
    /** 给付责任类型 */
    private String GetDutyKind;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 保单号码 */
    private String PolNo;
    /** 险类编码 */
    private String KindCode;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 给付日期 */
    private Date GetDate;
    /** 赔付金额 */
    private double Pay;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 28; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LJSGetClaimSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "GetNoticeNo";
        pk[1] = "FeeFinaType";
        pk[2] = "FeeOperationType";
        pk[3] = "PolNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGetNoticeNo()
    {
        if (GetNoticeNo != null && !GetNoticeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetNoticeNo = StrTool.unicodeToGBK(GetNoticeNo);
        }
        return GetNoticeNo;
    }

    public void setGetNoticeNo(String aGetNoticeNo)
    {
        GetNoticeNo = aGetNoticeNo;
    }

    public String getFeeFinaType()
    {
        if (FeeFinaType != null && !FeeFinaType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FeeFinaType = StrTool.unicodeToGBK(FeeFinaType);
        }
        return FeeFinaType;
    }

    public void setFeeFinaType(String aFeeFinaType)
    {
        FeeFinaType = aFeeFinaType;
    }

    public String getFeeOperationType()
    {
        if (FeeOperationType != null && !FeeOperationType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FeeOperationType = StrTool.unicodeToGBK(FeeOperationType);
        }
        return FeeOperationType;
    }

    public void setFeeOperationType(String aFeeOperationType)
    {
        FeeOperationType = aFeeOperationType;
    }

    public String getOtherNo()
    {
        if (OtherNo != null && !OtherNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            OtherNo = StrTool.unicodeToGBK(OtherNo);
        }
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        if (OtherNoType != null && !OtherNoType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OtherNoType = StrTool.unicodeToGBK(OtherNoType);
        }
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getGetDutyCode()
    {
        if (GetDutyCode != null && !GetDutyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyCode = StrTool.unicodeToGBK(GetDutyCode);
        }
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public String getGetDutyKind()
    {
        if (GetDutyKind != null && !GetDutyKind.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyKind = StrTool.unicodeToGBK(GetDutyKind);
        }
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind)
    {
        GetDutyKind = aGetDutyKind;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getKindCode()
    {
        if (KindCode != null && !KindCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            KindCode = StrTool.unicodeToGBK(KindCode);
        }
        return KindCode;
    }

    public void setKindCode(String aKindCode)
    {
        KindCode = aKindCode;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getGetDate()
    {
        if (GetDate != null)
        {
            return fDate.getString(GetDate);
        }
        else
        {
            return null;
        }
    }

    public void setGetDate(Date aGetDate)
    {
        GetDate = aGetDate;
    }

    public void setGetDate(String aGetDate)
    {
        if (aGetDate != null && !aGetDate.equals(""))
        {
            GetDate = fDate.getDate(aGetDate);
        }
        else
        {
            GetDate = null;
        }
    }

    public double getPay()
    {
        return Pay;
    }

    public void setPay(double aPay)
    {
        Pay = aPay;
    }

    public void setPay(String aPay)
    {
        if (aPay != null && !aPay.equals(""))
        {
            Double tDouble = new Double(aPay);
            double d = tDouble.doubleValue();
            Pay = d;
        }
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentCom()
    {
        if (AgentCom != null && !AgentCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getAgentType()
    {
        if (AgentType != null && !AgentType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentType = StrTool.unicodeToGBK(AgentType);
        }
        return AgentType;
    }

    public void setAgentType(String aAgentType)
    {
        AgentType = aAgentType;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LJSGetClaimSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LJSGetClaimSchema aLJSGetClaimSchema)
    {
        this.GetNoticeNo = aLJSGetClaimSchema.getGetNoticeNo();
        this.FeeFinaType = aLJSGetClaimSchema.getFeeFinaType();
        this.FeeOperationType = aLJSGetClaimSchema.getFeeOperationType();
        this.OtherNo = aLJSGetClaimSchema.getOtherNo();
        this.OtherNoType = aLJSGetClaimSchema.getOtherNoType();
        this.SaleChnl = aLJSGetClaimSchema.getSaleChnl();
        this.GetDutyCode = aLJSGetClaimSchema.getGetDutyCode();
        this.GetDutyKind = aLJSGetClaimSchema.getGetDutyKind();
        this.GrpContNo = aLJSGetClaimSchema.getGrpContNo();
        this.ContNo = aLJSGetClaimSchema.getContNo();
        this.GrpPolNo = aLJSGetClaimSchema.getGrpPolNo();
        this.PolNo = aLJSGetClaimSchema.getPolNo();
        this.KindCode = aLJSGetClaimSchema.getKindCode();
        this.RiskCode = aLJSGetClaimSchema.getRiskCode();
        this.RiskVersion = aLJSGetClaimSchema.getRiskVersion();
        this.AgentCode = aLJSGetClaimSchema.getAgentCode();
        this.AgentGroup = aLJSGetClaimSchema.getAgentGroup();
        this.GetDate = fDate.getDate(aLJSGetClaimSchema.getGetDate());
        this.Pay = aLJSGetClaimSchema.getPay();
        this.ManageCom = aLJSGetClaimSchema.getManageCom();
        this.AgentCom = aLJSGetClaimSchema.getAgentCom();
        this.AgentType = aLJSGetClaimSchema.getAgentType();
        this.SerialNo = aLJSGetClaimSchema.getSerialNo();
        this.Operator = aLJSGetClaimSchema.getOperator();
        this.MakeDate = fDate.getDate(aLJSGetClaimSchema.getMakeDate());
        this.MakeTime = aLJSGetClaimSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLJSGetClaimSchema.getModifyDate());
        this.ModifyTime = aLJSGetClaimSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GetNoticeNo") == null)
            {
                this.GetNoticeNo = null;
            }
            else
            {
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();
            }

            if (rs.getString("FeeFinaType") == null)
            {
                this.FeeFinaType = null;
            }
            else
            {
                this.FeeFinaType = rs.getString("FeeFinaType").trim();
            }

            if (rs.getString("FeeOperationType") == null)
            {
                this.FeeOperationType = null;
            }
            else
            {
                this.FeeOperationType = rs.getString("FeeOperationType").trim();
            }

            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            if (rs.getString("GetDutyKind") == null)
            {
                this.GetDutyKind = null;
            }
            else
            {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("KindCode") == null)
            {
                this.KindCode = null;
            }
            else
            {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            this.GetDate = rs.getDate("GetDate");
            this.Pay = rs.getDouble("Pay");
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("AgentType") == null)
            {
                this.AgentType = null;
            }
            else
            {
                this.AgentType = rs.getString("AgentType").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSGetClaimSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LJSGetClaimSchema getSchema()
    {
        LJSGetClaimSchema aLJSGetClaimSchema = new LJSGetClaimSchema();
        aLJSGetClaimSchema.setSchema(this);
        return aLJSGetClaimSchema;
    }

    public LJSGetClaimDB getDB()
    {
        LJSGetClaimDB aDBOper = new LJSGetClaimDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJSGetClaim描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GetNoticeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FeeFinaType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FeeOperationType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OtherNoType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(KindCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(GetDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Pay) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJSGetClaim>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            FeeFinaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            FeeOperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              3, SysConst.PACKAGESPILTER);
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                   SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            GetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            Pay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    19, SysConst.PACKAGESPILTER))).doubleValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                       SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                       SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 27, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSGetClaimSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GetNoticeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetNoticeNo));
        }
        if (FCode.equals("FeeFinaType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FeeFinaType));
        }
        if (FCode.equals("FeeOperationType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FeeOperationType));
        }
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OtherNoType));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyCode));
        }
        if (FCode.equals("GetDutyKind"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyKind));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("KindCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(KindCode));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("GetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getGetDate()));
        }
        if (FCode.equals("Pay"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Pay));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCom));
        }
        if (FCode.equals("AgentType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentType));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(FeeFinaType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(FeeOperationType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getGetDate()));
                break;
            case 18:
                strFieldValue = String.valueOf(Pay);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GetNoticeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
            {
                GetNoticeNo = null;
            }
        }
        if (FCode.equals("FeeFinaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FeeFinaType = FValue.trim();
            }
            else
            {
                FeeFinaType = null;
            }
        }
        if (FCode.equals("FeeOperationType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FeeOperationType = FValue.trim();
            }
            else
            {
                FeeOperationType = null;
            }
        }
        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("GetDutyKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
            {
                GetDutyKind = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("KindCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
            {
                KindCode = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("GetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDate = fDate.getDate(FValue);
            }
            else
            {
                GetDate = null;
            }
        }
        if (FCode.equals("Pay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Pay = d;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("AgentType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
            {
                AgentType = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LJSGetClaimSchema other = (LJSGetClaimSchema) otherObject;
        return
                GetNoticeNo.equals(other.getGetNoticeNo())
                && FeeFinaType.equals(other.getFeeFinaType())
                && FeeOperationType.equals(other.getFeeOperationType())
                && OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && SaleChnl.equals(other.getSaleChnl())
                && GetDutyCode.equals(other.getGetDutyCode())
                && GetDutyKind.equals(other.getGetDutyKind())
                && GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && PolNo.equals(other.getPolNo())
                && KindCode.equals(other.getKindCode())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && fDate.getString(GetDate).equals(other.getGetDate())
                && Pay == other.getPay()
                && ManageCom.equals(other.getManageCom())
                && AgentCom.equals(other.getAgentCom())
                && AgentType.equals(other.getAgentType())
                && SerialNo.equals(other.getSerialNo())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GetNoticeNo"))
        {
            return 0;
        }
        if (strFieldName.equals("FeeFinaType"))
        {
            return 1;
        }
        if (strFieldName.equals("FeeOperationType"))
        {
            return 2;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return 3;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 4;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 5;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return 6;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return 7;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 8;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 9;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 10;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 11;
        }
        if (strFieldName.equals("KindCode"))
        {
            return 12;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 13;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 14;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 15;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 16;
        }
        if (strFieldName.equals("GetDate"))
        {
            return 17;
        }
        if (strFieldName.equals("Pay"))
        {
            return 18;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 19;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 20;
        }
        if (strFieldName.equals("AgentType"))
        {
            return 21;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 22;
        }
        if (strFieldName.equals("Operator"))
        {
            return 23;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 24;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 25;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 26;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 27;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GetNoticeNo";
                break;
            case 1:
                strFieldName = "FeeFinaType";
                break;
            case 2:
                strFieldName = "FeeOperationType";
                break;
            case 3:
                strFieldName = "OtherNo";
                break;
            case 4:
                strFieldName = "OtherNoType";
                break;
            case 5:
                strFieldName = "SaleChnl";
                break;
            case 6:
                strFieldName = "GetDutyCode";
                break;
            case 7:
                strFieldName = "GetDutyKind";
                break;
            case 8:
                strFieldName = "GrpContNo";
                break;
            case 9:
                strFieldName = "ContNo";
                break;
            case 10:
                strFieldName = "GrpPolNo";
                break;
            case 11:
                strFieldName = "PolNo";
                break;
            case 12:
                strFieldName = "KindCode";
                break;
            case 13:
                strFieldName = "RiskCode";
                break;
            case 14:
                strFieldName = "RiskVersion";
                break;
            case 15:
                strFieldName = "AgentCode";
                break;
            case 16:
                strFieldName = "AgentGroup";
                break;
            case 17:
                strFieldName = "GetDate";
                break;
            case 18:
                strFieldName = "Pay";
                break;
            case 19:
                strFieldName = "ManageCom";
                break;
            case 20:
                strFieldName = "AgentCom";
                break;
            case 21:
                strFieldName = "AgentType";
                break;
            case 22:
                strFieldName = "SerialNo";
                break;
            case 23:
                strFieldName = "Operator";
                break;
            case 24:
                strFieldName = "MakeDate";
                break;
            case 25:
                strFieldName = "MakeTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GetNoticeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeFinaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeOperationType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Pay"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
