/*
 * <p>ClassName: LMCertifySlaveSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMCertifySlaveDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMCertifySlaveSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 附标号 */
    private String SubCode;
    /** 总基本保额 */
    private double Amnt;
    /** 实际保费 */
    private double Prem;
    /** 特别约定 */
    private String noti;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMCertifySlaveSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "RiskCode";
        pk[1] = "RiskVersion";
        pk[2] = "SubCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getSubCode()
    {
        if (SubCode != null && !SubCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            SubCode = StrTool.unicodeToGBK(SubCode);
        }
        return SubCode;
    }

    public void setSubCode(String aSubCode)
    {
        SubCode = aSubCode;
    }

    public double getAmnt()
    {
        return Amnt;
    }

    public void setAmnt(double aAmnt)
    {
        Amnt = aAmnt;
    }

    public void setAmnt(String aAmnt)
    {
        if (aAmnt != null && !aAmnt.equals(""))
        {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getPrem()
    {
        return Prem;
    }

    public void setPrem(double aPrem)
    {
        Prem = aPrem;
    }

    public void setPrem(String aPrem)
    {
        if (aPrem != null && !aPrem.equals(""))
        {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public String getnoti()
    {
        if (noti != null && !noti.equals("") && SysConst.CHANGECHARSET == true)
        {
            noti = StrTool.unicodeToGBK(noti);
        }
        return noti;
    }

    public void setnoti(String anoti)
    {
        noti = anoti;
    }

    /**
     * 使用另外一个 LMCertifySlaveSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMCertifySlaveSchema aLMCertifySlaveSchema)
    {
        this.RiskCode = aLMCertifySlaveSchema.getRiskCode();
        this.RiskVersion = aLMCertifySlaveSchema.getRiskVersion();
        this.SubCode = aLMCertifySlaveSchema.getSubCode();
        this.Amnt = aLMCertifySlaveSchema.getAmnt();
        this.Prem = aLMCertifySlaveSchema.getPrem();
        this.noti = aLMCertifySlaveSchema.getnoti();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("SubCode") == null)
            {
                this.SubCode = null;
            }
            else
            {
                this.SubCode = rs.getString("SubCode").trim();
            }

            this.Amnt = rs.getDouble("Amnt");
            this.Prem = rs.getDouble("Prem");
            if (rs.getString("noti") == null)
            {
                this.noti = null;
            }
            else
            {
                this.noti = rs.getString("noti").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCertifySlaveSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMCertifySlaveSchema getSchema()
    {
        LMCertifySlaveSchema aLMCertifySlaveSchema = new LMCertifySlaveSchema();
        aLMCertifySlaveSchema.setSchema(this);
        return aLMCertifySlaveSchema;
    }

    public LMCertifySlaveDB getDB()
    {
        LMCertifySlaveDB aDBOper = new LMCertifySlaveDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCertifySlave描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SubCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Amnt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Prem) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(noti));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCertifySlave>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            SubCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    5, SysConst.PACKAGESPILTER))).doubleValue();
            noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                  SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCertifySlaveSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("SubCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SubCode));
        }
        if (FCode.equals("Amnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Amnt));
        }
        if (FCode.equals("Prem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prem));
        }
        if (FCode.equals("noti"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(noti));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SubCode);
                break;
            case 3:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 4:
                strFieldValue = String.valueOf(Prem);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(noti);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("SubCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubCode = FValue.trim();
            }
            else
            {
                SubCode = null;
            }
        }
        if (FCode.equals("Amnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equals("Prem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equals("noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                noti = FValue.trim();
            }
            else
            {
                noti = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMCertifySlaveSchema other = (LMCertifySlaveSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && SubCode.equals(other.getSubCode())
                && Amnt == other.getAmnt()
                && Prem == other.getPrem()
                && noti.equals(other.getnoti());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 1;
        }
        if (strFieldName.equals("SubCode"))
        {
            return 2;
        }
        if (strFieldName.equals("Amnt"))
        {
            return 3;
        }
        if (strFieldName.equals("Prem"))
        {
            return 4;
        }
        if (strFieldName.equals("noti"))
        {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVersion";
                break;
            case 2:
                strFieldName = "SubCode";
                break;
            case 3:
                strFieldName = "Amnt";
                break;
            case 4:
                strFieldName = "Prem";
                break;
            case 5:
                strFieldName = "noti";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Amnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("noti"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
