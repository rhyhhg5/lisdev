/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBDiseaseImpartDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LOBDiseaseImpartSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2005-06-27
 */
public class LOBDiseaseImpartSchema implements Schema, Cloneable
{
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 印刷号 */
    private String PrtNo;
    /** 发生时间 */
    private String OcurTime;
    /** 疾病名称 */
    private String DiseaseName;
    /** 患病人数 */
    private int DiseasePepoles;
    /** 当年发生医疗费用总额 */
    private double CureMoney;
    /** 备注 */
    private String Remark;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBDiseaseImpartSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LOBDiseaseImpartSchema cloned = (LOBDiseaseImpartSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getGrpContNo()
    {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getPrtNo()
    {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo)
    {
        PrtNo = aPrtNo;
    }

    public String getOcurTime()
    {
        return OcurTime;
    }

    public void setOcurTime(String aOcurTime)
    {
        OcurTime = aOcurTime;
    }

    public String getDiseaseName()
    {
        return DiseaseName;
    }

    public void setDiseaseName(String aDiseaseName)
    {
        DiseaseName = aDiseaseName;
    }

    public int getDiseasePepoles()
    {
        return DiseasePepoles;
    }

    public void setDiseasePepoles(int aDiseasePepoles)
    {
        DiseasePepoles = aDiseasePepoles;
    }

    public void setDiseasePepoles(String aDiseasePepoles)
    {
        if (aDiseasePepoles != null && !aDiseasePepoles.equals(""))
        {
            Integer tInteger = new Integer(aDiseasePepoles);
            int i = tInteger.intValue();
            DiseasePepoles = i;
        }
    }

    public double getCureMoney()
    {
        return CureMoney;
    }

    public void setCureMoney(double aCureMoney)
    {
        CureMoney = Arith.round(aCureMoney, 2);
    }

    public void setCureMoney(String aCureMoney)
    {
        if (aCureMoney != null && !aCureMoney.equals(""))
        {
            Double tDouble = new Double(aCureMoney);
            double d = tDouble.doubleValue();
            CureMoney = Arith.round(d, 2);
        }
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOBDiseaseImpartSchema 对象给 Schema 赋值
     * @param: aLOBDiseaseImpartSchema LOBDiseaseImpartSchema
     **/
    public void setSchema(LOBDiseaseImpartSchema aLOBDiseaseImpartSchema)
    {
        this.SerialNo = aLOBDiseaseImpartSchema.getSerialNo();
        this.GrpContNo = aLOBDiseaseImpartSchema.getGrpContNo();
        this.PrtNo = aLOBDiseaseImpartSchema.getPrtNo();
        this.OcurTime = aLOBDiseaseImpartSchema.getOcurTime();
        this.DiseaseName = aLOBDiseaseImpartSchema.getDiseaseName();
        this.DiseasePepoles = aLOBDiseaseImpartSchema.getDiseasePepoles();
        this.CureMoney = aLOBDiseaseImpartSchema.getCureMoney();
        this.Remark = aLOBDiseaseImpartSchema.getRemark();
        this.MakeDate = fDate.getDate(aLOBDiseaseImpartSchema.getMakeDate());
        this.MakeTime = aLOBDiseaseImpartSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBDiseaseImpartSchema.getModifyDate());
        this.ModifyTime = aLOBDiseaseImpartSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("PrtNo") == null)
            {
                this.PrtNo = null;
            }
            else
            {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("OcurTime") == null)
            {
                this.OcurTime = null;
            }
            else
            {
                this.OcurTime = rs.getString("OcurTime").trim();
            }

            if (rs.getString("DiseaseName") == null)
            {
                this.DiseaseName = null;
            }
            else
            {
                this.DiseaseName = rs.getString("DiseaseName").trim();
            }

            this.DiseasePepoles = rs.getInt("DiseasePepoles");
            this.CureMoney = rs.getDouble("CureMoney");
            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LOBDiseaseImpart表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBDiseaseImpartSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LOBDiseaseImpartSchema getSchema()
    {
        LOBDiseaseImpartSchema aLOBDiseaseImpartSchema = new
                LOBDiseaseImpartSchema();
        aLOBDiseaseImpartSchema.setSchema(this);
        return aLOBDiseaseImpartSchema;
    }

    public LOBDiseaseImpartDB getDB()
    {
        LOBDiseaseImpartDB aDBOper = new LOBDiseaseImpartDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBDiseaseImpart描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OcurTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DiseaseName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DiseasePepoles));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CureMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBDiseaseImpart>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            OcurTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            DiseaseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            DiseasePepoles = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            CureMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBDiseaseImpartSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("PrtNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("OcurTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OcurTime));
        }
        if (FCode.equals("DiseaseName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaseName));
        }
        if (FCode.equals("DiseasePepoles"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiseasePepoles));
        }
        if (FCode.equals("CureMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CureMoney));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OcurTime);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DiseaseName);
                break;
            case 5:
                strFieldValue = String.valueOf(DiseasePepoles);
                break;
            case 6:
                strFieldValue = String.valueOf(CureMoney);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
            {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("OcurTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OcurTime = FValue.trim();
            }
            else
            {
                OcurTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("DiseaseName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DiseaseName = FValue.trim();
            }
            else
            {
                DiseaseName = null;
            }
        }
        if (FCode.equalsIgnoreCase("DiseasePepoles"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DiseasePepoles = i;
            }
        }
        if (FCode.equalsIgnoreCase("CureMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CureMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBDiseaseImpartSchema other = (LOBDiseaseImpartSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && GrpContNo.equals(other.getGrpContNo())
                && PrtNo.equals(other.getPrtNo())
                && OcurTime.equals(other.getOcurTime())
                && DiseaseName.equals(other.getDiseaseName())
                && DiseasePepoles == other.getDiseasePepoles()
                && CureMoney == other.getCureMoney()
                && Remark.equals(other.getRemark())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return 2;
        }
        if (strFieldName.equals("OcurTime"))
        {
            return 3;
        }
        if (strFieldName.equals("DiseaseName"))
        {
            return 4;
        }
        if (strFieldName.equals("DiseasePepoles"))
        {
            return 5;
        }
        if (strFieldName.equals("CureMoney"))
        {
            return 6;
        }
        if (strFieldName.equals("Remark"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "GrpContNo";
                break;
            case 2:
                strFieldName = "PrtNo";
                break;
            case 3:
                strFieldName = "OcurTime";
                break;
            case 4:
                strFieldName = "DiseaseName";
                break;
            case 5:
                strFieldName = "DiseasePepoles";
                break;
            case 6:
                strFieldName = "CureMoney";
                break;
            case 7:
                strFieldName = "Remark";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OcurTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DiseaseName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DiseasePepoles"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CureMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
