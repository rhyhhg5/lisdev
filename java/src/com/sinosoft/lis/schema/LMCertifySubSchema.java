/*
 * <p>ClassName: LMCertifySubSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMCertifySubDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMCertifySubSchema implements Schema
{
    // @Field
    /** 单证编码 */
    private String CertifyCode;
    /** 单证类型 */
    private String CertifyClass;
    /** 数据库服务器 */
    private String DBServer;
    /** 数据库名称 */
    private String DBName;
    /** 表名 */
    private String TableName;
    /** 字段名 */
    private String FieldName;
    /** 执行前sql */
    private String BeforeSQL;
    /** 执行后sql */
    private String AfterSQL;
    /** 错误信息 */
    private String ErrorMessage;
    /** 自动回收 */
    private String AutoTakeBack;
    /** 执行调用接口类 */
    private String InterfaceClass;
    /** 打印后自动发放 */
    private String AutoSend;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMCertifySubSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "CertifyCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCertifyCode()
    {
        if (CertifyCode != null && !CertifyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CertifyCode = StrTool.unicodeToGBK(CertifyCode);
        }
        return CertifyCode;
    }

    public void setCertifyCode(String aCertifyCode)
    {
        CertifyCode = aCertifyCode;
    }

    public String getCertifyClass()
    {
        if (CertifyClass != null && !CertifyClass.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CertifyClass = StrTool.unicodeToGBK(CertifyClass);
        }
        return CertifyClass;
    }

    public void setCertifyClass(String aCertifyClass)
    {
        CertifyClass = aCertifyClass;
    }

    public String getDBServer()
    {
        if (DBServer != null && !DBServer.equals("") && SysConst.CHANGECHARSET == true)
        {
            DBServer = StrTool.unicodeToGBK(DBServer);
        }
        return DBServer;
    }

    public void setDBServer(String aDBServer)
    {
        DBServer = aDBServer;
    }

    public String getDBName()
    {
        if (DBName != null && !DBName.equals("") && SysConst.CHANGECHARSET == true)
        {
            DBName = StrTool.unicodeToGBK(DBName);
        }
        return DBName;
    }

    public void setDBName(String aDBName)
    {
        DBName = aDBName;
    }

    public String getTableName()
    {
        if (TableName != null && !TableName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TableName = StrTool.unicodeToGBK(TableName);
        }
        return TableName;
    }

    public void setTableName(String aTableName)
    {
        TableName = aTableName;
    }

    public String getFieldName()
    {
        if (FieldName != null && !FieldName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FieldName = StrTool.unicodeToGBK(FieldName);
        }
        return FieldName;
    }

    public void setFieldName(String aFieldName)
    {
        FieldName = aFieldName;
    }

    public String getBeforeSQL()
    {
        if (BeforeSQL != null && !BeforeSQL.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BeforeSQL = StrTool.unicodeToGBK(BeforeSQL);
        }
        return BeforeSQL;
    }

    public void setBeforeSQL(String aBeforeSQL)
    {
        BeforeSQL = aBeforeSQL;
    }

    public String getAfterSQL()
    {
        if (AfterSQL != null && !AfterSQL.equals("") && SysConst.CHANGECHARSET == true)
        {
            AfterSQL = StrTool.unicodeToGBK(AfterSQL);
        }
        return AfterSQL;
    }

    public void setAfterSQL(String aAfterSQL)
    {
        AfterSQL = aAfterSQL;
    }

    public String getErrorMessage()
    {
        if (ErrorMessage != null && !ErrorMessage.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ErrorMessage = StrTool.unicodeToGBK(ErrorMessage);
        }
        return ErrorMessage;
    }

    public void setErrorMessage(String aErrorMessage)
    {
        ErrorMessage = aErrorMessage;
    }

    public String getAutoTakeBack()
    {
        if (AutoTakeBack != null && !AutoTakeBack.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AutoTakeBack = StrTool.unicodeToGBK(AutoTakeBack);
        }
        return AutoTakeBack;
    }

    public void setAutoTakeBack(String aAutoTakeBack)
    {
        AutoTakeBack = aAutoTakeBack;
    }

    public String getInterfaceClass()
    {
        if (InterfaceClass != null && !InterfaceClass.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InterfaceClass = StrTool.unicodeToGBK(InterfaceClass);
        }
        return InterfaceClass;
    }

    public void setInterfaceClass(String aInterfaceClass)
    {
        InterfaceClass = aInterfaceClass;
    }

    public String getAutoSend()
    {
        if (AutoSend != null && !AutoSend.equals("") && SysConst.CHANGECHARSET == true)
        {
            AutoSend = StrTool.unicodeToGBK(AutoSend);
        }
        return AutoSend;
    }

    public void setAutoSend(String aAutoSend)
    {
        AutoSend = aAutoSend;
    }

    /**
     * 使用另外一个 LMCertifySubSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMCertifySubSchema aLMCertifySubSchema)
    {
        this.CertifyCode = aLMCertifySubSchema.getCertifyCode();
        this.CertifyClass = aLMCertifySubSchema.getCertifyClass();
        this.DBServer = aLMCertifySubSchema.getDBServer();
        this.DBName = aLMCertifySubSchema.getDBName();
        this.TableName = aLMCertifySubSchema.getTableName();
        this.FieldName = aLMCertifySubSchema.getFieldName();
        this.BeforeSQL = aLMCertifySubSchema.getBeforeSQL();
        this.AfterSQL = aLMCertifySubSchema.getAfterSQL();
        this.ErrorMessage = aLMCertifySubSchema.getErrorMessage();
        this.AutoTakeBack = aLMCertifySubSchema.getAutoTakeBack();
        this.InterfaceClass = aLMCertifySubSchema.getInterfaceClass();
        this.AutoSend = aLMCertifySubSchema.getAutoSend();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CertifyCode") == null)
            {
                this.CertifyCode = null;
            }
            else
            {
                this.CertifyCode = rs.getString("CertifyCode").trim();
            }

            if (rs.getString("CertifyClass") == null)
            {
                this.CertifyClass = null;
            }
            else
            {
                this.CertifyClass = rs.getString("CertifyClass").trim();
            }

            if (rs.getString("DBServer") == null)
            {
                this.DBServer = null;
            }
            else
            {
                this.DBServer = rs.getString("DBServer").trim();
            }

            if (rs.getString("DBName") == null)
            {
                this.DBName = null;
            }
            else
            {
                this.DBName = rs.getString("DBName").trim();
            }

            if (rs.getString("TableName") == null)
            {
                this.TableName = null;
            }
            else
            {
                this.TableName = rs.getString("TableName").trim();
            }

            if (rs.getString("FieldName") == null)
            {
                this.FieldName = null;
            }
            else
            {
                this.FieldName = rs.getString("FieldName").trim();
            }

            if (rs.getString("BeforeSQL") == null)
            {
                this.BeforeSQL = null;
            }
            else
            {
                this.BeforeSQL = rs.getString("BeforeSQL").trim();
            }

            if (rs.getString("AfterSQL") == null)
            {
                this.AfterSQL = null;
            }
            else
            {
                this.AfterSQL = rs.getString("AfterSQL").trim();
            }

            if (rs.getString("ErrorMessage") == null)
            {
                this.ErrorMessage = null;
            }
            else
            {
                this.ErrorMessage = rs.getString("ErrorMessage").trim();
            }

            if (rs.getString("AutoTakeBack") == null)
            {
                this.AutoTakeBack = null;
            }
            else
            {
                this.AutoTakeBack = rs.getString("AutoTakeBack").trim();
            }

            if (rs.getString("InterfaceClass") == null)
            {
                this.InterfaceClass = null;
            }
            else
            {
                this.InterfaceClass = rs.getString("InterfaceClass").trim();
            }

            if (rs.getString("AutoSend") == null)
            {
                this.AutoSend = null;
            }
            else
            {
                this.AutoSend = rs.getString("AutoSend").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCertifySubSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMCertifySubSchema getSchema()
    {
        LMCertifySubSchema aLMCertifySubSchema = new LMCertifySubSchema();
        aLMCertifySubSchema.setSchema(this);
        return aLMCertifySubSchema;
    }

    public LMCertifySubDB getDB()
    {
        LMCertifySubDB aDBOper = new LMCertifySubDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCertifySub描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CertifyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CertifyClass)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DBServer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DBName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TableName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FieldName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BeforeSQL)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AfterSQL)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrorMessage)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AutoTakeBack)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InterfaceClass)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AutoSend));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCertifySub>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            CertifyClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            DBServer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            DBName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            TableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            FieldName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            BeforeSQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            AfterSQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ErrorMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            AutoTakeBack = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            InterfaceClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            11, SysConst.PACKAGESPILTER);
            AutoSend = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMCertifySubSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CertifyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CertifyCode));
        }
        if (FCode.equals("CertifyClass"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CertifyClass));
        }
        if (FCode.equals("DBServer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DBServer));
        }
        if (FCode.equals("DBName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DBName));
        }
        if (FCode.equals("TableName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TableName));
        }
        if (FCode.equals("FieldName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FieldName));
        }
        if (FCode.equals("BeforeSQL"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BeforeSQL));
        }
        if (FCode.equals("AfterSQL"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AfterSQL));
        }
        if (FCode.equals("ErrorMessage"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrorMessage));
        }
        if (FCode.equals("AutoTakeBack"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AutoTakeBack));
        }
        if (FCode.equals("InterfaceClass"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InterfaceClass));
        }
        if (FCode.equals("AutoSend"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AutoSend));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CertifyCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CertifyClass);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DBServer);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DBName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(TableName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(FieldName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BeforeSQL);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AfterSQL);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ErrorMessage);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AutoTakeBack);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InterfaceClass);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AutoSend);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CertifyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CertifyCode = FValue.trim();
            }
            else
            {
                CertifyCode = null;
            }
        }
        if (FCode.equals("CertifyClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CertifyClass = FValue.trim();
            }
            else
            {
                CertifyClass = null;
            }
        }
        if (FCode.equals("DBServer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DBServer = FValue.trim();
            }
            else
            {
                DBServer = null;
            }
        }
        if (FCode.equals("DBName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DBName = FValue.trim();
            }
            else
            {
                DBName = null;
            }
        }
        if (FCode.equals("TableName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TableName = FValue.trim();
            }
            else
            {
                TableName = null;
            }
        }
        if (FCode.equals("FieldName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FieldName = FValue.trim();
            }
            else
            {
                FieldName = null;
            }
        }
        if (FCode.equals("BeforeSQL"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BeforeSQL = FValue.trim();
            }
            else
            {
                BeforeSQL = null;
            }
        }
        if (FCode.equals("AfterSQL"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AfterSQL = FValue.trim();
            }
            else
            {
                AfterSQL = null;
            }
        }
        if (FCode.equals("ErrorMessage"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrorMessage = FValue.trim();
            }
            else
            {
                ErrorMessage = null;
            }
        }
        if (FCode.equals("AutoTakeBack"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AutoTakeBack = FValue.trim();
            }
            else
            {
                AutoTakeBack = null;
            }
        }
        if (FCode.equals("InterfaceClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InterfaceClass = FValue.trim();
            }
            else
            {
                InterfaceClass = null;
            }
        }
        if (FCode.equals("AutoSend"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AutoSend = FValue.trim();
            }
            else
            {
                AutoSend = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMCertifySubSchema other = (LMCertifySubSchema) otherObject;
        return
                CertifyCode.equals(other.getCertifyCode())
                && CertifyClass.equals(other.getCertifyClass())
                && DBServer.equals(other.getDBServer())
                && DBName.equals(other.getDBName())
                && TableName.equals(other.getTableName())
                && FieldName.equals(other.getFieldName())
                && BeforeSQL.equals(other.getBeforeSQL())
                && AfterSQL.equals(other.getAfterSQL())
                && ErrorMessage.equals(other.getErrorMessage())
                && AutoTakeBack.equals(other.getAutoTakeBack())
                && InterfaceClass.equals(other.getInterfaceClass())
                && AutoSend.equals(other.getAutoSend());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CertifyCode"))
        {
            return 0;
        }
        if (strFieldName.equals("CertifyClass"))
        {
            return 1;
        }
        if (strFieldName.equals("DBServer"))
        {
            return 2;
        }
        if (strFieldName.equals("DBName"))
        {
            return 3;
        }
        if (strFieldName.equals("TableName"))
        {
            return 4;
        }
        if (strFieldName.equals("FieldName"))
        {
            return 5;
        }
        if (strFieldName.equals("BeforeSQL"))
        {
            return 6;
        }
        if (strFieldName.equals("AfterSQL"))
        {
            return 7;
        }
        if (strFieldName.equals("ErrorMessage"))
        {
            return 8;
        }
        if (strFieldName.equals("AutoTakeBack"))
        {
            return 9;
        }
        if (strFieldName.equals("InterfaceClass"))
        {
            return 10;
        }
        if (strFieldName.equals("AutoSend"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CertifyCode";
                break;
            case 1:
                strFieldName = "CertifyClass";
                break;
            case 2:
                strFieldName = "DBServer";
                break;
            case 3:
                strFieldName = "DBName";
                break;
            case 4:
                strFieldName = "TableName";
                break;
            case 5:
                strFieldName = "FieldName";
                break;
            case 6:
                strFieldName = "BeforeSQL";
                break;
            case 7:
                strFieldName = "AfterSQL";
                break;
            case 8:
                strFieldName = "ErrorMessage";
                break;
            case 9:
                strFieldName = "AutoTakeBack";
                break;
            case 10:
                strFieldName = "InterfaceClass";
                break;
            case 11:
                strFieldName = "AutoSend";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CertifyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifyClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DBServer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DBName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TableName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FieldName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BeforeSQL"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AfterSQL"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrorMessage"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AutoTakeBack"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterfaceClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AutoSend"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
