/*
 * <p>ClassName: LDBankRateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDBankRateDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDBankRateSchema implements Schema
{
    // @Field
    /** 开始日期 */
    private Date StartDate;
    /** 结束日期 */
    private Date EndDate;
    /** 利率类型 */
    private String RateType;
    /** 利率间隔 */
    private String RateIntv;
    /** 利率 */
    private double Rate;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDBankRateSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "StartDate";
        pk[1] = "EndDate";
        pk[2] = "RateType";
        pk[3] = "RateIntv";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }

    public String getRateType()
    {
        if (RateType != null && !RateType.equals("") && SysConst.CHANGECHARSET == true)
        {
            RateType = StrTool.unicodeToGBK(RateType);
        }
        return RateType;
    }

    public void setRateType(String aRateType)
    {
        RateType = aRateType;
    }

    public String getRateIntv()
    {
        if (RateIntv != null && !RateIntv.equals("") && SysConst.CHANGECHARSET == true)
        {
            RateIntv = StrTool.unicodeToGBK(RateIntv);
        }
        return RateIntv;
    }

    public void setRateIntv(String aRateIntv)
    {
        RateIntv = aRateIntv;
    }

    public double getRate()
    {
        return Rate;
    }

    public void setRate(double aRate)
    {
        Rate = aRate;
    }

    public void setRate(String aRate)
    {
        if (aRate != null && !aRate.equals(""))
        {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }


    /**
     * 使用另外一个 LDBankRateSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDBankRateSchema aLDBankRateSchema)
    {
        this.StartDate = fDate.getDate(aLDBankRateSchema.getStartDate());
        this.EndDate = fDate.getDate(aLDBankRateSchema.getEndDate());
        this.RateType = aLDBankRateSchema.getRateType();
        this.RateIntv = aLDBankRateSchema.getRateIntv();
        this.Rate = aLDBankRateSchema.getRate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("RateType") == null)
            {
                this.RateType = null;
            }
            else
            {
                this.RateType = rs.getString("RateType").trim();
            }

            if (rs.getString("RateIntv") == null)
            {
                this.RateIntv = null;
            }
            else
            {
                this.RateIntv = rs.getString("RateIntv").trim();
            }

            this.Rate = rs.getDouble("Rate");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBankRateSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDBankRateSchema getSchema()
    {
        LDBankRateSchema aLDBankRateSchema = new LDBankRateSchema();
        aLDBankRateSchema.setSchema(this);
        return aLDBankRateSchema;
    }

    public LDBankRateDB getDB()
    {
        LDBankRateDB aDBOper = new LDBankRateDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBankRate描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                StartDate))) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(EndDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RateType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RateIntv)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rate);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBankRate>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 1, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER));
            RateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RateIntv = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    5, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBankRateSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getStartDate()));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getEndDate()));
        }
        if (FCode.equals("RateType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RateType));
        }
        if (FCode.equals("RateIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RateIntv));
        }
        if (FCode.equals("Rate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rate));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RateType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RateIntv);
                break;
            case 4:
                strFieldValue = String.valueOf(Rate);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        if (FCode.equals("RateType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RateType = FValue.trim();
            }
            else
            {
                RateType = null;
            }
        }
        if (FCode.equals("RateIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RateIntv = FValue.trim();
            }
            else
            {
                RateIntv = null;
            }
        }
        if (FCode.equals("Rate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDBankRateSchema other = (LDBankRateSchema) otherObject;
        return
                fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && RateType.equals(other.getRateType())
                && RateIntv.equals(other.getRateIntv())
                && Rate == other.getRate();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("StartDate"))
        {
            return 0;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 1;
        }
        if (strFieldName.equals("RateType"))
        {
            return 2;
        }
        if (strFieldName.equals("RateIntv"))
        {
            return 3;
        }
        if (strFieldName.equals("Rate"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "StartDate";
                break;
            case 1:
                strFieldName = "EndDate";
                break;
            case 2:
                strFieldName = "RateType";
                break;
            case 3:
                strFieldName = "RateIntv";
                break;
            case 4:
                strFieldName = "Rate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RateType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RateIntv"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Rate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 1:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
