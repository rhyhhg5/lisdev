/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIAboriginalDataDB;

/*
 * <p>ClassName: LIAboriginalDataSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-19
 */
public class LIAboriginalDataSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 批次号 */
	private String BatchNo;
	/** 数据类型编码 */
	private String ClassID;
	/** 暂交费收据号码 */
	private String TempFeeNo;
	/** 实付号码 */
	private String ActuGetNo;
	/** 实收号码 */
	private String PayNo;
	/** 保全受理号 */
	private String EdorAcceptNo;
	/** 批单号码 */
	private String EndorsementNo;
	/** 赔案号 */
	private String CaseNo;
	/** 补/退费业务类型 */
	private String FeeOperationType;
	/** 补/退费财务类型 */
	private String FeeFinaType;
	/** 个团银明细标志 */
	private String ListFlag;
	/** 集体合同号码 */
	private String GrpContNo;
	/** 集体保单险种号码 */
	private String GrpPolNo;
	/** 合同号码 */
	private String ContNo;
	/** 保单险种号码 */
	private String PolNo;
	/** 交费间隔标志 */
	private String PayIntvFlag;
	/** 交费间隔 */
	private int PayIntv;
	/** 补/退费日期 */
	private Date PayDate;
	/** 补/退费类型 */
	private String PayType;
	/** 补/退费到帐日期 */
	private Date EnterAccDate;
	/** 补/退费确认日期 */
	private Date ConfDate;
	/** 原交至日期 */
	private Date LastPayToDate;
	/** 现交至日期 */
	private Date CurPayToDate;
	/** 保单/险种生效日期 */
	private Date CValiDate;
	/** 保单年度 */
	private int PolYear;
	/** 首续年标志 */
	private String FirstYearFlag;
	/** 第几次交费 */
	private int PayCount;
	/** 首续期标志 */
	private String FirstTermFlag;
	/** 保险年期 */
	private int Years;
	/** 险种编码 */
	private String RiskCode;
	/** 险种类别 */
	private String RiskPeriod;
	/** 险种分类 */
	private String RiskType;
	/** 险种分类1 */
	private String RiskType1;
	/** 销售渠道 */
	private String SaleChnl;
	/** 管理机构 */
	private String ManageCom;
	/** 处理机构 */
	private String ExecuteCom;
	/** 代理机构 */
	private String AgentCom;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐户名 */
	private String AccName;
	/** 银行帐号 */
	private String BankAccNo;
	/** 总应交/付金额 */
	private double SumDueMoney;
	/** 总实交/付金额 */
	private double SumActuMoney;
	/** 币别 */
	private String Currency;
	/** 交费方式 */
	private String PayMode;
	/** 出纳类型 */
	private String OperationType;
	/** 预算 */
	private String Budget;
	/** 渠道详细 */
	private String SaleChnlDetail;
	/** 备用字符串一 */
	private String StandByString1;
	/** 备用字符串二 */
	private String StandByString2;
	/** 备用字符串三 */
	private String StandByString3;
	/** 备用数字一 */
	private double StandByNum1;
	/** 备用数字二 */
	private double StandByNum2;
	/** 备用日期一 */
	private Date StandByDate1;
	/** 备用日期二 */
	private Date StandByDate2;
	/** 大客户 */
	private String BClient;
	/** 市场类型 */
	private String MarketType;
	/** 保费收入类型 */
	private String PremiumType;
	/** 缴费首年 */
	private String FirstYear;

	public static final int FIELDNUM = 61;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIAboriginalDataSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LIAboriginalDataSchema cloned = (LIAboriginalDataSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getClassID()
	{
		return ClassID;
	}
	public void setClassID(String aClassID)
	{
		ClassID = aClassID;
	}
	public String getTempFeeNo()
	{
		return TempFeeNo;
	}
	public void setTempFeeNo(String aTempFeeNo)
	{
		TempFeeNo = aTempFeeNo;
	}
	public String getActuGetNo()
	{
		return ActuGetNo;
	}
	public void setActuGetNo(String aActuGetNo)
	{
		ActuGetNo = aActuGetNo;
	}
	public String getPayNo()
	{
		return PayNo;
	}
	public void setPayNo(String aPayNo)
	{
		PayNo = aPayNo;
	}
	public String getEdorAcceptNo()
	{
		return EdorAcceptNo;
	}
	public void setEdorAcceptNo(String aEdorAcceptNo)
	{
		EdorAcceptNo = aEdorAcceptNo;
	}
	public String getEndorsementNo()
	{
		return EndorsementNo;
	}
	public void setEndorsementNo(String aEndorsementNo)
	{
		EndorsementNo = aEndorsementNo;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getFeeOperationType()
	{
		return FeeOperationType;
	}
	public void setFeeOperationType(String aFeeOperationType)
	{
		FeeOperationType = aFeeOperationType;
	}
	public String getFeeFinaType()
	{
		return FeeFinaType;
	}
	public void setFeeFinaType(String aFeeFinaType)
	{
		FeeFinaType = aFeeFinaType;
	}
	public String getListFlag()
	{
		return ListFlag;
	}
	public void setListFlag(String aListFlag)
	{
		ListFlag = aListFlag;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getPayIntvFlag()
	{
		return PayIntvFlag;
	}
	public void setPayIntvFlag(String aPayIntvFlag)
	{
		PayIntvFlag = aPayIntvFlag;
	}
	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public String getPayDate()
	{
		if( PayDate != null )
			return fDate.getString(PayDate);
		else
			return null;
	}
	public void setPayDate(Date aPayDate)
	{
		PayDate = aPayDate;
	}
	public void setPayDate(String aPayDate)
	{
		if (aPayDate != null && !aPayDate.equals("") )
		{
			PayDate = fDate.getDate( aPayDate );
		}
		else
			PayDate = null;
	}

	public String getPayType()
	{
		return PayType;
	}
	public void setPayType(String aPayType)
	{
		PayType = aPayType;
	}
	public String getEnterAccDate()
	{
		if( EnterAccDate != null )
			return fDate.getString(EnterAccDate);
		else
			return null;
	}
	public void setEnterAccDate(Date aEnterAccDate)
	{
		EnterAccDate = aEnterAccDate;
	}
	public void setEnterAccDate(String aEnterAccDate)
	{
		if (aEnterAccDate != null && !aEnterAccDate.equals("") )
		{
			EnterAccDate = fDate.getDate( aEnterAccDate );
		}
		else
			EnterAccDate = null;
	}

	public String getConfDate()
	{
		if( ConfDate != null )
			return fDate.getString(ConfDate);
		else
			return null;
	}
	public void setConfDate(Date aConfDate)
	{
		ConfDate = aConfDate;
	}
	public void setConfDate(String aConfDate)
	{
		if (aConfDate != null && !aConfDate.equals("") )
		{
			ConfDate = fDate.getDate( aConfDate );
		}
		else
			ConfDate = null;
	}

	public String getLastPayToDate()
	{
		if( LastPayToDate != null )
			return fDate.getString(LastPayToDate);
		else
			return null;
	}
	public void setLastPayToDate(Date aLastPayToDate)
	{
		LastPayToDate = aLastPayToDate;
	}
	public void setLastPayToDate(String aLastPayToDate)
	{
		if (aLastPayToDate != null && !aLastPayToDate.equals("") )
		{
			LastPayToDate = fDate.getDate( aLastPayToDate );
		}
		else
			LastPayToDate = null;
	}

	public String getCurPayToDate()
	{
		if( CurPayToDate != null )
			return fDate.getString(CurPayToDate);
		else
			return null;
	}
	public void setCurPayToDate(Date aCurPayToDate)
	{
		CurPayToDate = aCurPayToDate;
	}
	public void setCurPayToDate(String aCurPayToDate)
	{
		if (aCurPayToDate != null && !aCurPayToDate.equals("") )
		{
			CurPayToDate = fDate.getDate( aCurPayToDate );
		}
		else
			CurPayToDate = null;
	}

	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public int getPolYear()
	{
		return PolYear;
	}
	public void setPolYear(int aPolYear)
	{
		PolYear = aPolYear;
	}
	public void setPolYear(String aPolYear)
	{
		if (aPolYear != null && !aPolYear.equals(""))
		{
			Integer tInteger = new Integer(aPolYear);
			int i = tInteger.intValue();
			PolYear = i;
		}
	}

	public String getFirstYearFlag()
	{
		return FirstYearFlag;
	}
	public void setFirstYearFlag(String aFirstYearFlag)
	{
		FirstYearFlag = aFirstYearFlag;
	}
	public int getPayCount()
	{
		return PayCount;
	}
	public void setPayCount(int aPayCount)
	{
		PayCount = aPayCount;
	}
	public void setPayCount(String aPayCount)
	{
		if (aPayCount != null && !aPayCount.equals(""))
		{
			Integer tInteger = new Integer(aPayCount);
			int i = tInteger.intValue();
			PayCount = i;
		}
	}

	public String getFirstTermFlag()
	{
		return FirstTermFlag;
	}
	public void setFirstTermFlag(String aFirstTermFlag)
	{
		FirstTermFlag = aFirstTermFlag;
	}
	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskPeriod()
	{
		return RiskPeriod;
	}
	public void setRiskPeriod(String aRiskPeriod)
	{
		RiskPeriod = aRiskPeriod;
	}
	public String getRiskType()
	{
		return RiskType;
	}
	public void setRiskType(String aRiskType)
	{
		RiskType = aRiskType;
	}
	public String getRiskType1()
	{
		return RiskType1;
	}
	public void setRiskType1(String aRiskType1)
	{
		RiskType1 = aRiskType1;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getExecuteCom()
	{
		return ExecuteCom;
	}
	public void setExecuteCom(String aExecuteCom)
	{
		ExecuteCom = aExecuteCom;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public double getSumDueMoney()
	{
		return SumDueMoney;
	}
	public void setSumDueMoney(double aSumDueMoney)
	{
		SumDueMoney = Arith.round(aSumDueMoney,2);
	}
	public void setSumDueMoney(String aSumDueMoney)
	{
		if (aSumDueMoney != null && !aSumDueMoney.equals(""))
		{
			Double tDouble = new Double(aSumDueMoney);
			double d = tDouble.doubleValue();
                SumDueMoney = Arith.round(d,2);
		}
	}

	public double getSumActuMoney()
	{
		return SumActuMoney;
	}
	public void setSumActuMoney(double aSumActuMoney)
	{
		SumActuMoney = Arith.round(aSumActuMoney,2);
	}
	public void setSumActuMoney(String aSumActuMoney)
	{
		if (aSumActuMoney != null && !aSumActuMoney.equals(""))
		{
			Double tDouble = new Double(aSumActuMoney);
			double d = tDouble.doubleValue();
                SumActuMoney = Arith.round(d,2);
		}
	}

	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getOperationType()
	{
		return OperationType;
	}
	public void setOperationType(String aOperationType)
	{
		OperationType = aOperationType;
	}
	public String getBudget()
	{
		return Budget;
	}
	public void setBudget(String aBudget)
	{
		Budget = aBudget;
	}
	public String getSaleChnlDetail()
	{
		return SaleChnlDetail;
	}
	public void setSaleChnlDetail(String aSaleChnlDetail)
	{
		SaleChnlDetail = aSaleChnlDetail;
	}
	public String getStandByString1()
	{
		return StandByString1;
	}
	public void setStandByString1(String aStandByString1)
	{
		StandByString1 = aStandByString1;
	}
	public String getStandByString2()
	{
		return StandByString2;
	}
	public void setStandByString2(String aStandByString2)
	{
		StandByString2 = aStandByString2;
	}
	public String getStandByString3()
	{
		return StandByString3;
	}
	public void setStandByString3(String aStandByString3)
	{
		StandByString3 = aStandByString3;
	}
	public double getStandByNum1()
	{
		return StandByNum1;
	}
	public void setStandByNum1(double aStandByNum1)
	{
		StandByNum1 = Arith.round(aStandByNum1,2);
	}
	public void setStandByNum1(String aStandByNum1)
	{
		if (aStandByNum1 != null && !aStandByNum1.equals(""))
		{
			Double tDouble = new Double(aStandByNum1);
			double d = tDouble.doubleValue();
                StandByNum1 = Arith.round(d,2);
		}
	}

	public double getStandByNum2()
	{
		return StandByNum2;
	}
	public void setStandByNum2(double aStandByNum2)
	{
		StandByNum2 = Arith.round(aStandByNum2,2);
	}
	public void setStandByNum2(String aStandByNum2)
	{
		if (aStandByNum2 != null && !aStandByNum2.equals(""))
		{
			Double tDouble = new Double(aStandByNum2);
			double d = tDouble.doubleValue();
                StandByNum2 = Arith.round(d,2);
		}
	}

	public String getStandByDate1()
	{
		if( StandByDate1 != null )
			return fDate.getString(StandByDate1);
		else
			return null;
	}
	public void setStandByDate1(Date aStandByDate1)
	{
		StandByDate1 = aStandByDate1;
	}
	public void setStandByDate1(String aStandByDate1)
	{
		if (aStandByDate1 != null && !aStandByDate1.equals("") )
		{
			StandByDate1 = fDate.getDate( aStandByDate1 );
		}
		else
			StandByDate1 = null;
	}

	public String getStandByDate2()
	{
		if( StandByDate2 != null )
			return fDate.getString(StandByDate2);
		else
			return null;
	}
	public void setStandByDate2(Date aStandByDate2)
	{
		StandByDate2 = aStandByDate2;
	}
	public void setStandByDate2(String aStandByDate2)
	{
		if (aStandByDate2 != null && !aStandByDate2.equals("") )
		{
			StandByDate2 = fDate.getDate( aStandByDate2 );
		}
		else
			StandByDate2 = null;
	}

	public String getBClient()
	{
		return BClient;
	}
	public void setBClient(String aBClient)
	{
		BClient = aBClient;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getPremiumType()
	{
		return PremiumType;
	}
	public void setPremiumType(String aPremiumType)
	{
		PremiumType = aPremiumType;
	}
	public String getFirstYear()
	{
		return FirstYear;
	}
	public void setFirstYear(String aFirstYear)
	{
		FirstYear = aFirstYear;
	}

	/**
	* 使用另外一个 LIAboriginalDataSchema 对象给 Schema 赋值
	* @param: aLIAboriginalDataSchema LIAboriginalDataSchema
	**/
	public void setSchema(LIAboriginalDataSchema aLIAboriginalDataSchema)
	{
		this.SerialNo = aLIAboriginalDataSchema.getSerialNo();
		this.BatchNo = aLIAboriginalDataSchema.getBatchNo();
		this.ClassID = aLIAboriginalDataSchema.getClassID();
		this.TempFeeNo = aLIAboriginalDataSchema.getTempFeeNo();
		this.ActuGetNo = aLIAboriginalDataSchema.getActuGetNo();
		this.PayNo = aLIAboriginalDataSchema.getPayNo();
		this.EdorAcceptNo = aLIAboriginalDataSchema.getEdorAcceptNo();
		this.EndorsementNo = aLIAboriginalDataSchema.getEndorsementNo();
		this.CaseNo = aLIAboriginalDataSchema.getCaseNo();
		this.FeeOperationType = aLIAboriginalDataSchema.getFeeOperationType();
		this.FeeFinaType = aLIAboriginalDataSchema.getFeeFinaType();
		this.ListFlag = aLIAboriginalDataSchema.getListFlag();
		this.GrpContNo = aLIAboriginalDataSchema.getGrpContNo();
		this.GrpPolNo = aLIAboriginalDataSchema.getGrpPolNo();
		this.ContNo = aLIAboriginalDataSchema.getContNo();
		this.PolNo = aLIAboriginalDataSchema.getPolNo();
		this.PayIntvFlag = aLIAboriginalDataSchema.getPayIntvFlag();
		this.PayIntv = aLIAboriginalDataSchema.getPayIntv();
		this.PayDate = fDate.getDate( aLIAboriginalDataSchema.getPayDate());
		this.PayType = aLIAboriginalDataSchema.getPayType();
		this.EnterAccDate = fDate.getDate( aLIAboriginalDataSchema.getEnterAccDate());
		this.ConfDate = fDate.getDate( aLIAboriginalDataSchema.getConfDate());
		this.LastPayToDate = fDate.getDate( aLIAboriginalDataSchema.getLastPayToDate());
		this.CurPayToDate = fDate.getDate( aLIAboriginalDataSchema.getCurPayToDate());
		this.CValiDate = fDate.getDate( aLIAboriginalDataSchema.getCValiDate());
		this.PolYear = aLIAboriginalDataSchema.getPolYear();
		this.FirstYearFlag = aLIAboriginalDataSchema.getFirstYearFlag();
		this.PayCount = aLIAboriginalDataSchema.getPayCount();
		this.FirstTermFlag = aLIAboriginalDataSchema.getFirstTermFlag();
		this.Years = aLIAboriginalDataSchema.getYears();
		this.RiskCode = aLIAboriginalDataSchema.getRiskCode();
		this.RiskPeriod = aLIAboriginalDataSchema.getRiskPeriod();
		this.RiskType = aLIAboriginalDataSchema.getRiskType();
		this.RiskType1 = aLIAboriginalDataSchema.getRiskType1();
		this.SaleChnl = aLIAboriginalDataSchema.getSaleChnl();
		this.ManageCom = aLIAboriginalDataSchema.getManageCom();
		this.ExecuteCom = aLIAboriginalDataSchema.getExecuteCom();
		this.AgentCom = aLIAboriginalDataSchema.getAgentCom();
		this.AgentCode = aLIAboriginalDataSchema.getAgentCode();
		this.AgentGroup = aLIAboriginalDataSchema.getAgentGroup();
		this.BankCode = aLIAboriginalDataSchema.getBankCode();
		this.AccName = aLIAboriginalDataSchema.getAccName();
		this.BankAccNo = aLIAboriginalDataSchema.getBankAccNo();
		this.SumDueMoney = aLIAboriginalDataSchema.getSumDueMoney();
		this.SumActuMoney = aLIAboriginalDataSchema.getSumActuMoney();
		this.Currency = aLIAboriginalDataSchema.getCurrency();
		this.PayMode = aLIAboriginalDataSchema.getPayMode();
		this.OperationType = aLIAboriginalDataSchema.getOperationType();
		this.Budget = aLIAboriginalDataSchema.getBudget();
		this.SaleChnlDetail = aLIAboriginalDataSchema.getSaleChnlDetail();
		this.StandByString1 = aLIAboriginalDataSchema.getStandByString1();
		this.StandByString2 = aLIAboriginalDataSchema.getStandByString2();
		this.StandByString3 = aLIAboriginalDataSchema.getStandByString3();
		this.StandByNum1 = aLIAboriginalDataSchema.getStandByNum1();
		this.StandByNum2 = aLIAboriginalDataSchema.getStandByNum2();
		this.StandByDate1 = fDate.getDate( aLIAboriginalDataSchema.getStandByDate1());
		this.StandByDate2 = fDate.getDate( aLIAboriginalDataSchema.getStandByDate2());
		this.BClient = aLIAboriginalDataSchema.getBClient();
		this.MarketType = aLIAboriginalDataSchema.getMarketType();
		this.PremiumType = aLIAboriginalDataSchema.getPremiumType();
		this.FirstYear = aLIAboriginalDataSchema.getFirstYear();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("ClassID") == null )
				this.ClassID = null;
			else
				this.ClassID = rs.getString("ClassID").trim();

			if( rs.getString("TempFeeNo") == null )
				this.TempFeeNo = null;
			else
				this.TempFeeNo = rs.getString("TempFeeNo").trim();

			if( rs.getString("ActuGetNo") == null )
				this.ActuGetNo = null;
			else
				this.ActuGetNo = rs.getString("ActuGetNo").trim();

			if( rs.getString("PayNo") == null )
				this.PayNo = null;
			else
				this.PayNo = rs.getString("PayNo").trim();

			if( rs.getString("EdorAcceptNo") == null )
				this.EdorAcceptNo = null;
			else
				this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();

			if( rs.getString("EndorsementNo") == null )
				this.EndorsementNo = null;
			else
				this.EndorsementNo = rs.getString("EndorsementNo").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("FeeOperationType") == null )
				this.FeeOperationType = null;
			else
				this.FeeOperationType = rs.getString("FeeOperationType").trim();

			if( rs.getString("FeeFinaType") == null )
				this.FeeFinaType = null;
			else
				this.FeeFinaType = rs.getString("FeeFinaType").trim();

			if( rs.getString("ListFlag") == null )
				this.ListFlag = null;
			else
				this.ListFlag = rs.getString("ListFlag").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("PayIntvFlag") == null )
				this.PayIntvFlag = null;
			else
				this.PayIntvFlag = rs.getString("PayIntvFlag").trim();

			this.PayIntv = rs.getInt("PayIntv");
			this.PayDate = rs.getDate("PayDate");
			if( rs.getString("PayType") == null )
				this.PayType = null;
			else
				this.PayType = rs.getString("PayType").trim();

			this.EnterAccDate = rs.getDate("EnterAccDate");
			this.ConfDate = rs.getDate("ConfDate");
			this.LastPayToDate = rs.getDate("LastPayToDate");
			this.CurPayToDate = rs.getDate("CurPayToDate");
			this.CValiDate = rs.getDate("CValiDate");
			this.PolYear = rs.getInt("PolYear");
			if( rs.getString("FirstYearFlag") == null )
				this.FirstYearFlag = null;
			else
				this.FirstYearFlag = rs.getString("FirstYearFlag").trim();

			this.PayCount = rs.getInt("PayCount");
			if( rs.getString("FirstTermFlag") == null )
				this.FirstTermFlag = null;
			else
				this.FirstTermFlag = rs.getString("FirstTermFlag").trim();

			this.Years = rs.getInt("Years");
			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskPeriod") == null )
				this.RiskPeriod = null;
			else
				this.RiskPeriod = rs.getString("RiskPeriod").trim();

			if( rs.getString("RiskType") == null )
				this.RiskType = null;
			else
				this.RiskType = rs.getString("RiskType").trim();

			if( rs.getString("RiskType1") == null )
				this.RiskType1 = null;
			else
				this.RiskType1 = rs.getString("RiskType1").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ExecuteCom") == null )
				this.ExecuteCom = null;
			else
				this.ExecuteCom = rs.getString("ExecuteCom").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			this.SumDueMoney = rs.getDouble("SumDueMoney");
			this.SumActuMoney = rs.getDouble("SumActuMoney");
			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("OperationType") == null )
				this.OperationType = null;
			else
				this.OperationType = rs.getString("OperationType").trim();

			if( rs.getString("Budget") == null )
				this.Budget = null;
			else
				this.Budget = rs.getString("Budget").trim();

			if( rs.getString("SaleChnlDetail") == null )
				this.SaleChnlDetail = null;
			else
				this.SaleChnlDetail = rs.getString("SaleChnlDetail").trim();

			if( rs.getString("StandByString1") == null )
				this.StandByString1 = null;
			else
				this.StandByString1 = rs.getString("StandByString1").trim();

			if( rs.getString("StandByString2") == null )
				this.StandByString2 = null;
			else
				this.StandByString2 = rs.getString("StandByString2").trim();

			if( rs.getString("StandByString3") == null )
				this.StandByString3 = null;
			else
				this.StandByString3 = rs.getString("StandByString3").trim();

			this.StandByNum1 = rs.getDouble("StandByNum1");
			this.StandByNum2 = rs.getDouble("StandByNum2");
			this.StandByDate1 = rs.getDate("StandByDate1");
			this.StandByDate2 = rs.getDate("StandByDate2");
			if( rs.getString("BClient") == null )
				this.BClient = null;
			else
				this.BClient = rs.getString("BClient").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("PremiumType") == null )
				this.PremiumType = null;
			else
				this.PremiumType = rs.getString("PremiumType").trim();

			if( rs.getString("FirstYear") == null )
				this.FirstYear = null;
			else
				this.FirstYear = rs.getString("FirstYear").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIAboriginalData表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIAboriginalDataSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIAboriginalDataSchema getSchema()
	{
		LIAboriginalDataSchema aLIAboriginalDataSchema = new LIAboriginalDataSchema();
		aLIAboriginalDataSchema.setSchema(this);
		return aLIAboriginalDataSchema;
	}

	public LIAboriginalDataDB getDB()
	{
		LIAboriginalDataDB aDBOper = new LIAboriginalDataDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIAboriginalData描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClassID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActuGetNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorAcceptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeOperationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeFinaType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ListFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayIntvFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CurPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTermFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskPeriod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumDueMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumActuMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OperationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Budget)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnlDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandByNum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandByNum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate2 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BClient)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremiumType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYear));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIAboriginalData>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ClassID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			PayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			EndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FeeOperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			FeeFinaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ListFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			PayIntvFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).intValue();
			PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			PayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			LastPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			CurPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			PolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).intValue();
			FirstYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			PayCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).intValue();
			FirstTermFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).intValue();
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			RiskPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			RiskType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			SumDueMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,44,SysConst.PACKAGESPILTER))).doubleValue();
			SumActuMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).doubleValue();
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			OperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			Budget = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			SaleChnlDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			StandByString1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			StandByString2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			StandByString3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			StandByNum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,54,SysConst.PACKAGESPILTER))).doubleValue();
			StandByNum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,55,SysConst.PACKAGESPILTER))).doubleValue();
			StandByDate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56,SysConst.PACKAGESPILTER));
			StandByDate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57,SysConst.PACKAGESPILTER));
			BClient = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			PremiumType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			FirstYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIAboriginalDataSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ClassID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassID));
		}
		if (FCode.equals("TempFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
		}
		if (FCode.equals("ActuGetNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
		}
		if (FCode.equals("PayNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayNo));
		}
		if (FCode.equals("EdorAcceptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
		}
		if (FCode.equals("EndorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndorsementNo));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("FeeOperationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeOperationType));
		}
		if (FCode.equals("FeeFinaType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeFinaType));
		}
		if (FCode.equals("ListFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ListFlag));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("PayIntvFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntvFlag));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("PayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
		}
		if (FCode.equals("PayType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayType));
		}
		if (FCode.equals("EnterAccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
		}
		if (FCode.equals("ConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
		}
		if (FCode.equals("LastPayToDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
		}
		if (FCode.equals("CurPayToDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("PolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolYear));
		}
		if (FCode.equals("FirstYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYearFlag));
		}
		if (FCode.equals("PayCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
		}
		if (FCode.equals("FirstTermFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTermFlag));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPeriod));
		}
		if (FCode.equals("RiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
		}
		if (FCode.equals("RiskType1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType1));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ExecuteCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("SumDueMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumDueMoney));
		}
		if (FCode.equals("SumActuMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuMoney));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("OperationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperationType));
		}
		if (FCode.equals("Budget"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Budget));
		}
		if (FCode.equals("SaleChnlDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlDetail));
		}
		if (FCode.equals("StandByString1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString1));
		}
		if (FCode.equals("StandByString2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString2));
		}
		if (FCode.equals("StandByString3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString3));
		}
		if (FCode.equals("StandByNum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByNum1));
		}
		if (FCode.equals("StandByNum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByNum2));
		}
		if (FCode.equals("StandByDate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate1()));
		}
		if (FCode.equals("StandByDate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate2()));
		}
		if (FCode.equals("BClient"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BClient));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("PremiumType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremiumType));
		}
		if (FCode.equals("FirstYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYear));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ClassID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(PayNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(EndorsementNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(FeeOperationType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(FeeFinaType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ListFlag);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(PayIntvFlag);
				break;
			case 17:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(PayType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 25:
				strFieldValue = String.valueOf(PolYear);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(FirstYearFlag);
				break;
			case 27:
				strFieldValue = String.valueOf(PayCount);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(FirstTermFlag);
				break;
			case 29:
				strFieldValue = String.valueOf(Years);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(RiskPeriod);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(RiskType);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(RiskType1);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 43:
				strFieldValue = String.valueOf(SumDueMoney);
				break;
			case 44:
				strFieldValue = String.valueOf(SumActuMoney);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(OperationType);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(Budget);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(SaleChnlDetail);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(StandByString1);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(StandByString2);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(StandByString3);
				break;
			case 53:
				strFieldValue = String.valueOf(StandByNum1);
				break;
			case 54:
				strFieldValue = String.valueOf(StandByNum2);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate1()));
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate2()));
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(BClient);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(PremiumType);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(FirstYear);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("ClassID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassID = FValue.trim();
			}
			else
				ClassID = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeNo = FValue.trim();
			}
			else
				TempFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("ActuGetNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActuGetNo = FValue.trim();
			}
			else
				ActuGetNo = null;
		}
		if (FCode.equalsIgnoreCase("PayNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayNo = FValue.trim();
			}
			else
				PayNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorAcceptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorAcceptNo = FValue.trim();
			}
			else
				EdorAcceptNo = null;
		}
		if (FCode.equalsIgnoreCase("EndorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndorsementNo = FValue.trim();
			}
			else
				EndorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeOperationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeOperationType = FValue.trim();
			}
			else
				FeeOperationType = null;
		}
		if (FCode.equalsIgnoreCase("FeeFinaType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeFinaType = FValue.trim();
			}
			else
				FeeFinaType = null;
		}
		if (FCode.equalsIgnoreCase("ListFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ListFlag = FValue.trim();
			}
			else
				ListFlag = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("PayIntvFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayIntvFlag = FValue.trim();
			}
			else
				PayIntvFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PayDate = fDate.getDate( FValue );
			}
			else
				PayDate = null;
		}
		if (FCode.equalsIgnoreCase("PayType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayType = FValue.trim();
			}
			else
				PayType = null;
		}
		if (FCode.equalsIgnoreCase("EnterAccDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EnterAccDate = fDate.getDate( FValue );
			}
			else
				EnterAccDate = null;
		}
		if (FCode.equalsIgnoreCase("ConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConfDate = fDate.getDate( FValue );
			}
			else
				ConfDate = null;
		}
		if (FCode.equalsIgnoreCase("LastPayToDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastPayToDate = fDate.getDate( FValue );
			}
			else
				LastPayToDate = null;
		}
		if (FCode.equalsIgnoreCase("CurPayToDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CurPayToDate = fDate.getDate( FValue );
			}
			else
				CurPayToDate = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("PolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("FirstYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYearFlag = FValue.trim();
			}
			else
				FirstYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("FirstTermFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTermFlag = FValue.trim();
			}
			else
				FirstTermFlag = null;
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskPeriod = FValue.trim();
			}
			else
				RiskPeriod = null;
		}
		if (FCode.equalsIgnoreCase("RiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType = FValue.trim();
			}
			else
				RiskType = null;
		}
		if (FCode.equalsIgnoreCase("RiskType1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType1 = FValue.trim();
			}
			else
				RiskType1 = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ExecuteCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExecuteCom = FValue.trim();
			}
			else
				ExecuteCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("SumDueMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumDueMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumActuMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumActuMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("OperationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperationType = FValue.trim();
			}
			else
				OperationType = null;
		}
		if (FCode.equalsIgnoreCase("Budget"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Budget = FValue.trim();
			}
			else
				Budget = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnlDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnlDetail = FValue.trim();
			}
			else
				SaleChnlDetail = null;
		}
		if (FCode.equalsIgnoreCase("StandByString1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString1 = FValue.trim();
			}
			else
				StandByString1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByString2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString2 = FValue.trim();
			}
			else
				StandByString2 = null;
		}
		if (FCode.equalsIgnoreCase("StandByString3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString3 = FValue.trim();
			}
			else
				StandByString3 = null;
		}
		if (FCode.equalsIgnoreCase("StandByNum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandByNum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandByNum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandByNum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandByDate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate1 = fDate.getDate( FValue );
			}
			else
				StandByDate1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByDate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate2 = fDate.getDate( FValue );
			}
			else
				StandByDate2 = null;
		}
		if (FCode.equalsIgnoreCase("BClient"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BClient = FValue.trim();
			}
			else
				BClient = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("PremiumType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremiumType = FValue.trim();
			}
			else
				PremiumType = null;
		}
		if (FCode.equalsIgnoreCase("FirstYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYear = FValue.trim();
			}
			else
				FirstYear = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIAboriginalDataSchema other = (LIAboriginalDataSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (ClassID == null ? other.getClassID() == null : ClassID.equals(other.getClassID()))
			&& (TempFeeNo == null ? other.getTempFeeNo() == null : TempFeeNo.equals(other.getTempFeeNo()))
			&& (ActuGetNo == null ? other.getActuGetNo() == null : ActuGetNo.equals(other.getActuGetNo()))
			&& (PayNo == null ? other.getPayNo() == null : PayNo.equals(other.getPayNo()))
			&& (EdorAcceptNo == null ? other.getEdorAcceptNo() == null : EdorAcceptNo.equals(other.getEdorAcceptNo()))
			&& (EndorsementNo == null ? other.getEndorsementNo() == null : EndorsementNo.equals(other.getEndorsementNo()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (FeeOperationType == null ? other.getFeeOperationType() == null : FeeOperationType.equals(other.getFeeOperationType()))
			&& (FeeFinaType == null ? other.getFeeFinaType() == null : FeeFinaType.equals(other.getFeeFinaType()))
			&& (ListFlag == null ? other.getListFlag() == null : ListFlag.equals(other.getListFlag()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpPolNo == null ? other.getGrpPolNo() == null : GrpPolNo.equals(other.getGrpPolNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (PayIntvFlag == null ? other.getPayIntvFlag() == null : PayIntvFlag.equals(other.getPayIntvFlag()))
			&& PayIntv == other.getPayIntv()
			&& (PayDate == null ? other.getPayDate() == null : fDate.getString(PayDate).equals(other.getPayDate()))
			&& (PayType == null ? other.getPayType() == null : PayType.equals(other.getPayType()))
			&& (EnterAccDate == null ? other.getEnterAccDate() == null : fDate.getString(EnterAccDate).equals(other.getEnterAccDate()))
			&& (ConfDate == null ? other.getConfDate() == null : fDate.getString(ConfDate).equals(other.getConfDate()))
			&& (LastPayToDate == null ? other.getLastPayToDate() == null : fDate.getString(LastPayToDate).equals(other.getLastPayToDate()))
			&& (CurPayToDate == null ? other.getCurPayToDate() == null : fDate.getString(CurPayToDate).equals(other.getCurPayToDate()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& PolYear == other.getPolYear()
			&& (FirstYearFlag == null ? other.getFirstYearFlag() == null : FirstYearFlag.equals(other.getFirstYearFlag()))
			&& PayCount == other.getPayCount()
			&& (FirstTermFlag == null ? other.getFirstTermFlag() == null : FirstTermFlag.equals(other.getFirstTermFlag()))
			&& Years == other.getYears()
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskPeriod == null ? other.getRiskPeriod() == null : RiskPeriod.equals(other.getRiskPeriod()))
			&& (RiskType == null ? other.getRiskType() == null : RiskType.equals(other.getRiskType()))
			&& (RiskType1 == null ? other.getRiskType1() == null : RiskType1.equals(other.getRiskType1()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ExecuteCom == null ? other.getExecuteCom() == null : ExecuteCom.equals(other.getExecuteCom()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& SumDueMoney == other.getSumDueMoney()
			&& SumActuMoney == other.getSumActuMoney()
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (OperationType == null ? other.getOperationType() == null : OperationType.equals(other.getOperationType()))
			&& (Budget == null ? other.getBudget() == null : Budget.equals(other.getBudget()))
			&& (SaleChnlDetail == null ? other.getSaleChnlDetail() == null : SaleChnlDetail.equals(other.getSaleChnlDetail()))
			&& (StandByString1 == null ? other.getStandByString1() == null : StandByString1.equals(other.getStandByString1()))
			&& (StandByString2 == null ? other.getStandByString2() == null : StandByString2.equals(other.getStandByString2()))
			&& (StandByString3 == null ? other.getStandByString3() == null : StandByString3.equals(other.getStandByString3()))
			&& StandByNum1 == other.getStandByNum1()
			&& StandByNum2 == other.getStandByNum2()
			&& (StandByDate1 == null ? other.getStandByDate1() == null : fDate.getString(StandByDate1).equals(other.getStandByDate1()))
			&& (StandByDate2 == null ? other.getStandByDate2() == null : fDate.getString(StandByDate2).equals(other.getStandByDate2()))
			&& (BClient == null ? other.getBClient() == null : BClient.equals(other.getBClient()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (PremiumType == null ? other.getPremiumType() == null : PremiumType.equals(other.getPremiumType()))
			&& (FirstYear == null ? other.getFirstYear() == null : FirstYear.equals(other.getFirstYear()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("ClassID") ) {
			return 2;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return 3;
		}
		if( strFieldName.equals("ActuGetNo") ) {
			return 4;
		}
		if( strFieldName.equals("PayNo") ) {
			return 5;
		}
		if( strFieldName.equals("EdorAcceptNo") ) {
			return 6;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return 7;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 8;
		}
		if( strFieldName.equals("FeeOperationType") ) {
			return 9;
		}
		if( strFieldName.equals("FeeFinaType") ) {
			return 10;
		}
		if( strFieldName.equals("ListFlag") ) {
			return 11;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 12;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 13;
		}
		if( strFieldName.equals("ContNo") ) {
			return 14;
		}
		if( strFieldName.equals("PolNo") ) {
			return 15;
		}
		if( strFieldName.equals("PayIntvFlag") ) {
			return 16;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 17;
		}
		if( strFieldName.equals("PayDate") ) {
			return 18;
		}
		if( strFieldName.equals("PayType") ) {
			return 19;
		}
		if( strFieldName.equals("EnterAccDate") ) {
			return 20;
		}
		if( strFieldName.equals("ConfDate") ) {
			return 21;
		}
		if( strFieldName.equals("LastPayToDate") ) {
			return 22;
		}
		if( strFieldName.equals("CurPayToDate") ) {
			return 23;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 24;
		}
		if( strFieldName.equals("PolYear") ) {
			return 25;
		}
		if( strFieldName.equals("FirstYearFlag") ) {
			return 26;
		}
		if( strFieldName.equals("PayCount") ) {
			return 27;
		}
		if( strFieldName.equals("FirstTermFlag") ) {
			return 28;
		}
		if( strFieldName.equals("Years") ) {
			return 29;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 30;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return 31;
		}
		if( strFieldName.equals("RiskType") ) {
			return 32;
		}
		if( strFieldName.equals("RiskType1") ) {
			return 33;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 34;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 35;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return 36;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 37;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 38;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 39;
		}
		if( strFieldName.equals("BankCode") ) {
			return 40;
		}
		if( strFieldName.equals("AccName") ) {
			return 41;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 42;
		}
		if( strFieldName.equals("SumDueMoney") ) {
			return 43;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return 44;
		}
		if( strFieldName.equals("Currency") ) {
			return 45;
		}
		if( strFieldName.equals("PayMode") ) {
			return 46;
		}
		if( strFieldName.equals("OperationType") ) {
			return 47;
		}
		if( strFieldName.equals("Budget") ) {
			return 48;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return 49;
		}
		if( strFieldName.equals("StandByString1") ) {
			return 50;
		}
		if( strFieldName.equals("StandByString2") ) {
			return 51;
		}
		if( strFieldName.equals("StandByString3") ) {
			return 52;
		}
		if( strFieldName.equals("StandByNum1") ) {
			return 53;
		}
		if( strFieldName.equals("StandByNum2") ) {
			return 54;
		}
		if( strFieldName.equals("StandByDate1") ) {
			return 55;
		}
		if( strFieldName.equals("StandByDate2") ) {
			return 56;
		}
		if( strFieldName.equals("BClient") ) {
			return 57;
		}
		if( strFieldName.equals("MarketType") ) {
			return 58;
		}
		if( strFieldName.equals("PremiumType") ) {
			return 59;
		}
		if( strFieldName.equals("FirstYear") ) {
			return 60;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "ClassID";
				break;
			case 3:
				strFieldName = "TempFeeNo";
				break;
			case 4:
				strFieldName = "ActuGetNo";
				break;
			case 5:
				strFieldName = "PayNo";
				break;
			case 6:
				strFieldName = "EdorAcceptNo";
				break;
			case 7:
				strFieldName = "EndorsementNo";
				break;
			case 8:
				strFieldName = "CaseNo";
				break;
			case 9:
				strFieldName = "FeeOperationType";
				break;
			case 10:
				strFieldName = "FeeFinaType";
				break;
			case 11:
				strFieldName = "ListFlag";
				break;
			case 12:
				strFieldName = "GrpContNo";
				break;
			case 13:
				strFieldName = "GrpPolNo";
				break;
			case 14:
				strFieldName = "ContNo";
				break;
			case 15:
				strFieldName = "PolNo";
				break;
			case 16:
				strFieldName = "PayIntvFlag";
				break;
			case 17:
				strFieldName = "PayIntv";
				break;
			case 18:
				strFieldName = "PayDate";
				break;
			case 19:
				strFieldName = "PayType";
				break;
			case 20:
				strFieldName = "EnterAccDate";
				break;
			case 21:
				strFieldName = "ConfDate";
				break;
			case 22:
				strFieldName = "LastPayToDate";
				break;
			case 23:
				strFieldName = "CurPayToDate";
				break;
			case 24:
				strFieldName = "CValiDate";
				break;
			case 25:
				strFieldName = "PolYear";
				break;
			case 26:
				strFieldName = "FirstYearFlag";
				break;
			case 27:
				strFieldName = "PayCount";
				break;
			case 28:
				strFieldName = "FirstTermFlag";
				break;
			case 29:
				strFieldName = "Years";
				break;
			case 30:
				strFieldName = "RiskCode";
				break;
			case 31:
				strFieldName = "RiskPeriod";
				break;
			case 32:
				strFieldName = "RiskType";
				break;
			case 33:
				strFieldName = "RiskType1";
				break;
			case 34:
				strFieldName = "SaleChnl";
				break;
			case 35:
				strFieldName = "ManageCom";
				break;
			case 36:
				strFieldName = "ExecuteCom";
				break;
			case 37:
				strFieldName = "AgentCom";
				break;
			case 38:
				strFieldName = "AgentCode";
				break;
			case 39:
				strFieldName = "AgentGroup";
				break;
			case 40:
				strFieldName = "BankCode";
				break;
			case 41:
				strFieldName = "AccName";
				break;
			case 42:
				strFieldName = "BankAccNo";
				break;
			case 43:
				strFieldName = "SumDueMoney";
				break;
			case 44:
				strFieldName = "SumActuMoney";
				break;
			case 45:
				strFieldName = "Currency";
				break;
			case 46:
				strFieldName = "PayMode";
				break;
			case 47:
				strFieldName = "OperationType";
				break;
			case 48:
				strFieldName = "Budget";
				break;
			case 49:
				strFieldName = "SaleChnlDetail";
				break;
			case 50:
				strFieldName = "StandByString1";
				break;
			case 51:
				strFieldName = "StandByString2";
				break;
			case 52:
				strFieldName = "StandByString3";
				break;
			case 53:
				strFieldName = "StandByNum1";
				break;
			case 54:
				strFieldName = "StandByNum2";
				break;
			case 55:
				strFieldName = "StandByDate1";
				break;
			case 56:
				strFieldName = "StandByDate2";
				break;
			case 57:
				strFieldName = "BClient";
				break;
			case 58:
				strFieldName = "MarketType";
				break;
			case 59:
				strFieldName = "PremiumType";
				break;
			case 60:
				strFieldName = "FirstYear";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClassID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActuGetNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorAcceptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeOperationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeFinaType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ListFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntvFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PayType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnterAccDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ConfDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LastPayToDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CurPayToDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("FirstYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("FirstTermFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumDueMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OperationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Budget") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByNum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandByNum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandByDate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StandByDate2") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BClient") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremiumType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstYear") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_INT;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_INT;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_INT;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_INT;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 54:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 55:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 56:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
