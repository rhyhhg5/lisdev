/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAFirstAssessDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAFirstAssessSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAFirstAssessSchema implements Schema
{
    // @Field
    /** 考核代码 */
    private String AssessCode;
    /** 代理人职级 */
    private String AgentGrade;
    /** 展业类型 */
    private String BranchType;
    /** 考核sql */
    private String Assesssql;
    /** 默认值 */
    private String DefaultValue;
    /** 备注 */
    private String Remark;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAFirstAssessSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AssessCode";
        pk[1] = "AgentGrade";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAssessCode()
    {
        if (SysConst.CHANGECHARSET && AssessCode != null &&
            !AssessCode.equals(""))
        {
            AssessCode = StrTool.unicodeToGBK(AssessCode);
        }
        return AssessCode;
    }

    public void setAssessCode(String aAssessCode)
    {
        AssessCode = aAssessCode;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getAssesssql()
    {
        if (SysConst.CHANGECHARSET && Assesssql != null && !Assesssql.equals(""))
        {
            Assesssql = StrTool.unicodeToGBK(Assesssql);
        }
        return Assesssql;
    }

    public void setAssesssql(String aAssesssql)
    {
        Assesssql = aAssesssql;
    }

    public String getDefaultValue()
    {
        if (SysConst.CHANGECHARSET && DefaultValue != null &&
            !DefaultValue.equals(""))
        {
            DefaultValue = StrTool.unicodeToGBK(DefaultValue);
        }
        return DefaultValue;
    }

    public void setDefaultValue(String aDefaultValue)
    {
        DefaultValue = aDefaultValue;
    }

    public String getRemark()
    {
        if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAFirstAssessSchema 对象给 Schema 赋值
     * @param: aLAFirstAssessSchema LAFirstAssessSchema
     **/
    public void setSchema(LAFirstAssessSchema aLAFirstAssessSchema)
    {
        this.AssessCode = aLAFirstAssessSchema.getAssessCode();
        this.AgentGrade = aLAFirstAssessSchema.getAgentGrade();
        this.BranchType = aLAFirstAssessSchema.getBranchType();
        this.Assesssql = aLAFirstAssessSchema.getAssesssql();
        this.DefaultValue = aLAFirstAssessSchema.getDefaultValue();
        this.Remark = aLAFirstAssessSchema.getRemark();
        this.BranchType2 = aLAFirstAssessSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AssessCode") == null)
            {
                this.AssessCode = null;
            }
            else
            {
                this.AssessCode = rs.getString("AssessCode").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("Assesssql") == null)
            {
                this.Assesssql = null;
            }
            else
            {
                this.Assesssql = rs.getString("Assesssql").trim();
            }

            if (rs.getString("DefaultValue") == null)
            {
                this.DefaultValue = null;
            }
            else
            {
                this.DefaultValue = rs.getString("DefaultValue").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAFirstAssessSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAFirstAssessSchema getSchema()
    {
        LAFirstAssessSchema aLAFirstAssessSchema = new LAFirstAssessSchema();
        aLAFirstAssessSchema.setSchema(this);
        return aLAFirstAssessSchema;
    }

    public LAFirstAssessDB getDB()
    {
        LAFirstAssessDB aDBOper = new LAFirstAssessDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAFirstAssess描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Assesssql)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DefaultValue)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Remark)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAFirstAssess>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AssessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            Assesssql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            DefaultValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAFirstAssessSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AssessCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessCode));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("Assesssql"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Assesssql));
        }
        if (FCode.equals("DefaultValue"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultValue));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AssessCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Assesssql);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DefaultValue);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AssessCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessCode = FValue.trim();
            }
            else
            {
                AssessCode = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("Assesssql"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Assesssql = FValue.trim();
            }
            else
            {
                Assesssql = null;
            }
        }
        if (FCode.equals("DefaultValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DefaultValue = FValue.trim();
            }
            else
            {
                DefaultValue = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAFirstAssessSchema other = (LAFirstAssessSchema) otherObject;
        return
                AssessCode.equals(other.getAssessCode())
                && AgentGrade.equals(other.getAgentGrade())
                && BranchType.equals(other.getBranchType())
                && Assesssql.equals(other.getAssesssql())
                && DefaultValue.equals(other.getDefaultValue())
                && Remark.equals(other.getRemark())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AssessCode"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 1;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 2;
        }
        if (strFieldName.equals("Assesssql"))
        {
            return 3;
        }
        if (strFieldName.equals("DefaultValue"))
        {
            return 4;
        }
        if (strFieldName.equals("Remark"))
        {
            return 5;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AssessCode";
                break;
            case 1:
                strFieldName = "AgentGrade";
                break;
            case 2:
                strFieldName = "BranchType";
                break;
            case 3:
                strFieldName = "Assesssql";
                break;
            case 4:
                strFieldName = "DefaultValue";
                break;
            case 5:
                strFieldName = "Remark";
                break;
            case 6:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AssessCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Assesssql"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DefaultValue"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
