/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHContServPriceDB;

/*
 * <p>ClassName: LHContServPriceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表修改-服务价格表-20060530
 * @CreateDate：2006-05-31
 */
public class LHContServPriceSchema implements Schema, Cloneable
{
	// @Field
	/** 合同编号 */
	private String ContraNo;
	/** 合同名称 */
	private String ContraName;
	/** 合同类型 */
	private String ContraType;
	/** 合同责任项目编号 */
	private String ContraItemNo;
	/** 签约对象代码 */
	private String ContraObj;
	/** 责任项目编号 */
	private String DutyItemCode;
	/** 服务价格表类型 */
	private String ServPriceType;
	/** 费用类型 */
	private String ExpType;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 服务金额 */
	private double MoneySum;
	/** 服务时程 */
	private String ServPros;
	/** 最大服务人数 */
	private String MaxServNum;
	/** 补充说明 */
	private String ExplanOther;
	/** 体检套餐代码 */
	private String MedicaItemCode;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LHContServPriceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "ContraNo";
		pk[1] = "ContraItemNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LHContServPriceSchema cloned = (LHContServPriceSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getContraNo()
	{
		return ContraNo;
	}
	public void setContraNo(String aContraNo)
	{
            ContraNo = aContraNo;
	}
	public String getContraName()
	{
		return ContraName;
	}
	public void setContraName(String aContraName)
	{
            ContraName = aContraName;
	}
	public String getContraType()
	{
		return ContraType;
	}
	public void setContraType(String aContraType)
	{
            ContraType = aContraType;
	}
	public String getContraItemNo()
	{
		return ContraItemNo;
	}
	public void setContraItemNo(String aContraItemNo)
	{
            ContraItemNo = aContraItemNo;
	}
	public String getContraObj()
	{
		return ContraObj;
	}
	public void setContraObj(String aContraObj)
	{
            ContraObj = aContraObj;
	}
	public String getDutyItemCode()
	{
		return DutyItemCode;
	}
	public void setDutyItemCode(String aDutyItemCode)
	{
            DutyItemCode = aDutyItemCode;
	}
	public String getServPriceType()
	{
		return ServPriceType;
	}
	public void setServPriceType(String aServPriceType)
	{
            ServPriceType = aServPriceType;
	}
	public String getExpType()
	{
		return ExpType;
	}
	public void setExpType(String aExpType)
	{
            ExpType = aExpType;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public double getMoneySum()
	{
		return MoneySum;
	}
	public void setMoneySum(double aMoneySum)
	{
            MoneySum = Arith.round(aMoneySum,2);
	}
	public void setMoneySum(String aMoneySum)
	{
		if (aMoneySum != null && !aMoneySum.equals(""))
		{
			Double tDouble = new Double(aMoneySum);
			double d = tDouble.doubleValue();
                MoneySum = Arith.round(d,2);
		}
	}

	public String getServPros()
	{
		return ServPros;
	}
	public void setServPros(String aServPros)
	{
            ServPros = aServPros;
	}
	public String getMaxServNum()
	{
		return MaxServNum;
	}
	public void setMaxServNum(String aMaxServNum)
	{
            MaxServNum = aMaxServNum;
	}
	public String getExplanOther()
	{
		return ExplanOther;
	}
	public void setExplanOther(String aExplanOther)
	{
            ExplanOther = aExplanOther;
	}
	public String getMedicaItemCode()
	{
		return MedicaItemCode;
	}
	public void setMedicaItemCode(String aMedicaItemCode)
	{
            MedicaItemCode = aMedicaItemCode;
	}

	/**
	* 使用另外一个 LHContServPriceSchema 对象给 Schema 赋值
	* @param: aLHContServPriceSchema LHContServPriceSchema
	**/
	public void setSchema(LHContServPriceSchema aLHContServPriceSchema)
	{
		this.ContraNo = aLHContServPriceSchema.getContraNo();
		this.ContraName = aLHContServPriceSchema.getContraName();
		this.ContraType = aLHContServPriceSchema.getContraType();
		this.ContraItemNo = aLHContServPriceSchema.getContraItemNo();
		this.ContraObj = aLHContServPriceSchema.getContraObj();
		this.DutyItemCode = aLHContServPriceSchema.getDutyItemCode();
		this.ServPriceType = aLHContServPriceSchema.getServPriceType();
		this.ExpType = aLHContServPriceSchema.getExpType();
		this.ManageCom = aLHContServPriceSchema.getManageCom();
		this.Operator = aLHContServPriceSchema.getOperator();
		this.MakeDate = fDate.getDate( aLHContServPriceSchema.getMakeDate());
		this.MakeTime = aLHContServPriceSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLHContServPriceSchema.getModifyDate());
		this.ModifyTime = aLHContServPriceSchema.getModifyTime();
		this.MoneySum = aLHContServPriceSchema.getMoneySum();
		this.ServPros = aLHContServPriceSchema.getServPros();
		this.MaxServNum = aLHContServPriceSchema.getMaxServNum();
		this.ExplanOther = aLHContServPriceSchema.getExplanOther();
		this.MedicaItemCode = aLHContServPriceSchema.getMedicaItemCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ContraNo") == null )
				this.ContraNo = null;
			else
				this.ContraNo = rs.getString("ContraNo").trim();

			if( rs.getString("ContraName") == null )
				this.ContraName = null;
			else
				this.ContraName = rs.getString("ContraName").trim();

			if( rs.getString("ContraType") == null )
				this.ContraType = null;
			else
				this.ContraType = rs.getString("ContraType").trim();

			if( rs.getString("ContraItemNo") == null )
				this.ContraItemNo = null;
			else
				this.ContraItemNo = rs.getString("ContraItemNo").trim();

			if( rs.getString("ContraObj") == null )
				this.ContraObj = null;
			else
				this.ContraObj = rs.getString("ContraObj").trim();

			if( rs.getString("DutyItemCode") == null )
				this.DutyItemCode = null;
			else
				this.DutyItemCode = rs.getString("DutyItemCode").trim();

			if( rs.getString("ServPriceType") == null )
				this.ServPriceType = null;
			else
				this.ServPriceType = rs.getString("ServPriceType").trim();

			if( rs.getString("ExpType") == null )
				this.ExpType = null;
			else
				this.ExpType = rs.getString("ExpType").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.MoneySum = rs.getDouble("MoneySum");
			if( rs.getString("ServPros") == null )
				this.ServPros = null;
			else
				this.ServPros = rs.getString("ServPros").trim();

			if( rs.getString("MaxServNum") == null )
				this.MaxServNum = null;
			else
				this.MaxServNum = rs.getString("MaxServNum").trim();

			if( rs.getString("ExplanOther") == null )
				this.ExplanOther = null;
			else
				this.ExplanOther = rs.getString("ExplanOther").trim();

			if( rs.getString("MedicaItemCode") == null )
				this.MedicaItemCode = null;
			else
				this.MedicaItemCode = rs.getString("MedicaItemCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LHContServPrice表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHContServPriceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LHContServPriceSchema getSchema()
	{
		LHContServPriceSchema aLHContServPriceSchema = new LHContServPriceSchema();
		aLHContServPriceSchema.setSchema(this);
		return aLHContServPriceSchema;
	}

	public LHContServPriceDB getDB()
	{
		LHContServPriceDB aDBOper = new LHContServPriceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHContServPrice描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ContraNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraItemNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraObj)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DutyItemCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ServPriceType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ExpType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MoneySum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ServPros)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MaxServNum)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ExplanOther)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MedicaItemCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHContServPrice>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ContraNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ContraName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ContraType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ContraItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContraObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			DutyItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ServPriceType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ExpType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MoneySum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			ServPros = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MaxServNum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ExplanOther = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			MedicaItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHContServPriceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ContraNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraNo));
		}
		if (FCode.equals("ContraName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraName));
		}
		if (FCode.equals("ContraType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraType));
		}
		if (FCode.equals("ContraItemNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraItemNo));
		}
		if (FCode.equals("ContraObj"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraObj));
		}
		if (FCode.equals("DutyItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyItemCode));
		}
		if (FCode.equals("ServPriceType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServPriceType));
		}
		if (FCode.equals("ExpType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExpType));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("MoneySum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MoneySum));
		}
		if (FCode.equals("ServPros"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServPros));
		}
		if (FCode.equals("MaxServNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxServNum));
		}
		if (FCode.equals("ExplanOther"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExplanOther));
		}
		if (FCode.equals("MedicaItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ContraNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ContraName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ContraType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ContraItemNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ContraObj);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(DutyItemCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ServPriceType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ExpType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 14:
				strFieldValue = String.valueOf(MoneySum);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ServPros);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(MaxServNum);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ExplanOther);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MedicaItemCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ContraNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraNo = FValue.trim();
			}
			else
				ContraNo = null;
		}
		if (FCode.equalsIgnoreCase("ContraName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraName = FValue.trim();
			}
			else
				ContraName = null;
		}
		if (FCode.equalsIgnoreCase("ContraType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraType = FValue.trim();
			}
			else
				ContraType = null;
		}
		if (FCode.equalsIgnoreCase("ContraItemNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraItemNo = FValue.trim();
			}
			else
				ContraItemNo = null;
		}
		if (FCode.equalsIgnoreCase("ContraObj"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraObj = FValue.trim();
			}
			else
				ContraObj = null;
		}
		if (FCode.equalsIgnoreCase("DutyItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyItemCode = FValue.trim();
			}
			else
				DutyItemCode = null;
		}
		if (FCode.equalsIgnoreCase("ServPriceType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServPriceType = FValue.trim();
			}
			else
				ServPriceType = null;
		}
		if (FCode.equalsIgnoreCase("ExpType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExpType = FValue.trim();
			}
			else
				ExpType = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("MoneySum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MoneySum = d;
			}
		}
		if (FCode.equalsIgnoreCase("ServPros"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServPros = FValue.trim();
			}
			else
				ServPros = null;
		}
		if (FCode.equalsIgnoreCase("MaxServNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaxServNum = FValue.trim();
			}
			else
				MaxServNum = null;
		}
		if (FCode.equalsIgnoreCase("ExplanOther"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExplanOther = FValue.trim();
			}
			else
				ExplanOther = null;
		}
		if (FCode.equalsIgnoreCase("MedicaItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicaItemCode = FValue.trim();
			}
			else
				MedicaItemCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LHContServPriceSchema other = (LHContServPriceSchema)otherObject;
		return
			ContraNo.equals(other.getContraNo())
			&& ContraName.equals(other.getContraName())
			&& ContraType.equals(other.getContraType())
			&& ContraItemNo.equals(other.getContraItemNo())
			&& ContraObj.equals(other.getContraObj())
			&& DutyItemCode.equals(other.getDutyItemCode())
			&& ServPriceType.equals(other.getServPriceType())
			&& ExpType.equals(other.getExpType())
			&& ManageCom.equals(other.getManageCom())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& MoneySum == other.getMoneySum()
			&& ServPros.equals(other.getServPros())
			&& MaxServNum.equals(other.getMaxServNum())
			&& ExplanOther.equals(other.getExplanOther())
			&& MedicaItemCode.equals(other.getMedicaItemCode());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ContraNo") ) {
			return 0;
		}
		if( strFieldName.equals("ContraName") ) {
			return 1;
		}
		if( strFieldName.equals("ContraType") ) {
			return 2;
		}
		if( strFieldName.equals("ContraItemNo") ) {
			return 3;
		}
		if( strFieldName.equals("ContraObj") ) {
			return 4;
		}
		if( strFieldName.equals("DutyItemCode") ) {
			return 5;
		}
		if( strFieldName.equals("ServPriceType") ) {
			return 6;
		}
		if( strFieldName.equals("ExpType") ) {
			return 7;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 8;
		}
		if( strFieldName.equals("Operator") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 13;
		}
		if( strFieldName.equals("MoneySum") ) {
			return 14;
		}
		if( strFieldName.equals("ServPros") ) {
			return 15;
		}
		if( strFieldName.equals("MaxServNum") ) {
			return 16;
		}
		if( strFieldName.equals("ExplanOther") ) {
			return 17;
		}
		if( strFieldName.equals("MedicaItemCode") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ContraNo";
				break;
			case 1:
				strFieldName = "ContraName";
				break;
			case 2:
				strFieldName = "ContraType";
				break;
			case 3:
				strFieldName = "ContraItemNo";
				break;
			case 4:
				strFieldName = "ContraObj";
				break;
			case 5:
				strFieldName = "DutyItemCode";
				break;
			case 6:
				strFieldName = "ServPriceType";
				break;
			case 7:
				strFieldName = "ExpType";
				break;
			case 8:
				strFieldName = "ManageCom";
				break;
			case 9:
				strFieldName = "Operator";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "ModifyDate";
				break;
			case 13:
				strFieldName = "ModifyTime";
				break;
			case 14:
				strFieldName = "MoneySum";
				break;
			case 15:
				strFieldName = "ServPros";
				break;
			case 16:
				strFieldName = "MaxServNum";
				break;
			case 17:
				strFieldName = "ExplanOther";
				break;
			case 18:
				strFieldName = "MedicaItemCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ContraNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraItemNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraObj") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyItemCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServPriceType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExpType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MoneySum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ServPros") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MaxServNum") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExplanOther") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MedicaItemCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
