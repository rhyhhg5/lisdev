/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKCustomerStuDB;

/*
 * <p>ClassName: LKCustomerStuSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-10-19
 */
public class LKCustomerStuSchema implements Schema, Cloneable
{
	// @Field
	/** 主键 */
	private int cid;
	/** 银行代码 */
	private String bankcode;
	/** 机构编码 */
	private String managecom;
	/** 启用标识 */
	private String cflag;
	/** 渠道标识 */
	private String channel;
	/** 创建时间 */
	private String makedate;
	/** 修改时间 */
	private String modifydate;
	/** 操作用户 */
	private String makeuser;
	/** 备用1 */
	private String bak1;
	/** 备用2 */
	private String bak2;
	/** 备用3 */
	private String bak3;
	/** 备用4 */
	private String bak4;
	/** 备用5 */
	private String bak5;

	public static final int FIELDNUM = 13;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKCustomerStuSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "cid";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LKCustomerStuSchema cloned = (LKCustomerStuSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public int getcid()
	{
		return cid;
	}
	public void setcid(int acid)
	{
		cid = acid;
	}
	public void setcid(String acid)
	{
		if (acid != null && !acid.equals(""))
		{
			Integer tInteger = new Integer(acid);
			int i = tInteger.intValue();
			cid = i;
		}
	}

	public String getbankcode()
	{
		return bankcode;
	}
	public void setbankcode(String abankcode)
	{
		bankcode = abankcode;
	}
	public String getmanagecom()
	{
		return managecom;
	}
	public void setmanagecom(String amanagecom)
	{
		managecom = amanagecom;
	}
	public String getcflag()
	{
		return cflag;
	}
	public void setcflag(String acflag)
	{
		cflag = acflag;
	}
	public String getchannel()
	{
		return channel;
	}
	public void setchannel(String achannel)
	{
		channel = achannel;
	}
	public String getmakedate()
	{
		return makedate;
	}
	public void setmakedate(String amakedate)
	{
		makedate = amakedate;
	}
	public String getmodifydate()
	{
		return modifydate;
	}
	public void setmodifydate(String amodifydate)
	{
		modifydate = amodifydate;
	}
	public String getmakeuser()
	{
		return makeuser;
	}
	public void setmakeuser(String amakeuser)
	{
		makeuser = amakeuser;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}
	public String getbak4()
	{
		return bak4;
	}
	public void setbak4(String abak4)
	{
		bak4 = abak4;
	}
	public String getbak5()
	{
		return bak5;
	}
	public void setbak5(String abak5)
	{
		bak5 = abak5;
	}

	/**
	* 使用另外一个 LKCustomerStuSchema 对象给 Schema 赋值
	* @param: aLKCustomerStuSchema LKCustomerStuSchema
	**/
	public void setSchema(LKCustomerStuSchema aLKCustomerStuSchema)
	{
		this.cid = aLKCustomerStuSchema.getcid();
		this.bankcode = aLKCustomerStuSchema.getbankcode();
		this.managecom = aLKCustomerStuSchema.getmanagecom();
		this.cflag = aLKCustomerStuSchema.getcflag();
		this.channel = aLKCustomerStuSchema.getchannel();
		this.makedate = aLKCustomerStuSchema.getmakedate();
		this.modifydate = aLKCustomerStuSchema.getmodifydate();
		this.makeuser = aLKCustomerStuSchema.getmakeuser();
		this.bak1 = aLKCustomerStuSchema.getbak1();
		this.bak2 = aLKCustomerStuSchema.getbak2();
		this.bak3 = aLKCustomerStuSchema.getbak3();
		this.bak4 = aLKCustomerStuSchema.getbak4();
		this.bak5 = aLKCustomerStuSchema.getbak5();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.cid = rs.getInt("cid");
			if( rs.getString("bankcode") == null )
				this.bankcode = null;
			else
				this.bankcode = rs.getString("bankcode").trim();

			if( rs.getString("managecom") == null )
				this.managecom = null;
			else
				this.managecom = rs.getString("managecom").trim();

			if( rs.getString("cflag") == null )
				this.cflag = null;
			else
				this.cflag = rs.getString("cflag").trim();

			if( rs.getString("channel") == null )
				this.channel = null;
			else
				this.channel = rs.getString("channel").trim();

			if( rs.getString("makedate") == null )
				this.makedate = null;
			else
				this.makedate = rs.getString("makedate").trim();

			if( rs.getString("modifydate") == null )
				this.modifydate = null;
			else
				this.modifydate = rs.getString("modifydate").trim();

			if( rs.getString("makeuser") == null )
				this.makeuser = null;
			else
				this.makeuser = rs.getString("makeuser").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

			if( rs.getString("bak4") == null )
				this.bak4 = null;
			else
				this.bak4 = rs.getString("bak4").trim();

			if( rs.getString("bak5") == null )
				this.bak5 = null;
			else
				this.bak5 = rs.getString("bak5").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKCustomerStu表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKCustomerStuSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKCustomerStuSchema getSchema()
	{
		LKCustomerStuSchema aLKCustomerStuSchema = new LKCustomerStuSchema();
		aLKCustomerStuSchema.setSchema(this);
		return aLKCustomerStuSchema;
	}

	public LKCustomerStuDB getDB()
	{
		LKCustomerStuDB aDBOper = new LKCustomerStuDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKCustomerStu描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append( ChgData.chgData(cid));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cflag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(channel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makedate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifydate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makeuser)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak5));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKCustomerStu>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			cid= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).intValue();
			bankcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			cflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			channel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			makedate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			modifydate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			makeuser = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKCustomerStuSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("cid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cid));
		}
		if (FCode.equals("bankcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankcode));
		}
		if (FCode.equals("managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(managecom));
		}
		if (FCode.equals("cflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cflag));
		}
		if (FCode.equals("channel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(channel));
		}
		if (FCode.equals("makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makedate));
		}
		if (FCode.equals("modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifydate));
		}
		if (FCode.equals("makeuser"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makeuser));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (FCode.equals("bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
		}
		if (FCode.equals("bak5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak5));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(cid);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(bankcode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(managecom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(cflag);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(channel);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(makedate);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(modifydate);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(makeuser);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(bak4);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(bak5);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("cid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				cid = i;
			}
		}
		if (FCode.equalsIgnoreCase("bankcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankcode = FValue.trim();
			}
			else
				bankcode = null;
		}
		if (FCode.equalsIgnoreCase("managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				managecom = FValue.trim();
			}
			else
				managecom = null;
		}
		if (FCode.equalsIgnoreCase("cflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cflag = FValue.trim();
			}
			else
				cflag = null;
		}
		if (FCode.equalsIgnoreCase("channel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				channel = FValue.trim();
			}
			else
				channel = null;
		}
		if (FCode.equalsIgnoreCase("makedate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makedate = FValue.trim();
			}
			else
				makedate = null;
		}
		if (FCode.equalsIgnoreCase("modifydate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifydate = FValue.trim();
			}
			else
				modifydate = null;
		}
		if (FCode.equalsIgnoreCase("makeuser"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makeuser = FValue.trim();
			}
			else
				makeuser = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		if (FCode.equalsIgnoreCase("bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak4 = FValue.trim();
			}
			else
				bak4 = null;
		}
		if (FCode.equalsIgnoreCase("bak5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak5 = FValue.trim();
			}
			else
				bak5 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKCustomerStuSchema other = (LKCustomerStuSchema)otherObject;
		return
			cid == other.getcid()
			&& (bankcode == null ? other.getbankcode() == null : bankcode.equals(other.getbankcode()))
			&& (managecom == null ? other.getmanagecom() == null : managecom.equals(other.getmanagecom()))
			&& (cflag == null ? other.getcflag() == null : cflag.equals(other.getcflag()))
			&& (channel == null ? other.getchannel() == null : channel.equals(other.getchannel()))
			&& (makedate == null ? other.getmakedate() == null : makedate.equals(other.getmakedate()))
			&& (modifydate == null ? other.getmodifydate() == null : modifydate.equals(other.getmodifydate()))
			&& (makeuser == null ? other.getmakeuser() == null : makeuser.equals(other.getmakeuser()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()))
			&& (bak4 == null ? other.getbak4() == null : bak4.equals(other.getbak4()))
			&& (bak5 == null ? other.getbak5() == null : bak5.equals(other.getbak5()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("cid") ) {
			return 0;
		}
		if( strFieldName.equals("bankcode") ) {
			return 1;
		}
		if( strFieldName.equals("managecom") ) {
			return 2;
		}
		if( strFieldName.equals("cflag") ) {
			return 3;
		}
		if( strFieldName.equals("channel") ) {
			return 4;
		}
		if( strFieldName.equals("makedate") ) {
			return 5;
		}
		if( strFieldName.equals("modifydate") ) {
			return 6;
		}
		if( strFieldName.equals("makeuser") ) {
			return 7;
		}
		if( strFieldName.equals("bak1") ) {
			return 8;
		}
		if( strFieldName.equals("bak2") ) {
			return 9;
		}
		if( strFieldName.equals("bak3") ) {
			return 10;
		}
		if( strFieldName.equals("bak4") ) {
			return 11;
		}
		if( strFieldName.equals("bak5") ) {
			return 12;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "cid";
				break;
			case 1:
				strFieldName = "bankcode";
				break;
			case 2:
				strFieldName = "managecom";
				break;
			case 3:
				strFieldName = "cflag";
				break;
			case 4:
				strFieldName = "channel";
				break;
			case 5:
				strFieldName = "makedate";
				break;
			case 6:
				strFieldName = "modifydate";
				break;
			case 7:
				strFieldName = "makeuser";
				break;
			case 8:
				strFieldName = "bak1";
				break;
			case 9:
				strFieldName = "bak2";
				break;
			case 10:
				strFieldName = "bak3";
				break;
			case 11:
				strFieldName = "bak4";
				break;
			case 12:
				strFieldName = "bak5";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("cid") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("bankcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cflag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("channel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makedate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifydate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makeuser") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak5") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_INT;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
