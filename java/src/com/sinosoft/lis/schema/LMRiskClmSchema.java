/*
 * <p>ClassName: LMRiskClmSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMRiskClmDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMRiskClmSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 险种名称 */
    private String RiskName;
    /** 调查标记 */
    private String SurveyFlag;
    /** 调查开始位置 */
    private String SurveyStartFlag;
    /** 控制赔付比例标记 */
    private String ClmRateCtlFlag;
    /** 退预缴保费标记 */
    private String PrePremFlag;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMRiskClmSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getRiskName()
    {
        if (RiskName != null && !RiskName.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskName = StrTool.unicodeToGBK(RiskName);
        }
        return RiskName;
    }

    public void setRiskName(String aRiskName)
    {
        RiskName = aRiskName;
    }

    public String getSurveyFlag()
    {
        if (SurveyFlag != null && !SurveyFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SurveyFlag = StrTool.unicodeToGBK(SurveyFlag);
        }
        return SurveyFlag;
    }

    public void setSurveyFlag(String aSurveyFlag)
    {
        SurveyFlag = aSurveyFlag;
    }

    public String getSurveyStartFlag()
    {
        if (SurveyStartFlag != null && !SurveyStartFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SurveyStartFlag = StrTool.unicodeToGBK(SurveyStartFlag);
        }
        return SurveyStartFlag;
    }

    public void setSurveyStartFlag(String aSurveyStartFlag)
    {
        SurveyStartFlag = aSurveyStartFlag;
    }

    public String getClmRateCtlFlag()
    {
        if (ClmRateCtlFlag != null && !ClmRateCtlFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmRateCtlFlag = StrTool.unicodeToGBK(ClmRateCtlFlag);
        }
        return ClmRateCtlFlag;
    }

    public void setClmRateCtlFlag(String aClmRateCtlFlag)
    {
        ClmRateCtlFlag = aClmRateCtlFlag;
    }

    public String getPrePremFlag()
    {
        if (PrePremFlag != null && !PrePremFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PrePremFlag = StrTool.unicodeToGBK(PrePremFlag);
        }
        return PrePremFlag;
    }

    public void setPrePremFlag(String aPrePremFlag)
    {
        PrePremFlag = aPrePremFlag;
    }

    /**
     * 使用另外一个 LMRiskClmSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMRiskClmSchema aLMRiskClmSchema)
    {
        this.RiskCode = aLMRiskClmSchema.getRiskCode();
        this.RiskVer = aLMRiskClmSchema.getRiskVer();
        this.RiskName = aLMRiskClmSchema.getRiskName();
        this.SurveyFlag = aLMRiskClmSchema.getSurveyFlag();
        this.SurveyStartFlag = aLMRiskClmSchema.getSurveyStartFlag();
        this.ClmRateCtlFlag = aLMRiskClmSchema.getClmRateCtlFlag();
        this.PrePremFlag = aLMRiskClmSchema.getPrePremFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("RiskName") == null)
            {
                this.RiskName = null;
            }
            else
            {
                this.RiskName = rs.getString("RiskName").trim();
            }

            if (rs.getString("SurveyFlag") == null)
            {
                this.SurveyFlag = null;
            }
            else
            {
                this.SurveyFlag = rs.getString("SurveyFlag").trim();
            }

            if (rs.getString("SurveyStartFlag") == null)
            {
                this.SurveyStartFlag = null;
            }
            else
            {
                this.SurveyStartFlag = rs.getString("SurveyStartFlag").trim();
            }

            if (rs.getString("ClmRateCtlFlag") == null)
            {
                this.ClmRateCtlFlag = null;
            }
            else
            {
                this.ClmRateCtlFlag = rs.getString("ClmRateCtlFlag").trim();
            }

            if (rs.getString("PrePremFlag") == null)
            {
                this.PrePremFlag = null;
            }
            else
            {
                this.PrePremFlag = rs.getString("PrePremFlag").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskClmSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMRiskClmSchema getSchema()
    {
        LMRiskClmSchema aLMRiskClmSchema = new LMRiskClmSchema();
        aLMRiskClmSchema.setSchema(this);
        return aLMRiskClmSchema;
    }

    public LMRiskClmDB getDB()
    {
        LMRiskClmDB aDBOper = new LMRiskClmDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskClm描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SurveyFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SurveyStartFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmRateCtlFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrePremFlag));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskClm>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            SurveyStartFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             5, SysConst.PACKAGESPILTER);
            ClmRateCtlFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                            SysConst.PACKAGESPILTER);
            PrePremFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskClmSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("RiskName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskName));
        }
        if (FCode.equals("SurveyFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SurveyFlag));
        }
        if (FCode.equals("SurveyStartFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SurveyStartFlag));
        }
        if (FCode.equals("ClmRateCtlFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmRateCtlFlag));
        }
        if (FCode.equals("PrePremFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrePremFlag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SurveyStartFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ClmRateCtlFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PrePremFlag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("RiskName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
            {
                RiskName = null;
            }
        }
        if (FCode.equals("SurveyFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyFlag = FValue.trim();
            }
            else
            {
                SurveyFlag = null;
            }
        }
        if (FCode.equals("SurveyStartFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyStartFlag = FValue.trim();
            }
            else
            {
                SurveyStartFlag = null;
            }
        }
        if (FCode.equals("ClmRateCtlFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmRateCtlFlag = FValue.trim();
            }
            else
            {
                ClmRateCtlFlag = null;
            }
        }
        if (FCode.equals("PrePremFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrePremFlag = FValue.trim();
            }
            else
            {
                PrePremFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMRiskClmSchema other = (LMRiskClmSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && RiskName.equals(other.getRiskName())
                && SurveyFlag.equals(other.getSurveyFlag())
                && SurveyStartFlag.equals(other.getSurveyStartFlag())
                && ClmRateCtlFlag.equals(other.getClmRateCtlFlag())
                && PrePremFlag.equals(other.getPrePremFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskName"))
        {
            return 2;
        }
        if (strFieldName.equals("SurveyFlag"))
        {
            return 3;
        }
        if (strFieldName.equals("SurveyStartFlag"))
        {
            return 4;
        }
        if (strFieldName.equals("ClmRateCtlFlag"))
        {
            return 5;
        }
        if (strFieldName.equals("PrePremFlag"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "SurveyFlag";
                break;
            case 4:
                strFieldName = "SurveyStartFlag";
                break;
            case 5:
                strFieldName = "ClmRateCtlFlag";
                break;
            case 6:
                strFieldName = "PrePremFlag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyStartFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmRateCtlFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrePremFlag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
