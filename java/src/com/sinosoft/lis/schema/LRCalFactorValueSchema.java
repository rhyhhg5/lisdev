/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRCalFactorValueDB;

/*
 * <p>ClassName: LRCalFactorValueSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-11-28
 */
public class LRCalFactorValueSchema implements Schema, Cloneable {
    // @Field
    /** 合同编号 */
    private String ReContCode;
    /** 险种分类 */
    private String RiskSort;
    /** 险种编码 */
    private String RiskCode;
    /** 要素代码 */
    private String FactorCode;
    /** 要素名称 */
    private String FactorName;
    /** 要素值 */
    private String FactorValue;
    /** 数值类型 */
    private String ValueType;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 操作人 */
    private String Operator;
    /** 管理机构 */
    private String ManageCom;
    /** 再保公司 */
    private String ReComCode;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LRCalFactorValueSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "ReContCode";
        pk[1] = "RiskSort";
        pk[2] = "RiskCode";
        pk[3] = "FactorCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LRCalFactorValueSchema cloned = (LRCalFactorValueSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getReContCode() {
        return ReContCode;
    }

    public void setReContCode(String aReContCode) {
        ReContCode = aReContCode;
    }

    public String getRiskSort() {
        return RiskSort;
    }

    public void setRiskSort(String aRiskSort) {
        RiskSort = aRiskSort;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getFactorCode() {
        return FactorCode;
    }

    public void setFactorCode(String aFactorCode) {
        FactorCode = aFactorCode;
    }

    public String getFactorName() {
        return FactorName;
    }

    public void setFactorName(String aFactorName) {
        FactorName = aFactorName;
    }

    public String getFactorValue() {
        return FactorValue;
    }

    public void setFactorValue(String aFactorValue) {
        FactorValue = aFactorValue;
    }

    public String getValueType() {
        return ValueType;
    }

    public void setValueType(String aValueType) {
        ValueType = aValueType;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getReComCode() {
        return ReComCode;
    }

    public void setReComCode(String aReComCode) {
        ReComCode = aReComCode;
    }

    /**
     * 使用另外一个 LRCalFactorValueSchema 对象给 Schema 赋值
     * @param: aLRCalFactorValueSchema LRCalFactorValueSchema
     **/
    public void setSchema(LRCalFactorValueSchema aLRCalFactorValueSchema) {
        this.ReContCode = aLRCalFactorValueSchema.getReContCode();
        this.RiskSort = aLRCalFactorValueSchema.getRiskSort();
        this.RiskCode = aLRCalFactorValueSchema.getRiskCode();
        this.FactorCode = aLRCalFactorValueSchema.getFactorCode();
        this.FactorName = aLRCalFactorValueSchema.getFactorName();
        this.FactorValue = aLRCalFactorValueSchema.getFactorValue();
        this.ValueType = aLRCalFactorValueSchema.getValueType();
        this.MakeDate = fDate.getDate(aLRCalFactorValueSchema.getMakeDate());
        this.MakeTime = aLRCalFactorValueSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLRCalFactorValueSchema.getModifyDate());
        this.ModifyTime = aLRCalFactorValueSchema.getModifyTime();
        this.Operator = aLRCalFactorValueSchema.getOperator();
        this.ManageCom = aLRCalFactorValueSchema.getManageCom();
        this.ReComCode = aLRCalFactorValueSchema.getReComCode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ReContCode") == null) {
                this.ReContCode = null;
            } else {
                this.ReContCode = rs.getString("ReContCode").trim();
            }

            if (rs.getString("RiskSort") == null) {
                this.RiskSort = null;
            } else {
                this.RiskSort = rs.getString("RiskSort").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("FactorCode") == null) {
                this.FactorCode = null;
            } else {
                this.FactorCode = rs.getString("FactorCode").trim();
            }

            if (rs.getString("FactorName") == null) {
                this.FactorName = null;
            } else {
                this.FactorName = rs.getString("FactorName").trim();
            }

            if (rs.getString("FactorValue") == null) {
                this.FactorValue = null;
            } else {
                this.FactorValue = rs.getString("FactorValue").trim();
            }

            if (rs.getString("ValueType") == null) {
                this.ValueType = null;
            } else {
                this.ValueType = rs.getString("ValueType").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("ReComCode") == null) {
                this.ReComCode = null;
            } else {
                this.ReComCode = rs.getString("ReComCode").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LRCalFactorValue表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRCalFactorValueSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LRCalFactorValueSchema getSchema() {
        LRCalFactorValueSchema aLRCalFactorValueSchema = new
                LRCalFactorValueSchema();
        aLRCalFactorValueSchema.setSchema(this);
        return aLRCalFactorValueSchema;
    }

    public LRCalFactorValueDB getDB() {
        LRCalFactorValueDB aDBOper = new LRCalFactorValueDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRCalFactorValue描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ReContCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskSort));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FactorCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FactorName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FactorValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ValueType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReComCode));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRCalFactorValue>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            RiskSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            FactorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            FactorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            FactorValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            ValueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
            ReComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRCalFactorValueSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ReContCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
        }
        if (FCode.equals("RiskSort")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskSort));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("FactorCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorCode));
        }
        if (FCode.equals("FactorName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorName));
        }
        if (FCode.equals("FactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorValue));
        }
        if (FCode.equals("ValueType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValueType));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("ReComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReComCode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ReContCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RiskSort);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(FactorCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(FactorName);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(FactorValue);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ValueType);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ReComCode);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ReContCode")) {
            if (FValue != null && !FValue.equals("")) {
                ReContCode = FValue.trim();
            } else {
                ReContCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskSort")) {
            if (FValue != null && !FValue.equals("")) {
                RiskSort = FValue.trim();
            } else {
                RiskSort = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("FactorCode")) {
            if (FValue != null && !FValue.equals("")) {
                FactorCode = FValue.trim();
            } else {
                FactorCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("FactorName")) {
            if (FValue != null && !FValue.equals("")) {
                FactorName = FValue.trim();
            } else {
                FactorName = null;
            }
        }
        if (FCode.equalsIgnoreCase("FactorValue")) {
            if (FValue != null && !FValue.equals("")) {
                FactorValue = FValue.trim();
            } else {
                FactorValue = null;
            }
        }
        if (FCode.equalsIgnoreCase("ValueType")) {
            if (FValue != null && !FValue.equals("")) {
                ValueType = FValue.trim();
            } else {
                ValueType = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReComCode")) {
            if (FValue != null && !FValue.equals("")) {
                ReComCode = FValue.trim();
            } else {
                ReComCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LRCalFactorValueSchema other = (LRCalFactorValueSchema) otherObject;
        return
                ReContCode.equals(other.getReContCode())
                && RiskSort.equals(other.getRiskSort())
                && RiskCode.equals(other.getRiskCode())
                && FactorCode.equals(other.getFactorCode())
                && FactorName.equals(other.getFactorName())
                && FactorValue.equals(other.getFactorValue())
                && ValueType.equals(other.getValueType())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Operator.equals(other.getOperator())
                && ManageCom.equals(other.getManageCom())
                && ReComCode.equals(other.getReComCode());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ReContCode")) {
            return 0;
        }
        if (strFieldName.equals("RiskSort")) {
            return 1;
        }
        if (strFieldName.equals("RiskCode")) {
            return 2;
        }
        if (strFieldName.equals("FactorCode")) {
            return 3;
        }
        if (strFieldName.equals("FactorName")) {
            return 4;
        }
        if (strFieldName.equals("FactorValue")) {
            return 5;
        }
        if (strFieldName.equals("ValueType")) {
            return 6;
        }
        if (strFieldName.equals("MakeDate")) {
            return 7;
        }
        if (strFieldName.equals("MakeTime")) {
            return 8;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 9;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 10;
        }
        if (strFieldName.equals("Operator")) {
            return 11;
        }
        if (strFieldName.equals("ManageCom")) {
            return 12;
        }
        if (strFieldName.equals("ReComCode")) {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ReContCode";
            break;
        case 1:
            strFieldName = "RiskSort";
            break;
        case 2:
            strFieldName = "RiskCode";
            break;
        case 3:
            strFieldName = "FactorCode";
            break;
        case 4:
            strFieldName = "FactorName";
            break;
        case 5:
            strFieldName = "FactorValue";
            break;
        case 6:
            strFieldName = "ValueType";
            break;
        case 7:
            strFieldName = "MakeDate";
            break;
        case 8:
            strFieldName = "MakeTime";
            break;
        case 9:
            strFieldName = "ModifyDate";
            break;
        case 10:
            strFieldName = "ModifyTime";
            break;
        case 11:
            strFieldName = "Operator";
            break;
        case 12:
            strFieldName = "ManageCom";
            break;
        case 13:
            strFieldName = "ReComCode";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ReContCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskSort")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorValue")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ValueType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReComCode")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
