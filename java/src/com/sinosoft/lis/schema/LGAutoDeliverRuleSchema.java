/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LGAutoDeliverRuleDB;

/*
 * <p>ClassName: LGAutoDeliverRuleSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-07
 */
public class LGAutoDeliverRuleSchema implements Schema, Cloneable {
    // @Field
    /** 规则编号 */
    private String RuleNo;
    /** 规则内容 */
    private String RuleContent;
    /** 规则描述 */
    private String RuleDescription;
    /** 转到目标 */
    private String Target;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 默认使用 */
    private String DefaultFlag;
    /** 目标类型 */
    private String GoalType;
    /** 来源机构 */
    private String SourceComCode;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LGAutoDeliverRuleSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "RuleNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LGAutoDeliverRuleSchema cloned = (LGAutoDeliverRuleSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRuleNo() {
        return RuleNo;
    }

    public void setRuleNo(String aRuleNo) {
        RuleNo = aRuleNo;
    }

    public String getRuleContent() {
        return RuleContent;
    }

    public void setRuleContent(String aRuleContent) {
        RuleContent = aRuleContent;
    }

    public String getRuleDescription() {
        return RuleDescription;
    }

    public void setRuleDescription(String aRuleDescription) {
        RuleDescription = aRuleDescription;
    }

    public String getTarget() {
        return Target;
    }

    public void setTarget(String aTarget) {
        Target = aTarget;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getDefaultFlag() {
        return DefaultFlag;
    }

    public void setDefaultFlag(String aDefaultFlag) {
        DefaultFlag = aDefaultFlag;
    }

    public String getGoalType() {
        return GoalType;
    }

    public void setGoalType(String aGoalType) {
        GoalType = aGoalType;
    }

    public String getSourceComCode() {
        return SourceComCode;
    }

    public void setSourceComCode(String aSourceComCode) {
        SourceComCode = aSourceComCode;
    }

    /**
     * 使用另外一个 LGAutoDeliverRuleSchema 对象给 Schema 赋值
     * @param: aLGAutoDeliverRuleSchema LGAutoDeliverRuleSchema
     **/
    public void setSchema(LGAutoDeliverRuleSchema aLGAutoDeliverRuleSchema) {
        this.RuleNo = aLGAutoDeliverRuleSchema.getRuleNo();
        this.RuleContent = aLGAutoDeliverRuleSchema.getRuleContent();
        this.RuleDescription = aLGAutoDeliverRuleSchema.getRuleDescription();
        this.Target = aLGAutoDeliverRuleSchema.getTarget();
        this.Operator = aLGAutoDeliverRuleSchema.getOperator();
        this.MakeDate = fDate.getDate(aLGAutoDeliverRuleSchema.getMakeDate());
        this.MakeTime = aLGAutoDeliverRuleSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLGAutoDeliverRuleSchema.getModifyDate());
        this.ModifyTime = aLGAutoDeliverRuleSchema.getModifyTime();
        this.DefaultFlag = aLGAutoDeliverRuleSchema.getDefaultFlag();
        this.GoalType = aLGAutoDeliverRuleSchema.getGoalType();
        this.SourceComCode = aLGAutoDeliverRuleSchema.getSourceComCode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RuleNo") == null) {
                this.RuleNo = null;
            } else {
                this.RuleNo = rs.getString("RuleNo").trim();
            }

            if (rs.getString("RuleContent") == null) {
                this.RuleContent = null;
            } else {
                this.RuleContent = rs.getString("RuleContent").trim();
            }

            if (rs.getString("RuleDescription") == null) {
                this.RuleDescription = null;
            } else {
                this.RuleDescription = rs.getString("RuleDescription").trim();
            }

            if (rs.getString("Target") == null) {
                this.Target = null;
            } else {
                this.Target = rs.getString("Target").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("DefaultFlag") == null) {
                this.DefaultFlag = null;
            } else {
                this.DefaultFlag = rs.getString("DefaultFlag").trim();
            }

            if (rs.getString("GoalType") == null) {
                this.GoalType = null;
            } else {
                this.GoalType = rs.getString("GoalType").trim();
            }

            if (rs.getString("SourceComCode") == null) {
                this.SourceComCode = null;
            } else {
                this.SourceComCode = rs.getString("SourceComCode").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LGAutoDeliverRule表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGAutoDeliverRuleSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LGAutoDeliverRuleSchema getSchema() {
        LGAutoDeliverRuleSchema aLGAutoDeliverRuleSchema = new
                LGAutoDeliverRuleSchema();
        aLGAutoDeliverRuleSchema.setSchema(this);
        return aLGAutoDeliverRuleSchema;
    }

    public LGAutoDeliverRuleDB getDB() {
        LGAutoDeliverRuleDB aDBOper = new LGAutoDeliverRuleDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGAutoDeliverRule描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RuleNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RuleContent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RuleDescription));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Target));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DefaultFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GoalType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SourceComCode));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGAutoDeliverRule>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            RuleNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            RuleContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            RuleDescription = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             3, SysConst.PACKAGESPILTER);
            Target = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            DefaultFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            GoalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            SourceComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                           SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGAutoDeliverRuleSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("RuleNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RuleNo));
        }
        if (FCode.equals("RuleContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RuleContent));
        }
        if (FCode.equals("RuleDescription")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RuleDescription));
        }
        if (FCode.equals("Target")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Target));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("DefaultFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultFlag));
        }
        if (FCode.equals("GoalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GoalType));
        }
        if (FCode.equals("SourceComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SourceComCode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(RuleNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RuleContent);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(RuleDescription);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Target);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(DefaultFlag);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(GoalType);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(SourceComCode);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("RuleNo")) {
            if (FValue != null && !FValue.equals("")) {
                RuleNo = FValue.trim();
            } else {
                RuleNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RuleContent")) {
            if (FValue != null && !FValue.equals("")) {
                RuleContent = FValue.trim();
            } else {
                RuleContent = null;
            }
        }
        if (FCode.equalsIgnoreCase("RuleDescription")) {
            if (FValue != null && !FValue.equals("")) {
                RuleDescription = FValue.trim();
            } else {
                RuleDescription = null;
            }
        }
        if (FCode.equalsIgnoreCase("Target")) {
            if (FValue != null && !FValue.equals("")) {
                Target = FValue.trim();
            } else {
                Target = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("DefaultFlag")) {
            if (FValue != null && !FValue.equals("")) {
                DefaultFlag = FValue.trim();
            } else {
                DefaultFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("GoalType")) {
            if (FValue != null && !FValue.equals("")) {
                GoalType = FValue.trim();
            } else {
                GoalType = null;
            }
        }
        if (FCode.equalsIgnoreCase("SourceComCode")) {
            if (FValue != null && !FValue.equals("")) {
                SourceComCode = FValue.trim();
            } else {
                SourceComCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LGAutoDeliverRuleSchema other = (LGAutoDeliverRuleSchema) otherObject;
        return
                RuleNo.equals(other.getRuleNo())
                && RuleContent.equals(other.getRuleContent())
                && RuleDescription.equals(other.getRuleDescription())
                && Target.equals(other.getTarget())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && DefaultFlag.equals(other.getDefaultFlag())
                && GoalType.equals(other.getGoalType())
                && SourceComCode.equals(other.getSourceComCode());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("RuleNo")) {
            return 0;
        }
        if (strFieldName.equals("RuleContent")) {
            return 1;
        }
        if (strFieldName.equals("RuleDescription")) {
            return 2;
        }
        if (strFieldName.equals("Target")) {
            return 3;
        }
        if (strFieldName.equals("Operator")) {
            return 4;
        }
        if (strFieldName.equals("MakeDate")) {
            return 5;
        }
        if (strFieldName.equals("MakeTime")) {
            return 6;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 7;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 8;
        }
        if (strFieldName.equals("DefaultFlag")) {
            return 9;
        }
        if (strFieldName.equals("GoalType")) {
            return 10;
        }
        if (strFieldName.equals("SourceComCode")) {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "RuleNo";
            break;
        case 1:
            strFieldName = "RuleContent";
            break;
        case 2:
            strFieldName = "RuleDescription";
            break;
        case 3:
            strFieldName = "Target";
            break;
        case 4:
            strFieldName = "Operator";
            break;
        case 5:
            strFieldName = "MakeDate";
            break;
        case 6:
            strFieldName = "MakeTime";
            break;
        case 7:
            strFieldName = "ModifyDate";
            break;
        case 8:
            strFieldName = "ModifyTime";
            break;
        case 9:
            strFieldName = "DefaultFlag";
            break;
        case 10:
            strFieldName = "GoalType";
            break;
        case 11:
            strFieldName = "SourceComCode";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("RuleNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RuleContent")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RuleDescription")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Target")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DefaultFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GoalType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SourceComCode")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
