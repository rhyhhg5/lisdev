/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LYOutPayDetailDB;

/*
 * <p>ClassName: LYOutPayDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-07-03
 */
public class LYOutPayDetailSchema implements Schema, Cloneable
{
	// @Field
	/** 业务流水号 */
	private String BusiNo;
	/** 批次号 */
	private String BatchNo;
	/** 是否更新客户信息 */
	private String isupdate;
	/** 集团 */
	private String pkgroup;
	/** 组织 */
	private String pkorg;
	/** 客户编码 */
	private String code;
	/** 客户名称 */
	private String name;
	/** 创建人 */
	private String creator;
	/** 创建时间 */
	private String creationtime;
	/** 修改人 */
	private String modifier;
	/** 修改时间 */
	private String modifiedtime;
	/** 纳税人类型 */
	private String taxpayertype;
	/** 开票类型 */
	private String invtype;
	/** 开票周期 */
	private String makefreq;
	/** 开票间隔 */
	private String makespace;
	/** 联系电话 */
	private String telephone;
	/** 手机号码 */
	private String mobile;
	/** 寄送方式 */
	private String sendway;
	/** 发票收件人 */
	private String addressee;
	/** 邮编 */
	private String postcode;
	/** 邮寄地址 */
	private String mailingaddress;
	/** 客户类型 */
	private String customertype;
	/** 所属国家 */
	private String country;
	/** 法人名称 */
	private String legalperson;
	/** 客户税号 */
	private String taxnumber;
	/** 开户银行 */
	private String bank;
	/** 银行账户 */
	private String bankaccount;
	/** 开票日期 */
	private Date makeinvdate;
	/** 收票人 */
	private String receiveperson;
	/** 是否开具纸质发票 */
	private String ispaperyinv;
	/** 启用状态 */
	private String enablestate;
	/** 是否允许提前开票 */
	private String isearly;
	/** 开票方式 */
	private String billmode;
	/** 开票地点 */
	private String vaddress;
	/** 微信openid（电子） */
	private String openid;
	/** 发票通账号（电子） */
	private String billaccount;
	/** 电子邮箱（电子） */
	private String email;
	/** 个险客户编号 */
	private String vpsnid;
	/** 保单生效日期(个险) */
	private Date veffectdate;
	/** 保单开至日期(个险) */
	private Date vautodate;
	/** 投递省份 */
	private String vmailprovince;
	/** 投递城市 */
	private String vmailcity;
	/** 来源系统 */
	private String vsrcsystem;
	/** 来源数据id */
	private String vsrcrowid;
	/** 所属集团 */
	private String group;
	/** 所属组织 */
	private String org;
	/** 所属组织版本 */
	private String orgv;
	/** 计算流水号 */
	private String calnum;
	/** 计算日期 */
	private Date caldate;
	/** 数据日期 */
	private Date vdata;
	/** 汇率 */
	private String rate;
	/** 价税分离项目 */
	private String ptsitem;
	/** 计税方法 */
	private int methodcal;
	/** 是否即征即退 */
	private String drawback;
	/** 税率 */
	private String taxrate;
	/** 不含税金额（原币） */
	private String oriamt;
	/** 税额（原币） */
	private String oritax;
	/** 不含??金额（本币） */
	private String localamt;
	/** 税额（本币） */
	private String localtax;
	/** 申报日期 */
	private Date decldate;
	/** 业务日期 */
	private Date busidate;
	/** 客户号 */
	private String custcode;
	/** 客户的名称 */
	private String custname;
	/** 客户的类型 */
	private String custtype;
	/** 申报类型 */
	private int dectype;
	/** 来源的系统 */
	private String srcsystem;
	/** 凭证id */
	private String voucherid;
	/** 交易流水号 */
	private String transerial;
	/** 交易批次号 */
	private String tranbatch;
	/** 交易日期 */
	private Date trandate;
	/** 交易时间 */
	private String trantime;
	/** 交易机构 */
	private String tranorg;
	/** 交易渠道 */
	private String tranchannel;
	/** 交易柜员 */
	private String trancounte;
	/** 客户经理 */
	private String custmanager;
	/** 交易代码 */
	private String trantype;
	/** 产品代码 */
	private String procode;
	/** 业务类型代码 */
	private String busitype;
	/** 交易摘要 */
	private String tranremark;
	/** 账户号 */
	private String accountno;
	/** 交易币种 */
	private String trancurrency;
	/** 交易金额 */
	private String tranamt;
	/** 金额是否含税 */
	private int taxtype;
	/** 被冲抹标识 */
	private String cztype;
	/** 冲抹原交易日期 */
	private Date czolddate;
	/** 是否冲正 */
	private String iscurrent;
	/** 冲抹源交易流水号 */
	private String czbusino;
	/** 会计机构 */
	private String accno;
	/** 借贷标志 */
	private int loanflag;
	/** 科目编码 */
	private String acccode;
	/** 收付标志 */
	private int paymentflag;
	/** 摊销委托编号 */
	private String amortno;
	/** 中间业务合同编号 */
	private String midcontno;
	/** 是否我行账号 */
	private String isinnerbank;
	/** 境内外标示 */
	private int overseasflag;
	/** 税金计算方式 */
	private int taxcalmethod;
	/** 创建的人 */
	private String creator1;
	/** 创建的时间 */
	private String creationtime1;
	/** 修改的人 */
	private String modifier1;
	/** 修改的时间 */
	private String modifiedtime1;
	/** 证券类别 */
	private String negtype;
	/** 交易场所 */
	private String tranplace;
	/** 部门 */
	private String deptdoc;
	/** 收支确认类型 */
	private int confimtype;
	/** 是否价税分离 */
	private String ispts;
	/** 是否开票 */
	private String isbill;
	/** 汇总地类型 */
	private int areatype;
	/** 是否来自异常表 */
	private String isptserror;
	/** 接收日期 */
	private Date receivedate;
	/** 是否可以提前开票 */
	private String isalloweadvance;
	/** 业务交易流水号 */
	private String busipk;
	/** 业务项目编码 */
	private String busiitem;
	/** 财务险种 */
	private String fininsance;
	/** 财务险别 */
	private String fininstype;
	/** 纳税类型 */
	private String istaxfree;
	/** 单位 */
	private String bunit;
	/** 单价 */
	private String bunivalent;
	/** 数量 */
	private String bnumber;
	/** 发票类?? */
	private String billtype;
	/** 允许开票日期 */
	private Date billdate;
	/** 商品服务档案 */
	private String servicedoc;
	/** 是否历史保单 */
	private String ishistory;
	/** 见费情况 */
	private String ismoneny;
	/** 投保单号 */
	private String insureno;
	/** 是否首期保费 */
	private String isfirst;
	/** 个人保单号 */
	private String perspolicyno;
	/** 个人保单保费核销日期 */
	private String ppveridate;
	/** 保费开始日期 */
	private Date billstartdate;
	/** 保费结束日期 */
	private Date billenddate;
	/** 缴费方式 */
	private String paytype;
	/** 是否垫交保费 */
	private String ismat;
	/** 是否委托缴费 */
	private String isentrust;
	/** 保单生效日期 */
	private Date billeffectivedate;
	/** 确认收入日期 */
	private Date recoincomdate;
	/** 开票标识 */
	private String billfalg;
	/** 收费冲销日期 */
	private String offsetdate;
	/** 批单号（保全） */
	private String bqbatchno;
	/** 是否视同销售 */
	private String issale;
	/** 发票数据推送状态 */
	private String state;
	/** 发票是否打印状态 */
	private String printstate;
	/** 收付费号 */
	private String moneyno;
	/** 发票状态 */
	private String vstatus;
	/** 预打发票标识 */
	private String prebilltype;
	/** 发票打印号码 */
	private String billprintno;
	/** 发票打印日期 */
	private Date billprintdate;
	/** 收付类型 */
	private String moneytype;
	/** 开票与生效孰后的状态 */
	private String cvalistate;
	/** 保单号 */
	private String vdef2;
	/** 备注 */
	private String vdef1;
	/** 操作员 */
	private String operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 错误信息 */
	private String ErrorInfo;
	/** 代理机构编码 */
	private String vdef6;
	/** 代理机构名称 */
	private String vdef7;
	/** 业务员编码 */
	private String vdef8;
	/** 平台回写日期 */
	private String TS;
	/** 电子发票客户邮箱 */
	private String buymailbox;
	/** 电子发票客户手机号 */
	private String buyphoneno;
	/** 证件类型 */
	private String buyidtype;
	/** 证件类型 */
	private String buyidnumber;
	/** 发票代码 */
	private String invoiceCode;
	/** 校验码 */
	private String checkCode;
	/** 外网短链接地址 */
	private String shortLink;

	public static final int FIELDNUM = 166;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LYOutPayDetailSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BusiNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LYOutPayDetailSchema cloned = (LYOutPayDetailSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBusiNo()
	{
		return BusiNo;
	}
	public void setBusiNo(String aBusiNo)
	{
		BusiNo = aBusiNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getisupdate()
	{
		return isupdate;
	}
	public void setisupdate(String aisupdate)
	{
		isupdate = aisupdate;
	}
	public String getpkgroup()
	{
		return pkgroup;
	}
	public void setpkgroup(String apkgroup)
	{
		pkgroup = apkgroup;
	}
	public String getpkorg()
	{
		return pkorg;
	}
	public void setpkorg(String apkorg)
	{
		pkorg = apkorg;
	}
	public String getcode()
	{
		return code;
	}
	public void setcode(String acode)
	{
		code = acode;
	}
	public String getname()
	{
		return name;
	}
	public void setname(String aname)
	{
		name = aname;
	}
	public String getcreator()
	{
		return creator;
	}
	public void setcreator(String acreator)
	{
		creator = acreator;
	}
	public String getcreationtime()
	{
		return creationtime;
	}
	public void setcreationtime(String acreationtime)
	{
		creationtime = acreationtime;
	}
	public String getmodifier()
	{
		return modifier;
	}
	public void setmodifier(String amodifier)
	{
		modifier = amodifier;
	}
	public String getmodifiedtime()
	{
		return modifiedtime;
	}
	public void setmodifiedtime(String amodifiedtime)
	{
		modifiedtime = amodifiedtime;
	}
	public String gettaxpayertype()
	{
		return taxpayertype;
	}
	public void settaxpayertype(String ataxpayertype)
	{
		taxpayertype = ataxpayertype;
	}
	public String getinvtype()
	{
		return invtype;
	}
	public void setinvtype(String ainvtype)
	{
		invtype = ainvtype;
	}
	public String getmakefreq()
	{
		return makefreq;
	}
	public void setmakefreq(String amakefreq)
	{
		makefreq = amakefreq;
	}
	public String getmakespace()
	{
		return makespace;
	}
	public void setmakespace(String amakespace)
	{
		makespace = amakespace;
	}
	public String gettelephone()
	{
		return telephone;
	}
	public void settelephone(String atelephone)
	{
		telephone = atelephone;
	}
	public String getmobile()
	{
		return mobile;
	}
	public void setmobile(String amobile)
	{
		mobile = amobile;
	}
	public String getsendway()
	{
		return sendway;
	}
	public void setsendway(String asendway)
	{
		sendway = asendway;
	}
	public String getaddressee()
	{
		return addressee;
	}
	public void setaddressee(String aaddressee)
	{
		addressee = aaddressee;
	}
	public String getpostcode()
	{
		return postcode;
	}
	public void setpostcode(String apostcode)
	{
		postcode = apostcode;
	}
	public String getmailingaddress()
	{
		return mailingaddress;
	}
	public void setmailingaddress(String amailingaddress)
	{
		mailingaddress = amailingaddress;
	}
	public String getcustomertype()
	{
		return customertype;
	}
	public void setcustomertype(String acustomertype)
	{
		customertype = acustomertype;
	}
	public String getcountry()
	{
		return country;
	}
	public void setcountry(String acountry)
	{
		country = acountry;
	}
	public String getlegalperson()
	{
		return legalperson;
	}
	public void setlegalperson(String alegalperson)
	{
		legalperson = alegalperson;
	}
	public String gettaxnumber()
	{
		return taxnumber;
	}
	public void settaxnumber(String ataxnumber)
	{
		taxnumber = ataxnumber;
	}
	public String getbank()
	{
		return bank;
	}
	public void setbank(String abank)
	{
		bank = abank;
	}
	public String getbankaccount()
	{
		return bankaccount;
	}
	public void setbankaccount(String abankaccount)
	{
		bankaccount = abankaccount;
	}
	public String getmakeinvdate()
	{
		if( makeinvdate != null )
			return fDate.getString(makeinvdate);
		else
			return null;
	}
	public void setmakeinvdate(Date amakeinvdate)
	{
		makeinvdate = amakeinvdate;
	}
	public void setmakeinvdate(String amakeinvdate)
	{
		if (amakeinvdate != null && !amakeinvdate.equals("") )
		{
			makeinvdate = fDate.getDate( amakeinvdate );
		}
		else
			makeinvdate = null;
	}

	public String getreceiveperson()
	{
		return receiveperson;
	}
	public void setreceiveperson(String areceiveperson)
	{
		receiveperson = areceiveperson;
	}
	public String getispaperyinv()
	{
		return ispaperyinv;
	}
	public void setispaperyinv(String aispaperyinv)
	{
		ispaperyinv = aispaperyinv;
	}
	public String getenablestate()
	{
		return enablestate;
	}
	public void setenablestate(String aenablestate)
	{
		enablestate = aenablestate;
	}
	public String getisearly()
	{
		return isearly;
	}
	public void setisearly(String aisearly)
	{
		isearly = aisearly;
	}
	public String getbillmode()
	{
		return billmode;
	}
	public void setbillmode(String abillmode)
	{
		billmode = abillmode;
	}
	public String getvaddress()
	{
		return vaddress;
	}
	public void setvaddress(String avaddress)
	{
		vaddress = avaddress;
	}
	public String getopenid()
	{
		return openid;
	}
	public void setopenid(String aopenid)
	{
		openid = aopenid;
	}
	public String getbillaccount()
	{
		return billaccount;
	}
	public void setbillaccount(String abillaccount)
	{
		billaccount = abillaccount;
	}
	public String getemail()
	{
		return email;
	}
	public void setemail(String aemail)
	{
		email = aemail;
	}
	public String getvpsnid()
	{
		return vpsnid;
	}
	public void setvpsnid(String avpsnid)
	{
		vpsnid = avpsnid;
	}
	public String getveffectdate()
	{
		if( veffectdate != null )
			return fDate.getString(veffectdate);
		else
			return null;
	}
	public void setveffectdate(Date aveffectdate)
	{
		veffectdate = aveffectdate;
	}
	public void setveffectdate(String aveffectdate)
	{
		if (aveffectdate != null && !aveffectdate.equals("") )
		{
			veffectdate = fDate.getDate( aveffectdate );
		}
		else
			veffectdate = null;
	}

	public String getvautodate()
	{
		if( vautodate != null )
			return fDate.getString(vautodate);
		else
			return null;
	}
	public void setvautodate(Date avautodate)
	{
		vautodate = avautodate;
	}
	public void setvautodate(String avautodate)
	{
		if (avautodate != null && !avautodate.equals("") )
		{
			vautodate = fDate.getDate( avautodate );
		}
		else
			vautodate = null;
	}

	public String getvmailprovince()
	{
		return vmailprovince;
	}
	public void setvmailprovince(String avmailprovince)
	{
		vmailprovince = avmailprovince;
	}
	public String getvmailcity()
	{
		return vmailcity;
	}
	public void setvmailcity(String avmailcity)
	{
		vmailcity = avmailcity;
	}
	public String getvsrcsystem()
	{
		return vsrcsystem;
	}
	public void setvsrcsystem(String avsrcsystem)
	{
		vsrcsystem = avsrcsystem;
	}
	public String getvsrcrowid()
	{
		return vsrcrowid;
	}
	public void setvsrcrowid(String avsrcrowid)
	{
		vsrcrowid = avsrcrowid;
	}
	public String getgroup()
	{
		return group;
	}
	public void setgroup(String agroup)
	{
		group = agroup;
	}
	public String getorg()
	{
		return org;
	}
	public void setorg(String aorg)
	{
		org = aorg;
	}
	public String getorgv()
	{
		return orgv;
	}
	public void setorgv(String aorgv)
	{
		orgv = aorgv;
	}
	public String getcalnum()
	{
		return calnum;
	}
	public void setcalnum(String acalnum)
	{
		calnum = acalnum;
	}
	public String getcaldate()
	{
		if( caldate != null )
			return fDate.getString(caldate);
		else
			return null;
	}
	public void setcaldate(Date acaldate)
	{
		caldate = acaldate;
	}
	public void setcaldate(String acaldate)
	{
		if (acaldate != null && !acaldate.equals("") )
		{
			caldate = fDate.getDate( acaldate );
		}
		else
			caldate = null;
	}

	public String getvdata()
	{
		if( vdata != null )
			return fDate.getString(vdata);
		else
			return null;
	}
	public void setvdata(Date avdata)
	{
		vdata = avdata;
	}
	public void setvdata(String avdata)
	{
		if (avdata != null && !avdata.equals("") )
		{
			vdata = fDate.getDate( avdata );
		}
		else
			vdata = null;
	}

	public String getrate()
	{
		return rate;
	}
	public void setrate(String arate)
	{
		rate = arate;
	}
	public String getptsitem()
	{
		return ptsitem;
	}
	public void setptsitem(String aptsitem)
	{
		ptsitem = aptsitem;
	}
	public int getmethodcal()
	{
		return methodcal;
	}
	public void setmethodcal(int amethodcal)
	{
		methodcal = amethodcal;
	}
	public void setmethodcal(String amethodcal)
	{
		if (amethodcal != null && !amethodcal.equals(""))
		{
			Integer tInteger = new Integer(amethodcal);
			int i = tInteger.intValue();
			methodcal = i;
		}
	}

	public String getdrawback()
	{
		return drawback;
	}
	public void setdrawback(String adrawback)
	{
		drawback = adrawback;
	}
	public String gettaxrate()
	{
		return taxrate;
	}
	public void settaxrate(String ataxrate)
	{
		taxrate = ataxrate;
	}
	public String getoriamt()
	{
		return oriamt;
	}
	public void setoriamt(String aoriamt)
	{
		oriamt = aoriamt;
	}
	public String getoritax()
	{
		return oritax;
	}
	public void setoritax(String aoritax)
	{
		oritax = aoritax;
	}
	public String getlocalamt()
	{
		return localamt;
	}
	public void setlocalamt(String alocalamt)
	{
		localamt = alocalamt;
	}
	public String getlocaltax()
	{
		return localtax;
	}
	public void setlocaltax(String alocaltax)
	{
		localtax = alocaltax;
	}
	public String getdecldate()
	{
		if( decldate != null )
			return fDate.getString(decldate);
		else
			return null;
	}
	public void setdecldate(Date adecldate)
	{
		decldate = adecldate;
	}
	public void setdecldate(String adecldate)
	{
		if (adecldate != null && !adecldate.equals("") )
		{
			decldate = fDate.getDate( adecldate );
		}
		else
			decldate = null;
	}

	public String getbusidate()
	{
		if( busidate != null )
			return fDate.getString(busidate);
		else
			return null;
	}
	public void setbusidate(Date abusidate)
	{
		busidate = abusidate;
	}
	public void setbusidate(String abusidate)
	{
		if (abusidate != null && !abusidate.equals("") )
		{
			busidate = fDate.getDate( abusidate );
		}
		else
			busidate = null;
	}

	public String getcustcode()
	{
		return custcode;
	}
	public void setcustcode(String acustcode)
	{
		custcode = acustcode;
	}
	public String getcustname()
	{
		return custname;
	}
	public void setcustname(String acustname)
	{
		custname = acustname;
	}
	public String getcusttype()
	{
		return custtype;
	}
	public void setcusttype(String acusttype)
	{
		custtype = acusttype;
	}
	public int getdectype()
	{
		return dectype;
	}
	public void setdectype(int adectype)
	{
		dectype = adectype;
	}
	public void setdectype(String adectype)
	{
		if (adectype != null && !adectype.equals(""))
		{
			Integer tInteger = new Integer(adectype);
			int i = tInteger.intValue();
			dectype = i;
		}
	}

	public String getsrcsystem()
	{
		return srcsystem;
	}
	public void setsrcsystem(String asrcsystem)
	{
		srcsystem = asrcsystem;
	}
	public String getvoucherid()
	{
		return voucherid;
	}
	public void setvoucherid(String avoucherid)
	{
		voucherid = avoucherid;
	}
	public String gettranserial()
	{
		return transerial;
	}
	public void settranserial(String atranserial)
	{
		transerial = atranserial;
	}
	public String gettranbatch()
	{
		return tranbatch;
	}
	public void settranbatch(String atranbatch)
	{
		tranbatch = atranbatch;
	}
	public String gettrandate()
	{
		if( trandate != null )
			return fDate.getString(trandate);
		else
			return null;
	}
	public void settrandate(Date atrandate)
	{
		trandate = atrandate;
	}
	public void settrandate(String atrandate)
	{
		if (atrandate != null && !atrandate.equals("") )
		{
			trandate = fDate.getDate( atrandate );
		}
		else
			trandate = null;
	}

	public String gettrantime()
	{
		return trantime;
	}
	public void settrantime(String atrantime)
	{
		trantime = atrantime;
	}
	public String gettranorg()
	{
		return tranorg;
	}
	public void settranorg(String atranorg)
	{
		tranorg = atranorg;
	}
	public String gettranchannel()
	{
		return tranchannel;
	}
	public void settranchannel(String atranchannel)
	{
		tranchannel = atranchannel;
	}
	public String gettrancounte()
	{
		return trancounte;
	}
	public void settrancounte(String atrancounte)
	{
		trancounte = atrancounte;
	}
	public String getcustmanager()
	{
		return custmanager;
	}
	public void setcustmanager(String acustmanager)
	{
		custmanager = acustmanager;
	}
	public String gettrantype()
	{
		return trantype;
	}
	public void settrantype(String atrantype)
	{
		trantype = atrantype;
	}
	public String getprocode()
	{
		return procode;
	}
	public void setprocode(String aprocode)
	{
		procode = aprocode;
	}
	public String getbusitype()
	{
		return busitype;
	}
	public void setbusitype(String abusitype)
	{
		busitype = abusitype;
	}
	public String gettranremark()
	{
		return tranremark;
	}
	public void settranremark(String atranremark)
	{
		tranremark = atranremark;
	}
	public String getaccountno()
	{
		return accountno;
	}
	public void setaccountno(String aaccountno)
	{
		accountno = aaccountno;
	}
	public String gettrancurrency()
	{
		return trancurrency;
	}
	public void settrancurrency(String atrancurrency)
	{
		trancurrency = atrancurrency;
	}
	public String gettranamt()
	{
		return tranamt;
	}
	public void settranamt(String atranamt)
	{
		tranamt = atranamt;
	}
	public int gettaxtype()
	{
		return taxtype;
	}
	public void settaxtype(int ataxtype)
	{
		taxtype = ataxtype;
	}
	public void settaxtype(String ataxtype)
	{
		if (ataxtype != null && !ataxtype.equals(""))
		{
			Integer tInteger = new Integer(ataxtype);
			int i = tInteger.intValue();
			taxtype = i;
		}
	}

	public String getcztype()
	{
		return cztype;
	}
	public void setcztype(String acztype)
	{
		cztype = acztype;
	}
	public String getczolddate()
	{
		if( czolddate != null )
			return fDate.getString(czolddate);
		else
			return null;
	}
	public void setczolddate(Date aczolddate)
	{
		czolddate = aczolddate;
	}
	public void setczolddate(String aczolddate)
	{
		if (aczolddate != null && !aczolddate.equals("") )
		{
			czolddate = fDate.getDate( aczolddate );
		}
		else
			czolddate = null;
	}

	public String getiscurrent()
	{
		return iscurrent;
	}
	public void setiscurrent(String aiscurrent)
	{
		iscurrent = aiscurrent;
	}
	public String getczbusino()
	{
		return czbusino;
	}
	public void setczbusino(String aczbusino)
	{
		czbusino = aczbusino;
	}
	public String getaccno()
	{
		return accno;
	}
	public void setaccno(String aaccno)
	{
		accno = aaccno;
	}
	public int getloanflag()
	{
		return loanflag;
	}
	public void setloanflag(int aloanflag)
	{
		loanflag = aloanflag;
	}
	public void setloanflag(String aloanflag)
	{
		if (aloanflag != null && !aloanflag.equals(""))
		{
			Integer tInteger = new Integer(aloanflag);
			int i = tInteger.intValue();
			loanflag = i;
		}
	}

	public String getacccode()
	{
		return acccode;
	}
	public void setacccode(String aacccode)
	{
		acccode = aacccode;
	}
	public int getpaymentflag()
	{
		return paymentflag;
	}
	public void setpaymentflag(int apaymentflag)
	{
		paymentflag = apaymentflag;
	}
	public void setpaymentflag(String apaymentflag)
	{
		if (apaymentflag != null && !apaymentflag.equals(""))
		{
			Integer tInteger = new Integer(apaymentflag);
			int i = tInteger.intValue();
			paymentflag = i;
		}
	}

	public String getamortno()
	{
		return amortno;
	}
	public void setamortno(String aamortno)
	{
		amortno = aamortno;
	}
	public String getmidcontno()
	{
		return midcontno;
	}
	public void setmidcontno(String amidcontno)
	{
		midcontno = amidcontno;
	}
	public String getisinnerbank()
	{
		return isinnerbank;
	}
	public void setisinnerbank(String aisinnerbank)
	{
		isinnerbank = aisinnerbank;
	}
	public int getoverseasflag()
	{
		return overseasflag;
	}
	public void setoverseasflag(int aoverseasflag)
	{
		overseasflag = aoverseasflag;
	}
	public void setoverseasflag(String aoverseasflag)
	{
		if (aoverseasflag != null && !aoverseasflag.equals(""))
		{
			Integer tInteger = new Integer(aoverseasflag);
			int i = tInteger.intValue();
			overseasflag = i;
		}
	}

	public int gettaxcalmethod()
	{
		return taxcalmethod;
	}
	public void settaxcalmethod(int ataxcalmethod)
	{
		taxcalmethod = ataxcalmethod;
	}
	public void settaxcalmethod(String ataxcalmethod)
	{
		if (ataxcalmethod != null && !ataxcalmethod.equals(""))
		{
			Integer tInteger = new Integer(ataxcalmethod);
			int i = tInteger.intValue();
			taxcalmethod = i;
		}
	}

	public String getcreator1()
	{
		return creator1;
	}
	public void setcreator1(String acreator1)
	{
		creator1 = acreator1;
	}
	public String getcreationtime1()
	{
		return creationtime1;
	}
	public void setcreationtime1(String acreationtime1)
	{
		creationtime1 = acreationtime1;
	}
	public String getmodifier1()
	{
		return modifier1;
	}
	public void setmodifier1(String amodifier1)
	{
		modifier1 = amodifier1;
	}
	public String getmodifiedtime1()
	{
		return modifiedtime1;
	}
	public void setmodifiedtime1(String amodifiedtime1)
	{
		modifiedtime1 = amodifiedtime1;
	}
	public String getnegtype()
	{
		return negtype;
	}
	public void setnegtype(String anegtype)
	{
		negtype = anegtype;
	}
	public String gettranplace()
	{
		return tranplace;
	}
	public void settranplace(String atranplace)
	{
		tranplace = atranplace;
	}
	public String getdeptdoc()
	{
		return deptdoc;
	}
	public void setdeptdoc(String adeptdoc)
	{
		deptdoc = adeptdoc;
	}
	public int getconfimtype()
	{
		return confimtype;
	}
	public void setconfimtype(int aconfimtype)
	{
		confimtype = aconfimtype;
	}
	public void setconfimtype(String aconfimtype)
	{
		if (aconfimtype != null && !aconfimtype.equals(""))
		{
			Integer tInteger = new Integer(aconfimtype);
			int i = tInteger.intValue();
			confimtype = i;
		}
	}

	public String getispts()
	{
		return ispts;
	}
	public void setispts(String aispts)
	{
		ispts = aispts;
	}
	public String getisbill()
	{
		return isbill;
	}
	public void setisbill(String aisbill)
	{
		isbill = aisbill;
	}
	public int getareatype()
	{
		return areatype;
	}
	public void setareatype(int aareatype)
	{
		areatype = aareatype;
	}
	public void setareatype(String aareatype)
	{
		if (aareatype != null && !aareatype.equals(""))
		{
			Integer tInteger = new Integer(aareatype);
			int i = tInteger.intValue();
			areatype = i;
		}
	}

	public String getisptserror()
	{
		return isptserror;
	}
	public void setisptserror(String aisptserror)
	{
		isptserror = aisptserror;
	}
	public String getreceivedate()
	{
		if( receivedate != null )
			return fDate.getString(receivedate);
		else
			return null;
	}
	public void setreceivedate(Date areceivedate)
	{
		receivedate = areceivedate;
	}
	public void setreceivedate(String areceivedate)
	{
		if (areceivedate != null && !areceivedate.equals("") )
		{
			receivedate = fDate.getDate( areceivedate );
		}
		else
			receivedate = null;
	}

	public String getisalloweadvance()
	{
		return isalloweadvance;
	}
	public void setisalloweadvance(String aisalloweadvance)
	{
		isalloweadvance = aisalloweadvance;
	}
	public String getbusipk()
	{
		return busipk;
	}
	public void setbusipk(String abusipk)
	{
		busipk = abusipk;
	}
	public String getbusiitem()
	{
		return busiitem;
	}
	public void setbusiitem(String abusiitem)
	{
		busiitem = abusiitem;
	}
	public String getfininsance()
	{
		return fininsance;
	}
	public void setfininsance(String afininsance)
	{
		fininsance = afininsance;
	}
	public String getfininstype()
	{
		return fininstype;
	}
	public void setfininstype(String afininstype)
	{
		fininstype = afininstype;
	}
	public String getistaxfree()
	{
		return istaxfree;
	}
	public void setistaxfree(String aistaxfree)
	{
		istaxfree = aistaxfree;
	}
	public String getbunit()
	{
		return bunit;
	}
	public void setbunit(String abunit)
	{
		bunit = abunit;
	}
	public String getbunivalent()
	{
		return bunivalent;
	}
	public void setbunivalent(String abunivalent)
	{
		bunivalent = abunivalent;
	}
	public String getbnumber()
	{
		return bnumber;
	}
	public void setbnumber(String abnumber)
	{
		bnumber = abnumber;
	}
	public String getbilltype()
	{
		return billtype;
	}
	public void setbilltype(String abilltype)
	{
		billtype = abilltype;
	}
	public String getbilldate()
	{
		if( billdate != null )
			return fDate.getString(billdate);
		else
			return null;
	}
	public void setbilldate(Date abilldate)
	{
		billdate = abilldate;
	}
	public void setbilldate(String abilldate)
	{
		if (abilldate != null && !abilldate.equals("") )
		{
			billdate = fDate.getDate( abilldate );
		}
		else
			billdate = null;
	}

	public String getservicedoc()
	{
		return servicedoc;
	}
	public void setservicedoc(String aservicedoc)
	{
		servicedoc = aservicedoc;
	}
	public String getishistory()
	{
		return ishistory;
	}
	public void setishistory(String aishistory)
	{
		ishistory = aishistory;
	}
	public String getismoneny()
	{
		return ismoneny;
	}
	public void setismoneny(String aismoneny)
	{
		ismoneny = aismoneny;
	}
	public String getinsureno()
	{
		return insureno;
	}
	public void setinsureno(String ainsureno)
	{
		insureno = ainsureno;
	}
	public String getisfirst()
	{
		return isfirst;
	}
	public void setisfirst(String aisfirst)
	{
		isfirst = aisfirst;
	}
	public String getperspolicyno()
	{
		return perspolicyno;
	}
	public void setperspolicyno(String aperspolicyno)
	{
		perspolicyno = aperspolicyno;
	}
	public String getppveridate()
	{
		return ppveridate;
	}
	public void setppveridate(String appveridate)
	{
		ppveridate = appveridate;
	}
	public String getbillstartdate()
	{
		if( billstartdate != null )
			return fDate.getString(billstartdate);
		else
			return null;
	}
	public void setbillstartdate(Date abillstartdate)
	{
		billstartdate = abillstartdate;
	}
	public void setbillstartdate(String abillstartdate)
	{
		if (abillstartdate != null && !abillstartdate.equals("") )
		{
			billstartdate = fDate.getDate( abillstartdate );
		}
		else
			billstartdate = null;
	}

	public String getbillenddate()
	{
		if( billenddate != null )
			return fDate.getString(billenddate);
		else
			return null;
	}
	public void setbillenddate(Date abillenddate)
	{
		billenddate = abillenddate;
	}
	public void setbillenddate(String abillenddate)
	{
		if (abillenddate != null && !abillenddate.equals("") )
		{
			billenddate = fDate.getDate( abillenddate );
		}
		else
			billenddate = null;
	}

	public String getpaytype()
	{
		return paytype;
	}
	public void setpaytype(String apaytype)
	{
		paytype = apaytype;
	}
	public String getismat()
	{
		return ismat;
	}
	public void setismat(String aismat)
	{
		ismat = aismat;
	}
	public String getisentrust()
	{
		return isentrust;
	}
	public void setisentrust(String aisentrust)
	{
		isentrust = aisentrust;
	}
	public String getbilleffectivedate()
	{
		if( billeffectivedate != null )
			return fDate.getString(billeffectivedate);
		else
			return null;
	}
	public void setbilleffectivedate(Date abilleffectivedate)
	{
		billeffectivedate = abilleffectivedate;
	}
	public void setbilleffectivedate(String abilleffectivedate)
	{
		if (abilleffectivedate != null && !abilleffectivedate.equals("") )
		{
			billeffectivedate = fDate.getDate( abilleffectivedate );
		}
		else
			billeffectivedate = null;
	}

	public String getrecoincomdate()
	{
		if( recoincomdate != null )
			return fDate.getString(recoincomdate);
		else
			return null;
	}
	public void setrecoincomdate(Date arecoincomdate)
	{
		recoincomdate = arecoincomdate;
	}
	public void setrecoincomdate(String arecoincomdate)
	{
		if (arecoincomdate != null && !arecoincomdate.equals("") )
		{
			recoincomdate = fDate.getDate( arecoincomdate );
		}
		else
			recoincomdate = null;
	}

	public String getbillfalg()
	{
		return billfalg;
	}
	public void setbillfalg(String abillfalg)
	{
		billfalg = abillfalg;
	}
	public String getoffsetdate()
	{
		return offsetdate;
	}
	public void setoffsetdate(String aoffsetdate)
	{
		offsetdate = aoffsetdate;
	}
	public String getbqbatchno()
	{
		return bqbatchno;
	}
	public void setbqbatchno(String abqbatchno)
	{
		bqbatchno = abqbatchno;
	}
	public String getissale()
	{
		return issale;
	}
	public void setissale(String aissale)
	{
		issale = aissale;
	}
	public String getstate()
	{
		return state;
	}
	public void setstate(String astate)
	{
		state = astate;
	}
	public String getprintstate()
	{
		return printstate;
	}
	public void setprintstate(String aprintstate)
	{
		printstate = aprintstate;
	}
	public String getmoneyno()
	{
		return moneyno;
	}
	public void setmoneyno(String amoneyno)
	{
		moneyno = amoneyno;
	}
	public String getvstatus()
	{
		return vstatus;
	}
	public void setvstatus(String avstatus)
	{
		vstatus = avstatus;
	}
	public String getprebilltype()
	{
		return prebilltype;
	}
	public void setprebilltype(String aprebilltype)
	{
		prebilltype = aprebilltype;
	}
	public String getbillprintno()
	{
		return billprintno;
	}
	public void setbillprintno(String abillprintno)
	{
		billprintno = abillprintno;
	}
	public String getbillprintdate()
	{
		if( billprintdate != null )
			return fDate.getString(billprintdate);
		else
			return null;
	}
	public void setbillprintdate(Date abillprintdate)
	{
		billprintdate = abillprintdate;
	}
	public void setbillprintdate(String abillprintdate)
	{
		if (abillprintdate != null && !abillprintdate.equals("") )
		{
			billprintdate = fDate.getDate( abillprintdate );
		}
		else
			billprintdate = null;
	}

	public String getmoneytype()
	{
		return moneytype;
	}
	public void setmoneytype(String amoneytype)
	{
		moneytype = amoneytype;
	}
	public String getcvalistate()
	{
		return cvalistate;
	}
	public void setcvalistate(String acvalistate)
	{
		cvalistate = acvalistate;
	}
	public String getvdef2()
	{
		return vdef2;
	}
	public void setvdef2(String avdef2)
	{
		vdef2 = avdef2;
	}
	public String getvdef1()
	{
		return vdef1;
	}
	public void setvdef1(String avdef1)
	{
		vdef1 = avdef1;
	}
	public String getoperator()
	{
		return operator;
	}
	public void setoperator(String aoperator)
	{
		operator = aoperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getErrorInfo()
	{
		return ErrorInfo;
	}
	public void setErrorInfo(String aErrorInfo)
	{
		ErrorInfo = aErrorInfo;
	}
	public String getvdef6()
	{
		return vdef6;
	}
	public void setvdef6(String avdef6)
	{
		vdef6 = avdef6;
	}
	public String getvdef7()
	{
		return vdef7;
	}
	public void setvdef7(String avdef7)
	{
		vdef7 = avdef7;
	}
	public String getvdef8()
	{
		return vdef8;
	}
	public void setvdef8(String avdef8)
	{
		vdef8 = avdef8;
	}
	public String getTS()
	{
		return TS;
	}
	public void setTS(String aTS)
	{
		TS = aTS;
	}
	public String getbuymailbox()
	{
		return buymailbox;
	}
	public void setbuymailbox(String abuymailbox)
	{
		buymailbox = abuymailbox;
	}
	public String getbuyphoneno()
	{
		return buyphoneno;
	}
	public void setbuyphoneno(String abuyphoneno)
	{
		buyphoneno = abuyphoneno;
	}
	public String getbuyidtype()
	{
		return buyidtype;
	}
	public void setbuyidtype(String abuyidtype)
	{
		buyidtype = abuyidtype;
	}
	public String getbuyidnumber()
	{
		return buyidnumber;
	}
	public void setbuyidnumber(String abuyidnumber)
	{
		buyidnumber = abuyidnumber;
	}
	public String getinvoiceCode()
	{
		return invoiceCode;
	}
	public void setinvoiceCode(String ainvoiceCode)
	{
		invoiceCode = ainvoiceCode;
	}
	public String getcheckCode()
	{
		return checkCode;
	}
	public void setcheckCode(String acheckCode)
	{
		checkCode = acheckCode;
	}
	public String getshortLink()
	{
		return shortLink;
	}
	public void setshortLink(String ashortLink)
	{
		shortLink = ashortLink;
	}

	/**
	* 使用另外一个 LYOutPayDetailSchema 对象给 Schema 赋值
	* @param: aLYOutPayDetailSchema LYOutPayDetailSchema
	**/
	public void setSchema(LYOutPayDetailSchema aLYOutPayDetailSchema)
	{
		this.BusiNo = aLYOutPayDetailSchema.getBusiNo();
		this.BatchNo = aLYOutPayDetailSchema.getBatchNo();
		this.isupdate = aLYOutPayDetailSchema.getisupdate();
		this.pkgroup = aLYOutPayDetailSchema.getpkgroup();
		this.pkorg = aLYOutPayDetailSchema.getpkorg();
		this.code = aLYOutPayDetailSchema.getcode();
		this.name = aLYOutPayDetailSchema.getname();
		this.creator = aLYOutPayDetailSchema.getcreator();
		this.creationtime = aLYOutPayDetailSchema.getcreationtime();
		this.modifier = aLYOutPayDetailSchema.getmodifier();
		this.modifiedtime = aLYOutPayDetailSchema.getmodifiedtime();
		this.taxpayertype = aLYOutPayDetailSchema.gettaxpayertype();
		this.invtype = aLYOutPayDetailSchema.getinvtype();
		this.makefreq = aLYOutPayDetailSchema.getmakefreq();
		this.makespace = aLYOutPayDetailSchema.getmakespace();
		this.telephone = aLYOutPayDetailSchema.gettelephone();
		this.mobile = aLYOutPayDetailSchema.getmobile();
		this.sendway = aLYOutPayDetailSchema.getsendway();
		this.addressee = aLYOutPayDetailSchema.getaddressee();
		this.postcode = aLYOutPayDetailSchema.getpostcode();
		this.mailingaddress = aLYOutPayDetailSchema.getmailingaddress();
		this.customertype = aLYOutPayDetailSchema.getcustomertype();
		this.country = aLYOutPayDetailSchema.getcountry();
		this.legalperson = aLYOutPayDetailSchema.getlegalperson();
		this.taxnumber = aLYOutPayDetailSchema.gettaxnumber();
		this.bank = aLYOutPayDetailSchema.getbank();
		this.bankaccount = aLYOutPayDetailSchema.getbankaccount();
		this.makeinvdate = fDate.getDate( aLYOutPayDetailSchema.getmakeinvdate());
		this.receiveperson = aLYOutPayDetailSchema.getreceiveperson();
		this.ispaperyinv = aLYOutPayDetailSchema.getispaperyinv();
		this.enablestate = aLYOutPayDetailSchema.getenablestate();
		this.isearly = aLYOutPayDetailSchema.getisearly();
		this.billmode = aLYOutPayDetailSchema.getbillmode();
		this.vaddress = aLYOutPayDetailSchema.getvaddress();
		this.openid = aLYOutPayDetailSchema.getopenid();
		this.billaccount = aLYOutPayDetailSchema.getbillaccount();
		this.email = aLYOutPayDetailSchema.getemail();
		this.vpsnid = aLYOutPayDetailSchema.getvpsnid();
		this.veffectdate = fDate.getDate( aLYOutPayDetailSchema.getveffectdate());
		this.vautodate = fDate.getDate( aLYOutPayDetailSchema.getvautodate());
		this.vmailprovince = aLYOutPayDetailSchema.getvmailprovince();
		this.vmailcity = aLYOutPayDetailSchema.getvmailcity();
		this.vsrcsystem = aLYOutPayDetailSchema.getvsrcsystem();
		this.vsrcrowid = aLYOutPayDetailSchema.getvsrcrowid();
		this.group = aLYOutPayDetailSchema.getgroup();
		this.org = aLYOutPayDetailSchema.getorg();
		this.orgv = aLYOutPayDetailSchema.getorgv();
		this.calnum = aLYOutPayDetailSchema.getcalnum();
		this.caldate = fDate.getDate( aLYOutPayDetailSchema.getcaldate());
		this.vdata = fDate.getDate( aLYOutPayDetailSchema.getvdata());
		this.rate = aLYOutPayDetailSchema.getrate();
		this.ptsitem = aLYOutPayDetailSchema.getptsitem();
		this.methodcal = aLYOutPayDetailSchema.getmethodcal();
		this.drawback = aLYOutPayDetailSchema.getdrawback();
		this.taxrate = aLYOutPayDetailSchema.gettaxrate();
		this.oriamt = aLYOutPayDetailSchema.getoriamt();
		this.oritax = aLYOutPayDetailSchema.getoritax();
		this.localamt = aLYOutPayDetailSchema.getlocalamt();
		this.localtax = aLYOutPayDetailSchema.getlocaltax();
		this.decldate = fDate.getDate( aLYOutPayDetailSchema.getdecldate());
		this.busidate = fDate.getDate( aLYOutPayDetailSchema.getbusidate());
		this.custcode = aLYOutPayDetailSchema.getcustcode();
		this.custname = aLYOutPayDetailSchema.getcustname();
		this.custtype = aLYOutPayDetailSchema.getcusttype();
		this.dectype = aLYOutPayDetailSchema.getdectype();
		this.srcsystem = aLYOutPayDetailSchema.getsrcsystem();
		this.voucherid = aLYOutPayDetailSchema.getvoucherid();
		this.transerial = aLYOutPayDetailSchema.gettranserial();
		this.tranbatch = aLYOutPayDetailSchema.gettranbatch();
		this.trandate = fDate.getDate( aLYOutPayDetailSchema.gettrandate());
		this.trantime = aLYOutPayDetailSchema.gettrantime();
		this.tranorg = aLYOutPayDetailSchema.gettranorg();
		this.tranchannel = aLYOutPayDetailSchema.gettranchannel();
		this.trancounte = aLYOutPayDetailSchema.gettrancounte();
		this.custmanager = aLYOutPayDetailSchema.getcustmanager();
		this.trantype = aLYOutPayDetailSchema.gettrantype();
		this.procode = aLYOutPayDetailSchema.getprocode();
		this.busitype = aLYOutPayDetailSchema.getbusitype();
		this.tranremark = aLYOutPayDetailSchema.gettranremark();
		this.accountno = aLYOutPayDetailSchema.getaccountno();
		this.trancurrency = aLYOutPayDetailSchema.gettrancurrency();
		this.tranamt = aLYOutPayDetailSchema.gettranamt();
		this.taxtype = aLYOutPayDetailSchema.gettaxtype();
		this.cztype = aLYOutPayDetailSchema.getcztype();
		this.czolddate = fDate.getDate( aLYOutPayDetailSchema.getczolddate());
		this.iscurrent = aLYOutPayDetailSchema.getiscurrent();
		this.czbusino = aLYOutPayDetailSchema.getczbusino();
		this.accno = aLYOutPayDetailSchema.getaccno();
		this.loanflag = aLYOutPayDetailSchema.getloanflag();
		this.acccode = aLYOutPayDetailSchema.getacccode();
		this.paymentflag = aLYOutPayDetailSchema.getpaymentflag();
		this.amortno = aLYOutPayDetailSchema.getamortno();
		this.midcontno = aLYOutPayDetailSchema.getmidcontno();
		this.isinnerbank = aLYOutPayDetailSchema.getisinnerbank();
		this.overseasflag = aLYOutPayDetailSchema.getoverseasflag();
		this.taxcalmethod = aLYOutPayDetailSchema.gettaxcalmethod();
		this.creator1 = aLYOutPayDetailSchema.getcreator1();
		this.creationtime1 = aLYOutPayDetailSchema.getcreationtime1();
		this.modifier1 = aLYOutPayDetailSchema.getmodifier1();
		this.modifiedtime1 = aLYOutPayDetailSchema.getmodifiedtime1();
		this.negtype = aLYOutPayDetailSchema.getnegtype();
		this.tranplace = aLYOutPayDetailSchema.gettranplace();
		this.deptdoc = aLYOutPayDetailSchema.getdeptdoc();
		this.confimtype = aLYOutPayDetailSchema.getconfimtype();
		this.ispts = aLYOutPayDetailSchema.getispts();
		this.isbill = aLYOutPayDetailSchema.getisbill();
		this.areatype = aLYOutPayDetailSchema.getareatype();
		this.isptserror = aLYOutPayDetailSchema.getisptserror();
		this.receivedate = fDate.getDate( aLYOutPayDetailSchema.getreceivedate());
		this.isalloweadvance = aLYOutPayDetailSchema.getisalloweadvance();
		this.busipk = aLYOutPayDetailSchema.getbusipk();
		this.busiitem = aLYOutPayDetailSchema.getbusiitem();
		this.fininsance = aLYOutPayDetailSchema.getfininsance();
		this.fininstype = aLYOutPayDetailSchema.getfininstype();
		this.istaxfree = aLYOutPayDetailSchema.getistaxfree();
		this.bunit = aLYOutPayDetailSchema.getbunit();
		this.bunivalent = aLYOutPayDetailSchema.getbunivalent();
		this.bnumber = aLYOutPayDetailSchema.getbnumber();
		this.billtype = aLYOutPayDetailSchema.getbilltype();
		this.billdate = fDate.getDate( aLYOutPayDetailSchema.getbilldate());
		this.servicedoc = aLYOutPayDetailSchema.getservicedoc();
		this.ishistory = aLYOutPayDetailSchema.getishistory();
		this.ismoneny = aLYOutPayDetailSchema.getismoneny();
		this.insureno = aLYOutPayDetailSchema.getinsureno();
		this.isfirst = aLYOutPayDetailSchema.getisfirst();
		this.perspolicyno = aLYOutPayDetailSchema.getperspolicyno();
		this.ppveridate = aLYOutPayDetailSchema.getppveridate();
		this.billstartdate = fDate.getDate( aLYOutPayDetailSchema.getbillstartdate());
		this.billenddate = fDate.getDate( aLYOutPayDetailSchema.getbillenddate());
		this.paytype = aLYOutPayDetailSchema.getpaytype();
		this.ismat = aLYOutPayDetailSchema.getismat();
		this.isentrust = aLYOutPayDetailSchema.getisentrust();
		this.billeffectivedate = fDate.getDate( aLYOutPayDetailSchema.getbilleffectivedate());
		this.recoincomdate = fDate.getDate( aLYOutPayDetailSchema.getrecoincomdate());
		this.billfalg = aLYOutPayDetailSchema.getbillfalg();
		this.offsetdate = aLYOutPayDetailSchema.getoffsetdate();
		this.bqbatchno = aLYOutPayDetailSchema.getbqbatchno();
		this.issale = aLYOutPayDetailSchema.getissale();
		this.state = aLYOutPayDetailSchema.getstate();
		this.printstate = aLYOutPayDetailSchema.getprintstate();
		this.moneyno = aLYOutPayDetailSchema.getmoneyno();
		this.vstatus = aLYOutPayDetailSchema.getvstatus();
		this.prebilltype = aLYOutPayDetailSchema.getprebilltype();
		this.billprintno = aLYOutPayDetailSchema.getbillprintno();
		this.billprintdate = fDate.getDate( aLYOutPayDetailSchema.getbillprintdate());
		this.moneytype = aLYOutPayDetailSchema.getmoneytype();
		this.cvalistate = aLYOutPayDetailSchema.getcvalistate();
		this.vdef2 = aLYOutPayDetailSchema.getvdef2();
		this.vdef1 = aLYOutPayDetailSchema.getvdef1();
		this.operator = aLYOutPayDetailSchema.getoperator();
		this.MakeDate = fDate.getDate( aLYOutPayDetailSchema.getMakeDate());
		this.MakeTime = aLYOutPayDetailSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLYOutPayDetailSchema.getModifyDate());
		this.ModifyTime = aLYOutPayDetailSchema.getModifyTime();
		this.ErrorInfo = aLYOutPayDetailSchema.getErrorInfo();
		this.vdef6 = aLYOutPayDetailSchema.getvdef6();
		this.vdef7 = aLYOutPayDetailSchema.getvdef7();
		this.vdef8 = aLYOutPayDetailSchema.getvdef8();
		this.TS = aLYOutPayDetailSchema.getTS();
		this.buymailbox = aLYOutPayDetailSchema.getbuymailbox();
		this.buyphoneno = aLYOutPayDetailSchema.getbuyphoneno();
		this.buyidtype = aLYOutPayDetailSchema.getbuyidtype();
		this.buyidnumber = aLYOutPayDetailSchema.getbuyidnumber();
		this.invoiceCode = aLYOutPayDetailSchema.getinvoiceCode();
		this.checkCode = aLYOutPayDetailSchema.getcheckCode();
		this.shortLink = aLYOutPayDetailSchema.getshortLink();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BusiNo") == null )
				this.BusiNo = null;
			else
				this.BusiNo = rs.getString("BusiNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("isupdate") == null )
				this.isupdate = null;
			else
				this.isupdate = rs.getString("isupdate").trim();

			if( rs.getString("pkgroup") == null )
				this.pkgroup = null;
			else
				this.pkgroup = rs.getString("pkgroup").trim();

			if( rs.getString("pkorg") == null )
				this.pkorg = null;
			else
				this.pkorg = rs.getString("pkorg").trim();

			if( rs.getString("code") == null )
				this.code = null;
			else
				this.code = rs.getString("code").trim();

			if( rs.getString("name") == null )
				this.name = null;
			else
				this.name = rs.getString("name").trim();

			if( rs.getString("creator") == null )
				this.creator = null;
			else
				this.creator = rs.getString("creator").trim();

			if( rs.getString("creationtime") == null )
				this.creationtime = null;
			else
				this.creationtime = rs.getString("creationtime").trim();

			if( rs.getString("modifier") == null )
				this.modifier = null;
			else
				this.modifier = rs.getString("modifier").trim();

			if( rs.getString("modifiedtime") == null )
				this.modifiedtime = null;
			else
				this.modifiedtime = rs.getString("modifiedtime").trim();

			if( rs.getString("taxpayertype") == null )
				this.taxpayertype = null;
			else
				this.taxpayertype = rs.getString("taxpayertype").trim();

			if( rs.getString("invtype") == null )
				this.invtype = null;
			else
				this.invtype = rs.getString("invtype").trim();

			if( rs.getString("makefreq") == null )
				this.makefreq = null;
			else
				this.makefreq = rs.getString("makefreq").trim();

			if( rs.getString("makespace") == null )
				this.makespace = null;
			else
				this.makespace = rs.getString("makespace").trim();

			if( rs.getString("telephone") == null )
				this.telephone = null;
			else
				this.telephone = rs.getString("telephone").trim();

			if( rs.getString("mobile") == null )
				this.mobile = null;
			else
				this.mobile = rs.getString("mobile").trim();

			if( rs.getString("sendway") == null )
				this.sendway = null;
			else
				this.sendway = rs.getString("sendway").trim();

			if( rs.getString("addressee") == null )
				this.addressee = null;
			else
				this.addressee = rs.getString("addressee").trim();

			if( rs.getString("postcode") == null )
				this.postcode = null;
			else
				this.postcode = rs.getString("postcode").trim();

			if( rs.getString("mailingaddress") == null )
				this.mailingaddress = null;
			else
				this.mailingaddress = rs.getString("mailingaddress").trim();

			if( rs.getString("customertype") == null )
				this.customertype = null;
			else
				this.customertype = rs.getString("customertype").trim();

			if( rs.getString("country") == null )
				this.country = null;
			else
				this.country = rs.getString("country").trim();

			if( rs.getString("legalperson") == null )
				this.legalperson = null;
			else
				this.legalperson = rs.getString("legalperson").trim();

			if( rs.getString("taxnumber") == null )
				this.taxnumber = null;
			else
				this.taxnumber = rs.getString("taxnumber").trim();

			if( rs.getString("bank") == null )
				this.bank = null;
			else
				this.bank = rs.getString("bank").trim();

			if( rs.getString("bankaccount") == null )
				this.bankaccount = null;
			else
				this.bankaccount = rs.getString("bankaccount").trim();

			this.makeinvdate = rs.getDate("makeinvdate");
			if( rs.getString("receiveperson") == null )
				this.receiveperson = null;
			else
				this.receiveperson = rs.getString("receiveperson").trim();

			if( rs.getString("ispaperyinv") == null )
				this.ispaperyinv = null;
			else
				this.ispaperyinv = rs.getString("ispaperyinv").trim();

			if( rs.getString("enablestate") == null )
				this.enablestate = null;
			else
				this.enablestate = rs.getString("enablestate").trim();

			if( rs.getString("isearly") == null )
				this.isearly = null;
			else
				this.isearly = rs.getString("isearly").trim();

			if( rs.getString("billmode") == null )
				this.billmode = null;
			else
				this.billmode = rs.getString("billmode").trim();

			if( rs.getString("vaddress") == null )
				this.vaddress = null;
			else
				this.vaddress = rs.getString("vaddress").trim();

			if( rs.getString("openid") == null )
				this.openid = null;
			else
				this.openid = rs.getString("openid").trim();

			if( rs.getString("billaccount") == null )
				this.billaccount = null;
			else
				this.billaccount = rs.getString("billaccount").trim();

			if( rs.getString("email") == null )
				this.email = null;
			else
				this.email = rs.getString("email").trim();

			if( rs.getString("vpsnid") == null )
				this.vpsnid = null;
			else
				this.vpsnid = rs.getString("vpsnid").trim();

			this.veffectdate = rs.getDate("veffectdate");
			this.vautodate = rs.getDate("vautodate");
			if( rs.getString("vmailprovince") == null )
				this.vmailprovince = null;
			else
				this.vmailprovince = rs.getString("vmailprovince").trim();

			if( rs.getString("vmailcity") == null )
				this.vmailcity = null;
			else
				this.vmailcity = rs.getString("vmailcity").trim();

			if( rs.getString("vsrcsystem") == null )
				this.vsrcsystem = null;
			else
				this.vsrcsystem = rs.getString("vsrcsystem").trim();

			if( rs.getString("vsrcrowid") == null )
				this.vsrcrowid = null;
			else
				this.vsrcrowid = rs.getString("vsrcrowid").trim();

			if( rs.getString("group") == null )
				this.group = null;
			else
				this.group = rs.getString("group").trim();

			if( rs.getString("org") == null )
				this.org = null;
			else
				this.org = rs.getString("org").trim();

			if( rs.getString("orgv") == null )
				this.orgv = null;
			else
				this.orgv = rs.getString("orgv").trim();

			if( rs.getString("calnum") == null )
				this.calnum = null;
			else
				this.calnum = rs.getString("calnum").trim();

			this.caldate = rs.getDate("caldate");
			this.vdata = rs.getDate("vdata");
			if( rs.getString("rate") == null )
				this.rate = null;
			else
				this.rate = rs.getString("rate").trim();

			if( rs.getString("ptsitem") == null )
				this.ptsitem = null;
			else
				this.ptsitem = rs.getString("ptsitem").trim();

			this.methodcal = rs.getInt("methodcal");
			if( rs.getString("drawback") == null )
				this.drawback = null;
			else
				this.drawback = rs.getString("drawback").trim();

			if( rs.getString("taxrate") == null )
				this.taxrate = null;
			else
				this.taxrate = rs.getString("taxrate").trim();

			if( rs.getString("oriamt") == null )
				this.oriamt = null;
			else
				this.oriamt = rs.getString("oriamt").trim();

			if( rs.getString("oritax") == null )
				this.oritax = null;
			else
				this.oritax = rs.getString("oritax").trim();

			if( rs.getString("localamt") == null )
				this.localamt = null;
			else
				this.localamt = rs.getString("localamt").trim();

			if( rs.getString("localtax") == null )
				this.localtax = null;
			else
				this.localtax = rs.getString("localtax").trim();

			this.decldate = rs.getDate("decldate");
			this.busidate = rs.getDate("busidate");
			if( rs.getString("custcode") == null )
				this.custcode = null;
			else
				this.custcode = rs.getString("custcode").trim();

			if( rs.getString("custname") == null )
				this.custname = null;
			else
				this.custname = rs.getString("custname").trim();

			if( rs.getString("custtype") == null )
				this.custtype = null;
			else
				this.custtype = rs.getString("custtype").trim();

			this.dectype = rs.getInt("dectype");
			if( rs.getString("srcsystem") == null )
				this.srcsystem = null;
			else
				this.srcsystem = rs.getString("srcsystem").trim();

			if( rs.getString("voucherid") == null )
				this.voucherid = null;
			else
				this.voucherid = rs.getString("voucherid").trim();

			if( rs.getString("transerial") == null )
				this.transerial = null;
			else
				this.transerial = rs.getString("transerial").trim();

			if( rs.getString("tranbatch") == null )
				this.tranbatch = null;
			else
				this.tranbatch = rs.getString("tranbatch").trim();

			this.trandate = rs.getDate("trandate");
			if( rs.getString("trantime") == null )
				this.trantime = null;
			else
				this.trantime = rs.getString("trantime").trim();

			if( rs.getString("tranorg") == null )
				this.tranorg = null;
			else
				this.tranorg = rs.getString("tranorg").trim();

			if( rs.getString("tranchannel") == null )
				this.tranchannel = null;
			else
				this.tranchannel = rs.getString("tranchannel").trim();

			if( rs.getString("trancounte") == null )
				this.trancounte = null;
			else
				this.trancounte = rs.getString("trancounte").trim();

			if( rs.getString("custmanager") == null )
				this.custmanager = null;
			else
				this.custmanager = rs.getString("custmanager").trim();

			if( rs.getString("trantype") == null )
				this.trantype = null;
			else
				this.trantype = rs.getString("trantype").trim();

			if( rs.getString("procode") == null )
				this.procode = null;
			else
				this.procode = rs.getString("procode").trim();

			if( rs.getString("busitype") == null )
				this.busitype = null;
			else
				this.busitype = rs.getString("busitype").trim();

			if( rs.getString("tranremark") == null )
				this.tranremark = null;
			else
				this.tranremark = rs.getString("tranremark").trim();

			if( rs.getString("accountno") == null )
				this.accountno = null;
			else
				this.accountno = rs.getString("accountno").trim();

			if( rs.getString("trancurrency") == null )
				this.trancurrency = null;
			else
				this.trancurrency = rs.getString("trancurrency").trim();

			if( rs.getString("tranamt") == null )
				this.tranamt = null;
			else
				this.tranamt = rs.getString("tranamt").trim();

			this.taxtype = rs.getInt("taxtype");
			if( rs.getString("cztype") == null )
				this.cztype = null;
			else
				this.cztype = rs.getString("cztype").trim();

			this.czolddate = rs.getDate("czolddate");
			if( rs.getString("iscurrent") == null )
				this.iscurrent = null;
			else
				this.iscurrent = rs.getString("iscurrent").trim();

			if( rs.getString("czbusino") == null )
				this.czbusino = null;
			else
				this.czbusino = rs.getString("czbusino").trim();

			if( rs.getString("accno") == null )
				this.accno = null;
			else
				this.accno = rs.getString("accno").trim();

			this.loanflag = rs.getInt("loanflag");
			if( rs.getString("acccode") == null )
				this.acccode = null;
			else
				this.acccode = rs.getString("acccode").trim();

			this.paymentflag = rs.getInt("paymentflag");
			if( rs.getString("amortno") == null )
				this.amortno = null;
			else
				this.amortno = rs.getString("amortno").trim();

			if( rs.getString("midcontno") == null )
				this.midcontno = null;
			else
				this.midcontno = rs.getString("midcontno").trim();

			if( rs.getString("isinnerbank") == null )
				this.isinnerbank = null;
			else
				this.isinnerbank = rs.getString("isinnerbank").trim();

			this.overseasflag = rs.getInt("overseasflag");
			this.taxcalmethod = rs.getInt("taxcalmethod");
			if( rs.getString("creator1") == null )
				this.creator1 = null;
			else
				this.creator1 = rs.getString("creator1").trim();

			if( rs.getString("creationtime1") == null )
				this.creationtime1 = null;
			else
				this.creationtime1 = rs.getString("creationtime1").trim();

			if( rs.getString("modifier1") == null )
				this.modifier1 = null;
			else
				this.modifier1 = rs.getString("modifier1").trim();

			if( rs.getString("modifiedtime1") == null )
				this.modifiedtime1 = null;
			else
				this.modifiedtime1 = rs.getString("modifiedtime1").trim();

			if( rs.getString("negtype") == null )
				this.negtype = null;
			else
				this.negtype = rs.getString("negtype").trim();

			if( rs.getString("tranplace") == null )
				this.tranplace = null;
			else
				this.tranplace = rs.getString("tranplace").trim();

			if( rs.getString("deptdoc") == null )
				this.deptdoc = null;
			else
				this.deptdoc = rs.getString("deptdoc").trim();

			this.confimtype = rs.getInt("confimtype");
			if( rs.getString("ispts") == null )
				this.ispts = null;
			else
				this.ispts = rs.getString("ispts").trim();

			if( rs.getString("isbill") == null )
				this.isbill = null;
			else
				this.isbill = rs.getString("isbill").trim();

			this.areatype = rs.getInt("areatype");
			if( rs.getString("isptserror") == null )
				this.isptserror = null;
			else
				this.isptserror = rs.getString("isptserror").trim();

			this.receivedate = rs.getDate("receivedate");
			if( rs.getString("isalloweadvance") == null )
				this.isalloweadvance = null;
			else
				this.isalloweadvance = rs.getString("isalloweadvance").trim();

			if( rs.getString("busipk") == null )
				this.busipk = null;
			else
				this.busipk = rs.getString("busipk").trim();

			if( rs.getString("busiitem") == null )
				this.busiitem = null;
			else
				this.busiitem = rs.getString("busiitem").trim();

			if( rs.getString("fininsance") == null )
				this.fininsance = null;
			else
				this.fininsance = rs.getString("fininsance").trim();

			if( rs.getString("fininstype") == null )
				this.fininstype = null;
			else
				this.fininstype = rs.getString("fininstype").trim();

			if( rs.getString("istaxfree") == null )
				this.istaxfree = null;
			else
				this.istaxfree = rs.getString("istaxfree").trim();

			if( rs.getString("bunit") == null )
				this.bunit = null;
			else
				this.bunit = rs.getString("bunit").trim();

			if( rs.getString("bunivalent") == null )
				this.bunivalent = null;
			else
				this.bunivalent = rs.getString("bunivalent").trim();

			if( rs.getString("bnumber") == null )
				this.bnumber = null;
			else
				this.bnumber = rs.getString("bnumber").trim();

			if( rs.getString("billtype") == null )
				this.billtype = null;
			else
				this.billtype = rs.getString("billtype").trim();

			this.billdate = rs.getDate("billdate");
			if( rs.getString("servicedoc") == null )
				this.servicedoc = null;
			else
				this.servicedoc = rs.getString("servicedoc").trim();

			if( rs.getString("ishistory") == null )
				this.ishistory = null;
			else
				this.ishistory = rs.getString("ishistory").trim();

			if( rs.getString("ismoneny") == null )
				this.ismoneny = null;
			else
				this.ismoneny = rs.getString("ismoneny").trim();

			if( rs.getString("insureno") == null )
				this.insureno = null;
			else
				this.insureno = rs.getString("insureno").trim();

			if( rs.getString("isfirst") == null )
				this.isfirst = null;
			else
				this.isfirst = rs.getString("isfirst").trim();

			if( rs.getString("perspolicyno") == null )
				this.perspolicyno = null;
			else
				this.perspolicyno = rs.getString("perspolicyno").trim();

			if( rs.getString("ppveridate") == null )
				this.ppveridate = null;
			else
				this.ppveridate = rs.getString("ppveridate").trim();

			this.billstartdate = rs.getDate("billstartdate");
			this.billenddate = rs.getDate("billenddate");
			if( rs.getString("paytype") == null )
				this.paytype = null;
			else
				this.paytype = rs.getString("paytype").trim();

			if( rs.getString("ismat") == null )
				this.ismat = null;
			else
				this.ismat = rs.getString("ismat").trim();

			if( rs.getString("isentrust") == null )
				this.isentrust = null;
			else
				this.isentrust = rs.getString("isentrust").trim();

			this.billeffectivedate = rs.getDate("billeffectivedate");
			this.recoincomdate = rs.getDate("recoincomdate");
			if( rs.getString("billfalg") == null )
				this.billfalg = null;
			else
				this.billfalg = rs.getString("billfalg").trim();

			if( rs.getString("offsetdate") == null )
				this.offsetdate = null;
			else
				this.offsetdate = rs.getString("offsetdate").trim();

			if( rs.getString("bqbatchno") == null )
				this.bqbatchno = null;
			else
				this.bqbatchno = rs.getString("bqbatchno").trim();

			if( rs.getString("issale") == null )
				this.issale = null;
			else
				this.issale = rs.getString("issale").trim();

			if( rs.getString("state") == null )
				this.state = null;
			else
				this.state = rs.getString("state").trim();

			if( rs.getString("printstate") == null )
				this.printstate = null;
			else
				this.printstate = rs.getString("printstate").trim();

			if( rs.getString("moneyno") == null )
				this.moneyno = null;
			else
				this.moneyno = rs.getString("moneyno").trim();

			if( rs.getString("vstatus") == null )
				this.vstatus = null;
			else
				this.vstatus = rs.getString("vstatus").trim();

			if( rs.getString("prebilltype") == null )
				this.prebilltype = null;
			else
				this.prebilltype = rs.getString("prebilltype").trim();

			if( rs.getString("billprintno") == null )
				this.billprintno = null;
			else
				this.billprintno = rs.getString("billprintno").trim();

			this.billprintdate = rs.getDate("billprintdate");
			if( rs.getString("moneytype") == null )
				this.moneytype = null;
			else
				this.moneytype = rs.getString("moneytype").trim();

			if( rs.getString("cvalistate") == null )
				this.cvalistate = null;
			else
				this.cvalistate = rs.getString("cvalistate").trim();

			if( rs.getString("vdef2") == null )
				this.vdef2 = null;
			else
				this.vdef2 = rs.getString("vdef2").trim();

			if( rs.getString("vdef1") == null )
				this.vdef1 = null;
			else
				this.vdef1 = rs.getString("vdef1").trim();

			if( rs.getString("operator") == null )
				this.operator = null;
			else
				this.operator = rs.getString("operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("ErrorInfo") == null )
				this.ErrorInfo = null;
			else
				this.ErrorInfo = rs.getString("ErrorInfo").trim();

			if( rs.getString("vdef6") == null )
				this.vdef6 = null;
			else
				this.vdef6 = rs.getString("vdef6").trim();

			if( rs.getString("vdef7") == null )
				this.vdef7 = null;
			else
				this.vdef7 = rs.getString("vdef7").trim();

			if( rs.getString("vdef8") == null )
				this.vdef8 = null;
			else
				this.vdef8 = rs.getString("vdef8").trim();

			if( rs.getString("TS") == null )
				this.TS = null;
			else
				this.TS = rs.getString("TS").trim();

			if( rs.getString("buymailbox") == null )
				this.buymailbox = null;
			else
				this.buymailbox = rs.getString("buymailbox").trim();

			if( rs.getString("buyphoneno") == null )
				this.buyphoneno = null;
			else
				this.buyphoneno = rs.getString("buyphoneno").trim();

			if( rs.getString("buyidtype") == null )
				this.buyidtype = null;
			else
				this.buyidtype = rs.getString("buyidtype").trim();

			if( rs.getString("buyidnumber") == null )
				this.buyidnumber = null;
			else
				this.buyidnumber = rs.getString("buyidnumber").trim();

			if( rs.getString("invoiceCode") == null )
				this.invoiceCode = null;
			else
				this.invoiceCode = rs.getString("invoiceCode").trim();

			if( rs.getString("checkCode") == null )
				this.checkCode = null;
			else
				this.checkCode = rs.getString("checkCode").trim();

			if( rs.getString("shortLink") == null )
				this.shortLink = null;
			else
				this.shortLink = rs.getString("shortLink").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LYOutPayDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LYOutPayDetailSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LYOutPayDetailSchema getSchema()
	{
		LYOutPayDetailSchema aLYOutPayDetailSchema = new LYOutPayDetailSchema();
		aLYOutPayDetailSchema.setSchema(this);
		return aLYOutPayDetailSchema;
	}

	public LYOutPayDetailDB getDB()
	{
		LYOutPayDetailDB aDBOper = new LYOutPayDetailDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYOutPayDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BusiNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isupdate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(pkgroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(pkorg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(code)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(creator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(creationtime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifier)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifiedtime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(taxpayertype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(invtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makefreq)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makespace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(telephone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sendway)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(addressee)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(postcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(mailingaddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(customertype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(country)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(legalperson)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(taxnumber)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bank)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankaccount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makeinvdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(receiveperson)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ispaperyinv)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(enablestate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isearly)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(billmode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vaddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(openid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(billaccount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(email)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vpsnid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( veffectdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( vautodate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vmailprovince)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vmailcity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vsrcsystem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vsrcrowid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(group)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(org)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(orgv)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(calnum)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( caldate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( vdata ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(rate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ptsitem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(methodcal));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(drawback)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(taxrate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(oriamt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(oritax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(localamt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(localtax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( decldate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( busidate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(custcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(custname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(custtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(dectype));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(srcsystem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(voucherid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(transerial)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tranbatch)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( trandate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(trantime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tranorg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tranchannel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(trancounte)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(custmanager)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(trantype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(procode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(busitype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tranremark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(accountno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(trancurrency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tranamt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(taxtype));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cztype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( czolddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(iscurrent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(czbusino)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(accno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(loanflag));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(acccode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(paymentflag));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(amortno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(midcontno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isinnerbank)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(overseasflag));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(taxcalmethod));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(creator1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(creationtime1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifier1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifiedtime1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(negtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tranplace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(deptdoc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(confimtype));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ispts)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isbill)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(areatype));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isptserror)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( receivedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isalloweadvance)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(busipk)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(busiitem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fininsance)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fininstype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(istaxfree)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bunit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bunivalent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bnumber)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(billtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( billdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(servicedoc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ishistory)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ismoneny)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insureno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isfirst)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(perspolicyno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ppveridate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( billstartdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( billenddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paytype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ismat)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isentrust)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( billeffectivedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( recoincomdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(billfalg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(offsetdate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bqbatchno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(issale)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(state)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(printstate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(moneyno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vstatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(prebilltype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(billprintno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( billprintdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(moneytype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cvalistate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vdef2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vdef1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorInfo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vdef6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vdef7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(vdef8)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TS)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(buymailbox)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(buyphoneno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(buyidtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(buyidnumber)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(invoiceCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(checkCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(shortLink));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYOutPayDetail>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BusiNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			isupdate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			pkgroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			pkorg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			creator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			creationtime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			modifier = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			modifiedtime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			taxpayertype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			invtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			makefreq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			makespace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			telephone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			sendway = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			addressee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			postcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			mailingaddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			customertype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			country = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			legalperson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			taxnumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			bank = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			bankaccount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			makeinvdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			receiveperson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ispaperyinv = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			enablestate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			isearly = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			billmode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			vaddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			openid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			billaccount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			vpsnid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			veffectdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			vautodate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
			vmailprovince = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			vmailcity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			vsrcsystem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			vsrcrowid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			group = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			org = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			orgv = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			calnum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			caldate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49,SysConst.PACKAGESPILTER));
			vdata = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50,SysConst.PACKAGESPILTER));
			rate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			ptsitem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			methodcal= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,53,SysConst.PACKAGESPILTER))).intValue();
			drawback = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			taxrate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			oriamt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			oritax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			localamt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			localtax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			decldate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60,SysConst.PACKAGESPILTER));
			busidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61,SysConst.PACKAGESPILTER));
			custcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			custname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			custtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			dectype= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,65,SysConst.PACKAGESPILTER))).intValue();
			srcsystem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			voucherid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			transerial = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
			tranbatch = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
			trandate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70,SysConst.PACKAGESPILTER));
			trantime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
			tranorg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
			tranchannel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
			trancounte = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74, SysConst.PACKAGESPILTER );
			custmanager = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
			trantype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER );
			procode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
			busitype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78, SysConst.PACKAGESPILTER );
			tranremark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79, SysConst.PACKAGESPILTER );
			accountno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80, SysConst.PACKAGESPILTER );
			trancurrency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81, SysConst.PACKAGESPILTER );
			tranamt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 82, SysConst.PACKAGESPILTER );
			taxtype= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,83,SysConst.PACKAGESPILTER))).intValue();
			cztype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 84, SysConst.PACKAGESPILTER );
			czolddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 85,SysConst.PACKAGESPILTER));
			iscurrent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 86, SysConst.PACKAGESPILTER );
			czbusino = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 87, SysConst.PACKAGESPILTER );
			accno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 88, SysConst.PACKAGESPILTER );
			loanflag= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,89,SysConst.PACKAGESPILTER))).intValue();
			acccode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 90, SysConst.PACKAGESPILTER );
			paymentflag= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,91,SysConst.PACKAGESPILTER))).intValue();
			amortno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 92, SysConst.PACKAGESPILTER );
			midcontno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 93, SysConst.PACKAGESPILTER );
			isinnerbank = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 94, SysConst.PACKAGESPILTER );
			overseasflag= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,95,SysConst.PACKAGESPILTER))).intValue();
			taxcalmethod= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,96,SysConst.PACKAGESPILTER))).intValue();
			creator1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 97, SysConst.PACKAGESPILTER );
			creationtime1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 98, SysConst.PACKAGESPILTER );
			modifier1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 99, SysConst.PACKAGESPILTER );
			modifiedtime1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 100, SysConst.PACKAGESPILTER );
			negtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 101, SysConst.PACKAGESPILTER );
			tranplace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 102, SysConst.PACKAGESPILTER );
			deptdoc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 103, SysConst.PACKAGESPILTER );
			confimtype= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,104,SysConst.PACKAGESPILTER))).intValue();
			ispts = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 105, SysConst.PACKAGESPILTER );
			isbill = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 106, SysConst.PACKAGESPILTER );
			areatype= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,107,SysConst.PACKAGESPILTER))).intValue();
			isptserror = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 108, SysConst.PACKAGESPILTER );
			receivedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 109,SysConst.PACKAGESPILTER));
			isalloweadvance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 110, SysConst.PACKAGESPILTER );
			busipk = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 111, SysConst.PACKAGESPILTER );
			busiitem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 112, SysConst.PACKAGESPILTER );
			fininsance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 113, SysConst.PACKAGESPILTER );
			fininstype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 114, SysConst.PACKAGESPILTER );
			istaxfree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 115, SysConst.PACKAGESPILTER );
			bunit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 116, SysConst.PACKAGESPILTER );
			bunivalent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 117, SysConst.PACKAGESPILTER );
			bnumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 118, SysConst.PACKAGESPILTER );
			billtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 119, SysConst.PACKAGESPILTER );
			billdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 120,SysConst.PACKAGESPILTER));
			servicedoc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 121, SysConst.PACKAGESPILTER );
			ishistory = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 122, SysConst.PACKAGESPILTER );
			ismoneny = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 123, SysConst.PACKAGESPILTER );
			insureno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 124, SysConst.PACKAGESPILTER );
			isfirst = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 125, SysConst.PACKAGESPILTER );
			perspolicyno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 126, SysConst.PACKAGESPILTER );
			ppveridate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 127, SysConst.PACKAGESPILTER );
			billstartdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 128,SysConst.PACKAGESPILTER));
			billenddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 129,SysConst.PACKAGESPILTER));
			paytype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 130, SysConst.PACKAGESPILTER );
			ismat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 131, SysConst.PACKAGESPILTER );
			isentrust = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 132, SysConst.PACKAGESPILTER );
			billeffectivedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 133,SysConst.PACKAGESPILTER));
			recoincomdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 134,SysConst.PACKAGESPILTER));
			billfalg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 135, SysConst.PACKAGESPILTER );
			offsetdate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 136, SysConst.PACKAGESPILTER );
			bqbatchno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 137, SysConst.PACKAGESPILTER );
			issale = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 138, SysConst.PACKAGESPILTER );
			state = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 139, SysConst.PACKAGESPILTER );
			printstate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 140, SysConst.PACKAGESPILTER );
			moneyno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 141, SysConst.PACKAGESPILTER );
			vstatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 142, SysConst.PACKAGESPILTER );
			prebilltype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 143, SysConst.PACKAGESPILTER );
			billprintno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 144, SysConst.PACKAGESPILTER );
			billprintdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 145,SysConst.PACKAGESPILTER));
			moneytype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 146, SysConst.PACKAGESPILTER );
			cvalistate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 147, SysConst.PACKAGESPILTER );
			vdef2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 148, SysConst.PACKAGESPILTER );
			vdef1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 149, SysConst.PACKAGESPILTER );
			operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 150, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 151,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 152, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 153,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 154, SysConst.PACKAGESPILTER );
			ErrorInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 155, SysConst.PACKAGESPILTER );
			vdef6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 156, SysConst.PACKAGESPILTER );
			vdef7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 157, SysConst.PACKAGESPILTER );
			vdef8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 158, SysConst.PACKAGESPILTER );
			TS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 159, SysConst.PACKAGESPILTER );
			buymailbox = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 160, SysConst.PACKAGESPILTER );
			buyphoneno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 161, SysConst.PACKAGESPILTER );
			buyidtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 162, SysConst.PACKAGESPILTER );
			buyidnumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 163, SysConst.PACKAGESPILTER );
			invoiceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 164, SysConst.PACKAGESPILTER );
			checkCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 165, SysConst.PACKAGESPILTER );
			shortLink = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 166, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LYOutPayDetailSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BusiNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusiNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("isupdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isupdate));
		}
		if (FCode.equals("pkgroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(pkgroup));
		}
		if (FCode.equals("pkorg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(pkorg));
		}
		if (FCode.equals("code"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(code));
		}
		if (FCode.equals("name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(name));
		}
		if (FCode.equals("creator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(creator));
		}
		if (FCode.equals("creationtime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(creationtime));
		}
		if (FCode.equals("modifier"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifier));
		}
		if (FCode.equals("modifiedtime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifiedtime));
		}
		if (FCode.equals("taxpayertype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(taxpayertype));
		}
		if (FCode.equals("invtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(invtype));
		}
		if (FCode.equals("makefreq"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makefreq));
		}
		if (FCode.equals("makespace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makespace));
		}
		if (FCode.equals("telephone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(telephone));
		}
		if (FCode.equals("mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(mobile));
		}
		if (FCode.equals("sendway"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sendway));
		}
		if (FCode.equals("addressee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(addressee));
		}
		if (FCode.equals("postcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(postcode));
		}
		if (FCode.equals("mailingaddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(mailingaddress));
		}
		if (FCode.equals("customertype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(customertype));
		}
		if (FCode.equals("country"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(country));
		}
		if (FCode.equals("legalperson"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(legalperson));
		}
		if (FCode.equals("taxnumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(taxnumber));
		}
		if (FCode.equals("bank"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bank));
		}
		if (FCode.equals("bankaccount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankaccount));
		}
		if (FCode.equals("makeinvdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakeinvdate()));
		}
		if (FCode.equals("receiveperson"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(receiveperson));
		}
		if (FCode.equals("ispaperyinv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ispaperyinv));
		}
		if (FCode.equals("enablestate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(enablestate));
		}
		if (FCode.equals("isearly"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isearly));
		}
		if (FCode.equals("billmode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(billmode));
		}
		if (FCode.equals("vaddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vaddress));
		}
		if (FCode.equals("openid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(openid));
		}
		if (FCode.equals("billaccount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(billaccount));
		}
		if (FCode.equals("email"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(email));
		}
		if (FCode.equals("vpsnid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vpsnid));
		}
		if (FCode.equals("veffectdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getveffectdate()));
		}
		if (FCode.equals("vautodate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getvautodate()));
		}
		if (FCode.equals("vmailprovince"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vmailprovince));
		}
		if (FCode.equals("vmailcity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vmailcity));
		}
		if (FCode.equals("vsrcsystem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vsrcsystem));
		}
		if (FCode.equals("vsrcrowid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vsrcrowid));
		}
		if (FCode.equals("group"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(group));
		}
		if (FCode.equals("org"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(org));
		}
		if (FCode.equals("orgv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(orgv));
		}
		if (FCode.equals("calnum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(calnum));
		}
		if (FCode.equals("caldate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getcaldate()));
		}
		if (FCode.equals("vdata"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getvdata()));
		}
		if (FCode.equals("rate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(rate));
		}
		if (FCode.equals("ptsitem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ptsitem));
		}
		if (FCode.equals("methodcal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(methodcal));
		}
		if (FCode.equals("drawback"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(drawback));
		}
		if (FCode.equals("taxrate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(taxrate));
		}
		if (FCode.equals("oriamt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(oriamt));
		}
		if (FCode.equals("oritax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(oritax));
		}
		if (FCode.equals("localamt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(localamt));
		}
		if (FCode.equals("localtax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(localtax));
		}
		if (FCode.equals("decldate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getdecldate()));
		}
		if (FCode.equals("busidate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getbusidate()));
		}
		if (FCode.equals("custcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(custcode));
		}
		if (FCode.equals("custname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(custname));
		}
		if (FCode.equals("custtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(custtype));
		}
		if (FCode.equals("dectype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(dectype));
		}
		if (FCode.equals("srcsystem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(srcsystem));
		}
		if (FCode.equals("voucherid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(voucherid));
		}
		if (FCode.equals("transerial"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(transerial));
		}
		if (FCode.equals("tranbatch"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tranbatch));
		}
		if (FCode.equals("trandate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.gettrandate()));
		}
		if (FCode.equals("trantime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(trantime));
		}
		if (FCode.equals("tranorg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tranorg));
		}
		if (FCode.equals("tranchannel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tranchannel));
		}
		if (FCode.equals("trancounte"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(trancounte));
		}
		if (FCode.equals("custmanager"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(custmanager));
		}
		if (FCode.equals("trantype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(trantype));
		}
		if (FCode.equals("procode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(procode));
		}
		if (FCode.equals("busitype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(busitype));
		}
		if (FCode.equals("tranremark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tranremark));
		}
		if (FCode.equals("accountno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(accountno));
		}
		if (FCode.equals("trancurrency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(trancurrency));
		}
		if (FCode.equals("tranamt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tranamt));
		}
		if (FCode.equals("taxtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(taxtype));
		}
		if (FCode.equals("cztype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cztype));
		}
		if (FCode.equals("czolddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getczolddate()));
		}
		if (FCode.equals("iscurrent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(iscurrent));
		}
		if (FCode.equals("czbusino"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(czbusino));
		}
		if (FCode.equals("accno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(accno));
		}
		if (FCode.equals("loanflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(loanflag));
		}
		if (FCode.equals("acccode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(acccode));
		}
		if (FCode.equals("paymentflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paymentflag));
		}
		if (FCode.equals("amortno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(amortno));
		}
		if (FCode.equals("midcontno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(midcontno));
		}
		if (FCode.equals("isinnerbank"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isinnerbank));
		}
		if (FCode.equals("overseasflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(overseasflag));
		}
		if (FCode.equals("taxcalmethod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(taxcalmethod));
		}
		if (FCode.equals("creator1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(creator1));
		}
		if (FCode.equals("creationtime1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(creationtime1));
		}
		if (FCode.equals("modifier1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifier1));
		}
		if (FCode.equals("modifiedtime1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifiedtime1));
		}
		if (FCode.equals("negtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(negtype));
		}
		if (FCode.equals("tranplace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tranplace));
		}
		if (FCode.equals("deptdoc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(deptdoc));
		}
		if (FCode.equals("confimtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(confimtype));
		}
		if (FCode.equals("ispts"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ispts));
		}
		if (FCode.equals("isbill"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isbill));
		}
		if (FCode.equals("areatype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(areatype));
		}
		if (FCode.equals("isptserror"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isptserror));
		}
		if (FCode.equals("receivedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getreceivedate()));
		}
		if (FCode.equals("isalloweadvance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isalloweadvance));
		}
		if (FCode.equals("busipk"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(busipk));
		}
		if (FCode.equals("busiitem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(busiitem));
		}
		if (FCode.equals("fininsance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(fininsance));
		}
		if (FCode.equals("fininstype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(fininstype));
		}
		if (FCode.equals("istaxfree"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(istaxfree));
		}
		if (FCode.equals("bunit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bunit));
		}
		if (FCode.equals("bunivalent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bunivalent));
		}
		if (FCode.equals("bnumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bnumber));
		}
		if (FCode.equals("billtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(billtype));
		}
		if (FCode.equals("billdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getbilldate()));
		}
		if (FCode.equals("servicedoc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(servicedoc));
		}
		if (FCode.equals("ishistory"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ishistory));
		}
		if (FCode.equals("ismoneny"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ismoneny));
		}
		if (FCode.equals("insureno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insureno));
		}
		if (FCode.equals("isfirst"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isfirst));
		}
		if (FCode.equals("perspolicyno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(perspolicyno));
		}
		if (FCode.equals("ppveridate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ppveridate));
		}
		if (FCode.equals("billstartdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getbillstartdate()));
		}
		if (FCode.equals("billenddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getbillenddate()));
		}
		if (FCode.equals("paytype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paytype));
		}
		if (FCode.equals("ismat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ismat));
		}
		if (FCode.equals("isentrust"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isentrust));
		}
		if (FCode.equals("billeffectivedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getbilleffectivedate()));
		}
		if (FCode.equals("recoincomdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getrecoincomdate()));
		}
		if (FCode.equals("billfalg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(billfalg));
		}
		if (FCode.equals("offsetdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(offsetdate));
		}
		if (FCode.equals("bqbatchno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bqbatchno));
		}
		if (FCode.equals("issale"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(issale));
		}
		if (FCode.equals("state"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(state));
		}
		if (FCode.equals("printstate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(printstate));
		}
		if (FCode.equals("moneyno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(moneyno));
		}
		if (FCode.equals("vstatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vstatus));
		}
		if (FCode.equals("prebilltype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prebilltype));
		}
		if (FCode.equals("billprintno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(billprintno));
		}
		if (FCode.equals("billprintdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getbillprintdate()));
		}
		if (FCode.equals("moneytype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(moneytype));
		}
		if (FCode.equals("cvalistate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cvalistate));
		}
		if (FCode.equals("vdef2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vdef2));
		}
		if (FCode.equals("vdef1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vdef1));
		}
		if (FCode.equals("operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ErrorInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorInfo));
		}
		if (FCode.equals("vdef6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vdef6));
		}
		if (FCode.equals("vdef7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vdef7));
		}
		if (FCode.equals("vdef8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(vdef8));
		}
		if (FCode.equals("TS"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TS));
		}
		if (FCode.equals("buymailbox"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(buymailbox));
		}
		if (FCode.equals("buyphoneno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(buyphoneno));
		}
		if (FCode.equals("buyidtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(buyidtype));
		}
		if (FCode.equals("buyidnumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(buyidnumber));
		}
		if (FCode.equals("invoiceCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(invoiceCode));
		}
		if (FCode.equals("checkCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(checkCode));
		}
		if (FCode.equals("shortLink"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(shortLink));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BusiNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(isupdate);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(pkgroup);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(pkorg);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(code);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(name);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(creator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(creationtime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(modifier);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(modifiedtime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(taxpayertype);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(invtype);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(makefreq);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(makespace);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(telephone);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(mobile);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(sendway);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(addressee);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(postcode);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(mailingaddress);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(customertype);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(country);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(legalperson);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(taxnumber);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(bank);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(bankaccount);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakeinvdate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(receiveperson);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(ispaperyinv);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(enablestate);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(isearly);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(billmode);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(vaddress);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(openid);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(billaccount);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(email);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(vpsnid);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getveffectdate()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getvautodate()));
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(vmailprovince);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(vmailcity);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(vsrcsystem);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(vsrcrowid);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(group);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(org);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(orgv);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(calnum);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getcaldate()));
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getvdata()));
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(rate);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(ptsitem);
				break;
			case 52:
				strFieldValue = String.valueOf(methodcal);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(drawback);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(taxrate);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(oriamt);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(oritax);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(localamt);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(localtax);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getdecldate()));
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getbusidate()));
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(custcode);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(custname);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(custtype);
				break;
			case 64:
				strFieldValue = String.valueOf(dectype);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(srcsystem);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(voucherid);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(transerial);
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(tranbatch);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.gettrandate()));
				break;
			case 70:
				strFieldValue = StrTool.GBKToUnicode(trantime);
				break;
			case 71:
				strFieldValue = StrTool.GBKToUnicode(tranorg);
				break;
			case 72:
				strFieldValue = StrTool.GBKToUnicode(tranchannel);
				break;
			case 73:
				strFieldValue = StrTool.GBKToUnicode(trancounte);
				break;
			case 74:
				strFieldValue = StrTool.GBKToUnicode(custmanager);
				break;
			case 75:
				strFieldValue = StrTool.GBKToUnicode(trantype);
				break;
			case 76:
				strFieldValue = StrTool.GBKToUnicode(procode);
				break;
			case 77:
				strFieldValue = StrTool.GBKToUnicode(busitype);
				break;
			case 78:
				strFieldValue = StrTool.GBKToUnicode(tranremark);
				break;
			case 79:
				strFieldValue = StrTool.GBKToUnicode(accountno);
				break;
			case 80:
				strFieldValue = StrTool.GBKToUnicode(trancurrency);
				break;
			case 81:
				strFieldValue = StrTool.GBKToUnicode(tranamt);
				break;
			case 82:
				strFieldValue = String.valueOf(taxtype);
				break;
			case 83:
				strFieldValue = StrTool.GBKToUnicode(cztype);
				break;
			case 84:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getczolddate()));
				break;
			case 85:
				strFieldValue = StrTool.GBKToUnicode(iscurrent);
				break;
			case 86:
				strFieldValue = StrTool.GBKToUnicode(czbusino);
				break;
			case 87:
				strFieldValue = StrTool.GBKToUnicode(accno);
				break;
			case 88:
				strFieldValue = String.valueOf(loanflag);
				break;
			case 89:
				strFieldValue = StrTool.GBKToUnicode(acccode);
				break;
			case 90:
				strFieldValue = String.valueOf(paymentflag);
				break;
			case 91:
				strFieldValue = StrTool.GBKToUnicode(amortno);
				break;
			case 92:
				strFieldValue = StrTool.GBKToUnicode(midcontno);
				break;
			case 93:
				strFieldValue = StrTool.GBKToUnicode(isinnerbank);
				break;
			case 94:
				strFieldValue = String.valueOf(overseasflag);
				break;
			case 95:
				strFieldValue = String.valueOf(taxcalmethod);
				break;
			case 96:
				strFieldValue = StrTool.GBKToUnicode(creator1);
				break;
			case 97:
				strFieldValue = StrTool.GBKToUnicode(creationtime1);
				break;
			case 98:
				strFieldValue = StrTool.GBKToUnicode(modifier1);
				break;
			case 99:
				strFieldValue = StrTool.GBKToUnicode(modifiedtime1);
				break;
			case 100:
				strFieldValue = StrTool.GBKToUnicode(negtype);
				break;
			case 101:
				strFieldValue = StrTool.GBKToUnicode(tranplace);
				break;
			case 102:
				strFieldValue = StrTool.GBKToUnicode(deptdoc);
				break;
			case 103:
				strFieldValue = String.valueOf(confimtype);
				break;
			case 104:
				strFieldValue = StrTool.GBKToUnicode(ispts);
				break;
			case 105:
				strFieldValue = StrTool.GBKToUnicode(isbill);
				break;
			case 106:
				strFieldValue = String.valueOf(areatype);
				break;
			case 107:
				strFieldValue = StrTool.GBKToUnicode(isptserror);
				break;
			case 108:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getreceivedate()));
				break;
			case 109:
				strFieldValue = StrTool.GBKToUnicode(isalloweadvance);
				break;
			case 110:
				strFieldValue = StrTool.GBKToUnicode(busipk);
				break;
			case 111:
				strFieldValue = StrTool.GBKToUnicode(busiitem);
				break;
			case 112:
				strFieldValue = StrTool.GBKToUnicode(fininsance);
				break;
			case 113:
				strFieldValue = StrTool.GBKToUnicode(fininstype);
				break;
			case 114:
				strFieldValue = StrTool.GBKToUnicode(istaxfree);
				break;
			case 115:
				strFieldValue = StrTool.GBKToUnicode(bunit);
				break;
			case 116:
				strFieldValue = StrTool.GBKToUnicode(bunivalent);
				break;
			case 117:
				strFieldValue = StrTool.GBKToUnicode(bnumber);
				break;
			case 118:
				strFieldValue = StrTool.GBKToUnicode(billtype);
				break;
			case 119:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getbilldate()));
				break;
			case 120:
				strFieldValue = StrTool.GBKToUnicode(servicedoc);
				break;
			case 121:
				strFieldValue = StrTool.GBKToUnicode(ishistory);
				break;
			case 122:
				strFieldValue = StrTool.GBKToUnicode(ismoneny);
				break;
			case 123:
				strFieldValue = StrTool.GBKToUnicode(insureno);
				break;
			case 124:
				strFieldValue = StrTool.GBKToUnicode(isfirst);
				break;
			case 125:
				strFieldValue = StrTool.GBKToUnicode(perspolicyno);
				break;
			case 126:
				strFieldValue = StrTool.GBKToUnicode(ppveridate);
				break;
			case 127:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getbillstartdate()));
				break;
			case 128:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getbillenddate()));
				break;
			case 129:
				strFieldValue = StrTool.GBKToUnicode(paytype);
				break;
			case 130:
				strFieldValue = StrTool.GBKToUnicode(ismat);
				break;
			case 131:
				strFieldValue = StrTool.GBKToUnicode(isentrust);
				break;
			case 132:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getbilleffectivedate()));
				break;
			case 133:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getrecoincomdate()));
				break;
			case 134:
				strFieldValue = StrTool.GBKToUnicode(billfalg);
				break;
			case 135:
				strFieldValue = StrTool.GBKToUnicode(offsetdate);
				break;
			case 136:
				strFieldValue = StrTool.GBKToUnicode(bqbatchno);
				break;
			case 137:
				strFieldValue = StrTool.GBKToUnicode(issale);
				break;
			case 138:
				strFieldValue = StrTool.GBKToUnicode(state);
				break;
			case 139:
				strFieldValue = StrTool.GBKToUnicode(printstate);
				break;
			case 140:
				strFieldValue = StrTool.GBKToUnicode(moneyno);
				break;
			case 141:
				strFieldValue = StrTool.GBKToUnicode(vstatus);
				break;
			case 142:
				strFieldValue = StrTool.GBKToUnicode(prebilltype);
				break;
			case 143:
				strFieldValue = StrTool.GBKToUnicode(billprintno);
				break;
			case 144:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getbillprintdate()));
				break;
			case 145:
				strFieldValue = StrTool.GBKToUnicode(moneytype);
				break;
			case 146:
				strFieldValue = StrTool.GBKToUnicode(cvalistate);
				break;
			case 147:
				strFieldValue = StrTool.GBKToUnicode(vdef2);
				break;
			case 148:
				strFieldValue = StrTool.GBKToUnicode(vdef1);
				break;
			case 149:
				strFieldValue = StrTool.GBKToUnicode(operator);
				break;
			case 150:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 151:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 152:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 153:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 154:
				strFieldValue = StrTool.GBKToUnicode(ErrorInfo);
				break;
			case 155:
				strFieldValue = StrTool.GBKToUnicode(vdef6);
				break;
			case 156:
				strFieldValue = StrTool.GBKToUnicode(vdef7);
				break;
			case 157:
				strFieldValue = StrTool.GBKToUnicode(vdef8);
				break;
			case 158:
				strFieldValue = StrTool.GBKToUnicode(TS);
				break;
			case 159:
				strFieldValue = StrTool.GBKToUnicode(buymailbox);
				break;
			case 160:
				strFieldValue = StrTool.GBKToUnicode(buyphoneno);
				break;
			case 161:
				strFieldValue = StrTool.GBKToUnicode(buyidtype);
				break;
			case 162:
				strFieldValue = StrTool.GBKToUnicode(buyidnumber);
				break;
			case 163:
				strFieldValue = StrTool.GBKToUnicode(invoiceCode);
				break;
			case 164:
				strFieldValue = StrTool.GBKToUnicode(checkCode);
				break;
			case 165:
				strFieldValue = StrTool.GBKToUnicode(shortLink);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BusiNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusiNo = FValue.trim();
			}
			else
				BusiNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("isupdate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isupdate = FValue.trim();
			}
			else
				isupdate = null;
		}
		if (FCode.equalsIgnoreCase("pkgroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				pkgroup = FValue.trim();
			}
			else
				pkgroup = null;
		}
		if (FCode.equalsIgnoreCase("pkorg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				pkorg = FValue.trim();
			}
			else
				pkorg = null;
		}
		if (FCode.equalsIgnoreCase("code"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				code = FValue.trim();
			}
			else
				code = null;
		}
		if (FCode.equalsIgnoreCase("name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				name = FValue.trim();
			}
			else
				name = null;
		}
		if (FCode.equalsIgnoreCase("creator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				creator = FValue.trim();
			}
			else
				creator = null;
		}
		if (FCode.equalsIgnoreCase("creationtime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				creationtime = FValue.trim();
			}
			else
				creationtime = null;
		}
		if (FCode.equalsIgnoreCase("modifier"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifier = FValue.trim();
			}
			else
				modifier = null;
		}
		if (FCode.equalsIgnoreCase("modifiedtime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifiedtime = FValue.trim();
			}
			else
				modifiedtime = null;
		}
		if (FCode.equalsIgnoreCase("taxpayertype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				taxpayertype = FValue.trim();
			}
			else
				taxpayertype = null;
		}
		if (FCode.equalsIgnoreCase("invtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				invtype = FValue.trim();
			}
			else
				invtype = null;
		}
		if (FCode.equalsIgnoreCase("makefreq"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makefreq = FValue.trim();
			}
			else
				makefreq = null;
		}
		if (FCode.equalsIgnoreCase("makespace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makespace = FValue.trim();
			}
			else
				makespace = null;
		}
		if (FCode.equalsIgnoreCase("telephone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				telephone = FValue.trim();
			}
			else
				telephone = null;
		}
		if (FCode.equalsIgnoreCase("mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				mobile = FValue.trim();
			}
			else
				mobile = null;
		}
		if (FCode.equalsIgnoreCase("sendway"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sendway = FValue.trim();
			}
			else
				sendway = null;
		}
		if (FCode.equalsIgnoreCase("addressee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				addressee = FValue.trim();
			}
			else
				addressee = null;
		}
		if (FCode.equalsIgnoreCase("postcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				postcode = FValue.trim();
			}
			else
				postcode = null;
		}
		if (FCode.equalsIgnoreCase("mailingaddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				mailingaddress = FValue.trim();
			}
			else
				mailingaddress = null;
		}
		if (FCode.equalsIgnoreCase("customertype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				customertype = FValue.trim();
			}
			else
				customertype = null;
		}
		if (FCode.equalsIgnoreCase("country"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				country = FValue.trim();
			}
			else
				country = null;
		}
		if (FCode.equalsIgnoreCase("legalperson"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				legalperson = FValue.trim();
			}
			else
				legalperson = null;
		}
		if (FCode.equalsIgnoreCase("taxnumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				taxnumber = FValue.trim();
			}
			else
				taxnumber = null;
		}
		if (FCode.equalsIgnoreCase("bank"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bank = FValue.trim();
			}
			else
				bank = null;
		}
		if (FCode.equalsIgnoreCase("bankaccount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankaccount = FValue.trim();
			}
			else
				bankaccount = null;
		}
		if (FCode.equalsIgnoreCase("makeinvdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makeinvdate = fDate.getDate( FValue );
			}
			else
				makeinvdate = null;
		}
		if (FCode.equalsIgnoreCase("receiveperson"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				receiveperson = FValue.trim();
			}
			else
				receiveperson = null;
		}
		if (FCode.equalsIgnoreCase("ispaperyinv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ispaperyinv = FValue.trim();
			}
			else
				ispaperyinv = null;
		}
		if (FCode.equalsIgnoreCase("enablestate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				enablestate = FValue.trim();
			}
			else
				enablestate = null;
		}
		if (FCode.equalsIgnoreCase("isearly"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isearly = FValue.trim();
			}
			else
				isearly = null;
		}
		if (FCode.equalsIgnoreCase("billmode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				billmode = FValue.trim();
			}
			else
				billmode = null;
		}
		if (FCode.equalsIgnoreCase("vaddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vaddress = FValue.trim();
			}
			else
				vaddress = null;
		}
		if (FCode.equalsIgnoreCase("openid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				openid = FValue.trim();
			}
			else
				openid = null;
		}
		if (FCode.equalsIgnoreCase("billaccount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				billaccount = FValue.trim();
			}
			else
				billaccount = null;
		}
		if (FCode.equalsIgnoreCase("email"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				email = FValue.trim();
			}
			else
				email = null;
		}
		if (FCode.equalsIgnoreCase("vpsnid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vpsnid = FValue.trim();
			}
			else
				vpsnid = null;
		}
		if (FCode.equalsIgnoreCase("veffectdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				veffectdate = fDate.getDate( FValue );
			}
			else
				veffectdate = null;
		}
		if (FCode.equalsIgnoreCase("vautodate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				vautodate = fDate.getDate( FValue );
			}
			else
				vautodate = null;
		}
		if (FCode.equalsIgnoreCase("vmailprovince"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vmailprovince = FValue.trim();
			}
			else
				vmailprovince = null;
		}
		if (FCode.equalsIgnoreCase("vmailcity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vmailcity = FValue.trim();
			}
			else
				vmailcity = null;
		}
		if (FCode.equalsIgnoreCase("vsrcsystem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vsrcsystem = FValue.trim();
			}
			else
				vsrcsystem = null;
		}
		if (FCode.equalsIgnoreCase("vsrcrowid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vsrcrowid = FValue.trim();
			}
			else
				vsrcrowid = null;
		}
		if (FCode.equalsIgnoreCase("group"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				group = FValue.trim();
			}
			else
				group = null;
		}
		if (FCode.equalsIgnoreCase("org"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				org = FValue.trim();
			}
			else
				org = null;
		}
		if (FCode.equalsIgnoreCase("orgv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				orgv = FValue.trim();
			}
			else
				orgv = null;
		}
		if (FCode.equalsIgnoreCase("calnum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				calnum = FValue.trim();
			}
			else
				calnum = null;
		}
		if (FCode.equalsIgnoreCase("caldate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				caldate = fDate.getDate( FValue );
			}
			else
				caldate = null;
		}
		if (FCode.equalsIgnoreCase("vdata"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				vdata = fDate.getDate( FValue );
			}
			else
				vdata = null;
		}
		if (FCode.equalsIgnoreCase("rate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				rate = FValue.trim();
			}
			else
				rate = null;
		}
		if (FCode.equalsIgnoreCase("ptsitem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ptsitem = FValue.trim();
			}
			else
				ptsitem = null;
		}
		if (FCode.equalsIgnoreCase("methodcal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				methodcal = i;
			}
		}
		if (FCode.equalsIgnoreCase("drawback"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				drawback = FValue.trim();
			}
			else
				drawback = null;
		}
		if (FCode.equalsIgnoreCase("taxrate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				taxrate = FValue.trim();
			}
			else
				taxrate = null;
		}
		if (FCode.equalsIgnoreCase("oriamt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				oriamt = FValue.trim();
			}
			else
				oriamt = null;
		}
		if (FCode.equalsIgnoreCase("oritax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				oritax = FValue.trim();
			}
			else
				oritax = null;
		}
		if (FCode.equalsIgnoreCase("localamt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				localamt = FValue.trim();
			}
			else
				localamt = null;
		}
		if (FCode.equalsIgnoreCase("localtax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				localtax = FValue.trim();
			}
			else
				localtax = null;
		}
		if (FCode.equalsIgnoreCase("decldate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				decldate = fDate.getDate( FValue );
			}
			else
				decldate = null;
		}
		if (FCode.equalsIgnoreCase("busidate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				busidate = fDate.getDate( FValue );
			}
			else
				busidate = null;
		}
		if (FCode.equalsIgnoreCase("custcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				custcode = FValue.trim();
			}
			else
				custcode = null;
		}
		if (FCode.equalsIgnoreCase("custname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				custname = FValue.trim();
			}
			else
				custname = null;
		}
		if (FCode.equalsIgnoreCase("custtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				custtype = FValue.trim();
			}
			else
				custtype = null;
		}
		if (FCode.equalsIgnoreCase("dectype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				dectype = i;
			}
		}
		if (FCode.equalsIgnoreCase("srcsystem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				srcsystem = FValue.trim();
			}
			else
				srcsystem = null;
		}
		if (FCode.equalsIgnoreCase("voucherid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				voucherid = FValue.trim();
			}
			else
				voucherid = null;
		}
		if (FCode.equalsIgnoreCase("transerial"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				transerial = FValue.trim();
			}
			else
				transerial = null;
		}
		if (FCode.equalsIgnoreCase("tranbatch"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tranbatch = FValue.trim();
			}
			else
				tranbatch = null;
		}
		if (FCode.equalsIgnoreCase("trandate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				trandate = fDate.getDate( FValue );
			}
			else
				trandate = null;
		}
		if (FCode.equalsIgnoreCase("trantime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				trantime = FValue.trim();
			}
			else
				trantime = null;
		}
		if (FCode.equalsIgnoreCase("tranorg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tranorg = FValue.trim();
			}
			else
				tranorg = null;
		}
		if (FCode.equalsIgnoreCase("tranchannel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tranchannel = FValue.trim();
			}
			else
				tranchannel = null;
		}
		if (FCode.equalsIgnoreCase("trancounte"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				trancounte = FValue.trim();
			}
			else
				trancounte = null;
		}
		if (FCode.equalsIgnoreCase("custmanager"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				custmanager = FValue.trim();
			}
			else
				custmanager = null;
		}
		if (FCode.equalsIgnoreCase("trantype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				trantype = FValue.trim();
			}
			else
				trantype = null;
		}
		if (FCode.equalsIgnoreCase("procode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				procode = FValue.trim();
			}
			else
				procode = null;
		}
		if (FCode.equalsIgnoreCase("busitype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				busitype = FValue.trim();
			}
			else
				busitype = null;
		}
		if (FCode.equalsIgnoreCase("tranremark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tranremark = FValue.trim();
			}
			else
				tranremark = null;
		}
		if (FCode.equalsIgnoreCase("accountno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				accountno = FValue.trim();
			}
			else
				accountno = null;
		}
		if (FCode.equalsIgnoreCase("trancurrency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				trancurrency = FValue.trim();
			}
			else
				trancurrency = null;
		}
		if (FCode.equalsIgnoreCase("tranamt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tranamt = FValue.trim();
			}
			else
				tranamt = null;
		}
		if (FCode.equalsIgnoreCase("taxtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				taxtype = i;
			}
		}
		if (FCode.equalsIgnoreCase("cztype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cztype = FValue.trim();
			}
			else
				cztype = null;
		}
		if (FCode.equalsIgnoreCase("czolddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				czolddate = fDate.getDate( FValue );
			}
			else
				czolddate = null;
		}
		if (FCode.equalsIgnoreCase("iscurrent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				iscurrent = FValue.trim();
			}
			else
				iscurrent = null;
		}
		if (FCode.equalsIgnoreCase("czbusino"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				czbusino = FValue.trim();
			}
			else
				czbusino = null;
		}
		if (FCode.equalsIgnoreCase("accno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				accno = FValue.trim();
			}
			else
				accno = null;
		}
		if (FCode.equalsIgnoreCase("loanflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				loanflag = i;
			}
		}
		if (FCode.equalsIgnoreCase("acccode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				acccode = FValue.trim();
			}
			else
				acccode = null;
		}
		if (FCode.equalsIgnoreCase("paymentflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				paymentflag = i;
			}
		}
		if (FCode.equalsIgnoreCase("amortno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				amortno = FValue.trim();
			}
			else
				amortno = null;
		}
		if (FCode.equalsIgnoreCase("midcontno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				midcontno = FValue.trim();
			}
			else
				midcontno = null;
		}
		if (FCode.equalsIgnoreCase("isinnerbank"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isinnerbank = FValue.trim();
			}
			else
				isinnerbank = null;
		}
		if (FCode.equalsIgnoreCase("overseasflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				overseasflag = i;
			}
		}
		if (FCode.equalsIgnoreCase("taxcalmethod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				taxcalmethod = i;
			}
		}
		if (FCode.equalsIgnoreCase("creator1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				creator1 = FValue.trim();
			}
			else
				creator1 = null;
		}
		if (FCode.equalsIgnoreCase("creationtime1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				creationtime1 = FValue.trim();
			}
			else
				creationtime1 = null;
		}
		if (FCode.equalsIgnoreCase("modifier1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifier1 = FValue.trim();
			}
			else
				modifier1 = null;
		}
		if (FCode.equalsIgnoreCase("modifiedtime1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifiedtime1 = FValue.trim();
			}
			else
				modifiedtime1 = null;
		}
		if (FCode.equalsIgnoreCase("negtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				negtype = FValue.trim();
			}
			else
				negtype = null;
		}
		if (FCode.equalsIgnoreCase("tranplace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tranplace = FValue.trim();
			}
			else
				tranplace = null;
		}
		if (FCode.equalsIgnoreCase("deptdoc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				deptdoc = FValue.trim();
			}
			else
				deptdoc = null;
		}
		if (FCode.equalsIgnoreCase("confimtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				confimtype = i;
			}
		}
		if (FCode.equalsIgnoreCase("ispts"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ispts = FValue.trim();
			}
			else
				ispts = null;
		}
		if (FCode.equalsIgnoreCase("isbill"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isbill = FValue.trim();
			}
			else
				isbill = null;
		}
		if (FCode.equalsIgnoreCase("areatype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				areatype = i;
			}
		}
		if (FCode.equalsIgnoreCase("isptserror"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isptserror = FValue.trim();
			}
			else
				isptserror = null;
		}
		if (FCode.equalsIgnoreCase("receivedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				receivedate = fDate.getDate( FValue );
			}
			else
				receivedate = null;
		}
		if (FCode.equalsIgnoreCase("isalloweadvance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isalloweadvance = FValue.trim();
			}
			else
				isalloweadvance = null;
		}
		if (FCode.equalsIgnoreCase("busipk"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				busipk = FValue.trim();
			}
			else
				busipk = null;
		}
		if (FCode.equalsIgnoreCase("busiitem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				busiitem = FValue.trim();
			}
			else
				busiitem = null;
		}
		if (FCode.equalsIgnoreCase("fininsance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				fininsance = FValue.trim();
			}
			else
				fininsance = null;
		}
		if (FCode.equalsIgnoreCase("fininstype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				fininstype = FValue.trim();
			}
			else
				fininstype = null;
		}
		if (FCode.equalsIgnoreCase("istaxfree"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				istaxfree = FValue.trim();
			}
			else
				istaxfree = null;
		}
		if (FCode.equalsIgnoreCase("bunit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bunit = FValue.trim();
			}
			else
				bunit = null;
		}
		if (FCode.equalsIgnoreCase("bunivalent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bunivalent = FValue.trim();
			}
			else
				bunivalent = null;
		}
		if (FCode.equalsIgnoreCase("bnumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bnumber = FValue.trim();
			}
			else
				bnumber = null;
		}
		if (FCode.equalsIgnoreCase("billtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				billtype = FValue.trim();
			}
			else
				billtype = null;
		}
		if (FCode.equalsIgnoreCase("billdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				billdate = fDate.getDate( FValue );
			}
			else
				billdate = null;
		}
		if (FCode.equalsIgnoreCase("servicedoc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				servicedoc = FValue.trim();
			}
			else
				servicedoc = null;
		}
		if (FCode.equalsIgnoreCase("ishistory"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ishistory = FValue.trim();
			}
			else
				ishistory = null;
		}
		if (FCode.equalsIgnoreCase("ismoneny"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ismoneny = FValue.trim();
			}
			else
				ismoneny = null;
		}
		if (FCode.equalsIgnoreCase("insureno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insureno = FValue.trim();
			}
			else
				insureno = null;
		}
		if (FCode.equalsIgnoreCase("isfirst"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isfirst = FValue.trim();
			}
			else
				isfirst = null;
		}
		if (FCode.equalsIgnoreCase("perspolicyno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				perspolicyno = FValue.trim();
			}
			else
				perspolicyno = null;
		}
		if (FCode.equalsIgnoreCase("ppveridate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ppveridate = FValue.trim();
			}
			else
				ppveridate = null;
		}
		if (FCode.equalsIgnoreCase("billstartdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				billstartdate = fDate.getDate( FValue );
			}
			else
				billstartdate = null;
		}
		if (FCode.equalsIgnoreCase("billenddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				billenddate = fDate.getDate( FValue );
			}
			else
				billenddate = null;
		}
		if (FCode.equalsIgnoreCase("paytype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paytype = FValue.trim();
			}
			else
				paytype = null;
		}
		if (FCode.equalsIgnoreCase("ismat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ismat = FValue.trim();
			}
			else
				ismat = null;
		}
		if (FCode.equalsIgnoreCase("isentrust"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isentrust = FValue.trim();
			}
			else
				isentrust = null;
		}
		if (FCode.equalsIgnoreCase("billeffectivedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				billeffectivedate = fDate.getDate( FValue );
			}
			else
				billeffectivedate = null;
		}
		if (FCode.equalsIgnoreCase("recoincomdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				recoincomdate = fDate.getDate( FValue );
			}
			else
				recoincomdate = null;
		}
		if (FCode.equalsIgnoreCase("billfalg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				billfalg = FValue.trim();
			}
			else
				billfalg = null;
		}
		if (FCode.equalsIgnoreCase("offsetdate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				offsetdate = FValue.trim();
			}
			else
				offsetdate = null;
		}
		if (FCode.equalsIgnoreCase("bqbatchno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bqbatchno = FValue.trim();
			}
			else
				bqbatchno = null;
		}
		if (FCode.equalsIgnoreCase("issale"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				issale = FValue.trim();
			}
			else
				issale = null;
		}
		if (FCode.equalsIgnoreCase("state"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				state = FValue.trim();
			}
			else
				state = null;
		}
		if (FCode.equalsIgnoreCase("printstate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				printstate = FValue.trim();
			}
			else
				printstate = null;
		}
		if (FCode.equalsIgnoreCase("moneyno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				moneyno = FValue.trim();
			}
			else
				moneyno = null;
		}
		if (FCode.equalsIgnoreCase("vstatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vstatus = FValue.trim();
			}
			else
				vstatus = null;
		}
		if (FCode.equalsIgnoreCase("prebilltype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				prebilltype = FValue.trim();
			}
			else
				prebilltype = null;
		}
		if (FCode.equalsIgnoreCase("billprintno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				billprintno = FValue.trim();
			}
			else
				billprintno = null;
		}
		if (FCode.equalsIgnoreCase("billprintdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				billprintdate = fDate.getDate( FValue );
			}
			else
				billprintdate = null;
		}
		if (FCode.equalsIgnoreCase("moneytype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				moneytype = FValue.trim();
			}
			else
				moneytype = null;
		}
		if (FCode.equalsIgnoreCase("cvalistate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cvalistate = FValue.trim();
			}
			else
				cvalistate = null;
		}
		if (FCode.equalsIgnoreCase("vdef2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vdef2 = FValue.trim();
			}
			else
				vdef2 = null;
		}
		if (FCode.equalsIgnoreCase("vdef1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vdef1 = FValue.trim();
			}
			else
				vdef1 = null;
		}
		if (FCode.equalsIgnoreCase("operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				operator = FValue.trim();
			}
			else
				operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ErrorInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorInfo = FValue.trim();
			}
			else
				ErrorInfo = null;
		}
		if (FCode.equalsIgnoreCase("vdef6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vdef6 = FValue.trim();
			}
			else
				vdef6 = null;
		}
		if (FCode.equalsIgnoreCase("vdef7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vdef7 = FValue.trim();
			}
			else
				vdef7 = null;
		}
		if (FCode.equalsIgnoreCase("vdef8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				vdef8 = FValue.trim();
			}
			else
				vdef8 = null;
		}
		if (FCode.equalsIgnoreCase("TS"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TS = FValue.trim();
			}
			else
				TS = null;
		}
		if (FCode.equalsIgnoreCase("buymailbox"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				buymailbox = FValue.trim();
			}
			else
				buymailbox = null;
		}
		if (FCode.equalsIgnoreCase("buyphoneno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				buyphoneno = FValue.trim();
			}
			else
				buyphoneno = null;
		}
		if (FCode.equalsIgnoreCase("buyidtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				buyidtype = FValue.trim();
			}
			else
				buyidtype = null;
		}
		if (FCode.equalsIgnoreCase("buyidnumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				buyidnumber = FValue.trim();
			}
			else
				buyidnumber = null;
		}
		if (FCode.equalsIgnoreCase("invoiceCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				invoiceCode = FValue.trim();
			}
			else
				invoiceCode = null;
		}
		if (FCode.equalsIgnoreCase("checkCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				checkCode = FValue.trim();
			}
			else
				checkCode = null;
		}
		if (FCode.equalsIgnoreCase("shortLink"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				shortLink = FValue.trim();
			}
			else
				shortLink = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LYOutPayDetailSchema other = (LYOutPayDetailSchema)otherObject;
		return
			(BusiNo == null ? other.getBusiNo() == null : BusiNo.equals(other.getBusiNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (isupdate == null ? other.getisupdate() == null : isupdate.equals(other.getisupdate()))
			&& (pkgroup == null ? other.getpkgroup() == null : pkgroup.equals(other.getpkgroup()))
			&& (pkorg == null ? other.getpkorg() == null : pkorg.equals(other.getpkorg()))
			&& (code == null ? other.getcode() == null : code.equals(other.getcode()))
			&& (name == null ? other.getname() == null : name.equals(other.getname()))
			&& (creator == null ? other.getcreator() == null : creator.equals(other.getcreator()))
			&& (creationtime == null ? other.getcreationtime() == null : creationtime.equals(other.getcreationtime()))
			&& (modifier == null ? other.getmodifier() == null : modifier.equals(other.getmodifier()))
			&& (modifiedtime == null ? other.getmodifiedtime() == null : modifiedtime.equals(other.getmodifiedtime()))
			&& (taxpayertype == null ? other.gettaxpayertype() == null : taxpayertype.equals(other.gettaxpayertype()))
			&& (invtype == null ? other.getinvtype() == null : invtype.equals(other.getinvtype()))
			&& (makefreq == null ? other.getmakefreq() == null : makefreq.equals(other.getmakefreq()))
			&& (makespace == null ? other.getmakespace() == null : makespace.equals(other.getmakespace()))
			&& (telephone == null ? other.gettelephone() == null : telephone.equals(other.gettelephone()))
			&& (mobile == null ? other.getmobile() == null : mobile.equals(other.getmobile()))
			&& (sendway == null ? other.getsendway() == null : sendway.equals(other.getsendway()))
			&& (addressee == null ? other.getaddressee() == null : addressee.equals(other.getaddressee()))
			&& (postcode == null ? other.getpostcode() == null : postcode.equals(other.getpostcode()))
			&& (mailingaddress == null ? other.getmailingaddress() == null : mailingaddress.equals(other.getmailingaddress()))
			&& (customertype == null ? other.getcustomertype() == null : customertype.equals(other.getcustomertype()))
			&& (country == null ? other.getcountry() == null : country.equals(other.getcountry()))
			&& (legalperson == null ? other.getlegalperson() == null : legalperson.equals(other.getlegalperson()))
			&& (taxnumber == null ? other.gettaxnumber() == null : taxnumber.equals(other.gettaxnumber()))
			&& (bank == null ? other.getbank() == null : bank.equals(other.getbank()))
			&& (bankaccount == null ? other.getbankaccount() == null : bankaccount.equals(other.getbankaccount()))
			&& (makeinvdate == null ? other.getmakeinvdate() == null : fDate.getString(makeinvdate).equals(other.getmakeinvdate()))
			&& (receiveperson == null ? other.getreceiveperson() == null : receiveperson.equals(other.getreceiveperson()))
			&& (ispaperyinv == null ? other.getispaperyinv() == null : ispaperyinv.equals(other.getispaperyinv()))
			&& (enablestate == null ? other.getenablestate() == null : enablestate.equals(other.getenablestate()))
			&& (isearly == null ? other.getisearly() == null : isearly.equals(other.getisearly()))
			&& (billmode == null ? other.getbillmode() == null : billmode.equals(other.getbillmode()))
			&& (vaddress == null ? other.getvaddress() == null : vaddress.equals(other.getvaddress()))
			&& (openid == null ? other.getopenid() == null : openid.equals(other.getopenid()))
			&& (billaccount == null ? other.getbillaccount() == null : billaccount.equals(other.getbillaccount()))
			&& (email == null ? other.getemail() == null : email.equals(other.getemail()))
			&& (vpsnid == null ? other.getvpsnid() == null : vpsnid.equals(other.getvpsnid()))
			&& (veffectdate == null ? other.getveffectdate() == null : fDate.getString(veffectdate).equals(other.getveffectdate()))
			&& (vautodate == null ? other.getvautodate() == null : fDate.getString(vautodate).equals(other.getvautodate()))
			&& (vmailprovince == null ? other.getvmailprovince() == null : vmailprovince.equals(other.getvmailprovince()))
			&& (vmailcity == null ? other.getvmailcity() == null : vmailcity.equals(other.getvmailcity()))
			&& (vsrcsystem == null ? other.getvsrcsystem() == null : vsrcsystem.equals(other.getvsrcsystem()))
			&& (vsrcrowid == null ? other.getvsrcrowid() == null : vsrcrowid.equals(other.getvsrcrowid()))
			&& (group == null ? other.getgroup() == null : group.equals(other.getgroup()))
			&& (org == null ? other.getorg() == null : org.equals(other.getorg()))
			&& (orgv == null ? other.getorgv() == null : orgv.equals(other.getorgv()))
			&& (calnum == null ? other.getcalnum() == null : calnum.equals(other.getcalnum()))
			&& (caldate == null ? other.getcaldate() == null : fDate.getString(caldate).equals(other.getcaldate()))
			&& (vdata == null ? other.getvdata() == null : fDate.getString(vdata).equals(other.getvdata()))
			&& (rate == null ? other.getrate() == null : rate.equals(other.getrate()))
			&& (ptsitem == null ? other.getptsitem() == null : ptsitem.equals(other.getptsitem()))
			&& methodcal == other.getmethodcal()
			&& (drawback == null ? other.getdrawback() == null : drawback.equals(other.getdrawback()))
			&& (taxrate == null ? other.gettaxrate() == null : taxrate.equals(other.gettaxrate()))
			&& (oriamt == null ? other.getoriamt() == null : oriamt.equals(other.getoriamt()))
			&& (oritax == null ? other.getoritax() == null : oritax.equals(other.getoritax()))
			&& (localamt == null ? other.getlocalamt() == null : localamt.equals(other.getlocalamt()))
			&& (localtax == null ? other.getlocaltax() == null : localtax.equals(other.getlocaltax()))
			&& (decldate == null ? other.getdecldate() == null : fDate.getString(decldate).equals(other.getdecldate()))
			&& (busidate == null ? other.getbusidate() == null : fDate.getString(busidate).equals(other.getbusidate()))
			&& (custcode == null ? other.getcustcode() == null : custcode.equals(other.getcustcode()))
			&& (custname == null ? other.getcustname() == null : custname.equals(other.getcustname()))
			&& (custtype == null ? other.getcusttype() == null : custtype.equals(other.getcusttype()))
			&& dectype == other.getdectype()
			&& (srcsystem == null ? other.getsrcsystem() == null : srcsystem.equals(other.getsrcsystem()))
			&& (voucherid == null ? other.getvoucherid() == null : voucherid.equals(other.getvoucherid()))
			&& (transerial == null ? other.gettranserial() == null : transerial.equals(other.gettranserial()))
			&& (tranbatch == null ? other.gettranbatch() == null : tranbatch.equals(other.gettranbatch()))
			&& (trandate == null ? other.gettrandate() == null : fDate.getString(trandate).equals(other.gettrandate()))
			&& (trantime == null ? other.gettrantime() == null : trantime.equals(other.gettrantime()))
			&& (tranorg == null ? other.gettranorg() == null : tranorg.equals(other.gettranorg()))
			&& (tranchannel == null ? other.gettranchannel() == null : tranchannel.equals(other.gettranchannel()))
			&& (trancounte == null ? other.gettrancounte() == null : trancounte.equals(other.gettrancounte()))
			&& (custmanager == null ? other.getcustmanager() == null : custmanager.equals(other.getcustmanager()))
			&& (trantype == null ? other.gettrantype() == null : trantype.equals(other.gettrantype()))
			&& (procode == null ? other.getprocode() == null : procode.equals(other.getprocode()))
			&& (busitype == null ? other.getbusitype() == null : busitype.equals(other.getbusitype()))
			&& (tranremark == null ? other.gettranremark() == null : tranremark.equals(other.gettranremark()))
			&& (accountno == null ? other.getaccountno() == null : accountno.equals(other.getaccountno()))
			&& (trancurrency == null ? other.gettrancurrency() == null : trancurrency.equals(other.gettrancurrency()))
			&& (tranamt == null ? other.gettranamt() == null : tranamt.equals(other.gettranamt()))
			&& taxtype == other.gettaxtype()
			&& (cztype == null ? other.getcztype() == null : cztype.equals(other.getcztype()))
			&& (czolddate == null ? other.getczolddate() == null : fDate.getString(czolddate).equals(other.getczolddate()))
			&& (iscurrent == null ? other.getiscurrent() == null : iscurrent.equals(other.getiscurrent()))
			&& (czbusino == null ? other.getczbusino() == null : czbusino.equals(other.getczbusino()))
			&& (accno == null ? other.getaccno() == null : accno.equals(other.getaccno()))
			&& loanflag == other.getloanflag()
			&& (acccode == null ? other.getacccode() == null : acccode.equals(other.getacccode()))
			&& paymentflag == other.getpaymentflag()
			&& (amortno == null ? other.getamortno() == null : amortno.equals(other.getamortno()))
			&& (midcontno == null ? other.getmidcontno() == null : midcontno.equals(other.getmidcontno()))
			&& (isinnerbank == null ? other.getisinnerbank() == null : isinnerbank.equals(other.getisinnerbank()))
			&& overseasflag == other.getoverseasflag()
			&& taxcalmethod == other.gettaxcalmethod()
			&& (creator1 == null ? other.getcreator1() == null : creator1.equals(other.getcreator1()))
			&& (creationtime1 == null ? other.getcreationtime1() == null : creationtime1.equals(other.getcreationtime1()))
			&& (modifier1 == null ? other.getmodifier1() == null : modifier1.equals(other.getmodifier1()))
			&& (modifiedtime1 == null ? other.getmodifiedtime1() == null : modifiedtime1.equals(other.getmodifiedtime1()))
			&& (negtype == null ? other.getnegtype() == null : negtype.equals(other.getnegtype()))
			&& (tranplace == null ? other.gettranplace() == null : tranplace.equals(other.gettranplace()))
			&& (deptdoc == null ? other.getdeptdoc() == null : deptdoc.equals(other.getdeptdoc()))
			&& confimtype == other.getconfimtype()
			&& (ispts == null ? other.getispts() == null : ispts.equals(other.getispts()))
			&& (isbill == null ? other.getisbill() == null : isbill.equals(other.getisbill()))
			&& areatype == other.getareatype()
			&& (isptserror == null ? other.getisptserror() == null : isptserror.equals(other.getisptserror()))
			&& (receivedate == null ? other.getreceivedate() == null : fDate.getString(receivedate).equals(other.getreceivedate()))
			&& (isalloweadvance == null ? other.getisalloweadvance() == null : isalloweadvance.equals(other.getisalloweadvance()))
			&& (busipk == null ? other.getbusipk() == null : busipk.equals(other.getbusipk()))
			&& (busiitem == null ? other.getbusiitem() == null : busiitem.equals(other.getbusiitem()))
			&& (fininsance == null ? other.getfininsance() == null : fininsance.equals(other.getfininsance()))
			&& (fininstype == null ? other.getfininstype() == null : fininstype.equals(other.getfininstype()))
			&& (istaxfree == null ? other.getistaxfree() == null : istaxfree.equals(other.getistaxfree()))
			&& (bunit == null ? other.getbunit() == null : bunit.equals(other.getbunit()))
			&& (bunivalent == null ? other.getbunivalent() == null : bunivalent.equals(other.getbunivalent()))
			&& (bnumber == null ? other.getbnumber() == null : bnumber.equals(other.getbnumber()))
			&& (billtype == null ? other.getbilltype() == null : billtype.equals(other.getbilltype()))
			&& (billdate == null ? other.getbilldate() == null : fDate.getString(billdate).equals(other.getbilldate()))
			&& (servicedoc == null ? other.getservicedoc() == null : servicedoc.equals(other.getservicedoc()))
			&& (ishistory == null ? other.getishistory() == null : ishistory.equals(other.getishistory()))
			&& (ismoneny == null ? other.getismoneny() == null : ismoneny.equals(other.getismoneny()))
			&& (insureno == null ? other.getinsureno() == null : insureno.equals(other.getinsureno()))
			&& (isfirst == null ? other.getisfirst() == null : isfirst.equals(other.getisfirst()))
			&& (perspolicyno == null ? other.getperspolicyno() == null : perspolicyno.equals(other.getperspolicyno()))
			&& (ppveridate == null ? other.getppveridate() == null : ppveridate.equals(other.getppveridate()))
			&& (billstartdate == null ? other.getbillstartdate() == null : fDate.getString(billstartdate).equals(other.getbillstartdate()))
			&& (billenddate == null ? other.getbillenddate() == null : fDate.getString(billenddate).equals(other.getbillenddate()))
			&& (paytype == null ? other.getpaytype() == null : paytype.equals(other.getpaytype()))
			&& (ismat == null ? other.getismat() == null : ismat.equals(other.getismat()))
			&& (isentrust == null ? other.getisentrust() == null : isentrust.equals(other.getisentrust()))
			&& (billeffectivedate == null ? other.getbilleffectivedate() == null : fDate.getString(billeffectivedate).equals(other.getbilleffectivedate()))
			&& (recoincomdate == null ? other.getrecoincomdate() == null : fDate.getString(recoincomdate).equals(other.getrecoincomdate()))
			&& (billfalg == null ? other.getbillfalg() == null : billfalg.equals(other.getbillfalg()))
			&& (offsetdate == null ? other.getoffsetdate() == null : offsetdate.equals(other.getoffsetdate()))
			&& (bqbatchno == null ? other.getbqbatchno() == null : bqbatchno.equals(other.getbqbatchno()))
			&& (issale == null ? other.getissale() == null : issale.equals(other.getissale()))
			&& (state == null ? other.getstate() == null : state.equals(other.getstate()))
			&& (printstate == null ? other.getprintstate() == null : printstate.equals(other.getprintstate()))
			&& (moneyno == null ? other.getmoneyno() == null : moneyno.equals(other.getmoneyno()))
			&& (vstatus == null ? other.getvstatus() == null : vstatus.equals(other.getvstatus()))
			&& (prebilltype == null ? other.getprebilltype() == null : prebilltype.equals(other.getprebilltype()))
			&& (billprintno == null ? other.getbillprintno() == null : billprintno.equals(other.getbillprintno()))
			&& (billprintdate == null ? other.getbillprintdate() == null : fDate.getString(billprintdate).equals(other.getbillprintdate()))
			&& (moneytype == null ? other.getmoneytype() == null : moneytype.equals(other.getmoneytype()))
			&& (cvalistate == null ? other.getcvalistate() == null : cvalistate.equals(other.getcvalistate()))
			&& (vdef2 == null ? other.getvdef2() == null : vdef2.equals(other.getvdef2()))
			&& (vdef1 == null ? other.getvdef1() == null : vdef1.equals(other.getvdef1()))
			&& (operator == null ? other.getoperator() == null : operator.equals(other.getoperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (ErrorInfo == null ? other.getErrorInfo() == null : ErrorInfo.equals(other.getErrorInfo()))
			&& (vdef6 == null ? other.getvdef6() == null : vdef6.equals(other.getvdef6()))
			&& (vdef7 == null ? other.getvdef7() == null : vdef7.equals(other.getvdef7()))
			&& (vdef8 == null ? other.getvdef8() == null : vdef8.equals(other.getvdef8()))
			&& (TS == null ? other.getTS() == null : TS.equals(other.getTS()))
			&& (buymailbox == null ? other.getbuymailbox() == null : buymailbox.equals(other.getbuymailbox()))
			&& (buyphoneno == null ? other.getbuyphoneno() == null : buyphoneno.equals(other.getbuyphoneno()))
			&& (buyidtype == null ? other.getbuyidtype() == null : buyidtype.equals(other.getbuyidtype()))
			&& (buyidnumber == null ? other.getbuyidnumber() == null : buyidnumber.equals(other.getbuyidnumber()))
			&& (invoiceCode == null ? other.getinvoiceCode() == null : invoiceCode.equals(other.getinvoiceCode()))
			&& (checkCode == null ? other.getcheckCode() == null : checkCode.equals(other.getcheckCode()))
			&& (shortLink == null ? other.getshortLink() == null : shortLink.equals(other.getshortLink()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BusiNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("isupdate") ) {
			return 2;
		}
		if( strFieldName.equals("pkgroup") ) {
			return 3;
		}
		if( strFieldName.equals("pkorg") ) {
			return 4;
		}
		if( strFieldName.equals("code") ) {
			return 5;
		}
		if( strFieldName.equals("name") ) {
			return 6;
		}
		if( strFieldName.equals("creator") ) {
			return 7;
		}
		if( strFieldName.equals("creationtime") ) {
			return 8;
		}
		if( strFieldName.equals("modifier") ) {
			return 9;
		}
		if( strFieldName.equals("modifiedtime") ) {
			return 10;
		}
		if( strFieldName.equals("taxpayertype") ) {
			return 11;
		}
		if( strFieldName.equals("invtype") ) {
			return 12;
		}
		if( strFieldName.equals("makefreq") ) {
			return 13;
		}
		if( strFieldName.equals("makespace") ) {
			return 14;
		}
		if( strFieldName.equals("telephone") ) {
			return 15;
		}
		if( strFieldName.equals("mobile") ) {
			return 16;
		}
		if( strFieldName.equals("sendway") ) {
			return 17;
		}
		if( strFieldName.equals("addressee") ) {
			return 18;
		}
		if( strFieldName.equals("postcode") ) {
			return 19;
		}
		if( strFieldName.equals("mailingaddress") ) {
			return 20;
		}
		if( strFieldName.equals("customertype") ) {
			return 21;
		}
		if( strFieldName.equals("country") ) {
			return 22;
		}
		if( strFieldName.equals("legalperson") ) {
			return 23;
		}
		if( strFieldName.equals("taxnumber") ) {
			return 24;
		}
		if( strFieldName.equals("bank") ) {
			return 25;
		}
		if( strFieldName.equals("bankaccount") ) {
			return 26;
		}
		if( strFieldName.equals("makeinvdate") ) {
			return 27;
		}
		if( strFieldName.equals("receiveperson") ) {
			return 28;
		}
		if( strFieldName.equals("ispaperyinv") ) {
			return 29;
		}
		if( strFieldName.equals("enablestate") ) {
			return 30;
		}
		if( strFieldName.equals("isearly") ) {
			return 31;
		}
		if( strFieldName.equals("billmode") ) {
			return 32;
		}
		if( strFieldName.equals("vaddress") ) {
			return 33;
		}
		if( strFieldName.equals("openid") ) {
			return 34;
		}
		if( strFieldName.equals("billaccount") ) {
			return 35;
		}
		if( strFieldName.equals("email") ) {
			return 36;
		}
		if( strFieldName.equals("vpsnid") ) {
			return 37;
		}
		if( strFieldName.equals("veffectdate") ) {
			return 38;
		}
		if( strFieldName.equals("vautodate") ) {
			return 39;
		}
		if( strFieldName.equals("vmailprovince") ) {
			return 40;
		}
		if( strFieldName.equals("vmailcity") ) {
			return 41;
		}
		if( strFieldName.equals("vsrcsystem") ) {
			return 42;
		}
		if( strFieldName.equals("vsrcrowid") ) {
			return 43;
		}
		if( strFieldName.equals("group") ) {
			return 44;
		}
		if( strFieldName.equals("org") ) {
			return 45;
		}
		if( strFieldName.equals("orgv") ) {
			return 46;
		}
		if( strFieldName.equals("calnum") ) {
			return 47;
		}
		if( strFieldName.equals("caldate") ) {
			return 48;
		}
		if( strFieldName.equals("vdata") ) {
			return 49;
		}
		if( strFieldName.equals("rate") ) {
			return 50;
		}
		if( strFieldName.equals("ptsitem") ) {
			return 51;
		}
		if( strFieldName.equals("methodcal") ) {
			return 52;
		}
		if( strFieldName.equals("drawback") ) {
			return 53;
		}
		if( strFieldName.equals("taxrate") ) {
			return 54;
		}
		if( strFieldName.equals("oriamt") ) {
			return 55;
		}
		if( strFieldName.equals("oritax") ) {
			return 56;
		}
		if( strFieldName.equals("localamt") ) {
			return 57;
		}
		if( strFieldName.equals("localtax") ) {
			return 58;
		}
		if( strFieldName.equals("decldate") ) {
			return 59;
		}
		if( strFieldName.equals("busidate") ) {
			return 60;
		}
		if( strFieldName.equals("custcode") ) {
			return 61;
		}
		if( strFieldName.equals("custname") ) {
			return 62;
		}
		if( strFieldName.equals("custtype") ) {
			return 63;
		}
		if( strFieldName.equals("dectype") ) {
			return 64;
		}
		if( strFieldName.equals("srcsystem") ) {
			return 65;
		}
		if( strFieldName.equals("voucherid") ) {
			return 66;
		}
		if( strFieldName.equals("transerial") ) {
			return 67;
		}
		if( strFieldName.equals("tranbatch") ) {
			return 68;
		}
		if( strFieldName.equals("trandate") ) {
			return 69;
		}
		if( strFieldName.equals("trantime") ) {
			return 70;
		}
		if( strFieldName.equals("tranorg") ) {
			return 71;
		}
		if( strFieldName.equals("tranchannel") ) {
			return 72;
		}
		if( strFieldName.equals("trancounte") ) {
			return 73;
		}
		if( strFieldName.equals("custmanager") ) {
			return 74;
		}
		if( strFieldName.equals("trantype") ) {
			return 75;
		}
		if( strFieldName.equals("procode") ) {
			return 76;
		}
		if( strFieldName.equals("busitype") ) {
			return 77;
		}
		if( strFieldName.equals("tranremark") ) {
			return 78;
		}
		if( strFieldName.equals("accountno") ) {
			return 79;
		}
		if( strFieldName.equals("trancurrency") ) {
			return 80;
		}
		if( strFieldName.equals("tranamt") ) {
			return 81;
		}
		if( strFieldName.equals("taxtype") ) {
			return 82;
		}
		if( strFieldName.equals("cztype") ) {
			return 83;
		}
		if( strFieldName.equals("czolddate") ) {
			return 84;
		}
		if( strFieldName.equals("iscurrent") ) {
			return 85;
		}
		if( strFieldName.equals("czbusino") ) {
			return 86;
		}
		if( strFieldName.equals("accno") ) {
			return 87;
		}
		if( strFieldName.equals("loanflag") ) {
			return 88;
		}
		if( strFieldName.equals("acccode") ) {
			return 89;
		}
		if( strFieldName.equals("paymentflag") ) {
			return 90;
		}
		if( strFieldName.equals("amortno") ) {
			return 91;
		}
		if( strFieldName.equals("midcontno") ) {
			return 92;
		}
		if( strFieldName.equals("isinnerbank") ) {
			return 93;
		}
		if( strFieldName.equals("overseasflag") ) {
			return 94;
		}
		if( strFieldName.equals("taxcalmethod") ) {
			return 95;
		}
		if( strFieldName.equals("creator1") ) {
			return 96;
		}
		if( strFieldName.equals("creationtime1") ) {
			return 97;
		}
		if( strFieldName.equals("modifier1") ) {
			return 98;
		}
		if( strFieldName.equals("modifiedtime1") ) {
			return 99;
		}
		if( strFieldName.equals("negtype") ) {
			return 100;
		}
		if( strFieldName.equals("tranplace") ) {
			return 101;
		}
		if( strFieldName.equals("deptdoc") ) {
			return 102;
		}
		if( strFieldName.equals("confimtype") ) {
			return 103;
		}
		if( strFieldName.equals("ispts") ) {
			return 104;
		}
		if( strFieldName.equals("isbill") ) {
			return 105;
		}
		if( strFieldName.equals("areatype") ) {
			return 106;
		}
		if( strFieldName.equals("isptserror") ) {
			return 107;
		}
		if( strFieldName.equals("receivedate") ) {
			return 108;
		}
		if( strFieldName.equals("isalloweadvance") ) {
			return 109;
		}
		if( strFieldName.equals("busipk") ) {
			return 110;
		}
		if( strFieldName.equals("busiitem") ) {
			return 111;
		}
		if( strFieldName.equals("fininsance") ) {
			return 112;
		}
		if( strFieldName.equals("fininstype") ) {
			return 113;
		}
		if( strFieldName.equals("istaxfree") ) {
			return 114;
		}
		if( strFieldName.equals("bunit") ) {
			return 115;
		}
		if( strFieldName.equals("bunivalent") ) {
			return 116;
		}
		if( strFieldName.equals("bnumber") ) {
			return 117;
		}
		if( strFieldName.equals("billtype") ) {
			return 118;
		}
		if( strFieldName.equals("billdate") ) {
			return 119;
		}
		if( strFieldName.equals("servicedoc") ) {
			return 120;
		}
		if( strFieldName.equals("ishistory") ) {
			return 121;
		}
		if( strFieldName.equals("ismoneny") ) {
			return 122;
		}
		if( strFieldName.equals("insureno") ) {
			return 123;
		}
		if( strFieldName.equals("isfirst") ) {
			return 124;
		}
		if( strFieldName.equals("perspolicyno") ) {
			return 125;
		}
		if( strFieldName.equals("ppveridate") ) {
			return 126;
		}
		if( strFieldName.equals("billstartdate") ) {
			return 127;
		}
		if( strFieldName.equals("billenddate") ) {
			return 128;
		}
		if( strFieldName.equals("paytype") ) {
			return 129;
		}
		if( strFieldName.equals("ismat") ) {
			return 130;
		}
		if( strFieldName.equals("isentrust") ) {
			return 131;
		}
		if( strFieldName.equals("billeffectivedate") ) {
			return 132;
		}
		if( strFieldName.equals("recoincomdate") ) {
			return 133;
		}
		if( strFieldName.equals("billfalg") ) {
			return 134;
		}
		if( strFieldName.equals("offsetdate") ) {
			return 135;
		}
		if( strFieldName.equals("bqbatchno") ) {
			return 136;
		}
		if( strFieldName.equals("issale") ) {
			return 137;
		}
		if( strFieldName.equals("state") ) {
			return 138;
		}
		if( strFieldName.equals("printstate") ) {
			return 139;
		}
		if( strFieldName.equals("moneyno") ) {
			return 140;
		}
		if( strFieldName.equals("vstatus") ) {
			return 141;
		}
		if( strFieldName.equals("prebilltype") ) {
			return 142;
		}
		if( strFieldName.equals("billprintno") ) {
			return 143;
		}
		if( strFieldName.equals("billprintdate") ) {
			return 144;
		}
		if( strFieldName.equals("moneytype") ) {
			return 145;
		}
		if( strFieldName.equals("cvalistate") ) {
			return 146;
		}
		if( strFieldName.equals("vdef2") ) {
			return 147;
		}
		if( strFieldName.equals("vdef1") ) {
			return 148;
		}
		if( strFieldName.equals("operator") ) {
			return 149;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 150;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 151;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 152;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 153;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return 154;
		}
		if( strFieldName.equals("vdef6") ) {
			return 155;
		}
		if( strFieldName.equals("vdef7") ) {
			return 156;
		}
		if( strFieldName.equals("vdef8") ) {
			return 157;
		}
		if( strFieldName.equals("TS") ) {
			return 158;
		}
		if( strFieldName.equals("buymailbox") ) {
			return 159;
		}
		if( strFieldName.equals("buyphoneno") ) {
			return 160;
		}
		if( strFieldName.equals("buyidtype") ) {
			return 161;
		}
		if( strFieldName.equals("buyidnumber") ) {
			return 162;
		}
		if( strFieldName.equals("invoiceCode") ) {
			return 163;
		}
		if( strFieldName.equals("checkCode") ) {
			return 164;
		}
		if( strFieldName.equals("shortLink") ) {
			return 165;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BusiNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "isupdate";
				break;
			case 3:
				strFieldName = "pkgroup";
				break;
			case 4:
				strFieldName = "pkorg";
				break;
			case 5:
				strFieldName = "code";
				break;
			case 6:
				strFieldName = "name";
				break;
			case 7:
				strFieldName = "creator";
				break;
			case 8:
				strFieldName = "creationtime";
				break;
			case 9:
				strFieldName = "modifier";
				break;
			case 10:
				strFieldName = "modifiedtime";
				break;
			case 11:
				strFieldName = "taxpayertype";
				break;
			case 12:
				strFieldName = "invtype";
				break;
			case 13:
				strFieldName = "makefreq";
				break;
			case 14:
				strFieldName = "makespace";
				break;
			case 15:
				strFieldName = "telephone";
				break;
			case 16:
				strFieldName = "mobile";
				break;
			case 17:
				strFieldName = "sendway";
				break;
			case 18:
				strFieldName = "addressee";
				break;
			case 19:
				strFieldName = "postcode";
				break;
			case 20:
				strFieldName = "mailingaddress";
				break;
			case 21:
				strFieldName = "customertype";
				break;
			case 22:
				strFieldName = "country";
				break;
			case 23:
				strFieldName = "legalperson";
				break;
			case 24:
				strFieldName = "taxnumber";
				break;
			case 25:
				strFieldName = "bank";
				break;
			case 26:
				strFieldName = "bankaccount";
				break;
			case 27:
				strFieldName = "makeinvdate";
				break;
			case 28:
				strFieldName = "receiveperson";
				break;
			case 29:
				strFieldName = "ispaperyinv";
				break;
			case 30:
				strFieldName = "enablestate";
				break;
			case 31:
				strFieldName = "isearly";
				break;
			case 32:
				strFieldName = "billmode";
				break;
			case 33:
				strFieldName = "vaddress";
				break;
			case 34:
				strFieldName = "openid";
				break;
			case 35:
				strFieldName = "billaccount";
				break;
			case 36:
				strFieldName = "email";
				break;
			case 37:
				strFieldName = "vpsnid";
				break;
			case 38:
				strFieldName = "veffectdate";
				break;
			case 39:
				strFieldName = "vautodate";
				break;
			case 40:
				strFieldName = "vmailprovince";
				break;
			case 41:
				strFieldName = "vmailcity";
				break;
			case 42:
				strFieldName = "vsrcsystem";
				break;
			case 43:
				strFieldName = "vsrcrowid";
				break;
			case 44:
				strFieldName = "group";
				break;
			case 45:
				strFieldName = "org";
				break;
			case 46:
				strFieldName = "orgv";
				break;
			case 47:
				strFieldName = "calnum";
				break;
			case 48:
				strFieldName = "caldate";
				break;
			case 49:
				strFieldName = "vdata";
				break;
			case 50:
				strFieldName = "rate";
				break;
			case 51:
				strFieldName = "ptsitem";
				break;
			case 52:
				strFieldName = "methodcal";
				break;
			case 53:
				strFieldName = "drawback";
				break;
			case 54:
				strFieldName = "taxrate";
				break;
			case 55:
				strFieldName = "oriamt";
				break;
			case 56:
				strFieldName = "oritax";
				break;
			case 57:
				strFieldName = "localamt";
				break;
			case 58:
				strFieldName = "localtax";
				break;
			case 59:
				strFieldName = "decldate";
				break;
			case 60:
				strFieldName = "busidate";
				break;
			case 61:
				strFieldName = "custcode";
				break;
			case 62:
				strFieldName = "custname";
				break;
			case 63:
				strFieldName = "custtype";
				break;
			case 64:
				strFieldName = "dectype";
				break;
			case 65:
				strFieldName = "srcsystem";
				break;
			case 66:
				strFieldName = "voucherid";
				break;
			case 67:
				strFieldName = "transerial";
				break;
			case 68:
				strFieldName = "tranbatch";
				break;
			case 69:
				strFieldName = "trandate";
				break;
			case 70:
				strFieldName = "trantime";
				break;
			case 71:
				strFieldName = "tranorg";
				break;
			case 72:
				strFieldName = "tranchannel";
				break;
			case 73:
				strFieldName = "trancounte";
				break;
			case 74:
				strFieldName = "custmanager";
				break;
			case 75:
				strFieldName = "trantype";
				break;
			case 76:
				strFieldName = "procode";
				break;
			case 77:
				strFieldName = "busitype";
				break;
			case 78:
				strFieldName = "tranremark";
				break;
			case 79:
				strFieldName = "accountno";
				break;
			case 80:
				strFieldName = "trancurrency";
				break;
			case 81:
				strFieldName = "tranamt";
				break;
			case 82:
				strFieldName = "taxtype";
				break;
			case 83:
				strFieldName = "cztype";
				break;
			case 84:
				strFieldName = "czolddate";
				break;
			case 85:
				strFieldName = "iscurrent";
				break;
			case 86:
				strFieldName = "czbusino";
				break;
			case 87:
				strFieldName = "accno";
				break;
			case 88:
				strFieldName = "loanflag";
				break;
			case 89:
				strFieldName = "acccode";
				break;
			case 90:
				strFieldName = "paymentflag";
				break;
			case 91:
				strFieldName = "amortno";
				break;
			case 92:
				strFieldName = "midcontno";
				break;
			case 93:
				strFieldName = "isinnerbank";
				break;
			case 94:
				strFieldName = "overseasflag";
				break;
			case 95:
				strFieldName = "taxcalmethod";
				break;
			case 96:
				strFieldName = "creator1";
				break;
			case 97:
				strFieldName = "creationtime1";
				break;
			case 98:
				strFieldName = "modifier1";
				break;
			case 99:
				strFieldName = "modifiedtime1";
				break;
			case 100:
				strFieldName = "negtype";
				break;
			case 101:
				strFieldName = "tranplace";
				break;
			case 102:
				strFieldName = "deptdoc";
				break;
			case 103:
				strFieldName = "confimtype";
				break;
			case 104:
				strFieldName = "ispts";
				break;
			case 105:
				strFieldName = "isbill";
				break;
			case 106:
				strFieldName = "areatype";
				break;
			case 107:
				strFieldName = "isptserror";
				break;
			case 108:
				strFieldName = "receivedate";
				break;
			case 109:
				strFieldName = "isalloweadvance";
				break;
			case 110:
				strFieldName = "busipk";
				break;
			case 111:
				strFieldName = "busiitem";
				break;
			case 112:
				strFieldName = "fininsance";
				break;
			case 113:
				strFieldName = "fininstype";
				break;
			case 114:
				strFieldName = "istaxfree";
				break;
			case 115:
				strFieldName = "bunit";
				break;
			case 116:
				strFieldName = "bunivalent";
				break;
			case 117:
				strFieldName = "bnumber";
				break;
			case 118:
				strFieldName = "billtype";
				break;
			case 119:
				strFieldName = "billdate";
				break;
			case 120:
				strFieldName = "servicedoc";
				break;
			case 121:
				strFieldName = "ishistory";
				break;
			case 122:
				strFieldName = "ismoneny";
				break;
			case 123:
				strFieldName = "insureno";
				break;
			case 124:
				strFieldName = "isfirst";
				break;
			case 125:
				strFieldName = "perspolicyno";
				break;
			case 126:
				strFieldName = "ppveridate";
				break;
			case 127:
				strFieldName = "billstartdate";
				break;
			case 128:
				strFieldName = "billenddate";
				break;
			case 129:
				strFieldName = "paytype";
				break;
			case 130:
				strFieldName = "ismat";
				break;
			case 131:
				strFieldName = "isentrust";
				break;
			case 132:
				strFieldName = "billeffectivedate";
				break;
			case 133:
				strFieldName = "recoincomdate";
				break;
			case 134:
				strFieldName = "billfalg";
				break;
			case 135:
				strFieldName = "offsetdate";
				break;
			case 136:
				strFieldName = "bqbatchno";
				break;
			case 137:
				strFieldName = "issale";
				break;
			case 138:
				strFieldName = "state";
				break;
			case 139:
				strFieldName = "printstate";
				break;
			case 140:
				strFieldName = "moneyno";
				break;
			case 141:
				strFieldName = "vstatus";
				break;
			case 142:
				strFieldName = "prebilltype";
				break;
			case 143:
				strFieldName = "billprintno";
				break;
			case 144:
				strFieldName = "billprintdate";
				break;
			case 145:
				strFieldName = "moneytype";
				break;
			case 146:
				strFieldName = "cvalistate";
				break;
			case 147:
				strFieldName = "vdef2";
				break;
			case 148:
				strFieldName = "vdef1";
				break;
			case 149:
				strFieldName = "operator";
				break;
			case 150:
				strFieldName = "MakeDate";
				break;
			case 151:
				strFieldName = "MakeTime";
				break;
			case 152:
				strFieldName = "ModifyDate";
				break;
			case 153:
				strFieldName = "ModifyTime";
				break;
			case 154:
				strFieldName = "ErrorInfo";
				break;
			case 155:
				strFieldName = "vdef6";
				break;
			case 156:
				strFieldName = "vdef7";
				break;
			case 157:
				strFieldName = "vdef8";
				break;
			case 158:
				strFieldName = "TS";
				break;
			case 159:
				strFieldName = "buymailbox";
				break;
			case 160:
				strFieldName = "buyphoneno";
				break;
			case 161:
				strFieldName = "buyidtype";
				break;
			case 162:
				strFieldName = "buyidnumber";
				break;
			case 163:
				strFieldName = "invoiceCode";
				break;
			case 164:
				strFieldName = "checkCode";
				break;
			case 165:
				strFieldName = "shortLink";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BusiNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isupdate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("pkgroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("pkorg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("code") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("creator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("creationtime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifier") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifiedtime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("taxpayertype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("invtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makefreq") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makespace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("telephone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sendway") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("addressee") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("postcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("mailingaddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("customertype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("country") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("legalperson") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("taxnumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bank") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bankaccount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makeinvdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("receiveperson") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ispaperyinv") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("enablestate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isearly") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("billmode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vaddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("openid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("billaccount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("email") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vpsnid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("veffectdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("vautodate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("vmailprovince") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vmailcity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vsrcsystem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vsrcrowid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("group") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("org") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("orgv") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("calnum") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("caldate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("vdata") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("rate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ptsitem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("methodcal") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("drawback") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("taxrate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("oriamt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("oritax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("localamt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("localtax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("decldate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("busidate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("custcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("custname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("custtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("dectype") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("srcsystem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("voucherid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("transerial") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tranbatch") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("trandate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("trantime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tranorg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tranchannel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("trancounte") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("custmanager") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("trantype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("procode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("busitype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tranremark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("accountno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("trancurrency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tranamt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("taxtype") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("cztype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("czolddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("iscurrent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("czbusino") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("accno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("loanflag") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("acccode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paymentflag") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("amortno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("midcontno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isinnerbank") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("overseasflag") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("taxcalmethod") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("creator1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("creationtime1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifier1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifiedtime1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("negtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tranplace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("deptdoc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("confimtype") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ispts") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isbill") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("areatype") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("isptserror") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("receivedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("isalloweadvance") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("busipk") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("busiitem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("fininsance") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("fininstype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("istaxfree") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bunit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bunivalent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bnumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("billtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("billdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("servicedoc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ishistory") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ismoneny") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insureno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isfirst") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("perspolicyno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ppveridate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("billstartdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("billenddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("paytype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ismat") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isentrust") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("billeffectivedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("recoincomdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("billfalg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("offsetdate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bqbatchno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("issale") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("state") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("printstate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("moneyno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vstatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prebilltype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("billprintno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("billprintdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("moneytype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cvalistate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vdef2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vdef1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vdef6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vdef7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("vdef8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TS") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("buymailbox") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("buyphoneno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("buyidtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("buyidnumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("invoiceCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("checkCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("shortLink") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 49:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_INT;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 60:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_INT;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 69:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 70:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 71:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 72:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 73:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 74:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 75:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 76:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 77:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 78:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 79:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 80:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 81:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 82:
				nFieldType = Schema.TYPE_INT;
				break;
			case 83:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 84:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 85:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 86:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 87:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 88:
				nFieldType = Schema.TYPE_INT;
				break;
			case 89:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 90:
				nFieldType = Schema.TYPE_INT;
				break;
			case 91:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 92:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 93:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 94:
				nFieldType = Schema.TYPE_INT;
				break;
			case 95:
				nFieldType = Schema.TYPE_INT;
				break;
			case 96:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 97:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 98:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 99:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 100:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 101:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 102:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 103:
				nFieldType = Schema.TYPE_INT;
				break;
			case 104:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 105:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 106:
				nFieldType = Schema.TYPE_INT;
				break;
			case 107:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 108:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 109:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 110:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 111:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 112:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 113:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 114:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 115:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 116:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 117:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 118:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 119:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 120:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 121:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 122:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 123:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 124:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 125:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 126:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 127:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 128:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 129:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 130:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 131:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 132:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 133:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 134:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 135:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 136:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 137:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 138:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 139:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 140:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 141:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 142:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 143:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 144:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 145:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 146:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 147:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 148:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 149:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 150:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 151:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 152:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 153:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 154:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 155:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 156:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 157:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 158:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 159:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 160:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 161:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 162:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 163:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 164:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 165:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
