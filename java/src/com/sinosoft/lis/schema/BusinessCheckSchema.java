/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.BusinessCheckDB;

/*
 * <p>ClassName: BusinessCheckSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2015-10-27
 */
public class BusinessCheckSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerNo;
	/** 保险公司代码 */
	private String CompanyCode;
	/** 指定核对日期 */
	private Date CheckDate;
	/** 总记录数 */
	private double TotalNum;
	/** 成功记录数 */
	private double SuccessNum;
	/** 失败记录数 */
	private double FailNum;
	/** 核对请求类型 */
	private String CheckTransType;
	/** 交易时间 */
	private String TransDate;
	/** 保单号码 */
	private String PolicyNo;
	/** 分单号码 */
	private String SequenceNo;
	/** 保全批单号 */
	private String EndorsementNo;
	/** 理赔赔案号 */
	private String ClaimNo;
	/** 费用编码 */
	private String FeeId;
	/** 保费状态 */
	private String FeeStatus;
	/** 续保批单号 */
	private String RenewalEndorsementNo;
	/** 公司费用id */
	private String ComFeeId;
	/** 返回结果 */
	private String ReturnCode;
	/** 失败原因 */
	private String ErrorMessage;
	/** 结果状态 */
	private String ResultStatus;
	/** 结果描述 */
	private String ResultInfoDesc;
	/** Ciitc结果状态 */
	private String CiitcResultCode;
	/** Ciitc结果信息 */
	private String CiitcResultMessage;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 26;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public BusinessCheckSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		BusinessCheckSchema cloned = (BusinessCheckSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerNo()
	{
		return SerNo;
	}
	public void setSerNo(String aSerNo)
	{
		SerNo = aSerNo;
	}
	public String getCompanyCode()
	{
		return CompanyCode;
	}
	public void setCompanyCode(String aCompanyCode)
	{
		CompanyCode = aCompanyCode;
	}
	public String getCheckDate()
	{
		if( CheckDate != null )
			return fDate.getString(CheckDate);
		else
			return null;
	}
	public void setCheckDate(Date aCheckDate)
	{
		CheckDate = aCheckDate;
	}
	public void setCheckDate(String aCheckDate)
	{
		if (aCheckDate != null && !aCheckDate.equals("") )
		{
			CheckDate = fDate.getDate( aCheckDate );
		}
		else
			CheckDate = null;
	}

	public double getTotalNum()
	{
		return TotalNum;
	}
	public void setTotalNum(double aTotalNum)
	{
		TotalNum = Arith.round(aTotalNum,0);
	}
	public void setTotalNum(String aTotalNum)
	{
		if (aTotalNum != null && !aTotalNum.equals(""))
		{
			Double tDouble = new Double(aTotalNum);
			double d = tDouble.doubleValue();
                TotalNum = Arith.round(d,0);
		}
	}

	public double getSuccessNum()
	{
		return SuccessNum;
	}
	public void setSuccessNum(double aSuccessNum)
	{
		SuccessNum = Arith.round(aSuccessNum,0);
	}
	public void setSuccessNum(String aSuccessNum)
	{
		if (aSuccessNum != null && !aSuccessNum.equals(""))
		{
			Double tDouble = new Double(aSuccessNum);
			double d = tDouble.doubleValue();
                SuccessNum = Arith.round(d,0);
		}
	}

	public double getFailNum()
	{
		return FailNum;
	}
	public void setFailNum(double aFailNum)
	{
		FailNum = Arith.round(aFailNum,0);
	}
	public void setFailNum(String aFailNum)
	{
		if (aFailNum != null && !aFailNum.equals(""))
		{
			Double tDouble = new Double(aFailNum);
			double d = tDouble.doubleValue();
                FailNum = Arith.round(d,0);
		}
	}

	public String getCheckTransType()
	{
		return CheckTransType;
	}
	public void setCheckTransType(String aCheckTransType)
	{
		CheckTransType = aCheckTransType;
	}
	public String getTransDate()
	{
		return TransDate;
	}
	public void setTransDate(String aTransDate)
	{
		TransDate = aTransDate;
	}
	public String getPolicyNo()
	{
		return PolicyNo;
	}
	public void setPolicyNo(String aPolicyNo)
	{
		PolicyNo = aPolicyNo;
	}
	public String getSequenceNo()
	{
		return SequenceNo;
	}
	public void setSequenceNo(String aSequenceNo)
	{
		SequenceNo = aSequenceNo;
	}
	public String getEndorsementNo()
	{
		return EndorsementNo;
	}
	public void setEndorsementNo(String aEndorsementNo)
	{
		EndorsementNo = aEndorsementNo;
	}
	public String getClaimNo()
	{
		return ClaimNo;
	}
	public void setClaimNo(String aClaimNo)
	{
		ClaimNo = aClaimNo;
	}
	public String getFeeId()
	{
		return FeeId;
	}
	public void setFeeId(String aFeeId)
	{
		FeeId = aFeeId;
	}
	public String getFeeStatus()
	{
		return FeeStatus;
	}
	public void setFeeStatus(String aFeeStatus)
	{
		FeeStatus = aFeeStatus;
	}
	public String getRenewalEndorsementNo()
	{
		return RenewalEndorsementNo;
	}
	public void setRenewalEndorsementNo(String aRenewalEndorsementNo)
	{
		RenewalEndorsementNo = aRenewalEndorsementNo;
	}
	public String getComFeeId()
	{
		return ComFeeId;
	}
	public void setComFeeId(String aComFeeId)
	{
		ComFeeId = aComFeeId;
	}
	public String getReturnCode()
	{
		return ReturnCode;
	}
	public void setReturnCode(String aReturnCode)
	{
		ReturnCode = aReturnCode;
	}
	public String getErrorMessage()
	{
		return ErrorMessage;
	}
	public void setErrorMessage(String aErrorMessage)
	{
		ErrorMessage = aErrorMessage;
	}
	public String getResultStatus()
	{
		return ResultStatus;
	}
	public void setResultStatus(String aResultStatus)
	{
		ResultStatus = aResultStatus;
	}
	public String getResultInfoDesc()
	{
		return ResultInfoDesc;
	}
	public void setResultInfoDesc(String aResultInfoDesc)
	{
		ResultInfoDesc = aResultInfoDesc;
	}
	public String getCiitcResultCode()
	{
		return CiitcResultCode;
	}
	public void setCiitcResultCode(String aCiitcResultCode)
	{
		CiitcResultCode = aCiitcResultCode;
	}
	public String getCiitcResultMessage()
	{
		return CiitcResultMessage;
	}
	public void setCiitcResultMessage(String aCiitcResultMessage)
	{
		CiitcResultMessage = aCiitcResultMessage;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 BusinessCheckSchema 对象给 Schema 赋值
	* @param: aBusinessCheckSchema BusinessCheckSchema
	**/
	public void setSchema(BusinessCheckSchema aBusinessCheckSchema)
	{
		this.SerNo = aBusinessCheckSchema.getSerNo();
		this.CompanyCode = aBusinessCheckSchema.getCompanyCode();
		this.CheckDate = fDate.getDate( aBusinessCheckSchema.getCheckDate());
		this.TotalNum = aBusinessCheckSchema.getTotalNum();
		this.SuccessNum = aBusinessCheckSchema.getSuccessNum();
		this.FailNum = aBusinessCheckSchema.getFailNum();
		this.CheckTransType = aBusinessCheckSchema.getCheckTransType();
		this.TransDate = aBusinessCheckSchema.getTransDate();
		this.PolicyNo = aBusinessCheckSchema.getPolicyNo();
		this.SequenceNo = aBusinessCheckSchema.getSequenceNo();
		this.EndorsementNo = aBusinessCheckSchema.getEndorsementNo();
		this.ClaimNo = aBusinessCheckSchema.getClaimNo();
		this.FeeId = aBusinessCheckSchema.getFeeId();
		this.FeeStatus = aBusinessCheckSchema.getFeeStatus();
		this.RenewalEndorsementNo = aBusinessCheckSchema.getRenewalEndorsementNo();
		this.ComFeeId = aBusinessCheckSchema.getComFeeId();
		this.ReturnCode = aBusinessCheckSchema.getReturnCode();
		this.ErrorMessage = aBusinessCheckSchema.getErrorMessage();
		this.ResultStatus = aBusinessCheckSchema.getResultStatus();
		this.ResultInfoDesc = aBusinessCheckSchema.getResultInfoDesc();
		this.CiitcResultCode = aBusinessCheckSchema.getCiitcResultCode();
		this.CiitcResultMessage = aBusinessCheckSchema.getCiitcResultMessage();
		this.MakeDate = fDate.getDate( aBusinessCheckSchema.getMakeDate());
		this.MakeTime = aBusinessCheckSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aBusinessCheckSchema.getModifyDate());
		this.ModifyTime = aBusinessCheckSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerNo") == null )
				this.SerNo = null;
			else
				this.SerNo = rs.getString("SerNo").trim();

			if( rs.getString("CompanyCode") == null )
				this.CompanyCode = null;
			else
				this.CompanyCode = rs.getString("CompanyCode").trim();

			this.CheckDate = rs.getDate("CheckDate");
			this.TotalNum = rs.getDouble("TotalNum");
			this.SuccessNum = rs.getDouble("SuccessNum");
			this.FailNum = rs.getDouble("FailNum");
			if( rs.getString("CheckTransType") == null )
				this.CheckTransType = null;
			else
				this.CheckTransType = rs.getString("CheckTransType").trim();

			if( rs.getString("TransDate") == null )
				this.TransDate = null;
			else
				this.TransDate = rs.getString("TransDate").trim();

			if( rs.getString("PolicyNo") == null )
				this.PolicyNo = null;
			else
				this.PolicyNo = rs.getString("PolicyNo").trim();

			if( rs.getString("SequenceNo") == null )
				this.SequenceNo = null;
			else
				this.SequenceNo = rs.getString("SequenceNo").trim();

			if( rs.getString("EndorsementNo") == null )
				this.EndorsementNo = null;
			else
				this.EndorsementNo = rs.getString("EndorsementNo").trim();

			if( rs.getString("ClaimNo") == null )
				this.ClaimNo = null;
			else
				this.ClaimNo = rs.getString("ClaimNo").trim();

			if( rs.getString("FeeId") == null )
				this.FeeId = null;
			else
				this.FeeId = rs.getString("FeeId").trim();

			if( rs.getString("FeeStatus") == null )
				this.FeeStatus = null;
			else
				this.FeeStatus = rs.getString("FeeStatus").trim();

			if( rs.getString("RenewalEndorsementNo") == null )
				this.RenewalEndorsementNo = null;
			else
				this.RenewalEndorsementNo = rs.getString("RenewalEndorsementNo").trim();

			if( rs.getString("ComFeeId") == null )
				this.ComFeeId = null;
			else
				this.ComFeeId = rs.getString("ComFeeId").trim();

			if( rs.getString("ReturnCode") == null )
				this.ReturnCode = null;
			else
				this.ReturnCode = rs.getString("ReturnCode").trim();

			if( rs.getString("ErrorMessage") == null )
				this.ErrorMessage = null;
			else
				this.ErrorMessage = rs.getString("ErrorMessage").trim();

			if( rs.getString("ResultStatus") == null )
				this.ResultStatus = null;
			else
				this.ResultStatus = rs.getString("ResultStatus").trim();

			if( rs.getString("ResultInfoDesc") == null )
				this.ResultInfoDesc = null;
			else
				this.ResultInfoDesc = rs.getString("ResultInfoDesc").trim();

			if( rs.getString("CiitcResultCode") == null )
				this.CiitcResultCode = null;
			else
				this.CiitcResultCode = rs.getString("CiitcResultCode").trim();

			if( rs.getString("CiitcResultMessage") == null )
				this.CiitcResultMessage = null;
			else
				this.CiitcResultMessage = rs.getString("CiitcResultMessage").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的BusinessCheck表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BusinessCheckSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public BusinessCheckSchema getSchema()
	{
		BusinessCheckSchema aBusinessCheckSchema = new BusinessCheckSchema();
		aBusinessCheckSchema.setSchema(this);
		return aBusinessCheckSchema;
	}

	public BusinessCheckDB getDB()
	{
		BusinessCheckDB aDBOper = new BusinessCheckDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBusinessCheck描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CheckDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TotalNum));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SuccessNum));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FailNum));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckTransType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RenewalEndorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComFeeId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultInfoDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CiitcResultCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CiitcResultMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBusinessCheck>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CheckDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			TotalNum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).doubleValue();
			SuccessNum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			FailNum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			CheckTransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			TransDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			SequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			EndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ClaimNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			FeeId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			FeeStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			RenewalEndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ComFeeId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ReturnCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ErrorMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ResultStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ResultInfoDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			CiitcResultCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			CiitcResultMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BusinessCheckSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerNo));
		}
		if (FCode.equals("CompanyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyCode));
		}
		if (FCode.equals("CheckDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCheckDate()));
		}
		if (FCode.equals("TotalNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TotalNum));
		}
		if (FCode.equals("SuccessNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuccessNum));
		}
		if (FCode.equals("FailNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FailNum));
		}
		if (FCode.equals("CheckTransType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckTransType));
		}
		if (FCode.equals("TransDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransDate));
		}
		if (FCode.equals("PolicyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
		}
		if (FCode.equals("SequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
		}
		if (FCode.equals("EndorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndorsementNo));
		}
		if (FCode.equals("ClaimNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimNo));
		}
		if (FCode.equals("FeeId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeId));
		}
		if (FCode.equals("FeeStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeStatus));
		}
		if (FCode.equals("RenewalEndorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RenewalEndorsementNo));
		}
		if (FCode.equals("ComFeeId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComFeeId));
		}
		if (FCode.equals("ReturnCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnCode));
		}
		if (FCode.equals("ErrorMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorMessage));
		}
		if (FCode.equals("ResultStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultStatus));
		}
		if (FCode.equals("ResultInfoDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultInfoDesc));
		}
		if (FCode.equals("CiitcResultCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CiitcResultCode));
		}
		if (FCode.equals("CiitcResultMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CiitcResultMessage));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CompanyCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCheckDate()));
				break;
			case 3:
				strFieldValue = String.valueOf(TotalNum);
				break;
			case 4:
				strFieldValue = String.valueOf(SuccessNum);
				break;
			case 5:
				strFieldValue = String.valueOf(FailNum);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CheckTransType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(TransDate);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PolicyNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(SequenceNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(EndorsementNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ClaimNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(FeeId);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(FeeStatus);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(RenewalEndorsementNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ComFeeId);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ReturnCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ErrorMessage);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ResultStatus);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ResultInfoDesc);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(CiitcResultCode);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(CiitcResultMessage);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerNo = FValue.trim();
			}
			else
				SerNo = null;
		}
		if (FCode.equalsIgnoreCase("CompanyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyCode = FValue.trim();
			}
			else
				CompanyCode = null;
		}
		if (FCode.equalsIgnoreCase("CheckDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CheckDate = fDate.getDate( FValue );
			}
			else
				CheckDate = null;
		}
		if (FCode.equalsIgnoreCase("TotalNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TotalNum = d;
			}
		}
		if (FCode.equalsIgnoreCase("SuccessNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SuccessNum = d;
			}
		}
		if (FCode.equalsIgnoreCase("FailNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FailNum = d;
			}
		}
		if (FCode.equalsIgnoreCase("CheckTransType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckTransType = FValue.trim();
			}
			else
				CheckTransType = null;
		}
		if (FCode.equalsIgnoreCase("TransDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransDate = FValue.trim();
			}
			else
				TransDate = null;
		}
		if (FCode.equalsIgnoreCase("PolicyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyNo = FValue.trim();
			}
			else
				PolicyNo = null;
		}
		if (FCode.equalsIgnoreCase("SequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SequenceNo = FValue.trim();
			}
			else
				SequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("EndorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndorsementNo = FValue.trim();
			}
			else
				EndorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("ClaimNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimNo = FValue.trim();
			}
			else
				ClaimNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeId = FValue.trim();
			}
			else
				FeeId = null;
		}
		if (FCode.equalsIgnoreCase("FeeStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeStatus = FValue.trim();
			}
			else
				FeeStatus = null;
		}
		if (FCode.equalsIgnoreCase("RenewalEndorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RenewalEndorsementNo = FValue.trim();
			}
			else
				RenewalEndorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("ComFeeId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComFeeId = FValue.trim();
			}
			else
				ComFeeId = null;
		}
		if (FCode.equalsIgnoreCase("ReturnCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnCode = FValue.trim();
			}
			else
				ReturnCode = null;
		}
		if (FCode.equalsIgnoreCase("ErrorMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorMessage = FValue.trim();
			}
			else
				ErrorMessage = null;
		}
		if (FCode.equalsIgnoreCase("ResultStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultStatus = FValue.trim();
			}
			else
				ResultStatus = null;
		}
		if (FCode.equalsIgnoreCase("ResultInfoDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultInfoDesc = FValue.trim();
			}
			else
				ResultInfoDesc = null;
		}
		if (FCode.equalsIgnoreCase("CiitcResultCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CiitcResultCode = FValue.trim();
			}
			else
				CiitcResultCode = null;
		}
		if (FCode.equalsIgnoreCase("CiitcResultMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CiitcResultMessage = FValue.trim();
			}
			else
				CiitcResultMessage = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		BusinessCheckSchema other = (BusinessCheckSchema)otherObject;
		return
			(SerNo == null ? other.getSerNo() == null : SerNo.equals(other.getSerNo()))
			&& (CompanyCode == null ? other.getCompanyCode() == null : CompanyCode.equals(other.getCompanyCode()))
			&& (CheckDate == null ? other.getCheckDate() == null : fDate.getString(CheckDate).equals(other.getCheckDate()))
			&& TotalNum == other.getTotalNum()
			&& SuccessNum == other.getSuccessNum()
			&& FailNum == other.getFailNum()
			&& (CheckTransType == null ? other.getCheckTransType() == null : CheckTransType.equals(other.getCheckTransType()))
			&& (TransDate == null ? other.getTransDate() == null : TransDate.equals(other.getTransDate()))
			&& (PolicyNo == null ? other.getPolicyNo() == null : PolicyNo.equals(other.getPolicyNo()))
			&& (SequenceNo == null ? other.getSequenceNo() == null : SequenceNo.equals(other.getSequenceNo()))
			&& (EndorsementNo == null ? other.getEndorsementNo() == null : EndorsementNo.equals(other.getEndorsementNo()))
			&& (ClaimNo == null ? other.getClaimNo() == null : ClaimNo.equals(other.getClaimNo()))
			&& (FeeId == null ? other.getFeeId() == null : FeeId.equals(other.getFeeId()))
			&& (FeeStatus == null ? other.getFeeStatus() == null : FeeStatus.equals(other.getFeeStatus()))
			&& (RenewalEndorsementNo == null ? other.getRenewalEndorsementNo() == null : RenewalEndorsementNo.equals(other.getRenewalEndorsementNo()))
			&& (ComFeeId == null ? other.getComFeeId() == null : ComFeeId.equals(other.getComFeeId()))
			&& (ReturnCode == null ? other.getReturnCode() == null : ReturnCode.equals(other.getReturnCode()))
			&& (ErrorMessage == null ? other.getErrorMessage() == null : ErrorMessage.equals(other.getErrorMessage()))
			&& (ResultStatus == null ? other.getResultStatus() == null : ResultStatus.equals(other.getResultStatus()))
			&& (ResultInfoDesc == null ? other.getResultInfoDesc() == null : ResultInfoDesc.equals(other.getResultInfoDesc()))
			&& (CiitcResultCode == null ? other.getCiitcResultCode() == null : CiitcResultCode.equals(other.getCiitcResultCode()))
			&& (CiitcResultMessage == null ? other.getCiitcResultMessage() == null : CiitcResultMessage.equals(other.getCiitcResultMessage()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return 0;
		}
		if( strFieldName.equals("CompanyCode") ) {
			return 1;
		}
		if( strFieldName.equals("CheckDate") ) {
			return 2;
		}
		if( strFieldName.equals("TotalNum") ) {
			return 3;
		}
		if( strFieldName.equals("SuccessNum") ) {
			return 4;
		}
		if( strFieldName.equals("FailNum") ) {
			return 5;
		}
		if( strFieldName.equals("CheckTransType") ) {
			return 6;
		}
		if( strFieldName.equals("TransDate") ) {
			return 7;
		}
		if( strFieldName.equals("PolicyNo") ) {
			return 8;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return 9;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return 10;
		}
		if( strFieldName.equals("ClaimNo") ) {
			return 11;
		}
		if( strFieldName.equals("FeeId") ) {
			return 12;
		}
		if( strFieldName.equals("FeeStatus") ) {
			return 13;
		}
		if( strFieldName.equals("RenewalEndorsementNo") ) {
			return 14;
		}
		if( strFieldName.equals("ComFeeId") ) {
			return 15;
		}
		if( strFieldName.equals("ReturnCode") ) {
			return 16;
		}
		if( strFieldName.equals("ErrorMessage") ) {
			return 17;
		}
		if( strFieldName.equals("ResultStatus") ) {
			return 18;
		}
		if( strFieldName.equals("ResultInfoDesc") ) {
			return 19;
		}
		if( strFieldName.equals("CiitcResultCode") ) {
			return 20;
		}
		if( strFieldName.equals("CiitcResultMessage") ) {
			return 21;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 25;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerNo";
				break;
			case 1:
				strFieldName = "CompanyCode";
				break;
			case 2:
				strFieldName = "CheckDate";
				break;
			case 3:
				strFieldName = "TotalNum";
				break;
			case 4:
				strFieldName = "SuccessNum";
				break;
			case 5:
				strFieldName = "FailNum";
				break;
			case 6:
				strFieldName = "CheckTransType";
				break;
			case 7:
				strFieldName = "TransDate";
				break;
			case 8:
				strFieldName = "PolicyNo";
				break;
			case 9:
				strFieldName = "SequenceNo";
				break;
			case 10:
				strFieldName = "EndorsementNo";
				break;
			case 11:
				strFieldName = "ClaimNo";
				break;
			case 12:
				strFieldName = "FeeId";
				break;
			case 13:
				strFieldName = "FeeStatus";
				break;
			case 14:
				strFieldName = "RenewalEndorsementNo";
				break;
			case 15:
				strFieldName = "ComFeeId";
				break;
			case 16:
				strFieldName = "ReturnCode";
				break;
			case 17:
				strFieldName = "ErrorMessage";
				break;
			case 18:
				strFieldName = "ResultStatus";
				break;
			case 19:
				strFieldName = "ResultInfoDesc";
				break;
			case 20:
				strFieldName = "CiitcResultCode";
				break;
			case 21:
				strFieldName = "CiitcResultMessage";
				break;
			case 22:
				strFieldName = "MakeDate";
				break;
			case 23:
				strFieldName = "MakeTime";
				break;
			case 24:
				strFieldName = "ModifyDate";
				break;
			case 25:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TotalNum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SuccessNum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FailNum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CheckTransType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RenewalEndorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComFeeId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultInfoDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CiitcResultCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CiitcResultMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
