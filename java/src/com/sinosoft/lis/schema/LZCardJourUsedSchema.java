/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZCardJourUsedDB;

/*
 * <p>ClassName: LZCardJourUsedSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LZCardJourUsed
 * @CreateDate：2007-11-09
 */
public class LZCardJourUsedSchema implements Schema, Cloneable
{
	// @Field
	/** 单证编码 */
	private String CertifyCode;
	/** 单证名称 */
	private String CertifyName;
	/** 日结日期 */
	private Date JourDate;
	/** 印刷批次 */
	private String PrtNo;
	/** 单证价格 */
	private double CertifyPrice;
	/** 库存量 */
	private int StockpileNumber;
	/** 未用量 */
	private int UnemployedNumber;
	/** 接收入库量 */
	private int ReceiveNumber;
	/** 回收入库量 */
	private int CallBackNumber;
	/** 出库量 */
	private int SendOutNumber;
	/** 正常使用量 */
	private int NaturalNumber;
	/** 作废量 */
	private int BlankOutNumber;
	/** 损毁量 */
	private int MarNumber;
	/** 遗失量 */
	private int LostNumber;
	/** 空白回销量 */
	private int RetourNumber;
	/** 预留字段 */
	private String Flag;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 所有者 */
	private String Owner;

	public static final int FIELDNUM = 22;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LZCardJourUsedSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "CertifyCode";
		pk[1] = "JourDate";
		pk[2] = "PrtNo";
		pk[3] = "Owner";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LZCardJourUsedSchema cloned = (LZCardJourUsedSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
            CertifyCode = aCertifyCode;
	}
	public String getCertifyName()
	{
		return CertifyName;
	}
	public void setCertifyName(String aCertifyName)
	{
            CertifyName = aCertifyName;
	}
	public String getJourDate()
	{
		if( JourDate != null )
			return fDate.getString(JourDate);
		else
			return null;
	}
	public void setJourDate(Date aJourDate)
	{
            JourDate = aJourDate;
	}
	public void setJourDate(String aJourDate)
	{
		if (aJourDate != null && !aJourDate.equals("") )
		{
			JourDate = fDate.getDate( aJourDate );
		}
		else
			JourDate = null;
	}

	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
            PrtNo = aPrtNo;
	}
	public double getCertifyPrice()
	{
		return CertifyPrice;
	}
	public void setCertifyPrice(double aCertifyPrice)
	{
            CertifyPrice = Arith.round(aCertifyPrice,2);
	}
	public void setCertifyPrice(String aCertifyPrice)
	{
		if (aCertifyPrice != null && !aCertifyPrice.equals(""))
		{
			Double tDouble = new Double(aCertifyPrice);
			double d = tDouble.doubleValue();
                CertifyPrice = Arith.round(d,2);
		}
	}

	public int getStockpileNumber()
	{
		return StockpileNumber;
	}
	public void setStockpileNumber(int aStockpileNumber)
	{
            StockpileNumber = aStockpileNumber;
	}
	public void setStockpileNumber(String aStockpileNumber)
	{
		if (aStockpileNumber != null && !aStockpileNumber.equals(""))
		{
			Integer tInteger = new Integer(aStockpileNumber);
			int i = tInteger.intValue();
			StockpileNumber = i;
		}
	}

	public int getUnemployedNumber()
	{
		return UnemployedNumber;
	}
	public void setUnemployedNumber(int aUnemployedNumber)
	{
            UnemployedNumber = aUnemployedNumber;
	}
	public void setUnemployedNumber(String aUnemployedNumber)
	{
		if (aUnemployedNumber != null && !aUnemployedNumber.equals(""))
		{
			Integer tInteger = new Integer(aUnemployedNumber);
			int i = tInteger.intValue();
			UnemployedNumber = i;
		}
	}

	public int getReceiveNumber()
	{
		return ReceiveNumber;
	}
	public void setReceiveNumber(int aReceiveNumber)
	{
            ReceiveNumber = aReceiveNumber;
	}
	public void setReceiveNumber(String aReceiveNumber)
	{
		if (aReceiveNumber != null && !aReceiveNumber.equals(""))
		{
			Integer tInteger = new Integer(aReceiveNumber);
			int i = tInteger.intValue();
			ReceiveNumber = i;
		}
	}

	public int getCallBackNumber()
	{
		return CallBackNumber;
	}
	public void setCallBackNumber(int aCallBackNumber)
	{
            CallBackNumber = aCallBackNumber;
	}
	public void setCallBackNumber(String aCallBackNumber)
	{
		if (aCallBackNumber != null && !aCallBackNumber.equals(""))
		{
			Integer tInteger = new Integer(aCallBackNumber);
			int i = tInteger.intValue();
			CallBackNumber = i;
		}
	}

	public int getSendOutNumber()
	{
		return SendOutNumber;
	}
	public void setSendOutNumber(int aSendOutNumber)
	{
            SendOutNumber = aSendOutNumber;
	}
	public void setSendOutNumber(String aSendOutNumber)
	{
		if (aSendOutNumber != null && !aSendOutNumber.equals(""))
		{
			Integer tInteger = new Integer(aSendOutNumber);
			int i = tInteger.intValue();
			SendOutNumber = i;
		}
	}

	public int getNaturalNumber()
	{
		return NaturalNumber;
	}
	public void setNaturalNumber(int aNaturalNumber)
	{
            NaturalNumber = aNaturalNumber;
	}
	public void setNaturalNumber(String aNaturalNumber)
	{
		if (aNaturalNumber != null && !aNaturalNumber.equals(""))
		{
			Integer tInteger = new Integer(aNaturalNumber);
			int i = tInteger.intValue();
			NaturalNumber = i;
		}
	}

	public int getBlankOutNumber()
	{
		return BlankOutNumber;
	}
	public void setBlankOutNumber(int aBlankOutNumber)
	{
            BlankOutNumber = aBlankOutNumber;
	}
	public void setBlankOutNumber(String aBlankOutNumber)
	{
		if (aBlankOutNumber != null && !aBlankOutNumber.equals(""))
		{
			Integer tInteger = new Integer(aBlankOutNumber);
			int i = tInteger.intValue();
			BlankOutNumber = i;
		}
	}

	public int getMarNumber()
	{
		return MarNumber;
	}
	public void setMarNumber(int aMarNumber)
	{
            MarNumber = aMarNumber;
	}
	public void setMarNumber(String aMarNumber)
	{
		if (aMarNumber != null && !aMarNumber.equals(""))
		{
			Integer tInteger = new Integer(aMarNumber);
			int i = tInteger.intValue();
			MarNumber = i;
		}
	}

	public int getLostNumber()
	{
		return LostNumber;
	}
	public void setLostNumber(int aLostNumber)
	{
            LostNumber = aLostNumber;
	}
	public void setLostNumber(String aLostNumber)
	{
		if (aLostNumber != null && !aLostNumber.equals(""))
		{
			Integer tInteger = new Integer(aLostNumber);
			int i = tInteger.intValue();
			LostNumber = i;
		}
	}

	public int getRetourNumber()
	{
		return RetourNumber;
	}
	public void setRetourNumber(int aRetourNumber)
	{
            RetourNumber = aRetourNumber;
	}
	public void setRetourNumber(String aRetourNumber)
	{
		if (aRetourNumber != null && !aRetourNumber.equals(""))
		{
			Integer tInteger = new Integer(aRetourNumber);
			int i = tInteger.intValue();
			RetourNumber = i;
		}
	}

	public String getFlag()
	{
		return Flag;
	}
	public void setFlag(String aFlag)
	{
            Flag = aFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getOwner()
	{
		return Owner;
	}
	public void setOwner(String aOwner)
	{
            Owner = aOwner;
	}

	/**
	* 使用另外一个 LZCardJourUsedSchema 对象给 Schema 赋值
	* @param: aLZCardJourUsedSchema LZCardJourUsedSchema
	**/
	public void setSchema(LZCardJourUsedSchema aLZCardJourUsedSchema)
	{
		this.CertifyCode = aLZCardJourUsedSchema.getCertifyCode();
		this.CertifyName = aLZCardJourUsedSchema.getCertifyName();
		this.JourDate = fDate.getDate( aLZCardJourUsedSchema.getJourDate());
		this.PrtNo = aLZCardJourUsedSchema.getPrtNo();
		this.CertifyPrice = aLZCardJourUsedSchema.getCertifyPrice();
		this.StockpileNumber = aLZCardJourUsedSchema.getStockpileNumber();
		this.UnemployedNumber = aLZCardJourUsedSchema.getUnemployedNumber();
		this.ReceiveNumber = aLZCardJourUsedSchema.getReceiveNumber();
		this.CallBackNumber = aLZCardJourUsedSchema.getCallBackNumber();
		this.SendOutNumber = aLZCardJourUsedSchema.getSendOutNumber();
		this.NaturalNumber = aLZCardJourUsedSchema.getNaturalNumber();
		this.BlankOutNumber = aLZCardJourUsedSchema.getBlankOutNumber();
		this.MarNumber = aLZCardJourUsedSchema.getMarNumber();
		this.LostNumber = aLZCardJourUsedSchema.getLostNumber();
		this.RetourNumber = aLZCardJourUsedSchema.getRetourNumber();
		this.Flag = aLZCardJourUsedSchema.getFlag();
		this.Operator = aLZCardJourUsedSchema.getOperator();
		this.MakeDate = fDate.getDate( aLZCardJourUsedSchema.getMakeDate());
		this.MakeTime = aLZCardJourUsedSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLZCardJourUsedSchema.getModifyDate());
		this.ModifyTime = aLZCardJourUsedSchema.getModifyTime();
		this.Owner = aLZCardJourUsedSchema.getOwner();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			if( rs.getString("CertifyName") == null )
				this.CertifyName = null;
			else
				this.CertifyName = rs.getString("CertifyName").trim();

			this.JourDate = rs.getDate("JourDate");
			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			this.CertifyPrice = rs.getDouble("CertifyPrice");
			this.StockpileNumber = rs.getInt("StockpileNumber");
			this.UnemployedNumber = rs.getInt("UnemployedNumber");
			this.ReceiveNumber = rs.getInt("ReceiveNumber");
			this.CallBackNumber = rs.getInt("CallBackNumber");
			this.SendOutNumber = rs.getInt("SendOutNumber");
			this.NaturalNumber = rs.getInt("NaturalNumber");
			this.BlankOutNumber = rs.getInt("BlankOutNumber");
			this.MarNumber = rs.getInt("MarNumber");
			this.LostNumber = rs.getInt("LostNumber");
			this.RetourNumber = rs.getInt("RetourNumber");
			if( rs.getString("Flag") == null )
				this.Flag = null;
			else
				this.Flag = rs.getString("Flag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Owner") == null )
				this.Owner = null;
			else
				this.Owner = rs.getString("Owner").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LZCardJourUsed表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LZCardJourUsedSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LZCardJourUsedSchema getSchema()
	{
		LZCardJourUsedSchema aLZCardJourUsedSchema = new LZCardJourUsedSchema();
		aLZCardJourUsedSchema.setSchema(this);
		return aLZCardJourUsedSchema;
	}

	public LZCardJourUsedDB getDB()
	{
		LZCardJourUsedDB aDBOper = new LZCardJourUsedDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardJourUsed描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CertifyName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( JourDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(CertifyPrice));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(StockpileNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(UnemployedNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ReceiveNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(CallBackNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SendOutNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(NaturalNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(BlankOutNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MarNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(LostNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RetourNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Flag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Owner));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardJourUsed>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CertifyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			JourDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CertifyPrice = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			StockpileNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			UnemployedNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			ReceiveNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).intValue();
			CallBackNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			SendOutNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			NaturalNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).intValue();
			BlankOutNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).intValue();
			MarNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).intValue();
			LostNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).intValue();
			RetourNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).intValue();
			Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Owner = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LZCardJourUsedSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("CertifyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyName));
		}
		if (FCode.equals("JourDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getJourDate()));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("CertifyPrice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyPrice));
		}
		if (FCode.equals("StockpileNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StockpileNumber));
		}
		if (FCode.equals("UnemployedNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnemployedNumber));
		}
		if (FCode.equals("ReceiveNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveNumber));
		}
		if (FCode.equals("CallBackNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CallBackNumber));
		}
		if (FCode.equals("SendOutNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendOutNumber));
		}
		if (FCode.equals("NaturalNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NaturalNumber));
		}
		if (FCode.equals("BlankOutNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlankOutNumber));
		}
		if (FCode.equals("MarNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarNumber));
		}
		if (FCode.equals("LostNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LostNumber));
		}
		if (FCode.equals("RetourNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RetourNumber));
		}
		if (FCode.equals("Flag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Owner"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Owner));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CertifyName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getJourDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 4:
				strFieldValue = String.valueOf(CertifyPrice);
				break;
			case 5:
				strFieldValue = String.valueOf(StockpileNumber);
				break;
			case 6:
				strFieldValue = String.valueOf(UnemployedNumber);
				break;
			case 7:
				strFieldValue = String.valueOf(ReceiveNumber);
				break;
			case 8:
				strFieldValue = String.valueOf(CallBackNumber);
				break;
			case 9:
				strFieldValue = String.valueOf(SendOutNumber);
				break;
			case 10:
				strFieldValue = String.valueOf(NaturalNumber);
				break;
			case 11:
				strFieldValue = String.valueOf(BlankOutNumber);
				break;
			case 12:
				strFieldValue = String.valueOf(MarNumber);
				break;
			case 13:
				strFieldValue = String.valueOf(LostNumber);
				break;
			case 14:
				strFieldValue = String.valueOf(RetourNumber);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Flag);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Owner);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("CertifyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyName = FValue.trim();
			}
			else
				CertifyName = null;
		}
		if (FCode.equalsIgnoreCase("JourDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				JourDate = fDate.getDate( FValue );
			}
			else
				JourDate = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("CertifyPrice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CertifyPrice = d;
			}
		}
		if (FCode.equalsIgnoreCase("StockpileNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				StockpileNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("UnemployedNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				UnemployedNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("ReceiveNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ReceiveNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("CallBackNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CallBackNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("SendOutNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SendOutNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("NaturalNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				NaturalNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("BlankOutNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				BlankOutNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("MarNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MarNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("LostNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				LostNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("RetourNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RetourNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("Flag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Flag = FValue.trim();
			}
			else
				Flag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Owner"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Owner = FValue.trim();
			}
			else
				Owner = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LZCardJourUsedSchema other = (LZCardJourUsedSchema)otherObject;
		return
			CertifyCode.equals(other.getCertifyCode())
			&& CertifyName.equals(other.getCertifyName())
			&& fDate.getString(JourDate).equals(other.getJourDate())
			&& PrtNo.equals(other.getPrtNo())
			&& CertifyPrice == other.getCertifyPrice()
			&& StockpileNumber == other.getStockpileNumber()
			&& UnemployedNumber == other.getUnemployedNumber()
			&& ReceiveNumber == other.getReceiveNumber()
			&& CallBackNumber == other.getCallBackNumber()
			&& SendOutNumber == other.getSendOutNumber()
			&& NaturalNumber == other.getNaturalNumber()
			&& BlankOutNumber == other.getBlankOutNumber()
			&& MarNumber == other.getMarNumber()
			&& LostNumber == other.getLostNumber()
			&& RetourNumber == other.getRetourNumber()
			&& Flag.equals(other.getFlag())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& Owner.equals(other.getOwner());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CertifyCode") ) {
			return 0;
		}
		if( strFieldName.equals("CertifyName") ) {
			return 1;
		}
		if( strFieldName.equals("JourDate") ) {
			return 2;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 3;
		}
		if( strFieldName.equals("CertifyPrice") ) {
			return 4;
		}
		if( strFieldName.equals("StockpileNumber") ) {
			return 5;
		}
		if( strFieldName.equals("UnemployedNumber") ) {
			return 6;
		}
		if( strFieldName.equals("ReceiveNumber") ) {
			return 7;
		}
		if( strFieldName.equals("CallBackNumber") ) {
			return 8;
		}
		if( strFieldName.equals("SendOutNumber") ) {
			return 9;
		}
		if( strFieldName.equals("NaturalNumber") ) {
			return 10;
		}
		if( strFieldName.equals("BlankOutNumber") ) {
			return 11;
		}
		if( strFieldName.equals("MarNumber") ) {
			return 12;
		}
		if( strFieldName.equals("LostNumber") ) {
			return 13;
		}
		if( strFieldName.equals("RetourNumber") ) {
			return 14;
		}
		if( strFieldName.equals("Flag") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		if( strFieldName.equals("Owner") ) {
			return 21;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CertifyCode";
				break;
			case 1:
				strFieldName = "CertifyName";
				break;
			case 2:
				strFieldName = "JourDate";
				break;
			case 3:
				strFieldName = "PrtNo";
				break;
			case 4:
				strFieldName = "CertifyPrice";
				break;
			case 5:
				strFieldName = "StockpileNumber";
				break;
			case 6:
				strFieldName = "UnemployedNumber";
				break;
			case 7:
				strFieldName = "ReceiveNumber";
				break;
			case 8:
				strFieldName = "CallBackNumber";
				break;
			case 9:
				strFieldName = "SendOutNumber";
				break;
			case 10:
				strFieldName = "NaturalNumber";
				break;
			case 11:
				strFieldName = "BlankOutNumber";
				break;
			case 12:
				strFieldName = "MarNumber";
				break;
			case 13:
				strFieldName = "LostNumber";
				break;
			case 14:
				strFieldName = "RetourNumber";
				break;
			case 15:
				strFieldName = "Flag";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			case 21:
				strFieldName = "Owner";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JourDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyPrice") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StockpileNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("UnemployedNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ReceiveNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CallBackNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SendOutNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("NaturalNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BlankOutNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MarNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("LostNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RetourNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Flag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Owner") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_INT;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_INT;
				break;
			case 11:
				nFieldType = Schema.TYPE_INT;
				break;
			case 12:
				nFieldType = Schema.TYPE_INT;
				break;
			case 13:
				nFieldType = Schema.TYPE_INT;
				break;
			case 14:
				nFieldType = Schema.TYPE_INT;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
