/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPGrpPENoticeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LPGrpPENoticeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-04
 */
public class LPGrpPENoticeSchema implements Schema
{
    // @Field
    /** 保全受理号 */
    private String EdorAcceptNo;
    /** 批单号 */
    private String EdorNo;
    /** 批改类型 */
    private String EdorType;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 打印流水号 */
    private String PrtSeq;
    /** 体检日期 */
    private Date PEDate;
    /** 体检地点 */
    private String PEAddress;
    /** 体检前条件 */
    private String PEBeforeCond;
    /** 打印标记 */
    private String PrintFlag;
    /** 管理机构 */
    private String ManageCom;
    /** 代理人姓名 */
    private String AgentName;
    /** 代理人编码 */
    private String AgentCode;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 备注 */
    private String Remark;
    /** 体检结论 */
    private String PEResult;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPGrpPENoticeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "EdorAcceptNo";
        pk[1] = "EdorNo";
        pk[2] = "PrtSeq";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorAcceptNo()
    {
        if (EdorAcceptNo != null && !EdorAcceptNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EdorAcceptNo = StrTool.unicodeToGBK(EdorAcceptNo);
        }
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String aEdorAcceptNo)
    {
        EdorAcceptNo = aEdorAcceptNo;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getEdorType()
    {
        if (EdorType != null && !EdorType.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getPrtSeq()
    {
        if (PrtSeq != null && !PrtSeq.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtSeq = StrTool.unicodeToGBK(PrtSeq);
        }
        return PrtSeq;
    }

    public void setPrtSeq(String aPrtSeq)
    {
        PrtSeq = aPrtSeq;
    }

    public String getPEDate()
    {
        if (PEDate != null)
        {
            return fDate.getString(PEDate);
        }
        else
        {
            return null;
        }
    }

    public void setPEDate(Date aPEDate)
    {
        PEDate = aPEDate;
    }

    public void setPEDate(String aPEDate)
    {
        if (aPEDate != null && !aPEDate.equals(""))
        {
            PEDate = fDate.getDate(aPEDate);
        }
        else
        {
            PEDate = null;
        }
    }

    public String getPEAddress()
    {
        if (PEAddress != null && !PEAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PEAddress = StrTool.unicodeToGBK(PEAddress);
        }
        return PEAddress;
    }

    public void setPEAddress(String aPEAddress)
    {
        PEAddress = aPEAddress;
    }

    public String getPEBeforeCond()
    {
        if (PEBeforeCond != null && !PEBeforeCond.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PEBeforeCond = StrTool.unicodeToGBK(PEBeforeCond);
        }
        return PEBeforeCond;
    }

    public void setPEBeforeCond(String aPEBeforeCond)
    {
        PEBeforeCond = aPEBeforeCond;
    }

    public String getPrintFlag()
    {
        if (PrintFlag != null && !PrintFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PrintFlag = StrTool.unicodeToGBK(PrintFlag);
        }
        return PrintFlag;
    }

    public void setPrintFlag(String aPrintFlag)
    {
        PrintFlag = aPrintFlag;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentName()
    {
        if (AgentName != null && !AgentName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentName = StrTool.unicodeToGBK(AgentName);
        }
        return AgentName;
    }

    public void setAgentName(String aAgentName)
    {
        AgentName = aAgentName;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getPEResult()
    {
        if (PEResult != null && !PEResult.equals("") && SysConst.CHANGECHARSET == true)
        {
            PEResult = StrTool.unicodeToGBK(PEResult);
        }
        return PEResult;
    }

    public void setPEResult(String aPEResult)
    {
        PEResult = aPEResult;
    }

    /**
     * 使用另外一个 LPGrpPENoticeSchema 对象给 Schema 赋值
     * @param: aLPGrpPENoticeSchema LPGrpPENoticeSchema
     **/
    public void setSchema(LPGrpPENoticeSchema aLPGrpPENoticeSchema)
    {
        this.EdorAcceptNo = aLPGrpPENoticeSchema.getEdorAcceptNo();
        this.EdorNo = aLPGrpPENoticeSchema.getEdorNo();
        this.EdorType = aLPGrpPENoticeSchema.getEdorType();
        this.GrpContNo = aLPGrpPENoticeSchema.getGrpContNo();
        this.PrtSeq = aLPGrpPENoticeSchema.getPrtSeq();
        this.PEDate = fDate.getDate(aLPGrpPENoticeSchema.getPEDate());
        this.PEAddress = aLPGrpPENoticeSchema.getPEAddress();
        this.PEBeforeCond = aLPGrpPENoticeSchema.getPEBeforeCond();
        this.PrintFlag = aLPGrpPENoticeSchema.getPrintFlag();
        this.ManageCom = aLPGrpPENoticeSchema.getManageCom();
        this.AgentName = aLPGrpPENoticeSchema.getAgentName();
        this.AgentCode = aLPGrpPENoticeSchema.getAgentCode();
        this.Operator = aLPGrpPENoticeSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPGrpPENoticeSchema.getMakeDate());
        this.MakeTime = aLPGrpPENoticeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPGrpPENoticeSchema.getModifyDate());
        this.ModifyTime = aLPGrpPENoticeSchema.getModifyTime();
        this.Remark = aLPGrpPENoticeSchema.getRemark();
        this.PEResult = aLPGrpPENoticeSchema.getPEResult();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorAcceptNo") == null)
            {
                this.EdorAcceptNo = null;
            }
            else
            {
                this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();
            }

            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("PrtSeq") == null)
            {
                this.PrtSeq = null;
            }
            else
            {
                this.PrtSeq = rs.getString("PrtSeq").trim();
            }

            this.PEDate = rs.getDate("PEDate");
            if (rs.getString("PEAddress") == null)
            {
                this.PEAddress = null;
            }
            else
            {
                this.PEAddress = rs.getString("PEAddress").trim();
            }

            if (rs.getString("PEBeforeCond") == null)
            {
                this.PEBeforeCond = null;
            }
            else
            {
                this.PEBeforeCond = rs.getString("PEBeforeCond").trim();
            }

            if (rs.getString("PrintFlag") == null)
            {
                this.PrintFlag = null;
            }
            else
            {
                this.PrintFlag = rs.getString("PrintFlag").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentName") == null)
            {
                this.AgentName = null;
            }
            else
            {
                this.AgentName = rs.getString("AgentName").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("PEResult") == null)
            {
                this.PEResult = null;
            }
            else
            {
                this.PEResult = rs.getString("PEResult").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPGrpPENoticeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPGrpPENoticeSchema getSchema()
    {
        LPGrpPENoticeSchema aLPGrpPENoticeSchema = new LPGrpPENoticeSchema();
        aLPGrpPENoticeSchema.setSchema(this);
        return aLPGrpPENoticeSchema;
    }

    public LPGrpPENoticeDB getDB()
    {
        LPGrpPENoticeDB aDBOper = new LPGrpPENoticeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPGrpPENotice描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorAcceptNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtSeq)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(PEDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PEAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PEBeforeCond)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrintFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PEResult));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPGrpPENotice>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            PEDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            PEAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            PEBeforeCond = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            PrintFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                    SysConst.PACKAGESPILTER);
            PEResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPGrpPENoticeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorAcceptNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorAcceptNo));
        }
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorType));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("PrtSeq"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtSeq));
        }
        if (FCode.equals("PEDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getPEDate()));
        }
        if (FCode.equals("PEAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PEAddress));
        }
        if (FCode.equals("PEBeforeCond"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PEBeforeCond));
        }
        if (FCode.equals("PrintFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrintFlag));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("AgentName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentName));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("PEResult"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PEResult));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PrtSeq);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPEDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PEAddress);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PEBeforeCond);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PrintFlag);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AgentName);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(PEResult);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorAcceptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
            {
                EdorAcceptNo = null;
            }
        }
        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("PrtSeq"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
            {
                PrtSeq = null;
            }
        }
        if (FCode.equals("PEDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PEDate = fDate.getDate(FValue);
            }
            else
            {
                PEDate = null;
            }
        }
        if (FCode.equals("PEAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PEAddress = FValue.trim();
            }
            else
            {
                PEAddress = null;
            }
        }
        if (FCode.equals("PEBeforeCond"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PEBeforeCond = FValue.trim();
            }
            else
            {
                PEBeforeCond = null;
            }
        }
        if (FCode.equals("PrintFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
            {
                PrintFlag = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
            {
                AgentName = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("PEResult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PEResult = FValue.trim();
            }
            else
            {
                PEResult = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPGrpPENoticeSchema other = (LPGrpPENoticeSchema) otherObject;
        return
                EdorAcceptNo.equals(other.getEdorAcceptNo())
                && EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && GrpContNo.equals(other.getGrpContNo())
                && PrtSeq.equals(other.getPrtSeq())
                && fDate.getString(PEDate).equals(other.getPEDate())
                && PEAddress.equals(other.getPEAddress())
                && PEBeforeCond.equals(other.getPEBeforeCond())
                && PrintFlag.equals(other.getPrintFlag())
                && ManageCom.equals(other.getManageCom())
                && AgentName.equals(other.getAgentName())
                && AgentCode.equals(other.getAgentCode())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Remark.equals(other.getRemark())
                && PEResult.equals(other.getPEResult());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorAcceptNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return 1;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 3;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return 4;
        }
        if (strFieldName.equals("PEDate"))
        {
            return 5;
        }
        if (strFieldName.equals("PEAddress"))
        {
            return 6;
        }
        if (strFieldName.equals("PEBeforeCond"))
        {
            return 7;
        }
        if (strFieldName.equals("PrintFlag"))
        {
            return 8;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 9;
        }
        if (strFieldName.equals("AgentName"))
        {
            return 10;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 11;
        }
        if (strFieldName.equals("Operator"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 16;
        }
        if (strFieldName.equals("Remark"))
        {
            return 17;
        }
        if (strFieldName.equals("PEResult"))
        {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorAcceptNo";
                break;
            case 1:
                strFieldName = "EdorNo";
                break;
            case 2:
                strFieldName = "EdorType";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "PrtSeq";
                break;
            case 5:
                strFieldName = "PEDate";
                break;
            case 6:
                strFieldName = "PEAddress";
                break;
            case 7:
                strFieldName = "PEBeforeCond";
                break;
            case 8:
                strFieldName = "PrintFlag";
                break;
            case 9:
                strFieldName = "ManageCom";
                break;
            case 10:
                strFieldName = "AgentName";
                break;
            case 11:
                strFieldName = "AgentCode";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "Remark";
                break;
            case 18:
                strFieldName = "PEResult";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorAcceptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PEDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PEAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PEBeforeCond"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PEResult"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
