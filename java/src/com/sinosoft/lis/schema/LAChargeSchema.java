/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAChargeDB;

/*
 * <p>ClassName: LAChargeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 扎帐手续费
 * @CreateDate：2018-08-23
 */
public class LAChargeSchema implements Schema, Cloneable
{
	// @Field
	/** 代理机构 */
	private String AgentCom;
	/** 手续费类型 */
	private String ChargeType;
	/** 保单号码 */
	private String PolNo;
	/** 交易号 */
	private String ReceiptNo;
	/** 系列号 */
	private String CommisionSN;
	/** 计算年月代码 */
	private String WageNo;
	/** 集体合同号码 */
	private String GrpContNo;
	/** 集体保单号码 */
	private String GrpPolNo;
	/** 合同号码 */
	private String ContNo;
	/** 展业类型 */
	private String BranchType;
	/** 统计日期 */
	private Date CalDate;
	/** 主险保单号码 */
	private String MainPolNo;
	/** 管理机构 */
	private String ManageCom;
	/** 险种编码 */
	private String RiskCode;
	/** 交易类别 */
	private String TransType;
	/** 第几次交费 */
	private int PayCount;
	/** 交易金额 */
	private double TransMoney;
	/** 手续费比例 */
	private double ChargeRate;
	/** 手续费金额 */
	private double Charge;
	/** 手续费状态 */
	private String ChargeState;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 渠道 */
	private String BranchType2;
	/** 缴费间隔 */
	private int PayIntv;
	/** 提数日期 */
	private Date TMakeDate;
	/** 批次号 */
	private String BatchNo;
	/** 原系列号 */
	private String TCommisionSN;
	/** 可支付比例 */
	private double PerRate;
	/** 可支付佣金 */
	private double PerCharge;
	/** 剩余比例 */
	private double RemainRate;
	/** 剩余佣金 */
	private double RemainCharge;
	/** 增值税率 */
	private double AddTaxRate;
	/** 公对公含税佣金 */
	private double TaxCharge;

	public static final int FIELDNUM = 36;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAChargeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CommisionSN";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAChargeSchema cloned = (LAChargeSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getChargeType()
	{
		return ChargeType;
	}
	public void setChargeType(String aChargeType)
	{
		ChargeType = aChargeType;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getCommisionSN()
	{
		return CommisionSN;
	}
	public void setCommisionSN(String aCommisionSN)
	{
		CommisionSN = aCommisionSN;
	}
	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getCalDate()
	{
		if( CalDate != null )
			return fDate.getString(CalDate);
		else
			return null;
	}
	public void setCalDate(Date aCalDate)
	{
		CalDate = aCalDate;
	}
	public void setCalDate(String aCalDate)
	{
		if (aCalDate != null && !aCalDate.equals("") )
		{
			CalDate = fDate.getDate( aCalDate );
		}
		else
			CalDate = null;
	}

	public String getMainPolNo()
	{
		return MainPolNo;
	}
	public void setMainPolNo(String aMainPolNo)
	{
		MainPolNo = aMainPolNo;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getTransType()
	{
		return TransType;
	}
	public void setTransType(String aTransType)
	{
		TransType = aTransType;
	}
	public int getPayCount()
	{
		return PayCount;
	}
	public void setPayCount(int aPayCount)
	{
		PayCount = aPayCount;
	}
	public void setPayCount(String aPayCount)
	{
		if (aPayCount != null && !aPayCount.equals(""))
		{
			Integer tInteger = new Integer(aPayCount);
			int i = tInteger.intValue();
			PayCount = i;
		}
	}

	public double getTransMoney()
	{
		return TransMoney;
	}
	public void setTransMoney(double aTransMoney)
	{
		TransMoney = Arith.round(aTransMoney,2);
	}
	public void setTransMoney(String aTransMoney)
	{
		if (aTransMoney != null && !aTransMoney.equals(""))
		{
			Double tDouble = new Double(aTransMoney);
			double d = tDouble.doubleValue();
                TransMoney = Arith.round(d,2);
		}
	}

	public double getChargeRate()
	{
		return ChargeRate;
	}
	public void setChargeRate(double aChargeRate)
	{
		ChargeRate = Arith.round(aChargeRate,6);
	}
	public void setChargeRate(String aChargeRate)
	{
		if (aChargeRate != null && !aChargeRate.equals(""))
		{
			Double tDouble = new Double(aChargeRate);
			double d = tDouble.doubleValue();
                ChargeRate = Arith.round(d,6);
		}
	}

	public double getCharge()
	{
		return Charge;
	}
	public void setCharge(double aCharge)
	{
		Charge = Arith.round(aCharge,2);
	}
	public void setCharge(String aCharge)
	{
		if (aCharge != null && !aCharge.equals(""))
		{
			Double tDouble = new Double(aCharge);
			double d = tDouble.doubleValue();
                Charge = Arith.round(d,2);
		}
	}

	public String getChargeState()
	{
		return ChargeState;
	}
	public void setChargeState(String aChargeState)
	{
		ChargeState = aChargeState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public String getTMakeDate()
	{
		if( TMakeDate != null )
			return fDate.getString(TMakeDate);
		else
			return null;
	}
	public void setTMakeDate(Date aTMakeDate)
	{
		TMakeDate = aTMakeDate;
	}
	public void setTMakeDate(String aTMakeDate)
	{
		if (aTMakeDate != null && !aTMakeDate.equals("") )
		{
			TMakeDate = fDate.getDate( aTMakeDate );
		}
		else
			TMakeDate = null;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getTCommisionSN()
	{
		return TCommisionSN;
	}
	public void setTCommisionSN(String aTCommisionSN)
	{
		TCommisionSN = aTCommisionSN;
	}
	public double getPerRate()
	{
		return PerRate;
	}
	public void setPerRate(double aPerRate)
	{
		PerRate = Arith.round(aPerRate,6);
	}
	public void setPerRate(String aPerRate)
	{
		if (aPerRate != null && !aPerRate.equals(""))
		{
			Double tDouble = new Double(aPerRate);
			double d = tDouble.doubleValue();
                PerRate = Arith.round(d,6);
		}
	}

	public double getPerCharge()
	{
		return PerCharge;
	}
	public void setPerCharge(double aPerCharge)
	{
		PerCharge = Arith.round(aPerCharge,2);
	}
	public void setPerCharge(String aPerCharge)
	{
		if (aPerCharge != null && !aPerCharge.equals(""))
		{
			Double tDouble = new Double(aPerCharge);
			double d = tDouble.doubleValue();
                PerCharge = Arith.round(d,2);
		}
	}

	public double getRemainRate()
	{
		return RemainRate;
	}
	public void setRemainRate(double aRemainRate)
	{
		RemainRate = Arith.round(aRemainRate,6);
	}
	public void setRemainRate(String aRemainRate)
	{
		if (aRemainRate != null && !aRemainRate.equals(""))
		{
			Double tDouble = new Double(aRemainRate);
			double d = tDouble.doubleValue();
                RemainRate = Arith.round(d,6);
		}
	}

	public double getRemainCharge()
	{
		return RemainCharge;
	}
	public void setRemainCharge(double aRemainCharge)
	{
		RemainCharge = Arith.round(aRemainCharge,2);
	}
	public void setRemainCharge(String aRemainCharge)
	{
		if (aRemainCharge != null && !aRemainCharge.equals(""))
		{
			Double tDouble = new Double(aRemainCharge);
			double d = tDouble.doubleValue();
                RemainCharge = Arith.round(d,2);
		}
	}

	public double getAddTaxRate()
	{
		return AddTaxRate;
	}
	public void setAddTaxRate(double aAddTaxRate)
	{
		AddTaxRate = Arith.round(aAddTaxRate,6);
	}
	public void setAddTaxRate(String aAddTaxRate)
	{
		if (aAddTaxRate != null && !aAddTaxRate.equals(""))
		{
			Double tDouble = new Double(aAddTaxRate);
			double d = tDouble.doubleValue();
                AddTaxRate = Arith.round(d,6);
		}
	}

	public double getTaxCharge()
	{
		return TaxCharge;
	}
	public void setTaxCharge(double aTaxCharge)
	{
		TaxCharge = Arith.round(aTaxCharge,2);
	}
	public void setTaxCharge(String aTaxCharge)
	{
		if (aTaxCharge != null && !aTaxCharge.equals(""))
		{
			Double tDouble = new Double(aTaxCharge);
			double d = tDouble.doubleValue();
                TaxCharge = Arith.round(d,2);
		}
	}


	/**
	* 使用另外一个 LAChargeSchema 对象给 Schema 赋值
	* @param: aLAChargeSchema LAChargeSchema
	**/
	public void setSchema(LAChargeSchema aLAChargeSchema)
	{
		this.AgentCom = aLAChargeSchema.getAgentCom();
		this.ChargeType = aLAChargeSchema.getChargeType();
		this.PolNo = aLAChargeSchema.getPolNo();
		this.ReceiptNo = aLAChargeSchema.getReceiptNo();
		this.CommisionSN = aLAChargeSchema.getCommisionSN();
		this.WageNo = aLAChargeSchema.getWageNo();
		this.GrpContNo = aLAChargeSchema.getGrpContNo();
		this.GrpPolNo = aLAChargeSchema.getGrpPolNo();
		this.ContNo = aLAChargeSchema.getContNo();
		this.BranchType = aLAChargeSchema.getBranchType();
		this.CalDate = fDate.getDate( aLAChargeSchema.getCalDate());
		this.MainPolNo = aLAChargeSchema.getMainPolNo();
		this.ManageCom = aLAChargeSchema.getManageCom();
		this.RiskCode = aLAChargeSchema.getRiskCode();
		this.TransType = aLAChargeSchema.getTransType();
		this.PayCount = aLAChargeSchema.getPayCount();
		this.TransMoney = aLAChargeSchema.getTransMoney();
		this.ChargeRate = aLAChargeSchema.getChargeRate();
		this.Charge = aLAChargeSchema.getCharge();
		this.ChargeState = aLAChargeSchema.getChargeState();
		this.Operator = aLAChargeSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAChargeSchema.getMakeDate());
		this.MakeTime = aLAChargeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAChargeSchema.getModifyDate());
		this.ModifyTime = aLAChargeSchema.getModifyTime();
		this.BranchType2 = aLAChargeSchema.getBranchType2();
		this.PayIntv = aLAChargeSchema.getPayIntv();
		this.TMakeDate = fDate.getDate( aLAChargeSchema.getTMakeDate());
		this.BatchNo = aLAChargeSchema.getBatchNo();
		this.TCommisionSN = aLAChargeSchema.getTCommisionSN();
		this.PerRate = aLAChargeSchema.getPerRate();
		this.PerCharge = aLAChargeSchema.getPerCharge();
		this.RemainRate = aLAChargeSchema.getRemainRate();
		this.RemainCharge = aLAChargeSchema.getRemainCharge();
		this.AddTaxRate = aLAChargeSchema.getAddTaxRate();
		this.TaxCharge = aLAChargeSchema.getTaxCharge();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("ChargeType") == null )
				this.ChargeType = null;
			else
				this.ChargeType = rs.getString("ChargeType").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			if( rs.getString("CommisionSN") == null )
				this.CommisionSN = null;
			else
				this.CommisionSN = rs.getString("CommisionSN").trim();

			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			this.CalDate = rs.getDate("CalDate");
			if( rs.getString("MainPolNo") == null )
				this.MainPolNo = null;
			else
				this.MainPolNo = rs.getString("MainPolNo").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("TransType") == null )
				this.TransType = null;
			else
				this.TransType = rs.getString("TransType").trim();

			this.PayCount = rs.getInt("PayCount");
			this.TransMoney = rs.getDouble("TransMoney");
			this.ChargeRate = rs.getDouble("ChargeRate");
			this.Charge = rs.getDouble("Charge");
			if( rs.getString("ChargeState") == null )
				this.ChargeState = null;
			else
				this.ChargeState = rs.getString("ChargeState").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			this.PayIntv = rs.getInt("PayIntv");
			this.TMakeDate = rs.getDate("TMakeDate");
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("TCommisionSN") == null )
				this.TCommisionSN = null;
			else
				this.TCommisionSN = rs.getString("TCommisionSN").trim();

			this.PerRate = rs.getDouble("PerRate");
			this.PerCharge = rs.getDouble("PerCharge");
			this.RemainRate = rs.getDouble("RemainRate");
			this.RemainCharge = rs.getDouble("RemainCharge");
			this.AddTaxRate = rs.getDouble("AddTaxRate");
			this.TaxCharge = rs.getDouble("TaxCharge");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LACharge表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAChargeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAChargeSchema getSchema()
	{
		LAChargeSchema aLAChargeSchema = new LAChargeSchema();
		aLAChargeSchema.setSchema(this);
		return aLAChargeSchema;
	}

	public LAChargeDB getDB()
	{
		LAChargeDB aDBOper = new LAChargeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACharge描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChargeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CommisionSN)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ChargeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Charge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChargeState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TCommisionSN)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PerRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PerCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RemainRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RemainCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AddTaxRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TaxCharge));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACharge>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ChargeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CommisionSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MainPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			PayCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).intValue();
			TransMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			ChargeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			Charge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			ChargeState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			TMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			TCommisionSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			PerRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			PerCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			RemainRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).doubleValue();
			RemainCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).doubleValue();
			AddTaxRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).doubleValue();
			TaxCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAChargeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("ChargeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeType));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("CommisionSN"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionSN));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("CalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCalDate()));
		}
		if (FCode.equals("MainPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolNo));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("TransType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
		}
		if (FCode.equals("PayCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
		}
		if (FCode.equals("TransMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoney));
		}
		if (FCode.equals("ChargeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeRate));
		}
		if (FCode.equals("Charge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Charge));
		}
		if (FCode.equals("ChargeState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeState));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("TMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTMakeDate()));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("TCommisionSN"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TCommisionSN));
		}
		if (FCode.equals("PerRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PerRate));
		}
		if (FCode.equals("PerCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PerCharge));
		}
		if (FCode.equals("RemainRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemainRate));
		}
		if (FCode.equals("RemainCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemainCharge));
		}
		if (FCode.equals("AddTaxRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddTaxRate));
		}
		if (FCode.equals("TaxCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxCharge));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ChargeType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CommisionSN);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCalDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MainPolNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(TransType);
				break;
			case 15:
				strFieldValue = String.valueOf(PayCount);
				break;
			case 16:
				strFieldValue = String.valueOf(TransMoney);
				break;
			case 17:
				strFieldValue = String.valueOf(ChargeRate);
				break;
			case 18:
				strFieldValue = String.valueOf(Charge);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ChargeState);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 26:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTMakeDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(TCommisionSN);
				break;
			case 30:
				strFieldValue = String.valueOf(PerRate);
				break;
			case 31:
				strFieldValue = String.valueOf(PerCharge);
				break;
			case 32:
				strFieldValue = String.valueOf(RemainRate);
				break;
			case 33:
				strFieldValue = String.valueOf(RemainCharge);
				break;
			case 34:
				strFieldValue = String.valueOf(AddTaxRate);
				break;
			case 35:
				strFieldValue = String.valueOf(TaxCharge);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("ChargeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChargeType = FValue.trim();
			}
			else
				ChargeType = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("CommisionSN"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommisionSN = FValue.trim();
			}
			else
				CommisionSN = null;
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("CalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CalDate = fDate.getDate( FValue );
			}
			else
				CalDate = null;
		}
		if (FCode.equalsIgnoreCase("MainPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainPolNo = FValue.trim();
			}
			else
				MainPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("TransType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransType = FValue.trim();
			}
			else
				TransType = null;
		}
		if (FCode.equalsIgnoreCase("PayCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("TransMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChargeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ChargeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("Charge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Charge = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChargeState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChargeState = FValue.trim();
			}
			else
				ChargeState = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("TMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TMakeDate = fDate.getDate( FValue );
			}
			else
				TMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("TCommisionSN"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TCommisionSN = FValue.trim();
			}
			else
				TCommisionSN = null;
		}
		if (FCode.equalsIgnoreCase("PerRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PerRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("PerCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PerCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("RemainRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RemainRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("RemainCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RemainCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("AddTaxRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AddTaxRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("TaxCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TaxCharge = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAChargeSchema other = (LAChargeSchema)otherObject;
		return
			(AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (ChargeType == null ? other.getChargeType() == null : ChargeType.equals(other.getChargeType()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (CommisionSN == null ? other.getCommisionSN() == null : CommisionSN.equals(other.getCommisionSN()))
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpPolNo == null ? other.getGrpPolNo() == null : GrpPolNo.equals(other.getGrpPolNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (CalDate == null ? other.getCalDate() == null : fDate.getString(CalDate).equals(other.getCalDate()))
			&& (MainPolNo == null ? other.getMainPolNo() == null : MainPolNo.equals(other.getMainPolNo()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (TransType == null ? other.getTransType() == null : TransType.equals(other.getTransType()))
			&& PayCount == other.getPayCount()
			&& TransMoney == other.getTransMoney()
			&& ChargeRate == other.getChargeRate()
			&& Charge == other.getCharge()
			&& (ChargeState == null ? other.getChargeState() == null : ChargeState.equals(other.getChargeState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& PayIntv == other.getPayIntv()
			&& (TMakeDate == null ? other.getTMakeDate() == null : fDate.getString(TMakeDate).equals(other.getTMakeDate()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (TCommisionSN == null ? other.getTCommisionSN() == null : TCommisionSN.equals(other.getTCommisionSN()))
			&& PerRate == other.getPerRate()
			&& PerCharge == other.getPerCharge()
			&& RemainRate == other.getRemainRate()
			&& RemainCharge == other.getRemainCharge()
			&& AddTaxRate == other.getAddTaxRate()
			&& TaxCharge == other.getTaxCharge();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AgentCom") ) {
			return 0;
		}
		if( strFieldName.equals("ChargeType") ) {
			return 1;
		}
		if( strFieldName.equals("PolNo") ) {
			return 2;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 3;
		}
		if( strFieldName.equals("CommisionSN") ) {
			return 4;
		}
		if( strFieldName.equals("WageNo") ) {
			return 5;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 6;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 7;
		}
		if( strFieldName.equals("ContNo") ) {
			return 8;
		}
		if( strFieldName.equals("BranchType") ) {
			return 9;
		}
		if( strFieldName.equals("CalDate") ) {
			return 10;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return 11;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 12;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 13;
		}
		if( strFieldName.equals("TransType") ) {
			return 14;
		}
		if( strFieldName.equals("PayCount") ) {
			return 15;
		}
		if( strFieldName.equals("TransMoney") ) {
			return 16;
		}
		if( strFieldName.equals("ChargeRate") ) {
			return 17;
		}
		if( strFieldName.equals("Charge") ) {
			return 18;
		}
		if( strFieldName.equals("ChargeState") ) {
			return 19;
		}
		if( strFieldName.equals("Operator") ) {
			return 20;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 24;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 25;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 26;
		}
		if( strFieldName.equals("TMakeDate") ) {
			return 27;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 28;
		}
		if( strFieldName.equals("TCommisionSN") ) {
			return 29;
		}
		if( strFieldName.equals("PerRate") ) {
			return 30;
		}
		if( strFieldName.equals("PerCharge") ) {
			return 31;
		}
		if( strFieldName.equals("RemainRate") ) {
			return 32;
		}
		if( strFieldName.equals("RemainCharge") ) {
			return 33;
		}
		if( strFieldName.equals("AddTaxRate") ) {
			return 34;
		}
		if( strFieldName.equals("TaxCharge") ) {
			return 35;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AgentCom";
				break;
			case 1:
				strFieldName = "ChargeType";
				break;
			case 2:
				strFieldName = "PolNo";
				break;
			case 3:
				strFieldName = "ReceiptNo";
				break;
			case 4:
				strFieldName = "CommisionSN";
				break;
			case 5:
				strFieldName = "WageNo";
				break;
			case 6:
				strFieldName = "GrpContNo";
				break;
			case 7:
				strFieldName = "GrpPolNo";
				break;
			case 8:
				strFieldName = "ContNo";
				break;
			case 9:
				strFieldName = "BranchType";
				break;
			case 10:
				strFieldName = "CalDate";
				break;
			case 11:
				strFieldName = "MainPolNo";
				break;
			case 12:
				strFieldName = "ManageCom";
				break;
			case 13:
				strFieldName = "RiskCode";
				break;
			case 14:
				strFieldName = "TransType";
				break;
			case 15:
				strFieldName = "PayCount";
				break;
			case 16:
				strFieldName = "TransMoney";
				break;
			case 17:
				strFieldName = "ChargeRate";
				break;
			case 18:
				strFieldName = "Charge";
				break;
			case 19:
				strFieldName = "ChargeState";
				break;
			case 20:
				strFieldName = "Operator";
				break;
			case 21:
				strFieldName = "MakeDate";
				break;
			case 22:
				strFieldName = "MakeTime";
				break;
			case 23:
				strFieldName = "ModifyDate";
				break;
			case 24:
				strFieldName = "ModifyTime";
				break;
			case 25:
				strFieldName = "BranchType2";
				break;
			case 26:
				strFieldName = "PayIntv";
				break;
			case 27:
				strFieldName = "TMakeDate";
				break;
			case 28:
				strFieldName = "BatchNo";
				break;
			case 29:
				strFieldName = "TCommisionSN";
				break;
			case 30:
				strFieldName = "PerRate";
				break;
			case 31:
				strFieldName = "PerCharge";
				break;
			case 32:
				strFieldName = "RemainRate";
				break;
			case 33:
				strFieldName = "RemainCharge";
				break;
			case 34:
				strFieldName = "AddTaxRate";
				break;
			case 35:
				strFieldName = "TaxCharge";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChargeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CommisionSN") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("TransMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChargeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Charge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChargeState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("TMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TCommisionSN") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PerRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PerCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RemainRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RemainCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AddTaxRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TaxCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_INT;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 33:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 34:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
