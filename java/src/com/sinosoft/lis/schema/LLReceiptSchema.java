/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLReceiptDB;

/*
 * <p>ClassName: LLReceiptSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2014-02-12
 */
public class LLReceiptSchema implements Schema, Cloneable
{
	// @Field
	/** 单据项编码 */
	private String ReceiptNo;
	/** 所属单据唯一流水号 */
	private String ReceiptNum;
	/** 理赔号 */
	private String CaseNo;
	/** 账单号码 */
	private String MainfeeNo;
	/** 服务日期 */
	private Date ServiceDate;
	/** 产品编码 */
	private String ProductCode;
	/** 产品名称 */
	private String ProductName;
	/** 产品类别 */
	private String ProductType;
	/** 国家药品编码 */
	private String DrugCode;
	/** 收费项目级别 */
	private String PayService;
	/** 药物商品名 */
	private String DrugName;
	/** 单价 */
	private double UnitPrice;
	/** 数量 */
	private String Number;
	/** 规格 */
	private String Specifications;
	/** 剂型 */
	private String Dosage;
	/** 总价 */
	private double Sumprice;
	/** 执行医生执业编码 */
	private String ProfessionalCode;
	/** 医生名称 */
	private String DoctorName;
	/** 科室编码 */
	private String DepartmentCode;
	/** 科室名称 */
	private String DepartmentName;
	/** 用法 */
	private String Usage;
	/** 给药途径 */
	private String DrugType;
	/** 用量 */
	private String Consumption;
	/** 频次 */
	private String Frequency;
	/** 用药天数 */
	private double DrugDay;
	/** 医保内金额 */
	private double Receiptfeeinsecu;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 31;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLReceiptSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "ReceiptNo";
		pk[1] = "ReceiptNum";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLReceiptSchema cloned = (LLReceiptSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getReceiptNum()
	{
		return ReceiptNum;
	}
	public void setReceiptNum(String aReceiptNum)
	{
		ReceiptNum = aReceiptNum;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getMainfeeNo()
	{
		return MainfeeNo;
	}
	public void setMainfeeNo(String aMainfeeNo)
	{
		MainfeeNo = aMainfeeNo;
	}
	public String getServiceDate()
	{
		if( ServiceDate != null )
			return fDate.getString(ServiceDate);
		else
			return null;
	}
	public void setServiceDate(Date aServiceDate)
	{
		ServiceDate = aServiceDate;
	}
	public void setServiceDate(String aServiceDate)
	{
		if (aServiceDate != null && !aServiceDate.equals("") )
		{
			ServiceDate = fDate.getDate( aServiceDate );
		}
		else
			ServiceDate = null;
	}

	public String getProductCode()
	{
		return ProductCode;
	}
	public void setProductCode(String aProductCode)
	{
		ProductCode = aProductCode;
	}
	public String getProductName()
	{
		return ProductName;
	}
	public void setProductName(String aProductName)
	{
		ProductName = aProductName;
	}
	public String getProductType()
	{
		return ProductType;
	}
	public void setProductType(String aProductType)
	{
		ProductType = aProductType;
	}
	public String getDrugCode()
	{
		return DrugCode;
	}
	public void setDrugCode(String aDrugCode)
	{
		DrugCode = aDrugCode;
	}
	public String getPayService()
	{
		return PayService;
	}
	public void setPayService(String aPayService)
	{
		PayService = aPayService;
	}
	public String getDrugName()
	{
		return DrugName;
	}
	public void setDrugName(String aDrugName)
	{
		DrugName = aDrugName;
	}
	public double getUnitPrice()
	{
		return UnitPrice;
	}
	public void setUnitPrice(double aUnitPrice)
	{
		UnitPrice = Arith.round(aUnitPrice,2);
	}
	public void setUnitPrice(String aUnitPrice)
	{
		if (aUnitPrice != null && !aUnitPrice.equals(""))
		{
			Double tDouble = new Double(aUnitPrice);
			double d = tDouble.doubleValue();
                UnitPrice = Arith.round(d,2);
		}
	}

	public String getNumber()
	{
		return Number;
	}
	public void setNumber(String aNumber)
	{
		Number = aNumber;
	}
	public String getSpecifications()
	{
		return Specifications;
	}
	public void setSpecifications(String aSpecifications)
	{
		Specifications = aSpecifications;
	}
	public String getDosage()
	{
		return Dosage;
	}
	public void setDosage(String aDosage)
	{
		Dosage = aDosage;
	}
	public double getSumprice()
	{
		return Sumprice;
	}
	public void setSumprice(double aSumprice)
	{
		Sumprice = Arith.round(aSumprice,2);
	}
	public void setSumprice(String aSumprice)
	{
		if (aSumprice != null && !aSumprice.equals(""))
		{
			Double tDouble = new Double(aSumprice);
			double d = tDouble.doubleValue();
                Sumprice = Arith.round(d,2);
		}
	}

	public String getProfessionalCode()
	{
		return ProfessionalCode;
	}
	public void setProfessionalCode(String aProfessionalCode)
	{
		ProfessionalCode = aProfessionalCode;
	}
	public String getDoctorName()
	{
		return DoctorName;
	}
	public void setDoctorName(String aDoctorName)
	{
		DoctorName = aDoctorName;
	}
	public String getDepartmentCode()
	{
		return DepartmentCode;
	}
	public void setDepartmentCode(String aDepartmentCode)
	{
		DepartmentCode = aDepartmentCode;
	}
	public String getDepartmentName()
	{
		return DepartmentName;
	}
	public void setDepartmentName(String aDepartmentName)
	{
		DepartmentName = aDepartmentName;
	}
	public String getUsage()
	{
		return Usage;
	}
	public void setUsage(String aUsage)
	{
		Usage = aUsage;
	}
	public String getDrugType()
	{
		return DrugType;
	}
	public void setDrugType(String aDrugType)
	{
		DrugType = aDrugType;
	}
	public String getConsumption()
	{
		return Consumption;
	}
	public void setConsumption(String aConsumption)
	{
		Consumption = aConsumption;
	}
	public String getFrequency()
	{
		return Frequency;
	}
	public void setFrequency(String aFrequency)
	{
		Frequency = aFrequency;
	}
	public double getDrugDay()
	{
		return DrugDay;
	}
	public void setDrugDay(double aDrugDay)
	{
		DrugDay = Arith.round(aDrugDay,2);
	}
	public void setDrugDay(String aDrugDay)
	{
		if (aDrugDay != null && !aDrugDay.equals(""))
		{
			Double tDouble = new Double(aDrugDay);
			double d = tDouble.doubleValue();
                DrugDay = Arith.round(d,2);
		}
	}

	public double getReceiptfeeinsecu()
	{
		return Receiptfeeinsecu;
	}
	public void setReceiptfeeinsecu(double aReceiptfeeinsecu)
	{
		Receiptfeeinsecu = Arith.round(aReceiptfeeinsecu,2);
	}
	public void setReceiptfeeinsecu(String aReceiptfeeinsecu)
	{
		if (aReceiptfeeinsecu != null && !aReceiptfeeinsecu.equals(""))
		{
			Double tDouble = new Double(aReceiptfeeinsecu);
			double d = tDouble.doubleValue();
                Receiptfeeinsecu = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLReceiptSchema 对象给 Schema 赋值
	* @param: aLLReceiptSchema LLReceiptSchema
	**/
	public void setSchema(LLReceiptSchema aLLReceiptSchema)
	{
		this.ReceiptNo = aLLReceiptSchema.getReceiptNo();
		this.ReceiptNum = aLLReceiptSchema.getReceiptNum();
		this.CaseNo = aLLReceiptSchema.getCaseNo();
		this.MainfeeNo = aLLReceiptSchema.getMainfeeNo();
		this.ServiceDate = fDate.getDate( aLLReceiptSchema.getServiceDate());
		this.ProductCode = aLLReceiptSchema.getProductCode();
		this.ProductName = aLLReceiptSchema.getProductName();
		this.ProductType = aLLReceiptSchema.getProductType();
		this.DrugCode = aLLReceiptSchema.getDrugCode();
		this.PayService = aLLReceiptSchema.getPayService();
		this.DrugName = aLLReceiptSchema.getDrugName();
		this.UnitPrice = aLLReceiptSchema.getUnitPrice();
		this.Number = aLLReceiptSchema.getNumber();
		this.Specifications = aLLReceiptSchema.getSpecifications();
		this.Dosage = aLLReceiptSchema.getDosage();
		this.Sumprice = aLLReceiptSchema.getSumprice();
		this.ProfessionalCode = aLLReceiptSchema.getProfessionalCode();
		this.DoctorName = aLLReceiptSchema.getDoctorName();
		this.DepartmentCode = aLLReceiptSchema.getDepartmentCode();
		this.DepartmentName = aLLReceiptSchema.getDepartmentName();
		this.Usage = aLLReceiptSchema.getUsage();
		this.DrugType = aLLReceiptSchema.getDrugType();
		this.Consumption = aLLReceiptSchema.getConsumption();
		this.Frequency = aLLReceiptSchema.getFrequency();
		this.DrugDay = aLLReceiptSchema.getDrugDay();
		this.Receiptfeeinsecu = aLLReceiptSchema.getReceiptfeeinsecu();
		this.Operator = aLLReceiptSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLReceiptSchema.getMakeDate());
		this.MakeTime = aLLReceiptSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLReceiptSchema.getModifyDate());
		this.ModifyTime = aLLReceiptSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			if( rs.getString("ReceiptNum") == null )
				this.ReceiptNum = null;
			else
				this.ReceiptNum = rs.getString("ReceiptNum").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("MainfeeNo") == null )
				this.MainfeeNo = null;
			else
				this.MainfeeNo = rs.getString("MainfeeNo").trim();

			this.ServiceDate = rs.getDate("ServiceDate");
			if( rs.getString("ProductCode") == null )
				this.ProductCode = null;
			else
				this.ProductCode = rs.getString("ProductCode").trim();

			if( rs.getString("ProductName") == null )
				this.ProductName = null;
			else
				this.ProductName = rs.getString("ProductName").trim();

			if( rs.getString("ProductType") == null )
				this.ProductType = null;
			else
				this.ProductType = rs.getString("ProductType").trim();

			if( rs.getString("DrugCode") == null )
				this.DrugCode = null;
			else
				this.DrugCode = rs.getString("DrugCode").trim();

			if( rs.getString("PayService") == null )
				this.PayService = null;
			else
				this.PayService = rs.getString("PayService").trim();

			if( rs.getString("DrugName") == null )
				this.DrugName = null;
			else
				this.DrugName = rs.getString("DrugName").trim();

			this.UnitPrice = rs.getDouble("UnitPrice");
			if( rs.getString("Number") == null )
				this.Number = null;
			else
				this.Number = rs.getString("Number").trim();

			if( rs.getString("Specifications") == null )
				this.Specifications = null;
			else
				this.Specifications = rs.getString("Specifications").trim();

			if( rs.getString("Dosage") == null )
				this.Dosage = null;
			else
				this.Dosage = rs.getString("Dosage").trim();

			this.Sumprice = rs.getDouble("Sumprice");
			if( rs.getString("ProfessionalCode") == null )
				this.ProfessionalCode = null;
			else
				this.ProfessionalCode = rs.getString("ProfessionalCode").trim();

			if( rs.getString("DoctorName") == null )
				this.DoctorName = null;
			else
				this.DoctorName = rs.getString("DoctorName").trim();

			if( rs.getString("DepartmentCode") == null )
				this.DepartmentCode = null;
			else
				this.DepartmentCode = rs.getString("DepartmentCode").trim();

			if( rs.getString("DepartmentName") == null )
				this.DepartmentName = null;
			else
				this.DepartmentName = rs.getString("DepartmentName").trim();

			if( rs.getString("Usage") == null )
				this.Usage = null;
			else
				this.Usage = rs.getString("Usage").trim();

			if( rs.getString("DrugType") == null )
				this.DrugType = null;
			else
				this.DrugType = rs.getString("DrugType").trim();

			if( rs.getString("Consumption") == null )
				this.Consumption = null;
			else
				this.Consumption = rs.getString("Consumption").trim();

			if( rs.getString("Frequency") == null )
				this.Frequency = null;
			else
				this.Frequency = rs.getString("Frequency").trim();

			this.DrugDay = rs.getDouble("DrugDay");
			this.Receiptfeeinsecu = rs.getDouble("Receiptfeeinsecu");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLReceipt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLReceiptSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLReceiptSchema getSchema()
	{
		LLReceiptSchema aLLReceiptSchema = new LLReceiptSchema();
		aLLReceiptSchema.setSchema(this);
		return aLLReceiptSchema;
	}

	public LLReceiptDB getDB()
	{
		LLReceiptDB aDBOper = new LLReceiptDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLReceipt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNum)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainfeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ServiceDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProductCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProductName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProductType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayService)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(UnitPrice));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Number)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Specifications)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dosage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Sumprice));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProfessionalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DoctorName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DepartmentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DepartmentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Usage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrugType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Consumption)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Frequency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DrugDay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Receiptfeeinsecu));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLReceipt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReceiptNum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			MainfeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ServiceDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			ProductCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ProductName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ProductType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DrugCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PayService = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			DrugName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			UnitPrice = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			Number = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Specifications = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Dosage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Sumprice = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			ProfessionalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			DoctorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			DepartmentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			DepartmentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Usage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			DrugType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Consumption = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Frequency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			DrugDay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			Receiptfeeinsecu = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLReceiptSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("ReceiptNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNum));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("MainfeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainfeeNo));
		}
		if (FCode.equals("ServiceDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getServiceDate()));
		}
		if (FCode.equals("ProductCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProductCode));
		}
		if (FCode.equals("ProductName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProductName));
		}
		if (FCode.equals("ProductType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProductType));
		}
		if (FCode.equals("DrugCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugCode));
		}
		if (FCode.equals("PayService"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayService));
		}
		if (FCode.equals("DrugName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugName));
		}
		if (FCode.equals("UnitPrice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnitPrice));
		}
		if (FCode.equals("Number"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Number));
		}
		if (FCode.equals("Specifications"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Specifications));
		}
		if (FCode.equals("Dosage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dosage));
		}
		if (FCode.equals("Sumprice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sumprice));
		}
		if (FCode.equals("ProfessionalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProfessionalCode));
		}
		if (FCode.equals("DoctorName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctorName));
		}
		if (FCode.equals("DepartmentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepartmentCode));
		}
		if (FCode.equals("DepartmentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepartmentName));
		}
		if (FCode.equals("Usage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Usage));
		}
		if (FCode.equals("DrugType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugType));
		}
		if (FCode.equals("Consumption"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Consumption));
		}
		if (FCode.equals("Frequency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Frequency));
		}
		if (FCode.equals("DrugDay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugDay));
		}
		if (FCode.equals("Receiptfeeinsecu"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Receiptfeeinsecu));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNum);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(MainfeeNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getServiceDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ProductCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ProductName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ProductType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(DrugCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PayService);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(DrugName);
				break;
			case 11:
				strFieldValue = String.valueOf(UnitPrice);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Number);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Specifications);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Dosage);
				break;
			case 15:
				strFieldValue = String.valueOf(Sumprice);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ProfessionalCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(DoctorName);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(DepartmentCode);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(DepartmentName);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Usage);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(DrugType);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Consumption);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Frequency);
				break;
			case 24:
				strFieldValue = String.valueOf(DrugDay);
				break;
			case 25:
				strFieldValue = String.valueOf(Receiptfeeinsecu);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNum = FValue.trim();
			}
			else
				ReceiptNum = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("MainfeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainfeeNo = FValue.trim();
			}
			else
				MainfeeNo = null;
		}
		if (FCode.equalsIgnoreCase("ServiceDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ServiceDate = fDate.getDate( FValue );
			}
			else
				ServiceDate = null;
		}
		if (FCode.equalsIgnoreCase("ProductCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProductCode = FValue.trim();
			}
			else
				ProductCode = null;
		}
		if (FCode.equalsIgnoreCase("ProductName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProductName = FValue.trim();
			}
			else
				ProductName = null;
		}
		if (FCode.equalsIgnoreCase("ProductType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProductType = FValue.trim();
			}
			else
				ProductType = null;
		}
		if (FCode.equalsIgnoreCase("DrugCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugCode = FValue.trim();
			}
			else
				DrugCode = null;
		}
		if (FCode.equalsIgnoreCase("PayService"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayService = FValue.trim();
			}
			else
				PayService = null;
		}
		if (FCode.equalsIgnoreCase("DrugName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugName = FValue.trim();
			}
			else
				DrugName = null;
		}
		if (FCode.equalsIgnoreCase("UnitPrice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				UnitPrice = d;
			}
		}
		if (FCode.equalsIgnoreCase("Number"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Number = FValue.trim();
			}
			else
				Number = null;
		}
		if (FCode.equalsIgnoreCase("Specifications"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Specifications = FValue.trim();
			}
			else
				Specifications = null;
		}
		if (FCode.equalsIgnoreCase("Dosage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dosage = FValue.trim();
			}
			else
				Dosage = null;
		}
		if (FCode.equalsIgnoreCase("Sumprice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Sumprice = d;
			}
		}
		if (FCode.equalsIgnoreCase("ProfessionalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProfessionalCode = FValue.trim();
			}
			else
				ProfessionalCode = null;
		}
		if (FCode.equalsIgnoreCase("DoctorName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctorName = FValue.trim();
			}
			else
				DoctorName = null;
		}
		if (FCode.equalsIgnoreCase("DepartmentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DepartmentCode = FValue.trim();
			}
			else
				DepartmentCode = null;
		}
		if (FCode.equalsIgnoreCase("DepartmentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DepartmentName = FValue.trim();
			}
			else
				DepartmentName = null;
		}
		if (FCode.equalsIgnoreCase("Usage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Usage = FValue.trim();
			}
			else
				Usage = null;
		}
		if (FCode.equalsIgnoreCase("DrugType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugType = FValue.trim();
			}
			else
				DrugType = null;
		}
		if (FCode.equalsIgnoreCase("Consumption"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Consumption = FValue.trim();
			}
			else
				Consumption = null;
		}
		if (FCode.equalsIgnoreCase("Frequency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Frequency = FValue.trim();
			}
			else
				Frequency = null;
		}
		if (FCode.equalsIgnoreCase("DrugDay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DrugDay = d;
			}
		}
		if (FCode.equalsIgnoreCase("Receiptfeeinsecu"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Receiptfeeinsecu = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLReceiptSchema other = (LLReceiptSchema)otherObject;
		return
			(ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (ReceiptNum == null ? other.getReceiptNum() == null : ReceiptNum.equals(other.getReceiptNum()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (MainfeeNo == null ? other.getMainfeeNo() == null : MainfeeNo.equals(other.getMainfeeNo()))
			&& (ServiceDate == null ? other.getServiceDate() == null : fDate.getString(ServiceDate).equals(other.getServiceDate()))
			&& (ProductCode == null ? other.getProductCode() == null : ProductCode.equals(other.getProductCode()))
			&& (ProductName == null ? other.getProductName() == null : ProductName.equals(other.getProductName()))
			&& (ProductType == null ? other.getProductType() == null : ProductType.equals(other.getProductType()))
			&& (DrugCode == null ? other.getDrugCode() == null : DrugCode.equals(other.getDrugCode()))
			&& (PayService == null ? other.getPayService() == null : PayService.equals(other.getPayService()))
			&& (DrugName == null ? other.getDrugName() == null : DrugName.equals(other.getDrugName()))
			&& UnitPrice == other.getUnitPrice()
			&& (Number == null ? other.getNumber() == null : Number.equals(other.getNumber()))
			&& (Specifications == null ? other.getSpecifications() == null : Specifications.equals(other.getSpecifications()))
			&& (Dosage == null ? other.getDosage() == null : Dosage.equals(other.getDosage()))
			&& Sumprice == other.getSumprice()
			&& (ProfessionalCode == null ? other.getProfessionalCode() == null : ProfessionalCode.equals(other.getProfessionalCode()))
			&& (DoctorName == null ? other.getDoctorName() == null : DoctorName.equals(other.getDoctorName()))
			&& (DepartmentCode == null ? other.getDepartmentCode() == null : DepartmentCode.equals(other.getDepartmentCode()))
			&& (DepartmentName == null ? other.getDepartmentName() == null : DepartmentName.equals(other.getDepartmentName()))
			&& (Usage == null ? other.getUsage() == null : Usage.equals(other.getUsage()))
			&& (DrugType == null ? other.getDrugType() == null : DrugType.equals(other.getDrugType()))
			&& (Consumption == null ? other.getConsumption() == null : Consumption.equals(other.getConsumption()))
			&& (Frequency == null ? other.getFrequency() == null : Frequency.equals(other.getFrequency()))
			&& DrugDay == other.getDrugDay()
			&& Receiptfeeinsecu == other.getReceiptfeeinsecu()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ReceiptNo") ) {
			return 0;
		}
		if( strFieldName.equals("ReceiptNum") ) {
			return 1;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 2;
		}
		if( strFieldName.equals("MainfeeNo") ) {
			return 3;
		}
		if( strFieldName.equals("ServiceDate") ) {
			return 4;
		}
		if( strFieldName.equals("ProductCode") ) {
			return 5;
		}
		if( strFieldName.equals("ProductName") ) {
			return 6;
		}
		if( strFieldName.equals("ProductType") ) {
			return 7;
		}
		if( strFieldName.equals("DrugCode") ) {
			return 8;
		}
		if( strFieldName.equals("PayService") ) {
			return 9;
		}
		if( strFieldName.equals("DrugName") ) {
			return 10;
		}
		if( strFieldName.equals("UnitPrice") ) {
			return 11;
		}
		if( strFieldName.equals("Number") ) {
			return 12;
		}
		if( strFieldName.equals("Specifications") ) {
			return 13;
		}
		if( strFieldName.equals("Dosage") ) {
			return 14;
		}
		if( strFieldName.equals("Sumprice") ) {
			return 15;
		}
		if( strFieldName.equals("ProfessionalCode") ) {
			return 16;
		}
		if( strFieldName.equals("DoctorName") ) {
			return 17;
		}
		if( strFieldName.equals("DepartmentCode") ) {
			return 18;
		}
		if( strFieldName.equals("DepartmentName") ) {
			return 19;
		}
		if( strFieldName.equals("Usage") ) {
			return 20;
		}
		if( strFieldName.equals("DrugType") ) {
			return 21;
		}
		if( strFieldName.equals("Consumption") ) {
			return 22;
		}
		if( strFieldName.equals("Frequency") ) {
			return 23;
		}
		if( strFieldName.equals("DrugDay") ) {
			return 24;
		}
		if( strFieldName.equals("Receiptfeeinsecu") ) {
			return 25;
		}
		if( strFieldName.equals("Operator") ) {
			return 26;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 27;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 28;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 29;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 30;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ReceiptNo";
				break;
			case 1:
				strFieldName = "ReceiptNum";
				break;
			case 2:
				strFieldName = "CaseNo";
				break;
			case 3:
				strFieldName = "MainfeeNo";
				break;
			case 4:
				strFieldName = "ServiceDate";
				break;
			case 5:
				strFieldName = "ProductCode";
				break;
			case 6:
				strFieldName = "ProductName";
				break;
			case 7:
				strFieldName = "ProductType";
				break;
			case 8:
				strFieldName = "DrugCode";
				break;
			case 9:
				strFieldName = "PayService";
				break;
			case 10:
				strFieldName = "DrugName";
				break;
			case 11:
				strFieldName = "UnitPrice";
				break;
			case 12:
				strFieldName = "Number";
				break;
			case 13:
				strFieldName = "Specifications";
				break;
			case 14:
				strFieldName = "Dosage";
				break;
			case 15:
				strFieldName = "Sumprice";
				break;
			case 16:
				strFieldName = "ProfessionalCode";
				break;
			case 17:
				strFieldName = "DoctorName";
				break;
			case 18:
				strFieldName = "DepartmentCode";
				break;
			case 19:
				strFieldName = "DepartmentName";
				break;
			case 20:
				strFieldName = "Usage";
				break;
			case 21:
				strFieldName = "DrugType";
				break;
			case 22:
				strFieldName = "Consumption";
				break;
			case 23:
				strFieldName = "Frequency";
				break;
			case 24:
				strFieldName = "DrugDay";
				break;
			case 25:
				strFieldName = "Receiptfeeinsecu";
				break;
			case 26:
				strFieldName = "Operator";
				break;
			case 27:
				strFieldName = "MakeDate";
				break;
			case 28:
				strFieldName = "MakeTime";
				break;
			case 29:
				strFieldName = "ModifyDate";
				break;
			case 30:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNum") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainfeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServiceDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ProductCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProductName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProductType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayService") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnitPrice") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Number") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Specifications") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dosage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sumprice") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ProfessionalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctorName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DepartmentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DepartmentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Usage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Consumption") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Frequency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugDay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Receiptfeeinsecu") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
