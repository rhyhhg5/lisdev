/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDDataChkDescDB;

/*
 * <p>ClassName: LDDataChkDescSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-07-04
 */
public class LDDataChkDescSchema implements Schema, Cloneable {
    // @Field
    /** Sql编号 */
    private int SQLNo;
    /** 检查类型 */
    private String CheckType;
    /** Sql类型 */
    private String SQLType;
    /** Sql语句描述 */
    private String SQL;
    /** 逻辑信息描述 */
    private String InfoMation;
    /** 错误信息描述 */
    private String ErrInfo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDDataChkDescSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SQLNo";
        pk[1] = "CheckType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDDataChkDescSchema cloned = (LDDataChkDescSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public int getSQLNo() {
        return SQLNo;
    }

    public void setSQLNo(int aSQLNo) {
        SQLNo = aSQLNo;
    }

    public void setSQLNo(String aSQLNo) {
        if (aSQLNo != null && !aSQLNo.equals("")) {
            Integer tInteger = new Integer(aSQLNo);
            int i = tInteger.intValue();
            SQLNo = i;
        }
    }

    public String getCheckType() {
        return CheckType;
    }

    public void setCheckType(String aCheckType) {
        CheckType = aCheckType;
    }

    public String getSQLType() {
        return SQLType;
    }

    public void setSQLType(String aSQLType) {
        SQLType = aSQLType;
    }

    public String getSQL() {
        return SQL;
    }

    public void setSQL(String aSQL) {
        SQL = aSQL;
    }

    public String getInfoMation() {
        return InfoMation;
    }

    public void setInfoMation(String aInfoMation) {
        InfoMation = aInfoMation;
    }

    public String getErrInfo() {
        return ErrInfo;
    }

    public void setErrInfo(String aErrInfo) {
        ErrInfo = aErrInfo;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LDDataChkDescSchema 对象给 Schema 赋值
     * @param: aLDDataChkDescSchema LDDataChkDescSchema
     **/
    public void setSchema(LDDataChkDescSchema aLDDataChkDescSchema) {
        this.SQLNo = aLDDataChkDescSchema.getSQLNo();
        this.CheckType = aLDDataChkDescSchema.getCheckType();
        this.SQLType = aLDDataChkDescSchema.getSQLType();
        this.SQL = aLDDataChkDescSchema.getSQL();
        this.InfoMation = aLDDataChkDescSchema.getInfoMation();
        this.ErrInfo = aLDDataChkDescSchema.getErrInfo();
        this.Operator = aLDDataChkDescSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDDataChkDescSchema.getMakeDate());
        this.MakeTime = aLDDataChkDescSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            this.SQLNo = rs.getInt("SQLNo");
            if (rs.getString("CheckType") == null) {
                this.CheckType = null;
            } else {
                this.CheckType = rs.getString("CheckType").trim();
            }

            if (rs.getString("SQLType") == null) {
                this.SQLType = null;
            } else {
                this.SQLType = rs.getString("SQLType").trim();
            }

            if (rs.getString("SQL") == null) {
                this.SQL = null;
            } else {
                this.SQL = rs.getString("SQL").trim();
            }

            if (rs.getString("InfoMation") == null) {
                this.InfoMation = null;
            } else {
                this.InfoMation = rs.getString("InfoMation").trim();
            }

            if (rs.getString("ErrInfo") == null) {
                this.ErrInfo = null;
            } else {
                this.ErrInfo = rs.getString("ErrInfo").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDDataChkDesc表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDataChkDescSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDDataChkDescSchema getSchema() {
        LDDataChkDescSchema aLDDataChkDescSchema = new LDDataChkDescSchema();
        aLDDataChkDescSchema.setSchema(this);
        return aLDDataChkDescSchema;
    }

    public LDDataChkDescDB getDB() {
        LDDataChkDescDB aDBOper = new LDDataChkDescDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDataChkDesc描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(SQLNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CheckType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SQLType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SQL));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InfoMation));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ErrInfo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDataChkDesc>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SQLNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    1, SysConst.PACKAGESPILTER))).intValue();
            CheckType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            SQLType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            SQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                 SysConst.PACKAGESPILTER);
            InfoMation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            ErrInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDataChkDescSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SQLNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SQLNo));
        }
        if (FCode.equals("CheckType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckType));
        }
        if (FCode.equals("SQLType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SQLType));
        }
        if (FCode.equals("SQL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SQL));
        }
        if (FCode.equals("InfoMation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InfoMation));
        }
        if (FCode.equals("ErrInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrInfo));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = String.valueOf(SQLNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CheckType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(SQLType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(SQL);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(InfoMation);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ErrInfo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SQLNo")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SQLNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("CheckType")) {
            if (FValue != null && !FValue.equals("")) {
                CheckType = FValue.trim();
            } else {
                CheckType = null;
            }
        }
        if (FCode.equalsIgnoreCase("SQLType")) {
            if (FValue != null && !FValue.equals("")) {
                SQLType = FValue.trim();
            } else {
                SQLType = null;
            }
        }
        if (FCode.equalsIgnoreCase("SQL")) {
            if (FValue != null && !FValue.equals("")) {
                SQL = FValue.trim();
            } else {
                SQL = null;
            }
        }
        if (FCode.equalsIgnoreCase("InfoMation")) {
            if (FValue != null && !FValue.equals("")) {
                InfoMation = FValue.trim();
            } else {
                InfoMation = null;
            }
        }
        if (FCode.equalsIgnoreCase("ErrInfo")) {
            if (FValue != null && !FValue.equals("")) {
                ErrInfo = FValue.trim();
            } else {
                ErrInfo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDDataChkDescSchema other = (LDDataChkDescSchema) otherObject;
        return
                SQLNo == other.getSQLNo()
                && CheckType.equals(other.getCheckType())
                && SQLType.equals(other.getSQLType())
                && SQL.equals(other.getSQL())
                && InfoMation.equals(other.getInfoMation())
                && ErrInfo.equals(other.getErrInfo())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SQLNo")) {
            return 0;
        }
        if (strFieldName.equals("CheckType")) {
            return 1;
        }
        if (strFieldName.equals("SQLType")) {
            return 2;
        }
        if (strFieldName.equals("SQL")) {
            return 3;
        }
        if (strFieldName.equals("InfoMation")) {
            return 4;
        }
        if (strFieldName.equals("ErrInfo")) {
            return 5;
        }
        if (strFieldName.equals("Operator")) {
            return 6;
        }
        if (strFieldName.equals("MakeDate")) {
            return 7;
        }
        if (strFieldName.equals("MakeTime")) {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SQLNo";
            break;
        case 1:
            strFieldName = "CheckType";
            break;
        case 2:
            strFieldName = "SQLType";
            break;
        case 3:
            strFieldName = "SQL";
            break;
        case 4:
            strFieldName = "InfoMation";
            break;
        case 5:
            strFieldName = "ErrInfo";
            break;
        case 6:
            strFieldName = "Operator";
            break;
        case 7:
            strFieldName = "MakeDate";
            break;
        case 8:
            strFieldName = "MakeTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SQLNo")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CheckType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SQLType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SQL")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InfoMation")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrInfo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_INT;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
