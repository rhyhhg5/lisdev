/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAInspiritDB;

/*
 * <p>ClassName: LAInspiritSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-17
 */
public class LAInspiritSchema implements Schema, Cloneable {
    // @Field
    /** 序号 */
    private String SeqNo;
    /** 活动起始日 */
    private Date StartDate;
    /** 活动截止日 */
    private Date EndDate;
    /** 统计时间 */
    private Date StatDate;
    /** 组织单位 */
    private String Organizer;
    /** 活动名称 */
    private String PloyName;
    /** 获奖人代码 */
    private String AgentCode;
    /** 获奖人姓名 */
    private String AgentName;
    /** 获奖称号 */
    private String InspiritName;
    /** 奖励内容 */
    private String InspiritContent;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAInspiritSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SeqNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAInspiritSchema cloned = (LAInspiritSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(String aSeqNo) {
        SeqNo = aSeqNo;
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public String getStatDate() {
        if (StatDate != null) {
            return fDate.getString(StatDate);
        } else {
            return null;
        }
    }

    public void setStatDate(Date aStatDate) {
        StatDate = aStatDate;
    }

    public void setStatDate(String aStatDate) {
        if (aStatDate != null && !aStatDate.equals("")) {
            StatDate = fDate.getDate(aStatDate);
        } else {
            StatDate = null;
        }
    }

    public String getOrganizer() {
        return Organizer;
    }

    public void setOrganizer(String aOrganizer) {
        Organizer = aOrganizer;
    }

    public String getPloyName() {
        return PloyName;
    }

    public void setPloyName(String aPloyName) {
        PloyName = aPloyName;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getAgentName() {
        return AgentName;
    }

    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }

    public String getInspiritName() {
        return InspiritName;
    }

    public void setInspiritName(String aInspiritName) {
        InspiritName = aInspiritName;
    }

    public String getInspiritContent() {
        return InspiritContent;
    }

    public void setInspiritContent(String aInspiritContent) {
        InspiritContent = aInspiritContent;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAInspiritSchema 对象给 Schema 赋值
     * @param: aLAInspiritSchema LAInspiritSchema
     **/
    public void setSchema(LAInspiritSchema aLAInspiritSchema) {
        this.SeqNo = aLAInspiritSchema.getSeqNo();
        this.StartDate = fDate.getDate(aLAInspiritSchema.getStartDate());
        this.EndDate = fDate.getDate(aLAInspiritSchema.getEndDate());
        this.StatDate = fDate.getDate(aLAInspiritSchema.getStatDate());
        this.Organizer = aLAInspiritSchema.getOrganizer();
        this.PloyName = aLAInspiritSchema.getPloyName();
        this.AgentCode = aLAInspiritSchema.getAgentCode();
        this.AgentName = aLAInspiritSchema.getAgentName();
        this.InspiritName = aLAInspiritSchema.getInspiritName();
        this.InspiritContent = aLAInspiritSchema.getInspiritContent();
        this.Operator = aLAInspiritSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAInspiritSchema.getMakeDate());
        this.MakeTime = aLAInspiritSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAInspiritSchema.getModifyDate());
        this.ModifyTime = aLAInspiritSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SeqNo") == null) {
                this.SeqNo = null;
            } else {
                this.SeqNo = rs.getString("SeqNo").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            this.StatDate = rs.getDate("StatDate");
            if (rs.getString("Organizer") == null) {
                this.Organizer = null;
            } else {
                this.Organizer = rs.getString("Organizer").trim();
            }

            if (rs.getString("PloyName") == null) {
                this.PloyName = null;
            } else {
                this.PloyName = rs.getString("PloyName").trim();
            }

            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentName") == null) {
                this.AgentName = null;
            } else {
                this.AgentName = rs.getString("AgentName").trim();
            }

            if (rs.getString("InspiritName") == null) {
                this.InspiritName = null;
            } else {
                this.InspiritName = rs.getString("InspiritName").trim();
            }

            if (rs.getString("InspiritContent") == null) {
                this.InspiritContent = null;
            } else {
                this.InspiritContent = rs.getString("InspiritContent").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LAInspirit表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInspiritSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAInspiritSchema getSchema() {
        LAInspiritSchema aLAInspiritSchema = new LAInspiritSchema();
        aLAInspiritSchema.setSchema(this);
        return aLAInspiritSchema;
    }

    public LAInspiritDB getDB() {
        LAInspiritDB aDBOper = new LAInspiritDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInspirit描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SeqNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StatDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Organizer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PloyName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InspiritName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InspiritContent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInspirit>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SeqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            StatDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            Organizer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            PloyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            InspiritName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            InspiritContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             10, SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInspiritSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeqNo));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("StatDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStatDate()));
        }
        if (FCode.equals("Organizer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Organizer));
        }
        if (FCode.equals("PloyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PloyName));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equals("InspiritName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InspiritName));
        }
        if (FCode.equals("InspiritContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InspiritContent));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SeqNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStatDate()));
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Organizer);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(PloyName);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(AgentName);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(InspiritName);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(InspiritContent);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SeqNo")) {
            if (FValue != null && !FValue.equals("")) {
                SeqNo = FValue.trim();
            } else {
                SeqNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("StatDate")) {
            if (FValue != null && !FValue.equals("")) {
                StatDate = fDate.getDate(FValue);
            } else {
                StatDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Organizer")) {
            if (FValue != null && !FValue.equals("")) {
                Organizer = FValue.trim();
            } else {
                Organizer = null;
            }
        }
        if (FCode.equalsIgnoreCase("PloyName")) {
            if (FValue != null && !FValue.equals("")) {
                PloyName = FValue.trim();
            } else {
                PloyName = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if (FValue != null && !FValue.equals("")) {
                AgentName = FValue.trim();
            } else {
                AgentName = null;
            }
        }
        if (FCode.equalsIgnoreCase("InspiritName")) {
            if (FValue != null && !FValue.equals("")) {
                InspiritName = FValue.trim();
            } else {
                InspiritName = null;
            }
        }
        if (FCode.equalsIgnoreCase("InspiritContent")) {
            if (FValue != null && !FValue.equals("")) {
                InspiritContent = FValue.trim();
            } else {
                InspiritContent = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LAInspiritSchema other = (LAInspiritSchema) otherObject;
        return
                SeqNo.equals(other.getSeqNo())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && fDate.getString(StatDate).equals(other.getStatDate())
                && Organizer.equals(other.getOrganizer())
                && PloyName.equals(other.getPloyName())
                && AgentCode.equals(other.getAgentCode())
                && AgentName.equals(other.getAgentName())
                && InspiritName.equals(other.getInspiritName())
                && InspiritContent.equals(other.getInspiritContent())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SeqNo")) {
            return 0;
        }
        if (strFieldName.equals("StartDate")) {
            return 1;
        }
        if (strFieldName.equals("EndDate")) {
            return 2;
        }
        if (strFieldName.equals("StatDate")) {
            return 3;
        }
        if (strFieldName.equals("Organizer")) {
            return 4;
        }
        if (strFieldName.equals("PloyName")) {
            return 5;
        }
        if (strFieldName.equals("AgentCode")) {
            return 6;
        }
        if (strFieldName.equals("AgentName")) {
            return 7;
        }
        if (strFieldName.equals("InspiritName")) {
            return 8;
        }
        if (strFieldName.equals("InspiritContent")) {
            return 9;
        }
        if (strFieldName.equals("Operator")) {
            return 10;
        }
        if (strFieldName.equals("MakeDate")) {
            return 11;
        }
        if (strFieldName.equals("MakeTime")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SeqNo";
            break;
        case 1:
            strFieldName = "StartDate";
            break;
        case 2:
            strFieldName = "EndDate";
            break;
        case 3:
            strFieldName = "StatDate";
            break;
        case 4:
            strFieldName = "Organizer";
            break;
        case 5:
            strFieldName = "PloyName";
            break;
        case 6:
            strFieldName = "AgentCode";
            break;
        case 7:
            strFieldName = "AgentName";
            break;
        case 8:
            strFieldName = "InspiritName";
            break;
        case 9:
            strFieldName = "InspiritContent";
            break;
        case 10:
            strFieldName = "Operator";
            break;
        case 11:
            strFieldName = "MakeDate";
            break;
        case 12:
            strFieldName = "MakeTime";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SeqNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("StatDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Organizer")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PloyName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InspiritName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InspiritContent")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 2:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 3:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
