/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDPlanDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保险计划套餐
 * @CreateDate：2005-03-07
 */
public class LDPlanSchema implements Schema
{
    // @Field
    /** 保险计划编码 */
    private String ContPlanCode;
    /** 保险计划名称 */
    private String ContPlanName;
    /** 计划类别 */
    private String PlanType;
    /** 计划规则 */
    private String PlanRule;
    /** 计划规则sql */
    private String PlanSql;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 可投保人数 */
    private int Peoples3;
    /** 备注2 */
    private String Remark2;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDPlanSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ContPlanCode";
        pk[1] = "PlanType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getContPlanCode()
    {
        if (ContPlanCode != null && !ContPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ContPlanCode = StrTool.unicodeToGBK(ContPlanCode);
        }
        return ContPlanCode;
    }

    public void setContPlanCode(String aContPlanCode)
    {
        ContPlanCode = aContPlanCode;
    }

    public String getContPlanName()
    {
        if (ContPlanName != null && !ContPlanName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ContPlanName = StrTool.unicodeToGBK(ContPlanName);
        }
        return ContPlanName;
    }

    public void setContPlanName(String aContPlanName)
    {
        ContPlanName = aContPlanName;
    }

    public String getPlanType()
    {
        if (PlanType != null && !PlanType.equals("") && SysConst.CHANGECHARSET == true)
        {
            PlanType = StrTool.unicodeToGBK(PlanType);
        }
        return PlanType;
    }

    public void setPlanType(String aPlanType)
    {
        PlanType = aPlanType;
    }

    public String getPlanRule()
    {
        if (PlanRule != null && !PlanRule.equals("") && SysConst.CHANGECHARSET == true)
        {
            PlanRule = StrTool.unicodeToGBK(PlanRule);
        }
        return PlanRule;
    }

    public void setPlanRule(String aPlanRule)
    {
        PlanRule = aPlanRule;
    }

    public String getPlanSql()
    {
        if (PlanSql != null && !PlanSql.equals("") && SysConst.CHANGECHARSET == true)
        {
            PlanSql = StrTool.unicodeToGBK(PlanSql);
        }
        return PlanSql;
    }

    public void setPlanSql(String aPlanSql)
    {
        PlanSql = aPlanSql;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public int getPeoples3()
    {
        return Peoples3;
    }

    public void setPeoples3(int aPeoples3)
    {
        Peoples3 = aPeoples3;
    }

    public void setPeoples3(String aPeoples3)
    {
        if (aPeoples3 != null && !aPeoples3.equals(""))
        {
            Integer tInteger = new Integer(aPeoples3);
            int i = tInteger.intValue();
            Peoples3 = i;
        }
    }

    public String getRemark2()
    {
        if (Remark2 != null && !Remark2.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark2 = StrTool.unicodeToGBK(Remark2);
        }
        return Remark2;
    }

    public void setRemark2(String aRemark2)
    {
        Remark2 = aRemark2;
    }

    /**
     * 使用另外一个 LDPlanSchema 对象给 Schema 赋值
     * @param: aLDPlanSchema LDPlanSchema
     **/
    public void setSchema(LDPlanSchema aLDPlanSchema)
    {
        this.ContPlanCode = aLDPlanSchema.getContPlanCode();
        this.ContPlanName = aLDPlanSchema.getContPlanName();
        this.PlanType = aLDPlanSchema.getPlanType();
        this.PlanRule = aLDPlanSchema.getPlanRule();
        this.PlanSql = aLDPlanSchema.getPlanSql();
        this.Remark = aLDPlanSchema.getRemark();
        this.Operator = aLDPlanSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDPlanSchema.getMakeDate());
        this.MakeTime = aLDPlanSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDPlanSchema.getModifyDate());
        this.ModifyTime = aLDPlanSchema.getModifyTime();
        this.Peoples3 = aLDPlanSchema.getPeoples3();
        this.Remark2 = aLDPlanSchema.getRemark2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ContPlanCode") == null)
            {
                this.ContPlanCode = null;
            }
            else
            {
                this.ContPlanCode = rs.getString("ContPlanCode").trim();
            }

            if (rs.getString("ContPlanName") == null)
            {
                this.ContPlanName = null;
            }
            else
            {
                this.ContPlanName = rs.getString("ContPlanName").trim();
            }

            if (rs.getString("PlanType") == null)
            {
                this.PlanType = null;
            }
            else
            {
                this.PlanType = rs.getString("PlanType").trim();
            }

            if (rs.getString("PlanRule") == null)
            {
                this.PlanRule = null;
            }
            else
            {
                this.PlanRule = rs.getString("PlanRule").trim();
            }

            if (rs.getString("PlanSql") == null)
            {
                this.PlanSql = null;
            }
            else
            {
                this.PlanSql = rs.getString("PlanSql").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.Peoples3 = rs.getInt("Peoples3");
            if (rs.getString("Remark2") == null)
            {
                this.Remark2 = null;
            }
            else
            {
                this.Remark2 = rs.getString("Remark2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPlanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDPlanSchema getSchema()
    {
        LDPlanSchema aLDPlanSchema = new LDPlanSchema();
        aLDPlanSchema.setSchema(this);
        return aLDPlanSchema;
    }

    public LDPlanDB getDB()
    {
        LDPlanDB aDBOper = new LDPlanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDPlan描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ContPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContPlanName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PlanType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PlanRule)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PlanSql)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Peoples3) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark2));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDPlan>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ContPlanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            PlanType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            PlanRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            PlanSql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            Peoples3 = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).intValue();
            Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPlanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ContPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContPlanCode));
        }
        if (FCode.equals("ContPlanName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContPlanName));
        }
        if (FCode.equals("PlanType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PlanType));
        }
        if (FCode.equals("PlanRule"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PlanRule));
        }
        if (FCode.equals("PlanSql"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PlanSql));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("Peoples3"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Peoples3));
        }
        if (FCode.equals("Remark2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ContPlanName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PlanType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PlanRule);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PlanSql);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 11:
                strFieldValue = String.valueOf(Peoples3);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Remark2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ContPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
            {
                ContPlanCode = null;
            }
        }
        if (FCode.equals("ContPlanName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContPlanName = FValue.trim();
            }
            else
            {
                ContPlanName = null;
            }
        }
        if (FCode.equals("PlanType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanType = FValue.trim();
            }
            else
            {
                PlanType = null;
            }
        }
        if (FCode.equals("PlanRule"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanRule = FValue.trim();
            }
            else
            {
                PlanRule = null;
            }
        }
        if (FCode.equals("PlanSql"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PlanSql = FValue.trim();
            }
            else
            {
                PlanSql = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Peoples3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Peoples3 = i;
            }
        }
        if (FCode.equals("Remark2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
            {
                Remark2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDPlanSchema other = (LDPlanSchema) otherObject;
        return
                ContPlanCode.equals(other.getContPlanCode())
                && ContPlanName.equals(other.getContPlanName())
                && PlanType.equals(other.getPlanType())
                && PlanRule.equals(other.getPlanRule())
                && PlanSql.equals(other.getPlanSql())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Peoples3 == other.getPeoples3()
                && Remark2.equals(other.getRemark2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ContPlanCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ContPlanName"))
        {
            return 1;
        }
        if (strFieldName.equals("PlanType"))
        {
            return 2;
        }
        if (strFieldName.equals("PlanRule"))
        {
            return 3;
        }
        if (strFieldName.equals("PlanSql"))
        {
            return 4;
        }
        if (strFieldName.equals("Remark"))
        {
            return 5;
        }
        if (strFieldName.equals("Operator"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 10;
        }
        if (strFieldName.equals("Peoples3"))
        {
            return 11;
        }
        if (strFieldName.equals("Remark2"))
        {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ContPlanCode";
                break;
            case 1:
                strFieldName = "ContPlanName";
                break;
            case 2:
                strFieldName = "PlanType";
                break;
            case 3:
                strFieldName = "PlanRule";
                break;
            case 4:
                strFieldName = "PlanSql";
                break;
            case 5:
                strFieldName = "Remark";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            case 11:
                strFieldName = "Peoples3";
                break;
            case 12:
                strFieldName = "Remark2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ContPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanRule"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanSql"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Peoples3"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Remark2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_INT;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
