/*
 * <p>ClassName: LFRiskSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LFRiskDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFRiskSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 险种名称 */
    private String RiskName;
    /** 险种分类 */
    private String RiskType;
    /** 险种类别 */
    private String RiskPeriod;
    /** 主附险标记 */
    private String SubRiskFlag;
    /** 寿险分类 */
    private String LifeType;
    /** 险种事故责任分类（寿险） */
    private String RiskDutyType;
    /** 健康险和意外险期限分类 */
    private String RiskYearType;
    /** 健康险细分 */
    private String HealthType;
    /** 意外险细分 */
    private String AccidentType;
    /** 养老险标记 */
    private String EndowmentFlag;
    /** 养老险细分1 */
    private String EndowmentType1;
    /** 养老险细分2 */
    private String EndowmentType2;
    /** 险种的销售渠道 */
    private String RiskSaleChnl;
    /** 险种的团个单标志 */
    private String RiskGrpFlag;
    /** 手续费销售渠道 */
    private String ChargeSaleChnl;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFRiskSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getRiskName()
    {
        if (RiskName != null && !RiskName.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskName = StrTool.unicodeToGBK(RiskName);
        }
        return RiskName;
    }

    public void setRiskName(String aRiskName)
    {
        RiskName = aRiskName;
    }

    public String getRiskType()
    {
        if (RiskType != null && !RiskType.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskType = StrTool.unicodeToGBK(RiskType);
        }
        return RiskType;
    }

    public void setRiskType(String aRiskType)
    {
        RiskType = aRiskType;
    }

    public String getRiskPeriod()
    {
        if (RiskPeriod != null && !RiskPeriod.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskPeriod = StrTool.unicodeToGBK(RiskPeriod);
        }
        return RiskPeriod;
    }

    public void setRiskPeriod(String aRiskPeriod)
    {
        RiskPeriod = aRiskPeriod;
    }

    public String getSubRiskFlag()
    {
        if (SubRiskFlag != null && !SubRiskFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SubRiskFlag = StrTool.unicodeToGBK(SubRiskFlag);
        }
        return SubRiskFlag;
    }

    public void setSubRiskFlag(String aSubRiskFlag)
    {
        SubRiskFlag = aSubRiskFlag;
    }

    public String getLifeType()
    {
        if (LifeType != null && !LifeType.equals("") && SysConst.CHANGECHARSET == true)
        {
            LifeType = StrTool.unicodeToGBK(LifeType);
        }
        return LifeType;
    }

    public void setLifeType(String aLifeType)
    {
        LifeType = aLifeType;
    }

    public String getRiskDutyType()
    {
        if (RiskDutyType != null && !RiskDutyType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskDutyType = StrTool.unicodeToGBK(RiskDutyType);
        }
        return RiskDutyType;
    }

    public void setRiskDutyType(String aRiskDutyType)
    {
        RiskDutyType = aRiskDutyType;
    }

    public String getRiskYearType()
    {
        if (RiskYearType != null && !RiskYearType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskYearType = StrTool.unicodeToGBK(RiskYearType);
        }
        return RiskYearType;
    }

    public void setRiskYearType(String aRiskYearType)
    {
        RiskYearType = aRiskYearType;
    }

    public String getHealthType()
    {
        if (HealthType != null && !HealthType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HealthType = StrTool.unicodeToGBK(HealthType);
        }
        return HealthType;
    }

    public void setHealthType(String aHealthType)
    {
        HealthType = aHealthType;
    }

    public String getAccidentType()
    {
        if (AccidentType != null && !AccidentType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AccidentType = StrTool.unicodeToGBK(AccidentType);
        }
        return AccidentType;
    }

    public void setAccidentType(String aAccidentType)
    {
        AccidentType = aAccidentType;
    }

    public String getEndowmentFlag()
    {
        if (EndowmentFlag != null && !EndowmentFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EndowmentFlag = StrTool.unicodeToGBK(EndowmentFlag);
        }
        return EndowmentFlag;
    }

    public void setEndowmentFlag(String aEndowmentFlag)
    {
        EndowmentFlag = aEndowmentFlag;
    }

    public String getEndowmentType1()
    {
        if (EndowmentType1 != null && !EndowmentType1.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EndowmentType1 = StrTool.unicodeToGBK(EndowmentType1);
        }
        return EndowmentType1;
    }

    public void setEndowmentType1(String aEndowmentType1)
    {
        EndowmentType1 = aEndowmentType1;
    }

    public String getEndowmentType2()
    {
        if (EndowmentType2 != null && !EndowmentType2.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EndowmentType2 = StrTool.unicodeToGBK(EndowmentType2);
        }
        return EndowmentType2;
    }

    public void setEndowmentType2(String aEndowmentType2)
    {
        EndowmentType2 = aEndowmentType2;
    }

    public String getRiskSaleChnl()
    {
        if (RiskSaleChnl != null && !RiskSaleChnl.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskSaleChnl = StrTool.unicodeToGBK(RiskSaleChnl);
        }
        return RiskSaleChnl;
    }

    public void setRiskSaleChnl(String aRiskSaleChnl)
    {
        RiskSaleChnl = aRiskSaleChnl;
    }

    public String getRiskGrpFlag()
    {
        if (RiskGrpFlag != null && !RiskGrpFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskGrpFlag = StrTool.unicodeToGBK(RiskGrpFlag);
        }
        return RiskGrpFlag;
    }

    public void setRiskGrpFlag(String aRiskGrpFlag)
    {
        RiskGrpFlag = aRiskGrpFlag;
    }

    public String getChargeSaleChnl()
    {
        if (ChargeSaleChnl != null && !ChargeSaleChnl.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ChargeSaleChnl = StrTool.unicodeToGBK(ChargeSaleChnl);
        }
        return ChargeSaleChnl;
    }

    public void setChargeSaleChnl(String aChargeSaleChnl)
    {
        ChargeSaleChnl = aChargeSaleChnl;
    }

    /**
     * 使用另外一个 LFRiskSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFRiskSchema aLFRiskSchema)
    {
        this.RiskCode = aLFRiskSchema.getRiskCode();
        this.RiskVer = aLFRiskSchema.getRiskVer();
        this.RiskName = aLFRiskSchema.getRiskName();
        this.RiskType = aLFRiskSchema.getRiskType();
        this.RiskPeriod = aLFRiskSchema.getRiskPeriod();
        this.SubRiskFlag = aLFRiskSchema.getSubRiskFlag();
        this.LifeType = aLFRiskSchema.getLifeType();
        this.RiskDutyType = aLFRiskSchema.getRiskDutyType();
        this.RiskYearType = aLFRiskSchema.getRiskYearType();
        this.HealthType = aLFRiskSchema.getHealthType();
        this.AccidentType = aLFRiskSchema.getAccidentType();
        this.EndowmentFlag = aLFRiskSchema.getEndowmentFlag();
        this.EndowmentType1 = aLFRiskSchema.getEndowmentType1();
        this.EndowmentType2 = aLFRiskSchema.getEndowmentType2();
        this.RiskSaleChnl = aLFRiskSchema.getRiskSaleChnl();
        this.RiskGrpFlag = aLFRiskSchema.getRiskGrpFlag();
        this.ChargeSaleChnl = aLFRiskSchema.getChargeSaleChnl();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("RiskName") == null)
            {
                this.RiskName = null;
            }
            else
            {
                this.RiskName = rs.getString("RiskName").trim();
            }

            if (rs.getString("RiskType") == null)
            {
                this.RiskType = null;
            }
            else
            {
                this.RiskType = rs.getString("RiskType").trim();
            }

            if (rs.getString("RiskPeriod") == null)
            {
                this.RiskPeriod = null;
            }
            else
            {
                this.RiskPeriod = rs.getString("RiskPeriod").trim();
            }

            if (rs.getString("SubRiskFlag") == null)
            {
                this.SubRiskFlag = null;
            }
            else
            {
                this.SubRiskFlag = rs.getString("SubRiskFlag").trim();
            }

            if (rs.getString("LifeType") == null)
            {
                this.LifeType = null;
            }
            else
            {
                this.LifeType = rs.getString("LifeType").trim();
            }

            if (rs.getString("RiskDutyType") == null)
            {
                this.RiskDutyType = null;
            }
            else
            {
                this.RiskDutyType = rs.getString("RiskDutyType").trim();
            }

            if (rs.getString("RiskYearType") == null)
            {
                this.RiskYearType = null;
            }
            else
            {
                this.RiskYearType = rs.getString("RiskYearType").trim();
            }

            if (rs.getString("HealthType") == null)
            {
                this.HealthType = null;
            }
            else
            {
                this.HealthType = rs.getString("HealthType").trim();
            }

            if (rs.getString("AccidentType") == null)
            {
                this.AccidentType = null;
            }
            else
            {
                this.AccidentType = rs.getString("AccidentType").trim();
            }

            if (rs.getString("EndowmentFlag") == null)
            {
                this.EndowmentFlag = null;
            }
            else
            {
                this.EndowmentFlag = rs.getString("EndowmentFlag").trim();
            }

            if (rs.getString("EndowmentType1") == null)
            {
                this.EndowmentType1 = null;
            }
            else
            {
                this.EndowmentType1 = rs.getString("EndowmentType1").trim();
            }

            if (rs.getString("EndowmentType2") == null)
            {
                this.EndowmentType2 = null;
            }
            else
            {
                this.EndowmentType2 = rs.getString("EndowmentType2").trim();
            }

            if (rs.getString("RiskSaleChnl") == null)
            {
                this.RiskSaleChnl = null;
            }
            else
            {
                this.RiskSaleChnl = rs.getString("RiskSaleChnl").trim();
            }

            if (rs.getString("RiskGrpFlag") == null)
            {
                this.RiskGrpFlag = null;
            }
            else
            {
                this.RiskGrpFlag = rs.getString("RiskGrpFlag").trim();
            }

            if (rs.getString("ChargeSaleChnl") == null)
            {
                this.ChargeSaleChnl = null;
            }
            else
            {
                this.ChargeSaleChnl = rs.getString("ChargeSaleChnl").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFRiskSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFRiskSchema getSchema()
    {
        LFRiskSchema aLFRiskSchema = new LFRiskSchema();
        aLFRiskSchema.setSchema(this);
        return aLFRiskSchema;
    }

    public LFRiskDB getDB()
    {
        LFRiskDB aDBOper = new LFRiskDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFRisk描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskPeriod)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SubRiskFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(LifeType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskDutyType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskYearType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HealthType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccidentType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndowmentFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndowmentType1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndowmentType2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskSaleChnl)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskGrpFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChargeSaleChnl));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFRisk>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            RiskPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            SubRiskFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            LifeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            RiskDutyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            RiskYearType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            HealthType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            AccidentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            EndowmentFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                           SysConst.PACKAGESPILTER);
            EndowmentType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            13, SysConst.PACKAGESPILTER);
            EndowmentType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            14, SysConst.PACKAGESPILTER);
            RiskSaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                          SysConst.PACKAGESPILTER);
            RiskGrpFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            ChargeSaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            17, SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFRiskSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("RiskName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskName));
        }
        if (FCode.equals("RiskType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskType));
        }
        if (FCode.equals("RiskPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskPeriod));
        }
        if (FCode.equals("SubRiskFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SubRiskFlag));
        }
        if (FCode.equals("LifeType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LifeType));
        }
        if (FCode.equals("RiskDutyType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskDutyType));
        }
        if (FCode.equals("RiskYearType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskYearType));
        }
        if (FCode.equals("HealthType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HealthType));
        }
        if (FCode.equals("AccidentType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccidentType));
        }
        if (FCode.equals("EndowmentFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndowmentFlag));
        }
        if (FCode.equals("EndowmentType1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndowmentType1));
        }
        if (FCode.equals("EndowmentType2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndowmentType2));
        }
        if (FCode.equals("RiskSaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskSaleChnl));
        }
        if (FCode.equals("RiskGrpFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskGrpFlag));
        }
        if (FCode.equals("ChargeSaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChargeSaleChnl));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RiskPeriod);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(SubRiskFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(LifeType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RiskDutyType);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RiskYearType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(HealthType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AccidentType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(EndowmentFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(EndowmentType1);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(EndowmentType2);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(RiskSaleChnl);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(RiskGrpFlag);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ChargeSaleChnl);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("RiskName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
            {
                RiskName = null;
            }
        }
        if (FCode.equals("RiskType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskType = FValue.trim();
            }
            else
            {
                RiskType = null;
            }
        }
        if (FCode.equals("RiskPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskPeriod = FValue.trim();
            }
            else
            {
                RiskPeriod = null;
            }
        }
        if (FCode.equals("SubRiskFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubRiskFlag = FValue.trim();
            }
            else
            {
                SubRiskFlag = null;
            }
        }
        if (FCode.equals("LifeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LifeType = FValue.trim();
            }
            else
            {
                LifeType = null;
            }
        }
        if (FCode.equals("RiskDutyType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskDutyType = FValue.trim();
            }
            else
            {
                RiskDutyType = null;
            }
        }
        if (FCode.equals("RiskYearType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskYearType = FValue.trim();
            }
            else
            {
                RiskYearType = null;
            }
        }
        if (FCode.equals("HealthType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HealthType = FValue.trim();
            }
            else
            {
                HealthType = null;
            }
        }
        if (FCode.equals("AccidentType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccidentType = FValue.trim();
            }
            else
            {
                AccidentType = null;
            }
        }
        if (FCode.equals("EndowmentFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndowmentFlag = FValue.trim();
            }
            else
            {
                EndowmentFlag = null;
            }
        }
        if (FCode.equals("EndowmentType1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndowmentType1 = FValue.trim();
            }
            else
            {
                EndowmentType1 = null;
            }
        }
        if (FCode.equals("EndowmentType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndowmentType2 = FValue.trim();
            }
            else
            {
                EndowmentType2 = null;
            }
        }
        if (FCode.equals("RiskSaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskSaleChnl = FValue.trim();
            }
            else
            {
                RiskSaleChnl = null;
            }
        }
        if (FCode.equals("RiskGrpFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskGrpFlag = FValue.trim();
            }
            else
            {
                RiskGrpFlag = null;
            }
        }
        if (FCode.equals("ChargeSaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChargeSaleChnl = FValue.trim();
            }
            else
            {
                ChargeSaleChnl = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFRiskSchema other = (LFRiskSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && RiskName.equals(other.getRiskName())
                && RiskType.equals(other.getRiskType())
                && RiskPeriod.equals(other.getRiskPeriod())
                && SubRiskFlag.equals(other.getSubRiskFlag())
                && LifeType.equals(other.getLifeType())
                && RiskDutyType.equals(other.getRiskDutyType())
                && RiskYearType.equals(other.getRiskYearType())
                && HealthType.equals(other.getHealthType())
                && AccidentType.equals(other.getAccidentType())
                && EndowmentFlag.equals(other.getEndowmentFlag())
                && EndowmentType1.equals(other.getEndowmentType1())
                && EndowmentType2.equals(other.getEndowmentType2())
                && RiskSaleChnl.equals(other.getRiskSaleChnl())
                && RiskGrpFlag.equals(other.getRiskGrpFlag())
                && ChargeSaleChnl.equals(other.getChargeSaleChnl());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskName"))
        {
            return 2;
        }
        if (strFieldName.equals("RiskType"))
        {
            return 3;
        }
        if (strFieldName.equals("RiskPeriod"))
        {
            return 4;
        }
        if (strFieldName.equals("SubRiskFlag"))
        {
            return 5;
        }
        if (strFieldName.equals("LifeType"))
        {
            return 6;
        }
        if (strFieldName.equals("RiskDutyType"))
        {
            return 7;
        }
        if (strFieldName.equals("RiskYearType"))
        {
            return 8;
        }
        if (strFieldName.equals("HealthType"))
        {
            return 9;
        }
        if (strFieldName.equals("AccidentType"))
        {
            return 10;
        }
        if (strFieldName.equals("EndowmentFlag"))
        {
            return 11;
        }
        if (strFieldName.equals("EndowmentType1"))
        {
            return 12;
        }
        if (strFieldName.equals("EndowmentType2"))
        {
            return 13;
        }
        if (strFieldName.equals("RiskSaleChnl"))
        {
            return 14;
        }
        if (strFieldName.equals("RiskGrpFlag"))
        {
            return 15;
        }
        if (strFieldName.equals("ChargeSaleChnl"))
        {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "RiskType";
                break;
            case 4:
                strFieldName = "RiskPeriod";
                break;
            case 5:
                strFieldName = "SubRiskFlag";
                break;
            case 6:
                strFieldName = "LifeType";
                break;
            case 7:
                strFieldName = "RiskDutyType";
                break;
            case 8:
                strFieldName = "RiskYearType";
                break;
            case 9:
                strFieldName = "HealthType";
                break;
            case 10:
                strFieldName = "AccidentType";
                break;
            case 11:
                strFieldName = "EndowmentFlag";
                break;
            case 12:
                strFieldName = "EndowmentType1";
                break;
            case 13:
                strFieldName = "EndowmentType2";
                break;
            case 14:
                strFieldName = "RiskSaleChnl";
                break;
            case 15:
                strFieldName = "RiskGrpFlag";
                break;
            case 16:
                strFieldName = "ChargeSaleChnl";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskPeriod"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubRiskFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LifeType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskDutyType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskYearType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HealthType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccidentType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndowmentFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndowmentType1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndowmentType2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskSaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskGrpFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChargeSaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
