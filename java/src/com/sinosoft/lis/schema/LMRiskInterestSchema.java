/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMRiskInterestDB;

/*
 * <p>ClassName: LMRiskInterestSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-02-18
 */
public class LMRiskInterestSchema implements Schema, Cloneable {
    // @Field
    /** 利率编码 */
    private String InterestCode;
    /** 利率名称 */
    private String InterestName;
    /** 利率说明 */
    private String InterestNotice;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 险种编码 */
    private String RiskCode;
    /** 结息位置 */
    private String InterestTakePlace;
    /** 利率累计方式 */
    private String InterestType;
    /** 利率计算类型 */
    private String InterestCalType;
    /** 固定利率类型 */
    private String DefaultCalType;
    /** 默认固定利率 */
    private double DefaultRate;
    /** 浮动利率类型 */
    private String FloatCalType;
    /** 浮动利率编码 */
    private String FloatCalCode;
    /** 计算利率周期 */
    private String InterestPeriod;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 18; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMRiskInterestSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "InterestCode";
        pk[1] = "InsuAccNo";
        pk[2] = "RiskCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMRiskInterestSchema cloned = (LMRiskInterestSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getInterestCode() {
        return InterestCode;
    }

    public void setInterestCode(String aInterestCode) {
        InterestCode = aInterestCode;
    }

    public String getInterestName() {
        return InterestName;
    }

    public void setInterestName(String aInterestName) {
        InterestName = aInterestName;
    }

    public String getInterestNotice() {
        return InterestNotice;
    }

    public void setInterestNotice(String aInterestNotice) {
        InterestNotice = aInterestNotice;
    }

    public String getInsuAccNo() {
        return InsuAccNo;
    }

    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getInterestTakePlace() {
        return InterestTakePlace;
    }

    public void setInterestTakePlace(String aInterestTakePlace) {
        InterestTakePlace = aInterestTakePlace;
    }

    public String getInterestType() {
        return InterestType;
    }

    public void setInterestType(String aInterestType) {
        InterestType = aInterestType;
    }

    public String getInterestCalType() {
        return InterestCalType;
    }

    public void setInterestCalType(String aInterestCalType) {
        InterestCalType = aInterestCalType;
    }

    public String getDefaultCalType() {
        return DefaultCalType;
    }

    public void setDefaultCalType(String aDefaultCalType) {
        DefaultCalType = aDefaultCalType;
    }

    public double getDefaultRate() {
        return DefaultRate;
    }

    public void setDefaultRate(double aDefaultRate) {
        DefaultRate = Arith.round(aDefaultRate, 6);
    }

    public void setDefaultRate(String aDefaultRate) {
        if (aDefaultRate != null && !aDefaultRate.equals("")) {
            Double tDouble = new Double(aDefaultRate);
            double d = tDouble.doubleValue();
            DefaultRate = Arith.round(d, 6);
        }
    }

    public String getFloatCalType() {
        return FloatCalType;
    }

    public void setFloatCalType(String aFloatCalType) {
        FloatCalType = aFloatCalType;
    }

    public String getFloatCalCode() {
        return FloatCalCode;
    }

    public void setFloatCalCode(String aFloatCalCode) {
        FloatCalCode = aFloatCalCode;
    }

    public String getInterestPeriod() {
        return InterestPeriod;
    }

    public void setInterestPeriod(String aInterestPeriod) {
        InterestPeriod = aInterestPeriod;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LMRiskInterestSchema 对象给 Schema 赋值
     * @param: aLMRiskInterestSchema LMRiskInterestSchema
     **/
    public void setSchema(LMRiskInterestSchema aLMRiskInterestSchema) {
        this.InterestCode = aLMRiskInterestSchema.getInterestCode();
        this.InterestName = aLMRiskInterestSchema.getInterestName();
        this.InterestNotice = aLMRiskInterestSchema.getInterestNotice();
        this.InsuAccNo = aLMRiskInterestSchema.getInsuAccNo();
        this.RiskCode = aLMRiskInterestSchema.getRiskCode();
        this.InterestTakePlace = aLMRiskInterestSchema.getInterestTakePlace();
        this.InterestType = aLMRiskInterestSchema.getInterestType();
        this.InterestCalType = aLMRiskInterestSchema.getInterestCalType();
        this.DefaultCalType = aLMRiskInterestSchema.getDefaultCalType();
        this.DefaultRate = aLMRiskInterestSchema.getDefaultRate();
        this.FloatCalType = aLMRiskInterestSchema.getFloatCalType();
        this.FloatCalCode = aLMRiskInterestSchema.getFloatCalCode();
        this.InterestPeriod = aLMRiskInterestSchema.getInterestPeriod();
        this.Operator = aLMRiskInterestSchema.getOperator();
        this.MakeDate = fDate.getDate(aLMRiskInterestSchema.getMakeDate());
        this.MakeTime = aLMRiskInterestSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLMRiskInterestSchema.getModifyDate());
        this.ModifyTime = aLMRiskInterestSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("InterestCode") == null) {
                this.InterestCode = null;
            } else {
                this.InterestCode = rs.getString("InterestCode").trim();
            }

            if (rs.getString("InterestName") == null) {
                this.InterestName = null;
            } else {
                this.InterestName = rs.getString("InterestName").trim();
            }

            if (rs.getString("InterestNotice") == null) {
                this.InterestNotice = null;
            } else {
                this.InterestNotice = rs.getString("InterestNotice").trim();
            }

            if (rs.getString("InsuAccNo") == null) {
                this.InsuAccNo = null;
            } else {
                this.InsuAccNo = rs.getString("InsuAccNo").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("InterestTakePlace") == null) {
                this.InterestTakePlace = null;
            } else {
                this.InterestTakePlace = rs.getString("InterestTakePlace").trim();
            }

            if (rs.getString("InterestType") == null) {
                this.InterestType = null;
            } else {
                this.InterestType = rs.getString("InterestType").trim();
            }

            if (rs.getString("InterestCalType") == null) {
                this.InterestCalType = null;
            } else {
                this.InterestCalType = rs.getString("InterestCalType").trim();
            }

            if (rs.getString("DefaultCalType") == null) {
                this.DefaultCalType = null;
            } else {
                this.DefaultCalType = rs.getString("DefaultCalType").trim();
            }

            this.DefaultRate = rs.getDouble("DefaultRate");
            if (rs.getString("FloatCalType") == null) {
                this.FloatCalType = null;
            } else {
                this.FloatCalType = rs.getString("FloatCalType").trim();
            }

            if (rs.getString("FloatCalCode") == null) {
                this.FloatCalCode = null;
            } else {
                this.FloatCalCode = rs.getString("FloatCalCode").trim();
            }

            if (rs.getString("InterestPeriod") == null) {
                this.InterestPeriod = null;
            } else {
                this.InterestPeriod = rs.getString("InterestPeriod").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LMRiskInterest表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInterestSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LMRiskInterestSchema getSchema() {
        LMRiskInterestSchema aLMRiskInterestSchema = new LMRiskInterestSchema();
        aLMRiskInterestSchema.setSchema(this);
        return aLMRiskInterestSchema;
    }

    public LMRiskInterestDB getDB() {
        LMRiskInterestDB aDBOper = new LMRiskInterestDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskInterest描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(InterestCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InterestName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InterestNotice));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuAccNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InterestTakePlace));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InterestType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InterestCalType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DefaultCalType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DefaultRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FloatCalType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FloatCalCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InterestPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskInterest>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            InterestCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            InterestName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            InterestNotice = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            InterestTakePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               6, SysConst.PACKAGESPILTER);
            InterestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            InterestCalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             8, SysConst.PACKAGESPILTER);
            DefaultCalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                            SysConst.PACKAGESPILTER);
            DefaultRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            FloatCalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            FloatCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            InterestPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            13, SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInterestSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("InterestCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestCode));
        }
        if (FCode.equals("InterestName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestName));
        }
        if (FCode.equals("InterestNotice")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestNotice));
        }
        if (FCode.equals("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("InterestTakePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestTakePlace));
        }
        if (FCode.equals("InterestType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestType));
        }
        if (FCode.equals("InterestCalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestCalType));
        }
        if (FCode.equals("DefaultCalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultCalType));
        }
        if (FCode.equals("DefaultRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultRate));
        }
        if (FCode.equals("FloatCalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FloatCalType));
        }
        if (FCode.equals("FloatCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FloatCalCode));
        }
        if (FCode.equals("InterestPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestPeriod));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(InterestCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(InterestName);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(InterestNotice);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(InterestTakePlace);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(InterestType);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(InterestCalType);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(DefaultCalType);
            break;
        case 9:
            strFieldValue = String.valueOf(DefaultRate);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(FloatCalType);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(FloatCalCode);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(InterestPeriod);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("InterestCode")) {
            if (FValue != null && !FValue.equals("")) {
                InterestCode = FValue.trim();
            } else {
                InterestCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("InterestName")) {
            if (FValue != null && !FValue.equals("")) {
                InterestName = FValue.trim();
            } else {
                InterestName = null;
            }
        }
        if (FCode.equalsIgnoreCase("InterestNotice")) {
            if (FValue != null && !FValue.equals("")) {
                InterestNotice = FValue.trim();
            } else {
                InterestNotice = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if (FValue != null && !FValue.equals("")) {
                InsuAccNo = FValue.trim();
            } else {
                InsuAccNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("InterestTakePlace")) {
            if (FValue != null && !FValue.equals("")) {
                InterestTakePlace = FValue.trim();
            } else {
                InterestTakePlace = null;
            }
        }
        if (FCode.equalsIgnoreCase("InterestType")) {
            if (FValue != null && !FValue.equals("")) {
                InterestType = FValue.trim();
            } else {
                InterestType = null;
            }
        }
        if (FCode.equalsIgnoreCase("InterestCalType")) {
            if (FValue != null && !FValue.equals("")) {
                InterestCalType = FValue.trim();
            } else {
                InterestCalType = null;
            }
        }
        if (FCode.equalsIgnoreCase("DefaultCalType")) {
            if (FValue != null && !FValue.equals("")) {
                DefaultCalType = FValue.trim();
            } else {
                DefaultCalType = null;
            }
        }
        if (FCode.equalsIgnoreCase("DefaultRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DefaultRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FloatCalType")) {
            if (FValue != null && !FValue.equals("")) {
                FloatCalType = FValue.trim();
            } else {
                FloatCalType = null;
            }
        }
        if (FCode.equalsIgnoreCase("FloatCalCode")) {
            if (FValue != null && !FValue.equals("")) {
                FloatCalCode = FValue.trim();
            } else {
                FloatCalCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("InterestPeriod")) {
            if (FValue != null && !FValue.equals("")) {
                InterestPeriod = FValue.trim();
            } else {
                InterestPeriod = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LMRiskInterestSchema other = (LMRiskInterestSchema) otherObject;
        return
                InterestCode.equals(other.getInterestCode())
                && InterestName.equals(other.getInterestName())
                && InterestNotice.equals(other.getInterestNotice())
                && InsuAccNo.equals(other.getInsuAccNo())
                && RiskCode.equals(other.getRiskCode())
                && InterestTakePlace.equals(other.getInterestTakePlace())
                && InterestType.equals(other.getInterestType())
                && InterestCalType.equals(other.getInterestCalType())
                && DefaultCalType.equals(other.getDefaultCalType())
                && DefaultRate == other.getDefaultRate()
                && FloatCalType.equals(other.getFloatCalType())
                && FloatCalCode.equals(other.getFloatCalCode())
                && InterestPeriod.equals(other.getInterestPeriod())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("InterestCode")) {
            return 0;
        }
        if (strFieldName.equals("InterestName")) {
            return 1;
        }
        if (strFieldName.equals("InterestNotice")) {
            return 2;
        }
        if (strFieldName.equals("InsuAccNo")) {
            return 3;
        }
        if (strFieldName.equals("RiskCode")) {
            return 4;
        }
        if (strFieldName.equals("InterestTakePlace")) {
            return 5;
        }
        if (strFieldName.equals("InterestType")) {
            return 6;
        }
        if (strFieldName.equals("InterestCalType")) {
            return 7;
        }
        if (strFieldName.equals("DefaultCalType")) {
            return 8;
        }
        if (strFieldName.equals("DefaultRate")) {
            return 9;
        }
        if (strFieldName.equals("FloatCalType")) {
            return 10;
        }
        if (strFieldName.equals("FloatCalCode")) {
            return 11;
        }
        if (strFieldName.equals("InterestPeriod")) {
            return 12;
        }
        if (strFieldName.equals("Operator")) {
            return 13;
        }
        if (strFieldName.equals("MakeDate")) {
            return 14;
        }
        if (strFieldName.equals("MakeTime")) {
            return 15;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 16;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "InterestCode";
            break;
        case 1:
            strFieldName = "InterestName";
            break;
        case 2:
            strFieldName = "InterestNotice";
            break;
        case 3:
            strFieldName = "InsuAccNo";
            break;
        case 4:
            strFieldName = "RiskCode";
            break;
        case 5:
            strFieldName = "InterestTakePlace";
            break;
        case 6:
            strFieldName = "InterestType";
            break;
        case 7:
            strFieldName = "InterestCalType";
            break;
        case 8:
            strFieldName = "DefaultCalType";
            break;
        case 9:
            strFieldName = "DefaultRate";
            break;
        case 10:
            strFieldName = "FloatCalType";
            break;
        case 11:
            strFieldName = "FloatCalCode";
            break;
        case 12:
            strFieldName = "InterestPeriod";
            break;
        case 13:
            strFieldName = "Operator";
            break;
        case 14:
            strFieldName = "MakeDate";
            break;
        case 15:
            strFieldName = "MakeTime";
            break;
        case 16:
            strFieldName = "ModifyDate";
            break;
        case 17:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("InterestCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestNotice")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuAccNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestTakePlace")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestCalType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DefaultCalType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DefaultRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FloatCalType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FloatCalCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestPeriod")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
