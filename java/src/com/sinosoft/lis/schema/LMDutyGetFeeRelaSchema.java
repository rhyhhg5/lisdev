/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMDutyGetFeeRelaDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LMDutyGetFeeRelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-06-23
 */
public class LMDutyGetFeeRelaSchema implements Schema, Cloneable
{
    // @Field
    /** 给付代码 */
    private String GetDutyCode;
    /** 给付名称 */
    private String GetDutyName;
    /** 给付责任类型 */
    private String GetDutyKind;
    /** 账单项目编码 */
    private String Feecode;
    /** 账单项目名称 */
    private String FeeName;
    /** 每日限额控制方式 */
    private String DayFeeMAXCtrl;
    /** 每日限额计算公式 */
    private String DayFeeMaxCalCode;
    /** 每日限额固定值 */
    private double DayFeeMaxValue;
    /** 总限额控制方式 */
    private String TotalFeeMaxCtrl;
    /** 总限额计算公式 */
    private String TotalFeeMaxCalCode;
    /** 总限额固定值 */
    private double TotalFeeMaxValue;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMDutyGetFeeRelaSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "GetDutyCode";
        pk[1] = "GetDutyKind";
        pk[2] = "Feecode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LMDutyGetFeeRelaSchema cloned = (LMDutyGetFeeRelaSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGetDutyCode()
    {
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public String getGetDutyName()
    {
        return GetDutyName;
    }

    public void setGetDutyName(String aGetDutyName)
    {
        GetDutyName = aGetDutyName;
    }

    public String getGetDutyKind()
    {
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind)
    {
        GetDutyKind = aGetDutyKind;
    }

    public String getFeecode()
    {
        return Feecode;
    }

    public void setFeecode(String aFeecode)
    {
        Feecode = aFeecode;
    }

    public String getFeeName()
    {
        return FeeName;
    }

    public void setFeeName(String aFeeName)
    {
        FeeName = aFeeName;
    }

    public String getDayFeeMAXCtrl()
    {
        return DayFeeMAXCtrl;
    }

    public void setDayFeeMAXCtrl(String aDayFeeMAXCtrl)
    {
        DayFeeMAXCtrl = aDayFeeMAXCtrl;
    }

    public String getDayFeeMaxCalCode()
    {
        return DayFeeMaxCalCode;
    }

    public void setDayFeeMaxCalCode(String aDayFeeMaxCalCode)
    {
        DayFeeMaxCalCode = aDayFeeMaxCalCode;
    }

    public double getDayFeeMaxValue()
    {
        return DayFeeMaxValue;
    }

    public void setDayFeeMaxValue(double aDayFeeMaxValue)
    {
        DayFeeMaxValue = Arith.round(aDayFeeMaxValue, 6);
    }

    public void setDayFeeMaxValue(String aDayFeeMaxValue)
    {
        if (aDayFeeMaxValue != null && !aDayFeeMaxValue.equals(""))
        {
            Double tDouble = new Double(aDayFeeMaxValue);
            double d = tDouble.doubleValue();
            DayFeeMaxValue = Arith.round(d, 6);
        }
    }

    public String getTotalFeeMaxCtrl()
    {
        return TotalFeeMaxCtrl;
    }

    public void setTotalFeeMaxCtrl(String aTotalFeeMaxCtrl)
    {
        TotalFeeMaxCtrl = aTotalFeeMaxCtrl;
    }

    public String getTotalFeeMaxCalCode()
    {
        return TotalFeeMaxCalCode;
    }

    public void setTotalFeeMaxCalCode(String aTotalFeeMaxCalCode)
    {
        TotalFeeMaxCalCode = aTotalFeeMaxCalCode;
    }

    public double getTotalFeeMaxValue()
    {
        return TotalFeeMaxValue;
    }

    public void setTotalFeeMaxValue(double aTotalFeeMaxValue)
    {
        TotalFeeMaxValue = Arith.round(aTotalFeeMaxValue, 6);
    }

    public void setTotalFeeMaxValue(String aTotalFeeMaxValue)
    {
        if (aTotalFeeMaxValue != null && !aTotalFeeMaxValue.equals(""))
        {
            Double tDouble = new Double(aTotalFeeMaxValue);
            double d = tDouble.doubleValue();
            TotalFeeMaxValue = Arith.round(d, 6);
        }
    }


    /**
     * 使用另外一个 LMDutyGetFeeRelaSchema 对象给 Schema 赋值
     * @param: aLMDutyGetFeeRelaSchema LMDutyGetFeeRelaSchema
     **/
    public void setSchema(LMDutyGetFeeRelaSchema aLMDutyGetFeeRelaSchema)
    {
        this.GetDutyCode = aLMDutyGetFeeRelaSchema.getGetDutyCode();
        this.GetDutyName = aLMDutyGetFeeRelaSchema.getGetDutyName();
        this.GetDutyKind = aLMDutyGetFeeRelaSchema.getGetDutyKind();
        this.Feecode = aLMDutyGetFeeRelaSchema.getFeecode();
        this.FeeName = aLMDutyGetFeeRelaSchema.getFeeName();
        this.DayFeeMAXCtrl = aLMDutyGetFeeRelaSchema.getDayFeeMAXCtrl();
        this.DayFeeMaxCalCode = aLMDutyGetFeeRelaSchema.getDayFeeMaxCalCode();
        this.DayFeeMaxValue = aLMDutyGetFeeRelaSchema.getDayFeeMaxValue();
        this.TotalFeeMaxCtrl = aLMDutyGetFeeRelaSchema.getTotalFeeMaxCtrl();
        this.TotalFeeMaxCalCode = aLMDutyGetFeeRelaSchema.getTotalFeeMaxCalCode();
        this.TotalFeeMaxValue = aLMDutyGetFeeRelaSchema.getTotalFeeMaxValue();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            if (rs.getString("GetDutyName") == null)
            {
                this.GetDutyName = null;
            }
            else
            {
                this.GetDutyName = rs.getString("GetDutyName").trim();
            }

            if (rs.getString("GetDutyKind") == null)
            {
                this.GetDutyKind = null;
            }
            else
            {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

            if (rs.getString("Feecode") == null)
            {
                this.Feecode = null;
            }
            else
            {
                this.Feecode = rs.getString("Feecode").trim();
            }

            if (rs.getString("FeeName") == null)
            {
                this.FeeName = null;
            }
            else
            {
                this.FeeName = rs.getString("FeeName").trim();
            }

            if (rs.getString("DayFeeMAXCtrl") == null)
            {
                this.DayFeeMAXCtrl = null;
            }
            else
            {
                this.DayFeeMAXCtrl = rs.getString("DayFeeMAXCtrl").trim();
            }

            if (rs.getString("DayFeeMaxCalCode") == null)
            {
                this.DayFeeMaxCalCode = null;
            }
            else
            {
                this.DayFeeMaxCalCode = rs.getString("DayFeeMaxCalCode").trim();
            }

            this.DayFeeMaxValue = rs.getDouble("DayFeeMaxValue");
            if (rs.getString("TotalFeeMaxCtrl") == null)
            {
                this.TotalFeeMaxCtrl = null;
            }
            else
            {
                this.TotalFeeMaxCtrl = rs.getString("TotalFeeMaxCtrl").trim();
            }

            if (rs.getString("TotalFeeMaxCalCode") == null)
            {
                this.TotalFeeMaxCalCode = null;
            }
            else
            {
                this.TotalFeeMaxCalCode = rs.getString("TotalFeeMaxCalCode").
                                          trim();
            }

            this.TotalFeeMaxValue = rs.getDouble("TotalFeeMaxValue");
        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LMDutyGetFeeRela表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyGetFeeRelaSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LMDutyGetFeeRelaSchema getSchema()
    {
        LMDutyGetFeeRelaSchema aLMDutyGetFeeRelaSchema = new
                LMDutyGetFeeRelaSchema();
        aLMDutyGetFeeRelaSchema.setSchema(this);
        return aLMDutyGetFeeRelaSchema;
    }

    public LMDutyGetFeeRelaDB getDB()
    {
        LMDutyGetFeeRelaDB aDBOper = new LMDutyGetFeeRelaDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyGetFeeRela描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GetDutyCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyKind));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Feecode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DayFeeMAXCtrl));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DayFeeMaxCalCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DayFeeMaxValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TotalFeeMaxCtrl));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TotalFeeMaxCalCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TotalFeeMaxValue));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyGetFeeRela>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            GetDutyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            Feecode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            FeeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            DayFeeMAXCtrl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                           SysConst.PACKAGESPILTER);
            DayFeeMaxCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              7, SysConst.PACKAGESPILTER);
            DayFeeMaxValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            TotalFeeMaxCtrl = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             9, SysConst.PACKAGESPILTER);
            TotalFeeMaxCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                10, SysConst.PACKAGESPILTER);
            TotalFeeMaxValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyGetFeeRelaSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equals("GetDutyName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (FCode.equals("GetDutyKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equals("Feecode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Feecode));
        }
        if (FCode.equals("FeeName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeName));
        }
        if (FCode.equals("DayFeeMAXCtrl"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DayFeeMAXCtrl));
        }
        if (FCode.equals("DayFeeMaxCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DayFeeMaxCalCode));
        }
        if (FCode.equals("DayFeeMaxValue"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DayFeeMaxValue));
        }
        if (FCode.equals("TotalFeeMaxCtrl"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TotalFeeMaxCtrl));
        }
        if (FCode.equals("TotalFeeMaxCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TotalFeeMaxCalCode));
        }
        if (FCode.equals("TotalFeeMaxValue"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TotalFeeMaxValue));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GetDutyName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Feecode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(FeeName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DayFeeMAXCtrl);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DayFeeMaxCalCode);
                break;
            case 7:
                strFieldValue = String.valueOf(DayFeeMaxValue);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(TotalFeeMaxCtrl);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(TotalFeeMaxCalCode);
                break;
            case 10:
                strFieldValue = String.valueOf(TotalFeeMaxValue);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("GetDutyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
            {
                GetDutyName = null;
            }
        }
        if (FCode.equals("GetDutyKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
            {
                GetDutyKind = null;
            }
        }
        if (FCode.equals("Feecode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Feecode = FValue.trim();
            }
            else
            {
                Feecode = null;
            }
        }
        if (FCode.equals("FeeName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FeeName = FValue.trim();
            }
            else
            {
                FeeName = null;
            }
        }
        if (FCode.equals("DayFeeMAXCtrl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DayFeeMAXCtrl = FValue.trim();
            }
            else
            {
                DayFeeMAXCtrl = null;
            }
        }
        if (FCode.equals("DayFeeMaxCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DayFeeMaxCalCode = FValue.trim();
            }
            else
            {
                DayFeeMaxCalCode = null;
            }
        }
        if (FCode.equals("DayFeeMaxValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DayFeeMaxValue = d;
            }
        }
        if (FCode.equals("TotalFeeMaxCtrl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TotalFeeMaxCtrl = FValue.trim();
            }
            else
            {
                TotalFeeMaxCtrl = null;
            }
        }
        if (FCode.equals("TotalFeeMaxCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TotalFeeMaxCalCode = FValue.trim();
            }
            else
            {
                TotalFeeMaxCalCode = null;
            }
        }
        if (FCode.equals("TotalFeeMaxValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TotalFeeMaxValue = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMDutyGetFeeRelaSchema other = (LMDutyGetFeeRelaSchema) otherObject;
        return
                GetDutyCode.equals(other.getGetDutyCode())
                && GetDutyName.equals(other.getGetDutyName())
                && GetDutyKind.equals(other.getGetDutyKind())
                && Feecode.equals(other.getFeecode())
                && FeeName.equals(other.getFeeName())
                && DayFeeMAXCtrl.equals(other.getDayFeeMAXCtrl())
                && DayFeeMaxCalCode.equals(other.getDayFeeMaxCalCode())
                && DayFeeMaxValue == other.getDayFeeMaxValue()
                && TotalFeeMaxCtrl.equals(other.getTotalFeeMaxCtrl())
                && TotalFeeMaxCalCode.equals(other.getTotalFeeMaxCalCode())
                && TotalFeeMaxValue == other.getTotalFeeMaxValue();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GetDutyCode"))
        {
            return 0;
        }
        if (strFieldName.equals("GetDutyName"))
        {
            return 1;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return 2;
        }
        if (strFieldName.equals("Feecode"))
        {
            return 3;
        }
        if (strFieldName.equals("FeeName"))
        {
            return 4;
        }
        if (strFieldName.equals("DayFeeMAXCtrl"))
        {
            return 5;
        }
        if (strFieldName.equals("DayFeeMaxCalCode"))
        {
            return 6;
        }
        if (strFieldName.equals("DayFeeMaxValue"))
        {
            return 7;
        }
        if (strFieldName.equals("TotalFeeMaxCtrl"))
        {
            return 8;
        }
        if (strFieldName.equals("TotalFeeMaxCalCode"))
        {
            return 9;
        }
        if (strFieldName.equals("TotalFeeMaxValue"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GetDutyCode";
                break;
            case 1:
                strFieldName = "GetDutyName";
                break;
            case 2:
                strFieldName = "GetDutyKind";
                break;
            case 3:
                strFieldName = "Feecode";
                break;
            case 4:
                strFieldName = "FeeName";
                break;
            case 5:
                strFieldName = "DayFeeMAXCtrl";
                break;
            case 6:
                strFieldName = "DayFeeMaxCalCode";
                break;
            case 7:
                strFieldName = "DayFeeMaxValue";
                break;
            case 8:
                strFieldName = "TotalFeeMaxCtrl";
                break;
            case 9:
                strFieldName = "TotalFeeMaxCalCode";
                break;
            case 10:
                strFieldName = "TotalFeeMaxValue";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Feecode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DayFeeMAXCtrl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DayFeeMaxCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DayFeeMaxValue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TotalFeeMaxCtrl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TotalFeeMaxCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TotalFeeMaxValue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
