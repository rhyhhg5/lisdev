/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAInterCommisionDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAInterCommisionSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-29
 */
public class LAInterCommisionSchema implements Schema
{
    // @Field
    /** 流水号 */
    private String CommSN;
    /** 记录类型 */
    private String RecType;
    /** 团险合同号 */
    private String GrpContNo;
    /** 险种保单号 */
    private String GrpPolNo;
    /** 团险业务员号码 */
    private String AgCd2;
    /** 团险业务员流水号 */
    private String AgSN2;
    /** 个险业务员号码 */
    private String AgCd1;
    /** 综合开拓专员 */
    private String Supporter;
    /** 保单生效日期 */
    private int CValiDate;
    /** 保单发单日期 */
    private int SignDate;
    /** 缴费结束日期 */
    private int PayEndDate;
    /** 保单满期日期 */
    private int EndDate;
    /** 保单交单日期 */
    private int MkPolDate;
    /** 险种代码 */
    private String RiskCode;
    /** 主附险标记 */
    private String MnRkFlag;
    /** 标准保费 */
    private double StPrem;
    /** 折算标准保费比例 */
    private double StPremRate;
    /** 缴费年度 */
    private int Payyear;
    /** 保单类型 */
    private String PolType;
    /** 管理费比例 */
    private double MgFeeRate;
    /** 分红比例 */
    private double BonusRate;
    /** 保单年期 */
    private int Years;
    /** 保单送达日期 */
    private int GtPlDate;
    /** 保单回执客户签收日期 */
    private int CuGtPlDate;
    /** 扫描日期 */
    private int ScanDate;
    /** 缴费间隔 */
    private int PayIntv;
    /** 缴费次数 */
    private int PayCount;
    /** 交易类别 */
    private String TransType;
    /** 缴费方式 */
    private String PayMode;
    /** 实收保费 */
    private double TransMoney;
    /** 直接佣金 */
    private double DirectWage;
    /** 直接佣金比例 */
    private double StFYCRate;
    /** 个险佣金折扣率 */
    private double FYCRate;
    /** 个险折扣佣金 */
    private double FYC;
    /** 件数 */
    private double CalCount;
    /** 导入日期 */
    private int MakeDate;
    /** 导入时间 */
    private String MakeTime;
    /** 导入人员账号 */
    private String Operator;
    /** 备用字段1 */
    private double F01;
    /** 备用字段2 */
    private double F02;
    /** 备用字段3 */
    private double F03;
    /** 备用字段4 */
    private double F04;
    /** 备用字段5 */
    private double F05;

    public static final int FIELDNUM = 43; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAInterCommisionSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "CommSN";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCommSN()
    {
        if (SysConst.CHANGECHARSET && CommSN != null && !CommSN.equals(""))
        {
            CommSN = StrTool.unicodeToGBK(CommSN);
        }
        return CommSN;
    }

    public void setCommSN(String aCommSN)
    {
        CommSN = aCommSN;
    }

    public String getRecType()
    {
        if (SysConst.CHANGECHARSET && RecType != null && !RecType.equals(""))
        {
            RecType = StrTool.unicodeToGBK(RecType);
        }
        return RecType;
    }

    public void setRecType(String aRecType)
    {
        RecType = aRecType;
    }

    public String getGrpContNo()
    {
        if (SysConst.CHANGECHARSET && GrpContNo != null && !GrpContNo.equals(""))
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (SysConst.CHANGECHARSET && GrpPolNo != null && !GrpPolNo.equals(""))
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getAgCd2()
    {
        if (SysConst.CHANGECHARSET && AgCd2 != null && !AgCd2.equals(""))
        {
            AgCd2 = StrTool.unicodeToGBK(AgCd2);
        }
        return AgCd2;
    }

    public void setAgCd2(String aAgCd2)
    {
        AgCd2 = aAgCd2;
    }

    public String getAgSN2()
    {
        if (SysConst.CHANGECHARSET && AgSN2 != null && !AgSN2.equals(""))
        {
            AgSN2 = StrTool.unicodeToGBK(AgSN2);
        }
        return AgSN2;
    }

    public void setAgSN2(String aAgSN2)
    {
        AgSN2 = aAgSN2;
    }

    public String getAgCd1()
    {
        if (SysConst.CHANGECHARSET && AgCd1 != null && !AgCd1.equals(""))
        {
            AgCd1 = StrTool.unicodeToGBK(AgCd1);
        }
        return AgCd1;
    }

    public void setAgCd1(String aAgCd1)
    {
        AgCd1 = aAgCd1;
    }

    public String getSupporter()
    {
        if (SysConst.CHANGECHARSET && Supporter != null && !Supporter.equals(""))
        {
            Supporter = StrTool.unicodeToGBK(Supporter);
        }
        return Supporter;
    }

    public void setSupporter(String aSupporter)
    {
        Supporter = aSupporter;
    }

    public int getCValiDate()
    {
        return CValiDate;
    }

    public void setCValiDate(int aCValiDate)
    {
        CValiDate = aCValiDate;
    }

    public void setCValiDate(String aCValiDate)
    {
        if (aCValiDate != null && !aCValiDate.equals(""))
        {
            Integer tInteger = new Integer(aCValiDate);
            int i = tInteger.intValue();
            CValiDate = i;
        }
    }

    public int getSignDate()
    {
        return SignDate;
    }

    public void setSignDate(int aSignDate)
    {
        SignDate = aSignDate;
    }

    public void setSignDate(String aSignDate)
    {
        if (aSignDate != null && !aSignDate.equals(""))
        {
            Integer tInteger = new Integer(aSignDate);
            int i = tInteger.intValue();
            SignDate = i;
        }
    }

    public int getPayEndDate()
    {
        return PayEndDate;
    }

    public void setPayEndDate(int aPayEndDate)
    {
        PayEndDate = aPayEndDate;
    }

    public void setPayEndDate(String aPayEndDate)
    {
        if (aPayEndDate != null && !aPayEndDate.equals(""))
        {
            Integer tInteger = new Integer(aPayEndDate);
            int i = tInteger.intValue();
            PayEndDate = i;
        }
    }

    public int getEndDate()
    {
        return EndDate;
    }

    public void setEndDate(int aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            Integer tInteger = new Integer(aEndDate);
            int i = tInteger.intValue();
            EndDate = i;
        }
    }

    public int getMkPolDate()
    {
        return MkPolDate;
    }

    public void setMkPolDate(int aMkPolDate)
    {
        MkPolDate = aMkPolDate;
    }

    public void setMkPolDate(String aMkPolDate)
    {
        if (aMkPolDate != null && !aMkPolDate.equals(""))
        {
            Integer tInteger = new Integer(aMkPolDate);
            int i = tInteger.intValue();
            MkPolDate = i;
        }
    }

    public String getRiskCode()
    {
        if (SysConst.CHANGECHARSET && RiskCode != null && !RiskCode.equals(""))
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getMnRkFlag()
    {
        if (SysConst.CHANGECHARSET && MnRkFlag != null && !MnRkFlag.equals(""))
        {
            MnRkFlag = StrTool.unicodeToGBK(MnRkFlag);
        }
        return MnRkFlag;
    }

    public void setMnRkFlag(String aMnRkFlag)
    {
        MnRkFlag = aMnRkFlag;
    }

    public double getStPrem()
    {
        return StPrem;
    }

    public void setStPrem(double aStPrem)
    {
        StPrem = aStPrem;
    }

    public void setStPrem(String aStPrem)
    {
        if (aStPrem != null && !aStPrem.equals(""))
        {
            Double tDouble = new Double(aStPrem);
            double d = tDouble.doubleValue();
            StPrem = d;
        }
    }

    public double getStPremRate()
    {
        return StPremRate;
    }

    public void setStPremRate(double aStPremRate)
    {
        StPremRate = aStPremRate;
    }

    public void setStPremRate(String aStPremRate)
    {
        if (aStPremRate != null && !aStPremRate.equals(""))
        {
            Double tDouble = new Double(aStPremRate);
            double d = tDouble.doubleValue();
            StPremRate = d;
        }
    }

    public int getPayyear()
    {
        return Payyear;
    }

    public void setPayyear(int aPayyear)
    {
        Payyear = aPayyear;
    }

    public void setPayyear(String aPayyear)
    {
        if (aPayyear != null && !aPayyear.equals(""))
        {
            Integer tInteger = new Integer(aPayyear);
            int i = tInteger.intValue();
            Payyear = i;
        }
    }

    public String getPolType()
    {
        if (SysConst.CHANGECHARSET && PolType != null && !PolType.equals(""))
        {
            PolType = StrTool.unicodeToGBK(PolType);
        }
        return PolType;
    }

    public void setPolType(String aPolType)
    {
        PolType = aPolType;
    }

    public double getMgFeeRate()
    {
        return MgFeeRate;
    }

    public void setMgFeeRate(double aMgFeeRate)
    {
        MgFeeRate = aMgFeeRate;
    }

    public void setMgFeeRate(String aMgFeeRate)
    {
        if (aMgFeeRate != null && !aMgFeeRate.equals(""))
        {
            Double tDouble = new Double(aMgFeeRate);
            double d = tDouble.doubleValue();
            MgFeeRate = d;
        }
    }

    public double getBonusRate()
    {
        return BonusRate;
    }

    public void setBonusRate(double aBonusRate)
    {
        BonusRate = aBonusRate;
    }

    public void setBonusRate(String aBonusRate)
    {
        if (aBonusRate != null && !aBonusRate.equals(""))
        {
            Double tDouble = new Double(aBonusRate);
            double d = tDouble.doubleValue();
            BonusRate = d;
        }
    }

    public int getYears()
    {
        return Years;
    }

    public void setYears(int aYears)
    {
        Years = aYears;
    }

    public void setYears(String aYears)
    {
        if (aYears != null && !aYears.equals(""))
        {
            Integer tInteger = new Integer(aYears);
            int i = tInteger.intValue();
            Years = i;
        }
    }

    public int getGtPlDate()
    {
        return GtPlDate;
    }

    public void setGtPlDate(int aGtPlDate)
    {
        GtPlDate = aGtPlDate;
    }

    public void setGtPlDate(String aGtPlDate)
    {
        if (aGtPlDate != null && !aGtPlDate.equals(""))
        {
            Integer tInteger = new Integer(aGtPlDate);
            int i = tInteger.intValue();
            GtPlDate = i;
        }
    }

    public int getCuGtPlDate()
    {
        return CuGtPlDate;
    }

    public void setCuGtPlDate(int aCuGtPlDate)
    {
        CuGtPlDate = aCuGtPlDate;
    }

    public void setCuGtPlDate(String aCuGtPlDate)
    {
        if (aCuGtPlDate != null && !aCuGtPlDate.equals(""))
        {
            Integer tInteger = new Integer(aCuGtPlDate);
            int i = tInteger.intValue();
            CuGtPlDate = i;
        }
    }

    public int getScanDate()
    {
        return ScanDate;
    }

    public void setScanDate(int aScanDate)
    {
        ScanDate = aScanDate;
    }

    public void setScanDate(String aScanDate)
    {
        if (aScanDate != null && !aScanDate.equals(""))
        {
            Integer tInteger = new Integer(aScanDate);
            int i = tInteger.intValue();
            ScanDate = i;
        }
    }

    public int getPayIntv()
    {
        return PayIntv;
    }

    public void setPayIntv(int aPayIntv)
    {
        PayIntv = aPayIntv;
    }

    public void setPayIntv(String aPayIntv)
    {
        if (aPayIntv != null && !aPayIntv.equals(""))
        {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public int getPayCount()
    {
        return PayCount;
    }

    public void setPayCount(int aPayCount)
    {
        PayCount = aPayCount;
    }

    public void setPayCount(String aPayCount)
    {
        if (aPayCount != null && !aPayCount.equals(""))
        {
            Integer tInteger = new Integer(aPayCount);
            int i = tInteger.intValue();
            PayCount = i;
        }
    }

    public String getTransType()
    {
        if (SysConst.CHANGECHARSET && TransType != null && !TransType.equals(""))
        {
            TransType = StrTool.unicodeToGBK(TransType);
        }
        return TransType;
    }

    public void setTransType(String aTransType)
    {
        TransType = aTransType;
    }

    public String getPayMode()
    {
        if (SysConst.CHANGECHARSET && PayMode != null && !PayMode.equals(""))
        {
            PayMode = StrTool.unicodeToGBK(PayMode);
        }
        return PayMode;
    }

    public void setPayMode(String aPayMode)
    {
        PayMode = aPayMode;
    }

    public double getTransMoney()
    {
        return TransMoney;
    }

    public void setTransMoney(double aTransMoney)
    {
        TransMoney = aTransMoney;
    }

    public void setTransMoney(String aTransMoney)
    {
        if (aTransMoney != null && !aTransMoney.equals(""))
        {
            Double tDouble = new Double(aTransMoney);
            double d = tDouble.doubleValue();
            TransMoney = d;
        }
    }

    public double getDirectWage()
    {
        return DirectWage;
    }

    public void setDirectWage(double aDirectWage)
    {
        DirectWage = aDirectWage;
    }

    public void setDirectWage(String aDirectWage)
    {
        if (aDirectWage != null && !aDirectWage.equals(""))
        {
            Double tDouble = new Double(aDirectWage);
            double d = tDouble.doubleValue();
            DirectWage = d;
        }
    }

    public double getStFYCRate()
    {
        return StFYCRate;
    }

    public void setStFYCRate(double aStFYCRate)
    {
        StFYCRate = aStFYCRate;
    }

    public void setStFYCRate(String aStFYCRate)
    {
        if (aStFYCRate != null && !aStFYCRate.equals(""))
        {
            Double tDouble = new Double(aStFYCRate);
            double d = tDouble.doubleValue();
            StFYCRate = d;
        }
    }

    public double getFYCRate()
    {
        return FYCRate;
    }

    public void setFYCRate(double aFYCRate)
    {
        FYCRate = aFYCRate;
    }

    public void setFYCRate(String aFYCRate)
    {
        if (aFYCRate != null && !aFYCRate.equals(""))
        {
            Double tDouble = new Double(aFYCRate);
            double d = tDouble.doubleValue();
            FYCRate = d;
        }
    }

    public double getFYC()
    {
        return FYC;
    }

    public void setFYC(double aFYC)
    {
        FYC = aFYC;
    }

    public void setFYC(String aFYC)
    {
        if (aFYC != null && !aFYC.equals(""))
        {
            Double tDouble = new Double(aFYC);
            double d = tDouble.doubleValue();
            FYC = d;
        }
    }

    public double getCalCount()
    {
        return CalCount;
    }

    public void setCalCount(double aCalCount)
    {
        CalCount = aCalCount;
    }

    public void setCalCount(String aCalCount)
    {
        if (aCalCount != null && !aCalCount.equals(""))
        {
            Double tDouble = new Double(aCalCount);
            double d = tDouble.doubleValue();
            CalCount = d;
        }
    }

    public int getMakeDate()
    {
        return MakeDate;
    }

    public void setMakeDate(int aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            Integer tInteger = new Integer(aMakeDate);
            int i = tInteger.intValue();
            MakeDate = i;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public double getF01()
    {
        return F01;
    }

    public void setF01(double aF01)
    {
        F01 = aF01;
    }

    public void setF01(String aF01)
    {
        if (aF01 != null && !aF01.equals(""))
        {
            Double tDouble = new Double(aF01);
            double d = tDouble.doubleValue();
            F01 = d;
        }
    }

    public double getF02()
    {
        return F02;
    }

    public void setF02(double aF02)
    {
        F02 = aF02;
    }

    public void setF02(String aF02)
    {
        if (aF02 != null && !aF02.equals(""))
        {
            Double tDouble = new Double(aF02);
            double d = tDouble.doubleValue();
            F02 = d;
        }
    }

    public double getF03()
    {
        return F03;
    }

    public void setF03(double aF03)
    {
        F03 = aF03;
    }

    public void setF03(String aF03)
    {
        if (aF03 != null && !aF03.equals(""))
        {
            Double tDouble = new Double(aF03);
            double d = tDouble.doubleValue();
            F03 = d;
        }
    }

    public double getF04()
    {
        return F04;
    }

    public void setF04(double aF04)
    {
        F04 = aF04;
    }

    public void setF04(String aF04)
    {
        if (aF04 != null && !aF04.equals(""))
        {
            Double tDouble = new Double(aF04);
            double d = tDouble.doubleValue();
            F04 = d;
        }
    }

    public double getF05()
    {
        return F05;
    }

    public void setF05(double aF05)
    {
        F05 = aF05;
    }

    public void setF05(String aF05)
    {
        if (aF05 != null && !aF05.equals(""))
        {
            Double tDouble = new Double(aF05);
            double d = tDouble.doubleValue();
            F05 = d;
        }
    }


    /**
     * 使用另外一个 LAInterCommisionSchema 对象给 Schema 赋值
     * @param: aLAInterCommisionSchema LAInterCommisionSchema
     **/
    public void setSchema(LAInterCommisionSchema aLAInterCommisionSchema)
    {
        this.CommSN = aLAInterCommisionSchema.getCommSN();
        this.RecType = aLAInterCommisionSchema.getRecType();
        this.GrpContNo = aLAInterCommisionSchema.getGrpContNo();
        this.GrpPolNo = aLAInterCommisionSchema.getGrpPolNo();
        this.AgCd2 = aLAInterCommisionSchema.getAgCd2();
        this.AgSN2 = aLAInterCommisionSchema.getAgSN2();
        this.AgCd1 = aLAInterCommisionSchema.getAgCd1();
        this.Supporter = aLAInterCommisionSchema.getSupporter();
        this.CValiDate = aLAInterCommisionSchema.getCValiDate();
        this.SignDate = aLAInterCommisionSchema.getSignDate();
        this.PayEndDate = aLAInterCommisionSchema.getPayEndDate();
        this.EndDate = aLAInterCommisionSchema.getEndDate();
        this.MkPolDate = aLAInterCommisionSchema.getMkPolDate();
        this.RiskCode = aLAInterCommisionSchema.getRiskCode();
        this.MnRkFlag = aLAInterCommisionSchema.getMnRkFlag();
        this.StPrem = aLAInterCommisionSchema.getStPrem();
        this.StPremRate = aLAInterCommisionSchema.getStPremRate();
        this.Payyear = aLAInterCommisionSchema.getPayyear();
        this.PolType = aLAInterCommisionSchema.getPolType();
        this.MgFeeRate = aLAInterCommisionSchema.getMgFeeRate();
        this.BonusRate = aLAInterCommisionSchema.getBonusRate();
        this.Years = aLAInterCommisionSchema.getYears();
        this.GtPlDate = aLAInterCommisionSchema.getGtPlDate();
        this.CuGtPlDate = aLAInterCommisionSchema.getCuGtPlDate();
        this.ScanDate = aLAInterCommisionSchema.getScanDate();
        this.PayIntv = aLAInterCommisionSchema.getPayIntv();
        this.PayCount = aLAInterCommisionSchema.getPayCount();
        this.TransType = aLAInterCommisionSchema.getTransType();
        this.PayMode = aLAInterCommisionSchema.getPayMode();
        this.TransMoney = aLAInterCommisionSchema.getTransMoney();
        this.DirectWage = aLAInterCommisionSchema.getDirectWage();
        this.StFYCRate = aLAInterCommisionSchema.getStFYCRate();
        this.FYCRate = aLAInterCommisionSchema.getFYCRate();
        this.FYC = aLAInterCommisionSchema.getFYC();
        this.CalCount = aLAInterCommisionSchema.getCalCount();
        this.MakeDate = aLAInterCommisionSchema.getMakeDate();
        this.MakeTime = aLAInterCommisionSchema.getMakeTime();
        this.Operator = aLAInterCommisionSchema.getOperator();
        this.F01 = aLAInterCommisionSchema.getF01();
        this.F02 = aLAInterCommisionSchema.getF02();
        this.F03 = aLAInterCommisionSchema.getF03();
        this.F04 = aLAInterCommisionSchema.getF04();
        this.F05 = aLAInterCommisionSchema.getF05();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CommSN") == null)
            {
                this.CommSN = null;
            }
            else
            {
                this.CommSN = rs.getString("CommSN").trim();
            }

            if (rs.getString("RecType") == null)
            {
                this.RecType = null;
            }
            else
            {
                this.RecType = rs.getString("RecType").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("AgCd2") == null)
            {
                this.AgCd2 = null;
            }
            else
            {
                this.AgCd2 = rs.getString("AgCd2").trim();
            }

            if (rs.getString("AgSN2") == null)
            {
                this.AgSN2 = null;
            }
            else
            {
                this.AgSN2 = rs.getString("AgSN2").trim();
            }

            if (rs.getString("AgCd1") == null)
            {
                this.AgCd1 = null;
            }
            else
            {
                this.AgCd1 = rs.getString("AgCd1").trim();
            }

            if (rs.getString("Supporter") == null)
            {
                this.Supporter = null;
            }
            else
            {
                this.Supporter = rs.getString("Supporter").trim();
            }

            this.CValiDate = rs.getInt("CValiDate");
            this.SignDate = rs.getInt("SignDate");
            this.PayEndDate = rs.getInt("PayEndDate");
            this.EndDate = rs.getInt("EndDate");
            this.MkPolDate = rs.getInt("MkPolDate");
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("MnRkFlag") == null)
            {
                this.MnRkFlag = null;
            }
            else
            {
                this.MnRkFlag = rs.getString("MnRkFlag").trim();
            }

            this.StPrem = rs.getDouble("StPrem");
            this.StPremRate = rs.getDouble("StPremRate");
            this.Payyear = rs.getInt("Payyear");
            if (rs.getString("PolType") == null)
            {
                this.PolType = null;
            }
            else
            {
                this.PolType = rs.getString("PolType").trim();
            }

            this.MgFeeRate = rs.getDouble("MgFeeRate");
            this.BonusRate = rs.getDouble("BonusRate");
            this.Years = rs.getInt("Years");
            this.GtPlDate = rs.getInt("GtPlDate");
            this.CuGtPlDate = rs.getInt("CuGtPlDate");
            this.ScanDate = rs.getInt("ScanDate");
            this.PayIntv = rs.getInt("PayIntv");
            this.PayCount = rs.getInt("PayCount");
            if (rs.getString("TransType") == null)
            {
                this.TransType = null;
            }
            else
            {
                this.TransType = rs.getString("TransType").trim();
            }

            if (rs.getString("PayMode") == null)
            {
                this.PayMode = null;
            }
            else
            {
                this.PayMode = rs.getString("PayMode").trim();
            }

            this.TransMoney = rs.getDouble("TransMoney");
            this.DirectWage = rs.getDouble("DirectWage");
            this.StFYCRate = rs.getDouble("StFYCRate");
            this.FYCRate = rs.getDouble("FYCRate");
            this.FYC = rs.getDouble("FYC");
            this.CalCount = rs.getDouble("CalCount");
            this.MakeDate = rs.getInt("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.F01 = rs.getDouble("F01");
            this.F02 = rs.getDouble("F02");
            this.F03 = rs.getDouble("F03");
            this.F04 = rs.getDouble("F04");
            this.F05 = rs.getDouble("F05");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInterCommisionSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAInterCommisionSchema getSchema()
    {
        LAInterCommisionSchema aLAInterCommisionSchema = new
                LAInterCommisionSchema();
        aLAInterCommisionSchema.setSchema(this);
        return aLAInterCommisionSchema;
    }

    public LAInterCommisionDB getDB()
    {
        LAInterCommisionDB aDBOper = new LAInterCommisionDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterCommision描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CommSN)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RecType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgCd2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgSN2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgCd1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Supporter)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(CValiDate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(SignDate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayEndDate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(EndDate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(MkPolDate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MnRkFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StPremRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Payyear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(MgFeeRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BonusRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Years) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GtPlDate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CuGtPlDate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ScanDate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayIntv) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayCount) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TransType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayMode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(TransMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(DirectWage) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StFYCRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(FYCRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(FYC) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CalCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(MakeDate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(F01) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(F02) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(F03) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(F04) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(F05);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAInterCommision>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CommSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            RecType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            AgCd2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            AgSN2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            AgCd1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            Supporter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            CValiDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            SignDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            PayEndDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).intValue();
            EndDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).intValue();
            MkPolDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).intValue();
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MnRkFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            StPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    16, SysConst.PACKAGESPILTER))).doubleValue();
            StPremRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            Payyear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).intValue();
            PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                     SysConst.PACKAGESPILTER);
            MgFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            BonusRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            Years = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    22, SysConst.PACKAGESPILTER))).intValue();
            GtPlDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 23, SysConst.PACKAGESPILTER))).intValue();
            CuGtPlDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 24, SysConst.PACKAGESPILTER))).intValue();
            ScanDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 25, SysConst.PACKAGESPILTER))).intValue();
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 26, SysConst.PACKAGESPILTER))).intValue();
            PayCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).intValue();
            TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                       SysConst.PACKAGESPILTER);
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                     SysConst.PACKAGESPILTER);
            TransMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 30, SysConst.PACKAGESPILTER))).doubleValue();
            DirectWage = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 31, SysConst.PACKAGESPILTER))).doubleValue();
            StFYCRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 32, SysConst.PACKAGESPILTER))).doubleValue();
            FYCRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 33, SysConst.PACKAGESPILTER))).doubleValue();
            FYC = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    34, SysConst.PACKAGESPILTER))).doubleValue();
            CalCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 35, SysConst.PACKAGESPILTER))).doubleValue();
            MakeDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 36, SysConst.PACKAGESPILTER))).intValue();
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,
                                      SysConst.PACKAGESPILTER);
            F01 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    39, SysConst.PACKAGESPILTER))).doubleValue();
            F02 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    40, SysConst.PACKAGESPILTER))).doubleValue();
            F03 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    41, SysConst.PACKAGESPILTER))).doubleValue();
            F04 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    42, SysConst.PACKAGESPILTER))).doubleValue();
            F05 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    43, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInterCommisionSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CommSN"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CommSN));
        }
        if (FCode.equals("RecType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecType));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("AgCd2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgCd2));
        }
        if (FCode.equals("AgSN2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgSN2));
        }
        if (FCode.equals("AgCd1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgCd1));
        }
        if (FCode.equals("Supporter"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Supporter));
        }
        if (FCode.equals("CValiDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equals("SignDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
        }
        if (FCode.equals("PayEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDate));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equals("MkPolDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MkPolDate));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("MnRkFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MnRkFlag));
        }
        if (FCode.equals("StPrem"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StPrem));
        }
        if (FCode.equals("StPremRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StPremRate));
        }
        if (FCode.equals("Payyear"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Payyear));
        }
        if (FCode.equals("PolType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
        }
        if (FCode.equals("MgFeeRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MgFeeRate));
        }
        if (FCode.equals("BonusRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusRate));
        }
        if (FCode.equals("Years"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
        }
        if (FCode.equals("GtPlDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GtPlDate));
        }
        if (FCode.equals("CuGtPlDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CuGtPlDate));
        }
        if (FCode.equals("ScanDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanDate));
        }
        if (FCode.equals("PayIntv"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equals("PayCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
        }
        if (FCode.equals("TransType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
        }
        if (FCode.equals("PayMode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equals("TransMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoney));
        }
        if (FCode.equals("DirectWage"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirectWage));
        }
        if (FCode.equals("StFYCRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StFYCRate));
        }
        if (FCode.equals("FYCRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FYCRate));
        }
        if (FCode.equals("FYC"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FYC));
        }
        if (FCode.equals("CalCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCount));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("F01"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(F01));
        }
        if (FCode.equals("F02"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(F02));
        }
        if (FCode.equals("F03"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(F03));
        }
        if (FCode.equals("F04"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(F04));
        }
        if (FCode.equals("F05"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(F05));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CommSN);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RecType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AgCd2);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AgSN2);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AgCd1);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Supporter);
                break;
            case 8:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 9:
                strFieldValue = String.valueOf(SignDate);
                break;
            case 10:
                strFieldValue = String.valueOf(PayEndDate);
                break;
            case 11:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 12:
                strFieldValue = String.valueOf(MkPolDate);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MnRkFlag);
                break;
            case 15:
                strFieldValue = String.valueOf(StPrem);
                break;
            case 16:
                strFieldValue = String.valueOf(StPremRate);
                break;
            case 17:
                strFieldValue = String.valueOf(Payyear);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(PolType);
                break;
            case 19:
                strFieldValue = String.valueOf(MgFeeRate);
                break;
            case 20:
                strFieldValue = String.valueOf(BonusRate);
                break;
            case 21:
                strFieldValue = String.valueOf(Years);
                break;
            case 22:
                strFieldValue = String.valueOf(GtPlDate);
                break;
            case 23:
                strFieldValue = String.valueOf(CuGtPlDate);
                break;
            case 24:
                strFieldValue = String.valueOf(ScanDate);
                break;
            case 25:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 26:
                strFieldValue = String.valueOf(PayCount);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(TransType);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 29:
                strFieldValue = String.valueOf(TransMoney);
                break;
            case 30:
                strFieldValue = String.valueOf(DirectWage);
                break;
            case 31:
                strFieldValue = String.valueOf(StFYCRate);
                break;
            case 32:
                strFieldValue = String.valueOf(FYCRate);
                break;
            case 33:
                strFieldValue = String.valueOf(FYC);
                break;
            case 34:
                strFieldValue = String.valueOf(CalCount);
                break;
            case 35:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 38:
                strFieldValue = String.valueOf(F01);
                break;
            case 39:
                strFieldValue = String.valueOf(F02);
                break;
            case 40:
                strFieldValue = String.valueOf(F03);
                break;
            case 41:
                strFieldValue = String.valueOf(F04);
                break;
            case 42:
                strFieldValue = String.valueOf(F05);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CommSN"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CommSN = FValue.trim();
            }
            else
            {
                CommSN = null;
            }
        }
        if (FCode.equals("RecType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RecType = FValue.trim();
            }
            else
            {
                RecType = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("AgCd2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgCd2 = FValue.trim();
            }
            else
            {
                AgCd2 = null;
            }
        }
        if (FCode.equals("AgSN2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgSN2 = FValue.trim();
            }
            else
            {
                AgSN2 = null;
            }
        }
        if (FCode.equals("AgCd1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgCd1 = FValue.trim();
            }
            else
            {
                AgCd1 = null;
            }
        }
        if (FCode.equals("Supporter"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Supporter = FValue.trim();
            }
            else
            {
                Supporter = null;
            }
        }
        if (FCode.equals("CValiDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CValiDate = i;
            }
        }
        if (FCode.equals("SignDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SignDate = i;
            }
        }
        if (FCode.equals("PayEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayEndDate = i;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                EndDate = i;
            }
        }
        if (FCode.equals("MkPolDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MkPolDate = i;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("MnRkFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MnRkFlag = FValue.trim();
            }
            else
            {
                MnRkFlag = null;
            }
        }
        if (FCode.equals("StPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StPrem = d;
            }
        }
        if (FCode.equals("StPremRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StPremRate = d;
            }
        }
        if (FCode.equals("Payyear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Payyear = i;
            }
        }
        if (FCode.equals("PolType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolType = FValue.trim();
            }
            else
            {
                PolType = null;
            }
        }
        if (FCode.equals("MgFeeRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MgFeeRate = d;
            }
        }
        if (FCode.equals("BonusRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BonusRate = d;
            }
        }
        if (FCode.equals("Years"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Years = i;
            }
        }
        if (FCode.equals("GtPlDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GtPlDate = i;
            }
        }
        if (FCode.equals("CuGtPlDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CuGtPlDate = i;
            }
        }
        if (FCode.equals("ScanDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ScanDate = i;
            }
        }
        if (FCode.equals("PayIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equals("PayCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayCount = i;
            }
        }
        if (FCode.equals("TransType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TransType = FValue.trim();
            }
            else
            {
                TransType = null;
            }
        }
        if (FCode.equals("PayMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
            {
                PayMode = null;
            }
        }
        if (FCode.equals("TransMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TransMoney = d;
            }
        }
        if (FCode.equals("DirectWage"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DirectWage = d;
            }
        }
        if (FCode.equals("StFYCRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StFYCRate = d;
            }
        }
        if (FCode.equals("FYCRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FYCRate = d;
            }
        }
        if (FCode.equals("FYC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FYC = d;
            }
        }
        if (FCode.equals("CalCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CalCount = d;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MakeDate = i;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("F01"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                F01 = d;
            }
        }
        if (FCode.equals("F02"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                F02 = d;
            }
        }
        if (FCode.equals("F03"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                F03 = d;
            }
        }
        if (FCode.equals("F04"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                F04 = d;
            }
        }
        if (FCode.equals("F05"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                F05 = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAInterCommisionSchema other = (LAInterCommisionSchema) otherObject;
        return
                CommSN.equals(other.getCommSN())
                && RecType.equals(other.getRecType())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && AgCd2.equals(other.getAgCd2())
                && AgSN2.equals(other.getAgSN2())
                && AgCd1.equals(other.getAgCd1())
                && Supporter.equals(other.getSupporter())
                && CValiDate == other.getCValiDate()
                && SignDate == other.getSignDate()
                && PayEndDate == other.getPayEndDate()
                && EndDate == other.getEndDate()
                && MkPolDate == other.getMkPolDate()
                && RiskCode.equals(other.getRiskCode())
                && MnRkFlag.equals(other.getMnRkFlag())
                && StPrem == other.getStPrem()
                && StPremRate == other.getStPremRate()
                && Payyear == other.getPayyear()
                && PolType.equals(other.getPolType())
                && MgFeeRate == other.getMgFeeRate()
                && BonusRate == other.getBonusRate()
                && Years == other.getYears()
                && GtPlDate == other.getGtPlDate()
                && CuGtPlDate == other.getCuGtPlDate()
                && ScanDate == other.getScanDate()
                && PayIntv == other.getPayIntv()
                && PayCount == other.getPayCount()
                && TransType.equals(other.getTransType())
                && PayMode.equals(other.getPayMode())
                && TransMoney == other.getTransMoney()
                && DirectWage == other.getDirectWage()
                && StFYCRate == other.getStFYCRate()
                && FYCRate == other.getFYCRate()
                && FYC == other.getFYC()
                && CalCount == other.getCalCount()
                && MakeDate == other.getMakeDate()
                && MakeTime.equals(other.getMakeTime())
                && Operator.equals(other.getOperator())
                && F01 == other.getF01()
                && F02 == other.getF02()
                && F03 == other.getF03()
                && F04 == other.getF04()
                && F05 == other.getF05();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CommSN"))
        {
            return 0;
        }
        if (strFieldName.equals("RecType"))
        {
            return 1;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 3;
        }
        if (strFieldName.equals("AgCd2"))
        {
            return 4;
        }
        if (strFieldName.equals("AgSN2"))
        {
            return 5;
        }
        if (strFieldName.equals("AgCd1"))
        {
            return 6;
        }
        if (strFieldName.equals("Supporter"))
        {
            return 7;
        }
        if (strFieldName.equals("CValiDate"))
        {
            return 8;
        }
        if (strFieldName.equals("SignDate"))
        {
            return 9;
        }
        if (strFieldName.equals("PayEndDate"))
        {
            return 10;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MkPolDate"))
        {
            return 12;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 13;
        }
        if (strFieldName.equals("MnRkFlag"))
        {
            return 14;
        }
        if (strFieldName.equals("StPrem"))
        {
            return 15;
        }
        if (strFieldName.equals("StPremRate"))
        {
            return 16;
        }
        if (strFieldName.equals("Payyear"))
        {
            return 17;
        }
        if (strFieldName.equals("PolType"))
        {
            return 18;
        }
        if (strFieldName.equals("MgFeeRate"))
        {
            return 19;
        }
        if (strFieldName.equals("BonusRate"))
        {
            return 20;
        }
        if (strFieldName.equals("Years"))
        {
            return 21;
        }
        if (strFieldName.equals("GtPlDate"))
        {
            return 22;
        }
        if (strFieldName.equals("CuGtPlDate"))
        {
            return 23;
        }
        if (strFieldName.equals("ScanDate"))
        {
            return 24;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return 25;
        }
        if (strFieldName.equals("PayCount"))
        {
            return 26;
        }
        if (strFieldName.equals("TransType"))
        {
            return 27;
        }
        if (strFieldName.equals("PayMode"))
        {
            return 28;
        }
        if (strFieldName.equals("TransMoney"))
        {
            return 29;
        }
        if (strFieldName.equals("DirectWage"))
        {
            return 30;
        }
        if (strFieldName.equals("StFYCRate"))
        {
            return 31;
        }
        if (strFieldName.equals("FYCRate"))
        {
            return 32;
        }
        if (strFieldName.equals("FYC"))
        {
            return 33;
        }
        if (strFieldName.equals("CalCount"))
        {
            return 34;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 35;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 36;
        }
        if (strFieldName.equals("Operator"))
        {
            return 37;
        }
        if (strFieldName.equals("F01"))
        {
            return 38;
        }
        if (strFieldName.equals("F02"))
        {
            return 39;
        }
        if (strFieldName.equals("F03"))
        {
            return 40;
        }
        if (strFieldName.equals("F04"))
        {
            return 41;
        }
        if (strFieldName.equals("F05"))
        {
            return 42;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CommSN";
                break;
            case 1:
                strFieldName = "RecType";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "GrpPolNo";
                break;
            case 4:
                strFieldName = "AgCd2";
                break;
            case 5:
                strFieldName = "AgSN2";
                break;
            case 6:
                strFieldName = "AgCd1";
                break;
            case 7:
                strFieldName = "Supporter";
                break;
            case 8:
                strFieldName = "CValiDate";
                break;
            case 9:
                strFieldName = "SignDate";
                break;
            case 10:
                strFieldName = "PayEndDate";
                break;
            case 11:
                strFieldName = "EndDate";
                break;
            case 12:
                strFieldName = "MkPolDate";
                break;
            case 13:
                strFieldName = "RiskCode";
                break;
            case 14:
                strFieldName = "MnRkFlag";
                break;
            case 15:
                strFieldName = "StPrem";
                break;
            case 16:
                strFieldName = "StPremRate";
                break;
            case 17:
                strFieldName = "Payyear";
                break;
            case 18:
                strFieldName = "PolType";
                break;
            case 19:
                strFieldName = "MgFeeRate";
                break;
            case 20:
                strFieldName = "BonusRate";
                break;
            case 21:
                strFieldName = "Years";
                break;
            case 22:
                strFieldName = "GtPlDate";
                break;
            case 23:
                strFieldName = "CuGtPlDate";
                break;
            case 24:
                strFieldName = "ScanDate";
                break;
            case 25:
                strFieldName = "PayIntv";
                break;
            case 26:
                strFieldName = "PayCount";
                break;
            case 27:
                strFieldName = "TransType";
                break;
            case 28:
                strFieldName = "PayMode";
                break;
            case 29:
                strFieldName = "TransMoney";
                break;
            case 30:
                strFieldName = "DirectWage";
                break;
            case 31:
                strFieldName = "StFYCRate";
                break;
            case 32:
                strFieldName = "FYCRate";
                break;
            case 33:
                strFieldName = "FYC";
                break;
            case 34:
                strFieldName = "CalCount";
                break;
            case 35:
                strFieldName = "MakeDate";
                break;
            case 36:
                strFieldName = "MakeTime";
                break;
            case 37:
                strFieldName = "Operator";
                break;
            case 38:
                strFieldName = "F01";
                break;
            case 39:
                strFieldName = "F02";
                break;
            case 40:
                strFieldName = "F03";
                break;
            case 41:
                strFieldName = "F04";
                break;
            case 42:
                strFieldName = "F05";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CommSN"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RecType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgCd2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgSN2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgCd1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Supporter"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CValiDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("SignDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayEndDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MkPolDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MnRkFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StPremRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Payyear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PolType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MgFeeRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BonusRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Years"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GtPlDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CuGtPlDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ScanDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("TransType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TransMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DirectWage"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StFYCRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FYCRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FYC"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CalCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("F01"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("F02"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("F03"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("F04"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("F05"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_INT;
                break;
            case 11:
                nFieldType = Schema.TYPE_INT;
                break;
            case 12:
                nFieldType = Schema.TYPE_INT;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 17:
                nFieldType = Schema.TYPE_INT;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_INT;
                break;
            case 22:
                nFieldType = Schema.TYPE_INT;
                break;
            case 23:
                nFieldType = Schema.TYPE_INT;
                break;
            case 24:
                nFieldType = Schema.TYPE_INT;
                break;
            case 25:
                nFieldType = Schema.TYPE_INT;
                break;
            case 26:
                nFieldType = Schema.TYPE_INT;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 30:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 31:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 32:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 33:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 34:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 35:
                nFieldType = Schema.TYPE_INT;
                break;
            case 36:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 37:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 38:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 39:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 40:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 41:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 42:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
