/*
 * <p>ClassName: LMRiskRnewSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMRiskRnewDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMRiskRnewSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 险种名称 */
    private String RiskName;
    /** 续保类型 */
    private String RnewType;
    /** 宽限期 */
    private int GracePeriod;
    /** 宽限期单位 */
    private String GracePeriodUnit;
    /** 宽限日期计算方式 */
    private String GraceDateCalMode;
    /** 续保最大年龄 */
    private int MaxAge;
    /** 续保限制 */
    private String RnewLmt;
    /** 原保单终止日算法 */
    private int EndDateCalMode;
    /** 续保确认日算法 */
    private int ComfirmDateCalMode;
    /** 保证续保标记 */
    private String AssuRnewFlag;
    /** 保证续保要求的续保次数 */
    private int RnewTimes;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMRiskRnewSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getRiskName()
    {
        if (RiskName != null && !RiskName.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskName = StrTool.unicodeToGBK(RiskName);
        }
        return RiskName;
    }

    public void setRiskName(String aRiskName)
    {
        RiskName = aRiskName;
    }

    public String getRnewType()
    {
        if (RnewType != null && !RnewType.equals("") && SysConst.CHANGECHARSET == true)
        {
            RnewType = StrTool.unicodeToGBK(RnewType);
        }
        return RnewType;
    }

    public void setRnewType(String aRnewType)
    {
        RnewType = aRnewType;
    }

    public int getGracePeriod()
    {
        return GracePeriod;
    }

    public void setGracePeriod(int aGracePeriod)
    {
        GracePeriod = aGracePeriod;
    }

    public void setGracePeriod(String aGracePeriod)
    {
        if (aGracePeriod != null && !aGracePeriod.equals(""))
        {
            Integer tInteger = new Integer(aGracePeriod);
            int i = tInteger.intValue();
            GracePeriod = i;
        }
    }

    public String getGracePeriodUnit()
    {
        if (GracePeriodUnit != null && !GracePeriodUnit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GracePeriodUnit = StrTool.unicodeToGBK(GracePeriodUnit);
        }
        return GracePeriodUnit;
    }

    public void setGracePeriodUnit(String aGracePeriodUnit)
    {
        GracePeriodUnit = aGracePeriodUnit;
    }

    public String getGraceDateCalMode()
    {
        if (GraceDateCalMode != null && !GraceDateCalMode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GraceDateCalMode = StrTool.unicodeToGBK(GraceDateCalMode);
        }
        return GraceDateCalMode;
    }

    public void setGraceDateCalMode(String aGraceDateCalMode)
    {
        GraceDateCalMode = aGraceDateCalMode;
    }

    public int getMaxAge()
    {
        return MaxAge;
    }

    public void setMaxAge(int aMaxAge)
    {
        MaxAge = aMaxAge;
    }

    public void setMaxAge(String aMaxAge)
    {
        if (aMaxAge != null && !aMaxAge.equals(""))
        {
            Integer tInteger = new Integer(aMaxAge);
            int i = tInteger.intValue();
            MaxAge = i;
        }
    }

    public String getRnewLmt()
    {
        if (RnewLmt != null && !RnewLmt.equals("") && SysConst.CHANGECHARSET == true)
        {
            RnewLmt = StrTool.unicodeToGBK(RnewLmt);
        }
        return RnewLmt;
    }

    public void setRnewLmt(String aRnewLmt)
    {
        RnewLmt = aRnewLmt;
    }

    public int getEndDateCalMode()
    {
        return EndDateCalMode;
    }

    public void setEndDateCalMode(int aEndDateCalMode)
    {
        EndDateCalMode = aEndDateCalMode;
    }

    public void setEndDateCalMode(String aEndDateCalMode)
    {
        if (aEndDateCalMode != null && !aEndDateCalMode.equals(""))
        {
            Integer tInteger = new Integer(aEndDateCalMode);
            int i = tInteger.intValue();
            EndDateCalMode = i;
        }
    }

    public int getComfirmDateCalMode()
    {
        return ComfirmDateCalMode;
    }

    public void setComfirmDateCalMode(int aComfirmDateCalMode)
    {
        ComfirmDateCalMode = aComfirmDateCalMode;
    }

    public void setComfirmDateCalMode(String aComfirmDateCalMode)
    {
        if (aComfirmDateCalMode != null && !aComfirmDateCalMode.equals(""))
        {
            Integer tInteger = new Integer(aComfirmDateCalMode);
            int i = tInteger.intValue();
            ComfirmDateCalMode = i;
        }
    }

    public String getAssuRnewFlag()
    {
        if (AssuRnewFlag != null && !AssuRnewFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AssuRnewFlag = StrTool.unicodeToGBK(AssuRnewFlag);
        }
        return AssuRnewFlag;
    }

    public void setAssuRnewFlag(String aAssuRnewFlag)
    {
        AssuRnewFlag = aAssuRnewFlag;
    }

    public int getRnewTimes()
    {
        return RnewTimes;
    }

    public void setRnewTimes(int aRnewTimes)
    {
        RnewTimes = aRnewTimes;
    }

    public void setRnewTimes(String aRnewTimes)
    {
        if (aRnewTimes != null && !aRnewTimes.equals(""))
        {
            Integer tInteger = new Integer(aRnewTimes);
            int i = tInteger.intValue();
            RnewTimes = i;
        }
    }


    /**
     * 使用另外一个 LMRiskRnewSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMRiskRnewSchema aLMRiskRnewSchema)
    {
        this.RiskCode = aLMRiskRnewSchema.getRiskCode();
        this.RiskVer = aLMRiskRnewSchema.getRiskVer();
        this.RiskName = aLMRiskRnewSchema.getRiskName();
        this.RnewType = aLMRiskRnewSchema.getRnewType();
        this.GracePeriod = aLMRiskRnewSchema.getGracePeriod();
        this.GracePeriodUnit = aLMRiskRnewSchema.getGracePeriodUnit();
        this.GraceDateCalMode = aLMRiskRnewSchema.getGraceDateCalMode();
        this.MaxAge = aLMRiskRnewSchema.getMaxAge();
        this.RnewLmt = aLMRiskRnewSchema.getRnewLmt();
        this.EndDateCalMode = aLMRiskRnewSchema.getEndDateCalMode();
        this.ComfirmDateCalMode = aLMRiskRnewSchema.getComfirmDateCalMode();
        this.AssuRnewFlag = aLMRiskRnewSchema.getAssuRnewFlag();
        this.RnewTimes = aLMRiskRnewSchema.getRnewTimes();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("RiskName") == null)
            {
                this.RiskName = null;
            }
            else
            {
                this.RiskName = rs.getString("RiskName").trim();
            }

            if (rs.getString("RnewType") == null)
            {
                this.RnewType = null;
            }
            else
            {
                this.RnewType = rs.getString("RnewType").trim();
            }

            this.GracePeriod = rs.getInt("GracePeriod");
            if (rs.getString("GracePeriodUnit") == null)
            {
                this.GracePeriodUnit = null;
            }
            else
            {
                this.GracePeriodUnit = rs.getString("GracePeriodUnit").trim();
            }

            if (rs.getString("GraceDateCalMode") == null)
            {
                this.GraceDateCalMode = null;
            }
            else
            {
                this.GraceDateCalMode = rs.getString("GraceDateCalMode").trim();
            }

            this.MaxAge = rs.getInt("MaxAge");
            if (rs.getString("RnewLmt") == null)
            {
                this.RnewLmt = null;
            }
            else
            {
                this.RnewLmt = rs.getString("RnewLmt").trim();
            }

            this.EndDateCalMode = rs.getInt("EndDateCalMode");
            this.ComfirmDateCalMode = rs.getInt("ComfirmDateCalMode");
            if (rs.getString("AssuRnewFlag") == null)
            {
                this.AssuRnewFlag = null;
            }
            else
            {
                this.AssuRnewFlag = rs.getString("AssuRnewFlag").trim();
            }

            this.RnewTimes = rs.getInt("RnewTimes");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRnewSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMRiskRnewSchema getSchema()
    {
        LMRiskRnewSchema aLMRiskRnewSchema = new LMRiskRnewSchema();
        aLMRiskRnewSchema.setSchema(this);
        return aLMRiskRnewSchema;
    }

    public LMRiskRnewDB getDB()
    {
        LMRiskRnewDB aDBOper = new LMRiskRnewDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskRnew描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RnewType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GracePeriod) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GracePeriodUnit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GraceDateCalMode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(MaxAge) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RnewLmt)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(EndDateCalMode) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ComfirmDateCalMode) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AssuRnewFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(RnewTimes);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskRnew>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RnewType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            GracePeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            GracePeriodUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             6, SysConst.PACKAGESPILTER);
            GraceDateCalMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              7, SysConst.PACKAGESPILTER);
            MaxAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            RnewLmt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            EndDateCalMode = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            ComfirmDateCalMode = new Integer(ChgData.chgNumericStr(StrTool.
                    getStr(strMessage, 11, SysConst.PACKAGESPILTER))).intValue();
            AssuRnewFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            RnewTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).intValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRnewSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("RiskName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskName));
        }
        if (FCode.equals("RnewType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RnewType));
        }
        if (FCode.equals("GracePeriod"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GracePeriod));
        }
        if (FCode.equals("GracePeriodUnit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GracePeriodUnit));
        }
        if (FCode.equals("GraceDateCalMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GraceDateCalMode));
        }
        if (FCode.equals("MaxAge"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MaxAge));
        }
        if (FCode.equals("RnewLmt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RnewLmt));
        }
        if (FCode.equals("EndDateCalMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndDateCalMode));
        }
        if (FCode.equals("ComfirmDateCalMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    ComfirmDateCalMode));
        }
        if (FCode.equals("AssuRnewFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AssuRnewFlag));
        }
        if (FCode.equals("RnewTimes"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RnewTimes));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RnewType);
                break;
            case 4:
                strFieldValue = String.valueOf(GracePeriod);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(GracePeriodUnit);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(GraceDateCalMode);
                break;
            case 7:
                strFieldValue = String.valueOf(MaxAge);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RnewLmt);
                break;
            case 9:
                strFieldValue = String.valueOf(EndDateCalMode);
                break;
            case 10:
                strFieldValue = String.valueOf(ComfirmDateCalMode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AssuRnewFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(RnewTimes);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("RiskName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
            {
                RiskName = null;
            }
        }
        if (FCode.equals("RnewType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RnewType = FValue.trim();
            }
            else
            {
                RnewType = null;
            }
        }
        if (FCode.equals("GracePeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GracePeriod = i;
            }
        }
        if (FCode.equals("GracePeriodUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GracePeriodUnit = FValue.trim();
            }
            else
            {
                GracePeriodUnit = null;
            }
        }
        if (FCode.equals("GraceDateCalMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GraceDateCalMode = FValue.trim();
            }
            else
            {
                GraceDateCalMode = null;
            }
        }
        if (FCode.equals("MaxAge"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MaxAge = i;
            }
        }
        if (FCode.equals("RnewLmt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RnewLmt = FValue.trim();
            }
            else
            {
                RnewLmt = null;
            }
        }
        if (FCode.equals("EndDateCalMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                EndDateCalMode = i;
            }
        }
        if (FCode.equals("ComfirmDateCalMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ComfirmDateCalMode = i;
            }
        }
        if (FCode.equals("AssuRnewFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssuRnewFlag = FValue.trim();
            }
            else
            {
                AssuRnewFlag = null;
            }
        }
        if (FCode.equals("RnewTimes"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RnewTimes = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMRiskRnewSchema other = (LMRiskRnewSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && RiskName.equals(other.getRiskName())
                && RnewType.equals(other.getRnewType())
                && GracePeriod == other.getGracePeriod()
                && GracePeriodUnit.equals(other.getGracePeriodUnit())
                && GraceDateCalMode.equals(other.getGraceDateCalMode())
                && MaxAge == other.getMaxAge()
                && RnewLmt.equals(other.getRnewLmt())
                && EndDateCalMode == other.getEndDateCalMode()
                && ComfirmDateCalMode == other.getComfirmDateCalMode()
                && AssuRnewFlag.equals(other.getAssuRnewFlag())
                && RnewTimes == other.getRnewTimes();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskName"))
        {
            return 2;
        }
        if (strFieldName.equals("RnewType"))
        {
            return 3;
        }
        if (strFieldName.equals("GracePeriod"))
        {
            return 4;
        }
        if (strFieldName.equals("GracePeriodUnit"))
        {
            return 5;
        }
        if (strFieldName.equals("GraceDateCalMode"))
        {
            return 6;
        }
        if (strFieldName.equals("MaxAge"))
        {
            return 7;
        }
        if (strFieldName.equals("RnewLmt"))
        {
            return 8;
        }
        if (strFieldName.equals("EndDateCalMode"))
        {
            return 9;
        }
        if (strFieldName.equals("ComfirmDateCalMode"))
        {
            return 10;
        }
        if (strFieldName.equals("AssuRnewFlag"))
        {
            return 11;
        }
        if (strFieldName.equals("RnewTimes"))
        {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "RnewType";
                break;
            case 4:
                strFieldName = "GracePeriod";
                break;
            case 5:
                strFieldName = "GracePeriodUnit";
                break;
            case 6:
                strFieldName = "GraceDateCalMode";
                break;
            case 7:
                strFieldName = "MaxAge";
                break;
            case 8:
                strFieldName = "RnewLmt";
                break;
            case 9:
                strFieldName = "EndDateCalMode";
                break;
            case 10:
                strFieldName = "ComfirmDateCalMode";
                break;
            case 11:
                strFieldName = "AssuRnewFlag";
                break;
            case 12:
                strFieldName = "RnewTimes";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RnewType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GracePeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GracePeriodUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GraceDateCalMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MaxAge"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RnewLmt"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndDateCalMode"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ComfirmDateCalMode"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AssuRnewFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RnewTimes"))
        {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_INT;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_INT;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_INT;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
