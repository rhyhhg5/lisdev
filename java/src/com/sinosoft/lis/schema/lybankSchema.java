/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.lybankDB;

/*
 * <p>ClassName: lybankSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 文件回盘中间表
 * @CreateDate：2018-11-24
 */
public class lybankSchema implements Schema, Cloneable
{
	// @Field
	/** 序号 */
	private String idx;
	/** 币种 */
	private String currency;
	/** 收款账号 */
	private String bankaccno;
	/** 收款单位名称 */
	private String bankaccname;
	/** 付款账号/卡号 */
	private String accno;
	/** 付款单位名称 */
	private String accname;
	/** 金额 */
	private String paymoney;
	/** 用途 */
	private String use;
	/** 网上银行状态 */
	private String state;
	/** 用户备注 */
	private String remark;
	/** 银行反馈信息 */
	private String other;
	/** 其他 */
	private String code1;
	/** 其他2 */
	private String code2;
	/** 其他3 */
	private String code3;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public lybankSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "idx";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		lybankSchema cloned = (lybankSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getidx()
	{
		return idx;
	}
	public void setidx(String aidx)
	{
		idx = aidx;
	}
	public String getcurrency()
	{
		return currency;
	}
	public void setcurrency(String acurrency)
	{
		currency = acurrency;
	}
	public String getbankaccno()
	{
		return bankaccno;
	}
	public void setbankaccno(String abankaccno)
	{
		bankaccno = abankaccno;
	}
	public String getbankaccname()
	{
		return bankaccname;
	}
	public void setbankaccname(String abankaccname)
	{
		bankaccname = abankaccname;
	}
	public String getaccno()
	{
		return accno;
	}
	public void setaccno(String aaccno)
	{
		accno = aaccno;
	}
	public String getaccname()
	{
		return accname;
	}
	public void setaccname(String aaccname)
	{
		accname = aaccname;
	}
	public String getpaymoney()
	{
		return paymoney;
	}
	public void setpaymoney(String apaymoney)
	{
		paymoney = apaymoney;
	}
	public String getuse()
	{
		return use;
	}
	public void setuse(String ause)
	{
		use = ause;
	}
	public String getstate()
	{
		return state;
	}
	public void setstate(String astate)
	{
		state = astate;
	}
	public String getremark()
	{
		return remark;
	}
	public void setremark(String aremark)
	{
		remark = aremark;
	}
	public String getother()
	{
		return other;
	}
	public void setother(String aother)
	{
		other = aother;
	}
	public String getcode1()
	{
		return code1;
	}
	public void setcode1(String acode1)
	{
		code1 = acode1;
	}
	public String getcode2()
	{
		return code2;
	}
	public void setcode2(String acode2)
	{
		code2 = acode2;
	}
	public String getcode3()
	{
		return code3;
	}
	public void setcode3(String acode3)
	{
		code3 = acode3;
	}

	/**
	* 使用另外一个 lybankSchema 对象给 Schema 赋值
	* @param: alybankSchema lybankSchema
	**/
	public void setSchema(lybankSchema alybankSchema)
	{
		this.idx = alybankSchema.getidx();
		this.currency = alybankSchema.getcurrency();
		this.bankaccno = alybankSchema.getbankaccno();
		this.bankaccname = alybankSchema.getbankaccname();
		this.accno = alybankSchema.getaccno();
		this.accname = alybankSchema.getaccname();
		this.paymoney = alybankSchema.getpaymoney();
		this.use = alybankSchema.getuse();
		this.state = alybankSchema.getstate();
		this.remark = alybankSchema.getremark();
		this.other = alybankSchema.getother();
		this.code1 = alybankSchema.getcode1();
		this.code2 = alybankSchema.getcode2();
		this.code3 = alybankSchema.getcode3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("idx") == null )
				this.idx = null;
			else
				this.idx = rs.getString("idx").trim();

			if( rs.getString("currency") == null )
				this.currency = null;
			else
				this.currency = rs.getString("currency").trim();

			if( rs.getString("bankaccno") == null )
				this.bankaccno = null;
			else
				this.bankaccno = rs.getString("bankaccno").trim();

			if( rs.getString("bankaccname") == null )
				this.bankaccname = null;
			else
				this.bankaccname = rs.getString("bankaccname").trim();

			if( rs.getString("accno") == null )
				this.accno = null;
			else
				this.accno = rs.getString("accno").trim();

			if( rs.getString("accname") == null )
				this.accname = null;
			else
				this.accname = rs.getString("accname").trim();

			if( rs.getString("paymoney") == null )
				this.paymoney = null;
			else
				this.paymoney = rs.getString("paymoney").trim();

			if( rs.getString("use") == null )
				this.use = null;
			else
				this.use = rs.getString("use").trim();

			if( rs.getString("state") == null )
				this.state = null;
			else
				this.state = rs.getString("state").trim();

			if( rs.getString("remark") == null )
				this.remark = null;
			else
				this.remark = rs.getString("remark").trim();

			if( rs.getString("other") == null )
				this.other = null;
			else
				this.other = rs.getString("other").trim();

			if( rs.getString("code1") == null )
				this.code1 = null;
			else
				this.code1 = rs.getString("code1").trim();

			if( rs.getString("code2") == null )
				this.code2 = null;
			else
				this.code2 = rs.getString("code2").trim();

			if( rs.getString("code3") == null )
				this.code3 = null;
			else
				this.code3 = rs.getString("code3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的lybank表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "lybankSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public lybankSchema getSchema()
	{
		lybankSchema alybankSchema = new lybankSchema();
		alybankSchema.setSchema(this);
		return alybankSchema;
	}

	public lybankDB getDB()
	{
		lybankDB aDBOper = new lybankDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prplybank描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(idx)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankaccno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankaccname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(accno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(accname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paymoney)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(use)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(state)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(other)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(code1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(code2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(code3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prplybank>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			idx = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			bankaccno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			bankaccname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			accno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			accname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			paymoney = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			use = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			state = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			other = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			code1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			code2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			code3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "lybankSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("idx"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(idx));
		}
		if (FCode.equals("currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(currency));
		}
		if (FCode.equals("bankaccno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankaccno));
		}
		if (FCode.equals("bankaccname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankaccname));
		}
		if (FCode.equals("accno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(accno));
		}
		if (FCode.equals("accname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(accname));
		}
		if (FCode.equals("paymoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paymoney));
		}
		if (FCode.equals("use"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(use));
		}
		if (FCode.equals("state"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(state));
		}
		if (FCode.equals("remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(remark));
		}
		if (FCode.equals("other"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(other));
		}
		if (FCode.equals("code1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(code1));
		}
		if (FCode.equals("code2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(code2));
		}
		if (FCode.equals("code3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(code3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(idx);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(currency);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(bankaccno);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(bankaccname);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(accno);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(accname);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(paymoney);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(use);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(state);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(remark);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(other);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(code1);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(code2);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(code3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("idx"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				idx = FValue.trim();
			}
			else
				idx = null;
		}
		if (FCode.equalsIgnoreCase("currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				currency = FValue.trim();
			}
			else
				currency = null;
		}
		if (FCode.equalsIgnoreCase("bankaccno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankaccno = FValue.trim();
			}
			else
				bankaccno = null;
		}
		if (FCode.equalsIgnoreCase("bankaccname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankaccname = FValue.trim();
			}
			else
				bankaccname = null;
		}
		if (FCode.equalsIgnoreCase("accno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				accno = FValue.trim();
			}
			else
				accno = null;
		}
		if (FCode.equalsIgnoreCase("accname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				accname = FValue.trim();
			}
			else
				accname = null;
		}
		if (FCode.equalsIgnoreCase("paymoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paymoney = FValue.trim();
			}
			else
				paymoney = null;
		}
		if (FCode.equalsIgnoreCase("use"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				use = FValue.trim();
			}
			else
				use = null;
		}
		if (FCode.equalsIgnoreCase("state"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				state = FValue.trim();
			}
			else
				state = null;
		}
		if (FCode.equalsIgnoreCase("remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				remark = FValue.trim();
			}
			else
				remark = null;
		}
		if (FCode.equalsIgnoreCase("other"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				other = FValue.trim();
			}
			else
				other = null;
		}
		if (FCode.equalsIgnoreCase("code1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				code1 = FValue.trim();
			}
			else
				code1 = null;
		}
		if (FCode.equalsIgnoreCase("code2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				code2 = FValue.trim();
			}
			else
				code2 = null;
		}
		if (FCode.equalsIgnoreCase("code3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				code3 = FValue.trim();
			}
			else
				code3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		lybankSchema other = (lybankSchema)otherObject;
		return
			(idx == null ? other.getidx() == null : idx.equals(other.getidx()))
			&& (currency == null ? other.getcurrency() == null : currency.equals(other.getcurrency()))
			&& (bankaccno == null ? other.getbankaccno() == null : bankaccno.equals(other.getbankaccno()))
			&& (bankaccname == null ? other.getbankaccname() == null : bankaccname.equals(other.getbankaccname()))
			&& (accno == null ? other.getaccno() == null : accno.equals(other.getaccno()))
			&& (accname == null ? other.getaccname() == null : accname.equals(other.getaccname()))
			&& (paymoney == null ? other.getpaymoney() == null : paymoney.equals(other.getpaymoney()))
			&& (use == null ? other.getuse() == null : use.equals(other.getuse()))
			&& (state == null ? other.getstate() == null : state.equals(other.getstate()))
			&& (remark == null ? other.getremark() == null : remark.equals(other.getremark()))
			&& (other == null ? other.getother() == null : other.equals(other.getother()))
			&& (code1 == null ? other.getcode1() == null : code1.equals(other.getcode1()))
			&& (code2 == null ? other.getcode2() == null : code2.equals(other.getcode2()))
			&& (code3 == null ? other.getcode3() == null : code3.equals(other.getcode3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("idx") ) {
			return 0;
		}
		if( strFieldName.equals("currency") ) {
			return 1;
		}
		if( strFieldName.equals("bankaccno") ) {
			return 2;
		}
		if( strFieldName.equals("bankaccname") ) {
			return 3;
		}
		if( strFieldName.equals("accno") ) {
			return 4;
		}
		if( strFieldName.equals("accname") ) {
			return 5;
		}
		if( strFieldName.equals("paymoney") ) {
			return 6;
		}
		if( strFieldName.equals("use") ) {
			return 7;
		}
		if( strFieldName.equals("state") ) {
			return 8;
		}
		if( strFieldName.equals("remark") ) {
			return 9;
		}
		if( strFieldName.equals("other") ) {
			return 10;
		}
		if( strFieldName.equals("code1") ) {
			return 11;
		}
		if( strFieldName.equals("code2") ) {
			return 12;
		}
		if( strFieldName.equals("code3") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "idx";
				break;
			case 1:
				strFieldName = "currency";
				break;
			case 2:
				strFieldName = "bankaccno";
				break;
			case 3:
				strFieldName = "bankaccname";
				break;
			case 4:
				strFieldName = "accno";
				break;
			case 5:
				strFieldName = "accname";
				break;
			case 6:
				strFieldName = "paymoney";
				break;
			case 7:
				strFieldName = "use";
				break;
			case 8:
				strFieldName = "state";
				break;
			case 9:
				strFieldName = "remark";
				break;
			case 10:
				strFieldName = "other";
				break;
			case 11:
				strFieldName = "code1";
				break;
			case 12:
				strFieldName = "code2";
				break;
			case 13:
				strFieldName = "code3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("idx") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bankaccno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bankaccname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("accno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("accname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paymoney") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("use") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("state") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("other") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("code1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("code2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("code3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
