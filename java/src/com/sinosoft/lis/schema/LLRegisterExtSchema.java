/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLRegisterExtDB;

/*
 * <p>ClassName: LLRegisterExtSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-04-10
 */
public class LLRegisterExtSchema implements Schema, Cloneable
{
	// @Field
	/** 立案号(申请登记号) */
	private String RgtNo;
	/** 保单号码 */
	private String GrpContno;
	/** 证件号码 */
	private String IDNo;
	/** 证件类型 */
	private String IDType;
	/** 单位名称 */
	private String GrpName;
	/** 联系人 */
	private String LinkMan;
	/** 单位地址 */
	private String GrpAddress;
	/** 单位有效证件名称 */
	private String GrpIDName;
	/** 其他证明文件名称 */
	private String GrpIDName2;
	/** 单位有效证件生效日期 */
	private Date GrpIDStartDate;
	/** 单位有效证件失效日期 */
	private Date GrpIDEndDate;
	/** 统一社会信用代码 */
	private String UnifiedSocialCreditNo;
	/** 组织机构代码 */
	private String OrgancomCode;
	/** 税务登记证代码 */
	private String TaxNo;
	/** 经营范围 */
	private String BusinessScope;
	/** 控股股东或者实际控制人的姓名 */
	private String ShareholderName;
	/** 控股股东或者实际控制人证件类型 */
	private String ShareholderIDType;
	/** 控股股东或者实际控制人证件号码 */
	private String ShareholderIDNo;
	/** 控股股东或者实际控制人证件生效期限 */
	private Date ShareholderIDStart;
	/** 控股股东或者实际控制人证件失效期限 */
	private Date ShareholderIDEnd;
	/** 法定代表人姓名 */
	private String LegalPersonName1;
	/** 法定代表人证件类型 */
	private String LegalPersonIDType1;
	/** 法定代表人证件号码 */
	private String LegalPersonIDNo1;
	/** 法定代表人证件生效期限 */
	private Date LegalPersonIDStart1;
	/** 法定代表人证件失效期限 */
	private Date LegalPersonIDEnd1;
	/** 负责人姓名 */
	private String ResponsibleName;
	/** 负责人证件类型 */
	private String ResponsibleIDType;
	/** 负责人证件号码 */
	private String ResponsibleIDNo;
	/** 负责人证件生效期限 */
	private Date ResponsibleIDStart;
	/** 负责人证件失效期限 */
	private Date ResponsibleIDEnd;
	/** 临时1 */
	private String Other1;
	/** 临时2 */
	private String Other2;
	/** 标记1 */
	private String Remark1;
	/** 标记2 */
	private String Remark2;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 40;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLRegisterExtSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "RgtNo";
		pk[1] = "GrpContno";
		pk[2] = "IDNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLRegisterExtSchema cloned = (LLRegisterExtSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getGrpContno()
	{
		return GrpContno;
	}
	public void setGrpContno(String aGrpContno)
	{
		GrpContno = aGrpContno;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getLinkMan()
	{
		return LinkMan;
	}
	public void setLinkMan(String aLinkMan)
	{
		LinkMan = aLinkMan;
	}
	public String getGrpAddress()
	{
		return GrpAddress;
	}
	public void setGrpAddress(String aGrpAddress)
	{
		GrpAddress = aGrpAddress;
	}
	public String getGrpIDName()
	{
		return GrpIDName;
	}
	public void setGrpIDName(String aGrpIDName)
	{
		GrpIDName = aGrpIDName;
	}
	public String getGrpIDName2()
	{
		return GrpIDName2;
	}
	public void setGrpIDName2(String aGrpIDName2)
	{
		GrpIDName2 = aGrpIDName2;
	}
	public String getGrpIDStartDate()
	{
		if( GrpIDStartDate != null )
			return fDate.getString(GrpIDStartDate);
		else
			return null;
	}
	public void setGrpIDStartDate(Date aGrpIDStartDate)
	{
		GrpIDStartDate = aGrpIDStartDate;
	}
	public void setGrpIDStartDate(String aGrpIDStartDate)
	{
		if (aGrpIDStartDate != null && !aGrpIDStartDate.equals("") )
		{
			GrpIDStartDate = fDate.getDate( aGrpIDStartDate );
		}
		else
			GrpIDStartDate = null;
	}

	public String getGrpIDEndDate()
	{
		if( GrpIDEndDate != null )
			return fDate.getString(GrpIDEndDate);
		else
			return null;
	}
	public void setGrpIDEndDate(Date aGrpIDEndDate)
	{
		GrpIDEndDate = aGrpIDEndDate;
	}
	public void setGrpIDEndDate(String aGrpIDEndDate)
	{
		if (aGrpIDEndDate != null && !aGrpIDEndDate.equals("") )
		{
			GrpIDEndDate = fDate.getDate( aGrpIDEndDate );
		}
		else
			GrpIDEndDate = null;
	}

	public String getUnifiedSocialCreditNo()
	{
		return UnifiedSocialCreditNo;
	}
	public void setUnifiedSocialCreditNo(String aUnifiedSocialCreditNo)
	{
		UnifiedSocialCreditNo = aUnifiedSocialCreditNo;
	}
	public String getOrgancomCode()
	{
		return OrgancomCode;
	}
	public void setOrgancomCode(String aOrgancomCode)
	{
		OrgancomCode = aOrgancomCode;
	}
	public String getTaxNo()
	{
		return TaxNo;
	}
	public void setTaxNo(String aTaxNo)
	{
		TaxNo = aTaxNo;
	}
	public String getBusinessScope()
	{
		return BusinessScope;
	}
	public void setBusinessScope(String aBusinessScope)
	{
		BusinessScope = aBusinessScope;
	}
	public String getShareholderName()
	{
		return ShareholderName;
	}
	public void setShareholderName(String aShareholderName)
	{
		ShareholderName = aShareholderName;
	}
	public String getShareholderIDType()
	{
		return ShareholderIDType;
	}
	public void setShareholderIDType(String aShareholderIDType)
	{
		ShareholderIDType = aShareholderIDType;
	}
	public String getShareholderIDNo()
	{
		return ShareholderIDNo;
	}
	public void setShareholderIDNo(String aShareholderIDNo)
	{
		ShareholderIDNo = aShareholderIDNo;
	}
	public String getShareholderIDStart()
	{
		if( ShareholderIDStart != null )
			return fDate.getString(ShareholderIDStart);
		else
			return null;
	}
	public void setShareholderIDStart(Date aShareholderIDStart)
	{
		ShareholderIDStart = aShareholderIDStart;
	}
	public void setShareholderIDStart(String aShareholderIDStart)
	{
		if (aShareholderIDStart != null && !aShareholderIDStart.equals("") )
		{
			ShareholderIDStart = fDate.getDate( aShareholderIDStart );
		}
		else
			ShareholderIDStart = null;
	}

	public String getShareholderIDEnd()
	{
		if( ShareholderIDEnd != null )
			return fDate.getString(ShareholderIDEnd);
		else
			return null;
	}
	public void setShareholderIDEnd(Date aShareholderIDEnd)
	{
		ShareholderIDEnd = aShareholderIDEnd;
	}
	public void setShareholderIDEnd(String aShareholderIDEnd)
	{
		if (aShareholderIDEnd != null && !aShareholderIDEnd.equals("") )
		{
			ShareholderIDEnd = fDate.getDate( aShareholderIDEnd );
		}
		else
			ShareholderIDEnd = null;
	}

	public String getLegalPersonName1()
	{
		return LegalPersonName1;
	}
	public void setLegalPersonName1(String aLegalPersonName1)
	{
		LegalPersonName1 = aLegalPersonName1;
	}
	public String getLegalPersonIDType1()
	{
		return LegalPersonIDType1;
	}
	public void setLegalPersonIDType1(String aLegalPersonIDType1)
	{
		LegalPersonIDType1 = aLegalPersonIDType1;
	}
	public String getLegalPersonIDNo1()
	{
		return LegalPersonIDNo1;
	}
	public void setLegalPersonIDNo1(String aLegalPersonIDNo1)
	{
		LegalPersonIDNo1 = aLegalPersonIDNo1;
	}
	public String getLegalPersonIDStart1()
	{
		if( LegalPersonIDStart1 != null )
			return fDate.getString(LegalPersonIDStart1);
		else
			return null;
	}
	public void setLegalPersonIDStart1(Date aLegalPersonIDStart1)
	{
		LegalPersonIDStart1 = aLegalPersonIDStart1;
	}
	public void setLegalPersonIDStart1(String aLegalPersonIDStart1)
	{
		if (aLegalPersonIDStart1 != null && !aLegalPersonIDStart1.equals("") )
		{
			LegalPersonIDStart1 = fDate.getDate( aLegalPersonIDStart1 );
		}
		else
			LegalPersonIDStart1 = null;
	}

	public String getLegalPersonIDEnd1()
	{
		if( LegalPersonIDEnd1 != null )
			return fDate.getString(LegalPersonIDEnd1);
		else
			return null;
	}
	public void setLegalPersonIDEnd1(Date aLegalPersonIDEnd1)
	{
		LegalPersonIDEnd1 = aLegalPersonIDEnd1;
	}
	public void setLegalPersonIDEnd1(String aLegalPersonIDEnd1)
	{
		if (aLegalPersonIDEnd1 != null && !aLegalPersonIDEnd1.equals("") )
		{
			LegalPersonIDEnd1 = fDate.getDate( aLegalPersonIDEnd1 );
		}
		else
			LegalPersonIDEnd1 = null;
	}

	public String getResponsibleName()
	{
		return ResponsibleName;
	}
	public void setResponsibleName(String aResponsibleName)
	{
		ResponsibleName = aResponsibleName;
	}
	public String getResponsibleIDType()
	{
		return ResponsibleIDType;
	}
	public void setResponsibleIDType(String aResponsibleIDType)
	{
		ResponsibleIDType = aResponsibleIDType;
	}
	public String getResponsibleIDNo()
	{
		return ResponsibleIDNo;
	}
	public void setResponsibleIDNo(String aResponsibleIDNo)
	{
		ResponsibleIDNo = aResponsibleIDNo;
	}
	public String getResponsibleIDStart()
	{
		if( ResponsibleIDStart != null )
			return fDate.getString(ResponsibleIDStart);
		else
			return null;
	}
	public void setResponsibleIDStart(Date aResponsibleIDStart)
	{
		ResponsibleIDStart = aResponsibleIDStart;
	}
	public void setResponsibleIDStart(String aResponsibleIDStart)
	{
		if (aResponsibleIDStart != null && !aResponsibleIDStart.equals("") )
		{
			ResponsibleIDStart = fDate.getDate( aResponsibleIDStart );
		}
		else
			ResponsibleIDStart = null;
	}

	public String getResponsibleIDEnd()
	{
		if( ResponsibleIDEnd != null )
			return fDate.getString(ResponsibleIDEnd);
		else
			return null;
	}
	public void setResponsibleIDEnd(Date aResponsibleIDEnd)
	{
		ResponsibleIDEnd = aResponsibleIDEnd;
	}
	public void setResponsibleIDEnd(String aResponsibleIDEnd)
	{
		if (aResponsibleIDEnd != null && !aResponsibleIDEnd.equals("") )
		{
			ResponsibleIDEnd = fDate.getDate( aResponsibleIDEnd );
		}
		else
			ResponsibleIDEnd = null;
	}

	public String getOther1()
	{
		return Other1;
	}
	public void setOther1(String aOther1)
	{
		Other1 = aOther1;
	}
	public String getOther2()
	{
		return Other2;
	}
	public void setOther2(String aOther2)
	{
		Other2 = aOther2;
	}
	public String getRemark1()
	{
		return Remark1;
	}
	public void setRemark1(String aRemark1)
	{
		Remark1 = aRemark1;
	}
	public String getRemark2()
	{
		return Remark2;
	}
	public void setRemark2(String aRemark2)
	{
		Remark2 = aRemark2;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLRegisterExtSchema 对象给 Schema 赋值
	* @param: aLLRegisterExtSchema LLRegisterExtSchema
	**/
	public void setSchema(LLRegisterExtSchema aLLRegisterExtSchema)
	{
		this.RgtNo = aLLRegisterExtSchema.getRgtNo();
		this.GrpContno = aLLRegisterExtSchema.getGrpContno();
		this.IDNo = aLLRegisterExtSchema.getIDNo();
		this.IDType = aLLRegisterExtSchema.getIDType();
		this.GrpName = aLLRegisterExtSchema.getGrpName();
		this.LinkMan = aLLRegisterExtSchema.getLinkMan();
		this.GrpAddress = aLLRegisterExtSchema.getGrpAddress();
		this.GrpIDName = aLLRegisterExtSchema.getGrpIDName();
		this.GrpIDName2 = aLLRegisterExtSchema.getGrpIDName2();
		this.GrpIDStartDate = fDate.getDate( aLLRegisterExtSchema.getGrpIDStartDate());
		this.GrpIDEndDate = fDate.getDate( aLLRegisterExtSchema.getGrpIDEndDate());
		this.UnifiedSocialCreditNo = aLLRegisterExtSchema.getUnifiedSocialCreditNo();
		this.OrgancomCode = aLLRegisterExtSchema.getOrgancomCode();
		this.TaxNo = aLLRegisterExtSchema.getTaxNo();
		this.BusinessScope = aLLRegisterExtSchema.getBusinessScope();
		this.ShareholderName = aLLRegisterExtSchema.getShareholderName();
		this.ShareholderIDType = aLLRegisterExtSchema.getShareholderIDType();
		this.ShareholderIDNo = aLLRegisterExtSchema.getShareholderIDNo();
		this.ShareholderIDStart = fDate.getDate( aLLRegisterExtSchema.getShareholderIDStart());
		this.ShareholderIDEnd = fDate.getDate( aLLRegisterExtSchema.getShareholderIDEnd());
		this.LegalPersonName1 = aLLRegisterExtSchema.getLegalPersonName1();
		this.LegalPersonIDType1 = aLLRegisterExtSchema.getLegalPersonIDType1();
		this.LegalPersonIDNo1 = aLLRegisterExtSchema.getLegalPersonIDNo1();
		this.LegalPersonIDStart1 = fDate.getDate( aLLRegisterExtSchema.getLegalPersonIDStart1());
		this.LegalPersonIDEnd1 = fDate.getDate( aLLRegisterExtSchema.getLegalPersonIDEnd1());
		this.ResponsibleName = aLLRegisterExtSchema.getResponsibleName();
		this.ResponsibleIDType = aLLRegisterExtSchema.getResponsibleIDType();
		this.ResponsibleIDNo = aLLRegisterExtSchema.getResponsibleIDNo();
		this.ResponsibleIDStart = fDate.getDate( aLLRegisterExtSchema.getResponsibleIDStart());
		this.ResponsibleIDEnd = fDate.getDate( aLLRegisterExtSchema.getResponsibleIDEnd());
		this.Other1 = aLLRegisterExtSchema.getOther1();
		this.Other2 = aLLRegisterExtSchema.getOther2();
		this.Remark1 = aLLRegisterExtSchema.getRemark1();
		this.Remark2 = aLLRegisterExtSchema.getRemark2();
		this.MngCom = aLLRegisterExtSchema.getMngCom();
		this.Operator = aLLRegisterExtSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLRegisterExtSchema.getMakeDate());
		this.MakeTime = aLLRegisterExtSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLRegisterExtSchema.getModifyDate());
		this.ModifyTime = aLLRegisterExtSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("GrpContno") == null )
				this.GrpContno = null;
			else
				this.GrpContno = rs.getString("GrpContno").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("LinkMan") == null )
				this.LinkMan = null;
			else
				this.LinkMan = rs.getString("LinkMan").trim();

			if( rs.getString("GrpAddress") == null )
				this.GrpAddress = null;
			else
				this.GrpAddress = rs.getString("GrpAddress").trim();

			if( rs.getString("GrpIDName") == null )
				this.GrpIDName = null;
			else
				this.GrpIDName = rs.getString("GrpIDName").trim();

			if( rs.getString("GrpIDName2") == null )
				this.GrpIDName2 = null;
			else
				this.GrpIDName2 = rs.getString("GrpIDName2").trim();

			this.GrpIDStartDate = rs.getDate("GrpIDStartDate");
			this.GrpIDEndDate = rs.getDate("GrpIDEndDate");
			if( rs.getString("UnifiedSocialCreditNo") == null )
				this.UnifiedSocialCreditNo = null;
			else
				this.UnifiedSocialCreditNo = rs.getString("UnifiedSocialCreditNo").trim();

			if( rs.getString("OrgancomCode") == null )
				this.OrgancomCode = null;
			else
				this.OrgancomCode = rs.getString("OrgancomCode").trim();

			if( rs.getString("TaxNo") == null )
				this.TaxNo = null;
			else
				this.TaxNo = rs.getString("TaxNo").trim();

			if( rs.getString("BusinessScope") == null )
				this.BusinessScope = null;
			else
				this.BusinessScope = rs.getString("BusinessScope").trim();

			if( rs.getString("ShareholderName") == null )
				this.ShareholderName = null;
			else
				this.ShareholderName = rs.getString("ShareholderName").trim();

			if( rs.getString("ShareholderIDType") == null )
				this.ShareholderIDType = null;
			else
				this.ShareholderIDType = rs.getString("ShareholderIDType").trim();

			if( rs.getString("ShareholderIDNo") == null )
				this.ShareholderIDNo = null;
			else
				this.ShareholderIDNo = rs.getString("ShareholderIDNo").trim();

			this.ShareholderIDStart = rs.getDate("ShareholderIDStart");
			this.ShareholderIDEnd = rs.getDate("ShareholderIDEnd");
			if( rs.getString("LegalPersonName1") == null )
				this.LegalPersonName1 = null;
			else
				this.LegalPersonName1 = rs.getString("LegalPersonName1").trim();

			if( rs.getString("LegalPersonIDType1") == null )
				this.LegalPersonIDType1 = null;
			else
				this.LegalPersonIDType1 = rs.getString("LegalPersonIDType1").trim();

			if( rs.getString("LegalPersonIDNo1") == null )
				this.LegalPersonIDNo1 = null;
			else
				this.LegalPersonIDNo1 = rs.getString("LegalPersonIDNo1").trim();

			this.LegalPersonIDStart1 = rs.getDate("LegalPersonIDStart1");
			this.LegalPersonIDEnd1 = rs.getDate("LegalPersonIDEnd1");
			if( rs.getString("ResponsibleName") == null )
				this.ResponsibleName = null;
			else
				this.ResponsibleName = rs.getString("ResponsibleName").trim();

			if( rs.getString("ResponsibleIDType") == null )
				this.ResponsibleIDType = null;
			else
				this.ResponsibleIDType = rs.getString("ResponsibleIDType").trim();

			if( rs.getString("ResponsibleIDNo") == null )
				this.ResponsibleIDNo = null;
			else
				this.ResponsibleIDNo = rs.getString("ResponsibleIDNo").trim();

			this.ResponsibleIDStart = rs.getDate("ResponsibleIDStart");
			this.ResponsibleIDEnd = rs.getDate("ResponsibleIDEnd");
			if( rs.getString("Other1") == null )
				this.Other1 = null;
			else
				this.Other1 = rs.getString("Other1").trim();

			if( rs.getString("Other2") == null )
				this.Other2 = null;
			else
				this.Other2 = rs.getString("Other2").trim();

			if( rs.getString("Remark1") == null )
				this.Remark1 = null;
			else
				this.Remark1 = rs.getString("Remark1").trim();

			if( rs.getString("Remark2") == null )
				this.Remark2 = null;
			else
				this.Remark2 = rs.getString("Remark2").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLRegisterExt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLRegisterExtSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLRegisterExtSchema getSchema()
	{
		LLRegisterExtSchema aLLRegisterExtSchema = new LLRegisterExtSchema();
		aLLRegisterExtSchema.setSchema(this);
		return aLLRegisterExtSchema;
	}

	public LLRegisterExtDB getDB()
	{
		LLRegisterExtDB aDBOper = new LLRegisterExtDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLRegisterExt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LinkMan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpIDName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpIDName2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GrpIDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GrpIDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnifiedSocialCreditNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrgancomCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessScope)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShareholderName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShareholderIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShareholderIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ShareholderIDStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ShareholderIDEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonName1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonIDType1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonIDNo1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LegalPersonIDStart1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LegalPersonIDEnd1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponsibleName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponsibleIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponsibleIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ResponsibleIDStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ResponsibleIDEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Other2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLRegisterExt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			GrpContno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			LinkMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			GrpAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			GrpIDName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			GrpIDName2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			GrpIDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			GrpIDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			UnifiedSocialCreditNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			OrgancomCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			TaxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			BusinessScope = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ShareholderName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ShareholderIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ShareholderIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ShareholderIDStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ShareholderIDEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			LegalPersonName1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			LegalPersonIDType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			LegalPersonIDNo1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			LegalPersonIDStart1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			LegalPersonIDEnd1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ResponsibleName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ResponsibleIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ResponsibleIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			ResponsibleIDStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			ResponsibleIDEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			Other1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			Other2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLRegisterExtSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("GrpContno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContno));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("LinkMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan));
		}
		if (FCode.equals("GrpAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAddress));
		}
		if (FCode.equals("GrpIDName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpIDName));
		}
		if (FCode.equals("GrpIDName2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpIDName2));
		}
		if (FCode.equals("GrpIDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGrpIDStartDate()));
		}
		if (FCode.equals("GrpIDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGrpIDEndDate()));
		}
		if (FCode.equals("UnifiedSocialCreditNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnifiedSocialCreditNo));
		}
		if (FCode.equals("OrgancomCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrgancomCode));
		}
		if (FCode.equals("TaxNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxNo));
		}
		if (FCode.equals("BusinessScope"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessScope));
		}
		if (FCode.equals("ShareholderName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShareholderName));
		}
		if (FCode.equals("ShareholderIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShareholderIDType));
		}
		if (FCode.equals("ShareholderIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShareholderIDNo));
		}
		if (FCode.equals("ShareholderIDStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getShareholderIDStart()));
		}
		if (FCode.equals("ShareholderIDEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getShareholderIDEnd()));
		}
		if (FCode.equals("LegalPersonName1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonName1));
		}
		if (FCode.equals("LegalPersonIDType1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonIDType1));
		}
		if (FCode.equals("LegalPersonIDNo1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonIDNo1));
		}
		if (FCode.equals("LegalPersonIDStart1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLegalPersonIDStart1()));
		}
		if (FCode.equals("LegalPersonIDEnd1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLegalPersonIDEnd1()));
		}
		if (FCode.equals("ResponsibleName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponsibleName));
		}
		if (FCode.equals("ResponsibleIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponsibleIDType));
		}
		if (FCode.equals("ResponsibleIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponsibleIDNo));
		}
		if (FCode.equals("ResponsibleIDStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getResponsibleIDStart()));
		}
		if (FCode.equals("ResponsibleIDEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getResponsibleIDEnd()));
		}
		if (FCode.equals("Other1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other1));
		}
		if (FCode.equals("Other2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Other2));
		}
		if (FCode.equals("Remark1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
		}
		if (FCode.equals("Remark2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(GrpContno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(LinkMan);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(GrpAddress);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(GrpIDName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(GrpIDName2);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGrpIDStartDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGrpIDEndDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(UnifiedSocialCreditNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(OrgancomCode);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(TaxNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(BusinessScope);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ShareholderName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ShareholderIDType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ShareholderIDNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getShareholderIDStart()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getShareholderIDEnd()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonName1);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonIDType1);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonIDNo1);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLegalPersonIDStart1()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLegalPersonIDEnd1()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ResponsibleName);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ResponsibleIDType);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(ResponsibleIDNo);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getResponsibleIDStart()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getResponsibleIDEnd()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(Other1);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(Other2);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Remark1);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Remark2);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContno = FValue.trim();
			}
			else
				GrpContno = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("LinkMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LinkMan = FValue.trim();
			}
			else
				LinkMan = null;
		}
		if (FCode.equalsIgnoreCase("GrpAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAddress = FValue.trim();
			}
			else
				GrpAddress = null;
		}
		if (FCode.equalsIgnoreCase("GrpIDName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpIDName = FValue.trim();
			}
			else
				GrpIDName = null;
		}
		if (FCode.equalsIgnoreCase("GrpIDName2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpIDName2 = FValue.trim();
			}
			else
				GrpIDName2 = null;
		}
		if (FCode.equalsIgnoreCase("GrpIDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GrpIDStartDate = fDate.getDate( FValue );
			}
			else
				GrpIDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("GrpIDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GrpIDEndDate = fDate.getDate( FValue );
			}
			else
				GrpIDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("UnifiedSocialCreditNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnifiedSocialCreditNo = FValue.trim();
			}
			else
				UnifiedSocialCreditNo = null;
		}
		if (FCode.equalsIgnoreCase("OrgancomCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrgancomCode = FValue.trim();
			}
			else
				OrgancomCode = null;
		}
		if (FCode.equalsIgnoreCase("TaxNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxNo = FValue.trim();
			}
			else
				TaxNo = null;
		}
		if (FCode.equalsIgnoreCase("BusinessScope"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessScope = FValue.trim();
			}
			else
				BusinessScope = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShareholderName = FValue.trim();
			}
			else
				ShareholderName = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShareholderIDType = FValue.trim();
			}
			else
				ShareholderIDType = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShareholderIDNo = FValue.trim();
			}
			else
				ShareholderIDNo = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ShareholderIDStart = fDate.getDate( FValue );
			}
			else
				ShareholderIDStart = null;
		}
		if (FCode.equalsIgnoreCase("ShareholderIDEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ShareholderIDEnd = fDate.getDate( FValue );
			}
			else
				ShareholderIDEnd = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonName1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonName1 = FValue.trim();
			}
			else
				LegalPersonName1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDType1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonIDType1 = FValue.trim();
			}
			else
				LegalPersonIDType1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDNo1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonIDNo1 = FValue.trim();
			}
			else
				LegalPersonIDNo1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDStart1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LegalPersonIDStart1 = fDate.getDate( FValue );
			}
			else
				LegalPersonIDStart1 = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDEnd1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LegalPersonIDEnd1 = fDate.getDate( FValue );
			}
			else
				LegalPersonIDEnd1 = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponsibleName = FValue.trim();
			}
			else
				ResponsibleName = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponsibleIDType = FValue.trim();
			}
			else
				ResponsibleIDType = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponsibleIDNo = FValue.trim();
			}
			else
				ResponsibleIDNo = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ResponsibleIDStart = fDate.getDate( FValue );
			}
			else
				ResponsibleIDStart = null;
		}
		if (FCode.equalsIgnoreCase("ResponsibleIDEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ResponsibleIDEnd = fDate.getDate( FValue );
			}
			else
				ResponsibleIDEnd = null;
		}
		if (FCode.equalsIgnoreCase("Other1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other1 = FValue.trim();
			}
			else
				Other1 = null;
		}
		if (FCode.equalsIgnoreCase("Other2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Other2 = FValue.trim();
			}
			else
				Other2 = null;
		}
		if (FCode.equalsIgnoreCase("Remark1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark1 = FValue.trim();
			}
			else
				Remark1 = null;
		}
		if (FCode.equalsIgnoreCase("Remark2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark2 = FValue.trim();
			}
			else
				Remark2 = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLRegisterExtSchema other = (LLRegisterExtSchema)otherObject;
		return
			(RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (GrpContno == null ? other.getGrpContno() == null : GrpContno.equals(other.getGrpContno()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (LinkMan == null ? other.getLinkMan() == null : LinkMan.equals(other.getLinkMan()))
			&& (GrpAddress == null ? other.getGrpAddress() == null : GrpAddress.equals(other.getGrpAddress()))
			&& (GrpIDName == null ? other.getGrpIDName() == null : GrpIDName.equals(other.getGrpIDName()))
			&& (GrpIDName2 == null ? other.getGrpIDName2() == null : GrpIDName2.equals(other.getGrpIDName2()))
			&& (GrpIDStartDate == null ? other.getGrpIDStartDate() == null : fDate.getString(GrpIDStartDate).equals(other.getGrpIDStartDate()))
			&& (GrpIDEndDate == null ? other.getGrpIDEndDate() == null : fDate.getString(GrpIDEndDate).equals(other.getGrpIDEndDate()))
			&& (UnifiedSocialCreditNo == null ? other.getUnifiedSocialCreditNo() == null : UnifiedSocialCreditNo.equals(other.getUnifiedSocialCreditNo()))
			&& (OrgancomCode == null ? other.getOrgancomCode() == null : OrgancomCode.equals(other.getOrgancomCode()))
			&& (TaxNo == null ? other.getTaxNo() == null : TaxNo.equals(other.getTaxNo()))
			&& (BusinessScope == null ? other.getBusinessScope() == null : BusinessScope.equals(other.getBusinessScope()))
			&& (ShareholderName == null ? other.getShareholderName() == null : ShareholderName.equals(other.getShareholderName()))
			&& (ShareholderIDType == null ? other.getShareholderIDType() == null : ShareholderIDType.equals(other.getShareholderIDType()))
			&& (ShareholderIDNo == null ? other.getShareholderIDNo() == null : ShareholderIDNo.equals(other.getShareholderIDNo()))
			&& (ShareholderIDStart == null ? other.getShareholderIDStart() == null : fDate.getString(ShareholderIDStart).equals(other.getShareholderIDStart()))
			&& (ShareholderIDEnd == null ? other.getShareholderIDEnd() == null : fDate.getString(ShareholderIDEnd).equals(other.getShareholderIDEnd()))
			&& (LegalPersonName1 == null ? other.getLegalPersonName1() == null : LegalPersonName1.equals(other.getLegalPersonName1()))
			&& (LegalPersonIDType1 == null ? other.getLegalPersonIDType1() == null : LegalPersonIDType1.equals(other.getLegalPersonIDType1()))
			&& (LegalPersonIDNo1 == null ? other.getLegalPersonIDNo1() == null : LegalPersonIDNo1.equals(other.getLegalPersonIDNo1()))
			&& (LegalPersonIDStart1 == null ? other.getLegalPersonIDStart1() == null : fDate.getString(LegalPersonIDStart1).equals(other.getLegalPersonIDStart1()))
			&& (LegalPersonIDEnd1 == null ? other.getLegalPersonIDEnd1() == null : fDate.getString(LegalPersonIDEnd1).equals(other.getLegalPersonIDEnd1()))
			&& (ResponsibleName == null ? other.getResponsibleName() == null : ResponsibleName.equals(other.getResponsibleName()))
			&& (ResponsibleIDType == null ? other.getResponsibleIDType() == null : ResponsibleIDType.equals(other.getResponsibleIDType()))
			&& (ResponsibleIDNo == null ? other.getResponsibleIDNo() == null : ResponsibleIDNo.equals(other.getResponsibleIDNo()))
			&& (ResponsibleIDStart == null ? other.getResponsibleIDStart() == null : fDate.getString(ResponsibleIDStart).equals(other.getResponsibleIDStart()))
			&& (ResponsibleIDEnd == null ? other.getResponsibleIDEnd() == null : fDate.getString(ResponsibleIDEnd).equals(other.getResponsibleIDEnd()))
			&& (Other1 == null ? other.getOther1() == null : Other1.equals(other.getOther1()))
			&& (Other2 == null ? other.getOther2() == null : Other2.equals(other.getOther2()))
			&& (Remark1 == null ? other.getRemark1() == null : Remark1.equals(other.getRemark1()))
			&& (Remark2 == null ? other.getRemark2() == null : Remark2.equals(other.getRemark2()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RgtNo") ) {
			return 0;
		}
		if( strFieldName.equals("GrpContno") ) {
			return 1;
		}
		if( strFieldName.equals("IDNo") ) {
			return 2;
		}
		if( strFieldName.equals("IDType") ) {
			return 3;
		}
		if( strFieldName.equals("GrpName") ) {
			return 4;
		}
		if( strFieldName.equals("LinkMan") ) {
			return 5;
		}
		if( strFieldName.equals("GrpAddress") ) {
			return 6;
		}
		if( strFieldName.equals("GrpIDName") ) {
			return 7;
		}
		if( strFieldName.equals("GrpIDName2") ) {
			return 8;
		}
		if( strFieldName.equals("GrpIDStartDate") ) {
			return 9;
		}
		if( strFieldName.equals("GrpIDEndDate") ) {
			return 10;
		}
		if( strFieldName.equals("UnifiedSocialCreditNo") ) {
			return 11;
		}
		if( strFieldName.equals("OrgancomCode") ) {
			return 12;
		}
		if( strFieldName.equals("TaxNo") ) {
			return 13;
		}
		if( strFieldName.equals("BusinessScope") ) {
			return 14;
		}
		if( strFieldName.equals("ShareholderName") ) {
			return 15;
		}
		if( strFieldName.equals("ShareholderIDType") ) {
			return 16;
		}
		if( strFieldName.equals("ShareholderIDNo") ) {
			return 17;
		}
		if( strFieldName.equals("ShareholderIDStart") ) {
			return 18;
		}
		if( strFieldName.equals("ShareholderIDEnd") ) {
			return 19;
		}
		if( strFieldName.equals("LegalPersonName1") ) {
			return 20;
		}
		if( strFieldName.equals("LegalPersonIDType1") ) {
			return 21;
		}
		if( strFieldName.equals("LegalPersonIDNo1") ) {
			return 22;
		}
		if( strFieldName.equals("LegalPersonIDStart1") ) {
			return 23;
		}
		if( strFieldName.equals("LegalPersonIDEnd1") ) {
			return 24;
		}
		if( strFieldName.equals("ResponsibleName") ) {
			return 25;
		}
		if( strFieldName.equals("ResponsibleIDType") ) {
			return 26;
		}
		if( strFieldName.equals("ResponsibleIDNo") ) {
			return 27;
		}
		if( strFieldName.equals("ResponsibleIDStart") ) {
			return 28;
		}
		if( strFieldName.equals("ResponsibleIDEnd") ) {
			return 29;
		}
		if( strFieldName.equals("Other1") ) {
			return 30;
		}
		if( strFieldName.equals("Other2") ) {
			return 31;
		}
		if( strFieldName.equals("Remark1") ) {
			return 32;
		}
		if( strFieldName.equals("Remark2") ) {
			return 33;
		}
		if( strFieldName.equals("MngCom") ) {
			return 34;
		}
		if( strFieldName.equals("Operator") ) {
			return 35;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 36;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 37;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 38;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 39;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RgtNo";
				break;
			case 1:
				strFieldName = "GrpContno";
				break;
			case 2:
				strFieldName = "IDNo";
				break;
			case 3:
				strFieldName = "IDType";
				break;
			case 4:
				strFieldName = "GrpName";
				break;
			case 5:
				strFieldName = "LinkMan";
				break;
			case 6:
				strFieldName = "GrpAddress";
				break;
			case 7:
				strFieldName = "GrpIDName";
				break;
			case 8:
				strFieldName = "GrpIDName2";
				break;
			case 9:
				strFieldName = "GrpIDStartDate";
				break;
			case 10:
				strFieldName = "GrpIDEndDate";
				break;
			case 11:
				strFieldName = "UnifiedSocialCreditNo";
				break;
			case 12:
				strFieldName = "OrgancomCode";
				break;
			case 13:
				strFieldName = "TaxNo";
				break;
			case 14:
				strFieldName = "BusinessScope";
				break;
			case 15:
				strFieldName = "ShareholderName";
				break;
			case 16:
				strFieldName = "ShareholderIDType";
				break;
			case 17:
				strFieldName = "ShareholderIDNo";
				break;
			case 18:
				strFieldName = "ShareholderIDStart";
				break;
			case 19:
				strFieldName = "ShareholderIDEnd";
				break;
			case 20:
				strFieldName = "LegalPersonName1";
				break;
			case 21:
				strFieldName = "LegalPersonIDType1";
				break;
			case 22:
				strFieldName = "LegalPersonIDNo1";
				break;
			case 23:
				strFieldName = "LegalPersonIDStart1";
				break;
			case 24:
				strFieldName = "LegalPersonIDEnd1";
				break;
			case 25:
				strFieldName = "ResponsibleName";
				break;
			case 26:
				strFieldName = "ResponsibleIDType";
				break;
			case 27:
				strFieldName = "ResponsibleIDNo";
				break;
			case 28:
				strFieldName = "ResponsibleIDStart";
				break;
			case 29:
				strFieldName = "ResponsibleIDEnd";
				break;
			case 30:
				strFieldName = "Other1";
				break;
			case 31:
				strFieldName = "Other2";
				break;
			case 32:
				strFieldName = "Remark1";
				break;
			case 33:
				strFieldName = "Remark2";
				break;
			case 34:
				strFieldName = "MngCom";
				break;
			case 35:
				strFieldName = "Operator";
				break;
			case 36:
				strFieldName = "MakeDate";
				break;
			case 37:
				strFieldName = "MakeTime";
				break;
			case 38:
				strFieldName = "ModifyDate";
				break;
			case 39:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LinkMan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpIDName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpIDName2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpIDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GrpIDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("UnifiedSocialCreditNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrgancomCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessScope") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareholderName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareholderIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareholderIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShareholderIDStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ShareholderIDEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LegalPersonName1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDType1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDNo1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDStart1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LegalPersonIDEnd1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ResponsibleName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponsibleIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponsibleIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponsibleIDStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ResponsibleIDEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Other1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Other2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
