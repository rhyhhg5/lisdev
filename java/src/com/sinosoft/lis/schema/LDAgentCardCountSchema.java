/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDAgentCardCountDB;

/*
 * <p>ClassName: LDAgentCardCountSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 20061009
 * @CreateDate：2006-10-11
 */
public class LDAgentCardCountSchema implements Schema, Cloneable {
    // @Field
    /** Agentcode */
    private String AgentCode;
    /** Certifycode */
    private String CertifyCode;
    /** Maxcount */
    private int MaxCount;
    /** Managecom */
    private String ManageCom;
    /** Agentgrade */
    private String AgentGrade;
    /** 代理机构 */
    private String AgentCom;
    /** 回销期 */
    private int VerPeriod;
    /** 回销期单位 */
    private String VerPeriodFlag;
    /** 发放人 */
    private String SendOutCom;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDAgentCardCountSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "AgentCode";
        pk[1] = "CertifyCode";
        pk[2] = "ManageCom";
        pk[3] = "AgentGrade";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDAgentCardCountSchema cloned = (LDAgentCardCountSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getCertifyCode() {
        return CertifyCode;
    }

    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }

    public int getMaxCount() {
        return MaxCount;
    }

    public void setMaxCount(int aMaxCount) {
        MaxCount = aMaxCount;
    }

    public void setMaxCount(String aMaxCount) {
        if (aMaxCount != null && !aMaxCount.equals("")) {
            Integer tInteger = new Integer(aMaxCount);
            int i = tInteger.intValue();
            MaxCount = i;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getAgentGrade() {
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade) {
        AgentGrade = aAgentGrade;
    }

    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }

    public int getVerPeriod() {
        return VerPeriod;
    }

    public void setVerPeriod(int aVerPeriod) {
        VerPeriod = aVerPeriod;
    }

    public void setVerPeriod(String aVerPeriod) {
        if (aVerPeriod != null && !aVerPeriod.equals("")) {
            Integer tInteger = new Integer(aVerPeriod);
            int i = tInteger.intValue();
            VerPeriod = i;
        }
    }

    public String getVerPeriodFlag() {
        return VerPeriodFlag;
    }

    public void setVerPeriodFlag(String aVerPeriodFlag) {
        VerPeriodFlag = aVerPeriodFlag;
    }

    public String getSendOutCom() {
        return SendOutCom;
    }

    public void setSendOutCom(String aSendOutCom) {
        SendOutCom = aSendOutCom;
    }

    /**
     * 使用另外一个 LDAgentCardCountSchema 对象给 Schema 赋值
     * @param: aLDAgentCardCountSchema LDAgentCardCountSchema
     **/
    public void setSchema(LDAgentCardCountSchema aLDAgentCardCountSchema) {
        this.AgentCode = aLDAgentCardCountSchema.getAgentCode();
        this.CertifyCode = aLDAgentCardCountSchema.getCertifyCode();
        this.MaxCount = aLDAgentCardCountSchema.getMaxCount();
        this.ManageCom = aLDAgentCardCountSchema.getManageCom();
        this.AgentGrade = aLDAgentCardCountSchema.getAgentGrade();
        this.AgentCom = aLDAgentCardCountSchema.getAgentCom();
        this.VerPeriod = aLDAgentCardCountSchema.getVerPeriod();
        this.VerPeriodFlag = aLDAgentCardCountSchema.getVerPeriodFlag();
        this.SendOutCom = aLDAgentCardCountSchema.getSendOutCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("CertifyCode") == null) {
                this.CertifyCode = null;
            } else {
                this.CertifyCode = rs.getString("CertifyCode").trim();
            }

            this.MaxCount = rs.getInt("MaxCount");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentGrade") == null) {
                this.AgentGrade = null;
            } else {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("AgentCom") == null) {
                this.AgentCom = null;
            } else {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            this.VerPeriod = rs.getInt("VerPeriod");
            if (rs.getString("VerPeriodFlag") == null) {
                this.VerPeriodFlag = null;
            } else {
                this.VerPeriodFlag = rs.getString("VerPeriodFlag").trim();
            }

            if (rs.getString("SendOutCom") == null) {
                this.SendOutCom = null;
            } else {
                this.SendOutCom = rs.getString("SendOutCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDAgentCardCount表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDAgentCardCountSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDAgentCardCountSchema getSchema() {
        LDAgentCardCountSchema aLDAgentCardCountSchema = new
                LDAgentCardCountSchema();
        aLDAgentCardCountSchema.setSchema(this);
        return aLDAgentCardCountSchema;
    }

    public LDAgentCardCountDB getDB() {
        LDAgentCardCountDB aDBOper = new LDAgentCardCountDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDAgentCardCount描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CertifyCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MaxCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGrade));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(VerPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VerPeriodFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SendOutCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDAgentCardCount>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            MaxCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            VerPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            VerPeriodFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            SendOutCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDAgentCardCountSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equals("MaxCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxCount));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("AgentGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equals("VerPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VerPeriod));
        }
        if (FCode.equals("VerPeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VerPeriodFlag));
        }
        if (FCode.equals("SendOutCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendOutCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CertifyCode);
            break;
        case 2:
            strFieldValue = String.valueOf(MaxCount);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(AgentGrade);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(AgentCom);
            break;
        case 6:
            strFieldValue = String.valueOf(VerPeriod);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(VerPeriodFlag);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(SendOutCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if (FValue != null && !FValue.equals("")) {
                CertifyCode = FValue.trim();
            } else {
                CertifyCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("MaxCount")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MaxCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentGrade")) {
            if (FValue != null && !FValue.equals("")) {
                AgentGrade = FValue.trim();
            } else {
                AgentGrade = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCom = FValue.trim();
            } else {
                AgentCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("VerPeriod")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                VerPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("VerPeriodFlag")) {
            if (FValue != null && !FValue.equals("")) {
                VerPeriodFlag = FValue.trim();
            } else {
                VerPeriodFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("SendOutCom")) {
            if (FValue != null && !FValue.equals("")) {
                SendOutCom = FValue.trim();
            } else {
                SendOutCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDAgentCardCountSchema other = (LDAgentCardCountSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && CertifyCode.equals(other.getCertifyCode())
                && MaxCount == other.getMaxCount()
                && ManageCom.equals(other.getManageCom())
                && AgentGrade.equals(other.getAgentGrade())
                && AgentCom.equals(other.getAgentCom())
                && VerPeriod == other.getVerPeriod()
                && VerPeriodFlag.equals(other.getVerPeriodFlag())
                && SendOutCom.equals(other.getSendOutCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("AgentCode")) {
            return 0;
        }
        if (strFieldName.equals("CertifyCode")) {
            return 1;
        }
        if (strFieldName.equals("MaxCount")) {
            return 2;
        }
        if (strFieldName.equals("ManageCom")) {
            return 3;
        }
        if (strFieldName.equals("AgentGrade")) {
            return 4;
        }
        if (strFieldName.equals("AgentCom")) {
            return 5;
        }
        if (strFieldName.equals("VerPeriod")) {
            return 6;
        }
        if (strFieldName.equals("VerPeriodFlag")) {
            return 7;
        }
        if (strFieldName.equals("SendOutCom")) {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "AgentCode";
            break;
        case 1:
            strFieldName = "CertifyCode";
            break;
        case 2:
            strFieldName = "MaxCount";
            break;
        case 3:
            strFieldName = "ManageCom";
            break;
        case 4:
            strFieldName = "AgentGrade";
            break;
        case 5:
            strFieldName = "AgentCom";
            break;
        case 6:
            strFieldName = "VerPeriod";
            break;
        case 7:
            strFieldName = "VerPeriodFlag";
            break;
        case 8:
            strFieldName = "SendOutCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifyCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MaxCount")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("VerPeriod")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("VerPeriodFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SendOutCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_INT;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_INT;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
