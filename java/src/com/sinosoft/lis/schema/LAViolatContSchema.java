/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAViolatContDB;

/*
 * <p>ClassName: LAViolatContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新表结构
 * @CreateDate：2006-01-17
 */
public class LAViolatContSchema implements Schema, Cloneable {
    // @Field
    /** 违规立案编号 */
    private String ViolatNo;
    /** 违规立案日期 */
    private Date ViolatDate;
    /** 印刷号 */
    private String PrtNo;
    /** 保险单号 */
    private String ContNo;
    /** 案件处理状态 */
    private String State;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 管理机构 */
    private String ManageCom;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 被保人客户号1 */
    private String InsuredNo1;
    /** 被保人客户号2 */
    private String InsuredNo2;
    /** 被保人客户号3 */
    private String InsuredNo3;
    /** 直接损失 */
    private double DirectLoss;
    /** 追回损失 */
    private double ReplevyLoss;
    /** 结案日期 */
    private Date ViolatEndDate;
    /** 处理意见 */
    private String DealMind;
    /** 展业类型 */
    private String BranchType;
    /** 渠道 */
    private String BranchType2;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最近一次修改日期 */
    private Date ModifyDate;
    /** 最近一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 23; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAViolatContSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ViolatNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAViolatContSchema cloned = (LAViolatContSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getViolatNo() {
        return ViolatNo;
    }

    public void setViolatNo(String aViolatNo) {
        ViolatNo = aViolatNo;
    }

    public String getViolatDate() {
        if (ViolatDate != null) {
            return fDate.getString(ViolatDate);
        } else {
            return null;
        }
    }

    public void setViolatDate(Date aViolatDate) {
        ViolatDate = aViolatDate;
    }

    public void setViolatDate(String aViolatDate) {
        if (aViolatDate != null && !aViolatDate.equals("")) {
            ViolatDate = fDate.getDate(aViolatDate);
        } else {
            ViolatDate = null;
        }
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }

    public String getInsuredNo1() {
        return InsuredNo1;
    }

    public void setInsuredNo1(String aInsuredNo1) {
        InsuredNo1 = aInsuredNo1;
    }

    public String getInsuredNo2() {
        return InsuredNo2;
    }

    public void setInsuredNo2(String aInsuredNo2) {
        InsuredNo2 = aInsuredNo2;
    }

    public String getInsuredNo3() {
        return InsuredNo3;
    }

    public void setInsuredNo3(String aInsuredNo3) {
        InsuredNo3 = aInsuredNo3;
    }

    public double getDirectLoss() {
        return DirectLoss;
    }

    public void setDirectLoss(double aDirectLoss) {
        DirectLoss = Arith.round(aDirectLoss, 2);
    }

    public void setDirectLoss(String aDirectLoss) {
        if (aDirectLoss != null && !aDirectLoss.equals("")) {
            Double tDouble = new Double(aDirectLoss);
            double d = tDouble.doubleValue();
            DirectLoss = Arith.round(d, 2);
        }
    }

    public double getReplevyLoss() {
        return ReplevyLoss;
    }

    public void setReplevyLoss(double aReplevyLoss) {
        ReplevyLoss = Arith.round(aReplevyLoss, 2);
    }

    public void setReplevyLoss(String aReplevyLoss) {
        if (aReplevyLoss != null && !aReplevyLoss.equals("")) {
            Double tDouble = new Double(aReplevyLoss);
            double d = tDouble.doubleValue();
            ReplevyLoss = Arith.round(d, 2);
        }
    }

    public String getViolatEndDate() {
        if (ViolatEndDate != null) {
            return fDate.getString(ViolatEndDate);
        } else {
            return null;
        }
    }

    public void setViolatEndDate(Date aViolatEndDate) {
        ViolatEndDate = aViolatEndDate;
    }

    public void setViolatEndDate(String aViolatEndDate) {
        if (aViolatEndDate != null && !aViolatEndDate.equals("")) {
            ViolatEndDate = fDate.getDate(aViolatEndDate);
        } else {
            ViolatEndDate = null;
        }
    }

    public String getDealMind() {
        return DealMind;
    }

    public void setDealMind(String aDealMind) {
        DealMind = aDealMind;
    }

    public String getBranchType() {
        return BranchType;
    }

    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }

    public String getBranchType2() {
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAViolatContSchema 对象给 Schema 赋值
     * @param: aLAViolatContSchema LAViolatContSchema
     **/
    public void setSchema(LAViolatContSchema aLAViolatContSchema) {
        this.ViolatNo = aLAViolatContSchema.getViolatNo();
        this.ViolatDate = fDate.getDate(aLAViolatContSchema.getViolatDate());
        this.PrtNo = aLAViolatContSchema.getPrtNo();
        this.ContNo = aLAViolatContSchema.getContNo();
        this.State = aLAViolatContSchema.getState();
        this.AgentCode = aLAViolatContSchema.getAgentCode();
        this.AgentGroup = aLAViolatContSchema.getAgentGroup();
        this.ManageCom = aLAViolatContSchema.getManageCom();
        this.AppntNo = aLAViolatContSchema.getAppntNo();
        this.InsuredNo1 = aLAViolatContSchema.getInsuredNo1();
        this.InsuredNo2 = aLAViolatContSchema.getInsuredNo2();
        this.InsuredNo3 = aLAViolatContSchema.getInsuredNo3();
        this.DirectLoss = aLAViolatContSchema.getDirectLoss();
        this.ReplevyLoss = aLAViolatContSchema.getReplevyLoss();
        this.ViolatEndDate = fDate.getDate(aLAViolatContSchema.getViolatEndDate());
        this.DealMind = aLAViolatContSchema.getDealMind();
        this.BranchType = aLAViolatContSchema.getBranchType();
        this.BranchType2 = aLAViolatContSchema.getBranchType2();
        this.Operator = aLAViolatContSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAViolatContSchema.getMakeDate());
        this.MakeTime = aLAViolatContSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAViolatContSchema.getModifyDate());
        this.ModifyTime = aLAViolatContSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ViolatNo") == null) {
                this.ViolatNo = null;
            } else {
                this.ViolatNo = rs.getString("ViolatNo").trim();
            }

            this.ViolatDate = rs.getDate("ViolatDate");
            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null) {
                this.AgentGroup = null;
            } else {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AppntNo") == null) {
                this.AppntNo = null;
            } else {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("InsuredNo1") == null) {
                this.InsuredNo1 = null;
            } else {
                this.InsuredNo1 = rs.getString("InsuredNo1").trim();
            }

            if (rs.getString("InsuredNo2") == null) {
                this.InsuredNo2 = null;
            } else {
                this.InsuredNo2 = rs.getString("InsuredNo2").trim();
            }

            if (rs.getString("InsuredNo3") == null) {
                this.InsuredNo3 = null;
            } else {
                this.InsuredNo3 = rs.getString("InsuredNo3").trim();
            }

            this.DirectLoss = rs.getDouble("DirectLoss");
            this.ReplevyLoss = rs.getDouble("ReplevyLoss");
            this.ViolatEndDate = rs.getDate("ViolatEndDate");
            if (rs.getString("DealMind") == null) {
                this.DealMind = null;
            } else {
                this.DealMind = rs.getString("DealMind").trim();
            }

            if (rs.getString("BranchType") == null) {
                this.BranchType = null;
            } else {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("BranchType2") == null) {
                this.BranchType2 = null;
            } else {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LAViolatCont表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAViolatContSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LAViolatContSchema getSchema() {
        LAViolatContSchema aLAViolatContSchema = new LAViolatContSchema();
        aLAViolatContSchema.setSchema(this);
        return aLAViolatContSchema;
    }

    public LAViolatContDB getDB() {
        LAViolatContDB aDBOper = new LAViolatContDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAViolatCont描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ViolatNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ViolatDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirectLoss));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ReplevyLoss));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ViolatEndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DealMind));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAViolatCont>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ViolatNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ViolatDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 2, SysConst.PACKAGESPILTER));
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            InsuredNo1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            InsuredNo2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            InsuredNo3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            DirectLoss = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            ReplevyLoss = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            ViolatEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            DealMind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                         SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAViolatContSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ViolatNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ViolatNo));
        }
        if (FCode.equals("ViolatDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getViolatDate()));
        }
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equals("InsuredNo1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo1));
        }
        if (FCode.equals("InsuredNo2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo2));
        }
        if (FCode.equals("InsuredNo3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo3));
        }
        if (FCode.equals("DirectLoss")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirectLoss));
        }
        if (FCode.equals("ReplevyLoss")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplevyLoss));
        }
        if (FCode.equals("ViolatEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getViolatEndDate()));
        }
        if (FCode.equals("DealMind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DealMind));
        }
        if (FCode.equals("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ViolatNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getViolatDate()));
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(AgentGroup);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(AppntNo);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(InsuredNo1);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(InsuredNo2);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(InsuredNo3);
            break;
        case 12:
            strFieldValue = String.valueOf(DirectLoss);
            break;
        case 13:
            strFieldValue = String.valueOf(ReplevyLoss);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getViolatEndDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(DealMind);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(BranchType);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(BranchType2);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ViolatNo")) {
            if (FValue != null && !FValue.equals("")) {
                ViolatNo = FValue.trim();
            } else {
                ViolatNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ViolatDate")) {
            if (FValue != null && !FValue.equals("")) {
                ViolatDate = fDate.getDate(FValue);
            } else {
                ViolatDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if (FValue != null && !FValue.equals("")) {
                AgentGroup = FValue.trim();
            } else {
                AgentGroup = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if (FValue != null && !FValue.equals("")) {
                AppntNo = FValue.trim();
            } else {
                AppntNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredNo1")) {
            if (FValue != null && !FValue.equals("")) {
                InsuredNo1 = FValue.trim();
            } else {
                InsuredNo1 = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredNo2")) {
            if (FValue != null && !FValue.equals("")) {
                InsuredNo2 = FValue.trim();
            } else {
                InsuredNo2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredNo3")) {
            if (FValue != null && !FValue.equals("")) {
                InsuredNo3 = FValue.trim();
            } else {
                InsuredNo3 = null;
            }
        }
        if (FCode.equalsIgnoreCase("DirectLoss")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DirectLoss = d;
            }
        }
        if (FCode.equalsIgnoreCase("ReplevyLoss")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ReplevyLoss = d;
            }
        }
        if (FCode.equalsIgnoreCase("ViolatEndDate")) {
            if (FValue != null && !FValue.equals("")) {
                ViolatEndDate = fDate.getDate(FValue);
            } else {
                ViolatEndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("DealMind")) {
            if (FValue != null && !FValue.equals("")) {
                DealMind = FValue.trim();
            } else {
                DealMind = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if (FValue != null && !FValue.equals("")) {
                BranchType = FValue.trim();
            } else {
                BranchType = null;
            }
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if (FValue != null && !FValue.equals("")) {
                BranchType2 = FValue.trim();
            } else {
                BranchType2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LAViolatContSchema other = (LAViolatContSchema) otherObject;
        return
                ViolatNo.equals(other.getViolatNo())
                && fDate.getString(ViolatDate).equals(other.getViolatDate())
                && PrtNo.equals(other.getPrtNo())
                && ContNo.equals(other.getContNo())
                && State.equals(other.getState())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && ManageCom.equals(other.getManageCom())
                && AppntNo.equals(other.getAppntNo())
                && InsuredNo1.equals(other.getInsuredNo1())
                && InsuredNo2.equals(other.getInsuredNo2())
                && InsuredNo3.equals(other.getInsuredNo3())
                && DirectLoss == other.getDirectLoss()
                && ReplevyLoss == other.getReplevyLoss()
                && fDate.getString(ViolatEndDate).equals(other.getViolatEndDate())
                && DealMind.equals(other.getDealMind())
                && BranchType.equals(other.getBranchType())
                && BranchType2.equals(other.getBranchType2())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ViolatNo")) {
            return 0;
        }
        if (strFieldName.equals("ViolatDate")) {
            return 1;
        }
        if (strFieldName.equals("PrtNo")) {
            return 2;
        }
        if (strFieldName.equals("ContNo")) {
            return 3;
        }
        if (strFieldName.equals("State")) {
            return 4;
        }
        if (strFieldName.equals("AgentCode")) {
            return 5;
        }
        if (strFieldName.equals("AgentGroup")) {
            return 6;
        }
        if (strFieldName.equals("ManageCom")) {
            return 7;
        }
        if (strFieldName.equals("AppntNo")) {
            return 8;
        }
        if (strFieldName.equals("InsuredNo1")) {
            return 9;
        }
        if (strFieldName.equals("InsuredNo2")) {
            return 10;
        }
        if (strFieldName.equals("InsuredNo3")) {
            return 11;
        }
        if (strFieldName.equals("DirectLoss")) {
            return 12;
        }
        if (strFieldName.equals("ReplevyLoss")) {
            return 13;
        }
        if (strFieldName.equals("ViolatEndDate")) {
            return 14;
        }
        if (strFieldName.equals("DealMind")) {
            return 15;
        }
        if (strFieldName.equals("BranchType")) {
            return 16;
        }
        if (strFieldName.equals("BranchType2")) {
            return 17;
        }
        if (strFieldName.equals("Operator")) {
            return 18;
        }
        if (strFieldName.equals("MakeDate")) {
            return 19;
        }
        if (strFieldName.equals("MakeTime")) {
            return 20;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 21;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 22;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ViolatNo";
            break;
        case 1:
            strFieldName = "ViolatDate";
            break;
        case 2:
            strFieldName = "PrtNo";
            break;
        case 3:
            strFieldName = "ContNo";
            break;
        case 4:
            strFieldName = "State";
            break;
        case 5:
            strFieldName = "AgentCode";
            break;
        case 6:
            strFieldName = "AgentGroup";
            break;
        case 7:
            strFieldName = "ManageCom";
            break;
        case 8:
            strFieldName = "AppntNo";
            break;
        case 9:
            strFieldName = "InsuredNo1";
            break;
        case 10:
            strFieldName = "InsuredNo2";
            break;
        case 11:
            strFieldName = "InsuredNo3";
            break;
        case 12:
            strFieldName = "DirectLoss";
            break;
        case 13:
            strFieldName = "ReplevyLoss";
            break;
        case 14:
            strFieldName = "ViolatEndDate";
            break;
        case 15:
            strFieldName = "DealMind";
            break;
        case 16:
            strFieldName = "BranchType";
            break;
        case 17:
            strFieldName = "BranchType2";
            break;
        case 18:
            strFieldName = "Operator";
            break;
        case 19:
            strFieldName = "MakeDate";
            break;
        case 20:
            strFieldName = "MakeTime";
            break;
        case 21:
            strFieldName = "ModifyDate";
            break;
        case 22:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ViolatNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ViolatDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo1")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo3")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DirectLoss")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ReplevyLoss")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ViolatEndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("DealMind")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 13:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
