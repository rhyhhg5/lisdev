/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCCustomerImpartDetailDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LCCustomerImpartDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 告知明细
 * @CreateDate：2005-03-11
 */
public class LCCustomerImpartDetailSchema implements Schema
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 告知编码 */
    private String ImpartCode;
    /** 告知版别 */
    private String ImpartVer;
    /** 内部流水号 */
    private String SubSerialNo;
    /** 告知内容 */
    private String ImpartDetailContent;
    /** 疾病内容 */
    private String DiseaseContent;
    /** 开始时间 */
    private Date StartDate;
    /** 结束时间 */
    private Date EndDate;
    /** 证明人 */
    private String Prover;
    /** 目前状况 */
    private String CurrCondition;
    /** 能否证明 */
    private String IsProved;
    /** 客户号码 */
    private String CustomerNo;
    /** 客户号码类型 */
    private String CustomerNoType;
    /** 是否参与核保核赔标志 */
    private String UWClaimFlg;
    /** 打印标志 */
    private String PrtFlag;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 23; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCCustomerImpartDetailSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[7];
        pk[0] = "GrpContNo";
        pk[1] = "ProposalContNo";
        pk[2] = "ImpartCode";
        pk[3] = "ImpartVer";
        pk[4] = "SubSerialNo";
        pk[5] = "CustomerNo";
        pk[6] = "CustomerNoType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getProposalContNo()
    {
        if (ProposalContNo != null && !ProposalContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProposalContNo = StrTool.unicodeToGBK(ProposalContNo);
        }
        return ProposalContNo;
    }

    public void setProposalContNo(String aProposalContNo)
    {
        ProposalContNo = aProposalContNo;
    }

    public String getPrtNo()
    {
        if (PrtNo != null && !PrtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtNo = StrTool.unicodeToGBK(PrtNo);
        }
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo)
    {
        PrtNo = aPrtNo;
    }

    public String getImpartCode()
    {
        if (ImpartCode != null && !ImpartCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartCode = StrTool.unicodeToGBK(ImpartCode);
        }
        return ImpartCode;
    }

    public void setImpartCode(String aImpartCode)
    {
        ImpartCode = aImpartCode;
    }

    public String getImpartVer()
    {
        if (ImpartVer != null && !ImpartVer.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartVer = StrTool.unicodeToGBK(ImpartVer);
        }
        return ImpartVer;
    }

    public void setImpartVer(String aImpartVer)
    {
        ImpartVer = aImpartVer;
    }

    public String getSubSerialNo()
    {
        if (SubSerialNo != null && !SubSerialNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SubSerialNo = StrTool.unicodeToGBK(SubSerialNo);
        }
        return SubSerialNo;
    }

    public void setSubSerialNo(String aSubSerialNo)
    {
        SubSerialNo = aSubSerialNo;
    }

    public String getImpartDetailContent()
    {
        if (ImpartDetailContent != null && !ImpartDetailContent.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartDetailContent = StrTool.unicodeToGBK(ImpartDetailContent);
        }
        return ImpartDetailContent;
    }

    public void setImpartDetailContent(String aImpartDetailContent)
    {
        ImpartDetailContent = aImpartDetailContent;
    }

    public String getDiseaseContent()
    {
        if (DiseaseContent != null && !DiseaseContent.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DiseaseContent = StrTool.unicodeToGBK(DiseaseContent);
        }
        return DiseaseContent;
    }

    public void setDiseaseContent(String aDiseaseContent)
    {
        DiseaseContent = aDiseaseContent;
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }

    public String getProver()
    {
        if (Prover != null && !Prover.equals("") && SysConst.CHANGECHARSET == true)
        {
            Prover = StrTool.unicodeToGBK(Prover);
        }
        return Prover;
    }

    public void setProver(String aProver)
    {
        Prover = aProver;
    }

    public String getCurrCondition()
    {
        if (CurrCondition != null && !CurrCondition.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CurrCondition = StrTool.unicodeToGBK(CurrCondition);
        }
        return CurrCondition;
    }

    public void setCurrCondition(String aCurrCondition)
    {
        CurrCondition = aCurrCondition;
    }

    public String getIsProved()
    {
        if (IsProved != null && !IsProved.equals("") && SysConst.CHANGECHARSET == true)
        {
            IsProved = StrTool.unicodeToGBK(IsProved);
        }
        return IsProved;
    }

    public void setIsProved(String aIsProved)
    {
        IsProved = aIsProved;
    }

    public String getCustomerNo()
    {
        if (CustomerNo != null && !CustomerNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerNo = StrTool.unicodeToGBK(CustomerNo);
        }
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getCustomerNoType()
    {
        if (CustomerNoType != null && !CustomerNoType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerNoType = StrTool.unicodeToGBK(CustomerNoType);
        }
        return CustomerNoType;
    }

    public void setCustomerNoType(String aCustomerNoType)
    {
        CustomerNoType = aCustomerNoType;
    }

    public String getUWClaimFlg()
    {
        if (UWClaimFlg != null && !UWClaimFlg.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UWClaimFlg = StrTool.unicodeToGBK(UWClaimFlg);
        }
        return UWClaimFlg;
    }

    public void setUWClaimFlg(String aUWClaimFlg)
    {
        UWClaimFlg = aUWClaimFlg;
    }

    public String getPrtFlag()
    {
        if (PrtFlag != null && !PrtFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtFlag = StrTool.unicodeToGBK(PrtFlag);
        }
        return PrtFlag;
    }

    public void setPrtFlag(String aPrtFlag)
    {
        PrtFlag = aPrtFlag;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCCustomerImpartDetailSchema 对象给 Schema 赋值
     * @param: aLCCustomerImpartDetailSchema LCCustomerImpartDetailSchema
     **/
    public void setSchema(LCCustomerImpartDetailSchema
                          aLCCustomerImpartDetailSchema)
    {
        this.GrpContNo = aLCCustomerImpartDetailSchema.getGrpContNo();
        this.ContNo = aLCCustomerImpartDetailSchema.getContNo();
        this.ProposalContNo = aLCCustomerImpartDetailSchema.getProposalContNo();
        this.PrtNo = aLCCustomerImpartDetailSchema.getPrtNo();
        this.ImpartCode = aLCCustomerImpartDetailSchema.getImpartCode();
        this.ImpartVer = aLCCustomerImpartDetailSchema.getImpartVer();
        this.SubSerialNo = aLCCustomerImpartDetailSchema.getSubSerialNo();
        this.ImpartDetailContent = aLCCustomerImpartDetailSchema.
                                   getImpartDetailContent();
        this.DiseaseContent = aLCCustomerImpartDetailSchema.getDiseaseContent();
        this.StartDate = fDate.getDate(aLCCustomerImpartDetailSchema.
                                       getStartDate());
        this.EndDate = fDate.getDate(aLCCustomerImpartDetailSchema.getEndDate());
        this.Prover = aLCCustomerImpartDetailSchema.getProver();
        this.CurrCondition = aLCCustomerImpartDetailSchema.getCurrCondition();
        this.IsProved = aLCCustomerImpartDetailSchema.getIsProved();
        this.CustomerNo = aLCCustomerImpartDetailSchema.getCustomerNo();
        this.CustomerNoType = aLCCustomerImpartDetailSchema.getCustomerNoType();
        this.UWClaimFlg = aLCCustomerImpartDetailSchema.getUWClaimFlg();
        this.PrtFlag = aLCCustomerImpartDetailSchema.getPrtFlag();
        this.Operator = aLCCustomerImpartDetailSchema.getOperator();
        this.MakeDate = fDate.getDate(aLCCustomerImpartDetailSchema.getMakeDate());
        this.MakeTime = aLCCustomerImpartDetailSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCCustomerImpartDetailSchema.
                                        getModifyDate());
        this.ModifyTime = aLCCustomerImpartDetailSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ProposalContNo") == null)
            {
                this.ProposalContNo = null;
            }
            else
            {
                this.ProposalContNo = rs.getString("ProposalContNo").trim();
            }

            if (rs.getString("PrtNo") == null)
            {
                this.PrtNo = null;
            }
            else
            {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ImpartCode") == null)
            {
                this.ImpartCode = null;
            }
            else
            {
                this.ImpartCode = rs.getString("ImpartCode").trim();
            }

            if (rs.getString("ImpartVer") == null)
            {
                this.ImpartVer = null;
            }
            else
            {
                this.ImpartVer = rs.getString("ImpartVer").trim();
            }

            if (rs.getString("SubSerialNo") == null)
            {
                this.SubSerialNo = null;
            }
            else
            {
                this.SubSerialNo = rs.getString("SubSerialNo").trim();
            }

            if (rs.getString("ImpartDetailContent") == null)
            {
                this.ImpartDetailContent = null;
            }
            else
            {
                this.ImpartDetailContent = rs.getString("ImpartDetailContent").
                                           trim();
            }

            if (rs.getString("DiseaseContent") == null)
            {
                this.DiseaseContent = null;
            }
            else
            {
                this.DiseaseContent = rs.getString("DiseaseContent").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("Prover") == null)
            {
                this.Prover = null;
            }
            else
            {
                this.Prover = rs.getString("Prover").trim();
            }

            if (rs.getString("CurrCondition") == null)
            {
                this.CurrCondition = null;
            }
            else
            {
                this.CurrCondition = rs.getString("CurrCondition").trim();
            }

            if (rs.getString("IsProved") == null)
            {
                this.IsProved = null;
            }
            else
            {
                this.IsProved = rs.getString("IsProved").trim();
            }

            if (rs.getString("CustomerNo") == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("CustomerNoType") == null)
            {
                this.CustomerNoType = null;
            }
            else
            {
                this.CustomerNoType = rs.getString("CustomerNoType").trim();
            }

            if (rs.getString("UWClaimFlg") == null)
            {
                this.UWClaimFlg = null;
            }
            else
            {
                this.UWClaimFlg = rs.getString("UWClaimFlg").trim();
            }

            if (rs.getString("PrtFlag") == null)
            {
                this.PrtFlag = null;
            }
            else
            {
                this.PrtFlag = rs.getString("PrtFlag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCCustomerImpartDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCCustomerImpartDetailSchema getSchema()
    {
        LCCustomerImpartDetailSchema aLCCustomerImpartDetailSchema = new
                LCCustomerImpartDetailSchema();
        aLCCustomerImpartDetailSchema.setSchema(this);
        return aLCCustomerImpartDetailSchema;
    }

    public LCCustomerImpartDetailDB getDB()
    {
        LCCustomerImpartDetailDB aDBOper = new LCCustomerImpartDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCCustomerImpartDetail描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProposalContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SubSerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartDetailContent)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DiseaseContent)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                StartDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(EndDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Prover)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CurrCondition)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IsProved)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerNoType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWClaimFlg)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCCustomerImpartDetail>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                            SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            ImpartCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            ImpartVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            SubSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            ImpartDetailContent = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER);
            DiseaseContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                            SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            Prover = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                    SysConst.PACKAGESPILTER);
            CurrCondition = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                           SysConst.PACKAGESPILTER);
            IsProved = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            CustomerNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            16, SysConst.PACKAGESPILTER);
            UWClaimFlg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            PrtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCCustomerImpartDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("ProposalContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProposalContNo));
        }
        if (FCode.equals("PrtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtNo));
        }
        if (FCode.equals("ImpartCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartCode));
        }
        if (FCode.equals("ImpartVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartVer));
        }
        if (FCode.equals("SubSerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SubSerialNo));
        }
        if (FCode.equals("ImpartDetailContent"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    ImpartDetailContent));
        }
        if (FCode.equals("DiseaseContent"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DiseaseContent));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getStartDate()));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getEndDate()));
        }
        if (FCode.equals("Prover"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Prover));
        }
        if (FCode.equals("CurrCondition"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CurrCondition));
        }
        if (FCode.equals("IsProved"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IsProved));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerNo));
        }
        if (FCode.equals("CustomerNoType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerNoType));
        }
        if (FCode.equals("UWClaimFlg"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWClaimFlg));
        }
        if (FCode.equals("PrtFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ImpartCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ImpartVer);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(SubSerialNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ImpartDetailContent);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(DiseaseContent);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Prover);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(CurrCondition);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(IsProved);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(CustomerNoType);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(UWClaimFlg);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(PrtFlag);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("ProposalContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
            {
                ProposalContNo = null;
            }
        }
        if (FCode.equals("PrtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
            {
                PrtNo = null;
            }
        }
        if (FCode.equals("ImpartCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartCode = FValue.trim();
            }
            else
            {
                ImpartCode = null;
            }
        }
        if (FCode.equals("ImpartVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartVer = FValue.trim();
            }
            else
            {
                ImpartVer = null;
            }
        }
        if (FCode.equals("SubSerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubSerialNo = FValue.trim();
            }
            else
            {
                SubSerialNo = null;
            }
        }
        if (FCode.equals("ImpartDetailContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartDetailContent = FValue.trim();
            }
            else
            {
                ImpartDetailContent = null;
            }
        }
        if (FCode.equals("DiseaseContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DiseaseContent = FValue.trim();
            }
            else
            {
                DiseaseContent = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        if (FCode.equals("Prover"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Prover = FValue.trim();
            }
            else
            {
                Prover = null;
            }
        }
        if (FCode.equals("CurrCondition"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CurrCondition = FValue.trim();
            }
            else
            {
                CurrCondition = null;
            }
        }
        if (FCode.equals("IsProved"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IsProved = FValue.trim();
            }
            else
            {
                IsProved = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("CustomerNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNoType = FValue.trim();
            }
            else
            {
                CustomerNoType = null;
            }
        }
        if (FCode.equals("UWClaimFlg"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWClaimFlg = FValue.trim();
            }
            else
            {
                UWClaimFlg = null;
            }
        }
        if (FCode.equals("PrtFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtFlag = FValue.trim();
            }
            else
            {
                PrtFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCCustomerImpartDetailSchema other = (LCCustomerImpartDetailSchema)
                                             otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && ProposalContNo.equals(other.getProposalContNo())
                && PrtNo.equals(other.getPrtNo())
                && ImpartCode.equals(other.getImpartCode())
                && ImpartVer.equals(other.getImpartVer())
                && SubSerialNo.equals(other.getSubSerialNo())
                && ImpartDetailContent.equals(other.getImpartDetailContent())
                && DiseaseContent.equals(other.getDiseaseContent())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && Prover.equals(other.getProver())
                && CurrCondition.equals(other.getCurrCondition())
                && IsProved.equals(other.getIsProved())
                && CustomerNo.equals(other.getCustomerNo())
                && CustomerNoType.equals(other.getCustomerNoType())
                && UWClaimFlg.equals(other.getUWClaimFlg())
                && PrtFlag.equals(other.getPrtFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return 3;
        }
        if (strFieldName.equals("ImpartCode"))
        {
            return 4;
        }
        if (strFieldName.equals("ImpartVer"))
        {
            return 5;
        }
        if (strFieldName.equals("SubSerialNo"))
        {
            return 6;
        }
        if (strFieldName.equals("ImpartDetailContent"))
        {
            return 7;
        }
        if (strFieldName.equals("DiseaseContent"))
        {
            return 8;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 9;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 10;
        }
        if (strFieldName.equals("Prover"))
        {
            return 11;
        }
        if (strFieldName.equals("CurrCondition"))
        {
            return 12;
        }
        if (strFieldName.equals("IsProved"))
        {
            return 13;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 14;
        }
        if (strFieldName.equals("CustomerNoType"))
        {
            return 15;
        }
        if (strFieldName.equals("UWClaimFlg"))
        {
            return 16;
        }
        if (strFieldName.equals("PrtFlag"))
        {
            return 17;
        }
        if (strFieldName.equals("Operator"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 19;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 21;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 22;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "ProposalContNo";
                break;
            case 3:
                strFieldName = "PrtNo";
                break;
            case 4:
                strFieldName = "ImpartCode";
                break;
            case 5:
                strFieldName = "ImpartVer";
                break;
            case 6:
                strFieldName = "SubSerialNo";
                break;
            case 7:
                strFieldName = "ImpartDetailContent";
                break;
            case 8:
                strFieldName = "DiseaseContent";
                break;
            case 9:
                strFieldName = "StartDate";
                break;
            case 10:
                strFieldName = "EndDate";
                break;
            case 11:
                strFieldName = "Prover";
                break;
            case 12:
                strFieldName = "CurrCondition";
                break;
            case 13:
                strFieldName = "IsProved";
                break;
            case 14:
                strFieldName = "CustomerNo";
                break;
            case 15:
                strFieldName = "CustomerNoType";
                break;
            case 16:
                strFieldName = "UWClaimFlg";
                break;
            case 17:
                strFieldName = "PrtFlag";
                break;
            case 18:
                strFieldName = "Operator";
                break;
            case 19:
                strFieldName = "MakeDate";
                break;
            case 20:
                strFieldName = "MakeTime";
                break;
            case 21:
                strFieldName = "ModifyDate";
                break;
            case 22:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubSerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartDetailContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DiseaseContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Prover"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CurrCondition"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IsProved"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWClaimFlg"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
