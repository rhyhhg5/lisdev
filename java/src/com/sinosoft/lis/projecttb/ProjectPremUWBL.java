/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ProjectPremUWBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	
	Reflections tReflections = new Reflections();

	//录入
	private String mProjectNo = "";
	private String mSendDate = "";
	private String mSendOperator = "";
	private String mAuditOpinion = "";
	private LCProjectMonthInfoSet mLCProjectMonthInfoSet = new LCProjectMonthInfoSet();

	private MMap mMap = new MMap();

	public ProjectPremUWBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectPremUWBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()){
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectPremUWBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	////////////////////////////////////////////////
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mLCProjectMonthInfoSet = (LCProjectMonthInfoSet) cInputData.getObjectByObjectName("LCProjectMonthInfoSet", 0);
		mProjectNo = (String) tTransferData.getValueByName("ProjectNo");
		if(mProjectNo == null || "".equals(mProjectNo)){
			CError tError = new CError();
			tError.moduleName = "ProjectPremUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取项目编码失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mSendDate = (String) tTransferData.getValueByName("SendDate");
		if(mSendDate == null || "".equals(mSendDate)){
			CError tError = new CError();
			tError.moduleName = "ProjectPremUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取报送时间失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mSendOperator = (String) tTransferData.getValueByName("SendOperator");
		if(mSendOperator == null || "".equals(mSendOperator)){
			CError tError = new CError();
			tError.moduleName = "ProjectPremUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取报送人失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mAuditOpinion = (String) tTransferData.getValueByName("AuditOpinion");
		if(mAuditOpinion == null || "".equals(mAuditOpinion)){
			CError tError = new CError();
			tError.moduleName = "ProjectPremUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取审核意见失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(mLCProjectMonthInfoSet == null || mLCProjectMonthInfoSet.size()<1){
			CError tError = new CError();
			tError.moduleName = "ProjectPremUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取审核数据失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData(){
//		处理项目信息，并添加项目轨迹
		LCProjectInfoSet tLCProjectInfoSet = new LCProjectInfoSet();
		LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
		String tProjectSQL = "select * from LCProjectInfo where ProjectNo = '"+mProjectNo+"'";
		tLCProjectInfoSet = tLCProjectInfoDB.executeQuery(tProjectSQL);
		if(tLCProjectInfoSet == null || tLCProjectInfoSet.size() != 1){
			CError tError = new CError();
			tError.moduleName = "ProjectPremUWBL";
			tError.functionName = "dealData";
			tError.errorMessage = "根据项目编码，获取项目信息失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		LCProjectInfoSchema tLCProjectInfoSchema = new LCProjectInfoSchema();
		tLCProjectInfoSchema = tLCProjectInfoSet.get(1);
		if(!"05".equals(tLCProjectInfoSchema.getState())){
			CError tError = new CError();
			tError.moduleName = "ProjectPremUWBL";
			tError.functionName = "dealData";
			tError.errorMessage = "根据项目状态，无法提交审核！";
			this.mErrors.addOneError(tError);
			return false;
		}
//		以下处理年度信息		
		LCProjectYearInfoSet tLCProjectYearInfoSet = new LCProjectYearInfoSet();
		LCProjectYearInfoDB tLCProjectYearInfoDB = new LCProjectYearInfoDB();
		String tYearSQL = "select * from LCProjectYearInfo where ProjectNo = '"+mProjectNo+"'";
		tLCProjectYearInfoSet = tLCProjectYearInfoDB.executeQuery(tYearSQL);
		if(tLCProjectYearInfoSet == null || tLCProjectYearInfoSet.size() < 1){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "dealData";
			tError.errorMessage = "根据项目编码，获取项目年度信息失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		String tSQL = "";
		String tMonthFeeNo = "";
		LCProjectMonthInfoDB tLCProjectMonthInfoDB = new LCProjectMonthInfoDB();
		LCProjectMonthInfoSet tLCProjectMonthInfoSet = new LCProjectMonthInfoSet();
		LCProjectPremUWDB tLCProjectPremUWDB = new LCProjectPremUWDB();
		LCProjectPremUWSet tLCProjectPremUWSet = new LCProjectPremUWSet();
		for(int i=1;i<=mLCProjectMonthInfoSet.size();i++){
			tMonthFeeNo = mLCProjectMonthInfoSet.get(i).getMonthFeeNo();
			if(tMonthFeeNo == null || "".equals(tMonthFeeNo)){
				CError tError = new CError();
				tError.moduleName = "ProjectInfoSendUWBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取月度编码失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			tSQL = "select * from LCProjectMonthInfo where MonthFeeNo = '"+tMonthFeeNo+"'";
			LCProjectMonthInfoSet tempLCProjectMonthInfoSet = tLCProjectMonthInfoDB.executeQuery(tSQL);
			if(tempLCProjectMonthInfoSet == null || tempLCProjectMonthInfoSet.size() !=1){
				CError tError = new CError();
				tError.moduleName = "ProjectInfoSendUWBL";
				tError.functionName = "dealData";
				tError.errorMessage = "根据月度编码"+tMonthFeeNo+"，获取月度信息失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			LCProjectMonthInfoSchema tempLCProjectMonthInfoSchema = tempLCProjectMonthInfoSet.get(1);
			if(!"02".equals(tempLCProjectMonthInfoSchema.getState())){
				CError tError = new CError();
				tError.moduleName = "ProjectPremUWBL";
				tError.functionName = "dealData";
				tError.errorMessage = "根据月度费用状态，无法提交审核！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			tSQL = "select * from LCProjectPremUW where MonthFeeNo = '"+tMonthFeeNo+"' order by SerialNo desc fetch first 1 rows only ";
			LCProjectPremUWSet tempLCProjectPremUWSet = tLCProjectPremUWDB.executeQuery(tSQL);
			if(tempLCProjectPremUWSet == null || tempLCProjectPremUWSet.size() !=1){
				CError tError = new CError();
				tError.moduleName = "ProjectInfoSendUWBL";
				tError.functionName = "dealData";
				tError.errorMessage = "根据月度编码"+tMonthFeeNo+"，获取月度审核信息失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			LCProjectPremUWSchema tLCProjectPremUWSchema = tempLCProjectPremUWSet.get(1);
			if("ALLOW".equals(mOperate)){
				tempLCProjectMonthInfoSchema.setState("03");
				tLCProjectPremUWSchema.setConclusion("0");
			}else{
				tempLCProjectMonthInfoSchema.setState("04");
				tLCProjectPremUWSchema.setConclusion("1");
			}
			tempLCProjectMonthInfoSchema.setOperator(mGlobalInput.Operator);
			tempLCProjectMonthInfoSchema.setModifyDate(CurrentDate);
			tempLCProjectMonthInfoSchema.setModifyTime(CurrentTime);
			tLCProjectMonthInfoSet.add(tempLCProjectMonthInfoSchema);
			
			tLCProjectPremUWSchema.setAuditOpinion(mAuditOpinion);
			tLCProjectPremUWSchema.setUWDate(CurrentDate);
			tLCProjectPremUWSchema.setUWTime(CurrentTime);
			tLCProjectPremUWSchema.setManageCom(mGlobalInput.ManageCom);
			tLCProjectPremUWSchema.setOperator(mGlobalInput.Operator);
			tLCProjectPremUWSchema.setModifyDate(CurrentDate);
			tLCProjectPremUWSchema.setModifyTime(CurrentTime);
			tLCProjectPremUWSet.add(tLCProjectPremUWSchema);
		}
		mMap.put(tLCProjectMonthInfoSet, SysConst.UPDATE);
		mMap.put(tLCProjectPremUWSet, SysConst.UPDATE);
		
		return true;
	}
	
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

}
