package com.sinosoft.lis.projecttb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ProjectCancelUI {

	public CErrors mErrors = new CErrors();
	
	private VData mResult = new VData();
	
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	
	/** 数据操作字符串 */
	private String mOperate;
	
	
	public boolean submitData(VData cInputData, String cOperate) {
		mOperate = cOperate;
		this.mInputData = (VData) cInputData.clone();
		ProjectCancelBL projectCancelBl = new ProjectCancelBL();
		try {
			if (!projectCancelBl.submitData(mInputData, mOperate)) {
				
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mResult = projectCancelBl.getResult();
		return true;
	}
}
