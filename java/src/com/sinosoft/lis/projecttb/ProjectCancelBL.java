package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.LCProjectInfoDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCProjectCancelSchema;
import com.sinosoft.lis.schema.LCProjectInfoSchema;
import com.sinosoft.lis.vschema.LCProjectCancelSet;
import com.sinosoft.lis.vschema.LCProjectInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;


public class ProjectCancelBL {

	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	
	private MMap mMap = new MMap();
	
	private LCProjectInfoSchema mLCProjectInfoSchema = new LCProjectInfoSchema();
	
	private LCProjectCancelSchema tLCProjectCancelSchema=  new LCProjectCancelSchema();
	
	public ProjectCancelBL(){
		
	}
	

	public boolean submitData(VData cInputData, String cOperate) {
		
		
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LCProjectCancelBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			System.out.print("保存数据返回");
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

	
	//获得输出数据
	private boolean getInputData(VData cInputData) {
		if(cInputData.getObjectByObjectName("LCProjectCancelSchema", 0) != null){
			
			 tLCProjectCancelSchema=(LCProjectCancelSchema)cInputData.getObjectByObjectName("LCProjectCancelSchema", 0);
		}
		
		if(tLCProjectCancelSchema == null ){
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoBL";
			tError.functionName = "dealData";
			tError.errorMessage = "作废项目信息录入失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
	//  业务处理
	private boolean dealData() {
		LCProjectCancelSet tlcProjectCancelSet =  new  LCProjectCancelSet();
		if("INSERT||MAIN".equals(mOperate)){
				//校验是否存在该项目
				if(tLCProjectCancelSchema.getProjectNo()==null || "".equals(tLCProjectCancelSchema.getProjectNo())){
					CError tError = new CError();
					tError.moduleName = "LCProjectCancelBL";
					tError.functionName = "dealData";
					tError.errorMessage = "该项目不存在！";
					this.mErrors.addOneError(tError);
					return false;
				}
				//修改项目为作废
				LCProjectInfoSet tLCProjectInfoSet = new LCProjectInfoSet();
				LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
				tLCProjectInfoDB.setProjectNo(tLCProjectCancelSchema.getProjectNo());
				tLCProjectInfoSet = tLCProjectInfoDB.query();
				LCProjectInfoSchema tempLCProjectInfoSchema = new LCProjectInfoSchema();
				tempLCProjectInfoSchema = tLCProjectInfoSet.get(1);
				tempLCProjectInfoSchema.setState("07");
				mMap.put(tempLCProjectInfoSchema, SysConst.UPDATE);
				//添加作废信息
				tLCProjectCancelSchema.setSerialNo(PubFun1.CreateMaxNo("PROJECTCANCEL", 20));//流水号
				tlcProjectCancelSet.add(tLCProjectCancelSchema);
				mMap.put(tlcProjectCancelSet, SysConst.INSERT);
		}
		return true;
	}
	
	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectCancelBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}