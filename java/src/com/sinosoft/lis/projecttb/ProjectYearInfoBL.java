/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class ProjectYearInfoBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	//录入
	private LCProjectYearInfoSchema mLCProjectYearInfoSchema = new LCProjectYearInfoSchema();
	
	Reflections tReflections = new Reflections();

	private MMap mMap = new MMap();

	public ProjectYearInfoBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectYearInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectYearInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCProjectYearInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mLCProjectYearInfoSchema.setSchema((LCProjectYearInfoSchema) cInputData
				.getObjectByObjectName("LCProjectYearInfoSchema", 0));
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));

		return true;

	}

	private boolean dealData() {
		
		LCProjectInfoSet tLCProjectInfoSet = new LCProjectInfoSet();
		LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
		String getProjectSql = "select * from LCProjectInfo where projectno = '"+mLCProjectYearInfoSchema.getProjectNo()+"'";
		tLCProjectInfoSet = tLCProjectInfoDB.executeQuery(getProjectSql);
		if (tLCProjectInfoSet == null || tLCProjectInfoSet.size() != 1){
			CError tError = new CError();
			tError.moduleName = "LCProjectYearInfoBL";
			tError.functionName = "dealData";
			tError.errorMessage = "项目编号为"+ mLCProjectYearInfoSchema.getProjectNo()+ "项目信息获取失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		LCProjectContSet tLCProjectContSet = new LCProjectContSet();
		LCProjectContDB tLCProjectContDB = new LCProjectContDB();
		String getContSql = "select * from LCProjectCont where projectno = '"+mLCProjectYearInfoSchema.getProjectNo()+"' and projectyear = '"+mLCProjectYearInfoSchema.getProjectYear()+"'";
		tLCProjectContSet = tLCProjectContDB.executeQuery(getContSql);
		
		LCProjectYearInfoSet tLCProjectYearInfoSet = new LCProjectYearInfoSet();
		LCProjectYearInfoDB tLCProjectYearInfoDB = new LCProjectYearInfoDB();
		String getYearSql = "select * from LCProjectYearInfo where projectno = '"+mLCProjectYearInfoSchema.getProjectNo()+"' and projectyear = '"+mLCProjectYearInfoSchema.getProjectYear()+"'";
		tLCProjectYearInfoSet = tLCProjectYearInfoDB.executeQuery(getYearSql);
		
		LCProjectMonthInfoSet tLCProjectMonthInfoSet = new LCProjectMonthInfoSet();
		LCProjectMonthInfoDB tLCProjectMonthInfoDB = new LCProjectMonthInfoDB();
		String getMonthSql = "select * from LCProjectMonthInfo where projectno = '"+mLCProjectYearInfoSchema.getProjectNo()+"' and projectyear = '"+mLCProjectYearInfoSchema.getProjectYear()+"'";
		tLCProjectMonthInfoSet = tLCProjectMonthInfoDB.executeQuery(getMonthSql);
		
		if("INSERT||MAIN".equals(mOperate)){
			if (tLCProjectYearInfoSet != null && tLCProjectYearInfoSet.size()>0){
				CError tError = new CError();
				tError.moduleName = "LCProjectYearInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目编号为"+ mLCProjectYearInfoSchema.getProjectNo()+ ",年度为"+mLCProjectYearInfoSchema.getProjectYear()+"的数据已存在，请选中该年度进行修改！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			mLCProjectYearInfoSchema.setState("01");
			mLCProjectYearInfoSchema.setFirstOperator(mGlobalInput.Operator);
			mLCProjectYearInfoSchema.setCreateDate(CurrentDate);
			mLCProjectYearInfoSchema.setCreateTime(CurrentTime);
			mLCProjectYearInfoSchema.setManageCom(mGlobalInput.ManageCom);
			mLCProjectYearInfoSchema.setOperator(mGlobalInput.Operator);
			mLCProjectYearInfoSchema.setMakeDate(CurrentDate);
			mLCProjectYearInfoSchema.setMakeTime(CurrentTime);
			mLCProjectYearInfoSchema.setModifyDate(CurrentDate);
			mLCProjectYearInfoSchema.setModifyTime(CurrentTime);
				
			mMap.put(this.mLCProjectYearInfoSchema, SysConst.INSERT);
			
		} else	if (mOperate.equals("DELETE||MAIN")) {
			//暂时没有删除功能 20120615 by gzh
			if(tLCProjectContSet !=null && tLCProjectContSet.size()>0){
				CError tError = new CError();
				tError.moduleName = "LCProjectYearInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目编号为"+ mLCProjectYearInfoSchema.getProjectNo()+ ",年度为"+mLCProjectYearInfoSchema.getProjectYear()+"，存在已归属的保单，不允许删除！";
				this.mErrors.addOneError(tError);
				return false;
			}
			if(tLCProjectMonthInfoSet !=null && tLCProjectMonthInfoSet.size()>0){
				CError tError = new CError();
				tError.moduleName = "LCProjectYearInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目编号为"+ mLCProjectYearInfoSchema.getProjectNo()+ ",年度为"+mLCProjectYearInfoSchema.getProjectYear()+"，存在月度费用，不允许删除！";
				this.mErrors.addOneError(tError);
				return false;
			}
			if (tLCProjectYearInfoSet == null || tLCProjectYearInfoSet.size() != 1) {
				this.mErrors.copyAllErrors(tLCProjectYearInfoDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "LCProjectYearInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目编号为"+ mLCProjectYearInfoSchema.getProjectNo()+ ",年度为"+mLCProjectYearInfoSchema.getProjectYear()+"的数据不存在，无法删除！";
				this.mErrors.addOneError(tError);
				return false;
			}
			mMap.put(tLCProjectYearInfoSet, SysConst.DELETE);
			
//			项目启用后，修改记录轨迹。
			
			if("05".equals(tLCProjectInfoSet.get(1).getState())){
				LCProjectYearInfoSchema oldLCProjectYearInfoSchema = new LCProjectYearInfoSchema();
				oldLCProjectYearInfoSchema = tLCProjectYearInfoSet.get(1);
				LCProjectYearTraceSchema tLCProjectYearTraceSchema = new LCProjectYearTraceSchema();
		        tReflections.transFields(tLCProjectYearTraceSchema,oldLCProjectYearInfoSchema);
	        	String tYearSerialNo = PubFun1.CreateMaxNo("YearSerialNo", 20);
	        	tLCProjectYearTraceSchema.setSerialNo(tYearSerialNo);
	        	tLCProjectYearTraceSchema.setProjectSerialNo("00000000000000000000");
	        	
	        	mMap.put(tLCProjectYearTraceSchema, SysConst.INSERT);
			}
		}else if("UPDATE||MAIN".equals(mOperate)){
//			if(tLCProjectContSet !=null && tLCProjectContSet.size()>0){
//				CError tError = new CError();
//				tError.moduleName = "LCProjectYearInfoBL";
//				tError.functionName = "dealData";
//				tError.errorMessage = "项目编号为"+ mLCProjectYearInfoSchema.getProjectNo()+ ",年度为"+mLCProjectYearInfoSchema.getProjectYear()+"，存在已归属的保单，不允许修改！";
//				this.mErrors.addOneError(tError);
//				return false;
//			}
//			if(tLCProjectMonthInfoSet !=null && tLCProjectMonthInfoSet.size()>0){
//				CError tError = new CError();
//				tError.moduleName = "LCProjectYearInfoBL";
//				tError.functionName = "dealData";
//				tError.errorMessage = "项目编号为"+ mLCProjectYearInfoSchema.getProjectNo()+ ",年度为"+mLCProjectYearInfoSchema.getProjectYear()+"，存在月度费用，不允许修改！";
//				this.mErrors.addOneError(tError);
//				return false;
//			}
			if (tLCProjectYearInfoSet == null || tLCProjectYearInfoSet.size() != 1){
				CError tError = new CError();
				tError.moduleName = "LCProjectYearInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目编号为"+ mLCProjectYearInfoSchema.getProjectNo()+ ",年度为"+mLCProjectYearInfoSchema.getProjectYear()+"的数据不存在，无法修改！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			LCProjectYearInfoSchema oldLCProjectYearInfoSchema = new LCProjectYearInfoSchema();
			oldLCProjectYearInfoSchema = tLCProjectYearInfoSet.get(1);
			
			mLCProjectYearInfoSchema.setState(oldLCProjectYearInfoSchema.getState());
			mLCProjectYearInfoSchema.setFirstOperator(oldLCProjectYearInfoSchema.getFirstOperator());
			mLCProjectYearInfoSchema.setCreateDate(oldLCProjectYearInfoSchema.getCreateDate());
			mLCProjectYearInfoSchema.setCreateTime(oldLCProjectYearInfoSchema.getCreateTime());
			mLCProjectYearInfoSchema.setMakeDate(oldLCProjectYearInfoSchema.getMakeDate());
			mLCProjectYearInfoSchema.setMakeTime(oldLCProjectYearInfoSchema.getMakeTime());
			mLCProjectYearInfoSchema.setManageCom(oldLCProjectYearInfoSchema.getManageCom());
			mLCProjectYearInfoSchema.setOperator(mGlobalInput.Operator);
			mLCProjectYearInfoSchema.setModifyDate(CurrentDate);
			mLCProjectYearInfoSchema.setModifyTime(CurrentTime);
			
			mMap.put(this.mLCProjectYearInfoSchema, SysConst.UPDATE);
			
//			项目启用后，修改记录轨迹。
			
			if("05".equals(tLCProjectInfoSet.get(1).getState())){
				LCProjectYearTraceSchema tLCProjectYearTraceSchema = new LCProjectYearTraceSchema();
		        tReflections.transFields(tLCProjectYearTraceSchema,oldLCProjectYearInfoSchema);
	        	String tYearSerialNo = PubFun1.CreateMaxNo("YearSerialNo", 20);
	        	tLCProjectYearTraceSchema.setSerialNo(tYearSerialNo);
	        	tLCProjectYearTraceSchema.setProjectSerialNo("00000000000000000000");
	        	
	        	mMap.put(tLCProjectYearTraceSchema, SysConst.INSERT);
			}
		}
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectYearInfoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

}
