package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class ProjectInfoUpdateBL {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();	
	Reflections tReflections = new Reflections();
	
	//录入
	private LCProjectInfoSchema mLCProjectInfoSchema = new LCProjectInfoSchema();
	private LCProjectTraceSchema mLCProjectTraceSchema = new LCProjectTraceSchema();
	private MMap mMap = new MMap();

	public ProjectInfoUpdateBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		//System.out.println(cInputData);
		if (!getInputData(cInputData)) {
			return false;
		}
		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoUpdateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectInfoUpdateBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
       //保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectInfoUpdateBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			System.out.println("ProjectInfoUpdateBL 错误处理");
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectInfoUpdateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mLCProjectInfoSchema.setSchema((LCProjectInfoSchema) cInputData
				.getObjectByObjectName("LCProjectInfoSchema", 0));
		this.mLCProjectTraceSchema.setSchema((LCProjectTraceSchema) cInputData
				.getObjectByObjectName("LCProjectTraceSchema", 0));
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		return true;
	}
/**
 * 将前台传过来的数据和从数据库查出的数据一起封装到map中，并传入后台
 * @param MMap mMap
 * @return boolean
 */
	private boolean dealData() {

		LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
		LCProjectTraceSchema mLCProjectTraceSchema = new LCProjectTraceSchema(); 
		
		tLCProjectInfoDB.setProjectNo(mLCProjectInfoSchema.getProjectNo());		
		if(!tLCProjectInfoDB.getInfo()){			
			return false;//查询失败
		}
		mLCProjectInfoSchema.setState(tLCProjectInfoDB.getState());
		mLCProjectInfoSchema.setCreateDate(tLCProjectInfoDB.getCreateDate());
		mLCProjectInfoSchema.setCreateTime(tLCProjectInfoDB.getCreateTime());
//		mLCProjectInfoSchema.setManageCom(tLCProjectInfoDB.getManageCom());
		mLCProjectInfoSchema.setOperator(mGlobalInput.Operator);
		mLCProjectInfoSchema.setMakeDate(tLCProjectInfoDB.getMakeDate());
		mLCProjectInfoSchema.setMakeTime(tLCProjectInfoDB.getMakeTime());
		mLCProjectInfoSchema.setModifyDate(PubFun.getCurrentDate());
		mLCProjectInfoSchema.setModifyTime(PubFun.getCurrentTime()); 
		
		mMap.put(this.mLCProjectInfoSchema, SysConst.UPDATE);		
       
        String tProjectSerialNo = PubFun1.CreateMaxNo("ProjectSerialNo", 20);
        mLCProjectTraceSchema.setProjectSerialNo(tProjectSerialNo);    
        mLCProjectTraceSchema.setProjectNo(tLCProjectInfoDB.getProjectNo());
        mLCProjectTraceSchema.setProjectName(tLCProjectInfoDB.getProjectName());
        mLCProjectTraceSchema.setProvince(tLCProjectInfoDB.getProvince());
        mLCProjectTraceSchema.setCity(tLCProjectInfoDB.getCity());
        mLCProjectTraceSchema.setCounty(tLCProjectInfoDB.getCounty());
        mLCProjectTraceSchema.setProjectType(tLCProjectInfoDB.getProjectType());
        mLCProjectTraceSchema.setPersonType(tLCProjectInfoDB.getPersonType());
        mLCProjectTraceSchema.setGrade(tLCProjectInfoDB.getGrade());
        mLCProjectTraceSchema.setPersonType(tLCProjectInfoDB.getPersonType());
        mLCProjectTraceSchema.setLimit(tLCProjectInfoDB.getLimit());
        mLCProjectTraceSchema.setStartDate(tLCProjectInfoDB.getStartDate());
        mLCProjectTraceSchema.setEndDate(tLCProjectInfoDB.getEndDate());
        mLCProjectTraceSchema.setState(tLCProjectInfoDB.getState());
        mLCProjectTraceSchema.setCreateDate(tLCProjectInfoDB.getCreateDate());
        mLCProjectTraceSchema.setCreateTime(tLCProjectInfoDB.getCreateTime());
        mLCProjectTraceSchema.setManageCom(tLCProjectInfoDB.getManageCom());
        mLCProjectTraceSchema.setOperator(mGlobalInput.Operator);
        mLCProjectTraceSchema.setMakeDate(CurrentDate);
        mLCProjectTraceSchema.setMakeTime(CurrentTime);
        mLCProjectTraceSchema.setModifyDate(CurrentDate);
        mLCProjectTraceSchema.setModifyTime(CurrentTime);
        
        //mLCProjectInfoSchema.setState("02");//分公司审核
      //add by zjd 同步到社保项目要素表中
		LCGrpContSubSet tLCGrpContSubSet = new LCGrpContSubSet();
		LCGrpContSubDB tLCGrpContSubDB=new LCGrpContSubDB();
		tLCGrpContSubDB.setProjectNo(mLCProjectInfoSchema.getProjectNo());
		tLCGrpContSubSet=tLCGrpContSubDB.query();
		if(tLCGrpContSubSet!=null && tLCGrpContSubSet.size()> 0){
			for(int i=1 ;i<=tLCGrpContSubSet.size();i++){
				tLCGrpContSubSet.get(i).setProjectName(mLCProjectInfoSchema.getProjectName());
				tLCGrpContSubSet.get(i).setModifyDate(PubFun.getCurrentDate());
				tLCGrpContSubSet.get(i).setModifyTime(PubFun.getCurrentTime());
			}
		}
		mMap.put(tLCGrpContSubSet, SysConst.UPDATE);
		mMap.put(mLCProjectTraceSchema, SysConst.INSERT);
		
		/*项目启用后，修改记录轨迹。
		if("05".equals(mLCProjectInfoSchema.getState())){}*/
		return true;
	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoUpdateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}
}
