/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ProjectInfoSendUWBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	
	Reflections tReflections = new Reflections();

	//录入
	private String mProjectNo = "";

	private MMap mMap = new MMap();

	public ProjectInfoSendUWBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectInfoSendUWBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()){
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	////////////////////////////////////////////////
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mProjectNo = (String) tTransferData.getValueByName("ProjectNo");
		if(mProjectNo == null || "".equals(mProjectNo)){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "项目未建立，或获取项目编码失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData(){
//		处理项目信息，并添加项目轨迹
		LCProjectInfoSet tLCProjectInfoSet = new LCProjectInfoSet();
		LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
		String tProjectSQL = "select * from LCProjectInfo where ProjectNo = '"+mProjectNo+"'";
		tLCProjectInfoSet = tLCProjectInfoDB.executeQuery(tProjectSQL);
		if(tLCProjectInfoSet == null || tLCProjectInfoSet.size() != 1){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "dealData";
			tError.errorMessage = "根据项目编码，获取项目信息失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		LCProjectInfoSchema tLCProjectInfoSchema = new LCProjectInfoSchema();
		tLCProjectInfoSchema = tLCProjectInfoSet.get(1);
		if(!"01".equals(tLCProjectInfoSchema.getState()) && !"04".equals(tLCProjectInfoSchema.getState()) && !"06".equals(tLCProjectInfoSchema.getState())){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "dealData";
			tError.errorMessage = "根据项目状态，无法提交审核！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		LCProjectTraceSchema tLCProjectTraceSchema = new LCProjectTraceSchema();
        tReflections.transFields(tLCProjectTraceSchema,tLCProjectInfoSchema);
        
        String tProjectSerialNo = PubFun1.CreateMaxNo("ProjectSerialNo", 20);
        tLCProjectTraceSchema.setProjectSerialNo(tProjectSerialNo);
        
        tLCProjectInfoSchema.setState("02");//分公司审核
		tLCProjectInfoSchema.setModifyDate(CurrentDate);
		tLCProjectInfoSchema.setModifyTime(CurrentTime);
		
		mMap.put(tLCProjectTraceSchema, SysConst.INSERT);
		mMap.put(tLCProjectInfoSchema, SysConst.UPDATE);
		
//		以下处理年度信息，并生成年度轨迹		
		LCProjectYearInfoSet tLCProjectYearInfoSet = new LCProjectYearInfoSet();
		LCProjectYearInfoDB tLCProjectYearInfoDB = new LCProjectYearInfoDB();
		String tYearSQL = "select * from LCProjectYearInfo where ProjectNo = '"+mProjectNo+"'";
		tLCProjectYearInfoSet = tLCProjectYearInfoDB.executeQuery(tYearSQL);
		if(tLCProjectYearInfoSet == null || tLCProjectYearInfoSet.size() < 1){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "dealData";
			tError.errorMessage = "根据项目编码，获取项目年度信息失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		LCProjectYearTraceSet tLCProjectYearTraceSet = new LCProjectYearTraceSet();
		LCProjectYearTraceSchema tLCProjectYearTraceSchema = new LCProjectYearTraceSchema();
		tLCProjectYearTraceSet.add(tLCProjectYearTraceSchema);
        tReflections.transFields(tLCProjectYearTraceSet,tLCProjectYearInfoSet);
        for(int i=1;i<=tLCProjectYearTraceSet.size();i++){
        	String tYearSerialNo = PubFun1.CreateMaxNo("YearSerialNo", 20);
        	tLCProjectYearTraceSet.get(i).setSerialNo(tYearSerialNo);
        	tLCProjectYearTraceSet.get(i).setProjectSerialNo(tProjectSerialNo);
        }
        for(int i=1;i<=tLCProjectYearInfoSet.size();i++){
        	tLCProjectYearInfoSet.get(i).setState("02");
        	tLCProjectYearInfoSet.get(i).setModifyDate(CurrentDate);
        	tLCProjectYearInfoSet.get(i).setModifyTime(CurrentTime);
        }
        
        mMap.put(tLCProjectYearTraceSet, SysConst.INSERT);
		mMap.put(tLCProjectYearInfoSet, SysConst.UPDATE);
		
//		项目审核表
        LCProjectUWSchema tLCProjectUWSchema = new LCProjectUWSchema();
        String tUWSerialNo = PubFun1.CreateMaxNo("PUWSerialNo", 20);
        tLCProjectUWSchema.setSerialNo(tUWSerialNo);
        tLCProjectUWSchema.setProjectSerialNo(tProjectSerialNo);
        tLCProjectUWSchema.setProjectNo(mProjectNo);
        tLCProjectUWSchema.setSendOperator(mGlobalInput.Operator);
        tLCProjectUWSchema.setSendDate(CurrentDate);
        tLCProjectUWSchema.setAuditFlag("01");
        String tAuditTimes = getAuditTimes();
        tLCProjectUWSchema.setAuditTimes(tAuditTimes);
        tLCProjectUWSchema.setManageCom(mGlobalInput.ManageCom);
        tLCProjectUWSchema.setOperator(mGlobalInput.Operator);
        tLCProjectUWSchema.setMakeDate(CurrentDate);
        tLCProjectUWSchema.setMakeTime(CurrentTime);
        tLCProjectUWSchema.setModifyDate(CurrentDate);
        tLCProjectUWSchema.setModifyTime(CurrentTime);
        
        mMap.put(tLCProjectUWSchema, SysConst.INSERT);
        
		return true;

	}
	
	private String getAuditTimes(){
		String aAuditTimes = "";
		String aAuditTimesSql = "select count(distinct ProjectSerialNo) from LCProjectUW where ProjectNo = '"+mProjectNo+"' and AuditFlag = '01'";
		aAuditTimes = new ExeSQL().getOneValue(aAuditTimesSql);
		if(aAuditTimes == null || "".equals(aAuditTimes)){
			aAuditTimes = "0";
		}else{
			aAuditTimes = (Integer.parseInt(aAuditTimes) +1)+"";
		}
		return aAuditTimes;
	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

}
