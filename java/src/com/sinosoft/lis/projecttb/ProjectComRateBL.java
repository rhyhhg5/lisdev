/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ProjectComRateBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	//录入
	private String mProjectNo = "";
	private String mProjectYear = "";
	private String mComplexRate = "";

	private MMap mMap = new MMap();

	public ProjectComRateBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectYearInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectYearInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectYearInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mProjectNo = (String)tTransferData.getValueByName("ProjectNo");
		if (mProjectNo == null || "".equals(mProjectNo)){
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "项目编号为空，无法保存！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mProjectYear = (String)tTransferData.getValueByName("ProjectYear");
		if (mProjectYear == null || "".equals(mProjectYear)){
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "项目年度为空，无法保存！";
			this.mErrors.addOneError(tError);
			return false;
		}
		mComplexRate = (String)tTransferData.getValueByName("ComplexRate");
		if (mComplexRate == null || "".equals(mComplexRate)){
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "综合赔付率为空，无法保存！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	private boolean dealData(){
		
		LCProjectYearInfoSet tLCProjectYearInfoSet = new LCProjectYearInfoSet();
		LCProjectYearInfoDB tLCProjectYearInfoDB = new LCProjectYearInfoDB();
		String getYearSql = "select * from LCProjectYearInfo where projectno = '"+mProjectNo+"' and projectyear = '"+mProjectYear+"'";
		tLCProjectYearInfoSet = tLCProjectYearInfoDB.executeQuery(getYearSql);
		
		if (tLCProjectYearInfoSet == null || tLCProjectYearInfoSet.size() != 1){
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "dealData";
			tError.errorMessage = "项目编号为"+ mProjectNo+ ",年度为"+mProjectYear+"的数据不存在，无法修改！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		LCProjectYearInfoSchema oldLCProjectYearInfoSchema = new LCProjectYearInfoSchema();
		oldLCProjectYearInfoSchema = tLCProjectYearInfoSet.get(1);
		oldLCProjectYearInfoSchema.setComplexRate(mComplexRate);
		oldLCProjectYearInfoSchema.setManageCom(mGlobalInput.ManageCom);
		oldLCProjectYearInfoSchema.setOperator(mGlobalInput.Operator);
		oldLCProjectYearInfoSchema.setModifyDate(CurrentDate);
		oldLCProjectYearInfoSchema.setModifyTime(CurrentTime);
		mMap.put(oldLCProjectYearInfoSchema, SysConst.UPDATE);

		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData(){
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectComRateBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

}
