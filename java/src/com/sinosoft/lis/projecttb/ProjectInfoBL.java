/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class ProjectInfoBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();


	//录入
	private LCProjectInfoSchema mLCProjectInfoSchema = new LCProjectInfoSchema();
	private LCProjectYearInfoSchema mLCProjectYearInfoSchema = new LCProjectYearInfoSchema();


	private MMap mMap = new MMap();

	public ProjectInfoBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	////////////////////////////////////////////////
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mLCProjectInfoSchema.setSchema((LCProjectInfoSchema) cInputData
				.getObjectByObjectName("LCProjectInfoSchema", 0));
		this.mLCProjectYearInfoSchema.setSchema((LCProjectYearInfoSchema) cInputData
				.getObjectByObjectName("LCProjectYearInfoSchema", 0));
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));

		return true;

	}

	private boolean dealData() {
		
		if("INSERT||MAIN".equals(mOperate)){
			mLCProjectInfoSchema.setState("01");
			mLCProjectInfoSchema.setCreateDate(CurrentDate);
			mLCProjectInfoSchema.setCreateTime(CurrentTime);
			//mLCProjectInfoSchema.setManageCom(mGlobalInput.ManageCom);
			mLCProjectInfoSchema.setOperator(mGlobalInput.Operator);
			mLCProjectInfoSchema.setMakeDate(CurrentDate);
			mLCProjectInfoSchema.setMakeTime(CurrentTime);
			mLCProjectInfoSchema.setModifyDate(CurrentDate);
			mLCProjectInfoSchema.setModifyTime(CurrentTime);
			
			mLCProjectYearInfoSchema.setState("01");
			mLCProjectYearInfoSchema.setFirstOperator(mGlobalInput.Operator);
			mLCProjectYearInfoSchema.setCreateDate(CurrentDate);
			mLCProjectYearInfoSchema.setCreateTime(CurrentTime);
			//mLCProjectYearInfoSchema.setManageCom(mGlobalInput.ManageCom);
			mLCProjectYearInfoSchema.setOperator(mGlobalInput.Operator);
			mLCProjectYearInfoSchema.setMakeDate(CurrentDate);
			mLCProjectYearInfoSchema.setMakeTime(CurrentTime);
			mLCProjectYearInfoSchema.setModifyDate(CurrentDate);
			mLCProjectYearInfoSchema.setModifyTime(CurrentTime);
				
			mMap.put(this.mLCProjectInfoSchema, SysConst.INSERT);
			mMap.put(this.mLCProjectYearInfoSchema, SysConst.INSERT);
			
		} else	if (mOperate.equals("DELETE||MAIN")) {
			LCProjectInfoSet mLCProjectInfoSet = new LCProjectInfoSet();
			LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
			tLCProjectInfoDB.setProjectNo(mLCProjectInfoSchema.getProjectNo());
			mLCProjectInfoSet = tLCProjectInfoDB.query();
			if (mLCProjectInfoSet == null || mLCProjectInfoSet.size() == 0) {
				this.mErrors.copyAllErrors(tLCProjectInfoDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ProjectInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目编号为"+ mLCProjectInfoSchema.getProjectNo()+ "的项目不存在，无法删除！";
				this.mErrors.addOneError(tError);
				return false;
			} else {
				mMap.put(mLCProjectInfoSet, SysConst.DELETE);
			}
		}else if("UPDATE||MAIN".equals(mOperate)){
			LCProjectInfoSet tLCProjectInfoSet = new LCProjectInfoSet();
			LCProjectInfoDB tLCProjectInfoDB = new LCProjectInfoDB();
			tLCProjectInfoDB.setProjectNo(mLCProjectInfoSchema.getProjectNo());
			tLCProjectInfoSet = tLCProjectInfoDB.query();
			if (tLCProjectInfoSet == null || tLCProjectInfoSet.size() != 1){
				this.mErrors.copyAllErrors(tLCProjectInfoDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ProjectInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目编号为"+ mLCProjectInfoSchema.getProjectNo()+ "的项目不存在，无法修改！";
				this.mErrors.addOneError(tError);
				return false;
			}
			LCProjectInfoSchema tempLCProjectInfoSchema = new LCProjectInfoSchema();
			tempLCProjectInfoSchema = tLCProjectInfoSet.get(1);
			//01 录入  04分公司不通过 06总公司不通过，其余状态不可以修改数据。
			String oldState = tempLCProjectInfoSchema.getState();
			if(!"01".equals(oldState) && !"04".equals(oldState) && !"06".equals(oldState)){
				String tSQL = "select codename from ldcode where codetype = 'projectstate' and code = '"+oldState+"'";
				String tStateName = new ExeSQL().getOneValue(tSQL);
				CError tError = new CError();
				tError.moduleName = "ProjectInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目状态为【"+tStateName+"】，不允许修改！";
				this.mErrors.addOneError(tError);
				return false;
			}
			mLCProjectInfoSchema.setState(tempLCProjectInfoSchema.getState());
			mLCProjectInfoSchema.setCreateDate(tempLCProjectInfoSchema.getCreateDate());
			mLCProjectInfoSchema.setCreateTime(tempLCProjectInfoSchema.getCreateTime());
			//mLCProjectInfoSchema.setManageCom(mGlobalInput.ManageCom);
			mLCProjectInfoSchema.setOperator(mGlobalInput.Operator);
			mLCProjectInfoSchema.setMakeDate(tempLCProjectInfoSchema.getMakeDate());
			mLCProjectInfoSchema.setMakeTime(tempLCProjectInfoSchema.getMakeTime());
			mLCProjectInfoSchema.setModifyDate(CurrentDate);
			mLCProjectInfoSchema.setModifyTime(CurrentTime);
			//add by zjd 同步到社保项目要素表中
			LCGrpContSubSet tLCGrpContSubSet = new LCGrpContSubSet();
			LCGrpContSubDB tLCGrpContSubDB=new LCGrpContSubDB();
			tLCGrpContSubDB.setProjectNo(mLCProjectInfoSchema.getProjectNo());
			tLCGrpContSubSet=tLCGrpContSubDB.query();
			if(tLCGrpContSubSet!=null && tLCGrpContSubSet.size()> 0){
				for(int i=1 ;i<=tLCGrpContSubSet.size();i++){
					tLCGrpContSubSet.get(i).setProjectName(mLCProjectInfoSchema.getProjectName());
					tLCGrpContSubSet.get(i).setModifyDate(PubFun.getCurrentDate());
					tLCGrpContSubSet.get(i).setModifyTime(PubFun.getCurrentTime());
				}
			}
			mMap.put(tLCGrpContSubSet, SysConst.UPDATE);
			mMap.put(this.mLCProjectInfoSchema, SysConst.UPDATE);
			
//			一下处理年度			
			LCProjectYearInfoSet tLCProjectYearInfoSet = new LCProjectYearInfoSet();
			LCProjectYearInfoDB tLCProjectYearInfoDB = new LCProjectYearInfoDB();
			tLCProjectYearInfoSet = tLCProjectYearInfoDB.executeQuery("select * from LCProjectYearInfo where projectno = '"+mLCProjectInfoSchema.getProjectNo()+"' order by projectyear ");
//			if (tLCProjectYearInfoSet == null || tLCProjectYearInfoSet.size() <=0){
//				this.mErrors.copyAllErrors(tLCProjectInfoDB.mErrors);
//				CError tError = new CError();
//				tError.moduleName = "ProjectInfoBL";
//				tError.functionName = "dealData";
//				tError.errorMessage = "项目编号为"+ mLCProjectInfoSchema.getProjectNo()+ "的项目年度信息不存在，无法修改！";
//				this.mErrors.addOneError(tError);
//				return false;
//			}
			if(tLCProjectYearInfoSet!=null && tLCProjectYearInfoSet.size()>0){
				
				LCProjectYearInfoSchema oldLCProjectYearInfoSchema = new LCProjectYearInfoSchema();
				oldLCProjectYearInfoSchema = tLCProjectYearInfoSet.get(1);
				if(Integer.parseInt(oldLCProjectYearInfoSchema.getProjectYear()) < Integer.parseInt(mLCProjectYearInfoSchema.getProjectYear())){
					this.mErrors.copyAllErrors(tLCProjectInfoDB.mErrors);
					CError tError = new CError();
					tError.moduleName = "ProjectInfoBL";
					tError.functionName = "dealData";
					tError.errorMessage = "项目编号为"+ mLCProjectInfoSchema.getProjectNo()+ "的项目年度信息，变更后的首期年度"+mLCProjectYearInfoSchema.getProjectYear()+"应小于变更前的首期年度"+oldLCProjectYearInfoSchema.getProjectYear()+"，无法修改！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if (oldLCProjectYearInfoSchema.getProjectYear().equals(mLCProjectYearInfoSchema.getProjectYear())){
					mLCProjectYearInfoSchema.setState(oldLCProjectYearInfoSchema.getState());
					mLCProjectYearInfoSchema.setFirstOperator(oldLCProjectYearInfoSchema.getFirstOperator());
					mLCProjectYearInfoSchema.setCreateDate(oldLCProjectYearInfoSchema.getCreateDate());
					mLCProjectYearInfoSchema.setCreateTime(oldLCProjectYearInfoSchema.getCreateTime());
					mLCProjectYearInfoSchema.setMakeDate(oldLCProjectYearInfoSchema.getMakeDate());
					mLCProjectYearInfoSchema.setMakeTime(oldLCProjectYearInfoSchema.getMakeTime());
					//mLCProjectYearInfoSchema.setManageCom(mGlobalInput.ManageCom);
					mLCProjectYearInfoSchema.setOperator(mGlobalInput.Operator);
					mLCProjectYearInfoSchema.setModifyDate(CurrentDate);
					mLCProjectYearInfoSchema.setModifyTime(CurrentTime);
					
					mMap.put(this.mLCProjectYearInfoSchema, SysConst.UPDATE);
				}else{ //新增
					mLCProjectYearInfoSchema.setState("01");
					mLCProjectYearInfoSchema.setFirstOperator(mGlobalInput.Operator);
					mLCProjectYearInfoSchema.setCreateDate(CurrentDate);
					mLCProjectYearInfoSchema.setCreateTime(CurrentTime);
					mLCProjectYearInfoSchema.setMakeDate(CurrentDate);
					mLCProjectYearInfoSchema.setMakeTime(CurrentTime);
					//mLCProjectYearInfoSchema.setManageCom(mGlobalInput.ManageCom);
					mLCProjectYearInfoSchema.setOperator(mGlobalInput.Operator);
					mLCProjectYearInfoSchema.setModifyDate(CurrentDate);
					mLCProjectYearInfoSchema.setModifyTime(CurrentTime);
					
					mMap.put(this.mLCProjectYearInfoSchema, SysConst.INSERT);
				}
				
			}else{ //新增
				mLCProjectYearInfoSchema.setState("01");
				mLCProjectYearInfoSchema.setFirstOperator(mGlobalInput.Operator);
				mLCProjectYearInfoSchema.setCreateDate(CurrentDate);
				mLCProjectYearInfoSchema.setCreateTime(CurrentTime);
				mLCProjectYearInfoSchema.setMakeDate(CurrentDate);
				mLCProjectYearInfoSchema.setMakeTime(CurrentTime);
				//mLCProjectYearInfoSchema.setManageCom(mGlobalInput.ManageCom);
				mLCProjectYearInfoSchema.setOperator(mGlobalInput.Operator);
				mLCProjectYearInfoSchema.setModifyDate(CurrentDate);
				mLCProjectYearInfoSchema.setModifyTime(CurrentTime);
				
				mMap.put(this.mLCProjectYearInfoSchema, SysConst.INSERT);
			}
//			项目启用后，修改记录轨迹。
			if("05".equals(mLCProjectInfoSchema.getState())){
				
			}
		}
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

}
