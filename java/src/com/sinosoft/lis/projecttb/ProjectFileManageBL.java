/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ProjectFileManageBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();


	//录入
	private String mProjectNo = "";
	private String mFileNo = "";


	private MMap mMap = new MMap();

	public ProjectFileManageBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	////////////////////////////////////////////////
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mFileNo = (String) tTransferData.getValueByName("FileNo");
		mProjectNo = (String) tTransferData.getValueByName("ProjectNo");
		if(mFileNo == null || "".equals(mFileNo)){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "获取文件编码失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if(mProjectNo == null || "".equals(mProjectNo)){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "项目未建立，或获取项目编码失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;

	}

	private boolean dealData() {
		
		if("INSERT||MAIN".equals(mOperate)){
			
		}else if("UPDATE||MAIN".equals(mOperate)){
			
		}else if ("DELETE||MAIN".equals(mOperate)) {
			LCProjectFileManageSet tLCProjectFileManageSet = new LCProjectFileManageSet();
			LCProjectFileManageDB tLCProjectFileManageDB = new LCProjectFileManageDB();
			String tSQL = "select * from LCProjectFileManage where FileNo = '"+mFileNo+"' and ProjectNo = '"+mProjectNo+"'";
			tLCProjectFileManageSet = tLCProjectFileManageDB.executeQuery(tSQL);
			if (tLCProjectFileManageSet == null || tLCProjectFileManageSet.size() != 1) {
				this.mErrors.copyAllErrors(tLCProjectFileManageDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "ProjectInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "项目为"+ mProjectNo+ "政策编码为"+mFileNo+"的文件不存在，无法删除！";
				this.mErrors.addOneError(tError);
				return false;
			} else {
				LCProjectFileManageSchema tLCProjectFileManageSchema = tLCProjectFileManageSet.get(1);
				tLCProjectFileManageSchema.setState("02");
				tLCProjectFileManageSchema.setOperator(mGlobalInput.Operator);
				tLCProjectFileManageSchema.setModifyDate(CurrentDate);
				tLCProjectFileManageSchema.setModifyTime(CurrentTime);
				mMap.put(tLCProjectFileManageSchema, SysConst.UPDATE);
			}
		}
		return true;

	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

}
