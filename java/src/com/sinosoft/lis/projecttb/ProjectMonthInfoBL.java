/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

public class ProjectMonthInfoBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();


	//录入
	//private LCProjectMonthInfoSchema mLCProjectMonthInfoSchema = new LCProjectMonthInfoSchema();
	private LCProjectMonthInfoSet mLCProjectMonthInfoSet = new LCProjectMonthInfoSet();


	private MMap mMap = new MMap();
	
	public ProjectMonthInfoBL(){
		
	}
	

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败LCProjectMonthInfoBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start LCProjectMonthInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mLCProjectMonthInfoSet.set((LCProjectMonthInfoSet) cInputData.getObjectByObjectName("LCProjectMonthInfoSet", 0));
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		if(mLCProjectMonthInfoSet == null || mLCProjectMonthInfoSet.size()<1){
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoBL";
			tError.functionName = "dealData";
			tError.errorMessage = "获取待处理的费用月度费用信息失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData() {
		
		LCProjectMonthInfoSet tLCProjectMonthInfoSet = new LCProjectMonthInfoSet();
		LCProjectMonthInfoDB tLCProjectMonthInfoDB = new LCProjectMonthInfoDB();
		
		if("INSERT||MAIN".equals(mOperate)){
//			是否有重复数据
			for(int i=1;i<=mLCProjectMonthInfoSet.size();i++){
				LCProjectMonthInfoSchema atempSchema = mLCProjectMonthInfoSet.get(i);
				for(int j=i+1;j<=mLCProjectMonthInfoSet.size();j++){
					LCProjectMonthInfoSchema btempSchema = mLCProjectMonthInfoSet.get(j);
					if(atempSchema.getProjectYear().equals(btempSchema.getProjectYear()) 
							&& atempSchema.getPremYear().equals(btempSchema.getPremYear())
							&& atempSchema.getProjectMonth().equals(btempSchema.getProjectMonth()) 
							&& atempSchema.getProjectType().equals(btempSchema.getProjectType())){
						CError tError = new CError();
						tError.moduleName = "LCProjectMonthInfoBL";
						tError.functionName = "dealData";
						tError.errorMessage = "第"+i+"行与第"+j+"行的项目年度、费用年度、费用月度和费用类型相同，无法保存！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
			}
			for(int i=1;i<=mLCProjectMonthInfoSet.size();i++){
				LCProjectMonthInfoSchema tLCProjectMonthInfoSchema = new LCProjectMonthInfoSchema();
				tLCProjectMonthInfoSchema = mLCProjectMonthInfoSet.get(i);
				System.out.println("录入时月度费用编号："+tLCProjectMonthInfoSchema.getMonthFeeNo());
//				校验该月度费用编码是否存在
				if(tLCProjectMonthInfoSchema.getMonthFeeNo()!=null && !"".equals(tLCProjectMonthInfoSchema.getMonthFeeNo())){
					CError tError = new CError();
					tError.moduleName = "LCProjectMonthInfoBL";
					tError.functionName = "dealData";
					tError.errorMessage = "该月度费用已存在，请进行修改！";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				String tMonthFeeNo = PubFun1.CreateMaxNo("MonthFeeNo", 20);//自动生成月度费用编码
				tLCProjectMonthInfoSchema.setMonthFeeNo(tMonthFeeNo);
				tLCProjectMonthInfoSchema.setState("01");
				tLCProjectMonthInfoSchema.setManageCom(mGlobalInput.ManageCom);
				tLCProjectMonthInfoSchema.setOperator(mGlobalInput.Operator);
				tLCProjectMonthInfoSchema.setMakeDate(CurrentDate);
				tLCProjectMonthInfoSchema.setMakeTime(CurrentTime);
				tLCProjectMonthInfoSchema.setModifyDate(CurrentDate);
				tLCProjectMonthInfoSchema.setModifyTime(CurrentTime);
				
//				每个月度，每种费用类型仅能录入一次
				if(!checkMonthInfo(tLCProjectMonthInfoSchema)){
					return false;
				}
				tLCProjectMonthInfoSet.add(tLCProjectMonthInfoSchema);
			}
			
			mMap.put(tLCProjectMonthInfoSet, SysConst.INSERT);
			
		} else	if (mOperate.equals("DELETE||MAIN")){
			if(mLCProjectMonthInfoSet == null || mLCProjectMonthInfoSet.size()!=1){
				CError tError = new CError();
				tError.moduleName = "LCProjectMonthInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取传入的费用月度费用信息失败，无法删除！";
				this.mErrors.addOneError(tError);
				return false;
			}
			LCProjectMonthInfoSchema tLCProjectMonthInfoSchema = mLCProjectMonthInfoSet.get(1);
			String getMonthSql = "select * from LCProjectMonthInfo where MonthFeeNo = '"+tLCProjectMonthInfoSchema.getMonthFeeNo()+"'";
			tLCProjectMonthInfoSet = tLCProjectMonthInfoDB.executeQuery(getMonthSql);
			if (tLCProjectMonthInfoSet == null || tLCProjectMonthInfoSet.size() != 1) {
				this.mErrors.copyAllErrors(tLCProjectMonthInfoDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "LCProjectMonthInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取费用月度费用信息失败，无法删除！";
				this.mErrors.addOneError(tError);
				return false;
			}
			LCProjectMonthInfoSchema oldLCProjectMonthInfoSchema = new LCProjectMonthInfoSchema();
			oldLCProjectMonthInfoSchema = tLCProjectMonthInfoSet.get(1);
			if("03".equals(oldLCProjectMonthInfoSchema.getState())){
				CError tError = new CError();
				tError.moduleName = "LCProjectMonthInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "月度费用信息已通过审核，不允许删除！";
				this.mErrors.addOneError(tError);
				return false;
			}
			mMap.put(tLCProjectMonthInfoSchema, SysConst.DELETE);
			
		}else if("UPDATE||MAIN".equals(mOperate)){
			if(mLCProjectMonthInfoSet == null || mLCProjectMonthInfoSet.size()!=1){
				CError tError = new CError();
				tError.moduleName = "LCProjectMonthInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取传入的费用月度费用信息失败，无法修改！";
				this.mErrors.addOneError(tError);
				return false;
			}
			LCProjectMonthInfoSchema tLCProjectMonthInfoSchema = mLCProjectMonthInfoSet.get(1);
			String getMonthSql = "select * from LCProjectMonthInfo where MonthFeeNo = '"+tLCProjectMonthInfoSchema.getMonthFeeNo()+"'";
			tLCProjectMonthInfoSet = tLCProjectMonthInfoDB.executeQuery(getMonthSql);
			if (tLCProjectMonthInfoSet == null || tLCProjectMonthInfoSet.size() != 1){
				this.mErrors.copyAllErrors(tLCProjectMonthInfoDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "LCProjectMonthInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "获取费用月度费用信息失败，不允许修改！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			LCProjectMonthInfoSchema oldLCProjectMonthInfoSchema = new LCProjectMonthInfoSchema();
			oldLCProjectMonthInfoSchema = tLCProjectMonthInfoSet.get(1);
			if("03".equals(oldLCProjectMonthInfoSchema.getState())){
				CError tError = new CError();
				tError.moduleName = "LCProjectMonthInfoBL";
				tError.functionName = "dealData";
				tError.errorMessage = "月度费用信息已通过审核，无法修改！";
				this.mErrors.addOneError(tError);
				return false;
			}
//			每个月度，每种费用类型仅能录入一次
			if(!checkMonthInfo(tLCProjectMonthInfoSchema)){
				return false;
			}
			
			tLCProjectMonthInfoSchema.setState(oldLCProjectMonthInfoSchema.getState());
			tLCProjectMonthInfoSchema.setMakeDate(oldLCProjectMonthInfoSchema.getMakeDate());
			tLCProjectMonthInfoSchema.setMakeTime(oldLCProjectMonthInfoSchema.getMakeTime());
			tLCProjectMonthInfoSchema.setManageCom(mGlobalInput.ManageCom);
			tLCProjectMonthInfoSchema.setOperator(mGlobalInput.Operator);
			tLCProjectMonthInfoSchema.setModifyDate(CurrentDate);
			tLCProjectMonthInfoSchema.setModifyTime(CurrentTime);
			
			mMap.put(tLCProjectMonthInfoSchema, SysConst.UPDATE);
		}
		return true;
	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}
//	 每个月度，每种费用类型仅能录入一次
	private boolean checkMonthInfo(LCProjectMonthInfoSchema aLCProjectMonthInfoSchema){
		LCProjectMonthInfoSet aLCProjectMonthInfoSet = new LCProjectMonthInfoSet();
		LCProjectMonthInfoDB aLCProjectMonthInfoDB = new LCProjectMonthInfoDB();
		String getMonthSql = "select * from LCProjectMonthInfo "
	           + " where ProjectNo = '"+aLCProjectMonthInfoSchema.getProjectNo()+"' "
	           + " and ProjectYear = '"+aLCProjectMonthInfoSchema.getProjectYear()+"' "
	           + " and PremYear = '"+aLCProjectMonthInfoSchema.getPremYear()+"' "
	           + " and ProjectMonth = '"+aLCProjectMonthInfoSchema.getProjectMonth()+"'"
	           + " and ProjectType = '"+aLCProjectMonthInfoSchema.getProjectType()+"'"
	           + " and MonthFeeNo != '"+aLCProjectMonthInfoSchema.getMonthFeeNo()+"'";
		aLCProjectMonthInfoSet = aLCProjectMonthInfoDB.executeQuery(getMonthSql);
		if (aLCProjectMonthInfoSet != null && aLCProjectMonthInfoSet.size()>0){
			CError tError = new CError();
			tError.moduleName = "LCProjectYearInfoBL";
			tError.functionName = "dealData";
			tError.errorMessage = "项目编号为"+ aLCProjectMonthInfoSchema.getProjectNo()
			                    + ",项目年度为"+aLCProjectMonthInfoSchema.getProjectYear()
			                    + ",费用年度为"+aLCProjectMonthInfoSchema.getPremYear()
			                    + ",费用月度为"+aLCProjectMonthInfoSchema.getProjectMonth()
			                    + ",费用类型为"+aLCProjectMonthInfoSchema.getProjectType()
			                    +"的数据已存在，请选中该月度进行修改！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}


