/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.projecttb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ProjectMonthInfoSendUWBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	
	Reflections tReflections = new Reflections();

	//录入
	private String mProjectNo = "";

	private MMap mMap = new MMap();

	public ProjectMonthInfoSendUWBL() {
	}

	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		//进行业务处理
		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败ProjectInfoSendUWBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}

		//准备往后台的数据
		if (!prepareOutputData()){
			return false;
		}
		
//		保存数据
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start ProjectInfoBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	////////////////////////////////////////////////
	/**
	 * 从输入数据中得到所有对象
	 * @param cInputData VData
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
		TransferData tTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
		mProjectNo = (String) tTransferData.getValueByName("ProjectNo");
		if(mProjectNo == null || "".equals(mProjectNo)){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "项目未建立，或获取项目编码失败！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData(){
//		处理项目信息，并添加项目轨迹
		LCProjectMonthInfoSet tLCProjectMonthInfoSet = new LCProjectMonthInfoSet();
		LCProjectMonthInfoDB tLCProjectMonthInfoDB = new LCProjectMonthInfoDB();
		String tSQL = "select * from LCProjectMonthInfo where ProjectNo = '"+mProjectNo+"' and state in ('01','04') ";//录入和审核不通过的才需要送审
		tLCProjectMonthInfoSet = tLCProjectMonthInfoDB.executeQuery(tSQL);
		if(tLCProjectMonthInfoSet == null || tLCProjectMonthInfoSet.size() < 1){
			CError tError = new CError();
			tError.moduleName = "ProjectInfoSendUWBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有需要送审的月度费用信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		LCProjectMonthTraceSet tLCProjectMonthTraceSet = new LCProjectMonthTraceSet();
		LCProjectMonthTraceSchema tLCProjectMonthTraceSchema = new LCProjectMonthTraceSchema();
		tLCProjectMonthTraceSet.add(tLCProjectMonthTraceSchema);
        tReflections.transFields(tLCProjectMonthTraceSet,tLCProjectMonthInfoSet);
        
        LCProjectPremUWSet tLCProjectPremUWSet = new LCProjectPremUWSet();
        for(int i=1;i<=tLCProjectMonthTraceSet.size();i++){
        	String tMonthSerialNo = PubFun1.CreateMaxNo("MonthSerialNo", 20);
        	tLCProjectMonthTraceSet.get(i).setMonthSerialNo(tMonthSerialNo);
        	
        	String tMonthFeeNo = tLCProjectMonthTraceSet.get(i).getMonthFeeNo();
//          月度费用审核表
        	LCProjectPremUWSchema tLCProjectPremUWSchema = new LCProjectPremUWSchema();
            String tUWSerialNo = PubFun1.CreateMaxNo("MonthUWSerialNo", 20);
            tLCProjectPremUWSchema.setSerialNo(tUWSerialNo);
            tLCProjectPremUWSchema.setMonthSerialNo(tMonthSerialNo);
            tLCProjectPremUWSchema.setMonthFeeNo(tMonthFeeNo);
            tLCProjectPremUWSchema.setSendOperator(mGlobalInput.Operator);
            tLCProjectPremUWSchema.setSendDate(CurrentDate);
            tLCProjectPremUWSchema.setAuditFlag("01");
            String tAuditTimes = getAuditTimes(tMonthFeeNo);
            tLCProjectPremUWSchema.setAuditTimes(tAuditTimes);
            tLCProjectPremUWSchema.setManageCom(mGlobalInput.ManageCom);
            tLCProjectPremUWSchema.setOperator(mGlobalInput.Operator);
            tLCProjectPremUWSchema.setMakeDate(CurrentDate);
            tLCProjectPremUWSchema.setMakeTime(CurrentTime);
            tLCProjectPremUWSchema.setModifyDate(CurrentDate);
            tLCProjectPremUWSchema.setModifyTime(CurrentTime);
            
            tLCProjectPremUWSet.add(tLCProjectPremUWSchema);
        }
        for(int i=1;i<=tLCProjectMonthInfoSet.size();i++){
        	tLCProjectMonthInfoSet.get(i).setState("02");
        	tLCProjectMonthInfoSet.get(i).setModifyDate(CurrentDate);
        	tLCProjectMonthInfoSet.get(i).setModifyTime(CurrentTime);
        }
		
		mMap.put(tLCProjectMonthTraceSet, SysConst.INSERT);
		mMap.put(tLCProjectPremUWSet, SysConst.INSERT);
		mMap.put(tLCProjectMonthInfoSet, SysConst.UPDATE);
		
		return true;

	}
	
	private String getAuditTimes(String aMonthFeeNo){
		String aAuditTimes = "";
		String aAuditTimesSql = "select count(distinct MonthSerialNo) from LCProjectPremUW where MonthFeeNo = '"+aMonthFeeNo+"'";
		aAuditTimes = new ExeSQL().getOneValue(aAuditTimesSql);
		if(aAuditTimes == null || "".equals(aAuditTimes)){
			aAuditTimes = "0";
		}else{
			aAuditTimes = (Integer.parseInt(aAuditTimes) +1)+"";
		}
		return aAuditTimes;
	}

	/**
	 * 准备后台的数据
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			this.mInputData.add(mMap);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ProjectInfoBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	//////////////////////////////////////////////

	public VData getResult() {
		this.mResult.clear();
		return mResult;
	}

}
