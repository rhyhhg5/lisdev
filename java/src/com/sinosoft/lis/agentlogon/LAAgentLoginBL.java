package com.sinosoft.lis.agentlogon;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 用户业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author DingZhong
 * @version 1.0
 */
public class LAAgentLoginBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    /** 菜单组、菜单组到菜单的相关信息*/
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    LAAgentSet mLAAgentSet = new LAAgentSet();


    String mResultStr = "";
    int mResultNum = 0;

    public LAAgentLoginBL()
    {
        // just for debug

    }

    public static void main(String[] args)
    {

        LAAgentLoginBL tLAAgentLoginBL1 = new LAAgentLoginBL();

        LAAgentSchema tSchema = new LAAgentSchema();
        tSchema.setAgentCode("001");
        tSchema.setPassword("001");
        tSchema.setManageCom("86150000");

        VData tVData = new VData();
        tVData.add(tSchema);

        if (!tLAAgentLoginBL1.submitData(tVData, "query"))
        {
            System.out.println("ppp");
        }

        System.out.println("kkkk");
        for (int i = 0; i < tLAAgentLoginBL1.mErrors.getErrorCount(); i++)
        {
            CError tError = tLAAgentLoginBL1.mErrors.getError(i);
            System.out.println(tError.errorMessage);
            System.out.println(tError.moduleName);
            System.out.println(tError.functionName);
            System.out.println("----------------");
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        // 判断操作是不是查询
        if (cOperate != "query")
        {
            return false;
        }

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中

        System.out.println("start get inputdata...");
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---getInputData---");

        //进行业务处理
        if (!queryLAAgent())
        {
            return false;
        }

//System.out.println("---queryLAAgent---");

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


    public int getResultNum()
    {
        return mResultNum;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
        mLAAgentSchema = (LAAgentSchema) cInputData.getObjectByObjectName(
                "LAAgentSchema", 0);

        if (mLAAgentSchema == null)
        {
            return false;
        }

        return true;
    }

    /**
     * 查询符合条件的信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryLAAgent()
    {
        String encryptPwd = mLAAgentSchema.getPassword();
//
        //LisIDEA tIdea = new LisIDEA();
        //  String decryptPwd = tIdea.decryptString(encryptPwd);
        // decryptPwd = decryptPwd.trim();
        // System.out.println("now encryptPwd");
        //  System.out.println("decrypt pwd:" + decryptPwd);
        String sqlStr = "select * from LAAgent where Agentcode = '" +
                        mLAAgentSchema.getAgentCode() + "' ";
        sqlStr += "and password = '" + encryptPwd + "'";

        System.out.println(sqlStr);
        LAAgentDB tLAAgentDB = mLAAgentSchema.getDB();

        mLAAgentSet = tLAAgentDB.executeQuery(sqlStr);
        System.out.println("here here here");
        //*********************过渡代码*******************
         /*
           LisIDEA tIdea = new LisIDEA();
           if (mLAAgentSet.size() == 0) {
               String decryptPwd = tIdea.decryptString(encryptPwd);
          sqlStr = "select * from LAAgent where Agentcode = '" + mLAAgentSchema.getAgentCode() + "' ";
          sqlStr += "and password = '" + decryptPwd + "'";
               System.out.println(sqlStr);
               mLAAgentSet = tLAAgentDB.executeQuery(sqlStr);
           }
          */
         //******************************************************/
         if (tLAAgentDB.mErrors.needDealError())
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
             CError tError = new CError();
             tError.moduleName = "LAAgentLoginBL";
             tError.functionName = "queryLAAgent";
             tError.errorMessage = "用户查询失败!";
             this.mErrors.addOneError(tError);
             mLAAgentSet.clear();
             return false;
         }

        if (mLAAgentSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentLoginBL";
            tError.functionName = "queryLAAgent";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);

            mLAAgentSet.clear();
            return false;
        }
        System.out.println("here gose:");

        String ManageCom = mLAAgentSchema.getManageCom();
        //judge if ManageCom is valid station

        String strSql =
                "select * from ldcode where codetype = 'station' and code = " +
                ManageCom;
        System.out.println(strSql);
        LDCodeSet tCodeSet = new LDCodeSet();
        LDCodeDB tCodeDB = new LDCodeDB();
        tCodeSet = tCodeDB.executeQuery(strSql);
        if (tCodeSet == null || tCodeSet.size() == 0)
        {
            return false;
        }

        System.out.println("ManageCom:" + ManageCom);
        int matchCount = 0;
        for (int i = 1; i <= mLAAgentSet.size(); i++)
        {
            LAAgentSchema tLAAgentSchema = mLAAgentSet.get(i);
            String getManageCom = tLAAgentSchema.getManageCom();
            System.out.println("getManageCom:" + getManageCom);
            System.out.println("*************************");
            if (getManageCom.length() > ManageCom.length())
            {
                continue;
            }
            int j = 0;
            for (; j < getManageCom.length(); j++)
            {
                if (ManageCom.charAt(j) != getManageCom.charAt(j))
                {
                    break;
                }
            }

            if (j == getManageCom.length())
            {
                matchCount++;
            }
        }
        System.out.println("matchCount:" + matchCount);
        if (matchCount == 0)
        {
            return false;
        }
        mResult.add(mLAAgentSet);
        mResultNum = mLAAgentSet.size();

        System.out.println(mResult);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        try
        {
            mResult.add(mLAAgentSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentLoginBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
