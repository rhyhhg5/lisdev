package com.sinosoft.lis.agentlogon;

import com.sinosoft.lis.db.LAAgentMenuDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentMenuGrpSchema;
import com.sinosoft.lis.schema.LAAgentMenuSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentMenuSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 菜单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author DingZhong
 * @version 1.0
 */
public class LAAgentMenuQueryBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    /** 菜单组、菜单组到菜单的相关信息*/
    LAAgentMenuSchema mLAAgentMenuSchema = new LAAgentMenuSchema();
    LAAgentMenuSet mLAAgentMenuSet = new LAAgentMenuSet();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    LATreeSchema mLATreeSchema = new LATreeSchema();

    LAAgentMenuGrpSchema mLAAgentMenuGrpSchema = new LAAgentMenuGrpSchema();

    String mResultStr = "";
    int mResultNum = 0;

    public LAAgentMenuQueryBL()
    {
        // just for debug

    }

    public static void main(String[] args)
    {

        LAAgentMenuQueryBL tLAAgentMenuQueryBL1 = new LAAgentMenuQueryBL();

        LAAgentMenuGrpSchema tSchema = new LAAgentMenuGrpSchema();
        tSchema.setMenuGrpCode("007");

//      LAAgentSchema tAgentSchema = new LAAgentSchema();
//      tAgentSchema.setAgentCode("101");



        VData tVData = new VData();
        tVData.add(tSchema);

        tLAAgentMenuQueryBL1.submitData(tVData, "query");
        String str = tLAAgentMenuQueryBL1.getResultStr();
        System.out.println(str);

        for (int i = 0; i < tLAAgentMenuQueryBL1.mErrors.getErrorCount(); i++)
        {
            CError tError = tLAAgentMenuQueryBL1.mErrors.getError(i);
            System.out.println(tError.errorMessage);
            System.out.println(tError.moduleName);
            System.out.println(tError.functionName);
            System.out.println("----------------");
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        // 判断操作是不是查询
        if (cOperate != "query" && cOperate != "query_remain")
        {
            System.out.println("Operater is denied");
            return false;
        }
        System.out.println("start submit...");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---getInputData---");

        //进行业务处理
        if (cOperate == "query" && !queryMenu())
        {
            return false;
        }
        else if (cOperate == "query_remain" && !queryRemainMenu())
        {
            return false;
        }

        System.out.println("---queryMenuGrpToMenu---");

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public int getResultNum()
    {
        return mResultNum;
    }

    public String getResultStr()
    {
        String resultStr = "";

        for (int i = 1; i <= mResultNum; i++)
        {
            mLAAgentMenuSchema = mLAAgentMenuSet.get(i);
            if (i > 1)
            {
                resultStr += SysConst.RECORDSPLITER;
            }
            resultStr += mLAAgentMenuSchema.getParentNodeCode();
            resultStr += "|" + mLAAgentMenuSchema.getNodeCode();
            resultStr += "|" + mLAAgentMenuSchema.getNodeName();
            resultStr += "|" + mLAAgentMenuSchema.getChildFlag();
            resultStr += "|" + mLAAgentMenuSchema.getRunScript();
            /*
                    mLAAgentMenuSchema=mLAAgentMenuSet.get(i);
                    resultStr+=mLAAgentMenuSchema.getParentNodeCode()+"|";
                    resultStr+=mLAAgentMenuSchema.getNodeCode()+"|";
                    resultStr+=mLAAgentMenuSchema.getNodeName()+"|";
                    resultStr+=mLAAgentMenuSchema.getChildFlag()+"|";
                    resultStr+=mLAAgentMenuSchema.getRunScript()+"|";
             */
        }
        return resultStr;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
        mLAAgentSchema = (LAAgentSchema) cInputData.getObjectByObjectName(
                "LAAgentSchema", 0);

        if (mLAAgentSchema != null)
        {
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(mLAAgentSchema.getAgentCode());
            if (tLATreeDB.getInfo())
            {
                mLATreeSchema.setSchema(tLATreeDB.getSchema());
            }
            return true;
        }

        mLAAgentMenuGrpSchema = (LAAgentMenuGrpSchema) cInputData.
                                getObjectByObjectName("LAAgentMenuGrpSchema", 0);

        if (mLAAgentMenuGrpSchema != null)
        {
            return true;
        }

        mLAAgentMenuSchema = (LAAgentMenuSchema) cInputData.
                             getObjectByObjectName("LAAgentMenuSchema", 0);

        if (mLAAgentMenuSchema != null)
        {
            return true;
        }
        System.out.println("completed get input data");
        return true;
    }

    /**
     * 查询符合条件的菜单的信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryMenu()
    {
        LAAgentMenuSchema tLAAgentMenuSchema = new LAAgentMenuSchema();
        LAAgentMenuDB tLAAgentMenuDB = tLAAgentMenuSchema.getDB();

        if (mLAAgentSchema != null)
        { //通过用户编码查询菜单集合
            System.out.println("query by LAAgentSchema");
            // 构造查询语句
            String sqlStr = "select * from LAAgentMenu ";
            sqlStr = sqlStr +
                     "where NodeCode in ( select NodeCode from LAAgentMenuGrpToMenu ";
            sqlStr = sqlStr +
                     "where MenuGrpCode in ( select MenuGrpCode from LAAgentMenuGrp ";
            sqlStr = sqlStr + "where MenuGrpCode= '" +
                     mLATreeSchema.getAgentGrade() + "";
            sqlStr = sqlStr + "')  )    order by nodeorder";

            mLAAgentMenuSet = tLAAgentMenuDB.executeQuery(sqlStr);
            System.out.println(sqlStr);
        }
        else if (mLAAgentMenuGrpSchema != null)
        { //通过菜单组编码查询菜单集合
            // 构造查询语句
            System.out.println("query by MenuGrpSchema");
            String sqlStr = "select * from LAAgentMenu ";
            sqlStr = sqlStr +
                     "where NodeCode in ( select NodeCode from LAAgentMenuGrpToMenu ";
            sqlStr = sqlStr +
                     "where MenuGrpCode in ( select MenuGrpCode from LAAgentMenuGrp ";
            sqlStr = sqlStr + "where MenuGrpCode ='" +
                     mLAAgentMenuGrpSchema.getMenuGrpCode() + "'";
            sqlStr = sqlStr + ")  )";
            sqlStr = sqlStr + " order by nodeorder";
            System.out.println(sqlStr);

            LAAgentMenuSet tLAAgentMenuSet = tLAAgentMenuDB.executeQuery(sqlStr);
            System.out.println(tLAAgentMenuSet.size());
            mLAAgentMenuSet = tLAAgentMenuDB.executeQuery(sqlStr);

        }
        else if (mLAAgentMenuSchema != null)
        { //通过节点编码查询
            mLAAgentMenuSet = tLAAgentMenuDB.query();
        }
        else
        { //默认返回全部节点
            mLAAgentMenuSet = tLAAgentMenuDB.executeQuery(
                    "select * from LAAgentMenu order by nodeorder");
        }

        if (tLAAgentMenuDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentMenuDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentMenuQueryBL";
            tError.functionName = "queryMenu";
            tError.errorMessage = "用户菜单查询失败!";
            this.mErrors.addOneError(tError);
            mLAAgentMenuSet.clear();
            return false;
        }

        if (mLAAgentMenuSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentMenuBL";
            tError.functionName = "queryMenu";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            mLAAgentMenuSet.clear();
            return false;
        }
        mResultNum = mLAAgentMenuSet.size();
        mResult.add(mLAAgentMenuSet);
        System.out.println(mResult);
        return true;
    }


    /**
     * 查询符合条件的菜单的信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryRemainMenu()
    {
        LAAgentMenuSchema tLAAgentMenuSchema = new LAAgentMenuSchema();
        LAAgentMenuDB tLAAgentMenuDB = tLAAgentMenuSchema.getDB();

        if (mLAAgentSchema != null)
        { //通过用户编码查询菜单集合
            System.out.println("query by LAAgentSchema");
            // 构造查询语句
            String sqlStr = "select * from LAAgentMenu ";
            sqlStr = sqlStr +
                     "where NodeCode not in ( select NodeCode from LAAgentMenuGrpToMenu ";
            sqlStr = sqlStr +
                     "where MenuGrpCode in ( select MenuGrpCode from LAAgentMenuGrp ";
            sqlStr = sqlStr +
                     "where MenuGrpCode in (select MenuGrpCode from LAAgentToMenuGrp ";
            sqlStr = sqlStr + "where AgentCode = " +
                     mLAAgentSchema.getAgentCode() + "";
            sqlStr = sqlStr + ")  )   ) order by nodeorder";

            mLAAgentMenuSet = tLAAgentMenuDB.executeQuery(sqlStr);

        }
        else if (mLAAgentMenuGrpSchema != null)
        { //通过菜单组编码查询菜单集合
            // 构造查询语句
            String sqlStr = "select * from LAAgentMenu ";
            sqlStr = sqlStr +
                     "where NodeCode not in ( select NodeCode from LAAgentMenuGrpToMenu ";
            sqlStr = sqlStr +
                     "where MenuGrpCode in ( select MenuGrpCode from LAAgentMenuGrp ";
            sqlStr = sqlStr + "where MenuGrpCode ='" +
                     mLAAgentMenuGrpSchema.getMenuGrpCode() + "'";
            sqlStr = sqlStr + ")  )";
            sqlStr = sqlStr + " order by nodeorder";
            System.out.println(sqlStr);

            LAAgentMenuSet tLAAgentMenuSet = tLAAgentMenuDB.executeQuery(sqlStr);
            mLAAgentMenuSet = tLAAgentMenuDB.executeQuery(sqlStr);

        }
        else if (mLAAgentMenuSchema != null)
        { //通过节点编码查询
            String sqlStr = "select * from LAAgentMenu where NodeCode <> mDLMenuSchema.getNodeCode order by nodeorder";
            mLAAgentMenuSet = tLAAgentMenuDB.executeQuery(sqlStr);
        }
        else
        { //默认返回全部节点
            mLAAgentMenuSet = tLAAgentMenuDB.executeQuery(
                    "select * from LAAgentMenu order by nodeorder");
        }

        if (tLAAgentMenuDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentMenuDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentMenuQueryBL";
            tError.functionName = "queryRemainMenu";
            tError.errorMessage = "用户菜单查询失败!";
            this.mErrors.addOneError(tError);
            mLAAgentMenuSet.clear();
            return false;
        }

        if (mLAAgentMenuSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentMenuQueryBL";
            tError.functionName = "queryRemainMenu";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            mLAAgentMenuSet.clear();
            return false;
        }
        mResultNum = mLAAgentMenuSet.size();
        mResult.add(mLAAgentMenuSet);
        System.out.println(mResult);
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        try
        {
            mResult.add(mLAAgentMenuSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "MenuQueryBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
