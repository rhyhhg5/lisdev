package com.sinosoft.lis.agentlogon;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author DingZhong
 * @version 1.0
 */
public class LAAgentChangePwdBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    /** 用户的相关信息*/
    LAAgentSchema mLDOLAAgentSchema = new LAAgentSchema();
    LAAgentSchema mLDNewAgentSchema = new LAAgentSchema();

    String mResultStr = "";
    int mResultNum = 0;

    public LAAgentChangePwdBL()
    {
        // just for debug
    }

    public static void main(String[] args)
    {

        LAAgentChangePwdBL tLAAgentChangePwdBL = new LAAgentChangePwdBL();

        LAAgentSchema tOldSchema = new LAAgentSchema();
        LAAgentSchema tNewSchema = new LAAgentSchema();

        String oldPwd = "222";
        String newPwd = "333";

        for (int i = 0; i < 110; i++)
        {
            oldPwd = "333";
            newPwd = "333";
            tOldSchema.setAgentCode("002");
            tOldSchema.setPassword(oldPwd);
            tNewSchema.setAgentCode("002");
            tNewSchema.setPassword(newPwd);

            VData tVData = new VData();
            tVData.add(tOldSchema);
            tVData.add(tNewSchema);

            boolean suc = tLAAgentChangePwdBL.submitData(tVData, "changePwd");
            if (suc)
            {
                System.out.println("change successful");
            }
            else
            {
                System.out.println("change fail");
            }
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        // 判断操作是不是查询
        if (cOperate.compareTo("changePwd") != 0)
        {
            return false;
        }

        System.out.println("start BL submit...");

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("start dealData");
        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        System.out.println("After dealData！");

        return true;
    }


    private boolean dealData()
    {

        String Agentcode = mLDOLAAgentSchema.getAgentCode();
        System.out.println("oldPwd plain is :" + mLDOLAAgentSchema.getPassword());
        LisIDEA tLisIdea = new LisIDEA();

        String oldpwd = mLDOLAAgentSchema.getPassword();
        //oldpwd = tLisIdea.encryptString(oldpwd);

        System.out.println("Agentcode:" + Agentcode);
        System.out.println("oldpwd:" + oldpwd);
        System.out.println(mLDNewAgentSchema.getPassword());
        String sqlStr = "select * from LAAgent where Agentcode =  '" +
                        Agentcode + "' and password = '" + oldpwd + "'";
        System.out.println(sqlStr);
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentDB tLAAgentDB1 = tLAAgentSchema.getDB();
        LAAgentSet tLAAgentSet = tLAAgentDB1.executeQuery(sqlStr);
        LAAgentDB tLAAgentDB2 = tLAAgentSchema.getDB();

        System.out.println("start ......");
        // *********************过渡代码***************************

        if (tLAAgentDB1.mErrors.needDealError() || tLAAgentSet.size() != 1)
        {
            String decryptPwd = tLisIdea.decryptString(oldpwd);
            sqlStr = "select * from LAAgent where Agentcode =  '" + Agentcode +
                     "' and password = '" + decryptPwd + "'";
            System.out.println(sqlStr);

            tLAAgentSet = tLAAgentDB2.executeQuery(sqlStr);

        }
        //***********************************************************
         if (tLAAgentDB2.mErrors.needDealError() || tLAAgentSet.size() != 1)
         {
             this.mErrors.copyAllErrors(tLAAgentDB2.mErrors);
             CError tError = new CError();
             tError.moduleName = "LAAgentChangePwdBL";
             tError.functionName = "dealData";
             tError.errorMessage = "确认原密码出错";
             this.mErrors.addOneError(tError);
             return false;
         }
        System.out.println("old password is right");

        String newpwd = mLDNewAgentSchema.getPassword();
        if (newpwd == "")
        {
            return false;
        }

        LAAgentSchema newSchema = tLAAgentSet.get(1);
        newSchema.setPassword(newpwd);

        //开始更新用户密码
        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            System.out.println("更新密码连接数据库失败！");
            return false;

        }
        try
        {
            conn.setAutoCommit(false);

            System.out.println("Start 更新用户密码...");

            LAAgentDB tLAAgentDB = new LAAgentDB(conn);
            tLAAgentDB.setSchema(newSchema);

            //更新菜单组表
            if (!tLAAgentDB.update())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAgentChangePwdBL";
                tError.functionName = "dealData";
                tError.errorMessage = "用户表密码更新失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return mResult;
    }

    public int getResultNum()
    {
        return mResultNum;
    }

    public String getResultStr()
    {
        String resultStr = "";
        for (int i = 1; i <= mResultNum; i++)
        {

        }
        return resultStr;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
        mLDOLAAgentSchema = (LAAgentSchema) cInputData.getObjectByObjectName(
                "LAAgentSchema", 0);
        mLDNewAgentSchema = (LAAgentSchema) cInputData.getObjectByObjectName(
                "LAAgentSchema", 1);
        if (mLDOLAAgentSchema == null || mLDNewAgentSchema == null)
        {
            System.out.println("cant get password");
            return false;
        }
        System.out.println("completed get input data");
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        try
        {
            mInputData.add(mLDNewAgentSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentChangePwdBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
