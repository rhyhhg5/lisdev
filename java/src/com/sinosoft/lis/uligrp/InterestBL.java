package com.sinosoft.lis.uligrp;

import com.sinosoft.lis.db.Interest000001DB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AccountManage;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 20070306</p>
 * <p>Company: Sinosoft</p>
 * @author guoly
 * @version 1.0
 */

public class InterestBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private VData mResult = new VData();
    private GlobalInput mGlobalInput;

    private MMap map = new MMap();
    // 个人信息
    private Interest000001Schema mInterest000001Schema = new Interest000001Schema();
    private Interest000001Schema mInterest000001Schemaold = new Interest000001Schema();
    private Interest000001Set mInterest000001Set = new Interest000001Set();
    public InterestBL()
    {
    }
	public void setGlobalInput(GlobalInput aGlobalInput) {
		mGlobalInput = aGlobalInput;
	}
	public GlobalInput getGlobalInput() {
		return mGlobalInput;
	}


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        //得到输入数据
        if (!getInputData())
        {
            return false;
        }
        //检查数据合法性
        if (!checkInputData())
        {
            return false;
        }

        //进行业务处理
        if (!dealData(cOperate))
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDWorkTimeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData()
    {
        // 个人信息

    	mInterest000001Set = (Interest000001Set) mInputData.getObjectByObjectName("Interest000001Set", 0);
    	//mInterest000001Schema=mInterest000001Set.get(1);

        if (mInterest000001Set == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Interest000001BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在工作日期设定接受时没有得到足够的数据，请您确认有：完整的信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果输入数据有错，则返回false,否则返回true
    private boolean checkInputData()
    {
        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData(String cOperate)
    {
        //添加纪录
        if (cOperate.equals("INSERT"))
        {
        	Interest000001DB tInterest000001DB=new Interest000001DB();
        	tInterest000001DB.setRiskCode(mInterest000001Set.get(1).getRiskCode());
        	//tInterest000001DB.setEndDate("3000-01-01");
        	tInterest000001DB.setEndDate(mInterest000001Set.get(1).getEndDate());
        	Interest000001Set tInterest000001Set=tInterest000001DB.query();
        	if(tInterest000001Set.size()>0){
        		String StartDate=tInterest000001Set.get(1).getStartDate();
        		if(!PubFun.getBeforeDate(mInterest000001Set.get(1).getStartDate(),StartDate).equals(StartDate)){
                    CError tError = new CError();
                    tError.moduleName = "Interest000001BL.java";
                    tError.functionName = "dealData";
                    tError.errorMessage = "该险种保证利率设定的起始时间与已有的时间区间部分重复！不能保存。";
                    this.mErrors.addOneError(tError);
                    return false;
        		}
        	}
                map.put(mInterest000001Set, "INSERT");

              //清空服务器的利率缓存
              try {
                  AccountManage.deleteRateCash();
              } catch (Exception ex) {
              }

        }
        //更新纪录
        if (cOperate.equals("UPDATE"))
        {
        	mInterest000001Schemaold=mInterest000001Set.get(2);
            String sql="delete from Interest000001 where RiskCode='"
        		+ mInterest000001Schemaold.getRiskCode() +"' and StartDate='"+mInterest000001Schemaold.getStartDate()+"' and EndDate='"+mInterest000001Schemaold.getEndDate()+"'";
            map.put( sql, "DELETE");
            System.out.println(sql);
            mInterest000001Schema=mInterest000001Set.get(1);
            map.put(mInterest000001Schema, "INSERT");
            

            //清空服务器的利率缓存
            try {
                AccountManage.deleteRateCash();
            } catch (Exception ex) {
            }


        }
        //删除纪录
        if (cOperate.equals("DELETE"))
        {
            map.put(mInterest000001Set.get(1), "DELETE");

	        //清空服务器的利率缓存
	        try {
	            AccountManage.deleteRateCash();
	        } catch (Exception ex) {
	        }
        }

        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        try
        {

            mInputData.clear();
            mInputData.add(map);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "Interest000001BL.java";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //得到结果
    public VData getResult()
    {
        return this.mResult;
    }


}
