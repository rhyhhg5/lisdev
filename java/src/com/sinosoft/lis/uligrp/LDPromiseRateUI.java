package com.sinosoft.lis.uligrp;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
//import com.sinosoft.lis.schema.LDWorkTimeSchema;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保证汇率的设定
 * </p>
 * <p>Copyright: Copyright (c) 200705017</p>
 * <p>Company: Sinosoft</p>
 * @author 李磊
 * @version 1.0
 */

public class LDPromiseRateUI
{

    private VData mInputData;
    private VData mResult = new VData();
    public CErrors mErrors = new CErrors();
    
    public LDPromiseRateUI()
    {
    }



    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        LDPromiseRateBL tLDPromiseRateBL = new LDPromiseRateBL();
        tLDPromiseRateBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tLDPromiseRateBL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLDPromiseRateBL.mErrors);
        }
        //取得结果返回前台
        this.mResult.clear();
        this.mResult = tLDPromiseRateBL.getResult();
        mInputData = null;
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}

