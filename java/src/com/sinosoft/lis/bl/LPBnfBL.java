/*
 * <p>ClassName: LPBnfBL </p>
 * <p>Description: LPBnfBLSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全
 * @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LPBnfDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LPBnfSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LCBnfSet;
import com.sinosoft.lis.vschema.LPBnfSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;

public class LPBnfBL extends LPBnfSchema
{
    // @Constructor
    public CErrors mErrors = new CErrors(); // 错误信息
    public LPBnfBL()
    {}

    public void setUpdateFields()
    {
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }

    /**
     * 查询所有的客户信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPBnfSet queryAllLPBnf(LPEdorItemSchema aLPEdorItemSchema)
    {
        LCBnfSet tLCBnfSet = new LCBnfSet();
        Reflections tReflections = new Reflections();
        LPBnfSchema tLPBnfSchema = new LPBnfSchema();
        LPBnfSet tLPBnfSet = new LPBnfSet();

        LCBnfDB tLCBnfDB = new LCBnfDB();
        //tLCBnfDB.setPolNo(aLPEdorItemSchema.getPolNo());
        tLCBnfSet = tLCBnfDB.query();
        for (int i = 1; i <= tLCBnfSet.size(); i++)
        {
            tReflections.transFields(tLPBnfSchema, tLCBnfSet.get(i));
            tLPBnfSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            //tLPBnfSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            if (!this.queryLPBnf(tLPBnfSchema))
            {
                return tLPBnfSet;
            }
            tLPBnfSet.add(this.getSchema());
        }
        return tLPBnfSet;
    }

    //查询被保险人变动信息
    public boolean queryLPBnf(LPBnfSchema aLPBnfSchema)
    {
        LPBnfSchema tLPBnfSchema = new LPBnfSchema();
        LPBnfSet aLPBnfSet = new LPBnfSet();

        LCBnfSchema tLCBnfSchema = new LCBnfSchema();
        LCBnfSet tLCBnfSet = new LCBnfSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息
//        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
//        aLPEdorItemSchema.setEdorNo(aLPBnfSchema.getEdorNo());
//        //aLPEdorItemSchema.setEdorType(aLPBnfSchema.getEdorType());
//        //aLPEdorItemSchema.setPolNo(aLPBnfSchema.getPolNo());
//        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
//        sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorNo='" +
//              aLPEdorItemSchema.getEdorNo() + "' and PolNo='"
//              //+aLPEdorItemSchema.getPolNo()
//              + "' order by MakeDate desc,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPBnfDB tLPBnfDB = new LPBnfDB();

            tLPBnfSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            //tLPBnfSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            //tLPBnfSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            // System.out.println(tLPEdorItemSchema.getEdorType());
            tLPBnfSchema.setBnfType(aLPBnfSchema.getBnfType());
            tLPBnfSchema.setBnfNo(aLPBnfSchema.getBnfNo());

            tLPBnfDB.setSchema(tLPBnfSchema);
            if (!tLPBnfDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPBnfDB.setEdorNo(aLPBnfSchema.getEdorNo());
                tLPBnfDB.setEdorType(aLPBnfSchema.getEdorType());
                this.setSchema(tLPBnfDB.getSchema());
                return true;
            }
        }

//查找已经申请确认的保单批改信息（没有保全确认）

        tLPEdorItemSet.clear();
        m = 0;
        n = 0;

//        sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorState='2' and PolNo='"
//              //+aLPEdorItemSchema.getPolNo()
//              + "' order by MakeDate desc,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPBnfDB tLPBnfDB = new LPBnfDB();

            tLPBnfSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            //tLPBnfSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            //tLPBnfSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            //System.out.println(tLPEdorItemSchema.getEdorType());
            tLPBnfSchema.setBnfType(aLPBnfSchema.getBnfType());
            tLPBnfSchema.setBnfNo(aLPBnfSchema.getBnfNo());

            tLPBnfDB.setSchema(tLPBnfSchema);
            if (!tLPBnfDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPBnfDB.setEdorNo(aLPBnfSchema.getEdorNo());
                tLPBnfDB.setEdorType(aLPBnfSchema.getEdorType());
                this.setSchema(tLPBnfDB.getSchema());
                return true;
            }
        }

        n = 0;

        //如果是第一次申请,得到承保保单的客户信息
        LCBnfDB tLCBnfDB = new LCBnfDB();
        //tLCBnfDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCBnfSet = tLCBnfDB.query();
        n = tLCBnfSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            tLCBnfSchema = tLCBnfSet.get(i);
            System.out.println("PolNo:" + aLPBnfSchema.getPolNo());
            System.out.println("CustomerNo:" + aLPBnfSchema.getCustomerNo());
            if (tLCBnfSchema.getPolNo().equals(aLPBnfSchema.getPolNo())
                && tLCBnfSchema.getBnfType().equals(aLPBnfSchema.getBnfType())
                && tLCBnfSchema.getBnfNo() == aLPBnfSchema.getBnfNo())
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPBnfSchema, tLCBnfSchema);

                tLPBnfSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                //tLPBnfSchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPBnfSchema);
                return true;
            }
        }
        return false;
    }

    //查询被保险人变动信息
    public LPBnfSet queryLPBnf(LPEdorItemSchema aLPEdorItemSchema)
    {
        LPBnfSchema aLPBnfSchema = new LPBnfSchema();
        LPBnfSchema tLPBnfSchema = new LPBnfSchema();
        LPBnfSet aLPBnfSet = new LPBnfSet();

        LCBnfSchema tLCBnfSchema = new LCBnfSchema();
        LCBnfSet tLCBnfSet = new LCBnfSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        Reflections tReflections = new Reflections();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息

        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and EdorType='"+aLPEdorItemSchema.getEdorType()+"'"+
              " and InsuredNo='"+aLPEdorItemSchema.getInsuredNo()+"' and PolNo='" +
              aLPEdorItemSchema.getPolNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);
        if (iLPEdorItemDB.getInfo())
        {
            sql = "select * from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and EdorType='"+
                  aLPEdorItemSchema.getEdorType()+"' and InsuredNo='"+
                  aLPEdorItemSchema.getInsuredNo()+"' and PolNo='" +
                  aLPEdorItemSchema.getPolNo() + "' and (MakeDate<'" +
                  aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  aLPEdorItemSchema.getMakeDate() + "' and MakeTime<='" +
                  aLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";

        }

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPBnfDB tLPBnfDB = new LPBnfDB();

            tLPBnfSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPBnfSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPBnfSchema.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
            tLPBnfSchema.setPolNo(tLPEdorItemSchema.getPolNo());


            tLPBnfDB.setSchema(tLPBnfSchema);
            aLPBnfSet = tLPBnfDB.query();
            n = aLPBnfSet.size();
            System.out.println("------n" + n);
            if (n == 0)
            {
                continue;
            }
            return aLPBnfSet;
        }

        //查找已经申请确认的保单批改信息（没有保全确认）

        tLPEdorItemSet.clear();
        m = 0;
        n = 0;

        sql = "select * from LPEdorItem where EdorState='2' and InsuredNo='"
              +aLPEdorItemSchema.getInsuredNo()+"' and EdorType='"
              +aLPEdorItemSchema.getEdorType()+"' and PolNo='"
              +aLPEdorItemSchema.getPolNo()
              + "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {

            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPBnfDB tLPBnfDB = new LPBnfDB();

            tLPBnfSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPBnfSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPBnfSchema.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
            tLPBnfSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            //tLPBnfSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            //tLPBnfSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPBnfDB.setSchema(tLPBnfSchema);

            aLPBnfSet.clear();
            aLPBnfSet = tLPBnfDB.query();
            n = aLPBnfSet.size();
            if (n == 0)
            {
                continue;
            }
            return aLPBnfSet;
        }

        n = 0;

        //如果是第一次申请,得到承保保单的客户信息
        LCBnfDB tLCBnfDB = new LCBnfDB();

        //Modify by Minim at 2003-9-25 for BC保全项目中的批单显示问题，因为比较新旧信息时要用到bnfNo字段
        //    tLCBnfDB.setPolNo(aLPEdorItemSchema.getPolNo());
        //    tLCBnfSet = tLCBnfDB.query();
        String strSql = "select * from lcbnf where polno='"
                        + aLPEdorItemSchema.getPolNo()
                        + "' order by bnfno";
        tLCBnfSet = tLCBnfDB.executeQuery(strSql);

        n = tLCBnfSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            aLPBnfSchema = new LPBnfSchema();
            tLCBnfSchema = tLCBnfSet.get(i);
            //转换Schema
            tReflections.transFields(aLPBnfSchema, tLCBnfSchema);

            aLPBnfSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            aLPBnfSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            aLPBnfSet.add(aLPBnfSchema);
            System.out.println("aLPBnfSchema :" + aLPBnfSchema.getName());
        }
        return aLPBnfSet;
    }

//查询上次保全受益人信息
    public LPBnfSet queryLastLPBnf(LPEdorItemSchema aLPEdorItemSchema)
    {
        LPBnfSchema aLPBnfSchema = new LPBnfSchema();
        LPBnfSchema tLPBnfSchema = new LPBnfSchema();
        LPBnfSet aLPBnfSet = new LPBnfSet();

        LCBnfSchema tLCBnfSchema = new LCBnfSchema();
        LCBnfSet tLCBnfSet = new LCBnfSet();

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();

        Reflections tReflections = new Reflections();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找已经申请确认的保单批改信息（没有保全确认）
        tLPEdorItemSet.clear();
//        sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorState='2' and EdorNO<>'" +
//              aLPEdorItemSchema.getEdorNo() + "'and PolNo='"
//              //+aLPEdorItemSchema.getPolNo()
//              + "' order by MakeDate desc,MakeTime desc";
//        System.out.println(sql);
//
//        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {

            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPBnfDB tLPBnfDB = new LPBnfDB();

            tLPBnfSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            //tLPBnfSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            //tLPBnfSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPBnfDB.setSchema(tLPBnfSchema);

            aLPBnfSet.clear();
            aLPBnfSet = tLPBnfDB.query();
            n = aLPBnfSet.size();
            if (n == 0)
            {
                continue;
            }
            return aLPBnfSet;
        }

        n = 0;

        //如果是第一次申请,得到承保保单的客户信息
        LCBnfDB tLCBnfDB = new LCBnfDB();

        //Modify by Minim at 2003-9-25 for BC保全项目中的批单显示问题，因为比较新旧信息时要用到bnfNo字段
        //    tLCBnfDB.setPolNo(aLPEdorItemSchema.getPolNo());
        //    tLCBnfSet = tLCBnfDB.query();
        String strSql = "select * from lcbnf where polno='"
                        //+ aLPEdorItemSchema.getPolNo()
                        + "' order by bnfno";
        tLCBnfSet = tLCBnfDB.executeQuery(strSql);

        n = tLCBnfSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            aLPBnfSchema = new LPBnfSchema();
            tLCBnfSchema = tLCBnfSet.get(i);
            //转换Schema
            tReflections.transFields(aLPBnfSchema, tLCBnfSchema);

            aLPBnfSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            //aLPBnfSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            aLPBnfSet.add(aLPBnfSchema);
            System.out.println("aLPBnfSchema :" + aLPBnfSchema.getName());
        }
        return aLPBnfSet;
    }

    //查询被保险人当前变动信息,若LPBnf表为空则返回空的 LPBnfSet
    public LPBnfSet queryNewLPBnf(LPEdorItemSchema aLPEdorItemSchema)
    {
        LPBnfSchema aLPBnfSchema = new LPBnfSchema();
        LPBnfSchema tLPBnfSchema = new LPBnfSchema();
        LPBnfSet aLPBnfSet = new LPBnfSet();

        LCBnfSchema tLCBnfSchema = new LCBnfSchema();
        LCBnfSet tLCBnfSet = new LCBnfSet();

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();

        int n;
        n = 0;

            LPBnfDB tLPBnfDB = new LPBnfDB();
            tLPBnfSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPBnfSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPBnfSchema.setPolNo(tLPEdorItemSchema.getPolNo());

            tLPBnfDB.setSchema(tLPBnfSchema);
            aLPBnfSet = tLPBnfDB.query();
            n = aLPBnfSet.size();
            if (n == 0)
            {
                aLPBnfSet.clear();
            }


        return aLPBnfSet;
    }

    //查询上次保全受益人信息
    public boolean queryLastLPBnf(LPEdorItemSchema aLPEdorItemSchema,
                                  LPBnfSchema aLPBnfSchema)
    {
        LPBnfSchema tLPBnfSchema = new LPBnfSchema();
        LPBnfSet aLPBnfSet = new LPBnfSet();

        LCBnfSchema tLCBnfSchema = new LCBnfSchema();
        LCBnfSet tLCBnfSet = new LCBnfSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找最近申请的保单批改信息
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);

        sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where PolNo='"
              //+aLPEdorItemSchema.getPolNo()
              + "' and edorstate <>'0' and MakeDate<='" +
              aLPEdorItemSchema.getMakeDate() + "' and MakeTime<'" +
              aLPEdorItemSchema.getMakeTime() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPBnfDB tLPBnfDB = new LPBnfDB();

            tLPBnfSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            //tLPBnfSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            //tLPBnfSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            //System.out.println(tLPEdorItemSchema.getEdorType());
            tLPBnfSchema.setBnfType(aLPBnfSchema.getBnfType());
            tLPBnfSchema.setBnfNo(aLPBnfSchema.getBnfNo());

            tLPBnfDB.setSchema(tLPBnfSchema);
            if (!tLPBnfDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPBnfDB.setEdorNo(aLPBnfSchema.getEdorNo());
                tLPBnfDB.setEdorType(aLPBnfSchema.getEdorType());
                this.setSchema(tLPBnfDB.getSchema());
                return true;
            }
        }

        //如果是第一次申请,得到承保保单的客户信息
        LCBnfDB tLCBnfDB = new LCBnfDB();
        //tLCBnfDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCBnfSet = tLCBnfDB.query();
        n = tLCBnfSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            tLCBnfSchema = tLCBnfSet.get(i);
            System.out.println("PolNo:" + aLPBnfSchema.getPolNo());
            if (tLCBnfSchema.getPolNo().equals(aLPBnfSchema.getPolNo())
                && tLCBnfSchema.getBnfType().equals(aLPBnfSchema.getBnfType())
                && tLCBnfSchema.getBnfNo() == aLPBnfSchema.getBnfNo()
//         &&tLCBnfSchema.getBnfGrade().equals(aLPBnfSchema.getBnfGrade())
//         &&tLCBnfSchema.getBnfLot()==aLPBnfSchema.getBnfLot()
//         &&tLCBnfSchema.getName().equals(aLPBnfSchema.getName())
                    )
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPBnfSchema, tLCBnfSchema);

                tLPBnfSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                //tLPBnfSchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPBnfSchema);
                return true;
            }
        }
        return false;
    }
}
