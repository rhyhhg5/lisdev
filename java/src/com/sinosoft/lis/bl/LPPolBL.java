/*
 * <p>ClassName: LPPolBL </p>
 * <p>Description: LLClaimSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LPPolBL extends LPPolSchema
{
    // @Constructor
    public LPPolBL()
    {
    }

    public void setDefaultFields()
    {
        this.setMakeDate(PubFun.getCurrentDate());
        this.setMakeTime(PubFun.getCurrentTime());
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }

    public boolean queryLPPol(LPEdorItemSchema aLPEdorItemSchema)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        Reflections tReflections = new Reflections();

        LPPolSet tLPPolSet = new LPPolSet();
        LPPolSchema tLPPolSchema = new LPPolSchema();
        LPPolSchema aLPPolSchema = new LPPolSchema();
        LCPolSchema tLCPolSchema = new LCPolSchema();

        String sql;
        int m;

        //查找本次申请的保单批改信息

        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and PolNo='" +
              aLPEdorItemSchema.getPolNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);
        LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
        mLPEdorItemSet = iLPEdorItemDB.query();

        if (!iLPEdorItemDB.mErrors.needDealError() && mLPEdorItemSet.size() >= 1)
        {
            sql = "select * from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and PolNo='" +
                  aLPEdorItemSchema.getPolNo() + "' and (MakeDate<'" +
                  aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  aLPEdorItemSchema.getMakeDate() + "' and MakeTime<='" +
                  aLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";
        }
        else if (iLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }
        System.out.println("---sqlend:" + sql);

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }
        m = tLPEdorItemSet.size();
        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPPolDB tLPPolDB = new LPPolDB();
            tLPPolSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPPolSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPPolSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPPolDB.setSchema(tLPPolSchema);
            if (!tLPPolDB.getInfo())
            {
                continue;
            }

            aLPPolSchema = tLPPolDB.getSchema();
            aLPPolSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            aLPPolSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            //增加退保限制
            this.setSchema(aLPPolSchema);
            return true;
        }
        //查找已经申请确认的保单批改信息（没有保全确认）

        tLPEdorItemSet.clear();
        m = 0;
        //tLPEdorItemDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
/*----------------------------------090618修改,此部分完全无效.
//        sql = "select * from LPEdorItem where EdorState='2' and PolNo='" +
//              aLPEdorItemSchema.getPolNo() +
//              "' order by MakeDate,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        if (tLPEdorItemDB.mErrors.needDealError())
//        {
//            CError.buildErr(this, "查询已经申请确认的保单保全项目错误！");
//            return false;
//        }
//
//        m = tLPEdorItemSet.size();
//
//        for (int i = 1; i <= m; i++)
//        {
//            tLPEdorItemSchema = tLPEdorItemSet.get(i);
//            LPPolDB tLPPolDB = new LPPolDB();
//
//            tLPPolSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
//            tLPPolSchema.setPolNo(tLPEdorItemSchema.getPolNo());
//            tLPPolSchema.setEdorType(tLPEdorItemSchema.getEdorType());
//
//            tLPPolDB.setSchema(tLPPolSchema);
//
//            if (!tLPPolDB.getInfo())
//            {
//                continue;
//            }
//
//            aLPPolSchema = tLPPolDB.getSchema();
//            aLPPolSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
//            aLPPolSchema.setEdorType(aLPEdorItemSchema.getEdorType());
//            //增加退保限制
//            this.setSchema(aLPPolSchema);
//
//            return true;
//        }
-----------------------------------------------------------------------*/
        //如果是第一次申请，得到承保保单信息。
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(aLPEdorItemSchema.getPolNo());

        if (!tLCPolDB.getInfo())
        {
            return false;
        }

        tLCPolSchema = tLCPolDB.getSchema();
        //转换Schema
        tReflections.transFields(aLPPolSchema, tLCPolSchema);

        aLPPolSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        aLPPolSchema.setEdorType(aLPEdorItemSchema.getEdorType());
        System.out.println("aLPPolSchema=="+aLPPolSchema);
        this.setSchema(aLPPolSchema);
        return true;
    }

    public boolean queryOtherLPPol(LPEdorItemSchema aLPEdorItemSchema)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        Reflections tReflections = new Reflections();

        LPPolSet tLPPolSet = new LPPolSet();
        LPPolSchema tLPPolSchema = new LPPolSchema();
        LPPolSchema aLPPolSchema = new LPPolSchema();
        LCPolSchema tLCPolSchema = new LCPolSchema();

        String sql;
        int m;

        //查找本次申请的保单批改信息
        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and PolNo='" +
              aLPEdorItemSchema.getPolNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);
        LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
        mLPEdorItemSet = iLPEdorItemDB.query();

        if (!iLPEdorItemDB.mErrors.needDealError() && mLPEdorItemSet.size() >= 1)
        {
            sql = "select * from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and PolNo='" +
                  aLPEdorItemSchema.getPolNo() + "' and (MakeDate<'" +
                  aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  aLPEdorItemSchema.getMakeDate() + "' and MakeTime<='" +
                  aLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";
        }
        else if (iLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();

        //查找已经申请确认的保单批改信息（没有保全确认）
        tLPEdorItemSet.clear();
        m = 0;
        //tLPEdorItemDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
/*----------------------------------090618注释,此部分完全无效.        
//        sql = "select * from LPEdorItem where EdorState='2' and PolNo='" +
//              aLPEdorItemSchema.getPolNo() +
//              "' order by MakeDate,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        if (tLPEdorItemDB.mErrors.needDealError())
//        {
//            CError.buildErr(this, "查询已经申请确认的保单保全项目错误！");
//            return false;
//        }
//
//        m = tLPEdorItemSet.size();
//
//        for (int i = 1; i <= m; i++)
//        {
//            tLPEdorItemSchema = tLPEdorItemSet.get(i);
//            LPPolDB tLPPolDB = new LPPolDB();
//
//            tLPPolSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
//            tLPPolSchema.setPolNo(tLPEdorItemSchema.getPolNo());
//            tLPPolSchema.setEdorType(tLPEdorItemSchema.getEdorType());
//
//            tLPPolDB.setSchema(tLPPolSchema);
//
//            if (!tLPPolDB.getInfo())
//            {
//                continue;
//            }
//
//            aLPPolSchema = tLPPolDB.getSchema();
//            aLPPolSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
//            aLPPolSchema.setEdorType(aLPEdorItemSchema.getEdorType());
//            //增加退保限制
//            this.setSchema(aLPPolSchema);
//
//            return true;
//        }
-----------------------------------------------------------------------*/
        
        //如果是第一次申请，得到承保保单信息。
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(aLPEdorItemSchema.getPolNo());

        if (!tLCPolDB.getInfo())
        {
            return false;
        }

        tLCPolSchema = tLCPolDB.getSchema();
        //转换Schema
        tReflections.transFields(aLPPolSchema, tLCPolSchema);

        aLPPolSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        aLPPolSchema.setEdorType(aLPEdorItemSchema.getEdorType());

        this.setSchema(aLPPolSchema);
        return true;
    }

    /**
     * 取得附加险保单信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPPolSet queryAppendLPPol(LPEdorItemSchema aLPEdorItemSchema)
    {
        //得到承保附险保单信息。
        LCPolSet tLCPolSet = new LCPolSet();
        LPPolSet tLPPolSet = new LPPolSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setSchema(aLPEdorItemSchema);
        LCPolDB tLCPolDB = new LCPolDB();
        String tSql = "Select * from lcpol where mainPolNo='" +
                      aLPEdorItemSchema.getPolNo() + "' and MainPolNo<>PolNo";
        tLCPolSet = tLCPolDB.executeQuery(tSql);
        if (tLCPolDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询承保附险保单信息错误！");
        }

        if (tLCPolSet.size() > 0)
        {
            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                tLPEdorItemSchema.setPolNo(tLCPolSet.get(i).getPolNo());
                if (this.queryLPPol(tLPEdorItemSchema))
                {
                    tLPPolSet.add(this.getSchema());
                }
            }
        }
        return tLPPolSet;
    }


    /**
     * 取得主险和附加险保单信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPPolSet queryAllLPPol2(LPEdorItemSchema aLPEdorItemSchema)
    {
        System.out.println("start queryAll Main retail Pol .....");

        //得到承保附险保单信息。
        LCPolSet tLCPolSet = new LCPolSet();
        LPPolSet tLPPolSet = new LPPolSet();

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setSchema(aLPEdorItemSchema);

        LCPolDB tLCPolDB = new LCPolDB();

        String tSql1 = " select * from lcpol where polno = '" +
                       aLPEdorItemSchema.getPolNo() + "'";
        LCPolSet primPolSet = tLCPolDB.executeQuery(tSql1);
        if (tLCPolDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询主险和附加险保单信息错误！");
        }

        if (primPolSet.size() == 0)
        {
            return tLPPolSet;
        }
        String mainPolno = primPolSet.get(1).getMainPolNo();

        String tSql = "Select * from lcpol where mainPolNo='" + mainPolno + "'";
        System.out.println("tSql : " + tSql);
        tLCPolSet = tLCPolDB.executeQuery(tSql);
        if (tLCPolDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询主险和附加险保单信息错误！");
        }

        System.out.println("tLCPolSet size :" + tLCPolSet.size());
        if (tLCPolSet.size() > 0)
        {
            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                tLPEdorItemSchema.setPolNo(tLCPolSet.get(i).getPolNo());
                if (this.queryLPPol(tLPEdorItemSchema))
                {
                    System.out.println(">>>>>>>>> :" +
                                       this.getSchema().getPolNo());
                    System.out.println("sumPrem :" +
                                       this.getSchema().getSumPrem());
                    tLPPolSet.add(this.getSchema());
                }
            }
        }
        return tLPPolSet;
    }


    /**
     * 取得附加险保单信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPPolSet queryAllLPPol(LPEdorItemSchema aLPEdorItemSchema)
    {
        System.out.println("start queryAllLPPol .....");
        //得到承保附险保单信息。
        LCPolSet tLCPolSet = new LCPolSet();
        LPPolSet tLPPolSet = new LPPolSet();

        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        tLPEdorItemSchema.setSchema(aLPEdorItemSchema);

        LCPolDB tLCPolDB = new LCPolDB();
        String tSql = "Select * from lcpol where mainPolNo='" +
                      aLPEdorItemSchema.getPolNo() + "' or PolNo ='" +
                      aLPEdorItemSchema.getPolNo() + "'";
        System.out.println("tSql : " + tSql);
        tLCPolSet = tLCPolDB.executeQuery(tSql);
        System.out.println("tLCPolSet size :" + tLCPolSet.size());
        if (tLCPolSet.size() > 0)
        {
            for (int i = 1; i <= tLCPolSet.size(); i++)
            {
                tLPEdorItemSchema.setPolNo(tLCPolSet.get(i).getPolNo());
                if (this.queryLPPol(tLPEdorItemSchema))
                {
                    System.out.println(">>>>>>>>> :" +
                                       this.getSchema().getPolNo());
                    System.out.println("sumPrem :" +
                                       this.getSchema().getSumPrem());
                    tLPPolSet.add(this.getSchema());
                }
            }
        }
        return tLPPolSet;
    }

    //查询上次保全被保险人变动信息
    public boolean queryLastLPPol(LPEdorItemSchema aLPEdorItemSchema,
                                  LPPolSchema aLPPolSchema)
    {
        LPPolSchema tLPPolSchema = new LPPolSchema();
        LPPolSet aLPPolSet = new LPPolSet();

        LCPolSchema tLCPolSchema = new LCPolSchema();
        LCPolSet tLCPolSet = new LCPolSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找最近申请的保单批改信息
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);

        sql = "select * from LPEdorItem where PolNo='" +
              aLPEdorItemSchema.getPolNo() +
              "' and edorstate <>'0' and (MakeDate<'" +
              aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
              aLPEdorItemSchema.getMakeDate() + "' and MakeTime<='" +
              aLPEdorItemSchema.getMakeTime() +
              "')) order by MakeDate,MakeTime desc";

        System.out.println(sql);

        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询最近申请的保单批改信息错误！");
            return false;
        }

        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPPolDB tLPPolDB = new LPPolDB();

            tLPPolSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPPolSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPPolSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());

            tLPPolDB.setSchema(tLPPolSchema);
            if (!tLPPolDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPPolDB.setEdorNo(aLPPolSchema.getEdorNo());
                tLPPolDB.setEdorType(aLPPolSchema.getEdorType());
                this.setSchema(tLPPolDB.getSchema());
                return true;
            }
        }

        //如果是第一次申请,得到承保保单的客户信息
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCPolSet = tLCPolDB.query();
        n = tLCPolSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            tLCPolSchema = tLCPolSet.get(i);
            System.out.println("PolNo:" + aLPPolSchema.getPolNo());
            //转换Schema
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLPPolSchema, tLCPolSchema);

            tLPPolSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPPolSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            this.setSchema(tLPPolSchema);
            return true;
        }
        return false;
    }
}
