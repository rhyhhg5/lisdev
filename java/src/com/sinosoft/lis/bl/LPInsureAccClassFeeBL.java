/*
 * <p>ClassName: LPInsureAccClassFeeBL </p>
 * <p>Description: LPInsuredAccBLSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全
 * @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LPInsureAccClassFeeBL extends LPInsureAccClassFeeSchema
{
	// @Constructor
  public CErrors mErrors = new CErrors();		// 错误信息
  public LPInsureAccClassFeeBL()  {}
  public void setUpdateFields()
  {
    this.setModifyDate(PubFun.getCurrentDate());
    this.setModifyTime(PubFun.getCurrentTime());
  }

  /**
  * 查询所有的客户信息
  * @param aLPEdorItemSchema
  * @return
  */
 public LPInsureAccClassFeeSet queryAllLPInsureAccClassFee(LPEdorItemSchema aLPEdorItemSchema)
 {
   LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
   Reflections tReflections = new Reflections();
   LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
   LPInsureAccClassFeeSet tLPInsureAccClassFeeSet = new LPInsureAccClassFeeSet();

   LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
   tLCInsureAccClassFeeDB.setPolNo(aLPEdorItemSchema.getPolNo());
   tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
   for (int i=1;i<=tLCInsureAccClassFeeSet.size();i++)
   {
     tReflections.transFields(tLPInsureAccClassFeeSchema,tLCInsureAccClassFeeSet.get(i));
     tLPInsureAccClassFeeSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
     tLPInsureAccClassFeeSchema.setEdorType(aLPEdorItemSchema.getEdorType());
     if (!this.queryLPInsureAccClassFee(tLPInsureAccClassFeeSchema))
       return tLPInsureAccClassFeeSet;
     tLPInsureAccClassFeeSet.add(this.getSchema());
   }
   return tLPInsureAccClassFeeSet;
 }

  //查询帐户变动信息
  public boolean queryLPInsureAccClassFee(LPInsureAccClassFeeSchema aLPInsureAccClassFeeSchema)
  {
    LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
    LPInsureAccClassFeeSet aLPInsureAccClassFeeSet = new LPInsureAccClassFeeSet();

    LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = new LCInsureAccClassFeeSchema();
    LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
    LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
    LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

    String sql;
    int m,n ;
    m=0;
    n=0;

    //查找本次申请的保单批改信息

    aLPEdorItemSchema.setEdorNo(aLPInsureAccClassFeeSchema.getEdorNo());
    aLPEdorItemSchema.setEdorType(aLPInsureAccClassFeeSchema.getEdorType());
    aLPEdorItemSchema.setPolNo(aLPInsureAccClassFeeSchema.getPolNo());


    sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorNo='"+aLPEdorItemSchema.getEdorNo()+"' and PolNo='"+aLPEdorItemSchema.getPolNo()+"' order by MakeDate desc,MakeTime desc";
    System.out.println(sql);
    LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
    iLPEdorItemDB.setSchema(aLPEdorItemSchema);
    if (iLPEdorItemDB.getInfo())
    {
      //delete EdorValiDate by Minim at 2003-12-17
      sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorNo='"+aLPEdorItemSchema.getEdorNo()+"' and PolNo='"+aLPEdorItemSchema.getPolNo()+"' and MakeDate<='"+iLPEdorItemDB.getMakeDate()+"' and MakeTime<='"+iLPEdorItemDB.getMakeTime()+"' order by MakeDate desc,MakeTime desc";
    }

    LPEdorItemDB tLPEdorItemDB =new LPEdorItemDB();
    tLPEdorItemDB.setSchema(aLPEdorItemSchema);
    tLPEdorItemSet=tLPEdorItemDB.executeQuery(sql);
    m = tLPEdorItemSet.size();

    for (int i =1;i<=m;i++)
    {
      tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema=tLPEdorItemSet.get(i);
      LPInsureAccClassFeeDB tLPInsureAccClassFeeDB = new LPInsureAccClassFeeDB();

      tLPInsureAccClassFeeSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
      tLPInsureAccClassFeeSchema.setPolNo(tLPEdorItemSchema.getPolNo());
      tLPInsureAccClassFeeSchema.setEdorType(tLPEdorItemSchema.getEdorType());
      System.out.println(tLPEdorItemSchema.getEdorType());
      tLPInsureAccClassFeeSchema.setInsuAccNo(aLPInsureAccClassFeeSchema.getInsuAccNo());
      tLPInsureAccClassFeeSchema.setOtherNo(aLPInsureAccClassFeeSchema.getOtherNo());

      tLPInsureAccClassFeeDB.setSchema(tLPInsureAccClassFeeSchema);
      if (!tLPInsureAccClassFeeDB.getInfo())
        continue ;
      else
      {
        tLPInsureAccClassFeeDB.setEdorNo(aLPInsureAccClassFeeSchema.getEdorNo());
        tLPInsureAccClassFeeDB.setEdorType(aLPInsureAccClassFeeSchema.getEdorType());
        this.setSchema(tLPInsureAccClassFeeDB.getSchema());
        return true;
      }
    }

//查找已经申请确认的保单批改信息（没有保全确认）

    tLPEdorItemSet.clear();
    m=0;
    n =0;

    sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorState='2' and PolNo='"+aLPEdorItemSchema.getPolNo()+"' order by MakeDate desc,MakeTime desc";
    System.out.println(sql);
    tLPEdorItemSet=tLPEdorItemDB.executeQuery(sql);
    m = tLPEdorItemSet.size();

    for ( int i =1;i<=m;i++)
    {
      tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema=tLPEdorItemSet.get(i);
      LPInsureAccClassFeeDB tLPInsureAccClassFeeDB = new LPInsureAccClassFeeDB();

      tLPInsureAccClassFeeSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
      tLPInsureAccClassFeeSchema.setPolNo(tLPEdorItemSchema.getPolNo());
      tLPInsureAccClassFeeSchema.setEdorType(tLPEdorItemSchema.getEdorType());
      System.out.println(tLPEdorItemSchema.getEdorType());
      tLPInsureAccClassFeeSchema.setInsuAccNo(aLPInsureAccClassFeeSchema.getInsuAccNo());
      tLPInsureAccClassFeeSchema.setOtherNo(aLPInsureAccClassFeeSchema.getOtherNo());

      tLPInsureAccClassFeeDB.setSchema(tLPInsureAccClassFeeSchema);
      if (!tLPInsureAccClassFeeDB.getInfo())
        continue ;
      else
      {
        tLPInsureAccClassFeeDB.setEdorNo(aLPInsureAccClassFeeSchema.getEdorNo());
        tLPInsureAccClassFeeDB.setEdorType(aLPInsureAccClassFeeSchema.getEdorType());
        this.setSchema(tLPInsureAccClassFeeDB.getSchema());
        return true;
      }
    }

    n=0;

    //如果是第一次申请,得到承保保单的客户信息
    LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
    tLCInsureAccClassFeeDB.setPolNo(aLPEdorItemSchema.getPolNo());

    tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
    n=tLCInsureAccClassFeeSet.size();
    System.out.println("------n:"+n);
    for (int i=1;i<=n;i++)
    {
      tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(i);
      if (tLCInsureAccClassFeeSchema.getPolNo().equals(aLPInsureAccClassFeeSchema.getPolNo())&&tLCInsureAccClassFeeSchema.getInsuAccNo().equals(aLPInsureAccClassFeeSchema.getInsuAccNo())
          &&tLCInsureAccClassFeeSchema.getOtherNo().equals(aLPInsureAccClassFeeSchema.getOtherNo()))
      {
        //转换Schema
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLPInsureAccClassFeeSchema,tLCInsureAccClassFeeSchema);

        tLPInsureAccClassFeeSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPInsureAccClassFeeSchema.setEdorType(aLPEdorItemSchema.getEdorType());
        this.setSchema(tLPInsureAccClassFeeSchema);
        return true;
      }
    }
    return false;
  }
  //查询上次保全投保人资料信息
  public boolean queryLastLPInsureAccClassFee(LPEdorItemSchema aLPEdorItemSchema,LPInsureAccClassFeeSchema aLPInsureAccClassFeeSchema)
  {
    LPInsureAccClassFeeSchema tLPInsureAccClassFeeSchema = new LPInsureAccClassFeeSchema();
    LPInsureAccClassFeeSet aLPInsureAccClassFeeSet = new LPInsureAccClassFeeSet();

    LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema = new LCInsureAccClassFeeSchema();
    LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
    LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

    String sql;
    int m,n ;
    m=0;
    n=0;

    //查找最近申请的保单批改信息
    LPEdorItemDB tLPEdorItemDB =new LPEdorItemDB();
    tLPEdorItemDB.setSchema(aLPEdorItemSchema);

    //delete EdorValiDate by Minim at 2003-12-17
    sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where PolNo='"+aLPEdorItemSchema.getPolNo()+"' and edorstate <>'0' and MakeDate<='"+aLPEdorItemSchema.getMakeDate()+"' and MakeTime<'"+aLPEdorItemSchema.getMakeTime()+"' order by MakeDate desc,MakeTime desc";
    System.out.println(sql);

    tLPEdorItemSet=tLPEdorItemDB.executeQuery(sql);
    m = tLPEdorItemSet.size();

    for (int i =1;i<=m;i++)
    {
      tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema=tLPEdorItemSet.get(i);
      LPInsureAccClassFeeDB tLPInsureAccClassFeeDB = new LPInsureAccClassFeeDB();

      tLPInsureAccClassFeeSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
      tLPInsureAccClassFeeSchema.setPolNo(tLPEdorItemSchema.getPolNo());
      tLPInsureAccClassFeeSchema.setEdorType(tLPEdorItemSchema.getEdorType());
      System.out.println(tLPEdorItemSchema.getEdorType());
      tLPInsureAccClassFeeSchema.setInsuAccNo(aLPInsureAccClassFeeSchema.getInsuAccNo());
      tLPInsureAccClassFeeSchema.setOtherNo(aLPInsureAccClassFeeSchema.getOtherNo());

      tLPInsureAccClassFeeDB.setSchema(tLPInsureAccClassFeeSchema);
      if (!tLPInsureAccClassFeeDB.getInfo())
        continue ;
      else
      {
        tLPInsureAccClassFeeDB.setEdorNo(aLPInsureAccClassFeeSchema.getEdorNo());
        tLPInsureAccClassFeeDB.setEdorType(aLPInsureAccClassFeeSchema.getEdorType());
        this.setSchema(tLPInsureAccClassFeeDB.getSchema());
        return true;
      }
    }

    //如果是第一次申请,得到承保保单的客户信息
    LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
    tLCInsureAccClassFeeDB.setPolNo(aLPEdorItemSchema.getPolNo());

    tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.query();
    n=tLCInsureAccClassFeeSet.size();
    System.out.println("------n:"+n);
    for (int i=1;i<=n;i++)
    {
      tLCInsureAccClassFeeSchema = tLCInsureAccClassFeeSet.get(i);
      System.out.println("PolNo:"+aLPInsureAccClassFeeSchema.getPolNo());
      if (tLCInsureAccClassFeeSchema.getPolNo().equals(aLPInsureAccClassFeeSchema.getPolNo())
          &&tLCInsureAccClassFeeSchema.getInsuAccNo().equals(aLPInsureAccClassFeeSchema.getInsuAccNo())
          &&tLCInsureAccClassFeeSchema.getOtherNo().equals(aLPInsureAccClassFeeSchema.getOtherNo()))
      {
        //转换Schema
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLPInsureAccClassFeeSchema,tLCInsureAccClassFeeSchema);

        tLPInsureAccClassFeeSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPInsureAccClassFeeSchema.setEdorType(aLPEdorItemSchema.getEdorType());
        this.setSchema(tLPInsureAccClassFeeSchema);
        return true;
      }
    }
    return false;
  }
}
