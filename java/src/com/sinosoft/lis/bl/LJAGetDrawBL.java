/*
 * <p>ClassName: LJAGetDrawBL </p>
 * <p>Description: LJAGetDrawSchemaBL���ļ� </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate��2002-04-01
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LJAGetDrawBL extends LJAGetDrawSchema
{
	// @Constructor
	public LJAGetDrawBL() {}
        public LJAGetDrawSchema chg(LJSGetDrawSchema tLJSGetDrawSchema)
        {
            LJAGetDrawSchema tLJAGetDrawSchema =new LJAGetDrawSchema();
            tLJAGetDrawSchema.setAgentCode(tLJSGetDrawSchema.getAgentCode());
            tLJAGetDrawSchema.setAgentGroup(tLJSGetDrawSchema.getAgentGroup());
            tLJAGetDrawSchema.setAgentType(tLJSGetDrawSchema.getAgentType());
            tLJAGetDrawSchema.setAppntNo(tLJSGetDrawSchema.getAppntNo());
            tLJAGetDrawSchema.setApproveCode(tLJSGetDrawSchema.getApproveCode());
            tLJAGetDrawSchema.setApproveDate(tLJSGetDrawSchema.getApproveDate());

            tLJAGetDrawSchema.setConfDate(tLJSGetDrawSchema.getConfDate());
            tLJAGetDrawSchema.setEnterAccDate(tLJSGetDrawSchema.getEnterAccDate());

            tLJAGetDrawSchema.setGetDutyKind(tLJSGetDrawSchema.getGetDutyKind());
            tLJAGetDrawSchema.setGetMoney(tLJSGetDrawSchema.getGetMoney());
            tLJAGetDrawSchema.setGrpName(tLJSGetDrawSchema.getGrpName());

            tLJAGetDrawSchema.setDutyCode(tLJSGetDrawSchema.getDutyCode());
            tLJAGetDrawSchema.setPolNo(tLJSGetDrawSchema.getPolNo());
            tLJAGetDrawSchema.setDestrayFlag(tLJSGetDrawSchema.getDestrayFlag());
            tLJAGetDrawSchema.setFeeFinaType(tLJSGetDrawSchema.getFeeFinaType());
            tLJAGetDrawSchema.setRiskVersion(tLJSGetDrawSchema.getRiskVersion());
            tLJAGetDrawSchema.setGrpPolNo(tLJSGetDrawSchema.getGrpPolNo());
            tLJAGetDrawSchema.setMakeDate(tLJSGetDrawSchema.getMakeDate());
            tLJAGetDrawSchema.setMakeTime(tLJSGetDrawSchema.getMakeTime());
            tLJAGetDrawSchema.setOperator(tLJSGetDrawSchema.getOperator());
            return tLJAGetDrawSchema;
        }
}
