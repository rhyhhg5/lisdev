/*
 * <p>ClassName: LPContBL </p>
 * <p>Description: LLClaimSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LPContBL extends LPContSchema
{
    // @Constructor
    public LPContBL()
    {
    }

    public void setDefaultFields()
    {
        this.setMakeDate(PubFun.getCurrentDate());
        this.setMakeTime(PubFun.getCurrentTime());
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }

    public boolean queryLPCont(LPEdorItemSchema aLPEdorItemSchema)
    {
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        Reflections tReflections = new Reflections();

        LPContSet tLPContSet = new LPContSet();
        LPContSchema tLPContSchema = new LPContSchema();
        LPContSchema aLPContSchema = new LPContSchema();
        LCContSchema tLCContSchema = new LCContSchema();

        String sql;
        int m;

    //查找本次申请的保单批改信息

        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and ContNo='" +
              aLPEdorItemSchema.getContNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);
        LPEdorItemSet mLPEdorItemSet = new LPEdorItemSet();
        mLPEdorItemSet = iLPEdorItemDB.query();

        if (!iLPEdorItemDB.mErrors.needDealError() && mLPEdorItemSet.size() >= 1)
        {
            sql = "select * from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and ContNo='" +
                  aLPEdorItemSchema.getContNo() + "' and (MakeDate<'" +
                  aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  aLPEdorItemSchema.getMakeDate() + "' and MakeTime<='" +
                  aLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";
        }
        else if (iLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }
        System.out.println("---sqlend:" + sql);

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }
        m = tLPEdorItemSet.size();
        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPContDB tLPContDB = new LPContDB();
            tLPContSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPContSchema.setContNo(tLPEdorItemSchema.getContNo());
            tLPContSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPContDB.setSchema(tLPContSchema);
            if (!tLPContDB.getInfo())
                continue;

            aLPContSchema = tLPContDB.getSchema();
            aLPContSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            aLPContSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            //增加退保限制
            this.setSchema(aLPContSchema);
            return true;
        }
    //查找已经申请确认的保单批改信息（没有保全确认）

        tLPEdorItemSet.clear();
        m = 0;
        //tLPEdorItemDB.setEdorNo(aLPEdorItemSchema.getEdorNo());
        sql = "select * from LPEdorItem where EdorState='2' and ContNo='" +
              aLPEdorItemSchema.getContNo() +
              "' order by MakeDate,MakeTime desc";
        System.out.println(sql);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询已经申请确认的保单保全项目错误！");
            return false;
        }

        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPContDB tLPContDB = new LPContDB();

            tLPContSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPContSchema.setContNo(tLPEdorItemSchema.getContNo());
            tLPContSchema.setEdorType(tLPEdorItemSchema.getEdorType());

            tLPContDB.setSchema(tLPContSchema);

            if (!tLPContDB.getInfo())
                continue;

            aLPContSchema = tLPContDB.getSchema();
            aLPContSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            aLPContSchema.setEdorType(aLPEdorItemSchema.getEdorType());
    //增加退保限制
            this.setSchema(aLPContSchema);

            return true;
        }

        //如果是第一次申请，得到承保保单信息。
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());

        if (!tLCContDB.getInfo())
            return false;

        tLCContSchema = tLCContDB.getSchema();
        //转换Schema
        tReflections.transFields(aLPContSchema, tLCContSchema);

        aLPContSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        aLPContSchema.setEdorType(aLPEdorItemSchema.getEdorType());

        this.setSchema(aLPContSchema);
        return true;
    }

    /**
     * 取得附加险保单信息
     * @param aLPEdorItemSchema
     * @return
     */
//    public LPContSet queryAppendLPCont(LPEdorItemSchema aLPEdorItemSchema)
//    {
//        //得到承保附险保单信息。
//        LCContSet tLCContSet = new LCContSet();
//        LPContSet tLPContSet = new LPContSet();
//        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
//        tLPEdorItemSchema.setSchema(aLPEdorItemSchema);
//        LCContDB tLCContDB = new LCContDB();
//        String tSql = "Select * from LCCont where mainContNo='" +
//                      aLPEdorItemSchema.getContNo() + "' and MainContNo<>ContNo";
//        tLCContSet = tLCContDB.executeQuery(tSql);
//        if (tLCContDB.mErrors.needDealError())
//        {
//            CError.buildErr(this, "查询承保附险保单信息错误！");
//        }
//
//        if (tLCContSet.size() > 0)
//        {
//            for (int i = 1; i <= tLCContSet.size(); i++)
//            {
//                tLPEdorItemSchema.setContNo(tLCContSet.get(i).getContNo());
//                if (this.queryLPCont(tLPEdorItemSchema))
//                {
//                    tLPContSet.add(this.getSchema());
//                }
//            }
//        }
//        return tLPContSet;
//    }


    /**
     * 取得主险和附加险保单信息
     * @param aLPEdorItemSchema
     * @return
     */
//    public LPContSet queryAllLPCont2(LPEdorItemSchema aLPEdorItemSchema)
//    {
//        System.out.println("start queryAll Main retail Pol .....");
//
//        //得到承保附险保单信息。
//        LCContSet tLCContSet = new LCContSet();
//        LPContSet tLPContSet = new LPContSet();
//
//        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
//        tLPEdorItemSchema.setSchema(aLPEdorItemSchema);
//
//        LCContDB tLCContDB = new LCContDB();
//
//        String tSql1 = " select * from LCCont where ContNo = '" +
//                       aLPEdorItemSchema.getContNo() + "'";
//        LCContSet primPolSet = tLCContDB.executeQuery(tSql1);
//        if (tLCContDB.mErrors.needDealError())
//        {
//            CError.buildErr(this, "查询主险和附加险保单信息错误！");
//        }
//
//        if (primPolSet.size() == 0)
//            return tLPContSet;
//        String mainContNo = primPolSet.get(1).getMainContNo();
//
//        String tSql = "Select * from LCCont where mainContNo='" + mainContNo + "'";
//        System.out.println("tSql : " + tSql);
//        tLCContSet = tLCContDB.executeQuery(tSql);
//        if (tLCContDB.mErrors.needDealError())
//        {
//            CError.buildErr(this, "查询主险和附加险保单信息错误！");
//        }
//
//        System.out.println("tLCContSet size :" + tLCContSet.size());
//        if (tLCContSet.size() > 0)
//        {
//            for (int i = 1; i <= tLCContSet.size(); i++)
//            {
//                tLPEdorItemSchema.setContNo(tLCContSet.get(i).getContNo());
//                if (this.queryLPCont(tLPEdorItemSchema))
//                {
//                    System.out.println(">>>>>>>>> :" +
//                                       this.getSchema().getContNo());
//                    System.out.println("sumPrem :" +
//                                       this.getSchema().getSumPrem());
//                    tLPContSet.add(this.getSchema());
//                }
//            }
//        }
//        return tLPContSet;
//    }


    /**
     * 取得附加险保单信息
     * @param aLPEdorItemSchema
     * @return
     */
//    public LPContSet queryAllLPCont(LPEdorItemSchema aLPEdorItemSchema)
//    {
//        System.out.println("start queryAllLPCont .....");
//        //得到承保附险保单信息。
//        LCContSet tLCContSet = new LCContSet();
//        LPContSet tLPContSet = new LPContSet();
//
//        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
//        tLPEdorItemSchema.setSchema(aLPEdorItemSchema);
//
//        LCContDB tLCContDB = new LCContDB();
//        String tSql = "Select * from LCCont where mainContNo='" +
//                      aLPEdorItemSchema.getContNo() + "' or ContNo ='" +
//                      aLPEdorItemSchema.getContNo() + "'";
//        System.out.println("tSql : " + tSql);
//        tLCContSet = tLCContDB.executeQuery(tSql);
//        System.out.println("tLCContSet size :" + tLCContSet.size());
//        if (tLCContSet.size() > 0)
//        {
//            for (int i = 1; i <= tLCContSet.size(); i++)
//            {
//                tLPEdorItemSchema.setContNo(tLCContSet.get(i).getContNo());
//                if (this.queryLPCont(tLPEdorItemSchema))
//                {
//                    System.out.println(">>>>>>>>> :" +
//                                       this.getSchema().getContNo());
//                    System.out.println("sumPrem :" +
//                                       this.getSchema().getSumPrem());
//                    tLPContSet.add(this.getSchema());
//                }
//            }
//        }
//        return tLPContSet;
//    }

    //查询上次保全被保险人变动信息
    public boolean queryLastLPCont(LPEdorItemSchema aLPEdorItemSchema,
                                  LPContSchema aLPContSchema)
    {
        LPContSchema tLPContSchema = new LPContSchema();
        LPContSet aLPContSet = new LPContSet();

        LCContSchema tLCContSchema = new LCContSchema();
        LCContSet tLCContSet = new LCContSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找最近申请的保单批改信息
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);

        sql = "select * from LPEdorItem where ContNo='" +
              aLPEdorItemSchema.getContNo() +
              "' and edorstate <>'0' and (MakeDate<'" +
                  aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  aLPEdorItemSchema.getMakeDate() +"' and MakeTime<='" +
                  aLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";

        System.out.println(sql);

        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询最近申请的保单批改信息错误！");
            return false;
        }

        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPContDB tLPContDB = new LPContDB();

            tLPContSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPContSchema.setContNo(tLPEdorItemSchema.getContNo());
            tLPContSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());

            tLPContDB.setSchema(tLPContSchema);
            if (!tLPContDB.getInfo())
                continue;
            else
            {
                tLPContDB.setEdorNo(aLPContSchema.getEdorNo());
                tLPContDB.setEdorType(aLPContSchema.getEdorType());
                this.setSchema(tLPContDB.getSchema());
                return true;
            }
        }

        //如果是第一次申请,得到承保保单的客户信息
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(aLPEdorItemSchema.getContNo());

        tLCContSet = tLCContDB.query();
        n = tLCContSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            tLCContSchema = tLCContSet.get(i);
            System.out.println("ContNo:" + aLPContSchema.getContNo());
            //转换Schema
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLPContSchema, tLCContSchema);

            tLPContSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPContSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            this.setSchema(tLPContSchema);
            return true;
        }
        return false;
    }
}

