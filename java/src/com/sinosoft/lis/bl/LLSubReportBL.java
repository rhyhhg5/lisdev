/*
 * <p>ClassName: LLSubReportBL </p>
 * <p>Description: LLSubReportSchemaBL���ļ� </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: ����
 * @CreateDate��2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LLSubReportBL extends LLSubReportSchema
{
    // @Constructor
    public LLSubReportBL()
    {

    }

    public void setDefaultValue()
    {
        this.setMngCom("ddd");
        this.setOperator("ddd");
        this.setMakeDate(PubFun.getCurrentDate());
        this.setMakeTime(PubFun.getCurrentTime());
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }

}
