/*
 * <p>ClassName: LCSpecBL </p>
 * <p>Description: LCSpecSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-04-01
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LCSpecBL extends LCSpecSchema
{
	// @Constructor
	public LCSpecBL() {
    this.setModifyDate(PubFun.getCurrentDate());
    this.setModifyTime(PubFun.getCurrentTime()) ;
  }

  /**
   * 设置默认的字段属性
   */
  public void setDefaultFields()
  {
    this.setModifyDate(PubFun.getCurrentDate());
    this.setModifyTime(PubFun.getCurrentTime()) ;
  }

}
