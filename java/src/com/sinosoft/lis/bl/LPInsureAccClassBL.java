/*
 * <p>ClassName: LPInsureAccClassBL </p>
 * <p>Description: LPInsuredAccBLSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全
 * @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LPInsureAccClassBL extends LPInsureAccClassSchema
{
	// @Constructor
  public CErrors mErrors = new CErrors();		// 错误信息
  public LPInsureAccClassBL()  {}
  public void setUpdateFields()
  {
    this.setModifyDate(PubFun.getCurrentDate());
    this.setModifyTime(PubFun.getCurrentTime());
  }

  /**
  * 查询所有的客户信息
  * @param aLPEdorItemSchema
  * @return
  */
 public LPInsureAccClassSet queryAllLPInsureAccClass(LPEdorItemSchema aLPEdorItemSchema)
 {
   LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
   Reflections tReflections = new Reflections();
   LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
   LPInsureAccClassSet tLPInsureAccClassSet = new LPInsureAccClassSet();

   LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
   tLCInsureAccClassDB.setPolNo(aLPEdorItemSchema.getPolNo());
   tLCInsureAccClassSet = tLCInsureAccClassDB.query();
   for (int i=1;i<=tLCInsureAccClassSet.size();i++)
   {
     tReflections.transFields(tLPInsureAccClassSchema,tLCInsureAccClassSet.get(i));
     tLPInsureAccClassSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
     tLPInsureAccClassSchema.setEdorType(aLPEdorItemSchema.getEdorType());
     if (!this.queryLPInsureAccClass(tLPInsureAccClassSchema))
       return tLPInsureAccClassSet;
     tLPInsureAccClassSet.add(this.getSchema());
   }
   return tLPInsureAccClassSet;
 }

  //查询帐户变动信息
  public boolean queryLPInsureAccClass(LPInsureAccClassSchema aLPInsureAccClassSchema)
  {
    LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
    LPInsureAccClassSet aLPInsureAccClassSet = new LPInsureAccClassSet();

    LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
    LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
    LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
    LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

    String sql;
    int m,n ;
    m=0;
    n=0;

    //查找本次申请的保单批改信息

    aLPEdorItemSchema.setEdorNo(aLPInsureAccClassSchema.getEdorNo());
    aLPEdorItemSchema.setEdorType(aLPInsureAccClassSchema.getEdorType());
    aLPEdorItemSchema.setPolNo(aLPInsureAccClassSchema.getPolNo());


    sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorNo='"+aLPEdorItemSchema.getEdorNo()+"' and PolNo='"+aLPEdorItemSchema.getPolNo()+"' order by MakeDate desc,MakeTime desc";
    System.out.println(sql);
    LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
    iLPEdorItemDB.setSchema(aLPEdorItemSchema);
    if (iLPEdorItemDB.getInfo())
    {
      //delete EdorValiDate by Minim at 2003-12-17
      sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorNo='"+aLPEdorItemSchema.getEdorNo()+"' and PolNo='"+aLPEdorItemSchema.getPolNo()+"' and MakeDate<='"+iLPEdorItemDB.getMakeDate()+"' and MakeTime<='"+iLPEdorItemDB.getMakeTime()+"' order by MakeDate desc,MakeTime desc";
    }

    LPEdorItemDB tLPEdorItemDB =new LPEdorItemDB();
    tLPEdorItemDB.setSchema(aLPEdorItemSchema);
    tLPEdorItemSet=tLPEdorItemDB.executeQuery(sql);
    m = tLPEdorItemSet.size();

    for (int i =1;i<=m;i++)
    {
      tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema=tLPEdorItemSet.get(i);
      LPInsureAccClassDB tLPInsureAccClassDB = new LPInsureAccClassDB();

      tLPInsureAccClassSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
      tLPInsureAccClassSchema.setPolNo(tLPEdorItemSchema.getPolNo());
      tLPInsureAccClassSchema.setEdorType(tLPEdorItemSchema.getEdorType());
      System.out.println(tLPEdorItemSchema.getEdorType());
      tLPInsureAccClassSchema.setInsuAccNo(aLPInsureAccClassSchema.getInsuAccNo());
      tLPInsureAccClassSchema.setOtherNo(aLPInsureAccClassSchema.getOtherNo());

      tLPInsureAccClassDB.setSchema(tLPInsureAccClassSchema);
      if (!tLPInsureAccClassDB.getInfo())
        continue ;
      else
      {
        tLPInsureAccClassDB.setEdorNo(aLPInsureAccClassSchema.getEdorNo());
        tLPInsureAccClassDB.setEdorType(aLPInsureAccClassSchema.getEdorType());
        this.setSchema(tLPInsureAccClassDB.getSchema());
        return true;
      }
    }

//查找已经申请确认的保单批改信息（没有保全确认）

    tLPEdorItemSet.clear();
    m=0;
    n =0;

    sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorState='2' and PolNo='"+aLPEdorItemSchema.getPolNo()+"' order by MakeDate desc,MakeTime desc";
    System.out.println(sql);
    tLPEdorItemSet=tLPEdorItemDB.executeQuery(sql);
    m = tLPEdorItemSet.size();

    for ( int i =1;i<=m;i++)
    {
      tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema=tLPEdorItemSet.get(i);
      LPInsureAccClassDB tLPInsureAccClassDB = new LPInsureAccClassDB();

      tLPInsureAccClassSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
      tLPInsureAccClassSchema.setPolNo(tLPEdorItemSchema.getPolNo());
      tLPInsureAccClassSchema.setEdorType(tLPEdorItemSchema.getEdorType());
      System.out.println(tLPEdorItemSchema.getEdorType());
      tLPInsureAccClassSchema.setInsuAccNo(aLPInsureAccClassSchema.getInsuAccNo());
      tLPInsureAccClassSchema.setOtherNo(aLPInsureAccClassSchema.getOtherNo());

      tLPInsureAccClassDB.setSchema(tLPInsureAccClassSchema);
      if (!tLPInsureAccClassDB.getInfo())
        continue ;
      else
      {
        tLPInsureAccClassDB.setEdorNo(aLPInsureAccClassSchema.getEdorNo());
        tLPInsureAccClassDB.setEdorType(aLPInsureAccClassSchema.getEdorType());
        this.setSchema(tLPInsureAccClassDB.getSchema());
        return true;
      }
    }

    n=0;

    //如果是第一次申请,得到承保保单的客户信息
    LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
    tLCInsureAccClassDB.setPolNo(aLPEdorItemSchema.getPolNo());

    tLCInsureAccClassSet = tLCInsureAccClassDB.query();
    n=tLCInsureAccClassSet.size();
    System.out.println("------n:"+n);
    for (int i=1;i<=n;i++)
    {
      tLCInsureAccClassSchema = tLCInsureAccClassSet.get(i);
      if (tLCInsureAccClassSchema.getPolNo().equals(aLPInsureAccClassSchema.getPolNo())&&tLCInsureAccClassSchema.getInsuAccNo().equals(aLPInsureAccClassSchema.getInsuAccNo())
          &&tLCInsureAccClassSchema.getOtherNo().equals(aLPInsureAccClassSchema.getOtherNo()))
      {
        //转换Schema
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLPInsureAccClassSchema,tLCInsureAccClassSchema);

        tLPInsureAccClassSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPInsureAccClassSchema.setEdorType(aLPEdorItemSchema.getEdorType());
        this.setSchema(tLPInsureAccClassSchema);
        return true;
      }
    }
    return false;
  }
  //查询上次保全投保人资料信息
  public boolean queryLastLPInsureAccClass(LPEdorItemSchema aLPEdorItemSchema,LPInsureAccClassSchema aLPInsureAccClassSchema)
  {
    LPInsureAccClassSchema tLPInsureAccClassSchema = new LPInsureAccClassSchema();
    LPInsureAccClassSet aLPInsureAccClassSet = new LPInsureAccClassSet();

    LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
    LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
    LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

    String sql;
    int m,n ;
    m=0;
    n=0;

    //查找最近申请的保单批改信息
    LPEdorItemDB tLPEdorItemDB =new LPEdorItemDB();
    tLPEdorItemDB.setSchema(aLPEdorItemSchema);

    //delete EdorValiDate by Minim at 2003-12-17
    sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where PolNo='"+aLPEdorItemSchema.getPolNo()+"' and edorstate <>'0' and MakeDate<='"+aLPEdorItemSchema.getMakeDate()+"' and MakeTime<'"+aLPEdorItemSchema.getMakeTime()+"' order by MakeDate desc,MakeTime desc";
    System.out.println(sql);

    tLPEdorItemSet=tLPEdorItemDB.executeQuery(sql);
    m = tLPEdorItemSet.size();

    for (int i =1;i<=m;i++)
    {
      tLPEdorItemSchema = new LPEdorItemSchema();
      tLPEdorItemSchema=tLPEdorItemSet.get(i);
      LPInsureAccClassDB tLPInsureAccClassDB = new LPInsureAccClassDB();

      tLPInsureAccClassSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
      tLPInsureAccClassSchema.setPolNo(tLPEdorItemSchema.getPolNo());
      tLPInsureAccClassSchema.setEdorType(tLPEdorItemSchema.getEdorType());
      System.out.println(tLPEdorItemSchema.getEdorType());
      tLPInsureAccClassSchema.setInsuAccNo(aLPInsureAccClassSchema.getInsuAccNo());
      tLPInsureAccClassSchema.setOtherNo(aLPInsureAccClassSchema.getOtherNo());

      tLPInsureAccClassDB.setSchema(tLPInsureAccClassSchema);
      if (!tLPInsureAccClassDB.getInfo())
        continue ;
      else
      {
        tLPInsureAccClassDB.setEdorNo(aLPInsureAccClassSchema.getEdorNo());
        tLPInsureAccClassDB.setEdorType(aLPInsureAccClassSchema.getEdorType());
        this.setSchema(tLPInsureAccClassDB.getSchema());
        return true;
      }
    }

    //如果是第一次申请,得到承保保单的客户信息
    LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
    tLCInsureAccClassDB.setPolNo(aLPEdorItemSchema.getPolNo());

    tLCInsureAccClassSet = tLCInsureAccClassDB.query();
    n=tLCInsureAccClassSet.size();
    System.out.println("------n:"+n);
    for (int i=1;i<=n;i++)
    {
      tLCInsureAccClassSchema = tLCInsureAccClassSet.get(i);
      System.out.println("PolNo:"+aLPInsureAccClassSchema.getPolNo());
      if (tLCInsureAccClassSchema.getPolNo().equals(aLPInsureAccClassSchema.getPolNo())
          &&tLCInsureAccClassSchema.getInsuAccNo().equals(aLPInsureAccClassSchema.getInsuAccNo())
          &&tLCInsureAccClassSchema.getOtherNo().equals(aLPInsureAccClassSchema.getOtherNo()))
      {
        //转换Schema
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLPInsureAccClassSchema,tLCInsureAccClassSchema);

        tLPInsureAccClassSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
        tLPInsureAccClassSchema.setEdorType(aLPEdorItemSchema.getEdorType());
        this.setSchema(tLPInsureAccClassSchema);
        return true;
      }
    }
    return false;
  }
}
