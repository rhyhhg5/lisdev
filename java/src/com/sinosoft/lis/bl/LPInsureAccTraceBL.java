/*
 * <p>ClassName: LPInsureAccTraceBL </p>
 * <p>Description: LPInsuredAccTraceBLSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全
 * @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LPInsureAccTraceBL extends LPInsureAccTraceSchema
{
	// @Constructor
  public CErrors mErrors = new CErrors();		// 错误信息
  public LPInsureAccTraceBL()  {}

  public LPInsureAccTraceSet queryAllTrace(String polNo) {
    //将作退保之前的进行帐户调整的未保全确认的帐户调整轨迹查询出来
    String strSql = "select * from LPInsureAccTrace where PolNo='" + polNo
                  + "' and Edorno in (select edorno from lpedormain where edorstate<>'0' "
                  + " union select edorno from lpgrpedormain where edorstate<>'0')";
    LPInsureAccTraceDB tLPInsureAccTraceDB = new LPInsureAccTraceDB();
    LPInsureAccTraceSet tLPInsureAccTraceSet = tLPInsureAccTraceDB.executeQuery(strSql);

    //将原C表中的帐户调整轨迹查询出来
    LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
    tLCInsureAccTraceDB.setPolNo(polNo);
    LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.query();

    //将所有的数据合并成一个Set并返回
    Reflections r = new Reflections();
    for (int i=0; i<tLCInsureAccTraceSet.size(); i++) {
      LPInsureAccTraceSchema tLPInsureAccTraceSchema = new LPInsureAccTraceSchema();
      r.transFields(tLPInsureAccTraceSchema, tLCInsureAccTraceSet.get(i+1));
      tLPInsureAccTraceSet.add(tLPInsureAccTraceSchema);
    }

    return tLPInsureAccTraceSet;
  }
}
