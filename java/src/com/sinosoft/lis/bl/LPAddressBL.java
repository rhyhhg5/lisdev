/*
 * <p>ClassName: LPGrpBL </p>
 * <p>Description: LPGrpBLSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全
 * @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LPAddressDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LPAddressSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPPersonSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;

public class LPAddressBL extends LPAddressSchema
{

    // @Constructor
    public CErrors mErrors = new CErrors(); // 错误信息
    public LPAddressBL()
    {}

    public void setDefaultFields()
    {
        this.setMakeDate(PubFun.getCurrentDate());
        this.setMakeTime(PubFun.getCurrentTime());
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }


    //查询投保险人变动信息
    public boolean queryLPAddress(LPAddressSchema aLPAddressSchema)
    {
        LPAddressSchema tLPAddressSchema = new LPAddressSchema();
        LPPersonSet aLPPersonSet = new LPPersonSet();

        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        LCAddressSet tLCAddressSet = new LCAddressSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找本次申请的其他保全项目更新后得LPGrp表
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        aLPEdorItemSchema.setEdorNo(aLPAddressSchema.getEdorNo());
        //aLPGrpEdorItemSchema.setEdorType(aLPGrpAddressSchema.getEdorType());
        //aLPGrpEdorItemSchema.setGrpContNo(aLPGrpAddressSchema.getGrpContNo());
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }

        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPAddressDB tLPAddressDB = new LPAddressDB();

            tLPAddressSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPAddressSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPAddressSchema.setCustomerNo(aLPAddressSchema.getCustomerNo());
            tLPAddressSchema.setAddressNo(aLPAddressSchema.getAddressNo());
            //     System.out.println(tLPGrpEdorItemSchema.getEdorType());
//      tLPGrpAddressSchema.setCustomerNo(aLPGrpAddressSchema.getCustomerNo());

            tLPAddressDB.setSchema(tLPAddressSchema);
            if (!tLPAddressDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPAddressDB.setEdorNo(aLPAddressSchema.getEdorNo());
                tLPAddressDB.setEdorType(aLPAddressSchema.getEdorType());
                this.setSchema(tLPAddressDB.getSchema());
                return true;
            }

        }

//查找已经申请确认的保单批改信息（没有保全确认）

        tLPEdorItemSet.clear();
        m = 0;
        n = 0;

        sql = "select a.* from LPEdorItem a, LPAddress b where b.addressno='" +
              aLPAddressSchema.getAddressNo() + "' and a.edorno=b.edorno and a.EdorState='2' order by a.MakeDate desc,a.MakeTime desc";
        System.out.println(sql);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPAddressDB tLPAddressDB = new LPAddressDB();

            tLPAddressSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPAddressSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPAddressSchema.setCustomerNo(aLPAddressSchema.getCustomerNo());
            tLPAddressSchema.setAddressNo(aLPAddressSchema.getAddressNo());
            tLPAddressDB.setSchema(tLPAddressSchema);
            if (!tLPAddressDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPAddressDB.setEdorNo(aLPAddressSchema.getEdorNo());
                tLPAddressDB.setEdorType(aLPAddressSchema.getEdorType());
                this.setSchema(tLPAddressDB.getSchema());
                return true;
            }
        }

        //如果是第一次申请,得到承保保单的客户信息
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(aLPAddressSchema.getCustomerNo());
        tLCAddressDB.setAddressNo(aLPAddressSchema.getAddressNo());

        if (tLCAddressDB.getInfo())
        {
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLPAddressSchema, tLCAddressDB.getSchema());

            tLPAddressSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPAddressSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            this.setSchema(tLPAddressSchema);
            return true;

        }
        else
        {
            CError.buildErr(this, "查询团体地址信息错误！");
            return false;
        }
    }


}
