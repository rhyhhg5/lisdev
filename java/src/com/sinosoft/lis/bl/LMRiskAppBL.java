/*
 * <p>ClassName: LMRiskAppBL </p>
 * <p>Description: LMRiskAppSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2002-07-10
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LMRiskAppBL extends LMRiskAppSchema
{
	// @Constructor
	public LMRiskAppBL() {
  }
  public boolean  queryDB(String cRiskCode,String cRiskVer)
  {
    //查询险种描述表
    try
    {
    LMRiskAppDB tLMRiskAppDB=new LMRiskAppDB();
    tLMRiskAppDB.setRiskCode(cRiskCode);
    tLMRiskAppDB.setRiskVer(cRiskVer);
    this.setSchema((LMRiskAppSchema)tLMRiskAppDB.query().get(1));
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LMRiskAppBL";
      tError.functionName="queryDB";
      tError.errorMessage="查询投保险种描述表LMRiskApp时出错!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true ;
  }
}
