/*
 * <p>ClassName: LDPersonBL </p>
 * <p>Description: LDPersonSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-04-01
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LDPersonBL extends LDPersonSchema
{
	// @Constructor
	public LDPersonBL() {}

	/**
	* 通过客户号码查询数据库，得到该客户的信息
	* @param: no
	* @return: no
	* @author: YT
	**/
  public  void queryDataByDB(String cCustomerNo)
  {
    LDPersonDB tLDPersonDB=new LDPersonDB();
    tLDPersonDB.setCustomerNo(cCustomerNo) ;
    this.setSchema(tLDPersonDB.query().get(1));
  }
}
