/*
* <p>ClassName: LPDutyBL </p>
* <p>Description: LPDutyBLSchemaBL类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 保全
* @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class LPDutyBL extends LPDutySchema
{
    // @Constructor
    public CErrors mErrors = new CErrors(); // 错误信息

    public LPDutyBL()
    {
    }

    public void setUpdateFields()
    {
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }

    /**
     * 查询所有的客户信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPDutySet queryAllLPDuty(LPEdorItemSchema aLPEdorItemSchema)
    {
        LCDutySet tLCDutySet = new LCDutySet();
        Reflections tReflections = new Reflections();
        LPDutySchema tLPDutySchema = new LPDutySchema();
        LPDutySet tLPDutySet = new LPDutySet();

        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(aLPEdorItemSchema.getPolNo());
        tLCDutySet = tLCDutyDB.query();
        if (tLCDutyDB.mErrors.needDealError())
        {
            CError.buildErr(this,"查询责任错误！");
        }


        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            tReflections.transFields(tLPDutySchema, tLCDutySet.get(i));
            tLPDutySchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPDutySchema.setEdorType(aLPEdorItemSchema.getEdorType());

            if (!this.queryLPDuty(tLPDutySchema))
            {
                return tLPDutySet;
            }

            tLPDutySet.add(this.getSchema());
        }

        return tLPDutySet;
    }

    /**
     * 查询所有的保全责任表数据，为保全重算
     * @param aLPEdorItemSchema
     * @return
     */
    public LPDutySet queryAllLPDutyForReCal(LPEdorItemSchema aLPEdorItemSchema)
    {
        Reflections tReflections = new Reflections();

        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(aLPEdorItemSchema.getPolNo());

        LCDutySet tLCDutySet = tLCDutyDB.query();

        LPDutySchema tLPDutySchema = new LPDutySchema();
        LPDutySet tLPDutySet = new LPDutySet();

        for (int i = 1; i <= tLCDutySet.size(); i++)
        {
            tReflections.transFields(tLPDutySchema, tLCDutySet.get(i));
            tLPDutySchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPDutySchema.setEdorType(aLPEdorItemSchema.getEdorType());

            if (!this.queryLPDuty(tLPDutySchema))
            {
                return tLPDutySet;
            }

            tLPDutySet.add(this.getSchema());
        }

        //查询保全责任表中，该保单，未保全确认的，责任代码不在C表中的，保全日期小于当前保全的，数据
        String sql =
            "select * from lpduty where edorno in (select edorno from LPEdorItem where edorstate<>'0')" +
            " and polno='" + aLPEdorItemSchema.getPolNo() +
            "' and dutycode not in (select dutycode from lcduty where polno='" +
            aLPEdorItemSchema.getPolNo() + "') and (MakeDate<'" +
                  aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  aLPEdorItemSchema.getMakeDate() +"' and MakeTime<='" +
                  aLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";


        LPDutyDB tLPDutyDB = new LPDutyDB();
        LPDutySet LPDutySet2 = tLPDutyDB.executeQuery(sql);
        if (tLPDutyDB.mErrors.needDealError())
        {
            CError.buildErr(this,"查询责任错误！");
        }


        tLPDutySet.add(LPDutySet2);

        return tLPDutySet;
    }

    /**
     * 查询该保单下的最新的责任信息项集合
     * @param aLPEdorItemSchema
     * @return
     */
    public LPDutySet queryAllLPDuty2(LPEdorItemSchema aLPEdorItemSchema)
    {
        LCDutySet tLCDutySet = new LCDutySet();
        Reflections tReflections = new Reflections();
        LPDutySet mLPDutySet = new LPDutySet();

        LCDutySchema tLCDutySchema = new LCDutySchema();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m;
        int n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息
        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);
        LPEdorItemSet tempLPEdorItemSet=new LPEdorItemSet();
        tLPEdorItemSet=iLPEdorItemDB.query();
        if (!iLPEdorItemDB.mErrors.needDealError()&&tLPEdorItemSet.size()>0)
        {
            //delete EdorValiDate by Minim at 2003-12-17
            sql = "select * from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
                  "' and and (MakeDate<'" +
                  tLPEdorItemSet.get(1).getMakeDate() + "' or (MakeDate='" +
                  tLPEdorItemSet.get(1).getMakeDate() +"' and MakeTime<='" +
                  tLPEdorItemSet.get(1).getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";

        }
        else if (iLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this,"查询保全项目错误！");
            return null;
        }


        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this,"查询保全项目错误！");
            return null;
        }

        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPDutyDB tLPDutyDB = new LPDutyDB();

            tLPDutyDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPDutyDB.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPDutyDB.setEdorType(tLPEdorItemSchema.getEdorType());
            mLPDutySet = tLPDutyDB.query();
            if (tLPDutyDB.mErrors.needDealError())
            {
                CError.buildErr(this, "查询责任失败！");
                return null;
            }


            if (mLPDutySet == null)
            {
                return null;
            }

            if (mLPDutySet.size() == 0)
            {
                continue;
            }
            else
            {
                for (int j = 1; j <= mLPDutySet.size(); j++)
                {
                    mLPDutySet.get(j).setEdorNo(aLPEdorItemSchema.getEdorNo());
                    mLPDutySet.get(j).setEdorType(aLPEdorItemSchema.getEdorType());
                }

                return mLPDutySet;
            }
        }

        //查找已经申请确认的保单批改信息（没有保全确认）
        tLPEdorItemSet.clear();
        m = 0;
        n = 0;
//----------------------------- 090622注释
//        sql = "select * from LPEdorItem where EdorState='2' and PolNo='" +
//              aLPEdorItemSchema.getPolNo() + "' order by MakeDate desc,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        if (tLPEdorItemDB.mErrors.needDealError())
//        {
//            CError.buildErr(this, "查询保全项目失败！");
//            return null;
//        }
//
//        m = tLPEdorItemSet.size();
//
//        for (int i = 1; i <= m; i++)
//        {
//            tLPEdorItemSchema = new LPEdorItemSchema();
//            tLPEdorItemSchema = tLPEdorItemSet.get(i);
//
//            LPDutyDB tLPDutyDB = new LPDutyDB();
//
//            tLPDutyDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
//            tLPDutyDB.setPolNo(tLPEdorItemSchema.getPolNo());
//            tLPDutyDB.setEdorType(tLPEdorItemSchema.getEdorType());
//            mLPDutySet = tLPDutyDB.query();
//            if (tLPDutyDB.mErrors.needDealError())
//            {
//                CError.buildErr(this, "查询责任失败！");
//                return null;
//            }
//
//
//            if (mLPDutySet == null)
//            {
//                return null;
//            }
//
//            if (mLPDutySet.size() == 0)
//            {
//                continue;
//            }
//            else
//            {
//                for (int j = 1; j <= mLPDutySet.size(); j++)
//                {
//                    mLPDutySet.get(j).setEdorNo(aLPEdorItemSchema.getEdorNo());
//                    mLPDutySet.get(j).setEdorType(aLPEdorItemSchema.getEdorType());
//                }
//
//                return mLPDutySet;
//            }
//        }
//      -----------------------------
        n = 0;

        //如果是第一次申请,得到承保保单的保费项信息
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(aLPEdorItemSchema.getPolNo());
        tLCDutySet = tLCDutyDB.query();
        if (tLCDutyDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询责任失败！");
            return null;
        }

        n = tLCDutySet.size();
        System.out.println("------n:" + n);

        for (int i = 1; i <= n; i++)
        {
            tLCDutySchema = tLCDutySet.get(i);

            LPDutySchema tLPDutySchema = new LPDutySchema();

            //转换Schema
            tReflections.transFields(tLPDutySchema, tLCDutySchema);

            tLPDutySchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPDutySchema.setEdorType(aLPEdorItemSchema.getEdorType());
            mLPDutySet.add(tLPDutySchema);
        }

        return mLPDutySet;
    }

    //查询被保险人变动信息
    public boolean queryLPDuty(LPDutySchema aLPDutySchema)
    {
        LPDutySchema tLPDutySchema = new LPDutySchema();
        LPDutySet aLPDutySet = new LPDutySet();

        LCDutySchema tLCDutySchema = new LCDutySchema();
        LCDutySet tLCDutySet = new LCDutySet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m;
        int n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息
        aLPEdorItemSchema.setEdorNo(aLPDutySchema.getEdorNo());
        aLPEdorItemSchema.setEdorType(aLPDutySchema.getEdorType());
        aLPEdorItemSchema.setPolNo(aLPDutySchema.getPolNo());

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);

        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
              "' order by MakeDate desc,MakeTime desc";
        LPEdorItemSet tempLPEdorItemSet=new LPEdorItemSet();
        tempLPEdorItemSet=iLPEdorItemDB.query();
        if (!iLPEdorItemDB.mErrors.needDealError()&&tempLPEdorItemSet.size()>=1)
        {
            //delete EdorValiDate by Minim at 2003-12-17
            sql = "select * from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
                  "' and (MakeDate<'" +
                  tempLPEdorItemSet.get(1).getMakeDate() + "' or (MakeDate='" +
                  tempLPEdorItemSet.get(1).getMakeDate() +"' and MakeTime<='" +
                  tempLPEdorItemSet.get(1).getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";

        }
        else if (iLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }


        System.out.println(sql);

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (iLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }

        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPDutyDB tLPDutyDB = new LPDutyDB();

            tLPDutySchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPDutySchema.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPDutySchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPDutySchema.setDutyCode(aLPDutySchema.getDutyCode());

            tLPDutyDB.setSchema(tLPDutySchema);

            if (!tLPDutyDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPDutyDB.setEdorNo(aLPDutySchema.getEdorNo());
                tLPDutyDB.setEdorType(aLPDutySchema.getEdorType());
                this.setSchema(tLPDutyDB.getSchema());

                return true;
            }
        }

        //查找已经申请确认的保单批改信息（没有保全确认）
        tLPEdorItemSet.clear();
        m = 0;
        n = 0;
//      ----------------------------- 090622注释
//        sql = "select * from LPEdorItem where EdorState='2' and PolNo='" +
//              aLPEdorItemSchema.getPolNo() + "' order by MakeDate desc,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        if (tLPEdorItemDB.mErrors.needDealError())
//        {
//            CError.buildErr(this, "查询保全项目错误！");
//            return false;
//        }
//
//        m = tLPEdorItemSet.size();
//
//        for (int i = 1; i <= m; i++)
//        {
//            tLPEdorItemSchema = new LPEdorItemSchema();
//            tLPEdorItemSchema = tLPEdorItemSet.get(i);
//
//            LPDutyDB tLPDutyDB = new LPDutyDB();
//
//            tLPDutySchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
//            tLPDutySchema.setPolNo(tLPEdorItemSchema.getPolNo());
//            tLPDutySchema.setEdorType(tLPEdorItemSchema.getEdorType());
//            System.out.println(tLPEdorItemSchema.getEdorType());
//            tLPDutySchema.setDutyCode(aLPDutySchema.getDutyCode());
//
//            tLPDutyDB.setSchema(tLPDutySchema);
//
//            if (!tLPDutyDB.getInfo())
//            {
//                continue;
//            }
//            else
//            {
//                tLPDutyDB.setEdorNo(aLPDutySchema.getEdorNo());
//                tLPDutyDB.setEdorType(aLPDutySchema.getEdorType());
//                this.setSchema(tLPDutyDB.getSchema());
//
//                return true;
//            }
//        }
//      ----------------------------- 090622注释
        n = 0;

        //如果是第一次申请,得到承保保单的客户信息
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCDutySet = tLCDutyDB.query();
        if (tLCDutyDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询责任错误！");
            return false;
        }

        n = tLCDutySet.size();
        System.out.println("------n:" + n);

        for (int i = 1; i <= n; i++)
        {
            tLCDutySchema = tLCDutySet.get(i);

            if (tLCDutySchema.getPolNo().equals(aLPDutySchema.getPolNo()) &&
                    tLCDutySchema.getDutyCode().equals(aLPDutySchema.getDutyCode()))
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPDutySchema, tLCDutySchema);

                tLPDutySchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                tLPDutySchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPDutySchema);

                return true;
            }
        }

        return false;
    }

    //查询上次保全责任项信息
    public boolean queryLastLPDuty(LPEdorItemSchema aLPEdorItemSchema, LPDutySchema aLPDutySchema)
    {
        LPDutySchema tLPDutySchema = new LPDutySchema();
        LPDutySet aLPDutySet = new LPDutySet();

        LCDutySchema tLCDutySchema = new LCDutySchema();
        LCDutySet tLCDutySet = new LCDutySet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m;
        int n;
        m = 0;
        n = 0;

        //查找最近申请的保单批改信息
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);

        //delete EdorValiDate by Minim at 2003-12-17
        sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where PolNo='" +
              aLPEdorItemSchema.getPolNo() + "' and edorstate <>'0' and (MakeDate<'" +
                  aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  aLPEdorItemSchema.getMakeDate() +"' and MakeTime<='" +
                  aLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";

        System.out.println(sql);

        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询保全项目错误！");
            return false;
        }

        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPDutyDB tLPDutyDB = new LPDutyDB();

            tLPDutySchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPDutySchema.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPDutySchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPDutySchema.setDutyCode(aLPDutySchema.getDutyCode());

            tLPDutyDB.setSchema(tLPDutySchema);

            if (!tLPDutyDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPDutyDB.setEdorNo(aLPDutySchema.getEdorNo());
                tLPDutyDB.setEdorType(aLPDutySchema.getEdorType());
                this.setSchema(tLPDutyDB.getSchema());

                return true;
            }
        }

        //如果是第一次申请,得到承保保单的客户信息
        LCDutyDB tLCDutyDB = new LCDutyDB();
        tLCDutyDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCDutySet = tLCDutyDB.query();
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this, "查询责任错误！");
            return false;
        }

        n = tLCDutySet.size();
        System.out.println("------n:" + n);

        for (int i = 1; i <= n; i++)
        {
            tLCDutySchema = tLCDutySet.get(i);
            System.out.println("PolNo:" + aLPDutySchema.getPolNo());

            if (tLCDutySchema.getPolNo().equals(aLPDutySchema.getPolNo()) &&
                    tLCDutySchema.getDutyCode().equals(aLPDutySchema.getDutyCode()))
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPDutySchema, tLCDutySchema);

                tLPDutySchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                tLPDutySchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPDutySchema);

                return true;
            }
        }

        return false;
    }
}
