/*
 * <p>ClassName: LPAppntBL </p>
 * <p>Description: LPAppntBLSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全
 * @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LPAppntBL extends LPAppntSchema
{
    // @Constructor
    public CErrors mErrors = new CErrors(); // 错误信息
    public LPAppntBL()
    {}

    public void setUpdateFields()
    {
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }

    //查询被保险人变动信息
    public LPAppntSet queryLPAppnt(LPEdorItemSchema aLPEdorItemSchema)
    {
        LPAppntSchema aLPAppntSchema = new LPAppntSchema();
        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        LPAppntSet aLPAppntSet = new LPAppntSet();

        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
        LCAppntSet tLCAppntSet = new LCAppntSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();
        Reflections tReflections = new Reflections();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息

        sql = "select * from LPEdorItem where EdorNo='" + aLPEdorItemSchema.getEdorNo() + "' and ContNo='" +
              aLPEdorItemSchema.getContNo() + "' order by MakeDate desc,MakeTime desc";

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);
        if (iLPEdorItemDB.getInfo())
        {
            //delete EdorValiDate by Minim at 2003-12-17
            sql = "select * from LPEdorItem where EdorNo='" + aLPEdorItemSchema.getEdorNo() + "' and ContNo='" +
                  aLPEdorItemSchema.getContNo() + "' and MakeDate<='" + iLPEdorItemDB.getMakeDate() + "' and MakeTime<='" + iLPEdorItemDB.getMakeTime() +
                  "' order by MakeDate desc,MakeTime desc";
        }
        System.out.println(sql);

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPAppntDB tLPAppntDB = new LPAppntDB();

            tLPAppntSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPAppntSchema.setContNo(tLPEdorItemSchema.getContNo());
            tLPAppntSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());

            tLPAppntDB.setSchema(tLPAppntSchema);
            aLPAppntSet = tLPAppntDB.query();
            n = aLPAppntSet.size();
            System.out.println("------n" + n);
            if (n == 0)
            {
                continue;
            }
            return aLPAppntSet;
        }

//查找已经申请确认的保单批改信息（没有保全确认）

        tLPEdorItemSet.clear();
        m = 0;
        n = 0;

        sql = "select * from LPEdorItem " +
                "where EdorState = '2' " +
                "and ContNo = '" + aLPEdorItemSchema.getContNo() + "' " +
                "and InsuredNo = '" + aLPEdorItemSchema.getInsuredNo() + "' " +
                "order by MakeDate desc,MakeTime desc";
        System.out.println(sql);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {

            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPAppntDB tLPAppntDB = new LPAppntDB();

            tLPAppntSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPAppntSchema.setContNo(tLPEdorItemSchema.getContNo());
            tLPAppntSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            tLPAppntDB.setSchema(tLPAppntSchema);

            aLPAppntSet.clear();
            aLPAppntSet = tLPAppntDB.query();
            n = aLPAppntSet.size();
            if (n == 0)
            {
                continue;
            }
            return aLPAppntSet;
        }

        n = 0;

        //如果是第一次申请,得到承保保单的客户信息
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(aLPEdorItemSchema.getContNo());

        tLCAppntSet = tLCAppntDB.query();
        n = tLCAppntSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            tLCAppntSchema = tLCAppntSet.get(i);
            //转换Schema
            tReflections.transFields(aLPAppntSchema, tLCAppntSchema);

            aLPAppntSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            aLPAppntSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            aLPAppntSet.add(aLPAppntSchema);
        }
        return aLPAppntSet;
    }

    //查询投保险人变动信息
    public boolean queryLPAppnt(LPAppntSchema aLPAppntSchema)
    {
        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        LPAppntSet aLPAppntSet = new LPAppntSet();

        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
        LCAppntSet tLCAppntSet = new LCAppntSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        aLPEdorItemSchema.setEdorNo(aLPAppntSchema.getEdorNo());
        aLPEdorItemSchema.setEdorType(aLPAppntSchema.getEdorType());
        aLPEdorItemSchema.setContNo(aLPAppntSchema.getContNo());
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        sql = "select * from LPEdorItem where EdorNo='" + aLPEdorItemSchema.getEdorNo() + "' and ContNo='" +
              aLPEdorItemSchema.getContNo() + "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPAppntDB tLPAppntDB = new LPAppntDB();

            tLPAppntSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPAppntSchema.setContNo(tLPEdorItemSchema.getContNo());
            tLPAppntSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPAppntSchema.setAppntNo(aLPAppntSchema.getAppntNo());

            tLPAppntDB.setSchema(tLPAppntSchema);
            if (!tLPAppntDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPAppntDB.setEdorNo(aLPAppntSchema.getEdorNo());
                tLPAppntDB.setEdorType(aLPAppntSchema.getEdorType());
                this.setSchema(tLPAppntDB.getSchema());
                return true;
            }
        }

//查找已经申请确认的保单批改信息（没有保全确认）

        tLPEdorItemSet.clear();
        m = 0;
        n = 0;

        sql = "select * from LPEdorItem where EdorState='2' and ContNo='" + aLPEdorItemSchema.getContNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPAppntDB tLPAppntDB = new LPAppntDB();

            tLPAppntSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPAppntSchema.setContNo(tLPEdorItemSchema.getContNo());
            tLPAppntSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPAppntSchema.setAppntNo(aLPAppntSchema.getAppntNo());
            tLPAppntDB.setSchema(tLPAppntSchema);
            if (!tLPAppntDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPAppntDB.setEdorNo(aLPAppntSchema.getEdorNo());
                tLPAppntDB.setEdorType(aLPAppntSchema.getEdorType());
                this.setSchema(tLPAppntDB.getSchema());
                return true;
            }
        }

        n = 0;

        //如果是第一次申请,得到承保保单的客户信息
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(aLPEdorItemSchema.getContNo());

        tLCAppntSet = tLCAppntDB.query();
        n = tLCAppntSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            tLCAppntSchema = tLCAppntSet.get(i);
            System.out.println("ContNo:" + aLPAppntSchema.getContNo());
            System.out.println("AppntNo:" + aLPAppntSchema.getAppntNo());
            if (tLCAppntSchema.getContNo().equals(aLPAppntSchema.getContNo())
                && tLCAppntSchema.getAppntNo().equals(aLPAppntSchema.getAppntNo()))
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPAppntSchema, tLCAppntSchema);

                tLPAppntSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                tLPAppntSchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPAppntSchema);
                return true;
            }
        }
        return false;
    }

    /**
     * 查询所有的客户信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPAppntSet queryAllLPAppnt(LPEdorItemSchema aLPEdorItemSchema)
    {
        LCAppntSet tLCAppntSet = new LCAppntSet();
        Reflections tReflections = new Reflections();
        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        LPAppntSet tLPAppntSet = new LPAppntSet();

        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(aLPEdorItemSchema.getContNo());
        tLCAppntSet = tLCAppntDB.query();
        for (int i = 1; i <= tLCAppntSet.size(); i++)
        {
            tReflections.transFields(tLPAppntSchema, tLCAppntSet.get(i));
            tLPAppntSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPAppntSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            if (!this.queryLPAppnt(tLPAppntSchema))
            {
                return tLPAppntSet;
            }
            tLPAppntSet.add(this.getSchema());
        }
        return tLPAppntSet;
    }

    //查询上次保全投保人资料信息
    public boolean queryLastLPAppnt(LPEdorItemSchema aLPEdorItemSchema, LPAppntSchema aLPAppntSchema)
    {
        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        LPAppntSet aLPAppntSet = new LPAppntSet();

        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
        LCAppntSet tLCAppntSet = new LCAppntSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m, n;
        m = 0;
        n = 0;

        //查找最近申请的保单批改信息
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);

        //delete EdorValiDate by Minim at 2003-12-17
        sql = "select * from LPEdorItem where ContNo='" + aLPEdorItemSchema.getContNo() +
              "' and edorstate <>'0' and MakeDate<='" + aLPEdorItemSchema.getMakeDate() + "' and MakeTime<'" + aLPEdorItemSchema.getMakeTime() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);
            LPAppntDB tLPAppntDB = new LPAppntDB();

            tLPAppntSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPAppntSchema.setContNo(tLPEdorItemSchema.getContNo());
            tLPAppntSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPAppntSchema.setAppntNo(aLPAppntSchema.getAppntNo());

            tLPAppntDB.setSchema(tLPAppntSchema);
            if (!tLPAppntDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPAppntDB.setEdorNo(aLPAppntSchema.getEdorNo());
                tLPAppntDB.setEdorType(aLPAppntSchema.getEdorType());
                this.setSchema(tLPAppntDB.getSchema());
                return true;
            }
        }

        //如果是第一次申请,得到承保保单的客户信息
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(aLPEdorItemSchema.getContNo());

        tLCAppntSet = tLCAppntDB.query();
        n = tLCAppntSet.size();
        System.out.println("------n:" + n);
        for (int i = 1; i <= n; i++)
        {
            tLCAppntSchema = tLCAppntSet.get(i);
            System.out.println("ContNo:" + aLPAppntSchema.getContNo());
            if (tLCAppntSchema.getContNo().equals(aLPAppntSchema.getContNo())
                && tLCAppntSchema.getAppntNo().equals(aLPAppntSchema.getAppntNo()))
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPAppntSchema, tLCAppntSchema);

                tLPAppntSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                tLPAppntSchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPAppntSchema);
                return true;
            }
        }
        return false;
    }
}
