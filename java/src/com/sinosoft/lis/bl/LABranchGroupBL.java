/*
* <p>ClassName: OLABranchGroupBL </p>
* <p>Description: LABranchGroupBL类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 销售管理
* @CreateDate：2003-06-21
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LABranchGroupBL extends LABranchGroupSchema
{

    // @Constructor
    public LABranchGroupBL() {  }

    /**
     *从正式表或备份表中查询最近的符合条件的机构信息
     * 返回LABranchGroupSchema
     */
    public LABranchGroupSchema getInfo()
    {
        Reflections tR=new Reflections();
        LABranchGroupDB tDB=new LABranchGroupDB();
        tDB.setSchema(this);
        LABranchGroupSet tBGSet = new LABranchGroupSet();
        tBGSet = tDB.query();
        if (tDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tDB.mErrors);
            CError tError =new CError();
            tError.moduleName="LABranchGroupBL";
            tError.functionName="getInfo";
            tError.errorMessage="查询机构信息（正式）失败！";
            this.mErrors .addOneError(tError) ;
            return null;
        }
        if (tBGSet.size() == 0)//如果查询失败，查询B表
        {
            LABranchGroupBDB tDBB = new LABranchGroupBDB();
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            tR.transFields(tLABranchGroupBSchema,this.getSchema());
            tLABranchGroupBSchema.setMakeDate2(this.getMakeDate());
            tLABranchGroupBSchema.setModifyDate2(this.getModifyDate());
            tLABranchGroupBSchema.setMakeTime2(this.getMakeTime());
            tLABranchGroupBSchema.setModifyTime2(this.getModifyTime());
            tLABranchGroupBSchema.setMakeDate("");
            tLABranchGroupBSchema.setModifyDate("");
            tLABranchGroupBSchema.setMakeTime(null);
            tLABranchGroupBSchema.setModifyTime(null);
            tDBB.setSchema(tLABranchGroupBSchema);
            LABranchGroupBSet tBSet = tDBB.query();
            if (tDBB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tDBB.mErrors);
                CError tError =new CError();
                tError.moduleName="LABranchGroupBL";
                tError.functionName="getInfo";
                tError.errorMessage="查询机构信息(备份)失败！";
                this.mErrors .addOneError(tError) ;
                return null;
            }
            else if (tBSet.size() == 0)
            {
                CError tError =new CError();
                tError.moduleName="LABranchGroupBL";
                tError.functionName="getInfo";
                tError.errorMessage="没有查询到机构信息！";
                this.mErrors .addOneError(tError) ;
                return null;
            }
            else
            {
                LABranchGroupSchema tS=new LABranchGroupSchema();
                tR.transFields(tS,tBSet.get(tBSet.size()));
                this.setSchema(tS);
            }
        }
        else
        {
            this.setSchema(tBGSet.get(1));
        }
        return this.getSchema();
    }

    public static void main(String[] args)
    {
        //添加测试代码
    }
}