/*
 * <p>ClassName: LJSGetEndorseBL </p>
 * <p>Description: LJSGetEndorseSchemaBL���ļ� </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate��2002-04-01
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LJSGetEndorseBL extends LJSGetEndorseSchema
{
	// @Constructor
	public LJSGetEndorseBL() {}
        public void setDefaultFields()
        {
          this.setMakeDate(PubFun.getCurrentDate());
          this.setMakeTime(PubFun.getCurrentTime());
          this.setModifyDate(PubFun.getCurrentDate());
          this.setModifyTime(PubFun.getCurrentTime());
        }
}
