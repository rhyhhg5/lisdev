package com.sinosoft.lis.bl;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;


/**
 * <p>Title: Web电子商务系统</p>
 * <p>Description: 查询保单信息</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @version 1.0
 */
public class TestEBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public LCPolSchema mLCPolSchema = new LCPolSchema();

    /** 往界面传输数据的容器 */
    private String mResult = "";

    /** 数据操作字符串 */

    //private String mOperate;
    public TestEBL() {
    }

    public boolean QueryLCPol(VData mInputData) {
      mLCPolSchema = (LCPolSchema) mInputData
                                .getObjectByObjectName("LCPolSchema", 0);
        String strSQL = "select * from lcpol where polno='" +
            mLCPolSchema.getPolNo() + "'";
        LCPolSchema aLCPolSchema = new LCPolSchema();
        LCPolDB aLCPolDB = new LCPolDB();
        LCPolSet aLCPolSet = new LCPolSet();
        aLCPolSet = aLCPolDB.executeQuery(strSQL);

        if ((aLCPolSet == null) || (aLCPolSet.size() != 1)) {
            // @@错误处理
            CError.buildErr(this, "该保单不存在!");

            return false;
        }

        aLCPolSchema = aLCPolSet.get(1);
        mResult = aLCPolSchema.encode();
        System.out.println("==> TestEBL : mResult=" + mResult);

        return true;
    }
    public String getResult(){
        return mResult;
    }
}
