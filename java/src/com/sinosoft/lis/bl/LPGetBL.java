/*
* <p>ClassName: LPInsuredBL </p>
* <p>Description: LPInsuredBLSchemaBL类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 保全
* @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class LPGetBL extends LPGetSchema
{
    // @Constructor
    public CErrors mErrors = new CErrors(); // 错误信息

    public LPGetBL()
    {
    }

    public void setUpdateFields()
    {
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }

    /**
     * 查询所有的客户信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPGetSet queryAllLPGet(LPEdorItemSchema aLPEdorItemSchema)
    {
        LCGetSet tLCGetSet = new LCGetSet();
        Reflections tReflections = new Reflections();
        LPGetSchema tLPGetSchema = new LPGetSchema();
        LPGetSet tLPGetSet = new LPGetSet();

        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(aLPEdorItemSchema.getPolNo());
        tLCGetSet = tLCGetDB.query();

        for (int i = 1; i <= tLCGetSet.size(); i++)
        {
            tReflections.transFields(tLPGetSchema, tLCGetSet.get(i));
            tLPGetSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPGetSchema.setEdorType(aLPEdorItemSchema.getEdorType());

            if (!this.queryLPGet(tLPGetSchema))
            {
                return tLPGetSet;
            }

            tLPGetSet.add(this.getSchema());
        }

        return tLPGetSet;
    }

    /**
     * 查询所有的客户信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPGetSet queryAllLPGetForReCal(LPEdorItemSchema aLPEdorItemSchema)
    {
        Reflections tReflections = new Reflections();

        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(aLPEdorItemSchema.getPolNo());

        LCGetSet tLCGetSet = tLCGetDB.query();

        LPGetSchema tLPGetSchema = new LPGetSchema();
        LPGetSet tLPGetSet = new LPGetSet();

        for (int i = 1; i <= tLCGetSet.size(); i++)
        {
            tReflections.transFields(tLPGetSchema, tLCGetSet.get(i));
            tLPGetSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPGetSchema.setEdorType(aLPEdorItemSchema.getEdorType());

            if (!this.queryLPGet(tLPGetSchema))
            {
                return tLPGetSet;
            }

            tLPGetSet.add(this.getSchema());
        }

        //查询保全保费项表中，该保单，未保全确认的，责任代码不在C表中的，保全日期小于当前保全的，数据
        String sql =
            "select * from lpget where edorno in (select edorno from LPEdorItem where edorstate<>'0')" +
            " and polno='" + aLPEdorItemSchema.getPolNo() +
            "' and getdutycode not in (select getdutycode from lcget where polno='" +
            aLPEdorItemSchema.getPolNo() + ")' and (MakeDate<'" +
            aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
            aLPEdorItemSchema.getMakeDate() + "' and MakeTime<='" +
            aLPEdorItemSchema.getMakeTime() +
            "')) order by MakeDate,MakeTime desc";


        LPGetDB tLPGetDB = new LPGetDB();
        LPGetSet LPGetSet2 = tLPGetDB.executeQuery(sql);

        tLPGetSet.add(LPGetSet2);

        return tLPGetSet;
    }

    //查询领取项信息
    public boolean queryLPGet(LPGetSchema aLPGetSchema)
    {
        LPGetSchema tLPGetSchema = new LPGetSchema();
        LPGetSet aLPGetSet = new LPGetSet();

        LCGetSchema tLCGetSchema = new LCGetSchema();
        LCGetSet tLCGetSet = new LCGetSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m;
        int n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息
        aLPEdorItemSchema.setEdorNo(aLPGetSchema.getEdorNo());
        aLPEdorItemSchema.setEdorType(aLPGetSchema.getEdorType());
        aLPEdorItemSchema.setPolNo(aLPGetSchema.getPolNo());

        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
              "' order by MakeDate desc,MakeTime desc";

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);

        if (iLPEdorItemDB.getInfo())
        {
            //delete EdorValiDate by Minim at 2003-12-17
            sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
                  "' and (MakeDate<'" +
                  aLPEdorItemSchema.getMakeDate() + "' or (MakeDate='" +
                  aLPEdorItemSchema.getMakeDate() + "' and MakeTime<='" +
                  aLPEdorItemSchema.getMakeTime() +
                  "')) order by MakeDate,MakeTime desc";

        }

        System.out.println(sql);

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        if (tLPEdorItemDB.mErrors.needDealError())
        {
            CError.buildErr(this,"查询保全项目错误！");
            return false;
        }

        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPGetDB tLPGetDB = new LPGetDB();

            tLPGetSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPGetSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPGetSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPGetSchema.setDutyCode(aLPGetSchema.getDutyCode());
            tLPGetSchema.setGetDutyCode(aLPGetSchema.getGetDutyCode());

            tLPGetDB.setSchema(tLPGetSchema);

            if (!tLPGetDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPGetDB.setEdorNo(aLPGetSchema.getEdorNo());
                tLPGetDB.setEdorType(aLPGetSchema.getEdorType());
                this.setSchema(tLPGetDB.getSchema());

                return true;
            }
        }

        //查找已经申请确认的保单批改信息（没有保全确认）
        tLPEdorItemSet.clear();
        m = 0;
        n = 0;
//      ----------------------------- 090622注释
//        sql = "select * from LPEdorItem where EdorState='2' and PolNo='" +
//              aLPEdorItemSchema.getPolNo() + "' order by MakeDate desc,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        if (tLPEdorItemDB.mErrors.needDealError())
//        {
//            CError.buildErr(this,"查询保全项目错误！");
//            return false;
//        }
//
//        m = tLPEdorItemSet.size();
//
//        for (int i = 1; i <= m; i++)
//        {
//            tLPEdorItemSchema = new LPEdorItemSchema();
//            tLPEdorItemSchema = tLPEdorItemSet.get(i);
//
//            LPGetDB tLPGetDB = new LPGetDB();
//
//            tLPGetSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
//            tLPGetSchema.setPolNo(tLPEdorItemSchema.getPolNo());
//            tLPGetSchema.setEdorType(tLPEdorItemSchema.getEdorType());
//            System.out.println(tLPEdorItemSchema.getEdorType());
//            tLPGetSchema.setDutyCode(aLPGetSchema.getDutyCode());
//            tLPGetSchema.setGetDutyCode(aLPGetSchema.getGetDutyCode());
//
//            tLPGetDB.setSchema(tLPGetSchema);
//
//            if (!tLPGetDB.getInfo())
//            {
//                continue;
//            }
//            else
//            {
//                tLPGetDB.setEdorNo(aLPGetSchema.getEdorNo());
//                tLPGetDB.setEdorType(aLPGetSchema.getEdorType());
//                this.setSchema(tLPGetDB.getSchema());
//
//                return true;
//            }
//        }
//      ----------------------------- 090622注释
        n = 0;

        //如果是第一次申请,得到承保保单的客户信息
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCGetSet = tLCGetDB.query();
        if (tLCGetDB.mErrors.needDealError())
        {
            CError.buildErr(this,"查询保全项目错误！");
            return false;
        }

        n = tLCGetSet.size();
        System.out.println("------n:" + n);

        for (int i = 1; i <= n; i++)
        {
            tLCGetSchema = tLCGetSet.get(i);

            if (tLCGetSchema.getPolNo().equals(aLPGetSchema.getPolNo()) &&
                    tLCGetSchema.getDutyCode().equals(aLPGetSchema.getDutyCode()) &&
                    tLCGetSchema.getGetDutyCode().equals(aLPGetSchema.getGetDutyCode()))
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPGetSchema, tLCGetSchema);

                tLPGetSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                tLPGetSchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPGetSchema);

                return true;
            }
        }

        return false;
    }

    //查询上次保全给付信息
    public boolean queryLastLPGet(LPEdorItemSchema aLPEdorItemSchema, LPGetSchema aLPGetSchema)
    {
        LPGetSchema tLPGetSchema = new LPGetSchema();
        LPGetSet aLPGetSet = new LPGetSet();

        LCGetSchema tLCGetSchema = new LCGetSchema();
        LCGetSet tLCGetSet = new LCGetSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m;
        int n;
        m = 0;
        n = 0;

        //查找最近申请的保单批改信息
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);

        //delete EdorValiDate by Minim at 2003-12-17
        sql = "select EdorNo,PolNo,EdorType,EdorValiDate,MakeTime from LPEdorItem where PolNo='" +
              aLPEdorItemSchema.getPolNo() + "' and edorstate <>'0' and MakeDate<='" +
              aLPEdorItemSchema.getMakeDate() + "' and MakeTime<'" +
              aLPEdorItemSchema.getMakeTime() + "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPGetDB tLPGetDB = new LPGetDB();

            tLPGetSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPGetSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPGetSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPGetSchema.setDutyCode(aLPGetSchema.getDutyCode());
            tLPGetSchema.setGetDutyCode(aLPGetSchema.getGetDutyCode());

            tLPGetDB.setSchema(tLPGetSchema);

            if (!tLPGetDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPGetDB.setEdorNo(aLPGetSchema.getEdorNo());
                tLPGetDB.setEdorType(aLPGetSchema.getEdorType());
                this.setSchema(tLPGetDB.getSchema());

                return true;
            }
        }

        //如果是第一次申请,得到承保保单的客户信息
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCGetSet = tLCGetDB.query();
        n = tLCGetSet.size();
        System.out.println("------n:" + n);

        for (int i = 1; i <= n; i++)
        {
            tLCGetSchema = tLCGetSet.get(i);
            System.out.println("PolNo:" + aLPGetSchema.getPolNo());

            if (tLCGetSchema.getPolNo().equals(aLPGetSchema.getPolNo()) &&
                    tLCGetSchema.getDutyCode().equals(aLPGetSchema.getDutyCode()) &&
                    tLCGetSchema.getGetDutyCode().equals(aLPGetSchema.getGetDutyCode()))
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPGetSchema, tLCGetSchema);

                tLPGetSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                tLPGetSchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPGetSchema);

                return true;
            }
        }

        return false;
    }
}
