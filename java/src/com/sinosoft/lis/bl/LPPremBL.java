/*
* <p>ClassName: LPInsuredBL </p>
* <p>Description: LPInsuredBLSchemaBL类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 保全
* @CreateDate：2002-07-25
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class LPPremBL extends LPPremSchema
{
    // @Constructor
    public CErrors mErrors = new CErrors(); // 错误信息

    public LPPremBL()
    {
    }

    public void setUpdateFields()
    {
        this.setModifyDate(PubFun.getCurrentDate());
        this.setModifyTime(PubFun.getCurrentTime());
    }

    /**
     * 查询所有的客户信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPPremSet queryAllLPPrem(LPEdorItemSchema aLPEdorItemSchema)
    {
        LCPremSet tLCPremSet = new LCPremSet();
        Reflections tReflections = new Reflections();
        LPPremSchema tLPPremSchema = new LPPremSchema();
        LPPremSet tLPPremSet = new LPPremSet();

        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(aLPEdorItemSchema.getPolNo());
        tLCPremSet = tLCPremDB.query();

        for (int i = 1; i <= tLCPremSet.size(); i++)
        {
            tReflections.transFields(tLPPremSchema, tLCPremSet.get(i));
            tLPPremSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPPremSchema.setEdorType(aLPEdorItemSchema.getEdorType());

            if (!this.queryLPPrem(tLPPremSchema))
            {
                return tLPPremSet;
            }

            tLPPremSet.add(this.getSchema());
        }

        return tLPPremSet;
    }

    /**
     * 查询所有的保全保费项表数据，为保全重算
     * @param aLPEdorItemSchema
     * @return
     */
    public LPPremSet queryAllLPPremForReCal(LPEdorItemSchema aLPEdorItemSchema)
    {
        Reflections tReflections = new Reflections();

        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(aLPEdorItemSchema.getPolNo());

        LCPremSet tLCPremSet = tLCPremDB.query();

        LPPremSchema tLPPremSchema = new LPPremSchema();
        LPPremSet tLPPremSet = new LPPremSet();

        for (int i = 1; i <= tLCPremSet.size(); i++)
        {
            tReflections.transFields(tLPPremSchema, tLCPremSet.get(i));
            tLPPremSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPPremSchema.setEdorType(aLPEdorItemSchema.getEdorType());

            if (!this.queryLPPrem(tLPPremSchema))
            {
                return tLPPremSet;
            }

            tLPPremSet.add(this.getSchema());
        }

        //查询保全保费项表中，该保单，未保全确认的，责任代码不在C表中的，保全日期小于当前保全的，数据
        String sql =
            "select * from lpprem where edorno in (select edorno from LPEdorItem where edorstate<>'0')" +
            " and polno='" + aLPEdorItemSchema.getPolNo() +
            "' and payplancode not in (select payplancode from lcprem where polno='" +
            aLPEdorItemSchema.getPolNo() + ")' and MakeDate<='" + aLPEdorItemSchema.getMakeDate() +
            "' and MakeTime<='" + aLPEdorItemSchema.getMakeTime() +
            "' order by MakeDate desc, MakeTime desc";

        LPPremDB tLPPremDB = new LPPremDB();
        LPPremSet LPPremSet2 = tLPPremDB.executeQuery(sql);

        tLPPremSet.add(LPPremSet2);

        return tLPPremSet;
    }

    /**
     * 查询一个责任下所有的保费项信息
     * @param aLPEdorItemSchema
     * @return
     */
    public LPPremSet queryAllLPPrem(LPDutySchema pLPDutySchema)
    {
        LCPremSet tLCPremSet = new LCPremSet();
        Reflections tReflections = new Reflections();
        LPPremSchema tLPPremSchema = new LPPremSchema();
        LPPremSet tLPPremSet = new LPPremSet();

        //用保单号和责任编码一起限定查找
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(pLPDutySchema.getPolNo());
        tLCPremDB.setDutyCode(pLPDutySchema.getDutyCode());
        tLCPremSet = tLCPremDB.query();

        for (int i = 1; i <= tLCPremSet.size(); i++)
        {
            tReflections.transFields(tLPPremSchema, tLCPremSet.get(i));
            tLPPremSchema.setEdorNo(pLPDutySchema.getEdorNo());
            tLPPremSchema.setEdorType(pLPDutySchema.getEdorType());

            if (!this.queryLPPrem(tLPPremSchema))
            {
                return tLPPremSet;
            }

            tLPPremSet.add(this.getSchema());
        }

        return tLPPremSet;
    }

    /**
     * 查询所有该保单下的最新保费项信息。add by sxy2004-03-06
     * @param aLPEdorItemSchema
     * @return
     */
    public LPPremSet queryAllLPPrem2(LPEdorItemSchema aLPEdorItemSchema)
    {
        LCPremSet tLCPremSet = new LCPremSet();
        Reflections tReflections = new Reflections();
        LPPremSet mLPPremSet = new LPPremSet();

        LCPremSchema tLCPremSchema = new LCPremSchema();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m;
        int n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息
        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);

        if (iLPEdorItemDB.getInfo())
        {
            //delete EdorValiDate by Minim at 2003-12-17
            sql = "select * from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
                  "' and MakeDate<='" + iLPEdorItemDB.getMakeDate() + "' and MakeTime<='" +
                  iLPEdorItemDB.getMakeTime() + "' order by MakeDate desc,MakeTime desc";
        }

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPPremDB tLPPremDB = new LPPremDB();

            tLPPremDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPPremDB.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPPremDB.setEdorType(tLPEdorItemSchema.getEdorType());
            mLPPremSet = tLPPremDB.query();

            if (mLPPremSet == null)
            {
                return null;
            }

            if (mLPPremSet.size() == 0)
            {
                continue;
            }
            else
            {
                for (int j = 1; j <= mLPPremSet.size(); j++)
                {
                    mLPPremSet.get(j).setEdorNo(aLPEdorItemSchema.getEdorNo());
                    mLPPremSet.get(j).setEdorType(aLPEdorItemSchema.getEdorType());
                }

                return mLPPremSet;
            }
        }

        //查找已经申请确认的保单批改信息（没有保全确认）
        tLPEdorItemSet.clear();
        m = 0;
        n = 0;
//      ----------------------------- 090622注释
//        sql = "select * from LPEdorItem where EdorState='2' and PolNo='" +
//              aLPEdorItemSchema.getPolNo() + "' order by MakeDate desc,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        m = tLPEdorItemSet.size();
//
//        for (int i = 1; i <= m; i++)
//        {
//            tLPEdorItemSchema = new LPEdorItemSchema();
//            tLPEdorItemSchema = tLPEdorItemSet.get(i);
//
//            LPPremDB tLPPremDB = new LPPremDB();
//
//            tLPPremDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
//            tLPPremDB.setPolNo(tLPEdorItemSchema.getPolNo());
//            tLPPremDB.setEdorType(tLPEdorItemSchema.getEdorType());
//            mLPPremSet = tLPPremDB.query();
//
//            if (mLPPremSet == null)
//            {
//                return null;
//            }
//
//            if (mLPPremSet.size() == 0)
//            {
//                continue;
//            }
//            else
//            {
//                for (int j = 1; j <= mLPPremSet.size(); j++)
//                {
//                    mLPPremSet.get(j).setEdorNo(aLPEdorItemSchema.getEdorNo());
//                    mLPPremSet.get(j).setEdorType(aLPEdorItemSchema.getEdorType());
//                }
//
//                return mLPPremSet;
//            }
//        }
//      ----------------------------- 090622注释
        n = 0;

        //如果是第一次申请,得到承保保单的保费项信息
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(aLPEdorItemSchema.getPolNo());
        tLCPremSet = tLCPremDB.query();
        n = tLCPremSet.size();
        System.out.println("------n:" + n);

        for (int i = 1; i <= n; i++)
        {
            tLCPremSchema = tLCPremSet.get(i);

            LPPremSchema tLPPremSchema = new LPPremSchema();

            //转换Schema
            tReflections.transFields(tLPPremSchema, tLCPremSchema);

            tLPPremSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
            tLPPremSchema.setEdorType(aLPEdorItemSchema.getEdorType());
            mLPPremSet.add(tLPPremSchema);
            ;
        }

        return mLPPremSet;
    }

    //查询被保险人变动信息
    public boolean queryLPPrem(LPPremSchema aLPPremSchema)
    {
        LPPremSchema tLPPremSchema = new LPPremSchema();
        LPPremSet aLPPremSet = new LPPremSet();

        LCPremSchema tLCPremSchema = new LCPremSchema();
        LCPremSet tLCPremSet = new LCPremSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m;
        int n;
        m = 0;
        n = 0;

        //查找本次申请的保单批改信息
        aLPEdorItemSchema.setEdorNo(aLPPremSchema.getEdorNo());
        aLPEdorItemSchema.setEdorType(aLPPremSchema.getEdorType());
        aLPEdorItemSchema.setPolNo(aLPPremSchema.getPolNo());

        sql = "select * from LPEdorItem where EdorNo='" +
              aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
              "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        LPEdorItemDB iLPEdorItemDB = new LPEdorItemDB();
        iLPEdorItemDB.setSchema(aLPEdorItemSchema);

        if (iLPEdorItemDB.getInfo())
        {
            //delete EdorValiDate by Minim at 2003-12-17
            sql = "select * from LPEdorItem where EdorNo='" +
                  aLPEdorItemSchema.getEdorNo() + "' and PolNo='" + aLPEdorItemSchema.getPolNo() +
                  "' and MakeDate<='" + iLPEdorItemDB.getMakeDate() + "' and MakeTime<='" +
                  iLPEdorItemDB.getMakeTime() + "' order by MakeDate desc,MakeTime desc";
        }

        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);
        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPPremDB tLPPremDB = new LPPremDB();

            tLPPremSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPPremSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPPremSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPPremSchema.setDutyCode(aLPPremSchema.getDutyCode());
            tLPPremSchema.setPayPlanCode(aLPPremSchema.getPayPlanCode());

            tLPPremDB.setSchema(tLPPremSchema);

            if (!tLPPremDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPPremDB.setEdorNo(aLPPremSchema.getEdorNo());
                tLPPremDB.setEdorType(aLPPremSchema.getEdorType());
                this.setSchema(tLPPremDB.getSchema());

                return true;
            }
        }

        //查找已经申请确认的保单批改信息（没有保全确认）
        tLPEdorItemSet.clear();
        m = 0;
        n = 0;
//      ----------------------------- 090622注释
//        sql = "select * from LPEdorItem where EdorState='2' and PolNo='" +
//              aLPEdorItemSchema.getPolNo() + "' order by MakeDate desc,MakeTime desc";
//        System.out.println(sql);
//        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
//        m = tLPEdorItemSet.size();
//
//        for (int i = 1; i <= m; i++)
//        {
//            tLPEdorItemSchema = new LPEdorItemSchema();
//            tLPEdorItemSchema = tLPEdorItemSet.get(i);
//
//            LPPremDB tLPPremDB = new LPPremDB();
//
//            tLPPremSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
//            tLPPremSchema.setPolNo(tLPEdorItemSchema.getPolNo());
//            tLPPremSchema.setEdorType(tLPEdorItemSchema.getEdorType());
//            System.out.println(tLPEdorItemSchema.getEdorType());
//            tLPPremSchema.setDutyCode(aLPPremSchema.getDutyCode());
//            tLPPremSchema.setPayPlanCode(aLPPremSchema.getPayPlanCode());
//
//            tLPPremDB.setSchema(tLPPremSchema);
//
//            if (!tLPPremDB.getInfo())
//            {
//                continue;
//            }
//            else
//            {
//                tLPPremDB.setEdorNo(aLPPremSchema.getEdorNo());
//                tLPPremDB.setEdorType(aLPPremSchema.getEdorType());
//                this.setSchema(tLPPremDB.getSchema());
//
//                return true;
//            }
//        }
//      ----------------------------- 090622注释
        n = 0;

        //如果是第一次申请,得到承保保单的客户信息
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCPremSet = tLCPremDB.query();
        n = tLCPremSet.size();
        System.out.println("------n:" + n);

        for (int i = 1; i <= n; i++)
        {
            tLCPremSchema = tLCPremSet.get(i);

            if (tLCPremSchema.getPolNo().equals(aLPPremSchema.getPolNo()) &&
                    tLCPremSchema.getDutyCode().equals(aLPPremSchema.getDutyCode()) &&
                    tLCPremSchema.getPayPlanCode().equals(aLPPremSchema.getPayPlanCode()))
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPPremSchema, tLCPremSchema);

                tLPPremSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                tLPPremSchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPPremSchema);

                return true;
            }
        }

        return false;
    }

    //查询上次保全保费项信息
    public boolean queryLastLPPrem(LPEdorItemSchema aLPEdorItemSchema, LPPremSchema aLPPremSchema)
    {
        LPPremSchema tLPPremSchema = new LPPremSchema();
        LPPremSet aLPPremSet = new LPPremSet();

        LCPremSchema tLCPremSchema = new LCPremSchema();
        LCPremSet tLCPremSet = new LCPremSet();
        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
        LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet();

        String sql;
        int m;
        int n;
        m = 0;
        n = 0;

        //查找最近申请的保单批改信息
        LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
        tLPEdorItemDB.setSchema(aLPEdorItemSchema);

        //delete EdorValiDate by Minim at 2003-12-17
        sql = "select * from LPEdorItem where PolNo='" +
              aLPEdorItemSchema.getPolNo() + "' and edorstate <>'0' and MakeDate<='" +
              aLPEdorItemSchema.getMakeDate() + "' and MakeTime<'" +
              aLPEdorItemSchema.getMakeTime() + "' order by MakeDate desc,MakeTime desc";
        System.out.println(sql);

        tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
        m = tLPEdorItemSet.size();

        for (int i = 1; i <= m; i++)
        {
            tLPEdorItemSchema = new LPEdorItemSchema();
            tLPEdorItemSchema = tLPEdorItemSet.get(i);

            LPPremDB tLPPremDB = new LPPremDB();

            tLPPremSchema.setEdorNo(tLPEdorItemSchema.getEdorNo());
            tLPPremSchema.setPolNo(tLPEdorItemSchema.getPolNo());
            tLPPremSchema.setEdorType(tLPEdorItemSchema.getEdorType());
            System.out.println(tLPEdorItemSchema.getEdorType());
            tLPPremSchema.setDutyCode(aLPPremSchema.getDutyCode());
            tLPPremSchema.setPayPlanCode(aLPPremSchema.getPayPlanCode());

            tLPPremDB.setSchema(tLPPremSchema);

            if (!tLPPremDB.getInfo())
            {
                continue;
            }
            else
            {
                tLPPremDB.setEdorNo(aLPPremSchema.getEdorNo());
                tLPPremDB.setEdorType(aLPPremSchema.getEdorType());
                this.setSchema(tLPPremDB.getSchema());

                return true;
            }
        }

        //如果是第一次申请,得到承保保单的客户信息
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setPolNo(aLPEdorItemSchema.getPolNo());

        tLCPremSet = tLCPremDB.query();
        n = tLCPremSet.size();
        System.out.println("------n:" + n);

        for (int i = 1; i <= n; i++)
        {
            tLCPremSchema = tLCPremSet.get(i);
            System.out.println("PolNo:" + aLPPremSchema.getPolNo());

            if (tLCPremSchema.getPolNo().equals(aLPPremSchema.getPolNo()) &&
                    tLCPremSchema.getDutyCode().equals(aLPPremSchema.getDutyCode()) &&
                    tLCPremSchema.getPayPlanCode().equals(aLPPremSchema.getPayPlanCode()))
            {
                //转换Schema
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLPPremSchema, tLCPremSchema);

                tLPPremSchema.setEdorNo(aLPEdorItemSchema.getEdorNo());
                tLPPremSchema.setEdorType(aLPEdorItemSchema.getEdorType());
                this.setSchema(tLPPremSchema);

                return true;
            }
        }

        return false;
    }
}
