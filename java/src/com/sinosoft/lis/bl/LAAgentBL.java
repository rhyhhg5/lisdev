/*
 * <p>ClassName: LAAgentBL </p>
 * <p>Description: LAAgentSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-08-15
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LAAgentBL extends LAAgentSchema
{
	// @Constructor
	public LAAgentBL() {}
        public void setDefaultFields()
        {
          this.setMakeDate(PubFun.getCurrentDate());
          this.setMakeTime(PubFun.getCurrentTime());
          this.setModifyDate(PubFun.getCurrentDate());
          this.setModifyTime(PubFun.getCurrentTime());
        }
        public void setUpdateFields()
        {
           this.setModifyDate(PubFun.getCurrentDate());
           this.setModifyTime(PubFun.getCurrentTime());
        }

        /**
         * 校验代理人是否有权卖某险种.
         * @param aAgentCode
         * @param aRiskCode
         * @return
         */
        public boolean validateAgent(String aAgentCode,String aRiskCode)
        {
          LAAgentSet tLAAgentSet = new LAAgentSet();
          LAAgentDB tLAAgentDB = new LAAgentDB();
          tLAAgentDB.setAgentCode(aAgentCode);
          String tSql = "select * from LAAgent where agentcode='"+aAgentCode+"' and agentstate in ('01','02')";
          tLAAgentSet = tLAAgentDB.executeQuery(tSql);
          if (tLAAgentSet.size()>0)
          {
            LMRiskAppSet tLMRiskAppSet = new LMRiskAppSet();
            LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
            tLMRiskAppDB.setRiskCode(aRiskCode);
            tLMRiskAppSet = tLMRiskAppDB.query();
            if (tLMRiskAppSet.size()>0)
            {
              //传统险种
              if (StrTool.cTrim(tLMRiskAppSet.get(tLMRiskAppSet.size()).getRiskType3()).equals("1"))
              {
                System.out.println("传统型险种");
                return true;
              }
              //非传统险种
              else
              {
                LAAuthorizeDB tLAAuthorizeDB = new LAAuthorizeDB();
                tLAAuthorizeDB.setRiskCode(aRiskCode);
                tLAAuthorizeDB.setAgentCode(aAgentCode);
                if (!tLAAuthorizeDB.getInfo())
                  return false;
                else
                {
                  FDate tFDate = new FDate();
                  if (StrTool.cTrim(tLAAuthorizeDB.getAuthorEndDate()).equals("")
                      ||(tFDate.getDate(tLAAuthorizeDB.getAuthorStartDate()).compareTo(tFDate.getDate(PubFun.getCurrentDate()))<=0&&tFDate.getDate(tLAAuthorizeDB.getAuthorEndDate()).compareTo(tFDate.getDate(PubFun.getCurrentDate()))>=0))
                  {
                    return true;
                  }
                }
              }
            }
          }
          return false;
        }

        public String findChannel(String aAgentCode)
        {
          LAComToAgentDB tLAComToAgentDB = new LAComToAgentDB();
          tLAComToAgentDB.setAgentCode(aAgentCode);
          tLAComToAgentDB.setRelaType("1");
          LAComToAgentSet tLAComToAgentSet = tLAComToAgentDB.query();
          if (tLAComToAgentDB.mErrors.needDealError())
          {
            //@错误处理
            this.mErrors.copyAllErrors(tLAComToAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName="LAAgentBL";
            tError.functionName="findChannel";
            tError.errorMessage="代理人"+aAgentCode+"得到LAComToAgent数据出错！！！";
            this.mErrors.addOneError(tError);
            return null;
          }
          if (tLAComToAgentSet.size() == 0)
          {
            //@错误处理
            this.mErrors.copyAllErrors(tLAComToAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName="LAAgentBL";
            tError.functionName="findChannel";
            tError.errorMessage="代理人"+aAgentCode+"没有对应网点（LAComToAgent）";
            this.mErrors.addOneError(tError);
            return null;
          }
          LAComToAgentSchema tLAComToAgentSchema = tLAComToAgentSet.get(1);
          LAComDB tLAComDB = new LAComDB();
          tLAComDB.setAgentCom(tLAComToAgentSchema.getAgentCom());
          if (!tLAComDB.getInfo())
          {
            //@错误处理
            this.mErrors.copyAllErrors(tLAComDB.mErrors);
            CError tError = new CError();
            tError.moduleName="LAAgentBL";
            tError.functionName="findChannel";
            tError.errorMessage="代理机构"+tLAComToAgentSchema.getAgentCom()+"得到LACom数据出错！！！";
            this.mErrors.addOneError(tError);
            return null;
          }
          return tLAComDB.getChannelType();
        }
        /**
        *
        * @param aAgentCom
        * @return
        */
       public String findChannelByCom(String aAgentCom)
      {
        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setAgentCom(aAgentCom);
        if (!tLAComDB.getInfo())
        {
          //@错误处理
          this.mErrors.copyAllErrors(tLAComDB.mErrors);
          CError tError = new CError();
          tError.moduleName="LAAgentBL";
          tError.functionName="findChannel";
          tError.errorMessage="得到LACom数据出错！！！";
          this.mErrors.addOneError(tError);
        }
        return tLAComDB.getChannelType();
        }
}
