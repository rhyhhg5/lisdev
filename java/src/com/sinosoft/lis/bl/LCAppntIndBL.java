/*
 * <p>ClassName: LCAppntIndBL </p>
 * <p>Description: LCAppntIndSchemaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-04-01
 */
package com.sinosoft.lis.bl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LCAppntIndBL extends LCAppntIndSchema
{
	// @Constructor
	public LCAppntIndBL() {
    this.setModifyDate(PubFun.getCurrentDate());
    this.setModifyTime(PubFun.getCurrentTime()) ;
  }
  /**
   * 设置默认的字段属性
   */
  public void setDefaultFields()
  {
    this.setModifyDate(PubFun.getCurrentDate());
    this.setModifyTime(PubFun.getCurrentTime()) ;
  }

	/**
	* 通过客户号码查询数据库，得到该客户的信息，放到投保人对应的字段中。
	* @param: cCustomerNo 客户号 ,cType 查询类型，1，表示对于输入的字段不覆盖
	* @return: no
	* @author: YT
	**/
  public  void queryCustomerDataByDB(String cCustomerNo,String cType)
  {
    boolean isGrp=false;

    if(!isGrp)    //处理个人客户
    {
      LDPersonBL tBL=new LDPersonBL();
      tBL.queryDataByDB(cCustomerNo);
//    this.setAppntGrade();//不清楚是否需要从界面录入
//*    this.setRelationToInsured();
//*    this.setCustomerNo();
    this.setPassword(tBL.getPassword());
//*    this.setName();
//*    this.setSex();
//*    this.setBirthday();
    this.setNativePlace(tBL.getNativePlace() );
    this.setNationality(tBL.getNationality());
    this.setMarriage(tBL.getMarriage() );
    this.setMarriageDate(tBL.getMarriageDate() );
    this.setOccupationType(tBL.getOccupationType() );
    this.setStartWorkDate(tBL.getStartWorkDate());
    this.setSalary(tBL.getSalary() );
    this.setHealth(tBL.getHealth() );
    this.setStature(tBL.getStature() );
    this.setAvoirdupois(tBL.getAvoirdupois() );
    this.setCreditGrade(tBL.getCreditGrade() );

    this.setProterty(tBL.getProterty() );

    this.setOthIDType(tBL.getOthIDType() );
    this.setOthIDNo(tBL.getOthIDNo() );
    this.setICNo(tBL.getICNo() );
    /*Lis5.3 upgrade set
    this.setHomeAddressCode(tBL.getHomeAddressCode());
    this.setHomeAddress(tBL.getHomeAddress() );
    this.setBP(tBL.getBP() );
    this.setGrpName(tBL.getGrpName() );
    this.setGrpPhone(tBL.getGrpPhone() );
    this.setGrpAddressCode(tBL.getGrpAddressCode() );
    this.setGrpAddress(tBL.getGrpAddress() );
    */
    this.setJoinCompanyDate(tBL.getJoinCompanyDate() );
    this.setPosition(tBL.getPosition() );
    this.setGrpNo(tBL.getGrpNo());
    this.setDeathDate(tBL.getDeathDate());
    this.setRemark(tBL.getRemark() );
    this.setState(tBL.getState() );
    }
  }


  /**
   *从多投保人（个人）表和多投保人（个人）备份表读取信息
   * 返回true或false
   */
  public boolean getInfo()
  {
    Reflections tR=new Reflections();
    LCAppntIndDB tDB=new LCAppntIndDB();
    tDB.setSchema(this);
    if (!tDB.getInfo())//如果查询失败，查询B表
    {
      LBAppntIndDB tDBB=new LBAppntIndDB();
      LBAppntIndSchema tLBAppntIndSchema=new LBAppntIndSchema();
      tR.transFields(tLBAppntIndSchema,this.getSchema());
      tDBB.setSchema(tLBAppntIndSchema);
      if (! tDBB.getInfo())
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="LCAppntIndBL";
        tError.functionName="getInfo";
        tError.errorMessage="没有查询到多投保人（个人）表";
        this.mErrors .addOneError(tError) ;
        return false;
      }
      else
      {
        LCAppntIndSchema tS=new LCAppntIndSchema();
        tR.transFields(tS,tDBB.getSchema());
        this.setSchema(tS);
      }
    }
    else
    {
      this.setSchema(tDB.getSchema());
    }
    return true;
  }

  /**
   *从多投保人（个人）表和多投保人（个人）备份表读取信息
   * 返回LCAppntIndSet
   *
   */
  public LCAppntIndSet query()
  {
    Reflections tR=new Reflections();
    LCAppntIndSet tLCAppntIndSet =new LCAppntIndSet();
    LCAppntIndDB tLCAppntIndDB=new LCAppntIndDB();
    tLCAppntIndDB.setSchema(this.getSchema());
    tLCAppntIndSet=tLCAppntIndDB.query();
    if (tLCAppntIndSet.size()==0)
    {
      LBAppntIndSet tLBAppntIndSet =new LBAppntIndSet();
      LBAppntIndDB tLBAppntIndDB1=new LBAppntIndDB();
      tR.transFields(tLBAppntIndDB1.getSchema(),this.getSchema());
      tLBAppntIndSet=tLBAppntIndDB1.query();
      if (tLBAppntIndSet.size()==0)
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="LCAppntIndBL";
        tError.functionName="query";
        tError.errorMessage="没有查询到多投保人（个人）表";
        this.mErrors .addOneError(tError) ;
        return tLCAppntIndSet;
      }
      else
      {
        tLCAppntIndSet.add(this.getSchema());
        tR.transFields(tLCAppntIndSet,tLBAppntIndSet);
      }
    }
    return tLCAppntIndSet;
  }

  /**
   *从多投保人（个人）表和多投保人（个人）备份表读取信息
   * 返回LCAppntIndSet
   *
   */
  public LCAppntIndSet executeQuery(String sql)
  {
    Reflections tR=new Reflections();
    LCAppntIndSet tLCAppntIndSet =new LCAppntIndSet();
    LCAppntIndDB tLCAppntIndDB=new LCAppntIndDB();
    tLCAppntIndDB.setSchema(this.getSchema());
    tLCAppntIndSet=tLCAppntIndDB.executeQuery(sql);
    if (tLCAppntIndSet.size()==0)
    {
      LBAppntIndSet tLBAppntIndSet =new LBAppntIndSet();
      LBAppntIndDB tLBAppntIndDB1=new LBAppntIndDB();
      tR.transFields(tLBAppntIndDB1.getSchema(),this.getSchema());
      tLBAppntIndSet=tLBAppntIndDB1.executeQuery(sql);
      if (tLBAppntIndSet.size()==0)
      {
        // @@错误处理
        CError tError =new CError();
        tError.moduleName="LCAppntIndBL";
        tError.functionName="query";
        tError.errorMessage="没有查询到多投保人（个人）表";
        this.mErrors .addOneError(tError) ;
        return tLCAppntIndSet;
      }
      else
      {
        tLCAppntIndSet.add(this.getSchema());
        tR.transFields(tLCAppntIndSet,tLBAppntIndSet);
      }
    }
    return tLCAppntIndSet;
  }

  public static void main(String[] args)
  {
      //添加测试代码
  }


}
