package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_claimdetailquery extends CRMAbstractInterface {

	/** 赔案号 **/
	private static final String CASENO = "CASENO";
	private String mCaseno = "";
	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/**团体 保单号 **/
	private static final String GRPCONTNO = "GRPCONTNO";
	private String mGrpcontno = "";

	public Crm_claimdetailquery() {
		this.mRootName = "CRM_CLAIMDETAILQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CASENO.equals(tChildElement.getLocalName())) {
				mCaseno = tChildElement.getText();
				if ((!"".equals(mCaseno)) && mCaseno != null) {
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("请录入赔案号");
				return false;
			}
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					tsql = tsql + " and a.contno='" + mContno + "' ";
				}
				continue;
			}
			if (GRPCONTNO.equals(tChildElement.getLocalName())) {
				mGrpcontno = tChildElement.getText();
				if ((!"".equals(mGrpcontno)) && mGrpcontno != null) {
					tsql = tsql + " and a.grpcontno='" + mGrpcontno + "' ";
				}
				continue;
			}
		}
		if (("".equals(mContno) || mContno == null)
				&& ("".equals(mGrpcontno) || mGrpcontno == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("保单号、团体保单号必录其一");
			return false;
		}
		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

		String feemainsql = "select a.customername 被保险人,"
				+ "a.customerno 客户号码,a.age 年龄,a.insuredstat 人员类别,"
				+ "c.accdate 事件时间,a.feedate 就诊时间,"
				+ "a.HospitalName 就诊医院,a.RealHospDate 连续天数,"
				+ "a.feeatti 账单属性,a.HosGrade 医院级别  from "
				+ " llfeemain a,llcaserela b,llsubreport c "
				+ " where a.caseno='" + mCaseno + "' and a.caseno=b.caseno "
				+ "and a.caserelano=b.caserelano and b.subrptno=c.subrptno "
				+ " with ur ";
		SSRS feemainResult;
		feemainResult = tExeSQL.execSQL(feemainsql);
		if (feemainResult.getMaxRow() > 0) {
			OMElement feemainlist = tOMFactory.createOMElement("FEEMAINLIST",
					null);
			for (int i = 1; i <= feemainResult.getMaxRow(); i++) {
				OMElement feemaindata = tOMFactory.createOMElement(
						"FEEMAINDATA", null);
				tLoginVerifyTool.addOm(feemaindata, "CUSTOMERNAME",
						feemainResult.GetText(i, 1));
				tLoginVerifyTool.addOm(feemaindata, "CUSTOMERNO",
						feemainResult.GetText(i, 2));
				tLoginVerifyTool.addOm(feemaindata, "AGE",
						feemainResult.GetText(i, 3));
				tLoginVerifyTool.addOm(feemaindata, "INSUREDSTAT",
						feemainResult.GetText(i, 4));
				tLoginVerifyTool.addOm(feemaindata, "ACCDATE",
						feemainResult.GetText(i, 5));
				tLoginVerifyTool.addOm(feemaindata, "FEEDATE",
						feemainResult.GetText(i, 6));
				tLoginVerifyTool.addOm(feemaindata, "HOSPITALNAME",
						feemainResult.GetText(i, 7));
				tLoginVerifyTool.addOm(feemaindata, "REALHOSPDATE",
						feemainResult.GetText(i, 8));
				tLoginVerifyTool.addOm(feemaindata, "FEEATTI",
						feemainResult.GetText(i, 9));
				tLoginVerifyTool.addOm(feemaindata, "HOSGRADE",
						feemainResult.GetText(i, 10));
				feemainlist.addChild(feemaindata);
			}
			mResponseBodyElement.addChild(feemainlist);
		}

		String risksql = "select (select getdutyname "
				+ "from lmdutygetclm where getdutycode = a.getdutycode and getdutykind=a.getdutykind fetch first row only) 责任项目,"
				+ "b.PreAmnt 已报销,"
				+ "a.realpay 实赔金额,"
				+ "b.feedate 账单日期,"
				+ "a.RefuseAmnt 不合理费用,"
				+ "a.TabFeeMoney 账单金额,"
				+ "a.ClaimMoney 理算金额,"
				+ "nvl(c.FeeInSecu,0) 社保内,"
				+ "nvl(c.GetLimit,0) 免赔,"
				+ "nvl(c.FeeOutSecu,0) 社保外,"
				+ "a.OutDutyRate 给付比例,"
				+ "(select REMARK from lbpol where polno = a.polno union (select REMARK from lcpol where polno = a.polno)) 备注信息,"
				+ "(select riskname from lmriskapp where riskcode=a.riskcode) 险种名称"
				+ " from "
				+ " llclaimdetail a,llfeemain b left join LLSecurityReceipt c on b.mainfeeno=c.mainfeeno "
				+ " where a.caseno=b.caseno and a.caserelano=b.caserelano "
				+ " and a.caseno='"
				+ mCaseno + "' " + tsql + " with ur ";
		//System.out.println("更改后的SQL是==="+risksql);
		SSRS riskResult;
		riskResult = tExeSQL.execSQL(risksql);
		if (riskResult.getMaxRow() > 0) {
			OMElement risklist = tOMFactory.createOMElement("RISKLIST", null);
			for (int j = 1; j <= riskResult.getMaxRow(); j++) {
				OMElement riskdata = tOMFactory.createOMElement("RISKDATA",
						null);
				tLoginVerifyTool.addOm(riskdata, "GETDUTYNAME",
						riskResult.GetText(j, 1));
				tLoginVerifyTool.addOm(riskdata, "REIMBURSED",
						riskResult.GetText(j, 2));
				tLoginVerifyTool.addOm(riskdata, "REALPAY",
						riskResult.GetText(j, 3));
				tLoginVerifyTool.addOm(riskdata, "FEEDATE",
						riskResult.GetText(j, 4));
				tLoginVerifyTool.addOm(riskdata, "REFUSEAMNT",
						riskResult.GetText(j, 5));
				tLoginVerifyTool.addOm(riskdata, "TABFEEMONEY",
						riskResult.GetText(j, 6));
				tLoginVerifyTool.addOm(riskdata, "CLAIMMONEY",
						riskResult.GetText(j, 7));
				tLoginVerifyTool.addOm(riskdata, "FEEINSECU",
						riskResult.GetText(j, 8));
				tLoginVerifyTool.addOm(riskdata, "GETLIMIT",
						riskResult.GetText(j, 9));
				tLoginVerifyTool.addOm(riskdata, "FEEOUTSECU",
						riskResult.GetText(j, 10));
				tLoginVerifyTool.addOm(riskdata, "OUTDUTYRATE",
						riskResult.GetText(j, 11));
				tLoginVerifyTool.addOm(riskdata, "REMARK",
						riskResult.GetText(j, 12));
				tLoginVerifyTool.addOm(riskdata, "RISKNAME",
						riskResult.GetText(j, 13));
				risklist.addChild(riskdata);
			}
			mResponseBodyElement.addChild(risklist);
		}
		return true;
	}
}
