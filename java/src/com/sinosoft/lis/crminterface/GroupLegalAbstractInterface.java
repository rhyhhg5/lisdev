package com.sinosoft.lis.crminterface;

import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.crminterface.utility.DateUtil;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;


public abstract class GroupLegalAbstractInterface {

	public GroupLegalAbstractInterface() {

	}

	public String mRootName = "";
	public String mBatchNo = "";
	public String mSendDate = "";
	public String mSendTime = "";
	public OMElement mRootElement = null;
	public OMElement mHeadElement = null;
	public OMElement mBodyElement = null;
	public OMElement mResponseElement = null;
	public OMElement mResponseHeadElement = null;
	public OMElement mResponseBodyElement = null;

	public CErrors mErrors = new CErrors();

	/**
	 * 成功失败标志 成功-00
	 */
	public static final String STATE_SUCCESS = "00";
	/**
	 * 成功失败标志 失败-01
	 */
	public static final String STATE_FAIL = "01";

	/**
	 * 失败编码
	 */
	public String mErrCode = "";
	/**
	 * 失败编码 未知异常-E
	 */
	public static final String ERRCODE_E = "E";
	/**
	 * 失败编码 无异常-00
	 */
	public static final String ERRCODE_OK = "00";
	/**
	 * 失败编码 系统异常-01
	 */
	public static final String ERRCODE_SYS = "01";
	/**
	 * 失败编码 接口校验错误-02
	 */
	public static final String ERRCODE_CHECK = "02";
	/**
	 * 失败编码 业务规则错误-03
	 */
	public static final String ERRCODE_RULE = "03";

//	private String mXMLPath = "";

	/**
	 * 接口默认调用方法，子类统一继承该方法
	 * 
	 * @param aOMElement
	 * @return
	 */
	public OMElement queryData(OMElement aOMElement) {
		mRootElement = aOMElement;
		try {
			if (!initRequestData()) {
				buildErrorResponseElement();
				return mResponseElement;
			}
			
			if (!returnResponseBody()) {
				buildErrorResponseElement();
				return mResponseElement;
			}
			buildResponseElement();
		} catch (Exception ex) {
			mErrCode = ERRCODE_E;
			buildErrorResponseElement();
		} finally {
			//saveXML();
		}
		return mResponseElement;
	}

	/**
	 * 初始化报文 解析Head、Body
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean initRequestData() throws Exception {

		if ("".equals(mRootName)) {
			mErrCode = ERRCODE_SYS;
			mErrors.addOneError("被调用接口程序错误,未描述根节点");
			return false;
		}

		Iterator tRootIterator = mRootElement.getChildren();
		while (tRootIterator.hasNext()) {
			OMElement tRootElement = (OMElement) tRootIterator.next();
			if (!mRootName.equals(tRootElement.getLocalName())) {
				mErrCode = ERRCODE_SYS;
				mErrors.addOneError("被调用接口程序错误,描述根节点错误");
				return false;
			}

			Iterator tChildIterator = tRootElement.getChildren();
			while (tChildIterator.hasNext()) {
				OMElement tChildElement = (OMElement) tChildIterator.next();
				if ("HEAD".equals(tChildElement.getLocalName())) {
					mHeadElement = tChildElement;
					continue;
				}

				if ("BODY".equals(tChildElement.getLocalName())) {
					mBodyElement = tChildElement;
					continue;
				}
			}
		}

		Iterator tHeadIterator = mHeadElement.getChildren();
		while (tHeadIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tHeadIterator.next();
			if ("BATCHNO".equals(tChildElement.getLocalName())) {
				mBatchNo = tChildElement.getText();
				continue;
			}
			if ("SENDDATE".equals(tChildElement.getLocalName())) {
				mSendDate = tChildElement.getText();
				continue;
			}
			if ("SENDTIME".equals(tChildElement.getLocalName())) {
				mSendTime = tChildElement.getText();
				continue;
			}
		}

		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		mResponseElement = tOMFactory.createOMElement(mRootName, null);
		tOMFactory = OMAbstractFactory.getOMFactory();
		mResponseHeadElement = tOMFactory.createOMElement("HEAD", null);
		tOMFactory = OMAbstractFactory.getOMFactory();
		mResponseBodyElement = tOMFactory.createOMElement("BODY", null);
		
		if (mBatchNo == null || "".equals(mBatchNo)) {
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,无交易批次号");
			return false;
		}
		if (mBatchNo.length() !=20) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,批次号错误");
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			return false;
		}
		if (!mBatchNo.substring(0, 2).equals("LP")) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,批次号错误");
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			return false;
		}
		
		if (mSendDate == null || "".equals(mSendDate)) {
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,无报文发送日期");
			return false;
		}
		if (mSendTime == null || "".equals(mSendTime)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,无报文发送时间");
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			return false;
		}
        if(!validateTime(mSendTime) && !"".equals(mSendTime)){
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,报文发送时间格式错误");
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			return false;
        }
        if(!validateDate(mSendDate) && !"".equals(mSendDate)){
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,报文发送日期格式错误");
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			return false;
        }
		
		return true;
	}

	/**
	 * 错误报文返回
	 */
	private void buildErrorResponseElement() {

		String tErrInfo = "";
		if (ERRCODE_E.equals(mErrCode)) {
			tErrInfo = "系统异常，请联系运维人员";
		} else {
			tErrInfo = mErrors.getFirstError();
		}
		returnResponseHead(STATE_FAIL, mErrCode, tErrInfo);

		mResponseElement.addChild(mResponseHeadElement);
		mResponseElement.addChild(mResponseBodyElement);
		System.out.println("responseOME:" + mResponseElement);
		LoginVerifyTool.writeLog(mResponseElement, mBatchNo, "",
				PubFun.getCurrentDate(), PubFun.getCurrentTime(), "", "LP",
				mErrCode, tErrInfo);
	}

	/**
	 * 返回报文的HEAD部分
	 * 
	 * @param aState
	 *            状态
	 * @param aErrCode
	 *            失败编码
	 * @param aErrInfo
	 *            失败原因
	 * @return
	 */
	public void returnResponseHead(String aState, String aErrCode,
			String aErrInfo) {
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		tLoginVerifyTool.addOm(mResponseHeadElement, "BATCHNO", mBatchNo);
		tLoginVerifyTool.addOm(mResponseHeadElement, "SENDDATE",
				PubFun.getCurrentDate());
		tLoginVerifyTool.addOm(mResponseHeadElement, "SENDTIME",
				PubFun.getCurrentTime());
		tLoginVerifyTool.addOm(mResponseHeadElement, "STATE", aState);
		tLoginVerifyTool.addOm(mResponseHeadElement, "ERRCODE", aErrCode);
		tLoginVerifyTool.addOm(mResponseHeadElement, "ERRINFO", aErrInfo);
	}

	/**
	 * 返回成功完整报文
	 */
	public void buildResponseElement() {
		returnResponseHead(STATE_SUCCESS, "", "");
		mResponseElement.addChild(mResponseHeadElement);
		mResponseElement.addChild(mResponseBodyElement);
		System.out.println("responseOME:" + mResponseElement);

		LoginVerifyTool.writeLog(mResponseElement, mBatchNo, "",
				PubFun.getCurrentDate(), PubFun.getCurrentTime(), "", "LP",
				ERRCODE_OK, "成功处理");
	}

	/**
	 * 保存返回报文
	 */
//	public void saveXML() {
//		String tFileName = CRMMessageSave.generateFileName(mBatchNo, "xml");
//		if (getXMLPath()) {
//			CRMMessageSave.save(mResponseElement, tFileName, mXMLPath,
//					"OutNoStd");
//			CRMMessageSave.save(mRootElement, tFileName, mXMLPath, "InNoStd");
//		} else {
//			System.out.println(mResponseElement.toString());
//			System.out.println(mRootElement.toString());
//		}
//	}

//	private boolean getXMLPath() {
//		String tPathSQL = "select sysvarvalue from LDSYSVAR  where Sysvar='CRM_LOGPath'";
//		mXMLPath = new ExeSQL().getOneValue(tPathSQL);
//		if (mXMLPath == null || mXMLPath.equals("")) {
//			System.err.println("CRM接口报文保存目录查询失败！");
//			return false;
//		}
//		return true;
//	}

	/**
	 * 校验日期
	 * @param strDate
	 * @return
	 */
	public boolean validateDate(String strDate)
    {
        int yyyy = 0000;
        int mm = 00;
        int dd = 00;
        // 校验是否有非法字符
        if (!PubFun.validateNumber(strDate))
        {
            return false;
        }

        if (strDate.indexOf("-") >= 0)
        {
            StringTokenizer token = new StringTokenizer(strDate, "-");
            int i = 0;
            while (token.hasMoreElements())
            {
                if (i == 0)
                {
                    yyyy = Integer.parseInt(token.nextToken());
                }
                if (i == 1)
                {
                    mm = Integer.parseInt(token.nextToken());
                }
                if (i == 2)
                {
                    dd = Integer.parseInt(token.nextToken());
                }
                i++;
            }
        }
        else
        {
            if (strDate.length() != 8)
            {
                return false;
            }
            yyyy = Integer.parseInt(strDate.substring(0, 4));
            mm = Integer.parseInt(strDate.substring(4, 6));
            dd = Integer.parseInt(strDate.substring(6, 8));
        }
        if (yyyy > 9999 || yyyy < 1800)
        {
            return false;
        }
        if (mm > 12 || mm < 1)
        {
            return false;
        }
        if (dd > 31 || dd < 1)
        {
            return false;
        }
        if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && (dd == 31))
        {
            return false;
        }
        if (mm == 2)
        {
            // 校验闰年的情况下__日期格式
            boolean leap = (yyyy % 4 == 0 && (yyyy % 100 != 0 || yyyy % 400 == 0));
            if (dd > 29 || (dd == 29 && !leap))
            {
                return false;
            }
        }

        return true;
    }
	
	/**
	 * 校验时间
	 * @param strDate
	 * @return
	 */
	public boolean validateTime(String strDate)
    {
        int hh = 00;
        int mm = 00;
        int ss = 00;
        // 校验是否有非法字符
        String TIME_LIST="0123456789:";
        String tmp = null;
        for (int i = 0; i < strDate.length(); i++)
        {
            tmp = strDate.substring(i, i + 1);
            if (TIME_LIST.indexOf(tmp) == -1)
            {
                return false;
            }
        }

        if (strDate.indexOf(":") >= 0)
        {
            StringTokenizer token = new StringTokenizer(strDate, ":");
            int i = 0;
            while (token.hasMoreElements())
            {
                if (i == 0)
                {
                	hh = Integer.parseInt(token.nextToken());
                }
                if (i == 1)
                {
                    mm = Integer.parseInt(token.nextToken());
                }
                if (i == 2)
                {
                	ss = Integer.parseInt(token.nextToken());
                }
                i++;
            }
        }
        else
        {
            if (strDate.length() != 8)
            {
                return false;
            }
            hh = Integer.parseInt(strDate.substring(0, 4));
            mm = Integer.parseInt(strDate.substring(4, 6));
            ss = Integer.parseInt(strDate.substring(6, 8));
        }
        if (hh > 23 || hh < 0)
        {
            return false;
        }
        if (mm > 59 || mm < 0)
        {
            return false;
        }
        if (ss > 59 || ss < 0)
        {
            return false;
        }

        return true;
    }
	
	/**
	 * 解析Body报文，返回报文BODY部分，由子类实现
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean returnResponseBody() throws Exception;
}
