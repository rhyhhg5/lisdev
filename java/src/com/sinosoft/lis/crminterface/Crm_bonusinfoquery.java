package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_bonusinfoquery extends CRMAbstractInterface {

    /** 当前页码 **/
    private static final String PAGEINDEX = "PAGEINDEX";
    private String mPageindex = "";
    private int mPagein = 0;

    /** 每页记录条数 **/
    private static final String PAGESIZE = "PAGESIZE";
    private String mPagesize = "";
    private int mPagesi = 0;
	
	
	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";

	public Crm_bonusinfoquery() {
		this.mRootName = "CRM_BONUSINFOQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			

            if (PAGEINDEX.equals(tChildElement.getLocalName()))
            {
                mPageindex = tChildElement.getText();
                if ((!"".equals(mPageindex)) && mPageindex != null)
                {
                    mPagein = Integer.parseInt(mPageindex);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码和每页记录条数为必录项");
                return false;
            }

            if (PAGESIZE.equals(tChildElement.getLocalName()))
            {
                mPagesize = tChildElement.getText();
                if ((!"".equals(mPagesize)) && mPagesize != null)
                {
                    mPagesi = Integer.parseInt(mPagesize);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码和每页记录条数为必录项");
                return false;
            }
			
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("保单号不能为空");
				return false;
			}
		}
		String sql = "select a.polno 险种号, a.fiscalyear 红利公布年度,"
				+ "a.bonusflag  红利分配方式, a.sgetdate  红利应分时间,"
				+ "a.bonusmoney 分配金额   from lobonuspol a "
				+ " where a.contno='" + mContno + "' with ur ";
		
        /**实例化ExeSQL**/
        ExeSQL tExeSQL = new ExeSQL();
        String totalnum = "select count(*) from (" + sql + ") cou with ur";

        /**实例化结果集SSRS**/
        SSRS sResult;
        SSRS ttotal = tExeSQL.execSQL(totalnum);
        LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

        /**加入到body标签下*/
        tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM", ttotal.GetText(1, 1));
		
		
		
        /**根据已查询的结果进行分页查询处结果**/
        sql = "select * from (select c.*,rownumber() over() as rn from (" + sql + ") c) d where d.rn>"
                + ((mPagein - 1) * mPagesi) + " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		
		sResult = tExeSQL.execSQL(sql);
		if (sResult.getMaxRow() > 0) {
			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
			
			/**创建集合标签*/
			OMElement bonuslist = tOMFactory.createOMElement("BONUSLIST", null);
			for (int i = 1; i <= sResult.getMaxRow(); i++) {
				
				/**创建集合子标签*/
				OMElement bonusdata = tOMFactory.createOMElement("BONUSDATA",
						null);
				tLoginVerifyTool.addOm(bonusdata, "POLNO",
						sResult.GetText(i, 1));
				tLoginVerifyTool.addOm(bonusdata, "FISCALYEAR",
						sResult.GetText(i, 2));
				tLoginVerifyTool.addOm(bonusdata, "BONUSFLAG",
						sResult.GetText(i, 3));
				tLoginVerifyTool.addOm(bonusdata, "SGETDATE",
						sResult.GetText(i, 4));
				tLoginVerifyTool.addOm(bonusdata, "BONUSMONEY",
						sResult.GetText(i, 5));
				bonuslist.addChild(bonusdata);
			}
			mResponseBodyElement.addChild(bonuslist);
		}
		return true;
	}
}
