package com.sinosoft.lis.crminterface.utility;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import com.sinosoft.lis.crminterface.utility.DateUtil;

import org.apache.axiom.om.OMElement;

public class CRMMessageSave {

	/**
	 * 依据预订规则，生成报文文件名
	 * 
	 * @param sTransCode
	 * @param sSuffix
	 * @return
	 */
	public static String generateFileName(String sTransCode, String sSuffix) {
		return (sTransCode + "_" + DateUtil.getCurrentDate("HHmmss") + "." + sSuffix);
	}

	/**
	 * 依据预订规则生成报文存放路径
	 * 
	 * @param pType
	 * @return
	 */
	public static String generateFilePath(String aPath,String pType) {
		StringBuffer sFilePath = new StringBuffer();
		sFilePath
				// .append(ConfigFactory.getPreConfig().getString("YibaotongHome"))
				.append(aPath).append('/').append("FileContent").append('/')
				.append(DateUtil.getCurrentDate("yyyy")).append('/')
				.append(DateUtil.getCurrentDate("yyyyMM")).append('/')
				.append(DateUtil.getCurrentDate("yyyyMMdd")).append('/')
				.append(pType).append('/');
		return sFilePath.toString();
	}

	public static void save(OMElement pXmlDoc, String pName, String aPath,
			String pType) {
		StringBuffer mFilePath = new StringBuffer();
		mFilePath.append(generateFilePath(aPath,pType));
		File mFileDir = new File(mFilePath.toString());
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录失败！" + mFileDir.getPath());
			}
		}

		try {
			FileOutputStream tFos = new FileOutputStream(mFilePath.toString()
					+ pName);
			XMLStreamWriter writer = XMLOutputFactory.newInstance()
					.createXMLStreamWriter(tFos);

			pXmlDoc.serialize(writer); // cache on
			writer.flush();
			tFos.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
