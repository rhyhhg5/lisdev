package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_custinfoincard extends CRMAbstractInterface {

	/**
	 * 每页记录条数
	 * */
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/**
	 * 当前页码
	 * */
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/**
	 * 电话号
	 * */
	private static final String CALLNUMBER = "CALLNUMBER";
	private String mCallNumber = "";

	/**
	 * 保险卡号
	 * */
	private static final String CARDNO = "CARDNO";
	private String mCardno = "";
	/**
	 * 投/被保人证件号
	 * */
	private static final String IDNO = "IDNO";
	private String mIDNo = "";

	public Crm_custinfoincard() {
		this.mRootName = "CRM_CUSTINFOINCARD";
	}

	public boolean returnResponseBody() throws Exception {
		// String tsql =
		// "select a.appntname,a.appntno,a.idno,a.idtype,b.phone,b.PostalAddress,a.Managecom,'A' from lcappnt a ,lcaddress b where  a.Addressno=b.Addressno and b.CustomerNo=a.AppntNo ";
		// String tsq =
		// " Union select a.name,a.appntno,a.idno,a.idtype,b.phone,b.PostalAddress,a.Managecom,'I' from lcinsured a ,lcaddress b where  a.Addressno=b.Addressno and b.CustomerNo=a.AppntNo  ";
		// String tj = "";
		String tsql = "";

		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CALLNUMBER.equals(tChildElement.getLocalName())) {
				mCallNumber = tChildElement.getText();
				if ((!"".equals(mCallNumber)) && mCallNumber != null) {
					// tsql = tsql +
					// " and a.Appntno in (select customerno from lcaddress where Phone='"+mCallNumber+"' or Mobile='"+mCallNumber+"' or homephone='"+mCallNumber+"' or companyphone='"+mCallNumber+"')";
					// tsq = tsq +
					// " and a.insuredno in (select customerno from lcaddress where Phone='"+mCallNumber+"' or Mobile='"+mCallNumber+"' or homephone='"+mCallNumber+"' or companyphone='"+mCallNumber+"')";
					tsql = tsql + " and (c.phone='" + mCallNumber
							+ "' or c.mobile='" + mCallNumber + "') ";
				}
				continue;
			}
			if (CARDNO.equals(tChildElement.getLocalName())) {
				mCardno = tChildElement.getText();
				if ((!"".equals(mCardno)) && mCardno != null) {
					tsql = tsql + " and a.cardno='" + mCardno + "' ";
				}
				continue;
			}
			if (IDNO.equals(tChildElement.getLocalName())) {
				mIDNo = tChildElement.getText();
				if ((!"".equals(mIDNo)) && mIDNo != null) {
					tsql = tsql + " and b.idno='" + mIDNo + "' ";
				}
				continue;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if ((!"".equals(mPagesize)) && mPagesize != null) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数都不能为空");
				return false;
			}

			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if ((!"".equals(mPageindex)) && mPageindex != null) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数都不能为空");
				return false;
			}
		}
		if (("".equals(mCallNumber) || mCallNumber == null)
				&& ("".equals(mCardno) || mCardno == null)
				&& ("".equals(mIDNo) || mIDNo == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("电话号码、卡号、投/被保人证件号必录其一");
			return false;
		}

		String sql = "select b.name 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,c.managecom 保单所属机构,'A' 客户类型 "
				+ "from wfcontlist a,wfappntlist b,liactivecarddetail c "
				+ "where a.cardno = b.cardno and a.cardno = c.cardno "
				+ tsql
				+ "union all "
				+ "select b.name 姓名,b.insuno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,c.managecom 保单所属机构,'I' 客户类型 "
				+ "from wfcontlist a,wfinsulist b,liactivecarddetail c "
				+ "where a.cardno = b.cardno and a.cardno = c.cardno "
				+ tsql;

		ExeSQL tExeSQL = new ExeSQL();
		SSRS ttotal;
		String totalsql = "select count(*) from (" + sql + ") totlnum with ur";
		ttotal = tExeSQL.execSQL(totalsql);

		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
				ttotal.GetText(1, 1));
		SSRS tResult;
		String pagesql = "select * from (select c.*,rownumber() over() as rn from (" + sql
				+ ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		tResult = tExeSQL.execSQL(pagesql);
		if (tResult.getMaxRow() > 0) {
			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
			int trow = tResult.MaxRow;
			OMElement tcustomerlist = tOMFactory.createOMElement(
					"CUSTOMERLIST", null);
			for (int i = 0; i < trow; i++) {
				OMElement tcustomer = tOMFactory.createOMElement(
						"CUSTOMERDATA", null);
				tLoginVerifyTool.addOm(tcustomer, "CUSTOMERNAME",
						tResult.GetText(i + 1, 1));
				tLoginVerifyTool.addOm(tcustomer, "CUSTOMERNO",
						tResult.GetText(i + 1, 2));
				tLoginVerifyTool.addOm(tcustomer, "IDNO",
						tResult.GetText(i + 1, 3));
				tLoginVerifyTool.addOm(tcustomer, "IDTYPE",
						tResult.GetText(i + 1, 4));
				tLoginVerifyTool.addOm(tcustomer, "PHONE",
						tResult.GetText(i + 1, 5));
				tLoginVerifyTool.addOm(tcustomer, "ADDRESS",
						tResult.GetText(i + 1, 6));
				tLoginVerifyTool.addOm(tcustomer, "MANAGECOM",
						tResult.GetText(i + 1, 7));
				tLoginVerifyTool.addOm(tcustomer, "CUSTOMERTYPE",
						tResult.GetText(i + 1, 8));
				tcustomerlist.addChild(tcustomer);
			}
			mResponseBodyElement.addChild(tcustomerlist);
		}
		return true;
	}

}
