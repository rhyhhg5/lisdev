package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_cardinfoquery extends CRMAbstractInterface {
	/** ���� **/
	private static final String CARDNO = "CARDNO";
	private String mCardno = "";

	public Crm_cardinfoquery() {
		this.mRootName = "CRM_CARDINFOQUERY";
	}

	public boolean returnResponseBody() throws Exception {

		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CARDNO.equals(tChildElement.getLocalName())) {
				mCardno = tChildElement.getText();
				if ((!"".equals(mCardno)) && mCardno != null) {
					continue;
				} else {
					mErrCode = ERRCODE_CHECK;
					mErrors.addOneError("���Ų���Ϊ��");
					return false;
				}
			}
		}
		String cardsql = "select wfc.cardno,wfc.password,wfc.certifycode,wfc.certifyname,wfc.activedate,wfc.cvalidate,"
				+ "(select managecom from liactivecarddetail where cardno=wfc.cardno fetch first 1 rows only),"
				+ "wfc.prem,wfc.insuyear,"
				+ "((select InActiveDate from licardactiveinfolist where cardno=wfc.cardno fetch first 1 rows only) - 1 day),"
				+ "(select wrapname from ldwrap where riskwrapcode=wfc.riskcode ),wfc.riskcode "
				+ " from  LZCardNumber lz,wfcontlist wfc "
				+ " where lz.cardno=wfc.cardno "
				+ "and lz.cardno='"
				+ mCardno
				+ "' order by wfc.senddate desc ,wfc.sendtime desc "
				+ "fetch first 1 rows only with ur ";
		String appntsql = "select name,sex,birthday,idtype,idno,occupationtype,occupationcode,postaladdress,"
				+ "zipcode,phont,mobile,email,grpname,companyphone,compaddr,compzipcode"
				+ " from wfappntlist"
				+ " where cardno='"
				+ mCardno
				+ "' fetch first 1 rows only with ur ";
		String insuredsql = "select insuno,ToAppntRela,RelationCode,"
				+ "RelationToInsured,ReInsuNo,Name,Sex,"
				+ "Birthday,IDType,IDNo,OccupationType,OccupationCode,"
				+ "PostalAddress,ZipCode,Phont,Mobile,Email,"
				+ "GrpName,CompanyPhone,CompAddr,CompZipCode "
				+ " from wfinsulist where cardno='" + mCardno + "' with ur";
		String bnfsql = "select ToInsuNo, ToInsuRela,BnfType,"
				+ "BnfGrade,BnfRate,Name,Sex,Birthday,IDType,IDNo "
				+ " from wfbnflist where cardno='" + mCardno + "' with ur";
		String tcontplansql=" select (select dutyname from lmduty where dutycode= lc.dutycode) , calfactorvalue from lccontplandutyparam lc where grpcontno=(select grpcontno from lccont where contno='"+mCardno+"') and calfactor='Amnt' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS cardResult;
		SSRS appntResult;
		SSRS insuredResult;
		SSRS bnfResult;
		SSRS tplanResult;
		cardResult = tExeSQL.execSQL(cardsql);
		appntResult = tExeSQL.execSQL(appntsql);
		insuredResult = tExeSQL.execSQL(insuredsql);
		bnfResult = tExeSQL.execSQL(bnfsql);
		tplanResult =tExeSQL.execSQL(tcontplansql);
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		if (cardResult.getMaxRow() > 0) {
			OMElement carddate = tOMFactory.createOMElement("CARDDATA", null);
			tLoginVerifyTool
					.addOm(carddate, "CARDNO", cardResult.GetText(1, 1));
			tLoginVerifyTool.addOm(carddate, "PASSWORD",
					cardResult.GetText(1, 2));
			tLoginVerifyTool.addOm(carddate, "CERTIFYCODE",
					cardResult.GetText(1, 3));
			tLoginVerifyTool.addOm(carddate, "CERTIFYNAME",
					cardResult.GetText(1, 4));
			tLoginVerifyTool.addOm(carddate, "ACTIVEDATE",
					cardResult.GetText(1, 5));
			tLoginVerifyTool.addOm(carddate, "CVALIDATE",
					cardResult.GetText(1, 6));
			tLoginVerifyTool.addOm(carddate, "MANAGECOM",
					cardResult.GetText(1, 7));
			tLoginVerifyTool.addOm(carddate, "PREM", cardResult.GetText(1, 8));
			tLoginVerifyTool.addOm(carddate, "INSUYEAR",
					cardResult.GetText(1, 9));
			tLoginVerifyTool.addOm(carddate, "ENDDATE",
					cardResult.GetText(1, 10));
			tLoginVerifyTool.addOm(carddate, "PRODUCTNAME",
					cardResult.GetText(1, 11));
			tLoginVerifyTool.addOm(carddate, "PRODUCTID",
					cardResult.GetText(1, 12));
			mResponseBodyElement.addChild(carddate);
		}
		if (appntResult.getMaxRow() > 0) {
			OMElement appntdate = tOMFactory.createOMElement("APPNTDATA", null);
			tLoginVerifyTool
					.addOm(appntdate, "NAME", appntResult.GetText(1, 1));
			tLoginVerifyTool.addOm(appntdate, "SEX", appntResult.GetText(1, 2));
			tLoginVerifyTool.addOm(appntdate, "APPNTBIRTHDAY",
					appntResult.GetText(1, 3));
			tLoginVerifyTool.addOm(appntdate, "IDTYPE",
					appntResult.GetText(1, 4));
			tLoginVerifyTool
					.addOm(appntdate, "IDNO", appntResult.GetText(1, 5));
			tLoginVerifyTool.addOm(appntdate, "OCCUPATIONTYPE",
					appntResult.GetText(1, 6));
			tLoginVerifyTool.addOm(appntdate, "OCCUPATIONCODE",
					appntResult.GetText(1, 7));
			tLoginVerifyTool.addOm(appntdate, "POSTALADDRESS",
					appntResult.GetText(1, 8));
			tLoginVerifyTool.addOm(appntdate, "ZIPCODE",
					appntResult.GetText(1, 9));
			tLoginVerifyTool.addOm(appntdate, "PHONE",
					appntResult.GetText(1, 10));
			tLoginVerifyTool.addOm(appntdate, "MOBILE",
					appntResult.GetText(1, 11));
			tLoginVerifyTool.addOm(appntdate, "EMAIL",
					appntResult.GetText(1, 12));
			tLoginVerifyTool.addOm(appntdate, "GRPNAME",
					appntResult.GetText(1, 13));
			tLoginVerifyTool.addOm(appntdate, "COMPANYPHONE",
					appntResult.GetText(1, 14));
			tLoginVerifyTool.addOm(appntdate, "COMPANYADDRESS",
					appntResult.GetText(1, 15));
			tLoginVerifyTool.addOm(appntdate, "COMPANYZIPCODE",
					appntResult.GetText(1, 16));
			mResponseBodyElement.addChild(appntdate);
		}
		if (insuredResult.getMaxRow() > 0) {
			OMElement insuredlist = tOMFactory.createOMElement("INSUREDLIST",
					null);
			for (int i = 1; i <= insuredResult.MaxRow; i++) {
				OMElement insureddata = tOMFactory.createOMElement(
						"INSUREDDATA", null);
				tLoginVerifyTool.addOm(insureddata, "INSUREDNO",
						insuredResult.GetText(i, 1));
				tLoginVerifyTool.addOm(insureddata, "RELATIONTOAPPNT",
						insuredResult.GetText(i, 2));
				tLoginVerifyTool.addOm(insureddata, "RELATIONCODE",
						insuredResult.GetText(i, 3));
				tLoginVerifyTool.addOm(insureddata, "RELATIONTOMAININSURED",
						insuredResult.GetText(i, 4));
				tLoginVerifyTool.addOm(insureddata, "MAININSUREDNO",
						insuredResult.GetText(i, 5));
				tLoginVerifyTool.addOm(insureddata, "NAME",
						insuredResult.GetText(i, 6));
				tLoginVerifyTool.addOm(insureddata, "SEX",
						insuredResult.GetText(i, 7));
				tLoginVerifyTool.addOm(insureddata, "BIRTHDAY",
						insuredResult.GetText(i, 8));
				tLoginVerifyTool.addOm(insureddata, "IDTYPE",
						insuredResult.GetText(i, 9));
				tLoginVerifyTool.addOm(insureddata, "IDNO",
						insuredResult.GetText(i, 10));
				tLoginVerifyTool.addOm(insureddata, "OCCUPATIONTYPE",
						insuredResult.GetText(i, 11));
				tLoginVerifyTool.addOm(insureddata, "OCCUPATIONCODE",
						insuredResult.GetText(i, 12));
				tLoginVerifyTool.addOm(insureddata, "POSTALADDRESS",
						insuredResult.GetText(i, 13));
				tLoginVerifyTool.addOm(insureddata, "ZIPCODE",
						insuredResult.GetText(i, 14));
				tLoginVerifyTool.addOm(insureddata, "PHONE",
						insuredResult.GetText(i, 15));
				tLoginVerifyTool.addOm(insureddata, "MOBILE",
						insuredResult.GetText(i, 16));
				tLoginVerifyTool.addOm(insureddata, "EMAIL",
						insuredResult.GetText(i, 17));
				tLoginVerifyTool.addOm(insureddata, "GRPNAME",
						insuredResult.GetText(i, 18));
				tLoginVerifyTool.addOm(insureddata, "COMPANYPHONE",
						insuredResult.GetText(i, 19));
				tLoginVerifyTool.addOm(insureddata, "COMPADDR",
						insuredResult.GetText(i, 20));
				tLoginVerifyTool.addOm(insureddata, "COMPZIPCODE",
						insuredResult.GetText(i, 21));
				insuredlist.addChild(insureddata);
			}
			mResponseBodyElement.addChild(insuredlist);
		}
		if (bnfResult.getMaxRow() > 0) {
			OMElement bnflist = tOMFactory.createOMElement("BNFLIST", null);
			for (int i = 1; i <= bnfResult.MaxRow; i++) {
				OMElement bnfdata = tOMFactory.createOMElement("BNFDATA",
						null);
				tLoginVerifyTool.addOm(bnfdata, "INSUREDNO",
						bnfResult.GetText(i, 1));
				tLoginVerifyTool.addOm(bnfdata, "RELATIONTOINSURED",
						bnfResult.GetText(i, 2));
				tLoginVerifyTool.addOm(bnfdata, "BNFTYPE",
						bnfResult.GetText(i, 3));
				tLoginVerifyTool.addOm(bnfdata, "BNFGRADE",
						bnfResult.GetText(i, 4));
				tLoginVerifyTool.addOm(bnfdata, "BNFLOT",
						bnfResult.GetText(i, 5));
				tLoginVerifyTool
						.addOm(bnfdata, "NAME", bnfResult.GetText(i, 6));
				tLoginVerifyTool.addOm(bnfdata, "SEX", bnfResult.GetText(i, 7));
				tLoginVerifyTool.addOm(bnfdata, "BIRTHDAY",
						bnfResult.GetText(i, 8));
				tLoginVerifyTool.addOm(bnfdata, "IDTYPE",
						bnfResult.GetText(i, 9));
				tLoginVerifyTool.addOm(bnfdata, "IDNO",
						bnfResult.GetText(i, 10));
				bnflist.addChild(bnfdata);
			}
			mResponseBodyElement.addChild(bnflist);
		}
		if(tplanResult.getMaxRow()>0){
			OMElement plansdate = tOMFactory.createOMElement("DUTYLIST", null);
			for (int i = 1; i <= tplanResult.MaxRow; i++) {
				OMElement plandata = tOMFactory.createOMElement("DUTYDATA",
						null);
				tLoginVerifyTool.addOm(plandata, "POLICYLIABILITY",
						tplanResult.GetText(i, 1));
				tLoginVerifyTool.addOm(plandata, "POLICYAMOUNT",
						tplanResult.GetText(i, 2));
				plansdate.addChild(plandata);
			}
			mResponseBodyElement.addChild(plansdate);
		}
		return true;
	}
}
