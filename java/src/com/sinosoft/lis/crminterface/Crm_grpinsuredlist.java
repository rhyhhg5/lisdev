package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_grpinsuredlist extends CRMAbstractInterface {

	/** 当前页码 **/
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/** 每页记录条数 **/
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/** 团体保单号 **/
	private static final String GRPCONTNO = "GRPCONTNO";
	private String mGrpcontno = "";
	/** 被保人姓名 **/
	private static final String NAME = "NAME";
	private String mName = "";
	/** 被保人证件号码 **/
	private static final String IDNO = "IDNO";
	private String mIdno = "";
	/** 被保人出生日期 **/
	private static final String BIRTHDAY = "BIRTHDAY";
	private String mBirthday = "";

	public Crm_grpinsuredlist() {
		this.mRootName = "CRM_GRPINSUREDLIST";
	}

	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if ((!"".equals(mPageindex)) && mPageindex != null) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数均不能为空");
				return false;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if ((!"".equals(mPagesize)) && mPagesize != null) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数均不能为空");
				return false;
			}
			if (GRPCONTNO.equals(tChildElement.getLocalName())) {
				mGrpcontno = tChildElement.getText();
				if ((!"".equals(mGrpcontno)) && mGrpcontno != null) {
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("请录入团体保单号");
				return false;
			}
			if (NAME.equals(tChildElement.getLocalName())) {
				mName = tChildElement.getText();
				if ((!"".equals(mName)) && mName != null) {
					tsql += " and name='" + mName + "' ";
				}
				continue;
			}
			if (IDNO.equals(tChildElement.getLocalName())) {
				mIdno = tChildElement.getText();
				if ((!"".equals(mIdno)) && mIdno != null) {
					tsql += " and idno='" + mIdno + "' ";
				}
				continue;
			}
			if (BIRTHDAY.equals(tChildElement.getLocalName())) {
				mBirthday = tChildElement.getText();
				if ((!"".equals(mBirthday)) && mBirthday != null) {
					tsql += " and Birthday='" + mBirthday + "' ";
				}
				continue;
			}
		}

		if (("".equals(mName) || mName == null)
				&& ("".equals(mIdno) || mIdno == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被保人姓名、被保人证件号码必录其中之一");
			return false;
		}
//		String sql = "Select Contno,Insuredno,Name,Idno from Lcinsured "
//				+ " Where Grpcontno = '" + mGrpcontno + "' " + tsql;
		//加上查询出被保人已经失效的情况
		String sql = "Select Contno,Insuredno,Name,Idno from Lcinsured "
				   + " Where Grpcontno = '"+mGrpcontno+"' " + tsql
				   + " union "
				   + "Select Contno,Insuredno,Name,Idno from Lbinsured "
				   + " Where Grpcontno = '"+mGrpcontno+"' " + tsql;
		ExeSQL tExeSQL = new ExeSQL();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

		// 查询总条数语句
		String numsql = "select count(*) from (" + sql + ") cou with ur ";
		SSRS numResult;
		numResult = tExeSQL.execSQL(numsql);
		if (numResult.getMaxRow() > 0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
					numResult.GetText(1, 1));
		}
		// 分页语句
		String insuredsql = "select * from (select c.*,rownumber() over() as rn from ("
				+ sql
				+ ") c) d where d.rn>"
				+ ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		SSRS insuredREsult;
		insuredREsult = tExeSQL.execSQL(insuredsql);
		if (insuredREsult.getMaxRow() > 0) {
			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
			OMElement insuredlist = tOMFactory.createOMElement("INSUREDLIST",
					null);
			for (int i = 1; i <= insuredREsult.getMaxRow(); i++) {
				OMElement insureddata = tOMFactory.createOMElement(
						"INSUREDDATA", null);
				tLoginVerifyTool.addOm(insureddata, "CONTNO",
						insuredREsult.GetText(i, 1));
				tLoginVerifyTool.addOm(insureddata, "INSUREDNO",
						insuredREsult.GetText(i, 2));
				tLoginVerifyTool.addOm(insureddata, "NAME",
						insuredREsult.GetText(i, 3));
				tLoginVerifyTool.addOm(insureddata, "IDNO",
						insuredREsult.GetText(i, 4));
				insuredlist.addChild(insureddata);
			}
			mResponseBodyElement.addChild(insuredlist);
		}
		return true;
	}

}
