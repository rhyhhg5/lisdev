package com.sinosoft.lis.crminterface;

import java.util.Iterator;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.db.LLCaseDB;
import com.sinosoft.lis.schema.LLCaseSchema;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LPCaseDetailQueryBL extends GroupLegalAbstractInterface {

	/** 赔案号 **/
	private static final String rgtNo = "rgtNo";
	private String mCaseno = "";

	public LPCaseDetailQueryBL() {
		this.mRootName = "LPCaseDetailQuery";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (rgtNo.equals(tChildElement.getLocalName())) {
				mCaseno = tChildElement.getText();
				if ((!"".equals(mCaseno)) && mCaseno != null) {
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("请录入赔案号");
				return false;
			}
			
		}

		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

		//判断对方传入的是个案号还是批次号

		LLCaseSet tLLCaseSet = new LLCaseSet();
		LLCaseDB tLLCaseDB = new LLCaseDB();
		if("P".equals(mCaseno.substring(0, 1))){
			tLLCaseDB.setRgtNo(mCaseno);
		}else{
			tLLCaseDB.setCaseNo(mCaseno);
		}
        tLLCaseSet.set(tLLCaseDB.query());
        if (tLLCaseDB.mErrors.needDealError()) {
            // @@错误处理
        	mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("个人案件查询失败");
            return false;
        }
        for(int i=1;i<=tLLCaseSet.size();i++){
        	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
        	tLLCaseSchema = tLLCaseSet.get(i);
        	String tRgtState = tLLCaseSchema.getRgtState();
        	String tCustomerNo = tLLCaseSchema.getCustomerNo();
        	String tCaseNo = tLLCaseSchema.getCaseNo();
        	if("01".equals(tRgtState) || "02".equals(tRgtState) || 
        			"03".equals(tRgtState) || "13".equals(tRgtState) || 
        			"16".equals(tRgtState)){//案件状态在“理算确认”之前，尚未关联保单，通过被保险人的客户号查询保单信息
    			//处理保单信息清单
        		String tContQuerySQL = "select contno,grpcontno,cvalidate," +
        				"cinvalidate,db2inst1.codename('poltypeflag',poltype),appntname " +
        				"from lccont where insuredno='"+tCustomerNo+"' union " +
        				"select contno,grpcontno,cvalidate," +
        				"cinvalidate,db2inst1.codename('poltypeflag',poltype),appntname " +
        				"from lbcont where insuredno='"+tCustomerNo+"'";     		
        		SSRS tContQuerySSRS = tExeSQL.execSQL(tContQuerySQL);
        		for(int j=1;j <=tContQuerySSRS.getMaxRow();j++){
        			OMElement contdata = tOMFactory.createOMElement("POLICY_INFO", null);
        			if("00000000000000000000".equals(tContQuerySSRS.GetText(j, 2))){//个单
            			tLoginVerifyTool.addOm(contdata, "grpContNo",
            					tContQuerySSRS.GetText(j, 1));
            			tLoginVerifyTool.addOm(contdata, "contNo",
            					"");
        			}else{
        				tLoginVerifyTool.addOm(contdata, "grpContNo",
            					tContQuerySSRS.GetText(j, 2));
            			tLoginVerifyTool.addOm(contdata, "contNo",
            					tContQuerySSRS.GetText(j, 1));
        			}
        			tLoginVerifyTool.addOm(contdata, "cValidate",
        					tContQuerySSRS.GetText(j, 3));
        			tLoginVerifyTool.addOm(contdata, "cInValidate",
        					tContQuerySSRS.GetText(j, 4));
        			tLoginVerifyTool.addOm(contdata, "polType",
        					tContQuerySSRS.GetText(j, 5));
        			
        			//处理投保人清单
        			OMElement appntdata = tOMFactory.createOMElement(
        					"AppntList", null);
        			tLoginVerifyTool.addOm(appntdata, "appntName",
        					tContQuerySSRS.GetText(j, 6));
        			contdata.addChild(appntdata);
        			
        			//处理被保人清单-多被保人
        			String tInsuredQuerySQL = "select name ,idtype ,idno ,insuredno from lcinsured " +
        					"where contno='"+tContQuerySSRS.GetText(j, 1)+"' " +
        					"union select name ,idtype ,idno ,insuredno from lbinsured " +
        					"where contno='"+tContQuerySSRS.GetText(j, 1)+"' ";
        			SSRS tInsuredQuerySSRS = tExeSQL.execSQL(tInsuredQuerySQL);
        			for(int k=1;k <=tInsuredQuerySSRS.getMaxRow();k++){
            			OMElement insureddata = tOMFactory.createOMElement("InsuredList", null);
            			tLoginVerifyTool.addOm(insureddata, "insuredName",
            					tInsuredQuerySSRS.GetText(k, 1));
            			tLoginVerifyTool.addOm(insureddata, "idType",
            					tInsuredQuerySSRS.GetText(k, 2));
            			tLoginVerifyTool.addOm(insureddata, "idNo",
            					tInsuredQuerySSRS.GetText(k, 3));
            			
            			//处理险种信息
            			String tRiskQuerySQL = "select a.riskcode ,case when (select SubRiskFlag from lmriskapp where riskcode=a.riskcode)='M' then '主险' else '附加险' end ," +
            					"a.enddate from lcpol a where a.insuredno='"+tInsuredQuerySSRS.GetText(k, 4)+"' " +
            					"and a.contno='"+tContQuerySSRS.GetText(j, 1)+"' " +
            					"union select a.riskcode ,case when (select SubRiskFlag from lmriskapp where riskcode=a.riskcode)='M' then '主险' else '附加险' end ," +
            					"a.enddate from lbpol a where a.insuredno='"+tInsuredQuerySSRS.GetText(k, 4)+"' " +
            					"and a.contno='"+tContQuerySSRS.GetText(j, 1)+"' ";
            			SSRS tRiskQuerySSRS = tExeSQL.execSQL(tRiskQuerySQL);
            			for(int m=1;m <=tRiskQuerySSRS.getMaxRow();m++){
	            			OMElement riskdata = tOMFactory.createOMElement("RiskList", null);
	            			tLoginVerifyTool.addOm(riskdata, "riskCode",
	            					tRiskQuerySSRS.GetText(m, 1));
	            			tLoginVerifyTool.addOm(riskdata, "riskMark",
	            					tRiskQuerySSRS.GetText(m, 2));
	            			tLoginVerifyTool.addOm(riskdata, "endDate",
	            					tRiskQuerySSRS.GetText(m, 3));
	            			insureddata.addChild(riskdata);
            			}
            			contdata.addChild(insureddata);
        			}
        			mResponseBodyElement.addChild(contdata);
        		}
        	}else{//直接从llclaimdetail表中查询保单信息
    			//处理保单信息清单
        		String tContQuerySQL = "select a.contno,a.grpcontno,a.cvalidate," +
        				"a.cinvalidate,db2inst1.codename('poltypeflag',a.poltype),a.appntname " +
        				"from lccont a,llclaimdetail b where a.contno=b.contno and b.caseno='"+tCaseNo+"' union " +
        				"select a.contno,a.grpcontno,a.cvalidate," +
        				"a.cinvalidate,db2inst1.codename('poltypeflag',a.poltype),a.appntname " +
        				"from lbcont a,llclaimdetail b where a.contno=b.contno and b.caseno='"+tCaseNo+"'";  
        		SSRS tContQuerySSRS = tExeSQL.execSQL(tContQuerySQL);
        		for(int j=1;j <=tContQuerySSRS.getMaxRow();j++){
        			OMElement contdata = tOMFactory.createOMElement("POLICY_INFO", null);
        			if("00000000000000000000".equals(tContQuerySSRS.GetText(j, 2))){//个单
            			tLoginVerifyTool.addOm(contdata, "grpContNo",
            					tContQuerySSRS.GetText(j, 1));
            			tLoginVerifyTool.addOm(contdata, "contNo",
            					"");
        			}else{
        				tLoginVerifyTool.addOm(contdata, "grpContNo",
            					tContQuerySSRS.GetText(j, 2));
            			tLoginVerifyTool.addOm(contdata, "contNo",
            					tContQuerySSRS.GetText(j, 1));
        			}
        			tLoginVerifyTool.addOm(contdata, "cValidate",
        					tContQuerySSRS.GetText(j, 3));
        			tLoginVerifyTool.addOm(contdata, "cInValidate",
        					tContQuerySSRS.GetText(j, 4));
        			tLoginVerifyTool.addOm(contdata, "polType",
        					tContQuerySSRS.GetText(j, 5));
        			
        			//处理投保人清单
        			OMElement appntdata = tOMFactory.createOMElement(
        					"AppntList", null);
        			tLoginVerifyTool.addOm(appntdata, "appntName",
        					tContQuerySSRS.GetText(j, 6));
        			contdata.addChild(appntdata);
        			
        			//处理被保人清单-多被保人
        			String tInsuredQuerySQL = "select name ,idtype ,idno ,insuredno from lcinsured " +
        					"where contno='"+tContQuerySSRS.GetText(j, 1)+"' " +
        					"union select name ,idtype ,idno ,insuredno from lbinsured " +
        					"where contno='"+tContQuerySSRS.GetText(j, 1)+"' ";
        			SSRS tInsuredQuerySSRS = tExeSQL.execSQL(tInsuredQuerySQL);
        			for(int k=1;k <=tInsuredQuerySSRS.getMaxRow();k++){
            			OMElement insureddata = tOMFactory.createOMElement("InsuredList", null);
            			tLoginVerifyTool.addOm(insureddata, "insuredName",
            					tInsuredQuerySSRS.GetText(k, 1));
            			tLoginVerifyTool.addOm(insureddata, "idType",
            					tInsuredQuerySSRS.GetText(k, 2));
            			tLoginVerifyTool.addOm(insureddata, "idNo",
            					tInsuredQuerySSRS.GetText(k, 3));
            			
            			//处理险种信息
            			String tRiskQuerySQL = "select a.riskcode ,case when (select SubRiskFlag from lmriskapp where riskcode=a.riskcode)='M' then '主险' else '附加险' end ," +
            					"a.enddate from lcpol a where a.insuredno='"+tInsuredQuerySSRS.GetText(k, 4)+"' " +
            					"and a.contno='"+tContQuerySSRS.GetText(j, 1)+"' " +
            					"union select a.riskcode ,case when (select SubRiskFlag from lmriskapp where riskcode=a.riskcode)='M' then '主险' else '附加险' end ," +
            					"a.enddate from lbpol a where a.insuredno='"+tInsuredQuerySSRS.GetText(k, 4)+"' " +
            					"and a.contno='"+tContQuerySSRS.GetText(j, 1)+"' ";
            			SSRS tRiskQuerySSRS = tExeSQL.execSQL(tRiskQuerySQL);
            			for(int m=1;m <=tRiskQuerySSRS.getMaxRow();m++){
	            			OMElement riskdata = tOMFactory.createOMElement("RiskList", null);
	            			tLoginVerifyTool.addOm(riskdata, "riskCode",
	            					tRiskQuerySSRS.GetText(m, 1));
	            			tLoginVerifyTool.addOm(riskdata, "riskMark",
	            					tRiskQuerySSRS.GetText(m, 2));
	            			tLoginVerifyTool.addOm(riskdata, "endDate",
	            					tRiskQuerySSRS.GetText(m, 3));
	            			insureddata.addChild(riskdata);
            			}
            			contdata.addChild(insureddata);
        			}
        			mResponseBodyElement.addChild(contdata);
        		}
        	}
    		//处理案件相关信息
    		String tClaimInfoSQL = "select a.caseno,(select accdate from LLSubReport where subrptno in(select subrptno from llcaserela where caseno=a.caseno) fetch first 1 rows only) ," +
    				"a.rgtstate ,a.rgtdate ,(select sum(realpay) from llclaimdetail where caseno=a.caseno) ," +
    				"a.customername ,a.idtype ,a.idno " +
    				"from llcase a where a.caseno='"+tCaseNo+"' " ;		
    		SSRS tClaimInfoSSRS = tExeSQL.execSQL(tClaimInfoSQL);
    		for(int j=1;j <=tClaimInfoSSRS.getMaxRow();j++){
    			OMElement claiminfodata = tOMFactory.createOMElement("CLAIM_INFO", null);
				tLoginVerifyTool.addOm(claiminfodata, "caseNo",
						tClaimInfoSSRS.GetText(j, 1));
    			tLoginVerifyTool.addOm(claiminfodata, "accDate",
    					tClaimInfoSSRS.GetText(j, 2));
    			tLoginVerifyTool.addOm(claiminfodata, "rgtState",
    					tClaimInfoSSRS.GetText(j, 3));
    			tLoginVerifyTool.addOm(claiminfodata, "rgtDate",
    					tClaimInfoSSRS.GetText(j, 4));
    			if("".equals(tClaimInfoSSRS.GetText(j, 5)) || "null".equals(tClaimInfoSSRS.GetText(j, 5)) || tClaimInfoSSRS.GetText(j, 5)==null){
        			tLoginVerifyTool.addOm(claiminfodata, "realPay",
        					"");
    			}else{
        			tLoginVerifyTool.addOm(claiminfodata, "realPay",
        					tClaimInfoSSRS.GetText(j, 5));
    			}

    			
    			//处理出险人信息
    			OMElement claimcusdata = tOMFactory.createOMElement(
    					"ClaimCustList", null);
    			tLoginVerifyTool.addOm(claimcusdata, "name",
    					tClaimInfoSSRS.GetText(j, 6));
    			tLoginVerifyTool.addOm(claimcusdata, "idType",
    					tClaimInfoSSRS.GetText(j, 7));
    			tLoginVerifyTool.addOm(claimcusdata, "idNo",
    					tClaimInfoSSRS.GetText(j, 8));
    			claiminfodata.addChild(claimcusdata);  
    			
    			mResponseBodyElement.addChild(claiminfodata);
    		}
    		
    		//处理赔付相关信息
    		String tPayInfoSQL = "select a.caseno ," +
    				"(select sum(realpay) from llclaimdetail where caseno=a.caseno) " +
    				"from llcase a where a.rgtstate in ('11','12') " +
    				"and a.caseno='"+tCaseNo+"'"  ;		
    		SSRS tPayInfoSSRS = tExeSQL.execSQL(tPayInfoSQL);
    		if(tPayInfoSSRS.getMaxRow()>0){
    			for(int j=1;j <=tPayInfoSSRS.getMaxRow();j++){
        			OMElement payinfodata = tOMFactory.createOMElement("PAY_INFO", null);
    				tLoginVerifyTool.addOm(payinfodata, "caseNo",
    						tPayInfoSSRS.GetText(j, 1));
    				if("".equals(tPayInfoSSRS.GetText(j, 2)) || "null".equals(tPayInfoSSRS.GetText(j, 2)) || tPayInfoSSRS.GetText(j, 2)==null){
            			tLoginVerifyTool.addOm(payinfodata, "sumGetMoney",
            					"");
    				}else{
            			tLoginVerifyTool.addOm(payinfodata, "sumGetMoney",
            					tPayInfoSSRS.GetText(j, 2));
    				}

        			mResponseBodyElement.addChild(payinfodata);
        			
        		}
    		}else{
    			OMElement payinfodata = tOMFactory.createOMElement("PAY_INFO", null);
				tLoginVerifyTool.addOm(payinfodata, "caseNo",
						tCaseNo);
    			tLoginVerifyTool.addOm(payinfodata, "sumGetMoney",
    					"");
    			
    			mResponseBodyElement.addChild(payinfodata);
    		}
    		
    	}
		return true;
	}
}
