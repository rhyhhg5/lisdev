package com.sinosoft.lis.crminterface;

import java.util.Iterator;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_interestquery extends CRMAbstractInterface {
	/** 险种编码 **/
	private static final String RISKDATA = "RISKDATA";
	private static final String RISKCODE = "RISKCODE";
	private String mRiskcode = "";

	// private int i = 0;

	public Crm_interestquery() {
		this.mRootName = "CRM_INTERESTQUERY";
	}

	public boolean returnResponseBody() throws Exception {

		Iterator tBodyIterator = mBodyElement.getChildren();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		OMElement trisklist = tOMFactory.createOMElement("RISKLIST", null);

		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			Iterator trisk = tChildElement.getChildren();
			while (trisk.hasNext()) {
				OMElement triskdata = (OMElement) trisk.next();
				if (RISKDATA.equals(triskdata.getLocalName())) {
					Iterator triskcode = triskdata.getChildren();
					while (triskcode.hasNext()) {
						OMElement tcode = (OMElement) triskcode.next();
						if (RISKCODE.equals(tcode.getLocalName())) {
							mRiskcode = tcode.getText();
							if ((!"".equals(mRiskcode)) && mRiskcode != null) {
								// i++;
								String sql = "select riskcode as 险种代码,"
										+ "(select riskname from lmriskapp where riskcode=LMInsuAccRate.riskcode) as 险种代码,"
										+ "rate as 结算利率,BalaDate as 结算日期  from LMInsuAccRate "
										+ "where riskcode='" + mRiskcode
										+ "' order by startbaladate desc"
										+ " fetch first 1 rows only with ur ";
								ExeSQL tExeSQL = new ExeSQL();
								SSRS sResult;
								sResult = tExeSQL.execSQL(sql);
								if (sResult.getMaxRow() > 0) {
									OMElement riskdata = tOMFactory
											.createOMElement("RISKDATA", null);
									tLoginVerifyTool.addOm(riskdata,
											"RISKCODE", sResult.GetText(1, 1));
									tLoginVerifyTool.addOm(riskdata,
											"RISKNAME", sResult.GetText(1, 2));
                                    
                                    tLoginVerifyTool.addOm(riskdata,
                                            "BALADATE", sResult.GetText(1, 4));
                                    
									tLoginVerifyTool.addOm(riskdata, "RATE",
											sResult.GetText(1, 3));
									trisklist.addChild(riskdata);
								}
							}else{
                              mErrCode = ERRCODE_CHECK;
                              mErrors.addOneError("险种编码不能为空");
                              return false;
                            }
						}

					}
				}
			}
		}
		// if (i == 0) {
		// mErrCode = ERRCODE_CHECK;
		// mErrors.addOneError("险种编码不能为空");
		// return false;
		// }
		if (trisklist.getChildren().hasNext()) {
			mResponseBodyElement.addChild(trisklist);
		}
		return true;
	}
}
