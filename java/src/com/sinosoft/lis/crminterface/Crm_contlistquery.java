package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_contlistquery extends CRMAbstractInterface {

	/** 当前页码 **/
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/** 每页记录条数 **/
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/** 客户号 **/
	private static final String CUSTOMNO = "CUSTOMNO";
	private String mCustomno = "";
	/** 姓名 **/
	private static final String NAME = "NAME";
	private String mName = "";
	/** 投/被保人证件号 */
	private static final String IDNO = "IDNO";
	private String mIdno = "";
	/** 印刷号 **/
	private static final String PRTNO = "PRTNO";
	private String mPrtNo = "";

	public Crm_contlistquery() {
		this.mRootName = "CRM_CONTLISTQUERY";
	}
	public boolean returnResponseBody() throws Exception {
		String sql = "";
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if ((!"".equals(mPageindex)) && mPageindex != null) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数为必录项");
				return false;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if ((!"".equals(mPagesize)) && mPagesize != null) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数为必录项");
				return false;
			}
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					tsql = tsql + " and a.CONTNO='" + mContno + "' ";
				}
				continue;
			}
			if (CUSTOMNO.equals(tChildElement.getLocalName())) {
				mCustomno = tChildElement.getText();
				if ((!"".equals(mCustomno)) && mCustomno != null) {
					tsql = tsql + " and a.appntno='" + mCustomno + "'";
				}
				continue;
			}
			if (NAME.equals(tChildElement.getLocalName())) {
				mName = tChildElement.getText();
				if ((!"".equals(mName)) && mName != null) {
					tsql = tsql + " and a.appntname='" + mName + "'";
				}
				continue;
			}
			if (IDNO.equals(tChildElement.getLocalName())) {
				mIdno = tChildElement.getText();
				if ((!"".equals(mIdno)) && mIdno != null) {
					tsql = tsql
							+ " and a.contno in (select contno from lcinsured where idno='"
							+ mIdno
							+ "' union select contno from lcappnt where idno='"
							+ mIdno
							+ "'union select contno from lbinsured where idno='"
							+ mIdno
							+ "' union select contno from lbappnt where idno='"
							+ mIdno + "')";
				}
				continue;
			}
			if (PRTNO.equals(tChildElement.getLocalName())) {
				mPrtNo = tChildElement.getText();
				if ((!"".equals(mPrtNo)) && mPrtNo != null) {
					tsql = tsql + " and a.PrtNo='" + mPrtNo + "'";
				}
				continue;
			}
		}
		if (("".equals(mContno) || mContno == null)
				&& ("".equals(mCustomno) || mCustomno == null)&&("".equals(mName) || mName == null)
				&& ("".equals(mIdno) || mIdno == null)&& ("".equals(mPrtNo) || mPrtNo == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("保单号、客户号、姓名、投/被保人证件号、个单/团单投保单印刷号必录其一");
			return false;
		}
		
		sql ="select m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12 " 
			+ " from ( "
			+ "select a.contno as m1,"
				+ "a.conttype as m2 ,"
				+ "a.cvalidate as m3,"
				+ "a.stateflag as m4,"
				+ "a.appntname as m5,"
				+ "a.appntidno as m6,"
				+ "a.managecom as m7,"
				+ "(case when a.cardflag='b' then '97' when a.cardflag='c' then '96' when a.cardflag='d' then '98' when a.prtno like 'PD%' then '99' else a.salechnl end) as m8,"
				+ "(select Phone from  LCAddress where customerno=appntno "
				+ "and addressno=(select addressno from lcappnt where contno=a.contno and appntno=a.appntno) ) as m9 "
				+ ", case when poltype='0' then '团单-个人单'  when poltype='1' then'无名单' when poltype ='2' then '团单-公共帐户'  else '个人单'  end  m10,"
				+ "a.prem m11,a.cinvalidate m12 "
				+ " from lccont a "
				+ " where 1=1  "
				+ tsql
				+ " union all"
				+ " select a.contno as 保单号,"
				+ "a.conttype as 保单类型 ,"
				+ "a.cvalidate as 生效日期,"
				+ "(case when a.cardflag in ('9','c','d') then 'x' else a.stateflag end) as 保单状态,"
				+ "a.appntname as 投保人姓名,"
				+ "a.appntidno as 投保人证件号,"
				+ "a.managecom as 所属机构,"
				+ "(case when a.cardflag='b' then '97' when a.cardflag='c' then '96' when a.cardflag='d' then '98' when a.prtno like 'PD%' then '99' else a.salechnl end) as 渠道,"
				+ "(select Phone from  LCAddress where customerno=appntno "
				+ " and addressno=(select addressno from lbappnt where contno=a.contno and appntno=a.appntno) ) as 投保人联系电话"
				+ ", case when poltype='0' then '团单-个人单'  when poltype='1' then'无名单' when poltype ='2' then '团单-公共帐户'  else '个人单'  end  m10,"
				+ "a.prem m11,a.cinvalidate m12 "
				+ " from lbcont a where 1=1 " + tsql 
				+" ) temp "
				+" order by m4 ,m3 ";
		ExeSQL tExeSQL = new ExeSQL();
		String totalnum = "select count(*) from ("+sql+") cou with ur"; 
		SSRS tResult;
		SSRS ttotal = tExeSQL.execSQL(totalnum);
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
				ttotal.GetText(1, 1));
		sql="select * from (select c.*,rownumber() over() as rn from (" + sql
				+ ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		tResult = tExeSQL.execSQL(sql);
		if (tResult.getMaxRow() > 0) {
			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
			tOMFactory = OMAbstractFactory.getOMFactory();
			int trow = tResult.MaxRow;
			OMElement tcustomerlist = tOMFactory.createOMElement("CONTLIST",
					null);
			for (int i = 0; i < trow; i++) {
				OMElement tcustomer = tOMFactory.createOMElement("CONTDATA",
						null);
				tLoginVerifyTool.addOm(tcustomer, "CONTNO",
						tResult.GetText(i + 1, 1));
				tLoginVerifyTool.addOm(tcustomer, "CONTTYPE",
						tResult.GetText(i + 1, 2));
				tLoginVerifyTool.addOm(tcustomer, "CVALIDATE",
						tResult.GetText(i + 1, 3));
				tLoginVerifyTool.addOm(tcustomer, "STATEFLAG",
						tResult.GetText(i + 1, 4));
				tLoginVerifyTool.addOm(tcustomer, "APPNTNAME",
						tResult.GetText(i + 1, 5));
				tLoginVerifyTool.addOm(tcustomer, "APPNTIDNO",
						tResult.GetText(i + 1, 6));
				tLoginVerifyTool.addOm(tcustomer, "MANAGECOM",
						tResult.GetText(i + 1, 7));
				tLoginVerifyTool.addOm(tcustomer, "SALECHNL",
						tResult.GetText(i + 1, 8));
				tLoginVerifyTool.addOm(tcustomer, "APPNTPHONE",
						tResult.GetText(i + 1, 9));
				tLoginVerifyTool.addOm(tcustomer, "POLICYTYPE",
						tResult.GetText(i + 1, 10));
				tLoginVerifyTool.addOm(tcustomer, "EACHPREM",
						tResult.GetText(i + 1, 11));
				tLoginVerifyTool.addOm(tcustomer, "ENDDATE",
						tResult.GetText(i + 1, 12));
				
				String risksql ="select riskcode," +
                        "(select riskname from lmriskapp where riskcode=lcpol.riskcode) as 险种名称, " +
                        "insuredname as 被保险人姓名, " +
                        "(select idno from lcinsured where contno=lcpol.contno and insuredno=lcpol.insuredno) as 被保险人证件号 " +
                        ",(select idtype from lcinsured where contno=lcpol.contno and insuredno=lcpol.insuredno) as 被保险人证件类型 "+
                        ",(select sex from lcinsured where contno=lcpol.contno and insuredno=lcpol.insuredno) as 被保险人性别 "+
                        "from lcpol where contno ='"+tResult.GetText(i + 1, 1)+ "" +
						"'union " +
                        "select riskcode," +
                        "(select riskname from lmriskapp where riskcode=lbpol.riskcode) as 险种名称, " +
                        "insuredname as 被保险人姓名, " +
                        "(select idno from lbinsured where contno=lbpol.contno and insuredno=lbpol.insuredno union select idno from lcinsured where contno=lbpol.contno and insuredno=lbpol.insuredno fetch first 1 rows only) as 被保险人证件号 " +
                        ",(select idtype from lbinsured where contno=lbpol.contno and insuredno=lbpol.insuredno union select idtype from lcinsured where contno=lbpol.contno and insuredno=lbpol.insuredno fetch first 1 rows only) as 被保险人证件类型 "+
                        ",(select sex from lbinsured where contno=lbpol.contno and insuredno=lbpol.insuredno union select sex from lcinsured where contno=lbpol.contno and insuredno=lbpol.insuredno fetch first 1 rows only) as 被保险人性别 "+
                        " from lbpol where contno ='"+tResult.GetText(i + 1, 1)+"' with ur ";
				SSRS triskResult;
				triskResult = tExeSQL.execSQL(risksql);
				if (triskResult.getMaxRow()>0) {
					OMElement trisklist = tOMFactory.createOMElement("RISKLIST",
						null);
				
				int riskrow = triskResult.MaxRow;// triskResult.MaxRow;
				for (int j = 0; j < riskrow; j++) {
					OMElement trisk = tOMFactory.createOMElement("RISKDATA", null);
					tLoginVerifyTool.addOm(trisk, "RISKCODE",
							triskResult.GetText(j + 1, 1));
                    tLoginVerifyTool.addOm(trisk, "RISKNAME",
                            triskResult.GetText(j + 1, 2));
                    tLoginVerifyTool.addOm(trisk, "INSUREDNAME",
                            triskResult.GetText(j + 1, 3));
                    tLoginVerifyTool.addOm(trisk, "INSUREDIDNO",
                            triskResult.GetText(j + 1, 4));
                    tLoginVerifyTool.addOm(trisk, "IDTYPE",
                            triskResult.GetText(j + 1, 5));
                    tLoginVerifyTool.addOm(trisk, "SEX",
                            triskResult.GetText(j + 1, 6));
                    trisklist.addChild(trisk);
				}		
				tcustomer.addChild(trisklist);
				}
				tcustomerlist.addChild(tcustomer);
			}
			mResponseBodyElement.addChild(tcustomerlist);
		}
		return true;
	}

}
