package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_grpproposalquery extends CRMAbstractInterface {

	/** 当前页码 **/
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/** 每页记录条数 **/
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/** 印刷号码 **/
	private static final String PRTNO = "PRTNO";
	private String mPrtno = "";
	/** 业务员代码 **/
	private static final String AGENTCODE = "AGENTCODE";
	private String mAgentcode = "";
	/** 业务归档号 **/
	private static final String ARCHIVENO = "ARCHIVENO";
	private String mArchiveno = "";
	/** 管理机构 **/
	private static final String MANAGECOM = "MANAGECOM";
	private String mManagecom = "";

	public Crm_grpproposalquery() {
		this.mRootName = "CRM_GRPPROPOSALQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if (mPageindex != null && !"".equals(mPageindex)) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码、每页记录条数均不能为空");
				return false;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if (mPagesize != null && !"".equals(mPagesize)) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码、每页记录条数均不能为空");
				return false;
			}
			if (PRTNO.equals(tChildElement.getLocalName())) {
				mPrtno = tChildElement.getText();
				if ((!"".equals(mPrtno)) && mPrtno != null) {
					tsql += " and lcg.prtno='" + mPrtno + "' ";
				}
				continue;
			}
			if (AGENTCODE.equals(tChildElement.getLocalName())) {
				mAgentcode = tChildElement.getText();
				if ((!"".equals(mAgentcode)) && mAgentcode != null) {
					AgentCodeTransformation a = new AgentCodeTransformation();
					if(!a.AgentCode(mAgentcode, "Y")){
						System.out.println(a.getMessage());
						mErrCode = ERRCODE_CHECK;
						mErrors.addOneError(a.getMessage());
						return false;
					}
					mAgentcode=a.getResult();
					tsql += " and lcg.agentcode='" + mAgentcode + "' ";
				}
				
				continue;
			}
			if (ARCHIVENO.equals(tChildElement.getLocalName())) {
				mArchiveno = tChildElement.getText();
				if ((!"".equals(mArchiveno)) && mArchiveno != null) {
					tsql += " and exists (select 1 from es_doc_main where doccode=lcg.prtno and  ArchiveNo='"
							+ mArchiveno
							+ "' and BussType='TB' and subtype in ('TB02','TB03','TB07','TB08','TB09','TB25','TB26') ) ";
				}
				continue;
			}
			if (MANAGECOM.equals(tChildElement.getLocalName())) {
				mManagecom = tChildElement.getText();
				if ((!"".equals(mManagecom)) && mManagecom != null) {
					tsql +=" and Lcg.managecom='"+mManagecom+"' ";
				}
				continue;
//				mErrCode = ERRCODE_CHECK;
//				mErrors.addOneError("请录入管理机构");
//				return false;
			}
		}
		if (("".equals(mPrtno) || mPrtno == null)
				&& ("".equals(mAgentcode) || mAgentcode == null)
				&& ("".equals(mArchiveno) || mArchiveno == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("除管理机构外，其余条件必录其一");
			return false;
		}
		
		String sql = "Select Prtno,Agentcode,"
				+ "(Select Archiveno From Es_Doc_Main Where Doccode=Lcg.Prtno And Busstype='TB' And Subtype In ('TB02', 'TB03', 'TB07', 'TB08', 'TB09', 'TB25', 'TB26') Fetch First 1 Rows Only),"
				+ "Managecom,Grpname,Salechnl,Appflag,Cvalidate,Prem From Lcgrpcont Lcg Where 1=1 "
				+ tsql ;
		
        /**实例化ExeSQL**/
        ExeSQL tExeSQL = new ExeSQL();
        String totalnum = "select count(*) from (" + sql + ") cou with ur";

        /**实例化结果集SSRS**/
        SSRS proposalResult;
        SSRS ttotal = tExeSQL.execSQL(totalnum);
        LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

        /**加入到body标签下*/
        tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM", ttotal.GetText(1, 1));
		
		
		// 分页语句
		String proposalsql = "select * from (select c.*,rownumber() over() as rn from ("
				+ sql
				+ ") c) d where d.rn>"
				+ ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		
		
		/**执行sql结果**/
		proposalResult = tExeSQL.execSQL(proposalsql);
		
		/**创造标签并赋值**/
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		
		/**遍历结果**/
		if (proposalResult.getMaxRow() > 0) {
			OMElement proposallist = tOMFactory.createOMElement("PROPOSALLIST",
					null);
			for (int i = 1; i <= proposalResult.getMaxRow(); i++) {
				OMElement proposaldata = tOMFactory.createOMElement(
						"PROPOSALDATA", null);
				tLoginVerifyTool.addOm(proposaldata, "PRTNO",
						proposalResult.GetText(i, 1));
				tLoginVerifyTool.addOm(proposaldata, "AGENTCODE",
						proposalResult.GetText(i, 2));
				tLoginVerifyTool.addOm(proposaldata, "ARCHIVENO",
						proposalResult.GetText(i, 3));
				tLoginVerifyTool.addOm(proposaldata, "MANAGECOM",
						proposalResult.GetText(i, 4));
				tLoginVerifyTool.addOm(proposaldata, "GRPNAME",
						proposalResult.GetText(i, 5));
				tLoginVerifyTool.addOm(proposaldata, "SALECHNL",
						proposalResult.GetText(i, 6));
				tLoginVerifyTool.addOm(proposaldata, "APPFLAG",
						proposalResult.GetText(i, 7));
				tLoginVerifyTool.addOm(proposaldata, "CVALIDATE",
						proposalResult.GetText(i, 8));
				tLoginVerifyTool.addOm(proposaldata, "PREM",
						proposalResult.GetText(i, 9));
				proposallist.addChild(proposaldata);
			}
			mResponseBodyElement.addChild(proposallist);
		}
		return true;
	}

}
