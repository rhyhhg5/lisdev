package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_grpinsuredinfo extends CRMAbstractInterface {

	/** 团体分单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/** 被保人客户号 **/
	private static final String INSUREDNO = "INSUREDNO";
	private String mInsuredno = "";

	public Crm_grpinsuredinfo() {
		this.mRootName = "CRM_GRPINSUREDINFO";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("团体保单号、被保人客户号均为必录项");
				return false;
			}
			if (INSUREDNO.equals(tChildElement.getLocalName())) {
				mInsuredno = tChildElement.getText();
				if ((!"".equals(mInsuredno)) && mInsuredno != null) {
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("团体保单号、被保人客户号均为必录项");
				return false;
			}
		}

		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

		String contsql = "Select Contno,Cvalidate,Cinvalidate,Stateflag "
				+ " From Lccont Where Contno = '" + mContno + "' With Ur ";
		SSRS contResult;
		contResult = tExeSQL.execSQL(contsql);
		if (contResult.getMaxRow() > 0) {
			OMElement contdata = tOMFactory.createOMElement("CONTDATA", null);
			tLoginVerifyTool
					.addOm(contdata, "CONTNO", contResult.GetText(1, 1));
			tLoginVerifyTool.addOm(contdata, "CVALIDATE",
					contResult.GetText(1, 2));
			tLoginVerifyTool.addOm(contdata, "ENDDATE",
					contResult.GetText(1, 3));
			tLoginVerifyTool.addOm(contdata, "STATEFLAG",
					contResult.GetText(1, 4));
			mResponseBodyElement.addChild(contdata);
		}

		String risksql = "Select Riskcode,"
				+ "(Select riskName From Lmriskapp Where Riskcode = Lcp.Riskcode),"
				+ "Amnt From Lcpol Lcp Where Contno = '" + mContno
				+ "' And Insuredno = '" + mInsuredno + "' With Ur ";
		SSRS riskResult;
		riskResult = tExeSQL.execSQL(risksql);
		if (riskResult.getMaxRow() > 0) {
			OMElement risklist = tOMFactory.createOMElement("RISKLIST", null);
			for (int i = 1; i <= riskResult.getMaxRow(); i++) {
				OMElement riskdata = tOMFactory.createOMElement("RISKDATA",
						null);
				tLoginVerifyTool.addOm(riskdata, "RISKCODE",
						riskResult.GetText(i, 1));
				tLoginVerifyTool.addOm(riskdata, "RISKNAME",
						riskResult.GetText(i, 2));
				tLoginVerifyTool.addOm(riskdata, "AMNT",
						riskResult.GetText(i, 3));
				risklist.addChild(riskdata);
			}
			mResponseBodyElement.addChild(risklist);
		}

		String insuredsql = "Select Lci.Insuredno,Lci.Relationtoappnt,"
				+ "Case Lci.Relationtomaininsured When '00' Then '00' Else '01' End,"
				+ "Lci.Relationtomaininsured,"
				+ "Case Lci.Relationtomaininsured When '00' Then Lci.Insuredno Else "
				+ "(Select Insuredno From Lcinsured Where Contno=Lci.Contno And Relationtomaininsured = '00') "
				+ "End,Lci.Sequenceno,Lci.Name,Lci.Sex,Lci.Idtype,Lci.Idno,Lci.Birthday,Lci.Marriage,"
				+ "Lci.Nativeplace,Lci.Occupationcode,Lca.Mobile,Lca.Companyphone,Lca.Homephone,"
				+ "Lca.Postaladdress,Lca.Zipcode,Lca.Email,Lci.Contplancode,Lci.Insuredstat "
				+ " From Lcinsured Lci,Lcaddress Lca Where Lci.Contno = '"
				+ mContno + "'  And Lci.Insuredno = '" + mInsuredno
				+ "' And Lca.Addressno = Lci.Addressno "
				+ "And Lca.Customerno = Lci.Insuredno With Ur ";
		SSRS insuredResult;
		insuredResult = tExeSQL.execSQL(insuredsql);
		if (insuredResult.getMaxRow() > 0) {
			OMElement insureddata = tOMFactory.createOMElement("INSUREDDATA",
					null);
			tLoginVerifyTool.addOm(insureddata, "INSUREDNO",
					insuredResult.GetText(1, 1));
			tLoginVerifyTool.addOm(insureddata, "RELATIONTOAPPNT",
					insuredResult.GetText(1, 2));
			tLoginVerifyTool.addOm(insureddata, "RELATIONCODE",
					insuredResult.GetText(1, 3));
			tLoginVerifyTool.addOm(insureddata, "RELATIONTOMAININSURED",
					insuredResult.GetText(1, 4));
			tLoginVerifyTool.addOm(insureddata, "MAININSUREDNO",
					insuredResult.GetText(1, 5));
			tLoginVerifyTool.addOm(insureddata, "SEQUENCENO",
					insuredResult.GetText(1, 6));
			tLoginVerifyTool.addOm(insureddata, "INSUREDNAME",
					insuredResult.GetText(1, 7));
			tLoginVerifyTool.addOm(insureddata, "INSUREDSEX",
					insuredResult.GetText(1, 8));
			tLoginVerifyTool.addOm(insureddata, "INSUREDIDTYPE",
					insuredResult.GetText(1, 9));
			tLoginVerifyTool.addOm(insureddata, "INSUREDIDNO",
					insuredResult.GetText(1, 10));
			tLoginVerifyTool.addOm(insureddata, "INSUREDBIRTHDAY",
					insuredResult.GetText(1, 11));
			tLoginVerifyTool.addOm(insureddata, "INSUREDMARRIAGE",
					insuredResult.GetText(1, 12));
			tLoginVerifyTool.addOm(insureddata, "INSUREDNATIVE",
					insuredResult.GetText(1, 13));
			tLoginVerifyTool.addOm(insureddata, "INSUREDOCCUPATION",
					insuredResult.GetText(1, 14));
			tLoginVerifyTool.addOm(insureddata, "INSUREDMOBILE",
					insuredResult.GetText(1, 15));
			tLoginVerifyTool.addOm(insureddata, "INSUREDCOMPANYPHONE",
					insuredResult.GetText(1, 16));
			tLoginVerifyTool.addOm(insureddata, "INSUREDHOMEPHONE",
					insuredResult.GetText(1, 17));
			tLoginVerifyTool.addOm(insureddata, "INSUREDADDRESS",
					insuredResult.GetText(1, 18));
			tLoginVerifyTool.addOm(insureddata, "INSUREDZIPCODE",
					insuredResult.GetText(1, 19));
			tLoginVerifyTool.addOm(insureddata, "INSUREDEMAIL",
					insuredResult.GetText(1, 20));
			tLoginVerifyTool.addOm(insureddata, "CONTPLANCODE",
					insuredResult.GetText(1, 21));
			tLoginVerifyTool.addOm(insureddata, "INSUREDSTAT",
					insuredResult.GetText(1, 22));
			mResponseBodyElement.addChild(insureddata);
		}
		return true;
	}

}
