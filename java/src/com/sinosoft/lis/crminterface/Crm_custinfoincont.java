package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_custinfoincont extends CRMAbstractInterface {

	/**
	 * 电话号
	 * */
	private static final String CALLNUMBER = "CALLNUMBER";
	private String mCallNumber = "";

	/**
	 * 保单号
	 * */
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/**
	 * 投/被保人证件号
	 * */
	private static final String IDNO = "IDNO";
	private String mIDNo = "";
	/**
	 * 每页记录条数
	 * */
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/**
	 * 当前页码
	 * */
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;

	public Crm_custinfoincont() {
		this.mRootName = "CRM_CUSTINFOINCONT";
	}

	public boolean returnResponseBody() throws Exception {
		// String tsql =
		// "select a.appntname,a.appntno,a.idno,a.idtype,b.phone,b.PostalAddress,a.Managecom,'A' from lcappnt a ,lcaddress b where  a.Addressno=b.Addressno and b.CustomerNo=a.AppntNo ";
		// String tsq =
		// " Union select a.name,a.appntno,a.idno,a.idtype,b.phone,b.PostalAddress,a.Managecom,'I' from lcinsured a ,lcaddress b where  a.Addressno=b.Addressno and b.CustomerNo=a.AppntNo  ";

		String tsql = "";
        String tgrpsql="";
        String aidno="";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CALLNUMBER.equals(tChildElement.getLocalName())) {
				mCallNumber = tChildElement.getText();
				if ((!"".equals(mCallNumber)) && mCallNumber != null) {
					// tsql = tsql +
					// " and a.Appntno in (select customerno from lcaddress where Phone='"+mCallNumber+"' or Mobile='"+mCallNumber+"' or homephone='"+mCallNumber+"' or companyphone='"+mCallNumber+"')";
					// tsq = tsq +
					// " and a.insuredno in (select customerno from lcaddress where Phone='"+mCallNumber+"' or Mobile='"+mCallNumber+"' or homephone='"+mCallNumber+"' or companyphone='"+mCallNumber+"')";
					tsql = tsql + " and (c.phone='" + mCallNumber
							+ "' or c.homephone='" + mCallNumber
							+ "' or c.companyphone='" + mCallNumber
							+ "' or c.mobile='" + mCallNumber + "') ";
				}
				continue;
			}
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					tsql = tsql + " and a.contno='" + mContno + "' ";
                    tgrpsql=tgrpsql+" and a.grpcontno='" + mContno + "' ";
                    
				}
				continue;
			}
			if (IDNO.equals(tChildElement.getLocalName())) {
				mIDNo = tChildElement.getText();
				if ((!"".equals(mIDNo)) && mIDNo != null) {
					aidno=tsql + " and a.idno='" + mIDNo + "' ";
					tsql = tsql + " and b.idno='" + mIDNo + "' ";
				}
				continue;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if ((!"".equals(mPagesize)) && mPagesize != null) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数都不能为空");
				return false;
			}

			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if ((!"".equals(mPageindex)) && mPageindex != null) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数都不能为空");
				return false;
			}
		}
		if (("".equals(mCallNumber) || mCallNumber == null)
				&& ("".equals(mContno) || mContno == null)
				&& ("".equals(mIDNo) || mIDNo == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("电话号码、保单号、投/被保人证件号必录其一");
			return false;
		}

//		String sql = "select b.appntname 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'A' 客户类型 "
//				+ "from lccont a,lcappnt b,lcaddress c "
//				+ "where a.contno = b.contno "
//				+ "and a.appntno = b.appntno "
//				+ "and b.appntno = c.customerno "
//				+ "and b.addressno = c.addressno "
//				+ tsql
//				+ "union all "
//				+ "select b.appntname 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'A' 客户类型 "
//				+ " from lccont a,lbappnt b,lcaddress c "
//				+ "where a.contno = b.contno "
//				+ "and a.appntno = b.appntno "
//				+ "and b.appntno = c.customerno "
//				+ "and b.addressno = c.addressno "
//				+ tsql
//				+ " union all "
//				+ "select b.appntname 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'A' 客户类型 "
//				+ " from lbcont a,lcappnt b,lcaddress c "
//				+ "where a.contno = b.contno "
//				+ "and a.appntno = b.appntno "
//				+ "and b.appntno = c.customerno "
//				+ "and b.addressno = c.addressno "
//				+ tsql
//				+ " union all select b.appntname 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'A' 客户类型"
//				+ " from lbcont a,lbappnt b,lcaddress c "
//				+ "where a.contno = b.contno "
//				+ "and a.appntno = b.appntno "
//				+ "and b.appntno = c.customerno "
//				+ "and b.addressno = c.addressno "
//				+ tsql
//				+ " union all "
//				+ "select b.name 姓名,b.insuredno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'I' 客户类型"
//				+ " from lccont a,lcinsured b,lcaddress c"
//				+ " where a.contno = b.contno"
//				+ " and a.insuredno = b.insuredno"
//				+ " and b.insuredno = c.customerno"
//				+ " and b.addressno = c.addressno"
//				+ tsql
//				+ " union all"
//				+ " select b.name 姓名,b.insuredno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'I' 客户类型"
//				+ " from lccont a,lbinsured b,lcaddress c"
//				+ " where a.contno = b.contno"
//				+ " and a.insuredno = b.insuredno"
//				+ " and b.insuredno = c.customerno"
//				+ " and b.addressno = c.addressno"
//				+ tsql
//				+ " union all"
//				+ " select b.name 姓名,b.insuredno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'I' 客户类型"
//				+ " from lbcont a,lcinsured b,lcaddress c"
//				+ " where a.contno = b.contno"
//				+ " and a.insuredno = b.insuredno"
//				+ " and b.insuredno = c.customerno"
//				+ " and b.addressno = c.addressno"
//				+ tsql
//				+ " union all"
//				+ " select b.name 姓名,b.insuredno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'I' 客户类型"
//				+ " from lbcont a,lbinsured b,lcaddress c"
//				+ " where a.contno = b.contno"
//				+ " and a.insuredno = b.insuredno"
//				+ " and b.insuredno = c.customerno"
//				+ " and b.addressno = c.addressno" 
//                + tsql 
//                + " union all "
//                + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,c.phone1 联系电话,c.GrpAddress 地址,a.signcom 保单所属机构,'GR' 客户类型 "
//                + " from lcgrpcont a,lcgrpappnt b,lcgrpaddress c "
//                + " where a.grpcontno = b.grpcontno "
//                + " and a.appntno = b.customerno "
//                + " and b.customerno = c.customerno "
//                + " and b.addressno = c.addressno "
//                + " and c.phone1 = '"+mCallNumber+"' "
//                + tgrpsql 
//                + " union all "
//                + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,c.phone1 联系电话,c.GrpAddress 地址,a.signcom 保单所属机构,'GR' 客户类型 "
//                + " from lcgrpcont a,lbgrpappnt b,lcgrpaddress c "
//                + " where a.grpcontno = b.grpcontno "
//                + " and a.appntno = b.customerno "
//                + " and b.customerno = c.customerno "
//                + " and b.addressno = c.addressno "
//                + " and c.phone1 = '"+mCallNumber+"' "
//                + tgrpsql 
//                + " union all "
//                + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,c.phone1 联系电话,c.GrpAddress 地址,a.signcom 保单所属机构,'GR' 客户类型 "
//                + " from lbgrpcont a,lcgrpappnt b,lcgrpaddress c "
//                + "  where a.grpcontno = b.grpcontno "
//                + " and a.appntno = b.customerno "
//                + " and b.customerno = c.customerno "
//                + " and b.addressno = c.addressno "
//                + " and c.phone1 = '"+mCallNumber+"' "
//                + tgrpsql 
//                + " union all "
//                + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,c.phone1 联系电话,c.GrpAddress 地址,a.signcom 保单所属机构,'GR' 客户类型 "
//                + " from lbgrpcont a,lbgrpappnt b,lcgrpaddress c "
//                + " where a.grpcontno = b.grpcontno "
//                + " and a.appntno = b.customerno "
//                + " and b.customerno = c.customerno "
//                + " and b.addressno = c.addressno "
//                + " and c.phone1 = '"+mCallNumber+"' "
//                + tgrpsql 
//                + " union all "
//                + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,b.phone 联系电话,b.PostalAddress 地址,a.signcom 保单所属机构,'GA' 客户类型 "
//                + " from lcgrpcont a,lcgrpappnt b "
//                + " where a.grpcontno = b.grpcontno "
//                + " and a.appntno = b.customerno "
//                + " and b.phone = '"+mCallNumber+"' "
//                + tgrpsql 
//                + " union all "
//                + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,b.phone 联系电话,b.PostalAddress 地址,a.signcom 保单所属机构,'GA' 客户类型 "
//                + " from lcgrpcont a,lbgrpappnt b "
//                + " where a.grpcontno = b.grpcontno "
//                + " and a.appntno = b.customerno "
//                + " and b.phone = '"+mCallNumber+"' "
//                + tgrpsql 
//                + " union all "
//                + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,b.phone 联系电话,b.PostalAddress 地址,a.signcom 保单所属机构,'GA' 客户类型 "
//                + " from lbgrpcont a,lcgrpappnt b "
//                + " where a.grpcontno = b.grpcontno "
//                + " and a.appntno = b.customerno "
//                + " and b.phone = '' "
//                + tgrpsql 
//                + " union all "
//                + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,b.phone 联系电话,b.PostalAddress 地址,a.signcom 保单所属机构,'GA' 客户类型 "
//                + " from lbgrpcont a,lbgrpappnt b "
//                + " where a.grpcontno = b.grpcontno "
//                + " and a.appntno = b.customerno "
//                + " and b.phone = '"+mCallNumber+"'  "
//                + tgrpsql 
//                + "  with ur";
//                   
		String sql=" select distinct * from (select b.appntname 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'A' 客户类型 "
				+ "from lccont a,lcappnt b,lcaddress c "
		+ "where a.contno = b.contno "
		+ "and a.appntno = b.appntno "
		+ "and b.appntno = c.customerno "
		+ "and b.addressno = c.addressno "
		+ tsql 
		+ "union all "
		+ "select b.appntname 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'A' 客户类型 "
		+ "from lccont a,lbappnt b,lcaddress c "
		+ "where a.contno = b.contno "
		+ "and a.appntno = b.appntno "
		+ "and b.appntno = c.customerno "
		+ "and b.addressno = c.addressno "
		+ tsql 
		+ "union all "
		+ "select b.appntname 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'A' 客户类型 "
		+ "from lbcont a,lcappnt b,lcaddress c "
		+ "where a.contno = b.contno "
		+ "and a.appntno = b.appntno "
		+ "and b.appntno = c.customerno "
		+ "and b.addressno = c.addressno "
		+ tsql 
		+ "union all "
		+ "select b.appntname 姓名,b.appntno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'A' 客户类型 "
		+ "from lbcont a,lbappnt b,lcaddress c "
		+ "where a.contno = b.contno "
		+ "and a.appntno = b.appntno "
		+ "and b.appntno = c.customerno "
		+ "and b.addressno = c.addressno "
		+ tsql 
		+ "union all "
		+ "select a.name 姓名,a.insuredno 客户号,a.idno 证件号码,a.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,(select signcom from lccont where contno=a.contno union select signcom from lbcont where contno=a.contno) 保单所属机构,'I' 客户类型 "
		+ "from lcinsured a,lcaddress c "
		+ "where 1=1 "
		+ "and a.insuredno = c.customerno "
		+ "and a.addressno = c.addressno ";
		if(aidno!="" && aidno.length()>0){
			sql+=aidno;
		}else{
			sql+=tsql; 
		}
		sql=sql+ "union all "
		+ "select a.name 姓名,a.insuredno 客户号,a.idno 证件号码,a.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,(select signcom from lccont where contno=a.contno union select signcom from lbcont where contno=a.contno) 保单所属机构,'I' 客户类型 "
		+ "from lbinsured a,lcaddress c "
		+ "where 1=1 "
		+ "and a.insuredno = c.customerno "
		+ "and a.addressno = c.addressno ";
		if(aidno!="" && aidno.length() >0){
			sql+=aidno;
		}else{
			sql+=tsql; 
		}
		sql=sql 
		+ " union all"
		+ " select b.name 姓名,b.insuredno 客户号,b.idno 证件号码,b.idtype 证件类型,c.phone 联系电话,c.postaladdress 地址,a.signcom 保单所属机构,'I' 客户类型"
		+ " from lbcont a,lbinsured b,lcaddress c"
		+ " where a.contno = b.contno"
		+ " and a.insuredno = b.insuredno"
		+ " and b.insuredno = c.customerno"
		+ " and b.addressno = c.addressno" 
        + tsql 
        + " union all "
        + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,c.phone1 联系电话,c.GrpAddress 地址,a.signcom 保单所属机构,'GR' 客户类型 "
        + " from lcgrpcont a,lcgrpappnt b,lcgrpaddress c "
        + " where a.grpcontno = b.grpcontno "
        + " and a.appntno = b.customerno "
        + " and b.customerno = c.customerno "
        + " and b.addressno = c.addressno "
        + " and c.phone1 = '"+mCallNumber+"' "
        + tgrpsql 
        + " union all "
        + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,c.phone1 联系电话,c.GrpAddress 地址,a.signcom 保单所属机构,'GR' 客户类型 "
        + " from lcgrpcont a,lbgrpappnt b,lcgrpaddress c "
        + " where a.grpcontno = b.grpcontno "
        + " and a.appntno = b.customerno "
        + " and b.customerno = c.customerno "
        + " and b.addressno = c.addressno "
        + " and c.phone1 = '"+mCallNumber+"' "
        + tgrpsql 
        + " union all "
        + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,c.phone1 联系电话,c.GrpAddress 地址,a.signcom 保单所属机构,'GR' 客户类型 "
        + " from lbgrpcont a,lcgrpappnt b,lcgrpaddress c "
        + "  where a.grpcontno = b.grpcontno "
        + " and a.appntno = b.customerno "
        + " and b.customerno = c.customerno "
        + " and b.addressno = c.addressno "
        + " and c.phone1 = '"+mCallNumber+"' "
        + tgrpsql 
        + " union all "
        + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,c.phone1 联系电话,c.GrpAddress 地址,a.signcom 保单所属机构,'GR' 客户类型 "
        + " from lbgrpcont a,lbgrpappnt b,lcgrpaddress c "
        + " where a.grpcontno = b.grpcontno "
        + " and a.appntno = b.customerno "
        + " and b.customerno = c.customerno "
        + " and b.addressno = c.addressno "
        + " and c.phone1 = '"+mCallNumber+"' "
        + tgrpsql 
        + " union all "
        + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,b.phone 联系电话,b.PostalAddress 地址,a.signcom 保单所属机构,'GA' 客户类型 "
        + " from lcgrpcont a,lcgrpappnt b "
        + " where a.grpcontno = b.grpcontno "
        + " and a.appntno = b.customerno "
        + " and b.phone = '"+mCallNumber+"' "
        + tgrpsql 
        + " union all "
        + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,b.phone 联系电话,b.PostalAddress 地址,a.signcom 保单所属机构,'GA' 客户类型 "
        + " from lcgrpcont a,lbgrpappnt b "
        + " where a.grpcontno = b.grpcontno "
        + " and a.appntno = b.customerno "
        + " and b.phone = '"+mCallNumber+"' "
        + tgrpsql 
        + " union all "
        + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,b.phone 联系电话,b.PostalAddress 地址,a.signcom 保单所属机构,'GA' 客户类型 "
        + " from lbgrpcont a,lcgrpappnt b "
        + " where a.grpcontno = b.grpcontno "
        + " and a.appntno = b.customerno "
        + " and b.phone = '' "
        + tgrpsql 
        + " union all "
        + " select b.name 姓名,b.customerno 客户号,'' 证件号码,'' 证件类型,b.phone 联系电话,b.PostalAddress 地址,a.signcom 保单所属机构,'GA' 客户类型 "
        + " from lbgrpcont a,lbgrpappnt b "
        + " where a.grpcontno = b.grpcontno "
        + " and a.appntno = b.customerno "
        + " and b.phone = '"+mCallNumber+"'  "
        + tgrpsql 
        + "  with ur ) with ur ";

		ExeSQL tExeSQL = new ExeSQL();
		String totalsql = "select count(*) from (" + sql + ") totl with ur";
		SSRS ttotal;
		ttotal = tExeSQL.execSQL(totalsql);

		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
				ttotal.GetText(1, 1));
		String pagesql = "select * from (select distinct c.*,rownumber() over() as rn from ("
				+ sql + ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi)+" with ur ";
		SSRS tResult;
		tResult = tExeSQL.execSQL(pagesql);
		if (tResult.getMaxRow() > 0) {
			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
			int trow = tResult.MaxRow;
			OMElement tcustomerlist = tOMFactory.createOMElement(
					"CUSTOMERLIST", null);
			for (int i = 0; i < trow; i++) {
				OMElement tcustomer = tOMFactory.createOMElement(
						"CUSTOMERDATA", null);
				tLoginVerifyTool.addOm(tcustomer, "CUSTOMERNAME",
						tResult.GetText(i + 1, 1));
				tLoginVerifyTool.addOm(tcustomer, "CUSTOMERNO",
						tResult.GetText(i + 1, 2));
				tLoginVerifyTool.addOm(tcustomer, "IDNO",
						tResult.GetText(i + 1, 3));
				tLoginVerifyTool.addOm(tcustomer, "IDTYPE",
						tResult.GetText(i + 1, 4));
				tLoginVerifyTool.addOm(tcustomer, "PHONE",
						tResult.GetText(i + 1, 5));
				tLoginVerifyTool.addOm(tcustomer, "ADDRESS",
						tResult.GetText(i + 1, 6));
				tLoginVerifyTool.addOm(tcustomer, "MANAGECOM",
						tResult.GetText(i + 1, 7));
				tLoginVerifyTool.addOm(tcustomer, "CUSTOMERTYPE",
						tResult.GetText(i + 1, 8));
				tcustomerlist.addChild(tcustomer);
			}
			mResponseBodyElement.addChild(tcustomerlist);
		}
		return true;
	}

}
