package com.sinosoft.lis.crminterface;

import java.io.*;
import java.io.File.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.*;




/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class CrmTranDataBL {
    private String interFilePath = "";
    private String mLogDate="";
    private String mToday = PubFun.getCurrentDate();
    private String mTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    private String CrmPath = "";
    private ExeSQL mExeSQL = new ExeSQL();
    private String mBatchNo = PubFun1.CreateMaxNo("CRMBATCHNO", 20);

    public CrmTranDataBL() {
    }
    public boolean submitData() throws java.io.
            FileNotFoundException, java.io.IOException {
        if (!getInputData())
        {
            return false;
        }
//            System.out.println("getinputdata END...");
 // 进行业务处理
       if (!readTextDate())
      {
           return false;
      }

        return true;
    }
    private boolean getInputData()
   {
       String tSql = "select sysvarvalue from ldsysvar where sysvar = 'CrmInterfaceUrl'";
       CrmPath = mExeSQL.getOneValue(tSql);
//       CrmPath = "D:\\CRMDate\\";
       return true;
   }

    private boolean readTextDate() throws java.io.FileNotFoundException,
            java.io.IOException {
        String str[]= new String[7];
        String mLogName = FindLogFile(CrmPath);
        if(mLogName.equals(""))
            return false;
        interFilePath = CrmPath + mLogName;
        FileReader fr = new FileReader(interFilePath);
        BufferedReader br = new BufferedReader(fr);
        String Line = br.readLine();
        int intFileNum=0;
        while(Line!=null)
       {
          str[intFileNum]=Line;
          System.out.println("第"+intFileNum+"行 :"+str[intFileNum]);
          intFileNum = intFileNum + 1;
          Line   =   br.readLine();
        }
        br.close();
        fr.close();
        String BaseData[] = new String[12];
        BaseData = str[0].split(" | ");
        mLogDate = BaseData[2].substring(0,4)+"-"+BaseData[2].substring(4,6)+"-"+BaseData[2].substring(6,8);

        System.out.println("----"+mLogDate);
        String SingleData[] = new String[6];


        LICrmLogSet mLICrmLogSet = new LICrmLogSet();
        for (int i=1;i<=intFileNum-1;i++)
        {
            SingleData = null;
            SingleData = str[i].split(" | ");
            LICrmLogSchema mLICrmLogSchema = new LICrmLogSchema();
            mLICrmLogSchema.setBatchNo(mBatchNo);
            mLICrmLogSchema.setBatchDate(mLogDate);
            mLICrmLogSchema.setFileCode(SingleData[0]);
            mLICrmLogSchema.setFileName(SingleData[2]);
            mLICrmLogSchema.setRecordNum(SingleData[4]);
            mLICrmLogSchema.setMakeDate(mToday);
            mLICrmLogSchema.setMakeTime(mTime);
            mLICrmLogSchema.setOperator("crm");
            mLICrmLogSet.add(mLICrmLogSchema);
        }
        mMap.put(mLICrmLogSet, "INSERT");
        if (!SubmitMap() )
              return false;
        return true;
    }


    /*数据处理*/
private boolean SubmitMap()
{
    PubSubmit tPubSubmit = new PubSubmit();
    mResult.add(mMap);
    if (!tPubSubmit.submitData(mResult, ""))
    {
        System.out.println("提交数据失败！");
        return false;
    }
    return true;
}
public String GetBatchNo()
{
    return mBatchNo;
}
private String FindLogFile(String Path)
{
     File  dir = new File(Path);
     Filter aFilter = new Filter("log");
     String filename ="";
     if (dir.list(aFilter).length == 0)
          return filename;
     String  files[] =  dir.list(aFilter);
     if (!StrTool.cTrim(files[0]).equals(""))
            filename = files[0];
     System.out.println("filename "+filename);
     return filename;
}

    public static void main(String[] args) throws Exception
    {
        CrmTranDataBL mCrmTranDataBL = new CrmTranDataBL();
        mCrmTranDataBL.submitData();
        String aBatch = mCrmTranDataBL.GetBatchNo();
        CrmTranOtherDataBL aCrmTranOtherDataBL = new CrmTranOtherDataBL();
        String tSql="select * from LICrmLog where batchno = '"+aBatch+"'";
        LICrmLogSet aLICrmLogSet = new LICrmLogSet();
        LICrmLogDB aLICrmLogDB = new LICrmLogDB();
        aLICrmLogSet=aLICrmLogDB.executeQuery(tSql);
        VData tVData = new VData();
        tVData.add(aLICrmLogSet);
             aCrmTranOtherDataBL.submitData(tVData);

    }

}
class Filter implements FilenameFilter{

    String extent;

    Filter(String extent){

        this.extent=extent;

    }

    public boolean accept(File dir,String name){

        return name.endsWith("."+extent); //返回文件的后缀名

    }

}
