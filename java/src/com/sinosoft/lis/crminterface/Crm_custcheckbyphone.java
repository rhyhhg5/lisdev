package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMElement;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_custcheckbyphone extends CRMAbstractInterface {
	/**
	 * 电话号
	 * */
	private static final String PHONE = "PHONE";
	private String mPhone = "";
	/**
	 * 投/被保人证件号
	 * */
	private static final String IDNO = "IDNO";
	private String mIDNo = "";

	public Crm_custcheckbyphone() {
		this.mRootName = "CRM_CUSTCHECKBYPHONE";
	}

	public boolean returnResponseBody() throws Exception {
		String tsqlid = "";
		String tsqphone = "";

		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PHONE.equals(tChildElement.getLocalName())) {
				mPhone = tChildElement.getText();
				if ((!"".equals(mPhone)) && mPhone != null) {
					tsqphone = " and (c.phone='" + mPhone + "' or c.mobile='"
							+ mPhone + "') ";
					continue;
				}
				//tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "F");
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("电话号、投/被保人证件号均不能为空");
				return false;
			}
			if (IDNO.equals(tChildElement.getLocalName())) {
				mIDNo = tChildElement.getText();
				if ((!"".equals(mIDNo)) && mIDNo != null) {
					tsqlid = " and b.idno='" + mIDNo + "' ";
					continue;
				}
				//tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "F");
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("电话号、投/被保人证件号均不能为空");
				return false;
			}
		}
		String sql = "select 'T' from lccont a,lcappnt b "
				+ "where a.appntno=b.appntno " + "and a.contno=b.contno "
				+ tsqlid
				+ "and exists (select 1 from lcaddress c where c.customerno=b.appntno and c.addressno=b.addressno "
				+ tsqphone
				+ ") union all select 'T' from lccont a,lbappnt b where a.appntno=b.appntno "
				+ "and a.contno=b.contno "
				+ tsqlid
				+ "and exists (select 1 from lcaddress c where c.customerno=b.appntno and c.addressno=b.addressno "
				+ tsqphone
				+ ") union all select 'T' from lbcont a,lcappnt b where a.appntno=b.appntno "
				+ "and a.contno=b.contno "
				+ tsqlid
				+ "and exists (select 1 from lcaddress c where c.customerno=b.appntno and c.addressno=b.addressno "
				+ tsqphone
				+ ") union all "
				+ "select 'T'  "
				+ "from lbcont a,lbappnt b "
				+ "where "
				+ "a.appntno=b.appntno "
				+ "and a.contno=b.contno "
				+ tsqlid
				+ "and exists (select 1 from lcaddress c where c.customerno=b.appntno and c.addressno=b.addressno "
				+ tsqphone
				+ ") union all "
				+ "select 'T'  "
				+ "from lccont a,lcinsured b "
				+ "where "
				+ "a.insuredno=b.insuredno "
				+ "and a.contno=b.contno "
				+ tsqlid
				+ "and exists (select 1 from lcaddress c where c.customerno=b.insuredno and c.addressno=b.addressno "
				+ tsqphone
				+ ") union all "
				+ "select 'T'  "
				+ "from lccont a,lbinsured b "
				+ "where "
				+ "a.insuredno=b.insuredno "
				+ "and a.contno=b.contno "
				+ tsqlid
				+ "and exists (select 1 from lcaddress c where c.customerno=b.insuredno and c.addressno=b.addressno "
				+ tsqphone
				+ ") union all "
				+ "select 'T'  "
				+ "from lbcont a,lcinsured b "
				+ "where "
				+ "a.insuredno=b.insuredno "
				+ "and a.contno=b.contno "
				+ tsqlid
				+ "and exists (select 1 from lcaddress c where c.customerno=b.insuredno and c.addressno=b.addressno "
				+ tsqphone
				+ ") union all "
				+ "select 'T'  "
				+ "from lbcont a,lbinsured b "
				+ "where "
				+ "a.insuredno=b.insuredno "
				+ "and a.contno=b.contno "
				+ tsqlid
				+ "and exists (select 1 from lcaddress c where c.customerno=b.insuredno and c.addressno=b.addressno "
				+ tsqphone + ") with ur";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tResult;
		tResult = tExeSQL.execSQL(sql);
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		if (tResult.getMaxRow() > 0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "T");
		} else {
			tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "F");
		}
		return true;
	}

}
