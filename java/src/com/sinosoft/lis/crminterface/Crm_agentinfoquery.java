package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMElement;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_agentinfoquery extends CRMAbstractInterface {

	/** 业务员代码 **/
	private static final String AGENTCODE = "AGENTCODE";
	private String mAgentcode = "";

	public Crm_agentinfoquery() {
		this.mRootName = "CRM_AGENTINFOQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (AGENTCODE.equals(tChildElement.getLocalName())) {
				mAgentcode = tChildElement.getText();
				
				if ((!"".equals(mAgentcode)) && mAgentcode != null) {
					AgentCodeTransformation a = new AgentCodeTransformation();
					if(a.AgentCode(mAgentcode, "Y")){
						System.out.println(a.getMessage());
						mErrCode = ERRCODE_CHECK;
						mErrors.addOneError(a.getMessage());
						return false;
					}
					mAgentcode=a.getResult();
					continue;
				}
			
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("业务员代码不能为空");
				return false;
			}
		}
		String sql = "Select name,sex,Degree,idnotype,NativePlace,Nationality,"
				+"(select (select codename from ldcode where codetype='branchtype' and code = latree.branchtype) ||"
				+ "(select codename from ldcode where codetype='branchtype2' and code=latree.branchtype2) 销售渠道 "
				+ " from latree where agentcode =laagent.agentcode ),"
				+"ZipCode,agentcode,branchtype,birthday,"
				+ "idno,employdate,RgtAddress,"
				+ "(Select agentgrade from latree where agentcode =laagent.agentcode),"
				+ "agentstate,agentgroup,phone,PolityVisage,outworkdate,"
				+ "indueformdate,Marriage,EMail,managecom,"
				+ "(Select certifno from lacertification where agentcode = laagent.agentcode)"
				+ "  from laagent where 1=1 and AgentCode='" + mAgentcode
				+ "' with ur ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sResult;
		sResult = tExeSQL.execSQL(sql);
		if (sResult.getMaxRow() > 0) {
			LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
			tLoginVerifyTool.addOm(mResponseBodyElement, "AGENTNAME",
					sResult.GetText(1, 1));
			tLoginVerifyTool.addOm(mResponseBodyElement, "SEX",
					sResult.GetText(1, 2));
			tLoginVerifyTool.addOm(mResponseBodyElement, "DEGREE",
					sResult.GetText(1, 3));
			tLoginVerifyTool.addOm(mResponseBodyElement, "IDTYPE",
					sResult.GetText(1, 4));
			tLoginVerifyTool.addOm(mResponseBodyElement, "NATIVEPLACE",
					sResult.GetText(1, 5));
			tLoginVerifyTool.addOm(mResponseBodyElement, "NATIONALITY",
					sResult.GetText(1, 6));
			tLoginVerifyTool.addOm(mResponseBodyElement, "SALECHNL",
					sResult.GetText(1, 7));
			tLoginVerifyTool.addOm(mResponseBodyElement, "ZIPCODE",
					sResult.GetText(1, 8));
			tLoginVerifyTool.addOm(mResponseBodyElement, "AGENTCODE",
					sResult.GetText(1, 9));
			tLoginVerifyTool.addOm(mResponseBodyElement, "BRANCHTYPE",
					sResult.GetText(1, 10));
			tLoginVerifyTool.addOm(mResponseBodyElement, "BIRTHDAY",
					sResult.GetText(1, 11));
			tLoginVerifyTool.addOm(mResponseBodyElement, "IDNO",
					sResult.GetText(1, 12));
			tLoginVerifyTool.addOm(mResponseBodyElement, "EMPLOYDATE",
					sResult.GetText(1, 13));
			tLoginVerifyTool.addOm(mResponseBodyElement, "RGTADDRESS",
					sResult.GetText(1, 14));
			tLoginVerifyTool.addOm(mResponseBodyElement, "SALELEVEL",
					sResult.GetText(1, 15));
			tLoginVerifyTool.addOm(mResponseBodyElement, "AGENTSTATE",
					sResult.GetText(1, 16));
			tLoginVerifyTool.addOm(mResponseBodyElement, "AGENTGROUP",
					sResult.GetText(1, 17));
			tLoginVerifyTool.addOm(mResponseBodyElement, "PHONE",
					sResult.GetText(1, 18));
			tLoginVerifyTool.addOm(mResponseBodyElement, "POLITVISAGE",
					sResult.GetText(1, 19));
			tLoginVerifyTool.addOm(mResponseBodyElement, "OUTWORKDATE",
					sResult.GetText(1, 20));
			tLoginVerifyTool.addOm(mResponseBodyElement, "INDUEFORMDATE",
					sResult.GetText(1, 21));
			tLoginVerifyTool.addOm(mResponseBodyElement, "MARRIAGE",
					sResult.GetText(1, 22));
			tLoginVerifyTool.addOm(mResponseBodyElement, "EMAIL",
					sResult.GetText(1, 23));
			tLoginVerifyTool.addOm(mResponseBodyElement, "MANAGECOM",
					sResult.GetText(1, 24));
			tLoginVerifyTool.addOm(mResponseBodyElement, "CERTIFNO",
					sResult.GetText(1, 25));
		}
		return true;
	}
}
