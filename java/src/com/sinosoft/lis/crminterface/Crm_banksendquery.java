package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_banksendquery extends CRMAbstractInterface{
	
	/**当前页码**/
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/**每页记录条数**/
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/**保单号	**/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/**发盘起期	**/
	private static final String SENDSTARTDATE = "SENDSTARTDATE";
	private String mSendstartdate = "";
	/**发盘止期	**/
	private static final String SENDENDDATE = "SENDENDDATE";
	private String mSendenddate = "";
	/**管理机构	**/
	private static final String MANAGECOM = "MANAGECOM";
	private String mManagecom = "";
	/**是否成功	**/
	private static final String SUCCFLAG = "SUCCFLAG";
	private String mSuccflag = "";
	/**业务类型	**/
	private static final String NOTYPE = "NOTYPE";
	private String mNotype = "";
	/**业务员代码	**/
	private static final String AGENTCODE = "AGENTCODE";
	private String mAgentcode = "";
	/**账户名	**/
	private static final String ACCNAME = "ACCNAME";
	private String mAccname = "";
	/**银行代码	**/
	private static final String BANKCODE = "BANKCODE";
	private String mBankcode = "";
	/**流水号	**/
	private static final String SERIALNO = "SERIALNO";
	private String mSerialno = "";
		
	public Crm_banksendquery() {
		this.mRootName = "CRM_BANKSENDQUERY";
	}
	
	public boolean returnResponseBody() throws Exception {
		String tsql = "";
        String tflag="";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()){
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if (mPageindex!=null&&!"".equals(mPageindex)) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码、每页记录条数均不能为空");
				return false;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if (mPagesize!=null&&!"".equals(mPagesize)) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码、每页记录条数均不能为空");
				return false;
			}
			if (SENDSTARTDATE.equals(tChildElement.getLocalName())) {
				mSendstartdate = tChildElement.getText();
				if ((!"".equals(mSendstartdate)) && mSendstartdate != null) {
                    tsql += " and senddate >= '" + mSendstartdate + "' ";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("发盘起期、止期、管理机构、是否成功、业务类型为必录项");
				return false;				
			}
			if (SENDENDDATE.equals(tChildElement.getLocalName())) {
				mSendenddate = tChildElement.getText();
				if ((!"".equals(mSendenddate)) && mSendenddate != null) {
                    tsql += " and senddate <= '" + mSendenddate + "' ";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("发盘起期、止期、管理机构、是否成功、业务类型为必录项");
				return false;				
			}
			if (MANAGECOM.equals(tChildElement.getLocalName())) {
				mManagecom = tChildElement.getText();
				if ((!"".equals(mManagecom)) && mManagecom != null) {
                    tsql += " and c.Managecom = '" + mManagecom + "' ";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("发盘起期、止期、管理机构、是否成功、业务类型为必录项");
				return false;				
			}
			if (SUCCFLAG.equals(tChildElement.getLocalName())) {
				mSuccflag = tChildElement.getText();
				if ((!"".equals(mSuccflag)) && mSuccflag != null) {
                    tflag += " and succflag = '" + mSuccflag + "' ";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("发盘起期、止期、管理机构、是否成功、业务类型为必录项");
				return false;				
			}
			if (NOTYPE.equals(tChildElement.getLocalName())) {
				mNotype = tChildElement.getText();
				if ((!"".equals(mNotype)) && mNotype != null) {
                    //tsql += " and c.Managecom = '" + mSuccflag + "' ";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("发盘起期、止期、管理机构、是否成功、业务类型为必录项");
				return false;				
			}
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
                    tsql += " and c.contno = '" + mContno + "' ";
				}
				continue;
			}
			if (AGENTCODE.equals(tChildElement.getLocalName())) {
				mAgentcode = tChildElement.getText();
				if ((!"".equals(mAgentcode)) && mAgentcode != null) {
					tsql += " and c.AgentCode='"+mAgentcode+"'";
				}
				continue;
			}
			if (ACCNAME.equals(tChildElement.getLocalName())) {
				mAccname = tChildElement.getText();
				if ((!"".equals(mAccname)) && mAccname != null) {
                    tsql += " and c.AccName='"+mAgentcode+"'";
				}
				continue;
			}
			if (BANKCODE.equals(tChildElement.getLocalName())) {
				mBankcode = tChildElement.getText();
				if ((!"".equals(mBankcode)) && mBankcode != null) {
					tsql += " and c.BankCode='"+mBankcode+"'";
				}
				continue;
			}
			if (SERIALNO.equals(tChildElement.getLocalName())) {
				mSerialno = tChildElement.getText();
				if ((!"".equals(mSerialno)) && mSerialno != null) {
					tsql += "and s.Serialno='"+mSerialno+"'";
				}
				continue;
			}	
		}
		if (("".equals(mContno) || mContno == null)
				&& ("".equals(mAgentcode) || mAgentcode == null)
				&& ("".equals(mAccname) || mAccname == null)
				&& ("".equals(mBankcode) || mBankcode == null)
				&& ("".equals(mSerialno) || mSerialno == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("保单号、业务员代码、账户名、银行代码、流水号必录其一");
			return false;
			}
        
        String sql="select Managecom ,Contno,Cvalidate,Appntname,mobile,senddate,returndate,name,paymoney,paydates,paydatet,Accname,bankname,succflag,agentcode,mobile2,faileinfo,name2 from (select c.Managecom, "+
        " c.Contno, "+
        " c.Cvalidate, "+
        " c.Appntname, "+
        " (select mobile from lcaddress where addressno=a.addressno and customerno = a.appntno)  mobile, "+
        " (select senddate from lybanklog where serialno=(select serialno from lyreturnfrombank where paycode=j.tempfeeno fetch first 1 rows only)) senddate, "+
        " (select returndate from lybanklog where serialno=(select serialno from lyreturnfrombank where paycode=j.tempfeeno fetch first 1 rows only)) returndate, "+
        " (select name from Labranchgroup where agentgroup=c.agentgroup) name "+
        " j.paymoney paymoney , "+
        " s.paydate paydates, j.paydate paydatet , "+
        " c.Accname Accname, "+
        " (select bankname from ldbank where bankcode=c.bankcode) bankname, "+
        " case (select agentgetsuccflag from ldbank where bankcode= '7705') when '0000' then '1' else '0' end succflag , "+
        " c.agentcode agentcode, "+
        " (select mobile from laagent where agentcode=c.agentcode and agentgroup =c.agentgroup) mobile2, "+
        " '' faileinfo, "+
        " (select name from laagent where agentcode = c.agentcode and agentgroup =c.agentgroup) name2"+
        "  from lccont c,lcappnt a,ljtempfee j,ljspay s   "+
         " where  "+
        " 1=1 "+
        " "+tsql +""+
        " c.prtno=a.prtno and c.prtno = j.otherno "+
        " and s.getnoticeno = j.tempfeeno ) " +
        " where 1=1 " +
        " "+tflag+"" +
        " with ur";
		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
	    sql = "select " + sql;
		
		String numlsql = "select count(*) from ("+sql+") con with ur ";
		SSRS numResult;
		numResult = tExeSQL.execSQL(numlsql);
		if (numResult.getMaxRow()>0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
					numResult.GetText(1, 1));
		}
		String sendbanksql = "select * from (select c.*,rownumber() over() as rn from (" + sql
				+ ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		SSRS sendbankResult;
		sendbankResult = tExeSQL.execSQL(sendbanksql);
		if (sendbankResult.getMaxRow()>0) {
			OMElement proposallist = tOMFactory.createOMElement("SENDBANKLIST", null);
			for (int i = 1; i <= sendbankResult.getMaxRow(); i++) {
				OMElement proposaldata = tOMFactory.createOMElement("SENDBANKDATA", null);
				tLoginVerifyTool.addOm(proposaldata, "MANAGECOM",
						sendbankResult.GetText(i, 1));
				tLoginVerifyTool.addOm(proposaldata, "CONTNO",
						sendbankResult.GetText(i, 2));
				tLoginVerifyTool.addOm(proposaldata, "CVALIDATE",
						sendbankResult.GetText(i, 3));
				tLoginVerifyTool.addOm(proposaldata, "APPNTNAME",
						sendbankResult.GetText(i, 4));
				tLoginVerifyTool.addOm(proposaldata, "PHONE",
						sendbankResult.GetText(i, 5));
				tLoginVerifyTool.addOm(proposaldata, "SENDDATE",
						sendbankResult.GetText(i, 6));
				tLoginVerifyTool.addOm(proposaldata, "BANKDEALDATE",
						sendbankResult.GetText(i, 7));
				tLoginVerifyTool.addOm(proposaldata, "BRANCHNAME",
						sendbankResult.GetText(i, 8));
				tLoginVerifyTool.addOm(proposaldata, "PAYMONDEY",
						sendbankResult.GetText(i, 9));
				tLoginVerifyTool.addOm(proposaldata, "LASTPAYTODATE",
						sendbankResult.GetText(i, 10));
				tLoginVerifyTool.addOm(proposaldata, "PAYDATE",
						sendbankResult.GetText(i, 11));
				tLoginVerifyTool.addOm(proposaldata, "ACCNAME",
						sendbankResult.GetText(i, 12));
				tLoginVerifyTool.addOm(proposaldata, "BANKNAME",
						sendbankResult.GetText(i, 13));
				tLoginVerifyTool.addOm(proposaldata, "SUCCFLAG",
						sendbankResult.GetText(i, 14));
				tLoginVerifyTool.addOm(proposaldata, "AGENTCODE",
						sendbankResult.GetText(i, 15));
				tLoginVerifyTool.addOm(proposaldata, "AGENTPHONE",
						sendbankResult.GetText(i, 16));
				tLoginVerifyTool.addOm(proposaldata, "FAILREASON",
						sendbankResult.GetText(i, 17));
				tLoginVerifyTool.addOm(proposaldata, "AGENTNAME",
						sendbankResult.GetText(i, 18));
				proposallist.addChild(proposaldata);	
			}
			mResponseBodyElement.addChild(proposallist);
		}
		return true;
	}
}
