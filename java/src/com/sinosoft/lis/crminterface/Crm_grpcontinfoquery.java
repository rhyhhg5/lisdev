package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_grpcontinfoquery extends CRMAbstractInterface {
	/** 保单号 **/
	private static final String GRPCONTNO = "GRPCONTNO";
	private String mGrpContno = "";

	public Crm_grpcontinfoquery() {
		this.mRootName = "CRM_GRPCONTINFOQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (GRPCONTNO.equals(tChildElement.getLocalName())) {
                mGrpContno = tChildElement.getText();	
				if (("".equals(mGrpContno)) || mGrpContno == null) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("团体保单号不能为空");
			return false;
		}
			}
		}
		
		

		String contdatasql = "select lcc.grpcontno 保单号,lcc.GrpName 单位名称, "+
                             " lcc.BusinessType 行业分类,lcc.GrpNature 单位性质, "+
                             " lcc.RgtMoney 注册资本,lcc.Asset 资产总额, "+
                             " lcc.Fax 单位传真,lcd.LinkMan1 联系人1姓名,lcd.LinkMan2 联系人1姓名, "+
                             " lcd.Phone1 联系人1电话,lcd.Phone2 联系人2电话, lcd.E_Mail1 联系人1邮箱,lcd.E_Mail2 联系人2邮箱, "+
                             " lcd.GrpAddress 单位地址,lcd.GrpZipCode 单位邮政编码,(select name from lacom where agentcom=lcc.agentcom) 销售机构名称, "+
                             " (select address from lacom where agentcom=lcc.agentcom)销售机构地址, "+
                             " lcc.cvalidate 生效日期,lcc.CInValiDate 终止日期,lcc.Peoples2 投保人数,lcc.RelaPeoples 连带投保人数,lcc.StateFlag 团体保单状态 "+
                             " from lcgrpcont lcc,lcgrpaddress lcd "+
                             " where lcc.grpcontno='"+mGrpContno+"' "+
                             " and lcc.appntno=lcd.customerno "+
                             " and lcc.addressno=lcd.addressno "+
                             " with ur ";


		String riskcodesql = "select lcp.riskcode 险种代码, "+
                            " (select riskname from lmriskapp where riskcode=lcp.riskcode) 险种名称, "+
                            " (select riskshortname from lmrisk where riskcode=lcp.riskcode) 代码简称, "+
                            " lcp.prem 保费, "+
                            " lcp.amnt 保额 "+
                            " from lcgrppol lcp "+
                            " where grpcontno='"+mGrpContno+"'";

        String dutydatasql = "select distinct (select DutyName from lmduty where dutycode=lcd.DutyCode) 责任名称, "+
                            " lcd.dutycode 责任编码, "+
                              " lcp.riskcode 所属险种代码  "+
                              " from lcduty lcd,lcpol lcp "+
                              " where lcp.grpcontno='"+mGrpContno+"' "+
                              " and lcp.polno=lcd.polno ";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS contdataResult;
		SSRS riskcodeResult;
        SSRS dutyResult;

		contdataResult = tExeSQL.execSQL(contdatasql);
        
		riskcodeResult = tExeSQL.execSQL(riskcodesql);

		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		if (contdataResult.getMaxRow() > 0) {
			OMElement contdate = tOMFactory.createOMElement("GRPCONTDATA", null);
			tLoginVerifyTool.addOm(contdate, "GRPCONTNO",
					contdataResult.GetText(1, 1));
			tLoginVerifyTool.addOm(contdate, "GRPNAME",
					contdataResult.GetText(1, 2));
			tLoginVerifyTool.addOm(contdate, "BUSINESSTYPE",
					contdataResult.GetText(1, 3));
			tLoginVerifyTool.addOm(contdate, "GRPNATURE",
					contdataResult.GetText(1, 4));
			tLoginVerifyTool.addOm(contdate, "RGTMONEY",
					contdataResult.GetText(1, 5));
			tLoginVerifyTool.addOm(contdate, "ASSET",
					contdataResult.GetText(1, 6));
			tLoginVerifyTool.addOm(contdate, "FAX",
					contdataResult.GetText(1, 7));
			tLoginVerifyTool.addOm(contdate, "LINKMAN1",
					contdataResult.GetText(1, 8));
			tLoginVerifyTool.addOm(contdate, "LINKMAN2",
					contdataResult.GetText(1, 9));
			tLoginVerifyTool.addOm(contdate, "PHONE1",
					contdataResult.GetText(1, 10));
			tLoginVerifyTool.addOm(contdate, "PHONE2",
					contdataResult.GetText(1, 11));
			tLoginVerifyTool.addOm(contdate, "EMAIL1",
					contdataResult.GetText(1, 12));
			tLoginVerifyTool.addOm(contdate, "EMAIL2",
					contdataResult.GetText(1, 13));
			tLoginVerifyTool.addOm(contdate, "GRPADDRESS",
					contdataResult.GetText(1, 14));
			tLoginVerifyTool.addOm(contdate, "GRPZIPCODE",
					contdataResult.GetText(1, 15));
			tLoginVerifyTool.addOm(contdate, "BRANCHNAME",
					contdataResult.GetText(1, 16));
			tLoginVerifyTool.addOm(contdate, "BRANCHADDRESS",
					contdataResult.GetText(1, 17));
			tLoginVerifyTool.addOm(contdate, "CVALIDATE",
					contdataResult.GetText(1, 18));
			tLoginVerifyTool.addOm(contdate, "ENDDATE",
					contdataResult.GetText(1, 19));
			tLoginVerifyTool.addOm(contdate, "PEOPLES",
					contdataResult.GetText(1, 20));
			tLoginVerifyTool.addOm(contdate, "RELAMATEPEOPLES",
					contdataResult.GetText(1, 21));
			tLoginVerifyTool.addOm(contdate, "STATEFLAG",
					contdataResult.GetText(1, 22));
			
			mResponseBodyElement.addChild(contdate);
		}
		if (riskcodeResult.getMaxRow() > 0) {
            OMElement risklist = tOMFactory.createOMElement("RISKLIST", null);
            for(int j=1;j<=riskcodeResult.MaxRow;j++){
                OMElement crsdata = tOMFactory.createOMElement("RISKDATA", null);
                tLoginVerifyTool.addOm(crsdata, "RISKCODE",
                        riskcodeResult.GetText(1, 1));
                tLoginVerifyTool.addOm(crsdata, "RISKNAME",
                        riskcodeResult.GetText(1, 2));
                tLoginVerifyTool.addOm(crsdata, "RISKSHORTNAME",
                        riskcodeResult.GetText(1, 3));
                tLoginVerifyTool.addOm(crsdata, "PREM",
                        riskcodeResult.GetText(1, 4));
                tLoginVerifyTool.addOm(crsdata, "AMNT",
                        riskcodeResult.GetText(1, 5));
                
                dutydatasql =dutydatasql+"  and lcp.riskcode='"+riskcodeResult.GetText(1, 1)+"' ";
                dutyResult = tExeSQL.execSQL(dutydatasql);
                
                System.out.println("责任数:"+dutyResult.MaxRow);
                if (dutyResult.getMaxRow() > 0) {
                    OMElement dutylist = tOMFactory.createOMElement("DUTYLIST", null);
                    for (int i = 1; i <= dutyResult.MaxRow; i++) {
                        OMElement dutydata = tOMFactory.createOMElement("DUTYDATA",
                                null);
                        tLoginVerifyTool.addOm(dutydata, "DUTYNAME",
                                dutyResult.GetText(i, 1));
                        tLoginVerifyTool.addOm(dutydata, "DUTYCODE",
                                dutyResult.GetText(i, 2));
                        tLoginVerifyTool.addOm(dutydata, "RISKCODE",
                                dutyResult.GetText(i, 3));
                        
                        dutylist.addChild(dutydata);
                }
                    crsdata.addChild(dutylist);
               }
                
                risklist.addChild(crsdata); 
            }
			
            mResponseBodyElement.addChild(risklist);
		}
		
			
		return true;
	}
}
