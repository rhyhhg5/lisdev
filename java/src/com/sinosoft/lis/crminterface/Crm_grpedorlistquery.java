package com.sinosoft.lis.crminterface;
//团体保全
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_grpedorlistquery extends CRMAbstractInterface
{

    /** 当前页码 **/
    private static final String PAGEINDEX = "PAGEINDEX";

    private String mPageindex = "";

    private int mPagein = 0;

    /** 每页记录条数 **/
    private static final String PAGESIZE = "PAGESIZE";

    private String mPagesize = "";

    private int mPagesi = 0;

    /** 团体保单号 **/
    private static final String GRPCONTNO = "GRPCONTNO";

    private String grpcontno = "";

    private String mEdorInfo = "";

    /**初始化赋值类名**/
    public Crm_grpedorlistquery()
    {
        this.mRootName = "CRM_GRPEDORLISTQUERY";
    }

    /**返回输出**/
    public boolean returnResponseBody() throws Exception
    {

        String sql = "";
        String tsql = "";

        /**实例化迭代器,循环校验数据**/
        Iterator tBodyIterator = mBodyElement.getChildren();
        while (tBodyIterator.hasNext())
        {
            OMElement tChildElement = (OMElement) tBodyIterator.next();
            if (PAGEINDEX.equals(tChildElement.getLocalName()))
            {
                mPageindex = tChildElement.getText();
                if ((!"".equals(mPageindex)) && mPageindex != null)
                {
                    mPagein = Integer.parseInt(mPageindex);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码和每页记录条数为必录项");
                return false;
            }

            if (PAGESIZE.equals(tChildElement.getLocalName()))
            {
                mPagesize = tChildElement.getText();
                if ((!"".equals(mPagesize)) && mPagesize != null)
                {
                    mPagesi = Integer.parseInt(mPagesize);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码和每页记录条数为必录项");
                return false;
            }

            if (GRPCONTNO.equals(tChildElement.getLocalName()))
            {
                grpcontno = tChildElement.getText();
                if ((!"".equals(grpcontno)) && grpcontno != null)
                {
                    tsql = tsql + " and b.GRPCONTNO='" + grpcontno + "' ";
                }
                continue;
            }
        }

        /**校验**/
        if (("".equals(grpcontno) || grpcontno == null))
        {
            mErrCode = ERRCODE_CHECK;
            mErrors.addOneError("团体保单号必录其一");
            return false;
        }

        /**根据条件查询处结果**/
        sql = "select a.workno as 保全批单号," + " a.customername as 申请人姓名," + " a.acceptdate as 申请日期,"
                + " b.edorvalidate as 生效日期,"
                + " (select code from ldcode where codetype = 'reason_tb' and code=b.reasoncode) as 保全变更原因,"
                + " (select code from ldcode where codetype='applytypeno' and code =a.applytypeno) as 申请人类型,"
                + " (select code from ldcode where codetype='appedorstate' and code=("
                + " select edorstate from lpedorapp where edoracceptno=b.edoracceptno)) as 处理状态,"
                + " (select edorinfo from LPEdorAppPrint where edoracceptno = b.edoracceptno) as 保全批单内容"
                + " from lgwork a left join lpgrpedoritem b on a.workno=b.edoracceptno" + " where 1=1 " + tsql
                + " with ur ";

        /**实例化ExeSQL**/
        ExeSQL tExeSQL = new ExeSQL();
        String totalnum = "select count(*) from (" + sql + ") cou with ur";

        /**实例化结果集SSRS**/
        SSRS tResult;
        SSRS ttotal = tExeSQL.execSQL(totalnum);
        LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

        /**加入到body标签下*/
        tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM", ttotal.GetText(1, 1));

        /**根据已查询的结果进行分页查询处结果**/
        sql = "select * from (select c.*,rownumber() over() as rn from (" + sql + ") c) d where d.rn>"
                + ((mPagein - 1) * mPagesi) + " and d.rn<=" + (mPagein * mPagesi) + " with ur ";

        /**对结果集进行遍历赋值**/
        tResult = tExeSQL.execSQL(sql);
        if (tResult.getMaxRow() > 0)
        {
            OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
            tOMFactory = OMAbstractFactory.getOMFactory();
            int trow = tResult.MaxRow;
            System.out.println(trow);

            /**集合集合父标签*/
            OMElement tcustomerlist = tOMFactory.createOMElement("EDORLIST", null);
            for (int i = 1; i < trow; i++)
            {
                /**创建集合标签*/
                OMElement tcustomer = tOMFactory.createOMElement("EDORDATA", null);

                /**创建集合子标签并赋值*/
                tLoginVerifyTool.addOm(tcustomer, "EDORACCEPTNO", tResult.GetText(i, 1));
                tLoginVerifyTool.addOm(tcustomer, "APPLYNAME", tResult.GetText(i, 2));
                tLoginVerifyTool.addOm(tcustomer, "APPLYDATE", tResult.GetText(i, 3));
                tLoginVerifyTool.addOm(tcustomer, "EDORVALIDATE", tResult.GetText(i, 4));
                tLoginVerifyTool.addOm(tcustomer, "EDORTYPE", tResult.GetText(i, 5));
                tLoginVerifyTool.addOm(tcustomer, "APPLYTYPE", tResult.GetText(i, 6));
                tLoginVerifyTool.addOm(tcustomer, "EDORSTATE", tResult.GetText(i, 7));

                if (getEdorinfo(tResult.GetText(i+1, 1)))
                {
                    tLoginVerifyTool.addOm(tcustomer, "CONTENT", mEdorInfo);
                }

                /**循环一次加入一次*/
                tcustomerlist.addChild(tcustomer);
            }
            /**加入到body标签下*/
            mResponseBodyElement.addChild(tcustomerlist);
        }

        return true;
    }

    /**
     * 查询字段类型是 blob 的批单内容
     * @param mEdorNo
     * @return
     */
    private boolean getEdorinfo(String mEdorNo)
    {
        System.out.println("----开始查询保全内容-----");
        InputStream ins = null;
        String sql = "select * from LPEDORPRINT " + "where EdorNo = '" + mEdorNo + "' ";
        Connection conn = DBConnPool.getConnection();

        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next())
            {
                Blob tBlob = rs.getBlob("EdorInfo");
                ins = tBlob.getBinaryStream();
                mEdorInfo = convertStreamToString(ins);
                System.out.println("保全内容:" + mEdorInfo);
            }
            else
            {
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("没有查询到批单信息");
                return false;
            }

            rs.close();
            stmt.close();
            conn.close();
        }
        catch (Exception e)
        {
            mErrCode = ERRCODE_CHECK;
            mErrors.addOneError("查询批单信息出错");
            return false;
        }
        System.out.println("----保全内容查询完毕-----");
        return true;
    }

    /**
     *  InputStream to String 
     * @param is
     * @return 
     */
    public static String convertStreamToString(InputStream is)
    {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

}
