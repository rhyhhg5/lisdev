package com.sinosoft.lis.crminterface;

import com.sinosoft.lis.operfee.IndiPhoneVisitSaveBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LICrmChangePaySet;
import com.sinosoft.lis.vschema.LICrmRenewalPaySet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LGPhoneHastenDB;
import com.sinosoft.lis.vschema.LGPhoneHastenSet;
import com.sinosoft.lis.db.LICrmChangePayDB;
import com.sinosoft.task.TaskHastenUI;


/**
 * <p>Title: 保全,续期催缴处理</p>
 *
 * <p>Description: 保全,续期催缴处理</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author xunzl
 * @version 1.0
 */
public class BQPayInterfaceBL {
    public BQPayInterfaceBL() {
    }

    /** 传入参数 */
    private VData mInputData;

    /** 传入操作符 */
    private String mOperate;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 报错存储对象 */
    public CErrors mErrors = new CErrors();
    /** 最后保存结果 */
    private VData mResult = new VData();
    /** 最后递交Map */
    private MMap map = new MMap();

    private LICrmRenewalPaySet mLICrmRenewalPaySet;
    private LICrmChangePaySet mLICrmChangePaySet;
    private LICrmChangePaySchema mLICrmChangePaySchema;
    private LICrmRenewalPaySchema mLICrmRenewalPaySchema;

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        System.out.println("into BriefCardSignBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("BriefCardSignBL finished...");
        return true;
    }

    /**
     * 获取中间表数据
     * 其中包含 续期\电话回访
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("into BriefCardSignBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);

        mLICrmChangePaySet = (LICrmChangePaySet) mInputData.
                             getObjectByObjectName("LICrmChangePaySet", 0);

        mLICrmRenewalPaySet = (LICrmRenewalPaySet) mInputData.
                              getObjectByObjectName("LICrmRenewalPaySet", 0);

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("into BriefCardSignBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }
        /**
         *  完成针对业务类型的校验(中间表)
         */
        if (this.mOperate.equalsIgnoreCase("ChangePayListFeed")) {
            if (mLICrmChangePaySet == null || mLICrmChangePaySet.size() <= 0) {
                return false;
            }
        } else if (this.mOperate.equalsIgnoreCase("RenewalPayListFeed")) {
            if (mLICrmRenewalPaySet == null || mLICrmRenewalPaySet.size() <= 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * dealData
     * @ 1\完成业务数据的封装,2\调用业务类完成业务逻辑.3\记录操作状态
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("into BriefCardSignBL.dealData()...");
        if (this.mOperate.equalsIgnoreCase("ChangePayListFeed")) {
            if (!dealChangePayData()) {
                return false;
            }
        } else if (this.mOperate.equalsIgnoreCase("RenewalPayListFeed")) {
            if (!dealRenewalPayData()) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * 增量处理续期电话催缴
     * @return boolean
     */
    private boolean dealRenewalPayData() {

        String tOperatorType = "1"; // 必须的
        boolean isRenewalPay = true;
        for (int i = 1; i <= this.mLICrmRenewalPaySet.size(); i++) {
            System.out.println("do insertRenewalData");
            LICrmRenewalPaySchema tLICrmRenewalPaySchema = (
                    LICrmRenewalPaySchema)
                    mLICrmRenewalPaySet.get(i);
            /**
             * 准备需要得参数
             * 1\tPayDate  约定下次缴费日期
             * 2\tPayMode\tOldBankCode\tOldBackAccNo\tWorkNo 在LGPhoneHasten中使用tGetNoticeNo查询
             * 3\tGetNoticeNo 通知书号码
             **/
            String tGetNoticeNo = tLICrmRenewalPaySchema.getGetNoticeno();
            LGPhoneHastenSchema tLGPhoneHastenSchema = getLGPhoneHastenSchema(
                    isRenewalPay, tGetNoticeNo);
            tLGPhoneHastenSchema.setOldPayDate(tLGPhoneHastenSchema.
                                               getOldPayDate());
            tLGPhoneHastenSchema.setGetNoticeNo(tGetNoticeNo);

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("OperatorType", tOperatorType);

            VData data = new VData();
            data.addElement(tLGPhoneHastenSchema);
            data.addElement(this.mGlobalInput);
            data.addElement(tTransferData);

            /**
             * 调用 IndiPhoneVisitSaveBL 完成业务逻辑
             */
            IndiPhoneVisitSaveBL tIndiPhoneVisitSaveBL = new
                    IndiPhoneVisitSaveBL();
            boolean tReturn = tIndiPhoneVisitSaveBL.submitData(data, "");
            /**
             * 记录操作状态 (IndiPhoneVisitSaveBL得返回值)
             * 将状态记录中间表
             */
            if (!updateState(isRenewalPay, i, tReturn)) {
                return false;
            }
        }
        /** 封装中间表数据,进行更新 */
        this.map.put(mLICrmRenewalPaySet, "UPDATE");
        return true;
    }

    private boolean updateState(boolean isRenewalPay, int i, boolean tReturn) {
        if (isRenewalPay) {
            if (tReturn) {
                this.mLICrmRenewalPaySet.get(i).setState("1");
            } else {
                this.mLICrmRenewalPaySet.get(i).setState("0");
            }
        } else {
            if (tReturn) {
                this.mLICrmChangePaySet.get(i).setState("1");
            } else {
                this.mLICrmChangePaySet.get(i).setState("0");
            }

        }
        return true;
    }

    /**
     *  获得 LGPhoneHastenSchema
     */
    private LGPhoneHastenSchema getLGPhoneHastenSchema(boolean isRenewalPay,
            String tGetNoticeNo) {
        LGPhoneHastenDB tLGPhoneHastenDB = new LGPhoneHastenDB();
        if (isRenewalPay) {
            tLGPhoneHastenDB.setGetNoticeNo(tGetNoticeNo);
        } else {
            tLGPhoneHastenDB.setWorkNo(tGetNoticeNo);
        }
        LGPhoneHastenSet tLGPhoneHastenSet = tLGPhoneHastenDB.query();
        if (tLGPhoneHastenSet != null && tLGPhoneHastenSet.size() > 0) {
            return tLGPhoneHastenSet.get(1);
        }
        return null;
    }


    /**IndiPhoneVisitSaveBL
     * 增量处理保全电话催缴
     * @return boolean
     */
    private boolean dealChangePayData() {

        boolean isRenewalPay = false;
        for (int i = 1; i <= mLICrmChangePaySet.size(); i++) {
            LICrmChangePaySchema tLICrmChangePaySchema = (LICrmChangePaySchema)
                    mLICrmChangePaySet.
                    get(i);
            String tEndorseNo = tLICrmChangePaySchema.getEndorseno();
            LGPhoneHastenSchema tLGPhoneHastenSchema = getLGPhoneHastenSchema(
                    false, tEndorseNo);

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("workNo", tEndorseNo);
            tTransferData.setNameAndValue("payDate",
                                          tLGPhoneHastenSchema.getOldPayDate());
            tTransferData.setNameAndValue("payMode",
                                          tLGPhoneHastenSchema.getOldPayMode());
            tTransferData.setNameAndValue("edorAcceptNo", tEndorseNo);
            String remark = "自动批注：CRM传入!";
            tTransferData.setNameAndValue("remark", remark);

            // 准备传输数据 VData
            VData data = new VData();
            data.add(tTransferData);
            data.add(this.mGlobalInput);

            /**
             * 调用 IndiPhoneVisitSaveBL 完成业务逻辑
             */
            TaskHastenUI tTaskHastenUI = new TaskHastenUI();
            boolean tReturn = tTaskHastenUI.submitData(data, "");
            /**
             * 记录操作状态 (IndiPhoneVisitSaveBL得返回值)
             * 将状态记录中间表
             */
            if (!updateState(isRenewalPay, i, tReturn)) {
                return false;
            }
        }
        /** 封装中间表数据,进行更新 */
        this.map.put(mLICrmChangePaySet, "UPDATE");
        return true;

    }


    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("into BriefCardSignBL.prepareOutputData()...");
        this.mResult.add(map);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefCardSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {

        BQPayInterfaceBL tBQPayInterfaceBL = new BQPayInterfaceBL();
        VData data = new VData();
        //   String operate = "RenewalPayListFeed";
        String operate = "ChangePayListFeed";
        LICrmChangePaySchema tLICrmChangePaySchema = new LICrmChangePaySchema();
        tLICrmChangePaySchema.setBatchNo("20060301000009");
        tLICrmChangePaySchema.setEndorseno("0000019604");
        tLICrmChangePaySchema.setNextPaydate("2006-11-11");
        tLICrmChangePaySchema.setResult("1");

        boolean flag = tBQPayInterfaceBL.submitData(data, operate);
        System.out.println("the return is:" + flag);

    }

}
