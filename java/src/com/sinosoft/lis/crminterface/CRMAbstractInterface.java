package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.lis.crminterface.utility.CRMMessageSave;
import com.sinosoft.lis.crminterface.utility.DateUtil;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

public abstract class CRMAbstractInterface {

	public CRMAbstractInterface() {

	}

	public String mRootName = "";
	public String mBatchNo = "";
	public OMElement mRootElement = null;
	public OMElement mHeadElement = null;
	public OMElement mBodyElement = null;
	public OMElement mResponseElement = null;
	public OMElement mResponseHeadElement = null;
	public OMElement mResponseBodyElement = null;

	public CErrors mErrors = new CErrors();

	/**
	 * 成功失败标志 成功-00
	 */
	public static final String STATE_SUCCESS = "00";
	/**
	 * 成功失败标志 失败-01
	 */
	public static final String STATE_FAIL = "01";

	/**
	 * 失败编码
	 */
	public String mErrCode = "";
	/**
	 * 失败编码 未知异常-E
	 */
	public static final String ERRCODE_E = "E";
	/**
	 * 失败编码 无异常-00
	 */
	public static final String ERRCODE_OK = "00";
	/**
	 * 失败编码 系统异常-01
	 */
	public static final String ERRCODE_SYS = "01";
	/**
	 * 失败编码 接口校验错误-02
	 */
	public static final String ERRCODE_CHECK = "02";
	/**
	 * 失败编码 业务规则错误-03
	 */
	public static final String ERRCODE_RULE = "03";

	private String mXMLPath = "";

	/**
	 * 接口默认调用方法，子类统一继承该方法
	 * 
	 * @param aOMElement
	 * @return
	 */
	public OMElement queryData(OMElement aOMElement) {
		mRootElement = aOMElement;
		try {
			if (!initRequestData()) {
				buildErrorResponseElement();
				return mResponseElement;
			}
			
			if (!returnResponseBody()) {
				buildErrorResponseElement();
				return mResponseElement;
			}
			buildResponseElement();
		} catch (Exception ex) {
			mErrCode = ERRCODE_E;
			buildErrorResponseElement();
		} finally {
			saveXML();
		}
		return mResponseElement;
	}

	/**
	 * 初始化报文 解析Head、Body
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean initRequestData() throws Exception {

		if ("".equals(mRootName)) {
			mErrCode = ERRCODE_SYS;
			mErrors.addOneError("被调用接口程序错误,未描述根节点");
			return false;
		}

		Iterator tRootIterator = mRootElement.getChildren();
		while (tRootIterator.hasNext()) {
			OMElement tRootElement = (OMElement) tRootIterator.next();
			if (!mRootName.equals(tRootElement.getLocalName())) {
				mErrCode = ERRCODE_SYS;
				mErrors.addOneError("被调用接口程序错误,描述根节点错误");
				return false;
			}

			Iterator tChildIterator = tRootElement.getChildren();
			while (tChildIterator.hasNext()) {
				OMElement tChildElement = (OMElement) tChildIterator.next();
				if ("HEAD".equals(tChildElement.getLocalName())) {
					mHeadElement = tChildElement;
					continue;
				}

				if ("BODY".equals(tChildElement.getLocalName())) {
					mBodyElement = tChildElement;
					continue;
				}
			}
		}

		Iterator tHeadIterator = mHeadElement.getChildren();
		while (tHeadIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tHeadIterator.next();
			if ("BATCHNO".equals(tChildElement.getLocalName())) {
				mBatchNo = tChildElement.getText();
				continue;
			}
		}

		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		mResponseElement = tOMFactory.createOMElement(mRootName, null);
		tOMFactory = OMAbstractFactory.getOMFactory();
		mResponseHeadElement = tOMFactory.createOMElement("HEAD", null);
		tOMFactory = OMAbstractFactory.getOMFactory();
		mResponseBodyElement = tOMFactory.createOMElement("BODY", null);
		
		if (mBatchNo == null || "".equals(mBatchNo)) {
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,无交易批次号");
			return false;
		}
		if (mBatchNo.length() !=20) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,批次号错误");
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			return false;
		}
		if (!mBatchNo.substring(0, 3).equals("CRM")) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("被调用接口程序错误,批次号错误");
			mBatchNo = "ERROR"+DateUtil.getCurrentDate("yyyyMMdd")+DateUtil.getCurrentDate("HHmmss");
			return false;
		}

		return true;
	}

	/**
	 * 错误报文返回
	 */
	private void buildErrorResponseElement() {

		String tErrInfo = "";
		if (ERRCODE_E.equals(mErrCode)) {
			tErrInfo = "系统异常，请联系运维人员";
		} else {
			tErrInfo = mErrors.getFirstError();
		}
		returnResponseHead(STATE_FAIL, mErrCode, tErrInfo);

		mResponseElement.addChild(mResponseHeadElement);
		mResponseElement.addChild(mResponseBodyElement);
		System.out.println("responseOME:" + mResponseElement);
		LoginVerifyTool.writeLog(mResponseElement, mBatchNo, "",
				PubFun.getCurrentDate(), PubFun.getCurrentTime(), "", "CRM",
				mErrCode, tErrInfo);
	}

	/**
	 * 返回报文的HEAD部分
	 * 
	 * @param aState
	 *            状态
	 * @param aErrCode
	 *            失败编码
	 * @param aErrInfo
	 *            失败原因
	 * @return
	 */
	public void returnResponseHead(String aState, String aErrCode,
			String aErrInfo) {
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		tLoginVerifyTool.addOm(mResponseHeadElement, "BATCHNO", mBatchNo);
		tLoginVerifyTool.addOm(mResponseHeadElement, "SENDDATE",
				PubFun.getCurrentDate());
		tLoginVerifyTool.addOm(mResponseHeadElement, "SENDTIME",
				PubFun.getCurrentTime());
		tLoginVerifyTool.addOm(mResponseHeadElement, "STATE", aState);
		tLoginVerifyTool.addOm(mResponseHeadElement, "ERRCODE", aErrCode);
		tLoginVerifyTool.addOm(mResponseHeadElement, "ERRINFO", aErrInfo);
	}

	/**
	 * 返回成功完整报文
	 */
	public void buildResponseElement() {
		returnResponseHead(STATE_SUCCESS, "", "");
		mResponseElement.addChild(mResponseHeadElement);
		mResponseElement.addChild(mResponseBodyElement);
		System.out.println("responseOME:" + mResponseElement);

		LoginVerifyTool.writeLog(mResponseElement, mBatchNo, "",
				PubFun.getCurrentDate(), PubFun.getCurrentTime(), "", "CRM",
				ERRCODE_OK, "成功处理");
	}

	/**
	 * 保存返回报文
	 */
	public void saveXML() {
		String tFileName = CRMMessageSave.generateFileName(mBatchNo, "xml");
		if (getXMLPath()) {
			CRMMessageSave.save(mResponseElement, tFileName, mXMLPath,
					"OutNoStd");
			CRMMessageSave.save(mRootElement, tFileName, mXMLPath, "InNoStd");
		} else {
			System.out.println(mResponseElement.toString());
			System.out.println(mRootElement.toString());
		}
	}

	private boolean getXMLPath() {
		String tPathSQL = "select sysvarvalue from LDSYSVAR  where Sysvar='CRM_LOGPath'";
		mXMLPath = new ExeSQL().getOneValue(tPathSQL);
		if (mXMLPath == null || mXMLPath.equals("")) {
			System.err.println("CRM接口报文保存目录查询失败！");
			return false;
		}
		return true;
	}

	/**
	 * 解析Body报文，返回报文BODY部分，由子类实现
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean returnResponseBody() throws Exception;
}
