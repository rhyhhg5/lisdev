package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_probookquery extends CRMAbstractInterface {
	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";

	public Crm_probookquery() {
		this.mRootName = "CRM_PROBOOKQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CONTNO.equals(tChildElement.getLocalName())) {
                mContno = tChildElement.getText();	
				if (("".equals(mContno)) || mContno == null) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("保单号不能为空");
			return false;
		}
			}
		}
		
		

		String grpcontdatasql = "select PrtNo from LCGrpCont where GrpContNo='" + mContno + "'"
		                      + " union "
                              + "select PrtNo from LBGrpCont where GrpContNo='" + mContno + "'"  
                              + " with ur ";


		String contdatasql = "select PrtNo from LCCont where ContNo='" + mContno + "'"
                           + " union "
                           + "select PrtNo from LBCont where ContNo='" + mContno + "'"  
                           + " with ur ";

        String scandatasql = "";
        String strPrtNo = "";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS grpcontdataResult;
		SSRS contdataResult;
        SSRS scandataResult;

        grpcontdataResult = tExeSQL.execSQL(grpcontdatasql);
        
        contdataResult = tExeSQL.execSQL(contdatasql);
        
        if(grpcontdataResult.getMaxRow()>0){
        	strPrtNo=grpcontdataResult.GetText(1, 1);
        }else if(contdataResult.getMaxRow()>0){
        	strPrtNo=contdataResult.GetText(1, 1);
        }else{
        	mErrCode = ERRCODE_RULE;
			mErrors.addOneError("查询保单信息失败！");
			return false;
        }
        scandatasql="select a.docid,a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
		       + " a.maketime,a.modifydate,a.modifytime,a.docid "
		       + " from es_doc_main a ,es_doc_def b where 1=1 "
		       + " and a.subtype = b.subtype "
	           + " and a.doccode like '"+strPrtNo+"%%'"
	           + " and a.busstype='TB'"
	           + " and b.busstype='TB'";
        
        scandataResult=tExeSQL.execSQL(scandatasql);
        if(scandataResult.getMaxRow()<1){
        	mErrCode = ERRCODE_RULE;
			mErrors.addOneError("该保单无扫描件信息！");
			return false;
        }
        scandatasql="select codename from ldcode where codetype='crmscanhttp' ";
        String http=tExeSQL.execSQL(scandatasql).GetText(1, 1);
        http +="/easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+scandataResult.GetText(1, 1)
             + "&DocCode="+scandataResult.GetText(1, 2)
             + "&BussTpye=TB&SubTpye="+scandataResult.GetText(1, 3);
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		if (scandataResult.getMaxRow() > 0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "HTTPURL",
					http);
		}		
			
		return true;
	}
}
