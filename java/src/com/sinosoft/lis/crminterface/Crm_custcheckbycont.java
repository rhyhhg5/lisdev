package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_custcheckbycont extends CRMAbstractInterface {
	/**
	 * 电话号
	 * */
	private static final String PHONE = "PHONE";
	private String mPhone = "";

	/**
	 * 保单号
	 * */
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/**
	 * 投/被保人证件号
	 * */
	private static final String IDNO = "IDNO";
	private String mIDNo = "";
	private String mUpperIDNo = "";
	private String mLowerIDNo = "";
	
	public Crm_custcheckbycont() {
		this.mRootName = "CRM_CUSTCHECKBYCONT";
	}

	public boolean returnResponseBody() throws Exception {
		String sql = "";
		String contnosql = "";
		String phonesql = "";
		String idnosql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PHONE.equals(tChildElement.getLocalName())) {
				mPhone = tChildElement.getText();
				if ((!"".equals(mPhone)) && mPhone != null) {
					phonesql = " and (d.phone='" + mPhone + "' or d.mobile='"
							+ mPhone + "') ";
					continue;
				}
				//tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "F");
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("电话号、保单号、投/被保人证件号均不能为空");
				return false;
			}
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					contnosql = " and a.contno='" + mContno + "' ";
					continue;
				}
				//tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "F");
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("电话号、保单号、投/被保人证件号均不能为空");
				return false;
			}
			if (IDNO.equals(tChildElement.getLocalName())) {
				mIDNo = tChildElement.getText();
				mUpperIDNo = mIDNo.toUpperCase();
				mLowerIDNo = mIDNo.toLowerCase();
				
				if ((!"".equals(mIDNo)) && mIDNo != null) {
					idnosql = " and (c.idno='"+mUpperIDNo+"' or c.idno='"+mLowerIDNo+"')" ;
					continue;
				}
				//tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "F");
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("电话号、保单号、投/被保人证件号均不能为空");
				return false;
			}
		}
		sql = "select 'T',a.contno 保单号,a.stateflag 保单状态,a.appntno 投保人,a.insuredno 被保险人,a.cvalidate 保单生效日期,a.prem 期交保费,a.cinvalidate 终止日期,a.appntname 投保人姓名,a.insuredname 被保人姓名,a.paytodate 缴至日期 "
				+ " from lccont a where 1=1 "
				+ contnosql
				+ "and a.contno in (select contno from lcinsured c,lcaddress d where 1=1 "
				+ idnosql
				+ " and c.insuredno=d.customerno and c.addressno=d.addressno "
				+ phonesql
				+ " union select contno from lcappnt c,lcaddress d where 1=1 "
				+ idnosql
				+ " and c.appntno=d.customerno and c.addressno=d.addressno "
				+ phonesql
				+ " union select contno from lbinsured c,lcaddress d where 1=1"
				+ idnosql
				+ " and c.insuredno=d.customerno and c.addressno=d.addressno "
				+ phonesql
				+ " union select contno from lbappnt c,lcaddress d where 1=1 "
				+ idnosql
				+ " and c.appntno=d.customerno and c.addressno=d.addressno "
				+ phonesql
				+ ") union all "
				+ "select 'T',a.contno 保单号,a.stateflag 保单状态,a.appntno 投保人,a.insuredno 被保险人,a.cvalidate 保单生效日期,a.prem 期交保费,a.cinvalidate 终止日期,a.appntname 投保人姓名,a.insuredname 被保人姓名,a.paytodate 缴至日期  "
				+ "from lbcont a where 1=1 "
				+ contnosql
				+ "and a.contno in(select contno from lcinsured c,lcaddress d where 1=1 "
				+ idnosql
				+ " and c.insuredno=d.customerno and c.addressno=d.addressno "
				+ phonesql
				+ " union select contno from lcappnt c,lcaddress d where 1=1 "
				+ idnosql
				+ " and c.appntno=d.customerno and c.addressno=d.addressno "
				+ phonesql
				+ " union select contno from lbinsured c,lcaddress d where 1=1 "
				+ idnosql
				+ " and c.insuredno=d.customerno and c.addressno=d.addressno "
				+ phonesql
				+ " union select contno from lbappnt c,lcaddress d where 1=1 "
				+ idnosql
				+ " and c.appntno=d.customerno and c.addressno=d.addressno "
				+ phonesql + ") with ur ";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tResult;
		tResult = tExeSQL.execSQL(sql);
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		if (tResult.getMaxRow() > 0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG",
					tResult.GetText(1, 1));
			tLoginVerifyTool.addOm(mResponseBodyElement, "CONTNO",
					tResult.GetText(1, 2));
			tLoginVerifyTool.addOm(mResponseBodyElement, "STATEFLAG",
					tResult.GetText(1, 3));
			tLoginVerifyTool.addOm(mResponseBodyElement, "APPNTNO",
					tResult.GetText(1, 4));
            tLoginVerifyTool.addOm(mResponseBodyElement, "APPNTNAME",
                    tResult.GetText(1, 9));
			tLoginVerifyTool.addOm(mResponseBodyElement, "INSUREDNO",
					tResult.GetText(1, 5));
            tLoginVerifyTool.addOm(mResponseBodyElement, "INSUREDNAME",
                    tResult.GetText(1, 10));
			tLoginVerifyTool.addOm(mResponseBodyElement, "CVALIDATE",
					tResult.GetText(1, 6));
			tLoginVerifyTool.addOm(mResponseBodyElement, "PREM",
					tResult.GetText(1, 7));
			tLoginVerifyTool.addOm(mResponseBodyElement, "ENDDATE",
					tResult.GetText(1, 8));
            tLoginVerifyTool.addOm(mResponseBodyElement, "PAYTODATE",
                    tResult.GetText(1, 11));

			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
			String risksql = "select (select riskname from lmriskapp where riskcode=a.riskcode) 险种名称,a.amnt 保额,a.prem 保费,a.paytodate 交至日期 "
					+ " from lcpol a where a.contno='"
					+ tResult.GetText(1, 2)
					+ "' union all"
					+ " select (select riskname from lmriskapp where riskcode=a.riskcode) 险种名称,a.amnt 保额,a.prem 保费,a.paytodate 交至日期 "
					+ " from lbpol a where a.contno='"
					+ tResult.GetText(1, 2)
					+ "' with ur ";
			SSRS risklistResult;
			risklistResult = tExeSQL.execSQL(risksql);
			int trow = risklistResult.MaxRow;
			OMElement tcustomerlist = tOMFactory.createOMElement(
					"RISKLIST", null);
			for (int i = 0; i < trow; i++) {
				OMElement tcustomer = tOMFactory.createOMElement("RISKDATA",
						null);
				System.out.println();
				tLoginVerifyTool.addOm(tcustomer, "RISKNAME",
						risklistResult.GetText(i + 1, 1));
				tLoginVerifyTool.addOm(tcustomer, "AMNT",
						risklistResult.GetText(i + 1, 2));
				tLoginVerifyTool.addOm(tcustomer, "PREM",
						risklistResult.GetText(i + 1, 3));
				tLoginVerifyTool.addOm(tcustomer, "PAYTODATE",
						risklistResult.GetText(i + 1, 4));
				tcustomerlist.addChild(tcustomer);
			}
			mResponseBodyElement.addChild(tcustomerlist);
		} else {
			tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "F");
		}
		return true;
	}

//	public static void main(String[] args) {
//		String mIDNo = "61030319480425002x";
//		String mUpperIDNO = mIDNo.toUpperCase();
//		String mLowerIDNO = mIDNo.toLowerCase();
//		System.out.println("mIDNo:" + mIDNo);
//		System.out.println("mUpperIDNO:" + mUpperIDNO);
//		System.out.println("mLowerIDNO:" + mLowerIDNO);
//	}
}
