package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_acctracequery extends CRMAbstractInterface {
    /**当前页码**/
    private static final String PAGEINDEX = "PAGEINDEX";
    private String mPageindex = "";
    private int mPagein = 0;
    /**每页记录条数**/
    private static final String PAGESIZE = "PAGESIZE";
    private String mPagesize = "";
    private int mPagesi = 0;
	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
    
    private String mStrsql="";
	public Crm_acctracequery() {
		this.mRootName = "CRM_ACCTRACEQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CONTNO.equals(tChildElement.getLocalName())) {
                mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
                    mStrsql +=" and a.contno='"+mContno+"'";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("保单号不能为空");
				return false;
			}
            
            if (PAGEINDEX.equals(tChildElement.getLocalName())) {
                mPageindex = tChildElement.getText();
                if (mPageindex!=null&&!"".equals(mPageindex)) {
                    mPagein = Integer.parseInt(mPageindex);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码、每页记录条数均不能为空");
                return false;
            }
            if (PAGESIZE.equals(tChildElement.getLocalName())) {
                mPagesize = tChildElement.getText();
                if (mPagesize!=null&&!"".equals(mPagesize)) {
                    mPagesi = Integer.parseInt(mPagesize);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码、每页记录条数均不能为空");
                return false;
            }
		}
		String sql = "select distinct  "+
            "   (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='LX'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 月度收益, "+
            "   (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='MF'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 保单管理费, "+
            "   (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='RP'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 风险保费扣费, "+
            "   a.DUEBALADATE   AS 结算日期, "+
            "   (SELECT RATE FROM LMInsuAccRate WHERE BALADATE=a.duebaladate AND INSUACCNO=a.insuaccno "+
            " AND RateType = 'C' AND RateIntvUnit = 'Y' AND RateIntv = 1)   as 结算利率, "+
            "    (select sum(money) from lcinsureacctrace where contno=a.contno   "+
            " and paydate<= (a.duebaladate - 1 month))    as 期初账户价值, "+
            "case when (select count(1) from lbinsureacctrace where  otherno =sequenceno and moneytype in('CT','WT','XT')) ='1' then 0.00 else a.insuaccbalaafter end  as 期末账户价值, "+//modify by fuxin 20141222 如果ct xt wt以后的期末账户价值为0
            "   (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='ZB'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 追加保费, "+
            "    (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='B'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 持续奖金, "+
            " 0 AS 退保金额, "+
            "    (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='LQ'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 部分领取金额, "+
            "    (select sum(fee) from lcinsureaccfeetrace where contno=a.contno and moneytype='LQ'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 领取扣费, "+
            "    (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='BF' AND  othertype in ('1','2') "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 期缴保费, "+
            "    (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='GL' AND othertype in ('1','2')"+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 期缴初始扣费 ,"+
            "    (select sum(money) from lcinsureacctrace where contno=a.contno and moneytype='KF' "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 追加扣费 "+                
            " from lcinsureaccbalance a ,lcinsureacc b where 1=1 and a.contno=b.contno "+
            mStrsql
            +" union "+
            " select distinct "+
            "   (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='LX'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 月度收益, "+
            "   (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='MF'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 保单管理费, "+
            "   (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='RP'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 风险保费扣费, "+
            "   a.DUEBALADATE   AS 结算日期, "+
            "   (SELECT RATE FROM LMInsuAccRate WHERE BALADATE=a.duebaladate AND INSUACCNO=a.insuaccno "+
            " AND RateType = 'C' AND RateIntvUnit = 'Y' AND RateIntv = 1)   as 结算利率, "+
            "    (select sum(money) from lbinsureacctrace where contno=a.contno   "+
            " and paydate<= (a.duebaladate - 1 month))    as 期初账户价值, "+
            "case when (select count(1) from lbinsureacctrace where  otherno =sequenceno and moneytype in('CT','WT','XT')) ='1' then 0.00 else a.insuaccbalaafter end as 期末账户价值, "+
            "    (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='ZB'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 追加保费, "+
            "    (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='B'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 持续奖金, "+
            "    (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='CT'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))    AS 退保金额, "+
            "    (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='LQ'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 部分领取金额, "+
            "    (select sum(fee) from lbinsureaccfeetrace where contno=a.contno and moneytype='LQ'  "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 领取扣费, "+
            "    (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='BF' AND  othertype in ('1','2') "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 期缴保费, "+
            "    (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='GL' AND othertype in ('1','2')"+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 期缴初始扣费 ,"+
            "    (select sum(money) from lbinsureacctrace where contno=a.contno and moneytype='KF' "+
            " and paydate<=a.duebaladate and paydate> (a.duebaladate - 1 month))   as 追加扣费 "+            
            " from Lcinsureaccbalance a ,lbinsureacc b "+
            "  Where 1 = 1  "+
            "and a.contno=b.contno  "+
            mStrsql
            +" order by 4"
            +" with ur ";
        
        LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
        OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		ExeSQL tExeSQL = new ExeSQL();
        
        String total = "select count(*) from (" + sql + ") a with ur";// 查询总条数
        SSRS totalResult;
        totalResult = tExeSQL.execSQL(total);
        if (totalResult.getMaxRow() > 0 && Integer.parseInt(totalResult.GetText(1, 1)) >0) {
            tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
                    totalResult.GetText(1, 1));
        }
        
        sql = "select * from (select c.*,rownumber() over() as rn from (" + sql
        + ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
        + " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		SSRS sResult;
		sResult = tExeSQL.execSQL(sql);
		if (sResult.getMaxRow() > 0) {
            OMElement tbalancelist = tOMFactory.createOMElement("BALANCELIST",
                    null);
            for(int i=1;i<=sResult.getMaxRow();i++){
                
                OMElement tbalancedata = tOMFactory.createOMElement("BALANCEDATA",
                        null);
                if(!"".equals(sResult.GetText(i, 1)) && sResult.GetText(i, 1)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "LXMONEY",
                            sResult.GetText(i, 1));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "LXMONEY",
                            "0");
                }
                
                
                if(!"".equals(sResult.GetText(i, 2)) && sResult.GetText(i, 2)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "MFFEE",
                            sResult.GetText(i, 2));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "MFFEE",
                           "0");
                }
                
                if(!"".equals(sResult.GetText(i,3)) && sResult.GetText(i, 3)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "RPFEE",
                            sResult.GetText(i, 3));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "RPFEE",
                            "0");
                }
               
                if(!"".equals(sResult.GetText(i,4)) && sResult.GetText(i, 4)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "RUNDATE",
                            sResult.GetText(i, 4));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "RUNDATE",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,5)) && sResult.GetText(i, 5)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "BALANCERATE",
                            sResult.GetText(i, 5));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "BALANCERATE",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,6)) && sResult.GetText(i, 6)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "INSUREACCBALABEFORE",
                            sResult.GetText(i, 6));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "INSUREACCBALABEFORE",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,7)) && sResult.GetText(i, 7)!="null" && sResult.GetText(i, 7)!=null){
                    tLoginVerifyTool.addOm(tbalancedata, "INSUREACCBALAAFTER",
                            sResult.GetText(i, 7));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "INSUREACCBALAAFTER",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,8)) && sResult.GetText(i, 8)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "ZFMONEY",
                            sResult.GetText(i, 8));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "ZFMONEY",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,9)) && sResult.GetText(i, 9)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "BMONEY",
                            sResult.GetText(i, 9));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "BMONEY",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,10)) && sResult.GetText(i, 10)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "TFMONEY",
                            sResult.GetText(i, 10));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "TFMONEY",
                           "0");
                }
                
                if(!"".equals(sResult.GetText(i,11)) && sResult.GetText(i, 11)!="null"){
                    tLoginVerifyTool.addOm(tbalancedata, "LQMOENY",
                            sResult.GetText(i, 11));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "LQMOENY",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,13)) && sResult.GetText(i, 13)!="null" && sResult.GetText(i, 13)!=null){
                    tLoginVerifyTool.addOm(tbalancedata, "CFMONEY",
                            sResult.GetText(i, 13));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "CFMONEY",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,14)) && sResult.GetText(i, 14)!="null" && sResult.GetText(i, 14)!=null){
                    tLoginVerifyTool.addOm(tbalancedata, "CKMONEY",
                            sResult.GetText(i, 14));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "CKMONEY",
                            "0");
                }
                
                if(!"".equals(sResult.GetText(i,15)) && sResult.GetText(i, 15)!="null" && sResult.GetText(i, 15)!=null){
                    tLoginVerifyTool.addOm(tbalancedata, "ZKMONEY",
                            sResult.GetText(i, 15));
                }else{
                    tLoginVerifyTool.addOm(tbalancedata, "ZKMONEY",
                            "0");
                }
                
                tbalancelist.addChild(tbalancedata);
             
            }
			
            mResponseBodyElement.addChild(tbalancelist);
		}
        
		return true;
	}
}
