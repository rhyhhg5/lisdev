package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_agentcontinfo extends CRMAbstractInterface {

	/** 保单号/投保单号 **/
	private static final String CONTNNO = "CONTNNO";
	private String mContno = "";
	/** 业务员工号 **/
	private static final String CARDID = "CARDID";
	private String mCardid = "";

	public Crm_agentcontinfo() {
		this.mRootName = "CRM_AGENTCONTINFO";
	}

	public boolean returnResponseBody() throws Exception {
		String sql = "";
		String tsql = "";
		String tsqlgrp = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();

			if (CONTNNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					tsql = tsql + " and lc.CONTNO='" + mContno + "' ";
					tsqlgrp = tsqlgrp + " and lcg.GrpContNo ='" + mContno + "'";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("保单号/投保单号能为空");
				return false;

			}
			if (CARDID.equals(tChildElement.getLocalName())) {
				mCardid = tChildElement.getText();
				if ((!"".equals(mCardid)) && mCardid != null) {
					AgentCodeTransformation a = new AgentCodeTransformation();
					if(!a.AgentCode(mCardid, "Y")){
						System.out.println(a.getMessage());
						mErrCode = ERRCODE_CHECK;
						mErrors.addOneError(a.getMessage());
						return false;
					}
					mCardid = a.getResult();
					tsql = tsql + " and lc.AgentCode='" + mCardid + "'";
					tsqlgrp = tsqlgrp + " and lcg.AgentCode ='" + mCardid + "'";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("业务员工号不能为空");
				return false;
			}

		}

		sql = "Select Case When Lc.Stateflag = '0' Then  '00' When Lc.Stateflag = '1' Then  '01' Else  '02' End,  "
				+ " Lc.ProposalContNo, "
				+ " Lc.Appflag, "
				+ " Lc.Contno, "
				+ " Lc.Stateflag, "
				//+ " Lc.Appntno," //投保人号
				+ " Lc.Appntname,"//投保人姓名
				+ " Lc.Cvalidate, "
				+ " (Select SumActuPayMoney From Ljapay Lja  Where Lja.Incomeno = Lc.Contno  And Lja.Duefeetype = '0') As Eachprem, "
				+ " lc.CInValiDate "
				+ " From lccont lc "
				+ " Where 1=1 "
				+ tsql
				+ " Union "
				+ " Select Case When lcg.Stateflag = '0' Then  '00' When lcg.Stateflag = '1' Then  '01' Else  '02' End, "
				+ " lcg.ProposalGrpContNo, "
				+ " lcg.Appflag, "
				+ " lcg.grpContno, "
				+ " lcg.Stateflag, "
				+ " lcg.Appntno, "//投保人号
				+ " lcg.Cvalidate, "
				+ " (Select SumActuPayMoney From Ljapay Lja  Where Lja.Incomeno = lcg.grpContno  And Lja.Duefeetype = '0') As Eachprem, "
				+ " lcg.CInValiDate "
				+ " From lcgrpcont lcg   "
				+ " Where 1=1 "
				+ tsqlgrp + " with ur ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tResult;
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		tResult = tExeSQL.execSQL(sql);
		if (tResult.getMaxRow() > 0) {
			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
			tOMFactory = OMAbstractFactory.getOMFactory();

			OMElement tcustomer = tOMFactory.createOMElement("CONTDATA", null);
			tLoginVerifyTool.addOm(tcustomer, "STATUS", tResult.GetText(1, 1));
			//System.out.println("投保的保单是----"+tResult.GetText(1, 2));
			tLoginVerifyTool.addOm(tcustomer, "ProposalContNo", tResult.GetText(1, 2));//投保保单
			tLoginVerifyTool
					.addOm(tcustomer, "APPSTATE", tResult.GetText(1, 3));
			tLoginVerifyTool.addOm(tcustomer, "CONTNO", tResult.GetText(1, 4));
			tLoginVerifyTool.addOm(tcustomer, "CONTSTATE",
					tResult.GetText(1, 5));
			//System.out.println("投保人是----"+tResult.GetText(1, 6));
			tLoginVerifyTool.addOm(tcustomer, "AppntNo", tResult.GetText(1, 6));//投保人
			tLoginVerifyTool.addOm(tcustomer, "CVALIDATE",
					tResult.GetText(1, 7));
			tLoginVerifyTool
					.addOm(tcustomer, "EACHPREM", tResult.GetText(1, 8));
			tLoginVerifyTool.addOm(tcustomer, "ENDDATE", tResult.GetText(1, 9));

			mResponseBodyElement.addChild(tcustomer);

			String risksql = "select "
					+ "(select riskname from lmriskapp where riskcode=lcp.riskcode) as 险种名称, "
					+ "lcp.Amnt as 保额, "
					+ "lcp.Prem as 保费, "
					+ "lcp.PaytoDate as 交至日期 "
					+ "from lcpol lcp where contno ='"
					+ mContno
					+ ""
					+ "'union "
					+ "select "
					+ "(select riskname from lmriskapp where riskcode=lcgp.riskcode) as 险种名称, "
					+ "lcgp.Amnt as 保额, "
					+ "lcgp.Prem as 保费, "
					+ "lcgp.PaytoDate as 交至日期 "
					+ " from LCGrpPol lcgp where GrpContNo ='" 
					+ mContno
					+ "' with ur ";
			SSRS triskResult;
			triskResult = tExeSQL.execSQL(risksql);
			if (triskResult.getMaxRow() > 0) {
				OMElement trisklist = tOMFactory.createOMElement("RISKLIST",
						null);
				int riskrow = triskResult.MaxRow;// triskResult.MaxRow;
				for (int j = 0; j < riskrow; j++) {
					OMElement trisk = tOMFactory.createOMElement("ITEM", null);
					tLoginVerifyTool.addOm(trisk, "RISKNAME",
							triskResult.GetText(j + 1, 1));
					tLoginVerifyTool.addOm(trisk, "AMNT",
							triskResult.GetText(j + 1, 2));
					tLoginVerifyTool.addOm(trisk, "PREM",
							triskResult.GetText(j + 1, 3));
					tLoginVerifyTool.addOm(trisk, "PAYTODATE",
							triskResult.GetText(j + 1, 4));
					trisklist.addChild(trisk);
				}
				mResponseBodyElement.addChild(trisklist);
			}
		}else{
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("没有查询到符合条件的保单信息");
			return false;
		}
		return true;
	}

}
