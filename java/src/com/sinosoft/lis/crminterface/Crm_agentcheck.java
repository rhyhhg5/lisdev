package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMElement;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_agentcheck extends CRMAbstractInterface {
	/**
	 * 证件号码
	 * */
	private static final String IDNO = "IDNO";
	private String mIDNo = "";

	/**
	 * 业务员工号
	 * */
	private static final String AGENTCODE = "AGENTCODE";
	private String mAGENTCode = "";

	public Crm_agentcheck() {
		this.mRootName = "CRM_AGENTCHECK";
	}
	
	private String inAgentCode = "";
	
	public boolean returnResponseBody() throws Exception {
		String tsql = "select agentstate,agentcode,name from laagent where ";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();

			if (IDNO.equals(tChildElement.getLocalName())) {
				mIDNo = tChildElement.getText();
				if ((!"".equals(mIDNo)) || mIDNo == null) {
					tsql = tsql + "idno='" + mIDNo + "' and ";
				}
				continue;
			}

			if (AGENTCODE.equals(tChildElement.getLocalName())) {
				mAGENTCode = tChildElement.getText();
				
				if ((!"".equals(mAGENTCode)) || mAGENTCode == null) {
					AgentCodeTransformation a = new AgentCodeTransformation();
					if(a.AgentCode(mAGENTCode, "Y")){
						System.out.println(a.getMessage());
						mErrCode = ERRCODE_CHECK;
						mErrors.addOneError(a.getMessage());
						return false;
					}
					mAGENTCode=a.getResult();
					tsql = tsql + "agentcode='" + mAGENTCode + "' and ";
				}
				
				inAgentCode = tChildElement.getText();
				continue;
			}

		}
		tsql = tsql.substring(0, tsql.length() - 4);
		System.out.println(tsql);
		if (("".equals(mIDNo) || mIDNo == null)
				&& ("".equals(mAGENTCode) || mAGENTCode == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("证件号码和员工号必录其一");
			return false;
		}

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tResult;
		tResult = tExeSQL.execSQL(tsql);
		if (tResult.getMaxRow() > 0) {
			LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
			tLoginVerifyTool.addOm(mResponseBodyElement, "agentstate",
					tResult.GetText(1, 1));
			if(!"".equals(tResult.GetText(1, 2)) && tResult.GetText(1, 2)!=null){
				String ResultAgentCode = tResult.GetText(1, 2);
				if(inAgentCode.equals(ResultAgentCode)){
					tLoginVerifyTool.addOm(mResponseBodyElement, "agentcode",
							ResultAgentCode);
				}else{
					ResultAgentCode = new ExeSQL().getOneValue("select groupagentcode from laagent where agentcode='"+ResultAgentCode+"'");
					tLoginVerifyTool.addOm(mResponseBodyElement, "agentcode",
							ResultAgentCode);
				}
			}
			tLoginVerifyTool.addOm(mResponseBodyElement, "agentname",
					tResult.GetText(1, 3));
		}

		return true;
	}

}
