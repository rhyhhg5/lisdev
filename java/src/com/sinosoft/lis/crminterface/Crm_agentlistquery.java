package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_agentlistquery extends CRMAbstractInterface {

    /** 当前页码 **/
    private static final String PAGEINDEX = "PAGEINDEX";
    private String mPageindex = "";
    private int mPagein = 0;

    /** 每页记录条数 **/
    private static final String PAGESIZE = "PAGESIZE";
    private String mPagesize = "";
    private int mPagesi = 0;
	
	/** 业务员代码 **/
	private static final String AGENTCODE = "AGENTCODE";
	private String mAgentcode = "";
	/** 业务员姓名 **/
	private static final String AGENTNAME = "AGENTNAME";
	private String mAgentname = "";
	/** 主联系电话 **/
	private static final String PHONE = "PHONE";
	private String mPhone = "";
	/** 证件号 **/
	private static final String IDNO = "IDNO";
	private String mIdno = "";

	public Crm_agentlistquery() {
		this.mRootName = "CRM_AGENTLISTQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			
            if (PAGEINDEX.equals(tChildElement.getLocalName()))
            {
                mPageindex = tChildElement.getText();
                if ((!"".equals(mPageindex)) && mPageindex != null)
                {
                    mPagein = Integer.parseInt(mPageindex);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码和每页记录条数为必录项");
                return false;
            }

            if (PAGESIZE.equals(tChildElement.getLocalName()))
            {
                mPagesize = tChildElement.getText();
                if ((!"".equals(mPagesize)) && mPagesize != null)
                {
                    mPagesi = Integer.parseInt(mPagesize);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码和每页记录条数为必录项");
                return false;
            }
			
			if (AGENTCODE.equals(tChildElement.getLocalName())) {
				mAgentcode = tChildElement.getText();
				
				if ((!"".equals(mAgentcode)) && mAgentcode != null) {
					AgentCodeTransformation a = new AgentCodeTransformation();
					if(a.AgentCode(mAgentcode, "Y")){
						System.out.println(a.getMessage());
						 mErrCode = ERRCODE_CHECK;
			             mErrors.addOneError(a.getMessage());
						return false;
					}
					mAgentcode=a.getResult();
					tsql += " and AgentCode='" + mAgentcode + "' ";
				}
			
				continue;
			}
			if (AGENTNAME.equals(tChildElement.getLocalName())) {
				mAgentname = tChildElement.getText();
				if ((!"".equals(mAgentname)) && mAgentname != null) {
					tsql += " and name like '%" + mAgentname + "%' ";
				}
				continue;
			}
			if (PHONE.equals(tChildElement.getLocalName())) {
				mPhone = tChildElement.getText();
				if ((!"".equals(mPhone)) && mPhone != null) {
					tsql += " and phone='" + mPhone + "' ";
				}
				continue;
			}
			if (IDNO.equals(tChildElement.getLocalName())) {
				mIdno = tChildElement.getText();
				if ((!"".equals(mIdno)) && mIdno != null) {
					tsql += " and idno='" + mIdno + "' ";
				}
				continue;
			}
		}
		if (("".equals(mAgentcode) || mAgentcode == null)
				&& ("".equals(mAgentname) || mAgentname == null)
				&& ("".equals(mPhone) || mPhone == null)
				&& ("".equals(mIdno) || mIdno == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("业务员代码、姓名、电话、证件号码必录其中之一");
			return false;
		}
		String sql = "Select name, sex, idnotype,"
				+ "(select (select codename from ldcode  where codetype ='branchtype' and code=latree.branchtype) ||"
				+ "(select codename from ldcode  where codetype='branchtype2' and code = latree.branchtype2) 销售渠道 "
				+ " from latree where agentcode = laagent.agentcode ),"
				+ "agentcode,branchtype,"
				+ "(Select agentgrade from latree where agentcode =laagent.agentcode),"
				+ "agentstate,phone,groupagentcode from laagent where 1=1 " + tsql
				+ " with ur ";
		
        /**实例化ExeSQL**/
        ExeSQL tExeSQL = new ExeSQL();
        String totalnum = "select count(*) from (" + sql + ") cou with ur";

        /**实例化结果集SSRS**/
        SSRS sResult;
        SSRS ttotal = tExeSQL.execSQL(totalnum);
        LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

        /**加入到body标签下*/
        tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM", ttotal.GetText(1, 1));
		
		
		
        /**根据已查询的结果进行分页查询处结果**/
        sql = "select * from (select c.*,rownumber() over() as rn from (" + sql + ") c) d where d.rn>"
                + ((mPagein - 1) * mPagesi) + " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		
        /**执行sql**/
		sResult = tExeSQL.execSQL(sql);
		if (sResult.getMaxRow() > 0) {
			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
			/**创建集合标签*/
			OMElement agentlist = tOMFactory.createOMElement("AGENTLIST", null);
			for (int i = 1; i <= sResult.getMaxRow(); i++) {
				/**创建集合子标签*/
				OMElement agentdata = tOMFactory.createOMElement("AGENTDATA",
						null);
				tLoginVerifyTool.addOm(agentdata, "AGENTNAME",
						sResult.GetText(i, 1));
				tLoginVerifyTool.addOm(agentdata, "SEX", sResult.GetText(i, 2));
				tLoginVerifyTool.addOm(agentdata, "IDTYPE",
						sResult.GetText(i, 3));
				tLoginVerifyTool.addOm(agentdata, "SALECHNL",
						sResult.GetText(i, 4));
				tLoginVerifyTool.addOm(agentdata, "AGENTCODE",
						sResult.GetText(i, 5));
				tLoginVerifyTool.addOm(agentdata, "BRANCHTYPE",
						sResult.GetText(i, 6));
				tLoginVerifyTool.addOm(agentdata, "AGENTGRADE",
						sResult.GetText(i, 7));
				tLoginVerifyTool.addOm(agentdata, "AGENTSTATE",
						sResult.GetText(i, 8));
				tLoginVerifyTool.addOm(agentdata, "PHONE",
						sResult.GetText(i, 9));
				tLoginVerifyTool.addOm(agentdata, "GROUPAGENTCODE",
						sResult.GetText(i, 10));
				agentlist.addChild(agentdata);
			}
			mResponseBodyElement.addChild(agentlist);
		}
		return true;
	}
}
