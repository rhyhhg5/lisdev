package com.sinosoft.lis.crminterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.XmlUpdate;

public class Crm_edorlistquery extends CRMAbstractInterface
{

    /** 当前页码 **/
    private static final String PAGEINDEX = "PAGEINDEX";

    private String mPageindex = "";

    private int mPagein = 0;

    /** 每页记录条数 **/
    private static final String PAGESIZE = "PAGESIZE";

    private String mPagesize = "";

    private int mPagesi = 0;

    /** 保单号 **/
    private static final String CONTNO = "CONTNO";

    private String mContno = "";

    private String mEdorInfo = "";

    public Crm_edorlistquery()
    {
        this.mRootName = "CRM_EDORLISTQUERY";
    }

    public boolean returnResponseBody() throws Exception
    {
        Iterator tBodyIterator = mBodyElement.getChildren();
        while (tBodyIterator.hasNext())
        {
            OMElement tChildElement = (OMElement) tBodyIterator.next();
            if (PAGEINDEX.equals(tChildElement.getLocalName()))
            {
                mPageindex = tChildElement.getText();
                if ((!"".equals(mPageindex)) && mPageindex != null)
                {
                    mPagein = Integer.parseInt(mPageindex);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码和每页记录条数均不能为空");
                return false;
            }
            if (PAGESIZE.equals(tChildElement.getLocalName()))
            {
                mPagesize = tChildElement.getText();
                if ((!"".equals(mPagesize)) && mPagesize != null)
                {
                    mPagesi = Integer.parseInt(mPagesize);
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("当前页码和每页记录条数均不能为空");
                return false;
            }
            if (CONTNO.equals(tChildElement.getLocalName()))
            {
                mContno = tChildElement.getText();
                if ((!"".equals(mContno)) && mContno != null)
                {
                    continue;
                }
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("保单号不能为空");
                return false;
            }
        }
        String sql = "select a.edoracceptno as 保全批单号,"
                + "(select name from ldperson where customerno=c.customerno) as 申请人姓名," + "a.edorappdate as 申请日期,"
                + "b.edorvalidate as 生效日期,"
                + "(select edorname from lmedoritem where edorcode=b.edortype fetch first 1 rows only) as 保全变更原因,"
                + "codename('applytypeno',c.applytypeno) as 申请人类型,"
                + "codename('appedorstate',a.edorstate) as 保全状态,"
                + "(select edorinfo from LPEdorAppPrint where edoracceptno = a.edoracceptno) as 保全批单内容 "
                + " from lpedorapp a,lpedoritem b,lgwork c " + " where a.edoracceptno=b.edoracceptno "
                + " and b.edoracceptno=c.workno and b.contno='" + mContno + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
        LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

        // 查询总条数
        String numsql = "select count(*) from (" + sql + ") cou with ur ";
        SSRS numResult;
        numResult = tExeSQL.execSQL(numsql);
        if (numResult.getMaxRow() > 0)
        {
            tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM", numResult.GetText(1, 1));
        }

        // 分页查询
        String edorsql = "select * from (select g.*,rownumber() over() as rn from (" + sql + ") g) d where d.rn>"
                + ((mPagein - 1) * mPagesi) + " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
        SSRS edorResult;
        edorResult = tExeSQL.execSQL(edorsql);
        if (edorResult.getMaxRow() > 0)
        {
            OMElement edorlist = tOMFactory.createOMElement("EDORLIST", null);
            for (int i = 1; i <= edorResult.getMaxRow(); i++)
            {
                OMElement edordata = tOMFactory.createOMElement("EDORDATA", null);
                tLoginVerifyTool.addOm(edordata, "EDORACCEPTNO", edorResult.GetText(i, 1));
                tLoginVerifyTool.addOm(edordata, "APPLYNAME", edorResult.GetText(i, 2));
                tLoginVerifyTool.addOm(edordata, "APPLYDATE", edorResult.GetText(i, 3));
                tLoginVerifyTool.addOm(edordata, "EDORVALIDATE", edorResult.GetText(i, 4));
                tLoginVerifyTool.addOm(edordata, "EDORTYPE", edorResult.GetText(i, 5));
                tLoginVerifyTool.addOm(edordata, "APPLYTYPE", edorResult.GetText(i, 6));
                tLoginVerifyTool.addOm(edordata, "EDORSTATE", edorResult.GetText(i, 7));
                if (getEdorinfo(edorResult.GetText(i, 1)))
                {
                    tLoginVerifyTool.addOm(edordata, "CONTENT", mEdorInfo);
                }

                edorlist.addChild(edordata);
            }
            mResponseBodyElement.addChild(edorlist);
        }
        return true;
    }

    private boolean getEdorinfo(String mEdorNo)
    {
        System.out.println("----开始查询保全内容-----");
        InputStream ins = null;
        String sql = "select * from LPEDORAPPPRINT " + "where EdorAcceptNo = '" + mEdorNo + "' ";
        Connection conn = DBConnPool.getConnection();

        try
        {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next())
            {
                Blob tBlob = rs.getBlob("EdorInfo");
                ins = tBlob.getBinaryStream();
                mEdorInfo = convertStreamToString(ins);
                System.out.println("保全内容:" + mEdorInfo);
            }
            else
            {
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("没有查询到批单信息");
                return false;
            }

            rs.close();
            stmt.close();
            conn.close();
        }
        catch (Exception e)
        {
            mErrCode = ERRCODE_CHECK;
            mErrors.addOneError("查询批单信息出错");
            return false;
        }
        System.out.println("----保全内容查询完毕-----");
        return true;
    }

    public static String convertStreamToString(InputStream is)
    {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }
}
