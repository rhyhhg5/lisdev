package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_proposalinfo extends CRMAbstractInterface {

	/** ӡˢ���� **/
	private static final String PRTNO = "PRTNO";
	private String mPrtno = "";

	public Crm_proposalinfo() {
		this.mRootName = "CRM_PROPOSALINFO";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PRTNO.equals(tChildElement.getLocalName())) {
				mPrtno = tChildElement.getText();
				if ((!"".equals(mPrtno)) && mPrtno != null) {
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("ӡˢ���벻��Ϊ��");
				return false;
			}
		}
		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

		String proposalsql = "Select Prtno,Managecom,Appntname,Agentcode,"
				+ "(Select Name From Lcinsured Where Contno = Lcc.Contno Fetch First 1 Rows Only),"
				+ "(Select Archiveno From Es_Doc_Main Where Doccode=Lcc.Prtno And Busstype='TB' Fetch First 1 Rows Only),"
				+ "Polapplydate,(case when cardflag='b' then '97' when cardflag='c' then '96' when cardflag='d' then '98' when prtno like 'PD%' then '99' else salechnl end),Cvalidate,Agentcom From Lccont Lcc Where Prtno='"
				+ mPrtno + "' With Ur";
		SSRS proposalResult;
		proposalResult = tExeSQL.execSQL(proposalsql);
		if (proposalResult.getMaxRow() > 0) {
			OMElement poldata = tOMFactory
					.createOMElement("PROPOSALDATA", null);
			tLoginVerifyTool.addOm(poldata, "PRTNO",
					proposalResult.GetText(1, 1));
			tLoginVerifyTool.addOm(poldata, "MANAGECOM",
					proposalResult.GetText(1, 2));
			tLoginVerifyTool.addOm(poldata, "APPNTNAME",
					proposalResult.GetText(1, 3));
			tLoginVerifyTool.addOm(poldata, "AGENTCODE",
					proposalResult.GetText(1, 4));
			tLoginVerifyTool.addOm(poldata, "INSUREDNAME",
					proposalResult.GetText(1, 5));
			tLoginVerifyTool.addOm(poldata, "ARCHIVENO",
					proposalResult.GetText(1, 6));
			tLoginVerifyTool.addOm(poldata, "POLAPPLYDATE",
					proposalResult.GetText(1, 7));
			tLoginVerifyTool.addOm(poldata, "SALECHNL",
					proposalResult.GetText(1, 8));
			tLoginVerifyTool.addOm(poldata, "CVALIDATE",
					proposalResult.GetText(1, 9));
			tLoginVerifyTool.addOm(poldata, "AGENTCOM",
					proposalResult.GetText(1, 10));
			mResponseBodyElement.addChild(poldata);
		}

		String statesql = "Select Stateflag,"
				+ "(Select Makedate From Es_Doc_Main Where Doccode=Lcc.Prtno And Busstype ='TB' Fetch First 1 Rows Only),"
				+ "Uwoperator, (Select Operator From Lccontprint Where Otherno=Lcc.Contno And Othernotype ='1' Fetch First 1 Rows Only),"
				+ "Contno,Inputoperator,Uwdate,"
				+ "(Select Makedate From Lccontprint Where Otherno=Lcc.Contno And Othernotype='1' Fetch First 1 Rows Only),"
				+ "Firsttrialoperator,Inputdate,Signdate,"
				+ "(Select Getpoloperator From Lccontgetpol Where Contno = Lcc.Contno),"
				+ "Firsttrialdate,Approvecode,"
				+ "(Select Enteraccdate From Ljtempfee Where Otherno=Lcc.Contno Order By Enteraccdate Fetch First 1 Rows Only),"
				+ "Customgetpoldate,"
				+ "(Select Scanoperator From Es_Doc_Main Where Doccode=Lcc.Prtno And Busstype='TB' And Subtype In ('TB01', 'TB04', 'TB06', 'TB10', 'TB24') Fetch First 1 Rows Only),"
				+ "Approvedate,Getpoldate From Lccont Lcc Where Prtno='"
				+ mPrtno + "' With Ur ";
		SSRS stateResult;
		stateResult = tExeSQL.execSQL(statesql);
		if (stateResult.getMaxRow() > 0) {
			OMElement statedata = tOMFactory.createOMElement("STATEDATA", null);
			tLoginVerifyTool.addOm(statedata, "POLSTATE",
					stateResult.GetText(1, 1));
			tLoginVerifyTool.addOm(statedata, "SCANDATE",
					stateResult.GetText(1, 2));
			tLoginVerifyTool.addOm(statedata, "UWOPERATOR",
					stateResult.GetText(1, 3));
			tLoginVerifyTool.addOm(statedata, "PRINTOPERATOR",
					stateResult.GetText(1, 4));
			tLoginVerifyTool.addOm(statedata, "CONTNO",
					stateResult.GetText(1, 5));
			tLoginVerifyTool.addOm(statedata, "OPERATOR",
					stateResult.GetText(1, 6));
			tLoginVerifyTool.addOm(statedata, "UWDATE",
					stateResult.GetText(1, 7));
			tLoginVerifyTool.addOm(statedata, "PRINTDATE",
					stateResult.GetText(1, 8));
			tLoginVerifyTool.addOm(statedata, "FIRSTTRIALOPERATOR",
					stateResult.GetText(1, 9));
			tLoginVerifyTool.addOm(statedata, "MAKEDATE",
					stateResult.GetText(1, 10));
			tLoginVerifyTool.addOm(statedata, "SIGNDATE",
					stateResult.GetText(1, 11));
			tLoginVerifyTool.addOm(statedata, "TAKEBACKOPERATOR",
					stateResult.GetText(1, 12));
			tLoginVerifyTool.addOm(statedata, "FIRSTTRIALDATE",
					stateResult.GetText(1, 13));
			tLoginVerifyTool.addOm(statedata, "APPROVECODE",
					stateResult.GetText(1, 14));
			tLoginVerifyTool.addOm(statedata, "OPERFEEDATE",
					stateResult.GetText(1, 15));
			tLoginVerifyTool.addOm(statedata, "TAKEBACKDATE",
					stateResult.GetText(1, 16));
			tLoginVerifyTool.addOm(statedata, "SCANOPERATOR",
					stateResult.GetText(1, 17));
			tLoginVerifyTool.addOm(statedata, "APPROVEDATE",
					stateResult.GetText(1, 18));
			tLoginVerifyTool.addOm(statedata, "TAKEBACKMAKEDATE",
					stateResult.GetText(1, 19));
			mResponseBodyElement.addChild(statedata);
		}

		String risksql = "Select Insuredname,"
				+ "(select sum(fee) from lcinsureaccfeetrace where moneytype='RP' and polno=lcp.polno) Riskcode,"
				+ "Uwcode,(Select Passflag From Lcuwmaster Where Contno=Lcp.Contno And Polno=Lcp.Polno),"
				+ "Mult,Lcp.Amnt,(Select Uwidea From Lcuwmaster Where Contno=Lcp.Contno And Polno=Lcp.Polno),"
				+ "StandPrem From Lcpol Lcp Where Prtno = '" + mPrtno
				+ "'  With Ur ";
		SSRS riskResult;
		riskResult = tExeSQL.execSQL(risksql);
		if (riskResult.getMaxRow() > 0) {
			OMElement risklist = tOMFactory.createOMElement("RISKLIST", null);
			for (int i = 1; i <= riskResult.getMaxRow(); i++) {
				OMElement riskdata = tOMFactory.createOMElement("RISKDATA",
						null);
				tLoginVerifyTool.addOm(riskdata, "INSUREDNAME",
						riskResult.GetText(i, 1));
				tLoginVerifyTool.addOm(riskdata, "RISKPREM",
						riskResult.GetText(i, 2));
				tLoginVerifyTool.addOm(riskdata, "RISKCODE",
						riskResult.GetText(i, 3));
				tLoginVerifyTool.addOm(riskdata, "PASSFLAG",
						riskResult.GetText(i, 4));
				tLoginVerifyTool.addOm(riskdata, "MULT",
						riskResult.GetText(i, 5));
				tLoginVerifyTool.addOm(riskdata, "AMNT",
						riskResult.GetText(i, 6));
				tLoginVerifyTool.addOm(riskdata, "UWIDEA",
						riskResult.GetText(i, 7));
				tLoginVerifyTool.addOm(riskdata, "BASEPREMIUM",
						riskResult.GetText(i, 8));
				risklist.addChild(riskdata);
			}
			mResponseBodyElement.addChild(risklist);
		}
		return true;
	}

}
