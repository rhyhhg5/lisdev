package com.sinosoft.lis.crminterface;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public class TestCRMInterface {

	// private Logger cLogger = Logger.getLogger(getClass());

	private DocumentBuilder cDocbuilder = null;
	{
		DocumentBuilderFactory tDocFactory = DocumentBuilderFactory
				.newInstance();
		try {
			cDocbuilder = tDocFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	private TransformerFactory cTransformerFactory = TransformerFactory
			.newInstance();

	/**
	 * 测试WebService调用主程序
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("WebService调用测试开始…");

//		String mServiceURL = "http://localhost:8080/ui/services/piccserver/queryData";
//		String mServiceURL = "http://10.252.1.111:900/services/piccserver";   //核心测试地址
		String mServiceURL ="http://10.252.4.17:8083/WSAxis2App/services/MobileService"; //电商地址
		// String mServiceURL =
		// "http://localhost:8088/ybtpre/services/YibaotongService";
		String mInFilePath = "E:\\001.xml";
		String mOutFilePath = "E:\\response.xml";
		


		InputStream mInputStream = new FileInputStream(mInFilePath);
		Reader mReader = new InputStreamReader(mInputStream, "utf-8");

		TestCRMInterface mTestUI = new TestCRMInterface();
		try {
			Document mOutXmlDoc = mTestUI.callService(mReader, mServiceURL);

			mTestUI.outputDOM(mOutXmlDoc, new FileOutputStream(mOutFilePath));
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println("WebService调用测试异常！");
			return;
		}

		System.out.println("WebService调用测试成功！");
		return;
	}

	/**
	 * 1、客户端准备数据;2、调用WebService服务;3、获取服务端处理结果数据
	 * 
	 * @param pReader
	 * @param pServiceURL
	 * @return org.w3c.dom.Document
	 * @throws Exception
	 */
	public Document callService(Reader pReader, String pServiceURL)
			throws Exception {
		long mOldTimeMillis = System.currentTimeMillis();
		long mCurTimeMillis = mOldTimeMillis;

		SOAPBodyElement[] mSOAPBodyElements = new SOAPBodyElement[1];

		InputSource mInputSource = new InputSource(pReader);
		Document mInXmlDoc=cDocbuilder.parse(mInputSource);
	
		
		System.out.println(mInXmlDoc.getDocumentElement());
		System.out.println(mInXmlDoc.toString());
		mSOAPBodyElements[0] = new SOAPBodyElement(
				mInXmlDoc.getDocumentElement());

		mCurTimeMillis = System.currentTimeMillis();
		System.out.println("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis)
				/ 1000.0 + "s");

		System.out.println("开始调用WebService服务！" + pServiceURL);

		Service mService = new Service();
		Call mCall = (Call) mService.createCall();
		mCall.setTargetEndpointAddress(pServiceURL);
		
		
		Vector mReElements = (Vector) mCall.invoke(mSOAPBodyElements);

		mCurTimeMillis = System.currentTimeMillis();
		System.out.println("调用WebService服务成功！耗时："
				+ (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");

		SOAPBodyElement mReSOAPBodyElement = (SOAPBodyElement) mReElements
				.get(0);
		return mReSOAPBodyElement.getAsDocument();
	}

	/**
	 * 将org.w3c.dom.Document转化为文件输出流
	 * 
	 * @param pNode
	 * @param pOutputStream
	 * @throws Exception
	 */
	public void outputDOM(Node pNode, OutputStream pOutputStream)
			throws Exception {
		DOMSource mDOMSource = new DOMSource(pNode);

		StreamResult mStreamResult = new StreamResult(pOutputStream);

		Transformer mTransformer = cTransformerFactory.newTransformer();
		mTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		mTransformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");

		mTransformer.transform(mDOMSource, mStreamResult);
	}
}
