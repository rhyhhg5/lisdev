package com.sinosoft.lis.crminterface;

import java.util.Iterator;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_payriskinfoquery extends CRMAbstractInterface{

	/**保单号	**/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
    private String tsql = " " ;
    
	public Crm_payriskinfoquery() {
		this.mRootName = "CRM_PAYRISKINFOQUERY";
	}
	
	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()){
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {	
                    tsql += " and contno='" + mContno + "' ";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("请录入保单号");
				return false;				
			}	
		}
		ExeSQL tExeSQL = new ExeSQL();
		
        String sql="Select (Select Payno "+
        "  From Ljapay "+
        "  Where Getnoticeno = a.Getnoticeno Fetch First 1 Rows Only) As 缴费记录号, "+
        " (Select Sumactupaymoney "+
        "  From Ljspaypersonb "+
        "  Where Getnoticeno = a.Getnoticeno "+
        "  And Paytype = 'YEL') As 本次使用保费余额, "+
        "  b.Startpaydate As 应缴日期, "+
        " (Select Enteraccdate "+
        "  From Ljapay "+
        "  Where Getnoticeno = a.Getnoticeno Fetch First 1 Rows Only) As 到账日期, "+
        "  a.Makedate As 抽档日期, "+
        "  (Select Confdate "+
        "  From Ljapay "+
        "  Where Getnoticeno = a.Getnoticeno Fetch First 1 Rows Only) As 核销日期, "+
        "  a.Paydate As 缴费截止日期, "+
        "  (Select Codename "+
        "   From Ldcode "+
        "  Where Codetype = 'dealstate' "+
        "   And Code = (Select Dealstate "+
        "  From Ljspayb "+
        "  Where Getnoticeno = a.Getnoticeno)) As 处理状态, "+
        "  a.Getnoticeno As 应收记录号, "+
        "  (Select Riskname "+
        "   From Lmriskapp "+
        "  Where Riskcode = a.Riskcode) As 险种名称, "+
        "  a.Sumduepaymoney As 每期应缴保费金额, "+
        "  a.Sumactupaymoney As 实缴保费金额, "+
                   
        "  (Select Paymode "+
        "   From Ljtempfeeclass "+
        "  Where Tempfeeno = a.Getnoticeno Fetch First 1 Rows Only) As 缴费方式, "+
        "  a.Payintv As 缴费频次," +
        " (select senddate from lybanklog where serialno =(select serialno from lysendtobank where paycode =(select payno from ljapay where getnoticeno =a.getnoticeno fetch first 1 rows only))) as 扣款失败时间, "+
        " (select outfile||infile from lybanklog where serialno =(select serialno from lysendtobank where paycode =(select payno from ljapay where getnoticeno =a.getnoticeno fetch first 1 rows only))) as 扣款失败原因 "+
        "  From Ljspaypersonb a "+
        "  Left Join Ljspayb b On a.Getnoticeno = b.Getnoticeno "+
        "  Where 1 = 1 " 
        +tsql+""
        +" union all"
        +"  SELECT "
        +"  a.payno  AS 缴费记录号, "
        +"  (SELECT SUMACTUPAYMONEY FROM LJaPAYPERSON WHERE getnoticeno=a.GETNOTICENO and paytype='YEL') AS 本次使用保费余额, "
        +"  a.paydate  AS 应缴日期, "
        +"  a.ENTERACCDATE  AS 到账日期, "
        +" a.MAKEDATE AS 抽档日期, "
        +" b.CONFDATE  AS 核销日期, "
        +" a.PAYDATE AS 缴费截止日期, "
        +" '催收成功' as 处理状态, "
        +" a.getnoticeno as 应收记录号, "
        +" (select riskname from lmriskapp where riskcode=a.riskcode ) as 险种名称, "
        +" a.sumduepaymoney as 每期应缴保费金额, "
        +" a.SUMACTUPAYMONEY as 实缴保费金额, "
        +" (select paymode from ljtempfeeclass where tempfeeno=a.getnoticeno fetch first 1 rows only ) as 缴费方式, "
        +" a.payintv as 缴费频次, "
        +" (Select Senddate "
        +" From Lybanklog "
        +" Where Serialno = (Select Serialno "
        +" From Lysendtobank "
        +" Where Paycode = b.payno fetch first 1 rows only)) As 扣款失败时间, "
        +"  (Select Outfile || Infile "
        +" From Lybanklog "
        +" Where Serialno = (Select Serialno "
        +" From Lysendtobank "
        +" Where Paycode = b.payno fetch first 1 rows only)) As 扣款失败原因 "
        +" FROM LJaPAYPERSON a left join ljapay b on a.payno=b.payno "
        +" where 1=1 "
        +tsql+""
        +" and a.paycount='1' "
        +" with ur ";
		
		SSRS sResult;
		sResult = tExeSQL.execSQL(sql);
		if (sResult.getMaxRow()>0) {
			OMFactory tOMFactory = OMAbstractFactory.getOMFactory();		
			LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
			OMElement paylist = tOMFactory.createOMElement("PAYLIST", null);
			for (int i = 1; i <= sResult.getMaxRow(); i++){
				OMElement paydata = tOMFactory.createOMElement("PAYDATA", null);
				tLoginVerifyTool.addOm(paydata, "PAYNO",
						sResult.GetText(i, 1));
//				tLoginVerifyTool.addOm(paydata, "YELMONEY",
//						sResult.GetText(i, 2));
				tLoginVerifyTool.addOm(paydata, "LASTPAYTODATE",
						sResult.GetText(i, 3));
				tLoginVerifyTool.addOm(paydata, "ENTERACCDATE",
						sResult.GetText(i, 4));
//				tLoginVerifyTool.addOm(paydata, "PULLDATE",
//						sResult.GetText(i, 5));
				tLoginVerifyTool.addOm(paydata, "CONFDATE",
						sResult.GetText(i, 6));
				tLoginVerifyTool.addOm(paydata, "PAYDATE",
						sResult.GetText(i, 7));
				tLoginVerifyTool.addOm(paydata, "DEALSTATE",
						sResult.GetText(i, 8));
				tLoginVerifyTool.addOm(paydata, "GETNOTICENO",
						sResult.GetText(i, 9));
				tLoginVerifyTool.addOm(paydata, "RISKNAME",
						sResult.GetText(i, 10));
				tLoginVerifyTool.addOm(paydata, "SUMDUEPAYMONEY",
						sResult.GetText(i, 11));
				tLoginVerifyTool.addOm(paydata, "SUMACTUPAYMONEY",
						sResult.GetText(i, 12));
				tLoginVerifyTool.addOm(paydata, "PAYMODE",
						sResult.GetText(i, 13));
				tLoginVerifyTool.addOm(paydata, "PAYINTV",
						sResult.GetText(i, 14));
                
                tLoginVerifyTool.addOm(paydata, "BANKDEALDATE",
                        sResult.GetText(i, 15));
                tLoginVerifyTool.addOm(paydata, "FAILREASON",
                        sResult.GetText(i, 16));
                
				paylist.addChild(paydata);
			}
			mResponseBodyElement.addChild(paylist);
		}
		return true;
	}
}
