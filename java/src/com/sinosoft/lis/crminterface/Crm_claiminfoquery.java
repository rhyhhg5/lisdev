package com.sinosoft.lis.crminterface;

import java.util.Iterator;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_claiminfoquery extends CRMAbstractInterface {

	/** 赔案号 **/
	private static final String CASENO = "CASENO";
	private String mCaseno = "";
	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/** 团体保单号 **/
	private static final String GRPCONTNO = "GRPCONTNO";
	private String mGrpcontno = "";

	public Crm_claiminfoquery() {
		this.mRootName = "CRM_CLAIMINFOQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CASENO.equals(tChildElement.getLocalName())) {
				mCaseno = tChildElement.getText();
				if ((!"".equals(mCaseno)) && mCaseno != null) {
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("请录入赔案号");
				return false;
			}
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					tsql += " and a.contno='" + mContno + "' ";
					;
				}
				continue;
			}
			if (GRPCONTNO.equals(tChildElement.getLocalName())) {
				mGrpcontno = tChildElement.getText();
				if ((!"".equals(mGrpcontno)) && mGrpcontno != null) {
					tsql += " and a.grpcontno='" + mGrpcontno + "' ";
				}
				continue;
			}
		}
		if ((!"".equals(mContno) && mContno != null)
				&& (!"".equals(mGrpcontno) && mGrpcontno != null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("保单号、团体保单号只能录其一");
			return false;
		}
		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

		String clainsql = "select b.caseno 赔案号,b.rgtdate 立案日期,"
				+ "b.rgtstate 赔案状态,"
				+ "a.rgtantname 申请人姓名,"
				+ "a.rgtantphone 申请人电话,"
				+ "a.relation 与出险人关系,"
				+ "a.rgtdate 申请日期,"
				+ "a.rgttype 报案方式,"
				+ "b.customername 出险人姓名,"
				+ "b.customersex 出险人性别,"
				+ "b.customerage 出险人年龄,"
				+ "sum(c.realpay) 赔款合计,"
				+ "b.endcasedate 结案日期,"
				+ "b.mngcom 受理机构,"
				+ "c.givetype 理赔结论  "
				+ " from llregister a,llcase b,llclaim c "
				+ " where a.rgtno=b.rgtno "
				+ " and b.caseno=c.caseno "
				+ " and b.caseno='"
				+ mCaseno
				+ "' group by b.caseno,b.rgtdate,b.rgtstate,a.rgtantname,a.rgtantphone,a.relation,a.rgtdate,a.rgttype,b.customername,b.customersex,b.customerage,b.endcasedate,b.mngcom,c.givetype "
				+ " with ur ";
		SSRS clainResult;
		clainResult = tExeSQL.execSQL(clainsql);
		if (clainResult.getMaxRow() > 0) {
			OMElement claimdata = tOMFactory.createOMElement("CLAIMDATA", null);
			tLoginVerifyTool.addOm(claimdata, "CASENO",
					clainResult.GetText(1, 1));
			tLoginVerifyTool.addOm(claimdata, "RGTDATE",
					clainResult.GetText(1, 2));
			tLoginVerifyTool.addOm(claimdata, "RGTSTATE",
					clainResult.GetText(1, 3));
			tLoginVerifyTool.addOm(claimdata, "RGTANTNAME",
					clainResult.GetText(1, 4));
			tLoginVerifyTool.addOm(claimdata, "RGTANTPHONE",
					clainResult.GetText(1, 5));
			tLoginVerifyTool.addOm(claimdata, "RELATION",
					clainResult.GetText(1, 6));
//			tLoginVerifyTool.addOm(claimdata, "RGTDATE",
//					clainResult.GetText(1, 7));
			tLoginVerifyTool.addOm(claimdata, "RGTTYPE",
					clainResult.GetText(1, 8));
			tLoginVerifyTool.addOm(claimdata, "CUSTOMERNAME",
					clainResult.GetText(1, 9));
			tLoginVerifyTool.addOm(claimdata, "CUSTOMERSEX",
					clainResult.GetText(1, 10));
			tLoginVerifyTool.addOm(claimdata, "CUSTOMERAGE",
					clainResult.GetText(1, 11));
			tLoginVerifyTool.addOm(claimdata, "REALPAY",
					clainResult.GetText(1, 12));
			tLoginVerifyTool.addOm(claimdata, "ENDCASEDATE",
					clainResult.GetText(1, 13));
			tLoginVerifyTool.addOm(claimdata, "MANAGECOM",
					clainResult.GetText(1, 14));
			tLoginVerifyTool.addOm(claimdata, "GIVETYPE",
					clainResult.GetText(1, 15));
			mResponseBodyElement.addChild(claimdata);
		}

		String paysql = "select b.drawer 领取人姓名,b.drawerid 领取人身份证号码,"
				+ "b.paymode 领取方式,b.bankaccno 领取账号,b.confdate 划账时间,"
				+ "b.bankcode 领取银行 from  ljagetclaim a, ljaget b "
				+ " where a.actugetno=b.actugetno  and a.otherno='" + mCaseno
				+ "' " + tsql + " with ur ";
		SSRS payResult;
		payResult = tExeSQL.execSQL(paysql);
		if (payResult.getMaxRow() > 0) {
			OMElement paydata = tOMFactory.createOMElement("PAYDATA", null);
			tLoginVerifyTool.addOm(paydata, "DRAWER", payResult.GetText(1, 1));
			tLoginVerifyTool
					.addOm(paydata, "DRAWERID", payResult.GetText(1, 2));
			tLoginVerifyTool.addOm(paydata, "PAYMODE", payResult.GetText(1, 3));
			tLoginVerifyTool.addOm(paydata, "BANKACCNO",
					payResult.GetText(1, 4));
			tLoginVerifyTool
					.addOm(paydata, "CONFDATE", payResult.GetText(1, 5));
			tLoginVerifyTool
					.addOm(paydata, "BANKCODE", payResult.GetText(1, 6));
			mResponseBodyElement.addChild(paydata);
		}
		String subreportsql = "select a.subrptno 事件号,a.accdate 发生日期,"
				+ "a.accplace 发生地点,a.InHospitalDate 入院日期,"
				+ "a.OutHospitalDate 出院日期,a.AccidentType 事件类型,"
				+ "a.AccDesc 事件信息  from  llsubreport a,"
				+ "llcaserela b where a.subrptno=b.subrptno "
				+ "and b.caseno='" + mCaseno + "' with ur ";
		SSRS subreportResult;
		subreportResult = tExeSQL.execSQL(subreportsql);
		if (subreportResult.getMaxRow() > 0) {
			OMElement subreportlist = tOMFactory.createOMElement(
					"SUBREPORTLIST", null);
			for (int i = 1; i <= subreportResult.getMaxRow(); i++) {
				OMElement subreportdata = tOMFactory.createOMElement(
						"SUBREPORTDATA", null);
				tLoginVerifyTool.addOm(subreportdata, "SUBRPTNO",
						subreportResult.GetText(i, 1));
				tLoginVerifyTool.addOm(subreportdata, "ACCDATE",
						subreportResult.GetText(i, 2));
				tLoginVerifyTool.addOm(subreportdata, "ACCPLACE",
						subreportResult.GetText(i, 3));
				tLoginVerifyTool.addOm(subreportdata, "INHOSPITALDATE",
						subreportResult.GetText(i, 4));
				tLoginVerifyTool.addOm(subreportdata, "OUTHOSPITALDATE",
						subreportResult.GetText(i, 5));
				tLoginVerifyTool.addOm(subreportdata, "ACCIDENTTYPE",
						subreportResult.GetText(i, 6));
				tLoginVerifyTool.addOm(subreportdata, "ACCDESC",
						subreportResult.GetText(i, 7));
				subreportlist.addChild(subreportdata);
			}
			mResponseBodyElement.addChild(subreportlist);
		}
		String risksql = "select "
				+ "(select riskname from lmriskapp where riskcode=a.riskcode) 险种名称,"
				+ "a.riskcode 险种代码,"
				+ "(select getdutyname from lmdutygetclm where getdutycode=a.getdutycode and getdutykind=a.getdutykind) 给付责任,"
				+ "a.realpay 实赔金额   from  llclaimdetail a "
				+ " where a.caseno='" + mCaseno + "'" + tsql + " with ur ";
		SSRS riskResult;
		riskResult = tExeSQL.execSQL(risksql);
		if (riskResult.getMaxRow() > 0) {
			OMElement risklist = tOMFactory.createOMElement("RISKLIST", null);
			for (int j = 1; j <= riskResult.getMaxRow(); j++) {
				OMElement riskdata = tOMFactory.createOMElement("RISKDATA",
						null);
				tLoginVerifyTool.addOm(riskdata, "RISKNAME",
						riskResult.GetText(j, 1));
				tLoginVerifyTool.addOm(riskdata, "RISKCODE",
						riskResult.GetText(j, 2));
				tLoginVerifyTool.addOm(riskdata, "GETDUTYNAME",
						riskResult.GetText(j, 3));
				tLoginVerifyTool.addOm(riskdata, "REALPAY",
						riskResult.GetText(j, 4));
				risklist.addChild(riskdata);
			}
			mResponseBodyElement.addChild(risklist);
		}
		return true;
	}
}
