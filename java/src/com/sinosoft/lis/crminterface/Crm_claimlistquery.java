package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_claimlistquery extends CRMAbstractInterface {

	/** 当前页码 **/
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/** 每页记录条数 **/
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/** 赔案号 **/
	private static final String CASENO = "CASENO";
	private String mCase = "";
	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/** 团体保单号 **/
	private static final String GRPCONTNO = "GRPCONTNO";
	private String mGrpcontno = "";
	/** 理赔出险人姓名 **/
	private static final String CUSTOMERNAME = "CUSTOMERNAME";
	private String mCustomername = "";
	/** 出险人证件号码 **/
	private static final String CUSTOMERNO = "CUSTOMERNO";
	private String mCustomerno = "";
	/** 查询起始日期 */
	private static final String RGTDATESTART = "RGTDATESTART";
	private String mRgtdatestart = "";
	/** 查询截止日期 */
	private static final String RGTDATEEND = "RGTDATEEND";
	private String mRgtdateend = "";

	public Crm_claimlistquery() {
		this.mRootName = "CRM_CLAIMLISTQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if ((!"".equals(mPageindex)) && mPageindex != null) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数均不能为空");
				return false;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if ((!"".equals(mPagesize)) && mPagesize != null) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数均不能为空");
				return false;
			}
			if (CASENO.equals(tChildElement.getLocalName())) {
				mCase = tChildElement.getText();
				if ((!"".equals(mCase)) && mCase != null) {
					tsql += " and a.caseno='" + mCase + "' ";
				}
				continue;
			}
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					tsql += " and b.contno='" + mContno + "' ";
				}
				continue;
			}
			if (GRPCONTNO.equals(tChildElement.getLocalName())) {
				mGrpcontno = tChildElement.getText();
				if ((!"".equals(mGrpcontno)) && mGrpcontno != null) {
					tsql += " and b.grpcontno='" + mGrpcontno + "' ";
				}
				continue;
			}
			if (CUSTOMERNAME.equals(tChildElement.getLocalName())) {
				mCustomername = tChildElement.getText();
				if ((!"".equals(mCustomername)) && mCustomername != null) {
					tsql += " and a.customername like '" + mCustomername
							+ "%' ";
				}
				continue;
			}
			if (CUSTOMERNO.equals(tChildElement.getLocalName())) {
				mCustomerno = tChildElement.getText();
				if ((!"".equals(mCustomerno)) && mCustomerno != null) {
					tsql += " and a.idno = '" + mCustomerno
							+ "' ";
				}
				continue;
			}
			if (RGTDATESTART.equals(tChildElement.getLocalName())) {
				mRgtdatestart = tChildElement.getText();
				if ((!"".equals(mRgtdatestart)) && mRgtdatestart != null) {
					tsql += " and a.rgtdate >= to_date('" + mRgtdatestart
							+ "', 'yyyy-mm-dd') ";
				}
				continue;
			}
			if (RGTDATEEND.equals(tChildElement.getLocalName())) {
				mRgtdateend = tChildElement.getText();
				if ((!"".equals(mRgtdateend)) && mRgtdateend != null) {
					tsql += " and a.rgtdate <= to_date('" + mRgtdateend
							+ "', 'yyyy-mm-dd') ";
				}
				continue;
			}
		}
			if ((!"".equals(mGrpcontno) && mGrpcontno != null)
					&& (!"".equals(mContno) && mContno != null)) {
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("团体保单号、保单号只能录入其一");
				return false;
			}

			if (("".equals(mCase) || mCase == null)
					&& ("".equals(mContno) || mContno == null)
					&& ("".equals(mCustomername) || mCustomername == null)
					&& ("".equals(mGrpcontno) || mGrpcontno == null)
					&& ("".equals(mCustomerno) || mCustomerno == null)) {
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("赔案号、保单号或团体保单号、出险人姓名、出险人证件号码必录其中之一");
				return false;
			
		}
		String sql = "select distinct a.caseno 赔案号,a.customername 出险人姓名,"
				+ "a.rgtdate 立案日期,a.rgtstate 赔案状态  "
				+ "from llcase a left join  llclaimpolicy b  on a.caseno=b.caseno"
				+ " where 1=1 " + tsql;
		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		String numsql = "select count(*) from (" + sql + ") cou with ur";
		SSRS numResult;
		numResult = tExeSQL.execSQL(numsql);
		if (numResult.getMaxRow() > 0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
					numResult.GetText(1, 1));
		}
		String ssql = "select * from (select c.*,rownumber() over() as rn from (" + sql
				+ ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		SSRS sResult;
		sResult = tExeSQL.execSQL(ssql);
		if (sResult.getMaxRow() > 0) {
			OMElement claimlist = tOMFactory.createOMElement("CLAIMLIST", null);
			for (int i = 1; i <= sResult.getMaxRow(); i++) {
				OMElement claimdata = tOMFactory.createOMElement("CLAIMDATA",
						null);
				tLoginVerifyTool.addOm(claimdata, "CASENO",
						sResult.GetText(i, 1));
				tLoginVerifyTool.addOm(claimdata, "CUSTOMERNAME",
						sResult.GetText(i, 2));
				tLoginVerifyTool.addOm(claimdata, "RGTDATE",
						sResult.GetText(i, 3));
				tLoginVerifyTool.addOm(claimdata, "RGTSTATE",
						sResult.GetText(i, 4));
				claimlist.addChild(claimdata);
			}
			mResponseBodyElement.addChild(claimlist);
		}
		return true;

	}

}
