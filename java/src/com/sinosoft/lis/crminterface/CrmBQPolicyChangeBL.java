package com.sinosoft.lis.crminterface;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.task.CommonBL;
import com.sinosoft.task.EasyEdorBL;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * CRM电话变更业务逻辑处理接口
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class CrmBQPolicyChangeBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGI = null;  //操作员信息
    private LICrmPolicyChangeSet mLICrmPolicyChangeSet = null;
    private String mCurDate = PubFun.getCurrentDate();  //当前日期
    private String mCurTime = PubFun.getCurrentTime();  //当前时间

    private MMap map = new MMap();  //待提交的数据集合

    public CrmBQPolicyChangeBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     *
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if(getSubmitData(cInputData, operate) == null)
        {
            return false;
        }

        return true;
    }

    /**
     * getSubmitData
     *
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return boolean：true提交成功, false提交失败
     */
    private MMap getSubmitData(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        boolean sucFlag = true;  //全部成功标志
        for(int i = 1; i <= mLICrmPolicyChangeSet.size(); i++)
        {
            LICrmPolicyChangeSchema schema = mLICrmPolicyChangeSet.get(i);

            sucFlag = sucFlag && dealOneChange(schema);
        }

        return sucFlag;
    }

    /**
     * 处理一个电话变更
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealOneChange(LICrmPolicyChangeSchema crmSchema)
    {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(crmSchema.getContNo());
        if(!tLCContDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "CrmBQPolicyChangeBL";
            tError.functionName = "dealOneChange";
            tError.errorMessage = "没有查询到保单信息" + crmSchema.getContNo();
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        //得到工单信息
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(tLCContDB.getAppntNo());
        tLGWorkSchema.setTypeNo("0300001");
        tLGWorkSchema.setContNo(crmSchema.getContNo());
        tLGWorkSchema.setApplyTypeNo("3");
        tLGWorkSchema.setAcceptWayNo("6");
        tLGWorkSchema.setAcceptDate(mCurDate);
        tLGWorkSchema.setRemark("CRM调用简易保全生成");

        //得到客户投保信息
        LPAppntSchema tLPAppntSchema = new LPAppntSchema();
        tLPAppntSchema.setContNo(crmSchema.getContNo());
        tLPAppntSchema.setAppntNo(tLCContDB.getAppntNo());

        //得到客户保单联系地址
        //QULQ ADD 简易保全CRM问题
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(crmSchema.getContNo());
        tLCAppntDB.getInfo();
        LCAddressDB tLCAddressDB = new LCAddressDB();
        tLCAddressDB.setCustomerNo(tLCContDB.getAppntNo());
        tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo()==null?"1":tLCAppntDB.getAddressNo());
        tLCAddressDB.getInfo();
        LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        tLCAddressSchema.setSchema(tLCAddressDB.getSchema());

        //判断在此时间后是否有在柜台做过修改
        LPEdorItemDB tLPEdorItemDB=new LPEdorItemDB();
        String sql="select * from lpedoritem where contno='"+crmSchema.getContNo()+"' and edortype='AD' ";
            sql+="and modifydate='"+crmSchema.getMakeDate()+"' and modifytime>'"+crmSchema.getChangeTime()+"'";
        LPEdorItemSet tLPEdorItemSet=tLPEdorItemDB.executeQuery(sql);
        LPAddressSet tLPAddressSet=new LPAddressSet();
        if(tLPEdorItemSet!=null){
            for(int i=1;i<=tLPEdorItemSet.size();i++){
                LPAddressDB tLPAddressDB=new LPAddressDB();
                LPEdorItemSchema tLPEdorItemSchema=tLPEdorItemSet.get(1);
                sql="select * from LPAddress where edorno='"+tLPEdorItemSchema.getEdorNo()+"' and edortype='AD' and modifydate='"+crmSchema.getMakeDate()+"' and modifytime>='"+crmSchema.getChangeTime()+"'";
                tLPAddressSet.add(tLPAddressDB.executeQuery(sql));
            }
            
        }
        
//      tLCAddressSchema.setCustomerNo(tLCContDB.getAppntNo());
        if(checkModify(tLCAddressSchema,tLPAddressSet,"HomeAddress")){
            tLCAddressSchema.setHomeAddress(crmSchema.getHomeAddress()==null?tLCAddressSchema.getHomeAddress():crmSchema.getHomeAddress());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"HomeZipCode")){
            tLCAddressSchema.setHomeZipCode(crmSchema.getHomeZipCode()==null?tLCAddressSchema.getHomeZipCode():crmSchema.getHomeZipCode());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"HomePhone")){
            tLCAddressSchema.setHomePhone(crmSchema.getHomePhone()==null?tLCAddressSchema.getHomePhone():crmSchema.getHomePhone());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"HomeFax")){
            tLCAddressSchema.setHomeFax(crmSchema.getHomeFax()==null?tLCAddressSchema.getHomeFax():crmSchema.getHomeFax());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"CompanyAddress")){
            tLCAddressSchema.setCompanyAddress(crmSchema.getCompanyAddress()==null?tLCAddressSchema.getCompanyAddress():crmSchema.getCompanyAddress());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"CompanyZipCode")){
            tLCAddressSchema.setCompanyZipCode(crmSchema.getCompanyZipCode()==null?tLCAddressSchema.getCompanyZipCode():crmSchema.getCompanyZipCode());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"CompanyPhone")){
            tLCAddressSchema.setCompanyPhone(crmSchema.getCompanyPhone()==null?tLCAddressSchema.getCompanyPhone():crmSchema.getCompanyPhone());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"CompanyFax")){
            tLCAddressSchema.setCompanyFax(crmSchema.getCompanyFax()==null?tLCAddressSchema.getCompanyFax():crmSchema.getCompanyFax());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"PostalAddress")){
            tLCAddressSchema.setPostalAddress(crmSchema.getPostalAddress()==null?tLCAddressSchema.getPostalAddress():crmSchema.getPostalAddress());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"ZipCode")){
            tLCAddressSchema.setZipCode(crmSchema.getZipCode()==null?tLCAddressSchema.getZipCode():crmSchema.getZipCode());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"Phone")){
            tLCAddressSchema.setPhone(crmSchema.getPhone()==null?tLCAddressSchema.getPhone():crmSchema.getPhone());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"Fax")){
            tLCAddressSchema.setFax(crmSchema.getFax()==null?tLCAddressSchema.getFax():crmSchema.getFax());
        }
        if(checkModify(tLCAddressSchema,tLPAddressSet,"Mobile")){
            tLCAddressSchema.setMobile(crmSchema.getMobile()==null?tLCAddressSchema.getMobile():crmSchema.getMobile());
        }

        //得到关联保单
        String workNo = CommonBL.createWorkNo();
        LPContSet tLPContSet = new LPContSet();
        LPContSchema tLPContSchema = new LPContSchema();
        tLPContSchema.setEdorNo(workNo);
        tLPContSchema.setEdorType(BQ.EDORTYPE_AD);
        tLPContSchema.setContNo(tLCContDB.getContNo());
        tLPContSet.add(tLPContSchema);

        //CRM操作员信息
        mGI.Operator = "crm";
        mGI.ManageCom = tLCContDB.getManageCom();
        mGI.ComCode = mGI.ManageCom;

        VData data = new VData();
        data.add(mGI);
        data.add(tLGWorkSchema);
        data.add(tLPAppntSchema);
        data.add(tLCAddressSchema);
        data.add(tLPContSet);

        boolean flag = true;  //本次变更是否成功
        EasyEdorBL tEasyEdorBL = new EasyEdorBL();
        if(!tEasyEdorBL.submitData(data))
        {
            crmSchema.setState("0");
            mErrors.copyAllErrors(tEasyEdorBL.mErrors);
            flag = false;
        }

        //更新接口表处理状态
        crmSchema.setState("1");

        MMap tMMap = new MMap(); //待提交的数据集合
        tMMap.put(crmSchema, SysConst.UPDATE);
        data.clear();
        data.add(tMMap);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CrmPolicyChangeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            flag = false;
        }

        //flag = true;

        return flag;
    }
    /**
     * checkModify
     *判断电话保全后是否有柜台保全，如果有不允许再更新
     * @param tLCAddressSchema tLPAddressSchema flag：dealOneChange中传入的VData对象
     * @return boolean：true允许修改, false不允许修改
     */
    private boolean checkModify(LCAddressSchema tLCAddressSchema,LPAddressSet tLPAddressSet,String flag){
        for(int i=1;i<=tLPAddressSet.size();i++){
            LPAddressSchema tLPAddressSchema=tLPAddressSet.get(i);
            if(flag.equals("HomeAddress")){
                if(tLCAddressSchema.getHomeAddress()!=null && !tLCAddressSchema.getHomeAddress().equals(tLPAddressSchema.getHomeAddress())){
                    return false;
                }
                if(tLCAddressSchema.getHomeAddress()==null && tLPAddressSchema.getHomeAddress()!=null){
                    return false;
                }
            }else if(flag.equals("HomeZipCode")){
                if(tLCAddressSchema.getHomeZipCode()!=null && !tLCAddressSchema.getHomeZipCode().equals(tLPAddressSchema.getHomeZipCode())){
                    return false;
                }
                if(tLCAddressSchema.getHomeZipCode()==null && tLPAddressSchema.getHomeZipCode()!=null){
                    return false;
                }
            }else if(flag.equals("HomePhone")){
                if(tLCAddressSchema.getHomePhone()!=null && !tLCAddressSchema.getHomePhone().equals(tLPAddressSchema.getHomePhone())){
                    return false;
                }
                if(tLCAddressSchema.getHomePhone()==null && tLPAddressSchema.getHomePhone()!=null){
                    return false;
                }
            }else if(flag.equals("HomeFax")){
                if(tLCAddressSchema.getHomeFax()!=null && !tLCAddressSchema.getHomeFax().equals(tLPAddressSchema.getHomeFax())){
                    return false;
                }
                if(tLCAddressSchema.getHomeFax()==null && tLPAddressSchema.getHomeFax()!=null){
                    return false;
                }
            }else if(flag.equals("CompanyAddress")){
                if(tLCAddressSchema.getCompanyAddress()!=null && !tLCAddressSchema.getCompanyAddress().equals(tLPAddressSchema.getCompanyAddress())){
                    return false;
                }
                if(tLCAddressSchema.getCompanyAddress()==null && tLPAddressSchema.getCompanyAddress()!=null){
                    return false;
                }
            }else if(flag.equals("CompanyZipCode")){
                if(tLCAddressSchema.getCompanyZipCode()!=null && !tLCAddressSchema.getCompanyZipCode().equals(tLPAddressSchema.getCompanyZipCode())){
                    return false;
                }
                if(tLCAddressSchema.getCompanyZipCode()==null && tLPAddressSchema.getCompanyZipCode()!=null){
                    return false;
                }
            }else if(flag.equals("CompanyPhone")){
                if(tLCAddressSchema.getCompanyPhone()!=null && !tLCAddressSchema.getCompanyPhone().equals(tLPAddressSchema.getCompanyPhone())){
                    return false;
                }
                if(tLCAddressSchema.getCompanyPhone()==null && tLPAddressSchema.getCompanyPhone()!=null){
                    return false;
                }
            }else if(flag.equals("CompanyFax")){
                if(tLCAddressSchema.getCompanyFax()!=null && !tLCAddressSchema.getCompanyFax().equals(tLPAddressSchema.getCompanyFax())){
                    return false;
                }
                if(tLCAddressSchema.getCompanyFax()==null && tLPAddressSchema.getCompanyFax()!=null){
                    return false;
                }
            }else if(flag.equals("PostalAddress")){
                if(tLCAddressSchema.getPostalAddress()!=null && !tLCAddressSchema.getPostalAddress().equals(tLPAddressSchema.getPostalAddress())){
                    return false;
                }
            }else if(flag.equals("ZipCode")){
                if(tLCAddressSchema.getZipCode()!=null && !tLCAddressSchema.getZipCode().equals(tLPAddressSchema.getZipCode())){
                    return false;
                }
                if(tLCAddressSchema.getZipCode()==null && tLPAddressSchema.getZipCode()!=null){
                    return false;
                }
            }else if(flag.equals("Phone")){
                if(tLCAddressSchema.getPhone()!=null && !tLCAddressSchema.getPhone().equals(tLPAddressSchema.getPhone())){
                    return false;
                }
                if(tLCAddressSchema.getPhone()==null && tLPAddressSchema.getPhone()!=null){
                    return false;
                }
            }else if(flag.equals("Fax")){
                if(tLCAddressSchema.getFax()!=null && !tLCAddressSchema.getFax().equals(tLPAddressSchema.getFax())){
                    return false;
                }
                if(tLCAddressSchema.getFax()==null && tLPAddressSchema.getFax()!=null){
                    return false;
                }
            }else if(flag.equals("Mobile")){
                if(tLCAddressSchema.getMobile()!=null && !tLCAddressSchema.getMobile().equals(tLPAddressSchema.getMobile())){
                    return false;
                }
                if(tLCAddressSchema.getMobile()==null && tLPAddressSchema.getMobile()!=null){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mLICrmPolicyChangeSet = (LICrmPolicyChangeSet) data
                                .getObjectByObjectName(
                                    "LICrmPolicyChangeSet", 0);
        mGI = (GlobalInput)data
                       .getObjectByObjectName("GlobalInput", 0);

        if(mLICrmPolicyChangeSet == null || mLICrmPolicyChangeSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "CrmPolicyChangeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请传入电话变更信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    
    /**
     * 调试方法
     */
    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor0";
        gi.ComCode = "86";
/*
        LICrmPolicyChangeSchema schema = new LICrmPolicyChangeSchema();
        schema.setBatchNo("1");
        schema.setContNo("00082345901");
        schema.setChangePhoneDate("2007-01-01");
        schema.setPostalAddress("北京市朝阳区安外北苑大洋坊8号");
        schema.setZipCode("333333");
        schema.setPhone("123456789");
        schema.setFax("12222222");
        schema.setHomeAddress("北京市朝阳区安外北苑大洋坊8号2-3-601");
        schema.setHomeZipCode("111111");
        schema.setHomePhone("88776655");
        schema.setHomeFax("12345678");
        schema.setCompanyAddress("引无数英雄尽折腰");
        schema.setCompanyZipCode("876543");
        schema.setCompanyPhone("23054897");
        schema.setCompanyFax("0.231546");
        schema.setMobile("23154786254");
        schema.setEMail("lin@sina.com");
        schema.setBP("2222");
        schema.setState("0");
        schema.setOperator("endor");
        schema.setMakeDate("2006-01-1");
        schema.setMakeTime("10:1:1");
*/

        LICrmPolicyChangeSet set = new LICrmPolicyChangeSet();
        LICrmPolicyChangeDB temp = new LICrmPolicyChangeDB();
        set = temp.executeQuery("select * from LICrmPolicyChange where batchno ='00000000000000000049'");
//        set.add(schema);

        VData data = new VData();
        data.add(gi);
        data.add(set);

        CrmBQPolicyChangeBL bl = new CrmBQPolicyChangeBL();
        if(!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
