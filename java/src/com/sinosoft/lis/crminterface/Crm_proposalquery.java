package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.ecwebservice.utils.AgentCodeTransformation;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_proposalquery extends CRMAbstractInterface{

	/**当前页码**/
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/**每页记录条数**/
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/**印刷号码**/
	private static final String PRTNO = "PRTNO";
	private String mPrtno = "";
	/**投保人姓名**/
	private static final String APPNTNAME = "APPNTNAME";
	private String mAppntname = "";
	/**被保人姓名**/
	private static final String INSUREDNAME = "INSUREDNAME";
	private String mInsuredname = "";
	/**申请日期**/
	private static final String POLAPPLYDATE = "POLAPPLYDATE";
	private String mPolapplydate = "";
	/**生效日期**/
	private static final String CVALIDATE = "CVALIDATE";
	private String mCvalidate = "";
	/**业务员代码**/
	private static final String AGENTCODE = "AGENTCODE";
	private String mAgentcode = "";
	/**业务归档号**/
	private static final String ARCHIVENO = "ARCHIVENO";
	private String mArchivedo = "";
	/**管理机构**/
	private static final String MANAGECOM = "MANAGECOM";
	private String mManagecom = "";
	
    private String  mSQL = "";
    private String mArchiveno = "";

	
	public Crm_proposalquery() {
		this.mRootName = "CRM_PROPOSALQUERY";
	}
	
	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()){
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if (mPageindex!=null&&!"".equals(mPageindex)) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码、每页记录条数均不能为空");
				return false;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if (mPagesize!=null&&!"".equals(mPagesize)) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码、每页记录条数均不能为空");
				return false;
			}
			if (PRTNO.equals(tChildElement.getLocalName())) {
				mPrtno = tChildElement.getText();
				if ((!"".equals(mPrtno)) && mPrtno != null) {
                    mSQL = mSQL+" and lcc.prtno='"+mPrtno+"' ";
				}
				continue;
			}
			if (APPNTNAME.equals(tChildElement.getLocalName())) {
				mAppntname = tChildElement.getText();
				if ((!"".equals(mAppntname)) && mAppntname != null) {
                    mSQL = mSQL + " and lcc.appntname='"+mAppntname+"'";
				}
				continue;
			}
			if (INSUREDNAME.equals(tChildElement.getLocalName())) {
				mInsuredname = tChildElement.getText();
				if ((!"".equals(mInsuredname)) && mInsuredname != null) {
                    mSQL = mSQL + " and lci.name='"+mInsuredname+"'";
				}
				continue;
			}
			if (POLAPPLYDATE.equals(tChildElement.getLocalName())) {
				mPolapplydate = tChildElement.getText();
				if ((!"".equals(mPolapplydate)) && mPolapplydate != null) {
                    mSQL = mSQL + " and lcc.Polapplydate='"+mPolapplydate+"'";
				}
				continue;
			}
			if (CVALIDATE.equals(tChildElement.getLocalName())) {
				mCvalidate = tChildElement.getText();
				if ((!"".equals(mCvalidate)) && mCvalidate != null) {
                    mSQL = mSQL + " and lcc.cvalidate='"+mCvalidate+"'";
				}
				continue;
			}
			if (AGENTCODE.equals(tChildElement.getLocalName())) {
				mAgentcode = tChildElement.getText();
				if ((!"".equals(mAgentcode)) && mAgentcode != null) {
					AgentCodeTransformation a = new AgentCodeTransformation();
					if(!a.AgentCode(mAgentcode, "Y")){
						System.out.println(a.getMessage());
						mErrCode = ERRCODE_CHECK;
						mErrors.addOneError(a.getMessage());
						return false;
					}
					mAgentcode=a.getResult();
                    mSQL = mSQL + " and lcc.agentcode='"+mAgentcode+"'";
				}
				
				continue;
			}
			if (ARCHIVENO.equals(tChildElement.getLocalName())) {
				mArchivedo = tChildElement.getText();
				if ((!"".equals(mArchivedo)) && mArchivedo != null) {
                    mSQL = mSQL + " and exists (select 1 from es_doc_main where Doccode = Lcc.Prtno And Busstype = 'TB' And Subtype In ('TB01', 'TB04', 'TB06', 'TB10', 'TB24') and  Archiveno='"+mArchivedo+"')";
                    mArchiveno = mArchivedo +",";
                }else{
                  mArchiveno = "(Select Archiveno From Es_Doc_Main Where Doccode = Lcc.Prtno And Busstype = 'TB' And Subtype In ('TB01', 'TB04', 'TB06', 'TB10', 'TB24') Fetch First 1 Rows Only),";

				}
				continue;
			}
			if (MANAGECOM.equals(tChildElement.getLocalName())) {
				mManagecom = tChildElement.getText();
				if ((!"".equals(mManagecom)) && mManagecom != null) {
                    mSQL = mSQL + " and Lcc.Managecom='"+mManagecom+"'";
					continue;
				}
//				mErrCode = ERRCODE_CHECK;
//				mErrors.addOneError("请录入管理机构");
//				return false;
				
			}
		}
		if (("".equals(mPrtno) || mPrtno == null)
				&& ("".equals(mAppntname) || mAppntname == null)
				&& ("".equals(mInsuredname) || mInsuredname == null)
				&& ("".equals(mPolapplydate) || mPolapplydate == null)
				&& ("".equals(mCvalidate) || mCvalidate == null)
				&& ("".equals(mAgentcode) || mAgentcode == null)
				&& ("".equals(mArchivedo) || mArchivedo == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("除管理机构外，其余条件必录其一");
			return false;
		}
        
        tsql="Select Lcc.Prtno, "+
               " Lcc.Managecom, "+
               " Lcc.Appntname, "+
               " Lcc.Agentcode, "+
               " Lci.name, "+
               " "+mArchiveno+" "+
               " Lcc.Polapplydate, "+
               " (case when Lcc.cardflag='b' then '97' when Lcc.cardflag='c' then '96' when Lcc.cardflag='d' then '98' when Lcc.prtno like 'PD%' then '99' else Lcc.salechnl end), "+
               " Lcc.Cvalidate, "+
               " Lcc.Agentcom "+
               " From Lccont Lcc, "+
               " Lcinsured Lci "+
               " Where 1 = 1 "+
               " And Lcc.Contno = Lci.Contno  "+
               " "+mSQL+" "+
               "  With Ur";
        
		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

        String total = "select count(*) from (" + tsql + ") a with ur";// 查询总条数
        SSRS totalResult;
        totalResult = tExeSQL.execSQL(total);
        if (totalResult.getMaxRow() > 0 && Integer.parseInt(totalResult.GetText(1, 1)) >0) {
            tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
                    totalResult.GetText(1, 1));
        }
		//proposalsql为分页sql
		String proposalsql = "select * from (select c.*,rownumber() over() as rn from (" + tsql
				+ ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		SSRS proposalResult;
		proposalResult = tExeSQL.execSQL(proposalsql);
		if (proposalResult.getMaxRow() > 0){
			OMElement pollist = tOMFactory.createOMElement("PROPOSALLIST", null);
			for (int i = 1; i <= proposalResult.getMaxRow(); i++) {
				OMElement poldata = tOMFactory.createOMElement("PROPOSALDATA", null);	
				tLoginVerifyTool.addOm(poldata, "PRTNO",
						proposalResult.GetText(i, 1));
				tLoginVerifyTool.addOm(poldata, "MANAGECOM",
						proposalResult.GetText(i, 2));
				tLoginVerifyTool.addOm(poldata, "APPNTNAME",
						proposalResult.GetText(i, 3));
				tLoginVerifyTool.addOm(poldata, "AGENTCODE",
						proposalResult.GetText(i, 4));
				tLoginVerifyTool.addOm(poldata, "INSUREDNAME",
						proposalResult.GetText(i, 5));
				tLoginVerifyTool.addOm(poldata, "ARCHIVENO",
						proposalResult.GetText(i, 6));
				tLoginVerifyTool.addOm(poldata, "POLAPPLYDATE",
						proposalResult.GetText(i, 7));
				tLoginVerifyTool.addOm(poldata, "SALECHNL",
						proposalResult.GetText(i, 8));
				tLoginVerifyTool.addOm(poldata, "CVALIDATE",
						proposalResult.GetText(i, 9));
				tLoginVerifyTool.addOm(poldata, "AGENTCOM",
						proposalResult.GetText(i, 10));			
				pollist.addChild(poldata);			
			}		
			mResponseBodyElement.addChild(pollist);
		}
		return true;
	}
}
