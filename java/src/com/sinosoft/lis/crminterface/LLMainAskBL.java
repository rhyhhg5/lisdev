/*
 * <p>ClassName: LLMainAskBL </p>
 * <p>Description: LLMainAskBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-12 16:05:52
 */
package com.sinosoft.lis.crminterface;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.LCInsuredBL;

public class LLMainAskBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 业务处理相关变量 */
    private LICrmClaimSet mLICrmClaimSet = new LICrmClaimSet();

    public LLMainAskBL() {
    }

    public static void main(String[] args) {
        LICrmClaimSet tLICrmClaimSet = new LICrmClaimSet();
        LICrmClaimSchema tSchema = new LICrmClaimSchema();
        tSchema.setBatchNo("");
        tSchema.setRgtType("1");
        tSchema.setRptorName("杨文忠");
        tSchema.setRptorSex("男");
        tSchema.setRelation("00");
        tSchema.setRptorAddress("");
        tSchema.setRptorPhone("86212121212");
        tSchema.setRptorMobile("");
        tSchema.setEmail("");
        tSchema.setPostCode("");
        tSchema.setRptorIDType("");
        tSchema.setRptorIDNo("");
        tSchema.setRptDate("2006-11-06");
        tSchema.setCustomerNo("9503000003");
        tSchema.setCustomerName("杨文忠");
        tSchema.setCustomerSex("0");
        tSchema.setCustomerAge("");
        tSchema.setCustomerIDType("4");
        tSchema.setCustomerIDNo("120113770427083");
        tSchema.setAccidentDate("2006-11-06");
        tSchema.setAccdentDesc(" in patient");
        tSchema.setState("");
        tLICrmClaimSet.add(tSchema);
        LLMainAskBL tLLMainAskBL = new LLMainAskBL();
        VData tVData = new VData();
        tVData.add(tLICrmClaimSet);
        tLLMainAskBL.submitData(tVData);
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData) {
        if (!getInputData(cInputData)) {
            CError.buildErr(this, "外部数据传入失败LLMainAskBL——>getInputData！");
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            CError.buildErr(this, "数据处理失败LLMainAskBL——>dealData！");
            return false;
        }

        System.out.println("Start OLLMainAskBL Submit...");
        PubSubmit pubSubmit = new PubSubmit();
        if (!pubSubmit.submitData(this.mResult, "")) {
            CError.buildErr(this, "提交事务失败!");
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        //加入结果集
        MMap tmpMap = new MMap();
        LLMainAskSet tLLMainAskSet = new LLMainAskSet();
        LLConsultSet tLLConsultSet = new LLConsultSet();
        LLSubReportSet tLLSubReportSet = new LLSubReportSet();
        LLAskRelaSet tLLAskRelaSet = new LLAskRelaSet();
        String cDate = PubFun.getCurrentDate();
        String cTime = PubFun.getCurrentTime();
        String cMngCom = "86";
        String cOperator = "crm";
        for (int i = 1; i <= mLICrmClaimSet.size(); i++) {
            try{
                LICrmClaimSchema tLICrmClaimSchema = mLICrmClaimSet.get(i);
                LLMainAskSchema tLLMainAskSchema = new LLMainAskSchema();
                LLConsultSchema tLLConsultSchema = new LLConsultSchema();
                if(tLICrmClaimSchema.getAccidentDate()==null || tLICrmClaimSchema.getAccidentDate().equals(""))//by zhangzh 20101011
                {
                	System.out.println(tLICrmClaimSchema.getCustomerNo()+"出险日期为空！");
                    continue;
                }
                String CustomerNo = "" + tLICrmClaimSchema.getCustomerNo();
                String tsql = "select managecom,RelationToMainInsured from lcinsured where insuredno='"
                              +CustomerNo+"' union select managecom,RelationToMainInsured from lbinsured "
                              +"where insuredno='"+CustomerNo+"'";
                ExeSQL texesql = new ExeSQL();
                SSRS tssrs = texesql.execSQL(tsql);
                if (tssrs.getMaxRow() <= 0) {
                    System.out.println("客户信息查询失败");
                    continue;
                } else {
                    cMngCom = tssrs.GetText(1,1);
                    tLLConsultSchema.setCustomerType(tssrs.GetText(1,2));
                }
                String tLimit = PubFun.getNoLimit(cMngCom);
                String evNo = PubFun1.CreateMaxNo("SERIALNO", "");
                String CNo = "";
                tLLMainAskSchema.setLogNo(evNo);
                tLLMainAskSchema.setAskType(tLICrmClaimSchema.getRgtType());
                tLLMainAskSchema.setMngCom(cMngCom);
                tLLMainAskSchema.setOperator(cOperator);
                tLLMainAskSchema.setMakeDate(cDate);
                tLLMainAskSchema.setMakeTime(cTime);
                tLLMainAskSchema.setModifyDate(cDate);
                tLLMainAskSchema.setModifyTime(cTime);
                tLLMainAskSchema.setLogState("0");
                tLLMainAskSchema.setAskMode("9");
                tLLMainAskSchema.setLogName(tLICrmClaimSchema.getRptorName());
                tLLMainAskSchema.setOtherNoType(tLICrmClaimSchema.getRptorIDType());
                tLLMainAskSchema.setOtherNo(tLICrmClaimSchema.getRptorIDNo());
                tLLMainAskSchema.setLogDate(tLICrmClaimSchema.getRptDate());
                tLLMainAskSchema.setPhone(tLICrmClaimSchema.getRptorPhone());
                tLLMainAskSchema.setMobile(tLICrmClaimSchema.getRptorMobile());
                tLLMainAskSchema.setPostCode(tLICrmClaimSchema.getPostCode());
                tLLMainAskSchema.setAskAddress(tLICrmClaimSchema.getRptorAddress());
                tLLMainAskSchema.setEmail(tLICrmClaimSchema.getEmail());
                tLLMainAskSchema.setAnswerType("01"); //即时回复
                tLLMainAskSchema.setAnswerMode("9");
                tLLMainAskSchema.setSwitchCom("LP");
                tLLMainAskSchema.setSwitchDate(cDate);
                tLLMainAskSchema.setSwitchTime(cTime);
                tLLMainAskSchema.setDespatcher(cOperator);
                tLLMainAskSchema.setCNSDate(tLICrmClaimSchema.getRptDate());
                tLLMainAskSchema.setReplyFDate(tLICrmClaimSchema.getRptDate());
                tLLMainAskSchema.setDealFDate(tLICrmClaimSchema.getRptDate());
                tLLMainAskSchema.setCNSOperator(cOperator);
                tLLMainAskSchema.setAvaiFlag("1");
//            tLLMainAskSchema.setLogTime(tLICrmClaimSchema);

                CNo = PubFun1.CreateMaxNo("NOTICENO", tLimit);
                tLLConsultSchema.setConsultNo(CNo);
                tLLConsultSchema.setLogNo(evNo);
                tLLConsultSchema.setMngCom(cMngCom);
                tLLConsultSchema.setOperator(cOperator);
                tLLConsultSchema.setMakeDate(cDate);
                tLLConsultSchema.setMakeTime(cTime);
                tLLConsultSchema.setModifyDate(cDate);
                tLLConsultSchema.setModifyTime(cTime);
                tLLConsultSchema.setCustomerNo(CustomerNo);
                tLLConsultSchema.setCustomerName(tLICrmClaimSchema.getCustomerName());
                tLLConsultSchema.setCSubject("");
                tLLConsultSchema.setCContent(tLICrmClaimSchema.getAccdentDesc());
                tLLConsultSchema.setReplyState("0");
                tLLConsultSchema.setAskGrade("1");
                tLLConsultSchema.setInHospitalDate(tLICrmClaimSchema.
                        getAccidentDate());

                String SubRptNo = "";
                LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
                SubRptNo = PubFun1.CreateMaxNo("SUBRPTNO", tLimit);
                tLLSubReportSchema.setSubRptNo(SubRptNo);
                tLLSubReportSchema.setMngCom(cMngCom);
                tLLSubReportSchema.setOperator(cOperator);
                tLLSubReportSchema.setMakeDate(cDate);
                tLLSubReportSchema.setMakeTime(cTime);
                tLLSubReportSchema.setModifyDate(cDate);
                tLLSubReportSchema.setModifyTime(cTime);
                tLLSubReportSchema.setCustomerNo(tLLConsultSchema.getCustomerNo());
                tLLSubReportSchema.setCustomerName(tLLConsultSchema.
                        getCustomerName());
                tLLSubReportSchema.setCustomerType(tLLConsultSchema.
                        getCustomerType());
                tLLSubReportSchema.setAccDate(tLICrmClaimSchema.getAccidentDate());
                tLLSubReportSchema.setAccDesc(tLICrmClaimSchema.getAccdentDesc());
                LLAskRelaSchema tLLAskRelaSchema = new LLAskRelaSchema();
                tLLAskRelaSchema.setConsultNo(CNo);
                tLLAskRelaSchema.setSubRptNo(SubRptNo);

                tLLMainAskSet.add(tLLMainAskSchema);
                tLLConsultSet.add(tLLConsultSchema);
                tLLSubReportSet.add(tLLSubReportSchema);
                tLLAskRelaSet.add(tLLAskRelaSchema);
                mLICrmClaimSet.get(i).setState("1");
            }catch(Exception ex){
                mLICrmClaimSet.get(i).setState("0");
            }
        }
        tmpMap.put(tLLMainAskSet,"INSERT");
        tmpMap.put(tLLConsultSet,"INSERT");
        tmpMap.put(tLLSubReportSet,"INSERT");
        tmpMap.put(tLLAskRelaSet,"INSERT");
        tmpMap.put(mLICrmClaimSet,"UPDATE");
        //更新时间
        this.mResult.add(tmpMap);
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mLICrmClaimSet = (LICrmClaimSet) cInputData.getObjectByObjectName(
                "LICrmClaimSet", 0);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
