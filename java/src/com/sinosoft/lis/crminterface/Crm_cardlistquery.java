package com.sinosoft.lis.crminterface;
//卡单接口
import java.util.Iterator;
//卡单查询
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.task.GetMaxNo;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_cardlistquery extends CRMAbstractInterface {
	/** 当前页码 **/
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/** 每页记录条数 **/
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/** 卡号 **/
	private static final String CARDNO = "CARDNO";
	private String mCardno = "";
	/** 产品编码 **/
	private static final String PRODUCTID = "PRODUCTID";
	private String mProduciId = "";
	/** 被保人姓名 **/
	private static final String INSUREDNAME = "INSUREDNAME";
	private String mInsuredname = "";
	/** 被保人证件号 */
	private static final String INSUREDIDNO = "INSUREDIDNO";
	private String mInsuredidno = "";
	/** 投保人姓名 */
	private static final String APPNTNAME = "APPNTNAME";
	private String mAppntname = "";
	/** 投保人证件号 **/
	private static final String APPNTIDNO = "APPNTIDNO";
	private String mAppntidno = "";
	/** 生效日起开始时间 **/
	private static final String CVALIDATESTART = "CVALIDATESTART";
	private String mCvalidatestart = "";
	/** 生效日期结束时间 **/
	private static final String CVALIDATEEND = "CVALIDATEEND";
	private String mCvalidateend = "";

	public Crm_cardlistquery() {
		this.mRootName = "CRM_CARDLISTQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		StringBuffer msql = new StringBuffer();
		Iterator tBodyIterator = mBodyElement.getChildren();
        String tcardSqlw="";
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if ((!"".equals(mPageindex)) && mPageindex != null) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数均不能为空");
				return false;

			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if ((!"".equals(mPagesize)) && mPagesize != null) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数均不能为空");
				return false;
			}
			if (CARDNO.equals(tChildElement.getLocalName())) {
				mCardno = tChildElement.getText();
				if ((!"".equals(mCardno)) && mCardno != null) {
					msql.append(" and wfc.cardno='" + mCardno + "' "); // cardno
                    tcardSqlw +=" and lic.CardNo='"+mCardno+"' ";//卡单未激活(查的是核心数据库)
				}
				continue;
			}
			if (PRODUCTID.equals(tChildElement.getLocalName())) {
				mProduciId = tChildElement.getText();
				if ((!"".equals(mProduciId)) && mProduciId != null) {
					msql.append(" and wfc.riskcode='" + mProduciId + "' ");// productid
				}
				continue;
			}

			if (INSUREDNAME.equals(tChildElement.getLocalName())) {
				mInsuredname = tChildElement.getText();
				if ((!"".equals(mInsuredname)) && mInsuredname != null) {
					msql.append(" and wfi.name='" + mInsuredname + "' "); // insuredname
				}
				continue;
			}
			if (INSUREDIDNO.equals(tChildElement.getLocalName())) {
				mInsuredidno = tChildElement.getText();
				if ((!"".equals(mInsuredidno)) && mInsuredidno != null) {
					msql.append(" and wfi.idno='" + mInsuredidno + "' "); // insuredidno
				}
				continue;
			}
			if (APPNTNAME.equals(tChildElement.getLocalName())) {
				mAppntname = tChildElement.getText();
				if ((!"".equals(mAppntname)) && mAppntname != null) {
					msql.append(" and wfa.name='" + mAppntname + "' "); // appntname
				}
				continue;
			}
			if (APPNTIDNO.equals(tChildElement.getLocalName())) {
				mAppntidno = tChildElement.getText();
				if ((!"".equals(mAppntidno)) && mAppntidno != null) {
					msql.append(" and wfa.idno='" + mAppntidno + "' "); // appntidno
				}
				continue;
			}
			if (CVALIDATESTART.equals(tChildElement.getLocalName())) {
				mCvalidatestart = tChildElement.getText();
				if ((!"".equals(mCvalidatestart)) && mCvalidatestart != null) {
					msql.append(" and wfc.cvalidate >='" + mCvalidatestart
							+ "' "); // CVALIDATESTART
                    tcardSqlw +=" and lic.CValidate >= '"+mCvalidatestart+"' ";//未激活
				}
				continue;
			}

			if (CVALIDATEEND.equals(tChildElement.getLocalName())) {
				mCvalidateend = tChildElement.getText();
				if ((!"".equals(mCvalidateend)) && mCvalidateend != null) {
					msql.append(" and wfc.cvalidate <='" + mCvalidateend + "' ");// wfcontlist  wfc
                    tcardSqlw +=" and lic.CValidate <= '"+mCvalidatestart+"' ";//未激活   LICertify lic
				}
				continue;
			}
		}

		if (("".equals(mCardno) || mCardno == null)
				&& ("".equals(mInsuredidno) || mInsuredidno == null)
				&& ("".equals(mAppntidno) || mAppntidno == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("卡号、投/被保人证件号必录其一");
			return false;
		}
		
		SSRS wtotalResult= new SSRS();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		ExeSQL tExeSQL = new ExeSQL();
		SSRS totalResult;
		
		String sql = "select m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13 "
			    + "from ("
			    + "select distinct wfc.cardno m1,(select certifyname  from lmcertifydes where certifycode=wfc.Certifycode) m2,wfi.name m3,wfi.idno m4,"
				  + "(select signdate from lcgrpcont a,licertify b where b.cardno=wfc.cardno and b.prtno=a.prtno fetch first 1 rows only) m5,"
				  + "wfc.cvalidate m6,(select InActiveDate from licardactiveinfolist where cardno=wfc.cardno fetch first 1 rows only) m7,"
				  + "wfc.prem m8,wfa.name m9,(select cardstatus from licardactiveinfolist where cardno=wfc.cardno fetch first 1 rows only) m10,"
				  + "wfa.phont m11,(select managecom from liactivecarddetail where cardno=wfc.cardno fetch first 1 rows only) m12,"
				  + "(select salechnl from licertify where cardno=wfc.cardno fetch first 1 rows only) m13 "
			  	+ "from wfcontlist wfc,wfappntlist wfa,wfinsulist wfi "
				  + "where 1=1 and wfc.cardno=wfi.cardno and wfc.cardno=wfa.cardno " + msql
				  + " )temp "
				  + " order by m10,m6 ";
		
//		String sql = "select m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13 "
//			    + "from ("
//			    + "select distinct wfc.cardno m1,(select certifyname  from lmcertifydes where certifycode=wfc.Certifycode) m2,wfa.name m3,wfa.idno m4,"
//				+ "(select signdate from lcgrpcont a,licertify b where b.cardno=wfc.cardno and b.prtno=a.prtno fetch first 1 rows only) m5,"
//				+ "wfc.cvalidate m6,(select InActiveDate from licardactiveinfolist where cardno=wfc.cardno fetch first 1 rows only) m7,"
//				+ "wfc.prem m8,wfa.name m9,(select cardstatus from licardactiveinfolist where cardno=wfc.cardno fetch first 1 rows only) m10,"
//				+ "wfa.phont m11,(select managecom from liactivecarddetail where cardno=wfc.cardno fetch first 1 rows only) m12,"
//				+ "(select salechnl from licertify where cardno=wfc.cardno fetch first 1 rows only) m13 "
//				+ "from wfcontlist wfc,wfappntlist wfa "
//				+ "where 1=1 and wfc.cardno=wfa.cardno " + msql
//				+"  union "
//				+ "select distinct wfc.cardno m1,(select certifyname  from lmcertifydes where certifycode=wfc.Certifycode) m2,wfa.name m3,wfa.idno m4,"
//				+ "(select signdate from lcgrpcont a,licertify b where b.cardno=wfc.cardno and b.prtno=a.prtno fetch first 1 rows only) m5,"
//				+ "wfc.cvalidate m6,(select InActiveDate from licardactiveinfolist where cardno=wfc.cardno fetch first 1 rows only) m7,"
//				+ "wfc.prem m8,wfa.name m9,(select cardstatus from licardactiveinfolist where cardno=wfc.cardno fetch first 1 rows only) m10,"
//				+ "wfa.phont m11,(select managecom from liactivecarddetail where cardno=wfc.cardno fetch first 1 rows only) m12,"
//				+ "(select salechnl from licertify where cardno=wfc.cardno fetch first 1 rows only) m13 "
//				+ "from wfcontlist wfc,wfinsulist wfa "
//				+ "where 1=1 and wfc.cardno=wfa.cardno " + msql
//				+ " )temp "
//				+ " order by m10,m6 ";
		String total = "select count(*) from (" + sql + ") a with ur";// 查询总条数
		totalResult = tExeSQL.execSQL(total);
		
		System.out.println("msql============="+msql);
		System.out.println("tcardSqlw============="+tcardSqlw);
		
		
		if (totalResult.getMaxRow() > 0 && Integer.parseInt(totalResult.GetText(1, 1)) >0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",totalResult.GetText(1, 1));
			sql = "select * from (select c.*,rownumber() over() as rn from (" + sql
					+ ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
					+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
			SSRS tResult;
			tResult = tExeSQL.execSQL(sql);
			if (tResult.getMaxRow() > 0  ) {
				int trow = tResult.MaxRow;
				OMElement tcustomerlist = tOMFactory.createOMElement("CARDLIST",
						null);
				for (int i = 0; i < trow; i++) {
					OMElement tcustomer = tOMFactory.createOMElement("CARDDATA",
							null);
					tLoginVerifyTool.addOm(tcustomer, "CARDNO",
							tResult.GetText(i + 1, 1));
					tLoginVerifyTool.addOm(tcustomer, "PRODUCTID",
							tResult.GetText(i + 1, 2));
					tLoginVerifyTool.addOm(tcustomer, "INSUREDNAME",
							tResult.GetText(i + 1, 3));
					tLoginVerifyTool.addOm(tcustomer, "INSUREDIDNO",
							tResult.GetText(i + 1, 4));
					tLoginVerifyTool.addOm(tcustomer, "SIGNDATE",
							tResult.GetText(i + 1, 5));
					tLoginVerifyTool.addOm(tcustomer, "CVALIDATE",
							tResult.GetText(i + 1, 6));
					tLoginVerifyTool.addOm(tcustomer, "ENDDATE",
							tResult.GetText(i + 1, 7));
					tLoginVerifyTool.addOm(tcustomer, "PREM",
							tResult.GetText(i + 1, 8));
					tLoginVerifyTool.addOm(tcustomer, "APPNTNAME",
							tResult.GetText(i + 1, 9));
					tLoginVerifyTool.addOm(tcustomer, "CARDSTATE",
							tResult.GetText(i + 1, 10));
					tLoginVerifyTool.addOm(tcustomer, "APPNTPHONE",
							tResult.GetText(i + 1, 11));
					tLoginVerifyTool.addOm(tcustomer, "MANAGECOM",
							tResult.GetText(i + 1, 12));
					tLoginVerifyTool.addOm(tcustomer, "SALECHNL",
							tResult.GetText(i + 1, 13));
	                
	                //增加的客户实名化标记
	                String tInsuredWs=" select lcc.contno from LCcont lcc where lcc.contno='"+tResult.GetText(i + 1, 1)+"'";
	                String tcontnows=tExeSQL.getOneValue(tInsuredWs);
	                if(tcontnows!=null && !"".equals(tcontnows)){
	                    tLoginVerifyTool.addOm(tcustomer, "WSFLAG",
	                            "1");
	                }else{
	                    tLoginVerifyTool.addOm(tcustomer, "WSFLAG",
	                    "0");
	                }
	                
					String risksql = "select distinct ldr.riskcode 险种编码,(select riskname from lmriskapp where riskcode=ldr.riskcode) 险种名称 from ldriskwrap ldr,wfcontlist wfc "
							+ "where wfc.cardno='"
							+ tResult.GetText(i + 1, 1)
							+ "' and wfc.riskcode=ldr.riskwrapcode with ur";
					SSRS triskResult;
					triskResult = tExeSQL.execSQL(risksql);
					if (triskResult.getMaxRow()>0) {
						OMElement trisklist = tOMFactory.createOMElement("RISKLIST",
							null);
					
					for (int j = 0; j < triskResult.MaxRow; j++) {
						OMElement triskdata = tOMFactory.createOMElement("RISKDATA",
							null);
						tLoginVerifyTool.addOm(triskdata, "RISKCODE",
								triskResult.GetText(j + 1, 1));
						tLoginVerifyTool.addOm(triskdata, "RISKNAME",
								triskResult.GetText(j + 1, 2));
						trisklist.addChild(triskdata);
					}
					
					tcustomer.addChild(trisklist);
					}				
					tcustomerlist.addChild(tcustomer);
				}
				mResponseBodyElement.addChild(tcustomerlist);
			}
			
		}else if((!"".equals(mCardno)) && mCardno != null){
			String sqlw ="select lic.CardNo,lic.State,lic.ManageCom,lic.WSState,lic.CValidate,lic.SaleChnl,lic.Prem,lic.Certifycode,(select certifyname  from lmcertifydes where certifycode=lic.Certifycode)  from LICertify lic where 1=1 " +
	                 tcardSqlw
	                 +"with ur ";  //未激活
	        String totalw = "select count(*) from (" + sqlw + ") a with ur";// 查询总条数
	        wtotalResult=tExeSQL.execSQL(totalw);//未激活
	        if(wtotalResult.getMaxRow()>0 && Integer.parseInt(wtotalResult.GetText(1, 1)) >0) {
                tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",wtotalResult.GetText(1, 1));
              //未激活
                sqlw = "select * from (select c.*,rownumber() over() as rn from (" + sqlw
                + ") c) d where d.rn>" + ((mPagein - 1) * mPagesi)
                + " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
                SSRS wResult;
                wResult = tExeSQL.execSQL(sqlw);
                if(wResult.getMaxRow() > 0){
                	int trow = wResult.MaxRow;
                    OMElement tcustomerlist = tOMFactory.createOMElement("CARDLIST",
                            null);
                    for (int i = 0; i < trow; i++) {
                        OMElement tcustomer = tOMFactory.createOMElement("CARDDATA",
                                null);
                        tLoginVerifyTool.addOm(tcustomer, "CARDNO",
                                wResult.GetText(i + 1, 1));
                        tLoginVerifyTool.addOm(tcustomer, "PRODUCTID",
                                wResult.GetText(i + 1, 9));
                        tLoginVerifyTool.addOm(tcustomer, "INSUREDNAME",
                                "");
                        tLoginVerifyTool.addOm(tcustomer, "INSUREDIDNO",
                                "");
                        tLoginVerifyTool.addOm(tcustomer, "SIGNDATE",
                                "");
                        tLoginVerifyTool.addOm(tcustomer, "CVALIDATE",
                                wResult.GetText(i + 1, 5));
                        tLoginVerifyTool.addOm(tcustomer, "ENDDATE",
                                "");
                        tLoginVerifyTool.addOm(tcustomer, "PREM",
                                wResult.GetText(i + 1, 7));
                        tLoginVerifyTool.addOm(tcustomer, "APPNTNAME",
                                "");
                        tLoginVerifyTool.addOm(tcustomer, "CARDSTATE",
                                "");
                        tLoginVerifyTool.addOm(tcustomer, "APPNTPHONE",
                                "");
                        tLoginVerifyTool.addOm(tcustomer, "MANAGECOM",
                                wResult.GetText(i + 1, 3));
                        tLoginVerifyTool.addOm(tcustomer, "SALECHNL",
                                wResult.GetText(i + 1, 6));
                        
                        
                        if(wResult.GetText(i + 1, 4)!=null && "01".equals(wResult.GetText(i + 1, 4))){
                            tLoginVerifyTool.addOm(tcustomer, "WSFLAG",
                                    "1");
                        }else{
                            tLoginVerifyTool.addOm(tcustomer, "WSFLAG",
                            "0");
                        }
                        
                        String tproductno= wResult.GetText(i + 1, 8);
                        
                        String risksql = "select distinct ldr.riskcode,(select riskname from lmriskapp where riskcode=ldr.riskcode) from ldriskwrap ldr,lmcardrisk lmc "
                                + "where lmc.certifycode='"
                                + tproductno
                                + "' and lmc.riskcode=ldr.riskwrapcode with ur";
                        SSRS triskResult;
                        triskResult = tExeSQL.execSQL(risksql);
                        if (triskResult.getMaxRow()>0) {
                            OMElement trisklist = tOMFactory.createOMElement("RISKLIST",
                                null);
                        
                        for (int j = 0; j < triskResult.MaxRow; j++) {
                            OMElement triskdata = tOMFactory.createOMElement("RISKDATA",
                                null);
                            tLoginVerifyTool.addOm(triskdata, "RISKCODE",
                                    triskResult.GetText(j + 1, 1));
                            tLoginVerifyTool.addOm(triskdata, "RISKNAME",
                                    triskResult.GetText(j + 1, 2));
                            trisklist.addChild(triskdata);
                        }
                        
                        tcustomer.addChild(trisklist);
                        }               
                        tcustomerlist.addChild(tcustomer);
                    }
                    mResponseBodyElement.addChild(tcustomerlist);
            }
        }
		}		
		return true;
	}

}
