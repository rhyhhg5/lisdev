package com.sinosoft.lis.crminterface;

import java.io.*;
import java.io.File.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CrmTranOtherDataBL {

    private String interFilePath = "";
    private String mLogDate="";
    private String mToday = PubFun.getCurrentDate();
    private String mTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    private VData mResult = new VData();
    private String mBatchno = "";
    private String mLogData = "";
    private String mType = "";
    private String mFilename = "";
    private LICrmLogSet mLICrmLogSet = new LICrmLogSet();
    private String CrmInterFacePath = "";
    private ExeSQL mExeSQL = new ExeSQL();



    public CrmTranOtherDataBL() {
    }
    private boolean getInputData(VData cInputData) {
        mLICrmLogSet.set((LICrmLogSet) cInputData.getObjectByObjectName("LICrmLogSet",0));
        String tSql = "select sysvarvalue from ldsysvar where sysvar = 'CrmInterfaceUrl'";
        CrmInterFacePath = mExeSQL.getOneValue(tSql);
//        CrmInterFacePath = "D:\\CRMDate\\";
        return true;
    }
    public boolean submitData(VData cInputData) throws java.io.
            FileNotFoundException, java.io.IOException {


      // 得到外部传入的数据，将数据备份到本类中
       if (!getInputData(cInputData))
       {
           return false;
       }

      // 进行业务处理
      for (int i=1;i<=mLICrmLogSet.size();i++)
      {
          LICrmLogSchema mLICrmLogSchema = new LICrmLogSchema();
          mLICrmLogSchema = mLICrmLogSet.get(i);
          if (!ReadDate(mLICrmLogSchema)) {
              return false;
          }
      }
      if (!SubmitMap() )
         return false;
        return true;
    }
    private boolean ReadDate(LICrmLogSchema aLICrmLogSchema) throws java.io.FileNotFoundException,
                java.io.IOException {
            mFilename = aLICrmLogSchema.getFileName();
            VData aVData = new VData();
            interFilePath = CrmInterFacePath+mFilename;
            mType = aLICrmLogSchema.getFileCode();
            mBatchno = aLICrmLogSchema.getBatchNo();
            mLogDate = aLICrmLogSchema.getBatchDate();

            FileReader fr = new FileReader(interFilePath);
            BufferedReader br = new BufferedReader(fr);
            String Line = br.readLine();
            int intFileNum=0;
            while(Line!=null)
           {
              Line = DelSpace(Line);
              aVData.add(Line);
              System.out.println("第"+intFileNum+"行 :"+aVData.get(intFileNum));
              intFileNum = intFileNum + 1;
              Line   =   br.readLine();
            }
            br.close();
            fr.close();

            if (mType.equals("PolicySimpleChange"))
                InsertLICrmPolicyChange(aVData);
            else if (mType.equals("Claim"))
                InsertLICrmClaim(aVData);
            else if (mType.equals("ChangePayListFeed"))
                InsertLICrmChangePay(aVData);
            else if (mType.equals("RenewalPayListFeed"))
                InsertLICrmRenewalPay(aVData);
            else if (mType.equals("LHQuesImportMain"))
                InsertLHQuesImportMain(aVData);
            else if (mType.equals("LHQuesImportDetail"))
                InsertLHQuesImportDetail(aVData);
            return true;
        }
        private boolean InsertLICrmPolicyChange(VData mVData)
        {
            LICrmPolicyChangeSet mLICrmPolicyChangeSet = new LICrmPolicyChangeSet();
            String SingleData[] = new String[33];
            for (int i=0;i<mVData.size();i++)
            {
                SingleData = null;
                SingleData = ((String)mVData.get(i)).split(" | ");
                if (StrTool.cTrim( SingleData[4]).equals("") && StrTool.cTrim( SingleData[6]).equals("") &&
                      StrTool.cTrim( SingleData[8]).equals("") && StrTool.cTrim( SingleData[10]).equals("") &&
                      StrTool.cTrim( SingleData[12]).equals("") && StrTool.cTrim( SingleData[14]).equals("") &&
                      StrTool.cTrim( SingleData[16]).equals("") && StrTool.cTrim( SingleData[18]).equals("") &&
                      StrTool.cTrim( SingleData[20]).equals("") && StrTool.cTrim( SingleData[22]).equals("") &&
                      StrTool.cTrim( SingleData[24]).equals("") && StrTool.cTrim( SingleData[26]).equals("") &&
                      StrTool.cTrim( SingleData[28]).equals("") && StrTool.cTrim( SingleData[30]).equals("") &&
                      StrTool.cTrim( SingleData[32]).equals(""))
                    continue;
                LICrmPolicyChangeSchema mLICrmPolicyChangeSchema = new LICrmPolicyChangeSchema();
                mLICrmPolicyChangeSchema.setBatchNo(mBatchno);
                mLICrmPolicyChangeSchema.setContNo(SingleData[0]);
                mLICrmPolicyChangeSchema.setChangePhoneDate(SingleData[2]);
                mLICrmPolicyChangeSchema.setPostalAddress(SingleData[4]);
                mLICrmPolicyChangeSchema.setZipCode(SingleData[6]);
                mLICrmPolicyChangeSchema.setPhone(SingleData[8]);
                mLICrmPolicyChangeSchema.setFax(SingleData[10]);
                mLICrmPolicyChangeSchema.setHomeAddress(SingleData[12]);
                mLICrmPolicyChangeSchema.setHomeZipCode(SingleData[14]);
                mLICrmPolicyChangeSchema.setHomePhone(SingleData[16]);
                mLICrmPolicyChangeSchema.setHomeFax(SingleData[18]);
                mLICrmPolicyChangeSchema.setCompanyAddress(SingleData[20]);
                mLICrmPolicyChangeSchema.setCompanyZipCode(SingleData[22]);
                mLICrmPolicyChangeSchema.setCompanyPhone(SingleData[24]);
                mLICrmPolicyChangeSchema.setCompanyFax(SingleData[26]);
                mLICrmPolicyChangeSchema.setMobile(SingleData[28]);
                mLICrmPolicyChangeSchema.setEMail(SingleData[30]);
                mLICrmPolicyChangeSchema.setBP(SingleData[32]);
                mLICrmPolicyChangeSchema.setOperator("crm");
                mLICrmPolicyChangeSchema.setMakeDate(mToday);
                mLICrmPolicyChangeSchema.setMakeTime(mTime);
                if(SingleData[2]!=null){
                    mLICrmPolicyChangeSchema.setChangeTime(SingleData[2].substring(SingleData[2].lastIndexOf("-")+1,SingleData[2].lastIndexOf(".")));
                }
                mLICrmPolicyChangeSet.add(mLICrmPolicyChangeSchema);
            }
            mMap.put(mLICrmPolicyChangeSet, "INSERT");
            return true;
        }
        private boolean InsertLICrmClaim(VData mVData)
       {
           LICrmClaimSet mLICrmClaimSet = new LICrmClaimSet();
           String SingleData[] = new String[40];
           String aSerialno = "";
           for (int i=0;i<mVData.size();i++)
           {
               String strSex = "";
               SingleData = null;
               SingleData = ((String)mVData.get(i)).split(" | ");
               LICrmClaimSchema mLICrmClaimSchema = new LICrmClaimSchema();
               mLICrmClaimSchema.setBatchNo(mBatchno);
               if (SingleData[2] != null || !SingleData[2].equals(""))
                  mLICrmClaimSchema.setRgtType(SingleData[2]);
               if (SingleData[4] != null || !SingleData[4].equals(""))
                  mLICrmClaimSchema.setRptorName(SingleData[4]);
               if (SingleData[6].equals("男"))
                    strSex = "0";
               else if  (SingleData[6].equals("女"))
                    strSex = "1";
               else
                    strSex = "2";
               mLICrmClaimSchema.setRptorSex(strSex);
               if (SingleData[8] != null || !SingleData[8].equals(""))
                  mLICrmClaimSchema.setRelation(SingleData[8]);
               if (SingleData[10] != null || !SingleData[10].equals(""))
                  mLICrmClaimSchema.setRptorAddress(SingleData[10]);
               if (SingleData[12] != null || !SingleData[12].equals(""))
                  mLICrmClaimSchema.setRptorPhone(SingleData[12]);
               if (SingleData[14] != null || !SingleData[14].equals(""))
                  mLICrmClaimSchema.setRptorMobile(SingleData[14]);
               if (SingleData[16] != null || !SingleData[16].equals(""))
                  mLICrmClaimSchema.setEmail(SingleData[16]);
               if (SingleData[18] != null || !SingleData[18].equals(""))
                  mLICrmClaimSchema.setPostCode(SingleData[18]);
               if (SingleData[20] != null || !SingleData[20].equals(""))
                  mLICrmClaimSchema.setRptorIDType(SingleData[20]);
               if (SingleData[22] != null || !SingleData[22].equals(""))
                   mLICrmClaimSchema.setRptorIDNo(SingleData[22]);
               if (SingleData[24] != null || !SingleData[24].equals(""))
                   mLICrmClaimSchema.setRptDate(SingleData[24]);
               if (!StrTool.cTrim(SingleData[26]).equals(""))
               {
                   System.out.println("SingleData[26] :" + SingleData[26]);
                   mLICrmClaimSchema.setCustomerNo(SingleData[26]);
               }
               else
                   mLICrmClaimSchema.setCustomerNo("000000000");
               if (SingleData[28] != null || !SingleData[28].equals(""))
                   mLICrmClaimSchema.setCustomerName(SingleData[28]);
               if (SingleData[30] != null || !SingleData[30].equals(""))
                   mLICrmClaimSchema.setCustomerSex(SingleData[30]);
               if (SingleData[32] != null || !SingleData[32].equals(""))
                  mLICrmClaimSchema.setCustomerAge(SingleData[32]);
               if (SingleData[34] != null || !SingleData[34].equals(""))
                  mLICrmClaimSchema.setCustomerIDType(SingleData[34]);
               if (SingleData[36] != null || !SingleData[36].equals(""))
                  mLICrmClaimSchema.setCustomerIDNo(SingleData[36]);
               if (SingleData[38] != null || !SingleData[38].equals(""))
                  mLICrmClaimSchema.setAccidentDate(SingleData[38]);
               if (SingleData[40] != null || !SingleData[40].equals(""))
                  mLICrmClaimSchema.setAccdentDesc(SingleData[40]);
               aSerialno = PubFun1.CreateMaxNo("CRMSERIALNO", 10);
               mLICrmClaimSchema.setSerialno(aSerialno);
               mLICrmClaimSchema.setOperator("crm");
               mLICrmClaimSchema.setMakeDate(mToday);
               mLICrmClaimSchema.setMakeTime(mTime);
               mLICrmClaimSet.add(mLICrmClaimSchema);
            }
           mMap.put(mLICrmClaimSet, "INSERT");
            return true;
       }
        private boolean InsertLICrmChangePay(VData mVData)
       {
           LICrmChangePaySet mLICrmChangePaySet = new LICrmChangePaySet();
           String SingleData[] = new String[15];
           for (int i=0;i<mVData.size();i++)
           {
               SingleData = null;
               SingleData = ((String)mVData.get(i)).split(" | ");
               LICrmChangePaySchema mLICrmChangePaySchema = new LICrmChangePaySchema();
               mLICrmChangePaySchema.setBatchNo(mBatchno);
               mLICrmChangePaySchema.setAppntno(SingleData[0]);
               mLICrmChangePaySchema.setEndorseno(SingleData[2]);
               mLICrmChangePaySchema.setContno(SingleData[4]);
               mLICrmChangePaySchema.setPaydate(SingleData[6]);
               mLICrmChangePaySchema.setPaymoney(SingleData[8]);
               mLICrmChangePaySchema.setNextPaydate(SingleData[10]);
               mLICrmChangePaySchema.setResult(SingleData[12]);
               mLICrmChangePaySchema.setOperator("crm");
               mLICrmChangePaySchema.setMakeDate(mToday);
               mLICrmChangePaySchema.setMakeTime(mTime);
               mLICrmChangePaySet.add(mLICrmChangePaySchema);
            }
           mMap.put(mLICrmChangePaySet, "INSERT");
            return true;
       }
        private boolean InsertLICrmRenewalPay(VData mVData)
       {
           LICrmRenewalPaySet mLICrmRenewalPaySet = new LICrmRenewalPaySet();
           String SingleData[] = new String[20];
           for (int i=0;i<mVData.size();i++)
           {
               SingleData = null;
               SingleData = ((String)mVData.get(i)).split(" | ");
               LICrmRenewalPaySchema mLICrmRenewalPaySchema = new LICrmRenewalPaySchema();
               mLICrmRenewalPaySchema.setBatchNo(mBatchno);
               mLICrmRenewalPaySchema.setAppntno(SingleData[0]);
               mLICrmRenewalPaySchema.setGetNoticeno(SingleData[2]);
               mLICrmRenewalPaySchema.setContno(SingleData[4]);
               mLICrmRenewalPaySchema.setPaydate(SingleData[6]);
               mLICrmRenewalPaySchema.setPaymoney(SingleData[8]);
               mLICrmRenewalPaySchema.setNextPaydate(SingleData[10]);
               mLICrmRenewalPaySchema.setResult(SingleData[12]);
               mLICrmRenewalPaySchema.setCrmOperator(SingleData[14]);
               mLICrmRenewalPaySchema.setOperator("crm");
               mLICrmRenewalPaySchema.setMakeDate(mToday);
               mLICrmRenewalPaySchema.setMakeTime(mTime);
               mLICrmRenewalPaySet.add(mLICrmRenewalPaySchema);
            }
           mMap.put(mLICrmRenewalPaySet, "INSERT");
            return true;
       }
        private boolean InsertLHQuesImportDetail(VData mVData)
       {
           LHQuesImportDetailSet mLHQuesImportDetailSet = new LHQuesImportDetailSet();
           String SingleData[] = new String[11];
           for (int i=0;i<mVData.size();i++)
           {
               SingleData = null;
               SingleData = ((String)mVData.get(i)).split(" | ");
               LHQuesImportDetailSchema mLHQuesImportDetailSchema = new LHQuesImportDetailSchema();
               mLHQuesImportDetailSchema.setCusRegistNo(SingleData[0]);
               mLHQuesImportDetailSchema.setStandCode(SingleData[2]);
               mLHQuesImportDetailSchema.setOptionResult(SingleData[4]);
               mLHQuesImportDetailSchema.setLetterResult(SingleData[6]);
               mLHQuesImportDetailSchema.setQuesStatus(SingleData[8]);
               mLHQuesImportDetailSchema.setManageCom("86");
               mLHQuesImportDetailSchema.setOperator("CRM");
               mLHQuesImportDetailSchema.setMakeDate(mToday);
               mLHQuesImportDetailSchema.setMakeTime(mTime);
               mLHQuesImportDetailSchema.setModifyDate(mToday);
               mLHQuesImportDetailSchema.setModifyTime(mTime);
               mLHQuesImportDetailSet.add(mLHQuesImportDetailSchema);
            }
           mMap.put(mLHQuesImportDetailSet, "INSERT");

            return true;
       }
         private boolean InsertLHQuesImportMain(VData mVData)
       {
           LHQuesImportMainSet mLHQuesImportMainSet = new LHQuesImportMainSet();
           String SingleData[] = new String[43];
           for (int i=0;i<mVData.size();i++)
           {
               SingleData = null;
               SingleData = ((String)mVData.get(i)).split(" | ");
               LHQuesImportMainSchema mLHQuesImportMainSchema = new LHQuesImportMainSchema();
               mLHQuesImportMainSchema.setCusRegistNo(SingleData[0]);
               mLHQuesImportMainSchema.setQuesType(SingleData[2]);
               mLHQuesImportMainSchema.setSubmitDate(SingleData[4]);
               mLHQuesImportMainSchema.setSubmitTime(SingleData[6]);
               mLHQuesImportMainSchema.setGrpContNo(SingleData[8]);
               mLHQuesImportMainSchema.setContNo(SingleData[10]);
               mLHQuesImportMainSchema.setServPlanNo(SingleData[12]);
               mLHQuesImportMainSchema.setServItemNo(SingleData[14]);
               mLHQuesImportMainSchema.setServCaseCode(SingleData[16]);
               mLHQuesImportMainSchema.setServTaskNo(SingleData[18]);
               mLHQuesImportMainSchema.setServTaskCode(SingleData[20]);
               mLHQuesImportMainSchema.setTaskExecNo(SingleData[22]);
               mLHQuesImportMainSchema.setCustomerNo(SingleData[24]);
               mLHQuesImportMainSchema.setGrpCustomerNo(SingleData[26]);
               mLHQuesImportMainSchema.setSource(SingleData[28]);
               mLHQuesImportMainSchema.setWhethAchieve(SingleData[30]);
               mLHQuesImportMainSchema.setManageCom(SingleData[32]);
               mLHQuesImportMainSchema.setOperator(SingleData[34]);
               mLHQuesImportMainSchema.setMakeDate(SingleData[36]);
               mLHQuesImportMainSchema.setMakeTime(SingleData[38]);
               mLHQuesImportMainSchema.setModifyDate(SingleData[40]);
               mLHQuesImportMainSchema.setModifyTime(SingleData[42]);
               mLHQuesImportMainSet.add(mLHQuesImportMainSchema);
            }
           mMap.put(mLHQuesImportMainSet, "INSERT");
            return true;
       }




        /*数据处理*/
    private boolean SubmitMap()
    {
        PubSubmit tPubSubmit = new PubSubmit();
        mResult.add(mMap);
        if (!tPubSubmit.submitData(mResult, ""))
        {
            System.out.println("提交数据失败！");
            return false;
        }
        return true;
    }
    private String DelSpace(String aLine)
    {
        int len=0;
        len = aLine.length();
        String strTemp = "";
        for (int i=0;i<=len-1;i++)
        {
            if (!aLine.substring(i,i+1).equals(" "))
              strTemp = strTemp + aLine.substring(i,i+1);
        }
        len = 0;
        len = strTemp.length();
        String strTemp1 = "";
        for (int j=0;j<len;j++)
        {
            if (!strTemp.substring(j,j+1).equals("|"))
              strTemp1 = strTemp1 + strTemp.substring(j,j+1);
            else
              strTemp1 = strTemp1 + " | ";
        }
        return strTemp1;
    }
        public static void main(String[] args) throws Exception
        {
//             CrmTranOtherDataBL aCrmTranOtherDataBL = new CrmTranOtherDataBL();
//             String tSql="select * from LICrmLog where batchno = '00000000000000000007'";
//             LICrmLogSet aLICrmLogSet = new LICrmLogSet();
//             LICrmLogDB aLICrmLogDB = new LICrmLogDB();
//             aLICrmLogSet=aLICrmLogDB.executeQuery(tSql);
//             VData tVData = new VData();
//             tVData.add(aLICrmLogSet);
//             aCrmTranOtherDataBL.submitData(tVData);
            String str="2007-10-04-17.51.43.000000";
            String s=str.substring(str.lastIndexOf("-")+1,str.lastIndexOf("."));
            System.out.println(s);

        }
}
