package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_grpcontlistquery extends CRMAbstractInterface {

	/** 当前页码 **/
	private static final String PAGEINDEX = "PAGEINDEX";
	private String mPageindex = "";
	private int mPagein = 0;
	/** 每页记录条数 **/
	private static final String PAGESIZE = "PAGESIZE";
	private String mPagesize = "";
	private int mPagesi = 0;
	/** 团体保单号 **/
	private static final String GRPCONTNO = "GRPCONTNO";
	private String mGrpcontno = "";
	/** 客户号 **/
	private static final String GRPAPPNTNO = "GRPAPPNTNO";
	private String mGrpAppntNo = "";
	/** 单位名称 **/
	private static final String GRPNAME = "GRPNAME";
	private String mGrpname = "";

    
	/** 被保人证件号 */
	private static final String INSUREDIDNO = "INSUREDIDNO";
	private String mInsuredidno = "";
	
	/** 印刷号 */
	private static final String PRTNO = "PRTNO";
	private String mPrtNo = "";
    
    

	public Crm_grpcontlistquery() {
		this.mRootName = "CRM_GRPCONTLISTQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (PAGEINDEX.equals(tChildElement.getLocalName())) {
				mPageindex = tChildElement.getText();
				if ((!"".equals(mPageindex)) && mPageindex != null) {
					mPagein = Integer.parseInt(mPageindex);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数均不能为空");
				return false;
			}
			if (PAGESIZE.equals(tChildElement.getLocalName())) {
				mPagesize = tChildElement.getText();
				if ((!"".equals(mPagesize)) && mPagesize != null) {
					mPagesi = Integer.parseInt(mPagesize);
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("当前页码和每页记录条数均不能为空");
				return false;
			}
			if (GRPCONTNO.equals(tChildElement.getLocalName())) {
				mGrpcontno = tChildElement.getText();
				if ((!"".equals(mGrpcontno)) && mGrpcontno != null) {
					tsql = tsql + " and a.grpcontno='" + mGrpcontno + "' ";
				}
				continue;
			}
			if (GRPAPPNTNO.equals(tChildElement.getLocalName())) {
                mGrpAppntNo = tChildElement.getText();
				if ((!"".equals(mGrpAppntNo)) && mGrpAppntNo != null) {
					tsql = tsql + " and a.AppntNo='" + mGrpAppntNo + "'";
				}
				continue;
			}
			if (GRPNAME.equals(tChildElement.getLocalName())) {
                mGrpname = tChildElement.getText();
				if ((!"".equals(mGrpname)) && mGrpname != null) {
					tsql = tsql + " and a.grpname='" + mGrpname + "'";
				}
				continue;
			}
            
          
            
			if (INSUREDIDNO.equals(tChildElement.getLocalName())) {
				mInsuredidno = tChildElement.getText();
				if ((!"".equals(mInsuredidno)) && mInsuredidno != null) {
					tsql = tsql
							+ " and a.grpcontno in (select grpcontno from lcinsured where idno='"
							+ mInsuredidno
							+ "' union select grpcontno from lbinsured where idno='"
							+ mInsuredidno + "') ";
				}
				continue;
			}
			//add by fuxin 
			if(PRTNO.equals(tChildElement.getLocalName())){
				mPrtNo = tChildElement.getText();
				if ((!"".equals(mPrtNo)) && mPrtNo != null) {
					tsql = tsql + " and a.prtno='" + mPrtNo + "'";
				}
				continue;
			}
			

		}
		/**
		if (("".equals(mGrpcontno) || mGrpcontno == null)
				&& ("".equals(mGrpAppntNo) || mGrpAppntNo == null)&&("".equals(mGrpname) || mGrpname == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("团体保单号、客户号、单位名称 必录其一");
			return false;
		}
		*/
		
		String sql = "select m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18 "
			    + " from ("
			    + " select a.grpcontno as m1 ,"
				+ "a.grpname as m2,"
				+ "a.cvalidate as m3,"
				+ "a.peoples as m4,"
				+ "a.stateflag as m5,'' as m6, "
				//+ "(select riskname from lmriskapp where riskcode = b.riskcode) as 主险名称,"
				+ "a.prem as m7,"
				+ "a.amnt as m8,"
				+ "a.businesstype as m9,"
				+ "(select name from laagent where agentcode=a.agentcode) as m10,"
				+ "a.agentcode as m11,"
				+ "(select agentstate from laagent where agentcode=a.agentcode) as m12,"
				+ "a.HandlerName as m13,"
				+ "a.managecom as m14,"
				+ "a.SALECHNL as m15,"
				+ "(select phone from LDGrp where customerno=a.appntno) as m16"
				+ ",a.cinvalidate m17,(select max(PayEndDate) from lcgrppol where grpcontno=a.grpcontno ) m18 "
				+ " from lcgrpcont a "//"inner join lcgrppol b on a.grpcontno=b.grpcontno"
				+ " where 1=1 "//"exists (select 1 from lmriskapp where riskcode=b.riskcode and SubRiskFlag='M')"
				+ tsql
				+ " union all"
				+ " select a.grpcontno as 团体保单号 ,"
				+ "a.grpname as 单位名称,"
				+ "a.cvalidate as 生效日期,"
				+ "a.peoples as 投保人数,"
				+ "a.stateflag as 保单状态,'',"
				//+ "(select riskname from lmriskapp where riskcode = b.riskcode) as 主险名称,"
				+ "a.prem as 保费,"
				+ "a.amnt as 保额,"
				+ "a.businesstype as 单位工商编号,"
				+ "(select name from laagent where agentcode=a.agentcode) as 业务员姓名,"
				+ "a.agentcode as 业务员代码,"
				+ "(select agentstate from laagent where agentcode=a.agentcode) as 业务员状态,"
				+ "a.HandlerName as 经办人姓名,"
				+ "a.managecom as 所属机构,"
				+ "a.SALECHNL as 渠道,"
				+ "(select phone from LDGrp where customerno=a.appntno) as 单位联系电话"
				+ ",a.cinvalidate m17,(select max(PayEndDate) from lcgrppol where grpcontno=a.grpcontno ) m18 "
				+ " from lbgrpcont a "//"inner join lbgrppol b on a.grpcontno=b.grpcontno"
				+ " where 1=1 " //"exists (select 1 from lmriskapp where riskcode=b.riskcode and SubRiskFlag='M')"
				+ tsql
				+" ) temp "
				+" order by m5,m3 ";
		String total = "select count(*) from (" + sql + ") totl with ur";// 查询总条数
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

		ExeSQL tExeSQL = new ExeSQL();
		SSRS totalResult;
		totalResult = tExeSQL.execSQL(total);
		if (totalResult.getMaxRow() > 0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "TOTALROWNUM",
					totalResult.GetText(1, 1));
		}
		String pagesql = "select * from (select c.*,rownumber() over() as rn from ("
				+ sql
				+ ") c) d where d.rn>"
				+ ((mPagein - 1) * mPagesi)
				+ " and d.rn<=" + (mPagein * mPagesi) + " with ur ";
		SSRS tResult;
		tResult = tExeSQL.execSQL(pagesql);
		if (tResult.getMaxRow() > 0) {
			int trow = tResult.MaxRow;
			OMElement tcustomerlist = tOMFactory.createOMElement("GRPCONTLIST",
					null);
			for (int i = 0; i < trow; i++) {
				OMElement tcustomer = tOMFactory.createOMElement("GRPCONTDATA",
						null);
				tLoginVerifyTool.addOm(tcustomer, "GRPCONTNO",
						tResult.GetText(i + 1, 1));
				tLoginVerifyTool.addOm(tcustomer, "GRPNAME",
						tResult.GetText(i + 1, 2));
				tLoginVerifyTool.addOm(tcustomer, "CVALIDATE",
						tResult.GetText(i + 1, 3));
				tLoginVerifyTool.addOm(tcustomer, "PEOPLES",
						tResult.GetText(i + 1, 4));
				tLoginVerifyTool.addOm(tcustomer, "STATEFLAG",
						tResult.GetText(i + 1, 5));
//				tLoginVerifyTool.addOm(tcustomer, "RISKNAME",
//						tResult.GetText(i + 1, 6));
				tLoginVerifyTool.addOm(tcustomer, "PREM",
						tResult.GetText(i + 1, 7));
				tLoginVerifyTool.addOm(tcustomer, "AMNT",
						tResult.GetText(i + 1, 8));
				tLoginVerifyTool.addOm(tcustomer, "BUSINESSNO",
						tResult.GetText(i + 1, 9));
				tLoginVerifyTool.addOm(tcustomer, "AGENTNAME",
						tResult.GetText(i + 1, 10));
				tLoginVerifyTool.addOm(tcustomer, "AGENTCODE",
						tResult.GetText(i + 1, 11));
				tLoginVerifyTool.addOm(tcustomer, "AGENTSTATE",
						tResult.GetText(i + 1, 12));
				tLoginVerifyTool.addOm(tcustomer, "LINKMAN",
						tResult.GetText(i + 1, 13));
				tLoginVerifyTool.addOm(tcustomer, "MANAGECOM",
						tResult.GetText(i + 1, 14));
				tLoginVerifyTool.addOm(tcustomer, "SALECHNL",
						tResult.GetText(i + 1, 15));
				tLoginVerifyTool.addOm(tcustomer, "PHONE",
						tResult.GetText(i + 1, 16));
				tLoginVerifyTool.addOm(tcustomer, "ENDDATE",
						tResult.GetText(i + 1, 17));
				tLoginVerifyTool.addOm(tcustomer, "PAYENDDATE",
						tResult.GetText(i + 1, 18));
				OMElement trisklist = tOMFactory.createOMElement("RISKLIST",
						null);
				
				String riskresult = "select riskcode from lcgrppol where grpcontno='"
						+ tResult.GetText(i + 1, 1)
						+ "' union all select riskcode "
						+ "from lbgrppol where grpcontno='"
						+ tResult.GetText(i + 1, 1) + "' with ur ";
				SSRS triskResult;
				triskResult = tExeSQL.execSQL(riskresult);
				int riskrow = triskResult.MaxRow;
				for (int j = 0; j < riskrow; j++) {
					OMElement triskdata = tOMFactory.createOMElement("RISKDATA",
						null);
					tLoginVerifyTool.addOm(triskdata, "RISKCODE",
                            triskResult.GetText(j + 1, 1));
					trisklist.addChild(triskdata);
				}
				
				tcustomer.addChild(trisklist);
				tcustomerlist.addChild(tcustomer);
			}
			mResponseBodyElement.addChild(tcustomerlist);
		}
		return true;
	}
}
