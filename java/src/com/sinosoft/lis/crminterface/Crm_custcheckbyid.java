package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMElement;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_custcheckbyid extends CRMAbstractInterface {
	/**
	 * 投/被保人证件号
	 * */
	
	private static final String IDNO = "IDNO";
	private String mIDNo = "";
	private String mUpperIDNo = "";
	private String mLowerIDNo = "";

	public Crm_custcheckbyid() {
		this.mRootName = "CRM_CUSTCHECKBYID";
	}

	public boolean returnResponseBody() throws Exception {

		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (IDNO.equals(tChildElement.getLocalName())) {
				mIDNo = tChildElement.getText();
				mUpperIDNo = mIDNo.toUpperCase();
				mLowerIDNo = mIDNo.toLowerCase();
				continue;
			}
		}
		if ("".equals(mIDNo) || mIDNo == null) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("请录入投/被保人证件号");
			return false;
		}
		/*String tsql = "select 'T' from lccont lcc where lcc.appntidno='"
				+ mIDNo
				+ "' union all select 'T' from lbcont lbc where lbc.appntidno='"
				+ mIDNo
				+ "' union all select 'T' from lccont lcc where lcc.insuredidno='"
				+ mIDNo
				+ "' union all select 'T' from lbcont lbc where lbc.insuredidno='"
				+ mIDNo + "' with ur";*/
		
		/*String tsql = "select 'T' from lcappnt lcp where lcp.idno='"
					+ mIDNo+"' and exists(select 1 from lccont lcc where lcc.contno=lcp.contno and lcc.appflag ='1' and lcc.conttype='1') " 
					+ " union " 
					+ " select 'T' from lcinsured lci where lci.idno='"+mIDNo+"' and exists( select 1 from lccont lcc where lcc.contno=lci.contno and lcc.appflag ='1' and lcc.conttype='1') "
					+ " union " 
					+ "select 'T' from lbcont lbc where lbc.appntidno='"+mIDNo+"' or lbc.insuredidno='"+ mIDNo+"' with ur";
		*/
		
		String tsql = "select 'T' from lcappnt lcp,lccont lcc where lcp.contno=lcc.contno and (lcp.idno='"+mUpperIDNo+"' or lcp.idno='"+mLowerIDNo+"') "
					+ "and lcc.appflag='1' and lcc.conttype='1' "
					+ "union "
					+ "select 'T' from lcinsured lci,lccont lcc where lci.contno=lcc.contno and (lci.idno='"+mUpperIDNo+"' or lci.idno='"+mLowerIDNo+"') "
					+ "and lcc.appflag='1' and lcc.conttype='1'" 
					+ "union "
					+ "select 'T' from lbappnt where (idno='"+mUpperIDNo+"' or idno='"+mLowerIDNo+"') "
					+ "union "
					+ "select 'T' from lbinsured lbi,lbcont lbc where lbi.contno=lbc.contno and (lbi.idno='"+mUpperIDNo+"' or lbi.idno='"+mLowerIDNo+"') "
					+ "and lbc.conttype='1' "
					+ "union "
					+ "select 'T' from lbinsured lbi,lccont lcc where lbi.contno=lcc.contno and (lbi.idno='"+mUpperIDNo+"' or lbi.idno='"+mLowerIDNo+"') " 
					+ "and lcc.conttype='1' "
					+ "with ur";
				
		System.out.println("tsql============"+tsql);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tResult;
		tResult = tExeSQL.execSQL(tsql);
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		if (tResult.getMaxRow() > 0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "T");
		} else {
			tLoginVerifyTool.addOm(mResponseBodyElement, "SUCCESSFLAG", "F");
		}
		return true;
	}

}
