package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMElement;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_accinfoquery extends CRMAbstractInterface {

	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
    
    private String mStrsql="";
	public Crm_accinfoquery() {
		this.mRootName = "CRM_ACCINFOQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CONTNO.equals(tChildElement.getLocalName())) {
                mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
                    mStrsql +=" and contno='"+mContno+"'";
					continue;
				}
				mErrCode = ERRCODE_CHECK;
				mErrors.addOneError("保单号不能为空");
				return false;
			}
		}
		String sql = "select "+
                     " (select insuaccname from lmriskinsuacc where insuaccno=lci.insuaccno) 账户名称, "+
                     "  nvl((select Fee "+
                     "  from lcinsureaccfeetrace "+
                     " where moneytype = 'GL' "+
                     " and contno = lci.contno "+
                     "  order by makedate fetch first 1 rows only),0) 期初管理费用, " +
                     " insuAccBala 最新保单价值, "+
                     " LastAccBala 期初保单价值 "+
                     "  from lcinsureacc lci "+
                     " where 1 = 1" +
                     mStrsql
                     +" with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS sResult;
		sResult = tExeSQL.execSQL(sql);
		if (sResult.getMaxRow() > 0) {
			LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
			tLoginVerifyTool.addOm(mResponseBodyElement, "INSUACCNAME",
					sResult.GetText(1, 1));
			tLoginVerifyTool.addOm(mResponseBodyElement, "CREATEPAY",
					sResult.GetText(1, 2));
			tLoginVerifyTool.addOm(mResponseBodyElement, "INSUACCBALA",
					sResult.GetText(1, 3));
			tLoginVerifyTool.addOm(mResponseBodyElement, "CONTSTARTVALUE",
					sResult.GetText(1, 4));
			
		}
		return true;
	}
}
