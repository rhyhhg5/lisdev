package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_insuredinfoquery extends CRMAbstractInterface {

	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";
	/** 客户号 **/
	private static final String INSUREDNO = "INSUREDNO";
	private String mInsured = "";

	public Crm_insuredinfoquery() {
		this.mRootName = "CRM_INSUREDINFOQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		String tsql = "";
		String tsql1 = "";
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();
				if ((!"".equals(mContno)) && mContno != null) {
					tsql += " and a.contno='" + mContno + "' ";
					tsql1 += " and a.contno='" + mContno + "' ";
                    continue;
				}
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("保单号、客户号都不能为空!");
                return false;
				
			}
			if (INSUREDNO.equals(tChildElement.getLocalName())) {
				mInsured = tChildElement.getText();
				if ((!"".equals(mInsured)) && mInsured != null) {
					tsql += " and a.insuredno='" + mInsured + "' ";
					tsql1 += " and a.customerno='" + mInsured + "' ";
                    continue;
				}
                mErrCode = ERRCODE_CHECK;
                mErrors.addOneError("保单号、客户号都不能为空!");
                return false;
				
			}
		}
		if (("".equals(mContno) || mContno == null)
				&& ("".equals(mInsured) || mInsured == null)) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("保单号、客户号必录其一");
			return false;
		}
		ExeSQL tExeSQL = new ExeSQL();
		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();

		String sql = "select a.insuredno 客户号,a.birthday 出生日期,a.idtype 证件类型,"
				+ "(Select Occupationname From LDOccupation Where Occupationcode = a.Occupationcode) 职业,"
				+ "a.occupationcode 职业代码,b.mobile 移动电话,b.phone 联系电话,b.zipcode 邮政编码,"
				+ "a.name 姓名,a.marriage 婚姻状况,a.idno 证件号码, b.email 电子邮箱,"
				+ "b.postaladdress 联系地址,a.sex 性别,a.relationtomaininsured 与第一被保险人关系,"
				+ "a.nativeplace 国籍,b.companyaddress 工作单位,a.relationtoappnt 与投保人关系 "
				+ "  from lcinsured a, lcaddress b  where 1=1 "
				+ tsql
				+ " and a.insuredno = b.customerno  and a.addressno = b.addressno union all "
				+ "select a.insuredno 客户号,a.birthday 出生日期,a.idtype 证件类型,"
				+ "(Select Occupationname From LDOccupation Where Occupationcode = a.Occupationcode) 职业,"
				+ "a.occupationcode 职业代码,b.mobile 移动电话,b.phone 联系电话,b.zipcode 邮政编码,"
				+ "a.name 姓名,a.marriage 婚姻状况, a.idno 证件号码,b.email 电子邮箱,"
				+ "b.postaladdress 联系地址,a.sex 性别,a.relationtomaininsured 与第一被保险人关系,"
				+ "a.nativeplace 国籍, b.companyaddress 工作单位,a.relationtoappnt 与投保人关系 "
				+ "from lbinsured a, lcaddress b where 1=1 "
				+ tsql
				+ " and a.insuredno = b.customerno and a.addressno = b.addressno with ur";
		SSRS sResult;
		sResult = tExeSQL.execSQL(sql);
		if (sResult.getMaxRow() > 0) {
			tLoginVerifyTool.addOm(mResponseBodyElement, "INSUREDNO",
					sResult.GetText(1, 1));
			tLoginVerifyTool.addOm(mResponseBodyElement, "BIRTHDAY",
					sResult.GetText(1, 2));
			tLoginVerifyTool.addOm(mResponseBodyElement, "IDTYP",
					sResult.GetText(1, 3));
			tLoginVerifyTool.addOm(mResponseBodyElement, "OCCUPATIONNAME",
					sResult.GetText(1, 4));
			tLoginVerifyTool.addOm(mResponseBodyElement, "OCCUPATIONCODE",
					sResult.GetText(1, 5));
			tLoginVerifyTool.addOm(mResponseBodyElement, "MOBILE",
					sResult.GetText(1, 6));
			tLoginVerifyTool.addOm(mResponseBodyElement, "PHONE",
					sResult.GetText(1, 7));
			tLoginVerifyTool.addOm(mResponseBodyElement, "ZIPCODE",
					sResult.GetText(1, 8));
			tLoginVerifyTool.addOm(mResponseBodyElement, "NAME",
					sResult.GetText(1, 9));
			tLoginVerifyTool.addOm(mResponseBodyElement, "MARRIAGE",
					sResult.GetText(1, 10));
			tLoginVerifyTool.addOm(mResponseBodyElement, "IDNO",
					sResult.GetText(1, 11));
			tLoginVerifyTool.addOm(mResponseBodyElement, "EMAIL",
					sResult.GetText(1, 12));
			tLoginVerifyTool.addOm(mResponseBodyElement, "POSTALADDRESS",
					sResult.GetText(1, 13));
			tLoginVerifyTool.addOm(mResponseBodyElement, "SEX",
					sResult.GetText(1, 14));
			tLoginVerifyTool.addOm(mResponseBodyElement,
					"RELATIONTOMAININSURED", sResult.GetText(1, 15));
			tLoginVerifyTool.addOm(mResponseBodyElement, "NATIVEPLACE",
					sResult.GetText(1, 16));
			tLoginVerifyTool.addOm(mResponseBodyElement, "GRPNAME",
					sResult.GetText(1, 17));
			tLoginVerifyTool.addOm(mResponseBodyElement, "RELATIONTOAPPNT",
					sResult.GetText(1, 18));
		}
		String banksql = "select (select bankname from ldbank where bankcode = a.bankcode) 银行,"
				+ "a.bankaccno 帐号,a.accname 户名  from lcinsured a where 1=1 "
				+ tsql
				+ "union all select (select bankname from ldbank where bankcode = a.bankcode) 银行,"
				+ "a.bankaccno 帐号, a.accname 户名  from lbinsured a where 1=1 "
				+ tsql + " with ur";

		SSRS bankResult;
		bankResult = tExeSQL.execSQL(banksql);
		if (bankResult.getMaxRow() > 0) {
			OMElement bankdata = tOMFactory.createOMElement("BANKDATA", null);
			tLoginVerifyTool.addOm(bankdata, "BANKNAME",
					bankResult.GetText(1, 1));
			tLoginVerifyTool.addOm(bankdata, "BANKACCNO",
					bankResult.GetText(1, 2));
			tLoginVerifyTool.addOm(bankdata, "ACCNAME",
					bankResult.GetText(1, 3));
			mResponseBodyElement.addChild(bankdata);
		}
		String penoticesql = "select a.pedate 体检时间, a.peaddress 体检医院, '' 体检费用 "
				+ " from LCPENotice a where 1=1 " + tsql1 + " with ur ";
		SSRS penoticeResult;
		penoticeResult = tExeSQL.execSQL(penoticesql);
		if (penoticeResult.getMaxRow() > 0) {
			OMElement penoticelist = tOMFactory.createOMElement("PENOTICELIST",
					null);
			for (int i = 1; i <= penoticeResult.getMaxRow(); i++) {
				OMElement penoticedata = tOMFactory.createOMElement(
						"PENOTICEDATA", null);
				tLoginVerifyTool.addOm(penoticedata, "PEDATE",
						penoticeResult.GetText(i, 1));
				tLoginVerifyTool.addOm(penoticedata, "PEADDRESS",
						penoticeResult.GetText(i, 2));
				tLoginVerifyTool.addOm(penoticedata, "PEMONEY",
						penoticeResult.GetText(i, 3));
				penoticelist.addChild(penoticedata);
			}
			mResponseBodyElement.addChild(penoticelist);
		}
		String prisksql = "select (select riskname from lmriskapp where riskcode = a.riskcode) 险种名称,"
				+ "a.riskcode 险种代码,a.amnt 保额, a.prem 保费 from lcpol a "
				+ "where 1=1 "
				+ tsql
				+ " union all select (select riskname from lmriskapp where riskcode = a.riskcode) 险种名称,"
				+ "a.riskcode 险种代码,a.amnt 保额,a.prem 保费 "
				+ "from lbpol a where 1=1 " + tsql + " with ur ";
		SSRS riskResult;
		riskResult = tExeSQL.execSQL(prisksql);
		if (riskResult.getMaxRow() > 0) {
			OMElement risklist = tOMFactory.createOMElement("RISKLIST", null);
			for (int j = 1; j <= riskResult.getMaxRow(); j++) {
				OMElement riskdata = tOMFactory.createOMElement("RISKDATA",
						null);
				tLoginVerifyTool.addOm(riskdata, "RISKNAME",
						riskResult.GetText(j, 1));
				tLoginVerifyTool.addOm(riskdata, "RISKCODE",
						riskResult.GetText(j, 2));
				tLoginVerifyTool.addOm(riskdata, "AMNT",
						riskResult.GetText(j, 3));
				tLoginVerifyTool.addOm(riskdata, "PREM",
						riskResult.GetText(j, 4));
				risklist.addChild(riskdata);
			}
			mResponseBodyElement.addChild(risklist);
		}
		return true;
	}

}
