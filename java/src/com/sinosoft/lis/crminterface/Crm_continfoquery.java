package com.sinosoft.lis.crminterface;

import java.util.Iterator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;

import com.ecwebservice.subinterface.LoginVerifyTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class Crm_continfoquery extends CRMAbstractInterface {
	/** 保单号 **/
	private static final String CONTNO = "CONTNO";
	private String mContno = "";

	public Crm_continfoquery() {
		this.mRootName = "CRM_CONTINFOQUERY";
	}

	public boolean returnResponseBody() throws Exception {
		Iterator tBodyIterator = mBodyElement.getChildren();
		while (tBodyIterator.hasNext()) {
			OMElement tChildElement = (OMElement) tBodyIterator.next();
			if (CONTNO.equals(tChildElement.getLocalName())) {
				mContno = tChildElement.getText();	
				if (("".equals(mContno)) || mContno == null) {
			mErrCode = ERRCODE_CHECK;
			mErrors.addOneError("保单号不能为空");
			return false;
		}
			}
		}
		
		

		String contdatasql = "select a.stateflag as 保单状态,(case when a.cardflag='b' then '97' when a.cardflag='c' then '96' when a.cardflag='d' then '98' when a.prtno like 'PD%' then '99' else a.salechnl end) 销售渠道,"
				+ "(select phone from laagent where agentcode=a.agentcode) as 业务员联系电话,"
				+ "CUSTOMGETPOLDATE as 保单回执客户签收日期,SIGNDATE as 承保日期,"
				+ "REMARK as 特别约定,OPERATOR as 最后处理人,AGENTCODE as 业务员代码,"
				+ "(select postaladdress from laagent where agentcode=a.agentcode) as 联系地址,"
				+ "(select branchaddress from labranchgroup where agentgroup=a.agentgroup) as 销售机构地址,a.customgetpoldate as 回执回销日期,"
				+ "a.cvalidate as 保单生效日期,a.uwflag as 核保结论,a.MODIFYDATE as 最后处理时间,"
				+ "(select name from laagent where agentcode=a.agentcode) as 业务员名称,"
				+ "(case (select standbyflag1 from lcpol where contno=a.contno "
				+ "and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and risktype4='4'))"
				+ " when '1' then '是' else '否' end) as 是否缓缴,(select BankName from LDBank where BankCode = a.BankCode) as 银代销售,"
				+ "a.LostTimes as 保单补发次数,a.cinvalidate as 保单满期日,"
				+ "a.managecom as 管理机构,(select BranchCode from LAAgent where AgentCode=a.agentcode ) as 所属部门代码,"
				+ "a.paytodate as 保费交至日, (select Name from LACom where a.Agentcom = Agentcom and  salechnl='03') as 中介销售,"
				+ "(select max(lastgettodate) from ljsgetdraw where contno=a.contno) as 上次抽档日期  "
				+ ",(select agentstate from laagent where agentcode=a.agentcode) 业务员状态 "
				+ "from lccont a where contno='"
				+ mContno
				+ "' union all select (case when a.cardflag in ('9','c','d') then 'x' else a.stateflag end) as 保单状态,"
				+ "(case when a.cardflag='b' then '97' when a.cardflag='c' then '96' when a.cardflag='d' then '98' when a.prtno like 'PD%' then '99' else a.salechnl end) 销售渠道,"
				+ "(select phone from laagent where agentcode=a.agentcode) as 业务员联系电话,"
				+ "CUSTOMGETPOLDATE as 保单回执客户签收日期,SIGNDATE as 承保日期,"
				+ "REMARK as 特别约定,OPERATOR as 最后处理人,AGENTCODE as 业务员代码,"
				+ "(select postaladdress from laagent where agentcode=a.agentcode) as 联系地址,"
				+ "(select branchaddress from labranchgroup where agentgroup=a.agentgroup) as 销售机构地址,a.customgetpoldate as 回执回销日期 ,"
				+ "a.cvalidate as 保单生效日期,a.uwflag as 核保结论,a.MODIFYDATE as 最后处理时间,"
				+ "(select name from laagent where agentcode=a.agentcode) as 业务员名称,"
				+ "(case (select standbyflag1 from lbpol where contno=a.contno "
				+ "and exists (select 1 from lmriskapp where riskcode=lbpol.riskcode and risktype4='4'))"
				+ " when '1' then '是' else '否' end) as 是否缓缴,(select BankName from LDBank where BankCode = a.BankCode) as 银代销售,"
				+ "a.LostTimes as 保单补发次数,a.cinvalidate as 保单满期日,"
				+ "a.managecom as 管理机构,(select BranchCode from LAAgent where AgentCode=a.agentcode ) as 所属部门代码,"
				+ "a.paytodate as 保费交至日, (select Name from LACom where a.Agentcom = Agentcom and  salechnl='03') as 中介销售,"
				+ "(select max(lastgettodate) from ljsgetdraw where contno=a.contno) as 上次抽档日期 "
				+ ",(select agentstate from laagent where agentcode=a.agentcode) 业务员状态 "
				+ "from lbcont a where contno='" + mContno + "' with ur ";

		String crsdatasql = "select Crs_SaleChnl as 交叉销售渠道,"
				+ "GrpAgentCom as 对方机构代码,GrpAgentName aS 对方业务员姓名,"
				+ "Crs_BussType as 交叉销售业务类型,GrpAgentCom as 对方机构名称,"
				+ "GrpAgentIDNo as 对方业务员身份证号,GrpAgentCode as 对方业务员代码"
				+ " from lbcont a  where contno ='"+mContno+"' union "
				+ "select Crs_SaleChnl as 交叉销售渠道,GrpAgentCom as 对方机构代码,"
				+ "GrpAgentName aS 对方业务员姓名,Crs_BussType as 交叉销售业务类型,"
				+ "GrpAgentCom as 对方机构名称,GrpAgentIDNo as 对方业务员身份证号,"
				+ "GrpAgentCode as 对方业务员代码  from lccont a where contno ='"
				+ mContno + "' with ur ";

		String appntdatasql = "select LDPerson.CustomerNo as 客户号,"
				+ " LDPerson.Birthday as 出生日期, LDPerson.IDType as 证件类型,"
				+ " LCAppnt.OccupationCode as 职业代码,"
				+ " LCAddress.Phone as 联系电话,LCAddress.Mobile as 移动电话,"
				+ " LCAddress.ZipCode as 邮政编码,LDPerson.Name as 姓名,"
				+ " LDPerson.Marriage as 婚姻状况,LDPerson.IDNo as 证件号码,"
				+ "LCAddress.EMail as 电子邮箱,"
				+ "LCAddress.PostalAddress as 联系地址,LDPerson.Sex as 性别,"
				+ "LDPerson.NativePlace as 国籍,LDPerson.GrpName as 工作单位"
				+ " from LCAppnt,LDPerson,LCAddress "
				+ " where LCAppnt.ContNo= '"
				+ mContno
				+ "' and LDPerson.CustomerNo=LCAppnt.AppntNo "
				+ "and LDPerson.CustomerNo = LCAddress.CustomerNo "
				+ "and LCAppnt.AddressNo=LCAddress.AddressNo"
				+ " union all "
				+ "select LDPerson.CustomerNo as 客户号,"
				+ "LDPerson.Birthday as 出生日期,"
				+ " LDPerson.IDType as 证件类型,"
				+ "LbAppnt.OccupationCode as 职业代码,"
				+ "LCAddress.Phone as 联系电话,"
				+ "LCAddress.Mobile as 移动电话,"
				+ "LCAddress.ZipCode as 邮政编码,"
				+ "LDPerson.Name as 姓名,"
				+ "LDPerson.Marriage as 婚姻状况,"
				+ "LDPerson.IDNo as 证件号码,"
				+ "LCAddress.EMail as 电子邮箱,"
				+ "LCAddress.PostalAddress as 联系地址,"
				+ "LDPerson.Sex as 性别,"
				+ "LDPerson.NativePlace as 国籍,"
				+ "LDPerson.GrpName as 工作单位"
				+ " from LbAppnt,LDPerson,LCAddress "
				+ " where LbAppnt.ContNo= '"
				+ mContno
				+ "' and LDPerson.CustomerNo=LbAppnt.AppntNo "
				+ " and LDPerson.CustomerNo = LCAddress.CustomerNo "
				+ " and LbAppnt.AddressNo=LCAddress.AddressNo with ur ";

		String paydatasql = "select paymode as 缴费方式,BankAccNo as 帐号,"
				+ "BankCode as 开户银行,prem as 期交保费,AccName as 户名 "
				+ " from lccont where contno ='" + mContno + "' union all "
				+ " select paymode as 缴费方式,BankAccNo as 帐号,"
				+ "BankCode as 开户银行,prem as 期交保费,AccName as 户名  "
				+ " from lbcont where contno='" + mContno + "' with ur ";

		String riskcodesql = "select a.InsuredName as 被保人姓名,a.InsuredNo as 客户号,"
				+ "b.riskname as 险种名称,a.RiskCode as 险种代码,"
				+ "a.Amnt as 保额,a.Prem as 保费,a.CVALIDATE as 生效日期,date(a.EndDate) - 1 day as 满期日期,"
				+ "payintv as 缴费频次,MULT as 档次,PAYTODATE as 交至日期,PAYENDDATE as 终止日期,"
				+ "case when CodeName('polstate',a.PolState) is not null "
				+ "then CodeName('polstate',a.PolState) else CodeName('stateflag',a.stateflag) end as 原因,"
				+ "(select case riskperiod when 'L' then 60 else 30 end from lmriskapp where riskcode=a.riskcode) as 宽限期,"
				+ "case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end as 缴费年期,"
				+ "coalesce((select sum(sumduepaymoney) from ljspayperson where polno=a.polno ),0) as 应缴但未缴保费,"
				+ "a.INSUYEAR||a.INSUYEARflag  as 保障期间  "
				+ " from LCPol a inner join lmrisk b on b.RiskCode=a.RiskCode where a.ContNo='"
				+ mContno
				+ "' union all "
				+ " select a.InsuredName as 被保人姓名,a.InsuredNo as 客户号,"
				+ "b.riskname as 险种名称,a.RiskCode as 险种代码,"
				+ "a.Amnt as 保额,a.Prem as 保费,a.CVALIDATE as 生效日期,date(a.EndDate) - 1 day as 满期日期,"
				+ "payintv as 缴费频次,MULT as 档次,PAYTODATE as 交至日期,PAYENDDATE as 终止日期,"
				+ "case when CodeName('polstate',a.PolState) is not null "
				+ "then CodeName('polstate',a.PolState) else CodeName('stateflag',a.stateflag) end as 原因,"
				+ "(select case riskperiod when 'L' then 60 else 30 end from lmriskapp where riskcode=a.riskcode) as 宽限期,"
				+ "case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end as 缴费年期,"
				+ "coalesce((select sum(sumduepaymoney) from ljspayperson where polno=a.polno ),0) as 应缴但未缴保费,"
				+ "a.INSUYEAR ||a.INSUYEARflag  as 保障期间 "
				+ " from LbPol a inner join lmrisk b on b.RiskCode=a.RiskCode "
				+ " where a.ContNo='" + mContno + "' with ur ";

		String bnflistsql = "select a.Name as 受益人姓名 ,"
				+ "CodeName('relation', a.RelationToInsured) as 与被保人关系,"
				+ "a.sex as 性别,a.birthday as 出生日期,"
				+ "CodeName('idtype', a.IDType) as 证件类型,"
				+ "a.IDNo as 证件号码,"
				+ "a.Bnfgrade  as 受益顺位,"
				+ "a.BnfLot as 受益比例  ,"
				+ "a.InsuredNo as 所属被保人客户号 "
				+ " from lcbnf a where contno='"
				+ mContno
				+ "' group by contno,insuredno,a.Name,a.RelationToInsured,a.sex,a.birthday,a.IDType,a.IDNo,"
				+ "a.Bnfgrade,a.BnfLot,a.polno "
				+ " union all"
				+ " select a.Name as 受益人姓名 ,"
				+ "CodeName('relation', a.RelationToInsured) as 与被保人关系,"
				+ "a.sex as 性别,"
				+ "a.birthday as 出生日期,"
				+ "CodeName('idtype', a.IDType) as 证件类型,a.IDNo as 证件号码,"
				+ "a.Bnfgrade  as 受益顺位,a.BnfLot as 受益比例,"
				+ "a.InsuredNo as 所属被保人客户号 "
				+ " from lbbnf a where contno='"
				+ mContno
				+ "' group by contno,insuredno,a.Name,a.RelationToInsured,a.sex,a.birthday,a.IDType,a.IDNo,"
				+ "a.Bnfgrade,a.BnfLot,a.polno with ur ";
		// 1)合同信息 CONTDATA:contdatasql
		// 2)交叉销售信息 CRSDATA:crsdatasql
		// 3)投保人信息 APPNTDATA:appntdatasql
		// 4)缴费资料 PAYDATA:patdatasql
		// 5)保单险种信息（集合）RISKLIST:riskcodesql
		// 5.1)受益人集合BNFLIST（隶属于险种集合）:bnflistsql

		ExeSQL tExeSQL = new ExeSQL();
		SSRS contdataResult;
		SSRS crsdataResult;
		SSRS appntdataResult;
		SSRS paydataResult;
		SSRS riskcodeResult;
		SSRS bnflistResult;

		contdataResult = tExeSQL.execSQL(contdatasql);
		crsdataResult = tExeSQL.execSQL(crsdatasql);
		appntdataResult = tExeSQL.execSQL(appntdatasql);
		paydataResult = tExeSQL.execSQL(paydatasql);
		riskcodeResult = tExeSQL.execSQL(riskcodesql);
		bnflistResult = tExeSQL.execSQL(bnflistsql);

		OMFactory tOMFactory = OMAbstractFactory.getOMFactory();
		LoginVerifyTool tLoginVerifyTool = new LoginVerifyTool();
		if (contdataResult.getMaxRow() > 0) {
			OMElement contdate = tOMFactory.createOMElement("CONTDATA", null);
			tLoginVerifyTool.addOm(contdate, "STATEFLAG",
					contdataResult.GetText(1, 1));
			tLoginVerifyTool.addOm(contdate, "SALECH",
					contdataResult.GetText(1, 2));
			tLoginVerifyTool.addOm(contdate, "AGENTPHONE",
					contdataResult.GetText(1, 3));
			tLoginVerifyTool.addOm(contdate, "CUSTOMGETPOLDATE",
					contdataResult.GetText(1, 4));
			tLoginVerifyTool.addOm(contdate, "SIGNDATE",
					contdataResult.GetText(1, 5));
			tLoginVerifyTool.addOm(contdate, "REMARK",
					contdataResult.GetText(1, 6));
			tLoginVerifyTool.addOm(contdate, "OPERATOR",
					contdataResult.GetText(1, 7));
			tLoginVerifyTool.addOm(contdate, "AGENTCODE",
					contdataResult.GetText(1, 8));
			tLoginVerifyTool.addOm(contdate, "AGENTPOSTALADDRESS",
					contdataResult.GetText(1, 9));
			tLoginVerifyTool.addOm(contdate, "COMADDRESS",
					contdataResult.GetText(1, 10));
			tLoginVerifyTool.addOm(contdate, "RECEIPTTIMESPAN",
					contdataResult.GetText(1, 11));
			tLoginVerifyTool.addOm(contdate, "CVALIDATE",
					contdataResult.GetText(1, 12));
			tLoginVerifyTool.addOm(contdate, "PASSFLAG",
					contdataResult.GetText(1, 13));
			tLoginVerifyTool.addOm(contdate, "MODIFYTIME",
					contdataResult.GetText(1, 14));
			tLoginVerifyTool.addOm(contdate, "AGENTNAME",
					contdataResult.GetText(1, 15));
			tLoginVerifyTool.addOm(contdate, "SLOWFLAG",
					contdataResult.GetText(1, 16));
			tLoginVerifyTool.addOm(contdate, "AGENTCOMNAME",
					contdataResult.GetText(1, 17));
			tLoginVerifyTool.addOm(contdate, "POLPOLICYNUM",
					contdataResult.GetText(1, 18));
			tLoginVerifyTool.addOm(contdate, "ENDDATE",
					contdataResult.GetText(1, 19));
			tLoginVerifyTool.addOm(contdate, "MANAGECOM",
					contdataResult.GetText(1, 20));
			tLoginVerifyTool.addOm(contdate, "BRANCHCODE",
					contdataResult.GetText(1, 21));
			tLoginVerifyTool.addOm(contdate, "PAYTODATE",
					contdataResult.GetText(1, 22));
			tLoginVerifyTool.addOm(contdate, "AGENCYSALEAGENTCOMNAME",
					contdataResult.GetText(1, 23));
			tLoginVerifyTool.addOm(contdate, "POSTALDATE",
					contdataResult.GetText(1, 24));
			tLoginVerifyTool.addOm(contdate, "AGENTSTATE",
					contdataResult.GetText(1, 25));
			mResponseBodyElement.addChild(contdate);
		}
		if (crsdataResult.getMaxRow() > 0) {
			OMElement crsdata = tOMFactory.createOMElement("CRSDATA", null);
			tLoginVerifyTool.addOm(crsdata, "CRS_SALECHNL",
					crsdataResult.GetText(1, 1));
			tLoginVerifyTool.addOm(crsdata, "GRPAGENTCOM",
					crsdataResult.GetText(1, 2));
			tLoginVerifyTool.addOm(crsdata, "GRPAGENTNAME",
					crsdataResult.GetText(1, 3));
			tLoginVerifyTool.addOm(crsdata, "CRS_BUSSTYPE",
					crsdataResult.GetText(1, 4));
			tLoginVerifyTool.addOm(crsdata, "GRPAGENTCOMNAME",
					crsdataResult.GetText(1, 5));
			tLoginVerifyTool.addOm(crsdata, "GRPAGENTIDNO",
					crsdataResult.GetText(1, 6));
			tLoginVerifyTool.addOm(crsdata, "GRPAGENTCODE",
					crsdataResult.GetText(1, 7));
			mResponseBodyElement.addChild(crsdata);
		}
		if (appntdataResult.getMaxRow() > 0) {
			OMElement appntdate = tOMFactory.createOMElement("APPNTDATA", null);
			tLoginVerifyTool.addOm(appntdate, "APPNTNO",
					appntdataResult.GetText(1, 1));
			tLoginVerifyTool.addOm(appntdate, "APPNTBIRTHDAY",
					appntdataResult.GetText(1, 2));
			tLoginVerifyTool.addOm(appntdate, "IDTYPE",
					appntdataResult.GetText(1, 3));
			tLoginVerifyTool.addOm(appntdate, "OCCUPATIONCODE",
					appntdataResult.GetText(1, 4));
			tLoginVerifyTool.addOm(appntdate, "PHONE",
					appntdataResult.GetText(1, 5));
			tLoginVerifyTool.addOm(appntdate, "MOBILE",
					appntdataResult.GetText(1, 6));
			tLoginVerifyTool.addOm(appntdate, "ZIPCODE",
					appntdataResult.GetText(1, 7));
			tLoginVerifyTool.addOm(appntdate, "APPNTNAME",
					appntdataResult.GetText(1, 8));
			tLoginVerifyTool.addOm(appntdate, "MARRIAGE",
					appntdataResult.GetText(1, 9));
			tLoginVerifyTool.addOm(appntdate, "IDNO",
					appntdataResult.GetText(1, 10));
			tLoginVerifyTool.addOm(appntdate, "MAIL",
					appntdataResult.GetText(1, 11));
			tLoginVerifyTool.addOm(appntdate, "POSTALADDRESS",
					appntdataResult.GetText(1, 12));
			tLoginVerifyTool.addOm(appntdate, "APPNTSEX",
					appntdataResult.GetText(1, 13));
			tLoginVerifyTool.addOm(appntdate, "NATIVEPLACE",
					appntdataResult.GetText(1, 14));
			tLoginVerifyTool.addOm(appntdate, "GRPNAME",
					appntdataResult.GetText(1, 15));
			mResponseBodyElement.addChild(appntdate);
		}
		if (paydataResult.getMaxRow() > 0) {
			OMElement paytdate = tOMFactory.createOMElement("PAYDATA", null);
			tLoginVerifyTool.addOm(paytdate, "PAYMODE",
					paydataResult.GetText(1, 1));
			tLoginVerifyTool.addOm(paytdate, "BANKACCNO",
					paydataResult.GetText(1, 2));
			tLoginVerifyTool.addOm(paytdate, "BANKNAME",
					paydataResult.GetText(1, 3));
			tLoginVerifyTool.addOm(paytdate, "PREM",
					paydataResult.GetText(1, 4));
			tLoginVerifyTool.addOm(paytdate, "ACCNAME",
					paydataResult.GetText(1, 5));
			mResponseBodyElement.addChild(paytdate);
		}
		if (riskcodeResult.getMaxRow() > 0) {
			OMElement risklist = tOMFactory.createOMElement("RISKLIST", null);
			for (int i = 1; i <= riskcodeResult.MaxRow; i++) {
				OMElement riskdata = tOMFactory.createOMElement("RISKDATA",
						null);
				tLoginVerifyTool.addOm(riskdata, "INSUREDNAME",
						riskcodeResult.GetText(i, 1));
				tLoginVerifyTool.addOm(riskdata, "INSUREDNO",
						riskcodeResult.GetText(i, 2));
				tLoginVerifyTool.addOm(riskdata, "RISKNAME",
						riskcodeResult.GetText(i, 3));
				tLoginVerifyTool.addOm(riskdata, "RISKCODE",
						riskcodeResult.GetText(i, 4));
				tLoginVerifyTool.addOm(riskdata, "AMNT",
						riskcodeResult.GetText(i, 5));
				tLoginVerifyTool.addOm(riskdata, "PREM",
						riskcodeResult.GetText(i, 6));
				tLoginVerifyTool.addOm(riskdata, "CVALIDATE",
						riskcodeResult.GetText(i, 7));
				tLoginVerifyTool.addOm(riskdata, "ENDDATE",
						riskcodeResult.GetText(i, 8));
				tLoginVerifyTool.addOm(riskdata, "PAYINTV",
						riskcodeResult.GetText(i, 9));
				tLoginVerifyTool.addOm(riskdata, "MULN",
						riskcodeResult.GetText(i, 10));
				tLoginVerifyTool.addOm(riskdata, "PAYDATE",
						riskcodeResult.GetText(i, 11));
				tLoginVerifyTool.addOm(riskdata, "PAYENDDATE",
						riskcodeResult.GetText(i, 12));
				tLoginVerifyTool.addOm(riskdata, "POLSTATE",
						riskcodeResult.GetText(i, 13));
				tLoginVerifyTool.addOm(riskdata, "GRACEPERIOD",
						riskcodeResult.GetText(i, 14));
				tLoginVerifyTool.addOm(riskdata, "PAYENDYEAR",
						riskcodeResult.GetText(i, 15));
				tLoginVerifyTool.addOm(riskdata, "DUEPAYMONEY",
						riskcodeResult.GetText(i, 16));
				tLoginVerifyTool.addOm(riskdata, "INSUYEAR",
						riskcodeResult.GetText(i, 17));
				if (bnflistResult.getMaxRow() > 0) {
			OMElement bnflist = tOMFactory.createOMElement("BNFLIST", null);
			for (int j = 1; j <= bnflistResult.MaxRow; j++) {
				OMElement bnfdata = tOMFactory
						.createOMElement("BNFDATA", null);
				tLoginVerifyTool.addOm(bnfdata, "NAME",
						bnflistResult.GetText(j, 1));
				tLoginVerifyTool.addOm(bnfdata, "RELATIONTOINSURED",
						bnflistResult.GetText(j, 2));
				tLoginVerifyTool.addOm(bnfdata, "INSUREDNO",
						bnflistResult.GetText(j, 9));
				tLoginVerifyTool.addOm(bnfdata, "SEX",
						bnflistResult.GetText(j, 3));
				tLoginVerifyTool.addOm(bnfdata, "BIRTHDAY",
						bnflistResult.GetText(j, 4));
				tLoginVerifyTool.addOm(bnfdata, "IDTYPE",
						bnflistResult.GetText(j, 5));
				tLoginVerifyTool.addOm(bnfdata, "IDNO",
						bnflistResult.GetText(j, 6));
				tLoginVerifyTool.addOm(bnfdata, "BNFGRADE",
						bnflistResult.GetText(j, 7));
				tLoginVerifyTool.addOm(bnfdata, "BNFLOT",
						bnflistResult.GetText(j, 8));
				bnflist.addChild(bnfdata);
			}
			riskdata.addChild(bnflist);
		}
				risklist.addChild(riskdata);
			}
			mResponseBodyElement.addChild(risklist);
		}
		
		return true;
	}
}
