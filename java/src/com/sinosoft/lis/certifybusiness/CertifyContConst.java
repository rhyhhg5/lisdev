/**
 * 2008-11-14
 */
package com.sinosoft.lis.certifybusiness;

/**
 * 
 * @author LY
 *
 */
public class CertifyContConst
{
    // LCGrpCont->CardFlag
    /** 卡折业务团体无名单保单标志 */
    public static final String LGC_CC_CONTFLAG = new String("2");
    /** 卡折业务团体无名单网销保单标志 */
    public static final String LGW_CC_CONTFLAG = new String("3");
    // --------------------

    // LICertify->State
    /** 导入待确认状态 */
    public static final String CC_IMPORT_FIRST = new String("00");

    /** 导入确认状态 */
    public static final String CC_IMPORT_CONFIRM = new String("01");

    /** 单证结算状态 */
    public static final String CC_CASHING_UP = new String("02");

    /** 已产生结算单待收费 */
    public static final String CC_SETTLEMENT = new String("03");

    /** 已结算 */
    public static final String CC_SETTLEMENT_CONFIRM = new String("04");

    // --------------------

    // LICertifyInsured->State
    /** 导入状态 */
    public static final String CI_IMPORT_FIRST = new String("00");

    // --------------------

    // LICertify->ActiveFlag
    /** 未激活 */
    public static final String CC_ACTIVEFLAG_INACTIVE = new String("00");

    /** 导入时已激活 */
    public static final String CC_ACTIVEFLAG_ACTIVE = new String("01");

    /** 网站激活 */
    public static final String CC_ACTIVEFLAG_WEBACTIVE = new String("02");

    // --------------------

    // LICertify->WSState
    /** 未实名化 */
    public static final String CC_WSSTATE_UNCREATED = new String("00");

    /** 已实名化 */
    public static final String CC_WSSTATE_CREATED = new String("01");

    // --------------------

    // LICardActiveInfoList->DealFlag
    /** 激活导入待处理 */
    public static final String CA_ACT_INFO_UNDEALED = new String("00");

    /** 激活导入已处理 */
    public static final String CA_ACT_INFO_DEALED = new String("01");

    /** 待激活卡信息错误－旧卡数据 */
    public static final String CA_ACT_INFO_ERR_OLDCARD = new String("02");

    /** 待激活卡信息错误－待激活基础数据不全 */
    public static final String CA_ACT_INFO_ERR_BASEINFOERR = new String("03");
    
    /** 待激活卡信息错误－待激活卡号在新卡折系统中不存在 */
    public static final String CA_ACT_INFO_ERR_CARDNONOTEXISTS = new String("04");

    // --------------------

    private CertifyContConst()
    {
    }
}
