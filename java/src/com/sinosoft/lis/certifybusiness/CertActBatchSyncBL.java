/**
 * 2010-10-25
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertActBatchSyncBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 卡号清单 */
    private String[] mCardNoList = null;

    public CertActBatchSyncBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCardNoList = (String[]) mTransferData.getValueByName("CardNoList");
        if (mCardNoList == null || mCardNoList.length == 0)
        {
            buildError("getInputData", "获取单证清单信息失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("ActSync".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = actCertInfoSync();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap actCertInfoSync()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        String tBatchNo = getBatchNo();
        if (tBatchNo == null || tBatchNo.equals(""))
        {
            buildError("getInputData", "获取导入批次号失败。");
            return null;
        }

        CertifyDiskImportLog tImportLog = null;
        try
        {
            tImportLog = new CertifyDiskImportLog(mGlobalInput, tBatchNo);
        }
        catch (Exception e)
        {
            String tStrErr = "创建日志失败。";
            buildError("getInputData", tStrErr);
            e.printStackTrace();
            return null;
        }

        for (int i = 0; i < mCardNoList.length; i++)
        {
            TransferData tTransferData = new TransferData();
            String tCardNo = mCardNoList[i];
            tTransferData.setNameAndValue("CardNo", tCardNo);
            tTransferData.setNameAndValue("BatchNo", tBatchNo);

            VData tInputData = new VData();
            tInputData.add(mGlobalInput);
            tInputData.add(tTransferData);

            CertActSyncBL tLogicBL = new CertActSyncBL();
            tTmpMap = null;
            tTmpMap = tLogicBL.getSubmitMap(tInputData, "ActSync");
            if (tTmpMap == null)
            {
                this.mErrors.copyAllErrors(tLogicBL.mErrors);
                String tStrErr = tLogicBL.mErrors.getFirstError();
                if (!tImportLog.errLog(tCardNo, tStrErr))
                {
                    mErrors.copyAllErrors(tImportLog.mErrors);
                    continue;
                }
                continue;
            }
            tMMap.add(tTmpMap);
            tTmpMap = null;
        }

        if (tMMap != null)
        {
            // 终止导入日志。
            MMap tTmpLogEnd = null;
            tTmpLogEnd = tImportLog.logEnd4Map();
            if (tTmpLogEnd == null)
            {
                String tStrErr = "终止导入日志动作失败。";
                buildError("createCertifyList", tStrErr);
                System.out.println(tStrErr);
                return null;
            }
            tMMap.add(tTmpLogEnd);
            tTmpLogEnd = null;
            // ---------------------------
        }

        return tMMap;
    }

    private String getBatchNo()
    {
        String tBatchNo = null;
        try
        {
            tBatchNo = PubFun1.CreateMaxNo("LICCARDBNO", PubFun.getCurrentDate());
            System.out.println("BatchNo:" + tBatchNo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            String tStrErr = "导入批次号生成失败。";
            buildError("getBatchNo", tStrErr);
            return null;
        }

        return tBatchNo;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ":" + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
