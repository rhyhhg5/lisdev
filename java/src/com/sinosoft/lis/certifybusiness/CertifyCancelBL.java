/**
 * 2010-2-1
 */
package com.sinosoft.lis.certifybusiness;

import java.util.Hashtable;

import com.sinosoft.lis.certify.AuditCancelBL;
import com.sinosoft.lis.certify.CertifyFunc;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 根据单证号作废单证。
 * <br />如需作废，需提供明确的单证号码。
 * <br />每次作废逻辑，仅处理一个单证号码。
 * 
 * @author LY
 *
 */
public class CertifyCancelBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mCardNo = null;

    public CertifyCancelBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCardNo = (String) mTransferData.getValueByName("CardNo");
        if (mCardNo == null || mCardNo.equals(""))
        {
            buildError("getInputData", "获取单证号码失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        if ("Cancel".equals(mOperate))
        {
            if (!cancleCertify())
            {
                return false;
            }
        }

        return true;
    }

    private boolean cancleCertify()
    {
        LZCardSchema tCardInfo = loadCardInfo(mCardNo);
        if (tCardInfo == null)
        {
            return false;
        }

        LZCardSchema tCancleCardInfo = new LZCardSchema();
        tCancleCardInfo.setCertifyCode(tCardInfo.getCertifyCode());
        tCancleCardInfo.setStartNo(tCardInfo.getStartNo());
        tCancleCardInfo.setEndNo(tCardInfo.getEndNo());
        tCancleCardInfo.setStateFlag("6");
        tCancleCardInfo.setState("6");
        tCancleCardInfo.setSendOutCom(tCardInfo.getReceiveCom());
        tCancleCardInfo.setReceiveCom(tCardInfo.getReceiveCom());
        tCancleCardInfo.setSumCount(1);
        tCancleCardInfo.setHandler(mGlobalInput.Operator);
        tCancleCardInfo.setHandleDate(PubFun.getCurrentDate());

        LZCardSet tCardList = new LZCardSet();
        tCardList.add(tCancleCardInfo);

        VData vData = new VData();
        vData.addElement(mGlobalInput);
        vData.addElement(tCardList);
        vData.addElement("SINGLE");
        Hashtable hashParams = new Hashtable();
        hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CARD);
        vData.addElement(hashParams);

        AuditCancelBL tAuditCancelBL = new AuditCancelBL();
        if (!tAuditCancelBL.submitData(vData, "INSERT"))
        {
            this.mErrors.copyAllErrors(tAuditCancelBL.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 根据单证号码获取单证相关信息。
     * @param cCardNo
     * @return
     */
    private LZCardSchema loadCardInfo(String cCardNo)
    {
        String tStrSql = " select lzc.* from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode "
                + " and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where 1 = 1 " + " and lzcn.CardNo = '" + cCardNo + "' ";
        LZCardSet tCardInfoSet = null;
        tCardInfoSet = new LZCardDB().executeQuery(tStrSql);
        if (tCardInfoSet == null || tCardInfoSet.size() != 1)
        {
            buildError("cancelCertify", "获取系统中单证信息失败。");
            return null;
        }

        return tCardInfoSet.get(1);
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return mResult;
    }
}
