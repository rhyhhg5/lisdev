/**
 * 2008-12-05
 */
package com.sinosoft.lis.certifybusiness;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * @author LY
 *
 */
public class CCSFirstPayBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mOperate = "";

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCGrpContSchema mGrpContInfo = null;

    private String mLoadFlag;

    private String mflag = null;

    public CCSFirstPayBL()
    {
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        try
        {
            //            if (!cOperate.equals("CONFIRM"))
            //            {
            //                buildError("submitData", "不支持的操作字符串");
            //                return false;
            //            }

            // 得到外部传入的数据，将数据备份到本类中（不管有没有operate,都要执行这一部）
            if (!getInputData(cInputData))
            {
                return false;
            }

            //            if (cOperate.equals("CONFIRM"))
            //            {
            mResult.clear();

            // 准备所有要打印的数据
            if (!getPrintData())
            {
                return false;
            }
            //            }

            return true;

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }

    private String getDate(Date date)
    {

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(date);
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));
        if (!loadPrintManager())
        {
            return false;
        }

        TransferData tTransferData = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        if (tTransferData != null)
        {
            this.mLoadFlag = (String) tTransferData.getValueByName("LoadFlag");
        }
        //只赋给schema一个prtseq

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        mGrpContInfo = loadGrpContInfo(mLOPRTManagerSchema.getOtherNo());
        if (mGrpContInfo == null)
        {
            buildError("getInputData", "未找到团体合同信息。");
            return false;
        }

        mflag = mOperate;

        return true;
    }

    //得到返回值
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CCSFirstPayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    // 准备所有要打印的数据
    private boolean getPrintData()
    {
        XmlExport xmlExport = new XmlExport();
        TextTag tTextTag = new TextTag();

        xmlExport.createDocument("CCSFirstPay", "");

        // 预设pdf接口节点。
        if (!dealPdfBaseInfo(tTextTag))
        {
            return false;
        }
        // ------------------------------

        // 处理团体保单层信息。
        if (!dealGrpContInfo(tTextTag))
        {
            return false;
        }
        // ------------------------------

        // 处理销售信息。
        if (!dealAgentInfo(tTextTag))
        {
            return false;
        }
        // ------------------------------

        if (tTextTag.size() > 0)
        {
            xmlExport.addTextTag(tTextTag);
        }

        // 处理卡折清单。
        if (!dealCardListInfo(xmlExport))
        {
            return false;
        }
        // ------------------------------

        // 置文件终止节点。
        if (!setEndNode(xmlExport))
        {
            return false;
        }
        // ------------------------------

        //xmlExport.outputDocumentToFile("e:\\", "CCSFirstPay");

        mResult.clear();
        mResult.addElement(xmlExport);

        // 处理打印轨迹。
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema
                .setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);
        mResult.add(mLOPRTManagerSchema);
        // ------------------------------

        return true;
    }

    /**
     * 产生PDF打印相关基本信息。
     * @param cTextTag
     * @return
     */
    private boolean dealPdfBaseInfo(TextTag cTextTag)
    {
        TextTag tTmpTextTag = cTextTag;

        // 单证类型标志。
        tTmpTextTag.add("JetFormType", "LC01");
        // -------------------------------

        // 四位管理机构代码。
        String sqlusercom = "select comcode from lduser where usercode='"
                + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600")
                || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            buildError("dealGrpContInfo", "操作员机构查询出错!");
            return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"
                + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        tTmpTextTag.add("ManageComLength4", printcode);
        // -------------------------------

        // 客户端IP。
        tTmpTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        // -------------------------------

        // 预览标志。
        if (mflag.equals("batch"))
        {
            tTmpTextTag.add("previewflag", "0");
        }
        else
        {
            tTmpTextTag.add("previewflag", "1");
        }
        // -------------------------------

        return true;
    }

    /**
     * 处理团体保单层信息。
     * @param cTextTag
     * @return
     */
    private boolean dealGrpContInfo(TextTag cTextTag)
    {
        TextTag tTmpTextTag = cTextTag;

        tTmpTextTag.add("GrpContNo", mGrpContInfo.getGrpContNo());
        tTmpTextTag.add("PrtNo", mGrpContInfo.getPrtNo());
        tTmpTextTag.add("PrtSeq", mLOPRTManagerSchema.getPrtSeq());
        tTmpTextTag.add("PayMode", getPayMode(mGrpContInfo.getPayMode()));

        tTmpTextTag.add("AppntName", mGrpContInfo.getGrpName());
        tTmpTextTag.add("AppntAddr", "");
        tTmpTextTag.add("AppntZipcode", "");
        tTmpTextTag.add("HandlerName", "");

        String tManageCom = mGrpContInfo.getManageCom();
        LDComDB tComInfo = new LDComDB();
        tComInfo.setComCode(tManageCom);
        if (!tComInfo.getInfo())
        {
            buildError("dealGrpContInfo", "管理机构[" + tManageCom + "]信息未找到。");
            return false;
        }

        tTmpTextTag.add("ManageCom", mGrpContInfo.getManageCom());

        tTmpTextTag.add("Letterservicename", tComInfo.getLetterServiceName());
        tTmpTextTag.add("Address", tComInfo.getAddress());
        tTmpTextTag.add("ZipCode", tComInfo.getZipCode());
        tTmpTextTag.add("Fax", tComInfo.getFax());
        tTmpTextTag.add("Phone", tComInfo.getPhone());
        tTmpTextTag.add("ServicePhone", tComInfo.getServicePhone());

        String tStrSql = " select nvl(sum(tmp.Prem), 0) SumPrem, count(1) CertCount "
                + " from "
                + " ( "
                + " select nvl(Prem, 0) Prem, CardNo from LICertify where GrpContNo = '"
                + mGrpContInfo.getGrpContNo()
                + "' "
                + " union all "
                + " select nvl((-1 * Prem), 0) Prem, CardNo from LIBCertify where GrpContNo = '"
                + mGrpContInfo.getGrpContNo() + "' " + " ) as tmp ";
        SSRS tSSRS = new ExeSQL().execSQL(tStrSql);
        if (tSSRS == null || tSSRS.MaxRow != 1)
        {
            buildError("dealGrpContInfo", "查询卡折清单出错。");
            return false;
        }

        String[] tDatas = tSSRS.getRowData(1);

        tTmpTextTag.add("SumPrem", tDatas[0]);
        tTmpTextTag.add("CertifyCount", tDatas[1]);

        tTmpTextTag.add("Today", PubFun.getCurrentDate());

        return true;
    }

    /**
     * 处理销售信息。
     * @param cTextTag
     * @return
     */
    private boolean dealAgentInfo(TextTag cTextTag)
    {
        TextTag tTmpTextTag = cTextTag;
        ExeSQL tExeSQL = new ExeSQL();

        String tAgentCode = mGrpContInfo.getAgentCode();

        String tStrSql = " select laa.AgentCode, laa.Name, laa.Mobile, labg.BranchAttr, labg.Name,laa.groupagentcode "
                + " from LAAgent laa "
                + " inner join LABranchGroup labg on labg.AgentGroup = laa.AgentGroup "
                + " where laa.AgentCode = '" + tAgentCode + "' ";

        SSRS tSSRS = tExeSQL.execSQL(tStrSql);

        if (tSSRS == null || tSSRS.MaxRow != 1)
        {
            buildError("dealAgentInfo", "业务员[" + tAgentCode + "]信息未找到或出现多条。");
            return false;
        }

        String[] tDatas = tSSRS.getRowData(1);

        tTmpTextTag.add("AgentCode", tDatas[5]);
        tTmpTextTag.add("AgentName", tDatas[1]);
        tTmpTextTag.add("AgentMobile", tDatas[2]);

        tTmpTextTag.add("BarCode1", tDatas[3]);
        tTmpTextTag.add("BarCodeName", tDatas[4]);
        tTmpTextTag.add("BarCodeParam1", "");

        String tAgentCom = mGrpContInfo.getAgentCom();
        String tAgentComName = tExeSQL
                .getOneValue("select lac.Name from LACom lac where lac.AgentCom = '"
                        + tAgentCom + "'");
        tTmpTextTag.add("AgentCom", tAgentCom);
        tTmpTextTag.add("AgentComName", tAgentComName);

        return true;
    }

    private boolean dealCardListInfo(XmlExport cXmlExport)
    {
//        SSRS tCardInfoList = null;
        String tStrSql = " select "
                + " lict.CertifyCode, lict.CardNo, lict.CValidate, lict.Prem, "
                + " lmcd.CertifyName, lict.BatchNo, CodeName('cardstatus', lict.certifystatus) CertStatus  "
                + " from LICertify lict "
                + " inner join LMCertifyDes lmcd on lmcd.CertifyCode = lict.CertifyCode "
                + " where 1 = 1 "
                + " and lict.GrpContNo = '"
                + mGrpContInfo.getGrpContNo()
                + "' "
                + " union all "
                + " select "
                + " libct.CertifyCode, libct.CardNo, libct.CValidate, (-1 * libct.Prem) Prem, "
                + " lmcd.CertifyName, libct.BatchNo, CodeName('cardstatus', libct.certifystatus) CertStatus  "
                + " from LIBCertify libct "
                + " inner join LMCertifyDes lmcd on lmcd.CertifyCode = libct.CertifyCode "
                + " where 1 = 1 " + " and libct.GrpContNo = '"
                + mGrpContInfo.getGrpContNo() + "' ";
//        tCardInfoList = new ExeSQL().execSQL(tStrSql);
//
//        if (tCardInfoList == null || tCardInfoList.MaxRow == 0)
//        {
//            buildError("dealCardListInfo", "查询卡折清单出错。");
//            return false;
//        }

        String[] tCertifyListInfoTitle = new String[8];

        tCertifyListInfoTitle[0] = "Id";
        tCertifyListInfoTitle[1] = "CertifyCode";
        tCertifyListInfoTitle[2] = "CardNo";
        tCertifyListInfoTitle[3] = "CValidate";
        tCertifyListInfoTitle[4] = "Prem";
        tCertifyListInfoTitle[5] = "CertifyName";
        tCertifyListInfoTitle[6] = "BatchNo";
        tCertifyListInfoTitle[7] = "CertStatus";

        ListTable tListTable = new ListTable();
        tListTable.setName("CertifyListInfo");

        String[] oneCardInfo = null;
        
        RSWrapper tRSWrapper = new RSWrapper();
        SSRS tCardInfoList = new SSRS();
        tRSWrapper.prepareData(null,tStrSql);
        try
        {
			do
			{
				tCardInfoList = tRSWrapper.getSSRS();
				if (tCardInfoList != null && tCardInfoList.MaxRow > 0) {
					for (int i = 1; i <= tCardInfoList.MaxRow; i++)
			        {
						String[] tTmpCardInfo = tCardInfoList.getRowData(i);

			            oneCardInfo = new String[8];
			            oneCardInfo[0] = String.valueOf(i);
			            oneCardInfo[1] = tTmpCardInfo[0];
			            oneCardInfo[2] = tTmpCardInfo[1];
			            oneCardInfo[3] = tTmpCardInfo[2];
			            oneCardInfo[4] = tTmpCardInfo[3];
			            oneCardInfo[5] = tTmpCardInfo[4];
			            oneCardInfo[6] = tTmpCardInfo[5];
			            oneCardInfo[7] = tTmpCardInfo[6];

			            tListTable.add(oneCardInfo);
			        }
				}
			}
			while (tCardInfoList != null && tCardInfoList.MaxRow > 0);
		}
        catch (Exception ex)
        {
            ex.printStackTrace();
            tRSWrapper.close();
        }finally{
        	try{
        		tRSWrapper.close();
        	}catch(Exception ex){}
        }

//        for (int i = 1; i <= tCardInfoList.MaxRow; i++)
//        {
//            String[] tTmpCardInfo = tCardInfoList.getRowData(i);
//
//            oneCardInfo = new String[8];
//            oneCardInfo[0] = String.valueOf(i);
//            oneCardInfo[1] = tTmpCardInfo[0];
//            oneCardInfo[2] = tTmpCardInfo[1];
//            oneCardInfo[3] = tTmpCardInfo[2];
//            oneCardInfo[4] = tTmpCardInfo[3];
//            oneCardInfo[5] = tTmpCardInfo[4];
//            oneCardInfo[6] = tTmpCardInfo[5];
//            oneCardInfo[7] = tTmpCardInfo[6];
//
//            tListTable.add(oneCardInfo);
//        }

        cXmlExport.addListTable(tListTable, tCertifyListInfoTitle);

        return true;
    }

    /**
     * 设置文件终止节点。
     * @param cXmlExport
     * @return
     */
    private boolean setEndNode(XmlExport cXmlExport)
    {
        ListTable tEndTable = new ListTable();

        tEndTable.setName("END");
        String[] tEndTitle = new String[0];

        cXmlExport.addListTable(tEndTable, tEndTitle);

        return true;
    }

    /**
     * 根据团体合同号，加载团体保单信息。
     * @param cGrpContNo
     * @return
     */
    private LCGrpContSchema loadGrpContInfo(String cGrpContNo)
    {
        LCGrpContDB tGrpContInfo = new LCGrpContDB();
        tGrpContInfo.setGrpContNo(cGrpContNo);
        if (!tGrpContInfo.getInfo())
        {
            return null;
        }

        return tGrpContInfo.getSchema();
    }

    private String getPayMode(String tPayMode)
    {
        return StrTool.cTrim(tPayMode).equals("1") ? "现金" : "转账支票";
    }

    /**
     *
     * @return LOPRTManagerDB
     * @throws Exception
     */
    private boolean loadPrintManager()
    {
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setSchema(mLOPRTManagerSchema);
        if (!tLOPRTManagerDB.getInfo())
        {
            buildError("loadPrintManager", "查询打印管理轨迹失败。");
            return false;
        }
        mLOPRTManagerSchema = tLOPRTManagerDB.getSchema();

        return true;
    }

    public CErrors getErrors()
    {
        return null;
    }

}
