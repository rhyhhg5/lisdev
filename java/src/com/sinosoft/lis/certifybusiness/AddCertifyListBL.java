/**
 * 2008-11-12
 */
package com.sinosoft.lis.certifybusiness;

import java.math.BigDecimal;
//import java.util.Calendar;
//import java.util.Date;





import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.Arith;
//import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LIBCertifySchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LBMissionSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class AddCertifyListBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mFilePath = null;

    private String mFileName = null;

    private String mBatchNo = null;

    private CertifyInfoColl mCertifyInfoList = null;

    /**
     * 退保部分的数据清单
     */
    private LICertifySet mWTCertifyInfoList = null;

    private CertifyDiskImportLog mImportLog = null;

    private String[] mSheetName = { "LICertify", "Ver", "LICertifyInsured" };

    private String mConfigName = "CertifyListDiskImport.xml";

    public AddCertifyListBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        if ("ImportConfirm".equals(mOperate))
        {
            actCard();
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCertifyInfoList = null;

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "获取批次信息失败，请检查文件名是否符合批次命名规则。");
            return false;
        }

        try
        {
            mImportLog = new CertifyDiskImportLog(mGlobalInput, mBatchNo);
        }
        catch (Exception e)
        {
            String tStrErr = "创建日志失败。";
            buildError("getInputData", tStrErr);
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Import".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = importCertifyList();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        if ("ImportConfirm".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = confirmImportList();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        if ("ImportDelete".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = deleteImportList();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        if ("ActiveImport".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = activeImportList();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap importCertifyList()
    {
        MMap tMMap = new MMap();

        MMap tTmpMap = null;

        // 准备导入所需数据。
        if (!prepareBeforeImportListDatas())
        {
            return null;
        }
        // ----------------------------

        // 获取导入文件数据。
        if (!parseDiskFile())
        {
            return null;
        }
        // ----------------------------

        // 创建卡单信息。
        tTmpMap = null;
        tTmpMap = createCertifyList();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ----------------------------

        // 创建退保卡单信息。
        tTmpMap = null;
        tTmpMap = createWTCertifyList();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ----------------------------

        return tMMap;
    }

    /**
     * 获取磁盘导入文件相关数据。
     * <br />如不存在数据信息，则置 size为0的set集合。
     * @return true：获取数据信息成功；false：获取数据信息失败
     */
    private boolean parseDiskFile()
    {
        String tUIRoot = CommonBL.getUIRoot();
//    	tUIRoot = "E:\\workspace\\lisdev\\ui\\";
        if (tUIRoot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }

        String tConfigPath = new ExeSQL()
                .getOneValue("select SysVarValue from LDSysVar where SysVar = 'CCImpConfXmlPath'");

        CertifyDiskImportBL importFile = new CertifyDiskImportBL(mFilePath + mFileName, tUIRoot + tConfigPath
                + mConfigName, mSheetName);

        if (!importFile.doImport())
        {
            this.mErrors.copyAllErrors(importFile.mErrors);
            return false;
        }

        LICertifySet tLICertifySet = importFile.getLICertifySet();
        if (tLICertifySet == null)
        {
            tLICertifySet = new LICertifySet();
        }
        
        //add by 赵庆涛  2016-06-30
        if(tLICertifySet.size()>0){
        	for(int i=1;i<=tLICertifySet.size();i++){
        		ExeSQL tExeSQL=new ExeSQL();
        		if(tLICertifySet.get(i).getCrs_BussType().equals("01")){
        			String result1=tExeSQL.getOneValue(" select 1 from ldcode1 where codetype='crssalechnl' and code='01' and code1='"+tLICertifySet.get(i).getSaleChnl()+"' ");
        			String result2=tExeSQL.getOneValue(" select 1 from LACom where AgentCom ='"+tLICertifySet.get(i).getAgentCom()+"' and BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and AcType NOT in ('05') ");        			
        			if((!"".equals(result1) && result1!=null)&&(!"".equals(result2) && result2!=null)){
        				System.out.println("交叉销售和中介信息信息正确！"); 
        			}else{
        				System.out.println("选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
        				String tStrErrs="选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！";
        				buildError("parseDiskFile", tStrErrs);
        				if (!mImportLog.errLog(tLICertifySet.get(i).getCardNo(), tStrErrs))
        				{
        					mErrors.copyAllErrors(mImportLog.mErrors);
        					return false;
        				}
        				return false;
        			}
        		}
        	}
        }
        
      //销售渠道为互动中介时，只可选择相互代理；销售渠道为互动直销时，只可选择联合展业、渠道共享、互动部、农网共建。
        if(tLICertifySet.size()>0){
        	for(int i=1;i<=tLICertifySet.size();i++){
        		String Crs_BussType = tLICertifySet.get(i).getCrs_BussType();
        		String salechnl = tLICertifySet.get(i).getSaleChnl();
        		String tStrErrs = null;
        		if("".equals(Crs_BussType) || Crs_BussType == null){
        		}else{
        			if("16".equals(salechnl) || "18".equals(salechnl) || "20".equals(salechnl)){
        				System.out.println("社保渠道不得有交叉销售业务！");
        				 tStrErrs="社保渠道不得有交叉销售业务！";
                        buildError("parseDiskFile", tStrErrs);
        			}else if("14".equals(salechnl)  && !"02".equals(Crs_BussType)  && !"03".equals(Crs_BussType) && !"13".equals(Crs_BussType) && !"14".equals(Crs_BussType)){
        				System.out.println("销售渠道为互动直销时，交叉销售业务类型只能是联合展业、渠道共享、互动部、农网共建！");
        				 tStrErrs="销售渠道为互动直销时，交叉销售业务类型只能是联合展业、渠道共享、互动部、农网共建！";
                        buildError("parseDiskFile", tStrErrs);
        			}else if ("15".equals(salechnl) && !"01" .equals(Crs_BussType)) {
        				System.out.println("销售渠道为互动中介时，交叉销售业务类型只能是相互代理！");
        				 tStrErrs="销售渠道为互动中介时，交叉销售业务类型只能是相互代理！";
                        buildError("parseDiskFile", tStrErrs);
					}else if(!"14".equals(salechnl)  && !"15".equals(salechnl) && !"16".equals(salechnl) && !"18".equals(salechnl) && !"20".equals(salechnl) && !"02".equals(Crs_BussType)){
						//System.out.println("非社保渠道和互动渠道，交叉销售业务类型只能是相互代理、联合展业！");
						 //tStrErrs="非社保渠道和互动渠道，交叉销售业务类型只能是相互代理、联合展业！";
						System.out.println("非互动中介渠道，交叉销售业务类型只可选择联合展业！");
						 tStrErrs="非互动中介渠道，交叉销售业务类型只可选择联合展业！";
                        buildError("parseDiskFile", tStrErrs);
					}
        			if (tStrErrs != null) {
        				if (!mImportLog.errLog(tLICertifySet.get(i).getCardNo(), tStrErrs))
        				{
        					mErrors.copyAllErrors(mImportLog.mErrors);
        					return false;
        				}
        				return false;
					}
        		}
        	}
       	}
    
        //add by zjd 集团统一工号 2014-11-27
        if(tLICertifySet.size()>0){
        	for(int i=1;i<=tLICertifySet.size();i++){
        		ExeSQL tExeSQL=new ExeSQL();
        		String tagentcode=tExeSQL.getOneValue(" select getAgentCode('"+tLICertifySet.get(i).getAgentCode()+"') from dual ");
        		System.out.println("统一工号查询业务员！");
        		if(!"".equals(tagentcode) && tagentcode!=null){
        			tLICertifySet.get(i).setAgentCode(tagentcode);
        		}else{
        			System.out.println("统一工号查询业务员失败！");
        			String tStrErrs="业务员["+tLICertifySet.get(i).getAgentCode()+"]查询失败！";
                    buildError("parseDiskFile", tStrErrs);
                    if (!mImportLog.errLog(tLICertifySet.get(i).getCardNo(), tStrErrs))
                    {
                        mErrors.copyAllErrors(mImportLog.mErrors);
                        return false;
                    }
                    return false;
        		}
        		
        	}
        }
       
        
        LICertifyInsuredSet tLICertifyInsuredSet = importFile.getLICertifyInsuredSet();
        if (tLICertifyInsuredSet == null)
        {
            tLICertifyInsuredSet = new LICertifyInsuredSet();
        }
        // 从清单中分离退保数据
        LICertifySet tCBCertifylist = new LICertifySet();
        if (mWTCertifyInfoList == null)
            mWTCertifyInfoList = new LICertifySet();

        for (int i = 1; i <= tLICertifySet.size(); i++)
        {
            LICertifySchema tTmpCardInfo = tLICertifySet.get(i);
            if ("01".equals(tTmpCardInfo.getCertifyStatus()))
            {
                tCBCertifylist.add(tTmpCardInfo);
            }
            else if ("03".equals(tTmpCardInfo.getCertifyStatus()))
            {
                mWTCertifyInfoList.add(tTmpCardInfo);
            }
            else
            {
                String tStrErr = "[" + tTmpCardInfo.getCardNo() + "]单证状态填写不符合规范，请核实后重新尝试导入。";
                buildError("parseDiskFile", tStrErr);
                if (!mImportLog.errLog(tTmpCardInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        
        //  校验批次中退保单要有对应的保单，承保保单和退保保单保费要相等，并且保费不能为零。
        if(mWTCertifyInfoList.size()!=0)
        {  
           boolean parseFlag = false;
           boolean feeFlag = false;
     	   for(int m=1;m<=mWTCertifyInfoList.size();m++)
     	   {
         	   LICertifySchema mTmpCardInfo =mWTCertifyInfoList.get(m);
         	   if(tCBCertifylist.size()==0)
         	   {
    			   String tStrErr= "[" + mTmpCardInfo.getCardNo() + "]退保保单没有对应的承保保单。";
                   buildError("parseDiskFile", tStrErr);
                   parseFlag = true;
                   if (!mImportLog.errLog(mTmpCardInfo.getCardNo(), tStrErr))
                   {
                       mErrors.copyAllErrors(mImportLog.mErrors);
                       return false;
                   }
                   continue;
         	   }else
         	   {
         		  for(int n=1;n<=tCBCertifylist.size();n++)
         		  {
            		   LICertifySchema nTmpCardInfo =tCBCertifylist.get(n);
            		   if(mTmpCardInfo.getCardNo().equalsIgnoreCase(nTmpCardInfo.getCardNo()))
            		   {
            			   if(mTmpCardInfo.getPrem()!=nTmpCardInfo.getPrem())
            			   {
            				   String tStrErrs = "[" + mTmpCardInfo.getCardNo() + "]承保保单和退保保单保费不相等。";
                               buildError("parseDiskFile", tStrErrs);
                               feeFlag = true;
                               if (!mImportLog.errLog(mTmpCardInfo.getCardNo(), tStrErrs))
                               {
                                   mErrors.copyAllErrors(mImportLog.mErrors);
                                   return false; 
                               } 
                               break;
            			   }
            			   break;
            		   }
            		   if(n==tCBCertifylist.size())
            		   {
            			   String tStrErrs = "[" + mTmpCardInfo.getCardNo() + "]退保保单没有对应的承保保单";
                           buildError("parseDiskFile", tStrErrs);
                           parseFlag = true;
                           if (!mImportLog.errLog(mTmpCardInfo.getCardNo(), tStrErrs))
                           {
                               mErrors.copyAllErrors(mImportLog.mErrors);
                               return false;
                           }
                           break;
            		   }
            	   }
         	   }
            }
     	   if(feeFlag)
     	   {
     	   	return false;
     	   }
           if(mWTCertifyInfoList.size() == tCBCertifylist.size())
           {
           	   String tStrErrs = "该批次对应的保费为零。";
               buildError("parseDiskFile", tStrErrs);
               parseFlag = true;
               if (!mImportLog.errLog("00000000000", tStrErrs))
               {         
                   mErrors.copyAllErrors(mImportLog.mErrors);
                   return false;
               }
           }
         if( parseFlag)
     	   {
     		  return false;
     	   }
        }

       
       

        // --------------------

        mCertifyInfoList = new CertifyInfoColl();
        if (!mCertifyInfoList.loadCertifyInfo(tCBCertifylist, tLICertifyInsuredSet))
        {
            String tStrErr = "获取单证导入数据失败。";
            buildError("parseDiskFile", tStrErr);
            if (!mImportLog.errLog(mBatchNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        if (mCertifyInfoList.size() == 0)
        {
            String tStrErr = "清单数据信息为空。请核实后重新导入。";
            buildError("parseDiskFile", tStrErr);
            if (!mImportLog.errLog(mBatchNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        return true;
    }

    /**
     * 创建单证清单。
     * @return
     */
    private MMap createCertifyList()
    {
        MMap tTmpMap = new MMap();
        boolean tAllSuccFlag = true;
        String tStrSql = null;

        for (int i = 0; i < mCertifyInfoList.size(); i++)
        {
            LICertifySchema tLICertifySchema = null;
            tLICertifySchema = mCertifyInfoList.getCertifyInfoByIndex(i);

            LICertifyInsuredSet tLICertifyInsuredSet = null;
            tLICertifyInsuredSet = mCertifyInfoList.getCertifyInsuByIndex(i);

            if (tLICertifySchema == null)
            {
                continue;
            }

            // 处理POS数据要素
            if (!dealPOSWrapParams(tLICertifySchema, tLICertifyInsuredSet))
            {
                tAllSuccFlag = false;
                continue;
            }
            // --------------------

            // 处理要素信息。
            if (!dealCertfiyContParams(tLICertifySchema))
            {
                tAllSuccFlag = false;
                continue;
            }
            // ---------------------------

            // 单证信息完整性校验。
            if (!chkCertify(tLICertifySchema.getCardNo(), i))
            {
                tAllSuccFlag = false;
                continue;
            }
            // ---------------------------

            // 补全数据信息。

            tLICertifySchema.setBatchNo(mBatchNo);
            tLICertifySchema.setDiskSeqNo(String.valueOf(i));

            // 处理被保人人数
            int tInsuPeoples = 0;
            if (tLICertifyInsuredSet != null)
            {
                tInsuPeoples = tLICertifyInsuredSet.size();
            }
            tLICertifySchema.setInsuPeoples(tInsuPeoples);
            // --------------------

            //            if (tLICertifySchema.getCopys() == 0)
            //            {
            //                tLICertifySchema.setCopys(1);
            //            }

            tLICertifySchema.setState(CertifyContConst.CC_IMPORT_FIRST);

            tLICertifySchema.setWSState(CertifyContConst.CC_WSSTATE_UNCREATED);

            // 如果单证类型为卡单，激活状态置为：00－未激活；其余均置为：01－已激活，激活日期为导入当天。
            String tCertifyType = null;

            tStrSql = null;
            tStrSql = " select OperateType from LMCertifyDes where CertifyCode = '" + tLICertifySchema.getCertifyCode()
                    + "' and CertifyClass = 'D' and OperateType in ('0', '1', '2', '3') ";
            tCertifyType = new ExeSQL().getOneValue(tStrSql);

            if (tCertifyType.equals(""))
            {
                String tStrErr = "单证类型[" + tLICertifySchema.getCertifyCode() + "]为非可用单证类型。";
                if (!mImportLog.errLog(tLICertifySchema.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return null;
                }
                continue;
            }
            tStrSql = null;

            if ("0".equals(tCertifyType))
            {
                tLICertifySchema.setActiveFlag(CertifyContConst.CC_ACTIVEFLAG_INACTIVE);
            }
            else
            {
                tLICertifySchema.setActiveFlag(CertifyContConst.CC_ACTIVEFLAG_ACTIVE);
                tLICertifySchema.setActiveDate(PubFun.getCurrentDate());
            }
            // ----------------------------

            tLICertifySchema.setOperator(mGlobalInput.Operator);
            tLICertifySchema.setMakeDate(PubFun.getCurrentDate());
            tLICertifySchema.setMakeTime(PubFun.getCurrentTime());
            tLICertifySchema.setModifyDate(PubFun.getCurrentDate());
            tLICertifySchema.setModifyTime(PubFun.getCurrentTime());

            if (tLICertifyInsuredSet != null)
            {
                for (int idx = 1; idx <= tLICertifyInsuredSet.size(); idx++)
                {
                    LICertifyInsuredSchema tTmpCertifyInsu = null;
                    tTmpCertifyInsu = tLICertifyInsuredSet.get(idx);

                    tTmpCertifyInsu.setBatchNo(mBatchNo);
                    tTmpCertifyInsu.setSequenceNo(String.valueOf(idx));

                    tTmpCertifyInsu.setState(CertifyContConst.CI_IMPORT_FIRST);

                    tTmpCertifyInsu.setOperator(mGlobalInput.Operator);
                    tTmpCertifyInsu.setMakeDate(PubFun.getCurrentDate());
                    tTmpCertifyInsu.setMakeTime(PubFun.getCurrentTime());
                    tTmpCertifyInsu.setModifyDate(PubFun.getCurrentDate());
                    tTmpCertifyInsu.setModifyTime(PubFun.getCurrentTime());
                }
            }
            // ---------------------------

            tTmpMap.put(tLICertifySchema, SysConst.INSERT);
            tTmpMap.put(tLICertifyInsuredSet, SysConst.INSERT);

            // 回置单证状态为：预售。（临时处理方式）
            tStrSql = null;
            tStrSql = " select lzc.* from LZCard lzc "
                    + " inner join LMCertifyDes lmcd on lmcd.CertifyCode = lzc.CertifyCode "
                    + " inner join LZCardNumber lzcn on lzc.SubCode = lzcn.CardType and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                    + " where lmcd.CertifyClass = 'D' " + " and lmcd.OperateType in ('0', '1', '2', '3') "
                    + " and lzc.State in ('10', '11', '14') " + " and lzcn.CardNo = '" + tLICertifySchema.getCardNo()
                    + "' ";
            LZCardSet tLZCardSet = new LZCardDB().executeQuery(tStrSql);

            if (tLZCardSet == null || tLZCardSet.size() != 1)
            {
                tAllSuccFlag = false;
                String tStrErr = "回置单证预售状态失败。";
                if (!mImportLog.errLog(tLICertifySchema.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return null;
                }
                continue;
            }
            tStrSql = null;

            LZCardSchema tLZCardSchema = tLZCardSet.get(1);

            // 如果是已售出保持原状态。
            if (!"14".equals(tLZCardSchema.getState()))
            {
                tLZCardSchema.setState("13");
            }
            // --------------------

            tLZCardSchema.setOperator(mGlobalInput.Operator);
            tLZCardSchema.setModifyDate(PubFun.getCurrentDate());
            tLZCardSchema.setModifyTime(PubFun.getCurrentTime());

            tTmpMap.put(tLZCardSchema, SysConst.UPDATE);
            // ---------------------------

            if (!mImportLog.infoLog(tLICertifySchema.getCardNo(), "数据正常。"))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return null;
            }
        }

        if (tAllSuccFlag && tTmpMap != null)
        {
            // 终止导入日志。
            MMap tTmpLogEnd = null;
            tTmpLogEnd = mImportLog.logEnd4Map();
            if (tTmpLogEnd == null)
            {
                String tStrErr = "终止导入日志动作失败。";
                buildError("createCertifyList", tStrErr);
                System.out.println(tStrErr);
                return null;
            }
            tTmpMap.add(tTmpLogEnd);
            tTmpLogEnd = null;
            // ---------------------------
        }

        return (tAllSuccFlag ? tTmpMap : null);
    }

    /**
     * 处理退保卡单
     * @return
     */
    private MMap createWTCertifyList()
    {
        MMap tTmpMap = new MMap();
        boolean tAllSuccFlag = true;

        for (int i = 1; i <= mWTCertifyInfoList.size(); i++)
        {
            LICertifySchema tTmpCardInfo = null;
            tTmpCardInfo = mWTCertifyInfoList.get(i);

            if (tTmpCardInfo == null)
            {
                continue;
            }

            // 处理POS数据要素
            if (!dealPOSWrapParams(tTmpCardInfo, null))
            {
                tAllSuccFlag = false;
                continue;
            }
            // --------------------

            // 处理要素信息。
            if (!dealCertfiyContParams(tTmpCardInfo))
            {
                tAllSuccFlag = false;
                continue;
            }
            // ---------------------------

            // 单证信息完整性校验。暂时未加
            //            if (!chkCertify(tTmpCardInfo.getCardNo(), i))
            //            {
            //                tAllSuccFlag = false;
            //                continue;
            //            }
            // ---------------------------

            // 补全数据信息。
            tTmpCardInfo.setBatchNo(mBatchNo);
            tTmpCardInfo.setDiskSeqNo(String.valueOf(i));

            tTmpCardInfo.setInsuPeoples(1);
            tTmpCardInfo.setState(CertifyContConst.CC_IMPORT_FIRST);

            // ----------------------------

            tTmpCardInfo.setOperator(mGlobalInput.Operator);
            tTmpCardInfo.setMakeDate(PubFun.getCurrentDate());
            tTmpCardInfo.setMakeTime(PubFun.getCurrentTime());
            tTmpCardInfo.setModifyDate(PubFun.getCurrentDate());
            tTmpCardInfo.setModifyTime(PubFun.getCurrentTime());

            // ---------------------------

            // 将退保数据信息存入B表
            Reflections tRef = new Reflections();
            LIBCertifySchema tTmpWTCardInfo = new LIBCertifySchema();
            tRef.transFields(tTmpWTCardInfo, tTmpCardInfo);
            tTmpMap.put(tTmpWTCardInfo, SysConst.INSERT);
            // --------------------

            // ---------------------------

            if (!mImportLog.infoLog(tTmpCardInfo.getCardNo(), "数据正常。"))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return null;
            }
        }

        if (tAllSuccFlag && tTmpMap != null)
        {
            // 终止导入日志。
            MMap tTmpLogEnd = null;
            tTmpLogEnd = mImportLog.logEnd4Map();
            if (tTmpLogEnd == null)
            {
                String tStrErr = "终止导入日志动作失败。";
                buildError("createCertifyList", tStrErr);
                System.out.println(tStrErr);
                return null;
            }
            tTmpMap.add(tTmpLogEnd);
            tTmpLogEnd = null;
            // ---------------------------
        }

        return (tAllSuccFlag ? tTmpMap : null);
    }

    /**
     * 批次导入确认。
     * @return
     */
    private MMap confirmImportList()
    {
        MMap tMMap = new MMap();

        MMap tTmpMap = null;

        // 导入确认。
        tTmpMap = null;
        tTmpMap = confirmImportByBatchNo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ----------------------------

        return tMMap;
    }

    /**
     * 确认批次下全部单证。
     * @return
     */
    private MMap confirmImportByBatchNo()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 置清单确认状态。
        String tStrSql = " update LICertify set State = '" + CertifyContConst.CC_IMPORT_CONFIRM + "' where BatchNo = '"
                + mBatchNo + "' and State = '" + CertifyContConst.CC_IMPORT_FIRST + "' ";
        tMMap.put(tStrSql, SysConst.UPDATE);
        // ---------------------------

        // 置退保清单确认状态。
        tStrSql = " update LIBCertify set State = '" + CertifyContConst.CC_IMPORT_CONFIRM + "' where BatchNo = '"
                + mBatchNo + "' and State = '" + CertifyContConst.CC_IMPORT_FIRST + "' ";
        tMMap.put(tStrSql, SysConst.UPDATE);
        // ---------------------------

        // 处理批次导入工作流。
        tTmpMap = null;
        tTmpMap = dealImportMission();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ---------------------------

        return tMMap;
    }

    /**
     * 处理批次导入工作流节点
     * @return
     */
    private MMap dealImportMission()
    {
        MMap tMMap = new MMap();

        String tStrSql = "select * from LWMission " + " where ProcessId = '0000000011' and ActivityId = '0000011102' "
                + " and MissionProp1 = '" + mBatchNo + "'";

        LWMissionSet tLWMissionSet = new LWMissionDB().executeQuery(tStrSql);

        LBMissionSet tLBMissionSet = new LBMissionSet();
        Reflections tReflections = new Reflections();

        for (int i = 1; i <= tLWMissionSet.size(); i++)
        {
            String tSerielNo = null;
            try
            {
                tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
            }
            catch (Exception e)
            {
                String tStrErr = "处理批次导入工作流节点失败。";
                buildError("dealImportMission", tStrErr);
                return null;
            }

            LBMissionSchema tLBMissionSchema = new LBMissionSchema();
            tReflections.transFields(tLBMissionSchema, tLWMissionSet.get(i));
            tLBMissionSchema.setSerialNo(tSerielNo);

            tLBMissionSet.add(tLBMissionSchema);
        }

        tMMap.put(tLBMissionSet, SysConst.INSERT);
        tMMap.put(tLWMissionSet, SysConst.DELETE);

        return tMMap;
    }

    /**
     * 批次导入确认。
     * @return
     */
    private MMap deleteImportList()
    {
        MMap tMMap = new MMap();

        MMap tTmpMap = null;

        // 导入确认。
        tTmpMap = null;
        tTmpMap = deleteImportByBatchNo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ----------------------------

        return tMMap;
    }

    /**
     * 删除批次下全部单证。
     * @return
     */
    private MMap deleteImportByBatchNo()
    {
        MMap tMMap = new MMap();
        String tStrSql = null;

        // 判断是否已存在非导入时被激活卡单，如果存在，不能删除。
        tStrSql = " select count(1) from LICertify " + " where ActiveFlag not in ('"
                + CertifyContConst.CC_ACTIVEFLAG_INACTIVE + "', '" + CertifyContConst.CC_ACTIVEFLAG_ACTIVE + "') "
                + " and BatchNo = '" + mBatchNo + "' " + " with ur ";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if (tResult.equals("") || !tResult.equals("0"))
        {
            String tStrErr = "该批次下存在已被激活的卡折，不能进行删除。";
            buildError("deleteImportByBatchNo", tStrErr);
            System.out.println(tStrErr);
            return null;
        }
        // ------------------------------

        // 删除导入批次校验报告。
        tStrSql = "delete from LICChkError where BatchNo = '" + mBatchNo + "'";
        tMMap.put(tStrSql, SysConst.DELETE);
        tStrSql = null;
        // ------------------------------

        // 删除卡折业务卡折清单表。
        tStrSql = " delete from LICertify where BatchNo = '" + mBatchNo + "' ";
        tMMap.put(tStrSql, SysConst.DELETE);
        tStrSql = null;
        // ------------------------------

        // 删除卡折业务被保人清单表。
        tStrSql = " delete from LICertifyInsured where BatchNo = '" + mBatchNo + "' ";
        tMMap.put(tStrSql, SysConst.DELETE);
        tStrSql = null;
        // ------------------------------

        // 删除退保卡折信息。
        tStrSql = " delete from LIBCertify where BatchNo = '" + mBatchNo + "' ";
        tMMap.put(tStrSql, SysConst.DELETE);
        tStrSql = null;
        // --------------------

        // 回置单证状态为：预售。（临时处理方式）
        tStrSql = " select lzc.* from LZCard lzc "
                + " inner join LMCertifyDes lmcd on lmcd.CertifyCode = lzc.CertifyCode "
                + " inner join LZCardNumber lzcn on lzc.SubCode = lzcn.CardType and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " inner join LICertify lict on lict.CardNo = lzcn.CardNo " + " where lmcd.CertifyClass = 'D' "
                + " and lmcd.OperateType in ('0', '1', '2', '3') " + " and lzc.State in ('13', '14') "
                + " and lict.BatchNo = '" + mBatchNo + "'";
        LZCardSet tLZCardSet = new LZCardDB().executeQuery(tStrSql);

        if (tLZCardSet == null || tLZCardSet.size() == 0)
        {
            buildError("deleteImportByBatchNo", "回置单证状态失败。");
            return null;
        }

        System.out.println("tLZCardSet.size():" + tLZCardSet.size());

        for (int i = 1; i <= tLZCardSet.size(); i++)
        {
            LZCardSchema tLZCardSchema = tLZCardSet.get(i);

            String tReceiveCom = tLZCardSchema.getReceiveCom();
            String tCardSendFlag = tReceiveCom.substring(0, 1);

            if ("D".equals(tCardSendFlag))
            {
                tLZCardSchema.setState("10");
            }
            else if ("E".equals(tCardSendFlag))
            {
                tLZCardSchema.setState("11");
            }
            else
            {
                buildError("deleteImportByBatchNo", "单证接收机构有误。");
                return null;
            }

            tLZCardSchema.setOperator(mGlobalInput.Operator);
            tLZCardSchema.setModifyDate(PubFun.getCurrentDate());
            tLZCardSchema.setModifyTime(PubFun.getCurrentTime());
        }

        tMMap.put(tLZCardSet, SysConst.UPDATE);
        // ---------------------------

        MMap tTmpMap = null;
        tTmpMap = mImportLog.cleanAllLogOfBatchNo4Map();
        if (tTmpMap == null)
        {
            mErrors.copyAllErrors(mImportLog.mErrors);
            String tStrErr = "清除日志动作失败。";
            buildError("deleteImportByBatchNo", tStrErr);
            System.out.println(tStrErr);
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ---------------------------

        return tMMap;
    }

    /**
     * 校验单证信息。（应提出校验类，暂时先写成函数。）
     * @param LICertifySchema
     * @param cChkType
     * @return
     */
    private boolean chkCertify(String cCardNo, int m)
    {
        String tStrSql = null;
        String tChkResult = null;

        LICertifySchema tCertifyInfo = null;
        tCertifyInfo = mCertifyInfoList.getCertifyInfoByCardNo(cCardNo);

        LICertifyInsuredSet tCertifyInsuInfo = null;
        tCertifyInsuInfo = mCertifyInfoList.getCertifyInsuByCardNo(cCardNo);

        // 校验单证是否可用。
        if (tCertifyInfo.getCertifyCode() == null || tCertifyInfo.getCertifyCode().equals(""))
        {
            String tStrErr = "单证号不能为空。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        tStrSql = null;
        tChkResult = null;

        tStrSql = " select 1 from LZCard lzc "
                + " inner join LMCertifyDes lmcd on lmcd.CertifyCode = lzc.CertifyCode "
                + " inner join LZCardNumber lzcn on lzc.SubCode = lzcn.CardType and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where lmcd.CertifyClass = 'D' " + " and lmcd.OperateType in ('0', '1', '2', '3') "
                + " and lzc.State in ('10', '11', '14') " + " and lzcn.CardNo = '" + cCardNo + "' " + " with ur ";

        tChkResult = new ExeSQL().getOneValue(tStrSql);
        if (!tChkResult.equals("1"))
        {
            String tStrErr = "单证号[" + cCardNo + "]已领用或尚未下发到业务员（或中介机构）。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        tStrSql = null;
        tChkResult = null;
        // -------------------------

        // 检查基本数据信息完整性。
        if (tCertifyInfo.getManageCom() == null || tCertifyInfo.getManageCom().equals(""))
        {
            String tStrErr = "管理机构不能为空。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        //管理机构长度必须是8位
        if (tCertifyInfo.getManageCom().length() != 8)
        {
            String tStrErr = "管理机构必须为8位机构";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        //管理机构必须与单证号相对应
        String tSQL = "select managecom from LAAgent where AgentCode = '" + tCertifyInfo.getAgentCode() + "'";
        String tManageCom = new ExeSQL().getOneValue(tSQL);
        if (!"".equals(tManageCom) && !tManageCom.equals(tCertifyInfo.getManageCom()))
        {
            String tStrErr = "管理机构录入错误，卡号为" + cCardNo + "的管理机构应该为" + tManageCom + ",请去模板中修改管理机构";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        if (tCertifyInfo.getSaleChnl() == null || tCertifyInfo.getSaleChnl().equals(""))
        {
            String tStrErr = "销售渠道不能为空。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        // ------------------------

        // 校验销售渠道。
        String tSaleChnl = tCertifyInfo.getSaleChnl();
        if ("02".equals(tSaleChnl) || "14".equals(tSaleChnl) || "01".equals(tSaleChnl) || "13".equals(tSaleChnl) || "17".equals(tSaleChnl)||"18".equals(tSaleChnl)||"19".equals(tSaleChnl))
        {
            if (tCertifyInfo.getAgentCom() != null && !tCertifyInfo.getAgentCom().equals(""))
            {
                String tStrErr = "单证销售渠道填写为[" + tSaleChnl + "]，中介机构必须为空。";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }

            if (tCertifyInfo.getAgentCode() == null || tCertifyInfo.getAgentCode().equals(""))
            {
                String tStrErr = "单证销售渠道填写为[" + tSaleChnl + "]，业务员代码不能为空。";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        else if ("03".equals(tSaleChnl) || "10".equals(tSaleChnl) || "11".equals(tSaleChnl) || "12".equals(tSaleChnl) || "15".equals(tSaleChnl) || "04".equals(tSaleChnl) || "20".equals(tSaleChnl))
        {
            if (tCertifyInfo.getAgentCom() == null || tCertifyInfo.getAgentCom().equals(""))
            {
                String tStrErr = "单证销售渠道填写为[" + tSaleChnl + "]，中介机构不能为空。";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
            String aAgentFlag=new ExeSQL().getOneValue("select 1 from lacomtoagent where agentcode = '"+tCertifyInfo.getAgentCode()+"' and agentcom = '"+tCertifyInfo.getAgentCom()+"'");
            if("".equals(aAgentFlag)) {
            	String tStrErr = "单证销售渠道填写为[" + tSaleChnl + "]，业务员与中介机构不匹配。";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        else
        {
            String tStrErr = "单证销售渠道只能为[01－个险直销；02－团险直销；03－团险中介；04－银行代理；10-个险中介；13－银代直销；14-互动直销；15-互动中介；17-健管直销；18-社保综拓直销；19-个险续收；20-社保综拓中介]之一。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        // ------------------------

        // 确定定额单证类型
        String tCertifyType = null;

        tStrSql = null;
        tStrSql = " select OperateType from LMCertifyDes where CertifyCode = '" + tCertifyInfo.getCertifyCode()
                + "' and CertifyClass = 'D' and OperateType in ('0', '1', '2', '3') ";
        tCertifyType = new ExeSQL().getOneValue(tStrSql);

        if (tCertifyType.equals(""))
        {
            String tStrErr = "单证类型[" + tCertifyInfo.getCertifyCode() + "]为非可用单证类型。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        tStrSql = null;
        // -------------------------

        // 撕单时校验被保人信息。
        if (tCertifyType.equals("1") || tCertifyType.equals("3"))
        {

            if (!dealPOSWrapParams(tCertifyInfo, null))
            {
                String tStrErr = "pos套餐信息处理失败";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
            System.out.println(mCertifyInfoList.getCertifyInsuByIndex(m).size());
            if ("".equals(mCertifyInfoList.getCertifyInsuByIndex(m).get(1).getTicketNo()))
            {
                if (tCertifyInfo.getCValidate() == null || tCertifyInfo.getCValidate().equals(""))
                {
                    String tStrErr = "撕单必须填写生效日期。";
                    if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                    {
                        mErrors.copyAllErrors(mImportLog.mErrors);
                        return false;
                    }
                    return false;
                }

                if (!tCertifyType.equals("3"))
                {
                    if (tCertifyInsuInfo.size() == 0)
                    {
                        String tStrErr = "撕单必须填写被保人。";
                        if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                    }

                    for (int i = 1; i <= tCertifyInsuInfo.size(); i++)
                    {
                        LICertifyInsuredSchema tInsuInfo = null;
                        tInsuInfo = tCertifyInsuInfo.get(i);

                        if (tInsuInfo.getName() == null || tInsuInfo.getName().equals("") || tInsuInfo.getSex() == null
                                || tInsuInfo.getSex().equals("") || tInsuInfo.getBirthday() == null
                                || tInsuInfo.getBirthday().equals("") || tInsuInfo.getIdType() == null
                                || tInsuInfo.getIdType().equals("") || tInsuInfo.getIdNo() == null
                                || tInsuInfo.getIdNo().equals(""))
                        {
                            String tStrErr = "被保人姓名、性别、出生日期、证件类型、证件号为必填项。";
                            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                            {
                                mErrors.copyAllErrors(mImportLog.mErrors);
                                return false;
                            }
                            return false;
                        }

                        if (!CheckOccupationType(tInsuInfo.getOccupationType()))
                        {
                            String tStrErr = "被保人有职业类别与系统描述不一致，请查看！系统规定职业类别为半角数字的1至6!";
                            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                            {
                                mErrors.copyAllErrors(mImportLog.mErrors);
                                return false;
                            }
                            return false;
                        }
                    }
                }
            }
        }
        //【如果是交叉销售，则交叉销售渠道、交叉销售业务类型、对方机构代码、对方业务员代码、对方业务员姓名、对方业务员身份证不能为空】
        if (!"".equals(StrTool.cTrim(tCertifyInfo.getCrs_SaleChnl()))
                || !"".equals(StrTool.cTrim(tCertifyInfo.getCrs_BussType()))
                || !"".equals(StrTool.cTrim(tCertifyInfo.getGrpAgentCom()))
                || !"".equals(StrTool.cTrim(tCertifyInfo.getGrpAgentCode()))
                || !"".equals(StrTool.cTrim(tCertifyInfo.getGrpAgentName()))
                || !"".equals(StrTool.cTrim(tCertifyInfo.getGrpAgentIDNo())))
        {
            if ("".equals(StrTool.cTrim(tCertifyInfo.getCrs_SaleChnl()))
                    || "".equals(StrTool.cTrim(tCertifyInfo.getCrs_BussType()))
                    || "".equals(StrTool.cTrim(tCertifyInfo.getGrpAgentCom()))
                    || "".equals(StrTool.cTrim(tCertifyInfo.getGrpAgentCode()))
                    || "".equals(StrTool.cTrim(tCertifyInfo.getGrpAgentName()))
                    || "".equals(StrTool.cTrim(tCertifyInfo.getGrpAgentIDNo())))
            {
                String tStrErr = "如果是交叉销售，则交叉销售渠道、交叉销售业务类型、对方机构代码、对方业务员代码、对方业务员姓名、对方业务员身份证不能为空";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
            if (!tCertifyInfo.getCrs_SaleChnl().equals("01") && !tCertifyInfo.getCrs_SaleChnl().equals("02"))
            {
                String tStrErr = "交叉销售渠道必须为01或者02,请修改模板之后重新导入";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }

        // 卡单校验。
        if (tCertifyType.equals("0"))
        {
            if (tCertifyInfo.getCopys() > 1 || tCertifyInfo.getMult() > 0)
            {
                String tStrErr = "该单证类型为卡单，不支持份数、档次等要素。";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }

            if (tCertifyInsuInfo.size() > 0)
            {
                String tStrErr = "该单证类型为卡单，清单导入时不能存在被保人。";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        // -------------------------

        return true;
    }

    /**
     * 准备导入前所需相关数据。
     * @return
     */
    private boolean prepareBeforeImportListDatas()
    {
        mFileName = (String) mTransferData.getValueByName("FileName");
        if (mFileName == null || mFileName.equals(""))
        {
            buildError("getInputData", "参数[导入文件名（FileName）]不存在。");
            return false;
        }

        mFilePath = (String) mTransferData.getValueByName("FilePath");
        if (mFilePath == null || mFilePath.equals(""))
        {
            buildError("getInputData", "参数[导入文件路径（FilePath）]不存在。");
            return false;
        }

        return true;
    }

    /**
     * 处理导入单证相关要素信息。
     * @param cLICertifySchema 要处理的单证对象。
     * @return
     */
    private boolean dealCertfiyContParams(LICertifySchema cLICertifySchema)
    {
        // 处理单证类型。
        if (!dealCertifyCode(cLICertifySchema))
        {
            return false;
        }
        // -----------------------------

        // 处理业务员相关要素。
        if (!dealAgentInfo(cLICertifySchema))
        {
            return false;
        }
        // -----------------------------

        // 处理单证关键要素（保额、保费、档次、份数等）。
        if (!dealCertifyWrapParams(cLICertifySchema))
        {
            return false;
        }
        // -----------------------------

        return true;
    }

    /**
     * 处理单证类型。
     * <br />若清单中已填写单证类别，则根据单证号与系统中单证类型进行比较，如不一致返回false，并记录报错日志。
     * <br />若清单中未填写，则获取系统中所对应的单证类别。
     * @param cLICertifySchema
     * @return
     */
    private boolean dealCertifyCode(LICertifySchema cLICertifySchema)
    {
        String tCardNo = cLICertifySchema.getCardNo();

        String tCertifyCode = cLICertifySchema.getCertifyCode();

        String tStrSql = " select lzc.CertifyCode "
                + " from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where lzcn.CardNo = '" + tCardNo + "' ";
        String tSysCertifyCode = new ExeSQL().getOneValue(tStrSql);

        if (tSysCertifyCode.equals(""))
        {
            String tStrErr = "单证号[" + tCardNo + "]在系统中未找到对应单证类别。";
            buildError("dealCertfiyContParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        if (tCertifyCode != null && !tCertifyCode.equals(""))
        {
            if (!tSysCertifyCode.equals(tCertifyCode))
            {
                String tStrErr = "单证号[" + tCardNo + "]在系统中对应单证类别为[" + tSysCertifyCode + "]而清单中填写的单证类型为[" + tCertifyCode
                        + "]，请进行核实。";
                buildError("dealCertfiyContParams", tStrErr);
                System.out.println(tStrErr);

                if (!mImportLog.errLog(tCardNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        
//        by gzh 对停售套餐进行处理
        String tSaleChnl = cLICertifySchema.getSaleChnl();
        String tSQL = "select code from ldcode1 where codetype = 'stopwrap' and code in (select RiskCode From LMCardRisk where certifycode = '"+tSysCertifyCode+"' ) and code1 = '"+tSaleChnl+"'  ";
        SSRS tSSRS = new ExeSQL().execSQL(tSQL);
        if (tSSRS != null && tSSRS.getMaxRow() >0)
        {
            String tStrErr = "单证号[" + tCardNo + "]在系统中对应的套餐编码为[" + tSSRS.GetText(1, 1) + "]，该套餐已停售。";
            buildError("dealCertfiyContParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        cLICertifySchema.setCertifyCode(tSysCertifyCode);

        return true;
    }

    /**
     * 处理业务员/中介机构相关信息。
     * @param cLICertifySchema
     * @return
     */
    private boolean dealAgentInfo(LICertifySchema cLICertifySchema)
    {
        String tCardNo = cLICertifySchema.getCardNo();

        String tAgentCode = cLICertifySchema.getAgentCode();
        String tAgentCom = cLICertifySchema.getAgentCom();

        String tStrSql = " select (case lzc.State when '10' then 'D' when '11' then 'E' when '14' then SubStr(lzc.ReceiveCom, 1, 1) end) ReceiveState, "
                + " SubStr(lzc.ReceiveCom, 1, 1) ReceiveFlag, "
                + " SubStr(lzc.ReceiveCom, 2, length(lzc.ReceiveCom)) ReceiveFlag "
                + " from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where lzcn.CardNo = '" + tCardNo + "' ";
        SSRS tSSRS = new ExeSQL().execSQL(tStrSql);

        if (tSSRS == null || tSSRS.getMaxRow() <= 0)
        {
            String tStrErr = "单证号[" + tCardNo + "]在系统中未找到对应下发的业务员或中介机构。";
            buildError("dealAgentInfo", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        if (tSSRS.getMaxRow() > 1)
        {
            String tStrErr = "单证号[" + tCardNo + "]在系统中所对应下发的业务员或中介机构存在多条信息。";
            buildError("dealAgentInfo", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        String[] tAgentInfo = tSSRS.getRowData(1);

        String tSysAgentFlag = tAgentInfo[0];
        String tSysReceiveCom = tAgentInfo[2];

        //        String tChkSysAgentFlag = tAgentInfo[1];
        //
        //        if (!tSysAgentFlag.equals(tChkSysAgentFlag))
        //        {
        //            String tStrErr = "单证号[" + tCardNo + "]在系统中所对应下发到业务员或中介机构的状态标志有误。";
        //            buildError("dealAgentInfo", tStrErr);
        //            System.out.println(tStrErr);
        //
        //            if (!mImportLog.errLog(tCardNo, tStrErr))
        //            {
        //                mErrors.copyAllErrors(mImportLog.mErrors);
        //                return false;
        //            }
        //            return false;
        //        }

        if ("D".equals(tSysAgentFlag))
        {
            if (tAgentCode != null && !tAgentCode.equals(""))
            {
                if (!tAgentCode.equals(tSysReceiveCom))
                {
                    String tStrErr = "单证号[" + tCardNo + "]在系统中对应业务员代码为[" + tSysReceiveCom + "]而清单中填写的业务员代码为["
                            + tAgentCode + "]，请进行核实。";
                    buildError("dealAgentInfo", tStrErr);
                    System.out.println(tStrErr);

                    if (!mImportLog.errLog(tCardNo, tStrErr))
                    {
                        mErrors.copyAllErrors(mImportLog.mErrors);
                        return false;
                    }
                    return false;
                }
            }

            cLICertifySchema.setAgentCode(tSysReceiveCom);
        }
        else if ("E".equals(tSysAgentFlag))
        {
            if (tAgentCom != null && !tAgentCom.equals(""))
            {
                if (!tAgentCom.equals(tSysReceiveCom))
                {
                    String tStrErr = "单证号[" + tCardNo + "]在系统中对应中介机构为[" + tSysReceiveCom + "]而清单中填写的中介机构为["
                            + tAgentCom + "]，请进行核实。";
                    buildError("dealAgentInfo", tStrErr);
                    System.out.println(tStrErr);

                    if (!mImportLog.errLog(tCardNo, tStrErr))
                    {
                        mErrors.copyAllErrors(mImportLog.mErrors);
                        return false;
                    }
                    return false;
                }
            }

            cLICertifySchema.setAgentCom(tSysReceiveCom);

            // 中介机构所对应的业务员。
            //            String tTmpComToAgentCode = new ExeSQL()
            //                    .getOneValue("select AgentCode from LAComToAgent where AgentCom = '"
            //                            + tSysReceiveCom + "'");
            //
            //            if (tAgentCode != null && !tAgentCode.equals(""))
            //            {
            //                if (!tAgentCode.equals(tTmpComToAgentCode))
            //                {
            //                    String tStrErr = "单证号[" + tCardNo + "]在系统中对应中介机构["
            //                            + tSysReceiveCom + "]的业务员代码为[" + tTmpComToAgentCode
            //                            + "]而清单中填写的业务员代码为[" + tAgentCode + "]，请进行核实。";
            //                    buildError("dealAgentInfo", tStrErr);
            //                    System.out.println(tStrErr);
            //
            //                    if (!mImportLog.errLog(tCardNo, tStrErr))
            //                    {
            //                        mErrors.copyAllErrors(mImportLog.mErrors);
            //                        return false;
            //                    }
            //                    return false;
            //                }
            //            }
            //
            //            cLICertifySchema.setAgentCode(tTmpComToAgentCode);
            // --------------------------------------
        }

        return true;
    }

    /**
     * 处理单证关键要素（保额、保费、档次、份数等）。
     * @param cLICertifySchema
     * @return
     */
    private boolean dealCertifyWrapParams(LICertifySchema cLICertifySchema)
    {
        String tStrSql = null;

        String tCardNo = cLICertifySchema.getCardNo();

        String tCertifyCode = cLICertifySchema.getCertifyCode();
        double tAmnt = cLICertifySchema.getAmnt();
        double tPrem = cLICertifySchema.getPrem();
        double tCopys = cLICertifySchema.getCopys();
        double tMult = cLICertifySchema.getMult();
        String tOccupationType=new ExeSQL().getOneValue("select OccupationType from LICardActiveInfoList where Cardno='"+tCardNo+"' ");
        String tCardType = null;
        String tInsuYear = cLICertifySchema.getInsuYear();
        // 获取单证类型
        tStrSql = "select OperateType from LMCertifyDes where CertifyCode = '" + tCertifyCode + "'";
        tCardType = new ExeSQL().getOneValue(tStrSql);
        if (tCardType == null || tCardType.equals(""))
        {
            String tStrErr = "单证号[" + tCardNo + "]对应单证中，单证业务类别为空。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        tStrSql = null;
        // --------------------

        // 处理Copys要素。如果套餐中无Copys要素，清单中份数要素必须为1。
        if (cLICertifySchema.getCopys() == 0)
        {
            tCopys = 1;
            cLICertifySchema.setCopys(tCopys);
        }

        String tCopysFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Copys' and lmcr.CertifyCode = '" + tCertifyCode + "' ");
        if (!"1".equals(tCopysFlag) && tCopys != 1)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐份数不能大于1。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        // -------------------------------------------

        // 校验档次，但不对POS单证进行档次的对应校验
        String tMultFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + tCertifyCode + "' " + "union all "
                + " select 1 from LMCertifyDes lmcd where lmcd.CertifyCode = '" + tCertifyCode
                + "' and lmcd.OperateType = '3'");
        if (!"1".equals(tMultFlag) && tMult != 0)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐不含有档次要素。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        // -------------------------------------------

        LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

        tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
                + " where lmcr.CertifyCode = '"
                + tCertifyCode
                + "' "
                + " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
        tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐要素信息未找到。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        tStrSql = null;

        double tSysWrapSumPrem = 0;
        double tSysWrapSumAmnt = 0;

        // 处理要素信息。
        for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++)
        {
            LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);

            String tCalFactorType = tRiskDutyParam.getCalFactorType();
            String tCalFactor = tRiskDutyParam.getCalFactor();
            String tCalFactorValue = tRiskDutyParam.getCalFactorValue();

            if ("Prem".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(tMult));
                    tCal.addBasicFactor("OccupationType", String.valueOf(tOccupationType));
                    tCal.addBasicFactor("Amnt", String.valueOf(tAmnt));
                    tCal.addBasicFactor("InsuYear", tInsuYear);
                    tCal.addBasicFactor("Prem", String.valueOf(tPrem));
                    
                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + tCardNo + "]保费计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        System.out.println(tStrErr);

                        if (!mImportLog.errLog(tCardNo, tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumPrem = Arith.add(tSysWrapSumPrem, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保费出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }

            if ("Amnt".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(tMult));
                    tCal.addBasicFactor("OccupationType", String.valueOf(tOccupationType));
                    tCal.addBasicFactor("Amnt", String.valueOf(tAmnt));
                    tCal.addBasicFactor("InsuYear", tInsuYear);
                    tCal.addBasicFactor("Prem", String.valueOf(tPrem));
                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + tCardNo + "]保额计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        System.out.println(tStrErr);

                        if (!mImportLog.errLog(tCardNo, tStrErr))
                        {
                            mErrors.copyAllErrors(mImportLog.mErrors);
                            return false;
                        }
                        return false;
                        // tTmpCalResult = "0";
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保额出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }
        }
        // ------------------------------------------------

        if (tPrem != 0 && tPrem != tSysWrapSumPrem)
        {
            String tStrErr = "单证号[" + tCardNo + "]系统计算出保费：" + tSysWrapSumPrem + "，与填写保费：" + tPrem + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            System.out.println(tStrErr);

            if (!mImportLog.errLog(tCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        cLICertifySchema.setPrem(tSysWrapSumPrem);

        if (!"3".equals(tCardType))
        {
            if (tAmnt != 0 && tAmnt != tSysWrapSumAmnt)
            {
                String tStrErr = "单证号[" + tCardNo + "]系统计算出保额：" + tSysWrapSumAmnt + "，与填写保额：" + tAmnt + "不一致。";
                buildError("dealCertifyWrapParams", tStrErr);
                System.out.println(tStrErr);

                if (!mImportLog.errLog(tCardNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        cLICertifySchema.setAmnt(tSysWrapSumAmnt);

        return true;
    }

    /**
     * 卡激活清单导入
     * @return
     */
    private MMap activeImportList()
    {
        MMap tMMap = new MMap();

        MMap tTmpMap = null;

        // 准备导入所需数据。
        if (!prepareBeforeImportListDatas())
        {
            return null;
        }
        // ----------------------------

        // 获取导入文件数据。
        if (!parseDiskFile())
        {
            return null;
        }
        // ----------------------------

        // 创建卡单信息。
        tTmpMap = null;
        tTmpMap = activeCardInfoList();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // ----------------------------

        return tMMap;
    }

    /**
     * 卡激活。
     * @return
     */
    private MMap activeCardInfoList()
    {
        MMap tMMap = new MMap();
        boolean tAllSuccFlag = true;

        for (int i = 0; i < mCertifyInfoList.size(); i++)
        {
            LICertifySchema tLICertifySchema = null;
            tLICertifySchema = mCertifyInfoList.getCertifyInfoByIndex(i);

            LICertifyInsuredSet tLICertifyInsuredSet = null;
            tLICertifyInsuredSet = mCertifyInfoList.getCertifyInsuByIndex(i);

            if (tLICertifySchema == null)
            {
                continue;
            }

            // 单证信息完整性校验。
            if (!chkActiveCardDatas(tLICertifySchema.getCardNo()))
            {
                tAllSuccFlag = false;
                continue;
            }
            // ---------------------------

            // 加载卡清单信息。
            LICertifySchema tTmpCardInfo = null;
            tTmpCardInfo = loadCertifyInfo(tLICertifySchema.getCardNo());
            if (tTmpCardInfo == null)
            {
                tAllSuccFlag = false;

                String tStrErr = "单证号不能为空。";
                if (!mImportLog.errLog(tLICertifySchema.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return null;
                }

                continue;
            }
            // ---------------------------

            tTmpCardInfo.setActiveDate(tLICertifySchema.getActiveDate());
            tTmpCardInfo.setActiveFlag(CertifyContConst.CC_ACTIVEFLAG_WEBACTIVE);

            //            Date tCValidate = calCValidate(tLICertifySchema);
            //            if (tCValidate == null)
            //            {
            //                tAllSuccFlag = false;
            //
            //                String tStrErr = "卡单生效日期计算失败。";
            //                if (!mImportLog.errLog(tLICertifySchema.getCardNo(), tStrErr))
            //                {
            //                    mErrors.copyAllErrors(mImportLog.mErrors);
            //                    return null;
            //                }
            //
            //                continue;
            //            }
            //            tTmpCardInfo.setCValidate(tCValidate);

            tTmpCardInfo.setCValidate(tLICertifySchema.getCValidate());

            tTmpCardInfo.setPrem(tLICertifySchema.getPrem());
            tTmpCardInfo.setAmnt(tLICertifySchema.getAmnt());
            tTmpCardInfo.setMult(tLICertifySchema.getMult());
            tTmpCardInfo.setCopys(tLICertifySchema.getCopys());

            // 处理激活卡要素：保额、保费、档次、份数等。（主要为了重算保额）
            if (!dealCertifyWrapParams(tTmpCardInfo))
            {
                return null;
            }
            // --------------------

            if (tLICertifyInsuredSet != null)
            {
                for (int idx = 1; idx <= tLICertifyInsuredSet.size(); idx++)
                {
                    LICertifyInsuredSchema tTmpCertifyInsu = null;
                    tTmpCertifyInsu = tLICertifyInsuredSet.get(idx);

                    tTmpCertifyInsu.setBatchNo(mBatchNo);
                    tTmpCertifyInsu.setSequenceNo(String.valueOf(idx));

                    tTmpCertifyInsu.setGrpContNo(tTmpCardInfo.getGrpContNo());
                    tTmpCertifyInsu.setProposalGrpContNo(tTmpCardInfo.getProposalGrpContNo());
                    tTmpCertifyInsu.setPrtNo(tTmpCardInfo.getPrtNo());

                    tTmpCertifyInsu.setState(CertifyContConst.CI_IMPORT_FIRST);

                    tTmpCertifyInsu.setOperator(mGlobalInput.Operator);
                    tTmpCertifyInsu.setMakeDate(PubFun.getCurrentDate());
                    tTmpCertifyInsu.setMakeTime(PubFun.getCurrentTime());
                    tTmpCertifyInsu.setModifyDate(PubFun.getCurrentDate());
                    tTmpCertifyInsu.setModifyTime(PubFun.getCurrentTime());
                }
                tTmpCardInfo.setInsuPeoples(tLICertifyInsuredSet.size());
            }

            tMMap.put(tLICertifyInsuredSet, SysConst.INSERT);
            tMMap.put(tTmpCardInfo, SysConst.UPDATE);

            // 回置单证激活状态。
            MMap tTmpMap = null;
            tTmpMap = dealCardSysActive(tTmpCardInfo.getCardNo());
            if (tTmpMap == null)
            {
                return null;
            }
            tMMap.add(tTmpMap);
            tTmpMap = null;
            // -----------------------------------

            if (!mImportLog.infoLog(tLICertifySchema.getCardNo(), "数据正常。"))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return null;
            }
        }

        if (tAllSuccFlag && tMMap != null)
        {
            // 终止导入日志。
            MMap tTmpLogEnd = null;
            tTmpLogEnd = mImportLog.logEnd4Map();
            if (tTmpLogEnd == null)
            {
                String tStrErr = "终止导入日志动作失败。";
                buildError("createCertifyList", tStrErr);
                System.out.println(tStrErr);
                return null;
            }
            tMMap.add(tTmpLogEnd);
            tTmpLogEnd = null;
            // ---------------------------
        }

        return (tAllSuccFlag ? tMMap : null);
    }

    /**
     * 根据激活日期计算生效日期。
     * <br />激活日期次日零时，为保单生效日期。
     * @param cActiveDate 激活日期
     * @return
     */
    //    private Date calCValidate(LICertifySchema cCardInfo)
    //    {
    //        Date tResCValidate = null;
    //
    //        FDate tFDate = new FDate();
    //        Date tCValidate = tFDate.getDate(cCardInfo.getCValidate());
    //        Date tActiveDate = tFDate.getDate(cCardInfo.getActiveDate());
    //
    //        if (tActiveDate == null)
    //        {
    //            return null;
    //        }
    //
    //        if (tCValidate != null && tCValidate.after(tActiveDate))
    //        {
    //            tResCValidate = tCValidate;
    //        }
    //        else
    //        {
    //            int tIntervalDays = 1;
    //            Calendar tParamDate = Calendar.getInstance();
    //            try
    //            {
    //                tParamDate.setTime(tActiveDate);
    //                tParamDate.add(Calendar.DATE, tIntervalDays);
    //                tResCValidate = tParamDate.getTime();
    //            }
    //            catch (Exception e)
    //            {
    //                e.printStackTrace();
    //                return null;
    //            }
    //        }
    //
    //        return tResCValidate;
    //    }
    /**
     * 根据卡号从数据库加载卡信息。
     * @param cCardNo
     * @return
     */
    private LICertifySchema loadCertifyInfo(String cCardNo)
    {
        LICertifyDB tLICertifyDB = new LICertifyDB();
        tLICertifyDB.setCardNo(cCardNo);
        if (!tLICertifyDB.getInfo())
        {
            return null;
        }

        return tLICertifyDB.getSchema();
    }

    /**
     * 校验卡单激活信息是否正确、完整。
     * @param cCardNo
     * @return
     */
    private boolean chkActiveCardDatas(String cCardNo)
    {
        String tStrSql = null;

        LICertifySchema tCertifyInfo = null;
        tCertifyInfo = mCertifyInfoList.getCertifyInfoByCardNo(cCardNo);

        LICertifyInsuredSet tCertifyInsuInfo = null;
        tCertifyInsuInfo = mCertifyInfoList.getCertifyInsuByCardNo(cCardNo);

        // 校验卡号所对应单证是否可被激活。
        if (tCertifyInfo.getCardNo() == null || tCertifyInfo.getCardNo().equals(""))
        {
            String tStrErr = "单证号不能为空。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        LICertifyDB tActCardInfo = new LICertifyDB();
        tActCardInfo.setCardNo(tCertifyInfo.getCardNo());
        if (!tActCardInfo.getInfo())
        {
            String tStrErr = "单证号[" + tCertifyInfo.getCardNo() + "]尚未导入。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        String tCardState = tActCardInfo.getState();

        if (tCardState == null || tCardState.equals(CertifyContConst.CC_IMPORT_FIRST))
        {
            String tStrErr = "单证号[" + tCertifyInfo.getCardNo() + "]尚未进行导入确认。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        String tCardActiveState = tActCardInfo.getActiveFlag();
        if (tCardActiveState == null || !tCardActiveState.equals(CertifyContConst.CC_ACTIVEFLAG_INACTIVE))
        {
            String tStrErr = "单证号[" + tCertifyInfo.getCardNo() + "]已经被激活。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        tStrSql = null;
        tStrSql = "select 1 from LICertifyInsured where CardNo = '" + tCertifyInfo.getCardNo() + "'";
        String tCardInsuCount = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tCardInsuCount))
        {
            String tStrErr = "单证号[" + tCertifyInfo.getCardNo() + "]已经存在被保人信息。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        tStrSql = null;

        if (tCertifyInfo.getActiveDate() == null || tCertifyInfo.getActiveDate().equals(""))
        {
            String tStrErr = "激活日期不能为空。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        if (tCertifyInsuInfo.size() == 0)
        {
            String tStrErr = "被保人信息不存在。";
            if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        for (int i = 1; i <= tCertifyInsuInfo.size(); i++)
        {
            LICertifyInsuredSchema tTmpInsuInfo = tCertifyInsuInfo.get(i);

            if (tTmpInsuInfo.getName() == null || tTmpInsuInfo.getName().equals("") || tTmpInsuInfo.getSex() == null
                    || tTmpInsuInfo.getSex().equals("") || tTmpInsuInfo.getBirthday() == null
                    || tTmpInsuInfo.getBirthday().equals("") || tTmpInsuInfo.getIdType() == null
                    || tTmpInsuInfo.getIdType().equals("") || tTmpInsuInfo.getIdNo() == null
                    || tTmpInsuInfo.getIdNo().equals(""))
            {
                String tStrErr = "被保人姓名、性别、出生日期、证件类型、证件号为必填项。";
                if (!mImportLog.errLog(tCertifyInfo.getCardNo(), tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
                return false;
            }
        }
        // -----------------------------------

        return true;
    }

    /**
     * 处理单证系统中，卡激活状态。
     * @param cCardNo
     * @return
     */
    private MMap dealCardSysActive(String cCardNo)
    {
        MMap tMMap = new MMap();

        String tStrSql = " select lzc.* from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where lzcn.CardNo = '" + cCardNo + "' ";

        LZCardSet tLZCardSet = new LZCardDB().executeQuery(tStrSql);
        if (tLZCardSet == null || tLZCardSet.size() != 1)
        {
            String tStrErr = "获取卡信息数据失败。";
            if (!mImportLog.errLog(cCardNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return null;
            }
            return null;
        }

        for (int i = 1; i <= tLZCardSet.size(); i++)
        {
            LZCardSchema tLZCardSchema = tLZCardSet.get(i).getSchema();

            tLZCardSchema.setActiveFlag("1");
            tLZCardSchema.setOperator(mGlobalInput.Operator);
            tLZCardSchema.setModifyDate(PubFun.getCurrentDate());
            tLZCardSchema.setModifyTime(PubFun.getCurrentTime());

            tMMap.put(tLZCardSchema, SysConst.UPDATE);
        }

        return tMMap;
    }

    private boolean dealPOSWrapParams(LICertifySchema cCardInfo, LICertifyInsuredSet cInsuSet)
    {
//    	by gzh 校验卡是否已结算过
    	String tSql = null;
        tSql = "select batchno from licertify where cardno = '"+cCardInfo.getCardNo()+"'";
        String tBatchno = new ExeSQL().getOneValue(tSql);

        if (tBatchno != null && !"".equals(tBatchno))
        {
            String tStrErr = "单证号[" + cCardInfo.getCardNo() + "]在批次["+tBatchno+"]中已结算。";
            if (!mImportLog.errLog(cCardInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }
        
        String tStrSql = null;
        tStrSql = " select lmcd.OperateType from LZCard lzc "
                + " inner join LMCertifyDes lmcd on lmcd.CertifyCode = lzc.CertifyCode "
                + " inner join LZCardNumber lzcn on lzc.SubCode = lzcn.CardType and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where lmcd.CertifyClass = 'D' " + " and lmcd.OperateType in ('0', '1', '2', '3') "
                + " and lzc.State in ('10', '11', '13', '14') " + " and lzcn.CardNo = '" + cCardInfo.getCardNo() + "' ";
        String tOperateType = new ExeSQL().getOneValue(tStrSql);

        if (tOperateType.equals(""))
        {
            String tStrErr = "单证号[" + cCardInfo.getCardNo() + "]获取单证描述信息失败，可能该单证已被结算或回销。";
            if (!mImportLog.errLog(cCardInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        // 如果不是POS类保单，则跳过，该段程序处理。
        if (!"3".equals(tOperateType) && !"1".equals(tOperateType))
        {
            System.out.println("单证类别为：【" + tOperateType + "】,非POS类，跳过。");
            return true;
        }
        // --------------------
        //      by gzh 20110726 忽略上载文件中的被保人清单信息
        if (cInsuSet != null)
        {
            cInsuSet.clear();
            cCardInfo.setCValidate("");
        }

        if (("".equals(cCardInfo.getCValidate()) || cCardInfo.getCValidate() == null)
                && (cInsuSet == null || cInsuSet.size() == 0))
        {
            System.out.println("生成LICertifyInsured数据");
        }
        else
        {
            System.out.println("该单已经存在生效日期和被保人,跳过。");
            return true;
        }

        LICardActiveInfoListSet tCardInfoList = null;
        tStrSql = null;
        tStrSql = "select * from LICardActiveInfoList where CardNo = '" + cCardInfo.getCardNo() + "'";
        tCardInfoList = new LICardActiveInfoListDB().executeQuery(tStrSql);

        if (tCardInfoList == null || tCardInfoList.size() < 1)
        {
            //            String tStrErr = "单证号[" + cCardInfo.getCardNo() + "]获取POS承保信息失败。";
            String tStrErr = "POS或电子商务报送的[" + cCardInfo.getCardNo() + "]单证的承保信息缺失或者还未进行报送！";
            if (!mImportLog.errLog(cCardInfo.getCardNo(), tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
            return false;
        }

        if (cInsuSet == null)
            cInsuSet = new LICertifyInsuredSet();

        double tSumPrem = 0;
        double tSumAmnt = 0;
        double tSumCopys = 0;
        String tCvalidate = "";
        String tMult = "";
        String tOccupationType="";
        int tInsuYear = 0;
        String tInsuYearFlag = "";
        
        for (int i = 1; i <= tCardInfoList.size(); i++)
        {
            LICardActiveInfoListSchema tTmpCardInfo = tCardInfoList.get(i);

            // 保单层信息只录入一次。
            /*
            if (i == 1)
            {
                cCardInfo.setCValidate(tTmpCardInfo.getCValidate());

                cCardInfo.setPrem(tTmpCardInfo.getPrem());
                cCardInfo.setAmnt(tTmpCardInfo.getAmnt());
                cCardInfo.setMult(tTmpCardInfo.getMult());
                cCardInfo.setCopys(tTmpCardInfo.getCopys());
            }
            */
            // --------------------
            if(tTmpCardInfo.getPrem() != null && !"".equals(tTmpCardInfo.getPrem())){
            	tSumPrem = tSumPrem + Double.parseDouble(tTmpCardInfo.getPrem());
            }
            
            if(tTmpCardInfo.getAmnt() != null && !"".equals(tTmpCardInfo.getAmnt())){
            	tSumAmnt = tSumAmnt + Double.parseDouble(tTmpCardInfo.getAmnt());
            }
            
            if(tTmpCardInfo.getCopys() != null && !"".equals(tTmpCardInfo.getCopys())){
            	tSumCopys = tSumCopys + Double.parseDouble(tTmpCardInfo.getCopys());
            }
            
            if(tTmpCardInfo.getInsuYear() != 0 ){
            	tInsuYear = tTmpCardInfo.getInsuYear();
            }
            
            if(tTmpCardInfo.getInsuYearFlag() != null && !"".equals(tTmpCardInfo.getInsuYearFlag())){
            	tInsuYearFlag = tTmpCardInfo.getInsuYearFlag();
            }
            
            tCvalidate = tTmpCardInfo.getCValidate();
            tMult = tTmpCardInfo.getMult();
            tOccupationType=tTmpCardInfo.getOccupationType();

            // 录入被保人信息
            if ((tTmpCardInfo.getName() != null && !tTmpCardInfo.getName().equals(""))
                    || (tTmpCardInfo.getSex() != null && !tTmpCardInfo.getSex().equals(""))
                    || (tTmpCardInfo.getBirthday() != null && !tTmpCardInfo.getBirthday().equals(""))
                    || (tTmpCardInfo.getIdType() != null && !tTmpCardInfo.getIdType().equals(""))
                    || (tTmpCardInfo.getIdNo() != null && !tTmpCardInfo.getIdNo().equals(""))
                    || (tTmpCardInfo.getTicketNo() != null && !tTmpCardInfo.getTicketNo().equals("")))
            {
                LICertifyInsuredSchema tInsuInfo = new LICertifyInsuredSchema();
                tInsuInfo.setCardNo(cCardInfo.getCardNo());
                tInsuInfo.setSequenceNo(String.valueOf(i));

                tInsuInfo.setName(tTmpCardInfo.getName());
                tInsuInfo.setSex(tTmpCardInfo.getSex());
                tInsuInfo.setBirthday(tTmpCardInfo.getBirthday());
                tInsuInfo.setIdType(tTmpCardInfo.getIdType());
                tInsuInfo.setIdNo(tTmpCardInfo.getIdNo());

                tInsuInfo.setOccupationCode(tTmpCardInfo.getOccupationCode());
                tInsuInfo.setOccupationType(tTmpCardInfo.getOccupationType());

                tInsuInfo.setPostalAddress(tTmpCardInfo.getPostalAddress());
                tInsuInfo.setZipCode(tTmpCardInfo.getZipCode());
                tInsuInfo.setPhone(tTmpCardInfo.getPhone());
                tInsuInfo.setEMail(tTmpCardInfo.getEMail());

                tInsuInfo.setGrpName(tTmpCardInfo.getGrpName());
                tInsuInfo.setCompanyPhone(tTmpCardInfo.getCompanyPhone());

                tInsuInfo.setTicketNo(tTmpCardInfo.getTicketNo());
                tInsuInfo.setTeamNo(tTmpCardInfo.getTeamNo());
                tInsuInfo.setSeatNo(tTmpCardInfo.getSeatNo());
                tInsuInfo.setStartTeam(tTmpCardInfo.getFrom());
                tInsuInfo.setEndTeam(tTmpCardInfo.getTo());

                tInsuInfo.setRemark(tTmpCardInfo.getRemark());

                cInsuSet.add(tInsuInfo);
            }

            // --------------------
        }
        
        cCardInfo.setCValidate(tCvalidate);
        cCardInfo.setPrem(tSumPrem);
        cCardInfo.setAmnt(tSumAmnt);
        cCardInfo.setMult(tMult);
        cCardInfo.setCopys(tSumCopys);
        cCardInfo.setInsuYear(String.valueOf(tInsuYear));
        cCardInfo.setInsuYearFlag(tInsuYearFlag);

        return true;
    }

    /**
     * 检查是否存在先激活卡数据，如果存在进行自动补激活处理。
     * @return
     */
    private void actCard()
    {
        String tStrSql = null;
        tStrSql = "select * from LICertify where ActiveFlag = '" + CertifyContConst.CC_ACTIVEFLAG_INACTIVE
                + "' and BatchNo = '" + mBatchNo + "' ";

        LICertifySet tCardList = null;
        tCardList = new LICertifyDB().executeQuery(tStrSql);

        for (int i = 1; i <= tCardList.size(); i++)
        {
            LICertifySchema tLICertifySchema = null;
            tLICertifySchema = tCardList.get(i);

            if (tLICertifySchema == null)
            {
                continue;
            }

            tStrSql = null;
            tStrSql = "select 1 from LICardActiveInfoList where DealFlag in ('04') " + " and CardNo = '"
                    + tLICertifySchema.getCardNo() + "' ";

            String tResult = new ExeSQL().getOneValue(tStrSql);
            if (!"1".equals(tResult))
            {
                continue;
            }

            VData tVData = new VData();

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("CardNo", tLICertifySchema.getCardNo());

            tVData.add(tTransferData);
            tVData.add(mGlobalInput);

            CertActSyncUI tCertActSyncUI = new CertActSyncUI();
            if (!tCertActSyncUI.submitData(tVData, "ActSync"))
            {
                String tStrErr = " 卡激活数据处理失败，原因是: " + tCertActSyncUI.mErrors.getFirstError();
                buildError("submitData", tStrErr);
                continue;
            }
        }
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 校验被保人职业类别。
     * @param OccupationType
     * @return
     */
    private boolean CheckOccupationType(String OccupationType)
    {
        if (OccupationType != null && !OccupationType.equals(""))
        {
            String sql = "select 1 from dual where '" + OccupationType
                    + "' in (select distinct occupationtype from ldoccupation)";
            ExeSQL tExeSQL = new ExeSQL();
            String chk = tExeSQL.getOneValue(sql);
            if ("".equals(chk) || chk == null)
            {
                System.out.println("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
                return false;
            }
        }
        return true;
    }
}
