/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.certifybusiness;


import java.util.Hashtable;

import com.sinosoft.lis.certify.AuditCancelBL;
import com.sinosoft.lis.certify.CertifyFunc;
import com.sinosoft.lis.db.LCCertifyTakeBackDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCCertifyTakeBackSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LCCertifyTakeBackSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理回收操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */

public class CertContTakeBackBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();


    /* 私有成员 */
    private String mszOperate = "";
    private String tError="";

    /* 业务相关的数据 */
    private LCCertifyTakeBackSet mLCCertifyTakeBackSet = new LCCertifyTakeBackSet();
    private VData mResult = new VData();
    private GlobalInput globalInput = new GlobalInput();
    private String mCertifyCode = "";
    private TransferData mTransferData = null;


    // 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。

    public CertContTakeBackBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mszOperate = cOperate;
        if (mszOperate.equals(""))
        {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
        {
        	return false;
        }
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        boolean flag = true;
    	for(int i=1;i<=mLCCertifyTakeBackSet.size();i++){
    		MMap tMMap = new MMap();
    		LCCertifyTakeBackSchema tLCCertifyTakeBackSchema = mLCCertifyTakeBackSet.get(i);
    		String tCardNo = tLCCertifyTakeBackSchema.getCardNo();
    		if(!isSign(tLCCertifyTakeBackSchema.getContNo())){
    			buildError("dealData","保单号码["+tLCCertifyTakeBackSchema.getContNo()+"]未签单或无对应保单！");
    			flag = false;
    			continue;
    		}
        	if("UPDATE".equals(mszOperate)){
        		String tSQL = "select * From LCCertifyTakeBack where cardno = '"+tCardNo+"' and state = '1' ";
        		LCCertifyTakeBackDB tLCCertifyTakeBackDB = new LCCertifyTakeBackDB();
        		LCCertifyTakeBackSet tempLCCertifyTakeBackSet = tLCCertifyTakeBackDB.executeQuery(tSQL);
        		if(tempLCCertifyTakeBackSet == null || tempLCCertifyTakeBackSet.size()!=1){
        			buildError("dealData","单证号码["+tLCCertifyTakeBackSchema.getCardNo()+"]未与保单关联，不可执行修改操作！");
        			flag = false;
        			continue;
        		}
        		LCCertifyTakeBackSchema tempLCCertifyTakeBackSchema = tempLCCertifyTakeBackSet.get(1);
        		tempLCCertifyTakeBackSchema.setState("0");
        		tMMap.put(tempLCCertifyTakeBackSchema, SysConst.UPDATE);
        	}
        	String tStateFlag = tLCCertifyTakeBackSchema.getState();
        	String tBatchNo = (PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+PubFun1.CreateMaxNo("CERTCONT", 5)).replaceAll(":", "");
		    tLCCertifyTakeBackSchema.setBatchNo(tBatchNo);
		    tLCCertifyTakeBackSchema.setState("1");
		    tLCCertifyTakeBackSchema.setOperator(globalInput.Operator);
        	tLCCertifyTakeBackSchema.setMakeDate(PubFun.getCurrentDate());
        	tLCCertifyTakeBackSchema.setMakeTime(PubFun.getCurrentTime());
        	tLCCertifyTakeBackSchema.setModifyDate(PubFun.getCurrentDate());
        	tLCCertifyTakeBackSchema.setModifyTime(PubFun.getCurrentTime());
        	tMMap.put(tLCCertifyTakeBackSchema, SysConst.INSERT);
        	//核销单证
        	if("INSERT".equals(mszOperate)){
	        	if(!takeBackCard(tCardNo,tStateFlag)){
	         		System.out.println(tError);
	        		buildError("dealData","单证号码["+tLCCertifyTakeBackSchema.getCardNo()+ "]"+tError+"，核销失败！");
	    			flag = false;
	    			continue;
	        	}
        	}
        	//单证核销完毕
        	if(!submit(tMMap))
            {
            	return false;
            }
        	tMMap = null;
        }
    	return flag;
    }


    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData(VData vData)
    {
    	
    	globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                "GlobalInput", 0));
    	mLCCertifyTakeBackSet.set((LCCertifyTakeBackSet) vData.getObjectByObjectName("LCCertifyTakeBackSet",0));
        if(mLCCertifyTakeBackSet == null || mLCCertifyTakeBackSet.size()<=0){
        	buildError("getInputData","获取待回销单证数据失败！");
        	return false;
        }
        mTransferData = (TransferData) vData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCertifyCode = (String) mTransferData.getValueByName("CertifyCode");
        if (mCertifyCode == null || "".equals(mCertifyCode))
        {
            buildError("getInputData", "获取单证编码失败。");
            return false;
        }

        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertifyTakeBackUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap aMMap)
    {
        VData data = new VData();
        data.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
    
    private boolean isSign(String aGrpContNo){
    	String tSQL = "select 1 from lccont where appflag = '1' and contno = '"+aGrpContNo+"' " 
    			  + " union all select 1 from lcgrpcont where appflag = '1' and grpcontno = '"+aGrpContNo+"' "
    			  + " union all select 1 from lbcont where appflag in ('1','3') and contno = '"+aGrpContNo+"' "
			      + " union all select 1 from lbgrpcont where appflag in ('1','3') and grpcontno = '"+aGrpContNo+"'";
    	String signFlag = new ExeSQL().getOneValue(tSQL);
    	if(!"1".equals(signFlag)){
    		return false;
    	}
    	return true;
    }
    
    private boolean takeBackCard(String aCardNo, String aStateFlag) {
		LZCardSet setLZCard = new LZCardSet();
		LZCardSchema schemaLZCard = new LZCardSchema();
		schemaLZCard.setCertifyCode(mCertifyCode);
		String auditType = "HANDAUDIT";
		
		schemaLZCard.setStartNo(aCardNo.trim());
		schemaLZCard.setEndNo(aCardNo.trim());
		schemaLZCard.setStateFlag("5");

		if (globalInput.ComCode.length() <= 4) {
			schemaLZCard.setSendOutCom("A" + globalInput.ComCode);
		} else {
			schemaLZCard.setSendOutCom("B" + globalInput.Operator);
		}
		schemaLZCard.setReceiveCom("SYS");

		schemaLZCard.setSumCount(1);
		schemaLZCard.setHandler(globalInput.Operator);
		schemaLZCard.setHandleDate(PubFun.getCurrentDate());

		setLZCard.add(schemaLZCard);

		//	  准备传输数据 VData
		VData vData = new VData();

		vData.addElement(globalInput);
		vData.addElement(setLZCard);
		vData.addElement(auditType);

		Hashtable hashParams = new Hashtable();
		hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
		vData.addElement(hashParams);

		// 数据传输
		AuditCancelBL tCertTakeBackUI = new AuditCancelBL();

		if (!tCertTakeBackUI.submitData(vData, "INSERT")) {
			tError = tCertTakeBackUI.getError().getFirstError();
			return false;
		} else {
			return true;
		}
	}

}
