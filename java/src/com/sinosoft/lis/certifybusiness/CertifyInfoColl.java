/**
 * 2008-11-13
 */
package com.sinosoft.lis.certifybusiness;

import java.util.ArrayList;
import java.util.HashMap;

import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;

/**
 * 完整单证信息的载体。
 * <br />一套单证信息包含：1个LICertify、n个LICertifyInsured
 * 
 * @author LY
 *
 */
public class CertifyInfoColl
{
    /** 错误处理类 */
    public CErrors mErrors = null;

    private ArrayList mCertifyColls = null;

    private HashMap mKeyIndex = null;

    private int mCount = 0;

    public CertifyInfoColl()
    {
        initDatas();
    }

    private void initDatas()
    {
        mCertifyColls = new ArrayList();
        mErrors = new CErrors();
        mKeyIndex = new HashMap();
        mCount = 0;
    }

    public boolean loadCertifyInfo(LICertifySet cLICertifySet,
            LICertifyInsuredSet cLICertifyInsuredSet)
    {
        // 初始化类，防止数据泄漏。
        initDatas();
        // ---------------------

        for (int i = 1; i <= cLICertifySet.size(); i++)
        {
            LICertifySchema tTmpCertifyInfo = cLICertifySet.get(i);
            if (!dealOneCertify(tTmpCertifyInfo, cLICertifyInsuredSet))
            {
                return false;
            }
        }

        return true;
    }

    private boolean dealOneCertify(LICertifySchema cLICertifySchema,
            LICertifyInsuredSet cLICertifyInsuredSet)
    {
        String tCardNo = StrTool.cTrim(cLICertifySchema.getCardNo());

        if (mKeyIndex.get(tCardNo) != null)
        {
            String tStrErr = "清单中单证号出现重复。";
            buildError("dealOneCertify", tStrErr);
            System.out.println(tStrErr);

            return false;
        }

        // 根据CardNo获取到对应的被保人信息。
        LICertifyInsuredSet tInsuColls = new LICertifyInsuredSet();

        for (int i = 1; i <= cLICertifyInsuredSet.size(); i++)
        {
            LICertifyInsuredSchema tTmpInsuSchema = cLICertifyInsuredSet.get(i);

            if (!StrTool.cTrim(tTmpInsuSchema.getCardNo()).equals(tCardNo))
            {
                continue;
            }

			//增加身份证号信息的校验
            if(tTmpInsuSchema.getIdNo() != null && !tTmpInsuSchema.getIdNo().equals("")){
            	 String tFlag = PubFun.CheckIDNo(tTmpInsuSchema.getIdType(), tTmpInsuSchema.getIdNo(), tTmpInsuSchema.getBirthday(), tTmpInsuSchema.getSex());
            	 if( !tFlag.equals("")){
                     buildError("dealOneCertify", tFlag);
                     System.out.println(tFlag);
                     return false;
            	 }
            }
			
            tInsuColls.add(tTmpInsuSchema);
        }
        // --------------------------------

        // 创建完整单证信息。并将信息追加到集合列表。
        HashMap tCertifyInfo = new HashMap();
        tCertifyInfo.put("Certify", cLICertifySchema);
        tCertifyInfo.put("CertifyInsured", tInsuColls);

        mCertifyColls.add(tCertifyInfo);
        // --------------------------------

        // 建立卡号索引
        mKeyIndex.put(tCardNo, new Integer(mCount++));
        // --------------------------------

        return true;
    }

    /**
     * 获取指定单证号的单证信息。
     * @param cCardNo 单证号
     * @return 如存在，返回LICertifySchema；否则，返回null
     */
    public LICertifySchema getCertifyInfoByCardNo(String cCardNo)
    {
        Integer idx = (Integer) mKeyIndex.get(cCardNo);

        if (idx == null)
            return null;

        return getCertifyInfoByIndex(idx.intValue());
    }

    /**
     * 获取指定位置上的单证信息。
     * @param cIndex 位置序号
     * @return 如存在，返回LICertifySchema；否则，返回null
     */
    public LICertifySchema getCertifyInfoByIndex(int cIndex)
    {
        if (cIndex > mCertifyColls.size())
            return null;

        HashMap tTmpCertifyInfo = (HashMap) mCertifyColls.get(cIndex);

        return (LICertifySchema) tTmpCertifyInfo.get("Certify");
    }

    /**
     * 获取指定单证号的单证承保人。
     * @param cCardNo
     * @return
     */
    public LICertifyInsuredSet getCertifyInsuByCardNo(String cCardNo)
    {
        Integer idx = (Integer) mKeyIndex.get(cCardNo);

        if (idx == null)
            return null;

        return getCertifyInsuByIndex(idx.intValue());
    }

    /**
     * 获取指定位置上的单证承保人。
     * @param cIndex 位置序号
     * @return 如存在，返回LICertifyInsuredSet；否则，返回null。
     */
    public LICertifyInsuredSet getCertifyInsuByIndex(int cIndex)
    {
        if (cIndex > mCertifyColls.size())
            return null;

        HashMap tTmpCertifyInfo = (HashMap) mCertifyColls.get(cIndex);

        return (LICertifyInsuredSet) tTmpCertifyInfo.get("CertifyInsured");
    }

    /**
     * 获取集合中单证数量。
     * @return 
     */
    public int size()
    {
        return mCertifyColls == null ? 0 : mCertifyColls.size();
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
