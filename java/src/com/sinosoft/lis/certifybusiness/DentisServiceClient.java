/*
 * @(#)DentisServiceClient.java 1.0 2010/04/07
 *
 * Copyright (c) 2010 SinoSoft  filon51.
 * All rights reserved.
 *
 */
package com.sinosoft.lis.certifybusiness;

/**
 * 调用口腔医院WEBSERVICE服务接口程序
 * 
 * @version 1.0 
 * @since 2010.04.07
 * @author filon51
 */
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.message.MessageElement;
import org.jdom.JDOMException;
import org.jdom.input.DOMBuilder;
import org.jdom.output.DOMOutputter;
import org.w3c.dom.Document;

import com.sinosoft.lis.yibaotong.JdomUtil;

public class DentisServiceClient {
	/**
	 * 调用C# WEBSERVICE测试主程序
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("调用开始！");

		String mInFilePath = "E:/SDTSingle/INSURED_REQUEST.xml";
		String mOutFilePath = "E:/SDTSingle/response.xml";
		String mServiceAddress = "http://121.52.210.101/Service.asmx";

		org.jdom.Document mReceiveDoc = JdomUtil.build(new FileInputStream(
				mInFilePath), "GBK");
		System.out.println("输入XML内容：");
		JdomUtil.print(mReceiveDoc);
		
		DentisServiceClient tDentisServiceClient = new DentisServiceClient();
		org.jdom.Document tOutJDom = null;
		try {
			tOutJDom = tDentisServiceClient.callUploadXmlService(mServiceAddress, mReceiveDoc);
			if (tOutJDom == null) {
				System.err.println("返回报文为空!");
				return;
			}
		}catch(Exception ex){
			System.err.println("callUploadXmlService exception!");
			ex.printStackTrace();
			return;
		}
		
		// 输出调用WEBSERVICE处理结果，并写入文件。
		System.out.println("输出XML内容：");
		JdomUtil.print(tOutJDom);

		JdomUtil.output(tOutJDom, new FileOutputStream(mOutFilePath));
		System.out.println("调用成功！");
		return;
	}

	/**
	 * 
	 * 调用口腔医院WEBSERVICE服务
	 * 
	 * @param aServiceAddress
	 * @param aReceiveDoc
	 * @param tMain
	 * @return
	 * @throws ServiceException
	 * @throws RemoteException
	 * @throws Exception
	 */
	public org.jdom.Document callUploadXmlService(
			String aServiceAddress, org.jdom.Document aReceiveDoc)
			throws ServiceException, RemoteException, Exception {
		// 1、将org.jdom.Document 转换为 MessageElement
		MessageElement[] tMessageElement = convertJdomToMessage(aReceiveDoc);
		if (tMessageElement == null){
			return null;
		}
		
		// 2、调用webservice
		MessageElement[] returnValue = callWebService(aServiceAddress,
				tMessageElement);

		// 3、 将org.w3c.dom.Document转化为org.jdom.Document
		org.jdom.Document tOutJDom = convertMessageToJdom(returnValue);
		return tOutJDom;
	}

	/**
	 * 将消息转换为Jdom
	 * @param returnValue
	 * @return
	 * @throws Exception
	 */
	private org.jdom.Document convertMessageToJdom(
			MessageElement[] returnValue) throws Exception {
		Document mOutXmlDoc = returnValue[0].getAsDocument();
		DOMBuilder outDB = new DOMBuilder();
		org.jdom.Document tOutJDom = outDB.build(mOutXmlDoc);
		return tOutJDom;
	}

	/**
	 * 将Jdom转换为消息
	 * @param tMain
	 * @param aReceiveDoc
	 * @return
	 */
	private MessageElement[] convertJdomToMessage(org.jdom.Document aReceiveDoc) {
		org.w3c.dom.Element tReceiveElement = convertJDomToDom(aReceiveDoc);
		if (tReceiveElement == null) {
			return null;
		}
		MessageElement[] tMessageElement = new MessageElement[1];
		tMessageElement[0] = new MessageElement(tReceiveElement);
		return tMessageElement;
	}

	/**
	 * 调用WEBSERVICE
	 * @param aServiceAddress
	 * @param aMessageElement
	 * @return
	 * @throws ServiceException
	 * @throws RemoteException
	 */
	private MessageElement[] callWebService(String aServiceAddress,
			MessageElement[] aMessageElement) throws ServiceException,
			RemoteException {
		ServiceLocator tService = new ServiceLocator();
		tService.setServiceSoapEndpointAddress(aServiceAddress);
		ServiceSoap tServiceSoap = tService.getServiceSoap();
		UploadXmlDataXml xml = new UploadXmlDataXml();
		xml.set_any(aMessageElement);
		UploadXmlDataResponseResult result = tServiceSoap.uploadXmlData(xml);
		MessageElement[] returnValue = result.get_any();
		return returnValue;
	}

	/**
	 * JDom -> Dom
	 * 
	 * @param aJDomXml
	 * @return
	 */
	private org.w3c.dom.Element convertJDomToDom(org.jdom.Document aJDomXml) {
		org.w3c.dom.Document tOutDomDoc = null;
		try {
			tOutDomDoc = new DOMOutputter().output(aJDomXml);
		} catch (JDOMException ex) {
			System.err.println("jdom.Document --> dom.Document转换失败！"
					+ ex.getMessage());
			ex.printStackTrace();
			return null;
		}
		org.w3c.dom.Element tOutDomElements = tOutDomDoc.getDocumentElement();

		return tOutDomElements;
	}
}
