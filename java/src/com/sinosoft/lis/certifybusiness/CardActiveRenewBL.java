package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CardActiveRenewBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private GlobalInput tGI = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 单证号码 */
    private String cardNo;

    private Reflections mReflections = new Reflections();

    /**
     * submitData
     * 
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        if (!getInputData())
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mInputData, null))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     * 
     * @return boolean
     */
    private boolean getInputData()
    {
        try
        {
            tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
                    0);

            TransferData mTransferData = (TransferData) mInputData
                    .getObjectByObjectName("TransferData", 0);
            cardNo = (String) mTransferData.getValueByName("CardNo");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ScanDeleBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * checkData
     * 
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 
     * @return boolean
     */
    private boolean dealData()
    {
        if (!dealCardState())
        {
            return false;
        }

        if (!actCard())
        {
            return false;
        }

        return true;
    }

    /**
     * 重置卡状态
     * @return
     */
    private boolean dealCardState()
    {
        String tStrSql = " update licardactiveinfolist set dealflag = '00',ModifyDate = Current Date, "
                + " ModifyTime = Current Time "
                + " where CardNo = '"
                + cardNo
                + "' ";
        MMap tTmpMap = new MMap();
        tTmpMap.put(tStrSql, SysConst.UPDATE);
        VData tVData = new VData();
        tVData.add(tTmpMap);
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(tVData, null))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 重新激活卡数据
     * @return
     */
    private boolean actCard()
    {
        String tStrSql = "select ActiveDate from LICardActiveInfoList where CardNo = '"
                + cardNo + "'";
        String tActiveDate = new ExeSQL().getOneValue(tStrSql);

        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ActiveDate", tActiveDate);

        tVData.add(tTransferData);
        tVData.add(tGI);

        CardActiveXlsImportUI tCardActiveXlsImportUI = new CardActiveXlsImportUI();
        if (!tCardActiveXlsImportUI.submitData(tVData, null))
        {
            this.mErrors.copyAllErrors(tCardActiveXlsImportUI.mErrors);
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     * 
     * @return boolean
     */
    private boolean prepareOutputData()
    {

        try
        {
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CardActiveRenewBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "准备传输数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }
}
