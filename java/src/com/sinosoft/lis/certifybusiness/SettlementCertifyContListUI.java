/**
 * 2008-11-18
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class SettlementCertifyContListUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public SettlementCertifyContListUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            SettlementCertifyContListBL tSettlementCertifyContListBL = new SettlementCertifyContListBL();
            if (!tSettlementCertifyContListBL.submitData(cInputData, cOperate))
            {
                this.mErrors
                        .copyAllErrors(tSettlementCertifyContListBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
