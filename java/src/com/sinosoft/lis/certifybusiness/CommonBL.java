/**
 * 2009-1-12
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.ExeSQL;

/**
 * @author LY
 *
 */
public class CommonBL
{
    private CommonBL()
    {
    }

    /**
     * 获取应用根路径。
     * @return
     */
    public static final String getUIRoot()
    {
        String tUIRoot = null;

        try
        {
            String tStrSql = "select SysVarValue from LDSysVar where SysVar = 'UIRoot'";
            tUIRoot = new ExeSQL().getOneValue(tStrSql);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return tUIRoot;
    }
}
