/**
 * 2008-11-12
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class AddCertifyListUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public AddCertifyListUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            AddCertifyListBL tAddCertifyListBL = new AddCertifyListBL();
            if (!tAddCertifyListBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tAddCertifyListBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
