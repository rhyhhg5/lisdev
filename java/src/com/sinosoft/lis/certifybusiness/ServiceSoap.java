/**
 * ServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sinosoft.lis.certifybusiness;

public interface ServiceSoap extends java.rmi.Remote {

    public com.sinosoft.lis.certifybusiness.UploadXmlDataResponseResult uploadXmlData(com.sinosoft.lis.certifybusiness.UploadXmlDataXml xml) throws java.rmi.RemoteException;
}
