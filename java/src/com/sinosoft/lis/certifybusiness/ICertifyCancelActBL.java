/**
 * 2008-12-22
 */
package com.sinosoft.lis.certifybusiness;

import java.io.ByteArrayOutputStream;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ICertifyCancelActBL
{
    private String mErrorInfo = null;

    public ICertifyCancelActBL()
    {
    }

    public String deal(String cCardNo, String cBatchNo, String cSendDate,
            String cSendTime, String cBranchCode, String cSendOperator)
    {
        // 单证作废逻辑调用
        String tDealState = null;
        if (!cancelDeal(cCardNo))
        {
            tDealState = "01";
        }
        else
        {
            tDealState = "00";
        }
        // --------------------

        // 反馈信息
        Element tBatchNo = new Element("BATCHNO");
        tBatchNo.setText(cBatchNo);

        Element tSendDate = new Element("SENDDATE");
        tSendDate.setText(cSendDate);

        Element tSendTime = new Element("SENDTIME");
        tSendTime.setText(cSendTime);

        Element tBranchCode = new Element("BRANCHCODE");
        tBranchCode.setText(cBranchCode);

        Element tSendOperator = new Element("SENDOPERATOR");
        tSendOperator.setText(cSendOperator);

        Element tCardNo = new Element("CARDNO");
        tCardNo.setText(cCardNo);

        Element tCardPassword = new Element("PASSWORD");
        tCardPassword.setText("");

        Element tState = new Element("STATE");
        tState.setText(tDealState);

        Element tErrCode = new Element("ERRCODE");
        tErrCode.setText("");

        Element tErrInfo = new Element("ERRINFO");
        tErrInfo.setText(this.mErrorInfo);
        // --------------------

        Element tContInfo = new Element("CONTINFO");
        tContInfo.addContent(tCardNo);
        tContInfo.addContent(tCardPassword);
        tContInfo.addContent(tState);
        tContInfo.addContent(tErrCode);
        tContInfo.addContent(tErrInfo);

        Element mRootData = new Element("DATASET");
        mRootData.addContent(tBatchNo);
        mRootData.addContent(tSendDate);
        mRootData.addContent(tSendTime);
        mRootData.addContent(tBranchCode);
        mRootData.addContent(tSendOperator);

        mRootData.addContent(tContInfo);

        Document tDocDealResult = new Document(mRootData);
        //        JdomUtil.print(tDocDealResult);

        String tDocStr = toStr(tDocDealResult, "GBK");
        System.out.println("StrDoc:" + tDocStr);

        return tDocStr;
    }

    private boolean cancelDeal(String cCardNo)
    {
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.Operator = "GR_POS";
        mGlobalInput.ComCode = "86";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CardNo", cCardNo);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        CertifyCancelBL tBL = new CertifyCancelBL();
        if (!tBL.submitData(tVData, "Cancel"))
        {
            mErrorInfo = tBL.mErrors.getFirstError();
            return false;
        }

        return true;
    }

    private String toStr(Document pXmlDoc, String pCharset)
    {
        String tStr = null;
        try
        {
            XMLOutputter mXMLOutputter = new XMLOutputter();
            mXMLOutputter.setEncoding(pCharset);
            mXMLOutputter.setTrimText(true);
            ByteArrayOutputStream mBaos = new ByteArrayOutputStream();
            mXMLOutputter.output(pXmlDoc, mBaos);
            tStr = new String(mBaos.toByteArray());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return tStr;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        //        ICertifyCancleActBL tmp = new ICertifyCancleActBL();
        //
        //        String tCardNo = "S3000001820";
        //        tmp.deal(tCardNo, "aaaaa", "2010-02-03", "00:00:00", "", "GR_POS");
        //
        //        System.out.println("ok");
    }
}
