/**
 * 2008-12-22
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LZCardNumberDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.pubfun.LisMD5;
import com.sinosoft.lis.schema.LZCardNumberSchema;
import com.sinosoft.lis.vschema.LZCardNumberSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.XmlExport;

/**
 * @author LY
 *
 */
public class CardStateQueryBL
{
    public CardStateQueryBL()
    {
    }

    public String query(String cCardNo, String cPassword)
    {
        XmlExport tReturnXml = new XmlExport();
        tReturnXml.createDocument("", "");
        
        TextTag tTextTag = new TextTag();

        // 卡号。<CardNo />
        tTextTag.add("CardNo", cCardNo);
        // -----------------------------

        // 卡密码校验。<ChkPwd />
        String tCardLoginState = null;
        tCardLoginState = getCardLoginState(cCardNo, cPassword);
        tTextTag.add("ChkPwd", tCardLoginState);
        // -----------------------------

        // 卡状态。<CardState />
        String tCardState = null;
        tCardState = getCardState(cCardNo, cPassword);
        tTextTag.add("CardState", tCardState);
        // -----------------------------

        // 卡激活状态。<ActiveState />
        String tCardActiveState = null;
        tCardActiveState = getCardActiveState(cCardNo, cPassword);
        tTextTag.add("ActiveState", tCardActiveState);
        // -----------------------------

        tReturnXml.addTextTag(tTextTag);

        String tStrXml = tReturnXml.outputString();

        return (tStrXml == null ? "" : tStrXml);
    }

    /**
     * 卡号密码校验。
     * @param cCardNo
     * @param cPassword
     * @return
     */
    private String getCardLoginState(String cCardNo, String cPassword)
    {
        String tState = "00";

        String tStrSql = "select * from LZCardNumber where CardNo = '"
                + cCardNo + "'";

        LZCardNumberSet tLZCardNumberSet = new LZCardNumberDB()
                .executeQuery(tStrSql);

        if (tLZCardNumberSet == null || tLZCardNumberSet.size() != 1)
        {
            tState = "01";
            System.out.println("卡号在LZCardNumber表中不存在或出现多条。");
            return tState;
        }

        LZCardNumberSchema tCardInfo = tLZCardNumberSet.get(1);

        String tCardNo = tCardInfo.getCardNo();
        String tPassword = tCardInfo.getCardPassword();

        // 转换成与网站约定的密文。
        String tFinalPassword = encryptPassword(tCardNo, tPassword);

        if (tFinalPassword == null || !tFinalPassword.equals(cPassword))
        {
            tState = "01";
            System.out.println("卡激活密码错误。");
            return tState;
        }

        return tState;
    }

    /**
     * 获取卡状态。00－有效；01－失效
     * @param cCardNo
     * @param cPassword
     * @return 
     */
    private String getCardState(String cCardNo, String cPassword)
    {
        String tState = "00";

        LICertifyDB tLICertifyDB = new LICertifyDB();
        tLICertifyDB.setCardNo(cCardNo);
        if (!tLICertifyDB.getInfo())
        {
            tState = "01";
            System.out.println("卡单尚未导入卡折清单。");
            return tState;
        }

        String tStateFlag = tLICertifyDB.getState();
        if (tStateFlag == null
                || !tStateFlag.equals(CertifyContConst.CC_IMPORT_FIRST))
        {
            tState = "01";
            System.out.println("该卡单尚未导入确认。");
            return tState;
        }

        String tStrSql = " select 1 from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where lzcn.State in ('10', '11', '13') "
                + " and lzcn.CardNo = '" + cCardNo + "' " + " with ur ";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if (!tResult.equals("1"))
        {
            System.out.println("LZCard表中，该卡号卡状态为无效状态。");
        }

        return tState;
    }

    /**
     * 获取卡激活状态。00－未激活；01－已激活
     * @param cCardNo
     * @param cPassword
     * @return
     */
    private String getCardActiveState(String cCardNo, String cPassword)
    {
        String tState = "00";

        String tStrSql = " select 1 from LICertify where CardNo = '"
                + cCardNo
                + "' and ActiveFlag = '"
                + CertifyContConst.CC_ACTIVEFLAG_INACTIVE
                + "' "
                + " union all "
                + " select 1 from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where 1 = 1 and lzc.ActiveFlag = '0' "
                + " and lzcn.CardNo = '" + cCardNo + "' " + " with ur ";

        String tResult = new ExeSQL().getOneValue(tStrSql);

        if (!tResult.equals("1"))
        {
            tState = "01";
            System.out.println("卡单已激活。");
            return tState;
        }

        return tState;
    }

    /**
     * 根据约定处理卡单密码。
     * @param cCardNo
     * @param cPassword
     * @return
     */
    private String encryptPassword(String cCardNo, String cPassword)
    {
        // 把库中密码转换成明文。
        String tPassword = cPassword;
        try
        {
            tPassword = new LisIDEA().decryptString(tPassword);
            if (tPassword == null)
            {
                System.out.println("数据库中密码转明文出错。");
                return null;
            }
            tPassword = LisMD5.encryptString(cCardNo + tPassword);
        }
        catch (Exception e)
        {
            System.out.println("处理密文时程序出现异常。");
            return null;
        }
        // -----------------------------

        return tPassword;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        CardStateQueryBL tmp = new CardStateQueryBL();

        String tCardNo = "S3000001820";
        String tPassword = "123456";
        tmp.query(tCardNo, tPassword);
    }
}
