/**
 * 2010-7-8
 */
package com.sinosoft.lis.certifybusiness.api;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 修改
 * 
 * @author LY
 *
 */
public class CardInfoDeal
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mCardNo = null;

    private String mCValidate = null;

    private String mActiveDate = null;

    private String mOperator = null;

    public CardInfoDeal()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            mGlobalInput = new GlobalInput();
            mGlobalInput.Operator = "001";
            mGlobalInput.ComCode = "86";
            mGlobalInput.ManageCom = mGlobalInput.ComCode;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCardNo = (String) mTransferData.getValueByName("CardNo");
        if (mCardNo == null || mCardNo.equals(""))
        {
            buildError("getInputData", "获取单证号码失败。");
            return false;
        }

        mCValidate = (String) mTransferData.getValueByName("CValidate");
        if (mCValidate == null || mCValidate.equals(""))
        {
            buildError("getInputData", "获取单证生效日期失败。");
            return false;
        }

        mActiveDate = (String) mTransferData.getValueByName("ActiveDate");
        if (mActiveDate == null || mActiveDate.equals(""))
        {
            buildError("getInputData", "获取单证激活日期失败。");
            return false;
        }

        mOperator = (String) mTransferData.getValueByName("Operator");
        if (mOperator == null || mOperator.equals(""))
        {
            buildError("getInputData", "获取单证操作员失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        if (!dealCard())
        {
            return false;
        }

        return true;
    }

    /**
     * 保险卡处理类别：01－报文中明确提供卡号密码
     * @return
     */
    private boolean dealCard()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 校验卡号是否在核心进行了录入以及日期格式是否符合格式
        if (!chkCardUsed())
        {
            return false;
        }

        // 处理核心数据
        tTmpMap = null;
        tTmpMap = dealWebCardDatas();
        if (tTmpMap == null)
        {
            return false;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return true;
    }

    /**
     * 校验单证是否在核心系统录入
     * @return
     */
    private boolean chkCardUsed()
    {
        String tCardNo = mCardNo;

        String tStrSql = null;
        String tResult = null;

        // 判断CardNo是否存在或可以使用。
        tStrSql = " select 1 from licertify where Cardno = '" + tCardNo + "' ";
        tResult = new ExeSQL().getOneValue(tStrSql);
        if (!"1".equals(tResult))
        {
            buildError("chkCardUsed", "该单证还没有在核心进行录入。");
            return false;
        }

        Date actDate = new FDate().getDate(mActiveDate);
        if (actDate == null)
        {
            buildError("chkCardUsed", "系统激活时间不符合YYYY-MM-DD格式。");
            return false;
        }

        Date cvaDate = new FDate().getDate(this.mCValidate);
        if (cvaDate == null)
        {
            buildError("chkCardUsed", "系统激活时间不符合YYYY-MM-DD格式。");
            return false;
        }

        // ---------------------------

        return true;
    }

    /**
     * 处理网站保险卡数据
     * <br />1、将保险卡信息（包括：保险卡卡号、密码、相关被保人信息等）保存到 Card_Used 表中。
     * <br />2、删除未使用卡表 Card_New 中该保险卡记录。
     * @return
     */
    private MMap dealWebCardDatas()
    {
        MMap tMMap = new MMap();
        String tStrSql = null;

        Date tCValidate = calCValidate(this.mCValidate, this.mActiveDate);
        // 删除未使用表中该卡号数据
        tStrSql = " update LICertify set cvalidate = '" + tCValidate
                + "',activedate = '" + this.mActiveDate + "' where CardNO = '"
                + this.mCardNo + "' ";
        tMMap.put(tStrSql, "UPDATE");
        tStrSql = null;
        // ----------------------------------------------

        return tMMap;
    }

    /**
     * 根据激活日期计算生效日期。
     * <br />激活日期次日零时，为保单生效日期。
     * @param cActiveDate 激活日期
     * @return
     */
    private Date calCValidate(String strCValidate, String strActiveDate)
    {
        Date tResCValidate = null;

        FDate tFDate = new FDate();
        Date tCValidate = tFDate.getDate(strCValidate);
        Date tActiveDate = tFDate.getDate(strActiveDate);

        if (tActiveDate == null)
        {
            return null;
        }

        if (tCValidate != null && tCValidate.after(tActiveDate))
        {
            tResCValidate = tCValidate;
        }
        else
        {
            int tIntervalDays = 1;
            Calendar tParamDate = Calendar.getInstance();
            try
            {
                tParamDate.setTime(tActiveDate);
                tParamDate.add(Calendar.DATE, tIntervalDays);
                tResCValidate = tParamDate.getTime();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return null;
            }
        }

        return tResCValidate;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
