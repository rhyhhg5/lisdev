/**
 * 2010-10-26
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertActSyncUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public CertActSyncUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            CertActSyncBL tCertActSyncBL = new CertActSyncBL();
            if (!tCertActSyncBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCertActSyncBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
