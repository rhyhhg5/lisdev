/**
 * 2009-2-6
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertifyImpListCheckUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();
    
    public CertifyImpListCheckUI()
    {
    }
    
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            CertifyImpListCheckBL tCertifyImpListCheckBL = new CertifyImpListCheckBL();
            if (!tCertifyImpListCheckBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCertifyImpListCheckBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
