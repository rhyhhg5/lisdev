/**
 * 2008-11-13
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertifyImportLogSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertifyDiskImportLog
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    /**传输到后台处理的map*/
    private MMap mMap = null;

    private String mBatchNo = null;

    /** 初次记录标志 */
    private boolean mFirstLogFlag = true;

    /** 日志起始标志 */
    private static final String LOG_START_FLAG = new String("S");

    /** 日志终止标志 */
    private static final String LOG_END_FLAG = new String("E");

    /** 正常日志标志 */
    private static final String LOG_NORMAL_FLAG = new String("0");

    /** 错误日志标志 */
    private static final String LOG_ERROR_FLAG = new String("1");

    /** 默认日志状态 */
    private static final String LOG_DEFAULT_STATE = new String("1");

    public CertifyDiskImportLog(GlobalInput cGlobalInput, String cBatchNo)
            throws Exception
    {
        mGlobalInput = cGlobalInput;
        if (mGlobalInput == null)
        {
            String tStrErr = "登录信息获取失败。";
            buildError("CertifyDiskImportLog", tStrErr);
            throw new Exception(tStrErr);
        }

        mBatchNo = cBatchNo;
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            String tStrErr = "批次号格式有误";
            buildError("CertifyDiskImportLog", tStrErr);
            throw new Exception(tStrErr);
        }
    }

    private boolean writeLog(String cCardNo, String cErrInfo, String cErrType,
            String cErrState)
    {
        try
        {
            // 如果首次开启日志，记录开始轨迹。
            if (mFirstLogFlag)
            {
                if (!logStart())
                {
                    return false;
                }
            }
            // -------------------------------

            // 创建日志信息，同步数据库。
            MMap tTmpMap = null;
            tTmpMap = createLogInfo(cCardNo, cErrInfo, cErrType, cErrState);
            if (tTmpMap == null)
            {
                buildError("writeLog", "创建日志信息失败。");
                return false;
            }
            mMap = tTmpMap;
            // -------------------------------

            // 即时同步数据库。
            if (!submit())
            {
                buildError("writeLog", "写日志失败。");
                return false;
            }
            mMap = null;
            // -------------------------------
        }
        catch (Exception e)
        {
            buildError("writeLog", "写日志时出现异常。");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 创建一条日志。
     * @param cCardNo 单证号
     * @param cErrInfo 日志信息
     * @param cErrType 日志类型
     * @param cErrState 日志状态
     * @return 返回处理后的事务集MMap。
     */
    private MMap createLogInfo(String cCardNo, String cErrInfo,
            String cErrType, String cErrState)
    {
        MMap tMMap = new MMap();

        // 创建一条日志。
        LICertifyImportLogSchema tOneLog = new LICertifyImportLogSchema();

        tOneLog.setBatchNo(mBatchNo);

        tOneLog.setCardNo(cCardNo);

        tOneLog.setErrorType(cErrType);
        tOneLog.setErrorInfo(cErrInfo);
        tOneLog.setErrorState(cErrState);

        tOneLog.setOperator(mGlobalInput.Operator);
        tOneLog.setMakeDate(PubFun.getCurrentDate());
        tOneLog.setMakeTime(PubFun.getCurrentTime());
        tOneLog.setModifyDate(PubFun.getCurrentDate());
        tOneLog.setModifyTime(PubFun.getCurrentTime());
        // ---------------------------

        tMMap.put(tOneLog, SysConst.DELETE_AND_INSERT);

        return tMMap;
    }

    /**
     * 记录导入日志（正常）。
     * @param cCardNo
     * @param cStrInfo
     * @return
     */
    public boolean infoLog(String cCardNo, String cStrInfo)
    {
        return writeLog(cCardNo, cStrInfo,
                CertifyDiskImportLog.LOG_NORMAL_FLAG,
                CertifyDiskImportLog.LOG_DEFAULT_STATE);
    }

    /**
     * 记录导入日志（正常）。（返回事务包）
     * @return
     */
    public MMap infoLog4Map(String cCardNo, String cStrInfo)
    {
        return createLogInfo(cCardNo, cStrInfo,
                CertifyDiskImportLog.LOG_NORMAL_FLAG,
                CertifyDiskImportLog.LOG_DEFAULT_STATE);
    }

    /**
     * 记录导入异常日志。
     * @param cCardNo
     * @param cStrInfo
     * @return
     */
    public boolean errLog(String cCardNo, String cStrInfo)
    {
        return writeLog(cCardNo, cStrInfo, CertifyDiskImportLog.LOG_ERROR_FLAG,
                CertifyDiskImportLog.LOG_DEFAULT_STATE);
    }

    /**
     * 记录导入日志（正常）。（返回事务包）
     * @return
     */
    public MMap errLog4Map(String cCardNo, String cStrInfo)
    {
        return createLogInfo(cCardNo, cStrInfo,
                CertifyDiskImportLog.LOG_ERROR_FLAG,
                CertifyDiskImportLog.LOG_DEFAULT_STATE);
    }

    /**
     * 开启日志。
     * @return
     */
    private boolean logStart()
    {
        if (isEndOfBatchNo())
        {
            String tStrErr = "[" + mBatchNo + "]批次已完成导入，请尝试重新申请批次。";
            buildError("logStart", tStrErr);
            System.out.println(tStrErr);
            return false;
        }

        mFirstLogFlag = false;

        return writeLog(CertifyDiskImportLog.LOG_START_FLAG, "批次开始导入",
                CertifyDiskImportLog.LOG_START_FLAG,
                CertifyDiskImportLog.LOG_DEFAULT_STATE);
    }

    /**
     * 终止整个批次导入日志。（即时同步数据库）
     * @return
     */
    public boolean logEnd()
    {
        return writeLog(CertifyDiskImportLog.LOG_END_FLAG, "批次导入正常终止",
                CertifyDiskImportLog.LOG_END_FLAG,
                CertifyDiskImportLog.LOG_DEFAULT_STATE);
    }

    /**
     * 终止整个批次导入日志。（返回事务包）
     * @return
     */
    public MMap logEnd4Map()
    {
        return createLogInfo(CertifyDiskImportLog.LOG_END_FLAG, "批次导入正常终止",
                CertifyDiskImportLog.LOG_END_FLAG,
                CertifyDiskImportLog.LOG_DEFAULT_STATE);
    }

    /**
     * 获取当前批次是否已被终止记录。
     * @return 被终止，返回true；反之，返回true。
     */
    public boolean isEndOfBatchNo()
    {
        String tStrSql = "select 1 from LICertifyImportLog lictil "
                + " where lictil.ErrorType = '"
                + CertifyDiskImportLog.LOG_END_FLAG + "' "
                + " and lictil.BatchNo = '" + mBatchNo + "' with ur ";

        return "1".equals(new ExeSQL().getOneValue(tStrSql));
    }

    /**
     * 判断某一批次号是否已经被终止记录。
     * @param cBatchNo
     * @return
     */
    public static boolean isEndOfBatchNo(String cBatchNo)
    {
        String tStrSql = "select 1 from LICertifyImportLog lictil "
                + " where lictil.ErrorType = '"
                + CertifyDiskImportLog.LOG_END_FLAG + "' "
                + " and lictil.BatchNo = '" + cBatchNo + "' with ur ";

        return "1".equals(new ExeSQL().getOneValue(tStrSql));
    }

    /**
     * 根据批次号清除相关全部导入日志。（返回事务包）
     * @return
     */
    public MMap cleanAllLogOfBatchNo4Map()
    {
        MMap tMMap = new MMap();

        String tStrSql = " delete from LICertifyImportLog lictil "
                + " where lictil.BatchNo = '" + mBatchNo + "' ";

        tMMap.put(tStrSql, SysConst.DELETE);

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyDiskImportLog";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
}
