/**
 * 2009-5-5
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class WSCertifyUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public WSCertifyUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            WSCertifyBL tWSCertifyBL = new WSCertifyBL();
            if (!tWSCertifyBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tWSCertifyBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }

}
