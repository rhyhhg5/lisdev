/**
 * 2010-11-5
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertActBatchImportBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mActiveDate = null;

    public CertActBatchImportBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if (getSubmitMap(cInputData, operate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String operate)
    {
        if (!getInputData(cInputData, operate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mActiveDate = (String) mTransferData.getValueByName("ActiveDate");
        if (mActiveDate == null || mActiveDate.equals(""))
        {
            buildError("getInputData", "获取激活日期失败。");
            return false;
        }

        return true;
    }

    /**
     * 校验操作是否合法
     * 1、校验是否录入了GlobalInpt信息，若没有录入，则构造如下信息：
     * Operator = “001”；
     * ComCode = ‘86“
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            mGlobalInput = new GlobalInput();
            mGlobalInput.Operator = "001";
            mGlobalInput.ComCode = "86";
            mGlobalInput.ManageCom = mGlobalInput.ComCode;
        }

        return true;
    }

    private boolean dealData()
    {
        if ("ActSyncBatch".equals(mOperate))
        {
            String[] tCardNoList = loadActiveCardInfo();
            if (tCardNoList == null || tCardNoList.length == 0)
            {
                // 如果未查到有待激活的卡信息，直接返回。
                return true;
            }

            // 处理待激活卡单，并生成卡激活导入文件到指定文件夹下。
            if (!dealCardActiveDatas(tCardNoList))
            {
                return false;
            }
            // --------------------------------
        }

        return true;
    }

    private boolean dealCardActiveDatas(String[] cCardNoList)
    {
        try
        {
            VData tVData = new VData();

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("CardNoList", cCardNoList);

            tVData.add(tTransferData);
            tVData.add(mGlobalInput);

            CertActBatchSyncBL tCertActBatchSyncBL = new CertActBatchSyncBL();
            if (!tCertActBatchSyncBL.submitData(tVData, "ActSync"))
            {
                this.mErrors.copyAllErrors(tCertActBatchSyncBL.mErrors);
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 获取待处理卡激活清单信息
     * @return
     */
    private String[] loadActiveCardInfo()
    {
        String tStrSql = " select distinct cardno from LICardActiveInfoList " + " where DealFlag = '"
                + CertifyContConst.CA_ACT_INFO_UNDEALED + "' " + " and ActiveDate = '" + mActiveDate + "'";

        SSRS cardNoSSRS = new SSRS();
        cardNoSSRS = new ExeSQL().execSQL(tStrSql);
        if (cardNoSSRS == null || cardNoSSRS.MaxRow == 0)
            return null;

        String[] tCardNoList = new String[cardNoSSRS.MaxRow];

        for (int i = 1; i <= cardNoSSRS.MaxRow; i++)
        {
        	tCardNoList[i - 1] = cardNoSSRS.GetText(i, 1);
        }

        return tCardNoList;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
