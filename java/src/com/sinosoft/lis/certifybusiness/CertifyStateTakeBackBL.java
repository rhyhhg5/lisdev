/**
 * 2008-12-1
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.vschema.LZCardTrackSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertifyStateTakeBackBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 团体无名单印刷号（结算单号） */
    private String mPrtNo = null;

    /** 卡折清单 */
    private LZCardSet mCardList = null;

    public CertifyStateTakeBackBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mPrtNo = (String) mTransferData.getValueByName("PrtNo");

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("ConfBack".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = certifyConfBack();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    /**
     * 单证核销
     * @return
     */
    private MMap certifyConfBack()
    {
        MMap tMMap = new MMap();

        // 根据印刷号装载对应的全部单证列表。
        if (!loadCardListByPrtNo())
        {
            return null;
        }
        // -------------------------------

        LZCardSet tLZCardSet = new LZCardSet();
        LZCardTrackSet tLZCardTrackSet = new LZCardTrackSet();

        if (mCardList != null && mCardList.size() > 0)
        {
            Reflections tReflections = new Reflections();

            String tCurDate = PubFun.getCurrentDate();
            String tCurTime = PubFun.getCurrentTime();

            for (int i = 1; i <= mCardList.size(); i++)
            {
                LZCardSchema tTmpCardInfo = mCardList.get(i);

                tTmpCardInfo.setState("2");

                tTmpCardInfo.setHandler(mGlobalInput.Operator);
                tTmpCardInfo.setHandleDate(tCurDate);

                String tTmpSendOutCom = tTmpCardInfo.getSendOutCom();
                String tTmpReceiveCom = tTmpCardInfo.getReceiveCom();
                tTmpCardInfo.setSendOutCom(tTmpReceiveCom);
                tTmpCardInfo.setReceiveCom(tTmpSendOutCom);

                tTmpCardInfo.setOperator(mGlobalInput.Operator);
                tTmpCardInfo.setMakeDate(tCurDate);
                tTmpCardInfo.setMakeTime(tCurTime);
                tTmpCardInfo.setModifyDate(tCurDate);
                tTmpCardInfo.setModifyTime(tCurTime);

                LZCardTrackSchema tTmpCardTrack = new LZCardTrackSchema();
                tReflections.transFields(tTmpCardTrack, tTmpCardInfo);

                tLZCardSet.add(tTmpCardInfo);
                tLZCardTrackSet.add(tTmpCardTrack);
            }

            tMMap.put(tLZCardSet, SysConst.UPDATE);
            tMMap.put(tLZCardTrackSet, SysConst.INSERT);

            //        VData vData = new VData();
            //
            //        vData.addElement(mGlobalInput);
            //        vData.addElement(mCardList);
            //
            //        CertTakeBackUI tCertTakeBackUI = new CertTakeBackUI();
            //
            //        if (!tCertTakeBackUI.submitData(vData, "HEXIAO"))
            //        {
            //            System.out.println(tCertTakeBackUI.mErrors.getFirstError());
            //            String tStrErr = "单证核销失败。";
            //            buildError("certifyConfBack", tStrErr);
            //            return false;
            //        }

        }

        return tMMap;
    }

    /**
     * 根据印刷号装载对应的全部单证列表。
     * @return
     */
    private boolean loadCardListByPrtNo()
    {
        if (mPrtNo == null && mPrtNo.equals(""))
        {
            String tStrErr = "获取结算单号失败。";
            buildError("loadCardListByPrtNo", tStrErr);
            return false;
        }

        String tStrSql = "select * from LZCard lzc "
                + " inner join LZCardNumber lzcn on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " inner join LICertify lict on lict.CardNo = lzcn.CardNo "
                + " where lict.PrtNo = '" + mPrtNo + "' ";

        mCardList = new LZCardDB().executeQuery(tStrSql);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyStateTakeBackBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
