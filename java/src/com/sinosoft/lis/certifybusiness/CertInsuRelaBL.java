/**
 * 2010-9-10
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertInsuRelaBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 卡折主信息 */
    private LICertifySchema mCertifyInfo = null;

    /** 卡折被保人信息 */
    private LICertifyInsuredSchema mCertifyInsuInfo = null;

    /** 合同信息 */
    private LCContSchema mSrcContSchema = null;

    /** 险种信息样本 */
    private LCPolSet mSrcPolSet = null;

    /** 险种责任信息样本 */
    private LCDutySet mSrcDutySet = null;

    /** 险种责任保费信息样本 */
    private LCPremSet mSrcPremSet = null;

    /** 险种给付责任信息样本 */
    private LCGetSet mSrcGetSet = null;

    /** 被保人信息 */
    private LCInsuredSchema mInsuSchema = null;

    /** 被保人客户信息 */
    private LDPersonSchema mPersonSchema = null;

    /** 被保人客户地址信息 */
    private LCAddressSchema mInsuAddrSchema = null;

    /** 险种信息 */
    private LCPolSet mPolSet = null;

    /** 险种责任信息 */
    private LCDutySet mDutySet = null;

    /** 险种责任保费信息 */
    private LCPremSet mPremSet = null;

    /** 险种给付责任信息 */
    private LCGetSet mGetSet = null;

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        // 将处理后的schema放入map中
        if (!prepareOutputData())
        {
            return null;
        }
        // --------------------

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCertifyInfo = (LICertifySchema) cInputData.getObjectByObjectName("LICertifySchema", 0);
        if (mCertifyInfo == null)
        {
            buildError("getInputData", "未获取到卡折承保信息。");
            return false;
        }

        mCertifyInsuInfo = (LICertifyInsuredSchema) cInputData.getObjectByObjectName("LICertifyInsuredSchema", 0);
        if (mCertifyInsuInfo == null)
        {
            buildError("getInputData", "未获取到卡折被保人信息。");
            return false;
        }

        mSrcContSchema = ((LCContSchema) cInputData.getObjectByObjectName("LCContSchema", 0)).getSchema();
        if (mSrcContSchema == null)
        {
            buildError("getInputData", "未获取到合同信息样本。");
            return false;
        }

        mSrcPolSet = (LCPolSet) cInputData.getObjectByObjectName("LCPolSet", 0);
        if (mSrcPolSet == null || mSrcPolSet.size() == 0)
        {
            buildError("getInputData", "未获取到险种信息样本。");
            return false;
        }

        mSrcDutySet = (LCDutySet) cInputData.getObjectByObjectName("LCDutySet", 0);
        if (mSrcDutySet == null || mSrcPolSet.size() == 0)
        {
            buildError("getInputData", "未获取到责任信息样本。");
            return false;
        }

        mSrcPremSet = (LCPremSet) cInputData.getObjectByObjectName("LCPremSet", 0);
        if (mSrcPremSet == null || mSrcPremSet.size() == 0)
        {
            buildError("getInputData", "未获取到费用信息样本。");
            return false;
        }

        mSrcGetSet = (LCGetSet) cInputData.getObjectByObjectName("LCGetSet", 0);
        if (mSrcGetSet == null || mSrcGetSet.size() == 0)
        {
            buildError("getInputData", "未获取到给付责任信息样本。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Create".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = cloneInsuRelaCont();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap cloneInsuRelaCont()
    {
        MMap tMMap = new MMap();

        // 创建被保人信息LCInsured
        if (!dealInsuInfo())
        {
            return null;
        }
        // --------------------

        // 创建险种信息LCPol
        if (!dealPolRiskInfo())
        {
            return null;
        }
        // --------------------

        // 回置相关清单状态
        if (!dealCertInfo())
        {
            return null;
        }
        // --------------------

        return tMMap;
    }

    /**
     * 处理险种信息。
     * @return
     */
    private boolean dealPolRiskInfo()
    {
        Reflections tRef = new Reflections();

        mPolSet = new LCPolSet();
        mDutySet = new LCDutySet();
        mPremSet = new LCPremSet();
        mGetSet = new LCGetSet();
        for (int i = 1; i <= mSrcPolSet.size(); i++)
        {
            String tProposalNo = PubFun1.CreateMaxNo("ProposalNo", PubFun.getNoLimit(mSrcContSchema.getManageCom()));
            if (tProposalNo == null || tProposalNo.equals(""))
            {
                buildError("createInsuPol", "生成险种投保号失败。");
                return false;
            }

            String tNewPolNo = PubFun1.CreateMaxNo("POLNO", PubFun.getNoLimit(mSrcContSchema.getManageCom()));
            if (tNewPolNo == null || tNewPolNo.equals(""))
            {
                buildError("createInsuPol", "生成险种号失败。");
                return false;
            }

            // 处理险种信息
            LCPolSchema tRPolInfo = new LCPolSchema();
            LCPolSchema tSPolInfo = mSrcPolSet.get(i).getSchema();

            tRef.transFields(tRPolInfo, tSPolInfo);
            tRPolInfo.setPolNo(tNewPolNo);
            tRPolInfo.setProposalNo(tProposalNo);
            
            tRPolInfo.setInsuredNo(mInsuSchema.getInsuredNo());
            tRPolInfo.setInsuredName(mInsuSchema.getName());
            tRPolInfo.setInsuredSex(mInsuSchema.getSex());
            tRPolInfo.setInsuredBirthday(mInsuSchema.getBirthday());
            
            mPolSet.add(tRPolInfo);
            // --------------------

            String tSPolNo = tSPolInfo.getPolNo();

            // 处理责任信息
            for (int duty_idx = 1; duty_idx <= mSrcDutySet.size(); duty_idx++)
            {
                LCDutySchema tSDutyInfo = mSrcDutySet.get(duty_idx).getSchema();
                if (!tSPolNo.equals(tSDutyInfo.getPolNo()))
                {
                    continue;
                }
                LCDutySchema tRDutyInfo = new LCDutySchema();
                tRef.transFields(tRDutyInfo, tSDutyInfo);
                tRDutyInfo.setPolNo(tRPolInfo.getPolNo());
                mDutySet.add(tRDutyInfo);
            }
            // --------------------

            // 处理费用信息
            for (int prem_idx = 1; prem_idx <= mSrcDutySet.size(); prem_idx++)
            {
                LCPremSchema tSPremInfo = mSrcPremSet.get(prem_idx).getSchema();
                if (!tSPolNo.equals(tSPremInfo.getPolNo()))
                {
                    continue;
                }
                LCPremSchema tRPremInfo = new LCPremSchema();
                tRef.transFields(tRPremInfo, tSPremInfo);
                tRPremInfo.setPolNo(tRPolInfo.getPolNo());
                mPremSet.add(tRPremInfo);
            }
            // --------------------

            // 处理给付责任信息
            for (int get_idx = 1; get_idx <= mSrcGetSet.size(); get_idx++)
            {
                LCGetSchema tSGetInfo = mSrcGetSet.get(get_idx).getSchema();
                if (!tSPolNo.equals(tSGetInfo.getPolNo()))
                {
                    continue;
                }
                LCGetSchema tRGetInfo = new LCGetSchema();
                tRef.transFields(tRGetInfo, tSGetInfo);
                tRGetInfo.setPolNo(tRPolInfo.getPolNo());
                mGetSet.add(tRGetInfo);
            }
            // --------------------
        }

        if (mPolSet.size() == 0 || mPolSet.size() != mSrcPolSet.size())
        {
            buildError("dealPolRiskInfo", "连带被保险人险种信息处理失败。");
            return false;
        }

        if (mDutySet.size() == 0 || mDutySet.size() != mSrcDutySet.size())
        {
            buildError("dealPolRiskInfo", "连带被保险人责任信息处理失败。");
            return false;
        }

        if (mPremSet.size() == 0 || mPremSet.size() != mSrcPremSet.size())
        {
            buildError("dealPolRiskInfo", "连带被保险人险种费用信息处理失败。");
            return false;
        }

        if (mGetSet.size() == 0 || mGetSet.size() != mSrcGetSet.size())
        {
            buildError("dealPolRiskInfo", "连带被保险人给付责任信息处理失败。");
            return false;
        }

        return true;
    }

    /**
     * 产生被保人信息，如果为新客户同时产生客户资料信息。
     * @return
     */
    private boolean dealInsuInfo()
    {
        Reflections tRef = new Reflections();
        ExeSQL tExeSQL = new ExeSQL();

        String tStrSql = null;
        String tResult = null;

        // 校验传入被保人的信息是否完整。
        if (!chkCertInsudata(mCertifyInsuInfo))
        {
            return false;
        }
        // --------------------

        if (mInsuSchema == null)
            mInsuSchema = new LCInsuredSchema();

        // 封装被保人信息
        mInsuSchema.setGrpContNo(mSrcContSchema.getGrpContNo());

        mInsuSchema.setContNo(mSrcContSchema.getContNo());
        mInsuSchema.setPrtNo(mCertifyInfo.getPrtNo());
        mInsuSchema.setManageCom(mSrcContSchema.getManageCom());
        mInsuSchema.setExecuteCom(mGlobalInput.ManageCom);

        mInsuSchema.setContPlanCode("A");//卡折保障计划默认为A

        mInsuSchema.setAppntNo(mSrcContSchema.getAppntNo());

        mInsuSchema.setName(mCertifyInsuInfo.getName());
        mInsuSchema.setSex(mCertifyInsuInfo.getSex());
        mInsuSchema.setBirthday(mCertifyInsuInfo.getBirthday());
        mInsuSchema.setIDNo(mCertifyInsuInfo.getIdNo());
        mInsuSchema.setIDType(mCertifyInsuInfo.getIdType());

        mInsuSchema.setOccupationType(mCertifyInsuInfo.getOccupationType());
        mInsuSchema.setOccupationCode(mCertifyInsuInfo.getOccupationCode());
        mInsuSchema.setRelationToMainInsured("30");//卡折连带被保人默认为“30－其他”
        mInsuSchema.setGrpInsuredPhone(mCertifyInsuInfo.getPhone());

        mInsuSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mInsuSchema);
        // --------------------

        // 产生被保人客户信息
        tStrSql = null;
        tResult = null;
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tRef.transFields(tLDPersonSchema, mInsuSchema);
        tStrSql = " select CustomerNo from LDPerson " + " where 1 = 1 " + " and Name = '" + tLDPersonSchema.getName()
                + "' " + " and Sex = '" + tLDPersonSchema.getSex() + "' " + " and Birthday = '"
                + tLDPersonSchema.getBirthday() + "' " + " and IDNo = '" + tLDPersonSchema.getIDNo() + "' "
                + " and IDType = '" + tLDPersonSchema.getIDType() + "' " + " order by CustomerNo ";//如果存在多个客户，固定选择最小的客户号做为默认客户号。
        tResult = tExeSQL.getOneValue(tStrSql);
        if (!tResult.equals(""))
        {
            mInsuSchema.setInsuredNo(tResult);
            tLDPersonSchema.setCustomerNo(tResult);//如果客户信息已经存在，则不进行保存或修改。
        }
        else
        {
            String tInsuredNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
            if (tInsuredNo == null || tInsuredNo.equals(""))
            {
                buildError("dealInsuInfo", "生成客户号失败。");
                return false;
            }
            mInsuSchema.setInsuredNo(tInsuredNo);
            tLDPersonSchema.setCustomerNo(tInsuredNo);
            mPersonSchema = tLDPersonSchema.getSchema();//被保人为新客户，创建客户信息
        }

        if (mInsuSchema.getInsuredNo() == null || mInsuSchema.getInsuredNo().equals(""))
        {
            buildError("dealInsuInfo", "被保人客户号为空。");
            return false;
        }
        tStrSql = null;
        tResult = null;
        // --------------------

        // 产生被保人地址信息
        if (mInsuAddrSchema == null)
            mInsuAddrSchema = new LCAddressSchema();

        String tAddressNo = tExeSQL
                .getOneValue("Select Case When (max(integer(AddressNo))) Is Null Then 0 Else (max(integer(AddressNo))) End from LCAddress where CustomerNo='"
                        + mInsuSchema.getInsuredNo() + "'");
        if (tAddressNo == null || tAddressNo.equals(""))
        {
            buildError("checkAppntAddress", "生成地址代码错误！");
            return false;
        }

        try
        {
            tAddressNo = String.valueOf(Integer.parseInt(tAddressNo) + 1);
        }
        catch (Exception e)
        {
            buildError("checkAppntAddress", "生成新地址代码错误，可能地址号出现了非数字型数据！");
            return false;
        }

        mInsuSchema.setAddressNo(tAddressNo);

        mInsuAddrSchema.setCustomerNo(mInsuSchema.getInsuredNo());
        mInsuAddrSchema.setAddressNo(tAddressNo);
        mInsuAddrSchema.setPostalAddress(mCertifyInsuInfo.getPostalAddress());
        mInsuAddrSchema.setZipCode(mCertifyInsuInfo.getZipCode());
        mInsuAddrSchema.setGrpName(mCertifyInsuInfo.getGrpName());
        mInsuAddrSchema.setCompanyPhone(mCertifyInsuInfo.getCompanyPhone());
        mInsuAddrSchema.setPhone(mCertifyInsuInfo.getPhone());
        mInsuAddrSchema.setMobile(mCertifyInsuInfo.getMobile());
        mInsuAddrSchema.setEMail(mCertifyInsuInfo.getEMail());

        mInsuAddrSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mInsuAddrSchema);
        // --------------------

        return true;
    }

    /**
     * 校验连带被保人信息是否完整。
     * 
     * @param cCertInsuInfo
     * @return
     */
    private boolean chkCertInsudata(LICertifyInsuredSchema cCertInsuInfo)
    {
        // 判断被保人五要素信息是否齐全
        if (cCertInsuInfo.getName() == null || cCertInsuInfo.getName().equals(""))
        {
            buildError("chkCertInsudata", "[" + cCertInsuInfo.getCardNo() + "]保险卡被保人姓名信息缺失。");
            return false;
        }
        if (cCertInsuInfo.getSex() == null || cCertInsuInfo.getSex().equals(""))
        {
            buildError("chkCertInsudata", "[" + cCertInsuInfo.getCardNo() + "]保险卡被保人性别信息缺失。");
            return false;
        }
        if (cCertInsuInfo.getBirthday() == null || cCertInsuInfo.getBirthday().equals(""))
        {
            buildError("chkCertInsudata", "[" + cCertInsuInfo.getCardNo() + "]保险卡被保人出生日期信息缺失。");
            return false;
        }
        if (cCertInsuInfo.getIdType() == null || cCertInsuInfo.getIdType().equals(""))
        {
            buildError("chkCertInsudata", "[" + cCertInsuInfo.getCardNo() + "]保险卡被保人证件类别信息缺失。");
            return false;
        }
        if (cCertInsuInfo.getIdNo() == null || cCertInsuInfo.getIdNo().equals(""))
        {
            buildError("chkCertInsudata", "[" + cCertInsuInfo.getCardNo() + "]保险卡被保人证件号码信息缺失。");
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 处理卡折清单状态及回写数据。
     * @return
     */
    private boolean dealCertInfo()
    {
        mCertifyInsuInfo.setCustomerNo(mInsuSchema.getInsuredNo());
        return true;
    }

    /**
     * 准备处理好的数据。
     * @return
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        mResult.add(mInsuSchema);
        mResult.add(mPersonSchema);
        mResult.add(mInsuAddrSchema);
        mResult.add(mPolSet);
        mResult.add(mDutySet);
        mResult.add(mPremSet);
        mResult.add(mGetSet);
        mResult.add(mCertifyInsuInfo);

        if (mInsuSchema != null)
        {
            mMap.put(mInsuSchema, SysConst.INSERT);
        }
        if (mPersonSchema != null)
        {
            mMap.put(mPersonSchema, SysConst.INSERT);
        }
        if (mInsuAddrSchema != null)
        {
            mMap.put(mInsuAddrSchema, SysConst.INSERT);
        }
        if (mPolSet != null && mPolSet.size() > 0)
        {
            mMap.put(mPolSet, SysConst.INSERT);
        }
        if (mDutySet != null && mDutySet.size() > 0)
        {
            mMap.put(mDutySet, SysConst.INSERT);
        }
        if (mPremSet != null && mPremSet.size() > 0)
        {
            mMap.put(mPremSet, SysConst.INSERT);
        }
        if (mGetSet != null && mGetSet.size() > 0)
        {
            mMap.put(mGetSet, SysConst.INSERT);
        }
        if (mCertifyInsuInfo != null)
        {
            mMap.put(mCertifyInsuInfo, SysConst.UPDATE);
        }

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return mResult;
    }
}
