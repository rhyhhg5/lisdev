/**
 * 2009-6-5
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 * 
 * 卡激活校验
 *
 */
public class CardActImpChkBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mCurDate = PubFun.getCurrentDate();

    private String mCurTime = PubFun.getCurrentTime();

    /** 待校验卡信息 */
    private LICardActiveInfoListSet mCardInfoList = null;

    public CardActImpChkBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if (getSubmitMap(cInputData, operate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String operate)
    {
        if (!getInputData(cInputData, operate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCardInfoList = (LICardActiveInfoListSet) cInputData
                .getObjectByObjectName("LICardActiveInfoListSet", 0);
        if (mCardInfoList == null)
        {
            buildError("getInputData", "获取卡清单激活信息失败。");
            return false;
        }

        return true;
    }

    /**
     * 校验操作是否合法
     * 1、校验是否录入了GlobalInpt信息，若没有录入，则构造如下信息：
     * Operator = “001”；
     * ComCode = ‘86“
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Check".equals(mOperate))
        {
            if (mCardInfoList == null || mCardInfoList.size() == 0)
            {
                // 如果未查到有待激活的卡信息，直接返回。
                return true;
            }

            // 处理待激活卡单，并生成卡激活导入文件到指定文件夹下。
            tTmpMap = null;
            tTmpMap = dealCardActInfo();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
            // --------------------------------
        }

        return true;
    }

    /**
     * 校验卡信息，处理中间表卡操作状态（DealFlag）
     * @return
     */
    private MMap dealCardActInfo()
    {
        MMap tTmpMap = new MMap();

        LICardActiveInfoListSet tCardListSet = new LICardActiveInfoListSet();
        for (int i = 1; i <= mCardInfoList.size(); i++)
        {
            LICardActiveInfoListSchema tTmpCardActInfo = mCardInfoList.get(i)
                    .getSchema();

            if (!chkOldCard(tTmpCardActInfo))
            {
                // 校验是否为旧卡数据
                tTmpCardActInfo
                        .setDealFlag(CertifyContConst.CA_ACT_INFO_ERR_OLDCARD);
                tTmpCardActInfo.setDealDate(mCurDate);

                tTmpCardActInfo.setOperator(mGlobalInput.Operator);
                tTmpCardActInfo.setModifyDate(mCurDate);
                tTmpCardActInfo.setModifyTime(mCurTime);
            }
            else if (!chkCardBaseInfo(tTmpCardActInfo))
            {
                // 校验卡清单中提供信息是否完整
                tTmpCardActInfo
                        .setDealFlag(CertifyContConst.CA_ACT_INFO_ERR_BASEINFOERR);
                tTmpCardActInfo.setDealDate(mCurDate);

                tTmpCardActInfo.setOperator(mGlobalInput.Operator);
                tTmpCardActInfo.setModifyDate(mCurDate);
                tTmpCardActInfo.setModifyTime(mCurTime);
            }
            else if (!chkCardExists(tTmpCardActInfo))
            {
                // 校验卡清单中提供信息是否完整
                tTmpCardActInfo
                        .setDealFlag(CertifyContConst.CA_ACT_INFO_ERR_CARDNONOTEXISTS);
                tTmpCardActInfo.setDealDate(mCurDate);

                tTmpCardActInfo.setOperator(mGlobalInput.Operator);
                tTmpCardActInfo.setModifyDate(mCurDate);
                tTmpCardActInfo.setModifyTime(mCurTime);
            }

            tCardListSet.add(tTmpCardActInfo);
        }

        tTmpMap.put(tCardListSet, SysConst.UPDATE);

        return tTmpMap;
    }

    /**
     * 校验该卡是否为旧卡。
     * <br />旧卡判断条件：卡对应单证类型在单证险种描述中，未绑定套餐的。
     * @return 
     */
    private boolean chkOldCard(LICardActiveInfoListSchema cCardInfo)
    {
        String tCardNo = cCardInfo.getCardNo();

        String tStrSql = " select 1 from LICardActiveInfoList licail "
                + " inner join LZCardNumber lzcn on lzcn.CardNo = licail.CardNo "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " inner join LMCardRisk lmcr on lmcr.CertifyCode = lzc.CertifyCode "
                + " where 1 = 1 and lmcr.RiskType = 'W' "
                + " and lzcn.CardNo = '" + tCardNo + "' ";

        ExeSQL tExeSQL = new ExeSQL();
        String tResult = tExeSQL.getOneValue(tStrSql);
        if (!"1".equals(tResult))
        {
            System.out.println("该卡为旧卡单类型。");
            return false;
        }

        return true;
    }

    /**
     * 校验清单中，卡必要激活信息是否完整。
     * @return
     */
    private boolean chkCardBaseInfo(LICardActiveInfoListSchema cCardInfo)
    {
        try
        {
            if (!CheckCardInfoField(cCardInfo))
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }

        return true;
    }

    private boolean CheckCardInfoField(LICardActiveInfoListSchema cCardInfo)
            throws Exception
    {
        String chkFlag = "KZIMPCHK";
        String tRiskCode = "000000";

        if (cCardInfo == null)
        {
            System.out.println("传入卡信息为空。");
            cCardInfo = new LICardActiveInfoListSchema();
        }

        Calculator tCalculator = new Calculator();
        for (int i = 1; i <= cCardInfo.getFieldCount(); i++)
        {
            tCalculator.addBasicFactor(cCardInfo.getFieldName(i), cCardInfo
                    .getV(i));
        }

        // 获取待校验字段描述
        CachedRiskInfo cri = CachedRiskInfo.getInstance();
        LMCheckFieldSet tLMCheckFieldSet = null;
        tLMCheckFieldSet = cri
                .findCheckFieldByRiskCodeClone(tRiskCode, chkFlag);

        if (tLMCheckFieldSet == null)
        {
            buildError("CheckSchemaField", "查询CheckSchemaField描述失败！");
            throw new Exception("查询CheckSchemaField描述失败！");
        }

        for (int i = 1; i <= tLMCheckFieldSet.size(); i++)
        {
            LMCheckFieldSchema tChkFieldSchema = tLMCheckFieldSet.get(i);

            tCalculator.setCalCode(tChkFieldSchema.getCalCode());
            String RSTR = tCalculator.calculate();

            if (RSTR == null || RSTR.equals(""))
            {
                buildError("CheckSchemaField", "字段校验sql结果出错！");
                throw new Exception("字段校验sql结果出错！");
            }

            if (RSTR.equals("1"))
            {
                return false;
            }
            else if (RSTR.equals("0"))
            {
                continue;
            }
        }

        return true;
    }

    /**
     * 判断该卡是否已经进行卡折导入系统
     * @param cCardInfo
     * @return
     */
    private boolean chkCardExists(LICardActiveInfoListSchema cCardInfo)
    {
        String tCardNo = cCardInfo.getCardNo();

        String tStrSql = " select 1 from LICertify lict "
                + " where lict.CardNo = '" + tCardNo + "' ";

        ExeSQL tExeSQL = new ExeSQL();
        String tResult = tExeSQL.getOneValue(tStrSql);
        if (!"1".equals(tResult))
        {
            System.out.println("该卡尚未导入新卡折系统。");
            return false;
        }
        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
