/**
 * Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sinosoft.lis.certifybusiness;

public interface Service extends javax.xml.rpc.Service {
    public java.lang.String getServiceSoapAddress();

    public com.sinosoft.lis.certifybusiness.ServiceSoap getServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.sinosoft.lis.certifybusiness.ServiceSoap getServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getServiceSoap12Address();

//    public com.sinosoft.lis.certifybusiness.ServiceSoap getServiceSoap12() throws javax.xml.rpc.ServiceException;
//
//    public com.sinosoft.lis.certifybusiness.ServiceSoap getServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
