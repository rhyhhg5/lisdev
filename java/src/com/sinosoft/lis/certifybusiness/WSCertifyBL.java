/**
 * 2009-5-5
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class WSCertifyBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    /** 卡折主信息 */
    private LICertifySchema mCertifyInfo = null;

    /** 卡折被保人信息 */
    private LICertifyInsuredSet mCertifyInsuInfoList = null;

    public WSCertifyBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCertifyInfo = (LICertifySchema) cInputData.getObjectByObjectName("LICertifySchema", 0);
        if (mCertifyInfo == null)
        {
            buildError("getInputData", "未获取到卡折信息。");
            return false;
        }

        //        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        //        if (mPrtNo == null || mPrtNo.equals(""))
        //        {
        //            buildError("getInputData", "获取结算单号失败。");
        //            return false;
        //        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("WSCertify".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = createWSCont();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    /**
     * 创建实名化分单数据。
     * @return
     */
    private MMap createWSCont()
    {
        MMap tMMap = new MMap();

        // 加载卡折相关信息。
        if (!loadCertifyAllInfo())
        {
            return null;
        }
        // --------------------

        // 在卡折结算单下创建分单
        VData tInputData = new VData();
        TransferData tTransferData = new TransferData();
        tInputData.add(mGlobalInput);
        tInputData.add(tTransferData);
        tInputData.add(mCertifyInfo);
        tInputData.add(mCertifyInsuInfoList);
        CertContBL tCertContBL = new CertContBL();
        if (!tCertContBL.submitData(tInputData, "Create"))
        {
            this.mErrors.copyAllErrors(tCertContBL.mErrors);
            System.out.println("CertContBL处理时出现异常：" + mErrors.getFirstError());
            return null;
        }
        // --------------------

        return tMMap;
    }

    /**
     * 加载卡折相关信息，并将信息保存到全局对象中。
     * @return
     */
    private boolean loadCertifyAllInfo()
    {
        String tCardNo = mCertifyInfo.getCardNo();

        // 校验卡号是否为空
        if (tCardNo == null || tCardNo.equals(""))
        {
            buildError("loadCertifyAllInfo", "保险卡（折）号为空。");
            return false;
        }
        // --------------------

        // 装载卡折承保信息
        if (!loadCertifyMainInfo(tCardNo))
        {
            return false;
        }
        // --------------------

        // 装载卡折被保人信息
        if (!loadCertifyInsuInfo(tCardNo))
        {
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 根据保险卡（折）号获取卡相关保障信息，并保存到全局对象中。
     * @param cCardNo 保险卡（折）号
     * @return
     */
    private boolean loadCertifyMainInfo(String cCardNo)
    {
        String tCardNo = cCardNo;

        // 校验保险卡（折）主信息是否存在
        LICertifyDB tLICertifyDB = new LICertifyDB();
        tLICertifyDB.setCardNo(tCardNo);
        if (!tLICertifyDB.getInfo())
        {
            buildError("loadCertifyMainInfo", "保险卡（折）不存在。");
            return false;
        }
        // --------------------

        mCertifyInfo.setSchema(tLICertifyDB.getSchema());

        // 校验保险卡是否已经成功结算
        if (!CertifyContConst.CC_SETTLEMENT_CONFIRM.equals(mCertifyInfo.getState()))
        {
            buildError("loadCertifyMainInfo", "[" + tCardNo + "]保险卡尚未成功结算。");
            return false;
        }
        // --------------------

        // 校验保险卡是否已经成功激活
        if (!CertifyContConst.CC_ACTIVEFLAG_ACTIVE.equals(mCertifyInfo.getActiveFlag())
                && !CertifyContConst.CC_ACTIVEFLAG_WEBACTIVE.equals(mCertifyInfo.getActiveFlag()))
        {
            buildError("loadCertifyMainInfo", "[" + tCardNo + "]保险卡尚未成功激活。");
            return false;
        }
        // --------------------

        // 校验保险卡是否已经被实名化过
        if (!CertifyContConst.CC_WSSTATE_UNCREATED.equals(mCertifyInfo.getWSState()))
        {
            buildError("loadCertifyMainInfo", "[" + tCardNo + "]保险卡已经被实名化。");
            return false;
        }
        // --------------------

        // 校验保险卡是否含有生效日期
        if (mCertifyInfo.getCValidate() == null || mCertifyInfo.getCValidate().equals(""))
        {
            buildError("loadCertifyMainInfo", "[" + tCardNo + "]保险卡清单缺少相关生效日期。");
            return false;
        }
        // --------------------

        // 校验是否退保
        if (!"01".equals(mCertifyInfo.getCertifyStatus()))
        {
            buildError("loadCertifyMainInfo", "[" + tCardNo + "]保险卡已经退保或作废，不能进行实名化。");
            return false;
        }
        String tStrSql = "select 1 from LIBCertify where CardNo = '" + mCertifyInfo.getCardNo() + "'";
        if ("1".equals(new ExeSQL().getOneValue(tStrSql)))
        {
            buildError("loadCertifyMainInfo", "[" + tCardNo + "]保险卡已经退保或作废，不能进行实名化。");
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 根据卡号获取保险卡（折）被保人信息，并保存到全局对象中。
     * @param cCardNo 保险卡（折）号
     * @return
     */
    private boolean loadCertifyInsuInfo(String cCardNo)
    {
        String tCardNo = cCardNo;

        // 校验被保人信息是否有且唯一
        String tStrSql = "select * from LICertifyInsured where CardNo = '" + tCardNo + "'";
        LICertifyInsuredDB tLICertifyInsuredDB = new LICertifyInsuredDB();
        LICertifyInsuredSet tLICertifyInsuredSet = tLICertifyInsuredDB.executeQuery(tStrSql);
        if (tLICertifyInsuredSet == null || tLICertifyInsuredSet.size() == 0)
        {
            buildError("loadCertifyInsuInfo", "[" + tCardNo + "]保险卡被保人信息缺失。");
            return false;
        }
        // --------------------

        mCertifyInsuInfoList = new LICertifyInsuredSet();
        for (int i = 1; i <= tLICertifyInsuredSet.size(); i++)
        {
            LICertifyInsuredSchema tCertInsuInfo = tLICertifyInsuredSet.get(i).getSchema();

            // 判断被保人五要素信息是否齐全
            if (tCertInsuInfo.getName() == null || tCertInsuInfo.getName().equals(""))
            {
                buildError("loadCertifyInsuInfo", "[" + tCardNo + "]保险卡被保人姓名信息缺失。");
                return false;
            }
            if (tCertInsuInfo.getSex() == null || tCertInsuInfo.getSex().equals(""))
            {
                buildError("loadCertifyInsuInfo", "[" + tCardNo + "]保险卡被保人性别信息缺失。");
                return false;
            }
            if (tCertInsuInfo.getBirthday() == null || tCertInsuInfo.getBirthday().equals(""))
            {
                buildError("loadCertifyInsuInfo", "[" + tCardNo + "]保险卡被保人出生日期信息缺失。");
                return false;
            }
            if (tCertInsuInfo.getIdType() == null || tCertInsuInfo.getIdType().equals(""))
            {
                buildError("loadCertifyInsuInfo", "[" + tCardNo + "]保险卡被保人证件类别信息缺失。");
                return false;
            }
            if (tCertInsuInfo.getIdNo() == null || tCertInsuInfo.getIdNo().equals(""))
            {
                buildError("loadCertifyInsuInfo", "[" + tCardNo + "]保险卡被保人证件号码信息缺失。");
                return false;
            }

            mCertifyInsuInfoList.add(tCertInsuInfo);
            // --------------------
        }

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
