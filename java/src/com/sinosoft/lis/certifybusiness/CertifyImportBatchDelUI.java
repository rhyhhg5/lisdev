/**
 * 2008-11-12
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertifyImportBatchDelUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public CertifyImportBatchDelUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	CertifyImportBatchDelBL tCertifyImportBatchDelBL = new CertifyImportBatchDelBL();
            if (!tCertifyImportBatchDelBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCertifyImportBatchDelBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
