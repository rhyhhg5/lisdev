/**
 * UploadXmlDataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sinosoft.lis.certifybusiness;

public class UploadXmlDataResponse  implements java.io.Serializable {
    private com.sinosoft.lis.certifybusiness.UploadXmlDataResponseResult uploadXmlDataResult;

    public UploadXmlDataResponse() {
    }

    public UploadXmlDataResponse(
           com.sinosoft.lis.certifybusiness.UploadXmlDataResponseResult uploadXmlDataResult) {
           this.uploadXmlDataResult = uploadXmlDataResult;
    }


    /**
     * Gets the uploadXmlDataResult value for this UploadXmlDataResponse.
     * 
     * @return uploadXmlDataResult
     */
    public com.sinosoft.lis.certifybusiness.UploadXmlDataResponseResult getUploadXmlDataResult() {
        return uploadXmlDataResult;
    }


    /**
     * Sets the uploadXmlDataResult value for this UploadXmlDataResponse.
     * 
     * @param uploadXmlDataResult
     */
    public void setUploadXmlDataResult(com.sinosoft.lis.certifybusiness.UploadXmlDataResponseResult uploadXmlDataResult) {
        this.uploadXmlDataResult = uploadXmlDataResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UploadXmlDataResponse)) return false;
        UploadXmlDataResponse other = (UploadXmlDataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uploadXmlDataResult==null && other.getUploadXmlDataResult()==null) || 
             (this.uploadXmlDataResult!=null &&
              this.uploadXmlDataResult.equals(other.getUploadXmlDataResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUploadXmlDataResult() != null) {
            _hashCode += getUploadXmlDataResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UploadXmlDataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://121.52.210.101/", ">UploadXmlDataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uploadXmlDataResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://121.52.210.101/", "UploadXmlDataResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://121.52.210.101/", ">>UploadXmlDataResponse>UploadXmlDataResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
