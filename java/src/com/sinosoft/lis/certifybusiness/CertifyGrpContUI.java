/**
 * 2008-11-19
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertifyGrpContUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public CertifyGrpContUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            CertifyGrpContBL tCertifyGrpContBL = new CertifyGrpContBL();
            if (!tCertifyGrpContBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCertifyGrpContBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
