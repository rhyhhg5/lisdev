/**
 * created 2009-1-22
 * by LY
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CardActiveXlsImportUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public CardActiveXlsImportUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            CardActiveXlsImportBL tCardActiveXlsImportBL = new CardActiveXlsImportBL();
            if (!tCardActiveXlsImportBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCardActiveXlsImportBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
