/**
 * 2008-11-18
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.db.LIBCertifyDB;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LIBCertifySchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.vschema.LIBCertifySet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class SettlementCertifyContListBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private LICertifySet mLICertifySet = null;

    private LIBCertifySet mLIBCertifySet = null;

    private String mPrtNo = null;

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mLICertifySet = (LICertifySet) cInputData.getObjectByObjectName(
                "LICertifySet", 0);
        if (mLICertifySet == null || mLICertifySet.size() <= 0)
        {
            buildError("getInputData", "所需参数不完整，不存在单证清单。");
            return false;
        }

        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        if (mPrtNo == null || mPrtNo.equals(""))
        {
            buildError("getInputData", "未取到结算单号。");
            return false;
        }

        if (!loadCertifyContInfo())
        {
            buildError("getInputData", "装载单证信息失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Insert".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = addCertifyContList();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        if ("Delete".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = delCertifyContList();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    /**
     * 对单证清单信息，增加归属结算单号。
     * @return
     */
    private MMap addCertifyContList()
    {
        MMap tMMap = new MMap();

        for (int i = 1; i <= mLICertifySet.size(); i++)
        {
            LICertifySchema tLICertifySchema = null;
            tLICertifySchema = mLICertifySet.get(i);

            tLICertifySchema.setPrtNo(mPrtNo);

            tLICertifySchema.setState(CertifyContConst.CC_CASHING_UP);

            tLICertifySchema.setOperator(mGlobalInput.Operator);
            tLICertifySchema.setModifyDate(PubFun.getCurrentDate());
            tLICertifySchema.setModifyTime(PubFun.getCurrentTime());

            tMMap.put(tLICertifySchema, SysConst.UPDATE);

            // 同步LICertifyInsured-PrtNo
            String tStrCCIUpdateSql = " update LICertifyInsured "
                    + " set PrtNo = '" + mPrtNo + "', " + " ModifyDate = '"
                    + PubFun.getCurrentDate() + "', " + " ModifyTime = '"
                    + PubFun.getCurrentTime() + "' " + " where CardNo = '"
                    + tLICertifySchema.getCardNo() + "' ";
            tMMap.put(tStrCCIUpdateSql, SysConst.UPDATE);
            // ------------------------------------------
        }

        // 退保单证处理
        for (int i = 1; i <= mLIBCertifySet.size(); i++)
        {
            LIBCertifySchema tLIBCertifySchema = null;
            tLIBCertifySchema = mLIBCertifySet.get(i);

            tLIBCertifySchema.setPrtNo(mPrtNo);

            tLIBCertifySchema.setState(CertifyContConst.CC_CASHING_UP);

            tLIBCertifySchema.setOperator(mGlobalInput.Operator);
            tLIBCertifySchema.setModifyDate(PubFun.getCurrentDate());
            tLIBCertifySchema.setModifyTime(PubFun.getCurrentTime());

            tMMap.put(tLIBCertifySchema, SysConst.UPDATE);

        }
        // --------------------

        return tMMap;
    }

    /**
     * 对单证清单信息，取消原归属结算单号。
     * @return
     */
    private MMap delCertifyContList()
    {
        MMap tMMap = new MMap();

        for (int i = 1; i <= mLICertifySet.size(); i++)
        {
            LICertifySchema tLICertifySchema = null;
            tLICertifySchema = mLICertifySet.get(i);

            tLICertifySchema.setPrtNo(null);

            tLICertifySchema.setState(CertifyContConst.CC_IMPORT_CONFIRM);

            tLICertifySchema.setOperator(mGlobalInput.Operator);
            tLICertifySchema.setModifyDate(PubFun.getCurrentDate());
            tLICertifySchema.setModifyTime(PubFun.getCurrentTime());

            tMMap.put(tLICertifySchema, SysConst.UPDATE);

            // 同步LICertifyInsured-PrtNo
            String tStrCCIUpdateSql = " update LICertifyInsured "
                    + " set PrtNo = null, " + " ModifyDate = '"
                    + PubFun.getCurrentDate() + "', " + " ModifyTime = '"
                    + PubFun.getCurrentTime() + "' " + " where CardNo = '"
                    + tLICertifySchema.getCardNo() + "' ";
            tMMap.put(tStrCCIUpdateSql, SysConst.UPDATE);
            // ------------------------------------------
        }

        for (int i = 1; i <= mLIBCertifySet.size(); i++)
        {
            LIBCertifySchema tLIBCertifySchema = null;
            tLIBCertifySchema = mLIBCertifySet.get(i);

            tLIBCertifySchema.setPrtNo(null);

            tLIBCertifySchema.setState(CertifyContConst.CC_IMPORT_CONFIRM);

            tLIBCertifySchema.setOperator(mGlobalInput.Operator);
            tLIBCertifySchema.setModifyDate(PubFun.getCurrentDate());
            tLIBCertifySchema.setModifyTime(PubFun.getCurrentTime());

            tMMap.put(tLIBCertifySchema, SysConst.UPDATE);
        }

        return tMMap;
    }

    /**
     * 根据单证号，从数据库中装载对应完整单证保单信息数据。
     * <br />并同时更新mLICertifySet中单证数据。
     * @return 如所含单证信息全部装载成功，返回true；否则，返回false
     */
    private boolean loadCertifyContInfo()
    {
        LICertifySet tLICertifySet = new LICertifySet();
        LIBCertifySet tLIBCertifySet = new LIBCertifySet();

        for (int i = 1; i <= mLICertifySet.size(); i++)
        {
            LICertifySchema tTmpCertifyInfo = null;
            tTmpCertifyInfo = mLICertifySet.get(i);
            String tCertStatus = tTmpCertifyInfo.getCertifyStatus();

            if ("03".equals(tCertStatus))
            {
                LIBCertifyDB tTmpWTCode = new LIBCertifyDB();
                tTmpWTCode.setCardNo(tTmpCertifyInfo.getCardNo());

                if (!tTmpWTCode.getInfo())
                {
                    String tStrErr = "单证[" + tTmpCertifyInfo.getCardNo()
                            + "]数据加载失败。";
                    buildError("loadCertifyContInfo", tStrErr);
                    System.out.println(tStrErr);
                    return false;
                }
                tLIBCertifySet.add(tTmpWTCode.getSchema());
            }
            else
            {
                LICertifyDB tLICertifyDB = new LICertifyDB();
                tLICertifyDB.setCardNo(tTmpCertifyInfo.getCardNo());

                if (!tLICertifyDB.getInfo())
                {
                    String tStrErr = "单证[" + tTmpCertifyInfo.getCardNo()
                            + "]数据加载失败。";
                    buildError("loadCertifyContInfo", tStrErr);
                    System.out.println(tStrErr);
                    return false;
                }
                tLICertifySet.add(tLICertifyDB.getSchema());
            }
        }

        mLICertifySet = tLICertifySet;
        mLIBCertifySet = tLIBCertifySet;

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
