/**
 * 2008-12-1
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertifyStateTakeBackUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public CertifyStateTakeBackUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            CertifyStateTakeBackBL tCertifyStateTakeBackBL = new CertifyStateTakeBackBL();
            if (!tCertifyStateTakeBackBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCertifyStateTakeBackBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
