/**
 * 2010-10-11
 */
package com.sinosoft.lis.certifybusiness;

import java.math.BigDecimal;

import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertActSyncBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mCurDate = PubFun.getCurrentDate();

    private String mCurTime = PubFun.getCurrentTime();

    /** 导入批次号 */
    private String mBatchNo = null;

    /** 单证号 */
    private String mCardNo = null;

    /** 单证主表信息 */
    private LICertifySchema mCertInfo = null;

    /** 单证被保人信息清单 */
    private LICertifyInsuredSet mCertInsuList = null;

    /** 单证激活（或承保）信息清单 */
    private LICardActiveInfoListSet mCardActInfoList = null;

    public CertActSyncBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCardNo = (String) mTransferData.getValueByName("CardNo");
        if (mCardNo == null || mCardNo.equals(""))
        {
            buildError("getInputData", "获取批次信息失败。");
            return false;
        }

        // 如未指定批次，则主动生成本次批次号
        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            mBatchNo = getBatchNo();
            if (mBatchNo == null || mBatchNo.equals(""))
            {
                buildError("getInputData", "获取导入批次号失败。");
                return false;
            }
        }
        // --------------------

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("ActSync".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = actCertInfoSync();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap actCertInfoSync()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 准备单证相关数据信息。
        if (!parseCertAllInfo(mCardNo))
        {
            return null;
        }
        // --------------------

        // 激活或承保数据同步
        tTmpMap = syncCertActInfo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    private boolean parseCertAllInfo(String cCardNo)
    {
        // 获取单证主表信息
        if (!loadCertInfo(cCardNo))
        {
            return false;
        }
        // --------------------

        // 获取单证被保人信息
        if (!loadCertInsuInfo(cCardNo))
        {
            return false;
        }
        // --------------------

        // 获取单证激活（或承保）数据信息
        if (!loadCertActInfoList(cCardNo))
        {
            return false;
        }
        // --------------------

        // 校验单证信息是否有效
        if (!chkCertAllInfo())
        {
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 激活或承保数据同步
     * @return
     */
    private MMap syncCertActInfo()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        if (mCertInsuList == null)
        {
            mCertInsuList = new LICertifyInsuredSet();
        }

        for (int i = 1; i <= mCardActInfoList.size(); i++)
        {
            LICardActiveInfoListSchema tTmpActCertInfo = mCardActInfoList.get(i);

            // 处理单证主表信息
            if (i == 1)
            {
                if (!dealActCertInfo(tTmpActCertInfo))
                {
                    return null;
                }

            }
            // --------------------

            // 处理单证被保人信息
            if (tTmpActCertInfo.getName() == null || tTmpActCertInfo.getName().equals("") // 姓名 
                    || tTmpActCertInfo.getSex() == null || tTmpActCertInfo.getSex().equals("") // 性别
                    || tTmpActCertInfo.getBirthday() == null || tTmpActCertInfo.getBirthday().equals("") // 生日
                    || tTmpActCertInfo.getIdType() == null || tTmpActCertInfo.getIdType().equals("") // 证件类别
                    || tTmpActCertInfo.getIdNo() == null || tTmpActCertInfo.getIdNo().equals("") // 证件号
            )
            {
                String tStrErr = "被保人姓名、性别、出生日期、证件类型、证件号为必填项。";
                buildError("dealActCertInsuInfo", tStrErr);
                return null;
            }

            LICertifyInsuredSchema tCertInsuInfo = new LICertifyInsuredSchema();

            tCertInsuInfo.setBatchNo(mBatchNo);
            tCertInsuInfo.setCardNo(mCertInfo.getCardNo());
            tCertInsuInfo.setSequenceNo(String.valueOf(i));

            tCertInsuInfo.setGrpContNo(mCertInfo.getGrpContNo());
            tCertInsuInfo.setProposalGrpContNo(mCertInfo.getProposalContNo());
            tCertInsuInfo.setPrtNo(mCertInfo.getPrtNo());

            tCertInsuInfo.setName(tTmpActCertInfo.getName());
            tCertInsuInfo.setSex(tTmpActCertInfo.getSex());
            tCertInsuInfo.setBirthday(tTmpActCertInfo.getBirthday());
            tCertInsuInfo.setIdType(tTmpActCertInfo.getIdType());
            tCertInsuInfo.setIdNo(tTmpActCertInfo.getIdNo());

            tCertInsuInfo.setOccupationCode(tTmpActCertInfo.getOccupationCode());
            tCertInsuInfo.setOccupationType(tTmpActCertInfo.getOccupationType());
            tCertInsuInfo.setPostalAddress(tTmpActCertInfo.getPostalAddress());
            tCertInsuInfo.setZipCode(tTmpActCertInfo.getZipCode());
            tCertInsuInfo.setPhone(tTmpActCertInfo.getPhone());
            tCertInsuInfo.setMobile(tTmpActCertInfo.getMobile());
            tCertInsuInfo.setGrpName(tTmpActCertInfo.getGrpName());
            tCertInsuInfo.setCompanyPhone(tTmpActCertInfo.getCompanyPhone());
            tCertInsuInfo.setEMail(tTmpActCertInfo.getEMail());

            tCertInsuInfo.setTicketNo(tTmpActCertInfo.getTicketNo());
            tCertInsuInfo.setTeamNo(tTmpActCertInfo.getTeamNo());
            tCertInsuInfo.setStartTeam(tTmpActCertInfo.getFrom());
            tCertInsuInfo.setEndTeam(tTmpActCertInfo.getTo());
            tCertInsuInfo.setSeatNo(tTmpActCertInfo.getSeatNo());

            tCertInsuInfo.setRemark(tTmpActCertInfo.getRemark());

            tCertInsuInfo.setState(CertifyContConst.CI_IMPORT_FIRST);

            tCertInsuInfo.setOperator(mGlobalInput.Operator);
            tCertInsuInfo.setMakeDate(mCurDate);
            tCertInsuInfo.setMakeTime(mCurTime);
            tCertInsuInfo.setModifyDate(mCurDate);
            tCertInsuInfo.setModifyTime(mCurTime);

            mCertInsuList.add(tCertInsuInfo);
            // --------------------
        }

        tMMap.put(mCertInsuList, SysConst.INSERT);
        tMMap.put(mCertInfo, SysConst.UPDATE);

        // 回置单证激活状态。
        tTmpMap = null;
        tTmpMap = dealCardSysActive(mCertInfo.getCardNo());
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 回置中间表单证处理状态
        tTmpMap = null;
        tTmpMap = dealCardListActive(mCertInfo.getCardNo());
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    /**
     * 处理单证主表信息
     * @param cCardInfo
     * @return
     */
    private boolean dealActCertInfo(LICardActiveInfoListSchema cCardInfo)
    {
        mCertInfo.setCValidate(cCardInfo.getCValidate());

        mCertInfo.setPrem(cCardInfo.getPrem());
        mCertInfo.setAmnt(cCardInfo.getAmnt());
        mCertInfo.setMult(cCardInfo.getMult());
        mCertInfo.setCopys(cCardInfo.getCopys());

        // 处理激活卡要素：保额、保费、档次、份数等。（主要为了重算保额）
        if (!dealCertifyWrapParams(mCertInfo))
        {
            return false;
        }
        // --------------------

        mCertInfo.setInsuPeoples(mCardActInfoList.size());

        if (cCardInfo.getCardStatus() != null && cCardInfo.getCardStatus().equals(""))
        {
            mCertInfo.setCertifyStatus(cCardInfo.getCardStatus());
        }

        mCertInfo.setActiveDate(cCardInfo.getActiveDate());
        mCertInfo.setActiveFlag(CertifyContConst.CC_ACTIVEFLAG_WEBACTIVE);

        mCertInfo.setOperator(mGlobalInput.Operator);
        mCertInfo.setModifyDate(mCurDate);
        mCertInfo.setModifyTime(mCurTime);

        return true;
    }

    /**
     * 获取单证主表信息
     * @param cCardNo
     * @return
     */
    private boolean loadCertInfo(String cCardNo)
    {
        LICertifyDB tLICertifyDB = new LICertifyDB();
        tLICertifyDB.setCardNo(cCardNo);
        if (!tLICertifyDB.getInfo())
        {
            buildError("loadCertInfo", "系统中未找到[" + cCardNo + "]单证主信息。");
            return false;
        }

        mCertInfo = tLICertifyDB.getSchema();

        return true;
    }

    /**
     * 获取单证被保人信息
     * @param cCardNo
     * @return
     */
    private boolean loadCertInsuInfo(String cCardNo)
    {
        String tStrSql = "select * from LICertifyInsured where CardNo = '" + cCardNo + "'";

        LICertifyInsuredDB tLICertifyInsuredDB = new LICertifyInsuredDB();
        mCertInsuList = tLICertifyInsuredDB.executeQuery(tStrSql);

        return true;
    }

    /**
     * 获取单证激活（或承保）数据信息
     * @param cCardNo
     * @return
     */
    private boolean loadCertActInfoList(String cCardNo)
    {
        String tStrSql = "select * from LICardActiveInfoList where CardNo = '" + cCardNo + "'";

        LICardActiveInfoListDB tCardActInfoDB = new LICardActiveInfoListDB();
        mCardActInfoList = tCardActInfoDB.executeQuery(tStrSql);

        return true;
    }

    /**
     * 校验单证信息是否有效
     * @return
     */
    private boolean chkCertAllInfo()
    {
        // 校验单证信息
        if (!chkCertInfo())
        {
            return false;
        }
        // --------------------

        // 校验第三方激活或承保信息
        if (!chkCertActInfo())
        {
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 校验单证数据合法性
     * @return
     */
    private boolean chkCertInfo()
    {
        // 必须有且仅有一个单证主表信息。
        if (mCertInfo == null)
        {
            String tErrInfo = "单证号[" + mCertInfo.getCardNo() + "]尚未导入。";
            buildError("chkCertAllInfo", tErrInfo);
            return false;
        }
        // --------------------

        // 不能存在被保人信息
        if (mCertInsuList != null && mCertInsuList.size() != 0)
        {
            String tErrInfo = "单证号[" + mCertInfo.getCardNo() + "]已存在被保人信息。";
            buildError("chkCertAllInfo", tErrInfo);
            return false;
        }
        // --------------------

        // 单证必须未激活状态
        if (!CertifyContConst.CC_ACTIVEFLAG_INACTIVE.equals(mCertInfo.getActiveFlag()))
        {
            String tErrInfo = "单证号[" + mCertInfo.getCardNo() + "]已经被激活。";
            buildError("chkCertAllInfo", tErrInfo);
            return false;
        }
        // --------------------

        // 单证必须已经导入核心并进行导入确认
        String tCardState = mCertInfo.getState();
        if (tCardState == null || tCardState.equals(CertifyContConst.CC_IMPORT_FIRST))
        {
            String tErrInfo = "单证号[" + mCertInfo.getCardNo() + "]尚未进行导入确认。";
            buildError("chkCertAllInfo", tErrInfo);
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 校验第三方激活或承保信息
     * @return
     */
    private boolean chkCertActInfo()
    {
        // 必须存在激活或承保信息
        if (mCardActInfoList == null || mCardActInfoList.size() == 0)
        {
            String tErrInfo = "单证号[" + mCertInfo.getCardNo() + "]不存在第三方激活或承保信息。";
            buildError("chkCertAllInfo", tErrInfo);
            return false;
        }
        // --------------------

        for (int i = 1; i <= mCardActInfoList.size(); i++)
        {
            LICardActiveInfoListSchema tCardActInfo = mCardActInfoList.get(i);

            // 激活日期不能为空。
            if (tCardActInfo.getActiveDate() == null || tCardActInfo.getActiveDate().equals(""))
            {
                String tErrInfo = "单证号[" + mCertInfo.getCardNo() + "]承保/激活信息中不存在激活日期。";
                buildError("chkCertAllInfo", tErrInfo);
                return false;
            }
            // --------------------

            // 生效日期不能为空。
            if (tCardActInfo.getCValidate() == null || tCardActInfo.getCValidate().equals(""))
            {
                String tErrInfo = "单证号[" + mCertInfo.getCardNo() + "]承保/激活信息中不存在生效日期。";
                buildError("chkCertAllInfo", tErrInfo);
                return false;
            }
            // --------------------
        }

        // 校验信息是否合法
        if (!chkCardInfoDatas(mCardActInfoList))
        {
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 处理单证关键要素（保额、保费、档次、份数等）。
     * @param cLICertifySchema
     * @return
     */
    private boolean dealCertifyWrapParams(LICertifySchema cLICertifySchema)
    {
        String tCardNo = cLICertifySchema.getCardNo();

        String tCertifyCode = cLICertifySchema.getCertifyCode();
        double tAmnt = cLICertifySchema.getAmnt();
        double tPrem = cLICertifySchema.getPrem();
        double tCopys = cLICertifySchema.getCopys();
        double tMult = cLICertifySchema.getMult();

        // 处理Copys要素。如果套餐中无Copys要素，清单中份数要素必须为1。
        if (cLICertifySchema.getCopys() == 0)
        {
            tCopys = 1;
            cLICertifySchema.setCopys(tCopys);
        }

        String tCopysFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Copys' and lmcr.CertifyCode = '" + tCertifyCode + "' ");
        if (!"1".equals(tCopysFlag) && tCopys != 1)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐份数不能大于1。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        // -------------------------------------------

        // 校验档次，如果套餐不需要档次要素时，清单表中档次要素（Mult）置为0
        String tMultFlag = new ExeSQL().getOneValue(" select 1 from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode "
                + " where ldrdw.CalFactor = 'Mult' and lmcr.CertifyCode = '" + tCertifyCode + "' ");
        if (!"1".equals(tMultFlag) && tMult != 0)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐不含有档次要素。";
            //            buildError("dealCertifyWrapParams", tStrErr);
            //            return false;

            System.out.println(tStrErr);
            cLICertifySchema.setMult(0);
        }
        // -------------------------------------------

        LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

        String tStrSql = " select ldrdw.* from LDRiskDutyWrap ldrdw "
                + " inner join LMCardRisk lmcr on lmcr.RiskCode = ldrdw.RiskWrapCode and lmcr.RiskType = 'W' "
                + " where lmcr.CertifyCode = '"
                + tCertifyCode
                + "' "
                + " order by ldrdw.RiskCode, ldrdw.DutyCode, ldrdw.CalFactorType, ldrdw.CalFactor, ldrdw.CalFactorValue ";
        tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
        {
            String tStrErr = "单证号[" + tCardNo + "]对应套餐要素信息未找到。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }

        double tSysWrapSumPrem = 0;
        double tSysWrapSumAmnt = 0;

        // 处理要素信息。
        for (int i = 1; i <= tLDRiskDutyWrapSet.size(); i++)
        {
            LDRiskDutyWrapSchema tRiskDutyParam = tLDRiskDutyWrapSet.get(i);

            String tCalFactorType = tRiskDutyParam.getCalFactorType();
            String tCalFactor = tRiskDutyParam.getCalFactor();
            String tCalFactorValue = tRiskDutyParam.getCalFactorValue();

            if ("Prem".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(tMult));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + tCardNo + "]保费计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        return false;
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumPrem = Arith.add(tSysWrapSumPrem, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保费出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }

            if ("Amnt".equals(tCalFactor))
            {
                String tTmpCalResult = null;

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = new PubCalculator();

                    tCal.addBasicFactor("RiskCode", tRiskDutyParam.getRiskCode());
                    tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                    tCal.addBasicFactor("Mult", String.valueOf(tMult));

                    /** 计算 */
                    tCal.setCalSql(tRiskDutyParam.getCalSql());
                    System.out.println("CalSql:" + tRiskDutyParam.getCalSql());

                    tTmpCalResult = tCal.calculate();

                    if (tTmpCalResult == null || tTmpCalResult.equals(""))
                    {
                        String tStrErr = "单证号[" + tCardNo + "]保额计算失败。";
                        buildError("dealCertifyWrapParams", tStrErr);
                        return false;
                        // tTmpCalResult = "0";
                    }
                }
                else
                {
                    tTmpCalResult = tCalFactorValue;
                }

                try
                {
                    tSysWrapSumAmnt = Arith.add(tSysWrapSumAmnt, new BigDecimal(tTmpCalResult).doubleValue());
                }
                catch (Exception e)
                {
                    String tStrErr = "套餐要素中保额出现非数值型字符串。";
                    buildError("dealCertifyWrapParams", tStrErr);
                    return false;
                }
            }
        }
        // ------------------------------------------------

        if (tPrem != 0 && tPrem != tSysWrapSumPrem)
        {
            String tStrErr = "单证号[" + tCardNo + "]系统计算出保费：" + tSysWrapSumPrem + "，与填写保费：" + tPrem + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        cLICertifySchema.setPrem(tSysWrapSumPrem);

        if (tAmnt != 0 && tAmnt != tSysWrapSumAmnt)
        {
            String tStrErr = "单证号[" + tCardNo + "]系统计算出保额：" + tSysWrapSumAmnt + "，与填写保额：" + tAmnt + "不一致。";
            buildError("dealCertifyWrapParams", tStrErr);
            return false;
        }
        cLICertifySchema.setAmnt(tSysWrapSumAmnt);

        return true;
    }

    /**
     * 处理单证系统中，卡激活状态。
     * @param cCardNo
     * @return
     */
    private MMap dealCardSysActive(String cCardNo)
    {
        MMap tMMap = new MMap();

        String tStrSql = " select lzc.* from LZCardNumber lzcn "
                + " inner join LZCard lzc on lzcn.CardType = lzc.SubCode and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                + " where lzcn.CardNo = '" + cCardNo + "' ";

        LZCardSet tLZCardSet = new LZCardDB().executeQuery(tStrSql);
        if (tLZCardSet == null || tLZCardSet.size() != 1)
        {
            String tStrErr = "获取卡信息数据失败。";
            buildError("dealCardSysActive", tStrErr);
            return null;
        }

        for (int i = 1; i <= tLZCardSet.size(); i++)
        {
            LZCardSchema tLZCardSchema = tLZCardSet.get(i).getSchema();

            tLZCardSchema.setActiveFlag("1");
            tLZCardSchema.setOperator(mGlobalInput.Operator);
            tLZCardSchema.setModifyDate(PubFun.getCurrentDate());
            tLZCardSchema.setModifyTime(PubFun.getCurrentTime());

            tMMap.put(tLZCardSchema, SysConst.UPDATE);
        }

        return tMMap;
    }

    private MMap dealCardListActive(String cCardNo)
    {
        MMap tMMap = new MMap();

        String tStrSql = " update LICardActiveInfoList set BatchNo = '" + mBatchNo + "', DealFlag = '"
                + CertifyContConst.CA_ACT_INFO_DEALED + "', DealDate = '" + mCurDate + "' where CardNo = '" + cCardNo
                + "' ";
        tMMap.put(tStrSql, SysConst.UPDATE);

        return tMMap;
    }

    /**
     * 检验待激活卡信息数据是否符合激活最基本要求
     * <br />将不符合的数据在中间表进行标志（DealFlag），暂不处理
     * @return
     */
    private boolean chkCardInfoDatas(LICardActiveInfoListSet cCardActInfoList)
    {
        // 调用卡单激活导入程序，进行批次导入
        VData tVData = new VData();

        TransferData tTransferData = new TransferData();

        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        tVData.add(cCardActInfoList);

        CardActImpChkBL tCardActImpChkBL = new CardActImpChkBL();

        if (!tCardActImpChkBL.submitData(tVData, "Check"))
        {
            String tStrErr = "校验卡折待激活信息数据处理时异常! 原因是: " + tCardActImpChkBL.mErrors.getLastError();
            buildError("chkCardInfoDatas", tStrErr);
            return false;
        }
        else if (tCardActImpChkBL.mErrors.needDealError())
        {
            String tStrErr = tCardActImpChkBL.mErrors.getErrContent();
            System.out.println(tStrErr);
        }
        // --------------------------------

        return true;
    }

    private String getBatchNo()
    {
        String tBatchNo = null;
        try
        {
            tBatchNo = PubFun1.CreateMaxNo("LICCARDBNO", mCurDate);
            System.out.println("BatchNo:" + tBatchNo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            String tStrErr = "导入批次号生成失败。";
            buildError("getBatchNo", tStrErr);
            return null;
        }

        return tBatchNo;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + ":" + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
