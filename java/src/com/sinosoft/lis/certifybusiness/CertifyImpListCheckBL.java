/**
 * 2009-2-6
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.CheckFieldCom;
import com.sinosoft.lis.pubfun.FieldCarrier;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICChkErrorSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LMCheckFieldSchema;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.vschema.LICChkErrorSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 批次清单校验
 * 
 * @author LY
 *
 */
public class CertifyImpListCheckBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mBatchNo = null;

    public CertifyImpListCheckBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "获取批次号失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Check".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = chkImpCertifyList();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap chkImpCertifyList()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        CertifyInfoColl tCertifyList = loadCertifyListByBatchNo(mBatchNo);

        if (tCertifyList == null || tCertifyList.size() == 0)
        {
            String tStrErr = "导入批次[" + mBatchNo + "]中，不存在卡折信息。";
            buildError("chkImpCertifyList", tStrErr);
            return null;
        }

        // 清除该批次历史检查信息轨迹
        tTmpMap = cleanChkHistoryInfo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // -------------------------

        for (int i = 0; i < tCertifyList.size(); i++)
        {
            LICertifySchema tLICertifySchema = tCertifyList
                    .getCertifyInfoByIndex(i);
            LICertifyInsuredSet tLICertifyInsuredSet = tCertifyList
                    .getCertifyInsuByIndex(i);

            for (int j = 1; j <= tLICertifyInsuredSet.size(); j++)
            {
                tTmpMap = CheckCTBField(tLICertifySchema, tLICertifyInsuredSet
                        .get(j), "INSERT");
                if (tTmpMap == null)
                {
                    return null;
                }
                tMMap.add(tTmpMap);
                tTmpMap = null;
            }
        }

        return tMMap;
    }

    /**
     * 获取指定导入批次中，卡折信息。
     * @param cBatchNo
     * @return
     */
    private CertifyInfoColl loadCertifyListByBatchNo(String cBatchNo)
    {
        String tStrSql = null;

        // 获取卡折信息。
        tStrSql = "select * from LICertify lic where lic.BatchNo = '"
                + cBatchNo + "'";
        LICertifySet tLICertifySet = new LICertifyDB().executeQuery(tStrSql);
        tStrSql = null;
        // -------------------------

        // 获取卡折被保人信息。
        tStrSql = " select lici.* from LICertifyInsured lici "
                + " inner join LICertify lic on lic.CardNo = lici.CardNo "
                + " where lic.BatchNo = '" + cBatchNo + "' ";
        LICertifyInsuredSet tLICertifyInsured = new LICertifyInsuredDB()
                .executeQuery(tStrSql);
        tStrSql = null;
        // -------------------------

        CertifyInfoColl tCertifyInfoColl = new CertifyInfoColl();
        if (!tCertifyInfoColl.loadCertifyInfo(tLICertifySet, tLICertifyInsured))
        {
            String tStrErr = "获取导入批次[" + cBatchNo + "]中，卡折清单信息数据失败。";
            buildError("loadCertifyListByBatchNo", tStrErr);
            return null;
        }

        return tCertifyInfoColl;
    }

    /**
     * 校验字段
     *
     * @return boolean
     * @param tLCPolSchema
     *            LCPolSchema
     * @param operType
     *            String
     */
    private MMap CheckCTBField(LICertifySchema cCerInfo,
            LICertifyInsuredSchema cCerInsuInfo, String operType)
    {
        MMap tMMap = new MMap();

        String chkFlag = "CTB";

        if (cCerInfo == null)
        {
            String tStrErr = "卡折主信息为空。";
            buildError("CheckCTBField", tStrErr);
            return null;
        }

        try
        {
            String tStrSql = " select distinct lmcr.RiskCode "
                    + " from LMCardRisk lmcr "
                    + " where lmcr.RiskType = 'W' and lmcr.CertifyCode = '"
                    + cCerInfo.getCertifyCode() + "' ";

            SSRS tResult = new ExeSQL().execSQL(tStrSql);

            if (tResult == null || tResult.MaxRow != 1)
            {
                String tStrErr = "未查询到该卡折险种信息，或查询到多条。";
                buildError("CheckCTBField", tStrErr);
                return null;
            }

            String tRiskWrapCode = tResult.GetText(1, 1);

            Calculator tCalculator = new Calculator();

            // 准备计算要素。            
            tCalculator.addBasicFactor("RiskWrapCode", tRiskWrapCode);

            tCalculator.addBasicFactor("CardNo", cCerInfo.getCardNo());
            tCalculator.addBasicFactor("CValidate", cCerInfo.getCValidate());

            String tCValidate = cCerInfo.getCValidate();
            String tBirthday = cCerInsuInfo.getBirthday();
            if (tCValidate == null || tCValidate.equals("")
                    || tBirthday == null || tBirthday.equals(""))
            {
                String tStrErr = "卡折生效日期或被保人出生日期为空。";
                buildError("CheckCTBField", tStrErr);
                return null;
            }
            int tIntervalYear = PubFun.calInterval(tBirthday, tCValidate, "Y");
            tCalculator.addBasicFactor("AppAge", String.valueOf(tIntervalYear));

            tCalculator.addBasicFactor("Prem", String.valueOf(cCerInfo
                    .getPrem()));
            tCalculator.addBasicFactor("Amnt", String.valueOf(cCerInfo
                    .getAmnt()));
            tCalculator.addBasicFactor("Mult", String.valueOf(cCerInfo
                    .getMult()));
            tCalculator.addBasicFactor("Copys", String.valueOf(cCerInfo
                    .getCopys()));

            tCalculator.addBasicFactor("InsuredName", cCerInsuInfo.getName());
            tCalculator.addBasicFactor("Sex", cCerInsuInfo.getSex());
            tCalculator.addBasicFactor("InsuredBirthday", cCerInsuInfo
                    .getBirthday());
            tCalculator.addBasicFactor("IDType", cCerInsuInfo.getIdType());
            tCalculator.addBasicFactor("IDNo", cCerInsuInfo.getIdNo());
            tCalculator.addBasicFactor("OccupationType", cCerInsuInfo
                    .getOccupationType());
            tCalculator.addBasicFactor("OccupationCode", cCerInsuInfo
                    .getOccupationCode());

            // 获取待校验字段描述
            CachedRiskInfo cri = CachedRiskInfo.getInstance();
            LMCheckFieldSet tLMCheckFieldSet = null;
            tLMCheckFieldSet = cri.findCheckFieldByRiskCodeClone(tRiskWrapCode,
                    chkFlag + operType);

            if (tLMCheckFieldSet == null)
            {
                // 如果不存在校验规则，缓存会返回0sizeSet集，而不会返回null。如果出现null，则视为有误。
                buildError("CheckCTBField", "查询CheckField描述失败！");
                return null;
            }

            LICChkErrorSet tLICChkErrorSet = new LICChkErrorSet(); // 用来记录出现的校验错误描述。

            for (int i = 1; i <= tLMCheckFieldSet.size(); i++)
            {
                LMCheckFieldSchema tChkFieldSchema = tLMCheckFieldSet.get(i);

                tCalculator.setCalCode(tChkFieldSchema.getCalCode());
                String RSTR = tCalculator.calculate();

                if (RSTR == null || RSTR.equals(""))
                {
                    buildError("CheckCTBField", "字段校验sql结果出错！");
                    return null;
                }

                if (RSTR.equals("1"))
                {
                    LICChkErrorSchema tLICChkErrorSchema = new LICChkErrorSchema();
                    tLICChkErrorSchema.setBatchNo(mBatchNo);
                    tLICChkErrorSchema.setCardNo(cCerInfo.getCardNo());

                    tLICChkErrorSchema.setSequenceNo(cCerInsuInfo
                            .getSequenceNo());
                    tLICChkErrorSchema.setName(cCerInsuInfo.getName());
                    tLICChkErrorSchema.setSex(cCerInsuInfo.getSex());
                    tLICChkErrorSchema.setBirthday(cCerInsuInfo.getBirthday());

                    tLICChkErrorSchema.setCheckRuleCode(tChkFieldSchema
                            .getCalCode());
                    tLICChkErrorSchema.setErrorInfo(tChkFieldSchema.getMsg());

                    tLICChkErrorSchema.setOperator(mGlobalInput.Operator);
                    tLICChkErrorSchema.setMakeDate(PubFun.getCurrentDate());
                    tLICChkErrorSchema.setMakeTime(PubFun.getCurrentTime());
                    tLICChkErrorSchema.setModifyDate(PubFun.getCurrentDate());
                    tLICChkErrorSchema.setModifyTime(PubFun.getCurrentTime());

                    tLICChkErrorSet.add(tLICChkErrorSchema);
                }
                else if (RSTR.equals("0"))
                {
                    continue;
                }
            }

            if (tLICChkErrorSet.size() > 0)
            {
                tMMap.put(tLICChkErrorSet, SysConst.INSERT);
            }

        }
        catch (Exception ex)
        {
            String str = "异常错误!" + ex.getMessage();
            buildError("CheckCTBField", str);
            return null;
        }

        return tMMap;
    }

    private MMap cleanChkHistoryInfo()
    {
        MMap tMMap = new MMap();
        String tStrSql = "delete from LICChkError where BatchNo = '" + mBatchNo
                + "'";
        tMMap.put(tStrSql, SysConst.DELETE);
        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
