/**
 * 2009-5-6
 */
package com.sinosoft.lis.certifybusiness;

import java.util.Date;
import java.util.HashMap;

import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LDRiskWrapDB;
import com.sinosoft.lis.db.LMDutyDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskDutyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCDutySchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDRiskWrapSchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LMRiskDutySchema;
import com.sinosoft.lis.tb.CalBL;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LMRiskDutySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CertContBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mCurDate = PubFun.getCurrentDate();

    private String mCurTime = PubFun.getCurrentTime();

    /** 卡折主信息 */
    private LICertifySchema mCertifyInfo = null;

    /** 卡折被保人信息 */
    private LICertifyInsuredSchema mCertifyInsuInfo = null;

    /** 卡折连带被保人信息清单 */
    private LICertifyInsuredSet mCertInsuRelaList = null;

    /** 结算团单合同信息 */
    private LCGrpContSchema mGrpContInfo = null;

    /** 结算团单险种信息 */
    private LCGrpPolSet mGrpPolInfo = null;

    /** 结算团单投保单位信息 */
    private LCGrpAppntSchema mGrpAppntInfo = null;

    /** 卡折套餐险种信息 */
    private LDRiskWrapSet mRiskWrapInfo = null;

    /** 卡折套餐险种责任要素信息 */
    private LDRiskDutyWrapSet mRiskDutyWrapInfo = null;

    private String mCertRiskWrapCode = null;

    // 分单数据
    private String mContNo = null;

    /** 合同信息 */
    private LCContSchema mContSchema = null;

    /** 被保人信息 */
    private LCInsuredSchema mInsuSchema = null;

    /** 被保人客户信息 */
    private LDPersonSchema mPersonSchema = null;

    /** 被保人客户地址信息 */
    private LCAddressSchema mInsuAddrSchema = null;

    /** 险种信息 */
    private LCPolSet mPolSet = null;

    /** 险种责任信息 */
    private LCDutySet mDutySet = null;

    /** 险种责任保费信息 */
    private LCPremSet mPremSet = null;

    /** 险种给付责任信息 */
    private LCGetSet mGetSet = null;
    
    /**卡折授权信息新增**/
    private LDPersonSchema aNewLDPersonSchema = null;
    // --------------------

    public CertContBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        // 将处理后的schema放入map中
        if (!prepareOutputData())
        {
            return null;
        }
        // --------------------

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mCertifyInfo = (LICertifySchema) cInputData.getObjectByObjectName("LICertifySchema", 0);
        if (mCertifyInfo == null)
        {
            buildError("getInputData", "未获取到卡折承保信息。");
            return false;
        }

        LICertifyInsuredSet tCertInsuList = (LICertifyInsuredSet) cInputData.getObjectByObjectName(
                "LICertifyInsuredSet", 0);
        if (tCertInsuList == null)
        {
            buildError("getInputData", "未获取到卡折被保人信息。");
            return false;
        }
        for (int i = 1; i <= tCertInsuList.size(); i++)
        {
            LICertifyInsuredSchema tTmpCertInfo = tCertInsuList.get(i);
            if (i == 1)
            {
                mCertifyInsuInfo = tTmpCertInfo;
            }
            else
            {
                if (mCertInsuRelaList == null)
                    mCertInsuRelaList = new LICertifyInsuredSet();
                mCertInsuRelaList.add(tTmpCertInfo);
            }
        }

        return true;
    }

    private boolean checkData()
    {
        if (!chkCertInfo())
        {
            return false;
        }

        if (!chkCertInsuInfo())
        {
            return false;
        }

        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Create".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = createCertCont();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap createCertCont()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 获取险种信息
        if (!loadWrapAllInfo())
        {
            return null;
        }
        // --------------------

        // 获取结算单团体合同相关信息。
        if (!loadGrpContAllInfo())
        {
            //buildError("createCertCont", "获取结算团体合同信息失败。");
            return null;
        }
        // --------------------

        // 创建分单信息
        tTmpMap = null;
        tTmpMap = dealSubContData();
        if (tTmpMap == null)
        {
            buildError("createCertCont", "创建合同层基本信息失败。");
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    private boolean loadWrapAllInfo()
    {
        mCertRiskWrapCode = getCertWrapCode();
        // 获取套餐代码
        if (mCertRiskWrapCode == null)
        {
            return false;
        }
        // --------------------

        // 获取套餐险种
        if (!loadWrapRiskInfo(mCertRiskWrapCode))
        {
            return false;
        }
        // --------------------

        // 获取险种责任要素
        if (!loadWrapRiskDutyInfo(mCertRiskWrapCode))
        {
            return false;
        }
        // --------------------

        return true;
    }

    private String getCertWrapCode()
    {
        String tCertWrapCode = null;

        String tCertifyCode = mCertifyInfo.getCertifyCode();
        String tStrSql = " select distinct RiskCode from LMCardRisk where CertifyCode = '" + tCertifyCode
                + "' and RiskType = 'W' ";
        SSRS tRes = new ExeSQL().execSQL(tStrSql);
        if (tRes == null || tRes.getMaxRow() != 1)
        {
            buildError("getCertWrapCode", "[" + tCertifyCode + "]类型单证，未找到绑定套餐信息或对应套餐信息出现多条。");
            return null;
        }

        tCertWrapCode = tRes.GetText(1, 1);

        return tCertWrapCode;
    }

    /**
     * 获取套餐险种信息。
     * @param cRiskWrapCode
     * @return
     */
    private boolean loadWrapRiskInfo(String cRiskWrapCode)
    {
        String tStrSql = " select * from LDRiskWrap where RiskWrapCode = '" + cRiskWrapCode + "' ";
        LDRiskWrapDB tLDRiskWrapDB = new LDRiskWrapDB();
        mRiskWrapInfo = tLDRiskWrapDB.executeQuery(tStrSql);
        if (mRiskWrapInfo == null || mRiskWrapInfo.size() <= 0)
        {
            buildError("loadWrapRiskInfo", "险种信息获取失败。");
            return false;
        }

        return true;
    }

    /**
     * 获取险种责任要素。
     * @param cRiskWrapCode
     * @return
     */
    private boolean loadWrapRiskDutyInfo(String cRiskWrapCode)
    {
        String tStrSql = " select * from LDRiskDutyWrap where RiskWrapCode = '" + cRiskWrapCode + "' ";
        LDRiskDutyWrapDB tLDRiskDutyWrapDB = new LDRiskDutyWrapDB();
        mRiskDutyWrapInfo = tLDRiskDutyWrapDB.executeQuery(tStrSql);
        if (mRiskDutyWrapInfo == null || mRiskDutyWrapInfo.size() <= 0)
        {
            buildError("loadWrapRiskDutyInfo", "获取险种责任要素。");
            return false;
        }

        if (!dealWrapRiskDutyParam())
        {
            return false;
        }

        return true;
    }

    /**
     * 处理每一个责任要素，将需要计算的要素计算出结果。
     * @return
     */
    private boolean dealWrapRiskDutyParam()
    {
        double tCopys = mCertifyInfo.getCopys();
        double tMult = mCertifyInfo.getMult();
        String InsuYear= mCertifyInfo.getInsuYear();
        for (int i = 1; i <= mRiskDutyWrapInfo.size(); i++)
        {
            LDRiskDutyWrapSchema tTmpDutyParam = mRiskDutyWrapInfo.get(i);// 用引用的方式修改mRiskDutyWrapInfo中每个元素。
            String tCalFactorType = tTmpDutyParam.getCalFactorType();
            if ("2".equals(tCalFactorType))
            {
                PubCalculator tCal = new PubCalculator();

                String tCalSql = tTmpDutyParam.getCalSql();
                System.out.println("CalSql:" + tCalSql);
                tCal.setCalSql(tCalSql);

                String tTmpRiskCode = tTmpDutyParam.getRiskCode();
                tCal.addBasicFactor("RiskCode", tTmpRiskCode);

                tCal.addBasicFactor("Copys", String.valueOf(tCopys));
                tCal.addBasicFactor("Mult", String.valueOf(tMult));
                tCal.addBasicFactor("InsuYear", String.valueOf(InsuYear));
                String tTmpCalResult = tCal.calculate();

                if (tTmpCalResult == null || tTmpCalResult.equals(""))
                {
                    buildError("dealWrapRiskDutyParam", "要素计算错误。");
                    return false;
                }

                tTmpDutyParam.setCalFactorValue(tTmpCalResult);
            }
            else if ("1".equals(tCalFactorType))
            {
                continue;
            }
            else
            {
                buildError("dealWrapRiskDutyParam", "套餐要素类型描述有误。");
                return false;
            }
        }

        return true;
    }

    /**
     * 获取团单相关数据。
     * @return
     */
    private boolean loadGrpContAllInfo()
    {
        // 获取LCGrpCont。
        if (!loadGrpContInfo())
        {
            buildError("createCertCont", "获取结算团体合同信息失败。");
            return false;
        }
        // --------------------

        // 获取LCGrpAppnt。
        if (!loadGrpAppntInfo())
        {
            buildError("createCertCont", "获取结算团体合同信息失败。");
            return false;
        }
        // --------------------

        // 获取LCGrpAppnt。
        if (!loadGrpCustomerInfo())
        {
            buildError("createCertCont", "获取结算团体合同信息失败。");
            return false;
        }
        // --------------------

        // 获取LCGrpPol。
        if (!loadGrpPolInfo())
        {
            buildError("createCertCont", "获取结算团体合同信息失败。");
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 生成分单数据
     * @return
     */
    private MMap dealSubContData()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        mContNo = mCertifyInfo.getCardNo();//暂时以卡号做为ContNo，入口进行非空校验，因此此时必有值。
        //        mContNo = PubFun1.CreateMaxNo("GRPPERSONCONTNO", "");
        //        if (mContNo == null || mContNo.equals(""))
        //        {
        //            buildError("dealSubContData", "生成分单合同号失败。");
        //            return false;
        //        }

        // 被保人信息
        if (!dealInsuInfo())
        {
            return null;
        }
        // --------------------

        // 被保人信息
        if (!dealContInfo())
        {
            return null;
        }
        // --------------------

        // 被险种信息
        if (!dealPolRiskInfo())
        {
            return null;
        }
        // --------------------

        // 处理卡折清单信息
        if (!dealCertInfo())
        {
            return null;
        }
        // --------------------

        // 被险种信息
        if (!dealContDateData())
        {
            return null;
        }
        // --------------------

        // 创建连带被保人分单信息
        tTmpMap = dealCertInsuRela();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    /**
     * 产生被保人信息，如果为新客户同时产生客户资料信息。
     * @return
     */
    private boolean dealInsuInfo()
    {
        Reflections tRef = new Reflections();
        ExeSQL tExeSQL = new ExeSQL();

        String tStrSql = null;
        String tResult = null;

        if (mInsuSchema == null)
            mInsuSchema = new LCInsuredSchema();

        // 封装被保人信息
        mInsuSchema.setGrpContNo(mGrpContInfo.getGrpContNo());

        mInsuSchema.setContNo(mContNo);
        mInsuSchema.setPrtNo(mCertifyInfo.getPrtNo());
        mInsuSchema.setManageCom(mGrpContInfo.getManageCom());
        mInsuSchema.setExecuteCom(mGlobalInput.ManageCom);

        mInsuSchema.setContPlanCode("A");//卡折保障计划默认为A

        mInsuSchema.setAppntNo(mGrpAppntInfo.getCustomerNo());

        mInsuSchema.setName(mCertifyInsuInfo.getName());
        mInsuSchema.setSex(mCertifyInsuInfo.getSex());
        mInsuSchema.setBirthday(mCertifyInsuInfo.getBirthday());
        mInsuSchema.setIDNo(mCertifyInsuInfo.getIdNo());
        mInsuSchema.setIDType(mCertifyInsuInfo.getIdType());

        mInsuSchema.setOccupationType(mCertifyInsuInfo.getOccupationType());
        mInsuSchema.setOccupationCode(mCertifyInsuInfo.getOccupationCode());
        mInsuSchema.setRelationToMainInsured("00");//卡折默认为“00－本人”
        mInsuSchema.setGrpInsuredPhone(mCertifyInsuInfo.getPhone());

        mInsuSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mInsuSchema);
        // --------------------

        // 产生被保人客户信息
        tStrSql = null;
        tResult = null;
        LDPersonSchema tLDPersonSchema = new LDPersonSchema();
        tRef.transFields(tLDPersonSchema, mInsuSchema);
        LDPersonDB tLDPersonDB = new LDPersonDB();
        tStrSql = " select CustomerNo from LDPerson " + " where 1 = 1 " + " and Name = '" + tLDPersonSchema.getName()
                + "' " + " and Sex = '" + tLDPersonSchema.getSex() + "' " + " and Birthday = '"
                + tLDPersonSchema.getBirthday() + "' " + " and IDNo = '" + tLDPersonSchema.getIDNo() + "' "
                + " and IDType = '" + tLDPersonSchema.getIDType() + "' " + " order by CustomerNo ";//如果存在多个客户，固定选择最小的客户号做为默认客户号。
        tResult = tExeSQL.getOneValue(tStrSql);
        String tAuthSql = "select Authorization from wfinsulist where cardno = '"+mCertifyInsuInfo.getCardNo()+"'";
        String tAuthorization = tExeSQL.getOneValue(tAuthSql);
        if (!tResult.equals(""))
        {
        	 String tAuthPensonSql = "select Authorization from ldperson where customerno = '"+tResult+"'";
             String tAuthorizationPerson = tExeSQL.getOneValue(tAuthPensonSql);
             if(!"1".equals(tAuthorizationPerson)){//ldperson表中授权字段不为是
                 if("1".equals(tAuthorization)){//卡折授权信息为是
                 	tLDPersonSchema.setCustomerNo(tResult);
                    if(tLDPersonDB.getInfo()){
                    	aNewLDPersonSchema = tLDPersonDB.getSchema();
                        aNewLDPersonSchema.setAuthorization(tAuthorization);//根据卡折赋值ldperson
                    }  
             	}
             }
            mInsuSchema.setInsuredNo(tResult);
           
        }
        else
        {
            String tInsuredNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
            if (tInsuredNo == null || tInsuredNo.equals(""))
            {
                buildError("dealInsuInfo", "生成客户号失败。");
                return false;
            }
            mInsuSchema.setInsuredNo(tInsuredNo);
            tLDPersonSchema.setCustomerNo(tInsuredNo);
            tLDPersonSchema.setAuthorization(tAuthorization);//卡折实名化
            mPersonSchema = tLDPersonSchema.getSchema();//被保人为新客户，创建客户信息
            
        }

        if (mInsuSchema.getInsuredNo() == null || mInsuSchema.getInsuredNo().equals(""))
        {
            buildError("dealInsuInfo", "被保人客户号为空。");
            return false;
        }
        tStrSql = null;
        tResult = null;
        // --------------------

        // 产生被保人地址信息
        if (mInsuAddrSchema == null)
            mInsuAddrSchema = new LCAddressSchema();

        String tAddressNo = tExeSQL
                .getOneValue("Select Case When (max(integer(AddressNo))) Is Null Then 0 Else (max(integer(AddressNo))) End from LCAddress where CustomerNo='"
                        + mInsuSchema.getInsuredNo() + "'");
        if (tAddressNo == null || tAddressNo.equals(""))
        {
            buildError("checkAppntAddress", "生成地址代码错误！");
            return false;
        }

        try
        {
            tAddressNo = String.valueOf(Integer.parseInt(tAddressNo) + 1);
        }
        catch (Exception e)
        {
            buildError("checkAppntAddress", "生成新地址代码错误，可能地址号出现了非数字型数据！");
            return false;
        }

        mInsuSchema.setAddressNo(tAddressNo);

        mInsuAddrSchema.setCustomerNo(mInsuSchema.getInsuredNo());
        mInsuAddrSchema.setAddressNo(tAddressNo);
        mInsuAddrSchema.setPostalAddress(mCertifyInsuInfo.getPostalAddress());
        mInsuAddrSchema.setZipCode(mCertifyInsuInfo.getZipCode());
        mInsuAddrSchema.setGrpName(mCertifyInsuInfo.getGrpName());
        mInsuAddrSchema.setCompanyPhone(mCertifyInsuInfo.getCompanyPhone());
        mInsuAddrSchema.setPhone(mCertifyInsuInfo.getPhone());
        mInsuAddrSchema.setMobile(mCertifyInsuInfo.getMobile());
        mInsuAddrSchema.setEMail(mCertifyInsuInfo.getEMail());

        mInsuAddrSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mInsuAddrSchema);
        // --------------------

        return true;
    }

    /**
     * 处理分单合同信息
     * @return
     */
    private boolean dealContInfo()
    {
        if (mContSchema == null)
            mContSchema = new LCContSchema();

        // 封装分单合同数据
        mContSchema.setGrpContNo(mGrpContInfo.getGrpContNo());
        mContSchema.setPrtNo(mGrpContInfo.getPrtNo());
        mContSchema.setContNo(mInsuSchema.getContNo());//由于创建分单信息是以被保人为基础生成数据，因此从mInsuSchema获取合同号等信息。
        mContSchema.setProposalContNo(mInsuSchema.getContNo());
        mContSchema.setManageCom(mGrpContInfo.getManageCom());
        mContSchema.setExecuteCom(mGlobalInput.ManageCom);

        mContSchema.setSaleChnl(mGrpContInfo.getSaleChnl());
        mContSchema.setSaleChnlDetail(mGrpContInfo.getSaleChnlDetail());

        mContSchema.setAgentCode(mGrpContInfo.getAgentCode());
        mContSchema.setAgentCom(mGrpContInfo.getAgentCom());
        mContSchema.setAgentGroup(mGrpContInfo.getAgentGroup());
        mContSchema.setAgentType(mGrpContInfo.getAgentType());

        mContSchema.setAppntName(mGrpContInfo.getGrpName());
        mContSchema.setAppntNo(mGrpContInfo.getAppntNo());

        mContSchema.setInsuredNo(mInsuSchema.getInsuredNo());
        mContSchema.setInsuredSex(mInsuSchema.getSex());
        mContSchema.setInsuredName(mInsuSchema.getName());
        mContSchema.setInsuredBirthday(mInsuSchema.getBirthday());
        mContSchema.setInsuredIDType(mInsuSchema.getIDType());
        mContSchema.setInsuredIDNo(mInsuSchema.getIDNo());

        mContSchema.setSignCom(mGlobalInput.ManageCom);
        mContSchema.setSignDate(mCurDate);
        mContSchema.setSignTime(mCurTime);

        //        mContSchema.setAppFlag(mGrpContInfo.getAppFlag());
        //        mContSchema.setStateFlag(mGrpContInfo.getStateFlag());
        mContSchema.setAppFlag("1");
        mContSchema.setStateFlag("1");

        mContSchema.setPrintCount(mGrpContInfo.getPrintCount());
        mContSchema.setPolApplyDate(mGrpContInfo.getPolApplyDate());

        mContSchema.setPeoples(1);

        mContSchema.setPolType("0");
        mContSchema.setContType("2");
        mContSchema.setCardFlag("0");

        mContSchema.setPayMode(mGrpContInfo.getPayMode());
        mContSchema.setPayIntv(mGrpContInfo.getPayIntv());

        mContSchema.setCValiDate(mCertifyInfo.getCValidate());

        mContSchema.setInputOperator(mGlobalInput.Operator);
        mContSchema.setInputDate(mCurDate);
        mContSchema.setInputTime(mCurTime);

        mContSchema.setApproveFlag("9");
        mContSchema.setApproveCode(mGlobalInput.Operator);
        mContSchema.setApproveDate(mCurDate);
        mContSchema.setApproveTime(mCurTime);

        mContSchema.setUWFlag("9");
        mContSchema.setUWOperator(mGlobalInput.Operator);
        mContSchema.setUWDate(mCurDate);
        mContSchema.setUWTime(mCurTime);

        mContSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mContSchema);
        // --------------------

        return true;
    }

    private boolean dealPolRiskInfo()
    {
        if (mPolSet == null)
            mPolSet = new LCPolSet();
        if (mDutySet == null)
            mDutySet = new LCDutySet();
        if (mPremSet == null)
            mPremSet = new LCPremSet();
        if (mGetSet == null)
            mGetSet = new LCGetSet();

        for (int i = 1; i <= mGrpPolInfo.size(); i++)
        {
            LCGrpPolSchema tTmpGrpPolInfo = mGrpPolInfo.get(i);
            LCPolSchema tContPolInfo = getSubContPolInfo(tTmpGrpPolInfo);
            if (tContPolInfo == null)
            {
                return false;
            }

            String tRiskCode = tContPolInfo.getRiskCode();

            /** 封装责任信息 */
            LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
            tLMRiskDutyDB.setRiskCode(tRiskCode);
            LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
            if (tLMRiskDutySet == null || tLMRiskDutySet.size() == 0)
            {
                buildError("dealPolRiskInfo", "获取险种责任描述信息失败。");
                return false;
            }

            LCDutySet tLCDutySet = getDutyInfo(tLMRiskDutySet, tContPolInfo);
            if (tLCDutySet == null || tLCDutySet.size() <= 0)
            {
                buildError("dealPolRiskInfo", "处理责任要素失败");
                return false;
            }

            CalBL tCalBL = new CalBL(tContPolInfo, tLCDutySet, null, null);
            tCalBL.setRiskWrapCode(mCertRiskWrapCode);
            if (!tCalBL.calPol())
            {
                this.mErrors.copyAllErrors(tCalBL.mErrors);
                return false;
            }
            if (tCalBL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tCalBL.mErrors);
                return false;
            }
            tContPolInfo.setSchema(tCalBL.getLCPol());
            tLCDutySet = tCalBL.getLCDuty();
            LCPremSet tLCPremSet = tCalBL.getLCPrem();
            LCGetSet tLCGetSet = tCalBL.getLCGet();

            for (int m = 1; m <= tLCDutySet.size(); m++)
            {
                tLCDutySet.get(m).setPolNo(tContPolInfo.getPolNo());
                tLCDutySet.get(m).setPrem(0);// 实名化的分单，保费置为0
                tContPolInfo.setGetYear(tLCDutySet.get(m).getGetYear());
                tContPolInfo.setGetYearFlag(tLCDutySet.get(m).getGetYearFlag());
                tContPolInfo.setPayEndYearFlag(tLCDutySet.get(m).getPayEndYearFlag());
                tContPolInfo.setPayEndYear(tLCDutySet.get(m).getPayEndYear());
                tContPolInfo.setInsuYearFlag(tLCDutySet.get(m).getInsuYearFlag());
                tContPolInfo.setInsuYear(tLCDutySet.get(m).getInsuYear());
                tContPolInfo.setMult(tLCDutySet.get(m).getMult());
            }
            for (int m = 1; m <= tLCPremSet.size(); m++)
            {
                tLCPremSet.get(m).setPolNo(tContPolInfo.getPolNo());
                tLCPremSet.get(m).setGrpContNo(tContPolInfo.getGrpContNo());
                tLCPremSet.get(m).setContNo(tContPolInfo.getContNo());
                tLCPremSet.get(m).setOperator(mGlobalInput.Operator);
                tLCPremSet.get(m).setPrem(0);// 实名化的分单，保费置为0
            }
            for (int m = 1; m <= tLCGetSet.size(); m++)
            {
                tLCGetSet.get(m).setPolNo(tContPolInfo.getPolNo());
                tLCGetSet.get(m).setGrpContNo(tContPolInfo.getGrpContNo());
                tLCGetSet.get(m).setContNo(tContPolInfo.getContNo());
                tLCGetSet.get(m).setOperator(mGlobalInput.Operator);
            }
            PubFun.fillDefaultField(tLCPremSet);
            PubFun.fillDefaultField(tLCGetSet);
            PubFun.fillDefaultField(tLCDutySet);

            tContPolInfo.setPrem(0);// 实名化的分单，保费置为0

            mPolSet.add(tContPolInfo);
            mDutySet.add(tLCDutySet);
            mPremSet.add(tLCPremSet);
            mGetSet.add(tLCGetSet);
        }

        // 统一处理MainPolNo
        if (!dealRiskMainPolNo())
        {
            return false;
        }
        // --------------------

        double sumPrem = 0;
        double sumAmnt = 0;
        double sumMult = 0;

        /** 将保费维护到LCCont表中 */
        for (int i = 1; i <= mPolSet.size(); i++)
        {
            sumPrem += mPolSet.get(i).getPrem();
            sumAmnt += mPolSet.get(i).getAmnt();
            sumMult += mPolSet.get(i).getMult();
        }
        mContSchema.setPrem(sumPrem);
        mContSchema.setAmnt(sumAmnt);
        mContSchema.setMult(sumMult);

        if (mContSchema.getCInValiDate() == null)
        {
            Date tCinValidate = null;
            for (int i = 1; i <= this.mPolSet.size(); i++)
            {
                Date lcpolDate = (new FDate()).getDate(mPolSet.get(i).getEndDate());
                if (tCinValidate == null || tCinValidate.before(lcpolDate))
                {
                    tCinValidate = lcpolDate;
                }
            }
            mContSchema.setCInValiDate(tCinValidate);
        }

        return true;
    }

    private LCDutySet getDutyInfo(LMRiskDutySet tLMRiskDutySet, LCPolSchema tLCPolSchema)
    {
        //        System.out.println(tLMRiskDutySet.get(1).getDutyCode());
        LCDutySet tLCDutySet = new LCDutySet();
        HashMap tDutyMap = null;
        if (mRiskDutyWrapInfo != null && mRiskDutyWrapInfo.size() > 0)
        {
            tDutyMap = new HashMap();
            for (int i = 1; i <= mRiskDutyWrapInfo.size(); i++)
            {
                tDutyMap.put(mRiskDutyWrapInfo.get(i).getDutyCode(), "1");
            }
        }
        boolean tFlag = false;
        for (int k = 1; k <= mRiskDutyWrapInfo.size(); k++)
        {
            if (mRiskDutyWrapInfo.get(k).getRiskCode().trim().equals(tLMRiskDutySet.get(1).getRiskCode()))
            {
                tFlag = true;
                break;
            }
        }

        for (int i = 1; i <= tLMRiskDutySet.size(); i++)
        {
            if (tDutyMap != null && tFlag)
            {
                System.out.println((String) tDutyMap.get(tLMRiskDutySet.get(i).getDutyCode()));
                String chk = (String) tDutyMap.get(tLMRiskDutySet.get(i).getDutyCode());
                if (chk == null)
                {
                    continue;
                }
            }

            LMRiskDutySchema tTmpRiskDutySchema = tLMRiskDutySet.get(i);

            String tRiskCode = tTmpRiskDutySchema.getRiskCode();
            String tDutyCode = tTmpRiskDutySchema.getDutyCode();

            /** 查询责任描述 */
            LMDutyDB tLMDutyDB = new LMDutyDB();
            tLMDutyDB.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());
            if (!tLMDutyDB.getInfo())
            {
                buildError("getDuty", "查询险种责任出错！");
                return null;
            }

            String tAmnt = getRiskDutyParamByCode(tDutyCode, tRiskCode, "Amnt");
            String tPrem = getRiskDutyParamByCode(tDutyCode, tRiskCode, "Prem");
            String tMutl = getRiskDutyParamByCode(tDutyCode, tRiskCode, "Mutl");
            String tCopys = getRiskDutyParamByCode(tDutyCode, tRiskCode, "Copys ");

            LCDutySchema tLCDutySchema = new LCDutySchema();

            tLCDutySchema.setDutyCode(tDutyCode);
            tLCDutySchema.setAmnt(tAmnt);
            tLCDutySchema.setMult(tMutl);
            tLCDutySchema.setCopys(tCopys);
            tLCDutySchema.setPrem(tPrem);

            tLCDutySchema.setPayIntv(tLCPolSchema.getPayIntv());

            String tInsuYear = getRiskDutyParamByCode(tDutyCode, tRiskCode, "InsuYear");
            String InsuYearFlag = getRiskDutyParamByCode(tDutyCode, tRiskCode, "InsuYearFlag");

            if (tInsuYear == null || tInsuYear.equals("") || InsuYearFlag == null || InsuYearFlag.equals(""))
            {
                buildError("getDutyInfo", "险种信息中保障期间要素为空。");
            }

            tLCDutySchema.setInsuYear(tLCPolSchema.getInsuYear());
            tLCDutySchema.setInsuYearFlag(tLMDutyDB.getInsuYearFlag());
            tLCDutySet.add(tLCDutySchema);

            /** 当描述指定责任的要素和要素值得情况下，以描述为主 */
            String tWrapCode = null;
            for (int m = 1; m <= mRiskDutyWrapInfo.size(); m++)
            {
                LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mRiskDutyWrapInfo.get(m);
                //                String tTmpRiskCode = tLCPolSchema.getRiskCode();
                //                String tTmpDutyCode = tLCDutySchema.getDutyCode();

                if (tLDRiskDutyWrapSchema.getRiskCode().equals(tRiskCode)
                        && tLDRiskDutyWrapSchema.getDutyCode().equals(tDutyCode))
                {
                    if (tWrapCode == null)
                    {
                        tWrapCode = tLDRiskDutyWrapSchema.getRiskWrapCode();
                    }
                    else if (!tWrapCode.equals(tLDRiskDutyWrapSchema.getRiskWrapCode()))
                    {
                        String str = "系统选择两个套餐中有相同的险种，不允许出现单个被保人拥有相同险种的情况!";
                        buildError("getDuty", str);
                        return null;
                    }

                    /** 直接取值 */
                    tLCDutySchema.setV(mRiskDutyWrapInfo.get(m).getCalFactor(), tLDRiskDutyWrapSchema
                            .getCalFactorValue());

                }
            }

            //若不录入给付比例，则系统默认为1
            if (Math.abs(0 - tLCDutySchema.getGetRate()) < 0.00001)
            {
                tLCDutySchema.setGetRate(1);
            }
        }
        return tLCDutySet;
    }

    /**
     * @param cDutyCode
     * @param cRiskCode
     * @param cCalFactor
     * @return
     */
    private String getRiskDutyParamByCode(String cDutyCode, String cRiskCode, String cCalFactor)
    {
        String tTmpResult = null;

        for (int i = 1; i <= mRiskDutyWrapInfo.size(); i++)
        {
            LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = mRiskDutyWrapInfo.get(i);
            String tTmpRiskCode = tLDRiskDutyWrapSchema.getRiskCode();
            String tTmpDutyCode = tLDRiskDutyWrapSchema.getDutyCode();
            String tTmpCalFactor = tLDRiskDutyWrapSchema.getCalFactor();

            if (StrTool.cTrim(cDutyCode).equals(tTmpDutyCode) && StrTool.cTrim(cRiskCode).equals(tTmpRiskCode)
                    && StrTool.cTrim(cCalFactor).equals(tTmpCalFactor))
            {
                tTmpResult = tLDRiskDutyWrapSchema.getCalFactorValue();
                break;
            }
        }

        return tTmpResult;
    }

    /**
     * 根据一个团体险种信息封装分单险种信息。
     * @return
     */
    private LCPolSchema getSubContPolInfo(LCGrpPolSchema cGrpPolSchema)
    {
        String tProposalNo = PubFun1.CreateMaxNo("ProposalNo", PubFun.getNoLimit(mContSchema.getManageCom()));
        if (tProposalNo == null || tProposalNo.equals(""))
        {
            buildError("getSubContPolInfo", "生成险种投保号失败。");
            return null;
        }

        String tNewPolNo = PubFun1.CreateMaxNo("POLNO", PubFun.getNoLimit(mContSchema.getManageCom()));
        if (tNewPolNo == null || tNewPolNo.equals(""))
        {
            buildError("getSubContPolInfo", "生成险种号失败。");
            return null;
        }

        System.out.println("PolN:" + tNewPolNo);
        System.out.println("ProposalNo:" + tProposalNo);
        System.out.println("GrpPolSchema.getRiskCode:" + cGrpPolSchema.getRiskCode());

        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cGrpPolSchema.getRiskCode());
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(cGrpPolSchema.getRiskCode());
        if (!tLMRiskAppDB.getInfo() || !tLMRiskDB.getInfo())
        {
            buildError("dealRisk", "查询险种信息失败！");
            return null;
        }

        LDRiskWrapSchema tRiskWrapSchema = getRiskWrapSetByCode(cGrpPolSchema.getRiskCode());
        if (tRiskWrapSchema == null)
        {
            return null;
        }

        LCPolSchema tPolSchema = new LCPolSchema();

        tPolSchema.setGrpContNo(cGrpPolSchema.getGrpContNo());
        tPolSchema.setGrpPolNo(cGrpPolSchema.getGrpPolNo());

        tPolSchema.setContNo(mContSchema.getContNo());
        tPolSchema.setProposalContNo(mContSchema.getProposalContNo());
        tPolSchema.setPrtNo(mContSchema.getPrtNo());
        tPolSchema.setPolNo(tNewPolNo);
        tPolSchema.setProposalNo(tProposalNo);

        tPolSchema.setMainPolNo(tRiskWrapSchema.getMainRiskCode());// 用MainPolNo保存对应主险险种号，为后期替换做准备。成功原则：卡折为单被保人单套餐，且套餐中相同险种有且仅有一个。
        
        //------- MasterPolNo 赋值  -------------------------   By zhangyang
        LCPolDB tLCPolDB = new LCPolDB();
        LCPolSet tLCPolSet = new LCPolSet();
        String strSQL = "select * from lcpol where PrtNo = '" + mContSchema.getPrtNo() 
                + "' and RiskCode = '" + cGrpPolSchema.getRiskCode() 
                + "' and PolTypeFlag = '1' ";
        tLCPolSet = tLCPolDB.executeQuery(strSQL);
        if(tLCPolSet.size() <= 0)
        {
            buildError("getSubContPolInfo", "查询险种分单信息失败！");
            return null;
        }
        System.out.println("\nPolNo = " + tLCPolSet.get(1).getPolNo() + "\nRiskCode = "
                + tLCPolSet.get(1).getRiskCode() + "\nInsuredName = " 
                + tLCPolSet.get(1).getInsuredName());
        tPolSchema.setMasterPolNo(tLCPolSet.get(1).getPolNo());
        //--------------------------------------------------------

        tPolSchema.setRiskCode(cGrpPolSchema.getRiskCode());

        tPolSchema.setKindCode(tLMRiskAppDB.getKindCode());
        tPolSchema.setRiskVersion(tLMRiskAppDB.getRiskVer());

        tPolSchema.setManageCom(mContSchema.getManageCom());

        tPolSchema.setSaleChnl(mContSchema.getSaleChnl());
        tPolSchema.setSaleChnlDetail(mContSchema.getSaleChnlDetail());

        tPolSchema.setAgentCom(mContSchema.getAgentCom());
        tPolSchema.setAgentType(mContSchema.getAgentType());
        tPolSchema.setAgentCode(mContSchema.getAgentCode());
        tPolSchema.setAgentGroup(mContSchema.getAgentGroup());

        tPolSchema.setAppntNo(mContSchema.getAppntNo());
        tPolSchema.setAppntName(mContSchema.getAppntName());

        tPolSchema.setCValiDate(mContSchema.getCValiDate());

        tPolSchema.setContType("2");
        tPolSchema.setPolTypeFlag("0");

        tPolSchema.setInsuredPeoples(1);

        tPolSchema.setInsuredAppAge(PubFun.getInsuredAppAge(mContSchema.getCValiDate(), mInsuSchema.getBirthday()));

        tPolSchema.setOccupationType(mInsuSchema.getOccupationType());
        tPolSchema.setInsuredSex(mInsuSchema.getSex());
        tPolSchema.setInsuredBirthday(mInsuSchema.getBirthday());
        tPolSchema.setInsuredName(mInsuSchema.getName());
        tPolSchema.setInsuredNo(mInsuSchema.getInsuredNo());

        tPolSchema.setAutoPayFlag(tLMRiskAppDB.getAutoPayFlag());
        if (StrTool.cTrim(tLMRiskDB.getRnewFlag()).equals("N"))
        {
            tPolSchema.setRnewFlag(-2);
        }
        tPolSchema.setComFeeRate(tLMRiskAppDB.getAppInterest());
        tPolSchema.setBranchFeeRate(tLMRiskAppDB.getAppPremRate());

        tPolSchema.setSpecifyValiDate("1");
        tPolSchema.setPayMode(mContSchema.getPayMode());
        tPolSchema.setPayIntv(mContSchema.getPayIntv());

        tPolSchema.setApproveFlag(mContSchema.getApproveFlag());
        tPolSchema.setApproveCode(mGlobalInput.Operator);
        tPolSchema.setApproveDate(mCurDate);
        tPolSchema.setApproveTime(mCurTime);

        tPolSchema.setUWFlag("9");
        tPolSchema.setUWDate(mCurDate);
        tPolSchema.setUWTime(mCurTime);

        tPolSchema.setUWFlag(mContSchema.getUWFlag());
        tPolSchema.setUWCode(mGlobalInput.Operator);
        tPolSchema.setUWDate(mCurDate);
        tPolSchema.setUWTime(mCurTime);

        tPolSchema.setSignCom(mGlobalInput.ComCode);
        tPolSchema.setSignDate(mCurDate);
        tPolSchema.setSignTime(mCurTime);

        tPolSchema.setAppFlag(mContSchema.getAppFlag());
        tPolSchema.setStateFlag(mContSchema.getStateFlag());

        tPolSchema.setPolApplyDate(mContSchema.getPolApplyDate());
        tPolSchema.setContPlanCode(mInsuSchema.getContPlanCode());

        tPolSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tPolSchema);

        return tPolSchema;
    }

    /**
     * 处理险种的MainPolNo
     * @return
     */
    private boolean dealRiskMainPolNo()
    {
        HashMap tRisk2PolNo = new HashMap();
        for (int i = 1; i <= mPolSet.size(); i++)
        {
            LCPolSchema tTmpPolSchema = mPolSet.get(i);
            String tRiskCode = tTmpPolSchema.getRiskCode();
            if (tRisk2PolNo.get(tRiskCode) != null)
            {
                buildError("dealRiskMainPolNo", "目前同一保险卡不支持多个相同险种。");
                return false;
            }
            String tRiskPolNo = tTmpPolSchema.getPolNo();
            tRisk2PolNo.put(tRiskCode, tRiskPolNo);
        }

        for (int i = 1; i <= mPolSet.size(); i++)
        {
            LCPolSchema tTmpPolSchema = mPolSet.get(i);
            String tTmpMainRiskCode = tTmpPolSchema.getMainPolNo();
            String tNewMainPolNo = (String) tRisk2PolNo.get(tTmpMainRiskCode);
            if (tNewMainPolNo == null || tNewMainPolNo.equals(""))
            {
                buildError("dealRiskMainPolNo", "未找到主险险种代码。");
                return false;
            }
            tTmpPolSchema.setMainPolNo(tNewMainPolNo);
        }

        return true;
    }

    /**
     * 处理卡折清单状态及回写数据。
     * @return
     */
    private boolean dealCertInfo()
    {
        mCertifyInfo.setWSState(CertifyContConst.CC_WSSTATE_CREATED);
        mCertifyInfo.setContNo(mContSchema.getContNo());
        mCertifyInsuInfo.setCustomerNo(mContSchema.getInsuredNo());

        return true;
    }

    /**
     * 根据险种号，获取套餐险种信息描述
     * @param cRiskCode
     * @return
     */
    private LDRiskWrapSchema getRiskWrapSetByCode(String cRiskCode)
    {
        LDRiskWrapSet tLDRiskWrapSet = new LDRiskWrapSet();

        for (int i = 1; i <= mRiskWrapInfo.size(); i++)
        {
            LDRiskWrapSchema tTmpRiskInfo = mRiskWrapInfo.get(i);
            if (StrTool.cTrim(cRiskCode).equals(tTmpRiskInfo.getRiskCode()))
            {
                tLDRiskWrapSet.add(tTmpRiskInfo.getSchema());
            }
        }
        if (tLDRiskWrapSet.size() != 1)
        {
            buildError("getRiskWrapSetByCode", "未找到套餐中该险种信息，或同一套餐中出现多次相同险种。");
            return null;
        }
        return tLDRiskWrapSet.get(1).getSchema();
    }

    /**
     * 校验必要卡折承保要素是否完整
     * @return
     */
    private boolean chkCertInfo()
    {
        // 校验保险卡是否含有生效日期
        if (mCertifyInfo.getCValidate() == null || mCertifyInfo.getCValidate().equals(""))
        {
            buildError("chkCertInfo", "保险卡清单缺少相关生效日期。");
            return false;
        }
        // --------------------

        // 校验保险卡是否含有卡号
        if (mCertifyInfo.getCardNo() == null || mCertifyInfo.getCardNo().equals(""))
        {
            buildError("chkCertInfo", "保险卡号缺失。");
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 校验必要卡折被保人要素是否完整
     * @return
     */
    private boolean chkCertInsuInfo()
    {
        // 判断被保人五要素信息是否齐全
        if (mCertifyInsuInfo.getName() == null || mCertifyInsuInfo.getName().equals(""))
        {
            buildError("chkCertInsuInfo", "保险卡被保人姓名信息缺失。");
            return false;
        }
        if (mCertifyInsuInfo.getSex() == null || mCertifyInsuInfo.getSex().equals(""))
        {
            buildError("chkCertInsuInfo", "保险卡被保人性别信息缺失。");
            return false;
        }
        if (mCertifyInsuInfo.getBirthday() == null || mCertifyInsuInfo.getBirthday().equals(""))
        {
            buildError("chkCertInsuInfo", "保险卡被保人出生日期信息缺失。");
            return false;
        }
        if (mCertifyInsuInfo.getIdType() == null || mCertifyInsuInfo.getIdType().equals(""))
        {
            buildError("chkCertInsuInfo", "保险卡被保人证件类别信息缺失。");
            return false;
        }
        if (mCertifyInsuInfo.getIdNo() == null || mCertifyInsuInfo.getIdNo().equals(""))
        {
            buildError("chkCertInsuInfo", "保险卡被保人证件号码信息缺失。");
            return false;
        }
        // --------------------

        // 连带被保人五要素信息是否齐全
        if (mCertInsuRelaList != null)
        {
            for (int i = 1; i <= mCertInsuRelaList.size(); i++)
            {
                LICertifyInsuredSchema tTmpCertInsuInfo = mCertInsuRelaList.get(i);
                if (tTmpCertInsuInfo.getName() == null || tTmpCertInsuInfo.getName().equals(""))
                {
                    buildError("chkCertInsuInfo", "保险卡连带被保人姓名信息缺失。");
                    return false;
                }
                if (tTmpCertInsuInfo.getSex() == null || tTmpCertInsuInfo.getSex().equals(""))
                {
                    buildError("chkCertInsuInfo", "保险卡连带被保人性别信息缺失。");
                    return false;
                }
                if (tTmpCertInsuInfo.getBirthday() == null || tTmpCertInsuInfo.getBirthday().equals(""))
                {
                    buildError("chkCertInsuInfo", "保险卡连带被保人出生日期信息缺失。");
                    return false;
                }
                if (tTmpCertInsuInfo.getIdType() == null || tTmpCertInsuInfo.getIdType().equals(""))
                {
                    buildError("chkCertInsuInfo", "保险卡连带被保人证件类别信息缺失。");
                    return false;
                }
                if (tTmpCertInsuInfo.getIdNo() == null || tTmpCertInsuInfo.getIdNo().equals(""))
                {
                    buildError("chkCertInsuInfo", "保险卡连带被保人证件号码信息缺失。");
                    return false;
                }
            }
        }
        // --------------------

        return true;
    }

    /**
     * 获取团体无名单合同数据LCGrpCont。
     * @return
     */
    private boolean loadGrpContInfo()
    {
        String tGrpContNo = mCertifyInfo.getGrpContNo();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tGrpContNo);
        if (!tLCGrpContDB.getInfo())
        {
            buildError("loadGrpContInfo", "获取团单数据失败。");
            return false;
        }

        mGrpContInfo = tLCGrpContDB.getSchema();

        // 核对结算团单的印刷号是否与卡折清单中结算号一致。
        if (!mGrpContInfo.getPrtNo().equals(mCertifyInfo.getPrtNo())
                && !mGrpContInfo.getPrtNo().equals(mCertifyInsuInfo.getPrtNo()))
        {
            buildError("loadGrpContInfo", "保险卡（折）对应的结算团单数据异常。");
            return false;
        }
        // --------------------

        // 校验结算团单是否已经签单
        if (mGrpContInfo.getAppFlag() == null || !mGrpContInfo.getAppFlag().equals("1"))
        {
            buildError("loadGrpContInfo", "保险卡（折）对应的结算团单承保状态异常。");
            return false;
        }
        // --------------------

        return true;
    }

    /**
     * 获取投保单位信息LCGrpAppnt。
     * @return
     */
    private boolean loadGrpAppntInfo()
    {
        String tGrpContNo = mGrpContInfo.getGrpContNo();
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(tGrpContNo);
        if (!tLCGrpAppntDB.getInfo())
        {
            buildError("loadGrpAppntInfo", "保险卡（折）对应的结算团单投保单位数据异常。");
            return false;
        }
        mGrpAppntInfo = tLCGrpAppntDB.getSchema();

        return true;
    }

    /**
     * 获取投保客户数据。
     * @return
     */
    private boolean loadGrpCustomerInfo()
    {
        String tCustomerNo = mGrpAppntInfo.getCustomerNo();
        LDGrpDB tLDGrpDB = new LDGrpDB();
        tLDGrpDB.setCustomerNo(tCustomerNo);
        if (!tLDGrpDB.getInfo())
        {
            buildError("loadGrpCustomerInfo", "保险卡（折）对应的结算团单投保客户数据异常。");
            return false;
        }

        return true;
    }

    /**
     * 获取团体无名单险种数据LCGrpPol。
     * @return
     */
    private boolean loadGrpPolInfo()
    {
        String tGrpContNo = mGrpContInfo.getGrpContNo();
        String tStrSql = "select * from LCGrpPol where GrpContNo = '" + tGrpContNo + "'";
        mGrpPolInfo = new LCGrpPolDB().executeQuery(tStrSql);
        if (mGrpPolInfo == null || mGrpPolInfo.size() == 0)
        {
            buildError("loadGrpPolInfo", "获取团单险种相关数据失败。");
            return false;
        }

        return true;
    }

    private boolean dealContDateData()
    {
        mContSchema.setSumPrem(mContSchema.getPrem());
        mContSchema.setPaytoDate(mContSchema.getCInValiDate());

        for (int i = 1; i <= mPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = mPolSet.get(i);
            tLCPolSchema.setPaytoDate(tLCPolSchema.getPayEndDate());
            tLCPolSchema.setSumPrem(tLCPolSchema.getPrem());
        }

        for (int i = 1; i <= mDutySet.size(); i++)
        {
            LCDutySchema tLCDutySchema = mDutySet.get(i);
            tLCDutySchema.setPaytoDate(tLCDutySchema.getPayEndDate());
            tLCDutySchema.setSumPrem(tLCDutySchema.getPrem());
        }

        for (int i = 1; i <= mPremSet.size(); i++)
        {
            LCPremSchema tLCPremSchema = mPremSet.get(i);
            tLCPremSchema.setPaytoDate(tLCPremSchema.getPayEndDate());
            tLCPremSchema.setSumPrem(tLCPremSchema.getPrem());
        }

        //        for (int i = 1; i <= mGetSet.size(); i++)
        //        {
        //            LCGetSchema tLCGetSchema = mGetSet.get(i);
        //            LCGetSchema.setSumPrem(LCGetSchema.getPrem());
        //        }

        return true;
    }

    /**
     * 创建连带被保人分单信息
     * @return
     */
    private MMap dealCertInsuRela()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        try
        {
            if (mCertInsuRelaList != null)
            {
                for (int i = 1; i <= mCertInsuRelaList.size(); i++)
                {
                    LICertifyInsuredSchema tCertRelaInsuInfo = mCertInsuRelaList.get(i);

                    VData tVData = new VData();

                    TransferData tTransferData = new TransferData();

                    tVData.add(mCertifyInfo);
                    tVData.add(tCertRelaInsuInfo);
                    tVData.add(mContSchema);
                    tVData.add(mPolSet);
                    tVData.add(mDutySet);
                    tVData.add(mPremSet);
                    tVData.add(mGetSet);

                    tVData.add(tTransferData);
                    tVData.add(mGlobalInput);

                    CertInsuRelaBL tCertInsuRelaBL = new CertInsuRelaBL();
                    tTmpMap = null;
                    tTmpMap = tCertInsuRelaBL.getSubmitMap(tVData, "Create");
                    if (tTmpMap == null)
                    {
                        buildError("dealCertInsuRela", tCertInsuRelaBL.mErrors.getFirstError());
                        return null;
                    }
                    tMMap.add(tTmpMap);
                    tTmpMap = null;
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("dealCertInsuRela", "创建连带被保人分单信息时出现异常。");
            return null;
        }

        return tMMap;
    }

    /**
     * 准备处理好的数据。
     * @return
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        mResult.add(mContSchema);
        mResult.add(mInsuSchema);
        mResult.add(mPersonSchema);
        mResult.add(mInsuAddrSchema);
        mResult.add(mPolSet);
        mResult.add(mDutySet);
        mResult.add(mPremSet);
        mResult.add(mGetSet);
        mResult.add(mCertifyInfo);
        mResult.add(mCertifyInsuInfo);
        
        //4049 客户授权字段
        if(aNewLDPersonSchema!=null) {
        	mResult.add(aNewLDPersonSchema);
        	mMap.put(aNewLDPersonSchema,SysConst.UPDATE);
        }
        
        if (mContSchema != null)
        {
            mMap.put(mContSchema, SysConst.INSERT);
        }
        if (mInsuSchema != null)
        {
            mMap.put(mInsuSchema, SysConst.INSERT);
        }
        if (mPersonSchema != null)
        {
            mMap.put(mPersonSchema, SysConst.INSERT);
        }
        if (mInsuAddrSchema != null)
        {
            mMap.put(mInsuAddrSchema, SysConst.INSERT);
        }
        if (mPolSet != null && mPolSet.size() > 0)
        {
            mMap.put(mPolSet, SysConst.INSERT);
        }
        if (mDutySet != null && mDutySet.size() > 0)
        {
            mMap.put(mDutySet, SysConst.INSERT);
        }
        if (mPremSet != null && mPremSet.size() > 0)
        {
            mMap.put(mPremSet, SysConst.INSERT);
        }
        if (mGetSet != null && mGetSet.size() > 0)
        {
            mMap.put(mGetSet, SysConst.INSERT);
        }
        if (mCertifyInfo != null)
        {
            mMap.put(mCertifyInfo, SysConst.UPDATE);
        }
        if (mCertifyInsuInfo != null)
        {
            mMap.put(mCertifyInsuInfo, SysConst.UPDATE);
        }

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
