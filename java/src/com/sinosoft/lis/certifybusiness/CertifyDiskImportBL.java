/**
 * 2008-11-12
 */
package com.sinosoft.lis.certifybusiness;

import java.util.HashMap;

import com.sinosoft.lis.pubfun.diskimport.MultiSheetImporter;
import com.sinosoft.lis.vschema.LCInsuredListPolSet;
import com.sinosoft.lis.vschema.LCInsuredListSet;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.vschema.LICertifySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

/**
 * @author LY
 *
 */
public class CertifyDiskImportBL
{
    /** 从xls文件读入Sheet的开始行*/
    private static final int STARTROW = 2;

    /** 使用默认的导入方式 */
    private MultiSheetImporter importer;

    /** 错误处理 */
    public CErrors mErrors = new CErrors();

    /** Sheet Name */
    private String[] mSheetName;

    /** Sheet对应table的名字*/
    private static final String[] mTableNames = { "LICertify", "",
            "LICertifyInsured" };

    public CertifyDiskImportBL(String fileName, String configFileName,
            String[] sheetName)
    {
        mSheetName = sheetName;
        importer = new MultiSheetImporter(fileName, configFileName, mSheetName);
    }

    public boolean doImport()
    {
        System.out.println(" Into GrpDiskImport doImport...");

        importer.setTableName(mTableNames);
        importer.setMStartRows(STARTROW);

        if (!importer.doImport())
        {
            mErrors.copyAllErrors(importer.mErrors);
            return false;
        }

        String tFileVersion = getVersion();

        if (!checkVersion(tFileVersion))
        {
            return false;
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyDiskImportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public HashMap getResult()
    {
        return importer.getResult();
    }

    /**
     * 用于获取导入文件版本号
     * @return String
     */
    public String getVersion()
    {
        HashMap tHashMap = importer.getResult();
        String version = "";
        version = (String) tHashMap.get(mSheetName[1]);
        System.out.println("getVersion():" + version);
        return version;
    }

    /**
     * 校验导入文件模版
     * <br />当前版本号描述在：LDSysvar表CertifyDiskImportVer变量中。
     * @param cVersion 导入文件的版本号
     * @return
     */
    private boolean checkVersion(String cVersion)
    {
        if (cVersion == null)
        {
            String str = "导入模版版本号获取失败，请更换最新的磁盘导入模版!";
            buildError("doImport", str);
            System.out.println(str);
            return false;
        }

        String strSql = "select SysVarValue from LDSysvar where SysVar='CertifyDiskImportVer'";

        String tSysFileVersion = new ExeSQL().getOneValue(strSql);

        if (tSysFileVersion == null || tSysFileVersion.equals(""))
        {
            String str = "数据库中没有存储版本号的信息!";
            buildError("checkVersion", str);
            System.out.println(str);

            return false;
        }

        if (!cVersion.equals(tSysFileVersion))
        {
            String str = "导入模版错误，请更换最新的磁盘导入模版!";
            buildError("checkVersion", str);
            System.out.println(str);

            return false;
        }

        return true;
    }

    public LICertifySet getLICertifySet()
    {
        LICertifySet tLICertifySet = new LICertifySet();
        HashMap tHashMap = importer.getResult();
        tLICertifySet.add((LICertifySet) tHashMap.get(mSheetName[0]));

        return tLICertifySet;
    }

    public LICertifyInsuredSet getLICertifyInsuredSet()
    {
        LICertifyInsuredSet tLICertifyInsuredSet = new LICertifyInsuredSet();
        HashMap tHashMap = importer.getResult();
        tLICertifyInsuredSet.add((LICertifyInsuredSet) tHashMap
                .get(mSheetName[2]));

        return tLICertifyInsuredSet;
    }
}
