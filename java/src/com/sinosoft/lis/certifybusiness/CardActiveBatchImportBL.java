package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.ftp.*;
import java.net.*;
import java.io.*;

public class CardActiveBatchImportBL
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private String mDealInfo = "";

    private GlobalInput mGI = null; //用户信息

    private MMap map = new MMap();

    public CardActiveBatchImportBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if (getSubmitMap(cInputData, operate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;

    }

    public MMap getSubmitMap(VData cInputData, String operate)
    {
        if (!getInputData(cInputData))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * 1、校验是否录入了GlobalInpt信息，若没有录入，则构造如下信息：
     * Operator = “001”；
     * ComCode = ‘86“
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGI == null)
        {
            mGI = new GlobalInput();
            mGI.Operator = "001";
            mGI.ComCode = "86";
            mGI.ManageCom = mGI.ComCode;
        }

        return true;
    }

    private boolean dealData()
    {
        if (!getXls())
        {
            return false;
        }

        return true;
    }

    private boolean getXls()
    {
        String tUIRoot = CommonBL.getUIRoot();

        if (tUIRoot == null)
        {
            mErrors.addOneError("没有查找到应用目录");
            return false;
        }

        int xmlCount = 0;

        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'ServerIP'");
        String tPort = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'Port'");
        String tUsername = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'Username'");
        String tPassword = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'Password'");

        String tFilePath = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'FtpFilePath'");
        String tFileBackPath = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'FileBackPath'");
        tFileBackPath = tUIRoot + tFileBackPath;

        try
        {
            FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer.parseInt(tPort), "aaActiveMode");
            if (!tFTPTool.loginFTP())
            {
                CError tError = new CError();
                tError.moduleName = "CardActiveBatchImportBL";
                tError.functionName = "getXls";
                tError.errorMessage = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            String tFilePathLocal = tFileBackPath;
            String tFileImportBackPath = tFilePath;

            String[] tPath = tFTPTool.downloadOneDirectory(tFileImportBackPath, tFilePathLocal);
            String errContent = tFTPTool.getErrContent(FTPReplyCodeName.LANGUSGE_CHINESE);
            if (tPath == null && null != errContent)
            {
                mErrors.addOneError(errContent);
                return false;
            }

            for (int j = 0; j < tPath.length; j++)
            {
                if (tPath[j] == null)
                {
                    continue;
                }

                String tBatchNo = (tPath[j].substring(0, tPath[j].lastIndexOf(".")));

                boolean tImportFlag = CertifyDiskImportLog.isEndOfBatchNo(tBatchNo);

                if (!tBatchNo.matches("^LICCard.*"))
                {
                    mErrors.addOneError("不符合批次命名格式。" + tPath[j]);
                    File f = new File(tFilePathLocal + tPath[j]);
                    f.delete();
                    try
                    {
                        tFTPTool.deleteFile(tPath[j]);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    continue;
                }

                if (tImportFlag)
                {
                    mErrors.addOneError("批次已经导入。" + tPath[j]);
                    File f = new File(tFilePathLocal + tPath[j]);
                    f.delete();
                    try
                    {
                        tFTPTool.deleteFile(tPath[j]);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    continue;
                }

                xmlCount++;

                try
                {
                    VData tVData = new VData();

                    TransferData tTransferData = new TransferData();
                    tTransferData.setNameAndValue("FileName", tPath[j]);
                    tTransferData.setNameAndValue("FilePath", tFilePathLocal);
                    tTransferData.setNameAndValue("BatchNo", tBatchNo);

                    tVData.add(tTransferData);
                    tVData.add(mGI);

                    AddCertifyListUI tAddCertifyListUI = new AddCertifyListUI();

                    if (!tAddCertifyListUI.submitData(tVData, "ActiveImport"))
                    {
                        mErrors.copyAllErrors(tAddCertifyListUI.mErrors);
                        try
                        {
                            tFTPTool.deleteFile(tPath[j]);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        tFTPTool.deleteFile(tPath[j]);
                    }
                }
                catch (Exception ex)
                {
                    System.out.println("批次导入失败: " + tPath[j]);
                    mErrors.addOneError("批次导入失败" + tPath[j]);
                    ex.printStackTrace();
                    try
                    {
                        tFTPTool.deleteFile(tPath[j]);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            tFTPTool.logoutFTP();
            //            }
        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "获取连接失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "CardActiveBatchImportBL";
            tError.functionName = "getXls";
            tError.errorMessage = "连接ftp失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (xmlCount == 0)
        {
            mErrors.addOneError("没有需要导入的数据");
        }

        return true;
    }

    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);

        return true;
    }

    public String getDealInfo()
    {
        return mDealInfo;
    }

    public static void main(String[] args)
    {
    }
}
