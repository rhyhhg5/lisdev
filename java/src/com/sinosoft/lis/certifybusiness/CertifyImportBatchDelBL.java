package com.sinosoft.lis.certifybusiness;

import com.sinosoft.lis.db.LICertifyDB;
import com.sinosoft.lis.db.LIBCertifyDB;
import com.sinosoft.lis.db.LICertifyInsuredDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LICertifySchema;
import com.sinosoft.lis.schema.LIBCertifySchema;
import com.sinosoft.lis.schema.LOBICertifySchema;
import com.sinosoft.lis.schema.LICertifyInsuredSchema;
import com.sinosoft.lis.vschema.LICertifyInsuredSet;
import com.sinosoft.lis.schema.LOBICertifyInsuredSchema;
import com.sinosoft.lis.vschema.LOBICertifyInsuredSet;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CertifyImportBatchDelBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private GlobalInput tGI = new GlobalInput();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 单证号码 */
    private String cardNo;

    /** 外包批次号 */
    private String batchNo;

    private Reflections mReflections = new Reflections();

    /**
     * submitData
     * 
     * @param cInputData
     *            VData
     * @param cOperate
     *            String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        if (!getInputData())
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mInputData, null))
        {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     * 
     * @return boolean
     */
    private boolean getInputData()
    {
        try
        {
            tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);

            TransferData mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
            cardNo = (String) mTransferData.getValueByName("cardNo");
            batchNo = (String) mTransferData.getValueByName("batchNo");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ScanDeleBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * checkData
     * 
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 
     * @return boolean
     */
    private boolean dealData()
    {

        if (mOperate.equals("DELETE"))
        {
            if (!cardNo.equals(""))
            {
                String tStrSql = null;
                // 删除导入批次校验报告。
                tStrSql = "delete from LICChkError where CardNo = '" + cardNo + "'";
                map.put(tStrSql, SysConst.DELETE);
                tStrSql = null;
                // ------------------------------

                // 删除卡折业务卡折清单表。
                tStrSql = " delete from LICertify where CardNo = '" + cardNo + "' ";
                map.put(tStrSql, SysConst.DELETE); 
                // 删除卡折业务退保单。
                LIBCertifyDB mLIBCertifyDB = new LIBCertifyDB();
                mLIBCertifyDB.setCardNo(cardNo);
                if(mLIBCertifyDB.getInfo())
                {
                	tStrSql = " delete from LIBCertify where CardNo = '" + cardNo + "' ";
                    map.put(tStrSql, SysConst.DELETE); 
                }
                tStrSql = null;
                // 备份卡折业务卡折清单表。
                LICertifySchema mLICertifySchema = new LICertifySchema();
                LICertifyDB mLICertifyDB = new LICertifyDB();
                mLICertifyDB.setCardNo(cardNo);
                if (!mLICertifyDB.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "CertifyImportBatchDelBL.java";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询单证清单信息失败！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLICertifySchema = mLICertifyDB.query().get(1);
                LOBICertifySchema mLOBICertifySchema = new LOBICertifySchema();
                String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);

                mReflections.transFields(mLOBICertifySchema, mLICertifySchema);
                mLOBICertifySchema.setSerialNo(tSerielNo);
                mLOBICertifySchema.setOperator(tGI.Operator);
                mLOBICertifySchema.setMakeDate(PubFun.getCurrentDate());
                mLOBICertifySchema.setMakeTime(PubFun.getCurrentTime());

                if (mLOBICertifySchema != null)
                {
                    map.put(mLOBICertifySchema, "INSERT");
                }
                
                // 备份卡折业务退保单。
                LIBCertifySchema nLIBCertifySchema = new LIBCertifySchema();
                LIBCertifyDB nLIBCertifyDB = new LIBCertifyDB();
                nLIBCertifyDB.setCardNo(cardNo);
                if (nLIBCertifyDB.getInfo())
                {
                	nLIBCertifySchema = nLIBCertifyDB.query().get(1);
                    nLIBCertifySchema.setCardNo(nLIBCertifySchema.getCardNo()+"b");
                    LOBICertifySchema nLOBICertifySchema = new LOBICertifySchema();
                    String sSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);

                    mReflections.transFields(nLOBICertifySchema, nLIBCertifySchema);
                    nLOBICertifySchema.setSerialNo(sSerielNo);
                    nLOBICertifySchema.setOperator(tGI.Operator);
                    nLOBICertifySchema.setMakeDate(PubFun.getCurrentDate());
                    nLOBICertifySchema.setMakeTime(PubFun.getCurrentTime());

                    if (nLOBICertifySchema != null)
                    {
                        map.put(nLOBICertifySchema, "INSERT");
                    }
                }

                // 删除卡折业务被保人清单表。
                tStrSql = " delete from LICertifyInsured where CardNo = '" + cardNo + "' ";
                map.put(tStrSql, SysConst.DELETE);
                tStrSql = null;
                //备份卡折被保人清单。
                LICertifyInsuredSchema mLICertifyInsuredSchema = new LICertifyInsuredSchema();
                LICertifyInsuredDB mLICertifyInsuredDB = new LICertifyInsuredDB();
                mLICertifyInsuredDB.setCardNo(cardNo);

                LICertifyInsuredSet mLICertifyInsuredSet = new LICertifyInsuredSet();
                LOBICertifyInsuredSet mLOBICertifyInsuredSet = new LOBICertifyInsuredSet();
                mLICertifyInsuredSet = mLICertifyInsuredDB.query();
                if (mLICertifyInsuredSet.size() > 0)
                {
                    for (int i = 1; i <= mLICertifyInsuredSet.size(); i++)
                    {
                        mLICertifyInsuredSchema = mLICertifyInsuredSet.get(i);
                        LOBICertifyInsuredSchema mLOBICertifyInsuredSchema = new LOBICertifyInsuredSchema();
                        String mSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);

                        mReflections.transFields(mLOBICertifyInsuredSchema, mLICertifyInsuredSchema);
                        mLOBICertifyInsuredSchema.setSerialNo(mSerielNo);
                        mLOBICertifyInsuredSchema.setOperator(tGI.Operator);
                        mLOBICertifyInsuredSchema.setMakeDate(PubFun.getCurrentDate());
                        mLOBICertifyInsuredSchema.setMakeTime(PubFun.getCurrentTime());

                        mLOBICertifyInsuredSet.add(mLOBICertifyInsuredSchema);
                    }
                }
                if (mLOBICertifyInsuredSet.size() > 0)
                {
                    map.put(mLOBICertifyInsuredSet, "INSERT");
                }

                //删除卡折业务日志表。
                tStrSql = " update licertifyimportlog set errorstate = '2', Operator = '" + tGI.Operator
                        + "', ModifyDate = Current Date, ModifyTime = Current Time " + " where CardNo = '" + cardNo
                        + "' ";
                map.put(tStrSql, SysConst.UPDATE);
                tStrSql = null;
                // ------------------------------

                // 回置单证状态为：预售。（临时处理方式）
                tStrSql = " select lzc.* from LZCard lzc "
                        + " inner join LMCertifyDes lmcd on lmcd.CertifyCode = lzc.CertifyCode "
                        + " inner join LZCardNumber lzcn on lzc.SubCode = lzcn.CardType and lzc.StartNo = lzcn.CardSerNo and lzc.EndNo = lzcn.CardSerNo "
                        + " inner join LICertify lict on lict.CardNo = lzcn.CardNo "
                        + " where lmcd.CertifyClass = 'D' " + " and lmcd.OperateType in ('0', '1', '2', '3', '4') "
                        + " and lzc.State in ('13', '14') " + " and lict.cardno = '" + cardNo + "'";
                LZCardSet tLZCardSet = new LZCardDB().executeQuery(tStrSql);

                if (tLZCardSet == null || tLZCardSet.size() != 1)
                {
                    buildError("deleteImportByBatchNo", "【" + cardNo + "】单证状态查询失败。");
                    return false;
                }

                System.out.println("tLZCardSet.size():" + tLZCardSet.size());

                for (int i = 1; i <= tLZCardSet.size(); i++)
                {
                    LZCardSchema tLZCardSchema = tLZCardSet.get(i);

                    String tReceiveCom = tLZCardSchema.getReceiveCom();
                    String tCardSendFlag = tReceiveCom.substring(0, 1);

                    // 对于电子商务售出保单，不进行单证状态回置。
                    String tCardState = tLZCardSchema.getState();
                    if("14".equals(tCardState))
                    {
                        System.out.println("单证状态为【14】，无须回置。");
                        continue;
                    }
                    // --------------------

                    if ("D".equals(tCardSendFlag))
                    {
                        tLZCardSchema.setState("10");
                    }
                    else if ("E".equals(tCardSendFlag))
                    {
                        tLZCardSchema.setState("11");
                    }
                    else
                    {
                        buildError("deleteImportByBatchNo", "单证接收机构有误。");
                        return false;
                    }

                    tLZCardSchema.setOperator(tGI.Operator);
                    tLZCardSchema.setModifyDate(PubFun.getCurrentDate());
                    tLZCardSchema.setModifyTime(PubFun.getCurrentTime());

                    if (tCardSendFlag.equals("D"))
                    {
                        map.put("update LZCard set State = '10', Operator = '" + tGI.Operator
                                + "', ModifyDate = Current Date, ModifyTime = Current Time where subcode = '"
                                + tLZCardSchema.getSubCode() + "' and startno = '" + tLZCardSchema.getStartNo() + "' "
                                + " and endno = '" + tLZCardSchema.getEndNo() + "'", "UPDATE");
                    }
                    if ("E".equals(tCardSendFlag))
                    {
                        map.put("update LZCard set State = '11', Operator = '" + tGI.Operator
                                + "', ModifyDate = Current Date, ModifyTime = Current Time where subcode = '"
                                + tLZCardSchema.getSubCode() + "' and startno = '" + tLZCardSchema.getStartNo() + "' "
                                + " and endno = '" + tLZCardSchema.getEndNo() + "'", "UPDATE");
                    }
                }
            }

        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "CertifyImportBatchDelBL";
            tError.functionName = "dealData";
            tError.errorMessage = "不支持的操作类型！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     * 
     * @return boolean
     */
    private boolean prepareOutputData()
    {

        try
        {
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "CertifyImportBatchDelBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "准备传输数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "CertifyImportBatchDelBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
