/**
 * 2009-1-21
 */
package com.sinosoft.lis.certifybusiness;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import com.sinosoft.lis.db.LICardActiveInfoListDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.pubfun.ftp.FTPReplyCodeName;
import com.sinosoft.lis.pubfun.ftp.FTPTool;
import com.sinosoft.lis.schema.LICardActiveInfoListSchema;
import com.sinosoft.lis.vschema.LICardActiveInfoListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CardActiveXlsImportBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mActiveDate = null;

    private LICardActiveInfoListSet mCardInfoList = null;

    private String mCurDate = PubFun.getCurrentDate();

    private String mCurTime = PubFun.getCurrentTime();

    public CardActiveXlsImportBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if (getSubmitMap(cInputData, operate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        // 调用卡单激活导入程序，为保证数据不重复生成导入文件，先修改中间表卡处理状态，然后再进行导入。
        if (!importBatch())
        {
            return false;
        }
        // ---------------------

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String operate)
    {
        if (!getInputData(cInputData, operate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        // 检查卡激活信息完整性。
        if (!chkCardInfoDatas())
        {
            return null;
        }
        // --------------------

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        //        if (mGlobalInput == null)
        //        {
        //            buildError("getInputData", "处理超时，请重新登录。");
        //            return false;
        //        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mActiveDate = (String) mTransferData.getValueByName("ActiveDate");
        if (mActiveDate == null || mActiveDate.equals(""))
        {
            buildError("getInputData", "获取激活日期失败。");
            return false;
        }

        return true;
    }

    /**
     * 校验操作是否合法
     * 1、校验是否录入了GlobalInpt信息，若没有录入，则构造如下信息：
     * Operator = “001”；
     * ComCode = ‘86“
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            mGlobalInput = new GlobalInput();
            mGlobalInput.Operator = "001";
            mGlobalInput.ComCode = "86";
            mGlobalInput.ManageCom = mGlobalInput.ComCode;
        }

        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        mCardInfoList = loadActiveCardInfo();
        if (mCardInfoList == null || mCardInfoList.size() == 0)
        {
            // 如果未查到有待激活的卡信息，直接返回。
            return true;
        }

        // 处理待激活卡单，并生成卡激活导入文件到指定文件夹下。
        tTmpMap = null;
        tTmpMap = dealCardActiveDatas();
        if (tTmpMap == null)
        {
            return false;
        }
        mMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------------------

        return true;
    }

    /**
     * 获取待处理卡激活清单信息
     * @return
     */
    private LICardActiveInfoListSet loadActiveCardInfo()
    {
        String tStrSql = " select * from LICardActiveInfoList "
                + " where DealFlag = '" + CertifyContConst.CA_ACT_INFO_UNDEALED
                + "' " + " and ActiveDate = '" + mActiveDate + "'";

        LICardActiveInfoListSet tCardList = null;
        tCardList = new LICardActiveInfoListDB().executeQuery(tStrSql);

        return tCardList;
    }

    /**
     * <ul>
     * <li>将需要激活的数据根据激活导入模版样式，输出到临时xls中。</li>
     * <li>处理激活数据，对处理成功的数据置（DealFlag）标志为“01－已处理”</li>
     * <li>将生成文件上传到指定ftp目录下。</li>
     * <li>处理成功后，删除临时xls文件。</li>
     * </ul>
     * @return
     */
    private MMap dealCardActiveDatas()
    {
        MMap tMMap = new MMap();

        String tBatchNo = getBatchNo();
        if (tBatchNo == null || tBatchNo.equals(""))
        {
            buildError("getInputData", "获取导入批次号失败。");
            return null;
        }

        WriteToExcel tWriteToExcel = new WriteToExcel("");
        String[] sheetName = { "LICertify", "Ver", "LICertifyInsured" };
        tWriteToExcel.createExcelFile();
        tWriteToExcel.addSheet(sheetName);

        String[] firstRowDatas = { "版本号", "v1.0_1" };
        tWriteToExcel.setRowData(0, 0, firstRowDatas);
        tWriteToExcel.setRowData(1, 0, firstRowDatas);
        tWriteToExcel.setRowData(2, 0, firstRowDatas);

        tWriteToExcel.setRowData(0, 1, new String[] { "序号" });
        tWriteToExcel.setRowData(1, 1, new String[] { "v1.0_1" });
        tWriteToExcel.setRowData(2, 1, new String[] { "序号" });

        int writeNo = 1;
        int j       = 0;

        for (int i = 1; i <= mCardInfoList.size(); i++)
        {
            LICardActiveInfoListSchema tCardInfo = mCardInfoList.get(i);

            tCardInfo.setBatchNo(tBatchNo);
            tCardInfo.setDealFlag(CertifyContConst.CA_ACT_INFO_DEALED);
            tCardInfo.setDealDate(mCurDate);

            tCardInfo.setOperator(mGlobalInput.Operator);
            tCardInfo.setModifyDate(mCurDate);
            tCardInfo.setModifyTime(mCurTime);

            tMMap.put(tCardInfo, SysConst.UPDATE);

            // 卡单信息（LICertify）。
            if ("1".equals(tCardInfo.getSequenceNo()))
            {
            	j++;
                String[] tStrCardInfoDatas = new String[21];
                for (int m = 0; m < tStrCardInfoDatas.length; m++)
                {
                    tStrCardInfoDatas[m] = "";
                }
                tStrCardInfoDatas[0] = String.valueOf(i);
                tStrCardInfoDatas[1] = tCardInfo.getCardNo();
                tStrCardInfoDatas[4] = tCardInfo.getCValidate();
                tStrCardInfoDatas[5] = tCardInfo.getPrem();
                tStrCardInfoDatas[6] = tCardInfo.getAmnt();
                tStrCardInfoDatas[7] = tCardInfo.getMult();
                tStrCardInfoDatas[8] = tCardInfo.getCopys();
                tStrCardInfoDatas[13] = tCardInfo.getActiveDate();
                tStrCardInfoDatas[20] = "01";

                tWriteToExcel.setRowData(0, writeNo + j, tStrCardInfoDatas);
            }
            // -----------------------------------

            // 卡单被保人信息。
            String[] tStrCardInsureInfoDatas = new String[15];
            for (int m = 0; m < tStrCardInsureInfoDatas.length; m++)
            {
                tStrCardInsureInfoDatas[m] = "";
            }
            tStrCardInsureInfoDatas[0] = String.valueOf(i);
            tStrCardInsureInfoDatas[1] = tCardInfo.getCardNo();
            tStrCardInsureInfoDatas[2] = tCardInfo.getName();
            tStrCardInsureInfoDatas[3] = tCardInfo.getSex();
            tStrCardInsureInfoDatas[4] = tCardInfo.getBirthday();
            tStrCardInsureInfoDatas[5] = tCardInfo.getIdType();
            tStrCardInsureInfoDatas[6] = tCardInfo.getIdNo();
            tStrCardInsureInfoDatas[7] = tCardInfo.getOccupationCode();
            tStrCardInsureInfoDatas[8] = tCardInfo.getPostalAddress();
            tStrCardInsureInfoDatas[9] = tCardInfo.getZipCode();
            tStrCardInsureInfoDatas[10] = tCardInfo.getPhone();
            tStrCardInsureInfoDatas[11] = tCardInfo.getMobile();
            tStrCardInsureInfoDatas[12] = tCardInfo.getGrpName();
            tStrCardInsureInfoDatas[13] = tCardInfo.getCompanyPhone();
            tStrCardInsureInfoDatas[14] = tCardInfo.getEMail();

            tWriteToExcel.setRowData(2, writeNo + i, tStrCardInsureInfoDatas);
            // -----------------------------------
        }

        String tUIRoot = CommonBL.getUIRoot();
        String tStrFileName = tUIRoot + "/temp/" + tBatchNo + ".xls";
        try
        {
            tWriteToExcel.write(tStrFileName);
        }
        catch (Exception e)
        {
            buildError("dealCardActiveDatas", "生成文件失败。");
            e.printStackTrace();
            return null;
        }

        tStrFileName = new File(tStrFileName).getPath();

        // 上传文件到Ftp
        if (!sendFile(tStrFileName))
        {
            buildError("getInputData", "上传文件失败。");
            return null;
        }
        // --------------------------------

        // 上传成功后，删除本地临时文件。
        deleteFile(tStrFileName);
        // --------------------------------

        return tMMap;
    }

    private String getBatchNo()
    {
        String tBatchNo = null;
        try
        {
            tBatchNo = PubFun1.CreateMaxNo("LICCARDBNO", mActiveDate);
            System.out.println("BatchNo:" + tBatchNo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

        return tBatchNo;
    }

    private boolean sendFile(String cXlsFilePath)
    {
        ExeSQL tExeSql = new ExeSQL();

        String tServerIP = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'ServerIP'");
        String tPort = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'Port'");
        String tUsername = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'Username'");
        String tPassword = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'Password'");

        String tFilePath = tExeSql
                .getOneValue("select codename from ldcode where codetype = 'cardactimportinfo' and code = 'FtpFilePath'");

        FTPTool tFTPTool = new FTPTool(tServerIP, tUsername, tPassword, Integer
                .parseInt(tPort), "aaActiveMode");
        try
        {
            if (!tFTPTool.loginFTP())
            {
                System.out.println(tFTPTool.getErrContent(1));
            }
            else
            {
                if (!tFTPTool.upload(tFilePath, cXlsFilePath))
                {
                    System.out.println("上载文件失败!");

                    deleteFile(cXlsFilePath);

                    return false;
                }
            }
        }
        catch (SocketException ex)
        {
            ex.printStackTrace();
            System.out.println("上载文件失败，可能是网络异常。");
            return false;
        }
        catch (IOException ex)
        {
            ex.printStackTrace();

            System.out.println("上载文件失败，可能是无法写入文件");
            return false;
        }
        finally
        {
            tFTPTool.logoutFTP();
        }

        return true;
    }

    private boolean deleteFile(String cFilePath)
    {
        File f = new File(cFilePath);
        if (!f.exists())
        {
            return false;
        }
        f.delete();

        return true;
    }

    private boolean importBatch()
    {
        // 调用卡单激活导入程序，进行批次导入
        VData tVData = new VData();

        TransferData tTransferData = new TransferData();

        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        CardActiveBatchImportBL tCardActiveBatchImportBL = new CardActiveBatchImportBL();

        if (!tCardActiveBatchImportBL.submitData(tVData, "ActiveImport"))
        {
            String tStrErr = " 导入清单失败! 原因是: "
                    + tCardActiveBatchImportBL.mErrors.getLastError();
            buildError("dealData", tStrErr);
            System.out.println(tStrErr);
            return false;
        }
        else if (tCardActiveBatchImportBL.mErrors.needDealError())
        {
            String tStrErr = tCardActiveBatchImportBL.mErrors.getErrContent();
            //buildError("dealData", tStrErr);
            System.out.println(tStrErr);
            //return false;
        }
        // --------------------------------

        return true;
    }

    /**
     * 检验待激活卡信息数据是否符合激活最基本要求
     * <br />将不符合的数据在中间表进行标志（DealFlag），暂不处理
     * @return
     */
    private boolean chkCardInfoDatas()
    {
        mCardInfoList = loadActiveCardInfo();
        if (mCardInfoList == null || mCardInfoList.size() == 0)
        {
            // 如果未查到有待激活的卡信息，直接返回。
            return true;
        }

        // 调用卡单激活导入程序，进行批次导入
        VData tVData = new VData();

        TransferData tTransferData = new TransferData();

        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        tVData.add(mCardInfoList);

        CardActImpChkBL tCardActImpChkBL = new CardActImpChkBL();

        if (!tCardActImpChkBL.submitData(tVData, "Check"))
        {
            String tStrErr = "校验卡折待激活信息数据处理时异常! 原因是: "
                    + tCardActImpChkBL.mErrors.getLastError();
            buildError("chkCardInfoDatas", tStrErr);
            System.out.println(tStrErr);
            return false;
        }
        else if (tCardActImpChkBL.mErrors.needDealError())
        {
            String tStrErr = tCardActImpChkBL.mErrors.getErrContent();
            System.out.println(tStrErr);
        }
        // --------------------------------

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AddCertifyListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
