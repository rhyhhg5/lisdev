/**
 * created 2009-1-22
 * by LY
 */
package com.sinosoft.lis.certifybusiness;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class CardActiveRenewUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public CardActiveRenewUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	CardActiveRenewBL tCardActiveRenewBL = new CardActiveRenewBL();
            if (!tCardActiveRenewBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tCardActiveRenewBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
