package com.sinosoft.lis.bdyz;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.IAC_LDPERSONSchema;
import com.sinosoft.lis.vschema.IAC_LDPERSONSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.VData;

public class DealBdyzDataOfAllIDNo {

	private MMap mMMap = new MMap();
	
	private VData mVData = new VData();
	
	public void update(String cYesterDay)
	{
		String tBatchNo = cYesterDay.replace("-", "") + "001";
		String strSQL = "select * from iac_ldperson where  batchno ='"+tBatchNo+"' and groupno ='0'  with ur";
		IAC_LDPERSONSet tIAC_LDPERSONSet = new IAC_LDPERSONSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tIAC_LDPERSONSet, strSQL);
		IAC_LDPERSONSet tUpdateIAC_LDPERSONSet = new IAC_LDPERSONSet();
		ExeSQL tExeSQL = new ExeSQL();
		do
		{
			tRSWrapper.getData();
			for(int i =1 ;i <= tIAC_LDPERSONSet.size();i++)
			{
				IAC_LDPERSONSchema tIAC_LDPERSONSchema = new IAC_LDPERSONSchema();
				tIAC_LDPERSONSchema = tIAC_LDPERSONSet.get(i);
				String tJudgeSql  = "select distinct  1  FROM lcpol c, lcinsured d  WHERE c.grpcontno = d.grpcontno  and c.InsuredNo = d.InsuredNo "
					+ " and c.conttype in ('1', '2') and c.stateflag = '1'  and c.appflag = '1'  and d.idtype = '0' and   d.idno = '"+tIAC_LDPERSONSchema.getIdNo()+"'  ";
				String tResult = tExeSQL.getOneValue(tJudgeSql);
				if("1".equals(tResult))
				{
					tIAC_LDPERSONSchema.setGroupNo("1");
				}
				else
				{
					tIAC_LDPERSONSchema.setGroupNo("2");
				}
				tUpdateIAC_LDPERSONSet.add(tIAC_LDPERSONSchema);
			}
			mMMap.put(tUpdateIAC_LDPERSONSet, "UPDATE");
			mVData.add(mMMap);
			PubSubmit tPubSubmit = new PubSubmit();
			tPubSubmit.submitData(mVData, "");
			tUpdateIAC_LDPERSONSet =  new IAC_LDPERSONSet();
			mMMap = new MMap();
			mVData.clear();
		}while(tIAC_LDPERSONSet.size()>0);
	}
}
