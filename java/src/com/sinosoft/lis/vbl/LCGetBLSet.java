/*
 * <p>ClassName: LCGetBLSet </p>
 * <p>Description: LCGetSetBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-04-01
 */
package com.sinosoft.lis.vbl;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LCGetBLSet extends LCGetSet
{
	// @Constructor
	public LCGetBLSet() {}

  /**
   * 设置该集合中的所有保单
   * @param cPolNo
   */
  public void setPolNo(String cPolNo)
  {
    int i,iMax;
    for(i=1;i<this.size() ;i++)
    {
      this.get(i).setPolNo(cPolNo);
    }
  }


}
