/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCTempCustomerDetailSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LCTempCustomerDetailSet </p>
 * <p>Description: LCTempCustomerDetailSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 集团客户提数数据临时表
 * @CreateDate：2011-10-17
 */
public class LCTempCustomerDetailSet extends SchemaSet
{
	// @Method
	public boolean add(LCTempCustomerDetailSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LCTempCustomerDetailSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LCTempCustomerDetailSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LCTempCustomerDetailSchema get(int index)
	{
		LCTempCustomerDetailSchema tSchema = (LCTempCustomerDetailSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LCTempCustomerDetailSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LCTempCustomerDetailSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCTempCustomerDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LCTempCustomerDetailSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LCTempCustomerDetailSchema aSchema = new LCTempCustomerDetailSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LCTempCustomerDetailSchema tSchema = new LCTempCustomerDetailSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
