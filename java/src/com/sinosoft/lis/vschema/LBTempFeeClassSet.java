/*
 * <p>ClassName: LBTempFeeClassSet </p>
 * <p>Description: LBTempFeeClassSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LBTempFeeClassSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LBTempFeeClassSet extends SchemaSet
{
    // @Method
    public boolean add(LBTempFeeClassSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LBTempFeeClassSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LBTempFeeClassSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LBTempFeeClassSchema get(int index)
    {
        LBTempFeeClassSchema tSchema = (LBTempFeeClassSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LBTempFeeClassSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LBTempFeeClassSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBTempFeeClass描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LBTempFeeClassSchema aSchema = (LBTempFeeClassSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LBTempFeeClassSchema aSchema = new LBTempFeeClassSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LBTempFeeClassSchema tSchema = new LBTempFeeClassSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
