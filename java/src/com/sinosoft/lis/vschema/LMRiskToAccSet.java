/*
 * <p>ClassName: LMRiskToAccSet </p>
 * <p>Description: LMRiskToAccSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LMRiskToAccSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LMRiskToAccSet extends SchemaSet
{
    // @Method
    public boolean add(LMRiskToAccSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LMRiskToAccSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LMRiskToAccSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LMRiskToAccSchema get(int index)
    {
        LMRiskToAccSchema tSchema = (LMRiskToAccSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LMRiskToAccSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LMRiskToAccSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskToAcc描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LMRiskToAccSchema aSchema = (LMRiskToAccSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LMRiskToAccSchema aSchema = new LMRiskToAccSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LMRiskToAccSchema tSchema = new LMRiskToAccSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
