/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.lknorealtimerecordingSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: lknorealtimerecordingSet </p>
 * <p>Description: lknorealtimerecordingSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2019-06-26
 */
public class lknorealtimerecordingSet extends SchemaSet
{
	// @Method
	public boolean add(lknorealtimerecordingSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(lknorealtimerecordingSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(lknorealtimerecordingSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public lknorealtimerecordingSchema get(int index)
	{
		lknorealtimerecordingSchema tSchema = (lknorealtimerecordingSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, lknorealtimerecordingSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(lknorealtimerecordingSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prplknorealtimerecording描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			lknorealtimerecordingSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			lknorealtimerecordingSchema aSchema = new lknorealtimerecordingSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		lknorealtimerecordingSchema tSchema = new lknorealtimerecordingSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
