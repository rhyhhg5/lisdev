/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.IAC_PERSON_INDEXSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: IAC_PERSON_INDEXSet </p>
 * <p>Description: IAC_PERSON_INDEXSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-05-23
 */
public class IAC_PERSON_INDEXSet extends SchemaSet
{
	// @Method
	public boolean add(IAC_PERSON_INDEXSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(IAC_PERSON_INDEXSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(IAC_PERSON_INDEXSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public IAC_PERSON_INDEXSchema get(int index)
	{
		IAC_PERSON_INDEXSchema tSchema = (IAC_PERSON_INDEXSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, IAC_PERSON_INDEXSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(IAC_PERSON_INDEXSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpIAC_PERSON_INDEX描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			IAC_PERSON_INDEXSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			IAC_PERSON_INDEXSchema aSchema = new IAC_PERSON_INDEXSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		IAC_PERSON_INDEXSchema tSchema = new IAC_PERSON_INDEXSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
