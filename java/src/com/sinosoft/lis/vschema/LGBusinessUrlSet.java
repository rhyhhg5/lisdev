/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LGBusinessUrlSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LGBusinessUrlSet </p>
 * <p>Description: LGBusinessUrlSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工单管理
 * @CreateDate：2005-03-02
 */
public class LGBusinessUrlSet extends SchemaSet
{
    // @Method
    public boolean add(LGBusinessUrlSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LGBusinessUrlSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LGBusinessUrlSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LGBusinessUrlSchema get(int index)
    {
        LGBusinessUrlSchema tSchema = (LGBusinessUrlSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LGBusinessUrlSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LGBusinessUrlSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGBusinessUrl描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LGBusinessUrlSchema aSchema = (LGBusinessUrlSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LGBusinessUrlSchema aSchema = new LGBusinessUrlSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LGBusinessUrlSchema tSchema = new LGBusinessUrlSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
