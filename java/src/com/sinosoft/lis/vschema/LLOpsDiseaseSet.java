/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LLOpsDiseaseSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLOpsDiseaseSet </p>
 * <p>Description: LLOpsDiseaseSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2016-01-18
 */
public class LLOpsDiseaseSet extends SchemaSet
{
	// @Method
	public boolean add(LLOpsDiseaseSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LLOpsDiseaseSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LLOpsDiseaseSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LLOpsDiseaseSchema get(int index)
	{
		LLOpsDiseaseSchema tSchema = (LLOpsDiseaseSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LLOpsDiseaseSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LLOpsDiseaseSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLOpsDisease描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LLOpsDiseaseSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LLOpsDiseaseSchema aSchema = new LLOpsDiseaseSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LLOpsDiseaseSchema tSchema = new LLOpsDiseaseSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
