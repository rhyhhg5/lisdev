/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCTempCustomerDetail2008Schema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LCTempCustomerDetail2008Set </p>
 * <p>Description: LCTempCustomerDetail2008SchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 集团客户提数数据临时表
 * @CreateDate：2011-10-17
 */
public class LCTempCustomerDetail2008Set extends SchemaSet
{
	// @Method
	public boolean add(LCTempCustomerDetail2008Schema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LCTempCustomerDetail2008Set aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LCTempCustomerDetail2008Schema aSchema)
	{
		return super.remove(aSchema);
	}

	public LCTempCustomerDetail2008Schema get(int index)
	{
		LCTempCustomerDetail2008Schema tSchema = (LCTempCustomerDetail2008Schema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LCTempCustomerDetail2008Schema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LCTempCustomerDetail2008Set aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCTempCustomerDetail2008描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LCTempCustomerDetail2008Schema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LCTempCustomerDetail2008Schema aSchema = new LCTempCustomerDetail2008Schema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LCTempCustomerDetail2008Schema tSchema = new LCTempCustomerDetail2008Schema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
