/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.XFYJAppntSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: XFYJAppntSet </p>
 * <p>Description: XFYJAppntSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 幸福一家数据存储表
 * @CreateDate：2017-09-13
 */
public class XFYJAppntSet extends SchemaSet
{
	// @Method
	public boolean add(XFYJAppntSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(XFYJAppntSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(XFYJAppntSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public XFYJAppntSchema get(int index)
	{
		XFYJAppntSchema tSchema = (XFYJAppntSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, XFYJAppntSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(XFYJAppntSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpXFYJAppnt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			XFYJAppntSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			XFYJAppntSchema aSchema = new XFYJAppntSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		XFYJAppntSchema tSchema = new XFYJAppntSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
