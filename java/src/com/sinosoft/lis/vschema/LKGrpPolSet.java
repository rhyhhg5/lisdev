/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LKGrpPolSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LKGrpPolSet </p>
 * <p>Description: LKGrpPolSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 在线报价系统模型
 * @CreateDate：2019-09-12
 */
public class LKGrpPolSet extends SchemaSet
{
	// @Method
	public boolean add(LKGrpPolSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LKGrpPolSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LKGrpPolSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LKGrpPolSchema get(int index)
	{
		LKGrpPolSchema tSchema = (LKGrpPolSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LKGrpPolSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LKGrpPolSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKGrpPol描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LKGrpPolSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LKGrpPolSchema aSchema = new LKGrpPolSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LKGrpPolSchema tSchema = new LKGrpPolSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
