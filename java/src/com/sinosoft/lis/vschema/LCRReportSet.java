/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCRReportSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LCRReportSet </p>
 * <p>Description: LCRReportSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 生调添加生调原因字段
 * @CreateDate：2005-03-24
 */
public class LCRReportSet extends SchemaSet
{
    // @Method
    public boolean add(LCRReportSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LCRReportSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LCRReportSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LCRReportSchema get(int index)
    {
        LCRReportSchema tSchema = (LCRReportSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LCRReportSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LCRReportSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCRReport描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LCRReportSchema aSchema = (LCRReportSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: str String 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LCRReportSchema aSchema = new LCRReportSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LCRReportSchema tSchema = new LCRReportSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
