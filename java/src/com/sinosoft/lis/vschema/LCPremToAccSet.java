/*
 * <p>ClassName: LCPremToAccSet </p>
 * <p>Description: LCPremToAccSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新系统模型
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCPremToAccSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LCPremToAccSet extends SchemaSet
{
    // @Method
    public boolean add(LCPremToAccSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LCPremToAccSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LCPremToAccSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LCPremToAccSchema get(int index)
    {
        LCPremToAccSchema tSchema = (LCPremToAccSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LCPremToAccSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LCPremToAccSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPremToAcc描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LCPremToAccSchema aSchema = (LCPremToAccSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LCPremToAccSchema aSchema = new LCPremToAccSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LCPremToAccSchema tSchema = new LCPremToAccSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
