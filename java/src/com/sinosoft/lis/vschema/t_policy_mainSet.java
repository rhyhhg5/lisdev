/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.t_policy_mainSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: t_policy_mainSet </p>
 * <p>Description: t_policy_mainSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-07-28
 */
public class t_policy_mainSet extends SchemaSet
{
	// @Method
	public boolean add(t_policy_mainSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(t_policy_mainSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(t_policy_mainSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public t_policy_mainSchema get(int index)
	{
		t_policy_mainSchema tSchema = (t_policy_mainSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, t_policy_mainSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(t_policy_mainSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpt_policy_main描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			t_policy_mainSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			t_policy_mainSchema aSchema = new t_policy_mainSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		t_policy_mainSchema tSchema = new t_policy_mainSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
