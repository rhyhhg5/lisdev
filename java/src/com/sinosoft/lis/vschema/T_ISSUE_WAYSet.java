/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.midplat.midplatDB.schema.T_ISSUE_WAYSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: T_ISSUE_WAYSet </p>
 * <p>Description: T_ISSUE_WAYSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 产品化中间业务平台
 * @CreateDate：2007-04-17
 */
public class T_ISSUE_WAYSet extends SchemaSet
{
	// @Method
	public boolean add(T_ISSUE_WAYSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(T_ISSUE_WAYSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(T_ISSUE_WAYSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public T_ISSUE_WAYSchema get(int index)
	{
		T_ISSUE_WAYSchema tSchema = (T_ISSUE_WAYSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, T_ISSUE_WAYSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(T_ISSUE_WAYSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpT_ISSUE_WAY描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			T_ISSUE_WAYSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			T_ISSUE_WAYSchema aSchema = new T_ISSUE_WAYSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		T_ISSUE_WAYSchema tSchema = new T_ISSUE_WAYSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
