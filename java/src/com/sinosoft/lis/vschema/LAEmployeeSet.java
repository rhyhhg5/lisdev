/*
 * <p>ClassName: LAEmployeeSet </p>
 * <p>Description: LAEmployeeSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LAEmployeeSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LAEmployeeSet extends SchemaSet
{
    // @Method
    public boolean add(LAEmployeeSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LAEmployeeSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LAEmployeeSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LAEmployeeSchema get(int index)
    {
        LAEmployeeSchema tSchema = (LAEmployeeSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LAEmployeeSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LAEmployeeSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAEmployee描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAEmployeeSchema aSchema = (LAEmployeeSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LAEmployeeSchema aSchema = new LAEmployeeSchema();
            if (!aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LAEmployeeSchema tSchema = new LAEmployeeSchema();
        if (!tSchema.decode(str.substring(nBeginPos)))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
