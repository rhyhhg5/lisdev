/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.ES_DOC_HANDLERSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: ES_DOC_HANDLERSet </p>
 * <p>Description: ES_DOC_HANDLERSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 影像件异步处理表
 * @CreateDate：2018-07-19
 */
public class ES_DOC_HANDLERSet extends SchemaSet
{
	// @Method
	public boolean add(ES_DOC_HANDLERSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(ES_DOC_HANDLERSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(ES_DOC_HANDLERSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public ES_DOC_HANDLERSchema get(int index)
	{
		ES_DOC_HANDLERSchema tSchema = (ES_DOC_HANDLERSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, ES_DOC_HANDLERSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(ES_DOC_HANDLERSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_DOC_HANDLER描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			ES_DOC_HANDLERSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			ES_DOC_HANDLERSchema aSchema = new ES_DOC_HANDLERSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		ES_DOC_HANDLERSchema tSchema = new ES_DOC_HANDLERSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
