/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LRAccountsSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LRAccountsSet </p>
 * <p>Description: LRAccountsSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 再保财务接口
 * @CreateDate：2008-07-11
 */
public class LRAccountsSet extends SchemaSet
{
	// @Method
	public boolean add(LRAccountsSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LRAccountsSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LRAccountsSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LRAccountsSchema get(int index)
	{
		LRAccountsSchema tSchema = (LRAccountsSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LRAccountsSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LRAccountsSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRAccounts描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LRAccountsSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LRAccountsSchema aSchema = new LRAccountsSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LRAccountsSchema tSchema = new LRAccountsSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
