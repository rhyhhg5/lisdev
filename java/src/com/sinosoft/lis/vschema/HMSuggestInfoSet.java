/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.HMSuggestInfoSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: HMSuggestInfoSet </p>
 * <p>Description: HMSuggestInfoSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-18
 */
public class HMSuggestInfoSet extends SchemaSet
{
	// @Method
	public boolean add(HMSuggestInfoSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(HMSuggestInfoSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(HMSuggestInfoSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public HMSuggestInfoSchema get(int index)
	{
		HMSuggestInfoSchema tSchema = (HMSuggestInfoSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, HMSuggestInfoSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(HMSuggestInfoSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMSuggestInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			HMSuggestInfoSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			HMSuggestInfoSchema aSchema = new HMSuggestInfoSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		HMSuggestInfoSchema tSchema = new HMSuggestInfoSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
