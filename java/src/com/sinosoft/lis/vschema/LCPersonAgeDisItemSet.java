/*
 * <p>ClassName: LCPersonAgeDisItemSet </p>
 * <p>Description: LCPersonAgeDisItemSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 询价管理－被保人年龄分布表
 * @CreateDate：2005-01-22
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCPersonAgeDisItemSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LCPersonAgeDisItemSet extends SchemaSet
{
    // @Method
    public boolean add(LCPersonAgeDisItemSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LCPersonAgeDisItemSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LCPersonAgeDisItemSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LCPersonAgeDisItemSchema get(int index)
    {
        LCPersonAgeDisItemSchema tSchema = (LCPersonAgeDisItemSchema)super.
                                           getObj(index);
        return tSchema;
    }

    public boolean set(int index, LCPersonAgeDisItemSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LCPersonAgeDisItemSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPersonAgeDisItem描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LCPersonAgeDisItemSchema aSchema = (LCPersonAgeDisItemSchema)this.
                                               get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LCPersonAgeDisItemSchema aSchema = new LCPersonAgeDisItemSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LCPersonAgeDisItemSchema tSchema = new LCPersonAgeDisItemSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
