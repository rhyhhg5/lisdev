/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCGrpPremPlanSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LCGrpPremPlanSet </p>
 * <p>Description: LCGrpPremPlanSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 大团单业务
 * @CreateDate：2013-06-20
 */
public class LCGrpPremPlanSet extends SchemaSet
{
	// @Method
	public boolean add(LCGrpPremPlanSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LCGrpPremPlanSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LCGrpPremPlanSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LCGrpPremPlanSchema get(int index)
	{
		LCGrpPremPlanSchema tSchema = (LCGrpPremPlanSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LCGrpPremPlanSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LCGrpPremPlanSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpPremPlan描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LCGrpPremPlanSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LCGrpPremPlanSchema aSchema = new LCGrpPremPlanSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LCGrpPremPlanSchema tSchema = new LCGrpPremPlanSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
