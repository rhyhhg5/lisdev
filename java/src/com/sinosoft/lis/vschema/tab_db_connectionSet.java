/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.tab_db_connectionSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: tab_db_connectionSet </p>
 * <p>Description: tab_db_connectionSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 产品化中间业务平台
 * @CreateDate：2007-04-17
 */
public class tab_db_connectionSet extends SchemaSet
{
	// @Method
	public boolean add(tab_db_connectionSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(tab_db_connectionSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(tab_db_connectionSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public tab_db_connectionSchema get(int index)
	{
		tab_db_connectionSchema tSchema = (tab_db_connectionSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, tab_db_connectionSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(tab_db_connectionSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prptab_db_connection描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			tab_db_connectionSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			tab_db_connectionSchema aSchema = new tab_db_connectionSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		tab_db_connectionSchema tSchema = new tab_db_connectionSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
