/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.ES_DOC_RELATIONSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: ES_DOC_RELATIONSet </p>
 * <p>Description: ES_DOC_RELATIONSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 单证业务关联表
 * @CreateDate：2005-02-22
 */
public class ES_DOC_RELATIONSet extends SchemaSet
{
    // @Method
    public boolean add(ES_DOC_RELATIONSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(ES_DOC_RELATIONSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(ES_DOC_RELATIONSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public ES_DOC_RELATIONSchema get(int index)
    {
        ES_DOC_RELATIONSchema tSchema = (ES_DOC_RELATIONSchema)super.getObj(
                index);
        return tSchema;
    }

    public boolean set(int index, ES_DOC_RELATIONSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(ES_DOC_RELATIONSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_DOC_RELATION描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            ES_DOC_RELATIONSchema aSchema = (ES_DOC_RELATIONSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            ES_DOC_RELATIONSchema aSchema = new ES_DOC_RELATIONSchema();
            if (!aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        ES_DOC_RELATIONSchema tSchema = new ES_DOC_RELATIONSchema();
        if (!tSchema.decode(str.substring(nBeginPos)))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
