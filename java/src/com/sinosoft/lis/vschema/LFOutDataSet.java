/*
 * <p>ClassName: LFOutDataSet </p>
 * <p>Description: LFOutDataSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LFOutDataSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LFOutDataSet extends SchemaSet
{
    // @Method
    public boolean add(LFOutDataSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LFOutDataSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LFOutDataSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LFOutDataSchema get(int index)
    {
        LFOutDataSchema tSchema = (LFOutDataSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LFOutDataSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LFOutDataSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFOutData描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LFOutDataSchema aSchema = (LFOutDataSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LFOutDataSchema aSchema = new LFOutDataSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LFOutDataSchema tSchema = new LFOutDataSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
