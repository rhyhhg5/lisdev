/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.Interest000001Schema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: Interest000001Set </p>
 * <p>Description: Interest000001SchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 万能险业务
 * @CreateDate：2011-01-27
 */
public class Interest000001Set extends SchemaSet
{
	// @Method
	public boolean add(Interest000001Schema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(Interest000001Set aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(Interest000001Schema aSchema)
	{
		return super.remove(aSchema);
	}

	public Interest000001Schema get(int index)
	{
		Interest000001Schema tSchema = (Interest000001Schema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, Interest000001Schema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(Interest000001Set aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpInterest000001描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			Interest000001Schema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			Interest000001Schema aSchema = new Interest000001Schema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		Interest000001Schema tSchema = new Interest000001Schema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
