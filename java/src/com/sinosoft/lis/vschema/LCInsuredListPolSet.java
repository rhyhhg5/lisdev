/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCInsuredListPolSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LCInsuredListPolSet </p>
 * <p>Description: LCInsuredListPolSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-10-24
 */
public class LCInsuredListPolSet extends SchemaSet {
    // @Method
    public boolean add(LCInsuredListPolSchema aSchema) {
        return super.add(aSchema);
    }

    public boolean add(LCInsuredListPolSet aSet) {
        return super.add(aSet);
    }

    public boolean remove(LCInsuredListPolSchema aSchema) {
        return super.remove(aSchema);
    }

    public LCInsuredListPolSchema get(int index) {
        LCInsuredListPolSchema tSchema = (LCInsuredListPolSchema)super.getObj(
                index);
        return tSchema;
    }

    public boolean set(int index, LCInsuredListPolSchema aSchema) {
        return super.set(index, aSchema);
    }

    public boolean set(LCInsuredListPolSet aSet) {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsuredListPol描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer("");
        int n = this.size();
        for (int i = 1; i <= n; i++) {
            LCInsuredListPolSchema aSchema = this.get(i);
            strReturn.append(aSchema.encode());
            if (i != n) {
                strReturn.append(SysConst.RECORDSPLITER);
            }
        }

        return strReturn.toString();
    }

    /**
     * 数据解包
     * @param: str String 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str) {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1) {
            LCInsuredListPolSchema aSchema = new LCInsuredListPolSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
                this.add(aSchema);
                nBeginPos = nEndPos + 1;
                nEndPos = str.indexOf('^', nEndPos + 1);
            } else {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
        }
        LCInsuredListPolSchema tSchema = new LCInsuredListPolSchema();
        if (tSchema.decode(str.substring(nBeginPos))) {
            this.add(tSchema);
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
    }

}
