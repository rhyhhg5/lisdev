/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LHServCostSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LHServCostSet </p>
 * <p>Description: LHServCostSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-03-06
 */
public class LHServCostSet extends SchemaSet
{
	// @Method
	public boolean add(LHServCostSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LHServCostSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LHServCostSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LHServCostSchema get(int index)
	{
		LHServCostSchema tSchema = (LHServCostSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LHServCostSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LHServCostSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServCost描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LHServCostSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LHServCostSchema aSchema = new LHServCostSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LHServCostSchema tSchema = new LHServCostSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
