/*
 * <p>ClassName: LAComTransLogSet </p>
 * <p>Description: LAComTransLogSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LAComTransLogSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LAComTransLogSet extends SchemaSet
{
    // @Method
    public boolean add(LAComTransLogSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LAComTransLogSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LAComTransLogSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LAComTransLogSchema get(int index)
    {
        LAComTransLogSchema tSchema = (LAComTransLogSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LAComTransLogSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LAComTransLogSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAComTransLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAComTransLogSchema aSchema = (LAComTransLogSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LAComTransLogSchema aSchema = new LAComTransLogSchema();
            if (!aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LAComTransLogSchema tSchema = new LAComTransLogSchema();
        if (!tSchema.decode(str.substring(nBeginPos)))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
