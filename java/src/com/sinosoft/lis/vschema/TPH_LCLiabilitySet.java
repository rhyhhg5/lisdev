/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.TPH_LCLiabilitySchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: TPH_LCLiabilitySet </p>
 * <p>Description: TPH_LCLiabilitySchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: SY
 * @CreateDate：2015-11-02
 */
public class TPH_LCLiabilitySet extends SchemaSet
{
	// @Method
	public boolean add(TPH_LCLiabilitySchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(TPH_LCLiabilitySet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(TPH_LCLiabilitySchema aSchema)
	{
		return super.remove(aSchema);
	}

	public TPH_LCLiabilitySchema get(int index)
	{
		TPH_LCLiabilitySchema tSchema = (TPH_LCLiabilitySchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, TPH_LCLiabilitySchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(TPH_LCLiabilitySet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LCLiability描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			TPH_LCLiabilitySchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			TPH_LCLiabilitySchema aSchema = new TPH_LCLiabilitySchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		TPH_LCLiabilitySchema tSchema = new TPH_LCLiabilitySchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
