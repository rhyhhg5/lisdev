/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LPUWConfirmInfo2Schema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LPUWConfirmInfo2Set </p>
 * <p>Description: LPUWConfirmInfo2SchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 万能险
 * @CreateDate：2007-11-15
 */
public class LPUWConfirmInfo2Set extends SchemaSet
{
	// @Method
	public boolean add(LPUWConfirmInfo2Schema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LPUWConfirmInfo2Set aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LPUWConfirmInfo2Schema aSchema)
	{
		return super.remove(aSchema);
	}

	public LPUWConfirmInfo2Schema get(int index)
	{
		LPUWConfirmInfo2Schema tSchema = (LPUWConfirmInfo2Schema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LPUWConfirmInfo2Schema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LPUWConfirmInfo2Set aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPUWConfirmInfo2描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LPUWConfirmInfo2Schema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LPUWConfirmInfo2Schema aSchema = new LPUWConfirmInfo2Schema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LPUWConfirmInfo2Schema tSchema = new LPUWConfirmInfo2Schema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
