/*
 * <p>ClassName: tab_riskcode_covertSet </p>
 * <p>Description: tab_riskcode_covertSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 产品化中间业务平台
 * @CreateDate：2007-07-06
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.tab_riskcode_covertSchema;
import com.sinosoft.utility.*;

public class tab_riskcode_covertSet extends SchemaSet
{
	// @Method
	public boolean add(tab_riskcode_covertSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(tab_riskcode_covertSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(tab_riskcode_covertSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public tab_riskcode_covertSchema get(int index)
	{
		tab_riskcode_covertSchema tSchema = (tab_riskcode_covertSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, tab_riskcode_covertSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(tab_riskcode_covertSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prptab_riskcode_covert描述/A>表字段
	* @param: 无
	* @return: 返回打包后字符串
	**/
	public String encode()
	{
		String strReturn = "";
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			tab_riskcode_covertSchema aSchema = (tab_riskcode_covertSchema)this.get(i);
			strReturn += aSchema.encode();
			if( i != n ) strReturn += SysConst.RECORDSPLITER;
		}

		return strReturn;
	}

	/**
	* 数据解包
	* @param: 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			tab_riskcode_covertSchema aSchema = new tab_riskcode_covertSchema();
			if( aSchema.decode(str.substring(nBeginPos, nEndPos)) == false )
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
		}
		tab_riskcode_covertSchema tSchema = new tab_riskcode_covertSchema();
		if( tSchema.decode(str.substring(nBeginPos)) == false )
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
		this.add(tSchema);

		return true;
	}

}
