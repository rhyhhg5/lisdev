/*
 * <p>ClassName: LOBGrpFeeParam2Set </p>
 * <p>Description: LOBGrpFeeParam2SchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 核心表变更
 * @CreateDate：2004-12-06
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LOBGrpFeeParam2Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LOBGrpFeeParam2Set extends SchemaSet
{
    // @Method
    public boolean add(LOBGrpFeeParam2Schema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LOBGrpFeeParam2Set aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LOBGrpFeeParam2Schema aSchema)
    {
        return super.remove(aSchema);
    }

    public LOBGrpFeeParam2Schema get(int index)
    {
        LOBGrpFeeParam2Schema tSchema = (LOBGrpFeeParam2Schema)super.getObj(
                index);
        return tSchema;
    }

    public boolean set(int index, LOBGrpFeeParam2Schema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LOBGrpFeeParam2Set aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBGrpFeeParam2描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LOBGrpFeeParam2Schema aSchema = (LOBGrpFeeParam2Schema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LOBGrpFeeParam2Schema aSchema = new LOBGrpFeeParam2Schema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LOBGrpFeeParam2Schema tSchema = new LOBGrpFeeParam2Schema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
