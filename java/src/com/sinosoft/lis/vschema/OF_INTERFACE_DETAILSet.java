/**
 * Copyright (c) 2006 Sinosoft Co.,LTD.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.OF_INTERFACE_DETAILSchema;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: OF_INTERFACE_DETAILSet </p>
 * <p>Description: OF_INTERFACE_DETAILSchemaSet类文件 </p>
 * <p>Company: Sinosoft Co.,LTD </p>
 * @Database: PhysicalDataModel_1
 * @author：Makerx
 * @CreateDate：2007-12-19
 */
public class OF_INTERFACE_DETAILSet extends SchemaSet
{
    // @Method
    public boolean add(OF_INTERFACE_DETAILSchema schema)
    {
        return super.add(schema);
    }

    public boolean add(OF_INTERFACE_DETAILSet set)
    {
        return super.add(set);
    }

    public boolean remove(OF_INTERFACE_DETAILSchema schema)
    {
        return super.remove(schema);
    }

    public OF_INTERFACE_DETAILSchema get(int index)
    {
        OF_INTERFACE_DETAILSchema schema = (OF_INTERFACE_DETAILSchema)super.getObj(index);
        return schema;
    }

    public boolean set(int index, OF_INTERFACE_DETAILSchema schema)
    {
        return super.set(index, schema);
    }

    public boolean set(OF_INTERFACE_DETAILSet set)
    {
        return super.set(set);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpOF_INTERFACE_DETAIL描述/A>表字段
     * @return: String 返回打包后字符串
     */
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer("");
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            OF_INTERFACE_DETAILSchema schema = this.get(i);
            strReturn.append(schema.encode());
            if (i != n)
                strReturn.append(SysConst.RECORDSPLITER);
        }
        return strReturn.toString();
    }

    /**
     * 数据解包
     * @param: str String 打包后字符串
     * @return: boolean
     */
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();
        while (nEndPos != -1)
        {
            OF_INTERFACE_DETAILSchema aSchema = new OF_INTERFACE_DETAILSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                this.add(aSchema);
                nBeginPos = nEndPos + 1;
                nEndPos = str.indexOf('^', nEndPos + 1);
            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
        }
        OF_INTERFACE_DETAILSchema tSchema = new OF_INTERFACE_DETAILSchema();
        if (tSchema.decode(str.substring(nBeginPos)))
        {
            this.add(tSchema);
            return true;
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
    }
}
