/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LAAgentPromRadix5Schema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAgentPromRadix5Set </p>
 * <p>Description: LAAgentPromRadix5SchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 互动渠道考核标准录入
 * @CreateDate：2015-01-13
 */
public class LAAgentPromRadix5Set extends SchemaSet
{
	// @Method
	public boolean add(LAAgentPromRadix5Schema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LAAgentPromRadix5Set aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LAAgentPromRadix5Schema aSchema)
	{
		return super.remove(aSchema);
	}

	public LAAgentPromRadix5Schema get(int index)
	{
		LAAgentPromRadix5Schema tSchema = (LAAgentPromRadix5Schema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LAAgentPromRadix5Schema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LAAgentPromRadix5Set aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentPromRadix5描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LAAgentPromRadix5Schema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LAAgentPromRadix5Schema aSchema = new LAAgentPromRadix5Schema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LAAgentPromRadix5Schema tSchema = new LAAgentPromRadix5Schema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
