/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.Crs_CheckResultLogSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: Crs_CheckResultLogSet </p>
 * <p>Description: Crs_CheckResultLogSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 卡式业务
 * @CreateDate：2010-07-09
 */
public class Crs_CheckResultLogSet extends SchemaSet
{
	// @Method
	public boolean add(Crs_CheckResultLogSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(Crs_CheckResultLogSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(Crs_CheckResultLogSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public Crs_CheckResultLogSchema get(int index)
	{
		Crs_CheckResultLogSchema tSchema = (Crs_CheckResultLogSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, Crs_CheckResultLogSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(Crs_CheckResultLogSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCrs_CheckResultLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			Crs_CheckResultLogSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			Crs_CheckResultLogSchema aSchema = new Crs_CheckResultLogSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		Crs_CheckResultLogSchema tSchema = new Crs_CheckResultLogSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
