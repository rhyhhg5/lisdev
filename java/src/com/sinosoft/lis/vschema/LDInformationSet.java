/*
 * <p>ClassName: LDInformationSet </p>
 * <p>Description: LDInformationSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 团单补充资料信息表
 * @CreateDate：2005-01-20
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LDInformationSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LDInformationSet extends SchemaSet
{
    // @Method
    public boolean add(LDInformationSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LDInformationSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LDInformationSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LDInformationSchema get(int index)
    {
        LDInformationSchema tSchema = (LDInformationSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LDInformationSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LDInformationSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDInformation描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LDInformationSchema aSchema = (LDInformationSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LDInformationSchema aSchema = new LDInformationSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LDInformationSchema tSchema = new LDInformationSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
