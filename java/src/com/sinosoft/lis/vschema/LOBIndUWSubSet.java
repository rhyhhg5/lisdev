/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LOBIndUWSubSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LOBIndUWSubSet </p>
 * <p>Description: LOBIndUWSubSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-24
 */
public class LOBIndUWSubSet extends SchemaSet
{
    // @Method
    public boolean add(LOBIndUWSubSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LOBIndUWSubSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LOBIndUWSubSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LOBIndUWSubSchema get(int index)
    {
        LOBIndUWSubSchema tSchema = (LOBIndUWSubSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LOBIndUWSubSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LOBIndUWSubSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBIndUWSub描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LOBIndUWSubSchema aSchema = (LOBIndUWSubSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LOBIndUWSubSchema aSchema = new LOBIndUWSubSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LOBIndUWSubSchema tSchema = new LOBIndUWSubSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
