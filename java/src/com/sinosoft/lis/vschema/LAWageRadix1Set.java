/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LAWageRadix1Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAWageRadix1Set </p>
 * <p>Description: LAWageRadix1SchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAWageRadix1Set extends SchemaSet
{
    // @Method
    public boolean add(LAWageRadix1Schema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LAWageRadix1Set aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LAWageRadix1Schema aSchema)
    {
        return super.remove(aSchema);
    }

    public LAWageRadix1Schema get(int index)
    {
        LAWageRadix1Schema tSchema = (LAWageRadix1Schema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LAWageRadix1Schema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LAWageRadix1Set aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageRadix1描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer("");
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAWageRadix1Schema aSchema = this.get(i);
            strReturn.append(aSchema.encode());
            if (i != n)
            {
                strReturn.append(SysConst.RECORDSPLITER);
            }
        }

        return strReturn.toString();
    }

    /**
     * 数据解包
     * @param: str String 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LAWageRadix1Schema aSchema = new LAWageRadix1Schema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                this.add(aSchema);
                nBeginPos = nEndPos + 1;
                nEndPos = str.indexOf('^', nEndPos + 1);
            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
        }
        LAWageRadix1Schema tSchema = new LAWageRadix1Schema();
        if (tSchema.decode(str.substring(nBeginPos)))
        {
            this.add(tSchema);
            return true;
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
    }

}
