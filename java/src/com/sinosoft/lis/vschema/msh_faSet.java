/*
 * <p>ClassName: msh_faSet </p>
 * <p>Description: msh_faSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.msh_faSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class msh_faSet extends SchemaSet
{
    // @Method
    public boolean add(msh_faSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(msh_faSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(msh_faSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public msh_faSchema get(int index)
    {
        msh_faSchema tSchema = (msh_faSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, msh_faSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(msh_faSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_fa描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            msh_faSchema aSchema = (msh_faSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            msh_faSchema aSchema = new msh_faSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        msh_faSchema tSchema = new msh_faSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
