/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LDTestPriceMgtSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDTestPriceMgtSet </p>
 * <p>Description: LDTestPriceMgtSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构修改提交单-20060323
 * @CreateDate：2006-03-23
 */
public class LDTestPriceMgtSet extends SchemaSet
{
	// @Method
	public boolean add(LDTestPriceMgtSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LDTestPriceMgtSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LDTestPriceMgtSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LDTestPriceMgtSchema get(int index)
	{
		LDTestPriceMgtSchema tSchema = (LDTestPriceMgtSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LDTestPriceMgtSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LDTestPriceMgtSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTestPriceMgt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LDTestPriceMgtSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LDTestPriceMgtSchema aSchema = new LDTestPriceMgtSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LDTestPriceMgtSchema tSchema = new LDTestPriceMgtSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
