/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.SY01ResultInfoSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: SY01ResultInfoSet </p>
 * <p>Description: SY01ResultInfoSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-04-10
 */
public class SY01ResultInfoSet extends SchemaSet
{
	// @Method
	public boolean add(SY01ResultInfoSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(SY01ResultInfoSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(SY01ResultInfoSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public SY01ResultInfoSchema get(int index)
	{
		SY01ResultInfoSchema tSchema = (SY01ResultInfoSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, SY01ResultInfoSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(SY01ResultInfoSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpSY01ResultInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			SY01ResultInfoSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			SY01ResultInfoSchema aSchema = new SY01ResultInfoSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		SY01ResultInfoSchema tSchema = new SY01ResultInfoSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
