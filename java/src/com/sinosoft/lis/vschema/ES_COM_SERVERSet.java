/*
 * <p>ClassName: ES_COM_SERVERSet </p>
 * <p>Description: ES_COM_SERVERSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: EasyScanV4
 * @CreateDate：2005-01-27
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.ES_COM_SERVERSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class ES_COM_SERVERSet extends SchemaSet
{
    // @Method
    public boolean add(ES_COM_SERVERSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(ES_COM_SERVERSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(ES_COM_SERVERSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public ES_COM_SERVERSchema get(int index)
    {
        ES_COM_SERVERSchema tSchema = (ES_COM_SERVERSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, ES_COM_SERVERSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(ES_COM_SERVERSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_COM_SERVER描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            ES_COM_SERVERSchema aSchema = (ES_COM_SERVERSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            ES_COM_SERVERSchema aSchema = new ES_COM_SERVERSchema();
            if (!aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        ES_COM_SERVERSchema tSchema = new ES_COM_SERVERSchema();
        if (!tSchema.decode(str.substring(nBeginPos)))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
