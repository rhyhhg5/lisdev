/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCGrpPayDueDetailSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LCGrpPayDueDetailSet </p>
 * <p>Description: LCGrpPayDueDetailSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 营改增
 * @CreateDate：2016-08-23
 */
public class LCGrpPayDueDetailSet extends SchemaSet
{
	// @Method
	public boolean add(LCGrpPayDueDetailSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LCGrpPayDueDetailSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LCGrpPayDueDetailSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LCGrpPayDueDetailSchema get(int index)
	{
		LCGrpPayDueDetailSchema tSchema = (LCGrpPayDueDetailSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LCGrpPayDueDetailSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LCGrpPayDueDetailSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpPayDueDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LCGrpPayDueDetailSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LCGrpPayDueDetailSchema aSchema = new LCGrpPayDueDetailSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LCGrpPayDueDetailSchema tSchema = new LCGrpPayDueDetailSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
