/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LPRenewBillLogSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LPRenewBillLogSet </p>
 * <p>Description: LPRenewBillLogSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: FP-Picch-SPTO-方瑞-新增表-（4113 关于官网在线续期续保缴费的需求）电子商务官网续期续保发票推送存放表
 * @CreateDate：2019-02-28
 */
public class LPRenewBillLogSet extends SchemaSet
{
	// @Method
	public boolean add(LPRenewBillLogSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LPRenewBillLogSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LPRenewBillLogSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LPRenewBillLogSchema get(int index)
	{
		LPRenewBillLogSchema tSchema = (LPRenewBillLogSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LPRenewBillLogSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LPRenewBillLogSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPRenewBillLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LPRenewBillLogSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LPRenewBillLogSchema aSchema = new LPRenewBillLogSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LPRenewBillLogSchema tSchema = new LPRenewBillLogSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
