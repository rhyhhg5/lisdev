/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.Staff_Info_tempSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: Staff_Info_tempSet </p>
 * <p>Description: Staff_Info_tempSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2009-03-07
 */
public class Staff_Info_tempSet extends SchemaSet
{
	// @Method
	public boolean add(Staff_Info_tempSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(Staff_Info_tempSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(Staff_Info_tempSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public Staff_Info_tempSchema get(int index)
	{
		Staff_Info_tempSchema tSchema = (Staff_Info_tempSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, Staff_Info_tempSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(Staff_Info_tempSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStaff_Info_temp描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			Staff_Info_tempSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			Staff_Info_tempSchema aSchema = new Staff_Info_tempSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		Staff_Info_tempSchema tSchema = new Staff_Info_tempSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
