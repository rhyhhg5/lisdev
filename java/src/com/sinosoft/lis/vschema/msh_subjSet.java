/*
 * <p>ClassName: msh_subjSet </p>
 * <p>Description: msh_subjSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.msh_subjSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class msh_subjSet extends SchemaSet
{
    // @Method
    public boolean add(msh_subjSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(msh_subjSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(msh_subjSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public msh_subjSchema get(int index)
    {
        msh_subjSchema tSchema = (msh_subjSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, msh_subjSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(msh_subjSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_subj描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            msh_subjSchema aSchema = (msh_subjSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            msh_subjSchema aSchema = new msh_subjSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        msh_subjSchema tSchema = new msh_subjSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
