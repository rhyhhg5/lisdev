/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LDOPSNameSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDOPSNameSet </p>
 * <p>Description: LDOPSNameSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新增--5.23
 * @CreateDate：2005-05-23
 */
public class LDOPSNameSet extends SchemaSet
{
    // @Method
    public boolean add(LDOPSNameSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LDOPSNameSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LDOPSNameSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LDOPSNameSchema get(int index)
    {
        LDOPSNameSchema tSchema = (LDOPSNameSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LDOPSNameSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LDOPSNameSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOPSName描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer("");
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LDOPSNameSchema aSchema = this.get(i);
            strReturn.append(aSchema.encode());
            if (i != n)
            {
                strReturn.append(SysConst.RECORDSPLITER);
            }
        }

        return strReturn.toString();
    }

    /**
     * 数据解包
     * @param: str String 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LDOPSNameSchema aSchema = new LDOPSNameSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                this.add(aSchema);
                nBeginPos = nEndPos + 1;
                nEndPos = str.indexOf('^', nEndPos + 1);
            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
        }
        LDOPSNameSchema tSchema = new LDOPSNameSchema();
        if (tSchema.decode(str.substring(nBeginPos)))
        {
            this.add(tSchema);
            return true;
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
    }

}
