/*
 * <p>ClassName: LOBGUWSub2Set </p>
 * <p>Description: LOBGUWSub2SchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 核心表变更
 * @CreateDate：2004-12-06
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LOBGUWSub2Schema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LOBGUWSub2Set extends SchemaSet
{
    // @Method
    public boolean add(LOBGUWSub2Schema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LOBGUWSub2Set aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LOBGUWSub2Schema aSchema)
    {
        return super.remove(aSchema);
    }

    public LOBGUWSub2Schema get(int index)
    {
        LOBGUWSub2Schema tSchema = (LOBGUWSub2Schema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LOBGUWSub2Schema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LOBGUWSub2Set aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBGUWSub2描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LOBGUWSub2Schema aSchema = (LOBGUWSub2Schema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LOBGUWSub2Schema aSchema = new LOBGUWSub2Schema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LOBGUWSub2Schema tSchema = new LOBGUWSub2Schema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
