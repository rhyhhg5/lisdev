/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.YBK_U01_ResultsSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: YBK_U01_ResultsSet </p>
 * <p>Description: YBK_U01_ResultsSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2016-12-14
 */
public class YBK_U01_ResultsSet extends SchemaSet
{
	// @Method
	public boolean add(YBK_U01_ResultsSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(YBK_U01_ResultsSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(YBK_U01_ResultsSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public YBK_U01_ResultsSchema get(int index)
	{
		YBK_U01_ResultsSchema tSchema = (YBK_U01_ResultsSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, YBK_U01_ResultsSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(YBK_U01_ResultsSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBK_U01_Results描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			YBK_U01_ResultsSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			YBK_U01_ResultsSchema aSchema = new YBK_U01_ResultsSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		YBK_U01_ResultsSchema tSchema = new YBK_U01_ResultsSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
