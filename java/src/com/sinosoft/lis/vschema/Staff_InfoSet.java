/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.Staff_InfoSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: Staff_InfoSet </p>
 * <p>Description: Staff_InfoSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2009-02-24
 */
public class Staff_InfoSet extends SchemaSet
{
	// @Method
	public boolean add(Staff_InfoSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(Staff_InfoSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(Staff_InfoSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public Staff_InfoSchema get(int index)
	{
		Staff_InfoSchema tSchema = (Staff_InfoSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, Staff_InfoSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(Staff_InfoSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStaff_Info描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			Staff_InfoSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			Staff_InfoSchema aSchema = new Staff_InfoSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		Staff_InfoSchema tSchema = new Staff_InfoSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
