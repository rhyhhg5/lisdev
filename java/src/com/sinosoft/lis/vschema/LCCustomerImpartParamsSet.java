/*
 * <p>ClassName: LCCustomerImpartParamsSet </p>
 * <p>Description: LCCustomerImpartParamsSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 修改主键
 * @CreateDate：2005-01-12
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCCustomerImpartParamsSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LCCustomerImpartParamsSet extends SchemaSet
{
    // @Method
    public boolean add(LCCustomerImpartParamsSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LCCustomerImpartParamsSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LCCustomerImpartParamsSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LCCustomerImpartParamsSchema get(int index)
    {
        LCCustomerImpartParamsSchema tSchema = (LCCustomerImpartParamsSchema)super.
                                               getObj(index);
        return tSchema;
    }

    public boolean set(int index, LCCustomerImpartParamsSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LCCustomerImpartParamsSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCCustomerImpartParams描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LCCustomerImpartParamsSchema aSchema = (
                    LCCustomerImpartParamsSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LCCustomerImpartParamsSchema aSchema = new
                    LCCustomerImpartParamsSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LCCustomerImpartParamsSchema tSchema = new LCCustomerImpartParamsSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
