/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.ReportStatisticsconditionSchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: ReportStatisticsconditionSet </p>
 * <p>Description: ReportStatisticsconditionSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: downloadreport
 * @CreateDate：2010-08-04
 */
public class ReportStatisticsconditionSet extends SchemaSet
{
	// @Method
	public boolean add(ReportStatisticsconditionSchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(ReportStatisticsconditionSet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(ReportStatisticsconditionSchema aSchema)
	{
		return super.remove(aSchema);
	}

	public ReportStatisticsconditionSchema get(int index)
	{
		ReportStatisticsconditionSchema tSchema = (ReportStatisticsconditionSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, ReportStatisticsconditionSchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(ReportStatisticsconditionSet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpReportStatisticscondition描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			ReportStatisticsconditionSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			ReportStatisticsconditionSchema aSchema = new ReportStatisticsconditionSchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		ReportStatisticsconditionSchema tSchema = new ReportStatisticsconditionSchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
