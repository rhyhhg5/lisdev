/*
 * <p>ClassName: LCUWReponsComSet </p>
 * <p>Description: LCUWReponsComSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 核保师权限描述表
 * @CreateDate：2005-01-24
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LCUWReponsComSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LCUWReponsComSet extends SchemaSet
{
    // @Method
    public boolean add(LCUWReponsComSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LCUWReponsComSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LCUWReponsComSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LCUWReponsComSchema get(int index)
    {
        LCUWReponsComSchema tSchema = (LCUWReponsComSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LCUWReponsComSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LCUWReponsComSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCUWReponsCom描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LCUWReponsComSchema aSchema = (LCUWReponsComSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LCUWReponsComSchema aSchema = new LCUWReponsComSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LCUWReponsComSchema tSchema = new LCUWReponsComSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
