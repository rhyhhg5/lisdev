/*
 * <p>ClassName: LBGeneralSet </p>
 * <p>Description: LBGeneralSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 分单信息表
 * @CreateDate：2005-01-24
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LBGeneralSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LBGeneralSet extends SchemaSet
{
    // @Method
    public boolean add(LBGeneralSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LBGeneralSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LBGeneralSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LBGeneralSchema get(int index)
    {
        LBGeneralSchema tSchema = (LBGeneralSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LBGeneralSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LBGeneralSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBGeneral描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LBGeneralSchema aSchema = (LBGeneralSchema)this.get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LBGeneralSchema aSchema = new LBGeneralSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LBGeneralSchema tSchema = new LBGeneralSchema();
        if (tSchema.decode(str.substring(nBeginPos)) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
