/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LDTabColRelSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;


/*
 * <p>ClassName: LDTabColRelSet </p>
 * <p>Description: LDTabColRelSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 费率表动态显示
 * @CreateDate：2005-04-08
 */
public class LDTabColRelSet extends SchemaSet
{
    // @Method
    public boolean add(LDTabColRelSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LDTabColRelSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LDTabColRelSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LDTabColRelSchema get(int index)
    {
        LDTabColRelSchema tSchema = (LDTabColRelSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LDTabColRelSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LDTabColRelSet aSet)
    {
        return super.set(aSet);
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTabColRel描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer("");
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LDTabColRelSchema aSchema = this.get(i);
            strReturn.append(aSchema.encode());
            if (i != n)
            {
                strReturn.append(SysConst.RECORDSPLITER);
            }
        }

        return strReturn.toString();
    }


    /**
     * 数据解包
     * @param: str String 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LDTabColRelSchema aSchema = new LDTabColRelSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                this.add(aSchema);
                nBeginPos = nEndPos + 1;
                nEndPos = str.indexOf('^', nEndPos + 1);
            }
            else
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
        }
        LDTabColRelSchema tSchema = new LDTabColRelSchema();
        if (tSchema.decode(str.substring(nBeginPos)))
        {
            this.add(tSchema);
            return true;
        }
        else
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
    }

}
