/*
 * <p>ClassName: LAAgentMenuGrpToMenuSet </p>
 * <p>Description: LAAgentMenuGrpToMenuSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LAAgentMenuGrpToMenuSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

public class LAAgentMenuGrpToMenuSet extends SchemaSet
{
    // @Method
    public boolean add(LAAgentMenuGrpToMenuSchema aSchema)
    {
        return super.add(aSchema);
    }

    public boolean add(LAAgentMenuGrpToMenuSet aSet)
    {
        return super.add(aSet);
    }

    public boolean remove(LAAgentMenuGrpToMenuSchema aSchema)
    {
        return super.remove(aSchema);
    }

    public LAAgentMenuGrpToMenuSchema get(int index)
    {
        LAAgentMenuGrpToMenuSchema tSchema = (LAAgentMenuGrpToMenuSchema)super.
                                             getObj(index);
        return tSchema;
    }

    public boolean set(int index, LAAgentMenuGrpToMenuSchema aSchema)
    {
        return super.set(index, aSchema);
    }

    public boolean set(LAAgentMenuGrpToMenuSet aSet)
    {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentMenuGrpToMenu描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        int n = this.size();
        for (int i = 1; i <= n; i++)
        {
            LAAgentMenuGrpToMenuSchema aSchema = (LAAgentMenuGrpToMenuSchema)this.
                                                 get(i);
            strReturn += aSchema.encode();
            if (i != n)
            {
                strReturn += SysConst.RECORDSPLITER;
            }
        }

        return strReturn;
    }

    /**
     * 数据解包
     * @param: 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str)
    {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1)
        {
            LAAgentMenuGrpToMenuSchema aSchema = new LAAgentMenuGrpToMenuSchema();
            if (!aSchema.decode(str.substring(nBeginPos, nEndPos)))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
            this.add(aSchema);
            nBeginPos = nEndPos + 1;
            nEndPos = str.indexOf('^', nEndPos + 1);
        }
        LAAgentMenuGrpToMenuSchema tSchema = new LAAgentMenuGrpToMenuSchema();
        if (!tSchema.decode(str.substring(nBeginPos)))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
        this.add(tSchema);

        return true;
    }

}
