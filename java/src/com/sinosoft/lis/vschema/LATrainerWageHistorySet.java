/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LATrainerWageHistorySchema;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LATrainerWageHistorySet </p>
 * <p>Description: LATrainerWageHistorySchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险培训组训信息表
 * @CreateDate：2018-05-11
 */
public class LATrainerWageHistorySet extends SchemaSet
{
	// @Method
	public boolean add(LATrainerWageHistorySchema aSchema)
	{
		return super.add(aSchema);
	}

	public boolean add(LATrainerWageHistorySet aSet)
	{
		return super.add(aSet);
	}

	public boolean remove(LATrainerWageHistorySchema aSchema)
	{
		return super.remove(aSchema);
	}

	public LATrainerWageHistorySchema get(int index)
	{
		LATrainerWageHistorySchema tSchema = (LATrainerWageHistorySchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LATrainerWageHistorySchema aSchema)
	{
		return super.set(index,aSchema);
	}

	public boolean set(LATrainerWageHistorySet aSet)
	{
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerWageHistory描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++)
		{
			LATrainerWageHistorySchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str )
	{
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 )
		{
			LATrainerWageHistorySchema aSchema = new LATrainerWageHistorySchema();
			if(aSchema.decode(str.substring(nBeginPos, nEndPos)))
			{
			this.add(aSchema);
			nBeginPos = nEndPos + 1;
			nEndPos = str.indexOf('^', nEndPos + 1);
			}
			else
			{
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LATrainerWageHistorySchema tSchema = new LATrainerWageHistorySchema();
		if(tSchema.decode(str.substring(nBeginPos)))
		{
		this.add(tSchema);
		return true;
		}
		else
		{
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
