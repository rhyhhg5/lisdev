/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LHStaQuOptionSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LHStaQuOptionSet </p>
 * <p>Description: LHStaQuOptionSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构修改提交单-200609011
 * @CreateDate：2006-09-11
 */
public class LHStaQuOptionSet extends SchemaSet {
    // @Method
    public boolean add(LHStaQuOptionSchema aSchema) {
        return super.add(aSchema);
    }

    public boolean add(LHStaQuOptionSet aSet) {
        return super.add(aSet);
    }

    public boolean remove(LHStaQuOptionSchema aSchema) {
        return super.remove(aSchema);
    }

    public LHStaQuOptionSchema get(int index) {
        LHStaQuOptionSchema tSchema = (LHStaQuOptionSchema)super.getObj(index);
        return tSchema;
    }

    public boolean set(int index, LHStaQuOptionSchema aSchema) {
        return super.set(index, aSchema);
    }

    public boolean set(LHStaQuOptionSet aSet) {
        return super.set(aSet);
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHStaQuOption描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer("");
        int n = this.size();
        for (int i = 1; i <= n; i++) {
            LHStaQuOptionSchema aSchema = this.get(i);
            strReturn.append(aSchema.encode());
            if (i != n) {
                strReturn.append(SysConst.RECORDSPLITER);
            }
        }

        return strReturn.toString();
    }

    /**
     * 数据解包
     * @param: str String 打包后字符串
     * @return: boolean
     **/
    public boolean decode(String str) {
        int nBeginPos = 0;
        int nEndPos = str.indexOf('^');
        this.clear();

        while (nEndPos != -1) {
            LHStaQuOptionSchema aSchema = new LHStaQuOptionSchema();
            if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
                this.add(aSchema);
                nBeginPos = nEndPos + 1;
                nEndPos = str.indexOf('^', nEndPos + 1);
            } else {
                // @@错误处理
                this.mErrors.copyAllErrors(aSchema.mErrors);
                return false;
            }
        }
        LHStaQuOptionSchema tSchema = new LHStaQuOptionSchema();
        if (tSchema.decode(str.substring(nBeginPos))) {
            this.add(tSchema);
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(tSchema.mErrors);
            return false;
        }
    }

}
