/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.agentcalculate;

import java.sql.Connection;

import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.vdb.LACommisionDBSet;
import com.sinosoft.lis.vdb.LAWageHistoryDBSet;
import com.sinosoft.lis.vdb.LAWageLogDBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: AgentWageCalSaveBLS </p>
 * <p>Description: LAAgentWageCalSaveBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-31
 */
public class AgentWageCalSaveBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    //传输数据类
    private VData mInputData;


    /** 数据操作字符串 */
    private String mOperate;
    public AgentWageCalSaveBLS()
    {}

    public static void main(String[] args)
    {}


    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        System.out.println("Start LAAgentWageCalSaveBLS Submit...");
        if (this.mOperate.equals("INSERT||AGENTWAGE"))
        {
            if (!saveLAAgentWageCalSave())
            {
                System.out.println("Insert failed");
                System.out.println("End LAAgentWageCalSaveBLS Submit...");
                return false;
            }
        }
        System.out.println(" sucessful");
        return true;
    }


    /**
     * 保存函数
     */
    private boolean saveLAAgentWageCalSave()
    {
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
        LACommisionSet tLACommisionSet = new LACommisionSet();
        tLAWageLogSchema = (LAWageLogSchema) mInputData.getObjectByObjectName(
                "LAWageLogSchema", 0);
        tLAWageHistorySchema = (LAWageHistorySchema) mInputData.
                               getObjectByObjectName("LAWageHistorySchema", 0);
        tLACommisionSet = (LACommisionSet) mInputData.getObjectByObjectName(
                "LACommisionSet", 2);

        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBLS";
            tError.functionName = "saveLAAgentWageCalSave";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAWageLogDB tLAWageLogDB = new LAWageLogDB(conn);
            LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB(conn);
            tLAWageLogDB.setSchema(tLAWageLogSchema);
            tLAWageHistoryDB.setSchema(tLAWageHistorySchema);
            LAWageLogDB queryDB = new LAWageLogDB();
            queryDB.setWageNo(tLAWageLogSchema.getWageNo());
            queryDB.setManageCom(tLAWageLogSchema.getManageCom());
            if (tLAWageLogSchema.getBranchType().equals("0"))
            {
                queryDB.setBranchType("1"); //置随便一个作代表
            }
            else
            {
                queryDB.setBranchType(tLAWageLogSchema.getBranchType());
            }
            LAWageLogSet tLAWageLogSet = queryDB.query();
            if (queryDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(queryDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSaveBLS";
                tError.functionName = "saveLAAgentWageCalSave";
                tError.errorMessage = "日志表数据查询失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //System.out.println("StartDate:"+tLAWageLogDB.getStartDate());
            //System.out.println("EndDate:"+tLAWageLogDB.getEndDate());
            //if (tLAWageLogDB.getStartDate().equals(tLAWageLogDB.getEndDate()))
            //{
            if (tLAWageLogSet.size() == 0)
            {
                //插入
                System.out.println("插入---");
                if (tLAWageLogSchema.getBranchType().equals("0")) //插入三条
                {
                    //日志表
                    LAWageLogDBSet tLAWageLogDBSet = new LAWageLogDBSet(conn);
                    tLAWageLogSchema.setBranchType("1");
                    tLAWageLogDBSet.add((new LAWageLogSchema()));
                    tLAWageLogDBSet.get(1).setSchema(tLAWageLogSchema);
                    tLAWageLogSchema.setBranchType("2");
                    tLAWageLogDBSet.add((new LAWageLogSchema()));
                    tLAWageLogDBSet.get(2).setSchema(tLAWageLogSchema);
                    tLAWageLogSchema.setBranchType("3");
                    tLAWageLogDBSet.add((new LAWageLogSchema()));
                    tLAWageLogDBSet.get(3).setSchema(tLAWageLogSchema);
                    if (!tLAWageLogDBSet.insert())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAWageLogDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBLS";
                        tError.functionName = "saveLAAgentWageCalSave";
                        tError.errorMessage = "日志表三条数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                    //历史表
                    LAWageHistoryDBSet tLAWageHistoryDBSet = new
                            LAWageHistoryDBSet(conn);
                    tLAWageHistorySchema.setBranchType("1");
                    tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                    tLAWageHistoryDBSet.get(1).setSchema(tLAWageHistorySchema);
                    tLAWageHistorySchema.setBranchType("2");
                    tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                    tLAWageHistoryDBSet.get(2).setSchema(tLAWageHistorySchema);
                    tLAWageHistorySchema.setBranchType("3");
                    tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                    tLAWageHistoryDBSet.get(3).setSchema(tLAWageHistorySchema);
                    if (!tLAWageHistoryDBSet.insert())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAWageHistoryDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBLS";
                        tError.functionName = "saveLAAgentWageCalSave";
                        tError.errorMessage = "历史表三条数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
                else
                {
                    if (!tLAWageLogDB.insert())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAWageLogDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBLS";
                        tError.functionName = "saveLAAgentWageCalSave";
                        tError.errorMessage = "日志表数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                    if (!tLAWageHistoryDB.insert())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBLS";
                        tError.functionName = "saveLAAgentWageCalSave";
                        tError.errorMessage = "历史表数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
            }
            else
            {
                //更新
                System.out.println("更新---");
                if (tLAWageLogSchema.getBranchType().equals("0")) //插入三条
                {
                    //日志表
                    LAWageLogDBSet tLAWageLogDBSet = new LAWageLogDBSet(conn);
                    tLAWageLogSchema.setBranchType("1");
                    tLAWageLogDBSet.add((new LAWageLogSchema()));
                    tLAWageLogDBSet.get(1).setSchema(tLAWageLogSchema);
                    tLAWageLogSchema.setBranchType("2");
                    tLAWageLogDBSet.add((new LAWageLogSchema()));
                    tLAWageLogDBSet.get(2).setSchema(tLAWageLogSchema);
                    tLAWageLogSchema.setBranchType("3");
                    tLAWageLogDBSet.add((new LAWageLogSchema()));
                    tLAWageLogDBSet.get(3).setSchema(tLAWageLogSchema);
                    if (!tLAWageLogDBSet.update())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAWageLogDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBLS";
                        tError.functionName = "saveLAAgentWageCalSave";
                        tError.errorMessage = "日志表三条数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                    //历史表
                    LAWageHistoryDBSet tLAWageHistoryDBSet = new
                            LAWageHistoryDBSet(conn);
                    tLAWageHistorySchema.setBranchType("1");
                    tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                    tLAWageHistoryDBSet.get(1).setSchema(tLAWageHistorySchema);
                    tLAWageHistorySchema.setBranchType("2");
                    tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                    tLAWageHistoryDBSet.get(2).setSchema(tLAWageHistorySchema);
                    tLAWageHistorySchema.setBranchType("3");
                    tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                    tLAWageHistoryDBSet.get(3).setSchema(tLAWageHistorySchema);
                    if (!tLAWageHistoryDBSet.update())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAWageHistoryDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBLS";
                        tError.functionName = "saveLAAgentWageCalSave";
                        tError.errorMessage = "历史表三条数据保存失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
                else
                {
                    if (!tLAWageLogDB.update())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAWageLogDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBLS";
                        tError.functionName = "saveLAAgentWageCalSave";
                        tError.errorMessage = "日志表数据更新失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                    if (!tLAWageHistoryDB.update())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBLS";
                        tError.functionName = "saveLAAgentWageCalSave";
                        tError.errorMessage = "历史表数据更新失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
            }
            if (tLACommisionSet.size() > 0)
            {
                LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
                tLACommisionDBSet.set(tLACommisionSet);
                if (!tLACommisionDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLACommisionDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveBLS";
                    tError.functionName = "saveLAAgentWageCalSave";
                    tError.errorMessage = "直接佣金表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("更新退费表中对应的扎帐表中的纪录。。。");
            tLACommisionSet.clear();
            tLACommisionSet = (LACommisionSet) mInputData.getObjectByObjectName(
                    "LACommisionSet", 3);
            if (tLACommisionSet.size() > 0)
            {
                LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
                tLACommisionDBSet.set(tLACommisionSet);
                if (!tLACommisionDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLACommisionDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveBLS";
                    tError.functionName = "saveLAAgentWageCalSave";
                    tError.errorMessage = "退费对应的扎帐表更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }
}
