package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun;


public class BankChargeGatherNewUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private VData mInputData = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAWageSchema mLAWageTerm = new LAWageSchema();

    private String mWageYM = ""; //薪资年月
    //获得转储号码
    private String mNewEdorNo = "";

    private LAWageSet mLAWageSet = null;
    private LAWageBSet mLAWageBSet = null;
    private LAWageHistorySet mLAWageHistorySet = null;
    private LARollBackTraceSet mLARollBackTraceSet = new LARollBackTraceSet();
    private LAChargeLogSchema mLAChargeLogSchema = new LAChargeLogSchema();

    public static void main(String args[]) {
        VData tVData = new VData();

        LAChargeLogSchema tLAChargeLogSchema = new LAChargeLogSchema();
        tLAChargeLogSchema = new LAChargeLogSchema();
        tLAChargeLogSchema.setManageCom("86110000");
        tLAChargeLogSchema.setBranchType("3");
        tLAChargeLogSchema.setBranchType2("01");
        tLAChargeLogSchema.setEndDate("2007-05-04");
        tLAChargeLogSchema.setChargeMonth("05");
        tLAChargeLogSchema.setChargeCalNo("200705");
        tLAChargeLogSchema.setChargeYear("2007");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "ac";
        tG.ManageCom = "86";
        tVData.clear();
        //提交
        tVData.addElement(tG);
        tVData.addElement(tLAChargeLogSchema);

        BankChargeGatherNewUI tBankChargeGatherNewUI = new BankChargeGatherNewUI();
        boolean tB = tBankChargeGatherNewUI.submitData(tVData, "");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (this.getInputData(cInputData) == false) {
            return false;
        }
        //准备后台处理的数据
        if (!prepareOutputData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChargeGatherNewUI";
            tError.functionName = "dealData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错！";
            System.out.println("在准备往后层处理所需要的数据时出错！");
            this.mErrors.addOneError(tError);

            return false;
        }
        //进行后台处理
        BankChargeGatherNewBL tBankChargeGatherNewBL = new BankChargeGatherNewBL();
        if (tBankChargeGatherNewBL.submitData(mInputData, "") == false) {
            // @@错误处理
            this.mErrors.copyAllErrors(tBankChargeGatherNewBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankChargeGatherNewUI";
            tError.functionName = "submitData";
            tError.errorMessage = "向后台提交数据时 处理失败！";
            System.out.println("向后台提交数据时 处理失败！");
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 准备后台处理的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLAChargeLogSchema);

        } catch (Exception ex) {
            return false;
        }

        return true;
    }


    /**
     * 从传入参数中得到全部数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //得到全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        //取得需要处理的数据的条件
        mLAChargeLogSchema = ((LAChargeLogSchema) cInputData.
                              getObjectByObjectName(
                                      "LAChargeLogSchema", 0));
        if (mGlobalInput == null || mLAChargeLogSchema == null) {
            CError tError = new CError();
            tError.moduleName = "AgentChargeCalUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;

    }
}
