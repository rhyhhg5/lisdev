package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.utility.CError;

/**
 * <p>Title: 打折处理具体类</p>
 * <p>Description: 打折处理具体类</p>
 * <p>Copyright: Copyright (c) sinosoft 2005</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */

public class CalFormDrawRate extends RebatePublic
{
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    public CalFormDrawRate()
    {
    }

    public static void main(String[] args)
    {
    }

    // 校验数据的正确性
    protected boolean check()
    {
        System.out.println("check");
        return true;
    }

    /**
     * 进行业务处理
     * @return
     */
    protected boolean dealData()
    {
        System.out.println("------------开始打折处理---------");
        String tBranchType = "";
        String tAgentCode = "";
        String tCurrAgentCode = "";
        String tAgentGrade = "";
        System.out.println(mSet.size() + "++++++++++++++++++++++++++++++++++");
        for (int i = 1; i <= mSet.size(); i++)
        {
            LACommisionSchema tSch = new LACommisionSchema();
            System.out.println("------------calRate--------" + i + "----");
            tSch.setSchema(mSet.get(i));
            tCurrAgentCode = tSch.getAgentCode();
            if (!tAgentCode.equals(tCurrAgentCode))
            {
                //查询代理人基本信息
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tCurrAgentCode);
                if (!tLAAgentDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalFormDrawRate";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询代理人" + tCurrAgentCode + "的基础信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                this.mLAAgentSchema = tLAAgentDB.getSchema();
                //查询代理人行政信息
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tCurrAgentCode);
                if (!tLATreeDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalFormDrawRate";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询代理人" + tCurrAgentCode + "的行政信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                this.mLATreeSchema = tLATreeDB.getSchema();
            }
            if (mAgentGrade==null || mAgentGrade.equals(""))
            {
                tAgentGrade = this.mLATreeSchema.getAgentGrade().trim();
            }
            else
            {
                tAgentGrade=mAgentGrade;
            }
            if (tAgentGrade == null || tAgentGrade.equals(""))
                return false;
            tBranchType = tSch.getBranchType();
            if (tBranchType.trim().equals("1"))
            { //个险
            	double p6= tSch.getP6();
            	if(p6!=0){
            		tSch.setP6(1);
            	}else{
            		tSch.setP6(0);
            	}
            	
                System.out.println("处理个险人员" + tCurrAgentCode + "打折");
                //具体处理过程由各项目组负责
                if (tAgentGrade.equals("A01"))
                {
                    rate = getRate();
                    double tStandFycRate = tSch.getStandFYCRate();
                    double tDirectWage = tSch.getDirectWage();
                    double tFycRate = tStandFycRate * rate;
                    double tFyc = tDirectWage * rate;
                    tSch.setFYCRate(tFycRate);
                    tSch.setFYC(tFyc);
                }
                else
                {
                    tSch.setFYC(tSch.getDirectWage());
                    tSch.setFYCRate(tSch.getStandFYCRate());
                }
            }
            else if (tBranchType.trim().equals("2"))
            { //团险
                System.out.println("处理团险人员" + tCurrAgentCode + "打折");
                //具体处理过程由各项目组负责
                    tSch.setFYC(tSch.getDirectWage());
                    tSch.setFYCRate(tSch.getStandFYCRate());
            }else if(tBranchType.trim().equals("3"))
            {
                 System.out.println("处理银邮人员" + tCurrAgentCode + "打折");
                 tSch.setFYC(tSch.getDirectWage());
                 tSch.setFYCRate(tSch.getStandFYCRate());
            }
            else if (tBranchType.trim().equals("5"))
            { //互动渠道
                System.out.println("处理互动渠道" + tCurrAgentCode + "打折");
                //具体处理过程由各项目组负责
                    rate = 1;
                    double tStandFycRate = tSch.getStandFYCRate();
                    double tDirectWage = tSch.getDirectWage();
                    double tFycRate = tStandFycRate * rate;
                    double tFyc = tDirectWage * rate;
                    tSch.setFYCRate(tFycRate);
                    tSch.setFYC(tFyc);
            }else if (tBranchType.trim().equals("7"))
            { //健管渠道
                System.out.println("处理健康渠道" + tCurrAgentCode + "打折");
                //具体处理过程由各项目组负责
                    rate = 1;
                    double tStandFycRate = tSch.getStandFYCRate();
                    double tDirectWage = tSch.getDirectWage();
                    double tFycRate = tStandFycRate * rate;
                    double tFyc = tDirectWage * rate;
                    tSch.setFYCRate(tFycRate);
                    tSch.setFYC(tFyc);
            }else if (tBranchType.trim().equals("6"))
            { //社保渠道
                System.out.println("处理社保渠道" + tCurrAgentCode + "打折");
                //具体处理过程由各项目组负责
                    rate = 1;
                    double tStandFycRate = tSch.getStandFYCRate();
                    double tDirectWage = tSch.getDirectWage();
                    double tFycRate = tStandFycRate * rate;
                    double tFyc = tDirectWage * rate;
                    tSch.setFYCRate(tFycRate);
                    tSch.setFYC(tFyc);
            }
            else
            {
                System.out.println("处理银代人员" + tCurrAgentCode + "打折");
                //具体处理过程由各项目组负责
            }
            
            this.mNewSet.add(tSch);
            tAgentCode = tCurrAgentCode;
        }
        return true;
    }

    protected boolean dealData1()
    {
        return dealData();
    }

    // 获取打折比例
    protected float getRate()
    {
        //这个比例由各项目组控制
        return Float.parseFloat("1.0");
    }

}
