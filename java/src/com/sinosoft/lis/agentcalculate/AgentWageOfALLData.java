package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LACommisionConfigDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionConfigSchema;
import com.sinosoft.lis.vschema.LACommisionConfigSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 把所有的提数给汇总一起，按顺序 进行
 * @author yangyang
 *
 */
public class AgentWageOfALLData {

	/**接收登陆的信息*/
	public GlobalInput mGlobalInput = new GlobalInput();
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
	 
	/**管理机构*/
	public String mManageCom;
	
	/**展业类型*/
	public String mBranchType;
	
	/**渠道*/
	public String mBranchType2;

	/**提数日期*/
	public String mTMakeDate;
	
	/**操作者即提取时，系统登陆人*/
	public String mOperator;
	
	/**存储对应的查询条件，为了满足后台逻辑*/
	public String mWhereSQL;
	
	/**团单号，为了运维功能，漏提数据时，可以根据保单号来提取*/
	public String mGrpContNo;
	
	/**个单号，为了运维功能，漏提数据时，可以根据保单号来提取*/
	public String mContNo;
	
	/**用来存储所有扎账提数具体处理类的信息*/
	private LACommisionConfigSet mLACommisionConfigSet = new LACommisionConfigSet();

	/**
	 * 调用初始化类，把需要初始化的数据放入到内存中
	 */
	private void init()
	{
		AgentWageInitialize tAgentWageInitialize = new AgentWageInitialize();
		initLACommsionConfig();
	}
	
	public AgentWageOfALLData()
	{
		init();
	}
	
	public boolean submitData(VData cVData,String cOperator)
	{
		System.out.println("AgentWageOfALLData开始提取数据"+PubFun.getCurrentDate()+" "+PubFun.getCurrentTime());
		if(null==mLACommisionConfigSet||0==mLACommisionConfigSet.size())
		{
			System.out.println("AgentWageOfALLData-->初始化核心提数类时出错，请查明原因");
			return false;
		}
		if(!getInputData(cVData,cOperator))
		{
			return false;
		}
		for(int i =1;i<=mLACommisionConfigSet.size();i++)
		{
			LACommisionConfigSchema tLACommisionConfigSchema = new LACommisionConfigSchema();
			tLACommisionConfigSchema = mLACommisionConfigSet.get(i);
			String tClassAP = tLACommisionConfigSchema.getClassAP();
			String tExtractSQL = getRealSQL(tLACommisionConfigSchema.getExtractSQL());
			String tExtractDataType = tLACommisionConfigSchema.getExtractDataType();
			String tTableName = tLACommisionConfigSchema.getSourceTableName();
			String tDataType = tLACommisionConfigSchema.getDataType();
			AgentWageOfAbstractClass tAgentWageOfAbstractClass;
				try {
					tAgentWageOfAbstractClass = (AgentWageOfAbstractClass)Class.forName(tClassAP).newInstance();
					//本来想用反射，不过就这几个类，再写个反射，有点不划算，直接 写个set方法得了
					tAgentWageOfAbstractClass.setBaseValue(tExtractDataType,tTableName, tDataType);
					if(!tAgentWageOfAbstractClass.dealData(tExtractSQL,cOperator))
					{
						System.out.println("AgentWageOfALLData-->"+tClassAP+"类执行报错，请查明原因");
						return false;
					}
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if(!updataLog(tLACommisionConfigSchema))
			{
				System.out.println("AgentWageOfALLData-->"+tClassAP+"类执行完成，但更新日志表出错，请查明原因");
			}
		}
		System.out.println("AgentWageOfALLData类已经执行完成,"+PubFun.getCurrentDate()+PubFun.getCurrentTime());
		return true;
	}
	/**
	 * 处理前面传来的参数 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData,String cOperator)
	{
		mOperator = cOperator;
		mGlobalInput =  (GlobalInput)cInputData.getObjectByObjectName(AgentWageOfCodeDescribe.WAGENO_GLOBALINPUT, 0);
		TransferData tTransferData = new TransferData();
		tTransferData = (TransferData)cInputData.getObjectByObjectName(AgentWageOfCodeDescribe.WAGENO_TRANSFERDATA, 0);
		mTMakeDate = (String) tTransferData.getValueByName(AgentWageOfCodeDescribe.WAGENO_TAMKEDATE);
		if("".equals(mTMakeDate)||null==mTMakeDate)
		{
			CError tError = new CError();
			tError.moduleName = "AgentWageOfCalWageNoData";
			tError.functionName = "getInputData";
			tError.errorMessage = "提数日期不能为空";
			this.mErrors.addOneError(tError);
			return false;
		}
		String tWhereSQL ="1 ";
		//如果为补提的数据，其中：管理机构不能为空
		if(!"sys".equals(mOperator))
		{
			mManageCom = (String) tTransferData.getValueByName(AgentWageOfCodeDescribe.WAGENO_MANAGECOM);
			mGrpContNo = (String) tTransferData.getValueByName(AgentWageOfCodeDescribe.WAGENO_GRPCONTNO);
			mContNo = (String) tTransferData.getValueByName(AgentWageOfCodeDescribe.WAGENO_CONTNO);
			if(!"".equals(mManageCom)&&null!=mManageCom)
			{
				tWhereSQL+= " and managecom ='"+mManageCom+"' ";
			}
			if(!"".equals(mGrpContNo)&&null!=mGrpContNo)
			{
				tWhereSQL+= " and grpcontno ='"+mGrpContNo+"' ";
			}
			if(!"".equals(mContNo)&&null!=mContNo)
			{
				tWhereSQL+= " and contno ='"+mContNo+"' ";
			}
		}
		mWhereSQL = tWhereSQL;
		return true;
	}
	
	/**
	 * 把要执行的提数类相关信息放入到内存中
	 */
	private void initLACommsionConfig()
	{
		String sql = "select * from LACommisionConfig where state ='1' order by orderno with ur";
		System.out.println("AgentWageOfALLData-->initLACommsionConfig:sql"+sql);
		LACommisionConfigDB LACommisionConfigDB = new LACommisionConfigDB();
		mLACommisionConfigSet = LACommisionConfigDB.executeQuery(sql);
		System.out.println("AgentWageOfALLData-->initLACommsionConfig:扎账提数佣金sql配置类始化成功");
	}
	
	/**
	 * 把sql中的变量给替换出来
	 * @param cSQL
	 * @return
	 */
	private String getRealSQL(String cSQL)
	{
		 PubCalculator tPubCalculator = new PubCalculator();
         tPubCalculator.addBasicFactor("TMakeDate",mTMakeDate);
         tPubCalculator.addBasicFactor("WhereSQL", mWhereSQL);
         tPubCalculator.setCalSql(cSQL);
         String tRealSql = tPubCalculator.calculateEx();
         return tRealSql;
	}
	
	/**
	 * 根据配置表，更新日志表状态为完成
	 * @param tLACommisionConfigSchema
	 * @return
	 */
	private boolean updataLog(LACommisionConfigSchema tLACommisionConfigSchema)
	{
		String tSourceTableName = tLACommisionConfigSchema.getSourceTableName();
		String tDataType = tLACommisionConfigSchema.getDataType();
		String tUpdateSQL = "update lawagenewlog set state ='11' ,operator='"+mOperator+"',modifydate = current date,modifytime = current time "
							+ " where SourceTableName ='"+tSourceTableName+"' "
							+ " and datatype ='"+tDataType+"'"
							+ " and enddate = '"+mTMakeDate+"'";
		ExeSQL tExeSQL = new ExeSQL();
		return tExeSQL.execUpdateSQL(tUpdateSQL);
	}
	public static void main(String[] args) {
		GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.ManageCom = "86";
    	tGlobalInput.Operator = "yy";
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue(AgentWageOfCodeDescribe.WAGENO_TAMKEDATE, "2017-9-5");
		tTransferData.setNameAndValue(AgentWageOfCodeDescribe.WAGENO_MANAGECOM, "86420000");
		tTransferData.setNameAndValue(AgentWageOfCodeDescribe.WAGENO_CONTNO, "034992925000001");
		tVData.add(tTransferData);
		tVData.add(tGlobalInput);
		AgentWageOfALLData tAgentWageOfALLData = new AgentWageOfALLData();
		tAgentWageOfALLData.submitData(tVData, "yy");
		
		
	}
} 
