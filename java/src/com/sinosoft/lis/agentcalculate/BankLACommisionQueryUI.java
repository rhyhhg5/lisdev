package com.sinosoft.lis.agentcalculate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BankLACommisionQueryUI {

  public CErrors mErrors = new CErrors();
  private VData mInputData = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private VData mResult = new VData();

  public BankLACommisionQueryUI() {
        try {
          jbInit();
      } catch (Exception ex) {
          ex.printStackTrace();
      }
   }
  public boolean submitData(VData cInputData,String cOperate)
   {
       //将操作数据拷贝到本类中
       this.mOperate = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       mInputData=(VData)cInputData.clone();
       System.out.println("String LARiskContDetailReportUI ...");
       BankLACommisionQueryBL  tBankLACommisionQueryBL = new BankLACommisionQueryBL();
       if (!tBankLACommisionQueryBL.submitData(mInputData, mOperate))
       {
           if (tBankLACommisionQueryBL.mErrors.needDealError())
           {
               mErrors.copyAllErrors(tBankLACommisionQueryBL.mErrors);
               return false;
           }
           else
           {
               buildError("submitData", "BankLACommisionQueryUI 发生错误，但是没有提供详细的出错信息");
               return false;
           }
       }
       else
       {
           mResult =tBankLACommisionQueryBL.getResult();
           return true;
       }
   }

   /**
 * 追加错误信息
 * @param szFunc String
 * @param szErrMsg String
 */
private void buildError(String szFunc, String szErrMsg)
{
    CError cError = new CError();

    cError.moduleName = "BankLACommisionQueryUI ";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}


/**
 * 取得返回处理过的结果
 * @return VData
 */
public VData getResult() {
    return this.mResult;
}

   private void jbInit() throws Exception {
   }

}
