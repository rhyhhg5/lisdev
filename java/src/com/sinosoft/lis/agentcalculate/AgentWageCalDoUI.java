/*
 * <p>ClassName: AgentWageCalDoUI </p>
 * <p>Description: LAAgentUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-31
 */
package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentWageCalDoUI
{

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData = new VData();
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();

    public AgentWageCalDoUI()
    {}

    public static void main(String[] args)
    {
        double aa=0.000001;
        String bb=String.valueOf(aa);
        VData tVData = new VData();
        String sqlstr = "select * from LAWageLog where wageno = '200708' and managecom='86110000' and branchtype='2' and branchtype2='01'";
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        LAWageLogSet tLAWageLogSet = new LAWageLogSet();
        LAWageLogDB tLAWageLogDB = tLAWageLogSchema.getDB();
        tLAWageLogSet = tLAWageLogDB.executeQuery(sqlstr);
        if (tLAWageLogSet.size() > 0)
        {
            tLAWageLogSchema = tLAWageLogSet.get(1);
        }
        else
        {
            System.out.println("error");
            return;
        }
        GlobalInput tG = new GlobalInput();
        tG.Operator = "ac";
        tG.ManageCom = "86";
        tVData.clear();

        //提交
        tVData.addElement(tG);
        tVData.addElement("86110000");
        tVData.addElement(tLAWageLogSchema);
        tVData.addElement("1");
        AgentWageCalDoUI tAgentWageCalDoUI = new AgentWageCalDoUI();
        tAgentWageCalDoUI.submitData(tVData, "INSERT||CALWAGEAGAIN");
        System.out.println("Error:" + tAgentWageCalDoUI.mErrors.getFirstError());
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        AgentWageCalDoNewBL tAgentWageCalDoNewBL = new AgentWageCalDoNewBL();
        System.out.println("Start AgentWageCalDoUI UI Submit...");
        if (!tAgentWageCalDoNewBL.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tAgentWageCalDoNewBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End AgentWageCalDo UI Submit...");
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(mGlobalInput);
            mInputData.add(mLAWageLogSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLAWageLogSchema = ((LAWageLogSchema) cInputData.getObjectByObjectName(
                "LAWageLogSchema", 0));
        if (mGlobalInput == null || mLAWageLogSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
