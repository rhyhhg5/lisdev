package com.sinosoft.lis.agentcalculate;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 考核评估计算程序(考核调整)
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  Howie
 * @version 1.0
 */
public class LAChangeFycRateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap = new MMap();
    private VData mOutputData = new VData();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LACommisionBSet mLACommisionBSet = new LACommisionBSet();
    private String mManageCom = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mRiskCode = "";
    private String mWageNo = "";
    private String mFlag = "";
    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //处理前进行验证
        if (!check())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 验证方法
     * @return boolean
     */
    private boolean check()
    {
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try
        {
            mMap.put(this.mLACommisionSet, "UPDATE");
            mMap.put(this.mLACommisionBSet, "INSERT");
            this.mOutputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
    	String tSQL="select calcode from lawagecalelement "
    		+" where branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"'"
    		+" and riskcode='"+mRiskCode+"' and caltype='00' ";
    	 SSRS tSSRS = new SSRS();
    	 ExeSQL tExeSQL = new ExeSQL();
    	 tSSRS = tExeSQL.execSQL(tSQL);    	 
    	 if (tExeSQL.mErrors.needDealError())
   	     {
    	     CError tError = new CError();
  	         tError.moduleName = "HDLAAscriptionBL";
   	         tError.functionName = "dealData";
 	         tError.errorMessage = "查询提奖代码的信息出错";
  	         this.mErrors.addOneError(tError);
   	         return false;
    	     } 
    	String mCalcode = tSSRS.GetText(1, 1);
        if(mCalcode.equals("AP0052")){
        	String updateSql=" update lacommision set "
        		+"fycrate=db2inst1.COMMISIONRATE(char(lacommision.riskcode),char(lacommision.GrpContNo),"
        		+"char(lacommision.managecom),char(lacommision.agentcode),char(lacommision.payyears),"
        		+"char(lacommision.payyear),char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2),char(lacommision.SIGNDATE),"
        		+"char(lacommision.PAYINTV),char(lacommision.RENEWCOUNT),char(lacommision.SCANDATE),char(lacommision.TMAKEDATE),"
        		+"char(lacommision.CONTNO),char(lacommision.FLAG),char(lacommision.PAYCOUNT),char(lacommision.TRANSSTATE),"
        		+"char(lacommision.F3)),modifydate=current date,modifytime=current time  "
        		+" where riskcode='"+mRiskCode+"' "
        		+" and managecom like '"+mManageCom+"%' "
        		+" and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"' "
        		+" and (wageno>='"+mWageNo+"' or wageno is null)";
        	mMap.put(updateSql, "UPDATE");
        	updateSql="update lacommision set standfycrate=fycrate,fyc=transmoney*fycrate,"
        		+"directwage=transmoney*fycrate,modifydate=current date,modifytime=current time"
        		+" where riskcode='"+mRiskCode+"' "
        		+" and managecom like '"+mManageCom+"%' "
        		+" and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"' "
        		+" and (wageno>='"+mWageNo+"' or wageno is null)";        	
        	mMap.put(updateSql, "UPDATE");
        }

        else if(mCalcode.equals("AP0160")){
        	String updateSql=" update lacommision set "
        		+"fycrate=db2inst1.FYCRATE(char(lacommision.riskcode),char(lacommision.managecom),"
        		+"char(lacommision.PAYINTV),char(lacommision.payyears),char(lacommision.payyear),"
        		+"char(lacommision.BRANCHTYPE),char(lacommision.BRANCHTYPE2), '31',"
        		+"char(lacommision.RENEWCOUNT),char(lacommision.CONTNO),char(lacommision.GRPCONTNO),"
        		+"char(lacommision.F3)),modifydate=current date,modifytime=current time  "
        		+" where riskcode='"+mRiskCode+"' "
        		+" and managecom like '"+mManageCom+"%' "
        		+" and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"' "
        		+" and (wageno>='"+mWageNo+"' or wageno is null)";
        	mMap.put(updateSql, "UPDATE");
        	updateSql="update lacommision set standfycrate=fycrate,fyc=transmoney*fycrate,"
        		+"directwage=transmoney*fycrate,modifydate=current date,modifytime=current time"
        		+" where riskcode='"+mRiskCode+"' "
        		+" and managecom like '"+mManageCom+"%' "
        		+" and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"' "
        		+" and (wageno>='"+mWageNo+"' or wageno is null)";        	
        	mMap.put(updateSql, "UPDATE");
        }
        else if(mCalcode.equals("AP3001")){
        	String updateSql=" update lacommision set "
        		+"fycrate=db2inst1.GetBankCommiRate(char(lacommision.branchtype),char(lacommision.branchtype2),"
        		+"char(lacommision.riskcode),char(lacommision.AgentCom),char(lacommision.contno),"
        		+"char(lacommision.grpcontno),char(lacommision.managecom),char(lacommision.payyears),"
        		+"char(lacommision.payyear),'11',char(lacommision.Renewcount),char(lacommision.TRANSSTATE),"
        		+"char(lacommision.PAYINTV)),modifydate=current date,modifytime=current time  "
        		+" where riskcode='"+mRiskCode+"' "
        		+" and managecom like '"+mManageCom+"%' "
        		+" and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"' "
        		+" and (wageno>='"+mWageNo+"' or wageno is null)";
        	mMap.put(updateSql, "UPDATE");
        	updateSql="update lacommision set standfycrate=fycrate,fyc=transmoney*fycrate,"
        		+"directwage=transmoney*fycrate,modifydate=current date,modifytime=current time"
        		+" where riskcode='"+mRiskCode+"' "
        		+" and managecom like '"+mManageCom+"%' "
        		+" and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"' "
        		+" and (wageno>='"+mWageNo+"' or wageno is null)";        	
        	mMap.put(updateSql, "UPDATE");
        }
        
        return true;
    }

   

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mManageCom = (String)cInputData.getObject(1);
        this.mBranchType = (String)cInputData.getObject(2);
        this.mBranchType2 = (String)cInputData.getObject(3);
        this.mRiskCode = (String)cInputData.getObject(4);
        this.mWageNo = (String)cInputData.getObject(5);
        this.mFlag = (String)cInputData.getObject(6);
        System.out.println(mManageCom+"/"+mBranchType+"/"+mBranchType2+"/"+mRiskCode+"/"+mWageNo);
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError.buildErr(this,"没有得到足够的信息！");
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try
        {
            if (conn == null)
                return 0;
            st = conn.prepareStatement(sql);
            if (st == null)
                return 0;
            rs = st.executeQuery();
            if (rs.next())
            {
                return rs.getInt(1);
            }
            return 0;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return -1;
        } finally
        {
            try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}
        }
    }


    public static void main(String[] args)
    {      
        System.out.println("add over");
    }


}
