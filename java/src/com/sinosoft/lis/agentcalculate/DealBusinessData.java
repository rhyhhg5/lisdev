package com.sinosoft.lis.agentcalculate;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: 代理人考核过程中，代理人组别发生变动时，
 *                 该代理人相应的业务表，代理人信息表的AgentGroup
 *                 需要用新组别更新
 * </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class DealBusinessData
{
    //错误类处理
    public CErrors mErrors = new CErrors();

    //传入数据
    private VData mInputData = new VData();
    private String mAgentCode = "";
    private String mAgentGroup = "";
    private String mBranchAttr = "";
    private String mIndexCalNo = "";
    private String mFlag = ""; // Flag = "Y" --更新BranchCode

    // Flag = "N" --不需要更新BranchCode
    private String mStartDate = "";
    private String mEndDate = "";
    private String mBranchCode = "";
    private String mBranchSeries = "";

    public DealBusinessData()
    {
    }

    public static void main(String[] args)
    {
        DealBusinessData dealBusinessData1 = new DealBusinessData();
        dealBusinessData1.getTime();
    }

    public boolean updateAgentGroup(VData cInputData, Connection conn)
    {
        this.mInputData = cInputData;

        if (!getInputData())
        {
            return false;
        }
        System.out.println("mFlag:" + this.mFlag);
        if (!updateLACommision(conn))
        {
            return false;
        }
//tjj 修改2004-03-30 没必要更新下列表。
//    if (!updateLAHols(conn))
//      return false;
//
//    if (!updateLAIndexInfo(conn))
//      return false;
//
//    if (!updateLAIndexInfoTemp(conn))
//      return false;
//
//    if (!updateLAPresence(conn))
//      return false;
//
//    if (!updateLAQualityAssess(conn))
//      return false;
//
//    if (!updateLARewardPunish(conn))
//      return false;
//
//    if (!updateLATrain(conn))
//      return false;

        if (this.mFlag.trim().equals("Y"))
        {

            /** xjh **/
            //更新个人保单表
            if (!updateLBCont(conn))
            {
                return false;
            }
            if (!updateLCCont(conn))
            {
                return false;
            }
            //更新集体保单表
            if (!updateLBGrpCont(conn))
            {
                return false;
            }
            if (!updateLCGrpCont(conn))
            {
                return false;
            }

            if (!updateLBPol(conn))
            {
                return false;
            }
            if (!updateLCPol(conn))
            {
                return false;
            }
            if (!updateLBGrpPol(conn))
            {
                return false;
            }
            if (!updateLCGrpPol(conn))
            {
                return false;
            }

            /**朱向峰注释，为了暂时通过，以后需要改回
                   //更新报单信息
                   if (!updateLJAPayGrp(conn))
              return false;
                   //更新报单信息
                   if (!updateLJAPayPerson(conn))
              return false;
             }
             **/
//      if (!updateLJTempFee(conn))
//        return false;
        }

//    if (!updateLAWelfareInfo(conn))
//      return false;

//    if (!updateLBCont(conn))
//      return false;

//    if (!updateLCCont(conn))
//      return false;

//    if (!updateLCUWMaster(conn))
//      return false;

//    if (!updateLJABonusGet(conn))
//      return false;

//    if (!updateLJAGet(conn))
//      return false;
//
//    if (!updateLJAGetClaim(conn))
//      return false;
//
//    if (!updateLJAGetDraw(conn))
//      return false;
//
//    if (!updateLJAGetEndorse(conn))
//      return false;
//
//    if (!updateLJAGetOther(conn))
//      return false;
//
//    if (!updateLJAGetTempFee(conn))
//      return false;
//
//    if (!updateLJAPay(conn))
//      return false;
//
//    if (!updateLJAPayCont(conn))
//      return false;

//    if (!updateLJFIGet(conn))
//      return false;
//
//    if (!updateLJSGet(conn))select abs(months_between(y.paydate,x.cvalidate))
//      return false;
//
//    if (!updateLJSGetClaim(conn))
//      return false;
//
//    if (!updateLJSGetDraw(conn))
//      return false;
//
//    if (!updateLJSGetEndorse(conn))
//      return false;

//    if (!updateLJSGetOther(conn))
//      return false;
//
//    if (!updateLJSGetTempFee(conn))
//      return false;
//
//    if (!updateLJSPay(conn))
//      return false;
//
//    if (!updateLJSPayCont(conn))
//      return false;
//
//    if (!updateLJSPayGrp(conn))
//      return false;

//    if (!updateLJSPayPerson(conn))
//      return false;

//    if (!updateLPCont(conn))
//      return false;
//
//    if (!updateLPGrpPol(conn))
//      return false;
//
//    if (!updateLPPol(conn))
//      return false;

//    if (!updateLPUWMaster(conn))
//      return false;

        return true;

    }

    /**
     * updateLBGrpCont
     *
     * @param conn Connection
     * @return boolean
     */
    private boolean updateLBGrpCont(Connection conn)
    {
        ExeSQL tExeSQL = new ExeSQL(conn);
        String tSql = "update LBGrpCont set agentgroup='" + this.mBranchCode +
                      "' where agentcode='" + this.mAgentCode + "'";
        System.out.println("LBGrpCont:" + tSql);
        if (!tExeSQL.execUpdateSQL(tSql))
        {
            buildErrMsg("updateLBGrpCont",
                        "刷新LBGrpCont失败！" + tExeSQL.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    /**
     * updateLCGrpCont
     *
     * @param conn Connection
     * @return boolean
     */
    private boolean updateLCGrpCont(Connection conn)
    {
        ExeSQL tExeSQL = new ExeSQL(conn);
        String tSql = "update lCGrpCont set agentgroup='" + this.mBranchCode +
                      "' where agentcode='" + this.mAgentCode + "'";
        System.out.println("LCGrpCont:" + tSql);
        if (!tExeSQL.execUpdateSQL(tSql))
        {
            buildErrMsg("updateLCGrpCont",
                        "刷新LCGrpCont失败！" + tExeSQL.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    /**
     * updateLCCont
     *
     * @param conn Connection
     * @return boolean
     */
    private boolean updateLCCont(Connection conn)
    {
        ExeSQL tExeSQL = new ExeSQL(conn);
        String tSql = "update lccont set agentgroup='" + this.mBranchCode +
                      "' where agentcode='" + this.mAgentCode + "'";
        System.out.println("lccont:" + tSql);
        if (!tExeSQL.execUpdateSQL(tSql))
        {
            buildErrMsg("updateLCCont",
                        "刷新LCCont失败！" + tExeSQL.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    /**
     * updateLBCont
     *
     * @param conn Connection
     * @return boolean
     */
    private boolean updateLBCont(Connection conn)
    {
        ExeSQL tExeSQL = new ExeSQL(conn);
        String tSql = "update lbcont set agentgroup='" + this.mBranchCode +
                      "' where agentcode='" + this.mAgentCode + "'";
        System.out.println("lbcont:" + tSql);
        if (!tExeSQL.execUpdateSQL(tSql))
        {
            buildErrMsg("updateLBCont",
                        "刷新LBCont失败！" + tExeSQL.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLBPol(Connection conn)
    {
        ExeSQL tExeSQL = new ExeSQL(conn);
        String tSql = "update lbpol set agentgroup='" + this.mBranchCode +
                      "' where agentcode='" + this.mAgentCode + "'";
        System.out.println("lcpol:" + tSql);
        if (!tExeSQL.execUpdateSQL(tSql))
        {
            buildErrMsg("updateLBPol",
                        "刷新LBPol失败！" + tExeSQL.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLCPol(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSql = "update lcpol set agentgroup='" + this.mBranchCode +
                      "' where agentcode='" + this.mAgentCode + "'";
        System.out.println("lcpol:" + tSql);
        if (!tExeSql.execUpdateSQL(tSql))
        {
            buildErrMsg("updateLCPol",
                        "刷新LCPol失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
//    LCPolDBSet tLCPolDBSet = new LCPolDBSet(conn);
//    LCPolDB tLCPolDB = new LCPolDB();
//    LCPolSet tLCPolSet = new LCPolSet();
//    LCPolSchema tSch = new LCPolSchema();
//    LCPolSet tNewSet = new LCPolSet();
//
//    tLCPolDB.setAgentCode(this.mAgentCode);
//    tLCPolSet = tLCPolDB.query();
//    int tCount = tLCPolSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tLCPolSet.get(i);
//      tSch.setAgentGroup(this.mBranchCode);
//      tNewSet.add(tSch);
//    }
//
//    tLCPolDBSet.set(tNewSet);
//
//    if (!tLCPolDBSet.update()) {
//      buildErrMsg("updateLCPol", "刷新LCPol失败！" + tLCPolDBSet.mErrors.getFirstError());
//      return false;
//    }
        return true;
    }

    private boolean updateLBGrpPol(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSql = "update LBGrpPol set agentgroup='" + this.mBranchCode +
                      "' where agentcode='" + this.mAgentCode + "'";
        System.out.println("lcgrppol:" + tSql);
        if (!tExeSql.execUpdateSQL(tSql))
        {
            buildErrMsg("updateLBGrpPol",
                        "刷新LBGrpPol失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLCGrpPol(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSql = "update LCGrpPol set agentgroup='" + this.mBranchCode +
                      "' where agentcode='" + this.mAgentCode + "'";
        System.out.println("lcgrppol:" + tSql);
        if (!tExeSql.execUpdateSQL(tSql))
        {
            buildErrMsg("updateLCGrpPol",
                        "刷新LCGrpPol失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
//    LCGrpPolDBSet tDBSet = new LCGrpPolDBSet(conn);
//    LCGrpPolDB tDB = new LCGrpPolDB();
//    LCGrpPolSet tSet = new LCGrpPolSet();
//    LCGrpPolSchema tSch = new LCGrpPolSchema();
//    LCGrpPolSet tNewSet = new LCGrpPolSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    System.out.println("-----updateLCGrpPol---this.mAgentCode = " + this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    System.out.println("-----updateLCGrpPol---tSet.size = " + tSet.size());
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i =1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mBranchCode);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLCGrpPol", "刷新LCGrpPol失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
        return true;
    }

//  private boolean updateLCCont(Connection conn) {
//    LCContDBSet tDBSet = new LCContDBSet(conn);
//    LCContDB tDB = new LCContDB();
//    LCContSet tSet = new LCContSet();
//    LCContSchema tSch = new LCContSchema();
//    LCContSet tNewSet = new LCContSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLCCont", "刷新LCCont失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }

    private boolean updateLJTempFee(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "update LJTempFee set agentgroup = '" +
                      this.mBranchCode.trim()
                      + "' where agentcode = '" + this.mAgentCode.trim()
                      + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
                      + "'";

        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLJTempFee",
                        "LJTempFee刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

//  private boolean updateLJSPayCont(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSPayCont set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSPayCont", "LJSPayCont刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }

//  private boolean updateLJSGetEndorse(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSGetEndorse set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSGetEndorse", "LJSGetEndorse刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJSPayPerson(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSPayPerson set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSPayPerson", "LJSPayPerson刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJSPayGrp(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSPayGrp set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSPayGrp", "LJSPayGrp刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJSPay(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSPay set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSPay", "LJSPay刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }

    private boolean updateLJAPayPerson(Connection conn)
    {
        //刷新首年度的记录
        String tFirstSql =
                "select abs(months_between(y.paydate,x.cvalidate)),x.polno"
                + " from lcpol x,ljapayperson y ,LCCont z where "
                + " z.getpoldate >= '" + this.mStartDate.trim() + "' and"
                + " z.getpoldate <= '" + this.mEndDate.trim() + "' and"
                + " x.polno = y.polno and x.agentcode = y.agentcode and"
                + " z.contno = y.contno and"
                + " x.agentcode = '" + this.mAgentCode.trim() + "'";
        System.out.println("-----tFirstSql = " + tFirstSql);
        if (!doUpdPayPerson(tFirstSql, "F", conn))
        {
            return false;
        }

        //刷新续年度记录
        String tSecondSql =
                "select abs(months_between(y.paydate,x.cvalidate)),x.polno"
                + " from lcpol x,ljapayperson y ,LCCont z  where "
                + " y.makedate >= '" + this.mStartDate.trim() + "' and"
                + " y.makedate <= '" + this.mEndDate.trim() + "' and"
                + " x.polno = y.polno and x.agentcode = y.agentcode and"
                + " z.contno = y.contno and"
                + " x.agentcode = '" + this.mAgentCode.trim() + "'";
        System.out.println("-----tSecondSql = " + tSecondSql);
        if (!doUpdPayPerson(tSecondSql, "S", conn))
        {
            return false;
        }

        return true;
    }

    //tFlag = 'F':首年度 'S':续年度
    private boolean doUpdPayPerson(String tSql, String tFlag, Connection conn)
    {
        ExeSQL tExe = new ExeSQL(conn);
        SSRS tRS = new SSRS();
        int MaxRow = 0;
        int MaxCol = 0;
        String tPolNo = "";
        String tStrIntvl = "";
        float tIntvl = 0;
        String tUpdSql = "";

        tRS = tExe.execSQL(tSql);
        MaxRow = tRS.getMaxRow();
        MaxCol = tRS.getMaxCol();
        String tResult[][] = new String[MaxRow][MaxCol];
        tResult = tRS.getAllData();

        for (int i = 0; i < MaxRow; i++)
        {
            tStrIntvl = tResult[i][0];
            tPolNo = tResult[i][1];
            tIntvl = Float.parseFloat(tStrIntvl);
            if (tFlag.trim().equals("F"))
            {
                if (tIntvl < 12)
                {
                    tUpdSql = "update ljapayperson set agentgroup = '"
                              + this.mBranchCode.trim() +
                              "' where agentcode = '"
                              + this.mAgentCode.trim() + "' and polno = '"
                              + tPolNo.trim() + "'";
                    if (!tExe.execUpdateSQL(tUpdSql))
                    {
                        buildErrMsg(" updateLJAPayPerson ",
                                    "刷新LJAPayGrp失败！" +
                                    tExe.mErrors.getFirstError());
                        return false;
                    }
                }
            }
            else
            {
                if (tIntvl > 12)
                {
                    tUpdSql = "update ljapayperson set agentgroup = '"
                              + this.mBranchCode.trim() +
                              "' where agentcode = '"
                              + this.mAgentCode.trim() + "' and polno = '"
                              + tPolNo.trim() + "'";
                    if (!tExe.execUpdateSQL(tUpdSql))
                    {
                        buildErrMsg(" updateLJAPayGrp ",
                                    "刷新LJAPayGrp失败！" +
                                    tExe.mErrors.getFirstError());
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean updateLJAPayGrp(Connection conn)
    {

        //刷新首年度的记录
        String tFirstSql =
                "select abs(months_between(y.paydate,x.cvalidate)),x.GrpContNo"
                + " from lcgrppol x,ljapaygrp y ,LCGrpCont z where "
                + " z.getpoldate >= '" + this.mStartDate.trim() + "' and"
                + " y.GrpContNo = z.GrpContNo and"
                + " x.agentcode = y.agentcode and"
                + " y.GrpPolNo = x.GrpPolNo and"
                + " x.agentcode = '" + this.mAgentCode.trim() + "'";
        System.out.println("-----tFirstSql = " + tFirstSql);
        if (!doUpdPayGrp(tFirstSql, "F", conn))
        {
            return false;
        }

        //刷新续年度记录
        String tSecondSql =
                "select abs(months_between(y.paydate,x.cvalidate)),x.GrpContNo"
                + " from lcgrppol x,ljapaygrp y ,LCGrpCont z where "
                + " y.makedate >= '" + this.mStartDate.trim() + "' and"
                + " y.GrpContNo = z.GrpContNo and"
                + " x.agentcode = y.agentcode and"
                + " y.GrpPolNo = x.GrpPolNo and"
                + " x.agentcode = '" + this.mAgentCode.trim() + "'";
        System.out.println("-----tSecondSql = " + tSecondSql);
        if (!doUpdPayGrp(tSecondSql, "S", conn))
        {
            return false;
        }
        return true;
    }

    //tFlag = 'F':首年度 'S':续年度
    private boolean doUpdPayGrp(String tSql, String tFlag, Connection conn)
    {
        ExeSQL tExe = new ExeSQL(conn);
        SSRS tRS = new SSRS();
        int MaxRow = 0;
        int MaxCol = 0;
        String tContNo = "";
        String tStrIntvl = "";
        float tIntvl = 0;
        String tUpdSql = "";

        tRS = tExe.execSQL(tSql);
        MaxRow = tRS.getMaxRow();
        MaxCol = tRS.getMaxCol();
        String tResult[][] = new String[MaxRow][MaxCol];
        tResult = tRS.getAllData();

        for (int i = 0; i < MaxRow; i++)
        {
            tStrIntvl = tResult[i][0];
            tContNo = tResult[i][1];
            tIntvl = Float.parseFloat(tStrIntvl);
            if (tFlag.trim().equals("F"))
            {
                if (tIntvl < 12)
                {
                    tUpdSql = "update ljapaygrp set agentgroup = '"
                              + this.mBranchCode.trim() +
                              "' where agentcode = '"
                              + this.mAgentCode.trim() + "' and GrpContNo = '"
                              + tContNo.trim() + "'";
                    if (!tExe.execUpdateSQL(tUpdSql))
                    {
                        buildErrMsg(" updateLJAPayGrp ",
                                    "刷新LJAPayGrp失败！" +
                                    tExe.mErrors.getFirstError());
                        return false;
                    }
                }
            }
            else
            {
                if (tIntvl > 12)
                {
                    tUpdSql = "update ljapaygrp set agentgroup = '"
                              + this.mBranchCode.trim() +
                              "' where agentcode = '"
                              + this.mAgentCode.trim() + "' and GrpContNo = '"
                              + tContNo.trim() + "'";
                    if (!tExe.execUpdateSQL(tUpdSql))
                    {
                        buildErrMsg(" updateLJAPayGrp ",
                                    "刷新LJAPayGrp失败！" +
                                    tExe.mErrors.getFirstError());
                        return false;
                    }
                }
            }
        }
        return true;
    }

//  private boolean updateLJAPayCont(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJAPayCont set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJAPayCont", "LJAPayCont刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }

//  private boolean updateLJAPay(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJAPay set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJAPay", "LJAPay刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }

//  private boolean updateLJAGetEndorse(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJAGetEndorse set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJAGetEndorse", "LJAGetEndorse刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJSGetDraw(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSGetDraw set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSGetDraw", "LJSGetDraw刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJSGet(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSGet set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSGet", "LJSGet刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }

//  private boolean updateLJAGet(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJAGet set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJAGet", "LJAGet刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJAGetDraw(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJAGetDraw set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJAGetDraw", "LJAGetDraw刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJFIGet(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJFIGet set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJFIGet", "LJFIGet刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJSGetTempFee(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSGetTempFee set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSGetTempFee", "LJSGetTempFee刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJAGetTempFee(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJAGetTempFee set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJAGetTempFee", "LJAGetTempFee刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJSGetOther(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSGetOther set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSGetOther", "LJSGetOther刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJAGetOther(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJAGetOther set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJAGetOther", "LJAGetOther刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJSGetClaim(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJSGetClaim set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJSGetClaim", "LJSGetClaim刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }
//
//  private boolean updateLJAGetClaim(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJAGetClaim set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJAGetClaim", "LJAGetClaim刷新失败！" + tExeSql.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//
//  }

//  private boolean updateLCUWMaster(Connection conn) {
//    LCUWMasterDBSet tDBSet = new LCUWMasterDBSet(conn);
//    LCUWMasterDB tDB = new LCUWMasterDB();
//    LCUWMasterSet tSet = new LCUWMasterSet();
//    LCUWMasterSchema tSch = new LCUWMasterSchema();
//    LCUWMasterSet tNewSet = new LCUWMasterSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLCUWMaster", "刷新LCUWMaster失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }
//
//  private boolean updateLPGrpPol(Connection conn) {
//    LPGrpPolDBSet tDBSet = new LPGrpPolDBSet(conn);
//    LPGrpPolDB tDB = new LPGrpPolDB();
//    LPGrpPolSet tSet = new LPGrpPolSet();
//    LPGrpPolSchema tSch = new LPGrpPolSchema();
//    LPGrpPolSet tNewSet = new LPGrpPolSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLPGrpPol", "刷新LPGrpPol失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }
//
//  private boolean updateLPCont(Connection conn) {
//    LPContDBSet tDBSet = new LPContDBSet(conn);
//    LPContDB tDB = new LPContDB();
//    LPContSet tSet = new LPContSet();
//    LPContSchema tSch = new LPContSchema();
//    LPContSet tNewSet = new LPContSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLPCont", "刷新LPCont失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }
//
//  private boolean updateLPPol(Connection conn) {
//    LPPolDBSet tDBSet = new LPPolDBSet(conn);
//    LPPolDB tDB = new LPPolDB();
//    LPPolSet tSet = new LPPolSet();
//    LPPolSchema tSch = new LPPolSchema();
//    LPPolSet tNewSet = new LPPolSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLPPol", "刷新LPPol失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }

//  private boolean updateLBGrpPol(Connection conn) {
//    LBGrpPolDBSet tDBSet = new LBGrpPolDBSet(conn);
//    LBGrpPolDB tDB = new LBGrpPolDB();
//    LBGrpPolSet tSet = new LBGrpPolSet();
//    LBGrpPolSchema tSch = new LBGrpPolSchema();
//    LBGrpPolSet tNewSet = new LBGrpPolSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLBGrpPol", "刷新LBGrpPol失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }

//  private boolean updateLBCont(Connection conn) {
//    LBContDBSet tDBSet = new LBContDBSet(conn);
//    LBContDB tDB = new LBContDB();
//    LBContSet tSet = new LBContSet();
//    LBContSchema tSch = new LBContSchema();
//    LBContSet tNewSet = new LBContSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLBCont", "刷新LBCont失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }
//
//  private boolean updateLBPol(Connection conn) {
//    LBPolDBSet tDBSet = new LBPolDBSet(conn);
//    LBPolDB tDB = new LBPolDB();
//    LBPolSet tSet = new LBPolSet();
//    LBPolSchema tSch = new LBPolSchema();
//    LBPolSet tNewSet = new LBPolSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i =1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLBPol", "刷新LBPol失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }
//
//  private boolean updateLPUWMaster(Connection conn) {
//    LPUWMasterDBSet tDBSet = new LPUWMasterDBSet(conn);
//    LPUWMasterDB tDB = new LPUWMasterDB();
//    LPUWMasterSet tSet = new LPUWMasterSet();
//    LPUWMasterSchema tSch = new LPUWMasterSchema();
//    LPUWMasterSet tNewSet = new LPUWMasterSet();
//
//    tDB.setAgentCode(this.mAgentCode);
//    tSet = tDB.query();
//    int tCount = tSet.size();
//    if ( tCount == 0 )
//      return true;
//    int i = 0;
//    tNewSet.clear();
//    for (i = 1; i <= tCount; i++) {
//      tSch = tSet.get(i);
//      tSch.setAgentGroup(this.mAgentGroup);
//      tNewSet.add(tSch);
//    }
//
//    tDBSet.set(tNewSet);
//
//    if (!tDBSet.update()) {
//      buildErrMsg("updateLPUWMaster", "刷新LPUWMaster失败！" + tDBSet.mErrors.getFirstError());
//      return false;
//    }
//    return true;
//  }
//
//  private boolean updateLJABonusGet(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update LJABonusGet set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLJABonusGet", "LJABounsGet刷新失败！" + tExeSql.mErrors.getLastError());
//      return false;
//    }
//    return true;
//
//  }

    private boolean updateLAPresence(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "update lapresence set agentgroup = '" +
                      this.mAgentGroup.trim()
                      + "' where agentcode = '" + this.mAgentCode.trim()
                      + "' and makedate >= '" + this.mStartDate.trim()
                      + "'";

        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLAPresence",
                        "LAPresence刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

//  此表暂不加agentgroup字段
//  private boolean updateLAWelfareInfo(Connection conn) {
//    ExeSQL tExeSql = new ExeSQL(conn);
//    String tSQL = "update lawelfareinfo set agentgroup = '" +
//        this.mAgentGroup.trim()
//        + "' where trim(agentcode) = '" + this.mAgentCode.trim()
//        + "' and makedate >= '" + this.mStartDate.trim()
//        + "' and makedate <= '" + this.mEndDate.trim()
//        + "'";
//
//    if (!tExeSql.execUpdateSQL(tSQL)) {
//      buildErrMsg("updateLAWelfareInfo", "刷新失败！");
//      return false;
//    }
//    return true;
//  }

    private boolean updateLAHols(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "update lahols set agentgroup = '" +
                      this.mAgentGroup.trim()
                      + "' where agentcode = '" + this.mAgentCode.trim()
                      + "' and makedate >= '" + this.mStartDate.trim()
                      + "'";

        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLAHols",
                        "LAHols刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLARewardPunish(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "update larewardpunish set agentgroup = '" +
                      this.mAgentGroup.trim()
                      + "' where agentcode = '" + this.mAgentCode.trim()
                      + "' and makedate >= '" + this.mStartDate.trim()
                      + "'";

        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLARewardPunish",
                        "LARewardPunish刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLATrain(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "update latrain set agentgroup = '" +
                      this.mAgentGroup.trim()
                      + "' where agentcode = '" + this.mAgentCode.trim()
                      + "' and makedate >= '" + this.mStartDate.trim()
                      + "'";

        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLATrain",
                        "LATrain刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLAIndexInfo(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "update laindexinfo set agentgroup = '" +
                      this.mAgentGroup.trim()
                      + "' where agentcode = '" + this.mAgentCode.trim()
                      + "' and makedate >= '" + this.mStartDate.trim()
                      + "'";

        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLAIndexInfo",
                        "LAIndexInfo刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLACommision(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "";

        //更新业绩前校验
        AgentPubFun tAPF = new AgentPubFun();
        String tCheckFlag = tAPF.AdjustCommCheck(this.mAgentCode.trim(),
                                                 this.mStartDate.trim());
        if (!tCheckFlag.trim().equals("00"))
        {
            buildErrMsg("updateLACommision", tCheckFlag.trim());
            return false;
        }

        if (this.mFlag.trim().equalsIgnoreCase("Y"))
        {
            tSQL = "update lacommision set agentgroup = '" +
                   this.mAgentGroup.trim()
                   + "' , branchcode = '" + this.mBranchCode.trim()
                   + "' , branchattr = '" + this.mBranchAttr.trim()
                   + "' , branchseries = '" + this.mBranchSeries.trim()
                   + "' where agentcode = '" + this.mAgentCode.trim()
                   + "' and (caldate is null or caldate >= '" +
                   this.mStartDate.trim()
                   + "')";
        }
        else
        {
            int tLen = this.mBranchAttr.length();
            if (tLen == 18)
            {
                tSQL = "update lacommision set agentgroup = '" +
                       this.mAgentGroup.trim()
                       + "' , branchattr = '" + this.mBranchAttr.trim()
                       + "' where agentcode = '" + this.mAgentCode.trim()
                       + "' and (caldate is null or caldate >= '" +
                       this.mStartDate.trim()
                       + "')";
            }
            else
            {
                tSQL = "update lacommision set agentgroup = '" +
                       this.mAgentGroup.trim()
                       + "' where agentcode = '" + this.mAgentCode.trim()
                       + "' and (caldate is null or caldate >= '" +
                       this.mStartDate.trim()
                       + "')";
            }
        }

        System.out.println("---updLACommision---tSQL : " + tSQL);
        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLACommision",
                        "LACommision刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLAQualityAssess(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "update laqualityassess set agentgroup = '" +
                      this.mAgentGroup.trim()
                      + "' where agentcode = '" + this.mAgentCode.trim()
                      + "' and makedate >= '" + this.mStartDate.trim() + "'";

        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLAQualityAssess",
                        "LAQualityAssess刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean updateLAIndexInfoTemp(Connection conn)
    {
        ExeSQL tExeSql = new ExeSQL(conn);
        String tSQL = "update laindexinfotemp set agentgroup = '" +
                      this.mAgentGroup.trim()
                      + "' where agentcode = '" + this.mAgentCode.trim()
                      + "' and makedate >= '" + this.mStartDate.trim() + "'";

        if (!tExeSql.execUpdateSQL(tSQL))
        {
            buildErrMsg("updateLAIndexInfoTemp",
                        "LAIndexInfoTemp刷新失败！" + tExeSql.mErrors.getFirstError());
            return false;
        }
        return true;
    }

    private boolean getInputData()
    {
        this.mAgentCode = (String)this.mInputData.getObjectByObjectName(
                "String", 0);
        this.mAgentGroup = (String)this.mInputData.getObjectByObjectName(
                "String",
                1);
        this.mFlag = (String)this.mInputData.getObjectByObjectName("String", 2);
        this.mBranchAttr = (String)this.mInputData.getObjectByObjectName(
                "String",
                3);
        this.mIndexCalNo = (String)this.mInputData.getObjectByObjectName(
                "String",
                4);
        this.mBranchCode = (String)this.mInputData.getObjectByObjectName(
                "String",
                5);
        this.mBranchSeries = (String)this.mInputData.getObjectByObjectName(
                "String",
                6);

        if (this.mAgentCode == null || this.mAgentCode.equals("")
            || this.mAgentGroup == null || this.mAgentGroup.equals("")
            || this.mIndexCalNo == null || this.mIndexCalNo.equals(""))
        {
            buildErrMsg("getInputData", "传入代理人信息为空或考核年月代码为空！");
        }

        if (this.mStartDate == null || this.mStartDate.equals("") ||
            this.mEndDate == null || this.mEndDate.equals(""))
        {
            getTime();
        }
        return true;
    }

    private void getTime()
    {
        PubFun tPF = new PubFun();
        String tMon = this.mIndexCalNo.trim().substring(4);
        String tYear = this.mIndexCalNo.trim().substring(0, 4);
        if (tMon.equals("12"))
        {
            tMon = "01";
            tYear = Integer.toString(Integer.parseInt(tYear) + 1);
        }
        else
        {
            tMon = Integer.toString(Integer.parseInt(tMon) + 1);
        }
        int tLength = tMon.length();
        if (tLength == 1)
        {
            tMon = "0" + tMon;
        }
        String tDate = tYear.trim() + "-" + tMon.trim() + "-" + "01";

        this.mStartDate = tDate;
        this.mEndDate = tPF.getCurrentDate();
        System.out.println("--DealBusinessData--getTime--StartDate--" +
                           this.mStartDate);
        System.out.println("--DealBusinessData--getTime--EndDate--" +
                           this.mEndDate);
    }

    public void inputPeriod(String tStartDate, String tEndDate)
    {
        this.mStartDate = tStartDate;
        this.mEndDate = tEndDate;
    }

    private void buildErrMsg(String FuncName, String ErrMsg)
    {
        CError tError = new CError();

        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;
        tError.moduleName = "DealBusinessData";

        this.mErrors.addOneError(tError);
    }
}
