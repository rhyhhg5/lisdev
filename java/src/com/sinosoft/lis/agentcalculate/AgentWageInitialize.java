package com.sinosoft.lis.agentcalculate;

import java.util.HashMap;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 此类只用来初始化一些配置数据到内存中，用来减少对于数据库的访问 
 * 所以，此类中添加的初始化数据，必须是占用内存较少的
 * 比如：渠道与展业类型的对应关系，地区类型的对应关系，万能险区分基本保费和额外保费的配置，及基本保费和额外保费的算法等
 * @author yangyang
 *
 */
public class AgentWageInitialize {

	/**保单层的销售渠道对应销售中的展业类型加渠道
	 *  key--salechnl，value是数组--branchtype,branchtype2 
	 */
	private static HashMap<String,String[]> mSaleChnlToBranchType = new HashMap<String,String[]>();
	
	/**回访险种配置 
	 * key--riskcode,value--标志'Y'
	 */
	private static HashMap<String,String> mVisitRiskCode = new HashMap<String,String>();
	
	/**存储的是所有的万能险，分为三大类：1、税优产品，会区分账户保费和风险保费   --Y
	 * 						  2、区分 基本保费和额外保费的万能险,配置对应的算法
	 * 						  3、不区分的万能险 
	 * key--riskcode,value--算法代码或'N'
	 * Universal Life Ins 万能险全称
	 */
	private static HashMap<String,String> mUniversalLifeIns = new HashMap<String,String>();
	
	/**对于湖南计算万能险基本保费时特殊的保单加载 到内存中 */
	private static HashMap<String,Integer> mHuiNanSpecialPolicy = new HashMap<String,Integer>();
	
	/**查看目前commisionsn的最大号，配置表中最大只能存储9位*/
	private static int mInitializeCommisionsn;
	
	/**本次程序生成了多少 条数据*/
	private static int mUseNo = 0;
	
	/**根据展业类型（branchtype），渠道（branchtype2）,险种（riskcode）三个字段来获得计算fyc的代码
	 * 只存储lawagecalelement中caltype=00的配置*/
	private static HashMap<String,String> mCalFycCode = new HashMap<String,String>();
	
	/**根据展业类型（branchtype），渠道（branchtype2）,险种（riskcode）三个字段来获得计算标准保费的代码
	 * 只存储lawagecalelement中caltype=03的配置*/
	private static HashMap<String,String> mCalStandPremCode = new HashMap<String,String>();
	
	/**根据展业类型（branchtype），渠道（branchtype2）,险种（riskcode）三个字段来获得计算职团标准保费的代码
	 * 只存储lawagecalelement中caltype=06的配置*/
	public static HashMap<String,String> mCalStandPremGroupCode = new HashMap<String,String>();
	
	/**根据展业类型（branchtype），渠道（branchtype2）,险种（riskcode）三个字段来获得计算fyc的代码,其中fyc是用管理费来计算的
	 * 只存储lawagecalelement中caltype=10的配置*/
	private static HashMap<String,String> mCalSpecialFycCode = new HashMap<String,String>();
	
	/**根据展业类型（branchtype），渠道（branchtype2）,险种（riskcode）三个字段来获得计算手续费的代码,
	 * 只存储lawagecalelement中branchtype2='02'的配置,是用来计算手续费的
	 * 目前只用团险中介是在批处理中计算手续费的，所以此配置里面只存储团险的*/
	private static HashMap<String,String> mCalChargeCode = new HashMap<String,String>();
	
	/**
	 * 用来存储目前扎账提数中的错误代码与其对应的描述
	 */
	private static HashMap<String,String> mErrorDescribe = new HashMap<String,String>();
	
	/**
	 * 用来存储标准保费比例计算时特殊的险种
	 */
	private static HashMap<String,String> mStandPremFycRiskCode = new HashMap<String,String>();
	
	private ExeSQL mExeSQL = new ExeSQL();
	
	public AgentWageInitialize()
	{
		System.out.println("AgentWageInitialize-->AgentWageInitialize:开始执行");
		init();
		System.out.println("AgentWageInitialize-->AgentWageInitialize:执行完成");
	}
	
	private void init()
	{
		initSwitchSaleChnl();
		initVisitRiskCode();
		initUniversalLifeIns();
		initMaxCommisionSN();
		initHuiNanSpecialPolicy();
		initCalCode();
		initErrorDescribe();
		initStandPremFycRiskCode();
	}
	
	/**
	 * 初始化目前系统中对于保单销售渠道与销售中的展业类型转化 
	 */
	private void initSwitchSaleChnl()
	{
		String sql  = "select code,code1,codename from  ldcode1 where codetype ='salechnl' "
					+ " and code not in ('11','12') "
//					+ " and code in  (select code from ldcode where codetype in ('unitesalechnlsgrp') "//团单销售渠道 
//					+ "		   union select code from ldcode where codetype in ('unitesalechnl')) "//个单销售渠道 
//					+ " union "
//					+ " select '08','4','01' from dual "//兼容历史数据，电话销售
//					+ " union "
//					+ " select '07','2','01' from dual "//兼容历史渠道，职团开拓
					+ " with ur";
		System.out.println("AgentWageInitialize-->initSwitchSaleChnl:"+sql);
		SSRS tSSRS = mExeSQL.execSQL(sql);
		for(int i = 1;i<=tSSRS.getMaxRow();i++)
		{
			String tSaleChnl = tSSRS.GetText(i, 1);
			String tBranchType = tSSRS.GetText(i, 2);
			String tBranchType2 = tSSRS.GetText(i, 3);
			String[] tBranchTypes = new String[]{tBranchType,tBranchType2};
			if(!mSaleChnlToBranchType.containsKey(tSaleChnl))
			{
				mSaleChnlToBranchType.put(tSaleChnl, tBranchTypes);
			}
		}
		System.out.println("AgentWageInitialize-->initSwitchSaleChnl:保单销售渠道转化展业类型初始化成功");
	}
	/**
	 * 初始化目前所有的校验回访日期的险种
	 * 个险保障期间一年期以上产品 及 保证续保产品
	 */
	private void initVisitRiskCode()
	{
   	 	String sql = "select riskcode,'Y' from lmriskapp where riskprop='I'  and  RiskPeriod = 'L' "
                    +" union "
                    +" select b.riskcode,'Y' from lmrisk a ,lmriskapp b where a.RnewFlag='B' and a.riskcode=b.riskcode and b.riskprop='I' with ur";
   	 	System.out.println("AgentWageInitialize-->initVisitRiskCode:"+sql);
   	 	SSRS tSSRS = mExeSQL.execSQL(sql);
		for(int i = 1;i<=tSSRS.getMaxRow();i++)
		{
			String tRiskCode = tSSRS.GetText(i, 1);
			String tVisitFlag = tSSRS.GetText(i, 2);
			if(!mVisitRiskCode.containsKey(tRiskCode))
			{
				mVisitRiskCode.put(tRiskCode, tVisitFlag);
			}
		}
		System.out.println("回访险种始化成功");
	}
	
	/**
	 * 初始化目前所有的万能险，如果区分基本保费和额外保费，则把其算法代码也加载到内存中
	 * 如果 有新上的万能险，要根据需求，进行配置表ldcode,codetype=universallifeins
	 */
	private void initUniversalLifeIns()
	{
   	 	String sql = "select code,codename from ldcode where codetype ='universallifeins' with ur ";
   	 	System.out.println("AgentWageInitialize-->initUniversalLifeIns:"+sql);
   	 	SSRS tSSRS = mExeSQL.execSQL(sql);
		for(int i = 1;i<=tSSRS.getMaxRow();i++)
		{
			String tRiskCode = tSSRS.GetText(i, 1);
			String tCalCode = tSSRS.GetText(i, 2);
			if(!mUniversalLifeIns.containsKey(tRiskCode))
			{
				mUniversalLifeIns.put(tRiskCode, tCalCode);
			}
		}
		System.out.println("万能险险种始化成功");
	}
	/**
	 * 得到目前lacommisionnew表中最大commisionsn与ldmaxno中最大commisionsn中两个的最大值 
	 * 取lacommisionnew表时，加了个条件tmakedate>='2017-1-1',理论上来说不会再修改2017之前的数据，且去除手工维护过的数据
	 */
	private void initMaxCommisionSN()
	{
		String tSqlCommision = "select substr(max(commisionsn),1,9) from lacommisionnew where tmakedate>='2017-1-1' and translate(commisionsn,'', '0123456789') ='' with ur";
		String tSqlMaxNo = " select maxno from ldmaxno where notype ='COMMISIONSNNEW'  and nolimit ='SN' with ur";
		System.out.println("AgentWageInitialize-->initMaxCommisionSN:tSqlCommision:"+tSqlCommision);
		System.out.println("AgentWageInitialize-->initMaxCommisionSN:tSqlMaxNo:"+tSqlMaxNo);
		String tCommisionSn = mExeSQL.getOneValue(tSqlCommision);
		String tMaxNo = mExeSQL.getOneValue(tSqlMaxNo);
		if("".equals(tMaxNo)||null==tMaxNo)
		{
			System.out.println("ldmaxno表没有查询到数据，是不是有人误删除了");
			tMaxNo = "0";
		}
		int tMaxNo1 = Integer.valueOf(tCommisionSn);
		int tMaxNo2 = Integer.valueOf(tMaxNo);
		mInitializeCommisionsn = tMaxNo1>=tMaxNo2?tMaxNo1:tMaxNo2;
		System.out.println("commisionsn最大值初始化成功");
	}
	
	/**
	 * 对于湖南特殊的保单进行初始化加载到内存中
	 */
	private void initHuiNanSpecialPolicy()
	{
		String sql = "select edorno,count(1) from lpedorespecialdata where edorvalue='1' and edortype = 'WN' and detailtype = 'ULIFEE' group by edorno with ur";
		System.out.println("AgentWageInitialize-->initHuiNanSpecialPolicy:"+sql);
		SSRS tSSRS = mExeSQL.execSQL(sql);
		for(int i = 1;i<=tSSRS.getMaxRow();i++)
		{
			String tPolicyNo = tSSRS.GetText(i, 1);
			int tCount = Integer.valueOf(tSSRS.GetText(i, 2));
			if(!mHuiNanSpecialPolicy.containsKey(tPolicyNo))
			{
				mHuiNanSpecialPolicy.put(tPolicyNo, tCount);
			}
		}
		System.out.println("湖南特殊的保单进行始化成功");
	}
	
	/**
	 * 把lawagecalelement中的配置加载到内存中
	 * 包括：所有渠道的fyc计算及团险中介的手续费配置
	 */
	private void initCalCode()
	{
		String sql = "select trim(branchtype)||trim(branchtype2)||trim(riskcode),caltype,calcode from lawagecalelement where ((branchtype2 ='01' and caltype in ('00','03','06','10')) or (branchtype='2' and branchtype2='02' and caltype ='51')) with ur";
		System.out.println("AgentWageInitialize-->initCalCode:"+sql);
		SSRS tSSRS = mExeSQL.execSQL(sql);
		for(int i = 1;i<=tSSRS.getMaxRow();i++)
		{
			String tKey = tSSRS.GetText(i, 1);
			String tCalType = tSSRS.GetText(i, 2);
			String tCalCode = tSSRS.GetText(i, 3);
			if("00".equals(tCalType))
			{
				if(!mCalFycCode.containsKey(tKey))
				{
					mCalFycCode.put(tKey, tCalCode);
				}
			}
			else if("03".equals(tCalType))
			{
				if(!mCalStandPremCode.containsKey(tKey))
				{
					mCalStandPremCode.put(tKey, tCalCode);
				}
			}
			else if("06".equals(tCalType))
			{
				if(!mCalStandPremGroupCode.containsKey(tKey))
				{
					mCalStandPremGroupCode.put(tKey, tCalCode);
				}
			}
			else if("10".equals(tCalType))
			{
				if(!mCalSpecialFycCode.containsKey(tKey))
				{
					mCalSpecialFycCode.put(tKey, tCalCode);
				}
			}
			else if("51".equals(tCalType))
			{
				if(!mCalChargeCode.containsKey(tKey))
				{
					mCalChargeCode.put(tKey, tCalCode);
				}
			}
		}
		System.out.println("fyc提奖比例相关配置始化成功");
	}
	/**
	 * 初始化目前所有的扎账提数错误代码
	 */
	private void initErrorDescribe()
	{
   	 	String sql = "select code,codename from ldcode where codetype ='lacommisionerror' with ur";
   	 	System.out.println("AgentWageInitialize-->initErrorDescribe:"+sql);
   	 	SSRS tSSRS = mExeSQL.execSQL(sql);
		for(int i = 1;i<=tSSRS.getMaxRow();i++)
		{
			String tRiskCode = tSSRS.GetText(i, 1);
			String tCalCode = tSSRS.GetText(i, 2);
			if(!mErrorDescribe.containsKey(tRiskCode))
			{
				mErrorDescribe.put(tRiskCode, tCalCode);
			}
		}
		System.out.println("扎账提数错误代码始化成功");
	}
	/**
	 * 初始化团险折标比例特殊险种
	 */
	private void initStandPremFycRiskCode()
	{
		String sql = "select codename,othersign from ldcode where codetype='GSPRISKCODE' and comcode='86' with ur";
		System.out.println("AgentWageInitialize-->initStandPremFycRiskCode:"+sql);
		SSRS tSSRS = mExeSQL.execSQL(sql);
		for(int i = 1;i<=tSSRS.getMaxRow();i++)
		{
			String tRiskCode = tSSRS.GetText(i, 1);
			String tCalCode = tSSRS.GetText(i, 2);
			if(!mStandPremFycRiskCode.containsKey(tRiskCode))
			{
				mStandPremFycRiskCode.put(tRiskCode, tCalCode);
			}
		}
		System.out.println("团险折标比例特殊险种始化成功");
	}
	
	public static HashMap<String, String[]> getmSaleChnlToBranchType() {
		return mSaleChnlToBranchType;
	}

	public static HashMap<String, String> getmVisitRiskCode() {
		return mVisitRiskCode;
	}

	public static HashMap<String, String> getmUniversalLifeIns() {
		return mUniversalLifeIns;
	}

	public static HashMap<String, Integer> getmHuiNanSpecialPolicy() {
		return mHuiNanSpecialPolicy;
	}

	public static int getmInitializeCommisionsn() {
		return mInitializeCommisionsn;
	}

	public static int getmUseNo() {
		return mUseNo;
	}
	
	public static void setmUseNo(int mUseNo) {
		AgentWageInitialize.mUseNo += mUseNo;
	}
	public static HashMap<String, String> getmCalFycCode() {
		return mCalFycCode;
	}

	public static HashMap<String, String> getmCalStandPremCode() {
		return mCalStandPremCode;
	}

	public static HashMap<String, String> getmCalStandPremGroupCode() {
		return mCalStandPremGroupCode;
	}

	public static HashMap<String, String> getmCalSpecialFycCode() {
		return mCalSpecialFycCode;
	}

	public static HashMap<String, String> getmCalChargeCode() {
		return mCalChargeCode;
	}

	public static HashMap<String, String> getmErrorDescribe() {
		return mErrorDescribe;
	}

	public static HashMap<String, String> getmStandPremFycRiskCode() {
		return mStandPremFycRiskCode;
	}
	
}
