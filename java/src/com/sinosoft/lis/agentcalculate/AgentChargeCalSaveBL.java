/*
 * <p>ClassName: AgentWageCalSaveUI </p>
 * <p>Description: LAAgentUI类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-31
 */
package com.sinosoft.lis.agentcalculate;

import java.math.BigDecimal;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AgentChargeCalSaveBL {

	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	private VData mInputData = new VData();
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	private String mOperate;
	private String mManageCom;
	private String mBranchType;
	private String mBranchType2;
	private String mMakeDate;
	private GlobalInput mGlobalInput = new GlobalInput();
	private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();

	public AgentChargeCalSaveBL() {
	}

	public static void main(String[] args) {
		AgentChargeCalSaveBL tAgentWageCalSaveUI = new AgentChargeCalSaveBL();
		VData tVData = new VData();
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备往后台的数据
		// if (!prepareOutputData()) {
		// return false;
		// }
		// PubSubmit tPubSubmit = new PubSubmit();
		// System.out.println("Start AgentChargeCalSaveNewBL Submit...");
		// try {
		// if (!tPubSubmit.submitData(mInputData, mOperate)) {
		// // @@错误处理
		// System.out.println("手续费计算提交失败");
		// this.mErrors.copyAllErrors(tPubSubmit.mErrors);
		// CError tError = new CError();
		// tError.moduleName = "AgentWageCalSaveNewBL";
		// tError.functionName = "submitData";
		// tError.errorMessage = "数据提交失败!";
		// this.mErrors.addOneError(tError);
		// return false;
		// }
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
		System.out.println("End AgentChargeCalSaveNewBL Submit...");
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	// private boolean prepareOutputData() {
	// try {
	// System.out
	// .println("Begin LAActiveChargeBL.prepareOutputData.........");
	// mInputData.clear();
	// mInputData.add(map);
	// System.out
	// .println("End LAActiveChargeBL.prepareOutputData.........");
	// } catch (Exception ex) {
	// // @@错误处理
	// System.out.println("准备提交数据时失败");
	// return false;
	// }
	// return true;
	// }

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	public boolean dealData() {
		System.out.println("AgentChargeCalSaveBL-->dealData:开始执行");
		String YearMonth = mMakeDate.substring(0, 4)
				+ mMakeDate.substring(5, 7);
		String endDateSql = "select (enddate -6 day),enddate  from lastatsegment where stattype = '1' and yearmonth = '"
				+ YearMonth + "'";
		ExeSQL eExeSQL = new ExeSQL();
		SSRS eSSRS = eExeSQL.execSQL(endDateSql);
		String checkSql = "select date('"+mMakeDate+"') +1 day from dual ";
		String checkDate = eExeSQL.getOneValue(checkSql);
		String cSQL = "select * from lacommision where managecom = '"
				+ mManageCom
				+ "' and "
				+ " branchtype = '"
				+ mBranchType
				+ "' and branchtype2 = '"
				+ mBranchType2
				+ "' "
				// branchtype3='2' 表示交叉销售出单，互动中介交叉销售只有相互代理
				+ " and branchtype3= '2' and customgetpoldate>='2018-8-1' "
				// 只有新单才有回执回销日期
				+ " and payyear=0 and renewcount=0 "
				+ " and agentcom is not null and agentcom <>'' "
				+ " and customgetpoldate is not null "
				+ " and (days('"
				+ mMakeDate
				+ "')+ 1 -days(customgetpoldate))>=15 "
				// 此处没有用a.tcommisionsn关联，考虑到sql效率，同时业务逻辑应是业务手续费与管理手续费成对出现，校验一个即可。
				+ " and not exists (select '1' from lacharge a where a.commisionsn=lacommision.commisionsn)"
				+ " union"
				// 续期续保的保单按照财务实收日期计算,需待销售扎帐批处理执行完成后再执行此批处理
				+ " select * from lacommision where managecom = '" + mManageCom
				+ "' and " + " branchtype = '" + mBranchType
				+ "' and branchtype2 = '" + mBranchType2
				+ "' and branchtype3 = '2' "
				+ " and agentcom is not null and agentcom <>'' ";
		if ("01".equals(checkDate.substring(8, 10))) {
			cSQL += " and tmakedate between '" + eSSRS.GetText(1, 1)
					+ "' and '" + eSSRS.GetText(1, 2) + "'";
		} else {
			cSQL += " and tmakedate = '" + mMakeDate + "'";
		}
		cSQL += " and (payyear>0 or renewcount>0)"
				+ " and not exists (select '1' from lacharge a where a.commisionsn=lacommision.commisionsn)";
		System.out.println("AgentChargeCalSaveBL-->dealData:cSQL" + cSQL);
		LACommisionSet tLACommisionSet = new LACommisionSet();
		LACommisionDB tLACommisionDB = new LACommisionDB();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLACommisionSet, cSQL);
		do {
			tRSWrapper.getData();
			if (null == tLACommisionSet || 0 == tLACommisionSet.size()) {
				System.out
						.println("AgentChargeCalSaveBL-->dealData:没有满足要计算手续费的数据！！");
				return true;
			}
			LAChargeSet mLAChargeSet = new LAChargeSet();
			LJAGetSet mLJAGetSet = new LJAGetSet();
			MMap mMap = new MMap();
			Reflections tReflections = new Reflections();
			for (int i = 1; i <= tLACommisionSet.size(); i++) {
				LACommisionSchema tLACommisionSchema = new LACommisionSchema();
				LAChargeSchema businessLAChargeSchema = new LAChargeSchema();
				LAChargeSchema manageLAChargeSchema = new LAChargeSchema();
				tLACommisionSchema = tLACommisionSet.get(i);
				if (!checkCalFlag(tLACommisionSchema)) {
					continue;
				}
				String tRiskCode = "";
				String tAgentCom = "";
				String tContNo = "";
				int payyear = 0;
				int renewcount = 0;
				int payyears = 0;
				int tPayIntv = 0;
				tRiskCode = tLACommisionSchema.getRiskCode();
				tAgentCom = tLACommisionSchema.getAgentCom();
				tContNo = tLACommisionSchema.getContNo();
				payyear = tLACommisionSchema.getPayYear();
				renewcount = tLACommisionSchema.getReNewCount();
				payyears = tLACommisionSchema.getPayYears();
				String tTransState = tLACommisionSchema.getTransState();
				String tWrapCode = tLACommisionSchema.getF3();
				String tTmakedate = tLACommisionSchema.getTMakeDate();
				String tGrpContNo = tLACommisionSchema.getGrpContNo();
				String tGrpPolNo = tLACommisionSchema.getGrpPolNo();
				String tPolNo = tLACommisionSchema.getPolNo();
				tPayIntv = tLACommisionSchema.getPayIntv();
				LAChargeSchema tLAChargeSchema = new LAChargeSchema();
				tLAChargeSchema.setAgentCom(tLACommisionSchema.getAgentCom());
				tLAChargeSchema.setBranchType(tLACommisionSchema
						.getBranchType());
				tLAChargeSchema.setBranchType2(tLACommisionSchema
						.getBranchType2());
				tLAChargeSchema.setCalDate(tLACommisionSchema.getCalDate());
				tLAChargeSchema.setTCommisionSN(tLACommisionSchema
						.getCommisionSN());// 新增字段存储commisionsn
				tLAChargeSchema.setContNo(tLACommisionSchema.getContNo());
				tLAChargeSchema.setGrpContNo(tLACommisionSchema.getGrpContNo());
				tLAChargeSchema.setPolNo(tLACommisionSchema.getPolNo());
				tLAChargeSchema.setGrpPolNo(tLACommisionSchema.getGrpPolNo());
				tLAChargeSchema.setManageCom(tLACommisionSchema.getManageCom());
				tLAChargeSchema.setMainPolNo(tLACommisionSchema.getMainPolNo());
				tLAChargeSchema.setPayCount(tLACommisionSchema.getPayCount());
				tLAChargeSchema.setTransMoney(tLACommisionSchema
						.getTransMoney()); // 实收保费
				tLAChargeSchema.setTransType(tLACommisionSchema.getTransType());
				tLAChargeSchema.setRiskCode(tLACommisionSchema.getRiskCode());
				tLAChargeSchema.setReceiptNo(tLACommisionSchema.getReceiptNo());
				tLAChargeSchema.setWageNo(tLACommisionSchema.getWageNo());
				// 只进行自动计算，不进行结算
				tLAChargeSchema.setChargeState("0");
				tLAChargeSchema.setMakeDate(CurrentDate);
				tLAChargeSchema.setMakeTime(CurrentTime);
				tLAChargeSchema.setModifyDate(CurrentDate);
				tLAChargeSchema.setModifyTime(CurrentTime);
				tLAChargeSchema.setOperator("000");
				tLAChargeSchema.setPayIntv(tLACommisionSchema.getPayIntv());
				tLAChargeSchema.setTMakeDate(tLACommisionSchema.getTMakeDate());

				tReflections.transFields(businessLAChargeSchema,
						tLAChargeSchema);
				tReflections.transFields(manageLAChargeSchema, tLAChargeSchema);
				// 业务手续费的处理
				businessLAChargeSchema.setChargeType("55"); // 互动中介业务手续费类型
				double chargeRate = calculate(tRiskCode, tAgentCom, tContNo,
						tGrpContNo, payyear, renewcount, payyears, tTransState,
						tPayIntv, tWrapCode, tTmakedate, tGrpPolNo, tPolNo,
						"01");
				businessLAChargeSchema.setChargeRate(chargeRate);
				double charge = 0;
				charge = mulrate(chargeRate, tLACommisionSchema.getTransMoney());
				System.out.println("算出的手续费比例为:" + chargeRate);
				System.out.println("算出的手续费为:" + charge);
				businessLAChargeSchema.setCharge(charge);
				//增加可支付佣金80%和20%拆分
				double perRate=0.8;
				double remainRate=0.2;
				businessLAChargeSchema.setPerRate(perRate);
				businessLAChargeSchema.setPerCharge(charge*perRate);
				businessLAChargeSchema.setRemainRate(remainRate);
				businessLAChargeSchema.setRemainCharge(charge*remainRate);
				businessLAChargeSchema.setCommisionSN(tLACommisionSchema
						.getCommisionSN());

				// 管理手续费的处理
				manageLAChargeSchema.setChargeType("56"); // 互动中介管理手续费类型
				double manageChargeRate = calculate(tRiskCode, tAgentCom,
						tContNo, tGrpContNo, payyear, renewcount, payyears,
						tTransState, tPayIntv, tWrapCode, tTmakedate,
						tGrpPolNo, tPolNo, "02");
				manageLAChargeSchema.setChargeRate(manageChargeRate);
				double manageCharge = 0;
				manageCharge = mulrate(manageChargeRate, charge);
				manageLAChargeSchema.setCharge(manageCharge);
				String manageCommisionSN = tLACommisionSchema.getCommisionSN()
						.substring(0, 9) + "1";
				manageLAChargeSchema.setCommisionSN(manageCommisionSN);
				mLAChargeSet.add(businessLAChargeSchema);
				mLAChargeSet.add(manageLAChargeSchema);

				// 生成实付数据
//				LJAGetSchema tLJAGetSchema = new LJAGetSchema();
//				LJAGetSchema manageLJAGetSchema = new LJAGetSchema();
//				LJAGetSchema businessLJAGetSchema = new LJAGetSchema();
//				String tSQL = "select * from lacommision "
//						+ "where receiptno='"
//						+ businessLAChargeSchema.getReceiptNo() + "'"
//						+ " and  commisionsn='"
//						+ businessLAChargeSchema.getTCommisionSN() + "' "
//						+ " fetch first 1 rows only ";
//				LACommisionSet mLACommisionSet = new LACommisionSet();
//				LACommisionDB mLACommisionDB = new LACommisionDB();
//				mLACommisionSet = mLACommisionDB.executeQuery(tSQL);
				// 业务手续费实付
//				businessLJAGetSchema.setActuGetNo(businessLAChargeSchema
//						.getCommisionSN());
//				businessLJAGetSchema.setOtherNo(businessLAChargeSchema
//						.getReceiptNo());
//				businessLJAGetSchema.setOtherNoType("BC");
//				businessLJAGetSchema.setPayMode("12");
//				businessLJAGetSchema.setManageCom(businessLAChargeSchema
//						.getManageCom());
//				businessLJAGetSchema.setAgentCom(mLACommisionSet.get(1)
//						.getAgentCom());
//				businessLJAGetSchema.setGetNoticeNo(businessLAChargeSchema
//						.getCommisionSN());
//				businessLJAGetSchema.setAgentType(mLACommisionSet.get(1)
//						.getAgentType());
//				businessLJAGetSchema.setAgentCode(mLACommisionSet.get(1)
//						.getAgentCode());
//				businessLJAGetSchema.setAgentGroup(mLACommisionSet.get(1)
//						.getAgentGroup());
//				businessLJAGetSchema.setAppntNo(mLACommisionSet.get(1)
//						.getAppntNo());
//				businessLJAGetSchema.setSumGetMoney(businessLAChargeSchema
//						.getCharge());
//				businessLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet
//						.get(1).getAgentCom()));
//				businessLJAGetSchema.setDrawerID(mLACommisionSet.get(1)
//						.getAgentCom());
//				businessLJAGetSchema.setShouldDate(CurrentDate);
//				businessLJAGetSchema.setOperator(businessLAChargeSchema
//						.getOperator());
//				businessLJAGetSchema.setMakeDate(CurrentDate);
//				businessLJAGetSchema.setMakeTime(CurrentTime);
//				businessLJAGetSchema.setModifyDate(CurrentDate);
//				businessLJAGetSchema.setModifyTime(CurrentTime);
//				// 管理手续费实付
//				manageLJAGetSchema.setActuGetNo(manageLAChargeSchema
//						.getCommisionSN());
//				manageLJAGetSchema.setOtherNo(manageLAChargeSchema
//						.getReceiptNo());
//				manageLJAGetSchema.setOtherNoType("MC");
//				manageLJAGetSchema.setPayMode("12");
//				manageLJAGetSchema.setManageCom(manageLAChargeSchema
//						.getManageCom());
//				manageLJAGetSchema.setAgentCom(mLACommisionSet.get(1)
//						.getAgentCom());
//				manageLJAGetSchema.setGetNoticeNo(manageLAChargeSchema
//						.getCommisionSN());
//				manageLJAGetSchema.setAgentType(mLACommisionSet.get(1)
//						.getAgentType());
//				manageLJAGetSchema.setAgentCode(mLACommisionSet.get(1)
//						.getAgentCode());
//				manageLJAGetSchema.setAgentGroup(mLACommisionSet.get(1)
//						.getAgentGroup());
//				manageLJAGetSchema.setAppntNo(mLACommisionSet.get(1)
//						.getAppntNo());
//				manageLJAGetSchema.setSumGetMoney(manageLAChargeSchema
//						.getCharge());
//				manageLJAGetSchema.setDrawer(getAgentComName(mLACommisionSet
//						.get(1).getAgentCom()));
//				manageLJAGetSchema.setDrawerID(mLACommisionSet.get(1)
//						.getAgentCom());
//				manageLJAGetSchema.setShouldDate(CurrentDate);
//				manageLJAGetSchema.setOperator(manageLAChargeSchema
//						.getOperator());
//				manageLJAGetSchema.setMakeDate(CurrentDate);
//				manageLJAGetSchema.setMakeTime(CurrentTime);
//				manageLJAGetSchema.setModifyDate(CurrentDate);
//				manageLJAGetSchema.setModifyTime(CurrentTime);
//				if (businessLJAGetSchema.getSumGetMoney() != 0) {
//					mLJAGetSet.add(businessLJAGetSchema);
//				}
//				if (manageLJAGetSchema.getSumGetMoney() != 0) {
//					mLJAGetSet.add(manageLJAGetSchema);
//				}

			}
			mMap.put(mLAChargeSet, "INSERT");
//			mMap.put(mLJAGetSet, "INSERT");
			try {
				VData tVData = new VData();
				tVData.add(mMap);
				System.out.println("开始提交数据");
				PubSubmit tPubSubmit = new PubSubmit();
				tPubSubmit.submitData(tVData, "");
				System.out.println("数据提交完成");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} while (tLACommisionSet.size() > 0);
		System.out.println("AgentChargeCalSaveBL-->dealData:执行结束 ");
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mBranchType = (String) cInputData.getObject(0);
		mBranchType2 = (String) cInputData.getObject(1);
		mManageCom = (String) cInputData.getObject(2);
		mMakeDate = (String) cInputData.getObject(3);
		if ("".equals(mBranchType) || "".equals(mBranchType2)
				|| "".equals(mManageCom) || "".equals(mMakeDate)) {
			System.out
					.println("手续费自动计算获取要素信息失败branchtype--branchtype2--mManagecom--mMakeDate:"
							+ mBranchType
							+ "--"
							+ mBranchType2
							+ "--"
							+ mManageCom + "--" + mMakeDate);
			CError tError = new CError();
			tError.moduleName = "AgentWageCalSaveUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 判断此数据是否要计算手续费
	 * 
	 * @param cLACommisionSchema
	 *            要要计算的手续费
	 * @return
	 */
	private boolean checkCalFlag(LACommisionSchema cLACommisionSchema) {
		String crs_chnl = "";
		String crs_bussType = "";
		String tBranchType3 = cLACommisionSchema.getBranchType3();
		if ("N".equals(tBranchType3)) {
			return true;
		}
		if (!"00000000000000000000".equals(cLACommisionSchema.getGrpContNo())) {
			LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
			LCGrpContDB tLCGrpContDB = new LCGrpContDB();
			tLCGrpContDB.setGrpContNo(cLACommisionSchema.getGrpContNo());
			if (tLCGrpContDB.getInfo()) {
				tLCGrpContSchema = tLCGrpContDB.getSchema();
				crs_chnl = tLCGrpContSchema.getCrs_SaleChnl();
				crs_bussType = tLCGrpContSchema.getCrs_BussType();
			} else {
				LBGrpContSchema tLBGrpContSchema = new LBGrpContSchema();
				LBGrpContDB tLBGrpContDB = new LBGrpContDB();
				tLBGrpContDB.setGrpContNo(cLACommisionSchema.getGrpContNo());
				if (tLBGrpContDB.getInfo()) {
					tLBGrpContSchema = tLBGrpContDB.getSchema();
					crs_chnl = tLBGrpContSchema.getCrs_SaleChnl();
					crs_bussType = tLBGrpContSchema.getCrs_BussType();
				}
			}
		} else {
			LCContSchema tLCContSchema = new LCContSchema();
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(cLACommisionSchema.getContNo());
			if (tLCContDB.getInfo()) {
				tLCContSchema = tLCContDB.getSchema();
				crs_chnl = tLCContSchema.getCrs_SaleChnl();
				crs_bussType = tLCContSchema.getCrs_BussType();
			} else {
				LBContSchema tLBContSchema = new LBContSchema();
				LBContDB tLBContDB = new LBContDB();
				tLBContDB.setContNo(cLACommisionSchema.getContNo());
				if (tLBContDB.getInfo()) {
					tLBContSchema = tLBContDB.getSchema();
					crs_chnl = tLBContSchema.getCrs_SaleChnl();
					crs_bussType = tLBContSchema.getCrs_BussType();
				}
			}
		}
		if (!"01".equals(crs_chnl) && !"02".equals(crs_chnl)) {
			return false;
		}
		if (!"01".equals(crs_bussType)) {
			return false;
		}
		return true;
	}

	public double calculate(String cRiskCode, String cAgentCom, String cContno,
			String cGrpContNo, int cPayyear, int cRenewcount, int cPayyears,
			String tTransState, int cPayintv, String cWrapCode,
			String cTMakeDate, String cGrpPolNo, String cPolNo, String tFlag) {

		double strWageRate = 0.0;
		// LAWageCalElementSchema tLAWageCalElementSchema=new
		// LAWageCalElementSchema();
		// LAWageCalElementDB tLAWageCalElementDB=new LAWageCalElementDB();
		// tLAWageCalElementDB.setRiskCode(cRiskCode);
		// tLAWageCalElementDB.setCalType("21");
		// tLAWageCalElementDB.setBranchType("3");
		// tLAWageCalElementDB.setBranchType2("01");
		// tLAWageCalElementDB.getInfo();
		// tLAWageCalElementSchema=tLAWageCalElementDB.getSchema();
		String tGrpPolNo = cGrpPolNo;
		if ("00000000000000000000".equals(cGrpContNo)) {
			tGrpPolNo = cPolNo;
		}
		Calculator tCalculator = new Calculator(); // 计算类
		if ("01".equals(tFlag)) {
			SSRS tSSRS = new SSRS();
			String tsql = "select CalCode from LAWageCalElement where  branchtype='5'"
					+ " and branchtype2='01' and CalType='21' "
					+ " and  RiskCode='" + cRiskCode + "' ";
			ExeSQL tExeSQL = new ExeSQL();
			tSSRS = tExeSQL.execSQL(tsql);
			if (tSSRS.getMaxRow() <= 0) {
				tCalculator.setCalCode("AC9901");
			} else {
				tCalculator.setCalCode(tSSRS.GetText(1, 1));
			}
		}
		if ("02".equals(tFlag)) {
			tCalculator.setCalCode("AC9801");
		}
		// tCalculator.setCalCode(tLAWageCalElementSchema.getCalCode());
		// //添加计算编码
		// tCalculator.setCalCode("AP9901"); //添加计算编码

		tCalculator.addBasicFactor("RISKCODE", cRiskCode);
		tCalculator.addBasicFactor("CONTNO", cContno);
		tCalculator.addBasicFactor("BRANCHTYPE", this.mBranchType);
		tCalculator.addBasicFactor("BRANCHTYPE2", this.mBranchType2);
		tCalculator.addBasicFactor("AGENTCOM", cAgentCom);
		tCalculator.addBasicFactor("TRANSSTATE", tTransState);
		// 添加套餐编码
		tCalculator.addBasicFactor("WRAPCODE", cWrapCode);
		// 添加套餐编码判断是否在生效日期中
		tCalculator.addBasicFactor("TMAKEDATE", cTMakeDate);
		tCalculator.addBasicFactor("MANAGECOM", mManageCom);
		tCalculator.addBasicFactor("GRPCONTNO", cGrpContNo);
		tCalculator.addBasicFactor("RENEWCOUNT", String.valueOf(cRenewcount));
		tCalculator.addBasicFactor("PAYYEAR", String.valueOf(cPayyear));
		tCalculator.addBasicFactor("PAYYEARS", String.valueOf(cPayyears));
		tCalculator.addBasicFactor("PAYINTV", String.valueOf(cPayintv));
		tCalculator.addBasicFactor("GRPPOLNO", tGrpPolNo);

		strWageRate = Double.parseDouble(tCalculator.calculate());
		if (tCalculator.mErrors.needDealError()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ActiveAgentChargeCalNewBL";
			tError.functionName = "calWage1";
			tError.errorMessage = tCalculator.mErrors.getFirstError();
			// this.mErrors.addOneError(tError);
			return 0;
		}

		return strWageRate;
	}

	public static double mulrate(double v1, double v2) {

		BigDecimal b1 = new BigDecimal(Double.toString(v1));

		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.multiply(b2).doubleValue();

	}

	private String getAgentComName(String tAgentCom) {
		String comName = "";
		ExeSQL tExeSQL = new ExeSQL();
		String sql = " select name from lacom where agentcom='" + tAgentCom
				+ "' with ur ";
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if(tSSRS.MaxRow>=1){
			comName = tSSRS.GetText(1, 1);
		}else{
			comName=null;
		}
		
		return comName;
	}

	public VData getResult() {
		return mResult;
	}

}
