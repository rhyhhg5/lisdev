package com.sinosoft.lis.agentcalculate;

import java.text.ParseException;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class AgentWageOfLJAGetEndorseGrpCT_Special extends AgentWageOfAbstractClass {

	//用来标记是否是本次查询数据的最后一条
	boolean  tEndDataFlag = false; 
	public double mSumMoney =0; 
	public LJAGetEndorseSchema mCurrentLJAEndorseSchema = new LJAGetEndorseSchema();
	/**
	 * 主要提数逻辑
	 * @param cSQL
	 * @param cOperator
	 * @return
	 */
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfLJAGetEndorseGrpCT_Special-->dealData:开始执行");
		System.out.println("AgentWageOfLJAGetEndorseGrpCT_Special-->dealData:cSQL"+cSQL);
		LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAGetEndorseSet, cSQL);
		boolean tQueryFlag = false;
		do 
		{
			tRSWrapper.getData();
			if(!tQueryFlag)
			{
				if(null == tLJAGetEndorseSet || 0 == tLJAGetEndorseSet.size())
				{
					System.out.println("AgentWageOfLJAGetEndorseGrpDataWT-->dealData没有满足条件 的数据");
					return true;
				}
				tQueryFlag = true;
			}
			if(null != tLJAGetEndorseSet && 0 < tLJAGetEndorseSet.size())
			{
				if(mCurrentLJAEndorseSchema.getActuGetNo() == null)
				{
					mCurrentLJAEndorseSchema = tLJAGetEndorseSet.get(1);
					
				}
			}
			if(0 == tLJAGetEndorseSet.size() && !tEndDataFlag)
			{
				tLJAGetEndorseSet.add(mCurrentLJAEndorseSchema);
				tEndDataFlag = true;
			}
			dealSet(tLJAGetEndorseSet,cOperator);
			
		} while (tLJAGetEndorseSet.size() > 0);
		System.out.println("AgentWageOfLJAGetEndorseGrpCT_Special-->dealData:执行完成");
		return true;
	}
	
	private void dealSet(LJAGetEndorseSet cLJAGetEndorseSet,String cOperator)
	{
		System.out.println("AgentWageOfLJAGetEndorseGrpCT_Special-->dealSet:开始执行");
		LJAGetEndorseSchema tLastLJAGetEndorseSchema = new LJAGetEndorseSchema();
		double tLastSumMoney = 0;
		MMap tMMap = new MMap();
		LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
		LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
		boolean tJudgeDataFlag = false;
		for (int i = 1; i <= cLJAGetEndorseSet.size(); i++) 
		{
			LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
			tLJAGetEndorseSchema = cLJAGetEndorseSet.get(i);
			String tGrpPolNoNew = tLJAGetEndorseSchema.getGrpPolNo();
			String tPayPlanCodeNew  = tLJAGetEndorseSchema.getPayPlanCode();
			double tGetMoneyNew  = tLJAGetEndorseSchema.getGetMoney();
			
			if(!mCurrentLJAEndorseSchema.getGrpPolNo().equals(tGrpPolNoNew)
					||!mCurrentLJAEndorseSchema.getPayPlanCode().equals(tPayPlanCodeNew))
				
			{
				tLastLJAGetEndorseSchema = tLJAGetEndorseSchema;
				tLastSumMoney = mSumMoney;
				tJudgeDataFlag = true;
				mSumMoney = tGetMoneyNew;
				mCurrentLJAEndorseSchema = tLJAGetEndorseSchema;
				
			}
			else
			{   
				if(tEndDataFlag)
				{
					tLastLJAGetEndorseSchema = tLJAGetEndorseSchema;
					tLastSumMoney = mSumMoney;
					tJudgeDataFlag = true;
				}
				else
				{
					mSumMoney += tGetMoneyNew;
				}
			}
	
			if(tJudgeDataFlag)
			{
				LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
				String tAgentCode = tLastLJAGetEndorseSchema.getAgentCode();
	            String tGrpContNo = tLastLJAGetEndorseSchema.getGrpContNo();
	            String tRiskCode = tLastLJAGetEndorseSchema.getRiskCode();
	            String tGrpPolNo = tLastLJAGetEndorseSchema.getGrpPolNo();
	            LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tAgentCode);
	            if(null == tLAAgentSchema)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tLastLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            LCGrpContSchema tLCGrpContSchema = mAgentWageCommonFunction.queryLCGrpCont(tGrpContNo);
	            if(null == tLCGrpContSchema)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tLastLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCGRPCONT, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            
	            LCGrpPolSchema tLCGrpPolSchema = mAgentWageCommonFunction.queryLCGrpPol(tGrpPolNo);
	            if(null == tLCGrpPolSchema)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tLastLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCGRPPOL, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            //团队
	            String tAgentGroup = tLAAgentSchema.getAgentGroup();
	            LABranchGroupSchema tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tAgentGroup);
	            if(null == tLABranchGroupSchema)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema =
	            			dealErrorData(tLastLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            String tBranchCode = tLAAgentSchema.getBranchCode();
	            LABranchGroupSchema t2LABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tBranchCode);
	            String tBranchAttr = t2LABranchGroupSchema.getBranchAttr();
	            if(null == tBranchAttr || "".equals(tBranchAttr))
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tLastLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_BRANCHATTR, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            
	            //添加对互动的处理
	            String tSaleChnl = tLCGrpPolSchema.getSaleChnl();
	            String tBranchType3 = "";
	            if("14".equals(tSaleChnl)||"15".equals(tSaleChnl))
	            {
	            	String tCrs_SaleChnl = tLCGrpContSchema.getCrs_SaleChnl();
	            	String tCrs_BussType = tLCGrpContSchema.getCrs_BussType();
	            	tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
	            }
	            if("no".equals(tBranchType3))
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tLastLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_ACTIVE, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            String tBranchTypes[] = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
	            if(null == tBranchTypes || 0 == tBranchTypes.length)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tLastLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_SALECHNL, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            String tBranchType = tBranchTypes[0];
	            String tBranchType2 = tBranchTypes[1];
	            
	            //获取保单续保次数
	            int PayYear = 0;
	            int tRenewCount = mAgentWageCommonFunction.queryLCRnewPol(tGrpPolNo, "grppolno");
	            //封装数据从lcgrpcont,lcgrppol中取
	            tLACommisionNewSchema = mAgentWageCommonFunction.packageGrpContDataOfLACommisionNew(tLCGrpContSchema, tLCGrpPolSchema);
	            String tCommisionSn = mAgentWageCommonFunction.getCommisionSN();
	            
	            tLACommisionNewSchema.setCommisionSN(tCommisionSn);
	            tLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
	            tLACommisionNewSchema.setBranchType(tBranchType);
	            tLACommisionNewSchema.setBranchType2(tBranchType2);
	            tLACommisionNewSchema.setBranchType3(tBranchType3);
	            tLACommisionNewSchema.setReNewCount(tRenewCount);
	            
	            tLACommisionNewSchema.setManageCom(tLastLJAGetEndorseSchema.getManageCom());
	            tLACommisionNewSchema.setRiskCode(tLastLJAGetEndorseSchema.getRiskCode());
	            tLACommisionNewSchema.setEndorsementNo(tLastLJAGetEndorseSchema.getEndorsementNo());
	            tLACommisionNewSchema.setDutyCode(tLastLJAGetEndorseSchema.getDutyCode());
	            tLACommisionNewSchema.setPayPlanCode(tLastLJAGetEndorseSchema.getPayPlanCode());
	            tLACommisionNewSchema.setReceiptNo(tLastLJAGetEndorseSchema.getActuGetNo());
	            
	            tLACommisionNewSchema.setTPayDate(tLastLJAGetEndorseSchema.getGetDate());
	            tLACommisionNewSchema.setTEnterAccDate(tLastLJAGetEndorseSchema.getEnterAccDate());
	            tLACommisionNewSchema.setTConfDate(tLastLJAGetEndorseSchema.getGetConfirmDate());
	            tLACommisionNewSchema.setTMakeDate(tLastLJAGetEndorseSchema.getMakeDate());
	            tLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
	            
	            tLACommisionNewSchema.setTransMoney(0-java.lang.Math.abs(tLastSumMoney));
	            tLACommisionNewSchema.setTransState("00");//正常保单
	            tLACommisionNewSchema.setTransStandMoney(tLACommisionNewSchema.getTransMoney());
	            tLACommisionNewSchema.setCurPayToDate(tLastLJAGetEndorseSchema.getGetDate());
	            tLACommisionNewSchema.setTransType("CT");
	            tLACommisionNewSchema.setCommDire("1");
	            if("07".equals(tSaleChnl))
	            {
	            	tLACommisionNewSchema.setF1("02");//职团开拓
	            	tLACommisionNewSchema.setP5(tLACommisionNewSchema.getTransMoney());//职团开拓实收保费
	            }
	            else
	            {
	            	tLACommisionNewSchema.setF1("01");
	            }
	            
	            tLACommisionNewSchema.setFlag("0");//犹豫期撤件
	            tLACommisionNewSchema.setPayYear(PayYear);
	            tLACommisionNewSchema.setPayYears(mAgentWageCommonFunction.getPayYears(tGrpPolNo));
	            tLACommisionNewSchema.setPayCount(1);
	            tLACommisionNewSchema.setAgentCom(tLastLJAGetEndorseSchema.getAgentCom());
	            tLACommisionNewSchema.setAgentType(tLastLJAGetEndorseSchema.getAgentType());
	            tLACommisionNewSchema.setAgentCode(tLastLJAGetEndorseSchema.getAgentCode());
	            
	            String tScanDate = mAgentWageCommonFunction.getScanDate(tLCGrpPolSchema.getPrtNo());
	            tLACommisionNewSchema.setScanDate(tScanDate);
	            tLACommisionNewSchema.setOperator(cOperator);
	            tLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
	            tLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
	            tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
	            tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
	           
	            tLACommisionNewSchema.setBranchCode(tBranchCode);
	            tLACommisionNewSchema.setBranchSeries(t2LABranchGroupSchema.getBranchSeries());
	            tLACommisionNewSchema.setAgentGroup(tAgentGroup);
	            tLACommisionNewSchema.setBranchAttr(tBranchAttr);
	           
	            //套餐赋值
	    	    String  tWrapCode = "";
	    	    if("2".equals(tLCGrpContSchema.getCardFlag()))
	    	    {
	    	    	tWrapCode = mAgentWageCommonFunction.getWrapCode(tLastLJAGetEndorseSchema.getGrpContNo(), tLastLJAGetEndorseSchema.getRiskCode());
	    	    }
	    	    tLACommisionNewSchema.setF3(tWrapCode);
	        	//薪资月算法相关字段处理
	        	//计算薪资月的时候，传的paycount是0，但实际上是1，此处是为了能调用公共的薪资月方法(原来逻辑中是用一个变量，传的是0)
	        	tLACommisionNewSchema.setPayCount(0);
	        	String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLCGrpPolSchema.getGrpContNo(),"CT",tLACommisionNewSchema.getTMakeDate(),
	        			tLastLJAGetEndorseSchema.getGetConfirmDate(),tLastLJAGetEndorseSchema.getActuGetNo());
	        	//用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
	        	String tAFlag =tConfDates[0];
	        	//财务确认日期 ，团单会用到此字段
	        	String tConfDate = tConfDates[1];
	        	String tVisitTime  = mAgentWageCommonFunction.queryReturnVisit(tGrpContNo);
	        	String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
	        	String tFlag = "02";
	        	//根据薪资月函数 得到薪资月
	        	String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, tFlag, tAFlag, tVisitTime, tRiskFlag);
	        	tLACommisionNewSchema.setCalDate(tCalDate);
	        	if(!"".equals(tCalDate)&&null!=tCalDate)
	        	{
	        		tLACommisionNewSchema.setCalcDate(tCalDate);
	        		String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate).substring(0,6);
	        		tLACommisionNewSchema.setWageNo(tWageNo);
	        		tLACommisionNewSchema.setTConfDate(tConfDate);
	        	}
	        	tLACommisionNewSchema.setPayCount(1);
	            tLACommisionNewSet.add(tLACommisionNewSchema);
			}
		}
		if(0<tLACommisionNewSet.size())
		{
			tMMap.put(tLACommisionNewSet, "INSERT");
		}
		if(0<tLACommisionErrorSet.size())
		{
			tMMap.put(tLACommisionErrorSet, "INSERT");
		}
		if(0==tMMap.size())
		{
			return;
		}
		if(!mAgentWageCommonFunction.dealDataToTable(tMMap))
		{
			// @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageOfLJAGetEndorseGrpCT_Special";
            tError.functionName = "PubSubmit";
            tError.errorMessage = "数据插入到LACommisionNew中失败!";
            this.mErrors.addOneError(tError);
            return ;
		}
		System.out.println("AgentWageOfLJAGetEndorseGrpCT_Special-->dealSet:执行完成");
	}
	public static void main(String[] args) throws ParseException {
		AgentWageInitialize tAgentWageInitialize = new AgentWageInitialize();
		AgentWageOfLJAGetEndorseGrpCT_Special tAgentWageOfLJAGetEndorseGrpDataWT = new AgentWageOfLJAGetEndorseGrpCT_Special();
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator="000";
		tGlobalInput.ManageCom="86";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("TMakeDate", "2017-5-2");
		tTransferData.setNameAndValue("ManageCom", "86610400");
		tTransferData.setNameAndValue("BranchType", "3");
		tTransferData.setNameAndValue("BranchType2", "01");
		tTransferData.setNameAndValue("ContNo", "019844849000003");
		VData tVData = new VData();
		tVData.add(tGlobalInput);
		tVData.add(tTransferData);
	}
	
}
	
	