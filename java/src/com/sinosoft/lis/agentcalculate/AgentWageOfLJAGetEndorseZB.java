package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

public class AgentWageOfLJAGetEndorseZB extends AgentWageOfAbstractClass 
{
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfLJAGetEndorseZB-->dealData:开始执行");
		System.out.println("AgentWageOfLJAGetEndorseZB-->dealData:cSQL："+cSQL);
		LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAGetEndorseSet, cSQL);
		//用来判断数据是不是为空，为了防止 最后一次循环时，为空时，也打印错误语句  
		boolean tJudgeNullFlag = false;
		do {
			MMap tMMap = new MMap();
			tRSWrapper.getData();
			if(!tJudgeNullFlag)
			{
				if(null == tLJAGetEndorseSet || 0==tLJAGetEndorseSet.size())
				{
					System.out.println("AgentWageOfLJAGetEndorseZB---->dealData没有满足条件的数据！");
					return true;
				}
				tJudgeNullFlag = true;
			}
			LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
			LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
			for (int i = 1; i <= tLJAGetEndorseSet.size(); i++) 
			{
			   LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
	           tLJAGetEndorseSchema = tLJAGetEndorseSet.get(i);
	           LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
	           String tContNo = tLJAGetEndorseSchema.getContNo();
	           String tAgentCode = tLJAGetEndorseSchema.getAgentCode();
	           String tPolNo = tLJAGetEndorseSchema.getPolNo();
	           LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tAgentCode);
	           if(null == tLAAgentSchema)
	           {
	        	   LACommisionErrorSchema tLACommisionErrorSchema = 
	        			   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE, cOperator);
	        	   tLACommisionErrorSet.add(tLACommisionErrorSchema);
	        	   continue;
	           }
			   LCContSchema tLCContSchema = mAgentWageCommonFunction.queryLCCont(tContNo);
			   if(null == tLCContSchema)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema =
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCCONT, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   LCPolSchema tLCPolSchema = mAgentWageCommonFunction.queryLCPol(tPolNo);
			   if(null == tLCPolSchema)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCPOL, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   String tAgentGroup = tLAAgentSchema.getAgentGroup();
			   LABranchGroupSchema tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tAgentGroup);
			   if(null == tLABranchGroupSchema)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   String tSaleChnl = tLCPolSchema.getSaleChnl();
			   String tAgentCom = tLJAGetEndorseSchema.getAgentCom();
			   //契约渠道为中介
			   if("03".equals(tSaleChnl))
			   {
				   LAComSchema tLAComSchema = mAgentWageCommonFunction.querytLACom(tAgentCom);
				   if(null == tLAComSchema)
				   {
					   LACommisionErrorSchema tLACommisionErrorSchema = 
							   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCOM, cOperator);
					   tLACommisionErrorSet.add(tLACommisionErrorSchema);
					   continue;
				   }
			   }
			 //对互动的处理
			   String tBranchType3="";
			   if("15".equals(tSaleChnl)||"14".equals(tSaleChnl))
			   {
				   String tCrs_SaleChnl = tLCContSchema.getCrs_SaleChnl();
				   String tCrs_BussType = tLCContSchema.getCrs_BussType();
				   tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
			   }
			   if("no".equals(tBranchType3))
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_ACTIVE, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   String[] tBranchTypes = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
			   if(null == tBranchTypes || 0==tBranchTypes.length)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
							dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_SALECHNL, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			
			   String tRiskCode = tLCPolSchema.getRiskCode();
			   String tBranchCode = tLAAgentSchema.getBranchCode();
			   LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
			   tLABranchGroupDB.setAgentGroup(tBranchCode);
			   tLABranchGroupDB.getInfo();
			   String tBranchAttr = tLABranchGroupDB.getBranchAttr();
			   //对团队编码
			   if(null == tBranchAttr || "".equals(tBranchAttr))
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_BRANCHATTR, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			    //续保的处理
			   int tRenewCount = mAgentWageCommonFunction.queryLCRnewPol(tPolNo, "polno");
			   
			   //riskcode,agentcom 重新赋值
			   tLACommisionNewSchema = mAgentWageCommonFunction.packageContDataOfLACommisionNew(tLCContSchema, tLCPolSchema);
			   int PayYear =0; 
			   //获取最大的Commisionsn
			   String tCommisionsn = mAgentWageCommonFunction.getCommisionSN();
			   tLACommisionNewSchema.setCommisionSN(tCommisionsn);
			   tLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
			   tLACommisionNewSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
			   tLACommisionNewSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
			   tLACommisionNewSchema.setBranchType(tBranchTypes[0]);
			   tLACommisionNewSchema.setBranchType2(tBranchTypes[1]);
			   tLACommisionNewSchema.setBranchType3(tBranchType3);
			   tLACommisionNewSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo());
			   tLACommisionNewSchema.setReceiptNo(tLJAGetEndorseSchema.getActuGetNo());
			   tLACommisionNewSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
			   tLACommisionNewSchema.setTEnterAccDate(tLJAGetEndorseSchema.getEnterAccDate());
			   //tLACommisionNewSchema.setTConfDate(tLJAGetEndorseSchema.getGetConfirmDate());
			   tLACommisionNewSchema.setTMakeDate(tLJAGetEndorseSchema.getMakeDate());
			   //caldate
			   tLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
			   tLACommisionNewSchema.setTransMoney(tLJAGetEndorseSchema.getGetMoney());
			   
			   tLACommisionNewSchema.setTransState("03"); //03-代表追加保费
			   tLACommisionNewSchema.setTransStandMoney(tLACommisionNewSchema.getTransMoney());
			   tLACommisionNewSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
			   tLACommisionNewSchema.setCurPayToDate(tLJAGetEndorseSchema.getGetDate());
			   tLACommisionNewSchema.setTransType("ZC");
			   if(tLCContSchema.getSaleChnl().equals("07"))
		       {
		           tLACommisionNewSchema.setF1("02");
		           tLACommisionNewSchema.setP5( tLACommisionNewSchema.getTransMoney());//职团开拓实收保费
		       }
		       else
		       {
		    	   tLACommisionNewSchema.setF1("01");
		       }
			   tLACommisionNewSchema.setFlag("1");//犹豫期撤件
			   tLACommisionNewSchema.setPayYear(PayYear);
			   tLACommisionNewSchema.setReNewCount(tRenewCount);
			   tLACommisionNewSchema.setAgentType(tLJAGetEndorseSchema.getAgentType());
			   tLACommisionNewSchema.setAgentCode(tLJAGetEndorseSchema.getAgentCode());
			   tLACommisionNewSchema.setBranchCode(tLAAgentSchema.getBranchCode());
			   tLACommisionNewSchema.setBranchSeries(tLABranchGroupDB.getBranchSeries());
			   tLACommisionNewSchema.setAgentGroup(tLAAgentSchema.getAgentGroup());
			   tLACommisionNewSchema.setBranchAttr(tBranchAttr);
			   
			   
			   tLACommisionNewSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
			   tLACommisionNewSchema.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
			   tLACommisionNewSchema.setManageCom(tLJAGetEndorseSchema.getManageCom());
			   tLACommisionNewSchema.setCommDire("1");
			   tLACommisionNewSchema.setOperator(cOperator);
			   tLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
		       tLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
		       tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
		       tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
			   
		       //caigang 2004-09-22添加，用于提取扫描日期
		       String tScanDate = mAgentWageCommonFunction.getScanDate(tLCPolSchema.getPrtNo()); 
		       tLACommisionNewSchema.setScanDate(tScanDate);
		       
		       //薪资月计算
		       String tVisitTime = mAgentWageCommonFunction.queryReturnVisit(tContNo);
		       String tVisitFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
		       
		       String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLCPolSchema.getGrpContNo(),"WT",
		    		   tLACommisionNewSchema.getTMakeDate(),tLJAGetEndorseSchema.getGetConfirmDate(),tLJAGetEndorseSchema.getActuGetNo());
		       //用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
		       String tAFlag = tConfDates[0];
		       //财务确认日期,团单会用到此字段
		       String tConfDate = tConfDates[1];
		       //paycount 赋为0，是为了用公共的薪资月函数
		       tLACommisionNewSchema.setPayCount(0);
		       //获取薪资月
		       String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, "02", tAFlag, tVisitTime, tVisitFlag);
		       if(!"".equals(tCalDate) && null != tCalDate)
		       {
		    	   String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate);
		    	   tLACommisionNewSchema.setWageNo(tWageNo);
		    	   tLACommisionNewSchema.setTConfDate(tConfDate);
		    	   tLACommisionNewSchema.setCalDate(tCalDate);
		       }
		       tLACommisionNewSchema.setPayCount(1);
		       tLACommisionNewSet.add(tLACommisionNewSchema);
			}
			if(0<tLACommisionNewSet.size())
			{
				tMMap.put(tLACommisionNewSet, "INSERT");
			}
			if(0<tLACommisionErrorSet.size())
			{
				tMMap.put(tLACommisionErrorSet, "INSERT");
			}
			if(0==tMMap.size())
			{
				continue;
			}
			if (!mAgentWageCommonFunction.dealDataToTable(tMMap)) 
			{
	        	// @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "AgentWageOfLJAGetEndorseZB";
	            tError.functionName = "PubSubmit";
	            tError.errorMessage = "数据插入到LACommisionNew中失败!";
	            this.mErrors.addOneError(tError);
	            return false;
			}
		} while (tLJAGetEndorseSet.size()>0);
		System.out.println("AgentWageOfLJAGetEndorseZB-->dealData:执行结束");
		return true;
		
	}
}
