package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p> 
 *
 * @author caigang
 * @version 1.0
 */
public class DealDataOfWageNoIsNull {		
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private String mOperate;
    private String  AFlag = "0";//0表示收费的情况,1表示付费情况
    private String mSaleChnl[] = {
                                 "", "", ""};
    private LACommisionSchema mLACommisionSchema = new LACommisionSchema();
    private LACommisionSet mLAUpdateCommisionSet = new LACommisionSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    public DealDataOfWageNoIsNull() {
        try {
        } catch (Exception ex) {
            ex.printStackTrace();
        }	
    }

    //处理数据
    public boolean dealData(String cBranchtype,String cBranchtype2,String cManageCom) {
    	
    	this.mSaleChnl = AgentPubFun.switchBranchTypetoSalChnl(cBranchtype,cBranchtype2);
    	
        //处理wageno为空的数据
        System.out.println("*****************置扎帐表中回单日期为空的（已回单的） 开始***************");
        LCGrpContSchema tLCGrpContSchema;
        LCContSchema tLCContSchema;
        String tContNo = "";
        String tGrpContNo = "";
        String getPolDate = "";
        String tWageNo = "";
        String custPolDate = "";
        String tTransType = "";
        String tMakeDate = "";//---songgh因为一个保单可能会做多个保全项目,所以在进行查找的时候按照提数当天发生的保全项目进行搜索
        //犹豫期撤保的不查询
        String sql = "select CommisionSN from lacommision where managecom like '" +cManageCom + "%' and caldate is null and commdire='1'  ";
        sql += " and BranchType='" + cBranchtype + "'";
        sql += " and BranchType2='" + cBranchtype2 + "' ";
        //测试用
//        sql += " and commisionsn in ('0095092570','0095092580','0095092590')";
        sql += " order by CommisionSN with ur ";
        System.out.println("查询扎帐表中回单日期为空即薪资年月为空的保单："+sql);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionDB.setCommisionSN(tSSRS.GetText(i, 1));
            tLACommisionDB.getInfo();
            tLACommisionSchema = tLACommisionDB.getSchema();
            String tBranchType = tLACommisionSchema.getBranchType();
            String tBranchType2 = tLACommisionSchema.getBranchType2();
            tContNo = tLACommisionSchema.getContNo();
            tGrpContNo = tLACommisionSchema.getGrpContNo();
            tTransType = tLACommisionSchema.getTransType();
            tMakeDate = tLACommisionSchema.getTMakeDate();
            String tGrpPolNo = tLACommisionSchema.getGrpPolNo();
            String tManageCom =tLACommisionSchema.getManageCom();
            //如果是团单，查询团单信息
            if (!tGrpPolNo.equals("00000000000000000000")) {
                tLCGrpContSchema = new LCGrpContSchema();
                //查询团险的保单信息
                tLCGrpContSchema = this.queryLCGrpCont(tGrpContNo);
                if (tLCGrpContSchema == null) {
                    continue;
                }
                getPolDate = tLCGrpContSchema.getGetPolDate();
                custPolDate = tLCGrpContSchema.getCustomGetPolDate();
              //团险保全项目根据财务的确认日期计算佣金月---modifydate:20080329-songgh
                String tConfDate = "";
                tConfDate = this.queryConfDate(tGrpContNo,tTransType,tMakeDate,
                		                         tLACommisionSchema.getTConfDate(),tLACommisionSchema.getReceiptNo());
                //if (getPolDate != null && !getPolDate.equals("")) {
                    tLACommisionSchema.setCustomGetPolDate(custPolDate);
                    tLACommisionSchema.setGetPolDate(getPolDate);
//                    chargeDate = queryChargeDate(mLACommisionSchema.getGrpPolNo(),
//                                                 mLACommisionSchema.getAgentCom(),
//                                                 mLACommisionSchema.
//                                                 getReceiptNo());

                    //=== 增加访后付费 日期校验--针对山东分公司需求
                    String tRiskCode = tLACommisionSchema.getRiskCode(); // 查询出险种信息
                    String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                    String tTransState = tLACommisionSchema.getTransState();
                    String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
                    String tVisitFlag = this.judgeVisitFlag(tLACommisionSchema.getReceiptNo(), tLACommisionSchema.getGrpContNo(),"0");
                    if("Y".equals(tVisitFlag))
                    {
                    	continue;
                    }
                    // add new 
                    String tAgentcode =tLACommisionSchema.getAgentCode() ;

                    String caldate = this.calCalDateForAll(tBranchType,
                            tBranchType2,
                            tLACommisionSchema.getSignDate(),
                            getPolDate,
                            String.valueOf(tLACommisionSchema.getPayCount()),
                            tLACommisionSchema.getTMakeDate(), custPolDate,
                            tLACommisionSchema.getCValiDate(),"02",
                            tConfDate,AFlag,tManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,tGrpPolNo,tVisitFlag,tRiskCode);
                    if (caldate != null && !caldate.equals("")) {
                        sql =
                                "select yearmonth from LAStatSegment where startdate<='" +
                                caldate + "' and enddate>='" + caldate +
                                "' and stattype='91' "; //团险薪资月为91,2005-11-23修改
                        tExeSQL = new ExeSQL();
                        tWageNo = tExeSQL.getOneValue(sql);
                        tLACommisionSchema.setWageNo(tWageNo);
                        tLACommisionSchema.setCalDate(caldate);
                        tLACommisionSchema.setTConfDate(tConfDate);//财务确认日期
                        tLACommisionSchema.setModifyDate(this.currentDate);
                        tLACommisionSchema.setModifyTime(this.currentTime);
                    }
                    this.mLAUpdateCommisionSet.add(tLACommisionSchema);


            } // 团单结束
            else { //如果是个单
                tLCContSchema = new LCContSchema();
                tLCContSchema = this.queryLCCont(tContNo);
                if (tLCContSchema == null) {
                   System.out.println("渠道检验出错!");
                	continue;
                }
                System.out.println("after get LAContSchema!");
                getPolDate = tLCContSchema.getGetPolDate();
                custPolDate = tLCContSchema.getCustomGetPolDate();
                if (getPolDate != null && !getPolDate.equals("")) {
                    tLACommisionSchema.setCustomGetPolDate(custPolDate);
                    tLACommisionSchema.setGetPolDate(getPolDate);
                    String tFlag="01";
                    if(tLACommisionSchema.getReNewCount()>0)
                    {
                        tFlag="02";
                    }
                    //简易险
                    if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
                    {
                        tFlag="02";
                    }
                    String tTMakeDate=tLACommisionSchema.getTMakeDate();
                    //犹豫期退保需要修改tTMakeDate
                    if(tLACommisionSchema.getTransType().equals("WT"))
                    {
                        String tSQL1 =
                                "select tMakeDate from LACommision where transtype='ZC' and PolNo = '" +
                                tLACommisionSchema.getPolNo() +
                                "' and SignDate='" +
                                tLACommisionSchema.getSignDate() +
                                "' order by commisionsn with ur ";
                        tExeSQL = new ExeSQL();
                        tTMakeDate=tExeSQL.getOneValue(tSQL1);

                    }
                    String tConfDate = "";
                    tConfDate = this.queryConfDate(tGrpContNo,tTransType,tTMakeDate,tLACommisionSchema.getTConfDate(),tLACommisionSchema.getReceiptNo());
                    //=== 增加访后付费 日期校验--针对山东分公司需求
                    String tRiskCode = tLACommisionSchema.getRiskCode(); // 查询出险种信息
                    String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                    String tTransState = tLACommisionSchema.getTransState();
                    String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
                    String tVisitFlag = this.judgeVisitFlag(tLACommisionSchema.getReceiptNo(), tLACommisionSchema.getContNo(),"1");
                    if("Y".equals(tVisitFlag))
                    {
                    	continue;
                    }
                    // add new 
                    String tAgentcode =tLACommisionSchema.getAgentCode() ;
                    String caldate = this.calCalDateForAll(tBranchType,
                           tBranchType2,
                           tLACommisionSchema.getSignDate(),
                           getPolDate,
                           String.valueOf(tLACommisionSchema.
                                          getPayCount()),
                           tTMakeDate, custPolDate,
                           tLACommisionSchema.getCValiDate(),tFlag,
                            tConfDate, AFlag,tManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,tLCContSchema.getGrpContNo(),tVisitFlag,tRiskCode);
                   if (!caldate.equals("") && !caldate.equals("0")) {

                        sql =
                                "select yearmonth from LAStatSegment where startdate<='" +
                                caldate + "' and enddate>='" + caldate +
                                "' and stattype='5' ";
                        tExeSQL = new ExeSQL();
                        tWageNo = tExeSQL.getOneValue(sql);
                        tLACommisionSchema.setWageNo(tWageNo);
                        tLACommisionSchema.setCalDate(caldate);
                        tLACommisionSchema.setTConfDate(tConfDate);
                        tLACommisionSchema.setModifyDate(this.currentDate);
                        tLACommisionSchema.setModifyTime(this.currentTime);
                    }
                    this.mLAUpdateCommisionSet.add(tLACommisionSchema);
                }

            }
        }
          
        mMap.put(this.mLAUpdateCommisionSet , "UPDATE");

        this.mInputData.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AgentWageCalSaveNewBL Submit...");
        try{
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
        	// @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        }catch(Exception ex)
        {
        	ex.printStackTrace();
        }
        return true;
    }
    /**
     * caigang
     * 查询集体保单表
     * 输出：如果查询时发生错误则返回null,否则返回Schema
     */
    private LCGrpContSchema queryLCGrpCont(String tGrpContNo) {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tGrpContNo);
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        if (!tLCGrpContDB.getInfo()) {
            LBGrpContDB tLBGrpContDB = new LBGrpContDB();
            tLBGrpContDB.setGrpContNo(tGrpContNo);
            if (!tLBGrpContDB.getInfo()) {
                if (tLBGrpContDB.mErrors.needDealError() == true) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLBGrpContDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveNewBL";
                    tError.functionName = "queryLCGrpCont";
                    tError.errorMessage = "集体保单备份表查询失败!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                return null;
            }
            Reflections tReflections = new Reflections();
            LBGrpContSchema tLBGrpContSchema = new LBGrpContSchema();
            tLBGrpContSchema = tLBGrpContDB.getSchema();
            tReflections.transFields(tLCGrpContSchema, tLBGrpContSchema);
        } else {
            tLCGrpContSchema = tLCGrpContDB.getSchema();
        }
        String tSalChnl = tLCGrpContSchema.getSaleChnl().trim();
        String tSalChnlDetail = tLCGrpContSchema.getSaleChnlDetail();
        System.out.println("SaleChnl:" + tLCGrpContSchema.getSaleChnl());
        System.out.println("SaleChnlDetail:" +tLCGrpContSchema.getSaleChnlDetail());
        if (tSalChnl == null || tSalChnl.equals("")) {
            return null;
        }
        if (tSalChnlDetail == null || tSalChnlDetail.equals("")) {
            tSalChnlDetail = "01"; //2005-11-7   MODIFY:XIANGCHUN
        }
        //2014-12-26 modify yangyang
      //2014-12-26 modify yangyang
        String Crs_SaleChnl = tLCGrpContSchema.getCrs_SaleChnl();
        String Crs_BussType = tLCGrpContSchema.getCrs_BussType();
        if(tSalChnl.equals("14")&&Crs_SaleChnl!=null&&!Crs_SaleChnl.equals(""))
        {
        	return null;
        }
        if(tSalChnl.equals("15"))
        {
        	if("".equals(Crs_SaleChnl)||null==Crs_SaleChnl)
        	{
        		return tLCGrpContSchema;
        	}
        	else if(this.mSaleChnl[2].indexOf(Crs_BussType) != -1)
        	{
        		return tLCGrpContSchema;
        	}
        	else
        	{
        		return null;
        	}
        }
    	if (this.mSaleChnl[0].indexOf(tSalChnl) != -1 &&
                this.mSaleChnl[1].indexOf(tSalChnlDetail) != -1) {
                return tLCGrpContSchema;
        }
        else {
            return null;
        }
        
    }
    
    /**
     * songguohui
     * 查询收费的财务日期
     * 如果是保全收费则取实收表的财务确认日期，如果是保全付费则是付费表的makedate
     * 定期结算的时间是按照最终结算时的财务日期
     * 输出：如果查询时发生错误则返回null,否则返回tconfdate
     */
    private String  queryConfDate(String tGrpContNo,String tTransType,String tMakeDate,String tConfDate,String tPayNo) {
        String mconfdate = tConfDate;
        String flag  = "";
        AFlag = "1";//表示付费
        ExeSQL tExeSQL = new ExeSQL();
        if(tGrpContNo!=null&&!tGrpContNo.equals("00000000000000000000")
        		&&!tGrpContNo.equals(""))//个单不做处理
        {
	        if(tTransType.equals("ZC")||tTransType.equals("ZT"))
	        {
	        	//首先判断是不是保全付费的情况
	        	String getSQL="select distinct paymode from ljaget where actugetno in ("
	        		+"select actugetno from ljagetendorse where grpcontno='"+tGrpContNo
	        		+"' and actugetno='"+tPayNo+"' and endorsementno is not null) with ur";
	        	String tPayMode = tExeSQL.getOneValue(getSQL);
           		if(tPayMode!=null&&!tPayMode.equals("")){
           			if("B".equals(tPayMode))//定期结算中的白条
            		{
            			 mconfdate = "";
            			 AFlag = "0";
            		}
            		else if("J".equals(tPayMode))//已经定期结算
            		{
            			String ttSQL="select actuno,makedate from LJAEdorBalDetail where btactuno='" +tPayNo+"' with ur";
            			SSRS tSSRS=new SSRS();
            			tSSRS=tExeSQL.execSQL(ttSQL);
            			if(tSSRS.getMaxRow()>0){
                    			mconfdate=tSSRS.GetText(1,2);
                				System.out.println("财务确认日期"+mconfdate);
            			}else{
            					mconfdate="";
            			}
//            			String SQL = "select confdate,MAX(INComeno) from ljapay where getnoticeno in " +
//    	        		" (select distinct actuno from ljaedorbaldetail where btactuno in " +
//    	        		" (select distinct actugetno from ljagetendorse where grpcontno in" +
//    	        		" (select grpcontno from LCGrpBalPlan where GrpContno = '"+tGrpContNo+"'))) " +
//    	        		" GROUP BY ConfDate,INComeno " +
//    	        		" order by confdate desc with ur ";
//    			          mconfdate = tExeSQL.getOneValue(SQL);
    			          AFlag = "0";
            		}
           		}else{
//           		首先判断是契约收费还是保全收费
                	String endorseSQL = "select distinct endorsementno from ljapaygrp where grpcontno='"+tGrpContNo+"'" +
                			" and makedate='"+tMakeDate+"' and payno = '"+tPayNo+"' with ur";
                	String endorsementno = tExeSQL.getOneValue(endorseSQL);
                	if(endorsementno!=null&&!endorsementno.equals(""))//为空的时候表示是契约收费,不为空表示是保全的工单号
                	{
                		String incometypeSQL = "select distinct incometype from ljapay where incomeno='"+endorsementno+"'" +
                				" with ur ";
                		String   tIncometype = tExeSQL.getOneValue(incometypeSQL);
                		if(tIncometype!=null&&!tIncometype.equals("")){
                			if("B".equals(tIncometype))//定期结算中的白条
                    		{
                    			 mconfdate = "";
                    			 AFlag = "0";
                    		}
                    		else if("J".equals(tIncometype))//已经定期结算
                    		{
                    			String tSQL="select incometype,confdate from ljapay where getnoticeno in (" +
                    					"select actuno from LJAEdorBalDetail where btactuno='" +tPayNo+"') with ur";
                    			SSRS tSSRS=new SSRS();
                    			tSSRS=tExeSQL.execSQL(tSQL);
                    			if(tSSRS.getMaxRow()>0){
                    				if(tSSRS.GetText(1,1).equals("13")){
                    					mconfdate=tSSRS.GetText(1,2);
                        				System.out.println("财务确认日期"+mconfdate);
                    				}else{
                    					mconfdate="";
                    				}

                    			}else{
                    				tSQL="select othernotype,confdate from ljaget where actugetno in (" +
                					"select actuno from LJAEdorBalDetail where btactuno='" +tPayNo+"') with ur";
                    				tSSRS=tExeSQL.execSQL(tSQL);
                    				if(tSSRS.getMaxRow()>0&&tSSRS.GetText(1,1).equals("13")
                    						&&tSSRS.GetText(1,2)!=null
                    						&&!tSSRS.GetText(1,2).equals("")){
                        				mconfdate=tSSRS.GetText(1,2);
                        				System.out.println("财务确认日期"+mconfdate);
                        			}else{
                        				mconfdate = "";
                        			}

                    			}
//                    			String SQL = "select confdate,MAX(INComeno) from ljapay where getnoticeno in " +
//            	        		" (select distinct actuno from ljaedorbaldetail where btactuno in " +
//            	        		" (select distinct actugetno from ljagetendorse where grpcontno in" +
//            	        		" (select grpcontno from LCGrpBalPlan where GrpContno = '"+tGrpContNo+"'))) " +
//            	        		" GROUP BY ConfDate,INComeno " +
//            	        		" order by confdate desc with ur ";
//            			          mconfdate = tExeSQL.getOneValue(SQL);
            			          AFlag = "0";

                    		}
                		}
                   	 }
           		}
	        }
	        else
	        {
	        	   String AddSQL =  "select max(GETCONFIRMDATE) from ljagetendorse where  " +
	           		" FeeFinaType in (select code from ldcode where codetype='feefinatype2')" +
	           		" and grpcontno = '"+tGrpContNo+"' and feeoperationtype='"+tTransType+"' and  makedate='"+tMakeDate+"' with ur";
	        	   String maxConfdate = tExeSQL.getOneValue(AddSQL);
		           if(maxConfdate!=null&&!maxConfdate.equals(""))//表示是保全加费
		           {
		        	   mconfdate = maxConfdate;
		        	   AFlag = "0";
		           }
		           else //付费
		           {
		        	   AFlag = "1";
		           }
	        }
        }
        return mconfdate;

    }
    
    /**
     * 查询保单回访信息
     * 目前山东访后付费只作用于 个险保障期间一年期以上产品 及 保证续保产品
     * @param 传入对应的险种号信息
     * @return String
     */
    private String queryRiskFlag(String tRiskCode){
   	 ExeSQL tExeSQL = new ExeSQL();
   	 String tFlag = "";
   	 String sql = "select 'Y' from lmriskapp where riskprop='I'  and  RiskPeriod = 'L' and riskcode = '"+tRiskCode+"' "
                    +" union "
                    +" select 'Y' from lmrisk a ,lmriskapp b where a.RnewFlag='B' and a.riskcode=b.riskcode and b.riskprop='I' and b.riskcode ='"+tRiskCode+"'";
   	 tFlag=tExeSQL.getOneValue(sql);
   	 if(tFlag==null||("").equals(tFlag))
   	 {
   		 tFlag="N";
   	 }
   	 return tFlag;
   	 
    }
    
    /**
     * 查询保单回访信息
     *
     * @param 
     * @return String
     */
    private String queryReturnVisit(String tContno)
    {
   	 String tVisitTime = "";
   	 String sql = "select CompleteTime from db2inst1.ReturnVisitTable where RETURNVISITFLAG in ('1','4') and POLICYNO = '"+tContno+"' order by makedate desc fetch first 1 rows only";
   	 ExeSQL tExeSQL = new ExeSQL();
   	 tVisitTime = tExeSQL.getOneValue(sql);
   	 if (tExeSQL.mErrors.needDealError() == true) 
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tExeSQL.mErrors);
          CError tError = new CError();
          tError.moduleName = "AgentWageCalSaveNewBL";
          tError.functionName = "queryReturnVisit";
          tError.errorMessage = "回访数据表查询失败!";
          this.mErrors.addOneError(tError);
          return null;
        }
   	 
   	 return tVisitTime;
    }
    /*
    tFlag=01 按照回执回销计算
    tFlag=02 按照实收表的makedate计算
   */
   private String calCalDateForAll(String cBranchType, String cBranchType2,
                                   String cSignDate, String cGetPolDate,
                                   String cPayCount, String cTMakeDate,
                                   String cCustomGetPolDate, String cValidDate,String cFlag,
                                   String cConfDate,
                                   String cAFlag,String cManageCom,String cVisistTime,String cRiskFlag,String cTransState,String cAgentCode,String cGrpcontno,String cVisitFlag,String cRiskCode)
   {
       String tGetPolDate = cGetPolDate == null ? "" : cGetPolDate;
       String tSignDate = cSignDate == null ? "" : cSignDate;
       String tTMakeDate = cTMakeDate == null ? "" : cTMakeDate;
       String tCustomGetPolDate = cCustomGetPolDate == null ? "" : cCustomGetPolDate;
       String tBranchType = cBranchType == null ? "" : cBranchType;
       String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
       String tPayCount = cPayCount == null ? "" : cPayCount;
       String tConfDate = cConfDate ==null? "": cConfDate;
       String tFlag = cFlag== null ? "" : cFlag ;
       String tAFlag = cAFlag== null ? "" : cAFlag ;//团险根据收费方式采取不同的佣金计算方式
       String tManageCom=cManageCom==null ? "" : cManageCom ;
       String tVisistTime = cVisistTime==null ? "" : cVisistTime ;
       String tRiskFlag = cRiskFlag==null ? "" : cRiskFlag ;
       String tTransState = cTransState==null ? "" : cTransState ;
       String tAgentCode =cAgentCode ==null ? "" :cAgentCode;
       String tGrpcontno =cGrpcontno ==null ? "" :cGrpcontno;
       String tVisitFlag =cVisitFlag ==null ? "Y" :cVisitFlag;
       String tRiskCode =cRiskCode ==null ? "Y" :cRiskCode;
       
       if (tBranchType == "") 
       {
           return "";
       }
       if (tBranchType2 == "") {
           return "";
       }
       //如果是续保、续期，则不需要按照回执回销计算
       else if (mLACommisionSchema.getPayCount()>1)
       {
           tFlag="02";
       }

       Calculator tCalculator = new Calculator();
       tCalculator.setCalCode("wageno");
       tCalculator.addBasicFactor("GetPolDate", tGetPolDate);
       tCalculator.addBasicFactor("SignDate", tSignDate);
       tCalculator.addBasicFactor("TMakeDate", tTMakeDate);
       tCalculator.addBasicFactor("CustomGetPolDate", tCustomGetPolDate);
       tCalculator.addBasicFactor("BranchType", tBranchType);
       tCalculator.addBasicFactor("BranchType2", tBranchType2);
       tCalculator.addBasicFactor("ValiDate", cValidDate);
       tCalculator.addBasicFactor("PayCount", tPayCount);
       tCalculator.addBasicFactor("Flag", tFlag);
       tCalculator.addBasicFactor("ConfDate", tConfDate);
       tCalculator.addBasicFactor("AFlag", tAFlag);
       tCalculator.addBasicFactor("ManageCom", tManageCom);
       tCalculator.addBasicFactor("VisistTime", tVisistTime);
       tCalculator.addBasicFactor("RiskFlag", tRiskFlag);
       tCalculator.addBasicFactor("TransState", tTransState);
       tCalculator.addBasicFactor("AgentCode", tAgentCode);
       tCalculator.addBasicFactor("GroupAgentCode", tGrpcontno);
       tCalculator.addBasicFactor("VisitFlag", tVisitFlag);
       //用来兼容校验回访日期中：河北追加保费的不用校验回访日期
       tCalculator.addBasicFactor("RiskCode", tRiskCode);
       
       String tResult = tCalculator.calculate();
       return tResult == null ? "" : tResult;

   }
   
   /**
    * caigang
    * 查询集体保单表
    * 输出：如果查询时发生错误则返回null,否则返回Schema
    */
   private LCContSchema queryLCCont(String tContNo) {
       LCContDB tLCContDB = new LCContDB();
       tLCContDB.setContNo(tContNo);
       LCContSchema tLCContSchema = new LCContSchema();
       if (!tLCContDB.getInfo()) {
           LBContDB tLBContDB = new LBContDB();
           tLBContDB.setContNo(tContNo);
           if (!tLBContDB.getInfo()) {
               if (tLBContDB.mErrors.needDealError() == true) {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tLBContDB.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "AgentWageCalSaveNewBL";
                   tError.functionName = "queryLCCont";
                   tError.errorMessage = "集体保单备份表查询失败!";
                   this.mErrors.addOneError(tError);
                   return null;
               }
               return null;
           }
           Reflections tReflections = new Reflections();
           LBContSchema tLBContSchema = new LBContSchema();
           tLBContSchema = tLBContDB.getSchema();
           tReflections.transFields(tLCContSchema, tLBContSchema);
       } else {
           tLCContSchema = tLCContDB.getSchema();
       }
       String tSalChnl = tLCContSchema.getSaleChnl().trim();
       String tSalChnlDetail = tLCContSchema.getSaleChnlDetail();
       
       System.out.println("SaleChnl:" + tLCContSchema.getSaleChnl());
       System.out.println("SaleChnlDetail:" + tLCContSchema.getSaleChnlDetail());
       if (tSalChnl == null || tSalChnl.equals("")) {
           return null;
       }
       if (tSalChnlDetail == null || tSalChnlDetail.equals("")) {
           tSalChnlDetail = "01"; //2005-11-7   MODIFY:XIANGCHUN
       }
       System.out.println("mSaleChnl[0]:" + mSaleChnl[0]);
       System.out.println("mSaleChnl[1]:" + mSaleChnl[1]);
       System.out.println("mSaleChnl[1]:" + mSaleChnl[2]);
       
     //2014-12-26 modify yangyang
       String Crs_SaleChnl = tLCContSchema.getCrs_SaleChnl();
       String Crs_BussType = tLCContSchema.getCrs_BussType();
       if(tSalChnl.equals("14")&&Crs_SaleChnl!=null&&!Crs_SaleChnl.equals(""))
       {
       	return null;
       }
       if(tSalChnl.equals("15"))
       {
       	if("".equals(Crs_SaleChnl)||null==Crs_SaleChnl)
       	{
       		return tLCContSchema;
       	}
       	else if(this.mSaleChnl[2].indexOf(Crs_BussType) != -1)
       	{
       		return tLCContSchema;
       	}
       	else
       	{
       		return null;
       	}
       }
   	if (this.mSaleChnl[0].indexOf(tSalChnl) != -1 &&
   			this.mSaleChnl[1].indexOf(tSalChnlDetail) != -1) {
   		
   		return tLCContSchema;
   		
   	} else {
   		return null;
   	}
   }
   /**
    * 2655 需求，对于个险河北回访日期的数据进行不校验回访日期：
    * 1)、2015-4-1日前的历史保单万能追加保费数据和涉及到的保全数据 2)、非历史保单中通过保全来收取的追加保费的数据
    * @param cReceiptNo   
    * @param cContNo 保单号
    * @return
    */
   private String judgeVisitFlag(String cReceiptNo,String cContNo,String cContType)
   {	
	   String  tJudgeSql ="";
	   String tHistoryFlag  = "";
	   if("1".equals(cContType))
	   {
		   tHistoryFlag ="select 1 from lacommision where contno ='"+cContNo+"' and tmakedate <'2015-4-1' with ur";
		   tJudgeSql="select  1  from lpedoritem where edorno in(select incomeno from ljapay where payno ='"+cReceiptNo+"')  and edortype='ZB' and edorstate='0'";
	   }
	   else if("0".equals(cContType))
	   {
		   tHistoryFlag ="select 1 from lacommision where grpcontno ='"+cContNo+"' and tmakedate <'2015-4-1' with ur";  
		   tJudgeSql="select  1  from lpgrpedoritem where edorno in(select incomeno from ljapay where payno ='"+cReceiptNo+"')  and edortype='ZB' and edorstate='0'";
	   }
	   String tResult = new ExeSQL().getOneValue(tJudgeSql);
	   if("1".equals(tResult))
	   {
		   return "N";
	   }
	   tResult = new ExeSQL().getOneValue(tHistoryFlag);
	   if("1".equals(tResult))
	   {
		   return "N";
	   }
	   return "Y";
   }
}
