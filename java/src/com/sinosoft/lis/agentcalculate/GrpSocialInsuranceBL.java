package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.*;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;


/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p> 
 *
 * @author caigang
 * @version 1.0
 */
public class GrpSocialInsuranceBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private FDate fDate = new FDate();
    private String mOperate;
    private SSRS mBranchTypes;
    private String mManageCom;
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mStartDate = "";
    private String mEndDate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();

    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LACommisionSet mLAUpdateCommisionSet = new LACommisionSet();

    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    private boolean mReturnState=false;

    private MMap mMap = new MMap();

    public GrpSocialInsuranceBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //获取数据
    private boolean getInputData(VData cInputData) {
        mLAWageLogSchema = (LAWageLogSchema) cInputData.getObjectByObjectName(
                "LAWageLogSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mLAWageLogSchema == null || mGlobalInput == null) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        this.mBranchType = mLAWageLogSchema.getBranchType();
        this.mBranchType2 = mLAWageLogSchema.getBranchType2();
        this.mManageCom = mLAWageLogSchema.getManageCom();
        this.mStartDate = mLAWageLogSchema.getStartDate();
        this.mEndDate = mLAWageLogSchema.getEndDate();
        System.out.println("团险社保业务维护："+mBranchType+"/"+mBranchType2+"/"+mManageCom+"/"+mEndDate);
        return true;
    }

  


   
    //提交数据
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
     
        if (!dealData()) {
        	 CError tError = new CError();
             tError.moduleName = "RgtSurveyBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据处理失败RgtSurveyBL-->dealData!";
             this.mErrors.addOneError(tError);
             return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    

    //提交数据
    private boolean prepareOutputData() {
    	
        mMap.put(mLAUpdateCommisionSet, "UPDATE");
        this.mInputData.add(mMap);
        return true;
    }
    
    
    //处理数据
    private boolean dealData() {

    String  sql =" select CommisionSN from lacommision where managecom='" +mManageCom + "' "
                +" and branchtype='2' and branchtype2='01' and grpcontno<>'00000000000000000000'  " //团险渠道 团单
                +" and tmakedate='"+mEndDate+"' " //提数当天
                +" and ( "
                +" markettype not in ('1','9') or "
                +" riskcode in ('161101','161201','161301','161401','161601','560301','560501','460201','560901')"
                +" )"
                +" and commdire<>'2' "  //去除犹豫期撤保
                +" with ur";

        System.out.println("查询当天团险渠道为社保业务的保单："+sql);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionDB.setCommisionSN(tSSRS.GetText(i, 1));
            tLACommisionDB.getInfo();
            tLACommisionSchema = tLACommisionDB.getSchema();
            tLACommisionSchema.setCommDire("0");
            tLACommisionSchema.setModifyDate(this.currentDate);
            tLACommisionSchema.setModifyTime(this.currentTime);
            this.mLAUpdateCommisionSet.add(tLACommisionSchema);
            }
        return true;
   }
          

    private void jbInit() throws Exception {
    }


   
     public static  void main(String[] args)
     {
   	   LCPolDB tLCPolDB=new LCPolDB();
   	   tLCPolDB.setPolNo("21006924937");
   	   tLCPolDB.getInfo();
   	   LCPolSchema tLCPolSchema=new LCPolSchema();
   	   tLCPolSchema=tLCPolDB.getSchema();
   	  // double tamnt=getOminAmnt(tLCPolSchema);
   	   double tamnt=CommonBL.getTBAmnt(tLCPolSchema);
   	   System.out.println("amnt:"+tamnt);
     }
   }
