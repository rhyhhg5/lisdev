package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.agentcharge.LAChargeCheckBL;
import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LAActiveMixedBalanceBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author miaoxz
 * @version 1.0----2009-1-13
 */
public class LAActiveMixedBalanceBL {
  //错误处理类
  public static CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */ 
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAChargeSet mLAChargeSet = new LAChargeSet();
  private LAChargeSet mUpLAChargeSet = new LAChargeSet();
  private LAChargeSet mJSLAChargeSet = new LAChargeSet();
  private LJAGetSet mLJAGetSet=new LJAGetSet();
  private String mManageCom="";
  private String mAgentCom="";
  private String mStartDate="";
  private String mEndDate="";
  private String mCrsType="";
  private String mContType = "";
  private String mBatchNo ="";
  private String mContNo="";
  private String mOperator="";
  private String mChargeType = "";

  public LAActiveMixedBalanceBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAActiveMixedBalanceBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
//  如果是江苏中介，对于其时行 校验
    if(!checkChargePay())
    {
    	return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    
    if (!tPubSubmit.submitData(mInputData, "UPDATE||MAIN")) {
      // @@错误处理
      mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAActiveMixedBalanceBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
    	mErrors.clearErrors();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
      this.mOperator = this.mGlobalInput.Operator;
     if(this.mOperate.equals("SELECTPAY")){
    	 this.mLAChargeSet.set( (LAChargeSet) cInputData.getObjectByObjectName("LAChargeSet",0));
    	 System.out.println("LAChargeSet get"+mLAChargeSet.size());
    	 this.mManageCom = this.mLAChargeSet.get(1).getManageCom();
         this.mContType=(String)cInputData.getObjectByObjectName("String",0);
     }
     else if(this.mOperate.equals("ALLPAY")){
    	 this.mManageCom=(String)cInputData.get(1);
    	 this.mAgentCom=(String)cInputData.get(2);
    	 this.mStartDate=(String)cInputData.get(3);
    	 this.mEndDate=(String)cInputData.get(4);
    	 this.mCrsType=(String)cInputData.get(5);
    	 this.mContType=(String)cInputData.get(6);
    	 this.mContNo=(String)cInputData.get(7);
    	 this.mChargeType=(String)cInputData.get(8);
     }
     else if(this.mOperate.equals("DEALWT")){
    	 this.mManageCom=(String)cInputData.get(1);
    	 this.mAgentCom=(String)cInputData.get(2);
    	 this.mStartDate=(String)cInputData.get(3);
    	 this.mEndDate=(String)cInputData.get(4);
    	// this.mCrsType=(String)cInputData.get(5);
    	 this.mContType=(String)cInputData.get(5);
     }
     else{
    	 CError tError = new CError();
         tError.moduleName = "LAActiveMixedBalanceBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "不支持的操作类型,请验证操作类型.";
         mErrors.addOneError(tError);
         return false;
     }
     // this.mLARateCommisionSet.set( (LARateCommisionSet) cInputData.get(1));
//   对于江苏中介机构的批次号进行重新生成
     if("8632".equals(this.mManageCom.substring(0, 4)))
     {
    	 String serialno = PubFun.getCurrentDate2()+this.mManageCom.substring(0, 4);
    	 String prefix = PubFun1.CreateMaxNo(serialno, 4);
    	 this.mBatchNo = serialno.substring(0, 12)+this.mManageCom.substring(4, 8)+prefix;
    	 System.out.println("*****:"+mBatchNo);
     }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveMixedBalanceBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 新增，修改传入的数据校验
   */
  private boolean check(){
	  //不需要数据校验
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
	  System.out.println("fffffffffff");
    try {
    	LAChargeDB tLAChargeDB=new LAChargeDB();
    	
        LACommisionSet tLACommisionSet=null;
        LACommisionDB tLACommisionDB=new LACommisionDB();
        String tLimit = null;
       
        if (mOperate.equals("SELECTPAY")) {
          LAChargeSet tLAChargeSet ;
          for (int i = 1; i <= mLAChargeSet.size(); i++) {
          tLAChargeSet=new LAChargeSet();
          String tSQL="select * from lacharge where commisionsn='"+mLAChargeSet.get(i).getCommisionSN()+"' and   charge<>0 with ur";
          System.out.println(tSQL);
          tLAChargeSet=tLAChargeDB.executeQuery(tSQL);
          
          for(int j=1;j<=tLAChargeSet.size();j++){
        	  tLAChargeSet.get(j).setChargeState("1");
        	  tLAChargeSet.get(j).setBatchNo(this.mBatchNo);
        	  tLAChargeSet.get(j).setModifyDate(CurrentDate);
        	  tLAChargeSet.get(j).setModifyTime(CurrentTime);
        	  tLAChargeSet.get(j).setOperator(this.mGlobalInput.Operator);
        	 
        	  
        	  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
              //tLimit = PubFun.getNoLimit(tLAChargeSet.get(j).getManageCom());
              //tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("HDGETNO", tLimit));
        	  tLJAGetSchema.setActuGetNo(tLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setOtherNo(tLAChargeSet.get(j).getReceiptNo());
              if("55".equals(tLAChargeSet.get(j).getChargeType())){
            	  tLJAGetSchema.setOtherNoType("BC");
            	  this.mJSLAChargeSet.add(tLAChargeSet.get(j));
              }
              if("56".equals(tLAChargeSet.get(j).getChargeType())){
            	  tLJAGetSchema.setOtherNoType("MC");
              }
              tSQL="select * from lacommision where receiptno='"
        		  +tLAChargeSet.get(j).getReceiptNo()+"' and commisionsn='"+tLAChargeSet.get(j).getTCommisionSN()+"' fetch first 1 rows only ";
              System.out.println(tSQL);
        	  tLACommisionSet=tLACommisionDB.executeQuery(tSQL);
        	  String tRiskFlag =queryRiskFlag(tLACommisionSet.get(1).getRiskCode());
              if(this.mManageCom.equals("86371000")||this.mManageCom.equals("86370600"))
              {
            	  if((tLACommisionSet.get(1).getReNewCount()==0)&&(tLACommisionSet.get(1).getPayYear()==0)&&("Y").equals(tRiskFlag))
            	  {
            		  String check = "select '1' from  ReturnVisitTable where policyno ='"+mUpLAChargeSet.get(j).getContNo()+"' and returnvisitflag in ('1','4')";
            		  ExeSQL tExeSQL = new ExeSQL();
            		  String tResult = tExeSQL.getOneValue(check);
            		  if(tResult==null||("").equals(tResult))
            		  {
            			  continue;
            		  }
            	  }
              }
        	  
        	  tLJAGetSchema.setPayMode("12");
        	  tLJAGetSchema.setManageCom(tLAChargeSet.get(j).getManageCom());
              tLJAGetSchema.setAgentCom(tLAChargeSet.get(j).getAgentCom());
              //互动渠道与字段Tcommisionsn关联
              tLJAGetSchema.setGetNoticeNo(tLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setAgentType(tLACommisionSet.get(1).getAgentType());
              tLJAGetSchema.setAgentCode(tLACommisionSet.get(1).getAgentCode());
              tLJAGetSchema.setAgentGroup(tLACommisionSet.get(1).getAgentGroup());
              tLJAGetSchema.setAppntNo(tLACommisionSet.get(1).getAppntNo());
              tLJAGetSchema.setSumGetMoney(tLAChargeSet.get(j).getCharge());
              tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(1).getAgentCom()));
              tLJAGetSchema.setDrawerID(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setShouldDate(CurrentDate);
              tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
              tLJAGetSchema.setMakeDate(CurrentDate);
              tLJAGetSchema.setMakeTime(CurrentTime);
              tLJAGetSchema.setModifyDate(CurrentDate);
              tLJAGetSchema.setModifyTime(CurrentTime);
              this.mLJAGetSet.add(tLJAGetSchema);
              
          }  
          this.mUpLAChargeSet.add(tLAChargeSet);
        }
        map.put(this.mUpLAChargeSet, "UPDATE");
        map.put(this.mLJAGetSet, "INSERT");
      }
      else if(mOperate.equals("ALLPAY")){
    	  System.out.println("@@@@@@@@@@@@@@");
    	  String tSQL="select * from lacharge a where"
        	  +" branchtype='5' and branchtype2='01' and chargestate='0' and charge<>0 "
        	  +" and managecom='"+this.mManageCom
        	  +"' and tmakedate between '"
        	  +this.mStartDate+"' and '"+this.mEndDate+"'"
        	  +" and (DAYS(CURRENT DATE)-DAYS((select CustomGetPolDate from lacommision where commisionsn = a.commisionsn) ))>=15 ";
    	  if("0".equals(mChargeType)){
    		  tSQL+=" and chargetype = '55' ";
    	  }
    	  if("1".equals(mChargeType)){
    		  tSQL+=" and chargetype = '56' ";
    	  }
    	  if(this.mAgentCom!=null&&!this.mAgentCom.equals("")){
    		  tSQL+=" and agentcom like '"+this.mAgentCom+"%'";
    	  }
    	  if("0".equals(this.mCrsType)){
    		  tSQL+=" and 1=1 ";
    	  }
    	  else if("N".equals(this.mCrsType)){
    		  tSQL+=" and  exists(select 1 from lacommision where  a.tcommisionsn = commisionsn and branchtype3='"+this.mCrsType+"')" ;
    	  }
    	  else if("Y".equals(this.mCrsType)){
    		  tSQL+=" and  exists(select 1 from lacommision where  a.tcommisionsn = commisionsn and branchtype3='2')" ;
    	  }
//    	  //如果为烟台或威海  必须回访后才能结算
//    	  if(this.mManageCom.equals("86371000")||this.mManageCom.equals("86370600"))
//    	  {
//    		  tSQL+=" and exists (select '1' from ReturnVisitTable where policyno=lacharge.contno and returnvisitflag in ('1','4'))";
//    	  }
    	  if(this.mContType!=null&&!this.mContType.equals("")){
    		  if("0".equals(this.mContType))
    		  {
    			  tSQL+=" and exists(select 1 from lccont where grpcontno =a.grpcontno and conttype ='2' union select 1 from lbcont where grpcontno =a.grpcontno and conttype ='2' union select 1 from lpcont where grpcontno =a.grpcontno and conttype ='2' )";  
    		  }else if("1".equals(this.mContType))
    		  {
    			  tSQL+=" and exists(select 1 from lccont where contno =a.contno and conttype ='1' union select 1 from lbcont where contno =a.contno and conttype ='1' union select 1 from lpcont where contno =a.contno and conttype ='1' )";    
    		  }else{
    			  
    		  }
    	  }
    	  if(this.mContNo!=null && !this.mContNo.equals("")){
    		  tSQL+="and (contno ='"+mContNo+"' or grpcontno ='"+mContNo+"')";
    	  }
    	  tSQL+=" with ur";
    	  System.out.println(tSQL);

    	  this.mUpLAChargeSet=tLAChargeDB.executeQuery(tSQL);
    	  System.out.println("&&&&&&&&&&&&&&&&&&");
    	  // add new 
          for(int j=1;j<=mUpLAChargeSet.size();j++){    		  
        	  LAChargeSchema t_LAChargeSchema =mUpLAChargeSet.get(j).getSchema();
//        	 20120305 gaoy 防止并发加锁
      		  MMap tCekMap1 = null;
      		  tCekMap1 = lockLJAGet( t_LAChargeSchema);
      	      if (tCekMap1 == null)
      	      {
      	        return false;
      	      }
      	      map.add(tCekMap1);
        	//江苏中介手续费计算时会进行校验
//              if("8632".equals(t_LAChargeSchema.getManageCom().substring(0,4)))
//              {
//              	ContInputChargecomChkBL tContInputChargecomChkBL = new ContInputChargecomChkBL();
//              	//手续费校验
//              	String zChargeType = "ZX";
//              	VData zVData = new VData();
//              	zVData.add(t_LAChargeSchema);
//              	zVData.add(zChargeType);
//              	if(!tContInputChargecomChkBL.submitData(zVData, ""))
//              	{
//              		continue;
//              	}
//              	else
//              	{
//              		String hChargeType = "HX";
//                  	VData hVData = new VData();
//                  	hVData.add(t_LAChargeSchema);
//                  	hVData.add(hChargeType);
//              		if(!!tContInputChargecomChkBL.submitData(hVData, ""))
//              		{
//              			continue;
//              		}
//              	}
//              }
              //江苏中介手续费计算时会进行校验
              
      	    mUpLAChargeSet.get(j).setChargeState("1");
      	  mUpLAChargeSet.get(j).setBatchNo(this.mBatchNo);
      	  mUpLAChargeSet.get(j).setModifyDate(CurrentDate);
      	mUpLAChargeSet.get(j).setModifyTime(CurrentTime);
      	mUpLAChargeSet.get(j).setOperator(this.mGlobalInput.Operator);
    		  
    		  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
             // tLimit = PubFun.getNoLimit(mUpLAChargeSet.get(j).getManageCom());
             // tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("HDGETNO", tLimit));
    		  tLJAGetSchema.setActuGetNo(mUpLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setOtherNo(mUpLAChargeSet.get(j).getReceiptNo());
              if("55".equals(mUpLAChargeSet.get(j).getChargeType())){
                  tLJAGetSchema.setOtherNoType("BC");
                  //业务手续费需上报江苏中介
                  this.mJSLAChargeSet.add(mUpLAChargeSet.get(j).getSchema());
              }
              if("56".equals(mUpLAChargeSet.get(j).getChargeType())){
                  tLJAGetSchema.setOtherNoType("MC");
              }
              tSQL="select * from lacommision "
            	  +"where receiptno='" +mUpLAChargeSet.get(j).getReceiptNo()+"'"
            	  +" and  commisionsn='"+mUpLAChargeSet.get(j).getTCommisionSN()+"' "
            	  +" fetch first 1 rows only ";
              tLACommisionSet=tLACommisionDB.executeQuery(tSQL);
              
              // add new 
              String tRiskFlag =queryRiskFlag(tLACommisionSet.get(1).getRiskCode());
              if(this.mManageCom.equals("86371000")||this.mManageCom.equals("86370600"))
              {
            	  if((tLACommisionSet.get(1).getReNewCount()==0)&&(tLACommisionSet.get(1).getPayYear()==0)&&("Y").equals(tRiskFlag))
            	  {
            		  String check = "select '1' from  ReturnVisitTable where policyno ='"+mUpLAChargeSet.get(j).getContNo()+"' and returnvisitflag in ('1','4')";
            		  ExeSQL tExeSQL = new ExeSQL();
            		  String tResult = tExeSQL.getOneValue(check);
            		  if(tResult==null||("").equals(tResult))
            		  {
            			  continue;
            		  }
            	  }
              }
              
              tLJAGetSchema.setPayMode("12");
              tLJAGetSchema.setManageCom(mUpLAChargeSet.get(j).getManageCom());
              tLJAGetSchema.setAgentCom(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setGetNoticeNo(mUpLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setAgentType(tLACommisionSet.get(1).getAgentType());
              tLJAGetSchema.setAgentCode(tLACommisionSet.get(1).getAgentCode());
              tLJAGetSchema.setAgentGroup(tLACommisionSet.get(1).getAgentGroup());
              tLJAGetSchema.setAppntNo(tLACommisionSet.get(1).getAppntNo());
              tLJAGetSchema.setSumGetMoney(mUpLAChargeSet.get(j).getCharge());
              tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(1).getAgentCom()));
              tLJAGetSchema.setDrawerID(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setShouldDate(CurrentDate);
              tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
              tLJAGetSchema.setMakeDate(CurrentDate);
              tLJAGetSchema.setMakeTime(CurrentTime);
              tLJAGetSchema.setModifyDate(CurrentDate);
              tLJAGetSchema.setModifyTime(CurrentTime);
              this.mLJAGetSet.add(tLJAGetSchema); 
          }    	  
        map.put(mUpLAChargeSet, "UPDATE");
        map.put(mLJAGetSet, "INSERT");
      }
      else if(mOperate.equals("DEALWT")){
		  String SQL="select * from lacharge where chargetype='55'"
        	  +" and branchtype='3' and branchtype2='01' and chargestate='0"
        	  +"' and managecom='"+this.mManageCom+"'"
        	  +"  and contno in ( "
        	  +" select contno  from lacharge  where chargetype='55'"
        	  +" and branchtype='3' and branchtype2='01' and chargestate='0' "
        	  +" and managecom='"+this.mManageCom+"'"
        	  +" and transtype='ZC'"
        	  +" and  exists (select '1' from lacharge a where a.contno=lacharge.contno "
        	  +" and a.transtype='WT' and a.tmakedate>=lacharge.tmakedate "
        	  +" and a.chargestate='0' ))  with ur ";
    	
    	  System.out.println("..........SQL"+SQL);
    	  this.mUpLAChargeSet=tLAChargeDB.executeQuery(SQL);
    	  for(int j=1;j<=mUpLAChargeSet.size();j++){
    		  
    		  mUpLAChargeSet.get(j).setChargeState("1");
    		  mUpLAChargeSet.get(j).setModifyDate(CurrentDate);
    		  mUpLAChargeSet.get(j).setModifyTime(CurrentTime);
    		  mUpLAChargeSet.get(j).setOperator(this.mGlobalInput.Operator);
          } 
    	  map.put(mUpLAChargeSet, "UPDATE");
	  }
      else{
    	  CError tError = new CError();
          tError.moduleName = "LAActiveMixedBalanceBL";
          tError.functionName = "dealData";
          tError.errorMessage = "不支持的操作类型,请验证操作类型.";
          mErrors.addOneError(tError);
          return false;
      }
    }
    
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveMixedBalanceBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
    	
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAActiveMixedBalanceBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private String getAgentComName(String tAgentCom){
	  String comName = "";
	  ExeSQL tExeSQL=new ExeSQL();
	  String sql=" select name from lacom where agentcom='"+tAgentCom+"' with ur ";
	  SSRS tSSRS = tExeSQL.execSQL(sql);	    	 
	  comName = tSSRS.GetText(1, 1);
	  return comName ;
  }
  
  
  private MMap lockLJAGet(LAChargeSchema tLAChargeSchema)
  {
  	
  	
      MMap tmap = null;
      /**锁定标志"TX"*/
      String tLockNoType = "TX";
      /**锁定时间*/
      String tAIS = "300";
      System.out.println("设置了60分的解锁时间");
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("LockNoKey", tLAChargeSchema.getCommisionSN());
      tTransferData.setNameAndValue("LockNoType", tLockNoType);
      tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
     
      VData tVData = new VData();
      tVData.add(tTransferData);
      tVData.add(mGlobalInput);
      LockTableActionBL tLockTableActionBL = new LockTableActionBL();
      tmap = tLockTableActionBL.getSubmitMap(tVData, null);
      
      if (tmap == null)
      {           	
          mErrors.copyAllErrors(tLockTableActionBL.mErrors);
          return null;
      }
      return tmap;
  }
  
  private String queryRiskFlag(String riskcode)
  {
	  
	  String sql = "select 'Y' from ldcode where codetype = 'activeriskcode' and code in (select riskcode from lmriskapp where riskprop='I'  and  RiskPeriod = 'L') and code = '"+riskcode+"' ";
	  ExeSQL tExeSQL = new ExeSQL();
	  String tRiskFlag =tExeSQL.getOneValue(sql);
	  if(tRiskFlag==null||"".equals(tRiskFlag))
	  {
		  tRiskFlag="N"; 
	  }
	  return tRiskFlag;
  }
  private boolean checkChargePay()
  { System.out.println("bbbbbbbbbbbb");
	//江苏中介手续费计算时会进行校验
  System.out.println("-----------:"+this.mManageCom);
  if("8632".equals(this.mManageCom.substring(0,4))&&mJSLAChargeSet.size()>0)
  {System.out.println("aaaaaaaaaaaaaaaaa");
	  LAChargeCheckBL tLAChargeCheckBL = new LAChargeCheckBL();
  	//手续费校验
//  	String zChargeType = "ZX";
//  	VData zVData = new VData();
//  	zVData.add(mUpLAChargeSet);
//  	zVData.add(zChargeType);
//  	zVData.add(this.mBatchNo);
//  	zVData.add(this.mOperator);
//  	if(!tLAChargeCheckBL.submitData(zVData,""))
//  	{
//  		this.mErrors.copyAllErrors(tLAChargeCheckBL.mErrors);
//  		CError tError = new CError();
//			tError.moduleName = "LAARiskChargePayBL";
//			tError.functionName = "dealData";
//			tError.errorMessage = "江苏中介手续费校验出错。";
//			this.mErrors.addOneError(tError);
//  		return false;
//  	}
		String hChargeType = "HX";
  	VData hVData = new VData();
  	hVData.add(mJSLAChargeSet);
  	hVData.add(hChargeType);
  	hVData.add(this.mBatchNo);
  	hVData.add(this.mOperator);
	hVData.add(this.mContType);
		if(!tLAChargeCheckBL.submitData(hVData, ""))
		{
			this.mErrors.copyAllErrors(tLAChargeCheckBL.mErrors);
			CError tError = new CError();
		tError.moduleName = "LAARiskChargePayBL";
		tError.functionName = "dealData";
		tError.errorMessage = "江苏中介手续费结算出错。";
		this.mErrors.addOneError(tError);
  		return false;
		}
  }
  return true;
}
}

