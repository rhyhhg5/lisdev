package com.sinosoft.lis.agentcalculate;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAChargeLogSchema;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentChargeCalNewUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData = new VData();
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAChargeLogSchema mLAChargeLogSchema = new LAChargeLogSchema();


    public AgentChargeCalNewUI() {

    }

    public static void main(String[] args) {
        VData tVData = new VData();
        LAChargeLogSchema tLAChargeLogSchema = new LAChargeLogSchema();
        tLAChargeLogSchema = new LAChargeLogSchema();
        tLAChargeLogSchema.setManageCom("86950000");
        tLAChargeLogSchema.setBranchType("3");
        tLAChargeLogSchema.setBranchType2("01");
        tLAChargeLogSchema.setStartDate("2008-02-01");
        tLAChargeLogSchema.setEndDate("2008-02-03");
        tLAChargeLogSchema.setChargeMonth("02");
        tLAChargeLogSchema.setChargeCalNo("200802");
        tLAChargeLogSchema.setChargeYear("2008");

        GlobalInput tG = new GlobalInput();
        tG.Operator = "ac";
        tG.ManageCom = "86";
        tVData.clear();
        //提交
        tVData.addElement(tG);
        tVData.addElement("86110000");
        tVData.addElement(tLAChargeLogSchema);
        AgentChargeCalNewUI tAgentChargeCalNewUI = new AgentChargeCalNewUI();
        tAgentChargeCalNewUI.submitData(tVData, "INSERT||CALWAGEAGAIN");
        System.out.println("Error:" + tAgentChargeCalNewUI.mErrors.getFirstError());
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        AgentChargeCalNewBL tAgentChargeCalNewBL = new AgentChargeCalNewBL();
        System.out.println("Start AgentChargeCalNewBL Submit...");
        if (!tAgentChargeCalNewBL.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tAgentChargeCalNewBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentChargeCalNewUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End AgentChargeCalNewBL Submit...");
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(mGlobalInput);
            mInputData.add(mLAChargeLogSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentChargeCalNewUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLAChargeLogSchema = ((LAChargeLogSchema) cInputData.
                              getObjectByObjectName(
                                      "LAChargeLogSchema", 0));
        if (mGlobalInput == null || mLAChargeLogSchema == null) {
            CError tError = new CError();
            tError.moduleName = "AgentChargeCalNewUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

}
