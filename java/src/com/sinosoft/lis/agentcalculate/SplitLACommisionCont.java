/*
 * <p>ClassName: LABatchAuthorizeBL </p>
 * <p>Description: ALAAuthorizeUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-15
 */
package com.sinosoft.lis.agentcalculate;

import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agentdaily.LACompactSplitUI;

public class SplitLACommisionCont {
    public SplitLACommisionCont() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAGrpCommisionDetailSet mLAGrpCommisionDetailSet = new
            LAGrpCommisionDetailSet(); // 接收用保单发佣分配数据
    private LACommisionSet mLACommisionSet = new
                                             LACommisionSet(); // 接收用保单发佣分配数据
    private LACommisionBSet mLACommisionBSet = new
                                               LACommisionBSet(); // 接收用保单发佣分配数据
    private LACommisionSet mNewLACommisionSet = new
                                                LACommisionSet(); // 接收用保单发佣分配数据
    private LACommisionSet mDELACommisionSet = new
                                                LACommisionSet(); // 删除的数据
    private LAGrpCommisionDetailSet mLAGrpDealDetailSet = new
            LAGrpCommisionDetailSet(); // 保存用保单发佣分配数据
    private LACommisionSchema mLACommisionchema = new LACommisionSchema();
    private MMap mMap = new MMap();
    private VData mOutputDate = new VData();
    private String mGrpContNo = "";
    private String mCurrDate = PubFun.getCurrentDate();
    private String mCurrTime =PubFun.getCurrentTime();
    private String mModifyTime = PubFun.getCurrentTime();
    private int nCnt = 0;
    private int mCnt = 0;


    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行有关验证
        //if (!check()) {
        //    return false;
        //}
        //进行业务处理
        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputDate, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
                "GlobalInput", 0));
         mLACommisionchema= ((LACommisionSchema) pmInputData.
                            getObjectByObjectName("LACommisionSchema", 0));

        if (mGlobalInput == null || mLACommisionchema == null) {
            System.out.println("has null");
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("保单号：[ " + mLACommisionchema.getGrpContNo() + " ]");
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData() {

        StringBuffer tSb = new StringBuffer();
        String tGrpContNo = "";
        LACommisionDB tLACommisionDB = new
                                       LACommisionDB();
        tSb = new StringBuffer();
        tSb.append(" SELECT * FROM LACOMMISION WHERE wageno='")
                .append(mLACommisionchema.getWageNo())
                .append("' and grpcontno ='")
                .append(mLACommisionchema.getGrpContNo())
                .append("' AND BRANCHTYPE='")
                .append(mLACommisionchema.getBranchType())
                .append("' AND BRANCHTYPE2='")
                .append(mLACommisionchema.getBranchType2())
                .append("' and  exists (select grpcontno from LAGRPCOMMISIONDETAIL where grpcontno ='"+mLACommisionchema.getGrpContNo()+"')")
                ;
        String tSql = tSb.toString();
        System.out.println(tSql);
        mLACommisionSet = tLACommisionDB.executeQuery(tSql);
        System.out.println("+++++"+mLACommisionSet.size());
        if (mLACommisionSet == null || mLACommisionSet.size() == 0) {

            CError tError = new CError();
            tError.moduleName = "SplitLACommisionCont";
            tError.functionName = "dealData";
            tError.errorMessage = "扎帐表中不存在要拆分的保单";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= mLACommisionSet.size(); i++) {

            LACommisionSchema tLACommisionSchema = new
                    LACommisionSchema();
            tLACommisionSchema = mLACommisionSet.get(i);
            if (mGrpContNo.equals("") ||
                !mGrpContNo.equals(tLACommisionSchema.getGrpContNo())) {
                nCnt = 1;
            }
            tSb = new StringBuffer();
            tSb.append(
                    " select count(distinct agentcode) from LACommision WHERE GrpPolNo ='")
                    .append(tLACommisionSchema.getGrpPolNo()+"'")
                    .append(" and transtype='" +tLACommisionSchema.getTransType() + "'   ")
                    .append(" and wageno='" +tLACommisionSchema.getWageNo() + "'   ")
                    .append(" and tmakedate='" +tLACommisionSchema.getTMakeDate() + "'   ");
                    if(tLACommisionSchema.getTransType().equals("ZC"))
                    {
                      tSb.append(" and ReceiptNo='"+tLACommisionSchema.getReceiptNo()+"'   " );
                    }
            tSql = tSb.toString();
            System.out.println(tSql);
            ExeSQL exeSQL = new ExeSQL();
            SSRS ssrs = exeSQL.execSQL(tSql);
            //查询是否进行了保单拆分
            if (exeSQL.mErrors.needDealError()) {
                mErrors.copyAllErrors(exeSQL.mErrors);
                return false;
            }
            int nCurCount = Integer.parseInt(ssrs.GetText(1, 1));
            if (nCurCount > 1)
            {

//                if (tGrpContNo.equals("") ||
//                    !tGrpContNo.equals(tLACommisionSchema.getGrpContNo())) {
//                    DealOldLACommission(tLACommisionSchema);
//                    tGrpContNo = tLACommisionSchema.getGrpContNo();
//                } else {
//                    continue;
//                }

                //continue;
           CError tError = new CError();
           tError.moduleName = "SplitLACommisionCont";
           tError.functionName = "dealData";
           tError.errorMessage = "保单"+mLACommisionchema.getGrpContNo()+"在"+mLACommisionchema.getWageNo()+"已经进行过拆分，无法再次拆分";
           this.mErrors.addOneError(tError);
           return false;


            }
            else
            {
                mDELACommisionSet.add(tLACommisionSchema);
                if (!copyLACommission(tLACommisionSchema)) {
                    return false;
                }
                if (!DealNewLACommission(tLACommisionSchema))
                {
                    return false;
                }
            }
            mGrpContNo = tLACommisionSchema.getGrpContNo();
            nCnt++;
        }
        return true;
    }


    /**
     * 准备往后台的数据
     * @return boolean
     */
     private boolean prepareOutputData() {

//        String tGrpContNoCon = "";
//        LACommisionSet tLACommisionSet = new
//                                         LACommisionSet();
//        LACommisionDB tLACommisionDB = new
//                                       LACommisionDB();
//        LACommisionBSet tLACommisionBSet = new
//                                           LACommisionBSet();
//        LACommisionBDB tLACommisionBDB = new
//                                         LACommisionBDB();
//
//        StringBuffer tSb = new StringBuffer();
//        tSb.append(" SELECT * FROM LACOMMISION WHERE TMAKEDATE>='")
//                .append(mLAWageLogSchema.getStartDate())
//                .append("' and TMAKEDATE <='")
//                .append(mLAWageLogSchema.getEndDate())
//                .append("'  AND MANAGECOM  ='")
//                .append(mLAWageLogSchema.getManageCom())
//                .append("' AND BRANCHTYPE='")
//                .append(mLAWageLogSchema.getBranchType())
//                .append("' AND BRANCHTYPE2='")
//                .append(mLAWageLogSchema.getBranchType2())
//                .append(
//                        "' and  exists (select grpcontno from LAGRPCOMMISIONDETAIL where grpcontno =LACOMMISION.grpcontno)")
//                ;
//        String tSql = tSb.toString();
//        System.out.println(tSql);
//        tLACommisionSet = tLACommisionDB.executeQuery(tSql);
//
//        if (!tLACommisionSet.mErrors.needDealError()) {
//            if (tLACommisionSet.size() > 0) {
//                this.mMap.put(tLACommisionSet, "DELETE");
//            }
//        }

//        tSb = new StringBuffer();
//        tSb.append(" SELECT * FROM LACOMMISIONB WHERE TMAKEDATE>='")
//                .append(mLAWageLogSchema.getStartDate())
//                .append("' and TMAKEDATE <='")
//                .append(mLAWageLogSchema.getEndDate())
//                .append("'  AND MANAGECOM  ='")
//                .append(mLAWageLogSchema.getManageCom())
//                .append("' AND BRANCHTYPE='")
//                .append(mLAWageLogSchema.getBranchType())
//                .append("' AND BRANCHTYPE2='")
//                .append(mLAWageLogSchema.getBranchType2())
//
//                .append(
//                        "' and  EDORTYPE='12' AND exists (select grpcontno from LAGRPCOMMISIONDETAIL where grpcontno =LACOMMISIONB.grpcontno)")
//                ;
//
//        tSql = tSb.toString();
//        for (int i = 1; i <= mLACommisionBSet.size(); i++) {
//            if (tGrpContNoCon.equals("")) {
//                tGrpContNoCon = "'" + mLACommisionBSet.get(i).getGrpContNo() +
//                                "'";
//            } else {
//                tGrpContNoCon = tGrpContNoCon + ",'" +
//                                mLACommisionBSet.get(i).getGrpContNo() + "'";
//            }
//        }
//
//        if (!tGrpContNoCon.equals("")) {
//            tSql = tSql + " and grpcontno in (" + tGrpContNoCon + ") ";
//
//            System.out.println(tSql);
//            tLACommisionBSet = tLACommisionBDB.executeQuery(tSql);
//
//            if (!tLACommisionBSet.mErrors.needDealError()) {
//                if (tLACommisionBSet.size() > 0) {
////                    this.mMap.put(tLACommisionBSet, "DELETE"); //不删除备份表 20070907
//                }
//            }
//        }
        this.mMap.put(this.mDELACommisionSet, "DELETE");
        this.mMap.put(this.mNewLACommisionSet, "INSERT");
        this.mMap.put(this.mLACommisionBSet, "INSERT");

        mOutputDate.add(mMap);

        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean
     */
    private boolean DealOldLACommission(LACommisionSchema ppmLACommisionSchema) {

        StringBuffer tSb = new StringBuffer();
        tSb.append(
                " select * from LACommisionb WHERE EDORTYPE='12' AND GRPCONTNO ='")
                .append(ppmLACommisionSchema.getGrpContNo())
                .append("' order by  makedate desc");
        String tSql = tSb.toString();
        LACommisionBDB tLACommisionBDB = new LACommisionBDB();
        LACommisionBSet tLACommisionBSet = new LACommisionBSet();
        tLACommisionBSet = tLACommisionBDB.executeQuery(tSql);
        if (tLACommisionBSet == null || tLACommisionBSet.size() == 0) {

            CError tError = new CError();
            tError.moduleName = "SplitLACommisionCont";
            tError.functionName = "dealData";
            tError.errorMessage = "扎帐表中不存在要拆分的保单";
            this.mErrors.addOneError(tError);
        }
        String tGrpContNo = "";
        for (int i = 1; i <= tLACommisionBSet.size(); i++) {
            LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();
            tLACommisionBSchema = tLACommisionBSet.get(i);
            if (tGrpContNo.equals("") ||
                !tGrpContNo.equals(tLACommisionBSchema.getGrpContNo())) {
                mCnt = 1;
            }

            tSb = new StringBuffer();
            tSb.append(
                    " select * from LAGRPCOMMISIONDETAIL WHERE GRPCONTNO ='")
                    .append(tLACommisionBSchema.getGrpContNo())
                    .append("'");
            tSql = tSb.toString();
            System.out.println(tSql);

            LAGrpCommisionDetailDB tLAGrpCommisionDetailDB = new
                    LAGrpCommisionDetailDB();
            mLAGrpCommisionDetailSet = tLAGrpCommisionDetailDB.executeQuery(
                    tSql);
            if (mLAGrpCommisionDetailSet == null ||
                mLAGrpCommisionDetailSet.size() == 0) {

                CError tError = new CError();
                tError.moduleName = "SplitLACommisionCont";
                tError.functionName = "dealData";
                tError.errorMessage = "不存在要拆分的保单";
                this.mErrors.addOneError(tError);

            }
            for (int j = 1; j <= mLAGrpCommisionDetailSet.size(); j++) {
                LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new
                        LAGrpCommisionDetailSchema();
                tLAGrpCommisionDetailSchema = mLAGrpCommisionDetailSet.get(
                        j);
                if (!DataLACommissionB(tLACommisionBSchema,
                                       tLAGrpCommisionDetailSchema, j)) {
                    return false;
                }
            }
            tGrpContNo = tLACommisionBSchema.getGrpContNo();
            mCnt++;
        }

        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean
     */
    private boolean DealNewLACommission(LACommisionSchema pmLACommisionSchema) {

        StringBuffer tSb = new StringBuffer();
        tSb = new StringBuffer();
        tSb.append(
                " select * from LAGRPCOMMISIONDETAIL WHERE GRPCONTNO ='")
                .append(pmLACommisionSchema.getGrpContNo())
                .append("'");
        String tSql = tSb.toString();
        System.out.println(tSql);

        LAGrpCommisionDetailDB tLAGrpCommisionDetailDB = new
                LAGrpCommisionDetailDB();
        mLAGrpCommisionDetailSet = tLAGrpCommisionDetailDB.executeQuery(
                tSql);
        if (mLAGrpCommisionDetailSet == null ||
            mLAGrpCommisionDetailSet.size() == 0) {

            CError tError = new CError();
            tError.moduleName = "SplitLACommisionCont";
            tError.functionName = "dealData";
            tError.errorMessage = "不存在要拆分的保单";
            this.mErrors.addOneError(tError);

        }
        for (int j = 1; j <= mLAGrpCommisionDetailSet.size(); j++) {
            LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new
                    LAGrpCommisionDetailSchema();
            tLAGrpCommisionDetailSchema = mLAGrpCommisionDetailSet.get(
                    j);
            tSb = new StringBuffer();
            tSb.append(
                    " SELECT a.agentgroup,a.agentgroup,a.branchattr,a.agentgroup FROM lawage a WHERE 1=1 ")
                    .append(" and a.agentcode ='")
                    .append(tLAGrpCommisionDetailSchema.getAgentCode())
                    .append("' and a.indexcalno='")
                    .append(pmLACommisionSchema.getWageNo())
                    .append("'");
            tSql = tSb.toString();
            ExeSQL tExeSQL = new ExeSQL();
            SSRS ssrs = tExeSQL.execSQL(tSql);
            System.out.println(tSql);
            if (tExeSQL.mErrors.needDealError()) {
                mErrors.copyAllErrors(tExeSQL.mErrors);
                return false;

            }
            if(ssrs.getMaxRow()<=0)
            {
                tSb = new StringBuffer();
                tSb.append(
                        " SELECT a.agentgroup,a.branchcode,b.branchattr,b.branchseries FROM LAAGENT a ,LABRANCHGROUP b WHERE a.AGENTGROUP = b.AGENTGROUP")
                        .append(" and a.agentcode ='")
                        .append(tLAGrpCommisionDetailSchema.getAgentCode())
                        .append("'");
                tSql = tSb.toString();
                System.out.println(tSql);
                ExeSQL exeSQL = new ExeSQL();
                ssrs = exeSQL.execSQL(tSql);
                //查询是否进行了保单拆分
                if (exeSQL.mErrors.needDealError()) {
                    mErrors.copyAllErrors(exeSQL.mErrors);
                    return false;
                }
            }
            pmLACommisionSchema.setAgentGroup(ssrs.GetText(1, 1));
            pmLACommisionSchema.setBranchCode(ssrs.GetText(1, 2));
            pmLACommisionSchema.setBranchAttr(ssrs.GetText(1, 3));
            pmLACommisionSchema.setBranchSeries(ssrs.GetText(1, 4));

            if (!DataLACommission(pmLACommisionSchema,
                                  tLAGrpCommisionDetailSchema, j)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean
     */
    private boolean copyLACommission(LACommisionSchema pmLACommisionSchema) {

        String commEdor = "";
        System.out.println("11111111111111111111|||||||||22222222222222222222");
        commEdor = PubFun1.CreateMaxNo("COMMISIONEDOR", "SN");
        System.out.println("11111111111111111111|||||||||commEdor");
        LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();
        tLACommisionBSchema.setEdorNo(commEdor);
        tLACommisionBSchema.setEdorType("12"); //备份保单拆分
        tLACommisionBSchema.setCommisionSN(pmLACommisionSchema.
                                           getCommisionSN());
        tLACommisionBSchema.setCommisionBaseNo(pmLACommisionSchema.
                                               getCommisionBaseNo());
        tLACommisionBSchema.setWageNo(pmLACommisionSchema.getWageNo());
        tLACommisionBSchema.setGrpContNo(pmLACommisionSchema.getGrpContNo());
        tLACommisionBSchema.setGrpPolNo(pmLACommisionSchema.getGrpPolNo());
        tLACommisionBSchema.setContNo(pmLACommisionSchema.getContNo());
        tLACommisionBSchema.setPolNo(pmLACommisionSchema.getPolNo());
        tLACommisionBSchema.setMainPolNo(pmLACommisionSchema.getMainPolNo());
        tLACommisionBSchema.setManageCom(pmLACommisionSchema.getManageCom());
        tLACommisionBSchema.setAppntNo(pmLACommisionSchema.getAppntNo());
        tLACommisionBSchema.setRiskCode(pmLACommisionSchema.getRiskCode());
        tLACommisionBSchema.setRiskVersion(pmLACommisionSchema.
                                           getRiskVersion());
        tLACommisionBSchema.setDutyCode(pmLACommisionSchema.getDutyCode());
        tLACommisionBSchema.setPayPlanCode(pmLACommisionSchema.
                                           getPayPlanCode());
        tLACommisionBSchema.setCValiDate(pmLACommisionSchema.getCValiDate());
        tLACommisionBSchema.setPayIntv(pmLACommisionSchema.getPayIntv());
        tLACommisionBSchema.setPayMode(pmLACommisionSchema.getPayMode());
        tLACommisionBSchema.setReceiptNo(pmLACommisionSchema.getReceiptNo());
        tLACommisionBSchema.setTPayDate(pmLACommisionSchema.getTPayDate());
        tLACommisionBSchema.setTEnterAccDate(pmLACommisionSchema.
                                             getTEnterAccDate());
        tLACommisionBSchema.setTConfDate(pmLACommisionSchema.getTConfDate());
        tLACommisionBSchema.setTMakeDate(pmLACommisionSchema.getTMakeDate());
        tLACommisionBSchema.setCommDate(pmLACommisionSchema.getCommDate());
        tLACommisionBSchema.setTransMoney(pmLACommisionSchema.getTransMoney());
        tLACommisionBSchema.setTransStandMoney(pmLACommisionSchema.
                                               getTransStandMoney());
        tLACommisionBSchema.setLastPayToDate(pmLACommisionSchema.
                                             getLastPayToDate());
        tLACommisionBSchema.setCurPayToDate(pmLACommisionSchema.
                                            getCurPayToDate());
        tLACommisionBSchema.setTransType(pmLACommisionSchema.getTransType());
        tLACommisionBSchema.setCommDire(pmLACommisionSchema.getCommDire());
        tLACommisionBSchema.setTransState(pmLACommisionSchema.getTransState());
        tLACommisionBSchema.setDirectWage(pmLACommisionSchema.getDirectWage());
        tLACommisionBSchema.setAppendWage(pmLACommisionSchema.getAppendWage());
        tLACommisionBSchema.setGrpFYC(pmLACommisionSchema.getGrpFYC());
        tLACommisionBSchema.setCalCount(pmLACommisionSchema.getCalCount());
        tLACommisionBSchema.setCalDate(pmLACommisionSchema.getCalDate());

        tLACommisionBSchema.setStandFYCRate(pmLACommisionSchema.
                                            getStandFYCRate());
        tLACommisionBSchema.setFYCRate(pmLACommisionSchema.getFYCRate());
        tLACommisionBSchema.setFYC(pmLACommisionSchema.getFYC());
        tLACommisionBSchema.setDepFYC(pmLACommisionSchema.getDepFYC());
        tLACommisionBSchema.setStandPrem(pmLACommisionSchema.getStandPrem());
        tLACommisionBSchema.setCommCharge(pmLACommisionSchema.getCommCharge());
        tLACommisionBSchema.setCommCharge1(pmLACommisionSchema.getCommCharge1());
        tLACommisionBSchema.setCommCharge2(pmLACommisionSchema.getCommCharge2());
        tLACommisionBSchema.setCommCharge3(pmLACommisionSchema.getCommCharge3());
        tLACommisionBSchema.setCommCharge4(pmLACommisionSchema.getCommCharge4());
        tLACommisionBSchema.setGrpFYCRate(pmLACommisionSchema.getGrpFYCRate());
        tLACommisionBSchema.setDepFYCRate(pmLACommisionSchema.getDepFYCRate());
        tLACommisionBSchema.setF1(pmLACommisionSchema.getF1());
        tLACommisionBSchema.setF2(pmLACommisionSchema.getF2());
        tLACommisionBSchema.setF3(pmLACommisionSchema.getF3());
        tLACommisionBSchema.setF4(pmLACommisionSchema.getF4());
        tLACommisionBSchema.setF5(pmLACommisionSchema.getF5());
        tLACommisionBSchema.setK1(pmLACommisionSchema.getK1());
        tLACommisionBSchema.setK2(pmLACommisionSchema.getK2());
        tLACommisionBSchema.setK3(pmLACommisionSchema.getK3());
        tLACommisionBSchema.setK4(pmLACommisionSchema.getK4());
        tLACommisionBSchema.setK5(pmLACommisionSchema.getK5());
        tLACommisionBSchema.setFlag(pmLACommisionSchema.getFlag());
        tLACommisionBSchema.setSignDate(pmLACommisionSchema.getSignDate());
        tLACommisionBSchema.setGetPolDate(pmLACommisionSchema.getGetPolDate());
        tLACommisionBSchema.setBranchType(pmLACommisionSchema.getBranchType());
        tLACommisionBSchema.setAgentCom(pmLACommisionSchema.getAgentCom());
        tLACommisionBSchema.setBankServer(pmLACommisionSchema.getBankServer());
        tLACommisionBSchema.setAgentType(pmLACommisionSchema.getAgentType());
        tLACommisionBSchema.setAgentCode(pmLACommisionSchema.getAgentCode());
        tLACommisionBSchema.setAgentGroup(pmLACommisionSchema.getAgentGroup());
        tLACommisionBSchema.setBranchCode(pmLACommisionSchema.getBranchCode());
        tLACommisionBSchema.setBranchSeries(pmLACommisionSchema.getBranchSeries());
        tLACommisionBSchema.setPolType(pmLACommisionSchema.getPolType());
        tLACommisionBSchema.setP1(pmLACommisionSchema.getP1());
        tLACommisionBSchema.setP2(pmLACommisionSchema.getP2());
        tLACommisionBSchema.setP3(pmLACommisionSchema.getP3());
        tLACommisionBSchema.setP4(pmLACommisionSchema.getP4());
        tLACommisionBSchema.setP5(pmLACommisionSchema.getP5());
        tLACommisionBSchema.setP6(pmLACommisionSchema.getP6());
        tLACommisionBSchema.setP7(pmLACommisionSchema.getP7());
        tLACommisionBSchema.setP8(pmLACommisionSchema.getP8());
        tLACommisionBSchema.setP9(pmLACommisionSchema.getP9());
        tLACommisionBSchema.setP10(pmLACommisionSchema.getP10());
        tLACommisionBSchema.setP11(pmLACommisionSchema.getP11());
        tLACommisionBSchema.setP12(pmLACommisionSchema.getP12());
        tLACommisionBSchema.setP13(pmLACommisionSchema.getP13());
        tLACommisionBSchema.setP14(pmLACommisionSchema.getP14());
        tLACommisionBSchema.setP15(pmLACommisionSchema.getP15());
        tLACommisionBSchema.setMakePolDate(pmLACommisionSchema.getMakePolDate());
        tLACommisionBSchema.setCustomGetPolDate(pmLACommisionSchema.getCustomGetPolDate());
        tLACommisionBSchema.setriskmark(pmLACommisionSchema.getriskmark());
        tLACommisionBSchema.setScanDate(pmLACommisionSchema.getScanDate());
        tLACommisionBSchema.setOperator(pmLACommisionSchema.getOperator());
        tLACommisionBSchema.setMakeDate(pmLACommisionSchema.getMakeDate());
        tLACommisionBSchema.setMakeTime(pmLACommisionSchema.getMakeTime());
        tLACommisionBSchema.setModifyDate(mCurrDate);
        tLACommisionBSchema.setModifyTime(mModifyTime);
        tLACommisionBSchema.setBranchType2(pmLACommisionSchema.
                                           getBranchType2());
        tLACommisionBSchema.setPayCount(pmLACommisionSchema.
                                        getPayCount());
        tLACommisionBSchema.setPayYear(pmLACommisionSchema.
                                       getPayYear());
        tLACommisionBSchema.setPayYears(pmLACommisionSchema.
                                        getPayYears());
        tLACommisionBSchema.setYears(pmLACommisionSchema.
                                     getYears());
        tLACommisionBSchema.setStandPremRate(
                pmLACommisionSchema.getStandPremRate());

        mLACommisionBSet.add(tLACommisionBSchema);
        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean
     */
    private boolean DataLACommission(LACommisionSchema pmLACommisionSchema,
                                     LAGrpCommisionDetailSchema
                                     mLAGrpCommisionDetailSchema,
                                     int rowCount) {

        String COMMISIONSN = "";
        String commEdor = "";

        //备份保单拆分
        //COMMISIONEDOR
        COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");

        LACommisionSchema tPerLACommisionSchema = new
                                                  LACommisionSchema();

        tPerLACommisionSchema = pmLACommisionSchema.getSchema();
        double tStandPrem = pmLACommisionSchema.getStandPrem();
        double tBusiRate = mLAGrpCommisionDetailSchema.getBusiRate();

        tPerLACommisionSchema.setAgentCode(mLAGrpCommisionDetailSchema.
                                           getAgentCode());
        tPerLACommisionSchema.setStandPrem(tStandPrem * tBusiRate);
        tPerLACommisionSchema.setTransMoney(tPerLACommisionSchema.
                                            getTransMoney() * tBusiRate);
        tPerLACommisionSchema.setTransStandMoney(tPerLACommisionSchema.
                                                 getTransStandMoney() *
                                                 tBusiRate);

        tPerLACommisionSchema.setDirectWage(tPerLACommisionSchema.
                                            getDirectWage() * tBusiRate);
        tPerLACommisionSchema.setP7(tPerLACommisionSchema.
                                    getP7() * tBusiRate);
        tPerLACommisionSchema.setFYC(tPerLACommisionSchema.getFYC() *
                                     tBusiRate);
        tPerLACommisionSchema.setModifyDate(mCurrDate);
        tPerLACommisionSchema.setModifyTime(mCurrTime);
        tPerLACommisionSchema.setOperator(mGlobalInput.Operator);

        if (nCnt == 1) {
            tPerLACommisionSchema.setK1(mLAGrpCommisionDetailSchema.
                                        getAppntRate());
        } else {
            tPerLACommisionSchema.setK1(0);
        }
        tPerLACommisionSchema.setCommisionSN(COMMISIONSN);
        mNewLACommisionSet.add(tPerLACommisionSchema);

        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean
     */
    private boolean DataLACommissionB(LACommisionBSchema pmLACommisionBSchema,
                                      LAGrpCommisionDetailSchema
                                      mLAGrpCommisionDetailSchema,
                                      int rowCount) {

        String COMMISIONSN = "";
        String commEdor = "";

        //备份保单拆分
        LACommisionSchema tLACommisionSchema = new LACommisionSchema();
        tLACommisionSchema.setCommisionSN(pmLACommisionBSchema.getCommisionSN());
        tLACommisionSchema.setCommisionBaseNo(pmLACommisionBSchema.
                                              getCommisionBaseNo());
        tLACommisionSchema.setWageNo(pmLACommisionBSchema.getWageNo());
        tLACommisionSchema.setGrpContNo(pmLACommisionBSchema.getGrpContNo());
        tLACommisionSchema.setGrpPolNo(pmLACommisionBSchema.getGrpPolNo());
        tLACommisionSchema.setContNo(pmLACommisionBSchema.getContNo());
        tLACommisionSchema.setPolNo(pmLACommisionBSchema.getPolNo());
        tLACommisionSchema.setMainPolNo(pmLACommisionBSchema.getMainPolNo());
        tLACommisionSchema.setManageCom(pmLACommisionBSchema.getManageCom());
        tLACommisionSchema.setAppntNo(pmLACommisionBSchema.getAppntNo());
        tLACommisionSchema.setRiskCode(pmLACommisionBSchema.getRiskCode());
        tLACommisionSchema.setRiskVersion(pmLACommisionBSchema.
                                          getRiskVersion());
        tLACommisionSchema.setDutyCode(pmLACommisionBSchema.getDutyCode());
        tLACommisionSchema.setPayPlanCode(pmLACommisionBSchema.
                                          getPayPlanCode());
        tLACommisionSchema.setCValiDate(pmLACommisionBSchema.getCValiDate());
        tLACommisionSchema.setPayIntv(pmLACommisionBSchema.getPayIntv());
        tLACommisionSchema.setPayMode(pmLACommisionBSchema.getPayMode());
        tLACommisionSchema.setReceiptNo(pmLACommisionBSchema.getReceiptNo());
        tLACommisionSchema.setTPayDate(pmLACommisionBSchema.getTPayDate());
        tLACommisionSchema.setTEnterAccDate(pmLACommisionBSchema.
                                            getTEnterAccDate());
        tLACommisionSchema.setTConfDate(pmLACommisionBSchema.getTConfDate());
        tLACommisionSchema.setTMakeDate(pmLACommisionBSchema.getTMakeDate());
        tLACommisionSchema.setCommDate(pmLACommisionBSchema.getCommDate());
        tLACommisionSchema.setTransMoney(pmLACommisionBSchema.getTransMoney());
        tLACommisionSchema.setTransStandMoney(pmLACommisionBSchema.
                                              getTransStandMoney());
        tLACommisionSchema.setLastPayToDate(pmLACommisionBSchema.
                                            getLastPayToDate());
        tLACommisionSchema.setCurPayToDate(pmLACommisionBSchema.
                                           getCurPayToDate());
        tLACommisionSchema.setTransType(pmLACommisionBSchema.getTransType());
        tLACommisionSchema.setCommDire(pmLACommisionBSchema.getCommDire());
        tLACommisionSchema.setTransState(pmLACommisionBSchema.getTransState());
        tLACommisionSchema.setDirectWage(pmLACommisionBSchema.getDirectWage());
        tLACommisionSchema.setAppendWage(pmLACommisionBSchema.getAppendWage());
        tLACommisionSchema.setGrpFYC(pmLACommisionBSchema.getGrpFYC());
        tLACommisionSchema.setStandFYCRate(pmLACommisionBSchema.
                                           getStandFYCRate());
        tLACommisionSchema.setFYCRate(pmLACommisionBSchema.getFYCRate());
        tLACommisionSchema.setFYC(pmLACommisionBSchema.getFYC());
        tLACommisionSchema.setDepFYC(pmLACommisionBSchema.getDepFYC());
        tLACommisionSchema.setStandPrem(pmLACommisionBSchema.getStandPrem());
        tLACommisionSchema.setCommCharge(pmLACommisionBSchema.getCommCharge());
        tLACommisionSchema.setCommCharge1(pmLACommisionBSchema.getCommCharge1());
        tLACommisionSchema.setCommCharge2(pmLACommisionBSchema.getCommCharge2());
        tLACommisionSchema.setCommCharge3(pmLACommisionBSchema.getCommCharge3());
        tLACommisionSchema.setCommCharge4(pmLACommisionBSchema.getCommCharge4());
        tLACommisionSchema.setGrpFYCRate(pmLACommisionBSchema.getGrpFYCRate());
        tLACommisionSchema.setDepFYCRate(pmLACommisionBSchema.getDepFYCRate());
        tLACommisionSchema.setF1(pmLACommisionBSchema.getF1());
        tLACommisionSchema.setF2(pmLACommisionBSchema.getF2());
        tLACommisionSchema.setF3(pmLACommisionBSchema.getF3());
        tLACommisionSchema.setF4(pmLACommisionBSchema.getF4());
        tLACommisionSchema.setF5(pmLACommisionBSchema.getF5());
        tLACommisionSchema.setK1(pmLACommisionBSchema.getK1());
        tLACommisionSchema.setK2(pmLACommisionBSchema.getK2());
        tLACommisionSchema.setK3(pmLACommisionBSchema.getK3());
        tLACommisionSchema.setK4(pmLACommisionBSchema.getK4());
        tLACommisionSchema.setK5(pmLACommisionBSchema.getK5());
        tLACommisionSchema.setFYCRate(pmLACommisionBSchema.getFYCRate());
        tLACommisionSchema.setSignDate(pmLACommisionBSchema.getSignDate());
        tLACommisionSchema.setBranchType(pmLACommisionBSchema.getBranchType());
        tLACommisionSchema.setAgentCom(pmLACommisionBSchema.getAgentCom());
        tLACommisionSchema.setBankServer(pmLACommisionBSchema.getBankServer());
        tLACommisionSchema.setAgentCode(pmLACommisionBSchema.getAgentCode());
        tLACommisionSchema.setAgentGroup(pmLACommisionBSchema.getAgentGroup());
        tLACommisionSchema.setBranchCode(pmLACommisionBSchema.getBranchCode());
        tLACommisionSchema.setBranchSeries(pmLACommisionBSchema.getBranchSeries());
        tLACommisionSchema.setPolType(pmLACommisionBSchema.getPolType());
        tLACommisionSchema.setP1(pmLACommisionBSchema.getP1());
        tLACommisionSchema.setP2(pmLACommisionBSchema.getP2());
        tLACommisionSchema.setP3(pmLACommisionBSchema.getP3());
        tLACommisionSchema.setP4(pmLACommisionBSchema.getP4());
        tLACommisionSchema.setP5(pmLACommisionBSchema.getP5());
        tLACommisionSchema.setP6(pmLACommisionBSchema.getP6());
        tLACommisionSchema.setP7(pmLACommisionBSchema.getP7());
        tLACommisionSchema.setP8(pmLACommisionBSchema.getP8());
        tLACommisionSchema.setP9(pmLACommisionBSchema.getP9());
        tLACommisionSchema.setP10(pmLACommisionBSchema.getP10());
        tLACommisionSchema.setP11(pmLACommisionBSchema.getP11());
        tLACommisionSchema.setP12(pmLACommisionBSchema.getP12());
        tLACommisionSchema.setP13(pmLACommisionBSchema.getP13());
        tLACommisionSchema.setP14(pmLACommisionBSchema.getP14());
        tLACommisionSchema.setP15(pmLACommisionBSchema.getP15());
        tLACommisionSchema.setMakePolDate(pmLACommisionBSchema.getMakePolDate());
        tLACommisionSchema.setCustomGetPolDate(pmLACommisionBSchema.getCustomGetPolDate());
        tLACommisionSchema.setriskmark(pmLACommisionBSchema.getriskmark());
        tLACommisionSchema.setScanDate(pmLACommisionBSchema.getScanDate());
        tLACommisionSchema.setBranchAttr(pmLACommisionBSchema.getBranchAttr());
        tLACommisionSchema.setP7(pmLACommisionBSchema.getP7());
        tLACommisionSchema.setOperator(pmLACommisionBSchema.getOperator());
        tLACommisionSchema.setMakeDate(pmLACommisionBSchema.getMakeDate());
        tLACommisionSchema.setMakeTime(pmLACommisionBSchema.getMakeTime());
        tLACommisionSchema.setModifyDate(mCurrDate);
        tLACommisionSchema.setModifyTime(mModifyTime);
        tLACommisionSchema.setBranchType2(pmLACommisionBSchema.
                                          getBranchType2());
        StringBuffer tSb = new StringBuffer();
        tSb.append(
                " SELECT a.agentgroup,a.branchcode,b.branchattr,b.branchseries FROM LAAGENT a ,LABRANCHGROUP b WHERE a.AGENTGROUP = b.AGENTGROUP")
                .append(" and a.agentcode ='")
                .append(mLAGrpCommisionDetailSchema.getAgentCode())
                .append("'");
        String tSql = tSb.toString();
        System.out.println(tSql);
        ExeSQL exeSQL = new ExeSQL();
        SSRS ssrs = exeSQL.execSQL(tSql);
        //查询是否进行了保单拆分
        if (exeSQL.mErrors.needDealError()) {
            mErrors.copyAllErrors(exeSQL.mErrors);
            return false;
        }
        tLACommisionSchema.setAgentGroup(ssrs.GetText(1, 1));
        tLACommisionSchema.setBranchCode(ssrs.GetText(1, 2));
        tLACommisionSchema.setBranchAttr(ssrs.GetText(1, 3));
        tLACommisionSchema.setBranchSeries(ssrs.GetText(1, 4));

        //COMMISIONEDOR
        COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");

        LACommisionSchema tPerLACommisionSchema = new
                                                  LACommisionSchema();

        tPerLACommisionSchema = tLACommisionSchema.getSchema();
        double tStandPrem = tLACommisionSchema.getStandPrem();
        double tBusiRate = mLAGrpCommisionDetailSchema.getBusiRate();

        tPerLACommisionSchema.setAgentCode(mLAGrpCommisionDetailSchema.
                                           getAgentCode());
        tPerLACommisionSchema.setStandPrem(tStandPrem * tBusiRate);
        tPerLACommisionSchema.setTransMoney(tPerLACommisionSchema.
                                            getTransMoney() * tBusiRate);
        tPerLACommisionSchema.setTransStandMoney(tPerLACommisionSchema.
                                                 getTransStandMoney() *
                                                 tBusiRate);
        tPerLACommisionSchema.setDirectWage(tPerLACommisionSchema.
                                            getDirectWage() * tBusiRate);
        tPerLACommisionSchema.setP7(tPerLACommisionSchema.
                                    getP7() * tBusiRate);
        tPerLACommisionSchema.setFYC(tPerLACommisionSchema.getFYC() *
                                     tBusiRate);
        if (mCnt == 1) {
            tPerLACommisionSchema.setK1(mLAGrpCommisionDetailSchema.
                                        getAppntRate());
        } else {
            tPerLACommisionSchema.setK1(0);
        }
        tPerLACommisionSchema.setCommisionSN(COMMISIONSN);
        mNewLACommisionSet.add(tPerLACommisionSchema);

        return true;
    }


    /**
     * 进行有关验证
     *    1、验证人员是否重复
     *    2、验证业务比例之和必须等于 100%
     *    3、参加分配保费的最多只能是4个人
     *    4、参加分配客户数的最多只能是2个人
     * @return boolean
     */
    private boolean check() {
        String wageLogSQL = "select * from lawagelog where managecom='" +
                            mLACommisionchema.getManageCom() + "'";
        if (mLACommisionchema.getBranchType() != null &&
            !mLACommisionchema.getBranchType().equals("")) {
            wageLogSQL += " and BranchType='" +
                    mLACommisionchema.getBranchType() +
                    "'";
        }
        if (mLACommisionchema.getBranchType2() != null &&
            !mLACommisionchema.getBranchType2().equals("")) {
            wageLogSQL += " and BranchType2='" +
                    mLACommisionchema.getBranchType2() + "'";
        }
        LAWageLogDB tLAWageLogDB = new LAWageLogDB();
        LAWageLogSet tLAWageLogSet = new LAWageLogSet();
        tLAWageLogSet = tLAWageLogDB.executeQuery(wageLogSQL);
        System.out.println(wageLogSQL);
        if (tLAWageLogSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoNewBL";
            tError.functionName = "dealData";
            tError.errorMessage = "机构" + mLACommisionchema.getManageCom() +
                                  "的数据未提取，不能进行计算!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode = "mak001";
        tG.ManageCom = "8611";

        String tOperate = "INSERT||MAIN"; // 操作类型
//        String tGrpContNo = "0000204601"; // 团体保单号
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        tLAWageLogSchema.setManageCom("86110000");

        tLAWageLogSchema.setStartDate("2007-07-06");
        tLAWageLogSchema.setEndDate("2007-07-06");
        tLAWageLogSchema.setBranchType("2");
        tLAWageLogSchema.setBranchType2("01");

        VData tVData = new VData();
        tVData.add(tLAWageLogSchema);
        tVData.add(tG);

        SplitLACommisionCont tSplitLACommisionCont = new
                SplitLACommisionCont();
        if (tSplitLACommisionCont.submitData(tVData, tOperate)) {
            System.out.println("成功了！");
        } else {
            System.out.println("失败了！");
        }
    }

    private void jbInit() throws Exception {
    }

}
