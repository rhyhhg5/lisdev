package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 续期孤儿单不计算薪资</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p> 
 *
 * @author caigang
 * @version 1.0
 */
public class NoCalWageRenewalData {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private String mManageCom;
    private String mWageNo;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    private LACommisionSet mLAUpdateCommisionSet = new LACommisionSet();

    private MMap mMap = new MMap();

   
    //提交数据
    public boolean submitData(String cManageCom,String cWageNo) {
     
    	this.mManageCom = cManageCom;
    	this.mWageNo = cWageNo;
        if (!dealData()) {
        	 CError tError = new CError();
             tError.moduleName = "RgtSurveyBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据处理失败RgtSurveyBL-->dealData!";
             this.mErrors.addOneError(tError);
             return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    

    //提交数据
    private boolean prepareOutputData() {
    	
        mMap.put(mLAUpdateCommisionSet, "UPDATE");
        this.mInputData.add(mMap);
        return true;
    }
    
    
    private boolean dealData() 
    {
    	String tAgentSQL ="select a.agentcode from " +
    			" (select agentcode,sum(fyc) fyc from lacommision where branchtype='1' and branchtype2='01' and payyear>=1 and renewcount =0 " +
    			" and managecom ='"+mManageCom+"' and wageno ='"+mWageNo+"' group by agentcode ) a ,"+
    			" (select agentcode ,f05 from lawage where branchtype='1' and branchtype2='01' and state ='1' and managecom ='"+mManageCom+"' and indexcalno ='"+mWageNo+"') b"+
    			" where a.agentcode = b.agentcode and a.fyc-b.f05<>0";
        String  sql =" select CommisionSN,(case when grpcontno ='00000000000000000000' then contno else grpcontno end ),tmakedate from lacommision " 
        			+" where branchtype='1' and branchtype2='01' and payyear>=1 and renewcount =0   " 
                    +" and  managecom ='"+mManageCom+"' and wageno ='"+mWageNo+"'" 
                    +" and agentcode in ("+tAgentSQL+")"
                    +" with ur";

            System.out.println("查询个险孤儿单："+sql);
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) 
            {
            	String tCommisionSN = tSSRS.GetText(i, 1);
            	String tContNo = tSSRS.GetText(i, 2);
            	String tTMakeDate = tSSRS.GetText(i, 3);
            	String tSQL ="select 1 from LAOrphanPolicy d where d.contno='"+tContNo+"' and d.calflag = 'N' and  d.caldate<='"+tTMakeDate+"'" +
            			" union " +
            			"select 1 from LAOrphanPolicyB c where c.contno='"+tContNo+"' and c.calflag = 'N' and c.caldate<='"+tTMakeDate+"'";
            	SSRS tFlag = new SSRS(); 
            	tFlag = tExeSQL.execSQL(tSQL);
            	if(tFlag.getMaxRow()<=0)
            	{
            		continue;
            	}
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                LACommisionDB tLACommisionDB = new LACommisionDB();
                tLACommisionDB.setCommisionSN(tCommisionSN);
                tLACommisionDB.getInfo();
                tLACommisionSchema = tLACommisionDB.getSchema();
                //续期孤儿单不计算薪资
                //修改为3，是为了财务接口提数时，不提取（财务接口提取直接佣金时，只提取commdire=‘1’ 的数据）
                if("1".equals(tLACommisionSchema.getCommDire()))
                {
                	tLACommisionSchema.setCommDire("3");
                }
                tLACommisionSchema.setOperator("ged");
                //对于机构调整的数据，进行操作人修改
                if("jgtz".equals(tLACommisionSchema.getOperator()))
                {
                	tLACommisionSchema.setOperator("gedt");
                }
                tLACommisionSchema.setModifyDate(this.CurrentDate);
                tLACommisionSchema.setModifyTime(this.CurrentTime);
                this.mLAUpdateCommisionSet.add(tLACommisionSchema);
           }
//            for(int k=1;k<=mLAUpdateCommisionSet.size();k++)
//            {
//            	System.out.println("'"+mLAUpdateCommisionSet.get(k).getCommisionSN()+"',");
//            }
            return true;
       }  

   
     public static  void main(String[] args)
     {
     }
}
