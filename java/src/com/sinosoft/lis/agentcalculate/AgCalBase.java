/*
 * <p>ClassName: AgCalBase </p>
 * <p>Description: 计算基础要素类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2003-02-12
 * @modify: WuHao 2007-03
 */
package com.sinosoft.lis.agentcalculate;

import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;

public class AgCalBase
{
    // @Field
    /** 佣金计算编码 */
    private String WageNo;
    /** 管理机构 */
    private String ManageCom;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人展业机构*/
    private String AgentGroup;
    /** 代理人组别*/
    private String BranchCode;
    /** 佣金编码 */
    private String WageCode;
    /** 代理人职级 */
    private String AgentGrade;
    /** 考核的目标职级 */
    private String DestAgentGrade;
    /** 考核期限 */
    private String LimitPeriod; //两位
    /** 月起期 */
    private Date MonthBegin;
    /** 月止期 */
    private Date MonthEnd;
    /** 季起期 */
    private Date QuauterBegin;
    /** 季止期 */
    private Date QuauterEnd;
    /** 半年起期 */
    private Date HalfYearBegin;
    /** 半年止期 */
    private Date HalfYearEnd;
    /** 年起期 */
    private Date YearBegin;
    /** 年止期 */
    private Date YearEnd;
    /** 月标记 */
    private String MonthMark = "0";
    /** 季标记 */
    private String QuauterMark = "0";
    /** 半年标记 */
    private String HalfYearMark = "0";
    /** 年标记 */
    private String YearMark = "0";
    /** 任意时间段起期 */
    private Date TempBegin;
    /** 任意时间段止期 */
    private Date TempEnd;
    /** 考核比例值 */
    private String Rate = "1";
    /** 是否考核标记 */
    /* 0: 维持晋升都不考核 1：只考核晋升 2：两者都考核 3:只考核维持*/
    public int AssessMark = 0;
    /** 判断理财服务专员降级标志 */
    private String A1State;
    /** 考核类型 */
    private String AssessType;
    /** 地区类型 */
    private String AreaType;
    /** 考核起止期的间隔 */
    public int AssessInv = 0;
    /** 第一次考核的标记 */
    public int FirstAssessMark = 0;
    /** AgentGroup的显式代吗 */
    private String BranchAttr;
    /** 渠道类型 */
    private String ChannelType;
    /** 所在直辖组的外部编码 */
    private String ZSGroupBranchAttr;
    /** 所在直辖组的BranchSeries */
    /**代理人展业机构序列号*/
    private String ZSGroupBranchSeries;
    /**BranchType */
    private String BranchType;
    /**BranchType2 */
    private String BranchType2;
    /** 任意计算年月起期 */
    private String WageNoBegin;
    /** 任意计算年月止期 */
    private String WageNoEnd;
    /** 任意薪资班版本 */
    private String WageVersion;
    /** 团队建设标志 */
    private String GbuildFlag;
    /** 团队建设起期 */
    private String GbuildStartDate;    
    // @Constructor
    public AgCalBase()
    {}

    // @Method
    public String getWageNo()
    {
        return WageNo;
    }

    public void setWageNo(String cWageNo)
    {
        WageNo = cWageNo;
    }

    public String getManageCom()
    {
        return ManageCom;
    }

    public void setManageCom(String cManageCom)
    {
        ManageCom = cManageCom;
    }

    public String getAgentCode()
    {
        return AgentCode;
    }

    public void setAgentCode(String cAgentCode)
    {
        AgentCode = cAgentCode;
    }

    public String getAgentGroup()
    {
        return AgentGroup;
    }

    public String getBranchCode()
    {
        return BranchCode;
    }

    public void setBranchAttr(String cBranchAttr)
    {
        BranchAttr = cBranchAttr;
    }

    public String getBranchAttr()
    {
        return BranchAttr;
    }

    public void setChannelType(String cChannelType)
    {
        ChannelType = cChannelType;
    }

    public String getChannelType()
    {
        return ChannelType;
    }

    public void setAgentGroup(String cAgentGroup)
    {
        AgentGroup = cAgentGroup;
    }

    public void setBranchCode(String cBranchCode)
    {
        BranchCode = cBranchCode;
    }

    public String getWageCode()
    {
        return WageCode;
    }

    public void setWageCode(String cWageCode)
    {
        WageCode = cWageCode;
    }

    public String getAgentGrade()
    {
        return AgentGrade;
    }

    public void setAgentGrade(String cAgentGrade)
    {
        AgentGrade = cAgentGrade;
    }

    public String getDestAgentGrade()
    {
        return DestAgentGrade;
    }

    public void setDestAgentGrade(String cDestAgentGrade)
    {
        DestAgentGrade = cDestAgentGrade;
    }

    public String getA1State()
    {
        return A1State;
    }

    public void setA1State(String cA1State)
    {
        A1State = cA1State;
    }

    public String getLimitPeriod()
    {
        return LimitPeriod;
    }

    public void setLimitPeriod(String cLimitPeriod)
    {
        LimitPeriod = cLimitPeriod;
    }

    public String getMonthBegin()
    {
        if (MonthBegin != null)
        {
            return fDate.getString(MonthBegin);
        }
        else
        {
            return null;
        }
    }

    public void setMonthBegin(Date cMonthBegin)
    {
        MonthBegin = cMonthBegin;
    }

    public void setMonthBegin(String cMonthBegin)
    {
        if (cMonthBegin != null && !cMonthBegin.equals(""))
        {
            MonthBegin = fDate.getDate(cMonthBegin);
        }
        else
        {
            MonthBegin = null;
        }
    }

    public String getMonthEnd()
    {
        if (MonthEnd != null)
        {
            return fDate.getString(MonthEnd);
        }
        else
        {
            return null;
        }
    }

    public void setMonthEnd(Date cMonthEnd)
    {
        MonthEnd = cMonthEnd;
    }

    public void setMonthEnd(String cMonthEnd)
    {
        if (cMonthEnd != null && !cMonthEnd.equals(""))
        {
            MonthEnd = fDate.getDate(cMonthEnd);
        }
        else
        {
            MonthEnd = null;
        }
    }

    public String getQuauterBegin()
    {
        if (QuauterBegin != null)
        {
            return fDate.getString(QuauterBegin);
        }
        else
        {
            return null;
        }
    }

    public void setQuauterBegin(Date cQuauterBegin)
    {
        QuauterBegin = cQuauterBegin;
    }

    public void setQuauterBegin(String cQuauterBegin)
    {
        if (cQuauterBegin != null && !cQuauterBegin.equals(""))
        {
            QuauterBegin = fDate.getDate(cQuauterBegin);
        }
        else
        {
            QuauterBegin = null;
        }
    }

    public String getQuauterEnd()
    {
        if (QuauterEnd != null)
        {
            return fDate.getString(QuauterEnd);
        }
        else
        {
            return null;
        }
    }

    public void setQuauterEnd(Date cQuauterEnd)
    {
        MonthEnd = cQuauterEnd;
    }

    public void setQuauterEnd(String cQuauterEnd)
    {
        if (cQuauterEnd != null && !cQuauterEnd.equals(""))
        {
            QuauterEnd = fDate.getDate(cQuauterEnd);
        }
        else
        {
            QuauterEnd = null;
        }
    }

    public String getHalfYearBegin()
    {
        if (HalfYearBegin != null)
        {
            return fDate.getString(HalfYearBegin);
        }
        else
        {
            return null;
        }
    }

    public void setHalfYearBegin(Date cHalfYearBegin)
    {
        HalfYearBegin = cHalfYearBegin;
    }

    public void setHalfYearBegin(String cHalfYearBegin)
    {
        if (cHalfYearBegin != null && !cHalfYearBegin.equals(""))
        {
            HalfYearBegin = fDate.getDate(cHalfYearBegin);
        }
        else
        {
            HalfYearBegin = null;
        }
    }

    public String getHalfYearEnd()
    {
        if (HalfYearEnd != null)
        {
            return fDate.getString(HalfYearEnd);
        }
        else
        {
            return null;
        }
    }

    public void setHalfYearEnd(Date cHalfYearEnd)
    {
        HalfYearEnd = cHalfYearEnd;
    }

    public void setHalfYearEnd(String cHalfYearEnd)
    {
        if (cHalfYearEnd != null && !cHalfYearEnd.equals(""))
        {
            HalfYearEnd = fDate.getDate(cHalfYearEnd);
        }
        else
        {
            HalfYearEnd = null;
        }
    }

    public String getYearBegin()
    {
        if (YearBegin != null)
        {
            return fDate.getString(YearBegin);
        }
        else
        {
            return null;
        }
    }

    public void setYearBegin(Date cYearBegin)
    {
        YearBegin = cYearBegin;
    }

    public void setYearBegin(String cYearBegin)
    {
        if (cYearBegin != null && !cYearBegin.equals(""))
        {
            YearBegin = fDate.getDate(cYearBegin);
        }
        else
        {
            YearBegin = null;
        }
    }

    public String getYearEnd()
    {
        if (YearEnd != null)
        {
            return fDate.getString(YearEnd);
        }
        else
        {
            return null;
        }
    }

    public void setYearEnd(Date cYearEnd)
    {
        YearEnd = cYearEnd;
    }

    public void setYearEnd(String cYearEnd)
    {
        if (cYearEnd != null && !cYearEnd.equals(""))
        {
            YearEnd = fDate.getDate(cYearEnd);
        }
        else
        {
            YearEnd = null;
        }
    }

    public String getMonthMark()
    {
        return MonthMark;
    }

    public void setMonthMark(String cMonthMark)
    {
        MonthMark = cMonthMark;
    }

    public String getQuauterMark()
    {
        return QuauterMark;
    }

    public void setQuauterMark(String cQuauterMark)
    {
        QuauterMark = cQuauterMark;
    }

    public String getHalfYearMark()
    {
        return HalfYearMark;
    }

    public void setHalfYearMark(String cHalfYearMark)
    {
        HalfYearMark = cHalfYearMark;
    }

    public String getYearMark()
    {
        return YearMark;
    }

    public void setYearMark(String cYearMark)
    {
        YearMark = cYearMark;
    }

    public String getTempBegin()
    {
        if (TempBegin != null)
        {
            return fDate.getString(TempBegin);
        }
        else
        {
            return null;
        }
    }

    public void setTempBegin(Date cTempBegin)
    {
        TempBegin = cTempBegin;
    }

    public void setTempBegin(String cTempBegin)
    {
        if (cTempBegin != null && !cTempBegin.equals(""))
        {
            TempBegin = fDate.getDate(cTempBegin);
        }
        else
        {
            TempBegin = null;
        }
    }

    public String getTempEnd()
    {
        if (TempEnd != null)
        {
            return fDate.getString(TempEnd);
        }
        else
        {
            return null;
        }
    }

    public void setTempEnd(Date cTempEnd)
    {
        TempEnd = cTempEnd;
    }

    public void setTempEnd(String cTempEnd)
    {
        if (cTempEnd != null && !cTempEnd.equals(""))
        {
            TempEnd = fDate.getDate(cTempEnd);
        }
        else
        {
            TempEnd = null;
        }
    }

    public String getWageNoBegin()
    {
        return WageNoBegin;
    }

    public void setWageNoBegin(String cWageNoBegin)
    {
        WageNoBegin = cWageNoBegin;
    }

    public String getWageNoEnd()
    {
        return WageNoEnd;
    }

    public void setWageNoEnd(String cWageNoEnd)
    {
        WageNoEnd = cWageNoEnd;
    }

    public String getRate()
    {
        return Rate;
    }

    public void setRate(String cRate)
    {
        Rate = cRate;
    }

    public String getAssessType()
    {
        return AssessType;
    }

    public void setAssessType(String cAssessType)
    {
        AssessType = cAssessType;
    }

    public String getAreaType()
    {
        return AreaType;
    }

    public void setAreaType(String cAreaType)
    {
        AreaType = cAreaType;
    }

    public String getZSGroupBranchAttr()
    {
        return ZSGroupBranchAttr;
    }

    public void setZSGroupBranchAttr(String cZSGroupBranchAttr)
    {
        ZSGroupBranchAttr = cZSGroupBranchAttr;
    }

    public String getZSGroupBranchSeries()
   {
       return ZSGroupBranchSeries;
   }

   public void setZSGroupBranchSeries(String cZSGroupBranchSeries)
   {
       ZSGroupBranchSeries = cZSGroupBranchSeries;
   }
   public String getBranchType()
  {
      return BranchType;
  }

  public void setBranchType(String cBranchType)
  {
      BranchType = cBranchType;
  }
  public String getBranchType2()
  {
      return BranchType2;
  }

  public void setBranchType2(String cBranchType2)
  {
      BranchType2 = cBranchType2;
  }

  public String getWageVersion()
   {
       return WageVersion;
   }

   public void setWageVersion(String cWageVersion)
   {
       WageVersion = cWageVersion;
   }
   // add new 
   public String getGbuildFlag()
   {
       return GbuildFlag;
   }

   public void setGbuildFlag(String cGbuildFlag)
   {
	   GbuildFlag = cGbuildFlag;
   }
   public String getGbuildStartDate()
   {
       return GbuildStartDate;
   }

   public void setGbuildStartDate(String cGbuildStartDate)
   {
	   GbuildStartDate = cGbuildStartDate;
   }

    private FDate fDate = new FDate(); // 处理日期
}
