package com.sinosoft.lis.agentcalculate;


import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.Reflections;


public class LAAMSpecialRateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate;
    private String currentTime;
    /** 业务处理相关变量 */
    private LARateChargeSchema mLARateChargeSchema = new LARateChargeSchema();
    private LARateChargeSet mLARateChargeSet = new LARateChargeSet();
    private LARateChargeSet mupLARateChargeSet = new LARateChargeSet();
    private LARateChargeSet mdelLARateChargeSet = new LARateChargeSet();
    private LARateChargeSet mInsLARateChargeSet= new LARateChargeSet();
    private LAChargeSet mLAChargeSet=new LAChargeSet();
    private LAChargeBSet mLAChargeBSet=new LAChargeBSet();
    private LARateChargeBSet mInsLARateChargeBSet= new LARateChargeBSet();
    private MMap mMap = new MMap();
//private LAArchieveSet mLAArchieveSet=new LAArchieveSet();
    public LAAMSpecialRateBL()
    {
    }

    public static void main(String[] args)
    {
        LAConRateSchema tLAConRateSchema = new LAConRateSchema();
        LAConRateSet tupLAConRateSet = new LAConRateSet();
        tLAConRateSchema.setGrpContNo("0000031701");
        tLAConRateSchema.setGrpPolNo("0000031701");
        tLAConRateSchema.setRiskCode("1605");
        tLAConRateSchema.setRate("0.8");
        tLAConRateSchema.setBranchType("03");
        tupLAConRateSet.add(tLAConRateSchema);
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        VData tVData = new VData();
        tVData.addElement(tG);

        tVData.addElement(tupLAConRateSet);
        

    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
//       System.out.println("transact"+transact);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContRateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContRateBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LAAMSpecialRateBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LAAMSpecialRateBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAMSpecialRateBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        currentDate = PubFun.getCurrentDate();
        currentTime = PubFun.getCurrentTime();
        System.out.print(mOperate+"|||||||||||||||||12122");
        if (this.mOperate.equals("INSERT||MAIN") )
        {

            if (!saveData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (!upDateData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if(!deleteData())
            {
                return false;
            }
        }

        return true;
    }
    private boolean saveData()
    {
    	if(!updateLACharge()){
    		CError tError = new CError();
            tError.moduleName = "LAContRateBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "更新手续费表信息失败!";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	if(!delQiYueRate()){
    		CError tError = new CError();
            tError.moduleName = "LAContRateBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "备份契约手续费设置信息失败!";
            this.mErrors.addOneError(tError);
            return false;
    	}
        for (int i = 1; i <= mLARateChargeSet.size(); i++)
        {
            LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
            tLARateChargeSchema = mLARateChargeSet.get(i);
            String tGrpContNo=tLARateChargeSchema.getOtherNo();
            String tRiskCode=tLARateChargeSchema.getRiskCode();
            LARateChargeDB tLARateChargeDB=new LARateChargeDB();
            tLARateChargeDB.setRiskCode(tRiskCode);
            tLARateChargeDB.setOtherNo(tGrpContNo);
            if(tLARateChargeDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAContRateBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "保单号:"+tGrpContNo+ "险种号:"+tRiskCode+" 的信息已存在，如要修改它的信息，请先查询再修改!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLARateChargeSchema.setMakeDate(currentDate);
            tLARateChargeSchema.setMakeTime(currentTime);
            tLARateChargeSchema.setModifyDate(currentDate);
            tLARateChargeSchema.setModifyTime(currentTime);
            tLARateChargeSchema.setOperator(mGlobalInput.Operator);
            mInsLARateChargeSet.add(tLARateChargeSchema) ;
         }
         mMap.put(this.mInsLARateChargeSet, "INSERT");
         return true;
    }
    private boolean upDateData()
    {
    	if(!updateLACharge()){
    		CError tError = new CError();
            tError.moduleName = "LAContRateBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "更新手续费表信息失败!";
            this.mErrors.addOneError(tError);
            return false;
    	}
        String tEdorNo = PubFun1.CreateMaxNo("ContEdorNo", 20);
        for (int i = 1; i <= mLARateChargeSet.size(); i++)
        {
            LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
            tLARateChargeSchema = mLARateChargeSet.get(i);
            System.out.println("...............getOtherNo"+tLARateChargeSchema.getOtherNo());
            String tGrpContNo=tLARateChargeSchema.getOtherNo();
            String tRiskCode=tLARateChargeSchema.getRiskCode();
            LARateChargeDB tLARateChargeDB=new LARateChargeDB();
            tLARateChargeDB.setOtherNo(tGrpContNo);
            tLARateChargeDB.setRiskCode(tRiskCode);
            tLARateChargeDB.setOtherNoType("0");
            tLARateChargeDB.setStartDate("0000-00-00");
            tLARateChargeDB.setEndDate("0000-00-00");
            tLARateChargeDB.setStartYear("0");
            tLARateChargeDB.setEndYear("999");
            tLARateChargeDB.setCalType("51");
            if(!tLARateChargeDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAContRateBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "保单号:"+tGrpContNo+ "险种号:"+tRiskCode+" 的信息不存在,不能修改!";
                this.mErrors.addOneError(tError);
                return false;
            }
             /*交验是否已经计算过手续费  此处现在还没有
              *
              *
            */
            tLARateChargeSchema.setMakeDate(tLARateChargeDB.getMakeDate());
            tLARateChargeSchema.setMakeTime(tLARateChargeDB.getMakeTime());
            tLARateChargeSchema.setModifyDate(currentDate);
            tLARateChargeSchema.setModifyTime(currentTime);
            tLARateChargeSchema.setOperator(mGlobalInput.Operator);
            mupLARateChargeSet.add(tLARateChargeSchema) ;
            //备份
            LARateChargeBSchema tLARateChargeBSchema = new LARateChargeBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLARateChargeBSchema, tLARateChargeDB.getSchema());
            tLARateChargeBSchema.setEdorNo(PubFun1.CreateMaxNo("ContEdorNo", 20));
            tLARateChargeBSchema.setEdorType("22");
            mInsLARateChargeBSet.add(tLARateChargeBSchema);
         }
         mMap.put(this.mupLARateChargeSet, "UPDATE");
         mMap.put(this.mInsLARateChargeBSet, "INSERT");
         return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        String tEdorNo = PubFun1.CreateMaxNo("ContEdorNo", 20);
        //判断 该保单是否已计算佣金
        for(int i=1;i<=mLARateChargeSet.size();i++)
        {
            LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
            tLARateChargeSchema = mLARateChargeSet.get(i);
            String tGrpContNo=tLARateChargeSchema.getOtherNo();
            String tRiskCode=tLARateChargeSchema.getRiskCode();
            LARateChargeDB tLARateChargeDB=new LARateChargeDB();
            tLARateChargeDB.setOtherNo(tLARateChargeSchema.getOtherNo());
            tLARateChargeDB.setRiskCode(tRiskCode);
            tLARateChargeDB.setOtherNoType("0");
            tLARateChargeDB.setStartDate("0000-00-00");
            tLARateChargeDB.setEndDate("0000-00-00");
            tLARateChargeDB.setStartYear("0");
            tLARateChargeDB.setEndYear("999");
            tLARateChargeDB.setCalType("51");
           if(!tLARateChargeDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAContRateBL";
                tError.functionName = "dealdata";
                tError.errorMessage ="保单号:"+tGrpContNo+ "险种号:"+tRiskCode+" 的信息不存在,不能修改!";
                this.mErrors.addOneError(tError);
                return false;
            }
            /*交验是否已经计算过手续费  此处现在还没有
             *
             *
            */
            mdelLARateChargeSet.add(tLARateChargeSchema) ;
            //备份
            LARateChargeBSchema tLARateChargeBSchema = new LARateChargeBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLARateChargeBSchema, tLARateChargeDB.getSchema());
            tLARateChargeBSchema.setEdorNo(PubFun1.CreateMaxNo("ContEdorNo", 20));
            tLARateChargeBSchema.setEdorType("22");
            mInsLARateChargeBSet.add(tLARateChargeBSchema);
        }
        // 备份数据
        mMap.put(this.mInsLARateChargeBSet, "INSERT");
        mMap.put(this.mdelLARateChargeSet, "DELETE");
        return true;
    }
    public boolean updateLACharge(){
    	LAChargeSet tLAChargeSet;
    	LAChargeSchema tLAChargeSchema;
    	LAChargeBSchema tLAChargeBSchema;
    	String tEdorNo ;
    	Reflections tReflections = new Reflections();
    	for(int i=1;i<=mLARateChargeSet.size();i++){
    		tLAChargeSet=new LAChargeSet();
    		String tSQL ="select * from lacharge where contno='"+mLARateChargeSet.get(i).getOtherNo()
    					+"' and riskcode='"+mLARateChargeSet.get(i).getRiskCode()+"' and chargestate='0' with ur";
    		ExeSQL tExeSQL=new ExeSQL();
    		LAChargeDB tLAChargeDB=new LAChargeDB();
    		tLAChargeSet=tLAChargeDB.executeQuery(tSQL);
    		for(int j=1;j<=tLAChargeSet.size();j++){
    			tEdorNo = PubFun1.CreateMaxNo("ContEdorNo", 20);
    			tLAChargeBSchema=new LAChargeBSchema();
    			tReflections.transFields(tLAChargeBSchema, tLAChargeSet.get(j));
    			tLAChargeBSchema.setEdorNo(tEdorNo);
    			tLAChargeBSchema.setEdorType("33");
    			this.mLAChargeBSet.add(tLAChargeBSchema);
    			tLAChargeSchema=new LAChargeSchema();
    			tReflections.transFields(tLAChargeSchema, tLAChargeSet.get(j));
    			tLAChargeSchema.setChargeRate(mLARateChargeSet.get(i).getRate());
    			tLAChargeSchema.setCharge(mLARateChargeSet.get(i).getRate()*tLAChargeSchema.getTransMoney());
    			tLAChargeSchema.setModifyDate(this.currentDate);
    			tLAChargeSchema.setModifyTime(this.currentTime);
    			this.mLAChargeSet.add(tLAChargeSchema);
    		}
    	}
    	mMap.put(this.mLAChargeSet, "UPDATE");
        mMap.put(this.mLAChargeBSet, "INSERT");
    	return true;
    }
    public boolean delQiYueRate(){
    	LARateChargeSet tLARateChargeSet=new LARateChargeSet();
    	LARateChargeBSchema tLARateChargeBSchema;
    	String tEdorNo ;
    	Reflections tReflections = new Reflections();
    		String tSQL ="select * from laratecharge where otherno  in (select prtno from lccont where contno='"+mLARateChargeSet.get(1).getOtherNo()
    					+"' or grpcontno='"+mLARateChargeSet.get(1).getOtherNo()+"') with ur";
    		ExeSQL tExeSQL=new ExeSQL();
    		LARateChargeDB tLARateChargeDB=new LARateChargeDB();
    		tLARateChargeSet=tLARateChargeDB.executeQuery(tSQL);
    		for(int j=1;j<=tLARateChargeSet.size();j++){
    			tEdorNo = PubFun1.CreateMaxNo("ContEdorNo", 20);
    			tLARateChargeBSchema=new LARateChargeBSchema();
    			tReflections.transFields(tLARateChargeBSchema, tLARateChargeSet.get(j));
    			tLARateChargeBSchema.setEdorNo(tEdorNo);
    			tLARateChargeBSchema.setEdorType("33");
    			this.mInsLARateChargeBSet.add(tLARateChargeBSchema);
    			this.mdelLARateChargeSet.add(tLARateChargeSet.get(j));
    		}
    	mMap.put(this.mdelLARateChargeSet, "DELETE");
        mMap.put(this.mInsLARateChargeBSet, "INSERT");
    	return true;
    }
    public VData getResult()
    {
        this.mResult.clear();
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLARateChargeSet.set((LARateChargeSet) cInputData.
                                getObjectByObjectName("LARateChargeSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LARateChargeDB tLARateChargeDB = new LARateChargeDB();
        int i;
        for (i = 0; i < mLARateChargeSet.size(); i++)
        {

        	tLARateChargeDB.setSchema(this.mLARateChargeSet.get(i));
            //如果有需要处理的错误，则返回
            if (tLARateChargeDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLARateChargeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAConRateDB";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData = null;
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContRateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    }

