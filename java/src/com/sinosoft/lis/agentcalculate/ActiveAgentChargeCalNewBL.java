package com.sinosoft.lis.agentcalculate;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

import java.math.BigDecimal;

import com.sinosoft.lis.db.LAChargeLogDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeBSchema;
import com.sinosoft.lis.schema.LAChargeLogSchema;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LAChargeBSet;
import com.sinosoft.lis.vschema.LAChargeLogSet;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ActiveAgentChargeCalNewBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mCalType = "99";
    //获得存储号码
    private String mNewEdorNo = "";
//    private static final String MNO = "1";
    
    /** 手续费序列号*/
//    private String mBatchNo = "00000000000000000000";;

    /** 数据操作字符串 */
    private String mOperate;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String ManageCom;
    private String mBranchType;
    private String mBranchType2;
    private boolean mReturnState = false;

    /** 业务处理相关变量 */
    private LAChargeLogSchema mLAChargeLogSchema = new LAChargeLogSchema();
    private LAChargeLogSet mLastLAChargeLogSet = new LAChargeLogSet();
    private LAChargeBSet mLAChargeBSet = new LAChargeBSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();

    private MMap mMap = new MMap();

    public ActiveAgentChargeCalNewBL() {
    }

    private boolean getInputData(VData cInputData) {
        mLAChargeLogSchema = ((LAChargeLogSchema) cInputData.
                              getObjectByObjectName(
                                      "LAChargeLogSchema", 0));
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mLAChargeLogSchema == null || mGlobalInput == null) {
            CError tError = new CError();
            tError.moduleName = "ActiveAgentChargeCalNewBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.ManageCom = mLAChargeLogSchema.getManageCom();
        this.mBranchType = mLAChargeLogSchema.getBranchType();
        this.mBranchType2 = mLAChargeLogSchema.getBranchType2();
        return true;
    }

    private boolean check() {

        return true;
    }

    private boolean prepareOutputData() {

        mMap.put(this.mLAChargeSet, "INSERT");
//        mMap.put(this.mLAChargeBSet, "INSERT");
        mMap.put(this.mLastLAChargeLogSet, "UPDATE");
        this.mInputData.add(mMap);
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            if (mErrors.needDealError()) {
                System.out.println("程序异常结束原因：" + mErrors.getFirstError());
            }
            if (mReturnState == true) {
                if (!returnState()) {
                    return false;
                }
            }
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start ActiveAgentChargeCalNewBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ActiveAgentChargeCalNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean dealData() {
//    	获取手续费计算月份的止期是否已经进行过提数计算
        System.out.println("~~~~~~~~~~开始手续费业务处理!");
       // System.out.println("1222222222222222222222222222222");
        LAWageLogDB tLAWageLogDB = new LAWageLogDB();
        LAWageLogSet tLAWageLogSet = new LAWageLogSet();
       // System.out.println("--------------------------------");
        tLAWageLogDB.setManageCom(ManageCom);
        tLAWageLogDB.setBranchType(mBranchType);
        tLAWageLogDB.setBranchType2(mBranchType2);
       // System.out.println("++++++++++++++++++++++++++++");
        tLAWageLogDB.setWageNo(mLAChargeLogSchema.getChargeCalNo());
        tLAWageLogDB.setWageMonth(mLAChargeLogSchema.getChargeMonth());
        //System.out.println("*************************************");
        tLAWageLogDB.setWageYear(mLAChargeLogSchema.getChargeYear());
        tLAWageLogSet = tLAWageLogDB.query();
        //System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
       //String enddate = tLAWageLogSet.get(1).getEndDate();
       // System.out.println("=============================================");
       // System.out.println("ENDDATE" + enddate);
        System.out.println("mLAChargeLogSchema.getEndDate()" +
                           mLAChargeLogSchema.getEndDate());
        FDate fd = new FDate();
        LAChargeLogSet tLAChargeLogSet = new LAChargeLogSet();
        LAChargeLogDB tLAChargeLogDB = new LAChargeLogDB();
        String sql = "";
        String Year = AgentPubFun.formatDate(mLAChargeLogSchema.getStartDate(),
                                             "yyyy");
        String Month = AgentPubFun.formatDate(mLAChargeLogSchema.getEndDate(),
                                              "MM");
        sql = "select * from LAChargeLog where  ChargeYear = '" + Year + "'"
              + " and ChargeMonth='" + Month + "'"
              + " and ManageCom = '" + ManageCom + "' ";
        sql += "And BranchType = '" + mBranchType + "'";
        sql += " And BranchType2 ='" + mBranchType2 + "'";

        System.out.println("sql:" + sql);

        tLAChargeLogSet = tLAChargeLogDB.executeQuery(sql);
        if (tLAChargeLogDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLAChargeLogDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询日志纪录存在是否出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAChargeLogSet.size() == 0) {
            System.out.println("日志表中没有纪录！");
            if (!prepareLogData(mBranchType, mBranchType2)) {
                return false;
            }
        } else {
            for (int i = 1; i <= tLAChargeLogSet.size(); i++) {
                String today = mLAChargeLogSchema.getEndDate();
                String yesterday = PubFun.calDate(today, -1, "D", null);
                LAChargeLogSchema tLAChargeLogSchema = new LAChargeLogSchema();
                tLAChargeLogSchema = tLAChargeLogSet.get(i);
                System.out.println("tLAChargeLogSchema.getEndDate():" +
                                   tLAChargeLogSchema.getEndDate());
                System.out.println("today:" + today);
                System.out.println("yesterday:" + yesterday);
                if (!tLAChargeLogSchema.getState().equals("11")) {
                    CError tError = new CError();
                    tError.moduleName = "ActiveAgentChargeCalNewBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "昨天(" + yesterday + ")的展业类型为" +
                                          tLAChargeLogSchema.getBranchType() +
                                          "，渠道" +
                                          tLAChargeLogSchema.getBranchType2() +
                                          "数据提取后未计算，不能进行今天的提取！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LAChargeLogDB mLAChargeLogDB = new LAChargeLogDB();
                mLAChargeLogDB.setManageCom(tLAChargeLogSchema.getManageCom());
                mLAChargeLogDB.setChargeCalNo(tLAChargeLogSchema.getChargeCalNo());
                mLAChargeLogDB.setChargeMonth(tLAChargeLogSchema.getChargeMonth());
                mLAChargeLogDB.setChargeYear(tLAChargeLogSchema.getChargeYear());
                mLAChargeLogDB.setBranchType(tLAChargeLogSchema.
                                             getBranchType());
                mLAChargeLogDB.setBranchType2(tLAChargeLogSchema.
                                              getBranchType2());
                mLAChargeLogDB.getInfo();
                mLAChargeLogDB.setModifyDate(CurrentDate);
                mLAChargeLogDB.setModifyTime(CurrentTime);
                mLAChargeLogDB.setOperator(mGlobalInput.Operator);
                mLAChargeLogDB.setState("00");
                mLAChargeLogDB.setEndDate(mLAChargeLogSchema.getEndDate());
                if (!mLAChargeLogDB.update()) {
                    return false;
                }
            }
        }
        mReturnState = true;
//        LAChargeBSet tLAChargeBSet = new LAChargeBSet();
        //进行非标业务处理

        //数据提取,根据TMAKEDATE查找
        SSRS commSSRS = new SSRS();
        commSSRS = queryLAcommision();
//        if (commSSRS.getMaxRow() <= 0) {
//            CError mError = new CError();
//            mError.moduleName = "ActiveAgentChargeCalNewBL";
//            mError.functionName = "dealdata";
//            mError.errorMessage = "没有需要处理的数据!";
//            this.mErrors.addOneError(mError);
//            return false;
//        }
//        mNewEdorNo = PubFun1.CreateMaxNo("EDORNO", 20);
        Reflections tReflections = new Reflections();
        for (int i = 1; i <= commSSRS.getMaxRow(); i++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            LAChargeSchema businessLAChargeSchema = new LAChargeSchema();
            LAChargeSchema manageLAChargeSchema = new LAChargeSchema();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionDB.setCommisionSN(commSSRS.GetText(i, 1));
            tLACommisionDB.getInfo();
            tLACommisionSchema = tLACommisionDB.getSchema();
            if(!checkCalFlag(tLACommisionSchema))
            {
            	continue;
            }
            String tRiskCode = "";
            String tAgentCom = "";
            String tContNo = "";
            int payyear = 0;
            int renewcount = 0;
            int payyears = 0;
            int tPayIntv=0;
            tRiskCode = tLACommisionSchema.getRiskCode();
            tAgentCom = tLACommisionSchema.getAgentCom();
            tContNo = tLACommisionSchema.getContNo();
            payyear = tLACommisionSchema.getPayYear();
            renewcount = tLACommisionSchema.getReNewCount();
            payyears = tLACommisionSchema.getPayYears();
            String tTransState=tLACommisionSchema.getTransState();
            String tWrapCode =tLACommisionSchema.getF3();
            String tTmakedate =tLACommisionSchema.getTMakeDate();
            String tGrpContNo = tLACommisionSchema.getGrpContNo();
            String tGrpPolNo = tLACommisionSchema.getGrpPolNo();
            String tPolNo = tLACommisionSchema.getPolNo();
            tPayIntv=tLACommisionSchema.getPayIntv();
            LAChargeSchema tLAChargeSchema = new LAChargeSchema();
            tLAChargeSchema.setAgentCom(tLACommisionSchema.getAgentCom());
            tLAChargeSchema.setBranchType(tLACommisionSchema.getBranchType());
            tLAChargeSchema.setBranchType2(tLACommisionSchema.getBranchType2());
            tLAChargeSchema.setCalDate(tLACommisionSchema.getCalDate());
            tLAChargeSchema.setTCommisionSN(tLACommisionSchema.getCommisionSN());//新增字段存储commisionsn
            tLAChargeSchema.setContNo(tLACommisionSchema.getContNo());
            tLAChargeSchema.setGrpContNo(tLACommisionSchema.getGrpContNo());
            tLAChargeSchema.setPolNo(tLACommisionSchema.getPolNo());
            tLAChargeSchema.setGrpPolNo(tLACommisionSchema.getGrpPolNo());
            tLAChargeSchema.setManageCom(tLACommisionSchema.getManageCom());
            tLAChargeSchema.setMainPolNo(tLACommisionSchema.getMainPolNo());
            tLAChargeSchema.setPayCount(tLACommisionSchema.getPayCount());
            tLAChargeSchema.setTransMoney(tLACommisionSchema.getTransMoney()); //实收保费
            tLAChargeSchema.setTransType(tLACommisionSchema.getTransType());
            tLAChargeSchema.setRiskCode(tLACommisionSchema.getRiskCode());
            tLAChargeSchema.setReceiptNo(tLACommisionSchema.getReceiptNo());
            tLAChargeSchema.setWageNo(tLACommisionSchema.getWageNo());
            
            tLAChargeSchema.setChargeState("0");
            tLAChargeSchema.setMakeDate(CurrentDate);
            tLAChargeSchema.setMakeTime(CurrentTime);
            tLAChargeSchema.setModifyDate(CurrentDate);
            tLAChargeSchema.setModifyTime(CurrentTime);
            tLAChargeSchema.setOperator(mGlobalInput.Operator);
            tLAChargeSchema.setPayIntv(tLACommisionSchema.getPayIntv());
            tLAChargeSchema.setTMakeDate(tLACommisionSchema.getTMakeDate());

            tReflections.transFields(businessLAChargeSchema,tLAChargeSchema);
            tReflections.transFields(manageLAChargeSchema, tLAChargeSchema);
            //业务手续费的处理
            businessLAChargeSchema.setChargeType("55"); //互动中介业务手续费类型
            double chargeRate = calculate(tRiskCode, tAgentCom, tContNo,tGrpContNo,
                                          payyear, renewcount, payyears,tTransState,tPayIntv,tWrapCode,tTmakedate,tGrpPolNo,tPolNo,"01");
            businessLAChargeSchema.setChargeRate(chargeRate);
            double charge = 0;
            charge = mulrate(chargeRate,
                             tLACommisionSchema.getTransMoney());
            businessLAChargeSchema.setCharge(charge);   
            businessLAChargeSchema.setCommisionSN(tLACommisionSchema.getCommisionSN());
            
            //管理手续费的处理
            manageLAChargeSchema.setChargeType("56"); //互动中介管理手续费类型
            double manageChargeRate = calculate(tRiskCode, tAgentCom, tContNo,tGrpContNo,
                    payyear, renewcount, payyears,tTransState,tPayIntv,tWrapCode,tTmakedate,tGrpPolNo,tPolNo,"02");
            manageLAChargeSchema.setChargeRate(manageChargeRate);
            double manageCharge = 0;
            manageCharge = mulrate(manageChargeRate,charge);
            manageLAChargeSchema.setCharge(manageCharge);
            String manageCommisionSN = tLACommisionSchema.getCommisionSN().substring(0, 9)+"1";
            manageLAChargeSchema.setCommisionSN(manageCommisionSN);
            mLAChargeSet.add(businessLAChargeSchema);
            mLAChargeSet.add(manageLAChargeSchema);
            
//            tLAChargeSchema.setBatchNo(this.mBatchNo);
//            mLAChargeSet.add(tLAChargeSchema);
//            String tNewEdorNo = mNewEdorNo;
//            if (tLAChargeSchema != null) {
//                LAChargeBSchema tLAChargeBSchema = setLAChargeBSchema(
//                        tLAChargeSchema, tNewEdorNo);
//                if (tLAChargeBSchema != null) {
//                    mLAChargeBSet.add(tLAChargeBSchema);
//                }
//            }
        }

        LAChargeLogDB mLAChargeLogDB = new LAChargeLogDB();
        tLAChargeLogDB.setChargeCalNo(mLAChargeLogSchema.getChargeCalNo());
        tLAChargeLogDB.setManageCom(ManageCom);
        tLAChargeLogDB.setBranchType(mBranchType);
        tLAChargeLogDB.setBranchType2(mBranchType2);
        tLAChargeLogDB.getInfo();
        tLAChargeLogDB.setOperator(mGlobalInput.Operator);

        tLAChargeLogDB.setState("11");

        tLAChargeLogDB.setMakeDate(CurrentDate);
        tLAChargeLogDB.setMakeTime(CurrentTime);
        tLAChargeLogDB.setModifyDate(CurrentDate);
        tLAChargeLogDB.setModifyTime(CurrentTime);
        mLastLAChargeLogSet.add(tLAChargeLogDB) ;
        return true;
    }

    /**
     * 手续费的计算从LCOMMISION 表中提取数据
     * @return
     */
    private SSRS queryLAcommision() {
        System.out.println("~~~~~~~数据提取!");
        SSRS tSSRS = new SSRS();
        String tsql = "select * from lacommision where managecom like '" +
                      this.ManageCom + "%'" +
                      " and tmakedate>='" + mLAChargeLogSchema.getStartDate() +
                      "' " +
                      "and tmakedate<='" + mLAChargeLogSchema.getEndDate() +
                      "'" +
                      " and branchtype='" + this.mBranchType + "'" +
                      " and branchtype2='" + this.mBranchType2 + "'" +
                      " and  agentcom is not null and  agentcom <>''  "+
                      " and not exists (select 'x' from lacharge where commisionsn=lacommision.commisionsn ) "+
                      "order by agentcom,tmakedate,contno";
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tsql);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryLJAGetEndorse";
            tError.errorMessage = "查询扎帐表失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }

    private boolean prepareLogData(String BranchType, String BranchType2) {
        System.out.println("填充佣金计算日志表");
        //填充佣金计算日志表
        LAChargeLogDB tLAChargeLogDB = new LAChargeLogDB();
        //  LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();

        tLAChargeLogDB.setChargeCalNo(mLAChargeLogSchema.getChargeCalNo());
        tLAChargeLogDB.setManageCom(ManageCom);
        tLAChargeLogDB.setBranchType(BranchType);
        tLAChargeLogDB.setBranchType2(BranchType2);
        tLAChargeLogDB.setStartDate(mLAChargeLogSchema.getStartDate());
        tLAChargeLogDB.setEndDate(mLAChargeLogSchema.getEndDate());
        tLAChargeLogDB.setChargeMonth(mLAChargeLogSchema.getChargeMonth());
        tLAChargeLogDB.setChargeYear(mLAChargeLogSchema.getChargeYear());
        tLAChargeLogDB.setOperator(mGlobalInput.Operator);

        tLAChargeLogDB.setState("00");
        tLAChargeLogDB.setMakeDate(CurrentDate);
        tLAChargeLogDB.setMakeTime(CurrentTime);
        tLAChargeLogDB.setModifyDate(CurrentDate);
        tLAChargeLogDB.setModifyTime(CurrentTime);
        if (!tLAChargeLogDB.insert()) {
            return false;
        }
        return true;
    }

    private boolean returnState() {
        System.out.println("填充佣金计算日志表");
        //填充佣金计算日志表
        String Year = AgentPubFun.formatDate(mLAChargeLogSchema.getStartDate(),
                                             "yyyy");
        String Month = AgentPubFun.formatDate(mLAChargeLogSchema.getEndDate(),
                                              "MM");
        String WageNo = AgentPubFun.formatDate(mLAChargeLogSchema.getStartDate(),
                                               "yyyyMM");
        String SQL = "select date('" + this.mLAChargeLogSchema.getEndDate() +
                     "')-1 day from dual";
        System.out.println(SQL);

        ExeSQL ttSQL = new ExeSQL();
        String enddate1 = ttSQL.getOneValue(SQL); //返回到前一天的日志
        System.out.println("enddate1" + enddate1);
        //判断是否是一个月的开始，如果是要删除数据，如果不是修改ENDDATE及状态即可；
        String month1 = AgentPubFun.formatDate(enddate1, "MM");
        if (Month.compareTo(month1) != 0) {
            //删除数据
            LAChargeLogDB tLAChargeLogDB = new LAChargeLogDB();
            tLAChargeLogDB.setChargeCalNo(mLAChargeLogSchema.getChargeCalNo());
            tLAChargeLogDB.setManageCom(ManageCom);
            tLAChargeLogDB.setBranchType(mBranchType);
            tLAChargeLogDB.setBranchType2(mBranchType2);
            tLAChargeLogDB.setChargeMonth(mLAChargeLogSchema.getChargeMonth());
            tLAChargeLogDB.setChargeYear(mLAChargeLogSchema.getChargeYear());
            tLAChargeLogDB.setOperator(mGlobalInput.Operator);

            tLAChargeLogDB.setState("00");
            tLAChargeLogDB.setMakeDate(CurrentDate);
            tLAChargeLogDB.setMakeTime(CurrentTime);
            tLAChargeLogDB.setModifyDate(CurrentDate);
            tLAChargeLogDB.setModifyTime(CurrentTime);
            if (!tLAChargeLogDB.delete()) {
                return false;
            }

        } else {
            LAChargeLogDB tLAChargeLogDB = new LAChargeLogDB();
            tLAChargeLogDB.setChargeCalNo(mLAChargeLogSchema.getChargeCalNo());
            tLAChargeLogDB.setManageCom(ManageCom);
            tLAChargeLogDB.setBranchType(mBranchType);
            tLAChargeLogDB.setBranchType2(mBranchType2);
            tLAChargeLogDB.getInfo();
            tLAChargeLogDB.setOperator(mGlobalInput.Operator);
            tLAChargeLogDB.setState("11");
            tLAChargeLogDB.setModifyDate(CurrentDate);
            tLAChargeLogDB.setModifyTime(CurrentTime);
            if (!tLAChargeLogDB.update()) {
                return false;
            }
        }
        return true;
    }

    public double calculate(String cRiskCode, String cAgentCom, String cContno,String cGrpContNo,
                            int cPayyear, int cRenewcount, int cPayyears,String tTransState,
                            int cPayintv,String cWrapCode,String cTMakeDate,String cGrpPolNo,String cPolNo,String tFlag) {

        double strWageRate = 0.0;
//        LAWageCalElementSchema tLAWageCalElementSchema=new LAWageCalElementSchema();
//        LAWageCalElementDB tLAWageCalElementDB=new LAWageCalElementDB();
//        tLAWageCalElementDB.setRiskCode(cRiskCode);
//        tLAWageCalElementDB.setCalType("21");
//        tLAWageCalElementDB.setBranchType("3");
//        tLAWageCalElementDB.setBranchType2("01");        
//        tLAWageCalElementDB.getInfo();
//        tLAWageCalElementSchema=tLAWageCalElementDB.getSchema();
        String tGrpPolNo = cGrpPolNo;
        if("00000000000000000000".equals(cGrpContNo))
        {
        	tGrpPolNo = cPolNo;
        }
        Calculator tCalculator = new Calculator(); //计算类
        if("01".equals(tFlag)){
	        SSRS tSSRS = new SSRS();
	        String tsql = "select CalCode from LAWageCalElement where  branchtype='5'" +
	                      " and branchtype2='01' and CalType='21' " +
	                      " and  RiskCode='"+cRiskCode+"' ";
	        ExeSQL tExeSQL = new ExeSQL();
	        tSSRS = tExeSQL.execSQL(tsql);
	        if(tSSRS.getMaxRow()<=0){
	        	tCalculator.setCalCode("AC9901"); 	
	        }
	        else
	        {
	        	tCalculator.setCalCode(tSSRS.GetText(1, 1)); 
	        }
        }
        if("02".equals(tFlag)){
        	   tCalculator.setCalCode("AC9801");
        }
//         tCalculator.setCalCode(tLAWageCalElementSchema.getCalCode()); //添加计算编码
//        tCalculator.setCalCode("AP9901"); //添加计算编码

        tCalculator.addBasicFactor("RISKCODE",
                                   cRiskCode);
        tCalculator.addBasicFactor("CONTNO",
                                   cContno);
        tCalculator.addBasicFactor("BRANCHTYPE",
                                   this.mBranchType);
        tCalculator.addBasicFactor("BRANCHTYPE2",
                                   this.mBranchType2);
        tCalculator.addBasicFactor("AGENTCOM",
                                   cAgentCom);
        tCalculator.addBasicFactor("TRANSSTATE",
                tTransState);
        //添加套餐编码
        tCalculator.addBasicFactor("WRAPCODE",cWrapCode);
        //添加套餐编码判断是否在生效日期中
        tCalculator.addBasicFactor("TMAKEDATE",cTMakeDate);
        tCalculator.addBasicFactor("MANAGECOM", ManageCom);
        tCalculator.addBasicFactor("GRPCONTNO", cGrpContNo);
        tCalculator.addBasicFactor("RENEWCOUNT", String.valueOf(cRenewcount));
        tCalculator.addBasicFactor("PAYYEAR", String.valueOf(cPayyear));
        tCalculator.addBasicFactor("PAYYEARS", String.valueOf(cPayyears));
        tCalculator.addBasicFactor("PAYINTV", String.valueOf(cPayintv));
        tCalculator.addBasicFactor("GRPPOLNO", tGrpPolNo);
        
        strWageRate = Double.parseDouble(tCalculator.calculate());
        if (tCalculator.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ActiveAgentChargeCalNewBL";
            tError.functionName = "calWage1";
            tError.errorMessage = tCalculator.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return 0;
        }

        return strWageRate;
    }


    //double类型乘法计算
    public static double mulrate(double v1, double v2) {

        BigDecimal b1 = new BigDecimal(Double.toString(v1));

        BigDecimal b2 = new BigDecimal(Double.toString(v2));

        return b1.multiply(b2).doubleValue();

    }

    public static double round(double v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(Double.toString(v));
        BigDecimal one = new BigDecimal("1");
        return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public VData getResult() {
        mResult = new VData();
        mResult.add(mMap);
        return mResult;
    }

    /**
     * 设置手续费备份的数据
     * @return boolean
     */
    private boolean setLAChargeBSet() {
        //判断待删除的手续费数据是否存在
        if (mLAChargeSet == null || mLAChargeSet.size() < 1) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChargeGatherUI";
            tError.functionName = "setLAChargeSet";
            tError.errorMessage = "没有备份务数据！";
            System.out.println("没有手续费备份务数据！");
            this.mErrors.addOneError(tError);
            return false;
        }
        //取得直接手续费明细表（手续费扎账表）的数据数
        int iMax = mLAChargeSet.size();
        LAChargeBSet tLAChargeBSet = new LAChargeBSet();
        //获得转储号码
        String tNewEdorNo = mNewEdorNo;
        //准备备份数据
        for (int i = 1; i <= iMax; i++) {
            LAChargeSchema tLAChargeSchema = new LAChargeSchema();
            tLAChargeSchema = mLAChargeSet.get(i).getSchema();
            if (tLAChargeSchema != null) {
                LAChargeBSchema tLAChargeBSchema = setLAChargeBSchema(
                        tLAChargeSchema, tNewEdorNo);
                if (tLAChargeBSchema != null) {
                    tLAChargeBSet.add(tLAChargeBSchema);
                }
            }
        }
        //缓存备份数据
        mLAChargeBSet = tLAChargeBSet;
        return true;
    }

    /**
     * 设置备份数据
     * @param pmLAWageSchema LAChargeSchema
     * @param pmEdorNo String
     * @return LAChargeBSchema
     */
    private LAChargeBSchema setLAChargeBSchema(LAChargeSchema pmLAChargeSchema,
                                               String pmEdorNo) {
        LAChargeBSchema tLAChargeBSchema = new LAChargeBSchema();
        Reflections tReflections = new Reflections();

        //设置备份表中和手续费表中相同的项
        tReflections.transFields(tLAChargeBSchema, pmLAChargeSchema);
        //设置转储号码
        tLAChargeBSchema.setEdorNo(pmEdorNo);
        //设置转储类型“手续费回退备份”
        tLAChargeBSchema.setEdorType("01");
        //设置提数回退时间
        tLAChargeBSchema.setTStartDate(mLAChargeLogSchema.getStartDate());
        tLAChargeBSchema.setTEndDate(mLAChargeLogSchema.getEndDate());

        return tLAChargeBSchema;
    }
    /**
     * 判断此数据是否要计算手续费
     * @param cLACommisionSchema  要要计算的手续费
     * @return
     */
    private boolean checkCalFlag(LACommisionSchema cLACommisionSchema)
    {
 	   String crs_chnl = "";
 	   String crs_bussType ="";
 	   String tBranchType3 = cLACommisionSchema.getBranchType3();
 	   if("N".equals(tBranchType3))
 	   {
 		  return true; 
 	   }
 	   if(!"00000000000000000000".equals(cLACommisionSchema.getGrpContNo()))
 	   {
 		   LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
 		   LCGrpContDB tLCGrpContDB = new LCGrpContDB();
 		   tLCGrpContDB.setGrpContNo(cLACommisionSchema.getGrpContNo());
 		   if(tLCGrpContDB.getInfo())
 		   {
 			   tLCGrpContSchema = tLCGrpContDB.getSchema();
 			   crs_chnl = tLCGrpContSchema.getCrs_SaleChnl();
 			   crs_bussType = tLCGrpContSchema.getCrs_BussType();
 		   }
 		   else
 		   {
 			  LBGrpContSchema tLBGrpContSchema = new LBGrpContSchema();
 			  LBGrpContDB tLBGrpContDB = new LBGrpContDB();
 			  tLBGrpContDB.setGrpContNo(cLACommisionSchema.getGrpContNo());
 			  if(tLBGrpContDB.getInfo())
 			  {
	 			  tLBGrpContSchema = tLBGrpContDB.getSchema();
	 			  crs_chnl = tLBGrpContSchema.getCrs_SaleChnl();
	 			  crs_bussType = tLBGrpContSchema.getCrs_BussType();
 			  }
 		   }
 	   }
 	   else
 	   {
 		   LCContSchema tLCContSchema = new LCContSchema();
 		   LCContDB tLCContDB = new LCContDB();
 		   tLCContDB.setContNo(cLACommisionSchema.getContNo());
 		   if(tLCContDB.getInfo())
 		   {
 			   tLCContSchema = tLCContDB.getSchema();
 			   crs_chnl = tLCContSchema.getCrs_SaleChnl();
 			   crs_bussType = tLCContSchema.getCrs_BussType();
 		   }
 		   else
 		   {
 			   LBContSchema tLBContSchema = new LBContSchema();
	 		   LBContDB tLBContDB = new LBContDB();
	 		   tLBContDB.setContNo(cLACommisionSchema.getContNo());
	 		   if(tLBContDB.getInfo())
	 		   {
	 			   tLBContSchema = tLBContDB.getSchema();
	 			   crs_chnl = tLBContSchema.getCrs_SaleChnl();
	 			   crs_bussType = tLBContSchema.getCrs_BussType();
	 		   }
 		   }
 	   }
 	   if(!"01".equals(crs_chnl)&&!"02".equals(crs_chnl))
 	   {
 		   return false;
 	   }
 	   if(!"01".equals(crs_bussType))
 	   {
 		   return false;
 	   }
 	   return true;
    }


}
