package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

/**
 * 处理团单无名单减人处理类
 * @author yangyang 2017-9-11
 *
 */
public class AgentWageOfLJAGetEndorseGrpWJ_WM extends AgentWageOfAbstractClass{

	private AgentWageOfLJAGetEndorseGrpMain mAgentWageOfLJAGetEndorseGrpMain = new AgentWageOfLJAGetEndorseGrpMain();
	
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfLJAGetEndorseGrpWJ_WM-->dealData:开始执行");
		System.out.println("AgentWageOfLJAGetEndorseGrpWJ_WM-->dealData:cSQL"+cSQL);
		mAgentWageOfLJAGetEndorseGrpMain.setBaseValue(this.mExtractDataType, this.mTableName, this.mDataType);
		LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAGetEndorseSet, cSQL);
		//用来判断数据是不是为空，为了防止 最后一次循环时，为空时，也打印错误语句  
		boolean tJudgeNullFlag = false;
		do
		{
			tRSWrapper.getData();
			if(!tJudgeNullFlag)
			{
				if(null==tLJAGetEndorseSet||0==tLJAGetEndorseSet.size())
				{
					System.out.println("AgentWageOfLJAGetEndorseGrpWJ_WM-->dealData 没有满足条件的数据");
					return true;
				}
				tJudgeNullFlag = true;
			}
			
			MMap tMMap = new MMap();
			LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
			LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
			for(int i =1;i<=tLJAGetEndorseSet.size();i++)
			{
				LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
				tLJAGetEndorseSchema = tLJAGetEndorseSet.get(i);
				double tTransMoney = tLJAGetEndorseSchema.getGetMoney();
				//管理费
				double tManageFee = mAgentWageCommonFunction.getManageFee(tLJAGetEndorseSchema.getPolNo(), tLJAGetEndorseSchema.getEndorsementNo(), tLJAGetEndorseSchema.getOtherNoType());
				//利息 
				double tAccuralFee = 0;
				boolean tReturnFlag = mAgentWageOfLJAGetEndorseGrpMain.dealData(tLJAGetEndorseSchema, tTransMoney, tManageFee, tAccuralFee, "14", cOperator);
				if(tReturnFlag)
				{
					tLACommisionNewSet.add(mAgentWageOfLJAGetEndorseGrpMain.getmLACommisionNewSchema());
				}
				else
				{
					tLACommisionErrorSet.add(mAgentWageOfLJAGetEndorseGrpMain.getmLACommisionErrorSchema());
				}
			}
			if(0<tLACommisionNewSet.size())
			{
				tMMap.put(tLACommisionNewSet, "INSERT");
			}
			if(0<tLACommisionErrorSet.size())
			{
				tMMap.put(tLACommisionErrorSet, "INSERT");
			}
			if(0==tMMap.size())
			{
				continue;
			}
			if (!mAgentWageCommonFunction.dealDataToTable(tMMap)) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "AgentWageOfLJAGetEndorseGrpWJ_WM";
				tError.functionName = "PubSubmit";
				tError.errorMessage = "数据插入到LACommisionNew中失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}while(tLJAGetEndorseSet.size()>0);
		System.out.println("AgentWageOfLJAGetEndorseGrpWJ_WM-->dealData:执行结束");
		return true;
	}
}
