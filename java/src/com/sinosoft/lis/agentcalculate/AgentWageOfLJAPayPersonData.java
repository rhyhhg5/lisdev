package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

public class AgentWageOfLJAPayPersonData extends AgentWageOfAbstractClass{
	/**
	 * 主要的提数逻辑
	 * @return
	 */
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfLJAPayPersonData-->dealData:开始执行");
		System.out.println("AgentWageOfLJAPayPersonData-->dealData:cSQL:"+cSQL);
		LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAPayPersonSet, cSQL);
		//用来判断数据是不是为空，为了防止 最后一次循环时，为空时，也打印错误语句  
		boolean tJudgeNullFlag = false;
		do
		{
			MMap tMMap = new MMap();
			tRSWrapper.getData();
			if(!tJudgeNullFlag)
			{
				if(null==tLJAPayPersonSet||0==tLJAPayPersonSet.size())
				{
					System.out.println("AgentWageOfLJAPayPersonData没有满足条件的数据");
					return true;
				}
				tJudgeNullFlag = true;
			}
			LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
			LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
			for(int i =1;i<=tLJAPayPersonSet.size();i++)
			{
				LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
				tLJAPayPersonSchema = tLJAPayPersonSet.get(i);
				LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
				String tPolNo = tLJAPayPersonSchema.getPolNo();
				String tContNo = tLJAPayPersonSchema.getContNo();
				String tPayTypeFlag=tLJAPayPersonSchema.getPayTypeFlag();
	            String tRiskCode=tLJAPayPersonSchema.getRiskCode();
	            String tPayType = tLJAPayPersonSchema.getPayType();
	            //查询业务员相关信息，
                LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tLJAPayPersonSchema.getAgentCode());
                if(null==tLAAgentSchema)
                {
                	LACommisionErrorSchema tLACommisionErrorSchema 
                		= dealErrorData(tLJAPayPersonSchema,AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE,cOperator);
                	tLACommisionErrorSet.add(tLACommisionErrorSchema);
                	continue;
                }
                String tBranchCode = tLAAgentSchema.getBranchCode();
                //查询团队相关信息
				LABranchGroupSchema tLABranchGroupSchema= new LABranchGroupSchema();
                tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tBranchCode);
                if(null==tLABranchGroupSchema)
                {
                	LACommisionErrorSchema tLACommisionErrorSchema 
            			= dealErrorData(tLJAPayPersonSchema,AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP,cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
                	continue;
                }
                LCContSchema tLCContSchema = new LCContSchema();
				tLCContSchema = mAgentWageCommonFunction.queryLCCont(tContNo);
				if(null == tLCContSchema)
				{
					LACommisionErrorSchema tLACommisionErrorSchema 
	        			= dealErrorData(tLJAPayPersonSchema,AgentWageOfCodeDescribe.ERRORCODE_LCCONT,cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
				LCPolSchema tLCPolSchema = new LCPolSchema();
		        tLCPolSchema = mAgentWageCommonFunction.queryLCPol(tPolNo);
		        if(null == tLCPolSchema)
				{
		        	LACommisionErrorSchema tLACommisionErrorSchema 
	        			= dealErrorData(tLJAPayPersonSchema,AgentWageOfCodeDescribe.ERRORCODE_LCPOL,cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
		       
		        //对于互动的处理
		        String tSaleChnl = tLCContSchema.getSaleChnl();
		        String tBranchType3 ="";
		        //对中介机构校验
		        if("03".equals(tSaleChnl))
		        {
		        	LAComSchema tLAComSchema = new LAComSchema();
			        tLAComSchema = mAgentWageCommonFunction.querytLACom(tLCPolSchema.getAgentCom());
			        if(null == tLAComSchema)
			        {
			        	LACommisionErrorSchema tLACommisionErrorSchema 
	        			    = dealErrorData(tLJAPayPersonSchema,AgentWageOfCodeDescribe.ERRORCODE_AGENTCOM,cOperator);
	            	    tLACommisionErrorSet.add(tLACommisionErrorSchema);
					    continue;
			        }
		        }
		        if("14".equals(tSaleChnl)||"15".equals(tSaleChnl))
		        {
		        	String tCrs_SaleChnl = tLCContSchema.getCrs_SaleChnl();
		        	String tCrs_BussType = tLCContSchema.getCrs_BussType();
		        	tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
		        }
		        if("no".equals(tBranchType3))
	        	{
		        	LACommisionErrorSchema tLACommisionErrorSchema 
	        			= dealErrorData(tLJAPayPersonSchema,AgentWageOfCodeDescribe.ERRORCODE_ACTIVE,cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
	        	}
		        String[] tBranchTypes = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
		        if(null == tBranchTypes||0==tBranchTypes.length)
		        {
		        	LACommisionErrorSchema tLACommisionErrorSchema 
	        			= dealErrorData(tLJAPayPersonSchema,AgentWageOfCodeDescribe.ERRORCODE_SALECHNL,cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
		        }
		        String tBranchType = tBranchTypes[0];
		        String tBranchType2 = tBranchTypes[1];
		        
		        tLACommisionNewSchema = mAgentWageCommonFunction.packageContDataOfLACommisionNew(tLCContSchema, tLCPolSchema);
		       
		        //获取最大commision
		        String tCommisionsn = mAgentWageCommonFunction.getCommisionSN();
		        tLACommisionNewSchema.setCommisionSN(tCommisionsn);
		        tLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
		        tLACommisionNewSchema.setBranchType(tBranchType);
		        tLACommisionNewSchema.setBranchType2(tBranchType2);
		        tLACommisionNewSchema.setBranchType3(tBranchType3);
		        tLACommisionNewSchema.setReceiptNo(tLJAPayPersonSchema.getPayNo());
		        tLACommisionNewSchema.setTPayDate(tLJAPayPersonSchema.getPayDate());
		        tLACommisionNewSchema.setTEnterAccDate(tLJAPayPersonSchema.getEnterAccDate());
		        tLACommisionNewSchema.setTConfDate(tLJAPayPersonSchema.getConfDate());
		        tLACommisionNewSchema.setTMakeDate(tLJAPayPersonSchema.getMakeDate());
		        // CalDate
		        tLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
		        tLACommisionNewSchema.setTransMoney(tLJAPayPersonSchema.getSumActuPayMoney());
		        String tTransState ="00";//正常保单
		        if("ZB".equals(tPayType))
		        {
		        	tTransState = "03";//追加保单
		        }
		        tLACommisionNewSchema.setTransState(tTransState);
		        tLACommisionNewSchema.setTransStandMoney(tLJAPayPersonSchema.getSumDuePayMoney());
		        tLACommisionNewSchema.setLastPayToDate(tLJAPayPersonSchema.getLastPayToDate());
		        tLACommisionNewSchema.setCurPayToDate(tLJAPayPersonSchema.getCurPayToDate());
		        tLACommisionNewSchema.setTransType("ZC");
		        //TransState 交易处理状态 590 - 614
		        tLACommisionNewSchema.setCommDire("1");
		        if(tLCContSchema.getSaleChnl().equals("07"))
		        {
		            tLACommisionNewSchema.setF1("02");
		            tLACommisionNewSchema.setP5( tLACommisionNewSchema.getTransMoney());//职团开拓实收保费
		        }
		        else
		        {
		            tLACommisionNewSchema.setF1("01");
		        }
		        //xjh Add 05/01/27 ,判断是否是批改保单
		        String tflag = mAgentWageCommonFunction.getIncomeType(tLJAPayPersonSchema.getPayNo());
		        tLACommisionNewSchema.setFlag(tflag);
		        int PayYear = 0;
		        if (tLJAPayPersonSchema.getPayIntv() == 0) { //趸交payIntv = 0
                    PayYear = 0;
                } else {
                    //计算交费年度=保单生效日到交至日期
                    PayYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),
                    		mFDate.getString(PubFun.calDate(
                    				mFDate.getDate(tLJAPayPersonSchema.getCurPayToDate()),-2,"D", null)
                    		),
                            "Y");
                    if(PayYear<0)  PayYear=0;
                }
		        tLACommisionNewSchema.setPayYear(PayYear);
		        int tRenewCount =  mAgentWageCommonFunction.queryLCRnewPol(tPolNo,"polno");
		        if(!"1".equals(tPayTypeFlag))
		        {
		        	tRenewCount = 0;
		        }
		        tLACommisionNewSchema.setReNewCount(tRenewCount);
		        tLACommisionNewSchema.setPayCount(tLJAPayPersonSchema.getPayCount());
		        tLACommisionNewSchema.setAgentType(tLJAPayPersonSchema.getAgentType());
		        tLACommisionNewSchema.setAgentCode(tLJAPayPersonSchema.getAgentCode());
		        tLACommisionNewSchema.setBranchCode(tLAAgentSchema.getBranchCode());
		        tLACommisionNewSchema.setBranchSeries(tLABranchGroupSchema.getBranchSeries());
		        tLACommisionNewSchema.setAgentGroup(tLAAgentSchema.getAgentGroup());
		        tLACommisionNewSchema.setBranchAttr(tLABranchGroupSchema.getBranchAttr());

		       	tLACommisionNewSchema.setDutyCode(tLJAPayPersonSchema.getDutyCode());
		        tLACommisionNewSchema.setPayPlanCode(tLJAPayPersonSchema.getPayPlanCode());
		        tLACommisionNewSchema.setManageCom(tLJAPayPersonSchema.getManageCom());
		        tLACommisionNewSchema.setOperator(cOperator);
		        tLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
		        tLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
		        tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
		        tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
		        //caigang 2004-09-22添加，用于提取扫描日期
		        String tScanDate = mAgentWageCommonFunction.getScanDate(tLCPolSchema.getPrtNo()); 
		        tLACommisionNewSchema.setScanDate(tScanDate);
		        //薪资月算法相关字段处理
		        String tVisitTime  = mAgentWageCommonFunction.queryReturnVisit(tContNo);
		        String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
				String tFlag = "01";
				if(tRenewCount>0)
                {
                    tFlag="02";
                }
                //简易险
                if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
                {
                    tFlag="02";
                }
                String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLCPolSchema.getGrpContNo(),"ZC",tLACommisionNewSchema.getTMakeDate(),
                        tLJAPayPersonSchema.getConfDate(),tLJAPayPersonSchema.getPayNo());
                //用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
                String tAFlag =tConfDates[0];
                //财务确认日期 ，团单会用到此字段
                String tConfDate = tConfDates[1];
                //根据薪资月函数 得到薪资月
                String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, tFlag, tAFlag, tVisitTime, tRiskFlag);
                tLACommisionNewSchema.setCalDate(tCalDate);
                if(!"".equals(tCalDate)&&null!=tCalDate)
                {
                	String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate);
                	tLACommisionNewSchema.setWageNo(tWageNo);
                }
                LACommisionNewSet tExtractLACommisionNewSet = new LACommisionNewSet();
                tExtractLACommisionNewSet = mAgentWageCommonFunction.dealExtractData(tLACommisionNewSchema, tLCPolSchema, tPayType,tLJAPayPersonSchema.getGetNoticeNo());
                if(0>=tExtractLACommisionNewSet.size())
                {
                	tLACommisionNewSet.add(tLACommisionNewSchema);
                }
                else
                {
                	tLACommisionNewSet.add(tExtractLACommisionNewSet);
                }
			}
			if(0<tLACommisionNewSet.size())
			{
				tMMap.put(tLACommisionNewSet, "INSERT");
			}
			if(0<tLACommisionErrorSet.size())
			{
				tMMap.put(tLACommisionErrorSet, "INSERT");
			}
			if(0==tMMap.size())
			{
				continue;
			}
			if (!mAgentWageCommonFunction.dealDataToTable(tMMap)) {
	        	// @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "AgentWageOfLJAPayPersonData";
	            tError.functionName = "PubSubmit";
	            tError.errorMessage = "数据插入到LACommisionNew中失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
		}
		while(tLJAPayPersonSet.size()>0);
		System.out.println("AgentWageOfLJAPayPersonData-->dealData:执行完成");
		return true;
	}
	
}
