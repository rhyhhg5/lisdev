package com.sinosoft.lis.agentcalculate;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class BankChargeQueryBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;

    private String mOutXmlPath = null;
    
    private String mType = null;

    SSRS tSSRS=new SSRS();
    public BankChargeQueryBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        System.out.println(mType);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "BankChargeQueryBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
    	String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
        if(mType.equals("Charge")){

            mToExcel[0][0] = "银代手续费明细表";

            mToExcel[1][0] = "管理机构";
            mToExcel[1][1] = "代理机构";
            mToExcel[1][2] = "代理机构名称";
            mToExcel[1][3] = "合同号";
            mToExcel[1][4] = "业务员姓名";
            mToExcel[1][5] = "投保人姓名";
            mToExcel[1][6] = "险种";        
            mToExcel[1][7] = "保费";
            mToExcel[1][8] = "手续费比例";
            mToExcel[1][9] = "手续费";
            mToExcel[1][10] = "缴费方式";  
            mToExcel[1][11] = "缴费时间";
            mToExcel[1][12] = "手续费给付号";
            mToExcel[1][13]= "手续费结算操作日期";
            //mToExcel[1][14] = "回访成功日期";
            mToExcel[1][14] = "财务实付状态";
            mToExcel[1][15] = "财务实付日期";
            mToExcel[1][16] = "回访成功日期";
        }
        if(mType.equals("Commision")){

            mToExcel[0][0] = "银代保费明细表";

            mToExcel[1][0] = "银邮机构";
            mToExcel[1][1] = "管理机构";
            mToExcel[1][2] = "营业部";
            mToExcel[1][3] = "销售人员代码";  
            mToExcel[1][4] = "销售人员姓名";
            mToExcel[1][5] = "合同号";
            mToExcel[1][6] = "客户姓名";
            mToExcel[1][7] = "险种代码";
            mToExcel[1][8] = "保费";  
            mToExcel[1][9] = "到帐日";
            mToExcel[1][10] = "生效日";
            mToExcel[1][11] = "满期日";

        }
       


        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "BankChargeQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mType = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null|| mType == null)
        {
            CError tError = new CError();
            tError.moduleName = "BankChargeQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

  

   
    public static void main(String[] args)
    {
        BankChargeQueryBL BankChargeQueryBL = new
            BankChargeQueryBL();
    System.out.println("11111111");
    }
}
