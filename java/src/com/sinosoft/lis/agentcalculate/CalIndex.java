/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessIndexDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAssessIndexSchema;
import com.sinosoft.lis.vschema.LAAssessIndexSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

/*
 * <p>Title: 指标计算类 </p>
 * <p>Description: 通过传入的指标信息（schema）计算指标，得到结果 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author wuwei
 * @version 1.0
 * @date 2003-01-12
 * @modified by WuHao 2007-03
 */
public class CalIndex
{
    public CErrors mErrors = new CErrors(); //错误处理类
    //指标计算结果存放表,取消静态变量设置，改用私有变量，通过get和set方法保持不或很少重复。
    private LAAssessIndexSet mResultIndexSet = new LAAssessIndexSet();
    public String IndexSet; //计算用到的指标字符串
    private String IndexCalNo; //指标计算编码
    private String AgentCode; //代理人编码
    private String Operator; //操作员
    private String IndexCalPrpty; //指标计算属性
    public LAAssessIndexSet mCalIndexSet = new LAAssessIndexSet(); //中间计算过程表(指标集分解生成的指标)
    private LAAssessIndexSchema mCalIndex = new LAAssessIndexSchema(); //指标计算中间存放表
    private LAAssessIndexSchema mAssessIndex = new LAAssessIndexSchema(); //当前需要计算的指标存放表
    public AgCalBase tAgCalBase = new AgCalBase(); //基本参数
    /**计算必要属性设置*/
    //将指标加入计算指标集中
    public void setAssessIndex(LAAssessIndexSchema cAssessIndex)
    {
        mAssessIndex.setSchema(cAssessIndex);
    }

    //计算批次编码
    public void setIndexCalNo(String tStr)
    {
        IndexCalNo = tStr.trim();
    }

    //计算代理人编码
    public void setAgentCode(String cAgentCode)
    {
        AgentCode = cAgentCode.trim();
    }

    //操作员
    public void setOperator(String cOperator)
    {
        Operator = cOperator;
    }

    //计算属性设置
    public void setIndexCalPrpty(String cIndexCalPrpty)
    {
        IndexCalPrpty = cIndexCalPrpty;
    }

    //计算指标计算结果设置
    public void setResultIndexSet(LAAssessIndexSet cResultIndexSet)
    {
        mResultIndexSet.set(cResultIndexSet);
    }

    public LAAssessIndexSet getResultIndexSet()
    {
        return mResultIndexSet;
    }

    public void clearIndexSet()
    {
        mResultIndexSet.clear();
    }

    //指标集个数
    public int getIndexSetSize()
    {
        return mResultIndexSet.size();
    }

    //计算,返回指标结果
    public String Calculate()
    {
        //计算前需准备的参数:
        //tAgCalBase中TempBegin,AgentCode
        //AssessIndex
        //IndexCalNo
        //AgentCode
        System.out.println("---------- CalIndex.Calculate-----------");
        System.out.println("TempBegin in CalIndex:" +
                           this.tAgCalBase.getTempBegin());
        System.out.println("agentcode in CalIndex:" +
                           this.tAgCalBase.getAgentCode());
        String tStr = "";
        setIndexCalPrpty(mAssessIndex.getCalPrpty());
        boolean tBeenCalFlag = false;
        int i, j;
        String tReturn = "0";

        tStr = mAssessIndex.getIndexSet();
        if (tStr == null)
        {
            tStr = "";
        }
        System.out.println("AssessIndex中IndexSet:" + tStr);
        //长度非零的需要做分解,并检查是否合法
        if (!(tStr.trim().length() == 0))
        {
            if (!this.splitIndexSet(tStr))
            {
                return "0";
            }
            try
            {
                for (i = 1; i <= mCalIndexSet.size(); i++)
                {
                    mCalIndex = new LAAssessIndexSchema();
                    mCalIndex = mCalIndexSet.get(i);
                    System.out.println("setIndex:" + mCalIndex.getIndexCode() +
                                       ",IndexCalPrpty:" + IndexCalPrpty);
                    if (!mCalIndex.getCalPrpty().trim().equals(IndexCalPrpty))
                    {
                        System.out.println("指标计算属性不符，跳过");
                        continue;
                    }
                    System.out.println("mResultIndexSet.size:" +
                                       mResultIndexSet.size());
                    if (mResultIndexSet.size() > 0)
                    {
                        for (j = 1; j <= mResultIndexSet.size(); j++)
                        {
                            System.out.println(mCalIndex.getIndexCode() + "--" +
                                               mResultIndexSet.get(j).
                                               getIndexCode());
                            if (mCalIndex.getIndexCode().equalsIgnoreCase(
                                    mResultIndexSet.get(j).getIndexCode()))
                            {
                                tBeenCalFlag = true;
                                break;
                            }
                        }
                        if (tBeenCalFlag == true)
                        { //指标已经计算过
                            tBeenCalFlag = false;
                            System.out.println("--指标已计算：" + AgentCode);
                            continue;
                        }
                    }
                    System.out.println("--IndexCalNo" + IndexCalNo);
                    System.out.println("--AgentCode" + AgentCode);
                    System.out.println("--TableName:" + mCalIndex.getITableName());
                    System.out.println("--Column Name:" + mCalIndex.getIColName());
                    //对未计算过的指标
                    //得到已经保存到指标表中的指标值
                    /*
                    tReturn = getIndexfromTab(IndexCalNo, AgentCode,
                                              mCalIndex.getITableName(),
                                              mCalIndex.getIColName());
                    System.out.println("出来！！！" + tReturn + "aa");
                    */
                    tReturn = "";
                    if ((tReturn == null) || tReturn.trim().equals("") ||
                        tReturn.equals("0"))
                    {
                        //尚未保存到指标表的指标值计算
                        CalIndex tSubCalIndex = new CalIndex();
                        setAgCalBase(tSubCalIndex);
                        tSubCalIndex.setAssessIndex(mCalIndex);
                        tSubCalIndex.setAgentCode(AgentCode);
                        tSubCalIndex.setIndexCalNo(IndexCalNo);
                        tSubCalIndex.setOperator(Operator);
                        tSubCalIndex.setIndexCalPrpty(IndexCalPrpty);
                        //xjh Add,2005/02/04,将结果集传入
                        tSubCalIndex.setResultIndexSet(mResultIndexSet);
                        //计算
                        tReturn = tSubCalIndex.Calculate();
                        if (tSubCalIndex.mErrors.needDealError())
                        {
                            // @@错误处理
                            this.mErrors.addOneError(tSubCalIndex.mErrors.getFirstError());
                            CError.buildErr(this,"指标"+tSubCalIndex.mAssessIndex.getIndexCode()+"计算出错。");
                            return "0";
                        }
                        //将结果集返回
                        //mResultIndexSet = tSubCalIndex.getResultIndexSet();
                        mResultIndexSet.set(tSubCalIndex.getResultIndexSet());
                    }
                    mCalIndex.setDefaultValue(tReturn);
                    mResultIndexSet.add(mCalIndex);
                }
            }
            catch (Exception ex)
            {
                // @@错误处理
                System.out.println("exception2:" + ex.getMessage());
                CError.buildErr(this,"指标"+mCalIndex.getIndexCode()+"计算出错！");
                return "0";
            }
        }
        //该指标对应的指标集为空，计算指标集
        Calculator tCal = new Calculator();
        if ((mAssessIndex.getCalCode() == null) ||
            (mAssessIndex.getCalCode().equals("")))
        {
            return "null";
        }
        //设置计算编码
        System.out.println("计算编码：" + mAssessIndex.getCalCode());
        tCal.setCalCode(mAssessIndex.getCalCode());
        //增加基本要素
        //tCal.addBasicFactor("agntcd", "J001" );
        addAgCalBase(tCal);
        tReturn = tCal.calculate();
//        System.out.println("计算结果:  " + tReturn);
        if (tCal.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tCal.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalIndex";
            tError.functionName = "Calculator";
            tError.errorMessage = "计算指标要素：失败!";
            this.mErrors.addOneError(tError);
            return "0";
        }
        if ((tReturn == null) || (tReturn.trim().equals("")))
        {
            tReturn = "0";
        }
        //else
        // {
        /**插入指标信息表
         */
        if (mAssessIndex.getITableName() == null ||
            mAssessIndex.getITableName().equals("") ||
            mAssessIndex.getITableName().equals("laassess"))
        {
//            System.out.println("考评信息表不在计算指标类中插入！");
            return tReturn;
        }
        if (!insertIndex(IndexCalNo, AgentCode, mAssessIndex.getITableName(),
                         mAssessIndex.getIColName(), tReturn,
                         mAssessIndex.getIndexType()))
        {
//            System.out.println("插入指标信息表出错！！");
            return "0";
        }
        return tReturn;
        // }
    }


    //拆分指标串生成指标集
    public boolean splitIndexSet(String tStr)
    {
        String tStr1 = "";
        int i = 0;
        mCalIndexSet.clear();
        tStr1 = tStr;
        while (true)
        {
            i = i + 1;
            tStr1 = PubFun.getStr(tStr, i, ",");
            if (tStr1.trim().equals(""))
            {
                break;
            }
            try
            {
                LAAssessIndexDB tAIndexDB1 = new LAAssessIndexDB();
                tAIndexDB1.setIndexCode(tStr1);
                if (!tAIndexDB1.getInfo())
                {
                    //@错误处理
                    CError tError = new CError();
                    tError.moduleName = "CalIndex";
                    tError.functionName = "SplitIndexSet";
                    tError.errorMessage = "指标代码非法!" + tStr1.trim();
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mCalIndexSet.add(tAIndexDB1.getSchema());
            }
            catch (Exception ex)
            {
                //@错误处理
                CError tError = new CError();
                tError.moduleName = "CalIndex";
                tError.functionName = "SplitIndexSet";
                tError.errorMessage = "指标集分解错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
//        System.out.println("指标集添加成功！");
        return true;
    }

    private String getIndexfromTab(String cIndexCalNo, String cAgentCode,
                                   String cTableName, String cIColName)
    {
        String tSQL = "", tReturn = "";
//        System.out.println("AssessType:" + this.tAgCalBase.getAssessType());
        int i = Integer.parseInt(this.tAgCalBase.getAssessType()) + 1;
        tSQL = "select " + cIColName + " from " + cTableName
               + " where IndexCalNo = '" + cIndexCalNo + "' and AgentCode = '" +
               cAgentCode + "'"
               + " and IndexType = '0" + String.valueOf(i) + "'";
//        System.out.println("Calindex/getIndexfromTab/sql:" + tSQL);
        ExeSQL tExeSQL = new ExeSQL();
        tReturn = tExeSQL.getOneValue(tSQL);
//        System.out.println("Calindex/getIndexfromTab/sql结果:" + tReturn);
        if (tExeSQL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "CalIndex";
            tError.functionName = "getIndexfromTab";
            tError.errorMessage = "执行SQL语句：从指标表中取值失败!";
            this.mErrors.addOneError(tError);
            System.out.println("error:" + mErrors.getFirstError());
            return "0";
        }
        return tReturn;
    }

    private boolean insertIndex(String cIndexCalNo, String cAgentCode,
                                String cTableName, String cIColName,
                                String cIndexValue, String cIndexType)
    {
        String tSQL = "", tCount = "";
        boolean tReturn = false;
        String IndexAssessType = "";
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        //确定是 佣金01 维持考核02 晋升考核03
        if (cIndexType.trim().equals("02"))
        {
            if (this.tAgCalBase.getAssessType().equals("01"))
            {
                IndexAssessType = "02";
            }
            else
            {
                IndexAssessType = "03";
            }
        }
        else
        {
            //IndexAssessType = "01";
            IndexAssessType = cIndexType;
        }
        ExeSQL tExeSQL = new ExeSQL();
        //先判断记录是否已存在
        tSQL = "select AgentCode from " + cTableName + " where IndexCalNo = '" +
               cIndexCalNo
               + "' and AgentCode = '" + cAgentCode + "' and IndexType = '" +
               IndexAssessType + "'";
        System.out.println("插入指标信息表的SQL:  " + tSQL);
        tCount = tExeSQL.getOneValue(tSQL);
        System.out.println("该代理人的指标记录是否已存在：" + tCount);
        if (tCount.trim().equals(""))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(cAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                //@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalIndex";
                tError.functionName = "insertIndex";
                tError.errorMessage = "代理人代码非法!" + cAgentCode.trim();
                this.mErrors.addOneError(tError);
                return false;
            }

            if (!cTableName.equals("laindexinfotemp"))
            {
                tSQL = "insert into " + cTableName +
                       "( IndexCalNo,AgentCode,AgentGroup,ManageCom,"
                       + "IndexType,StartDate,StartEnd," + cIColName +
                       ",Operator,makeDate,makeTime,"
                       +
                       "modifyDate,modifyTime,AssessMonth,AgentGrade,BranchAttr,BranchSeries,BranchType,BranchType2)"
                       + " values('" + cIndexCalNo + "','" + cAgentCode + "','" +
                       tLAAgentDB.getAgentGroup() + "','"
                       + tLAAgentDB.getManageCom() + "','" + IndexAssessType +
                       "','" + tAgCalBase.getTempBegin() + "','"
                       + tAgCalBase.getTempEnd() + "'," + cIndexValue + ",'" +
                       Operator + "','" + currentDate + "','"
                       + currentTime + "','" + currentDate + "','" +
                       currentTime + "','" + cIndexCalNo + "','"
                       + this.tAgCalBase.getAgentGrade() + "','" +
                       this.tAgCalBase.getZSGroupBranchAttr() + "' ,'"+
                       this.tAgCalBase.getZSGroupBranchSeries() + "' ,'"+
                       this.tAgCalBase.getBranchType() + "' ,'"+
                       this.tAgCalBase.getBranchType2() + "')";
            }
            else
            { //临时指标信息表
                tSQL = "insert into " + cTableName +
                       "( IndexCalNo,AgentCode,AgentGroup,ManageCom,StatDate,StartDate,EndDate," +
                       cIColName +
                       ",Operator,makeDate,makeTime,modifyDate,modifyTime,AssessMonth)"
                       + " values('" + cIndexCalNo + "','" + cAgentCode + "','" +
                       tLAAgentDB.getAgentGroup() + "','" +
                       tLAAgentDB.getManageCom() + "','"
                       + tAgCalBase.getTempEnd() + "','" +
                       tAgCalBase.getTempBegin() + "','" +
                       tAgCalBase.getTempEnd() + "','"
                       + cIndexValue + "','" + Operator + "','" + currentDate +
                       "','" + currentTime + "','" + currentDate + "','" +
                       currentTime + "','" + cIndexCalNo + "')";
            }
            System.out.println("插入记录的SQL: " + tSQL);
            tExeSQL = new ExeSQL();
            tReturn = tExeSQL.execUpdateSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalIndex";
                tError.functionName = "insertIndex";
                tError.errorMessage = "执行SQL语句：判断记录是否存在失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return tReturn;
        }
        else
        {
            tSQL = "update " + cTableName + " set " + cIColName + "=" +
                   cIndexValue + ",modifyDate = '" + currentDate +
                   "',modifyTime = '" + currentTime + "'"
                   + " where IndexCalNo = '" + cIndexCalNo +
                   "' and AgentCode = '" + cAgentCode + "'"
                   + " and IndexType = '" + IndexAssessType + "'";
            System.out.println("更新记录的SQL: " + tSQL);
            tExeSQL = new ExeSQL();
            tReturn = tExeSQL.execUpdateSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalIndex";
                tError.functionName = "insertIndex";
                tError.errorMessage = "执行SQL语句：向指标表中插入记录失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return tReturn;
        }
    }

    private void addAgCalBase(Calculator cCal)
    {
        cCal.addBasicFactor("AgentCode", tAgCalBase.getAgentCode());
        cCal.addBasicFactor("WageNo", tAgCalBase.getWageNo());
        cCal.addBasicFactor("IndexCalNo", tAgCalBase.getWageNo());
        cCal.addBasicFactor("WageCode", tAgCalBase.getWageCode());
        cCal.addBasicFactor("AgentGrade", tAgCalBase.getAgentGrade());
        cCal.addBasicFactor("AgentGroup", tAgCalBase.getAgentGroup());
        cCal.addBasicFactor("BranchCode", tAgCalBase.getBranchCode());
        cCal.addBasicFactor("MonthBegin", tAgCalBase.getMonthBegin());
        cCal.addBasicFactor("MonthEnd", tAgCalBase.getMonthEnd());
        cCal.addBasicFactor("QuauterBegin", tAgCalBase.getQuauterBegin());
        cCal.addBasicFactor("QuauterEnd", tAgCalBase.getQuauterEnd());
        cCal.addBasicFactor("HalfYearBegin", tAgCalBase.getHalfYearBegin());
        cCal.addBasicFactor("HalfYearEnd", tAgCalBase.getHalfYearEnd());
        cCal.addBasicFactor("YearBegin", tAgCalBase.getYearBegin());
        cCal.addBasicFactor("YearEnd", tAgCalBase.getYearEnd());
        cCal.addBasicFactor("MonthMark", tAgCalBase.getMonthMark());
        cCal.addBasicFactor("QuauterMark", tAgCalBase.getQuauterMark());
        cCal.addBasicFactor("HalfYearMark", tAgCalBase.getHalfYearMark());
        cCal.addBasicFactor("YearMark", tAgCalBase.getYearMark());
        cCal.addBasicFactor("AreaType", tAgCalBase.getAreaType());
        cCal.addBasicFactor("TempBegin", tAgCalBase.getTempBegin());
        cCal.addBasicFactor("TempEnd", tAgCalBase.getTempEnd());
        cCal.addBasicFactor("WageNoBegin", tAgCalBase.getWageNoBegin());
        cCal.addBasicFactor("WageNoEnd", tAgCalBase.getWageNoEnd());
        cCal.addBasicFactor("Rate", String.valueOf(tAgCalBase.getRate()));
        cCal.addBasicFactor("A1State", String.valueOf(tAgCalBase.getA1State()));
        cCal.addBasicFactor("AssessType", tAgCalBase.getAssessType());
        cCal.addBasicFactor("LimitPeriod", tAgCalBase.getLimitPeriod());
        cCal.addBasicFactor("DestAgentGrade", tAgCalBase.getDestAgentGrade());
        cCal.addBasicFactor("BranchAttr", tAgCalBase.getBranchAttr());
        cCal.addBasicFactor("BranchSeries",tAgCalBase.getZSGroupBranchSeries());
        cCal.addBasicFactor("BranchType",tAgCalBase.getBranchType());
        cCal.addBasicFactor("BranchType2",tAgCalBase.getBranchType2());
        cCal.addBasicFactor("FirstAssessMark",String.valueOf(tAgCalBase.FirstAssessMark));
        cCal.addBasicFactor("ChannelType", tAgCalBase.getChannelType());
        cCal.addBasicFactor("ManageCom", tAgCalBase.getManageCom());
        System.out.println("TempBegin:" + tAgCalBase.getTempBegin());
        System.out.println("YearBegin:" + tAgCalBase.getYearBegin());
        // add new 团队建设
        cCal.addBasicFactor("GbuildFlag", tAgCalBase.getGbuildFlag());
        cCal.addBasicFactor("GbuildStartDate", tAgCalBase.getGbuildStartDate());
    }

    private void setAgCalBase(CalIndex cCal)
    {
        cCal.tAgCalBase.setAgentCode(tAgCalBase.getAgentCode());
        cCal.tAgCalBase.setWageNo(tAgCalBase.getWageNo());
        cCal.tAgCalBase.setWageCode(tAgCalBase.getWageCode());
        cCal.tAgCalBase.setAgentGrade(tAgCalBase.getAgentGrade());
        cCal.tAgCalBase.setAgentGroup(tAgCalBase.getAgentGroup());
        cCal.tAgCalBase.setBranchCode(tAgCalBase.getBranchCode());
        cCal.tAgCalBase.setMonthBegin(tAgCalBase.getMonthBegin());
        cCal.tAgCalBase.setMonthEnd(tAgCalBase.getMonthEnd());
        cCal.tAgCalBase.setQuauterBegin(tAgCalBase.getQuauterBegin());
        cCal.tAgCalBase.setQuauterEnd(tAgCalBase.getQuauterEnd());
        cCal.tAgCalBase.setHalfYearBegin(tAgCalBase.getHalfYearBegin());
        cCal.tAgCalBase.setHalfYearEnd(tAgCalBase.getHalfYearEnd());
        cCal.tAgCalBase.setYearBegin(tAgCalBase.getYearBegin());
        cCal.tAgCalBase.setYearEnd(tAgCalBase.getYearEnd());
        cCal.tAgCalBase.setAreaType(tAgCalBase.getAreaType());
        cCal.tAgCalBase.setMonthMark(tAgCalBase.getMonthMark());
        cCal.tAgCalBase.setHalfYearMark(tAgCalBase.getHalfYearMark());
        cCal.tAgCalBase.setQuauterMark(tAgCalBase.getQuauterMark());
        cCal.tAgCalBase.setYearMark(tAgCalBase.getYearMark());
        cCal.tAgCalBase.setTempBegin(tAgCalBase.getTempBegin());
        cCal.tAgCalBase.setTempEnd(tAgCalBase.getTempEnd());
        cCal.tAgCalBase.setWageNoBegin(tAgCalBase.getWageNoBegin());
        cCal.tAgCalBase.setWageNoEnd(tAgCalBase.getWageNoEnd());
        cCal.tAgCalBase.setRate(tAgCalBase.getRate());
        cCal.tAgCalBase.setA1State(tAgCalBase.getA1State());
        cCal.tAgCalBase.setLimitPeriod(tAgCalBase.getLimitPeriod());
        cCal.tAgCalBase.setAssessType(tAgCalBase.getAssessType());
        cCal.tAgCalBase.setDestAgentGrade(tAgCalBase.getDestAgentGrade());
        cCal.tAgCalBase.setBranchAttr(tAgCalBase.getBranchAttr());
        cCal.tAgCalBase.setChannelType(tAgCalBase.getChannelType());
        cCal.tAgCalBase.FirstAssessMark = tAgCalBase.FirstAssessMark;
        cCal.tAgCalBase.setManageCom(tAgCalBase.getManageCom());
        cCal.tAgCalBase.setZSGroupBranchAttr(tAgCalBase.getZSGroupBranchAttr());
        cCal.tAgCalBase.setZSGroupBranchSeries(tAgCalBase.getZSGroupBranchSeries());
        cCal.tAgCalBase.setBranchType(tAgCalBase.getBranchType());
        cCal.tAgCalBase.setBranchType2(tAgCalBase.getBranchType2());
        // add new 团队建设
        cCal.tAgCalBase.setGbuildFlag(tAgCalBase.getGbuildFlag());
        cCal.tAgCalBase.setGbuildStartDate(tAgCalBase.getGbuildStartDate());
    }
}
