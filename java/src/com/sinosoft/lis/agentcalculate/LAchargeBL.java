package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.agentcharge.LAChargeCheckBL;
import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LAchargeBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2017</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author zyy
 * @version 1.0----2017-07-10
 */
public class LAchargeBL {
  //错误处理类
  public static CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */ 
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAChargeSet mLAChargeSet = new LAChargeSet();
  private LAChargeSet mUpLAChargeSet = new LAChargeSet();
  private LJAGetSet mLJAGetSet=new LJAGetSet();
  private String mManageCom="";
  private String mAgentCom="";
  private String mStartDate="";
  private String mEndDate="";
  private String mContNo="";
  private String mContType = "";
  private String mBackSign ="";
  private String mBatchNo ="";
  private String mOperator="";
  private String mchargetype="";
  private String mBranchtype="";
  private String mBranchtype2="";
  public LAchargeBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAchargeBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
//  如果是江苏中介，对于其时行 校验
//    if(!checkChargePay())
//    {
//    	return false;
//    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    
    if (!tPubSubmit.submitData(mInputData, "UPDATE||MAIN")) {
      // @@错误处理
      mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAchargeBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
    	mErrors.clearErrors();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
      this.mOperator = this.mGlobalInput.Operator;
      System.out.println("008:"+this.mOperator);
     if(this.mOperate.equals("SELECTPAY")){
    	 this.mUpLAChargeSet.set( (LAChargeSet) cInputData.getObjectByObjectName("LAChargeSet",0));
    	 //this.mLAChargeSet.set( (LAChargeSet) cInputData.getObjectByObjectName("LAChargeSet",0));
    	 System.out.println("LAChargeSet get:"+mUpLAChargeSet.size());
         this.mManageCom = this.mUpLAChargeSet.get(1).getManageCom();
         System.out.println("!!!!!:"+this.mManageCom);
         this.mContType=(String)cInputData.getObjectByObjectName("String",0);
         System.out.println("@@@@:"+this.mContType);
         this.mAgentCom =this.mUpLAChargeSet.get(1).getAgentCom();
         System.out.println("#####:"+this.mAgentCom);
         this.mchargetype=this.mUpLAChargeSet.get(1).getChargeType();
         System.out.println("$$$$:"+this.mchargetype);
     }
     else if(this.mOperate.equals("ALLPAY")){
    	 this.mManageCom=(String)cInputData.get(1);
    	 System.out.println("hello:"+this.mManageCom);
    	 this.mAgentCom=(String)cInputData.get(2);
    	 System.out.println("John:"+this.mAgentCom);
    	 this.mStartDate=(String)cInputData.get(3);
    	 System.out.println("Smith:"+this.mStartDate);
    	 this.mEndDate=(String)cInputData.get(4);
    	 System.out.println("Bill:"+this.mEndDate);
    	 this.mContNo=(String)cInputData.get(5);
    	 System.out.println("Jones:"+this.mContNo);
    	 this.mContType=(String)cInputData.get(6);
    	 System.out.println("Helen:"+this.mContType);
    	 this.mBackSign = (String)cInputData.get(7);
    	 System.out.println("Green:"+this.mBackSign);
    	 this.mchargetype= (String)cInputData.get(8);
    	 System.out.println("blue:"+this.mchargetype);
    	 this.mBranchtype= (String)cInputData.get(9);
    	 System.out.println("red:"+this.mBranchtype);
    	 this.mBranchtype2= (String)cInputData.get(10);
    	 System.out.println("yellow:"+this.mBranchtype2);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAchargeBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 新增，修改传入的数据校验
   */
  private boolean check(){
	  //不需要数据校验
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    try {
    	System.out.println("%%%%%%%%%%");
    	LAChargeDB tLAChargeDB=new LAChargeDB();
    	BalanceReturnVisit tBalanceReturnVisit = new BalanceReturnVisit();
        LACommisionSet tLACommisionSet=null;
        LACommisionDB tLACommisionDB=new LACommisionDB();
        //String tLimit = null;
       
        if (mOperate.equals("SELECTPAY")) {
          LAChargeSet tLAChargeSet ;
          for (int i = 1; i <= mUpLAChargeSet.size(); i++) {
          tLAChargeSet=new LAChargeSet();
          String tSQL="select * from lacharge where commisionsn='"+mUpLAChargeSet.get(i).getCommisionSN()+"' and   charge<>0 with ur";
          System.out.println(tSQL);
          tLAChargeSet=tLAChargeDB.executeQuery(tSQL);
          System.out.println("lily:"+tLAChargeSet.size());
          for(int j=1;j<=tLAChargeSet.size();j++){
        	  System.out.println("lucy");
        	  tLAChargeSet.get(j).setChargeState("1");
        	  tLAChargeSet.get(j).setBatchNo(this.mBatchNo);
        	  tLAChargeSet.get(j).setModifyDate(CurrentDate);
        	  tLAChargeSet.get(j).setModifyTime(CurrentTime);
        	  tLAChargeSet.get(j).setOperator(this.mGlobalInput.Operator);
        	 
        	  
        	  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
              //tLimit = PubFun.getNoLimit(tLAChargeSet.get(j).getManageCom());
              //tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("BCGETNO", tLimit));
        	  tLJAGetSchema.setActuGetNo(tLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setOtherNo(tLAChargeSet.get(j).getReceiptNo());
              if((tLAChargeSet.get(j).getBranchType().equals("5")&&tLAChargeSet.get(j).getBranchType2().equals("01"))||
            	(tLAChargeSet.get(j).getBranchType().equals("3")&&tLAChargeSet.get(j).getBranchType2().equals("01"))){
              tLJAGetSchema.setOtherNoType("BC");
              System.out.println("---------");
              }
              else if((tLAChargeSet.get(j).getBranchType().equals("2")&&tLAChargeSet.get(j).getBranchType2().equals("02"))||
            	(tLAChargeSet.get(j).getBranchType().equals("1")&&tLAChargeSet.get(j).getBranchType2().equals("02"))){
            	  tLJAGetSchema.setOtherNoType("AC");
            	  System.out.println("+++++++++");
              }
              tSQL="select * from lacommision where receiptno='"
        		  +tLAChargeSet.get(j).getReceiptNo()+"' and commisionsn='"+tLAChargeSet.get(j).getCommisionSN()+"' fetch first 1 rows only ";
              System.out.println(tSQL);
        	  tLACommisionSet=tLACommisionDB.executeQuery(tSQL);
        	  System.out.println("========");
    		  if (!tBalanceReturnVisit.queryVisit(tLACommisionSet.get(1))){
           		 continue;
           	 }
    		   System.out.println("..........");
        	  tLJAGetSchema.setPayMode("12");
        	  tLJAGetSchema.setManageCom(tLAChargeSet.get(j).getManageCom());
              tLJAGetSchema.setAgentCom(tLAChargeSet.get(j).getAgentCom());
              tLJAGetSchema.setGetNoticeNo(tLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setAgentType(tLACommisionSet.get(1).getAgentType());
              tLJAGetSchema.setAgentCode(tLACommisionSet.get(1).getAgentCode());
              tLJAGetSchema.setAgentGroup(tLACommisionSet.get(1).getAgentGroup());
              tLJAGetSchema.setAppntNo(tLACommisionSet.get(1).getAppntNo());
              tLJAGetSchema.setSumGetMoney(tLAChargeSet.get(j).getCharge());
              tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(1).getAgentCom()));
              tLJAGetSchema.setDrawerID(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setShouldDate(CurrentDate);
              tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
              tLJAGetSchema.setMakeDate(CurrentDate);
              tLJAGetSchema.setMakeTime(CurrentTime);
              tLJAGetSchema.setModifyDate(CurrentDate);
              tLJAGetSchema.setModifyTime(CurrentTime);
              this.mLJAGetSet.add(tLJAGetSchema);
              this.mLAChargeSet.add(tLAChargeSet);
              System.out.println(",,,,,,,,,:"+this.mLAChargeSet.size());
          }  
          System.out.println("mmmmmmmmm");
        }
          System.out.println("hanmei");
          map.put(this.mLAChargeSet, "UPDATE");
          map.put(this.mLJAGetSet, "INSERT");
        System.out.println("nnnnnn");
      }
      else if(mOperate.equals("ALLPAY")){
    	  LAChargeSet tLAChargeSet=new LAChargeSet();
    	  
    	  String tSQL="select * from lacharge where "
        	  +" charge<>0 and branchtype='"+this.mBranchtype+"' and branchtype2='"+this.mBranchtype2+"' and chargestate='0"
        	  +"' and managecom='"+this.mManageCom
        	  +"' and tmakedate between '"
        	  +this.mStartDate+"' and '"+this.mEndDate+"'";
    	  if(this.mBranchtype=="1"&&this.mBranchtype2=="02"){
			  tSQL+=" and chargetype='15'";
		  }
    	  else if(this.mBranchtype=="2"&&this.mBranchtype2=="02"){
    		  tSQL+=" and chargetype='51'";
    	  }
    	  else if(this.mBranchtype=="3"&&this.mBranchtype2=="02"){
    		  tSQL+=" and chargetype='99'";
    	  }
    	  else if(this.mBranchtype=="5"&&this.mBranchtype2=="02"){
    		  tSQL+=" and chargetype='55'";
    	  }
    	  if(this.mAgentCom!=null&&!this.mAgentCom.equals("")){
    		  tSQL+=" and agentcom  like '"+this.mAgentCom+"%'";
    	  }
    	  if(this.mContNo!=null&&!this.mContNo.equals("")){
    		  tSQL+=" and contno ='"+this.mContNo+"'";
    	  }
    	  if(this.mContType!=null&&!this.mContType.equals("")){
    		  if("0".equals(this.mContType))
    		  {
    			  tSQL+=" and exists(select 1 from ljapay where  incomeno in ( select grpcontno from lacommision where commisionsn = lacharge.commisionsn) and incometype ='1' )";  
    		  }else if("1".equals(this.mContType))
    		  {
    			  tSQL+=" and exists(select 1 from ljapay where  incomeno in ( select contno from lacommision where commisionsn = lacharge.commisionsn) and incometype ='2' )";    
    		  }else{
    			  
    		  }
    	  }
    	  if(this.mBackSign!=null&&!this.mBackSign.equals("")){
    		  if("1".equals(mBackSign)){
    			  tSQL += " and exists(select 1 from db2inst1.returnvisittable  where policyno in (select contno from lacommision where commisionsn = lacharge.commisionsn))";
    			  
    		  }else if("2".equals(mBackSign)){
    			  tSQL +=" and not exists(select 1 from db2inst1.returnvisittable where policyno in (select contno from lacommision where commisionsn = lacharge.commisionsn))";
    		  }else{
    			  
    		  }
    	  }
    	  tSQL+=" with ur";
    	  System.out.println(tSQL);
    	  tLAChargeSet=tLAChargeDB.executeQuery(tSQL);
          for(int j=1;j<=tLAChargeSet.size();j++){    		  
        	  LAChargeSchema t_LAChargeSchema = new LAChargeSchema();
        	  t_LAChargeSchema=tLAChargeSet.get(j).getSchema();
        	  t_LAChargeSchema.setChargeState("1");
        	  t_LAChargeSchema.setBatchNo(this.mBatchNo);
        	  t_LAChargeSchema.setModifyDate(CurrentDate);
        	  t_LAChargeSchema.setModifyTime(CurrentTime);
        	  t_LAChargeSchema.setOperator(this.mGlobalInput.Operator);
    		  
    		  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
              //tLimit = PubFun.getNoLimit(tLAChargeSet.get(j).getManageCom());
              //tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("BCGETNO", tLimit));
    		  tLJAGetSchema.setActuGetNo(t_LAChargeSchema.getCommisionSN());
              tLJAGetSchema.setOtherNo(tLAChargeSet.get(j).getReceiptNo());
              if((tLAChargeSet.get(j).getBranchType().equals("5")&&tLAChargeSet.get(j).getBranchType2().equals("01"))||
                  (tLAChargeSet.get(j).getBranchType().equals("3")&&tLAChargeSet.get(j).getBranchType2().equals("01"))){
                    tLJAGetSchema.setOtherNoType("BC");
              }
             else if((tLAChargeSet.get(j).getBranchType().equals("2")&&tLAChargeSet.get(j).getBranchType2().equals("02"))||
                  	(tLAChargeSet.get(j).getBranchType().equals("1")&&tLAChargeSet.get(j).getBranchType2().equals("02"))){
                  	tLJAGetSchema.setOtherNoType("AC");
              }
              tSQL="select * from lacommision "
            	  +"where receiptno='" +tLAChargeSet.get(j).getReceiptNo()+"'"
            	  +" and  commisionsn='"+tLAChargeSet.get(j).getCommisionSN()+"' "
            	  +" fetch first 1 rows only ";
              tLACommisionSet=tLACommisionDB.executeQuery(tSQL);
              
              
              // add new 
              String tRiskFlag =queryRiskFlag(tLACommisionSet.get(1).getRiskCode());
              // 查询需要回访日期校验的机构
    		  String comSql ="select '1' from ldcode where codetype = 'yindaihuifang' and code = '"+this.mManageCom+"'" ;
    		  ExeSQL tExeSQL1 = new ExeSQL();
    		  String cResult = tExeSQL1.getOneValue(comSql);
    		   if(cResult.equals("1")){
    			   System.out.println("此机构添加了回访日期校验"+this.mManageCom);
            	  if((tLACommisionSet.get(1).getReNewCount()==0)&&(tLACommisionSet.get(1).getPayYear()==0)&&("Y").equals(tRiskFlag))
            	  {
            		  String check = "select '1' from  ReturnVisitTable where policyno ='"+tLAChargeSet.get(j).getContNo()+"' and returnvisitflag in ('1','4')";
            		  ExeSQL tExeSQL = new ExeSQL();
            		  String tResult = tExeSQL.getOneValue(check);
            		  if(tResult==null||("").equals(tResult))
            		  {
            			  continue;
            		  }
            	  }
              }

    		  
              tLJAGetSchema.setPayMode("12");
              tLJAGetSchema.setManageCom(t_LAChargeSchema.getManageCom());
              tLJAGetSchema.setAgentCom(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setGetNoticeNo(t_LAChargeSchema.getCommisionSN());
              tLJAGetSchema.setAgentType(tLACommisionSet.get(1).getAgentType());
              tLJAGetSchema.setAgentCode(tLACommisionSet.get(1).getAgentCode());
              tLJAGetSchema.setAgentGroup(tLACommisionSet.get(1).getAgentGroup());
              tLJAGetSchema.setAppntNo(tLACommisionSet.get(1).getAppntNo());
              tLJAGetSchema.setSumGetMoney(t_LAChargeSchema.getCharge());
              tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(1).getAgentCom()));
              tLJAGetSchema.setDrawerID(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setShouldDate(CurrentDate);
              tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
              tLJAGetSchema.setMakeDate(CurrentDate);
              tLJAGetSchema.setMakeTime(CurrentTime);
              tLJAGetSchema.setModifyDate(CurrentDate);
              tLJAGetSchema.setModifyTime(CurrentTime);
              this.mLJAGetSet.add(tLJAGetSchema); 
              this.mLAChargeSet.add(t_LAChargeSchema);
              
          }    	  
        map.put(mLAChargeSet, "UPDATE");
        map.put(mLJAGetSet, "INSERT");
      }
      else{
    	  CError tError = new CError();
          tError.moduleName = "LAchargeBL";
          tError.functionName = "dealData";
          tError.errorMessage = "不支持的操作类型,请验证操作类型.";
          mErrors.addOneError(tError);
          return false;
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAchargeBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
    	
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAchargeBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private String getAgentComName(String tAgentCom){
	  String comName = "";
	  ExeSQL tExeSQL=new ExeSQL();
	  String sql=" select name from lacom where agentcom='"+tAgentCom+"' with ur ";
	  SSRS tSSRS = tExeSQL.execSQL(sql);	    	 
	  comName = tSSRS.GetText(1, 1);
	  return comName ;
  }
  
  
//  private MMap lockLJAGet(LAChargeSchema tLAChargeSchema)
//  {
//  	
//  	
//      MMap tmap = null;
//      /**锁定标志"TX"*/
//      String tLockNoType = "TX";
//      /**锁定时间*/
//      String tAIS = "300";
//      System.out.println("设置了60分的解锁时间");
//      TransferData tTransferData = new TransferData();
//      tTransferData.setNameAndValue("LockNoKey", tLAChargeSchema.getCommisionSN());
//      tTransferData.setNameAndValue("LockNoType", tLockNoType);
//      tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
//     
//      VData tVData = new VData();
//      tVData.add(tTransferData);
//      tVData.add(mGlobalInput);
//      LockTableActionBL tLockTableActionBL = new LockTableActionBL();
//      tmap = tLockTableActionBL.getSubmitMap(tVData, null);
//      
//      if (tmap == null)
//      {           	
//          mErrors.copyAllErrors(tLockTableActionBL.mErrors);
//          return null;
//      }
//      return tmap;
//  }
  
  private String queryRiskFlag(String riskcode)
  {
	  
	  String sql = "select 'Y' from ldcode where codetype = 'bankriskcode' and code in (select riskcode from lmriskapp where riskprop='I'  and  RiskPeriod = 'L') and code = '"+riskcode+"' ";
	  ExeSQL tExeSQL = new ExeSQL();
	  String tRiskFlag =tExeSQL.getOneValue(sql);
	  if(tRiskFlag==null||"".equals(tRiskFlag))
	  {
		  tRiskFlag="N"; 
	  }
	  return tRiskFlag;
  }
//  private boolean checkChargePay()
//  { System.out.println("bbbbbbbbbbbb");
//	//江苏中介手续费计算时会进行校验
//  System.out.println("-----------:"+this.mManageCom);
//  if("8632".equals(this.mManageCom.substring(0,4))&&mLAChargeSet.size()>0)
//  {System.out.println("aaaaaaaaaaaaaaaaa");
//   LAChargeCheckBL tLAChargeCheckBL = new LAChargeCheckBL();
//	String hChargeType = "HX";
//  	VData hVData = new VData();
//  	hVData.add(mLAChargeSet);
//  	hVData.add(hChargeType);
//  	hVData.add(this.mBatchNo);
//  	hVData.add(this.mOperator);
//  	hVData.add(this.mContType);
//  	hVData.add(this.mAgentCom);
//		if(!tLAChargeCheckBL.submitData(hVData, ""))
//		{
//			this.mErrors.copyAllErrors(tLAChargeCheckBL.mErrors);
//			CError tError = new CError();
//		tError.moduleName = "LAARiskChargePayBL";
//		tError.functionName = "dealData";
//		tError.errorMessage = "江苏中介手续费结算出错。";
//		this.mErrors.addOneError(tError);
//  		return false;
//		}
//  }
//  return true;
// }
}

