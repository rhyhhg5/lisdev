package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LADiscountBSchema;
import com.sinosoft.lis.schema.LADiscountSchema;
import com.sinosoft.lis.db.LADiscountDB;
import com.sinosoft.lis.vschema.LADiscountBSet;
import com.sinosoft.lis.vschema.LADiscountSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.StrTool;;

public class OperatorIndexMarkBL{
      /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String currentDate = PubFun.getCurrentDate();//当天日期
    private String currentTime = PubFun.getCurrentTime();//当时时间
    private LADiscountSet mLADiscountSet=null;
    private LADiscountBSet mLADiscountBSet=null;
    private String mOperate="";
    private MMap mMap=new MMap();
    public static void main(String[] args)
    {
    }
    public OperatorIndexMarkBL()
    {
        OperatorIndexMarkUI tOperatorIndexMarkUI=new OperatorIndexMarkUI();
    }

    private boolean getInputData(VData cInputData) {
        try {
            mLADiscountSet = (LADiscountSet) cInputData.getObjectByObjectName(
                    "LADiscountSet", 0);
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            if (mLADiscountSet == null || mGlobalInput == null) {
                CError tError = new CError();
                tError.moduleName = "OperatorIndexMarkBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }
        catch (Exception e) {
          // @@错误处理
          CError.buildErr(this, "接收数据失败");
          return false;
        }
    }
    public boolean checkData()
    {
        return true;
    }
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate=cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if(!dealDate())
        {
              CError tError = new CError();
              tError.moduleName = "OperatorIndexMarkBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
        }
        if(!prepareOutputData())
        {
            return false;
        }
         System.out.println("Start OperatorIndexMarkBL Submit...");
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, "")) {
             this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             CError tError = new CError();
             tError.moduleName = "OperatorIndexMarkBL";
             tError.functionName = "submitData";
             tError.errorMessage = "PubSubmit保存数据失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
        return true;
    }
    /**
     * 验证录入的信息
     * @param cLAWageLogSchema
     * @return
     */
    public boolean checkDiscount(LADiscountSchema tLADiscountSchema)
    {
        if(tLADiscountSchema==null){
            return false;
        }
        String sql="select count(1) from LADiscount where discounttype='"+tLADiscountSchema.getDiscountType();
        sql+="' and managecom='"+tLADiscountSchema.getManageCom()+"' and code1="+tLADiscountSchema.getCode1()+" and code2="+tLADiscountSchema.getCode2()+" with ur";
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        if(!tSSRS.GetText(1,1).equals("0")){
            CError.buildErr(this,"录入的信息系统中已经存在，请重新录入！");
            return false;
        }
        return true;
    }
    /**
     * 获得主键信息
     * @param cLAWageLogSchema
     * @return
     */
    public int getIDX(){
        String sql="select max(idx)+1 from LADiscount with ur";
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        String idx=tSSRS.GetText(1,1);
        if(idx!=null && !idx.equals("null")){
            if(StrTool.cTrim(idx).equals("0")){
                return 1;
            }
            return Integer.parseInt(idx);
        }else{
            return 1;
        }
    }

    public boolean dealDate()
    {
        LADiscountSchema tLADiscountbSchema = new LADiscountSchema();
        LADiscountSet tLADiscountSet = new LADiscountSet();
        LADiscountSet tLADiscountbSet = new LADiscountSet();
        if(this.mOperate.equals("INSERT")){
            int tIDX=getIDX();
            for(int i=1;i<=this.mLADiscountSet.size();i++){
                LADiscountSchema tLADiscountSchema=mLADiscountSet.get(i);
                if(tLADiscountSchema.getIdx()==-1){
                    tLADiscountSchema.setMakeDate(this.currentDate);
                    tLADiscountSchema.setMakeTime(this.currentTime);
                    tLADiscountSchema.setModifyDate(this.currentDate);
                    tLADiscountSchema.setModifyTime(this.currentTime);
                    tLADiscountSchema.setOperator(mGlobalInput.Operator);
                    tLADiscountSchema.setDiscountType("03");
                    if(!checkDiscount(tLADiscountSchema)){
                        return false;
                    }
                    tLADiscountSchema.setIdx(tIDX);
                    tIDX++; 
                    tLADiscountSet.add(tLADiscountSchema);
                }
            }
            this.mMap.put(tLADiscountSet, this.mOperate);
        }else if(this.mOperate.equals("UPDATE")){
            for(int i=1;i<=this.mLADiscountSet.size();i++){
                LADiscountSchema tLADiscountSchema=mLADiscountSet.get(i);
                if(tLADiscountSchema.getIdx()!=-1){
                    LADiscountDB tLADiscountDB=new LADiscountDB();
                    tLADiscountDB.setIdx(tLADiscountSchema.getIdx());
                    tLADiscountbSchema=tLADiscountDB.query().get(1);
                    tLADiscountbSet.add(tLADiscountbSchema);
                    
                    tLADiscountSchema.setMakeDate(this.currentDate);
                    tLADiscountSchema.setMakeTime(this.currentTime);
                    tLADiscountSchema.setModifyDate(this.currentDate);
                    tLADiscountSchema.setModifyTime(this.currentTime);
                    tLADiscountSchema.setOperator(mGlobalInput.Operator);
                    tLADiscountSchema.setDiscountType("03");
                    tLADiscountSchema.setIdx(tLADiscountSchema.getIdx());
                    tLADiscountSet.add(tLADiscountSchema);
                }
            } 
//          数据备份
            if(!backupLADiscount(tLADiscountbSet)){
                return false;
            }
            this.mMap.put(tLADiscountSet, this.mOperate);
        }
        
        return true;
    }
    /**
     * 提数日志表备份
     * @param cLAWageLogSchema
     * @return
     */
    public boolean backupLADiscount(LADiscountSet cLADiscountSet)
    {
        if(cLADiscountSet==null){
            CError.buildErr(this,"备份信息失败！");
            return false;
        }
        //循环备份
        for(int i=1;i<=cLADiscountSet.size();i++)
        {
            LADiscountSchema tLADiscountSchema = new LADiscountSchema();
            LADiscountBSchema tLADiscountBSchema = new LADiscountBSchema();
            mLADiscountBSet=new LADiscountBSet();
            Reflections tReflections=new Reflections();  
            tLADiscountSchema = cLADiscountSet.get(i);
            tReflections.transFields(tLADiscountBSchema,tLADiscountSchema);
            tLADiscountBSchema.setMakeDate(currentDate);
            tLADiscountBSchema.setMakeTime(currentTime);
            tLADiscountBSchema.setOperator(this.mGlobalInput.Operator);
            tLADiscountBSchema.setEdorType("11");
            tLADiscountBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
            this.mLADiscountBSet.add(tLADiscountBSchema); 
        }
        mMap.put(mLADiscountBSet, "INSERT");
        return true;
    }

    public boolean prepareOutputData()
    {
         try
         {
             mInputData = new VData();
             mInputData.add(mMap);
         }
         catch(Exception e)
         {
             CError.buildErr(this,"提交数据失败！");
             return false;
         }
         return true;
    }
}

