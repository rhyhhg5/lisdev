package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

public class AgentWageOfLJAPayGrpData extends AgentWageOfAbstractClass 
{
	
	/**
	 *主要提数逻辑 ljapaygrp 
	 * @param cSQL
	 * @param cOperator
	 * @return
	 */
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("*****************团险提数LJAPayGrp AgentWageOfLJAPayGrpData dealData  开始***************");
		LJAPayGrpSet tLJAPayGrpSet = new LJAPayGrpSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAPayGrpSet, cSQL);
		//用来判断数据是不是为空，为了防止 最后一次循环时，为空时，也打印错误语句  
		boolean tJudgeNullFlag = false;
		do {
			MMap tMMap = new MMap();
			tRSWrapper.getData();
			if(!tJudgeNullFlag)
			{
				if(null == tLJAPayGrpSet || 0 == tLJAPayGrpSet.size())
				{
					System.out.println("AgentWageOfLJAPayGrpData-->dealData 没有满足条件的数据");
				}
				tJudgeNullFlag = true;
			}
			LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
			LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
			for (int i = 1; i <= tLJAPayGrpSet.size(); i++) 
			{
				LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
				LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
				tLJAPayGrpSchema = tLJAPayGrpSet.get(i);
				String tGrpContNo = tLJAPayGrpSchema.getGrpContNo();
				String tGrpPolNo = tLJAPayGrpSchema.getGrpPolNo();
				String tAgentCode = tLJAPayGrpSchema.getAgentCode();
				String tPayType = tLJAPayGrpSchema.getPayType();
				String tPayNo = tLJAPayGrpSchema.getPayNo();
				String tRiskCode = tLJAPayGrpSchema.getRiskCode();
				String tEndorsementNo = tLJAPayGrpSchema.getEndorsementNo();
				String tGetNoticeNo = tLJAPayGrpSchema.getGetNoticeNo();
				LCGrpContSchema tLCGrpContSchema = mAgentWageCommonFunction.queryLCGrpCont(tGrpContNo);
				if(null == tLCGrpContSchema)
				{
					LACommisionErrorSchema tLACommisionErrorSchema = 
							dealErrorData(tLJAPayGrpSchema, AgentWageOfCodeDescribe.ERRORCODE_LCGRPCONT, cOperator);
					tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
				LCGrpPolSchema tLCGrpPolSchema = mAgentWageCommonFunction.queryLCGrpPol(tGrpPolNo);
				if(null == tLCGrpPolSchema)
				{
					LACommisionErrorSchema tLACommisionErrorSchema = 
							dealErrorData(tLJAPayGrpSchema, AgentWageOfCodeDescribe.ERRORCODE_LCGRPPOL, cOperator);
					tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
				LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tAgentCode);
				if(null == tLAAgentSchema)
				{
					LACommisionErrorSchema tLACommisionErrorSchema = 
							dealErrorData(tLJAPayGrpSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE, cOperator);
					tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
				//团队
				String tAgentGroup = tLAAgentSchema.getAgentGroup();
				String tBranchCode = tLAAgentSchema.getBranchCode();
				LABranchGroupSchema tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tBranchCode);
				if(null == tLABranchGroupSchema)
				{
					LACommisionErrorSchema tLACommisionErrorSchema = 
							dealErrorData(tLJAPayGrpSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP, cOperator);
					tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
				String tBranchAttr = tLABranchGroupSchema.getBranchAttr();
				if(null == tBranchAttr || "".equals(tBranchAttr))
				{
					LACommisionErrorSchema tLACommisionErrorSchema = 
							dealErrorData(tLJAPayGrpSchema, AgentWageOfCodeDescribe.ERRORCODE_BRANCHATTR, cOperator);
					tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
				//添加对互动的处理
				String tSaleChnl = tLCGrpPolSchema.getSaleChnl();
				String tBranchType3 = "";
				if("14".equals(tSaleChnl)||"15".equals(tSaleChnl))
				{
					String tCrs_SaleChnl = tLCGrpContSchema.getCrs_SaleChnl();
					String tCrs_BussType = tLCGrpContSchema.getCrs_BussType();
					tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
				}
				if("no".equals(tBranchType3))
				{
					LACommisionErrorSchema tLACommisionErrorSchema = 
							dealErrorData(tLJAPayGrpSchema, AgentWageOfCodeDescribe.ERRORCODE_ACTIVE, cOperator);
					tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
				String[] tBranchTypes = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
				if(null == tBranchTypes || 0 == tBranchTypes.length)
				{
					LACommisionErrorSchema tLACommisionErrorSchema = 
							dealErrorData(tLJAPayGrpSchema, AgentWageOfCodeDescribe.ERRORCODE_SALECHNL, cOperator);
					tLACommisionErrorSet.add(tLACommisionErrorSchema);
					continue;
				}
				
				String tBranchType = tBranchTypes[0];
				String tBranchType2 = tBranchTypes[1];
				//获取保单续保次数
				int tRenewCount = mAgentWageCommonFunction.queryLCRnewPol(tGrpPolNo, "grppolno");
				
				//获取最大CommisionSN
				String tCommisionSN = mAgentWageCommonFunction.getCommisionSN();
				//LastPayToDate,poltype,p2取值路径不同，重新赋值
				tLACommisionNewSchema = mAgentWageCommonFunction.packageGrpContDataOfLACommisionNew(tLCGrpContSchema, tLCGrpPolSchema);
				tLACommisionNewSchema.setCommisionSN(tCommisionSN);
				tLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
				tLACommisionNewSchema.setBranchType(tBranchType);
				tLACommisionNewSchema.setBranchType2(tBranchType2);
				tLACommisionNewSchema.setBranchType3(tBranchType3);
				tLACommisionNewSchema.setReNewCount(tRenewCount);
				
				//payyear 判断
				int tPayYear = 0;
				if(tLJAPayGrpSchema.getPayIntv() == 0)
				{
					tPayYear = 0;
				}
				else
				{
					//计算交费年度=保单生效日到交至日期
	                //查询交至日期：根据团单号到个人交费表找到对应保单号中最大的交至日期
	                String tCurrPaytoDate = mAgentWageCommonFunction.getGrpCurPaytoDate(tGrpPolNo);
	                if(tCurrPaytoDate == null)
	                {
	                	LACommisionErrorSchema tLACommisionErrorSchema = 
								dealErrorData(tLJAPayGrpSchema, AgentWageOfCodeDescribe.ERRORCODE_CURRPAYTODATE, cOperator);
						tLACommisionErrorSet.add(tLACommisionErrorSchema);
						continue;
	                }
	                tPayYear = PubFun.calInterval(tLCGrpPolSchema.getCValiDate(),
	                		mFDate.getString(PubFun.calDate(mFDate.getDate(tCurrPaytoDate), -2, "D", null)), "Y");
	                if(tPayYear<0) tPayYear = 0;
	                	
				}
				tLACommisionNewSchema.setPayYear(tPayYear);
				tLACommisionNewSchema.setLastPayToDate(tLJAPayGrpSchema.getLastPayToDate());
				tLACommisionNewSchema.setPolType("1"); //团单的保单类型置为"1"表示正常
				tLACommisionNewSchema.setP2(tLCGrpPolSchema.getBonusRate());
				tLACommisionNewSchema.setManageCom(tLJAPayGrpSchema.getManageCom());
				tLACommisionNewSchema.setEndorsementNo(tLJAPayGrpSchema.getEndorsementNo());
				tLACommisionNewSchema.setRiskCode(tLJAPayGrpSchema.getRiskCode());
				tLACommisionNewSchema.setReceiptNo(tLJAPayGrpSchema.getPayNo());
				tLACommisionNewSchema.setTPayDate(tLJAPayGrpSchema.getPayDate());
				tLACommisionNewSchema.setTEnterAccDate(tLJAPayGrpSchema.getEnterAccDate());
				tLACommisionNewSchema.setTConfDate(tLJAPayGrpSchema.getConfDate());
				tLACommisionNewSchema.setTMakeDate(tLJAPayGrpSchema.getMakeDate());
				
				tLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
				tLACommisionNewSchema.setTransMoney(tLJAPayGrpSchema.getSumActuPayMoney());
				tLACommisionNewSchema.setTransState("00");//正常保单
				tLACommisionNewSchema.setTransStandMoney(tLJAPayGrpSchema.getSumDuePayMoney());
				tLACommisionNewSchema.setCurPayToDate(tLJAPayGrpSchema.getCurPayToDate());
				tLACommisionNewSchema.setTransType("ZC");
				tLACommisionNewSchema.setCommDire("1");
				
				if(tLCGrpPolSchema.getSaleChnl().equals("07"))
				{
					tLACommisionNewSchema.setF1("02");//职团开拓
					tLACommisionNewSchema.setP5(tLJAPayGrpSchema.getSumActuPayMoney());
				}
				else
				{
					tLACommisionNewSchema.setF1("01");
				}
				tLACommisionNewSchema.setFlag("2");//正常件2
				tLACommisionNewSchema.setPayYears(mAgentWageCommonFunction.getPayYears(tGrpPolNo));
				tLACommisionNewSchema.setPayCount(tLJAPayGrpSchema.getPayCount());
				tLACommisionNewSchema.setAgentCom(tLJAPayGrpSchema.getAgentCom());
				tLACommisionNewSchema.setAgentType(tLJAPayGrpSchema.getAgentType());
				tLACommisionNewSchema.setAgentCode(tLJAPayGrpSchema.getAgentCode());
				 //套餐赋值
		        String  tWrapCode = "";
		        if("2".equals(tLCGrpContSchema.getCardFlag()))
                {
		        	tWrapCode = mAgentWageCommonFunction.getWrapCode(tLJAPayGrpSchema.getGrpContNo(), tLJAPayGrpSchema.getRiskCode());
                }
		        tLACommisionNewSchema.setF3(tWrapCode);
				
				if(tRenewCount>=1 || tLJAPayGrpSchema.getPayCount()>=2)
	            {
					tLACommisionNewSchema.setP7(0);
	            }
	            else
	            {
	                String tMoney = mAgentWageCommonFunction.getCManageFee(tEndorsementNo, tGrpPolNo, tPayType, tGetNoticeNo, tPayNo);
	                double tValue = Double.parseDouble(tMoney);
	                tLACommisionNewSchema.setP7(tValue);
	            }
				
				String tPrtNo = tLCGrpPolSchema.getPrtNo();
				String tScanDate = mAgentWageCommonFunction.getScanDate(tPrtNo);
				tLACommisionNewSchema.setScanDate(tScanDate);
				tLACommisionNewSchema.setOperator(cOperator);
				tLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
				tLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
				tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
				tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
				
				String tAFlag ="";
				String tConfDate ="";
				if(tPayType.equals("GB"))
				{
					//团险保单反冲 confdate = makedate
					tAFlag ="0";
					tConfDate =tLJAPayGrpSchema.getConfDate();
					tLACommisionNewSchema.setriskmark("1");//代表共保保单

				}else
				{
					String [] tConfDates = mAgentWageCommonFunction.queryConfDate(tGrpContNo, "ZC", tLACommisionNewSchema.getTMakeDate(), tLJAPayGrpSchema.getConfDate(),tPayNo);
					tAFlag = tConfDates[0];
					tConfDate = tConfDates[1];
				}
				//回访处理
				String tVisitTime = mAgentWageCommonFunction.queryReturnVisit(tGrpContNo);
				String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
				String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, "02", tAFlag, tVisitTime, tRiskFlag);
				tLACommisionNewSchema.setCalDate(tCalDate);
	            if(!"".equals(tCalDate)&&null!=tCalDate)
                {
                	String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate).substring(0,6);
                	tLACommisionNewSchema.setWageNo(tWageNo);
                	tLACommisionNewSchema.setCalcDate(tCalDate);
                	tLACommisionNewSchema.setTConfDate(tConfDate);
                }
	            tLACommisionNewSchema.setAgentGroup(tAgentGroup);
	            tLACommisionNewSchema.setBranchCode(tBranchCode);
	            tLACommisionNewSchema.setBranchAttr(tLABranchGroupSchema.getBranchAttr());
	            tLACommisionNewSchema.setBranchSeries(tLABranchGroupSchema.getBranchSeries());
				tLACommisionNewSet.add(tLACommisionNewSchema);
			}
			if(0<tLACommisionNewSet.size())
			{
				tMMap.put(tLACommisionNewSet, "INSERT");
			}
			if(0<tLACommisionErrorSet.size())
			{
				tMMap.put(tLACommisionErrorSet, "INSERT");
			}
			if(0==tMMap.size())
			{
				continue;
			}
			if(!mAgentWageCommonFunction.dealDataToTable(tMMap))
			{
				// @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "AgentWageOfLJAPayGrpData";
	            tError.functionName = "PubSubmit";
	            tError.errorMessage = "数据插入到LACommisionNew中失败!";
	            this.mErrors.addOneError(tError);
	            return false;
			}
		} while (tLJAPayGrpSet.size()>0);
		System.out.println("*****************团险提数LJAPayGrp AgentWageOfLJAPayGrpData dealData  结束***************");
		return true;
	}
	
	
	
}
