package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

/**
 * 对于薪资月为空的数据再次进行薪资月计算
 * @author yangyang
 *
 */
public class AgentWageOfCalWageNoData extends AgentWageOfAbstractClass {
	/**
	 * 主要的提数逻辑,实现具体的接口方法
	 * @return
	 */
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfCalWageNoData-->dealData:开始执行");
		System.out.println("AgentWageOfCalWageNoData-->dealData:cSQL"+cSQL);
		LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLACommisionNewSet, cSQL);
		do
		{
			tRSWrapper.getData();
			if(null==tLACommisionNewSet||0==tLACommisionNewSet.size())
			{
				System.out.println("AgentWageOfCalWageNoData-->dealData:没有薪资月为空的数据！！");
				return true;
			}
		    mMMap = new MMap();
			LACommisionNewSet tUpdateLACommisionNewSet = new LACommisionNewSet();
			for(int i =1;i<=tLACommisionNewSet.size();i++)
			{
				LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
				tLACommisionNewSchema = tLACommisionNewSet.get(i);
				String tContNo = tLACommisionNewSchema.getContNo();
				String tGrpContNo = tLACommisionNewSchema.getGrpContNo();
				String tTransType = tLACommisionNewSchema.getTransType();
				String tRiskCode = tLACommisionNewSchema.getRiskCode();
				String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
				String tGetPolDate = "";
				String tCustomGetPolDate = "";
				String tConfDate = "";
				String tFlag = "02";
				String tAFlag = "";
				String tVisitTime = ""; 
				if (!tGrpContNo.equals("00000000000000000000")) 
				{
					LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	                //查询团险的保单信息
	                tLCGrpContSchema = mAgentWageCommonFunction.queryLCGrpCont(tGrpContNo);
	                if (tLCGrpContSchema == null) {
	                	System.out.println("AgentWageOfCalWageNoData-->dealData团单"+tGrpContNo+"未在保单表中找到!");
	                    continue;
	                }
	                tGetPolDate = tLCGrpContSchema.getGetPolDate();
	                tCustomGetPolDate = tLCGrpContSchema.getCustomGetPolDate();
	                tVisitTime  = mAgentWageCommonFunction.queryReturnVisit(tLCGrpContSchema.getGrpContNo());
				}
				else
				{
					//个单默认为01
					tFlag="01";
					LCContSchema tLCContSchema = new LCContSchema();
	                tLCContSchema = mAgentWageCommonFunction.queryLCCont(tContNo);
	                if (tLCContSchema == null) {
	                	System.out.println("AgentWageOfCalWageNoData-->dealData个单"+tContNo+"未在保单表中找到!");
	                	continue;
	                }
	                tGetPolDate = tLCContSchema.getGetPolDate();
	                if (null == tGetPolDate||"".equals(tGetPolDate)) 
	                {
	                	System.out.println("AgentWageOfCalWageNoData-->dealData个单"+tContNo+"保单送达日期为空!");
	                	continue;
	                }
	                tCustomGetPolDate = tLCContSchema.getCustomGetPolDate();
	                tVisitTime  = mAgentWageCommonFunction.queryReturnVisit(tLCContSchema.getContNo());
	                int tRenewCount = tLACommisionNewSchema.getReNewCount();
	                //薪资月算法相关字段处理
	                if(tRenewCount>0)
	                {
	                	tFlag="02";
	                }
	                //简易险
	                if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
                    {
                        tFlag="02";
                    }
				}
				tLACommisionNewSchema.setCustomGetPolDate(tCustomGetPolDate);
                tLACommisionNewSchema.setGetPolDate(tGetPolDate);
				String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLACommisionNewSchema.getGrpContNo(),tTransType,tLACommisionNewSchema.getTMakeDate(),
						tLACommisionNewSchema.getTConfDate(),tLACommisionNewSchema.getReceiptNo());
				//用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
				tAFlag =tConfDates[0];
				//财务确认日期 ，团单会用到此字段
				tConfDate = tConfDates[1];
				//根据薪资月函数 得到薪资月
				String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, tFlag, tAFlag, tVisitTime, tRiskFlag);
				if(null!=tCalDate&&!"".equals(tCalDate))
				{
					tLACommisionNewSchema.setCalDate(tCalDate);
					String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate);
					tLACommisionNewSchema.setWageNo(tWageNo);
					tLACommisionNewSchema.setOperator(cOperator);
					tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
					tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());				
					tUpdateLACommisionNewSet.add(tLACommisionNewSchema);                
				}
			}
			//提交数据库，防止 内存溢出
			mMMap.put(tUpdateLACommisionNewSet,"UPDATE");
	        try{
	        	if (!mAgentWageCommonFunction.dealDataToTable(mMMap)) {
		        	// @@错误处理
		            CError tError = new CError();
		            tError.moduleName = "AgentWageOfCalWageNoData";
		            tError.functionName = "PubSubmit";
		            tError.errorMessage = "数据插入到LACommisionNew中失败!";
		            this.mErrors.addOneError(tError);
		            return false;
		        }
	        }catch(Exception ex)
	        {
	        	ex.printStackTrace();
	        }
		}
		while(tLACommisionNewSet.size()>0);
		System.out.println("AgentWageOfCalWageNoData-->dealData:执行结束 ");
		return true;
	}
	
}
