package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LBGrpPolDB;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LBRnewStateLogDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCRnewStateLogDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LPDiskImportDB;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageActivityLogSchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LBGrpPolSchema;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LBRnewStateLogSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCRnewStateLogSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageActivityLogSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.lis.vschema.LBRnewStateLogSet;
import com.sinosoft.lis.vschema.LCRnewStateLogSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p> 
 *
 * @author caigang
 * @version 1.0
 */
public class AgentWageCalSaveNewBL {		
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private FDate fDate = new FDate();
    private String mOperate;
    private SSRS mBranchTypes;
    private String mManageCom;
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String  AFlag = "0";//0表示收费的情况,1表示付费情况
    private String mSaleChnl[] = {
                                 "", "", ""};
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();
    private LAWageLogSet mUpdLAWageLogSet = new LAWageLogSet();
    private LAWageLogSet mInsLAWageLogSet = new LAWageLogSet();
    private LAWageHistorySet mUpdLAWageHistorySet = new LAWageHistorySet();
    private LAWageHistorySet mInsLAWageHistorySet = new LAWageHistorySet();
    private LACommisionSchema mLACommisionSchema = new LACommisionSchema();
    private LACommisionSchema mLACommisionSchema1 = new LACommisionSchema();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LACommisionSet mLAUpdateCommisionSet = new LACommisionSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAWageActivityLogSet mLAWageActivityLogSet = new
            LAWageActivityLogSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private int tRenewCount = 0;
    private boolean mReturnState=false;
    private SSRS mSSRS = new SSRS();//境外救援险种投保方式为单次的解约(在处理解约时需要判断)
    private SSRS mSSRS1 = new SSRS();//境外救援险种投保方式为单次的解约(在处理解约时需要判断)
    private String[][] mStrDate = null;//境外救援险种投保方式为单次的解约(在处理解约时需要判断)
    private MMap mMap = new MMap();
    

    public AgentWageCalSaveNewBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }	
    }

    //获取数据
    private boolean getInputData(VData cInputData) {
        mLAWageLogSchema = (LAWageLogSchema) cInputData.getObjectByObjectName(
                "LAWageLogSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mLAWageLogSchema == null || mGlobalInput == null) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        this.mBranchType = mLAWageLogSchema.getBranchType();
        this.mBranchType2 = mLAWageLogSchema.getBranchType2();
        this.mManageCom = mLAWageLogSchema.getManageCom();
        ////根据提数的渠道branchtype转化为对应salechnl
        this.mSaleChnl = AgentPubFun.switchBranchTypetoSalChnl(mBranchType,mBranchType2);
        if (mSaleChnl == null) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在转换销售渠道的过程返回空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //校验数据
    private boolean check() {
        FDate fd = new FDate();
        if (fd.getDate(mLAWageLogSchema.getStartDate()).compareTo(fd.getDate(
                mLAWageLogSchema.getEndDate())) != 0) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "一次提数只能提取一天的数据！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //前面月份没有进行提取结束的,也不能进行以后月份的数据提取,必须保持日志的连贯性
//        if()
//        String endSQL = "select wageno,wagemonth,enddate from lawagelog where managecom like ''"
        return true;
    }

    //提交数据
    private boolean prepareOutputData() {
        mMap.put(this.mInsLAWageHistorySet, "INSERT");
        mMap.put(this.mLACommisionSet, "INSERT");
        mMap.put(this.mInsLAWageLogSet, "INSERT");
        mMap.put(this.mLAWageActivityLogSet, "INSERT");
        mMap.put(this.mUpdLAWageHistorySet, "UPDATE");
        mMap.put(this.mUpdLAWageLogSet, "UPDATE");
        mMap.put(this.mLAUpdateCommisionSet , "UPDATE");

        this.mInputData.add(mMap);
        return true;
    }

    //返回状态
    private boolean returnState()
    {

       String sql = "select distinct branchtype,branchtype2 from labranchlevel where 1=1";
       if (this.mBranchType != null && !this.mBranchType.equals("")) {
           sql += " and branchType='" + mBranchType + "'";
       }
       if (this.mBranchType2 != null && !this.mBranchType2.equals("")) {
           sql += " and branchType2='" + mBranchType2 + "'";
       }
       sql += " and branchType2 not in('04','05') ";
       System.out.println("查询需要提数的渠道：" + sql);
       ExeSQL tExeSQL = new ExeSQL();
       mBranchTypes = tExeSQL.execSQL(sql);
       if (mBranchTypes == null) {
           return false;
       }
       for (int x = 1; x <= mBranchTypes.getMaxRow(); x++) {
           String tBranchType = mBranchTypes.GetText(x, 1);
           String tBranchType2 = mBranchTypes.GetText(x, 2);
           String Year = AgentPubFun.formatDate(mLAWageLogSchema.getStartDate(),"yyyy");
           String Month = AgentPubFun.formatDate(mLAWageLogSchema.getEndDate(), "MM");
           String WageNo = AgentPubFun.formatDate(mLAWageLogSchema.getStartDate(),"yyyyMM");
           String SQL = "select date('" + this.mLAWageLogSchema.getEndDate() + "')-1 day from dual";
           System.out.println("查询其一天日志："+SQL);

           ExeSQL ttSQL = new ExeSQL();
           String enddate1 = ttSQL.getOneValue(SQL); //返回到前一天的日志
           System.out.println("前一天时间Enddate1:" + enddate1);
           //判断是否是一个月的开始，如果是要删除数据，如果不是修改ENDDATE及状态即可；
           String month1 = AgentPubFun.formatDate(enddate1, "MM");
           //判断是否跨月
           if (Month.compareTo(month1) != 0) {
               //删除数据
               LAWageLogDB tLAWageLogDB = new LAWageLogDB();
               LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();

               tLAWageLogDB.setWageNo(WageNo);
               tLAWageLogDB.setManageCom(mManageCom);
               tLAWageLogDB.setBranchType(tBranchType);
               tLAWageLogDB.setBranchType2(tBranchType2);
               tLAWageLogDB.setStartDate(mLAWageLogSchema.getStartDate());
               tLAWageLogDB.setEndDate(mLAWageLogSchema.getEndDate());
               tLAWageLogDB.setWageMonth(AgentPubFun.formatDate(mLAWageLogSchema.
                       getStartDate(), "MM"));
               tLAWageLogDB.setWageYear(AgentPubFun.formatDate(mLAWageLogSchema.
                       getStartDate(), "yyyy"));
               tLAWageLogDB.setOperator(mGlobalInput.Operator);

               tLAWageLogDB.setState("00");
               tLAWageLogDB.setMakeDate(CurrentDate);
               tLAWageLogDB.setMakeTime(CurrentTime);
               tLAWageLogDB.setModifyDate(CurrentDate);
               tLAWageLogDB.setModifyTime(CurrentTime);
               if (!tLAWageLogDB.delete()) {
                   return false;
               }
               //this.mInsLAWageLogSet.add(tLAWageLogDB);
               System.out.println("删除佣金计算历史表");
               //填充佣金计算历史表
               tLAWageHistoryDB.setWageNo(WageNo);
               tLAWageHistoryDB.setManageCom(mManageCom);
               tLAWageHistoryDB.setAClass("03");
               tLAWageHistoryDB.setOperator(mGlobalInput.Operator);
               tLAWageHistoryDB.setState("00");
               tLAWageHistoryDB.setBranchType(tBranchType);
               tLAWageHistoryDB.setBranchType2(tBranchType2);
               tLAWageHistoryDB.setMakeDate(CurrentDate);
               tLAWageHistoryDB.setMakeTime(CurrentTime);
               tLAWageHistoryDB.setModifyDate(CurrentDate);
               tLAWageHistoryDB.setModifyTime(CurrentTime);
               if (!tLAWageHistoryDB.delete()) {
                   return false;
               }
           }
           else {
               LAWageLogDB tLAWageLogDB = new LAWageLogDB();
               LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();

               tLAWageLogDB.setWageNo(WageNo);
               tLAWageLogDB.setManageCom(mManageCom);
               tLAWageLogDB.setBranchType(tBranchType);
               tLAWageLogDB.setBranchType2(tBranchType2);
               tLAWageLogDB.getInfo();
               tLAWageLogDB.setEndDate(this.mLAWageLogSchema.getEndDate());
               tLAWageLogDB.setOperator(mGlobalInput.Operator);

               tLAWageLogDB.setState("11");
               tLAWageLogDB.setModifyDate(CurrentDate);
               tLAWageLogDB.setModifyTime(CurrentTime);
               if (!tLAWageLogDB.update()) {
                   return false;
               }
               System.out.println("填充佣金计算历史表");
               //填充佣金计算历史表
               tLAWageHistoryDB.setWageNo(WageNo);
               tLAWageHistoryDB.setManageCom(mManageCom);
               tLAWageHistoryDB.setAClass("03");
               tLAWageHistoryDB.setBranchType(tBranchType);
               tLAWageHistoryDB.setBranchType2(tBranchType2);
               tLAWageHistoryDB.getInfo();
               tLAWageHistoryDB.setOperator(mGlobalInput.Operator);
               tLAWageHistoryDB.setState("11");
               tLAWageHistoryDB.setModifyDate(CurrentDate);
               tLAWageHistoryDB.setModifyTime(CurrentTime);
               if (!tLAWageHistoryDB.update()) {
                   return false;
               }
           }
       }
        return true;
    }

    //提交数据
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!check()) {
            return false;
        }
        if (!dealData()) {
        	 if (mErrors.needDealError())
             {
                 System.out.println("程序异常结束原因：" + mErrors.getFirstError());
             }
             if(mReturnState==true)
             {
                 if (!returnState()) {
                    return false;
                 }
             }
             return false;
        }
        // 新增方法对银代渠道保单
        if(!dealBankCommision())
        {
        	return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AgentWageCalSaveNewBL Submit...");
        try{
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
        	// @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        }catch(Exception ex)
        {
        	ex.printStackTrace();
        }
        return true;
    }

    //处理数据
    private boolean dealData() {
        String sql =
                "select distinct branchtype,branchtype2 from labranchlevel where 1=1";
        if (this.mBranchType != null && !this.mBranchType.equals("")) {
            sql += " and branchType='" + mBranchType + "'";
        }
        if (this.mBranchType2 != null && !this.mBranchType2.equals("")) {
            sql += " and branchType2='" + mBranchType2 + "'";
        }
        sql += " and branchType2 not in ('04','05') with ur ";
        System.out.println("查询提数渠道语句:" + sql);
        ExeSQL tExeSQL = new ExeSQL();
        mBranchTypes = tExeSQL.execSQL(sql);
        if (mBranchTypes == null) {
            return false;
        }
        //校验提数日志
        for (int x = 1; x <= mBranchTypes.getMaxRow(); x++) {
            String tBranchType = mBranchTypes.GetText(x, 1);
            String tBranchType2 = mBranchTypes.GetText(x, 2);
            LAWageLogDB tLAWageLogDB = new LAWageLogDB();
            LAWageLogSet tLAWageLogSet = new LAWageLogSet();
            String Year = AgentPubFun.formatDate(mLAWageLogSchema.getStartDate(),"yyyy");
            String Month = AgentPubFun.formatDate(mLAWageLogSchema.getEndDate(), "MM");
            sql = "select * from LAWageLog where  WageYear = '" + Year + "'"
                  + " and WageMonth='" + Month + "'"
                  + " and ManageCom = '" + mManageCom + "' "
                  + " And BranchType = '" + tBranchType + "'"
                  + " And BranchType2 ='" + tBranchType2 + "'";

            System.out.println("查询日志语句:" + sql);
            tLAWageLogSet = tLAWageLogDB.executeQuery(sql);
            if (tLAWageLogDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLAWageLogDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSaveNewBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询日志纪录存在是否出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLAWageLogSet.size() == 0) {
                System.out.println("日志表中没有纪录！");
                if (!prepareLogData(tBranchType, tBranchType2)) {
                    return false;
                }
            }
            else {
                for (int i = 1; i <= tLAWageLogSet.size(); i++) {
                    String today = mLAWageLogSchema.getEndDate();//今天的日期
                    String yesterday = PubFun.calDate(today, -1, "D", null);
                    LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
                    tLAWageLogSchema = tLAWageLogSet.get(i);
                    System.out.println("Today:" + today);
                    System.out.println("Yesterday:" + yesterday);
                    System.out.println("tLAWageLogSchema.getEndDate():" +tLAWageLogSchema.getEndDate());//提数截止日期
                    FDate fd = new FDate();
                    if (fd.getDate(tLAWageLogSchema.getEndDate()).compareTo(fd.getDate(yesterday)) < 0) {
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveNewBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "昨天(" + yesterday + ")的展业类型为" +
                                              tLAWageLogSchema.getBranchType() +
                                              "，渠道" +
                                              tLAWageLogSchema.getBranchType2() +
                                              "数据未提取，不能进行今天的提取！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if ((fd.getDate(tLAWageLogSchema.getEndDate()).compareTo(fd.getDate(today)) >= 0) &&
                        (fd.getDate(tLAWageLogSchema.getStartDate()).compareTo(fd.getDate(today)) <= 0)) {
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveNewBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "日期为(" + today + ")的展业类型为" +
                                              tLAWageLogSchema.getBranchType() +
                                              "，渠道" +
                                              tLAWageLogSchema.getBranchType2() +
                                              "数据已提取，不能进行此日期的提取！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }

                    if (!tLAWageLogSchema.getState().equals("10") &&
                        !tLAWageLogSchema.getState().equals("11") &&
                        !tLAWageLogSchema.getState().equals("12")) {
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveNewBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "昨天(" + yesterday + ")的展业类型为" +
                                              tLAWageLogSchema.getBranchType() +
                                              "，渠道" +
                                              tLAWageLogSchema.getBranchType2() +
                                              "数据提取后未计算，不能进行今天的提取！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    System.out.println("填充佣金计算日志lawagelog表");
                    LAWageLogDB mLAWageLogDB = new LAWageLogDB();
                    mLAWageLogDB.setManageCom(tLAWageLogSchema.getManageCom());
                    mLAWageLogDB.setWageNo(tLAWageLogSchema.getWageNo());
                    mLAWageLogDB.setWageYear(Year);
                    mLAWageLogDB.setWageMonth(Month);
                    mLAWageLogDB.setBranchType(tLAWageLogSchema.getBranchType());
                    mLAWageLogDB.setBranchType2(tLAWageLogSchema.getBranchType2());
                    mLAWageLogDB.getInfo();
                    mLAWageLogDB.setModifyDate(currentDate);
                    mLAWageLogDB.setModifyTime(currentTime);
                    mLAWageLogDB.setOperator(mGlobalInput.Operator);
                    mLAWageLogDB.setState("00");
                    mLAWageLogDB.setEndDate(mLAWageLogSchema.getEndDate());
                    if(!mLAWageLogDB.update())
                    {
                    	return false;
                    }
                    System.out.println("填充佣金历史lawagehistory表");
                    LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
                    tLAWageHistoryDB.setManageCom(tLAWageLogSchema.getManageCom());
                    tLAWageHistoryDB.setWageNo(tLAWageLogSchema.getWageNo());
                    tLAWageHistoryDB.setBranchType(tLAWageLogSchema.getBranchType());
                    tLAWageHistoryDB.setBranchType2(tLAWageLogSchema.getBranchType2());
                    tLAWageHistoryDB.setAClass("03");
                    tLAWageHistoryDB.getInfo();
                    tLAWageHistoryDB.setModifyDate(currentDate);
                    tLAWageHistoryDB.setModifyTime(currentTime);
                    tLAWageHistoryDB.setState("00");
                    if(!tLAWageHistoryDB.update())
                    {
                    	return false;
                    }
                }
            }
        }
        mReturnState=true;
        /***********************************************************************/
        System.out.println("*****************数据开始提取***************");

        //处理wageno为空的数据
        System.out.println("*****************置扎帐表中回单日期为空的（已回单的） 开始***************");
        LCGrpPolSchema tLCGrpPolSchema;
        LCGrpContSchema tLCGrpContSchema;
        LCPolSchema tLCPolSchema;
        LCContSchema tLCContSchema;
        String tContNo = "";
        String tGrpContNo = "";
        String PolNo = "";
        String GrpPolNo = "";
        String tAgentGroup = "";
        String tBranchCode = "";
        String tAgentCode = "";
        String tBranchAttr = "";
        String COMMISIONSN = "";
        String getPolDate = "";
        String tWageNo = "";
        String custPolDate = "";
        String chargeDate = ""; //手续费审核日期
        String tTransType = "";
        String tMakeDate = "";//---songgh因为一个保单可能会做多个保全项目,所以在进行查找的时候按照提数当天发生的保全项目进行搜索
        int PayYear = 0;
//犹豫期撤保的不查询
//        sql = "select CommisionSN from lacommision where managecom='" +mManageCom + "' and caldate is null and commdire='1'  ";
//        if (mBranchType != null && !mBranchType.equals("")) {
//            sql += " and BranchType='" + mBranchType + "'";
//        }
//        if (mBranchType2 != null && !mBranchType2.equals("")) {
//            sql += " and BranchType2='" + mBranchType2 + "'";
//        }
//        sql += " order by BranchType,BranchType2,GrpPolNo,PolNo with ur ";
        System.out.println("查询扎帐表中回单日期为空即薪资年月为空的保单："+sql);
        SSRS tSSRS = new SSRS();
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
//        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
//            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
//            LACommisionDB tLACommisionDB = new LACommisionDB();
//            tLACommisionDB.setCommisionSN(tSSRS.GetText(i, 1));
//            tLACommisionDB.getInfo();
//            tLACommisionSchema = tLACommisionDB.getSchema();
//            String tBranchType = tLACommisionSchema.getBranchType();
//            String tBranchType2 = tLACommisionSchema.getBranchType2();
//            tContNo = tLACommisionSchema.getContNo();
//            tGrpContNo = tLACommisionSchema.getGrpContNo();
//            tTransType = tLACommisionSchema.getTransType();
//            tMakeDate = tLACommisionSchema.getTMakeDate();
//            String tGrpPolNo = tLACommisionSchema.getGrpPolNo();
//            //如果是团单，查询团单信息
//            if (!tGrpPolNo.equals("00000000000000000000")) {
//                tLCGrpContSchema = new LCGrpContSchema();
//                //查询团险的保单信息
//                tLCGrpContSchema = this.queryLCGrpCont(tGrpContNo);
//                if (tLCGrpContSchema == null) {
//                    continue;
//                }
//                getPolDate = tLCGrpContSchema.getGetPolDate();
//                custPolDate = tLCGrpContSchema.getCustomGetPolDate();
//              //团险保全项目根据财务的确认日期计算佣金月---modifydate:20080329-songgh
//                String tConfDate = "";
//                tConfDate = this.queryConfDate(tGrpContNo,tTransType,tMakeDate,
//                		                         tLACommisionSchema.getTConfDate(),tLACommisionSchema.getReceiptNo());
//                //if (getPolDate != null && !getPolDate.equals("")) {
//                    tLACommisionSchema.setCustomGetPolDate(custPolDate);
//                    tLACommisionSchema.setGetPolDate(getPolDate);
////                    chargeDate = queryChargeDate(mLACommisionSchema.getGrpPolNo(),
////                                                 mLACommisionSchema.getAgentCom(),
////                                                 mLACommisionSchema.
////                                                 getReceiptNo());
//
//                    //=== 增加访后付费 日期校验--针对山东分公司需求
//                    String tRiskCode = tLACommisionSchema.getRiskCode(); // 查询出险种信息
//                    String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
//                    String tTransState = tLACommisionSchema.getTransState();
//                    String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
//                    // add new 
//                    String tAgentcode =tLACommisionSchema.getAgentCode() ;
//
//                    String caldate = this.calCalDateForAll(tBranchType,
//                            tBranchType2,
//                            tLACommisionSchema.getSignDate(),
//                            getPolDate,
//                            String.valueOf(tLACommisionSchema.getPayCount()),
//                            tLACommisionSchema.getTMakeDate(), custPolDate,
//                            tLACommisionSchema.getCValiDate(),"02",
//                            tConfDate,AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,tGrpPolNo,tRiskCode);
//                    if (caldate != null && !caldate.equals("")) {
//                        sql =
//                                "select yearmonth from LAStatSegment where startdate<='" +
//                                caldate + "' and enddate>='" + caldate +
//                                "' and stattype='91' "; //团险薪资月为91,2005-11-23修改
//                        tExeSQL = new ExeSQL();
//                        tWageNo = tExeSQL.getOneValue(sql);
//                        tLACommisionSchema.setWageNo(tWageNo);
//                        tLACommisionSchema.setCalDate(caldate);
//                        tLACommisionSchema.setTConfDate(tConfDate);//财务确认日期
//                        tLACommisionSchema.setModifyDate(this.currentDate);
//                        tLACommisionSchema.setModifyTime(this.currentTime);
//                    }
//                    this.mLAUpdateCommisionSet.add(tLACommisionSchema);
//
//
//            } // 团单结束
//            else { //如果是个单
//                tLCContSchema = new LCContSchema();
//                tLCContSchema = this.queryLCCont(tContNo);
//                if (tLCContSchema == null) {
//                   System.out.println("渠道检验出错!");
//                	continue;
//                }
//                System.out.println("after get LAContSchema!");
//                getPolDate = tLCContSchema.getGetPolDate();
//                custPolDate = tLCContSchema.getCustomGetPolDate();
//                if (getPolDate != null && !getPolDate.equals("")) {
//                    tLACommisionSchema.setCustomGetPolDate(custPolDate);
//                    tLACommisionSchema.setGetPolDate(getPolDate);
//                    String tFlag="01";
//                    if(tLACommisionSchema.getReNewCount()>0)
//                    {
//                        tFlag="02";
//                    }
//                    //简易险
//                    if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
//                    {
//                        tFlag="02";
//                    }
//                    String tTMakeDate=tLACommisionSchema.getTMakeDate();
//                    //犹豫期退保需要修改tTMakeDate
//                    if(tLACommisionSchema.getTransType().equals("WT"))
//                    {
//                        String tSQL1 =
//                                "select tMakeDate from LACommision where transtype='ZC' and PolNo = '" +
//                                tLACommisionSchema.getPolNo() +
//                                "' and SignDate='" +
//                                tLACommisionSchema.getSignDate() +
//                                "' order by commisionsn with ur ";
//                        tExeSQL = new ExeSQL();
//                        tTMakeDate=tExeSQL.getOneValue(tSQL1);
//
//                    }
//
//                    String tConfDate = "";
//                    tConfDate = this.queryConfDate(tGrpContNo,tTransType,tTMakeDate,tLACommisionSchema.getTConfDate(),tLACommisionSchema.getReceiptNo());
//                    //=== 增加访后付费 日期校验--针对山东分公司需求
//                    String tRiskCode = tLACommisionSchema.getRiskCode(); // 查询出险种信息
//                    String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
//                    String tTransState = tLACommisionSchema.getTransState();
//                    String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
//                    // add new 
//                    String tAgentcode =tLACommisionSchema.getAgentCode() ;
//                    String caldate = this.calCalDateForAll(tBranchType,
//                           tBranchType2,
//                           tLACommisionSchema.getSignDate(),
//                           getPolDate,
//                           String.valueOf(tLACommisionSchema.
//                                          getPayCount()),
//                           tTMakeDate, custPolDate,
//                           tLACommisionSchema.getCValiDate(),tFlag,
//                            tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,tLCContSchema.getGrpContNo(),tRiskCode);
//                   if (!caldate.equals("") && !caldate.equals("0")) {
//
//                        sql =
//                                "select yearmonth from LAStatSegment where startdate<='" +
//                                caldate + "' and enddate>='" + caldate +
//                                "' and stattype='5' ";
//                        tExeSQL = new ExeSQL();
//                        tWageNo = tExeSQL.getOneValue(sql);
//                        tLACommisionSchema.setWageNo(tWageNo);
//                        tLACommisionSchema.setCalDate(caldate);
//                        tLACommisionSchema.setTConfDate(tConfDate);
//                        tLACommisionSchema.setModifyDate(this.currentDate);
//                        tLACommisionSchema.setModifyTime(this.currentTime);
//                    }
//                    this.mLAUpdateCommisionSet.add(tLACommisionSchema);
//                }
//
//            }
//        }
        System.out.println("*****************置扎帐表中回单日期为空的（已回单的）结束***************");

        System.out.println("*****************查询个单  开始***************");
        LJAPayPersonSchema tLJAPayPersonSchema;
        tSSRS = queryLJAPayPerson();
        if (tSSRS==null) {
            return false;
        }
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
            tLJAPayPersonDB.setPolNo(tSSRS.GetText(i, 1));
            tLJAPayPersonDB.setPayNo(tSSRS.GetText(i, 2));
            tLJAPayPersonDB.setDutyCode(tSSRS.GetText(i, 3));
            tLJAPayPersonDB.setPayPlanCode(tSSRS.GetText(i, 4));
            tLJAPayPersonDB.setPayType(tSSRS.GetText(i, 5));
            tLJAPayPersonDB.getInfo();
            tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema = tLJAPayPersonDB.getSchema();
            tContNo = tLJAPayPersonDB.getContNo();
            PolNo = tLJAPayPersonDB.getPolNo();
            String tPayTypeFlag=tLJAPayPersonDB.getPayTypeFlag();
            String tRiskCode=tLJAPayPersonDB.getRiskCode();
            if(tPayTypeFlag == null )
            {
                tPayTypeFlag="";
            }
            //处理 个险的卡单数据
            String tflag = getIncomeType(tLJAPayPersonSchema.getPayNo());
            if (tflag.equals("15")) { //个险提数或则全部提数，团险等提数不能提
                if (mBranchType == null || mBranchType.equals("") ||
                    (mBranchType.equals("1") && mBranchType2.equals("01"))
                //增加互动渠道
                    ||(mBranchType.equals("5") && mBranchType2.equals("01"))
                )
                {
                    if (!dealperCommision(tLJAPayPersonSchema, tContNo, PolNo)) {
                        return false;
                    }
                }
            }
           //现在暂时只处理330801  331801
           //如果以后增加处理的万能险种，向ldcode增加描述
           //处理将保费分为基本保费和额外保费
           //
           else if (tRiskCode.equals("330801")||tRiskCode.equals("331801")||tRiskCode.equals("332701")||tRiskCode.equals("333901")
        //2015-7-1    产品新上三款 万能险   334701	福泽一生个人护理保险（万能型，B款）  基本保费算法不一样
//        		   334801	附加个人护理保险（万能型，B款）  和之前一样
//        		   370201	安康无忧团体护理保险（万能型，B款）和之前一样
        		   ||tRiskCode.equals("334701")||tRiskCode.equals("334801")||tRiskCode.equals("370201")
           )
           {
               //if (mBranchType == null || mBranchType.equals("")|| mBranchType2.equals("01"))
               //{
                   if (!dealOmniCommision(tLJAPayPersonSchema, tContNo, PolNo,tPayTypeFlag)) {
                       return false;
                   }
               //}
           }
           //增加对于互动渠道的处理   yangyang 2015-2-3
           else if(mBranchType.equals("5") && mBranchType2.equals("01")
        		  &&(tRiskCode.equals("330801")||tRiskCode.equals("331801")
        		  ||tRiskCode.equals("332701")||tRiskCode.equals("331201")
        		  ||tRiskCode.equals("331301")||tRiskCode.equals("331601")
        		  ||tRiskCode.equals("331501")||tRiskCode.equals("333901")
        		  ||tRiskCode.equals("334701")||tRiskCode.equals("334801")||tRiskCode.equals("370201")
        		  ))
           {
	               //if (mBranchType == null || mBranchType.equals("")|| mBranchType2.equals("01"))
	               //{
	                   if (!dealOmniCommision(tLJAPayPersonSchema, tContNo, PolNo,tPayTypeFlag)) {
	                       return false;
	                   }
	               //}
           }
            //增加税优险种区分帐户保费和风险保费
           else if(tRiskCode.equals("122601")||tRiskCode.equals("122701")
         		  ||tRiskCode.equals("122801")||tRiskCode.equals("122901")
         		  ||tRiskCode.equals("123001")||tRiskCode.equals("123101")
         		  ||tRiskCode.equals("123501")||tRiskCode.equals("123601")
         		  ||tRiskCode.equals("123602")
         		  )
            {
 	                   if (!dealTaxCommision(tLJAPayPersonSchema, tContNo, PolNo,tPayTypeFlag)) {
 	                       return false;
 	                   }
            }
           else {
        	    tLCContSchema = new LCContSchema();
                tLCContSchema = this.queryLCCont(tContNo);
                if (tLCContSchema == null) {
                    continue;
                }
                System.out.println("~~~~~~~再报错!!!!!!!!!!~~~~~~~~~~~");
                tLCPolSchema = new LCPolSchema();
               try{
                tLCPolSchema = this.queryLCPol(PolNo);
               }
               catch(Exception ex)
               {
               	 ex.getStackTrace();
               }
                if (tLCPolSchema == null) {
                    continue;
                }
                //续保的处理
                LCRnewStateLogSchema tLCRnewStateLogSchema = new
                        LCRnewStateLogSchema();
                tLCRnewStateLogSchema = this.queryLCRnewPol(PolNo);

                if (tLCRnewStateLogSchema == null) {
                    tRenewCount = 0;
                }
                else
                {
                    if(tPayTypeFlag.equals("1"))
                    {
//                    	if (tRiskCode.equals("320106")){
//                    		tRenewCount = 0;
//                    	}
//                    	else 
                        tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                    }
                    else
                    {
                         tRenewCount = 0;
                    }
                }
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLJAPayPersonSchema.getAgentCode());
                tLAAgentDB.getInfo();
                mLAAgentSchema = tLAAgentDB.getSchema();
                COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
                if (tLJAPayPersonSchema.getPayIntv() == 0) { //趸交payIntv = 0
                    PayYear = 0;
                } else {
                    //计算交费年度=保单生效日到交至日期
                    PayYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),
                                                 fDate.
                                                 getString(PubFun.calDate(fDate.
                            getDate(tLJAPayPersonSchema.getCurPayToDate()), -2,
                            "D", null)),
                                                 "Y");
                    if(PayYear<0)  PayYear=0;
                }
                mLACommisionSchema = new LACommisionSchema();
                System.out.println("======" + COMMISIONSN);
                mLACommisionSchema.setCommisionSN(COMMISIONSN);
                mLACommisionSchema.setCommisionBaseNo("0000000000");
                //WageNo 563
                mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
                mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
                mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
                mLACommisionSchema.setManageCom(tLJAPayPersonSchema.
                                                getManageCom());
                mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
                mLACommisionSchema.setRiskCode(tLCPolSchema.getRiskCode());
                mLACommisionSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
                mLACommisionSchema.setDutyCode(tLJAPayPersonSchema.getDutyCode());
                mLACommisionSchema.setPayPlanCode(tLJAPayPersonSchema.
                                                  getPayPlanCode());
                mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
                mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
                mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
                mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
                mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
                mLACommisionSchema.setReceiptNo(tLJAPayPersonSchema.getPayNo());
                mLACommisionSchema.setTPayDate(tLJAPayPersonSchema.getPayDate());
                mLACommisionSchema.setTEnterAccDate(tLJAPayPersonSchema.
                        getEnterAccDate());
                mLACommisionSchema.setTConfDate(tLJAPayPersonSchema.getConfDate());
                mLACommisionSchema.setTMakeDate(tLJAPayPersonSchema.getMakeDate());
                // CalDate
                mLACommisionSchema.setCommDate(CurrentDate);
                mLACommisionSchema.setTransMoney(tLJAPayPersonSchema.
                                                 getSumActuPayMoney());
                mLACommisionSchema.setTransState("00");//正常保单
                mLACommisionSchema.setTransStandMoney(tLJAPayPersonSchema.
                        getSumDuePayMoney());
                mLACommisionSchema.setLastPayToDate(tLJAPayPersonSchema.
                        getLastPayToDate());
                mLACommisionSchema.setCurPayToDate(tLJAPayPersonSchema.
                        getCurPayToDate());
                mLACommisionSchema.setTransType("ZC");
                //TransState 交易处理状态 590 - 614
                mLACommisionSchema.setCommDire("1");
                if(tLCContSchema.getSaleChnl().equals("07"))
                {
                    mLACommisionSchema.setF1("02");
                    mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
                }
                else
                {
                    mLACommisionSchema.setF1("01");
                }
                //xjh Add 05/01/27 ,判断是否是批改保单
                tflag = getIncomeType(tLJAPayPersonSchema.getPayNo());
                mLACommisionSchema.setFlag(tflag);
                mLACommisionSchema.setPayYear(PayYear);
                mLACommisionSchema.setReNewCount(tRenewCount);
                mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
                mLACommisionSchema.setYears(tLCPolSchema.getYears());
                mLACommisionSchema.setPayCount(tLJAPayPersonSchema.getPayCount());
                mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
                mLACommisionSchema.setGetPolDate(tLCContSchema.getGetPolDate());
                //BranchType 581
                mLACommisionSchema.setAgentCom(tLCPolSchema.getAgentCom());
                mLACommisionSchema.setAgentType(tLJAPayPersonSchema.
                                                getAgentType());
                mLACommisionSchema.setAgentCode(tLJAPayPersonSchema.
                                                getAgentCode());
                /******************************************
                 * ****************************************
                 * *****************************************
                 */

                String tPolType = tLCPolSchema.getStandbyFlag2();
                mLACommisionSchema.setPolType(tPolType == null ||
                                              tPolType.equals("") ?
                                              "1" : tPolType); //0:优惠 1：正常
                mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());
                /*******************************************************
                 * *****************************************************
                 * *****************************************************
                 */
                mLACommisionSchema.setP11(tLCContSchema.getAppntName());
                mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
                mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
                mLACommisionSchema.setP14(tLCPolSchema.getPrtNo());
                mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
                mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //录入日期
                mLACommisionSchema.setCustomGetPolDate(tLCContSchema.
                        getCustomGetPolDate()); //签字日期
                // riskmark
                String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
                mLACommisionSchema.setScanDate(tScanDate);
                mLACommisionSchema.setOperator(mGlobalInput.Operator);
                mLACommisionSchema.setMakeDate(CurrentDate);
                mLACommisionSchema.setMakeTime(CurrentTime);
                mLACommisionSchema.setModifyDate(CurrentDate);
                mLACommisionSchema.setModifyTime(CurrentTime);
                String tAcType = "";
                if (tLCPolSchema.getSaleChnl().equals("03")) {
                    //如果契约渠道为中介
                    String tAgentCom = mLACommisionSchema.getAgentCom();
                    LAComDB tLAComDB = new LAComDB();
                    tLAComDB.setAgentCom(tAgentCom);
                    if (!tLAComDB.getInfo()) {
                        LAWageActivityLogSchema tLAWageActivityLogSchema = new
                                LAWageActivityLogSchema();
                        tLAWageActivityLogSchema.setAgentCode(
                                mLAAgentSchema.getAgentCode());
                        tLAWageActivityLogSchema.setAgentGroup(mLAAgentSchema.
                                getAgentGroup());
                        tLAWageActivityLogSchema.setContNo(mLACommisionSchema.
                                getContNo());
                        tLAWageActivityLogSchema.setDescribe(
                                "渠道类型为中介，但是没有查到机构编码为" + tAgentCom + "的中介机构!");
                        tLAWageActivityLogSchema.setGrpContNo(
                                mLACommisionSchema.getGrpContNo());
                        tLAWageActivityLogSchema.setGrpPolNo(mLACommisionSchema.
                                getGrpPolNo());
                        tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                        tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                        tLAWageActivityLogSchema.setManageCom(
                                mLACommisionSchema.getManageCom());
                        tLAWageActivityLogSchema.setOperator(mGlobalInput.
                                Operator);
                        tLAWageActivityLogSchema.setOtherNo("");
                        tLAWageActivityLogSchema.setOtherNoType("");
                        tLAWageActivityLogSchema.setPolNo(mLACommisionSchema.
                                getPolNo());
                        tLAWageActivityLogSchema.setRiskCode(mLACommisionSchema.
                                getRiskCode());

                        String tSerialNo = PubFun1.CreateMaxNo(
                                "WAGEACTIVITYLOG", 12);
                        tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                        tLAWageActivityLogSchema.setWageLogType("01");
                        tLAWageActivityLogSchema.setWageNo(mLAWageLogSchema.
                                getWageNo());
                        mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                        continue;
                    } else {
                        tAcType = tLAComDB.getSchema().getACType();
                    }
                }
                //添加对于互动中介的处理
                if (tLCPolSchema.getSaleChnl().equals("15")) 
                {
                	tAcType = checkActive(mLACommisionSchema);
                	if("".equals(tAcType))
                	{
                		continue;
                	}
                }

                String[] tBranchTypes = AgentPubFun.switchSalChnl(
                        tLCPolSchema.
                        getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                        tAcType);
                mLACommisionSchema.setBranchType(tBranchTypes[0]);
                mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
                mLACommisionSchema.setBranchType3(tBranchTypes[2]);
                String tFlag="01";
                if(mLACommisionSchema.getReNewCount()>0)
                {
                    tFlag="02";
                }
                //简易险
                if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
                {
                    tFlag="02";
                }
                String tConfDate = "";
                tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"ZC",mLAWageLogSchema.getEndDate(),
                		                         tLJAPayPersonSchema.getConfDate(),tLJAPayPersonSchema.getPayNo());
                //--查询回访成功数据
                String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
                //String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
                String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                String tTransState = mLACommisionSchema.getTransState();
                // add new 
                String tAgentcode =mLACommisionSchema.getAgentCode() ;
                String caldate = this.calCalDateForAll(tBranchTypes[0],
                        tBranchTypes[1],
                        tLCContSchema.getSignDate(),
                        mLACommisionSchema.getGetPolDate(),
                        String.valueOf(
                                mLACommisionSchema.
                                getPayCount()),
                        mLACommisionSchema.getTMakeDate(),
                       mLACommisionSchema.getCustomGetPolDate(),
                        mLACommisionSchema.getCValiDate(),tFlag,
                        tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,tLCContSchema.getGrpContNo(),mLACommisionSchema.getRiskCode());
                if (!caldate.equals("") && !caldate.equals("0")) {
                    sql =
                            "select yearmonth from LAStatSegment where startdate<='" +
                            caldate + "' and enddate>='" + caldate +
                            "' and stattype='5' ";
                    tExeSQL = new ExeSQL();
                    tWageNo = tExeSQL.getOneValue(sql);

                    mLACommisionSchema.setWageNo(tWageNo);
                    mLACommisionSchema.setCalDate(caldate);
                }
                mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode()); //直辖组
                tAgentCode = mLACommisionSchema.getAgentCode().trim();
                tBranchCode = mLAAgentSchema.getBranchCode(); //职级对应机构代码
                if (tBranchCode == null) {
                    return false;
                }
                LABranchGroupSchema tLABranchGroupSchema;
                tLABranchGroupSchema = new LABranchGroupSchema();
                tLABranchGroupSchema = querytLABranchGroup(tBranchCode);
                mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                        getBranchSeries());
                tBranchAttr = tLABranchGroupSchema.getBranchAttr();
                mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
                mLACommisionSchema.setBranchAttr(tBranchAttr);
                System.out.println("##########LACOMMISION--放入set###########");
                this.mLACommisionSet.add(mLACommisionSchema);
                System.out.println("@@@@@@@@@@@@@@@@@@@@@mLACommisionSet的条数：'"+mLACommisionSet.size()+"'@@@@@@@@@@@@@@@@@");
            }
        }

        System.out.println("*****************查询个单   结束***************");


        System.out.println("**万能险种330801 331801 331201 331301 追加保费**********查询LJAPayPerson实收表  开始***************");
        tSSRS = queryOmniLJAPayPerson();
        if (tSSRS==null) {
            return false;
        }
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
            tLJAPayPersonDB.setPolNo(tSSRS.GetText(i, 1));
            tLJAPayPersonDB.setPayNo(tSSRS.GetText(i, 2));
            tLJAPayPersonDB.setDutyCode(tSSRS.GetText(i, 3));
            tLJAPayPersonDB.setPayPlanCode(tSSRS.GetText(i, 4));
            tLJAPayPersonDB.setPayType(tSSRS.GetText(i, 5));
            tLJAPayPersonDB.getInfo();
            tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema = tLJAPayPersonDB.getSchema();
            tContNo = tLJAPayPersonDB.getContNo();
            PolNo = tLJAPayPersonDB.getPolNo();
            String tPayTypeFlag=tLJAPayPersonDB.getPayTypeFlag();
            String tRiskCode=tLJAPayPersonDB.getRiskCode();
            if(tPayTypeFlag == null )
            {
                tPayTypeFlag="";
            }
            //处理 个险的卡单数据
            String tflag = getIncomeType(tLJAPayPersonSchema.getPayNo());
            if (tflag.equals("15")) { //个险提数或则全部提数，团险等提数不能提
                if (mBranchType == null || mBranchType.equals("") ||
                    (mBranchType.equals("1") && mBranchType2.equals("01"))
                  //增加互动渠道
                    ||(mBranchType.equals("5") && mBranchType2.equals("01"))) 
                    {
                    if (!dealperCommision(tLJAPayPersonSchema, tContNo, PolNo)) {
                        return false;
                    }
                }
            }
            else {

                tLCContSchema = new LCContSchema();
                tLCContSchema = this.queryLCCont(tContNo);
                if (tLCContSchema == null) {
                    continue;
                }
                System.out.println("~~~~~~~再报错!!!!!!!!!!~~~~~~~~~~~");
                tLCPolSchema = new LCPolSchema();
               try{
                tLCPolSchema = this.queryLCPol(PolNo);
               }
               catch(Exception ex)
               {
               	 ex.getStackTrace();
               }
                if (tLCPolSchema == null) {
                    continue;
                }
                //续保的处理
                LCRnewStateLogSchema tLCRnewStateLogSchema = new
                        LCRnewStateLogSchema();
                tLCRnewStateLogSchema = this.queryLCRnewPol(PolNo);

                if (tLCRnewStateLogSchema == null) {
                    tRenewCount = 0;
                }
                else
                {
                    if(tPayTypeFlag.equals("1"))
                    {

//                    	if (tRiskCode.equals("320106")){
//                    		tRenewCount = 0;
//                    	}
//                    	else 
                        tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                    }
                    else
                    {
                         tRenewCount = 0;
                    }
                }
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLJAPayPersonSchema.getAgentCode());
                tLAAgentDB.getInfo();
                mLAAgentSchema = tLAAgentDB.getSchema();
                COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
                if (tLJAPayPersonSchema.getPayIntv() == 0) { //趸交payIntv = 0
                    PayYear = 0;
                } else {
                    //计算交费年度=保单生效日到交至日期
                    PayYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),
                                                 fDate.
                                                 getString(PubFun.calDate(fDate.
                            getDate(tLJAPayPersonSchema.getCurPayToDate()), -2,
                            "D", null)),
                                                 "Y");
                    if(PayYear<0)  PayYear=0;
                }

                mLACommisionSchema = new LACommisionSchema();
                System.out.println("======" + COMMISIONSN);
                mLACommisionSchema.setCommisionSN(COMMISIONSN);
                mLACommisionSchema.setCommisionBaseNo("0000000000");
                //WageNo 563
                mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
                mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
                mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
                mLACommisionSchema.setManageCom(tLJAPayPersonSchema.
                                                getManageCom());
                mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
                mLACommisionSchema.setRiskCode(tLCPolSchema.getRiskCode());
                mLACommisionSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
                mLACommisionSchema.setDutyCode(tLJAPayPersonSchema.getDutyCode());
                mLACommisionSchema.setPayPlanCode(tLJAPayPersonSchema.
                                                  getPayPlanCode());
                mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
                mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
                mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
                mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
                mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
                mLACommisionSchema.setReceiptNo(tLJAPayPersonSchema.getPayNo());
                mLACommisionSchema.setTPayDate(tLJAPayPersonSchema.getPayDate());
                mLACommisionSchema.setTEnterAccDate(tLJAPayPersonSchema.
                        getEnterAccDate());
                mLACommisionSchema.setTConfDate(tLJAPayPersonSchema.getConfDate());
                mLACommisionSchema.setTMakeDate(tLJAPayPersonSchema.getMakeDate());
                // CalDate
                mLACommisionSchema.setCommDate(CurrentDate);
                mLACommisionSchema.setTransMoney(tLJAPayPersonSchema.
                                                 getSumActuPayMoney());
                mLACommisionSchema.setTransState("03");//追加保费
                mLACommisionSchema.setTransStandMoney(tLJAPayPersonSchema.
                        getSumDuePayMoney());
                mLACommisionSchema.setLastPayToDate(tLJAPayPersonSchema.
                        getLastPayToDate());
                mLACommisionSchema.setCurPayToDate(tLJAPayPersonSchema.
                        getCurPayToDate());
                mLACommisionSchema.setTransType("ZC");
                //TransState 交易处理状态 590 - 614
                mLACommisionSchema.setCommDire("1");
                if(tLCContSchema.getSaleChnl().equals("07"))
                {
                    mLACommisionSchema.setF1("02");
                    mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
                }
                else
                {
                    mLACommisionSchema.setF1("01");
                }
                //xjh Add 05/01/27 ,判断是否是批改保单
                tflag = getIncomeType(tLJAPayPersonSchema.getPayNo());
                mLACommisionSchema.setFlag(tflag);
                mLACommisionSchema.setPayYear(PayYear);
                mLACommisionSchema.setReNewCount(tRenewCount);
                mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
                mLACommisionSchema.setYears(tLCPolSchema.getYears());
                mLACommisionSchema.setPayCount(tLJAPayPersonSchema.getPayCount());
                mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
                mLACommisionSchema.setGetPolDate(tLCContSchema.getGetPolDate());
                //BranchType 581
                mLACommisionSchema.setAgentCom(tLCPolSchema.getAgentCom());
                mLACommisionSchema.setAgentType(tLJAPayPersonSchema.
                                                getAgentType());
                mLACommisionSchema.setAgentCode(tLJAPayPersonSchema.
                                                getAgentCode());
                /******************************************
                 * ****************************************
                 * *****************************************
                 */

                String tPolType = tLCPolSchema.getStandbyFlag2();
                mLACommisionSchema.setPolType(tPolType == null ||
                                              tPolType.equals("") ?
                                              "1" : tPolType); //0:优惠 1：正常
                mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());
                /*******************************************************
                 * *****************************************************
                 * *****************************************************
                 */
                mLACommisionSchema.setP11(tLCContSchema.getAppntName());
                mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
                mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
                mLACommisionSchema.setP14(tLCPolSchema.getPrtNo());
                mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
                mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //录入日期
                mLACommisionSchema.setCustomGetPolDate(tLCContSchema.
                        getCustomGetPolDate()); //签字日期
                // riskmark
                String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
                mLACommisionSchema.setScanDate(tScanDate);
                mLACommisionSchema.setOperator(mGlobalInput.Operator);
                mLACommisionSchema.setMakeDate(CurrentDate);
                mLACommisionSchema.setMakeTime(CurrentTime);
                mLACommisionSchema.setModifyDate(CurrentDate);
                mLACommisionSchema.setModifyTime(CurrentTime);
                String tAcType = "";
                if (tLCPolSchema.getSaleChnl().equals("03")) {
                    //如果契约渠道为中介
                    String tAgentCom = mLACommisionSchema.getAgentCom();
                    LAComDB tLAComDB = new LAComDB();
                    tLAComDB.setAgentCom(tAgentCom);
                    if (!tLAComDB.getInfo()) {
                        LAWageActivityLogSchema tLAWageActivityLogSchema = new
                                LAWageActivityLogSchema();
                        tLAWageActivityLogSchema.setAgentCode(
                                mLAAgentSchema.getAgentCode());
                        tLAWageActivityLogSchema.setAgentGroup(mLAAgentSchema.
                                getAgentGroup());
                        tLAWageActivityLogSchema.setContNo(mLACommisionSchema.
                                getContNo());
                        tLAWageActivityLogSchema.setDescribe(
                                "渠道类型为中介，但是没有查到机构编码为" + tAgentCom + "的中介机构!");
                        tLAWageActivityLogSchema.setGrpContNo(
                                mLACommisionSchema.getGrpContNo());
                        tLAWageActivityLogSchema.setGrpPolNo(mLACommisionSchema.
                                getGrpPolNo());
                        tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                        tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                        tLAWageActivityLogSchema.setManageCom(
                                mLACommisionSchema.getManageCom());
                        tLAWageActivityLogSchema.setOperator(mGlobalInput.
                                Operator);
                        tLAWageActivityLogSchema.setOtherNo("");
                        tLAWageActivityLogSchema.setOtherNoType("");
                        tLAWageActivityLogSchema.setPolNo(mLACommisionSchema.
                                getPolNo());
                        tLAWageActivityLogSchema.setRiskCode(mLACommisionSchema.
                                getRiskCode());

                        String tSerialNo = PubFun1.CreateMaxNo(
                                "WAGEACTIVITYLOG", 12);
                        tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                        tLAWageActivityLogSchema.setWageLogType("01");
                        tLAWageActivityLogSchema.setWageNo(mLAWageLogSchema.
                                getWageNo());
                        mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                        continue;
                    } else {
                        tAcType = tLAComDB.getSchema().getACType();
                    }
                }
                //添加对于互动中介的处理
                if (tLCPolSchema.getSaleChnl().equals("15")) 
                {
                	tAcType = checkActive(mLACommisionSchema);
                	if("".equals(tAcType))
                	{
                		continue;
                	}
                }
                String[] tBranchTypes = AgentPubFun.switchSalChnl(
                        tLCPolSchema.
                        getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                        tAcType);
                mLACommisionSchema.setBranchType(tBranchTypes[0]);
                mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
                mLACommisionSchema.setBranchType3(tBranchTypes[2]);
                String tFlag="01";
                if(mLACommisionSchema.getReNewCount()>0)
                {
                    tFlag="02";
                }
                //简易险
                if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
                {
                    tFlag="02";
                }
                String tConfDate = "";
                tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"ZC",mLAWageLogSchema.getEndDate(),
                		                        tLJAPayPersonSchema.getConfDate(),tLJAPayPersonSchema.getPayNo());
		        //--查询回访成功数据
                String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
                //String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
                String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                String tTransState = mLACommisionSchema.getTransState();
                // add new 
                String tAgentcode =mLACommisionSchema.getAgentCode() ;
                
                String caldate = this.calCalDateForAll(tBranchTypes[0],
                        tBranchTypes[1],
                        tLCContSchema.getSignDate(),
                        mLACommisionSchema.getGetPolDate(),
                        String.valueOf(
                                mLACommisionSchema.
                                getPayCount()),
                        mLACommisionSchema.getTMakeDate(),
                       mLACommisionSchema.getCustomGetPolDate(),
                        mLACommisionSchema.getCValiDate(),tFlag,
                        tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,tLCContSchema.getGrpContNo(),mLACommisionSchema.getRiskCode());
                if (!caldate.equals("") && !caldate.equals("0")) {
                    sql =
                            "select yearmonth from LAStatSegment where startdate<='" +
                            caldate + "' and enddate>='" + caldate +
                            "' and stattype='5' ";
                    tExeSQL = new ExeSQL();
                    tWageNo = tExeSQL.getOneValue(sql);
                    mLACommisionSchema.setTConfDate(tConfDate);
                    mLACommisionSchema.setWageNo(tWageNo);
                    mLACommisionSchema.setCalDate(caldate);
                }
                mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode()); //直辖组
                tAgentCode = mLACommisionSchema.getAgentCode().trim();
                tBranchCode = mLAAgentSchema.getBranchCode(); //职级对应机构代码
                if (tBranchCode == null) {
                    return false;
                }
                LABranchGroupSchema tLABranchGroupSchema;
                tLABranchGroupSchema = new LABranchGroupSchema();
                tLABranchGroupSchema = querytLABranchGroup(tBranchCode);
                mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                        getBranchSeries());
                tBranchAttr = tLABranchGroupSchema.getBranchAttr();
                mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
                mLACommisionSchema.setBranchAttr(tBranchAttr);
                this.mLACommisionSet.add(mLACommisionSchema);
            }
        }

        System.out.println("**万能险种330801 331801 331201  331301追加保费**********查询LJAPayPerson实收表  结束***************");

        System.out.println("**万能险种330801 331801 331201  331301追加保费**********查询LJAGetEndorse批改退费表  开始***************");
        LJAGetEndorseSchema tLJAGetEndorseSchema;
        tSSRS = this.queryOmniLJAGetEndorse();
        if (tSSRS == null) {
            return false;
        }
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

//            LACommisionDB wLACommisionDB = new LACommisionDB();
//            wLACommisionDB.setPolNo(tSSRS.GetText(i, 5));
//            wLACommisionDB.setTransType("ZC");
//            wLACommisionDB.setTransState("03");//追加保费
//            wLACommisionDB.setDutyCode(tSSRS.GetText(i, 8));
//            wLACommisionDB.setPayPlanCode(tSSRS.GetText(i, 9));
//            LACommisionSet tLACommisionSet = new LACommisionSet();
//            tLACommisionSet = wLACommisionDB.query();
//            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
//                continue;
//            }
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setEndorsementNo(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setPolNo(tSSRS.GetText(i, 5));
            tLJAGetEndorseDB.setOtherNo(tSSRS.GetText(i, 6));
            tLJAGetEndorseDB.setOtherNoType(tSSRS.GetText(i, 7));
            tLJAGetEndorseDB.setDutyCode(tSSRS.GetText(i, 8));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 9));
            tLJAGetEndorseDB.getInfo();
            tLJAGetEndorseSchema = tLJAGetEndorseDB.getSchema();
            tAgentCode = tLJAGetEndorseSchema.getAgentCode();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            tLAAgentDB.getInfo();
            mLAAgentSchema = tLAAgentDB.getSchema();
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            //只处理个单
            if (tGrpPolNo.equals("00000000000000000000")) {
                PolNo = tLJAGetEndorseSchema.getPolNo();
                tContNo = tLJAGetEndorseSchema.getContNo();
                tLCPolSchema = new LCPolSchema();
                tLCContSchema = new LCContSchema();
                try{
                tLCPolSchema = queryLCPol(PolNo);
                }
                catch(Exception ex)
                {
                        ex.getStackTrace();
                }
                if (tLCPolSchema == null) {
                    continue;
                }
                tLCContSchema = queryLCCont(tContNo);
                if (tLCContSchema == null) {
                    continue;
                }
                //续保的处理
                LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
                tLCRnewStateLogSchema = this.queryLCRnewPol(PolNo);
                if (tLCRnewStateLogSchema == null) {
                    tRenewCount = 0;
                }
                else {

//                	if (tRiskCode.equals("320106")){
//                		tRenewCount = 0;
//                	}
//                	else 
                    tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }
                 String tRiskCode=tLCPolSchema.getRiskCode();
               //增加互动渠道的判断
                 boolean zbFlag = false;
               if (tRiskCode.equals("330801")||tRiskCode.equals("331801")||tRiskCode.equals("332701")||tRiskCode.equals("331201")||tRiskCode.equals("331301")||tRiskCode.equals("333901")
            		   ||tRiskCode.equals("334701")||tRiskCode.equals("334801")||tRiskCode.equals("370201")|| tRiskCode.equals("340501") || tRiskCode.equals("340601")|| tRiskCode.equals("340602")
               )
               {
            	   zbFlag= true;
               }
               else if(mBranchType.equals("5") && mBranchType2.equals("01"))
               {     
            	   if (tRiskCode.equals("330801")||tRiskCode.equals("331801")
                 		  ||tRiskCode.equals("332701")||tRiskCode.equals("331201")
                 		  ||tRiskCode.equals("331301")||tRiskCode.equals("331601")
                 		  ||tRiskCode.equals("331501")||tRiskCode.equals("333901")||tRiskCode.equals("334701")||tRiskCode.equals("334801")||tRiskCode.equals("370201")|| tRiskCode.equals("340501") || tRiskCode.equals("340601") )
            	   {
            		   zbFlag= true;
            	   }
               }
               if(zbFlag){
                COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
                //计算交费年度=保单生效日到交至日期
                PayYear = 0;
                //填充直接佣金明细表（佣金扎账表）
                mLACommisionSchema = new LACommisionSchema();
                mLACommisionSchema.setCommisionSN(COMMISIONSN);
                mLACommisionSchema.setCommisionBaseNo("0000000000");

                mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
                mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
                mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
                mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); //新加的 批单号码
                mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.
                                                getManageCom());
                mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
                mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
                mLACommisionSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
                mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
                mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.
                                                  getPayPlanCode());
                mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
                mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
                mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
                mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
                mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
                mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.
                                                getActuGetNo());
                mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
                mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.
                        getEnterAccDate());
                mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.
                                                getGetConfirmDate());
                mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.
                                                getMakeDate());

                mLACommisionSchema.setCommDate(CurrentDate);

                mLACommisionSchema.setTransMoney(tLJAGetEndorseSchema.getGetMoney());
                mLACommisionSchema.setTransState("03");//03-代表追加保费

                mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                        getTransMoney());
                mLACommisionSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
                mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                        getGetDate());
                mLACommisionSchema.setTransType("ZC");
                if(tLCContSchema.getSaleChnl().equals("07"))
                {
                    mLACommisionSchema.setF1("02");//职团开拓
                    mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
                }
                else
                {
                    mLACommisionSchema.setF1("01");
                }
                mLACommisionSchema.setFlag("1"); //犹豫期撤件
                mLACommisionSchema.setPayYear(PayYear);
                mLACommisionSchema.setReNewCount(tRenewCount);
                mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
                mLACommisionSchema.setYears(tLCPolSchema.getYears());
                mLACommisionSchema.setPayCount(1);
                mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
                mLACommisionSchema.setGetPolDate(tLCContSchema.getGetPolDate());
                mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
                mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                                                getAgentType());
                mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                                                getAgentCode());
                String tPolType = tLCPolSchema.getStandbyFlag2();
                mLACommisionSchema.setPolType(tPolType == null ||
                                              tPolType.equals("") ?
                                              "1" : tPolType);
                mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());
                mLACommisionSchema.setP11(tLCContSchema.getAppntName());
                mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
                mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
                mLACommisionSchema.setP14(tLCPolSchema.getPrtNo()); //cg add 2004-01-13
                mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
                mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //交单日期
                mLACommisionSchema.setCustomGetPolDate(tLCContSchema.
                        getCustomGetPolDate()); //签字日期
                String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
                mLACommisionSchema.setScanDate(tScanDate);
                mLACommisionSchema.setOperator(mGlobalInput.Operator);
                mLACommisionSchema.setMakeDate(CurrentDate);
                mLACommisionSchema.setMakeTime(CurrentTime);
                mLACommisionSchema.setModifyDate(CurrentDate);
                mLACommisionSchema.setModifyTime(CurrentTime);
                mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode());
                tAgentCode = mLACommisionSchema.getAgentCode().trim();
                tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
                if (tAgentGroup == null) {
                    return false;
                }
                LABranchGroupSchema tLABranchGroupSchema;
                tLABranchGroupSchema = new LABranchGroupSchema();
                tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);

                mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                        getBranchSeries());
                String tAcType = "";
                if (tLCPolSchema.getSaleChnl().equals("03")) {
                    //如果契约渠道为中介
                    String tAgentCom = mLACommisionSchema.getAgentCom();
                    LAComDB tLAComDB = new LAComDB();
                    tLAComDB.setAgentCom(tAgentCom);
                    if (!tLAComDB.getInfo()) {
                        LAWageActivityLogSchema tLAWageActivityLogSchema = new
                                LAWageActivityLogSchema();
                        tLAWageActivityLogSchema.setAgentCode(
                                mLAAgentSchema.getAgentCode());
                        tLAWageActivityLogSchema.setAgentGroup(
                                mLAAgentSchema.getAgentGroup());
                        tLAWageActivityLogSchema.setContNo(mLACommisionSchema.
                                getContNo());
                        tLAWageActivityLogSchema.setDescribe(
                                "渠道类型为中介，但是没有查到机构编码为" + tAgentCom + "的中介机构!");
                        tLAWageActivityLogSchema.setGrpContNo(
                                mLACommisionSchema.getGrpContNo());
                        tLAWageActivityLogSchema.setGrpPolNo(mLACommisionSchema.
                                getGrpPolNo());
                        tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                        tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                        tLAWageActivityLogSchema.setManageCom(
                                mLACommisionSchema.getManageCom());
                        tLAWageActivityLogSchema.setOperator(mGlobalInput.
                                Operator);
                        tLAWageActivityLogSchema.setOtherNo("");
                        tLAWageActivityLogSchema.setOtherNoType("");
                        tLAWageActivityLogSchema.setPolNo(mLACommisionSchema.
                                getPolNo());
                        tLAWageActivityLogSchema.setRiskCode(mLACommisionSchema.
                                getRiskCode());

                        String tSerialNo = PubFun1.CreateMaxNo(
                                "WAGEACTIVITYLOG", 12);
                        tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                        tLAWageActivityLogSchema.setWageLogType("01");
                        tLAWageActivityLogSchema.setWageNo(mLAWageLogSchema.
                                getWageNo());
                        mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                        continue;
                    }
                    else {
                        tAcType = tLAComDB.getSchema().getACType();
                    }
                }
                //添加对于互动中介的处理
                if (tLCPolSchema.getSaleChnl().equals("15")) 
                {
                	tAcType = checkActive(mLACommisionSchema);
                	if("".equals(tAcType))
                	{
                		continue;
                	}
                }
                String[] tBranchTypes = AgentPubFun.switchSalChnl(
                        tLCPolSchema.
                        getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                        tAcType);

                tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
                if (tBranchTypes == null) {
                    return false;
                }
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tBranchCode);
                tLABranchGroupDB.getInfo();
                tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
                if (tBranchAttr == null) {
                    return false;
                }
                mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                        getBranchSeries());
                mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
                mLACommisionSchema.setBranchAttr(tBranchAttr);
                mLACommisionSchema.setBranchType(tBranchTypes[0]);
                mLACommisionSchema.setBranchType2(tBranchTypes[1]);
                mLACommisionSchema.setBranchType3(tBranchTypes[2]);
                String mon = mLACommisionSchema.getGetPolDate();
                //先根据回单日期置CalDate, WAGENO
                String tConfDate = "";
                tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"WT",mLAWageLogSchema.getEndDate(),
                		               tLJAGetEndorseSchema.getGetConfirmDate(),tLJAGetEndorseSchema.
                                       getActuGetNo());
		        //--查询回访成功数据
                String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
                //String tRiskCode = tLACommisionSchema.getRiskCode(); // 查询出险种信息
                String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                String tTransState = mLACommisionSchema.getTransState();
                // add new 
                String tAgentcode =mLACommisionSchema.getAgentCode() ;
                String caldate = this.calCalDateForAll(tBranchTypes[0],
                        tBranchTypes[1],
                        mLACommisionSchema.getSignDate(),
                        tLCContSchema.getGetPolDate(), "0",
                        tLJAGetEndorseSchema.
                        getMakeDate(), mLACommisionSchema.getCustomGetPolDate(),
                        mLACommisionSchema.getCValiDate(), "02",
                        tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,tLCContSchema.getGrpContNo(),mLACommisionSchema.getRiskCode());

                if (!caldate.equals("") && !caldate.equals("0")) {
                    sql =
                            "select yearmonth from LAStatSegment where startdate<='" +
                            caldate + "' and enddate>='" + caldate +
                            "' and stattype='5' ";
                    tExeSQL = new ExeSQL();
                    tWageNo = tExeSQL.getOneValue(sql);
                    mLACommisionSchema.setTConfDate(tConfDate);
                    mLACommisionSchema.setWageNo(tWageNo);
                    mLACommisionSchema.setCalDate(caldate);
                }

                mLACommisionSchema.setCommDire("1");
                mLACommisionSchema.setFlag("1");

                mLACommisionSet.add(mLACommisionSchema);

              }
                }
              }
        System.out.println("**万能险种330801 331801 331201 331301追加保费**********查询LJAGetEndorse批改退费表  结束***************");


        System.out.println("*****************查询LJAGetEndorse批改退费表  开始***************");
        tSSRS = this.queryLJAGetEndorse();
        if (tSSRS == null) {
            return false;
        }
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            //判断此犹豫期撤单件 是否已经提过数 ,如果提过,则不再提取
            //提数回退时 不回退上个月的数据,所以会出现此种情况
            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setPolNo(tSSRS.GetText(i, 5));
            wLACommisionDB.setTransType("WT");
            wLACommisionDB.setDutyCode(tSSRS.GetText(i, 8));
            wLACommisionDB.setPayPlanCode(tSSRS.GetText(i, 9));
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setEndorsementNo(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setPolNo(tSSRS.GetText(i, 5));
            tLJAGetEndorseDB.setOtherNo(tSSRS.GetText(i, 6));
            tLJAGetEndorseDB.setOtherNoType(tSSRS.GetText(i, 7));
            tLJAGetEndorseDB.setDutyCode(tSSRS.GetText(i, 8));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 9));
            tLJAGetEndorseDB.getInfo();
            tLJAGetEndorseSchema = tLJAGetEndorseDB.getSchema();
            tAgentCode = tLJAGetEndorseSchema.getAgentCode();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            tLAAgentDB.getInfo();
            mLAAgentSchema = tLAAgentDB.getSchema();
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            //只处理个单
            if (tGrpPolNo.equals("00000000000000000000")) {
                PolNo = tLJAGetEndorseSchema.getPolNo();
                tContNo = tLJAGetEndorseSchema.getContNo();
                tLCPolSchema = new LCPolSchema();
                tLCContSchema = new LCContSchema();
                try {
                    tLCPolSchema = queryLCPol(PolNo);
                } catch (Exception ex) {
                    ex.getStackTrace();
                }
                if (tLCPolSchema == null) {
                    continue;
                }
                tLCContSchema = queryLCCont(tContNo);
                if (tLCContSchema == null) {
                    continue;
                }
                //续保的处理
                LCRnewStateLogSchema tLCRnewStateLogSchema = new
                        LCRnewStateLogSchema();
                tLCRnewStateLogSchema = this.queryLCRnewPol(PolNo);
                if (tLCRnewStateLogSchema == null) {
                    tRenewCount = 0;
                } else {

//                	if (tRiskCode.equals("320106")){
//                		tRenewCount = 0;
//                	}
//                	else 
                    tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }
                /////////////////////////////////////////////////////////////////
                // 处理犹豫期退保的情况
                String tRiskCode = tLCPolSchema.getRiskCode();
                if (tRiskCode.equals("330801")||tRiskCode.equals("331801")||tRiskCode.equals("332701")||tRiskCode.equals("333901")
                		||tRiskCode.equals("334701")||tRiskCode.equals("334801")||tRiskCode.equals("370201")		
                ) {
                  //  if (mBranchType == null || mBranchType.equals("") ||(mBranchType.equals("1") && mBranchType2.equals("01"))) {
                        if (!dealOmniEndorseCommision(tLCContSchema,
                                tLCPolSchema, tLJAGetEndorseSchema, tContNo,
                                PolNo)) {
                            return false;
                     //   }
                    }
                }
                //增加对于互动渠道的处理   yangyang 2015-2-6
                else if(mBranchType.equals("5") && mBranchType2.equals("01")
             		  &&(tRiskCode.equals("330801")||tRiskCode.equals("331801")
             		  ||tRiskCode.equals("332701")||tRiskCode.equals("331201")
             		  ||tRiskCode.equals("331301")||tRiskCode.equals("331601")
             		  ||tRiskCode.equals("331501")||tRiskCode.equals("333901"))
             	||tRiskCode.equals("334701")||tRiskCode.equals("334801")||tRiskCode.equals("370201")	  
                )
                {
                	if (!dealOmniEndorseCommision(tLCContSchema,
                            tLCPolSchema, tLJAGetEndorseSchema, tContNo,
                            PolNo)) {
                        return false;
                    }
                }
              //增加税优险种保费的拆分风险保费与帐户保费  
                else if(tRiskCode.equals("122601")||tRiskCode.equals("122701")
             		  ||tRiskCode.equals("122801")||tRiskCode.equals("122901")
             		  ||tRiskCode.equals("123001")||tRiskCode.equals("123101")
             		  ||tRiskCode.equals("123501")||tRiskCode.equals("123601")
             	      ||tRiskCode.equals("123602"))
                {
                	if (!dealTaxEndorseCommision(tLCContSchema,
                            tLCPolSchema, tLJAGetEndorseSchema, tContNo,
                            PolNo)) {
                        return false;
                    }
                }
                else {

                 ///////////////////////////////////////////////////////////////
                    COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
                    //计算交费年度=保单生效日到交至日期
                    PayYear = 0;
                    //填充直接佣金明细表（佣金扎账表）
                    mLACommisionSchema = new LACommisionSchema();
                    mLACommisionSchema.setCommisionSN(COMMISIONSN);
                    mLACommisionSchema.setCommisionBaseNo("0000000000");
                    //WageNo 783
                    mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                    mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                    mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
                    mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
                    mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
                    mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); //新加的 批单号码
                    mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.
                            getManageCom());
                    mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
                    mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.
                            getRiskCode());
                    mLACommisionSchema.setRiskVersion(tLCPolSchema.
                            getRiskVersion());
                    mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.
                            getDutyCode());
                    mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.
                            getPayPlanCode());
                    mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
                    mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
                    mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
                    mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
                    mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
                    mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.
                            getActuGetNo());
                    mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.
                            getGetDate());
                    mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.
                            getEnterAccDate());
                    mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.
                            getGetConfirmDate());
                    mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.
                            getMakeDate());
                    // CalcDate
                    mLACommisionSchema.setCommDate(CurrentDate);
                    mLACommisionSchema.setTransMoney(
                            0 -
                            java.lang.Math.abs(tLJAGetEndorseSchema.getGetMoney()));
                    if(tLJAGetEndorseSchema.getPayPlanCode().equals("222222")){
                     mLACommisionSchema.setTransState("03"); //追加保费
                    }
                    else{
                        mLACommisionSchema.setTransState("00"); //正常保单
                    }

                    mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                            getTransMoney());
                    mLACommisionSchema.setLastPayToDate(tLCPolSchema.
                            getPaytoDate());
                    mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                            getGetDate());
                    mLACommisionSchema.setTransType("WT");
                    if (tLCContSchema.getSaleChnl().equals("07")) {
                        mLACommisionSchema.setF1("02"); //职团开拓
                        mLACommisionSchema.setP5(mLACommisionSchema.
                                                 getTransMoney()); //职团开拓实收保费
                    }
                    else {
                        mLACommisionSchema.setF1("01");
                    }
                    mLACommisionSchema.setFlag("1"); //犹豫期撤件
                    mLACommisionSchema.setPayYear(PayYear);
                    mLACommisionSchema.setReNewCount(tRenewCount);
                    mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
                    mLACommisionSchema.setYears(tLCPolSchema.getYears());
                    mLACommisionSchema.setPayCount(1);
                    mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
                    mLACommisionSchema.setGetPolDate(tLCContSchema.
                            getGetPolDate());
                    mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.
                            getAgentCom());
                    mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                            getAgentType());
                    mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                            getAgentCode());
                    String tPolType = tLCPolSchema.getStandbyFlag2();
                    mLACommisionSchema.setPolType(tPolType == null ||
                                                  tPolType.equals("") ?
                                                  "1" : tPolType);
                    mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());
                    mLACommisionSchema.setP11(tLCContSchema.getAppntName());
                    mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
                    mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
                    mLACommisionSchema.setP14(tLCPolSchema.getPrtNo()); //cg add 2004-01-13
                    mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
                    mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //交单日期
                    mLACommisionSchema.setCustomGetPolDate(tLCContSchema.
                            getCustomGetPolDate()); //签字日期
                    String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
                    mLACommisionSchema.setScanDate(tScanDate);
                    mLACommisionSchema.setOperator(mGlobalInput.Operator);
                    mLACommisionSchema.setMakeDate(CurrentDate);
                    mLACommisionSchema.setMakeTime(CurrentTime);
                    mLACommisionSchema.setModifyDate(CurrentDate);
                    mLACommisionSchema.setModifyTime(CurrentTime);
                    mLACommisionSchema.setBranchCode(mLAAgentSchema.
                            getBranchCode());
                    tAgentCode = mLACommisionSchema.getAgentCode().trim();
                    tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
                    if (tAgentGroup == null) {
                        return false;
                    }
                    LABranchGroupSchema tLABranchGroupSchema;
                    tLABranchGroupSchema = new LABranchGroupSchema();
                    tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);

                    mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                            getBranchSeries());
                    String tAcType = "";
                    if (tLCPolSchema.getSaleChnl().equals("03")) {
                        //如果契约渠道为中介
                        String tAgentCom = mLACommisionSchema.getAgentCom();
                        LAComDB tLAComDB = new LAComDB();
                        tLAComDB.setAgentCom(tAgentCom);
                        if (!tLAComDB.getInfo()) {
                            LAWageActivityLogSchema tLAWageActivityLogSchema = new
                                    LAWageActivityLogSchema();
                            tLAWageActivityLogSchema.setAgentCode(
                                    mLAAgentSchema.getAgentCode());
                            tLAWageActivityLogSchema.setAgentGroup(
                                    mLAAgentSchema.getAgentGroup());
                            tLAWageActivityLogSchema.setContNo(
                                    mLACommisionSchema.
                                    getContNo());
                            tLAWageActivityLogSchema.setDescribe(
                                    "渠道类型为中介，但是没有查到机构编码为" + tAgentCom +
                                    "的中介机构!");
                            tLAWageActivityLogSchema.setGrpContNo(
                                    mLACommisionSchema.getGrpContNo());
                            tLAWageActivityLogSchema.setGrpPolNo(
                                    mLACommisionSchema.
                                    getGrpPolNo());
                            tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                            tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                            tLAWageActivityLogSchema.setManageCom(
                                    mLACommisionSchema.getManageCom());
                            tLAWageActivityLogSchema.setOperator(mGlobalInput.
                                    Operator);
                            tLAWageActivityLogSchema.setOtherNo("");
                            tLAWageActivityLogSchema.setOtherNoType("");
                            tLAWageActivityLogSchema.setPolNo(
                                    mLACommisionSchema.
                                    getPolNo());
                            tLAWageActivityLogSchema.setRiskCode(
                                    mLACommisionSchema.
                                    getRiskCode());

                            String tSerialNo = PubFun1.CreateMaxNo(
                                    "WAGEACTIVITYLOG", 12);
                            tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                            tLAWageActivityLogSchema.setWageLogType("01");
                            tLAWageActivityLogSchema.setWageNo(mLAWageLogSchema.
                                    getWageNo());
                            mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                            continue;
                        } else {
                            tAcType = tLAComDB.getSchema().getACType();
                        }
                    }
                    //添加对于互动中介的处理
                    if (tLCPolSchema.getSaleChnl().equals("15")) 
                    {
                    	tAcType = checkActive(mLACommisionSchema);
                    	if("".equals(tAcType))
                    	{
                    		continue;
                    	}
                    }
                    String[] tBranchTypes = AgentPubFun.switchSalChnl(
                            tLCPolSchema.
                            getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                            tAcType);

                    tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
                    if (tBranchTypes == null) {
                        return false;
                    }
                    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                    tLABranchGroupDB.setAgentGroup(tBranchCode);
                    tLABranchGroupDB.getInfo();
                    tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
                    if (tBranchAttr == null) {
                        return false;
                    }
                    mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                            getBranchSeries());
                    mLACommisionSchema.setAgentGroup(mLAAgentSchema.
                            getAgentGroup());
                    mLACommisionSchema.setBranchAttr(tBranchAttr);
                    mLACommisionSchema.setBranchType(tBranchTypes[0]);
                    mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
                    mLACommisionSchema.setBranchType3(tBranchTypes[2]);
                    String mon = mLACommisionSchema.getGetPolDate();
                    //先根据回单日期置CalDate, WAGENO
                    String tConfDate = "";
                    tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),
                            "WT", mLAWageLogSchema.getEndDate(),tLJAGetEndorseSchema.
                            getGetConfirmDate(),tLJAGetEndorseSchema.
                            getActuGetNo());
    		        //--查询回访成功数据
                    String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
                    //String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
                    String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                    String tTransState = mLACommisionSchema.getTransState();
                    // add new 
                    String tAgentcode =mLACommisionSchema.getAgentCode() ;
                    
                    String caldate = this.calCalDateForAll(tBranchTypes[0],
                            tBranchTypes[1],
                            mLACommisionSchema.getSignDate(),
                            tLCContSchema.getGetPolDate(), "0",
                            tLJAGetEndorseSchema.
                            getMakeDate(),
                                     mLACommisionSchema.getCustomGetPolDate(),
                            mLACommisionSchema.getCValiDate(), "02",
                            tConfDate, AFlag,this.mManageCom,tVisitTime ,tRiskFlag,tTransState,tAgentcode,tLCContSchema.getGrpContNo(),mLACommisionSchema.getRiskCode());
                    if (!caldate.equals("") && !caldate.equals("0")) {
                        mLACommisionSchema.setCalDate(caldate);
                    }
                    String tSignDate = mLACommisionSchema.getSignDate();
//                if (!caldate.equals("")) {
                    int z = judgeWageIsCal(PolNo, tSignDate);
                    switch (z) {
                    case 0: //判断过程出错
                        return false;
                    case 1: { //佣金已经计算,CommDire=1 可以参加下月佣金计算就扣除了这部分佣金
                        mLACommisionSchema.setCommDire("1");
                        mLACommisionSchema.setFlag("1");
                        sql =
                                "select yearmonth from LAStatSegment where startdate<='" +
                                caldate + "' and enddate>='" + caldate +
                                "' and stattype='5' ";
                        tExeSQL = new ExeSQL();
                        tWageNo = tExeSQL.getOneValue(sql);
                        mLACommisionSchema.setTConfDate(tConfDate);
                        mLACommisionSchema.setWageNo(tWageNo); //WageNo为下一月,下月佣金可以算到
                        mLACommisionSchema.setCalDate(caldate);
                        break;
                    }
                    case 2: { //佣金尚未计算，修改原来的数据，使原先的数据不参加佣金计算，那么本条数据也不参加下月的计算
                        mLACommisionSchema.setCommDire("2");
                        mLACommisionSchema.setFlag("1");

                        String tSQL1 =
                                "select * from LACommision where PolNo = '" +
                                mLACommisionSchema.getPolNo() +
                                "' and SignDate='" + tSignDate +
                                "' order by commisionsn with ur "; //modify :2005-11-05 xiangchun 加上order by commisionsn
                        System.out.println(tSQL1);
                        LACommisionDB tLACommisionDB = new LACommisionDB();
                        LACommisionSet dLACommisionSet = new LACommisionSet();
                        dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
                        LACommisionSet eLACommisionSet = new LACommisionSet();
                        System.out.println(dLACommisionSet.size());
                        for (int j = 1; j <= dLACommisionSet.size(); j++) {
                            boolean hasFound = false;
                            LACommisionSchema tLACommisionSchema = new
                                    LACommisionSchema();
                            tLACommisionSchema = dLACommisionSet.get(j);
                            //把wageno修改为新单的wageno
                            if (j == 1) {
                                mLACommisionSchema.setWageNo(
                                        tLACommisionSchema.getWageNo()); //modify :2005-11-05 xiangchun
                                mLACommisionSchema.setCalDate(
                                        tLACommisionSchema.getCalDate());
                            }
                            for (int k = 1; k <= mLAUpdateCommisionSet.size();
                                         k++) {
                                //如果mLAUpdateCommisionSet已经有了本次退保的新单数据
                                if (mLAUpdateCommisionSet.get(k).
                                    getCommisionSN().equals(
                                            tLACommisionSchema.
                                            getCommisionSN())) {
                                    hasFound = true;
                                    mLAUpdateCommisionSet.get(k).
                                            setCommDire("2");
                                    //tLACommisionSchema.gewageno如果为空，但是可能mLAUpdateCommisionSet.get(k).getWageNo()不为空，
                                    //所以需要在此出重新修改wageno和caldate

                                    mLACommisionSchema.setWageNo(
                                            mLAUpdateCommisionSet.get(k).
                                            getWageNo());
                                    mLACommisionSchema.setCalDate(
                                            mLAUpdateCommisionSet.get(k).
                                            getCalDate());
                                    // break;                 modify :2005-10-17 xiangchun
                                }
                            }
                            System.out.println(hasFound);
                            if (hasFound) {
                                continue;
                            } else {
                                //如果如果mLAUpdateCommisionSet没有本次退保的新单数据
                                tLACommisionSchema.setCommDire("2");
                                tLACommisionSchema.setModifyDate(PubFun.
                                        getCurrentDate());
                                tLACommisionSchema.setModifyTime(PubFun.
                                        getCurrentTime());
                                eLACommisionSet.add(tLACommisionSchema);
                            }
                        }
                        for (int k = 1; k <= mLACommisionSet.size(); k++) {
                            boolean WageNoFlag = false; //判断WageNo是否付值
                            if (mLACommisionSet.get(k).getPolNo().
                                equals(
                                        mLACommisionSchema.
                                        getPolNo())) {
                                if (!WageNoFlag) {
                                    mLACommisionSchema.setWageNo(
                                            mLACommisionSet.get(k).getWageNo()); //modify :2005-11-05 xiangchun
                                    mLACommisionSchema.setCalDate(
                                            mLACommisionSet.get(k).getCalDate());
                                }
                                mLACommisionSet.get(k).setCommDire("2");
                                WageNoFlag = true;
                                // break;                 modify :2005-10-17 xiangchun
                            }
                        }

                        mLAUpdateCommisionSet.add(eLACommisionSet);
                        break; //modify :2005-10-17 xiangchun   ///////////////////////////////////////////////////是否应该为continue
                    }
                    }
//                }
                    mLACommisionSet.add(mLACommisionSchema);
                }
            }
        }
        System.out.println("*****************查询LJAGetEndorse批改退费表  结束***************");

        System.out.println("*****************查询LJAPayGrp团险提数  开始***************");
        tSSRS = queryLJAPayGrp();
        if (tSSRS == null) {
            return false;
        }
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
            LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
            tLJAPayGrpDB.setGrpPolNo(tSSRS.GetText(i, 1));
            tLJAPayGrpDB.setPayNo(tSSRS.GetText(i, 2));
            tLJAPayGrpDB.setPayType(tSSRS.GetText(i, 3));

            tLJAPayGrpDB.getInfo();
            tAgentCode = tLJAPayGrpDB.getAgentCode();
            tLJAPayGrpSchema = tLJAPayGrpDB.getSchema();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            tLAAgentDB.getInfo();
            mLAAgentSchema = tLAAgentDB.getSchema();
            //查询集体险种和保单表
            String tGrpPolNo = tLJAPayGrpSchema.getGrpPolNo();
            tGrpContNo = tLJAPayGrpSchema.getGrpContNo();
            //System.out.println("集体保单号：" + tGrpPolNo);
            tLCGrpPolSchema = new LCGrpPolSchema();
            tLCGrpContSchema = new LCGrpContSchema();
            tLCGrpPolSchema = queryLCGrpPol(tGrpPolNo);
            if (tLCGrpPolSchema == null) {
                continue;
            }
            tLCGrpContSchema = queryLCGrpCont(tGrpContNo);
            if (tLCGrpContSchema == null) {
                continue;
            }
            //一卡通出单先不计提佣金 wrx-20180727
            if(tLCGrpContSchema.getCardFlag()!=null&&"cd".equals(tLCGrpContSchema.getCardFlag())){
            	continue;
            }
            COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
            if (tLJAPayGrpSchema.getPayIntv() == 0) { //趸交payIntv = 0
                PayYear = 0;
            } else {
                //计算交费年度=保单生效日到交至日期
                //查询交至日期：根据团单号到个人交费表找到对应保单号中最大的交至日期
                String tCurPaytoDate = getGrpCurPaytoDate(tGrpPolNo);
                if (tCurPaytoDate == null) {
                    return false;
                }
                PayYear = PubFun.calInterval(tLCGrpPolSchema.getCValiDate(),
                                             fDate.
                                             getString(PubFun.calDate(fDate.
                        getDate(tCurPaytoDate), -2, "D", null)), "Y");
                if(PayYear<0)  PayYear=0;

            }
            //续保的处理
            LCRnewStateLogSchema tLCRnewStateLogSchema = new
                    LCRnewStateLogSchema();
            tLCRnewStateLogSchema = this.queryLCRnewGrpPol(tGrpPolNo);
            if (tLCRnewStateLogSchema == null) {
                tRenewCount = 0;
            } else {

//            	if (tRiskCode.equals("320106")){
//            		tRenewCount = 0;
//            	}
//            	else 
                tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }

            mLACommisionSchema = new LACommisionSchema();
            mLACommisionSchema.setCommisionSN(COMMISIONSN);
            mLACommisionSchema.setCommisionBaseNo("0000000000");
            mLACommisionSchema.setReNewCount(tRenewCount);
            //WageNo
            mLACommisionSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            mLACommisionSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            mLACommisionSchema.setContNo(tLCGrpPolSchema.getGrpContNo());
            mLACommisionSchema.setPolNo(tLCGrpPolSchema.getGrpPolNo());
            mLACommisionSchema.setMainPolNo(tLCGrpPolSchema.getGrpPolNo()); //新加的 主险单号 用于继续率的计算
            mLACommisionSchema.setManageCom(tLJAPayGrpSchema.getManageCom());
            mLACommisionSchema.setEndorsementNo(tLJAPayGrpSchema.getEndorsementNo());//新加的  批单号码 
            mLACommisionSchema.setAppntNo(tLCGrpContSchema.getAppntNo());
            mLACommisionSchema.setRiskCode(tLJAPayGrpSchema.getRiskCode());
            mLACommisionSchema.setRiskVersion(tLCGrpPolSchema.
                                              getRiskVersion());
            mLACommisionSchema.setCValiDate(tLCGrpPolSchema.getCValiDate());
            mLACommisionSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            mLACommisionSchema.setPayMode(tLCGrpPolSchema.getPayMode());
            mSSRS1 = queryInsuYear(tLCGrpPolSchema.getGrpPolNo(),tLCGrpPolSchema.getRiskCode());
            mLACommisionSchema.setF4(mSSRS1.GetText(1, 1));
            mLACommisionSchema.setF5(mSSRS1.GetText(1, 2));
            mLACommisionSchema.setReceiptNo(tLJAPayGrpSchema.getPayNo());
            mLACommisionSchema.setTPayDate(tLJAPayGrpSchema.getPayDate());
            mLACommisionSchema.setTEnterAccDate(tLJAPayGrpSchema.
                                                getEnterAccDate());
            mLACommisionSchema.setTConfDate(tLJAPayGrpSchema.getConfDate());
            mLACommisionSchema.setTMakeDate(tLJAPayGrpSchema.getMakeDate());
            //    CalcDate
            mLACommisionSchema.setCommDate(CurrentDate);
            mLACommisionSchema.setTransMoney(tLJAPayGrpSchema.
                                             getSumActuPayMoney());
            mLACommisionSchema.setTransState("00"); //正常保单
            mLACommisionSchema.setTransStandMoney(tLJAPayGrpSchema.
                                                  getSumDuePayMoney());
            mLACommisionSchema.setLastPayToDate(tLJAPayGrpSchema.
                                                getLastPayToDate());
            mLACommisionSchema.setCurPayToDate(tLJAPayGrpSchema.
                                               getCurPayToDate());
            mLACommisionSchema.setTransType("ZC");
            //TransState
            mLACommisionSchema.setCommDire("1");
            //    DirectWage
            //    AppendWage
            if(tLCGrpPolSchema.getSaleChnl().equals("07"))
            {
                mLACommisionSchema.setF1("02");//职团开拓
                mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
            }
            else
            {
                mLACommisionSchema.setF1("01");
            }


            //////////////////////////////////
            mLACommisionSchema.setFlag("2"); //xiangchun  2005-12-01  2为正常件
            mLACommisionSchema.setPayYear(PayYear);
            mLACommisionSchema.setPayYears(getPayYears(tLCGrpPolSchema.getGrpPolNo()));
            mLACommisionSchema.setPayCount(tLJAPayGrpSchema.getPayCount());
            // mLACommisionSchema.setSignDate("2005-07-31");
            mLACommisionSchema.setSignDate(tLCGrpContSchema.getSignDate());
            mLACommisionSchema.setGetPolDate(tLCGrpContSchema.getGetPolDate());
            mLACommisionSchema.setAgentCom(tLJAPayGrpSchema.getAgentCom());
            String tActType = "";
            //添加对于互动中介的处理
            if (tLCGrpPolSchema.getSaleChnl().equals("15")) 
            {
            	tActType = checkActive(mLACommisionSchema);
            	if("".equals(tActType))
            	{
            		continue;
            	}
            }
            String[] tBranchTypes = AgentPubFun.switchSalChnl(
                    tLCGrpPolSchema.
                    getSaleChnl(), tLCGrpPolSchema.getSaleChnlDetail(),
                    tActType);
            mLACommisionSchema.setBranchType(tBranchTypes[0]);
            mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
            mLACommisionSchema.setBranchType3(tBranchTypes[2]);
            
            mLACommisionSchema.setAgentType(tLJAPayGrpSchema.getAgentType());
            mLACommisionSchema.setAgentCode(tLJAPayGrpSchema.getAgentCode());
            ////添加卡折结算单的套餐名称
            if(mLAAgentSchema.getBranchType() != null && !mLAAgentSchema.getBranchType().trim().equals("")
                && mLAAgentSchema.getBranchType2() != null && !mLAAgentSchema.getBranchType2().trim().equals("")){
             if(mLAAgentSchema.getBranchType().equals("1")
            		 //增加互动渠道,健管渠道
                     ||mLAAgentSchema.getBranchType().equals("5")||mLAAgentSchema.getBranchType().equals("7")||mLAAgentSchema.getBranchType().equals("6")||mLAAgentSchema.getBranchType().equals("8")){              
               if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
               {
                  String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                          +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                  tExeSQL = new ExeSQL();
                  String swarpcode = tExeSQL.getOneValue(warpsql);
                   System.out.println(warpsql+" a  "+swarpcode);
                  mLACommisionSchema.setF3(swarpcode);
               }
               else
               {
                  mLACommisionSchema.setF3("");
                   System.out.println("B");
               }
             }
//             if(mLAAgentSchema.getBranchType().equals("2") && mLAAgentSchema.getBranchType2().equals("01"))
//             {
//             	  System.out.println("come here ");
//            	  String wrapsql = "select distinct riskwrapcode from lccontplanrisk "
//             	               + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"' ";
//             	  ExeSQL tExe = new ExeSQL();
//             	  String swrapcode = tExe.getOneValue(wrapsql);
//             	  if(swrapcode==null){
//             	  mLACommisionSchema.setF3("");
//             	  }
//             	  else 
//             	  {
//             	  mLACommisionSchema.setF3(swrapcode);
//                }
//             }
             //团险渠道（包含团险直销与团险中介）
             if(mLAAgentSchema.getBranchType().equals("2")){
            	 if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                 {
                    String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                            +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                    tExeSQL = new ExeSQL();
                    String swarpcode = tExeSQL.getOneValue(warpsql);
                     System.out.println(warpsql+" a  "+swarpcode);
                    mLACommisionSchema.setF3(swarpcode);
                 }
                 else
                 {
                    mLACommisionSchema.setF3("");
                     System.out.println("B");
                 }
            }
            }
            mLACommisionSchema.setPolType("1"); //团单的保单类型置为"1"表示正常，
            mLACommisionSchema.setP1(tLCGrpPolSchema.getManageFeeRate());
            mLACommisionSchema.setP2(tLCGrpPolSchema.getBonusRate());

            String tgetGrpPolNo = tSSRS.GetText(i, 1);
            String tgetPayNo = tSSRS.GetText(i, 2);
            String tgetPayType = tSSRS.GetText(i, 3);

            if(tRenewCount>=1 || tLJAPayGrpSchema.getPayCount()>=2)
            {
                mLACommisionSchema.setP7(0);
            }
            else
            {
                String tMoney = getCManageFee(tgetGrpPolNo, tgetPayNo, tgetPayType,tLJAPayGrpSchema);
                double tValue = Double.parseDouble(tMoney);
                mLACommisionSchema.setP7(tValue);
            }

            mLACommisionSchema.setP11(tLCGrpPolSchema.getGrpName());

            mLACommisionSchema.setP14(tLCGrpPolSchema.getPrtNo()); //cg add 2004-01-13
            mLACommisionSchema.setP15(tLCGrpPolSchema.getGrpProposalNo());
            mLACommisionSchema.setMakePolDate(tLCGrpPolSchema.getMakeDate()); //交单日期
            mLACommisionSchema.setCustomGetPolDate(tLCGrpContSchema.
                    getCustomGetPolDate()); //签字日期
            //    riskmark
            String tScanDate = this.getScanDate(tLCGrpPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
            mLACommisionSchema.setScanDate(tScanDate);
            mLACommisionSchema.setOperator(mGlobalInput.Operator);
            mLACommisionSchema.setMakeDate(CurrentDate);
            mLACommisionSchema.setMakeTime(CurrentTime);
            mLACommisionSchema.setModifyDate(CurrentDate);
            mLACommisionSchema.setModifyTime(CurrentTime);
            mLACommisionSchema.setMarketType(tLCGrpContSchema.getMarketType());
            chargeDate = queryChargeDate(mLACommisionSchema.getGrpPolNo(),
                                         tLJAPayGrpSchema.getAgentCom(),
                                         mLACommisionSchema.
                                         getReceiptNo());
            getPolDate = mLACommisionSchema.getGetPolDate(); //xiangchun 2005-12-01团险根据回执日期判断年月
            custPolDate = mLACommisionSchema.getCustomGetPolDate();
            String tConfDate = "";
            String mPayType=tLJAPayGrpSchema.getPayType();
            if(mPayType.equals("GB"))
            {
                //团险保单反冲中，confdate=makedate
             AFlag = "0";
             tConfDate =tLJAPayGrpSchema.getConfDate();
             mLACommisionSchema.setriskmark("1");//代表共保保单

            }
            else
            {
             tConfDate = this.queryConfDate(tLCGrpPolSchema.getGrpContNo(),
                                               "ZC",
                                               mLAWageLogSchema.getEndDate(),
                                               tLJAPayGrpSchema.getConfDate(),
                                               tLJAPayGrpSchema.getPayNo());
            }
	        //--查询回访成功数据
            String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
            String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
            String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
            String tTransState = mLACommisionSchema.getTransState();
            // add new 
            String tAgentcode =mLACommisionSchema.getAgentCode() ;
            
            String caldate = this.calCalDateForAll(tBranchTypes[0],
                    tBranchTypes[1],
                    mLACommisionSchema.getSignDate(),
                    mLACommisionSchema.getGetPolDate(), //xiangchun 2005-12-01团险根据回执日期判断年月
                    String.valueOf(mLACommisionSchema.getPayCount()),
                    mLACommisionSchema.getTMakeDate(),
                    mLACommisionSchema.getCustomGetPolDate(),
                    mLACommisionSchema.getCValiDate(),"02",
                    tConfDate,AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);
            if (caldate != null && !caldate.trim().equals("") &&
                !caldate.trim().equals("0")) {
                mLACommisionSchema.setCalDate(caldate);
                sql = "select yearmonth from LAStatSegment where startdate<='" +
                      caldate + "' and enddate>='" + caldate +
                      "' and stattype='91' "; //团险薪资月为91,2005-11-23修改
                tExeSQL = new ExeSQL();
                tWageNo = tExeSQL.getOneValue(sql);

                mLACommisionSchema.setWageNo(tWageNo);
                mLACommisionSchema.setCalcDate(caldate);
                mLACommisionSchema.setTConfDate(tConfDate);
            }
            tBranchCode = mLAAgentSchema.getBranchCode();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tBranchCode);
            tLABranchGroupDB.getInfo();
            tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
            if (tBranchAttr == null) {
                return false;
            }
            mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                                               getBranchSeries());
            mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
            mLACommisionSchema.setBranchAttr(tBranchAttr);
            mLACommisionSchema.setBranchCode(tBranchCode);
            this.mLACommisionSet.add(mLACommisionSchema);
        }
        System.out.println("*****************查询LJAPayGrp团险提数  结束***************");

        System.out.println("*****************查询团险批改退费  开始***************" );
        //团险犹豫期撤单
        tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        tSSRS = new SSRS();
        tSSRS = queryGrpLJAGetEndorse();
        if (tSSRS == null) {
            return false;
        }
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            String Sql =
                    "select sum(getmoney) from LJAGetEndorse where  FeeOperationType = 'WT' " +
                    " and FeeFinaType = 'TF' and GrpPolNo='" +
                    tSSRS.GetText(i, 1) + "' and " +
                    "PayPlanCode='" + tSSRS.GetText(i, 2) + "' with ur ";
            tExeSQL = new ExeSQL();
            double tGetMoney = Double.parseDouble(tExeSQL.getOneValue(Sql));
//            tLJAGetEndorseDB.setGrpPolNo(tSSRS.GetText(i, 1));
//            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 2));
//            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
//            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
//            tLJAGetEndorseDB.setMakeDate(mLAWageLogSchema.getEndDate());
              LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
//            tLJAGetEndorseSet = tLJAGetEndorseDB.query();
            
              String getEndorseSQL = "select * from ljagetendorse where grppolno ='"+tSSRS.GetText(i, 1)+"' and payplancode = '"+tSSRS.GetText(i, 2)+"'" +
      		" and FeeOperationType ='"+tSSRS.GetText(i, 3)+"' and FeeFinaType='"+tSSRS.GetText(i, 4)+"' and makedate = '"+mLAWageLogSchema.getEndDate()+"' fetch first 1 rows only ";
      tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(getEndorseSQL);
      
            tLJAGetEndorseSchema =tLJAGetEndorseSet.get(1);

            //判断此犹豫期撤单件 是否已经提过数 ,如果提过,则不再提取
            //提数回退时 不回退上个月的数据,所以会出现此种情况
            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setGrpPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            wLACommisionDB.setTransType("WT");
            wLACommisionDB.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
            wLACommisionDB.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }

            tAgentCode = tLJAGetEndorseSchema.getAgentCode();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            tLAAgentDB.getInfo();
            mLAAgentSchema = tLAAgentDB.getSchema();
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            if (tGrpPolNo != null && !tGrpPolNo.equals("00000000000000000000") &&
                !tGrpPolNo.equals("")) {
                GrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
                tGrpContNo = tLJAGetEndorseSchema.getGrpContNo();
                tLCGrpPolSchema = new LCGrpPolSchema();
                tLCGrpContSchema = new LCGrpContSchema();
                tLCGrpPolSchema = queryLCGrpPol(GrpPolNo);
                if (tLCGrpPolSchema == null) {
                    continue;
                }
                tLCGrpContSchema = queryLCGrpCont(tGrpContNo);
                if (tLCGrpContSchema == null) {
                    continue;
                }
                //续保的处理
                LCRnewStateLogSchema tLCRnewStateLogSchema = new
                        LCRnewStateLogSchema();
                tLCRnewStateLogSchema = this.queryLCRnewGrpPol(tGrpPolNo);
                if (tLCRnewStateLogSchema == null) {
                    tRenewCount = 0;
                } else {

//                	if (tRiskCode.equals("320106")){
//                		tRenewCount = 0;
//                	}
//                	else 
                    tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }
                COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
                //计算交费年度=保单生效日到交至日期
                PayYear = 0;
                //填充直接佣金明细表（佣金扎账表）
                mLACommisionSchema = new LACommisionSchema();
                mLACommisionSchema.setCommisionSN(COMMISIONSN);
                mLACommisionSchema.setCommisionBaseNo("0000000000");
                mLACommisionSchema.setReNewCount(tRenewCount);
                //WageNo 783
                mLACommisionSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
                mLACommisionSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
                mLACommisionSchema.setContNo(tLCGrpPolSchema.getGrpContNo());
                mLACommisionSchema.setPolNo(tLCGrpPolSchema.getGrpPolNo());
                mLACommisionSchema.setMainPolNo(tLCGrpPolSchema.getGrpPolNo()); //新加的 主险单号 用于继续率的计算
                mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.
                                                getManageCom());
                mLACommisionSchema.setAppntNo(tLCGrpContSchema.getAppntNo());
                mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
                mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); //新加的   批单号码
                mLACommisionSchema.setRiskVersion(tLCGrpPolSchema.
                                                  getRiskVersion());
                mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
                mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.
                                                  getPayPlanCode());
                mLACommisionSchema.setCValiDate(tLCGrpPolSchema.getCValiDate());
                mLACommisionSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
                mLACommisionSchema.setPayMode(tLCGrpPolSchema.getPayMode());
                mSSRS1 = queryInsuYear(tLCGrpPolSchema.getGrpPolNo(),tLCGrpPolSchema.getRiskCode());
                mLACommisionSchema.setF4(mSSRS1.GetText(1, 1));
                mLACommisionSchema.setF5(mSSRS1.GetText(1, 2));
                mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.
                                                getActuGetNo());
                mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
                mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.
                        getEnterAccDate());
                mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.
                                                getGetConfirmDate());
                mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.
                                                getMakeDate());
                // CalcDate
                mLACommisionSchema.setCommDate(CurrentDate);
                mLACommisionSchema.setTransMoney(
                        0 - java.lang.Math.abs(tGetMoney));
                mLACommisionSchema.setTransState("00"); //正常保单
                mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                        getTransMoney());
                mLACommisionSchema.setLastPayToDate(tLCGrpPolSchema.
                        getPaytoDate());
                mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                        getGetDate());
                mLACommisionSchema.setTransType("WT");

                if(tLCGrpPolSchema.getSaleChnl().equals("07"))
                {
                    mLACommisionSchema.setF1("02");//职团开拓
                    mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
                }
                else
                {
                    mLACommisionSchema.setF1("01");
                }


                mLACommisionSchema.setFlag("1"); //犹豫期撤件
                mLACommisionSchema.setPayYear(PayYear);
                mLACommisionSchema.setPayYears(getPayYears(tLCGrpPolSchema.getGrpPolNo()));
                //mLACommisionSchema.setYears(tLCGrpPolSchema.getYears());
                mLACommisionSchema.setPayCount(1);
                mLACommisionSchema.setSignDate(tLCGrpContSchema.getSignDate());
                mLACommisionSchema.setGetPolDate(tLCGrpContSchema.getGetPolDate());
                //BranchType 788
                mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
                mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                                                getAgentType());
                mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                                                getAgentCode());

                String tPolType = tLCGrpPolSchema.getStandbyFlag2();
                mLACommisionSchema.setPolType(tPolType == null ||
                                              tPolType.equals("") ?
                                              "1" : tPolType);
                /****************************************
                 * **************************************
                 * **************************************
                 */

                mLACommisionSchema.setP1(tLCGrpPolSchema.getManageFeeRate());

                // P7  帐户管理费  xiangchun  20060226
                if(tRenewCount>=1 || PayYear>=1)
                {
                    mLACommisionSchema.setP7(0);
                }
                else
                {
                    String tSql =
                            "select value(sum(fee),0) from  lcinsureaccfee  where  grppolno='" +
                            mLACommisionSchema.getGrpPolNo() + "' with ur ";
                    ExeSQL aExeSQL = new ExeSQL();
                    String tMoney = aExeSQL.getOneValue(tSql);
                    double tValue = Double.parseDouble(tMoney);
                    mLACommisionSchema.setP7(0 - tValue);
                }

                mLACommisionSchema.setP11(tLCGrpPolSchema.getGrpName());
                //mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
                //mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
                mLACommisionSchema.setP14(tLCGrpPolSchema.getPrtNo()); //cg add 2004-01-13
                mLACommisionSchema.setP15(tLCGrpPolSchema.getGrpProposalNo());
                mLACommisionSchema.setMakePolDate(tLCGrpPolSchema.getMakeDate()); //交单日期
                mLACommisionSchema.setCustomGetPolDate(tLCGrpContSchema.
                        getCustomGetPolDate()); //签字日期
                //  riskmark
                String tScanDate = this.getScanDate(tLCGrpPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
                mLACommisionSchema.setScanDate(tScanDate);
                mLACommisionSchema.setOperator(mGlobalInput.Operator);
                mLACommisionSchema.setMakeDate(CurrentDate);
                mLACommisionSchema.setMakeTime(CurrentTime);
                mLACommisionSchema.setModifyDate(CurrentDate);
                mLACommisionSchema.setModifyTime(CurrentTime);
                mLACommisionSchema.setMarketType(tLCGrpContSchema.getMarketType());
                mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode());
                tAgentCode = mLACommisionSchema.getAgentCode().trim();
                tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
                if (tAgentGroup == null) {
                    return false;
                }
                LABranchGroupSchema tLABranchGroupSchema;
                tLABranchGroupSchema = new LABranchGroupSchema();
                tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);
                mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                        getBranchSeries());

                String tActType = "";
              //添加对于互动中介的处理
                if (tLCGrpPolSchema.getSaleChnl().equals("15")) 
                {
                	tActType = checkActive(mLACommisionSchema);
                	if("".equals(tActType))
                	{
                		continue;
                	}
                }
                String[] tBranchTypes = AgentPubFun.switchSalChnl(
                        tLCGrpPolSchema.
                        getSaleChnl(), tLCGrpPolSchema.getSaleChnlDetail(),
                        tActType);

                tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
                if (tBranchTypes == null) {
                    return false;
                }
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tBranchCode);
                tLABranchGroupDB.getInfo();
                tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
                if (tBranchAttr == null) {
                    return false;
                }
                mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                        getBranchSeries());
                mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
                mLACommisionSchema.setBranchAttr(tBranchAttr);
                mLACommisionSchema.setBranchType(tBranchTypes[0]);
                mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
                mLACommisionSchema.setBranchType3(tBranchTypes[2]);

                 //添加卡折结算单的套餐名称 现在只是计算个险人员销售 个销团渠道 结算单 团单
                if(mLAAgentSchema.getBranchType() != null && !mLAAgentSchema.getBranchType().trim().equals("")
                   && mLAAgentSchema.getBranchType2() != null && !mLAAgentSchema.getBranchType2().trim().equals("")){
                   if(mLAAgentSchema.getBranchType().equals("1")
                		   //添加互动渠道,健管渠道
                		   ||mLAAgentSchema.getBranchType().equals("5")||mLAAgentSchema.getBranchType().equals("7")||mLAAgentSchema.getBranchType().equals("6")||mLAAgentSchema.getBranchType().equals("8")){              
                     if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                     {
                        String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                                +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                        tExeSQL = new ExeSQL();
                        String swarpcode = tExeSQL.getOneValue(warpsql);
                         System.out.println(warpsql+" a  "+swarpcode);
                        mLACommisionSchema.setF3(swarpcode);
                     }
                     else
                     {
                        mLACommisionSchema.setF3("");
                         System.out.println("B");
                     }
                   }
//                   if(mLAAgentSchema.getBranchType().equals("2") && mLAAgentSchema.getBranchType2().equals("01"))
//                   {
//                   	 String wrapsql = "select distinct riskwrapcode from lccontplanrisk "
//                   	              + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"' ";
//                   	 ExeSQL tExe = new ExeSQL();
//                   	 String swrapcode = tExe.getOneValue(wrapsql);
//                   	 if(swrapcode==null){
//                   	 mLACommisionSchema.setF3("");
//                   	 }
//                   	 else 
//                   	 {
//                   	 mLACommisionSchema.setF3(swrapcode);
//                      }
//                   }
//                 团险渠道（包含团险直销与团险中介）
                   if(mLAAgentSchema.getBranchType().equals("2")){
                              	 if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                                   {
                                      String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                                              +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                                      tExeSQL = new ExeSQL();
                                      String swarpcode = tExeSQL.getOneValue(warpsql);
                                       System.out.println(warpsql+" a  "+swarpcode);
                                      mLACommisionSchema.setF3(swarpcode);
                                   }
                                   else
                                   {
                                      mLACommisionSchema.setF3("");
                                       System.out.println("B");
                                   }
                              }
                }


                String mon = mLACommisionSchema.getGetPolDate();
                //先根据回单日期置CalDate, WAGENO
                getPolDate = mLACommisionSchema.getGetPolDate(); //xiangchun 2005-12-01团险根据回执日期判断年月
                custPolDate = mLACommisionSchema.getCustomGetPolDate();
                String tConfDate = "";
                tConfDate = this.queryConfDate(tLCGrpPolSchema.getGrpContNo(),"WT",mLAWageLogSchema.getEndDate(),tLJAGetEndorseSchema.
                        getGetConfirmDate(),tLJAGetEndorseSchema.
                        getActuGetNo());
		        //--查询回访成功数据
                String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
                String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
                String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                String tTransState = mLACommisionSchema.getTransState();
                // add new 
                String tAgentcode =mLACommisionSchema.getAgentCode() ;
                
                String caldate = this.calCalDateForAll(tBranchTypes[0],
                        tBranchTypes[1],
                        mLACommisionSchema.getSignDate(),
                        mLACommisionSchema.getGetPolDate(), "0", //xiangchun 2005-12-01团险根据回执日期判断年月
                        tLJAGetEndorseSchema.
                        getMakeDate(), mLACommisionSchema.getCustomGetPolDate(),
                        mLACommisionSchema.getCValiDate(),"02",
                        tConfDate,AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);
                
                mLACommisionSchema.setCalDate(caldate);
                String tSignDate = mLACommisionSchema.getSignDate();
                if (caldate != null && !caldate.equals("")) {
                    int z = judgeWageIsCal(GrpPolNo, tSignDate);
                    switch (z) {
                    case 0: //判断过程出错
                        return false;
                    case 1: { //佣金已经计算,CommDire=1 可以参加下月佣金计算就扣除了这部分佣金
                        mLACommisionSchema.setCommDire("1");
                        mLACommisionSchema.setFlag("1");
                        sql =
                                "select yearmonth from LAStatSegment where startdate<='"
                                + caldate + "' and enddate>='" + caldate +
                                "' and stattype='91' ";
                        tExeSQL = new ExeSQL();
                        tWageNo = tExeSQL.getOneValue(sql);
                        mLACommisionSchema.setWageNo(tWageNo); //WageNo为下一月,下月佣金可以算到
                        mLACommisionSchema.setCalcDate(caldate);
                        mLACommisionSchema.setTConfDate(tConfDate);
                        break;
                    }
                    case 2: { //佣金尚未计算，修改原来的数据，使原先的数据不参加佣金计算，那么本条数据也不参加下月的计算
                        mLACommisionSchema.setCommDire("2");
                        mLACommisionSchema.setFlag("1");

                        String tSQL1 =
                                "select * from LACommision where GrpPolNo = '" +
                                mLACommisionSchema.getGrpPolNo() +
                                "' and  SignDate='" + tSignDate +
                                "' order by commisionsn with ur "; //modify :2005-11-05 xiangchun 加上order by commisionsn
                        System.out.println(tSQL1);
                        LACommisionDB tLACommisionDB = new LACommisionDB();
                        LACommisionSet dLACommisionSet = new LACommisionSet();
                        dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
                        LACommisionSet eLACommisionSet = new LACommisionSet();
                        System.out.println(dLACommisionSet.size());
                        for (int j = 1; j <= dLACommisionSet.size(); j++) {
                            boolean hasFound = false;
                            LACommisionSchema tLACommisionSchema = new
                                    LACommisionSchema();
                            tLACommisionSchema = dLACommisionSet.get(j);
                            if (j == 1) {
                                mLACommisionSchema.setWageNo(
                                        tLACommisionSchema.getWageNo()); //modify :2005-11-05 xiangchun
                                mLACommisionSchema.setCalcDate(caldate);
                                mLACommisionSchema.setTConfDate(tConfDate);
                            }
                            for (int k = 1; k <= mLAUpdateCommisionSet.size();
                                         k++) {
                                if (mLAUpdateCommisionSet.get(k).
                                    getCommisionSN().equals(
                                            tLACommisionSchema.
                                            getCommisionSN())) {
                                    hasFound = true;
                                    mLAUpdateCommisionSet.get(k).
                                            setCommDire("2");
                                    // break;                 modify :2005-10-17 xiangchun
                                }
                            }
                            System.out.println(hasFound);
                            if (hasFound) {
                                continue;
                            } else {
                                tLACommisionSchema.setCommDire("2");
                                tLACommisionSchema.setModifyDate(PubFun.
                                        getCurrentDate());
                                tLACommisionSchema.setModifyTime(PubFun.
                                        getCurrentTime());
                                eLACommisionSet.add(tLACommisionSchema);
                            }
                        }
                        for (int k = 1; k <= mLACommisionSet.size(); k++) {
                            boolean WageNoFlag = false; //判断WageNo是否付值
                            if (mLACommisionSet.get(k).getPolNo().
                                equals(
                                        mLACommisionSchema.
                                        getPolNo())) {
                                if (!WageNoFlag) {
                                    mLACommisionSchema.setWageNo(
                                            mLACommisionSet.get(k).getWageNo()); //modify :2005-11-05 xiangchun
                                }
                                mLACommisionSet.get(k).setCommDire("2");
                                WageNoFlag = true;
                                // break;                 modify :2005-10-17 xiangchun
                            }
                        }

                        mLAUpdateCommisionSet.add(eLACommisionSet);
                        break; //modify :2005-10-17 xiangchun   ///////////////////////////////////////////////////是否应该为continue
                    }
                    }
                } else { //如果没有回执日期
                    mLACommisionSchema.setCommDire("2");
                    mLACommisionSchema.setFlag("1");

                    String tSQL1 =
                            "select * from LACommision where GrpPolNo = '" +
                            mLACommisionSchema.getGrpPolNo() +
                            "' and  SignDate='" + tSignDate +
                            "' order by commisionsn with ur"; //modify :2005-12-01 xiangchun 加上order by commisionsn
                    System.out.println(tSQL1);
                    LACommisionDB tLACommisionDB = new LACommisionDB();
                    LACommisionSet dLACommisionSet = new LACommisionSet();
                    dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
                    LACommisionSet eLACommisionSet = new LACommisionSet();
                    System.out.println(dLACommisionSet.size());
                    for (int j = 1; j <= dLACommisionSet.size(); j++) {
                        boolean hasFound = false;
                        LACommisionSchema tLACommisionSchema = new
                                LACommisionSchema();
                        tLACommisionSchema = dLACommisionSet.get(j);
                        if (j == 1) {
                            mLACommisionSchema.setWageNo(
                                    tLACommisionSchema.getWageNo()); //modify :2005-12-01 xiangchun
                            mLACommisionSchema.setCalcDate(caldate);
                            mLACommisionSchema.setTConfDate(tLACommisionSchema.getTConfDate());
                        }
                        for (int k = 1; k <= mLAUpdateCommisionSet.size();
                                     k++) {
                            if (mLAUpdateCommisionSet.get(k).
                                getCommisionSN().equals(
                                        tLACommisionSchema.
                                        getCommisionSN())) {
                                hasFound = true;
                                mLAUpdateCommisionSet.get(k).
                                        setCommDire("2");
                                // break;                 modify :2005-12-01 xiangchun
                            }
                        }
                        System.out.println(hasFound);
                        if (hasFound) {
                            continue;
                        } else {
                            tLACommisionSchema.setCommDire("2");
                            tLACommisionSchema.setModifyDate(PubFun.
                                    getCurrentDate());
                            tLACommisionSchema.setModifyTime(PubFun.
                                    getCurrentTime());
                            eLACommisionSet.add(tLACommisionSchema);
                        }
                    }
                    for (int k = 1; k <= mLACommisionSet.size(); k++) {
                        boolean WageNoFlag = false; //判断WageNo是否付值
                        if (mLACommisionSet.get(k).getPolNo().
                            equals(
                                    mLACommisionSchema.
                                    getPolNo())) {

                            if (!WageNoFlag) { //wageno付值后就不再付值
                                mLACommisionSchema.setWageNo(
                                        mLACommisionSet.get(k).getWageNo()); //modify :2005-12-01 xiangchun
                            }
                            mLACommisionSet.get(k).setCommDire("2");
                            WageNoFlag = true; //已付值
                            // break;             //    modify :2005-12-01 xiangchun
                        }
                    }

                    mLAUpdateCommisionSet.add(eLACommisionSet);
                    //   break;
                }
                mLACommisionSet.add(mLACommisionSchema);
            }
        }
        System.out.println("*****************查询团险批改退费  结束***************" );

        System.out.println("*****************查询犹豫期撤保保全回退  开始***************");

        tSSRS = queryLJAGetRBPerson();
        if (tSSRS==null) {
            return false;
        }
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

//            LACommisionDB wLACommisionDB = new LACommisionDB();
//            wLACommisionDB.setPolNo(tSSRS.GetText(i, 5));
//            wLACommisionDB.setTransType("ZC");
//            wLACommisionDB.setTransState("03");//追加保费
//            wLACommisionDB.setDutyCode(tSSRS.GetText(i, 8));
//            wLACommisionDB.setPayPlanCode(tSSRS.GetText(i, 9));
//            LACommisionSet tLACommisionSet = new LACommisionSet();
//            tLACommisionSet = wLACommisionDB.query();
//            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
//                continue;
//            }
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setEndorsementNo(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setPolNo(tSSRS.GetText(i, 5));
            tLJAGetEndorseDB.setOtherNo(tSSRS.GetText(i, 6));
            tLJAGetEndorseDB.setOtherNoType(tSSRS.GetText(i, 7));
            tLJAGetEndorseDB.setDutyCode(tSSRS.GetText(i, 8));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 9));
            tLJAGetEndorseDB.getInfo();
            tLJAGetEndorseSchema = tLJAGetEndorseDB.getSchema();

            tAgentCode = tLJAGetEndorseSchema.getAgentCode();
            String tEndorsementNo=tLJAGetEndorseSchema.getEndorsementNo();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            tLAAgentDB.getInfo();
            mLAAgentSchema = tLAAgentDB.getSchema();
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            String tType=getTransType(tEndorsementNo);


            //只处理个单
            if (tGrpPolNo.equals("00000000000000000000")) {
            	if(!tType.equals("WT")) continue;
            	else {
                PolNo = tLJAGetEndorseSchema.getPolNo();
                tContNo = tLJAGetEndorseSchema.getContNo();
                tLCPolSchema = new LCPolSchema();
                tLCContSchema = new LCContSchema();
                try{
                tLCPolSchema = queryLCPol(PolNo);
                }
                catch(Exception ex)
                {
                        ex.getStackTrace();
                }
                if (tLCPolSchema == null) {
                    continue;
                }
                tLCContSchema = queryLCCont(tContNo);
                if (tLCContSchema == null) {
                    continue;
                }
                //续保的处理
                LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
                tLCRnewStateLogSchema = this.queryLCRnewPol(PolNo);
                if (tLCRnewStateLogSchema == null) {
                    tRenewCount = 0;
                }
                else {

//                	if (tRiskCode.equals("320106")){
//                		tRenewCount = 0;
//                	}
//                	else 
                    tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }
               String tRiskCode=tLCPolSchema.getRiskCode();
              //暂时330801 331801 不处理
               if (!tRiskCode.equals("330801")&&!tRiskCode.equals("331801")&&!tRiskCode.equals("332701")||tRiskCode.equals("333901")||tRiskCode.equals("334701")||tRiskCode.equals("334801")||tRiskCode.equals("370201"))
               {

                COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
                //计算交费年度=保单生效日到交至日期
                PayYear = 0;
                //填充直接佣金明细表（佣金扎账表）
                mLACommisionSchema = new LACommisionSchema();
                mLACommisionSchema.setCommisionSN(COMMISIONSN);
                mLACommisionSchema.setCommisionBaseNo("0000000000");

                mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
                mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
                mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
                mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
                mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
                mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); //新加的   批单号码
                mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.
                                                getManageCom());
                mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
                mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
                mLACommisionSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
                mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
                mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.
                                                  getPayPlanCode());
                mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
                mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
                mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
                mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
                mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
                mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.
                                                getActuGetNo());
                mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
                mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.
                        getEnterAccDate());
                mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.
                                                getGetConfirmDate());
                mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.
                                                getMakeDate());

                mLACommisionSchema.setCommDate(CurrentDate);

                mLACommisionSchema.setTransMoney(tLJAGetEndorseSchema.getGetMoney());
                mLACommisionSchema.setTransState("00");
                mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                        getTransMoney());
                mLACommisionSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
                mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                        getGetDate());
                mLACommisionSchema.setTransType("ZC");
                if(tLCContSchema.getSaleChnl().equals("07"))
                {
                    mLACommisionSchema.setF1("02");//职团开拓
                    mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
                }
                else
                {
                    mLACommisionSchema.setF1("01");
                }
                mLACommisionSchema.setFlag("1"); //犹豫期撤件
                mLACommisionSchema.setPayYear(PayYear);
                mLACommisionSchema.setReNewCount(tRenewCount);
                mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
                mLACommisionSchema.setYears(tLCPolSchema.getYears());
                mLACommisionSchema.setPayCount(1);
                mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
                mLACommisionSchema.setGetPolDate(tLCContSchema.getGetPolDate());
                mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
                mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                                                getAgentType());
                mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                                                getAgentCode());
                String tPolType = tLCPolSchema.getStandbyFlag2();
                mLACommisionSchema.setPolType(tPolType == null ||
                                              tPolType.equals("") ?
                                              "1" : tPolType);
                mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());
                mLACommisionSchema.setP11(tLCContSchema.getAppntName());
                mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
                mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
                mLACommisionSchema.setP14(tLCPolSchema.getPrtNo()); //cg add 2004-01-13
                mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
                mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //交单日期
                mLACommisionSchema.setCustomGetPolDate(tLCContSchema.
                        getCustomGetPolDate()); //签字日期
                String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
                mLACommisionSchema.setScanDate(tScanDate);
                mLACommisionSchema.setOperator(mGlobalInput.Operator);
                mLACommisionSchema.setMakeDate(CurrentDate);
                mLACommisionSchema.setMakeTime(CurrentTime);
                mLACommisionSchema.setModifyDate(CurrentDate);
                mLACommisionSchema.setModifyTime(CurrentTime);
                mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode());
                tAgentCode = mLACommisionSchema.getAgentCode().trim();
                tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
                if (tAgentGroup == null) {
                    return false;
                }
                LABranchGroupSchema tLABranchGroupSchema;
                tLABranchGroupSchema = new LABranchGroupSchema();
                tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);

                mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                        getBranchSeries());
                String tAcType = "";
                if (tLCPolSchema.getSaleChnl().equals("03")) {
                    //如果契约渠道为中介
                    String tAgentCom = mLACommisionSchema.getAgentCom();
                    LAComDB tLAComDB = new LAComDB();
                    tLAComDB.setAgentCom(tAgentCom);
                    if (!tLAComDB.getInfo()) {
                        LAWageActivityLogSchema tLAWageActivityLogSchema = new
                                LAWageActivityLogSchema();
                        tLAWageActivityLogSchema.setAgentCode(
                                mLAAgentSchema.getAgentCode());
                        tLAWageActivityLogSchema.setAgentGroup(
                                mLAAgentSchema.getAgentGroup());
                        tLAWageActivityLogSchema.setContNo(mLACommisionSchema.
                                getContNo());
                        tLAWageActivityLogSchema.setDescribe(
                                "渠道类型为中介，但是没有查到机构编码为" + tAgentCom + "的中介机构!");
                        tLAWageActivityLogSchema.setGrpContNo(
                                mLACommisionSchema.getGrpContNo());
                        tLAWageActivityLogSchema.setGrpPolNo(mLACommisionSchema.
                                getGrpPolNo());
                        tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                        tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                        tLAWageActivityLogSchema.setManageCom(
                                mLACommisionSchema.getManageCom());
                        tLAWageActivityLogSchema.setOperator(mGlobalInput.
                                Operator);
                        tLAWageActivityLogSchema.setOtherNo("");
                        tLAWageActivityLogSchema.setOtherNoType("");
                        tLAWageActivityLogSchema.setPolNo(mLACommisionSchema.
                                getPolNo());
                        tLAWageActivityLogSchema.setRiskCode(mLACommisionSchema.
                                getRiskCode());

                        String tSerialNo = PubFun1.CreateMaxNo(
                                "WAGEACTIVITYLOG", 12);
                        tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                        tLAWageActivityLogSchema.setWageLogType("01");
                        tLAWageActivityLogSchema.setWageNo(mLAWageLogSchema.
                                getWageNo());
                        mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                        continue;
                    }
                    else {
                        tAcType = tLAComDB.getSchema().getACType();
                    }
                }
                //添加对于互动中介的处理
                if (tLCPolSchema.getSaleChnl().equals("15")) 
                {
                	tAcType = checkActive(mLACommisionSchema);
                	if("".equals(tAcType))
                	{
                		continue;
                	}
                }
                String[] tBranchTypes = AgentPubFun.switchSalChnl(
                        tLCPolSchema.
                        getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                        tAcType);

                tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
                if (tBranchTypes == null) {
                    return false;
                }
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tBranchCode);
                tLABranchGroupDB.getInfo();
                tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
                if (tBranchAttr == null) {
                    return false;
                }
                mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                        getBranchSeries());
                mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
                mLACommisionSchema.setBranchAttr(tBranchAttr);
                mLACommisionSchema.setBranchType(tBranchTypes[0]);
                mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
                mLACommisionSchema.setBranchType3(tBranchTypes[2]);
                String mon = mLACommisionSchema.getGetPolDate();
                //先根据回单日期置CalDate, WAGENO
                String tConfDate = "";
                tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"WT",mLAWageLogSchema.getEndDate(),
                                               tLJAGetEndorseSchema.getGetConfirmDate(),tLJAGetEndorseSchema.
                                       getActuGetNo());
                
               // --tContNo
		        //--查询回访成功数据
                String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
                //String tRiskCode = tLACommisionSchema.getRiskCode(); // 查询出险种信息
                String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                String tTransState = mLACommisionSchema.getTransState();

                
                // add new 
                String tAgentcode =mLACommisionSchema.getAgentCode() ;
                
                String caldate = this.calCalDateForAll(tBranchTypes[0],
                        tBranchTypes[1],
                        mLACommisionSchema.getSignDate(),
                        tLCContSchema.getGetPolDate(), "0",
                        tLJAGetEndorseSchema.
                        getMakeDate(), mLACommisionSchema.getCustomGetPolDate(),
                        mLACommisionSchema.getCValiDate(), "02",
                        tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),mLACommisionSchema.getRiskCode());

                if (!caldate.equals("") && !caldate.equals("0")) {
                    sql =
                            "select yearmonth from LAStatSegment where startdate<='" +
                            caldate + "' and enddate>='" + caldate +
                            "' and stattype='5' ";
                    tExeSQL = new ExeSQL();
                    tWageNo = tExeSQL.getOneValue(sql);
                    mLACommisionSchema.setTConfDate(tConfDate);
                    mLACommisionSchema.setWageNo(tWageNo);
                    mLACommisionSchema.setCalDate(caldate);
                }

                mLACommisionSchema.setCommDire("1");
                mLACommisionSchema.setFlag("1");

                mLACommisionSet.add(mLACommisionSchema);

               }
              }
              }
            //团单
            else{//增加处理程序
            	if(!tType.equals("WT")||!tType.equals("CT")) continue;
            	else{
                GrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
                tGrpContNo = tLJAGetEndorseSchema.getGrpContNo();
                tLCGrpPolSchema = new LCGrpPolSchema();
                tLCGrpContSchema = new LCGrpContSchema();
                try{
                tLCGrpPolSchema = queryLCGrpPol(GrpPolNo);
                }
                catch(Exception ex)
                {
                        ex.getStackTrace();
                }
                if (tLCGrpPolSchema == null) {
                continue;
                }
                tLCGrpContSchema = queryLCGrpCont(tGrpContNo);
                if (tLCGrpContSchema == null) {
                continue;
                }
                 //续保的处理
                LCRnewStateLogSchema tLCRnewStateLogSchema = new   LCRnewStateLogSchema();
                tLCRnewStateLogSchema = this.queryLCRnewGrpPol(GrpPolNo);
                if (tLCRnewStateLogSchema == null) {
                   tRenewCount = 0;
                } else {

//                	if (tRiskCode.equals("320106")){
//                		tRenewCount = 0;
//                	}
//                	else 
                tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }
                 COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
            //计算交费年度=保单生效日到交至日期
            PayYear = 0;
            //填充直接佣金明细表（佣金扎账表）
            mLACommisionSchema = new LACommisionSchema();
            mLACommisionSchema.setCommisionSN(COMMISIONSN);
            mLACommisionSchema.setCommisionBaseNo("0000000000");
            mLACommisionSchema.setReNewCount(tRenewCount);
           ////////////////////////////////////////////////////
           ///////////////////////////////////////////////////
           //  计算管理费
            String tManageFee=getmanagefee(tLJAGetEndorseSchema.getPolNo(),
                         tLJAGetEndorseSchema.getEndorsementNo(),tLJAGetEndorseSchema.getOtherNoType());

            double tFee=Double.parseDouble(tManageFee);
            mLACommisionSchema.setP7(tFee);
           /////////////////////////////////////////////////////
           ////////////////////////////////////////////////////
            //WageNo 783
            mLACommisionSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            mLACommisionSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            mLACommisionSchema.setContNo(tLJAGetEndorseSchema.getGrpContNo());
            mLACommisionSchema.setPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            mLACommisionSchema.setMainPolNo(tLCGrpPolSchema.getGrpPolNo()); //新加的 主险单号 用于继续率的计算
            mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); // 新加的 批单号码
            mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.
                                            getManageCom());
            mLACommisionSchema.setAppntNo(tLCGrpContSchema.getAppntNo());
            mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
            mLACommisionSchema.setRiskVersion(tLCGrpPolSchema.
                                              getRiskVersion());
            mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
            mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.
                                              getPayPlanCode());
            mLACommisionSchema.setCValiDate(tLCGrpPolSchema.getCValiDate());
            mLACommisionSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            mLACommisionSchema.setPayMode(tLCGrpPolSchema.getPayMode());
            mSSRS1 = queryInsuYear(tLCGrpPolSchema.getGrpPolNo(),tLCGrpPolSchema.getRiskCode());
            mLACommisionSchema.setF4(mSSRS1.GetText(1, 1));
            mLACommisionSchema.setF5(mSSRS1.GetText(1, 2));
            mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.
                                            getActuGetNo());
            mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
            mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.
                                                getEnterAccDate());
            mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.
                                            getGetConfirmDate());
            mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.
                                            getMakeDate());

            // CalcDate
            mLACommisionSchema.setCommDate(CurrentDate);
            mLACommisionSchema.setTransMoney(tLJAGetEndorseSchema.getGetMoney());
            mLACommisionSchema.setTransState("00"); //正常保单
            mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                                                  getTransMoney());
            mLACommisionSchema.setLastPayToDate(tLCGrpPolSchema.getPaytoDate());
            mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                                               getGetDate());
            mLACommisionSchema.setCommDire("1");
            mLACommisionSchema.setTransType("ZC");
            mLACommisionSchema.setFlag("2"); //正常
             mLACommisionSchema.setPayYear(PayYear);
            mLACommisionSchema.setPayYears(getPayYears(tLCGrpPolSchema.getGrpPolNo()));
            //mLACommisionSchema.setYears(tLCGrpPolSchema.getYears());
            mLACommisionSchema.setPayCount(1);
            mLACommisionSchema.setSignDate(tLCGrpContSchema.getSignDate());
            mLACommisionSchema.setGetPolDate(tLCGrpContSchema.getGetPolDate());
            mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
            mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                                            getAgentType());
            mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                                            getAgentCode());

            String tPolType = tLCGrpPolSchema.getStandbyFlag2();
            mLACommisionSchema.setPolType(tPolType == null ||
                                          tPolType.equals("") ?
                                          "1" : tPolType);

            mLACommisionSchema.setP1(tLCGrpPolSchema.getManageFeeRate());
            mLACommisionSchema.setP11(tLCGrpPolSchema.getGrpName());
            //mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
            //mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
            mLACommisionSchema.setP14(tLCGrpPolSchema.getPrtNo()); //cg add 2004-01-13
            mLACommisionSchema.setP15(tLCGrpPolSchema.getGrpProposalNo());
            mLACommisionSchema.setMakePolDate(tLCGrpPolSchema.getMakeDate()); //交单日期
            mLACommisionSchema.setCustomGetPolDate(tLCGrpContSchema.
                    getCustomGetPolDate()); //签字日期
            //  riskmark
            String tScanDate = this.getScanDate(tLCGrpPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
            mLACommisionSchema.setScanDate(tScanDate);
            mLACommisionSchema.setOperator(mGlobalInput.Operator);
            mLACommisionSchema.setMakeDate(CurrentDate);
            mLACommisionSchema.setMakeTime(CurrentTime);
            mLACommisionSchema.setModifyDate(CurrentDate);
            mLACommisionSchema.setModifyTime(CurrentTime);
            mLACommisionSchema.setMarketType(tLCGrpContSchema.getMarketType());
            mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode());
            tAgentCode = mLACommisionSchema.getAgentCode().trim();
            tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
            if (tAgentGroup == null) {
                 return false;
            }
            LABranchGroupSchema tLABranchGroupSchema;
            tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);
            mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                                               getBranchSeries());

            String tActType = "";
          //添加对于互动中介的处理
            if (tLCGrpPolSchema.getSaleChnl().equals("15")) 
            {
            	tActType = checkActive(mLACommisionSchema);
            	if("".equals(tActType))
            	{
            		continue;
            	}
            }
            String[] tBranchTypes = AgentPubFun.switchSalChnl(
                    tLCGrpPolSchema.
                    getSaleChnl(), tLCGrpPolSchema.getSaleChnlDetail(),
                    tActType);

            tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
            if (tBranchTypes == null) {
                return false;
            }
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tBranchCode);
            tLABranchGroupDB.getInfo();
            tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
            if (tBranchAttr == null) {
                return false;
            }
            mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                                               getBranchSeries());
            mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
            mLACommisionSchema.setBranchAttr(tBranchAttr);
            mLACommisionSchema.setBranchType(tBranchTypes[0]);
            mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
            mLACommisionSchema.setBranchType3(tBranchTypes[2]);

               //添加卡折结算单的套餐名称 现在只是计算个险人员销售 个销团渠道 结算单 团单
            if(mLAAgentSchema.getBranchType() != null && !mLAAgentSchema.getBranchType().trim().equals("")
             && mLAAgentSchema.getBranchType2() != null && !mLAAgentSchema.getBranchType2().trim().equals("")){
             if(mLAAgentSchema.getBranchType().equals("1")
            		 //添加互动渠道,健康渠道
            		 ||mLAAgentSchema.getBranchType().equals("5")||mLAAgentSchema.getBranchType().equals("7")||mLAAgentSchema.getBranchType().equals("6")||mLAAgentSchema.getBranchType().equals("8")){              
               if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
               {
                  String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                          +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                  tExeSQL = new ExeSQL();
                  String swarpcode = tExeSQL.getOneValue(warpsql);
                   System.out.println(warpsql+" a  "+swarpcode);
                  mLACommisionSchema.setF3(swarpcode);
               }
               else
               {
                  mLACommisionSchema.setF3("");
                   System.out.println("B");
               }
             }
//             if(mLAAgentSchema.getBranchType().equals("2") && mLAAgentSchema.getBranchType2().equals("01"))
//             {
//             	  String wrapsql = "select distinct riskwrapcode from lccontplanrisk "
//             	               + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"' ";
//             	  ExeSQL tExe = new ExeSQL();
//             	  String swrapcode = tExe.getOneValue(wrapsql);
//             	  if(swrapcode==null){
//             	  mLACommisionSchema.setF3("");
//             	  }
//             	  else 
//             	  {
//             	  mLACommisionSchema.setF3(swrapcode);
//                }
//             }
//           团险渠道(包含团险直销与团险中介)
             if(mLAAgentSchema.getBranchType().equals("2")){
            	 if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                 {
                    String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                            +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                    tExeSQL = new ExeSQL();
                    String swarpcode = tExeSQL.getOneValue(warpsql);
                     System.out.println(warpsql+" a  "+swarpcode);
                    mLACommisionSchema.setF3(swarpcode);
                 }
                 else
                 {
                    mLACommisionSchema.setF3("");
                     System.out.println("B");
                 }
            }
            }

            String mon = mLACommisionSchema.getGetPolDate();
            //先根据回单日期置CalDate, WAGENO
            getPolDate = mLACommisionSchema.getGetPolDate(); //xiangchun 2005-12-01团险根据回执日期判断年月
            custPolDate = mLACommisionSchema.getCustomGetPolDate();
            String tConfDate = "";
            tConfDate = this.queryConfDate(tLCGrpPolSchema.getGrpContNo(),
            		                       tLJAGetEndorseSchema.getFeeOperationType(),tLJAGetEndorseSchema.
                                           getMakeDate(),tLJAGetEndorseSchema.
                                           getGetConfirmDate(),tLJAGetEndorseSchema.
                                           getActuGetNo());
            //--tGrpContNo
	        //--查询回访成功数据
            String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
            String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
            String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
            String tTransState = mLACommisionSchema.getTransState();
            // add new 
            String tAgentcode =mLACommisionSchema.getAgentCode() ;
            
            String caldate = this.calCalDateForAll(tBranchTypes[0],
                    tBranchTypes[1],
                    mLACommisionSchema.getSignDate(),
                    mLACommisionSchema.getGetPolDate(), "0",
                    tLJAGetEndorseSchema.
                    getMakeDate(), mLACommisionSchema.getCustomGetPolDate(),
                    mLACommisionSchema.getCValiDate(),"02",
                    tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);

            mLACommisionSchema.setCalDate(caldate);
            String tSignDate = mLACommisionSchema.getSignDate();

             if (!caldate.equals("") && !caldate.equals("0")) {
                    sql =
                            "select yearmonth from LAStatSegment where startdate<='" +
                            caldate + "' and enddate>='" + caldate +
                            "' and stattype='5' ";
                    tExeSQL = new ExeSQL();
                    tWageNo = tExeSQL.getOneValue(sql);
                    mLACommisionSchema.setTConfDate(tConfDate);
                    mLACommisionSchema.setWageNo(tWageNo);
                    mLACommisionSchema.setCalDate(caldate);
                }
            mLACommisionSet.add(mLACommisionSchema);
              }
           }


        }

        System.out.println("*****************查询犹豫期撤保保全回退   结束***************");

        System.out.println("|||境外旅游险|||******查询团险批改退费表  开始***************");
        //团险境外旅游险种的退保,保单次的退补佣金
        //个销团情况
        tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        String tBranchType = "2"; //团险撤单险种的渠道
        String tBranchType2 = "01";
        tSSRS = new SSRS();

        tSSRS = queryRiskLJAGetEndorse(tBranchType, tBranchType2);
        if (tSSRS == null) {
            return false;
        }
        mStrDate=new String[tSSRS.getMaxRow()][tSSRS.getMaxCol()];

        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            //tLJAGetEndorseDB.setActuGetNo(tSSRS.GetText(i, 1));  xiangchun 20060226 modify
            String Sql =
                    "select sum(getmoney) from LJAGetEndorse where  FeeOperationType = 'CT' " +
                    "  and GrpPolNo='" + tSSRS.GetText(i, 1) + "' and " +
                    "PayPlanCode='" + tSSRS.GetText(i, 2) +
                    "'  and makedate= '" + tSSRS.GetText(i, 4) + "' with ur ";
            tExeSQL = new ExeSQL();
            double tGetMoney = Double.parseDouble(tExeSQL.getOneValue(Sql));
            tLJAGetEndorseDB.setGrpPolNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setMakeDate(tSSRS.GetText(i, 4));
            LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
            tLJAGetEndorseSet = tLJAGetEndorseDB.query();

            tLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);

            //判断此犹豫期撤单件 是否已经提过数 ,如果提过,则不再提取
            //提数回退时 不回退上个月的数据,所以会出现此种情况
            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setGrpPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            wLACommisionDB.setTransType("CT");
            wLACommisionDB.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
            wLACommisionDB.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }

            tAgentCode = tLJAGetEndorseSchema.getAgentCode();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            tLAAgentDB.getInfo();
            mLAAgentSchema = tLAAgentDB.getSchema();
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            if (tGrpPolNo != null && !tGrpPolNo.equals("00000000000000000000") &&
                !tGrpPolNo.equals("")) {
                GrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
                tGrpContNo = tLJAGetEndorseSchema.getGrpContNo();
                tLCGrpPolSchema = new LCGrpPolSchema();
                tLCGrpContSchema = new LCGrpContSchema();
                tLCGrpPolSchema = queryLCGrpPol(GrpPolNo);
                if (tLCGrpPolSchema == null) {
                    continue;
                }
                tLCGrpContSchema = queryLCGrpCont(tGrpContNo);
                if (tLCGrpContSchema == null) {
                    continue;
                }
                if (!"0".equals(tLCGrpContSchema.getDegreeType())) { //不为单次
                    continue;
                }
                //续保的处理
                LCRnewStateLogSchema tLCRnewStateLogSchema = new
                        LCRnewStateLogSchema();
                tLCRnewStateLogSchema = this.queryLCRnewGrpPol(tGrpPolNo);
                if (tLCRnewStateLogSchema == null) {
                    tRenewCount = 0;
                } else {

//                	if (tRiskCode.equals("320106")){
//                		tRenewCount = 0;
//                	}
//                	else 
                    tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }
               //在下面处理解约时需要此变量,因为这里已经处理了,下面不再处理
                String[] tStrDate=new String[tSSRS.getMaxCol()];
                tStrDate=tSSRS.getRowData(i);
                mStrDate[i-1]=tStrDate;

                COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
                //计算交费年度=保单生效日到交至日期
                PayYear = 0;
                //填充直接佣金明细表（佣金扎账表）
                mLACommisionSchema = new LACommisionSchema();
                mLACommisionSchema.setCommisionSN(COMMISIONSN);
                mLACommisionSchema.setCommisionBaseNo("0000000000");
                mLACommisionSchema.setReNewCount(tRenewCount);
                //WageNo 783
                mLACommisionSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
                mLACommisionSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
                mLACommisionSchema.setContNo(tLCGrpPolSchema.getGrpContNo());
                mLACommisionSchema.setPolNo(tLCGrpPolSchema.getGrpPolNo());
                mLACommisionSchema.setMainPolNo(tLCGrpPolSchema.getGrpPolNo()); //新加的 主险单号 用于继续率的计算
                mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); // 新加的 批单号码
                mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.
                                                getManageCom());
                mLACommisionSchema.setAppntNo(tLCGrpContSchema.getAppntNo());
                mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
                mLACommisionSchema.setRiskVersion(tLCGrpPolSchema.
                                                  getRiskVersion());
                mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
                mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.
                                                  getPayPlanCode());
                mLACommisionSchema.setCValiDate(tLCGrpPolSchema.getCValiDate());
                mLACommisionSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
                mLACommisionSchema.setPayMode(tLCGrpPolSchema.getPayMode());
                mSSRS1 = queryInsuYear(tLCGrpPolSchema.getGrpPolNo(),tLCGrpPolSchema.getRiskCode());
                mLACommisionSchema.setF4(mSSRS1.GetText(1, 1));
                mLACommisionSchema.setF5(mSSRS1.GetText(1, 2));
                mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.
                                                getActuGetNo());
                mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
                mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.
                        getEnterAccDate());
                mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.
                                                getGetConfirmDate());
                mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.
                                                getMakeDate());



                // CalcDate
                mLACommisionSchema.setCommDate(CurrentDate);
                mLACommisionSchema.setTransMoney(
                        0 - java.lang.Math.abs(tGetMoney));
                mLACommisionSchema.setTransState("00"); //正常保单
                mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                        getTransMoney());
                mLACommisionSchema.setLastPayToDate(tLCGrpPolSchema.
                        getPaytoDate());
                mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                        getGetDate());
                mLACommisionSchema.setTransType("CT"); //境外旅游险种解约

                if(tLCGrpPolSchema.getSaleChnl().equals("07"))
                {
                    mLACommisionSchema.setF1("02");//职团开拓
                    mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
                }
                else
                {
                    mLACommisionSchema.setF1("01");
                }
                mLACommisionSchema.setFlag("0"); //部分撤件（境外旅游险种）
                mLACommisionSchema.setPayYear(PayYear);
                mLACommisionSchema.setPayCount(1);
                mLACommisionSchema.setSignDate(tLCGrpContSchema.getSignDate());
                mLACommisionSchema.setGetPolDate(tLCGrpContSchema.getGetPolDate());
                //BranchType 788
                mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
                mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                                                getAgentType());
                mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                                                getAgentCode());

                /*****************************************
                 * ***************************************
                 * ***************************************
                 */

                String tPolType = tLCGrpPolSchema.getStandbyFlag2();
                mLACommisionSchema.setPolType(tPolType == null ||
                                              tPolType.equals("") ?
                                              "1" : tPolType);
                /****************************************
                 * **************************************
                 * **************************************
                 */

                mLACommisionSchema.setP1(tLCGrpPolSchema.getManageFeeRate());

                mLACommisionSchema.setP11(tLCGrpPolSchema.getGrpName());
                mLACommisionSchema.setP14(tLCGrpPolSchema.getPrtNo()); //cg add 2004-01-13
                mLACommisionSchema.setP15(tLCGrpPolSchema.getGrpProposalNo());
                mLACommisionSchema.setMakePolDate(tLCGrpPolSchema.getMakeDate()); //交单日期
                mLACommisionSchema.setCustomGetPolDate(tLCGrpContSchema.
                        getCustomGetPolDate()); //签字日期
                //  riskmark
                String tScanDate = this.getScanDate(tLCGrpPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
                mLACommisionSchema.setScanDate(tScanDate);
                mLACommisionSchema.setOperator(mGlobalInput.Operator);
                mLACommisionSchema.setMakeDate(CurrentDate);
                mLACommisionSchema.setMakeTime(CurrentTime);
                mLACommisionSchema.setModifyDate(CurrentDate);
                mLACommisionSchema.setModifyTime(CurrentTime);
                mLACommisionSchema.setMarketType(tLCGrpContSchema.getMarketType());
                mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode());
                tAgentCode = mLACommisionSchema.getAgentCode().trim();
                tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
                if (tAgentGroup == null) {
                    return false;
                }
                LABranchGroupSchema tLABranchGroupSchema;
                tLABranchGroupSchema = new LABranchGroupSchema();
                tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);
                mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                        getBranchSeries());
                String tActType = "";
              //添加对于互动中介的处理
                if (tLCGrpPolSchema.getSaleChnl().equals("15")) 
                {
                	tActType = checkActive(mLACommisionSchema);
                	if("".equals(tActType))
                	{
                		continue;
                	}
                }
                String[] tBranchTypes = AgentPubFun.switchSalChnl(
                        tLCGrpPolSchema.
                        getSaleChnl(), tLCGrpPolSchema.getSaleChnlDetail(),
                        tActType);

                tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
                if (tBranchTypes == null) {
                    return false;
                }
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tBranchCode);
                tLABranchGroupDB.getInfo();
                tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
                if (tBranchAttr == null) {
                    return false;
                }
                mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                        getBranchSeries());
                mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
                mLACommisionSchema.setBranchAttr(tBranchAttr);
                mLACommisionSchema.setBranchType(tBranchTypes[0]);
                mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
                mLACommisionSchema.setBranchType3(tBranchTypes[2]);
                   //添加卡折结算单的套餐名称 现在只是计算个险人员销售 个销团渠道 结算单 团单
                if(mLAAgentSchema.getBranchType() != null && !mLAAgentSchema.getBranchType().trim().equals("")
                   && mLAAgentSchema.getBranchType2() != null && !mLAAgentSchema.getBranchType2().trim().equals("")){
                if(mLAAgentSchema.getBranchType().equals("1")
                		 //添加互动渠道,健管渠道
               		 ||mLAAgentSchema.getBranchType().equals("5")||mLAAgentSchema.getBranchType().equals("7")||mLAAgentSchema.getBranchType().equals("6")||mLAAgentSchema.getBranchType().equals("8")){              
                  if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                  {
                     String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                             +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                     tExeSQL = new ExeSQL();
                     String swarpcode = tExeSQL.getOneValue(warpsql);
                      System.out.println(warpsql+" a  "+swarpcode);
                     mLACommisionSchema.setF3(swarpcode);
                  }
                  else
                  {
                     mLACommisionSchema.setF3("");
                      System.out.println("B");
                  }
                }
//                if(mLAAgentSchema.getBranchType().equals("2") && mLAAgentSchema.getBranchType2().equals("01"))
//                {
//                	  String wrapsql = "select distinct riskwrapcode from lccontplanrisk "
//                	               + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"' ";
//                	  ExeSQL tExe = new ExeSQL();
//                	  String swrapcode = tExe.getOneValue(wrapsql);
//                	  if(swrapcode==null){
//                	  mLACommisionSchema.setF3("");
//                	  }
//                	  else 
//                	  {
//                	  mLACommisionSchema.setF3(swrapcode);
//                   }
//                }
                //团险渠道(包含团险直销与团险中介)
                if(mLAAgentSchema.getBranchType().equals("2")){              
                    if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                    {
                       String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                               +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                       tExeSQL = new ExeSQL();
                       String swarpcode = tExeSQL.getOneValue(warpsql);
                        System.out.println(warpsql+" a  "+swarpcode);
                       mLACommisionSchema.setF3(swarpcode);
                    }
                    else
                    {
                       mLACommisionSchema.setF3("");
                        System.out.println("B");
                    }
                  }
                }
                String mon = mLACommisionSchema.getGetPolDate();
                //先根据回单日期置CalDate, WAGENO
                getPolDate = mLACommisionSchema.getGetPolDate(); //xiangchun 2005-12-01
                custPolDate = mLACommisionSchema.getCustomGetPolDate();
            //    custPolDate = mLACommisionSchema.getCustomGetPolDate();
                String tConfDate = "";
                tConfDate = this.queryConfDate(tLCGrpPolSchema.getGrpContNo(),"CT",tSSRS.GetText(i, 4),
                		          tLJAGetEndorseSchema.
                        getGetConfirmDate(),tLJAGetEndorseSchema.
                        getActuGetNo());
                
               // --tGrpContNo
		        //--查询回访成功数据
                String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
                String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
                String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                String tTransState = mLACommisionSchema.getTransState();
                // add new 
                String tAgentcode =mLACommisionSchema.getAgentCode() ;
                
                String caldate = this.calCalDateForAll(tBranchTypes[0],
                        tBranchTypes[1],
                        mLACommisionSchema.getSignDate(),
                        mLACommisionSchema.getGetPolDate(), "0",
                        tLJAGetEndorseSchema.
                        getMakeDate(), mLACommisionSchema.getCustomGetPolDate(),
                        mLACommisionSchema.getCValiDate(),"02",
                        tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);
                mLACommisionSchema.setCalDate(caldate);
                mLACommisionSchema.setTConfDate(tConfDate);
                String tSignDate = mLACommisionSchema.getSignDate();

                if (!caldate.equals("")) {
                    int z = judgeWageIsCal(GrpPolNo, tSignDate);
                    switch (z) {
                    case 0: //判断过程出错
                        return false;
                    case 1: { //佣金已经计算,CommDire=1 可以参加下月佣金计算就扣除了这部分佣金
                        mLACommisionSchema.setCommDire("1");
                        mLACommisionSchema.setFlag("0");
                        sql =
                                "select yearmonth from LAStatSegment where startdate<='"
                                + caldate + "' and enddate>='" + caldate +
                                "' and stattype='91' ";
                        tExeSQL = new ExeSQL();
                        tWageNo = tExeSQL.getOneValue(sql);

                        mLACommisionSchema.setWageNo(tWageNo); //WageNo为下一月,下月佣金可以算到

                        break;
                    }
                    case 2: { //佣金尚未计算，修改原来的数据，使原先的数据不参加佣金计算，那么本条数据也不参加下月的计算
                        mLACommisionSchema.setCommDire("1");
                        mLACommisionSchema.setFlag("0");

                        String tSQL1 =
                                "select * from LACommision where GrpPolNo = '" +
                                mLACommisionSchema.getGrpPolNo() +
                                "' and  SignDate='" + tSignDate +
                                "' order by commisionsn with ur "; //modify :2005-11-05 xiangchun 加上order by commisionsn
                        System.out.println(tSQL1);
                        LACommisionDB tLACommisionDB = new LACommisionDB();
                        LACommisionSet dLACommisionSet = new LACommisionSet();
                        dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
                        LACommisionSet eLACommisionSet = new LACommisionSet();
                        System.out.println(dLACommisionSet.size());
                        for (int j = 1; j <= dLACommisionSet.size(); j++) {
                            boolean hasFound = false;
                            LACommisionSchema tLACommisionSchema = new
                                    LACommisionSchema();
                            tLACommisionSchema = dLACommisionSet.get(j);
                            if (j == 1) {
                                mLACommisionSchema.setWageNo(
                                        tLACommisionSchema.getWageNo()); //modify :2005-11-05 xiangchun
                            }
                            for (int k = 1; k <= mLAUpdateCommisionSet.size();
                                         k++) {
                                if (mLAUpdateCommisionSet.get(k).
                                    getCommisionSN().equals(
                                            tLACommisionSchema.
                                            getCommisionSN())) {
                                    hasFound = true;
                                    mLAUpdateCommisionSet.get(k).
                                            setCommDire("1");
                                    // break;                 modify :2005-10-17 xiangchun
                                }
                            }
                            System.out.println(hasFound);
                            if (hasFound) {
                                continue;
                            } else {
                                tLACommisionSchema.setCommDire("1");
                                tLACommisionSchema.setModifyDate(PubFun.
                                        getCurrentDate());
                                tLACommisionSchema.setModifyTime(PubFun.
                                        getCurrentTime());
                                eLACommisionSet.add(tLACommisionSchema);
                            }
                        }
                        for (int k = 1; k <= mLACommisionSet.size(); k++) 
                        {
                            boolean WageNoFlag = false; //判断WageNo是否付值
                            if (mLACommisionSet.get(k).getPolNo().
                                equals(
                                        mLACommisionSchema.
                                        getPolNo())) {
                                if (!WageNoFlag) {
                                    mLACommisionSchema.setWageNo(
                                            mLACommisionSet.get(k).getWageNo()); //modify :2005-11-05 xiangchun
                                }
                                mLACommisionSet.get(k).setCommDire("1");
                                WageNoFlag = true;
                                // break;                 modify :2005-10-17 xiangchun
                            }
                        }

                        mLAUpdateCommisionSet.add(eLACommisionSet);
                        break; // modify :2005-10-17 xiangchun   ///////////////////////////////////////////////////是否应该为continue
                    }
                    }
                }
                mLACommisionSet.add(mLACommisionSchema);
            }
        }
        System.out.println("|||境外旅游险|||******查询团险批改退费表  结束***************");

        System.out.println("|||减少被保人|||******查询团险批改退费表  开始***************");
        //减少被保人申请（以受理日为准）在生效日期前或生效日后15日内的的退补佣金
        //境外旅行系列险种，投保方式为单次的，办理解约后，扣回本单发生的佣金
        //减少被保人都应该参加佣金计算,不管佣金是否计算commdire='1'
        tSSRS = new SSRS();
        tSSRS = queryCutLJAGetEndorse();
        if (tSSRS == null) {
            return false;
        }
        //减少被保人的处理
        if (!dealCutInsruedNo(tSSRS)) {
            return false;

        }
        System.out.println("|||减少被保人|||******查询团险批改退费表  结束***************");

        System.out.println("|||无名单减人|||******查询团险批改退费表  开始***************");
        //无名单减人

        tSSRS = new SSRS();
        tSSRS = queryNotelessCut();
        if (tSSRS == null) {
            return false;
        }
        if (!dealNoteLessCut(tSSRS)) {
            return false;
        }
        System.out.println("|||无名单减人|||******查询团险批改退费表  结束***************");

        System.out.println("|||无名单增人|||******查询团险批改退费表  开始***************");
        //无名单增人
        tSSRS = new SSRS();
        tSSRS = queryNotelessAdd();
        if (tSSRS == null) {
            return false;
        }
        if (!dealNoteLessAdd(tSSRS)) {
                    return false;

        }
        System.out.println("|||无名单增人|||******查询团险批改退费表  结束***************");

        System.out.println("|||团单增加保费|||******查询团险批改退费表  开始***************");
        //团单增加保费的处理
        tSSRS = new SSRS();
        tSSRS = queryAddPrem();
        if (tSSRS == null) {
            return false;
        }
        tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            String Sql =
                    "select sum(getmoney) from LJAGetEndorse where  FeeOperationType ='" +
                    tSSRS.GetText(i, 3) + "' " +
                    " and FeeFinaType = '" + tSSRS.GetText(i, 4) +
                    "' and GrpPolNo='" +
                    tSSRS.GetText(i, 1) + "' and " +
                    "PayPlanCode='" + tSSRS.GetText(i, 2) + "' and makedate='" +
                    tSSRS.GetText(i, 5) + "' with ur ";
            tExeSQL = new ExeSQL();
            double tGetMoney = Double.parseDouble(tExeSQL.getOneValue(Sql));
            tLJAGetEndorseDB.setGrpPolNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setMakeDate(tSSRS.GetText(i, 5));
            LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
            tLJAGetEndorseSet = tLJAGetEndorseDB.query();
            double tSumFee=0;
            for(int k=1;k<=tLJAGetEndorseSet.size();k++)
            {

                LJAGetEndorseSchema tgetLJAGetEndorseSchema = new
                        LJAGetEndorseSchema();
                tgetLJAGetEndorseSchema = tLJAGetEndorseSet.get(k);
                //计算管理费
                String tManageFee = getmanagefee(tgetLJAGetEndorseSchema.
                                                 getPolNo(),
                                                 tgetLJAGetEndorseSchema.
                                                 getEndorsementNo(),
                                                 tgetLJAGetEndorseSchema.
                                                 getOtherNoType());
                if (tManageFee == null)
                    return false;
                double tFee = Double.parseDouble(tManageFee);
                tSumFee = Arith.add(tSumFee, tFee);
            }
            tLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);
            //判断 此数据是否已经提过数 ,如果提过,则不再提取
            //提数回退时不回退上个月的数据,所以会出现此种情况

            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setGrpPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            wLACommisionDB.setTransType(tLJAGetEndorseSchema.
                                        getFeeOperationType());
            wLACommisionDB.setTMakeDate(tLJAGetEndorseSchema.
                                        getMakeDate());
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            if (tGrpPolNo != null &&
                !tGrpPolNo.equals("00000000000000000000") &&
                !tGrpPolNo.equals("")) {
                int tReturn = dealLACommision(tLJAGetEndorseSchema, tGetMoney,tSumFee,0,"02");
                if (tReturn == 1) {
                    continue;
                } else if (tReturn == 2) {
                    return false;
                }
            }
        }
        System.out.println("|||团单增加保费|||******查询团险批改退费表  结束***************");

        System.out.println("|||团单减少保费  扣除提奖|||******查询团险批改退费表  开始***************");
        //团单减少保费的处理(扣除提奖)
        tSSRS = new SSRS();
        tSSRS = querycutPrem1();
        if (tSSRS == null) {
            return false;
        }
        tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            String Sql =
                    "select sum(getmoney) from LJAGetEndorse where  FeeOperationType ='" +
                    tSSRS.GetText(i, 3) + "' " +
                    " and FeeFinaType = '" + tSSRS.GetText(i, 4) +
                    "' and GrpPolNo='" +
                    tSSRS.GetText(i, 1) + "' and " +
                    "PayPlanCode='" + tSSRS.GetText(i, 2) + "' and makedate='" +
                    tSSRS.GetText(i, 5) + "' with ur ";
            tExeSQL = new ExeSQL();
            double tGetMoney = Double.parseDouble(tExeSQL.getOneValue(Sql));
            tLJAGetEndorseDB.setGrpPolNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setMakeDate(tSSRS.GetText(i, 5));
            LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
            tLJAGetEndorseSet = tLJAGetEndorseDB.query();
            double tSumFee=0;
            for(int k=1;k<=tLJAGetEndorseSet.size();k++)
            {

                LJAGetEndorseSchema tgetLJAGetEndorseSchema = new
                        LJAGetEndorseSchema();
                tgetLJAGetEndorseSchema = tLJAGetEndorseSet.get(k);
                //计算管理费
                String tManageFee = getmanagefee(tgetLJAGetEndorseSchema.
                                                 getPolNo(),
                                                 tgetLJAGetEndorseSchema.
                                                 getEndorsementNo(),
                                                 tgetLJAGetEndorseSchema.
                                                 getOtherNoType());
                if (tManageFee == null)
                    return false;
                double tFee = Double.parseDouble(tManageFee);
                tSumFee = Arith.add(tSumFee, tFee);
            }
            tLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);
            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setGrpPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            wLACommisionDB.setTransType(tLJAGetEndorseSchema.
                                        getFeeOperationType());
            wLACommisionDB.setTMakeDate(tLJAGetEndorseSchema.
                                        getMakeDate());
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            if (tGrpPolNo != null &&
                !tGrpPolNo.equals("00000000000000000000") &&
                !tGrpPolNo.equals("")) {
                int tReturn = dealLACommision(tLJAGetEndorseSchema, tGetMoney,tSumFee,0,"15");
                if (tReturn == 1) {
                    continue;
                } else if (tReturn == 2) {
                    return false;
                }
            }
        }
        System.out.println("|||团单减少保费|||******查询团险批改退费表  结束***************");

        System.out.println("|||团单减少保费的 不扣提奖|||******查询团险批改退费表  开始***************");
        //团单减少保费的处理(不扣除提奖)
        tSSRS = new SSRS();
        tSSRS = querycutPrem2();
        if (tSSRS == null) {
            return false;
        }
        tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String tGRPPolNo=tSSRS.GetText(i, 1);
            String tPayPlanCode=tSSRS.GetText(i, 2);
            String tFeeOperationType=tSSRS.GetText(i, 3);
            String tmakedate=tSSRS.GetText(i, 5);
            boolean tFlag=false;
            if(mStrDate!=null)
            {
                //如果上面已经处理过此数据,则不再处理(现在主要指境外救援险种投保方式为单次的解约)
                for (int k = 1; k <= mStrDate.length; k++) {
                    String tPreGRPPolNo = mStrDate[k - 1][0];
                    String tPrePayPlanCode = mStrDate[k - 1][1];
                    String tPreFeeOperationType = mStrDate[k - 1][2];
                    String tPremakedate = mStrDate[k - 1][3];
                    //如果为空，则返回
                    if(tPreGRPPolNo==null || tPrePayPlanCode==null || tPreFeeOperationType==null
                            || tPremakedate==null )
                    {
                             continue;
                    }
                    //如果上面已经处理过
                    if (tPreGRPPolNo.equals(tGRPPolNo) &&
                        tPrePayPlanCode.equals(tPayPlanCode) &&
                        tPreFeeOperationType.equals(tFeeOperationType) &&
                        tPremakedate.equals(tmakedate)) {
                        tFlag = true;
                        break;
                    }
                }
            }
            if(tFlag)
            {//如果上面已经处理过则不再处理(处理境外救援险种的解约时处理过了)
                continue;
            }

            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            String Sql =
                    "select sum(getmoney) from LJAGetEndorse where  FeeOperationType ='" +
                    tSSRS.GetText(i, 3) + "' " +
                    " and FeeFinaType = '" + tSSRS.GetText(i, 4) +
                    "' and GrpPolNo='" +
                    tSSRS.GetText(i, 1) + "' and " +
                    "PayPlanCode='" + tSSRS.GetText(i, 2) + "' and makedate='" +
                    tSSRS.GetText(i, 5) + "' with ur ";
            tExeSQL = new ExeSQL();
            double tGetMoney = Double.parseDouble(tExeSQL.getOneValue(Sql));
            tLJAGetEndorseDB.setGrpPolNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setMakeDate(tSSRS.GetText(i, 5));
            LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
            tLJAGetEndorseSet = tLJAGetEndorseDB.query();
            double tSumFee=0;
            double tAccural = 0;
            for(int k=1;k<=tLJAGetEndorseSet.size();k++)
            {

                LJAGetEndorseSchema tgetLJAGetEndorseSchema = new
                        LJAGetEndorseSchema();
                tgetLJAGetEndorseSchema = tLJAGetEndorseSet.get(k);
                //计算管理费
                //2017-3-22 yangyang
                //这个逻辑有问题，解约的时候 不是从lcinsureaccclassfee取管理费，所以给注了，后面有用到的，需要再讨论逻辑
//                String tManageFee = getmanagefee(tgetLJAGetEndorseSchema.
//                                                 getPolNo(),
//                                                 tgetLJAGetEndorseSchema.
//                                                 getEndorsementNo(),
//                                                 tgetLJAGetEndorseSchema.
//                                                 getOtherNoType());
//                if (tManageFee == null)
//                    return false;
//                double tFee = Double.parseDouble(tManageFee);
//                tSumFee = Arith.add(tSumFee, tFee);
                //2017-3-22 yangyang
                //添加利息的算法
                String tAccuralFee = getAccuralfee(tgetLJAGetEndorseSchema);
				double tFee = Double.parseDouble(tAccuralFee);
				tAccural = Arith.add(tAccural, tFee);
            }
            tLJAGetEndorseSchema = tLJAGetEndorseSet.get(1);
            //判断 此数据是否已经提过数 ,如果提过,则不再提取
            //提数回退时不回退上个月的数据,所以会出现此种情况

            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setGrpPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            wLACommisionDB.setTransType(tLJAGetEndorseSchema.
                                        getFeeOperationType());
            wLACommisionDB.setTMakeDate(tLJAGetEndorseSchema.
                                        getMakeDate());
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            if (tGrpPolNo != null &&
                !tGrpPolNo.equals("00000000000000000000") &&
                !tGrpPolNo.equals("")) {
                int tReturn = dealLACommision(tLJAGetEndorseSchema,(tGetMoney+tAccural),tSumFee,tAccural,"16");
                if (tReturn == 1) {
                    continue;
                } else if (tReturn == 2) {
                    return false;
                }
            }
        }
        System.out.println("|||团单减少保费的 不扣提奖|||******查询团险批改退费表  结束***************");

        return true;
    }

    //处理个险卡单
    private boolean dealperCommision(LJAPayPersonSchema tLJAPayPersonSchema,
                                     String tContNo, String PolNo) {
        String COMMISIONSN = "";
        int PayYear = 0;
        String tAgentCode = "";
        String tBranchCode = "";
        String tBranchAttr = "";

        //卡单没有续保,为tRenewCount 0
        tRenewCount = 0;
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLJAPayPersonSchema.getAgentCode());
        tLAAgentDB.getInfo();
        mLAAgentSchema = tLAAgentDB.getSchema();
        //进行险种提取的时候要从险种表中提取----songgh
        String tSQL = "select distinct riskcode,payintv from lcpol where prtno='"+tContNo+"'" +
        		" union (select distinct riskcode,payintv from lbpol where prtno='"+tContNo+"') with ur ";
        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = mExeSQL.execSQL(tSQL);
        if(tSSRS.getMaxRow()<=0)
        {
        	 return true;
        }
        else
        {
        	for(int i=1;i<=tSSRS.getMaxRow();i++)
        	{

		        COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
		        if (tLJAPayPersonSchema.getPayIntv() == 0) { //趸交payIntv = 0
		            PayYear = 0;
		        }
		        PayYear = 0;
		        mLACommisionSchema = new LACommisionSchema();
		        mLACommisionSchema.setCommisionSN(COMMISIONSN);
		        mLACommisionSchema = new LACommisionSchema();
		        System.out.println("======" + COMMISIONSN);
		        mLACommisionSchema.setCommisionSN(COMMISIONSN);
		        mLACommisionSchema.setCommisionBaseNo("0000000000");
		        //WageNo 563
		        mLACommisionSchema.setGrpContNo(tLJAPayPersonSchema.getGrpContNo());
		        mLACommisionSchema.setGrpPolNo(tLJAPayPersonSchema.getGrpPolNo());
		        mLACommisionSchema.setContNo(tLJAPayPersonSchema.getContNo());//印刷号
		        mLACommisionSchema.setPolNo(tLJAPayPersonSchema.getPolNo());
		        mLACommisionSchema.setMainPolNo(tLJAPayPersonSchema.getPolNo()); //新加的 主险单号 用于继续率的计算
		        mLACommisionSchema.setManageCom(tLJAPayPersonSchema.
		                                        getManageCom());
		        mLACommisionSchema.setAppntNo("");

		      //  mLACommisionSchema.setRiskCode(tLJAPayPersonSchema.getRiskCode());
		        mLACommisionSchema.setRiskCode(tSSRS.GetText(i, 1));//songgh险种只能从lcpol中获取
		        mLACommisionSchema.setRiskVersion("");
		        mLACommisionSchema.setDutyCode(tLJAPayPersonSchema.getDutyCode());
		        mLACommisionSchema.setPayPlanCode(tLJAPayPersonSchema.
		                                          getPayPlanCode());
		        mLACommisionSchema.setPayIntv(tSSRS.GetText(i, 2));

		        String tPolNo = tLJAPayPersonSchema.getPolNo();
		        String tIncomeNo = getIncomeNo(tPolNo);
		        String tPayMode = getPayMode(tIncomeNo);
		        mLACommisionSchema.setPayMode(tPayMode);
		        String tSQL2 = "select distinct insuyear,insuyearflag from lcpol where prtno='"+tContNo+"'" +
        		" union select distinct insuyear,insuyearflag from lbpol where prtno='"+tContNo+"' fetch first 1 rows only with ur ";
                ExeSQL mExeSQL2 = new ExeSQL();
                SSRS tSSRS2 = new SSRS();
                tSSRS2 = mExeSQL2.execSQL(tSQL2);
                mLACommisionSchema.setF4(tSSRS2.GetText(1, 1));
                mLACommisionSchema.setF5(tSSRS2.GetText(1, 2));		        
		        
		        mLACommisionSchema.setReceiptNo(tLJAPayPersonSchema.getPayNo());
		        mLACommisionSchema.setTPayDate(tLJAPayPersonSchema.getPayDate());
		        mLACommisionSchema.setTEnterAccDate(tLJAPayPersonSchema.
		                                            getEnterAccDate());
		        mLACommisionSchema.setTConfDate(tLJAPayPersonSchema.getConfDate());
		        mLACommisionSchema.setTMakeDate(tLJAPayPersonSchema.getMakeDate());
		        // CalcDate
		        mLACommisionSchema.setCommDate(CurrentDate);
		        mLACommisionSchema.setTransMoney(tLJAPayPersonSchema.
		                                         getSumActuPayMoney());
		        mLACommisionSchema.setTransState("00"); //正常保单
		        mLACommisionSchema.setTransStandMoney(tLJAPayPersonSchema.
		                                              getSumDuePayMoney());
		        mLACommisionSchema.setLastPayToDate(tLJAPayPersonSchema.
		                                            getLastPayToDate());
		        mLACommisionSchema.setCurPayToDate(tLJAPayPersonSchema.
		                                           getCurPayToDate());
		        mLACommisionSchema.setTransType("ZC");
		        //TransState 交易处理状态 590 - 614
		        mLACommisionSchema.setCommDire("1");
		        // DirectWage
		        // AppendWage
		        mLACommisionSchema.setF1("01");
		        mLACommisionSchema.setFlag("15"); //卡单
		        mLACommisionSchema.setPayYear(PayYear);
		        mLACommisionSchema.setReNewCount(tRenewCount);
		        mLACommisionSchema.setPayYears("");
		        String tCardNo = tLJAPayPersonSchema.getContNo();
		        tCardNo = tCardNo.substring(0, 2);
		        String tYears = getYears(tCardNo);
		        mLACommisionSchema.setYears(tYears);
		        mLACommisionSchema.setPayCount(tLJAPayPersonSchema.getPayCount());
		        mLACommisionSchema.setSignDate(tLJAPayPersonSchema.getMakeDate());
		        mLACommisionSchema.setAgentCom(tLJAPayPersonSchema.getAgentCom());
		        mLACommisionSchema.setAgentType(tLJAPayPersonSchema.
		                                        getAgentType());
		        mLACommisionSchema.setAgentCode(tLJAPayPersonSchema.
		                                        getAgentCode());
		        /******************************************
		         * ****************************************
		         * *****************************************
		         */
		        mLACommisionSchema.setPolType("1"); //0:优惠 1：正常
		//        mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());
		        /*******************************************************
		         * *****************************************************
		         * *****************************************************
		         */
		        mLACommisionSchema.setP14(tLJAPayPersonSchema.getContNo());
		        mLACommisionSchema.setOperator(mGlobalInput.Operator);
		        mLACommisionSchema.setMakeDate(CurrentDate);
		        mLACommisionSchema.setMakeTime(CurrentTime);
		        mLACommisionSchema.setModifyDate(CurrentDate);
		        mLACommisionSchema.setModifyTime(CurrentTime);
		        String[] tBranchTypes = AgentPubFun.switchSalChnl(
		                "01", "01", "");
		        if (mBranchType.equals("3") && mBranchType2.equals("01")) {
		            tBranchTypes = AgentPubFun.switchSalChnl(
		                    "03", "01", "01");
		        }
		        LCPolSchema tLCPolSchema = new LCPolSchema();
		        tLCPolSchema = this.queryLCPol(tPolNo);
		        if (mBranchType.equals("5") && mBranchType2.equals("01")) {
		            tBranchTypes = AgentPubFun.switchSalChnl(
		            		tLCPolSchema.getSaleChnl(), "", "");
		        }
		        mLACommisionSchema.setBranchType(tBranchTypes[0]);
		        mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
		        mLACommisionSchema.setBranchType3(tBranchTypes[2]);
		        String tConfDate = mLACommisionSchema.getTMakeDate();//个险不用根据CONFDATE计算
//                tConfDate = this.queryConfDate(tLJAPayPersonSchema.getGrpContNo(),"ZC",tLJAPayPersonSchema.getMakeDate());
		        //--查询回访成功数据
		        
                String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
                String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
                String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                String tTransState = mLACommisionSchema.getTransState();
                // add new 
                String tAgentcode =mLACommisionSchema.getAgentCode() ;
                
		        String caldate = this.calCalDateForAll(tBranchTypes[0],
		                                               tBranchTypes[1],
		                                               tLJAPayPersonSchema.getMakeDate(),
		                                               tLJAPayPersonSchema.getMakeDate(),
		                                               String.valueOf( mLACommisionSchema.getPayCount()),
		                                               mLACommisionSchema.getTMakeDate(),"",
		                                               mLACommisionSchema.getCValiDate(),"02",
		                                               tConfDate,AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode );

		        if (caldate != null && !caldate.equals("")) {
		            String sql =
		                    "select yearmonth from LAStatSegment where startdate<='" +
		                    caldate + "' and enddate>='" + caldate +
		                    "' and stattype='5' ";
		            ExeSQL tExeSQL = new ExeSQL();
		            String tWageNo = tExeSQL.getOneValue(sql);

		            mLACommisionSchema.setWageNo(tWageNo);
		            mLACommisionSchema.setCalDate(caldate);
		        }
		        if (!tLJAPayPersonSchema.getAgentCode().equals("0000000000")) {
		            mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode()); //直辖组
		            tAgentCode = mLACommisionSchema.getAgentCode().trim();
		            tBranchCode = mLAAgentSchema.getBranchCode(); //职级对应机构代码
		            if (tBranchCode == null) {
		                return false;
		            }
		            LABranchGroupSchema tLABranchGroupSchema;
		            tLABranchGroupSchema = new LABranchGroupSchema();
		            tLABranchGroupSchema = querytLABranchGroup(tBranchCode);
		            mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
		                                               getBranchSeries());
		            tBranchAttr = tLABranchGroupSchema.getBranchAttr();
		            mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
		            mLACommisionSchema.setBranchAttr(tBranchAttr);
		        } else { //卡单个险中介
		            mLACommisionSchema.setAgentGroup("0000000000");
		            mLACommisionSchema.setBranchSeries("0000000000");
		            mLACommisionSchema.setBranchCode("0000000000");
		            mLACommisionSchema.setBranchAttr("0000000000");
		            mLACommisionSchema.setBranchType2("01");
		        }
		        this.mLACommisionSet.add(mLACommisionSchema);
        	}
        }
        return true;
    }

    //减少被保人
    //减少被保人申请（以受理日为准）在生效日期前或生效日后15日内的的退补佣金
    //境外旅行系列险种，投保方式为单次的，办理解约后，扣回本单发生的佣金
    //减少被保人都应该参加佣金计算,不管佣金是否计算commdire='1'
    private boolean dealCutInsruedNo(SSRS tSSRS) {
        String tBranchType = "2"; //团险撤单险种的渠道
        String tBranchType2 = "01";
        double tTransMoney1 = 0; //需要计算计算提奖
        double tTransMoney2 = 0; //不需要计算提奖，比例〉=25%
        double tTransMoney3 = 0; //不需要提奖，导入保费
        double tSumFee = 0;     //管理费
        String tLastGrpPolNo = ""; //记录上一张grppolno，用于两条数据比较
        String tLastEndorsementNo = ""; //记录上一张endorsementno，用于两条数据比较
        String tFlag = "0"; //判断 money2是否为空
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        LJAGetEndorseSchema tLastLJAGetEndorseSchema = new LJAGetEndorseSchema(); //记录上一张LJAGetEndorseSchema，用于向后面传前一条数据
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

            tFlag = "0"; //每次循环,都置为0
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setEndorsementNo(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setPolNo(tSSRS.GetText(i, 7));
            tLJAGetEndorseDB.setOtherNo(tSSRS.GetText(i, 8));
            tLJAGetEndorseDB.setOtherNoType(tSSRS.GetText(i, 9));
            tLJAGetEndorseDB.setDutyCode(tSSRS.GetText(i, 10));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 11));
            tLJAGetEndorseDB.getInfo();
            tLJAGetEndorseSchema = tLJAGetEndorseDB.getSchema();
            //计算管理费
            String tManageFee=getmanagefee(tLJAGetEndorseSchema.getPolNo(),
                                           tLJAGetEndorseSchema.getEndorsementNo(),tLJAGetEndorseSchema.getOtherNoType());
            if(tManageFee==null)
                return false ;
            double tFee=Double.parseDouble(tManageFee);


            //判断此减人件 是否已经提过数 ,如果提过,则不再提取
            //提数回退时 不回退上个月的数据,所以会出现此种情况

            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setGrpPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            wLACommisionDB.setTransType("ZT");
            wLACommisionDB.setTMakeDate(tLJAGetEndorseSchema.getMakeDate());
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }
            //判断grppolno是否与上一个grpolno相同，如果不相同，表示进入下一个grppolno ，则要插入扎账表
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            String tEndorsementNo=tLJAGetEndorseSchema.getEndorsementNo();
            if (tGrpPolNo != null && !tGrpPolNo.equals("00000000000000000000") &&
                !tGrpPolNo.equals("")) {
                //判断是否为团险提数数,不是则返回
                String GrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
                String tGrpContNo = tLJAGetEndorseSchema.getGrpContNo();
                LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
                LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
                tLCGrpPolSchema = queryLCGrpPol(GrpPolNo);
                if (tLCGrpPolSchema == null) {
                    continue;
                }
                tLCGrpContSchema = queryLCGrpCont(tGrpContNo);
                if (tLCGrpContSchema == null) {
                    continue;
                }
                if ((!tGrpPolNo.equals(tLastGrpPolNo)||!tEndorsementNo.equals(tLastEndorsementNo)) && i != 1) {
                    //处理非导入保费,需要计算fyc
                    if (tTransMoney1 != 0) {
                        double tMoney = tTransMoney1;
                        int a = dealcutCommision(tLastLJAGetEndorseSchema,
                                                 tMoney,tSumFee, "1"); //插入扎账表
                        if (a == 1) {
                            continue;
                        } else if (a == 2) {
                            return false;
                        }

                    }
                    //处理非导入保费,不计算fyc
                    if (tTransMoney2 != 0) {
                        double tMoney = tTransMoney2;
                        int a = dealcutCommision(tLastLJAGetEndorseSchema,
                                                 tMoney,tSumFee, "2"); //插入扎账表
                        if (a == 1) {
                            continue;
                        } else if (a == 2) {
                            return false;
                        }
                    }
                    //处理导入保费,不计算fyc
                    if (tTransMoney3 != 0) {
                        double tMoney = tTransMoney3;
                        int a = dealcutCommision(tLastLJAGetEndorseSchema,
                                                 tMoney,tSumFee, "3"); //插入扎账表
                        if (a == 1) {
                            continue;
                        } else if (a == 2) {
                            return false;
                        }
                    }
                    tTransMoney1 = 0; //值0，从新计算下一grppolno
                    tTransMoney2 = 0; //值0，从新计算下一grppolno
                    tTransMoney3 = 0; //值0，从新计算下一grppolno
                    tSumFee=0;
                    tLastLJAGetEndorseSchema=new LJAGetEndorseSchema();
                }

                tLastGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo(); //不能把此代码放在tGrpPolNo前面
                tLastEndorsementNo=tLJAGetEndorseSchema.getEndorsementNo();
                //tLastLJAGetEndorseSchema = tLJAGetEndorseSchema;
                tLastLJAGetEndorseSchema.setSchema(tLJAGetEndorseSchema);
                tSumFee = Arith.add(tSumFee,tFee);

                //判断是否为导入保费
                LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
                tLPDiskImportDB.setGrpContNo(tLJAGetEndorseSchema.getGrpContNo());
                tLPDiskImportDB.setInsuredNo(tLJAGetEndorseSchema.getInsuredNo());
                tLPDiskImportDB.setEdorNo(tLJAGetEndorseSchema.getEndorsementNo());
                tLPDiskImportDB.setEdorType("ZT");
                LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
                tLPDiskImportSet = tLPDiskImportDB.query();
                if (tLPDiskImportSet != null && tLPDiskImportSet.size() >= 1) {
                    LPDiskImportSchema tLPDiskImportSchema = new
                            LPDiskImportSchema();
                    tLPDiskImportSchema = tLPDiskImportSet.get(1);
                    String tMoney2 = tLPDiskImportSchema.getMoney2();
                    //LPDiskImportSet不为空 并且 money2 不为空，为按比例录入
                    if (tMoney2 != null && !tMoney2.equals("")) {
                        //money2不为空
                        tTransMoney3 = Arith.add(tTransMoney3,
                                                 tLJAGetEndorseSchema.
                                                 getGetMoney());
                    } else {
                        //money2为空
                        tFlag = "1";
                    }
                }
                if (tLPDiskImportSet == null || tLPDiskImportSet.size() <= 0 ||
                    tFlag.equals("1")) {
                    //LPDiskImportSet为空或 money2 为空，为按比例录入lpedorespecialdata
                    LPEdorEspecialDataDB tLPEdorEspecialDataDB = new
                            LPEdorEspecialDataDB();
                    tLPEdorEspecialDataDB.setEdorNo(tLJAGetEndorseSchema.
                            getEndorsementNo());
                    tLPEdorEspecialDataDB.setEdorType("ZT");
                    tLPEdorEspecialDataDB.setDetailType("FEERATE");
                    LPEdorEspecialDataSet tLPEdorEspecialDataSet = new
                            LPEdorEspecialDataSet();
                    tLPEdorEspecialDataSet = tLPEdorEspecialDataDB.query();
                    System.out.println(tLPEdorEspecialDataSet.size());
                    String tEdorValue = "";
                    if (tLPEdorEspecialDataSet == null ||
                        tLPEdorEspecialDataSet.size() <= 0) {
                        tEdorValue = "0.25"; //没有数据,默认为0.25,保全一期没有数据,销售按0.25算
                    } else {
                        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new
                                LPEdorEspecialDataSchema();
                        tLPEdorEspecialDataSchema = tLPEdorEspecialDataSet.get(
                                1);

                        tEdorValue = tLPEdorEspecialDataSchema.getEdorValue();
                        if (tEdorValue == null || tEdorValue.equals("")) {
                            CError tError = new CError();
                            tError.moduleName = "AgentWageCalSaveNewBL";
                            tError.functionName = "dealCutInsruedNo";
                            tError.errorMessage = "保全特殊处理表中的比例不能为空!";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }
                    if (tEdorValue.compareTo("0.25") >= 0) {
                        tTransMoney2 = Arith.add(tTransMoney2,
                                                 tLJAGetEndorseSchema.
                                                 getGetMoney());
                    } else {
                        tTransMoney1 = Arith.add(tTransMoney1,
                                                 tLJAGetEndorseSchema.
                                                 getGetMoney());
                    }
                }
            }
        }

        //如果是最后一条数据
        if (tLastLJAGetEndorseSchema!=null) {
            //处理非导入保费,需要计算fyc
            if (tTransMoney1 != 0) {
                double tMoney = tTransMoney1;
                int a = dealcutCommision(tLastLJAGetEndorseSchema, tMoney,tSumFee,
                                         "1"); //插入扎账表
                if (a == 1) {

                } else if (a == 2) {
                    return false;
                }
            }
            //处理非导入保费,不计算fyc
            if (tTransMoney2 != 0) {
                double tMoney = tTransMoney2;
                int a = dealcutCommision(tLastLJAGetEndorseSchema, tMoney,tSumFee,
                                         "2"); //插入扎账表
                if (a == 1) {

                } else if (a == 2) {
                    return false;
                }
            }
            //处理导入保费,不计算fyc
            if (tTransMoney3 != 0) {
                double tMoney = tTransMoney3;
                int a = dealcutCommision(tLastLJAGetEndorseSchema, tMoney,tSumFee,
                                         "3"); //插入扎账表
                if (a == 1) {

                } else if (a == 2) {
                    return false;
                }
            }
            tTransMoney1 = 0; //值0，从新计算下一grppolno
            tTransMoney2 = 0; //值0，从新计算下一grppolno
            tTransMoney3 = 0; //值0，从新计算下一grppolno
        }

        return true;
    }

    //tFYCFlag :   "1" 表示算fyc,"2"表示 不算fyc,非导入保费  , "3"  导入保费,不要算fyc
    //tMoney   :   退费(加和)
    // return  1  continue
    //         2  错误
    //         3  正常
    private int dealcutCommision(LJAGetEndorseSchema tLJAGetEndorseSchema,
                                 double tMoney,double tManageFee, String tFYCFlag) {
        String tAgentCode = tLJAGetEndorseSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        tLAAgentDB.getInfo();
        mLAAgentSchema = tLAAgentDB.getSchema();
        String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
        if (tGrpPolNo != null && !tGrpPolNo.equals("00000000000000000000") &&
            !tGrpPolNo.equals("")) {
            String GrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            String tGrpContNo = tLJAGetEndorseSchema.getGrpContNo();
            LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
            LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
            tLCGrpPolSchema = queryLCGrpPol(GrpPolNo);
            if (tLCGrpPolSchema == null) {
                return 1;
            }
            tLCGrpContSchema = queryLCGrpCont(tGrpContNo);
            if (tLCGrpContSchema == null) {
                return 1;
            }
            //续保的处理
            LCRnewStateLogSchema tLCRnewStateLogSchema = new
                    LCRnewStateLogSchema();
            tLCRnewStateLogSchema = this.queryLCRnewGrpPol(GrpPolNo);
            if (tLCRnewStateLogSchema == null) {
                tRenewCount = 0;
            } else {

//            	if (tRiskCode.equals("320106")){
//            		tRenewCount = 0;
//            	}
//            	else 
                tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }
            String COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
            //计算交费年度=保单生效日到交至日期
            int PayYear = 0;
            //填充直接佣金明细表（佣金扎账表）
            mLACommisionSchema = new LACommisionSchema();
            mLACommisionSchema.setCommisionSN(COMMISIONSN);
            mLACommisionSchema.setCommisionBaseNo("0000000000");
            mLACommisionSchema.setReNewCount(tRenewCount);
            //WageNo 783
            mLACommisionSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            mLACommisionSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            mLACommisionSchema.setContNo(tLJAGetEndorseSchema.getGrpContNo());
            mLACommisionSchema.setPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            mLACommisionSchema.setMainPolNo(tLCGrpPolSchema.getGrpPolNo()); //新加的 主险单号 用于继续率的计算
            mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); // 新加的 批单号码
            mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.
                                            getManageCom());
            mLACommisionSchema.setAppntNo(tLCGrpContSchema.getAppntNo());
            mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
            mLACommisionSchema.setRiskVersion(tLCGrpPolSchema.
                                              getRiskVersion());
            mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
            mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.
                                              getPayPlanCode());
            mLACommisionSchema.setCValiDate(tLCGrpPolSchema.getCValiDate());
            mLACommisionSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            mLACommisionSchema.setPayMode(tLCGrpPolSchema.getPayMode());
            mSSRS1 = queryInsuYear(tLCGrpPolSchema.getGrpPolNo(),tLCGrpPolSchema.getRiskCode());
            mLACommisionSchema.setF4(mSSRS1.GetText(1, 1));
            mLACommisionSchema.setF5(mSSRS1.GetText(1, 2));
            mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.
                                            getActuGetNo());
            mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
            mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.
                                                getEnterAccDate());
            mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.
                                            getGetConfirmDate());
            mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.
                                            getMakeDate());
            // CalcDate
            mLACommisionSchema.setCommDate(CurrentDate);
            mLACommisionSchema.setTransMoney(
                    0 - java.lang.Math.abs(tMoney));
            mLACommisionSchema.setTransState("00"); //正常保单
            mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                                                  getTransMoney());
            mLACommisionSchema.setLastPayToDate(tLCGrpPolSchema.getPaytoDate());
            mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                                               getGetDate());
            mLACommisionSchema.setTransType("ZT");
            mLACommisionSchema.setP7(tManageFee);

            if(tLCGrpPolSchema.getSaleChnl().equals("07"))
            {
                mLACommisionSchema.setF1("02");//职团开拓
                mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
            }
            else
            {
                mLACommisionSchema.setF1("01");
            }


            mLACommisionSchema.setFlag("0"); //退一部分件
            mLACommisionSchema.setPayYear(PayYear);
            mLACommisionSchema.setPayYears(getPayYears(tLJAGetEndorseSchema.getGrpPolNo()));
            //mLACommisionSchema.setYears(tLCGrpPolSchema.getYears());
            mLACommisionSchema.setPayCount(1);
            mLACommisionSchema.setSignDate(tLCGrpContSchema.getSignDate());
            mLACommisionSchema.setGetPolDate(tLCGrpContSchema.getGetPolDate());
            //BranchType 788
            mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
            mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                                            getAgentType());
            mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                                            getAgentCode());

            String tPolType = tLCGrpPolSchema.getStandbyFlag2();
            mLACommisionSchema.setPolType(tPolType == null ||
                                          tPolType.equals("") ?
                                          "1" : tPolType);

            mLACommisionSchema.setP1(tLCGrpPolSchema.getManageFeeRate());

            mLACommisionSchema.setP11(tLCGrpPolSchema.getGrpName());
            //mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
            //mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
            mLACommisionSchema.setP14(tLCGrpPolSchema.getPrtNo()); //cg add 2004-01-13
            mLACommisionSchema.setP15(tLCGrpPolSchema.getGrpProposalNo());
            mLACommisionSchema.setMakePolDate(tLCGrpPolSchema.getMakeDate()); //交单日期
            mLACommisionSchema.setCustomGetPolDate(tLCGrpContSchema.
                    getCustomGetPolDate()); //签字日期
            //  riskmark
            String tScanDate = this.getScanDate(tLCGrpPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
            mLACommisionSchema.setScanDate(tScanDate);
            mLACommisionSchema.setOperator(mGlobalInput.Operator);
            mLACommisionSchema.setMakeDate(CurrentDate);
            mLACommisionSchema.setMakeTime(CurrentTime);
            mLACommisionSchema.setModifyDate(CurrentDate);
            mLACommisionSchema.setModifyTime(CurrentTime);
            mLACommisionSchema.setMarketType(tLCGrpContSchema.getMarketType());
            mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode());
            tAgentCode = mLACommisionSchema.getAgentCode().trim();
            String tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
            if (tAgentGroup == null) {
                return 2;
            }
            LABranchGroupSchema tLABranchGroupSchema;
            tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);
            mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                                               getBranchSeries());

            String tActType = "";
          //添加对于互动中介的处理
            if (tLCGrpPolSchema.getSaleChnl().equals("15")) 
            {
            	tActType = checkActive(mLACommisionSchema);
            	if("".equals(tActType))
            	{
            		return 1;
            	}
            }
            String[] tBranchTypes = AgentPubFun.switchSalChnl(
                    tLCGrpPolSchema.
                    getSaleChnl(), tLCGrpPolSchema.getSaleChnlDetail(),
                    tActType);

            String tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
            if (tBranchTypes == null) {
                return 2;
            }
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tBranchCode);
            tLABranchGroupDB.getInfo();
            String tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
            if (tBranchAttr == null) {
                return 2;
            }
            mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                                               getBranchSeries());
            mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
            mLACommisionSchema.setBranchAttr(tBranchAttr);
            mLACommisionSchema.setBranchType(tBranchTypes[0]);
            mLACommisionSchema.setBranchType2(tBranchTypes[1]);
            mLACommisionSchema.setBranchType3(tBranchTypes[2]);
                     //添加卡折结算单的套餐名称 现在只是计算个险人员销售 个销团渠道 结算单 团单
             if(mLAAgentSchema.getBranchType() != null && !mLAAgentSchema.getBranchType().trim().equals("")
                && mLAAgentSchema.getBranchType2() != null && !mLAAgentSchema.getBranchType2().trim().equals("")){
             if(mLAAgentSchema.getBranchType().equals("1")
            		 //添加互动渠道,健管渠道
            		 ||mLAAgentSchema.getBranchType().equals("5")||mLAAgentSchema.getBranchType().equals("7")||mLAAgentSchema.getBranchType().equals("6")||mLAAgentSchema.getBranchType().equals("8")){              
               if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
               {
                  String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                          +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                  ExeSQL tExeSQL = new ExeSQL();
                  String swarpcode = tExeSQL.getOneValue(warpsql);
                   System.out.println(warpsql+" a  "+swarpcode);
                  mLACommisionSchema.setF3(swarpcode);
               }
               else
               {
                  mLACommisionSchema.setF3("");
                   System.out.println("B");
               }
             }
//             if(mLAAgentSchema.getBranchType().equals("2") && mLAAgentSchema.getBranchType2().equals("01"))
//             {
//             	  String wrapsql = "select distinct riskwrapcode from lccontplanrisk "
//             	               + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"' ";
//             	  ExeSQL tExe = new ExeSQL();
//             	  String swrapcode = tExe.getOneValue(wrapsql);
//             	  if(swrapcode==null){
//             	  mLACommisionSchema.setF3("");
//             	  }
//             	  else 
//             	  {
//             	  mLACommisionSchema.setF3(swrapcode);
//                }
//             }
//           团险渠道(包含团险直销与团险中介)
             if(mLAAgentSchema.getBranchType().equals("2")){
                        	 if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                             {
                                String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                                        +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                                ExeSQL tExeSQL = new ExeSQL();
                                String swarpcode = tExeSQL.getOneValue(warpsql);
                                 System.out.println(warpsql+" a  "+swarpcode);
                                mLACommisionSchema.setF3(swarpcode);
                             }
                             else
                             {
                                mLACommisionSchema.setF3("");
                                 System.out.println("B");
                             }
                        }
            }
            String mon = mLACommisionSchema.getGetPolDate();

            //先根据回单日期置CalDate, WAGENO
            String getPolDate = mLACommisionSchema.getGetPolDate();
            String custPolDate = mLACommisionSchema.getCustomGetPolDate();
            String tconfDate = "";
            tconfDate = this.queryConfDate(tLCGrpPolSchema.getGrpContNo(),"ZT",tLJAGetEndorseSchema.
                    getMakeDate(),tLJAGetEndorseSchema.
                    getGetConfirmDate(),tLJAGetEndorseSchema.
                    getActuGetNo());
            //--tGrpContNo
	        //--查询回访成功数据
            String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
            String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
            String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
            String tTranState = mLACommisionSchema.getTransState();
//            if(!("03").equals(tTranState))
//            {
//            	tTranState="01";   // 函数判断
//            }
            // add new 
            String tAgentcode =mLACommisionSchema.getAgentCode() ;
            
            String caldate = this.calCalDateForAll(tBranchTypes[0],
                    tBranchTypes[1],
                    mLACommisionSchema.getSignDate(),
                    mLACommisionSchema.getGetPolDate(), "0",
                    tLJAGetEndorseSchema.
                    getMakeDate(), mLACommisionSchema.getCustomGetPolDate(),
                    mLACommisionSchema.getCValiDate(),"02",
                    tconfDate, AFlag,this.mManageCom,tVisitTime ,tRiskFlag,tTranState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);
            mLACommisionSchema.setCalDate(caldate);
            String tSignDate = mLACommisionSchema.getSignDate();
            if (tFYCFlag.equals("1")) {
                mLACommisionSchema.setFlag("0"); //减人，退一部分件,要退业务员钱
                mLACommisionSchema.setCommDire("1");
            } else if (tFYCFlag.equals("2")) {
                mLACommisionSchema.setFlag("3"); //减人，退一部分件,不退业务员提钱
                mLACommisionSchema.setCommDire("1");
                mLACommisionSchema.setF2("01");//不计算FYC
            } else if (tFYCFlag.equals("3")) {
                mLACommisionSchema.setFlag("4"); //减人，导入保费
                mLACommisionSchema.setCommDire("1");
                mLACommisionSchema.setF2("01");//不计算FYC
            }
            if (!caldate.equals("")) {
                int z = judgeWageIsCal(GrpPolNo, tSignDate);
                if (z == 0) {
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveBL";
                    tError.functionName = "dealcutCommision";
                    tError.errorMessage = "减人提奖时在扎账表中没有查询到正常的数据信息！";
                    this.mErrors.addOneError(tError);
                    return 2;
                } else {
                    String sql =
                            "select yearmonth from LAStatSegment where startdate<='" +
                            caldate + "' and enddate>='" + caldate +
                            "' and stattype='91' ";
                    ExeSQL tExeSQL = new ExeSQL();
                    String tWageNo = tExeSQL.getOneValue(sql);
                    mLACommisionSchema.setTConfDate(tconfDate);
                    mLACommisionSchema.setWageNo(tWageNo);
                    mLACommisionSchema.setCalDate(caldate);
                }
            }
            mLACommisionSet.add(mLACommisionSchema);
        }

        return 3;
    }
    //处理无名单增人
    private boolean dealNoteLessAdd(SSRS tSSRS) {
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        LJAGetEndorseSchema tLastLJAGetEndorseSchema = new LJAGetEndorseSchema(); //记录上一张LJAGetEndorseSchema，用于向后面传前一条数据
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setEndorsementNo(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setPolNo(tSSRS.GetText(i, 7));
            tLJAGetEndorseDB.setOtherNo(tSSRS.GetText(i, 8));
            tLJAGetEndorseDB.setOtherNoType(tSSRS.GetText(i, 9));
            tLJAGetEndorseDB.setDutyCode(tSSRS.GetText(i, 10));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 11));
            tLJAGetEndorseDB.getInfo();
            tLJAGetEndorseSchema = tLJAGetEndorseDB.getSchema();
            //计算管理费
            String tManageFee=getmanagefee(tLJAGetEndorseSchema.getPolNo(),
                                           tLJAGetEndorseSchema.getEndorsementNo(),tLJAGetEndorseSchema.getOtherNoType());
            if(tManageFee==null)
                return false ;
            double tFee=Double.parseDouble(tManageFee);
            //判断此减人件 是否已经提过数 ,如果提过,则不再提取
            //提数回退时 不回退上个月的数据,所以会出现此种情况

            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setGrpPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            wLACommisionDB.setTransType("WZ");
            wLACommisionDB.setTMakeDate(tLJAGetEndorseSchema.
                                        getMakeDate());
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            if (tGrpPolNo != null &&
                !tGrpPolNo.equals("00000000000000000000") &&
                !tGrpPolNo.equals("")) {
                int tReturn=dealLACommision(tLJAGetEndorseSchema, tLJAGetEndorseSchema.getGetMoney(),tFee,0,"13");
                if(tReturn==1)
                {
                    continue;
                }
                else if(tReturn==2)
                {
                     return false;
                }
            }

        }
        return true;
    }
    //处理无名单减
    private boolean dealNoteLessCut(SSRS tSSRS) {
        LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
        //LJAGetEndorseSchema tLastLJAGetEndorseSchema = new LJAGetEndorseSchema(); //记录上一张LJAGetEndorseSchema，用于向后面传前一条数据
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
            tLJAGetEndorseDB.setActuGetNo(tSSRS.GetText(i, 1));
            tLJAGetEndorseDB.setEndorsementNo(tSSRS.GetText(i, 2));
            tLJAGetEndorseDB.setFeeOperationType(tSSRS.GetText(i, 3));
            tLJAGetEndorseDB.setFeeFinaType(tSSRS.GetText(i, 4));
            tLJAGetEndorseDB.setPolNo(tSSRS.GetText(i, 7));
            tLJAGetEndorseDB.setOtherNo(tSSRS.GetText(i, 8));
            tLJAGetEndorseDB.setOtherNoType(tSSRS.GetText(i, 9));
            tLJAGetEndorseDB.setDutyCode(tSSRS.GetText(i, 10));
            tLJAGetEndorseDB.setPayPlanCode(tSSRS.GetText(i, 11));
            tLJAGetEndorseDB.getInfo();
            tLJAGetEndorseSchema = tLJAGetEndorseDB.getSchema();
            //计算管理费
            String tManageFee=getmanagefee(tLJAGetEndorseSchema.getPolNo(),
                                           tLJAGetEndorseSchema.getEndorsementNo(),tLJAGetEndorseSchema.getOtherNoType());
            if(tManageFee==null)
                return false ;
            double tFee=Double.parseDouble(tManageFee);


            //判断此减人件 是否已经提过数 ,如果提过,则不再提取
            //提数回退时 不回退上个月的数据,所以会出现此种情况

            LACommisionDB wLACommisionDB = new LACommisionDB();
            wLACommisionDB.setGrpPolNo(tLJAGetEndorseSchema.getGrpPolNo());
            wLACommisionDB.setTransType("WZ");
            wLACommisionDB.setTMakeDate(tLJAGetEndorseSchema.
                                        getMakeDate());
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionSet = wLACommisionDB.query();
            if (tLACommisionSet.size() > 0) { //如果提过数了,则返回,不再提取
                continue;
            }
            String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
            if (tGrpPolNo != null &&
                !tGrpPolNo.equals("00000000000000000000") &&
                !tGrpPolNo.equals("")) {
                int tReturn=dealLACommision(tLJAGetEndorseSchema, tLJAGetEndorseSchema.getGetMoney(),tFee,0,"14");
                if(tReturn==1)
                {
                    continue;
                }
                else if(tReturn==2)
                {
                     return false;
                }
            }

        }
        return true;
    }
    /**处理团险lacommision
     * cflag
     * 01 正常(还没用到)
     * 02 团单增加保费项目(目前除去增人和无增两个项目,此两个项目已经单独处理)
     * 13 无名单增人
     * 14 无名单减人
     * 2017-3-22 yangyang  添加一个参数，利息 （需求:3290）
     */

    private int dealLACommision(LJAGetEndorseSchema cLJAGetEndorseSchema, double cMoney,double cManageFee,double cAccuralFee,String cFlag)
    {
        String tAgentCode = cLJAGetEndorseSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        tLAAgentDB.getInfo();
        mLAAgentSchema = tLAAgentDB.getSchema();
        String tGrpPolNo = cLJAGetEndorseSchema.getGrpPolNo();
        if (tGrpPolNo != null && !tGrpPolNo.equals("00000000000000000000") &&
            !tGrpPolNo.equals("")) {
            String GrpPolNo = cLJAGetEndorseSchema.getGrpPolNo();
            String tGrpContNo = cLJAGetEndorseSchema.getGrpContNo();
            LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
            LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
            tLCGrpPolSchema = queryLCGrpPol(GrpPolNo);
            if (tLCGrpPolSchema == null) {
                return 1;
            }
            tLCGrpContSchema = queryLCGrpCont(tGrpContNo);
            if (tLCGrpContSchema == null) {
                return 1;
            }
            //续保的处理
            LCRnewStateLogSchema tLCRnewStateLogSchema = new
                    LCRnewStateLogSchema();
            tLCRnewStateLogSchema = this.queryLCRnewGrpPol(GrpPolNo);
            if (tLCRnewStateLogSchema == null) {
                tRenewCount = 0;
            } else {

//            	if (tRiskCode.equals("320106")){
//            		tRenewCount = 0;
//            	}
//            	else 
                tRenewCount = tLCRnewStateLogSchema.getRenewCount();
                }
            String COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
            //计算交费年度=保单生效日到交至日期
            int PayYear = 0;
            //填充直接佣金明细表（佣金扎账表）
            mLACommisionSchema = new LACommisionSchema();
            mLACommisionSchema.setCommisionSN(COMMISIONSN);
            mLACommisionSchema.setCommisionBaseNo("0000000000");
            mLACommisionSchema.setReNewCount(tRenewCount);
	        mLACommisionSchema.setP7(cManageFee);
	        //存储利息
	        mLACommisionSchema.setP8(cAccuralFee);
            //WageNo 783
            mLACommisionSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            mLACommisionSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            mLACommisionSchema.setContNo(cLJAGetEndorseSchema.getGrpContNo());
            mLACommisionSchema.setPolNo(cLJAGetEndorseSchema.getGrpPolNo());
            mLACommisionSchema.setMainPolNo(tLCGrpPolSchema.getGrpPolNo()); //新加的 主险单号 用于继续率的计算
            mLACommisionSchema.setEndorsementNo(cLJAGetEndorseSchema.getEndorsementNo()); //新加的 批单号码
            mLACommisionSchema.setManageCom(cLJAGetEndorseSchema.
                                            getManageCom());
            mLACommisionSchema.setAppntNo(tLCGrpContSchema.getAppntNo());
            mLACommisionSchema.setRiskCode(cLJAGetEndorseSchema.getRiskCode());
            mLACommisionSchema.setRiskVersion(tLCGrpPolSchema.
                                              getRiskVersion());
            mLACommisionSchema.setDutyCode(cLJAGetEndorseSchema.getDutyCode());
            mLACommisionSchema.setPayPlanCode(cLJAGetEndorseSchema.
                                              getPayPlanCode());
            mLACommisionSchema.setCValiDate(tLCGrpPolSchema.getCValiDate());
            mLACommisionSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            mLACommisionSchema.setPayMode(tLCGrpPolSchema.getPayMode());
            mSSRS1 = queryInsuYear(tLCGrpPolSchema.getGrpPolNo(),tLCGrpPolSchema.getRiskCode());
            mLACommisionSchema.setF4(mSSRS1.GetText(1, 1));
            mLACommisionSchema.setF5(mSSRS1.GetText(1, 2));
            mLACommisionSchema.setReceiptNo(cLJAGetEndorseSchema.
                                            getActuGetNo());
            mLACommisionSchema.setTPayDate(cLJAGetEndorseSchema.getGetDate());
            mLACommisionSchema.setTEnterAccDate(cLJAGetEndorseSchema.
                                                getEnterAccDate());
            mLACommisionSchema.setTConfDate(cLJAGetEndorseSchema.
                                            getGetConfirmDate());
            mLACommisionSchema.setTMakeDate(cLJAGetEndorseSchema.
                                            getMakeDate());

            //添加卡折结算单的套餐名称 现在只是计算个险人员销售 个销团渠道 结算单 团单
            if(mLAAgentSchema.getBranchType() != null && !mLAAgentSchema.getBranchType().trim().equals("")
                && mLAAgentSchema.getBranchType2() != null && !mLAAgentSchema.getBranchType2().trim().equals("")){
              if(mLAAgentSchema.getBranchType().equals("1")
            		  //添加互动渠道,健管渠道
             		 ||mLAAgentSchema.getBranchType().equals("5")||mLAAgentSchema.getBranchType().equals("7")||mLAAgentSchema.getBranchType().equals("6")||mLAAgentSchema.getBranchType().equals("8")){              
                if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                {
                   String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                           +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                   ExeSQL tExeSQL = new ExeSQL();
                   String swarpcode = tExeSQL.getOneValue(warpsql);
                    System.out.println(warpsql+" a  "+swarpcode);
                   mLACommisionSchema.setF3(swarpcode);
                }
                else
                {
                   mLACommisionSchema.setF3("");
                    System.out.println("B");
                }
              }
//              if(mLAAgentSchema.getBranchType().equals("2") && mLAAgentSchema.getBranchType2().equals("01"))
//              {
//              	  String wrapsql = "select distinct riskwrapcode from lccontplanrisk "
//              	               + " where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"' ";
//              	  ExeSQL tExe = new ExeSQL();
//              	  String swrapcode = tExe.getOneValue(wrapsql);
//              	  if(swrapcode==null){
//              	  mLACommisionSchema.setF3("");
//              	  }
//              	  else 
//              	  {
//              	  mLACommisionSchema.setF3(swrapcode);
//                 }
//              }
//            团险渠道(包含团险直销与团险中介)
              if(mLAAgentSchema.getBranchType().equals("2")){
                         	 if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") &&tLCGrpContSchema.getCardFlag().equals("2"))
                              {
                                 String warpsql = "select distinct riskwrapcode from lccontplanrisk "
                                         +"where grpcontno='" + tLCGrpContSchema.getGrpContNo() + "' and riskcode='" + tLCGrpPolSchema.getRiskCode()+"'  ";
                                 ExeSQL tExeSQL = new ExeSQL();
                                 String swarpcode = tExeSQL.getOneValue(warpsql);
                                  System.out.println(warpsql+" a  "+swarpcode);
                                 mLACommisionSchema.setF3(swarpcode);
                              }
                              else
                              {
                                 mLACommisionSchema.setF3("");
                                  System.out.println("B");
                              }
                         }
            }
            // CalcDate
            mLACommisionSchema.setCommDate(CurrentDate);
            mLACommisionSchema.setTransMoney(cMoney);
            mLACommisionSchema.setTransState("00"); //正常保单
            mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                                                  getTransMoney());
            mLACommisionSchema.setLastPayToDate(tLCGrpPolSchema.getPaytoDate());
            mLACommisionSchema.setCurPayToDate(cLJAGetEndorseSchema.
                                               getGetDate());
            mLACommisionSchema.setCommDire("1");
            mLACommisionSchema.setTransType("ZC");
            mLACommisionSchema.setFlag("2"); //正常
            if (cFlag.equals("01")) {
                //正常
                mLACommisionSchema.setTransType("ZC");
                mLACommisionSchema.setFlag("2"); //正常
            }
            else if (cFlag.equals("02"))
            {
                //团单增加保费保全项目
                mLACommisionSchema.setTransType(cLJAGetEndorseSchema.getFeeOperationType());
                mLACommisionSchema.setFlag("2"); //正常
            }
            else if (cFlag.equals("13")) {
                //无名单增人
                mLACommisionSchema.setTransType("WZ");
                mLACommisionSchema.setFlag("2"); //正常
            } else if (cFlag.equals("14")) {
                //无名单减人
                mLACommisionSchema.setTransType("WJ");
                mLACommisionSchema.setFlag("0"); //退一部分件
            }
            else if (cFlag.equals("15"))
            {
                //团单增加保费保全项目(计算提奖)
                mLACommisionSchema.setTransType(cLJAGetEndorseSchema.getFeeOperationType());
                mLACommisionSchema.setFlag("0"); //退一部分件
            }
            else if (cFlag.equals("16"))
            {
                //团单减少保费保全项目(不扣提奖)
                mLACommisionSchema.setTransType(cLJAGetEndorseSchema.getFeeOperationType());
                mLACommisionSchema.setFlag("0");//退一部分件
                mLACommisionSchema.setCommDire("1");//
                mLACommisionSchema.setF2("01");//不计算FYC，表示AgentWageCalDoNewBL计算提奖时不再计算此数据
            }
            else if (cFlag.equals("02"))
            {
                //团单增加保费保全项目
                mLACommisionSchema.setTransType(cLJAGetEndorseSchema.getFeeOperationType());
                mLACommisionSchema.setFlag("0"); //正常
            }
            if(tLCGrpPolSchema.getSaleChnl().equals("07"))
            {
                mLACommisionSchema.setF1("02");//职团开拓
            }
            else
            {
                mLACommisionSchema.setF1("01");
            }
            mLACommisionSchema.setPayYear(PayYear);
            mLACommisionSchema.setPayYears(getPayYears(cLJAGetEndorseSchema.getGrpPolNo()));
            //mLACommisionSchema.setYears(tLCGrpPolSchema.getYears());
            mLACommisionSchema.setPayCount(1);
            mLACommisionSchema.setSignDate(tLCGrpContSchema.getSignDate());
            mLACommisionSchema.setGetPolDate(tLCGrpContSchema.getGetPolDate());
            mLACommisionSchema.setAgentCom(cLJAGetEndorseSchema.getAgentCom());
            mLACommisionSchema.setAgentType(cLJAGetEndorseSchema.
                                            getAgentType());
            mLACommisionSchema.setAgentCode(cLJAGetEndorseSchema.
                                            getAgentCode());

            String tPolType = tLCGrpPolSchema.getStandbyFlag2();
            mLACommisionSchema.setPolType(tPolType == null ||
                                          tPolType.equals("") ?
                                          "1" : tPolType);

            mLACommisionSchema.setP1(tLCGrpPolSchema.getManageFeeRate());
            mLACommisionSchema.setP11(tLCGrpPolSchema.getGrpName());
            //mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
            //mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
            mLACommisionSchema.setP14(tLCGrpPolSchema.getPrtNo()); //cg add 2004-01-13
            mLACommisionSchema.setP15(tLCGrpPolSchema.getGrpProposalNo());
            mLACommisionSchema.setMakePolDate(tLCGrpPolSchema.getMakeDate()); //交单日期
            mLACommisionSchema.setCustomGetPolDate(tLCGrpContSchema.
                    getCustomGetPolDate()); //签字日期
            //  riskmark
            String tScanDate = this.getScanDate(tLCGrpPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
            mLACommisionSchema.setScanDate(tScanDate);
            mLACommisionSchema.setOperator(mGlobalInput.Operator);
            mLACommisionSchema.setMakeDate(CurrentDate);
            mLACommisionSchema.setMakeTime(CurrentTime);
            mLACommisionSchema.setModifyDate(CurrentDate);
            mLACommisionSchema.setModifyTime(CurrentTime);
            mLACommisionSchema.setMarketType(tLCGrpContSchema.getMarketType());
            mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode());
            tAgentCode = mLACommisionSchema.getAgentCode().trim();
            String tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
            if (tAgentGroup == null) {
                return 2;
            }
            LABranchGroupSchema tLABranchGroupSchema;
            tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);
            mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                                               getBranchSeries());

            String tActType = "";
          //添加对于互动中介的处理
            if (tLCGrpPolSchema.getSaleChnl().equals("15")) 
            {
            	tActType = checkActive(mLACommisionSchema);
            	if("".equals(tActType))
            	{
            		return 1;
            	}
            }
            String[] tBranchTypes = AgentPubFun.switchSalChnl(
                    tLCGrpPolSchema.
                    getSaleChnl(), tLCGrpPolSchema.getSaleChnlDetail(),
                    tActType);

            String tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
            if (tBranchTypes == null) {
                return 2;
            }
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tBranchCode);
            tLABranchGroupDB.getInfo();
            String tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
            if (tBranchAttr == null) {
                return 2;
            }
            mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                                               getBranchSeries());
            mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
            mLACommisionSchema.setBranchAttr(tBranchAttr);
            mLACommisionSchema.setBranchType(tBranchTypes[0]);
            mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
            mLACommisionSchema.setBranchType3(tBranchTypes[2]);
            String mon = mLACommisionSchema.getGetPolDate();
            //先根据回单日期置CalDate, WAGENO
            String getPolDate = mLACommisionSchema.getGetPolDate(); //xiangchun 2005-12-01团险根据回执日期判断年月
            String custPolDate = mLACommisionSchema.getCustomGetPolDate();
            String tConfDate = "";
            tConfDate = this.queryConfDate(tLCGrpPolSchema.getGrpContNo(),
            		                       cLJAGetEndorseSchema.getFeeOperationType(),cLJAGetEndorseSchema.
                                           getMakeDate(),cLJAGetEndorseSchema.
                                           getGetConfirmDate(),cLJAGetEndorseSchema.
                                           getActuGetNo());
            //--tGrpContNo
	        //--查询回访成功数据
            String tVisitTime = this.queryReturnVisit(tGrpContNo);//--查询回访日期
            String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
            String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
            String tTransState = mLACommisionSchema.getTransState();
            // add new 
            String tAgentcode =mLACommisionSchema.getAgentCode() ;
            
            String caldate = this.calCalDateForAll(tBranchTypes[0],
                    tBranchTypes[1],
                    mLACommisionSchema.getSignDate(),
                    mLACommisionSchema.getGetPolDate(), "0",
                    cLJAGetEndorseSchema.
                    getMakeDate(), mLACommisionSchema.getCustomGetPolDate(),
                    mLACommisionSchema.getCValiDate(),"02",
                    tConfDate, AFlag ,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);

            mLACommisionSchema.setCalDate(caldate);
            String tSignDate = mLACommisionSchema.getSignDate();
            if (cFlag.equals("13")||cFlag.equals("14")||cFlag.equals("15")||cFlag.equals("16")) {
                if (!caldate.equals("")) {
                    int z = judgeWageIsCal(GrpPolNo, tSignDate);
                    if (z == 0) {
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalSaveBL";
                        tError.functionName = "dealcutCommision";
                        tError.errorMessage = "提奖时在扎账表中没有查询到正常的数据信息！";
                        this.mErrors.addOneError(tError);
                        return 2;
                    }
                }
            }
            String sql =
                    "select yearmonth from LAStatSegment where startdate<='" +
                    caldate + "' and enddate>='" + caldate +
                    "' and stattype='91' ";
            ExeSQL tExeSQL = new ExeSQL();
            String tWageNo = tExeSQL.getOneValue(sql);
            mLACommisionSchema.setTConfDate(tConfDate);
            mLACommisionSchema.setWageNo(tWageNo);
            mLACommisionSchema.setCalDate(caldate);
            mLACommisionSet.add(mLACommisionSchema);
        }
        return 3;
    }

    private String queryChargeDate(String cPolNo, String cAgentCom,String cReceiptNo) {
        String tChargeDate = "";
        StringBuffer tSQL = new StringBuffer();
        //直接选择修改日期
        tSQL.append("select modifydate from LACharge ");
        tSQL.append("where PolNo = '" + cPolNo + "' ");
        tSQL.append("And AgentCom = '" + cAgentCom + "' ");
        tSQL.append("And ReceiptNo = '" + cReceiptNo + "' ");
        tSQL.append(" And ChargeState='2'  with ur");
        ExeSQL tExeSQL = new ExeSQL();
        tChargeDate = tExeSQL.getOneValue(tSQL.toString());
        if (tChargeDate.indexOf(" ") != -1) {
            //由于oracle数据库的日期类型，会存放时间，因此需要将时间部分信息剔除掉
        	tChargeDate = tChargeDate.substring(0, tChargeDate.indexOf("-1"));
        }
        if (tExeSQL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBL";
            tError.functionName = "queryChargeDate";
            tError.errorMessage = "查询手续费的审核日期出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tChargeDate;
    }


    private String getGrpCurPaytoDate(String cGrpPolNo) {
        String tSql = "", tReturn = "";
        ExeSQL tExeSQL = new ExeSQL();
        tSql =
                "SELECT Max(CurPaytoDate) FROM LJAPayPerson "
                + "WHERE GrpPolNo = '" + cGrpPolNo + "' with ur ";
        tReturn = tExeSQL.getOneValue(tSql);
        if (tExeSQL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBL";
            tError.functionName = "getGrpCurPaytoDate";
            tError.errorMessage = "查询团单" + cGrpPolNo + "的现交至日期出错！！";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (tReturn == null || tReturn.equals("")) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBL";
            tError.functionName = "getGrpCurPaytoDate";
            tError.errorMessage = "查询团单" + cGrpPolNo + "的现交至日期为空！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tReturn;
    }

    private SSRS queryLJAPayGrp() {
        SSRS tSSRS = new SSRS();
        //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
        //GB 是共保反冲数据
        String sqlStr =
                "select GrpPolNo,PayNo,PayType from LJAPayGrp where payType in ( 'ZC','GB') ";
        sqlStr += " and MakeDate='" + mLAWageLogSchema.getEndDate() +
                "' and manageCom = '" + mManageCom + "'";
        sqlStr += " order by AgentGroup,AgentCode ,PayType with ur ";
        System.out.println("sqlStr:" + sqlStr + "|");

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryLJAPayGrp";
            tError.errorMessage = "团体交费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }

    //查询管理费
    private String getCManageFee(String tgetGrpPolNo, String tgetPayNo,
                                 String tgetPayType,LJAPayGrpSchema tLJAPayGrpSchema) { //先算契约管理费 ，如果契约为0 ，则算增人管理费  团单契约：b.incometype='1'  保全：b.incometype='3'
        String tMoney = "0";
        // 如果是增人
        if(tLJAPayGrpSchema.getGetNoticeNo()!=null && tLJAPayGrpSchema.getGetNoticeNo()!=""
        		&& tLJAPayGrpSchema.getEndorsementNo()!=null&&tLJAPayGrpSchema.getEndorsementNo()!="")
           {
               String sqlStr =
                       " select sum(value(c.fee,0)) from lcinsureaccclassfee c where otherno = polno and polno in "
                       +
                       "(select b.polno from  ljagetendorse b where   "
                       + "    endorsementno='" +
                       tLJAPayGrpSchema.getEndorsementNo() +
                       "'  and  feeoperationtype ='NI'  and grppolno='" +
                       tgetGrpPolNo + "') with ur ";
               ExeSQL aExeSQL = new ExeSQL();
               tMoney = aExeSQL.getOneValue(sqlStr);
               System.out.println("sqlStr:" + sqlStr + "|");
           }
           else
           {
               //如果是新单
               String sqlStr =
                       "select sum(value(fee,0)) from lcinsureaccfee where polno in (select polno from LJAPayPerson where";
               sqlStr += " grppolno='" + tgetGrpPolNo +
                      "' and PayNo='" +
                       tgetPayNo + "' and PayType='" + tgetPayType 
                       + "' and exists (select 1 from ljapay where payno =ljapayperson.payno and incometype = '1'))"
                       +" with ur" ;
               System.out.println("sqlStr:" + sqlStr + "|");

               ExeSQL aExeSQL = new ExeSQL();
               tMoney = aExeSQL.getOneValue(sqlStr);
           }
        if(tMoney==null || tMoney.equals(""))
        {
            tMoney="0";
        }
        return tMoney;
    }

    //个险犹豫期撤单
    private SSRS queryLJAGetEndorse() {
        SSRS tSSRS = new SSRS();
        //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
        String
        sqlStr = "select ActuGetNo,EndorsementNo,FeeOperationType,FeeFinaType,PolNo,OtherNo,OtherNoType,DutyCode,PayPlanCode"
               +" from LJAGetEndorse "
               +" where FeeOperationType = 'WT' and FeeFinaType = 'TF'"
               +" and rtrim(grppolno)='00000000000000000000' "
               +" and MakeDate='" + mLAWageLogSchema.getEndDate()
               +"' and ManageCom = '" + mManageCom + "' with ur ";
        System.out.println("sqlStr:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryLJAGetEndorse";
            tError.errorMessage = "批改补退费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }

    //个险犹豫期撤单保全回退
       private SSRS queryLJAGetRBPerson() {
           SSRS tSSRS = new SSRS();
           //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
           String
           sqlStr = "select ActuGetNo,EndorsementNo,FeeOperationType,FeeFinaType,PolNo,OtherNo,OtherNoType,DutyCode,PayPlanCode,ContNo"
                  +" from LJAGetEndorse "
                  +" where feeoperationtype='RB' and feefinatype='BF'"
                 // +" and grppolno='00000000000000000000' "
                  +" and MakeDate='" + mLAWageLogSchema.getEndDate()
                  +"' and ManageCom = '" + mManageCom + "' with ur ";
           System.out.println("sqlStr:" + sqlStr);
           ExeSQL tExeSQL = new ExeSQL();
           tSSRS = tExeSQL.execSQL(sqlStr);
           if (tExeSQL.mErrors.needDealError() == true) {
               // @@错误处理
               this.mErrors.copyAllErrors(tExeSQL.mErrors);
               CError tError = new CError();
               tError.moduleName = "AgentWageCalSaveNewBL";
               tError.functionName = "queryLJAGetEndorse";
               tError.errorMessage = "批改补退费表查询失败!";
               this.mErrors.addOneError(tError);
               return null;
           }
           return tSSRS;
       }


//    //团险犹豫期撤单
    private SSRS queryGrpLJAGetEndorse() {
        SSRS tSSRS = new SSRS();
        //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
        String
        sqlStr = "select distinct GRPPolNo,PayPlanCode,FeeOperationType,FeeFinaType "
               +" from LJAGetEndorse where FeeOperationType = 'WT' and FeeFinaType = 'TF'"
               +" and rtrim(grppolno)<>'00000000000000000000' "
               +" and MakeDate='" + mLAWageLogSchema.getEndDate()
               +"' and ManageCom = '" + mManageCom + "' with ur ";
        System.out.println("sqlStr:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryGrpLJAGetEndorse";
            tError.errorMessage = "批改补退费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }

    //团险减少被保人
    private SSRS queryCutLJAGetEndorse() {
        SSRS tSSRS = new SSRS();
        //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
        String sqlStr = "select actugetno,endorsementno,FeeOperationType,FeeFinaType,GrpContNO,GrpPolNo,PolNo,OtherNo,OtherNoType,DutyCode,PayPlanCode from LJAGetEndorse where FeeOperationType = 'ZT' and FeeFinaType = 'TF'";
        sqlStr += " and rtrim(grppolno)<>'00000000000000000000' and MakeDate='" + mLAWageLogSchema.getEndDate() +
                "' and ManageCom = '" + mManageCom + "'  order by  GrpPolNo with ur"; //必须先按GrpPolNo排序

        System.out.println("sqlStr:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryGrpLJAGetEndorse";
            tError.errorMessage = "批改补退费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }
    //团险无名单减人
    private SSRS queryNotelessCut() {
        SSRS tSSRS = new SSRS();
        //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
        String sqlStr = "select actugetno,endorsementno,FeeOperationType,FeeFinaType,GrpContNO,GrpPolNo,PolNo,OtherNo,OtherNoType,DutyCode,PayPlanCode from LJAGetEndorse where FeeOperationType = 'WJ' and FeeFinaType = 'TF'";
        sqlStr += " and rtrim(grppolno)<>'00000000000000000000' and MakeDate='" + mLAWageLogSchema.getEndDate() +
                "' and ManageCom = '" + mManageCom + "'  order by  GrpPolNo with ur"; //必须先按GrpPolNo排序

        System.out.println("sqlStr:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryGrpLJAGetEndorse";
            tError.errorMessage = "批改补退费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }
    //团险无名单增人
    private SSRS queryNotelessAdd() {
        SSRS tSSRS = new SSRS();
        //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
        String sqlStr = "select actugetno,endorsementno,FeeOperationType,FeeFinaType,GrpContNO,GrpPolNo,PolNo,OtherNo,OtherNoType,DutyCode,PayPlanCode from LJAGetEndorse where FeeOperationType = 'WZ' and FeeFinaType = 'BF'";
        sqlStr += " and rtrim(grppolno)<>'00000000000000000000' and MakeDate='" + mLAWageLogSchema.getEndDate() +
                "' and ManageCom = '" + mManageCom + "'  order by  GrpPolNo with ur"; //必须先按GrpPolNo排序

        System.out.println("sqlStr:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryGrpLJAGetEndorse";
            tError.errorMessage = "批改补退费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }
    //团险增加被保人
     private SSRS queryAddPrem()
     {
         SSRS tSSRS = new SSRS();
         //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
         String sqlStr = "select  distinct GRPPolNo,PayPlanCode,FeeOperationType,FeeFinaType,makedate from LJAGetEndorse  where  " +
         		"FeeFinaType in (select code from ldcode where codetype='feefinatype2') and " +
         		" FeeOperationType  not in ('WZ','NI')";
         sqlStr += " and rtrim(grppolno)<>'00000000000000000000' and MakeDate='" + mLAWageLogSchema.getEndDate() +
                "' and ManageCom = '" + mManageCom + "'  order by  GrpPolNo with ur"; //必须先按GrpPolNo排序
         System.out.println("sqlStr:" + sqlStr);
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sqlStr);
         if (tExeSQL.mErrors.needDealError() == true)
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tExeSQL.mErrors);
             CError tError = new CError();
             tError.moduleName = "AgentWageCalSaveNewBL";
             tError.functionName = "queryAddPrem";
             tError.errorMessage = "批改补退费表查询失败!";
             this.mErrors.addOneError(tError);
             return null;
         }
         return tSSRS;
     }
     //保全减少保费 （扣除业务员提奖)
     private SSRS querycutPrem1()
     {
         SSRS tSSRS = new SSRS();
         //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
         String sqlStr = "select  distinct GRPPolNo,PayPlanCode,FeeOperationType,FeeFinaType,makedate from LJAGetEndorse " +
         		" where  FeeOperationType  in (select code from ldcode where codetype='feeoperationtype1')" +
         		" and FeeFinaType in (select code from ldcode where codetype='feefinatype1') ";
         sqlStr += " and rtrim(grppolno)<>'00000000000000000000' and MakeDate='" + mLAWageLogSchema.getEndDate() +
                "' and ManageCom = '" + mManageCom + "'  order by  GrpPolNo with ur "; //必须先按GrpPolNo排序
         System.out.println("sqlStr:" + sqlStr);
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sqlStr);
         if (tExeSQL.mErrors.needDealError() == true)
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tExeSQL.mErrors);
             CError tError = new CError();
             tError.moduleName = "AgentWageCalSaveNewBL";
             tError.functionName = "querycutPrem1";
             tError.errorMessage = "批改补退费表查询失败!";
             this.mErrors.addOneError(tError);
             return null;
         }
         return tSSRS;
     }
     //保全减少保费(不扣提奖)
     private SSRS querycutPrem2()
     {
         SSRS tSSRS = new SSRS();
         //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
         String sqlStr = "select  distinct GRPPolNo,PayPlanCode,FeeOperationType,FeeFinaType,makedate from LJAGetEndorse " +
         		" where  FeeOperationType  in (select code from ldcode where codetype='feeoperationtype2') " +
         		" and FeeFinaType in (select code from ldcode where codetype='feefinatype1') ";
         sqlStr += " and rtrim(grppolno)<>'00000000000000000000' and MakeDate='" + mLAWageLogSchema.getEndDate() +
                "' and ManageCom = '" + mManageCom + "'  order by  GrpPolNo with ur "; //必须先按GrpPolNo排序
         System.out.println("sqlStr:" + sqlStr);
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(sqlStr);
         if (tExeSQL.mErrors.needDealError() == true)
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tExeSQL.mErrors);
             CError tError = new CError();
             tError.moduleName = "AgentWageCalSaveNewBL";
             tError.functionName = "querycutPrem2";
             tError.errorMessage = "批改补退费表查询失败!";
             this.mErrors.addOneError(tError);
             return null;
         }
         return tSSRS;
     }

    //境外旅游撤单件 ,degreetype=0一次的才退佣,险种代码描述在LAWageCalElement ,caltype='91'中
    private SSRS queryRiskLJAGetEndorse(String tBranchType, String tBranchType2) {
        String sqlStr =
                "select distinct GRPPolNo,PayPlanCode,FeeOperationType,makedate " +
                "  from LJAGetEndorse where FeeOperationType='CT' " +
                " and MakeDate='" + mLAWageLogSchema.getEndDate() +
                "' and ManageCom = '" + mManageCom +
                "' and  riskcode in (select a.riskcode from LAWageCalElement a " +
                " where a.caltype='91' and a.branchtype='" + tBranchType +
                "' and a.branchtype2='" + tBranchType2 + "') with ur ";
        System.out.println("sqlStr:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryRiskLJAGetEndorse";
            tError.errorMessage = "批改补退费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;

    }

//  查询保险期间
    private SSRS queryInsuYear(String tGrpPolNo, String tRiskCode) {
        String sqlStr =
                "select distinct insuyear,insuyearflag  " 
                +"  from lcpol  where grppolno='"+tGrpPolNo+"' " 
                +" and riskcode='" + tRiskCode +"' "
                +" union select distinct insuyear,insuyearflag "
                +" from lbpol where grppolno='"+tGrpPolNo+"' " 
                +" and riskcode='" + tRiskCode +"' "
                +" fetch first 1 rows only with ur ";
        System.out.println("sqlStr:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryInsuYear";
            tError.errorMessage = "查询保险期间失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;

    }
    
    
    /**
     *
     * @param tPrtNo String
     * @return String
     */

    public String getScanDate(String tPrtNo) {
        String sql = "";
        String scanDate = "";
        ExeSQL tExeSQL = new ExeSQL();
//        sql = "select input_date from es_doc_main where   doc_code='" + tPrtNo + "' ";
//        sql = "select MakeDate from es_doc_relation,es_doc_main where BussNoType = '11'  and es_doc_relation.DocID = es_doc_main.DocID and BussNo = '" +
//              tPrtNo.trim() + "'";
        //20061107
        sql = "select MakeDate from es_doc_main where   doccode='" + tPrtNo +
              "' and subtype in ('TB01','TB02','TB04','TB06','TB28','TB29')  with ur";
        scanDate = tExeSQL.getOneValue(sql);
        System.out.println(scanDate);
        if (scanDate == null || scanDate.equals("")) {
            return null;
        }
        scanDate = AgentPubFun.formatDate(scanDate, "yyyy-MM-dd");
        return scanDate;
    }
    /**
     * songguohui
     * 查询收费的财务日期
     * 如果是保全收费则取实收表的财务确认日期，如果是保全付费则是付费表的makedate
     * 定期结算的时间是按照最终结算时的财务日期
     * 输出：如果查询时发生错误则返回null,否则返回tconfdate
     */
    private String  queryConfDate(String tGrpContNo,String tTransType,String tMakeDate,String tConfDate,String tPayNo) {
        String mconfdate = tConfDate;
        String flag  = "";
        AFlag = "1";//表示付费
        ExeSQL tExeSQL = new ExeSQL();
        if(tGrpContNo!=null&&!tGrpContNo.equals("00000000000000000000")
        		&&!tGrpContNo.equals(""))//个单不做处理
        {
	        if(tTransType.equals("ZC")||tTransType.equals("ZT"))
	        {
	        	//首先判断是不是保全付费的情况
	        	String getSQL="select distinct paymode from ljaget where actugetno in ("
	        		+"select actugetno from ljagetendorse where grpcontno='"+tGrpContNo
	        		+"' and actugetno='"+tPayNo+"' and endorsementno is not null) with ur";
	        	String tPayMode = tExeSQL.getOneValue(getSQL);
           		if(tPayMode!=null&&!tPayMode.equals("")){
           			if("B".equals(tPayMode))//定期结算中的白条
            		{
            			 mconfdate = "";
            			 AFlag = "0";
            		}
            		else if("J".equals(tPayMode))//已经定期结算
            		{
            			String ttSQL="select actuno,makedate from LJAEdorBalDetail where btactuno='" +tPayNo+"' with ur";
            			SSRS tSSRS=new SSRS();
            			tSSRS=tExeSQL.execSQL(ttSQL);
            			if(tSSRS.getMaxRow()>0){
                    			mconfdate=tSSRS.GetText(1,2);
                				System.out.println("财务确认日期"+mconfdate);
            			}else{
            					mconfdate="";
            			}
//            			String SQL = "select confdate,MAX(INComeno) from ljapay where getnoticeno in " +
//    	        		" (select distinct actuno from ljaedorbaldetail where btactuno in " +
//    	        		" (select distinct actugetno from ljagetendorse where grpcontno in" +
//    	        		" (select grpcontno from LCGrpBalPlan where GrpContno = '"+tGrpContNo+"'))) " +
//    	        		" GROUP BY ConfDate,INComeno " +
//    	        		" order by confdate desc with ur ";
//    			          mconfdate = tExeSQL.getOneValue(SQL);
    			          AFlag = "0";
            		}
           		}else{
//           		首先判断是契约收费还是保全收费
                	String endorseSQL = "select distinct endorsementno from ljapaygrp where grpcontno='"+tGrpContNo+"'" +
                			" and makedate='"+tMakeDate+"' and payno = '"+tPayNo+"' with ur";
                	String endorsementno = tExeSQL.getOneValue(endorseSQL);
                	if(endorsementno!=null&&!endorsementno.equals(""))//为空的时候表示是契约收费,不为空表示是保全的工单号
                	{
                		String incometypeSQL = "select distinct incometype from ljapay where incomeno='"+endorsementno+"'" +
                				" with ur ";
                		String   tIncometype = tExeSQL.getOneValue(incometypeSQL);
                		if(tIncometype!=null&&!tIncometype.equals("")){
                			if("B".equals(tIncometype))//定期结算中的白条
                    		{
                    			 mconfdate = "";
                    			 AFlag = "0";
                    		}
                    		else if("J".equals(tIncometype))//已经定期结算
                    		{
                    			String tSQL="select incometype,confdate from ljapay where getnoticeno in (" +
                    					"select actuno from LJAEdorBalDetail where btactuno='" +tPayNo+"') with ur";
                    			SSRS tSSRS=new SSRS();
                    			tSSRS=tExeSQL.execSQL(tSQL);
                    			if(tSSRS.getMaxRow()>0){
                    				if(tSSRS.GetText(1,1).equals("13")){
                    					mconfdate=tSSRS.GetText(1,2);
                        				System.out.println("财务确认日期"+mconfdate);
                    				}else{
                    					mconfdate="";
                    				}

                    			}else{
                    				tSQL="select othernotype,confdate from ljaget where actugetno in (" +
                					"select actuno from LJAEdorBalDetail where btactuno='" +tPayNo+"') with ur";
                    				tSSRS=tExeSQL.execSQL(tSQL);
                    				if(tSSRS.getMaxRow()>0&&tSSRS.GetText(1,1).equals("13")
                    						&&tSSRS.GetText(1,2)!=null
                    						&&!tSSRS.GetText(1,2).equals("")){
                        				mconfdate=tSSRS.GetText(1,2);
                        				System.out.println("财务确认日期"+mconfdate);
                        			}else{
                        				mconfdate = "";
                        			}

                    			}
//                    			String SQL = "select confdate,MAX(INComeno) from ljapay where getnoticeno in " +
//            	        		" (select distinct actuno from ljaedorbaldetail where btactuno in " +
//            	        		" (select distinct actugetno from ljagetendorse where grpcontno in" +
//            	        		" (select grpcontno from LCGrpBalPlan where GrpContno = '"+tGrpContNo+"'))) " +
//            	        		" GROUP BY ConfDate,INComeno " +
//            	        		" order by confdate desc with ur ";
//            			          mconfdate = tExeSQL.getOneValue(SQL);
            			          AFlag = "0";

                    		}
                		}
                   	 }
           		}
	        }
	        else
	        {
	        	   String AddSQL =  "select max(GETCONFIRMDATE) from ljagetendorse where  " +
	           		" FeeFinaType in (select code from ldcode where codetype='feefinatype2')" +
	           		" and grpcontno = '"+tGrpContNo+"' and feeoperationtype='"+tTransType+"' and  makedate='"+tMakeDate+"' with ur";
	        	   String maxConfdate = tExeSQL.getOneValue(AddSQL);
		           if(maxConfdate!=null&&!maxConfdate.equals(""))//表示是保全加费
		           {
		        	   mconfdate = maxConfdate;
		        	   AFlag = "0";
		           }
		           else //付费
		           {
		        	   AFlag = "1";
		           }
	        }
        }
        return mconfdate;

    }

    /**
     * 查询需要提取的个险保单
     * @return SSRS
     */
    private SSRS queryLJAPayPerson() {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        //只含个险和银代
        //length(trim(dutycode))<>10 or substr(dutycode,7,1)<>'1'   这两个条件是排除增额缴清的缴肥记录
        String
        sqlStr = "select PolNo,PayNo,DutyCode,PayPlanCode,PayType,RiskCode "
               +" from LJAPayPerson "
               +" where (length(trim(dutycode))<>10 or substr(dutycode,7,1)<>'1') "
               +" and payType = 'ZC' and  MakeDate='"+ mLAWageLogSchema.getEndDate() + "'"
               +" and manageCom = '" + mManageCom + "' "
               +" And rtrim(GrpPolNo) = '00000000000000000000'  "
               +" and not exists (select '1' from lccont where contno = ljapayperson.contno and salechnl='21')";
        sqlStr += " order by AgentGroup,AgentCode with ur" ;
        System.out.println("queryLJAPayPerson查询需要提取个单:" + sqlStr);
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryLJAPayPerson";
            tError.errorMessage = "实收个人交费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }


   /** 现在没有确定类型  payType = ？？？？
     * 查询个险万能先追加保费
     * @return SSRS
     */
    private SSRS queryOmniLJAPayPerson() {
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        //只含个险和银代
        //length(trim(dutycode))<>10 or substr(dutycode,7,1)<>'1'   这两个条件是排除增额缴清的缴肥记录
        String
        sqlStr = "select PolNo,PayNo,DutyCode,PayPlanCode,PayType,RiskCode "
               +" from LJAPayPerson "
               +" where (length(trim(dutycode))<>10 or substr(dutycode,7,1)<>'1') "
               +" and payType = 'ZB' and  MakeDate='"+ mLAWageLogSchema.getEndDate() + "'"
               +" and manageCom = '" + mManageCom + "' ";
        //增加对于互动渠道 的处理，   yangyang   2015-2-3
//        if(mBranchType.equals("5") && mBranchType2.equals("01"))
//        {
//        	sqlStr+=" and riskcode in ('331801','332701','331501','330801','331701','332001','333901','331601','334701','334801','370201') ";
//        }
//        else
//        {
//        	sqlStr+=" and riskcode in ('330801','331801','332701','331201','331301','331601','331501','333901','334701','334801','370201') ";
//        }      //+" And GrpPolNo = '00000000000000000000'  "
        sqlStr+="  and riskcode in(select riskcode from lmriskapp where risktype4='4')";
        sqlStr += " order by AgentGroup,AgentCode with ur" ;
        System.out.println("queryLJAPayPerson查询需要提取个单:" + sqlStr);
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "queryLJAPayPerson";
            tError.errorMessage = "实收个人交费表查询失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tSSRS;
    }

    private String getIncomeType(String cPayno) {
        if (cPayno == null || cPayno.equals("")) {
            return "";
        }
        String tIncomeType = "";
        String tSQL = "select incometype from LJAPay where payno = '" + cPayno +
                      "' with ur ";
        ExeSQL tExeSQL = new ExeSQL();
        tIncomeType = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            return "";
        }
        return tIncomeType;
    }

    private String getIncomeNo(String cPayno) {
        if (cPayno == null || cPayno.equals("")) {
            return "";
        }
        String tIncomeNo = "";
        String tSQL = "select incomeno from LJAPay where payno = '" + cPayno +
                      "'  with ur ";
        ExeSQL tExeSQL = new ExeSQL();
        tIncomeNo = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            return "";
        }
        return tIncomeNo;
    }

//查询追加类型
    private String getTransType(String cEndorsementNo) {
           if (cEndorsementNo == null || cEndorsementNo.equals("")) {
               return "W";
           }
           String tIncomeNo = "";
           String tSQL = "select edortype From lpedoritem where edorno =(select innersource from lgwork where workno ='"
                         + cEndorsementNo + "')  with ur ";
           ExeSQL tExeSQL = new ExeSQL();
           tIncomeNo = tExeSQL.getOneValue(tSQL);
           if (tExeSQL.mErrors.needDealError()) {
               return "W";
           }
           return tIncomeNo;
    }

    private String getPayMode(String cPayno) {
        if (cPayno == null || cPayno.equals("")) {
            return "";
        }
        String tPayMode = "";
        String tSQL = "select PayMode from ljtempfeeclass where tempfeeno = '" +
                      cPayno +
                      "'  with ur";
        ExeSQL tExeSQL = new ExeSQL();
        tPayMode = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            return "";
        }
        return tPayMode;
    }

    private String getYears(String cPayno) {
        if (cPayno == null || cPayno.equals("")) {
            return "";
        }
        String tYears = "";
        String tSQL =
                "select polperiod from lmcertifydes where certifyclass='D'  and subcode = '" +
                cPayno +
                "'  with ur";
        ExeSQL tExeSQL = new ExeSQL();
        tYears = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            return "";
        }
        return tYears;
    }

    /**
     *判断所输入年月的佣金是否已算
     * 返回值 0: 出错 1：已算 2：未算
     */
    private int judgeWageIsCal(String cPolNo, String tSignDate) {
        ExeSQL tExeSQL = new ExeSQL();
        String sql =
                "select wageno,branchtype,agentcode from lacommision where polno='" +
                cPolNo + "' and TransType='ZC' " +
                " and commisionsn=(select max(commisionsn) from lacommision where polno='" +
                cPolNo + "' and TransType='ZC' ) with ur";
        System.out.println("judgeWageIsCal+sql:" + sql);
        SSRS tSSRS = tExeSQL.execSQL(sql);

        if (tSSRS.getMaxRow() <= 0) {
            int flag = 0;
            for (int k = 1; k <= mLACommisionSet.size(); k++) {
                if (mLACommisionSet.get(k).getPolNo().
                    equals(
                            mLACommisionSchema.
                            getPolNo())) {
                    flag = 1;
                    break; //  make :2005-10-17 xiangchun
                }
            }
            if (flag == 1) {
                return 2;
            } else {
                return 0;
            }
        }
        String wageno = tSSRS.GetText(1, 1);
        String tBranchType = tSSRS.GetText(1, 2);
        String tAgentCode = tSSRS.GetText(1, 3);
        if (wageno == null || wageno.equals("")) {
            return 2;
        }
        int tReturn = 0;
        String tSQL = "select MakeDate from lawage where "
                      + " IndexCalNo = '" + wageno + "'"
                      + " and agentcode = '" + tAgentCode + "' with ur " ;

        System.out.println(tSQL);
        String tMakeDate = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBL";
            tError.functionName = "judgeWageIsCal";
            tError.errorMessage = "查询指标信息表的佣金指标出错！";
            this.mErrors.addOneError(tError);
            return tReturn;
        }
        if (tMakeDate == null || tMakeDate.equals("")) {
            tReturn = 2;
        } else {
            tReturn = 1;
        }

        return tReturn;
    }

    /**
     * caigang
     * 查询集体保单表
     * 输出：如果查询时发生错误则返回null,否则返回Schema
     */
    private LCContSchema queryLCCont(String tContNo) {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        LCContSchema tLCContSchema = new LCContSchema();
        if (!tLCContDB.getInfo()) {
            LBContDB tLBContDB = new LBContDB();
            tLBContDB.setContNo(tContNo);
            if (!tLBContDB.getInfo()) {
                if (tLBContDB.mErrors.needDealError() == true) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLBContDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveNewBL";
                    tError.functionName = "queryLCCont";
                    tError.errorMessage = "集体保单备份表查询失败!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                return null;
            }
            Reflections tReflections = new Reflections();
            LBContSchema tLBContSchema = new LBContSchema();
            tLBContSchema = tLBContDB.getSchema();
            tReflections.transFields(tLCContSchema, tLBContSchema);
        } else {
            tLCContSchema = tLCContDB.getSchema();
        }
        String tSalChnl = tLCContSchema.getSaleChnl().trim();
        String tSalChnlDetail = tLCContSchema.getSaleChnlDetail();
        
        System.out.println("SaleChnl:" + tLCContSchema.getSaleChnl());
        System.out.println("SaleChnlDetail:" + tLCContSchema.getSaleChnlDetail());
        if (tSalChnl == null || tSalChnl.equals("")) {
            return null;
        }
        if (tSalChnlDetail == null || tSalChnlDetail.equals("")) {
            tSalChnlDetail = "01"; //2005-11-7   MODIFY:XIANGCHUN
        }
        System.out.println("mSaleChnl[0]:" + mSaleChnl[0]);
        System.out.println("mSaleChnl[1]:" + mSaleChnl[1]);
        System.out.println("mSaleChnl[1]:" + mSaleChnl[2]);
        
      //2014-12-26 modify yangyang
        String Crs_SaleChnl = tLCContSchema.getCrs_SaleChnl();
        String Crs_BussType = tLCContSchema.getCrs_BussType();
        
        if(tSalChnl.equals("14")&&Crs_SaleChnl!=null&&!Crs_SaleChnl.equals("")&&Crs_BussType.equals("01"))
        {
        	return null;
        }
        
        if(tSalChnl.equals("15"))
        {
        	if("".equals(Crs_SaleChnl)||null==Crs_SaleChnl)
        	{
        		return tLCContSchema;
        	}
        	else if(this.mSaleChnl[2].indexOf(Crs_BussType) != -1)
        	{
        		return tLCContSchema;
        	}
        	else
        	{
        		return null;
        	}
        }
        if(tSalChnl != null && "21".equals(tSalChnl))
        {
        	return null;
        }
    	if (this.mSaleChnl[0].indexOf(tSalChnl) != -1 &&
    			this.mSaleChnl[1].indexOf(tSalChnlDetail) != -1) {
    		//System.out.println("33333");
    		return tLCContSchema;
    		
    	} else {
    		return null;
    	}
    }


    /**
     * caigang
     * 查询集体保单表
     * 输出：如果查询时发生错误则返回null,否则返回Schema
     */
    private LCGrpContSchema queryLCGrpCont(String tGrpContNo) {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tGrpContNo);
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        if (!tLCGrpContDB.getInfo()) {
            LBGrpContDB tLBGrpContDB = new LBGrpContDB();
            tLBGrpContDB.setGrpContNo(tGrpContNo);
            if (!tLBGrpContDB.getInfo()) {
                if (tLBGrpContDB.mErrors.needDealError() == true) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLBGrpContDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveNewBL";
                    tError.functionName = "queryLCGrpCont";
                    tError.errorMessage = "集体保单备份表查询失败!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                return null;
            }
            Reflections tReflections = new Reflections();
            LBGrpContSchema tLBGrpContSchema = new LBGrpContSchema();
            tLBGrpContSchema = tLBGrpContDB.getSchema();
            tReflections.transFields(tLCGrpContSchema, tLBGrpContSchema);
        } else {
            tLCGrpContSchema = tLCGrpContDB.getSchema();
        }
        String tSalChnl = tLCGrpContSchema.getSaleChnl().trim();
        String tSalChnlDetail = tLCGrpContSchema.getSaleChnlDetail();
   
        System.out.println("SaleChnl:" + tLCGrpContSchema.getSaleChnl());
        System.out.println("SaleChnlDetail:" +tLCGrpContSchema.getSaleChnlDetail());
        if (tSalChnl == null || tSalChnl.equals("")) {
            return null;
        }
/*        //一卡通出单个险、互动、健管、电商 先不计提佣金 wqm-20181017
        if(tLCGrpContSchema.getCardFlag()!=null&&"cd".equals(tLCGrpContSchema.getCardFlag())){
        	if(tSalChnl.equals("1")||tSalChnl.equals("10")||tSalChnl.equals("14")||tSalChnl.equals("15")||tSalChnl.equals("17")||tSalChnl.equals("21")||tSalChnl.equals("12")){
        	return null;
        	}
        }*/
        if (tSalChnlDetail == null || tSalChnlDetail.equals("")) {
            tSalChnlDetail = "01"; //2005-11-7   MODIFY:XIANGCHUN
        }
        //2014-12-26 modify yangyang
      //2014-12-26 modify yangyang
        String Crs_SaleChnl = tLCGrpContSchema.getCrs_SaleChnl();
        String Crs_BussType = tLCGrpContSchema.getCrs_BussType();
        if(tSalChnl.equals("14")&&Crs_SaleChnl!=null&&!Crs_SaleChnl.equals("")&&Crs_BussType.equals("01"))
        {
        	return null;
        }
        if(tSalChnl.equals("15"))
        {
        	if("".equals(Crs_SaleChnl)||null==Crs_SaleChnl)
        	{
        		return tLCGrpContSchema;
        	}
        	else if(this.mSaleChnl[2].indexOf(Crs_BussType) != -1)
        	{
        		return tLCGrpContSchema;
        	}
        	else
        	{
        		return null;
        	}
        }
        if(tSalChnl != null && "21".equals(tSalChnl))
        {
        	return null;
        }
    	if (this.mSaleChnl[0].indexOf(tSalChnl) != -1 &&
                this.mSaleChnl[1].indexOf(tSalChnlDetail) != -1) {
                return tLCGrpContSchema;
        }
        else {
            return null;
        }
        
    }

    private LABranchGroupSchema querytLABranchGroup(String tAgentGroup) {
        String sqlStr = "select * from LABranchGroup where AgentGroup='" +
                        tAgentGroup + "' with ur ";
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sqlStr);
        if (tLABranchGroupDB.mErrors.needDealError() == true) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBL";
            tError.functionName = "queryLABranchGroup";
            tError.errorMessage = "查询展业机构表查询失败!";
            this.mErrors.addOneError(tError);
            tLABranchGroupSet.clear();
            return null;
        }
        if (tLABranchGroupSet.size() > 0) {
            tLABranchGroupSchema = tLABranchGroupSet.get(1);
            return tLABranchGroupSchema;
        } else {
            return null;
        }
    }

    /**
     * caigang
     * 查询集体保单表
     * 输出：如果查询时发生错误则返回null,否则返回Schema
     */
    private LCPolSchema queryLCPol(String tPolNo) {
       System.out.println("~~~~~~~~~打出来!!!!!!!!!");
    	LCPolDB tLCPolDB = new LCPolDB();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolDB.setPolNo(tPolNo);
        System.out.println("~~~~~~~~~~~出不来了?~~~~~~~~~~~~~");

        if (!tLCPolDB.getInfo()) {
            LBPolDB tLBPolDB = new LBPolDB();
            tLBPolDB.setPolNo(tPolNo);
            if (!tLBPolDB.getInfo()) {
                if (tLBPolDB.mErrors.needDealError() == true) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLBPolDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveNewBL";
                    tError.functionName = "queryLCPol";
                    tError.errorMessage = "集体保单备份表查询失败!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                return null;
            }
            System.out.println("~~~~~~~~~~~出不来了?~~~~~~~~~~~~~");

            LBPolSchema tLBPolSchema = new LBPolSchema();
            tLBPolSchema = tLBPolDB.getSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLCPolSchema, tLBPolSchema);
        } else {
            tLCPolSchema = tLCPolDB.getSchema();
        }
        System.out.println("SaleChnl:" + tLCPolSchema.getSaleChnl());
        System.out.println("SaleChnlDetail:" + tLCPolSchema.getSaleChnlDetail());
        String tSalChnl = tLCPolSchema.getSaleChnl().trim();
        if (tSalChnl == null || tSalChnl.equals("")) {
            return null;
        }
        String tSalChnlDetail = tLCPolSchema.getSaleChnlDetail();
        if (tSalChnlDetail == null || tSalChnlDetail.equals("")) {
            tSalChnlDetail = "01"; //2005-11-7   MODIFY:XIANGCHUN
        }

        if (this.mSaleChnl[0].indexOf(tSalChnl) != -1 &&
            this.mSaleChnl[1].indexOf(tSalChnlDetail) != -1) {
            return tLCPolSchema;

        } else {
            return null;
        }

    }

    /**
     * xiangchun
     * 查询续保新号与旧号
     * 输出：如果查询时发生错误则返回null,否则返回Schema
     */
    private LCRnewStateLogSchema queryLCRnewPol(String tPolNo) {
        //查询 C表
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
        LCRnewStateLogSet tLCRnewStateLogSet = new LCRnewStateLogSet();
        String tSql = " select * from  LCRnewStateLog  where  PolNo='" + tPolNo +
                      "' order by newpolno desc with ur ";
        tLCRnewStateLogSet = tLCRnewStateLogDB.executeQuery(tSql);
       System.out.println("sql:"+tSql);
        //查询备份表
        LBRnewStateLogSchema tLBRnewStateLogSchema = new LBRnewStateLogSchema();
        LBRnewStateLogDB tLBRnewStateLogDB = new LBRnewStateLogDB();
        LBRnewStateLogSet tLBRnewStateLogSet = new LBRnewStateLogSet();
       tSql = " select * from  LBRnewStateLog  where  PolNo='" + tPolNo +
               "' order by newpolno desc with ur";
        System.out.println("sql:"+tSql);
        tLBRnewStateLogSet = tLBRnewStateLogDB.executeQuery(tSql);
        if (tLCRnewStateLogSet.size() <= 0 && tLBRnewStateLogSet.size() <= 0) {
            return null;
        } else if (tLCRnewStateLogSet.size() >= 1 && tLBRnewStateLogSet.size() >= 1) {
            //如果 B 表和 C 表都有，则返回renewcount最大的
            tLCRnewStateLogSchema = tLCRnewStateLogSet.get(1);
            tLBRnewStateLogSchema = tLBRnewStateLogSet.get(1);

            if (tLCRnewStateLogSchema.getRenewCount() >=
                tLBRnewStateLogSchema.getRenewCount()) {
                return tLCRnewStateLogSchema;
            } else {
                tLCRnewStateLogSchema = new LCRnewStateLogSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLCRnewStateLogSchema,
                                         tLBRnewStateLogSchema);
                return tLCRnewStateLogSchema;
            }
        } else if (tLCRnewStateLogSet.size() >= 1 && tLBRnewStateLogSet.size() <= 0) 
        {
             tLCRnewStateLogSchema = tLCRnewStateLogSet.get(1);
            return tLCRnewStateLogSchema;
        } else {
            tLBRnewStateLogSchema = tLBRnewStateLogSet.get(1);
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLCRnewStateLogSchema, tLBRnewStateLogSchema);
            return tLCRnewStateLogSchema;
        }
    }
    /**
     * xiangchun
     * 查询续保新号与旧号
     * 输出：如果查询时发生错误则返回null,否则返回Schema
     */
    private LCRnewStateLogSchema queryLCRnewGrpPol(String cGrpPolNo) {
        //查询 C表
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
        LCRnewStateLogSet tLCRnewStateLogSet = new LCRnewStateLogSet();
        String tSql = " select * from  LCRnewStateLog  where  grppolno='" + cGrpPolNo +
                      "' order by newpolno desc with ur ";
        tLCRnewStateLogSet = tLCRnewStateLogDB.executeQuery(tSql);
       System.out.println("sql:"+tSql);
        //查询备份表
        LBRnewStateLogSchema tLBRnewStateLogSchema = new LBRnewStateLogSchema();
        LBRnewStateLogDB tLBRnewStateLogDB = new LBRnewStateLogDB();
        LBRnewStateLogSet tLBRnewStateLogSet = new LBRnewStateLogSet();
        tSql = " select * from  LBRnewStateLog  where  grppolno='" + cGrpPolNo +
               "' order by newpolno desc with ur";
        System.out.println("sql:"+tSql);
        tLBRnewStateLogSet = tLBRnewStateLogDB.executeQuery(tSql);
        if (tLCRnewStateLogSet.size() <= 0 && tLBRnewStateLogSet.size() <= 0) {
            return null;
        } else if (tLCRnewStateLogSet.size() >= 1 && tLBRnewStateLogSet.size() >= 1) {
            //如果 B 表和 C 表都有，则返回renewcount最大的
            tLCRnewStateLogSchema = tLCRnewStateLogSet.get(1);
            tLBRnewStateLogSchema = tLBRnewStateLogSet.get(1);

            if (tLCRnewStateLogSchema.getRenewCount() >=
                tLBRnewStateLogSchema.getRenewCount()) {
                return tLCRnewStateLogSchema;
            } else {
                tLCRnewStateLogSchema = new LCRnewStateLogSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLCRnewStateLogSchema,
                                         tLBRnewStateLogSchema);
                return tLCRnewStateLogSchema;
            }
        } else if (tLCRnewStateLogSet.size() >= 1 && tLBRnewStateLogSet.size() <= 0) {
             tLCRnewStateLogSchema = tLCRnewStateLogSet.get(1);
            return tLCRnewStateLogSchema;
        } else {
            tLBRnewStateLogSchema = tLBRnewStateLogSet.get(1);
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLCRnewStateLogSchema, tLBRnewStateLogSchema);
            return tLCRnewStateLogSchema;
        }
    }
    /**
     * caigang
     * 查询集体保单表
     * 输出：如果查询时发生错误则返回null,否则返回Schema
     */
    private LCGrpPolSchema queryLCGrpPol(String tGrpPolNo) {

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(tGrpPolNo);
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        if (!tLCGrpPolDB.getInfo()) {
            LBGrpPolDB tLBGrpPolDB = new LBGrpPolDB();
            tLBGrpPolDB.setGrpPolNo(tGrpPolNo);
            if (!tLBGrpPolDB.getInfo()) {
                if (tLBGrpPolDB.mErrors.needDealError() == true) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLBGrpPolDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveNewBL";
                    tError.functionName = "queryLCGrpPol";
                    tError.errorMessage = "集体保单备份表查询失败!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                return null;
            }

            LBGrpPolSchema tLBGrpPolSchema = new LBGrpPolSchema();
            tLBGrpPolSchema = tLBGrpPolDB.getSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLCGrpPolSchema, tLBGrpPolSchema);
        } else {
            tLCGrpPolSchema = tLCGrpPolDB.getSchema();
        }
        String tSalChnl = tLCGrpPolSchema.getSaleChnl().trim();
        String tSalChnlDetail = tLCGrpPolSchema.getSaleChnlDetail();
        System.out.println("SaleChnl:" + tLCGrpPolSchema.getSaleChnl());
        System.out.println("SaleChnlDetail:" +
                           tLCGrpPolSchema.getSaleChnlDetail());
        if (tSalChnl == null || tSalChnl.equals("")) {
            return null;
        }
        if (tSalChnlDetail == null || tSalChnlDetail.equals("")) {
            tSalChnlDetail = "01"; //2005-11-7   MODIFY:XIANGCHUN
        }

        if (this.mSaleChnl[0].indexOf(tSalChnl) != -1 &&
            this.mSaleChnl[1].indexOf(tSalChnlDetail) != -1) {
            return tLCGrpPolSchema;
        } else {
            return null;
        }
    }

    //填充佣金日志表
    private boolean prepareLogData(String BranchType, String BranchType2) {
        String WageNo = AgentPubFun.formatDate(mLAWageLogSchema.getStartDate(),"yyyyMM");
        System.out.println("本月佣金计算年月代码：" + WageNo);
        System.out.println("新填充佣金计算日志lawagelog表");
        //填充佣金计算日志表
        LAWageLogDB tLAWageLogDB = new LAWageLogDB();
        LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();

        tLAWageLogDB.setWageNo(WageNo);
        tLAWageLogDB.setManageCom(mManageCom);
        tLAWageLogDB.setBranchType(BranchType);
        tLAWageLogDB.setBranchType2(BranchType2);
        tLAWageLogDB.setStartDate(mLAWageLogSchema.getStartDate());
        tLAWageLogDB.setEndDate(mLAWageLogSchema.getEndDate());
        tLAWageLogDB.setWageMonth(AgentPubFun.formatDate(mLAWageLogSchema.getStartDate(), "MM"));
        tLAWageLogDB.setWageYear(AgentPubFun.formatDate(mLAWageLogSchema.getStartDate(), "yyyy"));
        tLAWageLogDB.setOperator(mGlobalInput.Operator);
        tLAWageLogDB.setState("00");
        tLAWageLogDB.setMakeDate(CurrentDate);
        tLAWageLogDB.setMakeTime(CurrentTime);
        tLAWageLogDB.setModifyDate(CurrentDate);
        tLAWageLogDB.setModifyTime(CurrentTime);
        if(!tLAWageLogDB.insert())
        {
          return false;
        }
        //this.mInsLAWageLogSet.add(tLAWageLogDB);
        System.out.println("新填充佣金计算历史lawagehistory表");
        //填充佣金计算历史表
        tLAWageHistoryDB.setWageNo(WageNo);
        tLAWageHistoryDB.setManageCom(mManageCom);
        tLAWageHistoryDB.setAClass("03");
        tLAWageHistoryDB.setOperator(mGlobalInput.Operator);
        tLAWageHistoryDB.setState("00");
        tLAWageHistoryDB.setBranchType(BranchType);
        tLAWageHistoryDB.setBranchType2(BranchType2);
        tLAWageHistoryDB.setMakeDate(CurrentDate);
        tLAWageHistoryDB.setMakeTime(CurrentTime);
        tLAWageHistoryDB.setModifyDate(CurrentDate);
        tLAWageHistoryDB.setModifyTime(CurrentTime);
        //this.mInsLAWageHistorySet.add(tLAWageHistoryDB);
        if(!tLAWageHistoryDB.insert())
        {
           return false;
        }
        return true;
    }
    /*
     tFlag=01 按照回执回销计算
     tFlag=02 按照实收表的makedate计算
    */
    private String calCalDateForAll(String cBranchType, String cBranchType2,
                                    String cSignDate, String cGetPolDate,
                                    String cPayCount, String cTMakeDate,
                                    String cCustomGetPolDate, String cValidDate,String cFlag,
                                    String cConfDate,
                                    String cAFlag,String cManageCom,String cVisistTime,String cRiskFlag,String cTransState,String cAgentCode,String cGrpcontno,String cRiskCode)
    {
        String tGetPolDate = cGetPolDate == null ? "" : cGetPolDate;
        String tSignDate = cSignDate == null ? "" : cSignDate;
        String tTMakeDate = cTMakeDate == null ? "" : cTMakeDate;
        String tCustomGetPolDate = cCustomGetPolDate == null ? "" : cCustomGetPolDate;
        String tBranchType = cBranchType == null ? "" : cBranchType;
        String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
        String tPayCount = cPayCount == null ? "" : cPayCount;
        String tConfDate = cConfDate ==null? "": cConfDate;
        String tFlag = cFlag== null ? "" : cFlag ;
        String tAFlag = cAFlag== null ? "" : cAFlag ;//团险根据收费方式采取不同的佣金计算方式
        String tManageCom=cManageCom==null ? "" : cManageCom ;
        String tVisistTime = cVisistTime==null ? "" : cVisistTime ;
        String tRiskFlag = cRiskFlag==null ? "" : cRiskFlag ;
        String tTransState = cTransState==null ? "" : cTransState ;
        String tAgentCode =cAgentCode ==null ? "" :cAgentCode;
        String tGrpcontno =cGrpcontno ==null ? "" :cGrpcontno;
        String tRiskCode =cRiskCode ==null ? "" :cRiskCode;
        if (tBranchType == "") 
        {
            return "";
        }
        if (tBranchType2 == "") {
            return "";
        }
        //如果是续保、续期，则不需要按照回执回销计算
        else if (mLACommisionSchema.getPayCount()>1)
        {
            tFlag="02";
        }

        Calculator tCalculator = new Calculator();
        tCalculator.setCalCode("wageno");
        tCalculator.addBasicFactor("GetPolDate", tGetPolDate);
        tCalculator.addBasicFactor("SignDate", tSignDate);
        tCalculator.addBasicFactor("TMakeDate", tTMakeDate);
        tCalculator.addBasicFactor("CustomGetPolDate", tCustomGetPolDate);
        tCalculator.addBasicFactor("BranchType", tBranchType);
        tCalculator.addBasicFactor("BranchType2", tBranchType2);
        tCalculator.addBasicFactor("ValiDate", cValidDate);
        tCalculator.addBasicFactor("PayCount", tPayCount);
        tCalculator.addBasicFactor("Flag", tFlag);
        tCalculator.addBasicFactor("ConfDate", tConfDate);
        tCalculator.addBasicFactor("AFlag", tAFlag);
        tCalculator.addBasicFactor("ManageCom", tManageCom);
        tCalculator.addBasicFactor("VisistTime", tVisistTime);
        tCalculator.addBasicFactor("RiskFlag", tRiskFlag);
        tCalculator.addBasicFactor("TransState", tTransState);
        tCalculator.addBasicFactor("AgentCode", tAgentCode);
        tCalculator.addBasicFactor("GroupAgentCode", tGrpcontno);
        tCalculator.addBasicFactor("RiskCode", tRiskCode);
        tCalculator.addBasicFactor("VisitFlag", "Y");
        //用来兼容校验回访日期中：河北追加保费的不用校验回访日期
        
//        if("8634".equals(cManageCom.substring(0,4))&&"03".equals(cTransState)&&"1".equals(cBranchType)&&"01".equals(cBranchType2)){
//        	tCalculator.addBasicFactor("VisitFlag", "N");
//        }
//        else{
//        	tCalculator.addBasicFactor("VisitFlag", "Y");
//        }
        
        String tResult = tCalculator.calculate();
        return tResult == null ? "" : tResult;

    }

    //获取中介类型
    //01
//    private String getActType() {
//        String tAcType = "";
//        //如果契约渠道为中介
//        String tAgentCom = mLACommisionSchema.getAgentCom();
//        LAComDB tLAComDB = new LAComDB();
//        tLAComDB.setAgentCom(tAgentCom);
//        if (!tLAComDB.getInfo()) {
//            LAWageActivityLogSchema tLAWageActivityLogSchema = new
//                    LAWageActivityLogSchema();
//            tLAWageActivityLogSchema.setAgentCode(mLAAgentSchema.
//                                                  getAgentCode());
//            tLAWageActivityLogSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
//            tLAWageActivityLogSchema.setContNo(mLACommisionSchema.getContNo());
//            tLAWageActivityLogSchema.setDescribe("渠道类型为中介，但是没有查到机构编码为" +
//                                                 tAgentCom + "的中介机构!");
//            tLAWageActivityLogSchema.setGrpContNo(mLACommisionSchema.
//                                                  getGrpContNo());
//            tLAWageActivityLogSchema.setGrpPolNo(mLACommisionSchema.
//                                                 getGrpPolNo());
//            tLAWageActivityLogSchema.setMakeDate(CurrentDate);
//            tLAWageActivityLogSchema.setMakeTime(CurrentTime);
//            tLAWageActivityLogSchema.setManageCom(mLACommisionSchema.
//                                                  getManageCom());
//            tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
//            tLAWageActivityLogSchema.setOtherNo("");
//            tLAWageActivityLogSchema.setOtherNoType("");
//            tLAWageActivityLogSchema.setPolNo(mLACommisionSchema.getPolNo());
//            tLAWageActivityLogSchema.setRiskCode(mLACommisionSchema.
//                                                 getRiskCode());
//
//            String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
//            tLAWageActivityLogSchema.setSerialNo(tSerialNo);
//            tLAWageActivityLogSchema.setWageLogType("01");
//            tLAWageActivityLogSchema.setWageNo(mLAWageLogSchema.getWageNo());
//            mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
//
//        } else {
//            tAcType = tLAComDB.getSchema().getACType();
//        }
//
//        return tAcType;
//    }
    //得到保全项目管理费
    private String getmanagefee(String cPolNo,String cOtherNo,String cOtherNoType) {
        String tSql = "select sum(fee) from lcinsureaccclassfee where  PolNo='"
        	+cPolNo+"' and otherno='"+cOtherNo
        	+"' and othertype='"+cOtherNoType+"'  with ur ";
        //如果契约渠道为中介
        ExeSQL tExeSQL = new ExeSQL();
        String tvalue=tExeSQL.getOneValue(tSql);

        if ( tExeSQL.mErrors.needDealError() ) {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBL";
            tError.functionName = "getfee";
            tError.errorMessage = "查询管理费出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(tvalue==null||tvalue.equals(""))
        {
            tvalue="0";
        }
        return tvalue;
    }

    /**
     * 得到利息 ，由于有月息的情况，所以只用polno来关联
     * 2017-3-22 yangyang 需求：3290
     * @param cPolNo
     * @param cOtherNo
     * @param cOtherNoType
     * @return
     */
    /**
     * 得到利息 ，由于有月息的情况，所以根据险种来进行分别处理
     * 2017-3-22 yangyang 需求：3290
     * @param cLJAGetEndorseSchema
     * @return
     */
    private String getAccuralfee(LJAGetEndorseSchema cLJAGetEndorseSchema) {
    	String tRiskCode = cLJAGetEndorseSchema.getRiskCode();
    	String tPolNo = cLJAGetEndorseSchema.getPolNo();
    	String tSql ="";
    	if("370201".equals(tRiskCode))
    	{
    		String tPayPlanCode = cLJAGetEndorseSchema.getPayPlanCode();
    		tSql = "select sum(money) from Lbinsureacctrace where  1=1 "
				+ " and PolNo='"+tPolNo+"'"
				+ " and payplancode='"+tPayPlanCode+"'"
				+ " and moneytype='LX' with ur ";
    	}
    	else
    	{
    		String tEndorsementNo = cLJAGetEndorseSchema.getEndorsementNo();
    		String tOtherNoType = cLJAGetEndorseSchema.getOtherNoType();
    		tSql = "select sum(money) from Lbinsureacctrace where  PolNo='"
    	        	+tPolNo+"' and otherno='"+tEndorsementNo
    	        	+"' and othertype='"+tOtherNoType+"' and moneytype='LX' with ur ";
    	}
        ExeSQL tExeSQL = new ExeSQL();
        String tvalue=tExeSQL.getOneValue(tSql);

        if ( tExeSQL.mErrors.needDealError() ) {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveBL";
            tError.functionName = "getfee";
            tError.errorMessage = "查询利息出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        if(tvalue==null||tvalue.equals(""))
        {
            tvalue="0";
        }
        return tvalue;
    }

    //获取中介类型,用于控制是否从实收表取数
    //01：银邮；02：其他中介
    private String getAcType(String pmAgentCom) {
        String tAcType = "";
        //如果契约渠道为中介
        String tAgentCom = pmAgentCom;
        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setAgentCom(tAgentCom);
        if (tLAComDB.getInfo()) {
            tAcType = tLAComDB.getSchema().getACType();
        }
        return tAcType;
    }

    //获取万能险原始保额
    //
    private static double getOminAmnt(LCPolSchema pmLCPolSchema) {
        double tamnt=0;
        
    	String tSQL= " select a.amnt from lppol a,lpedoritem b,lpedorapp c"
    		+" where a.edorno = b.edorno"
    		+" and b.edorno = c.edoracceptno"
    		+" and a.contno = b.contno" 
    		+" and b.edortype ='BA'"
    		+" and c.edorstate ='0'"
    		+" and a.polno='"+pmLCPolSchema.getPolNo()+"'";
    	ExeSQL tExeSQL = new ExeSQL();
        String tvalue=tExeSQL.getOneValue(tSQL);
        System.out.println(tvalue);
        if(tvalue==null||tvalue.equals(""))
        {
        	tamnt=pmLCPolSchema.getAmnt();
        }
        else{
        tamnt = Double.parseDouble(tvalue);
        }
        return tamnt;
    }
    
    private void jbInit() throws Exception {
    }


    //计算万能险种基本保费
    public double calTrans(double cPrem, int cPayIntv,double cAmnt,String cRiskCode,int cInsuredAge) {
       Calculator tCalculator = new Calculator(); //计算类
       System.out.println("Calcode:");
       if("334701".equals(cRiskCode))
       {
    	   tCalculator.setCalCode("baspm2"); //添加计算编码
       }
       else
       {
    	   tCalculator.setCalCode("basprm"); //添加计算编码
       }
       String tPrem=String.valueOf(cPrem);
       String tPayIntv=String.valueOf(cPayIntv);
       String tAmnt=String.valueOf(cAmnt);
       tCalculator.addBasicFactor("Prem", tPrem); //
       tCalculator.addBasicFactor("PayIntv", tPayIntv); //
       tCalculator.addBasicFactor("Amnt", tAmnt); //
       tCalculator.addBasicFactor("InsuredAge", String.valueOf(cInsuredAge)); //
       if (tCalculator.mErrors.needDealError()) {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "AgentWageCalDoBL";
           tError.functionName = "calTrans";
           tError.errorMessage = tCalculator.mErrors.getFirstError();
           this.mErrors.addOneError(tError);
            return -99;
       }
       //得到结果
       String tvalue = tCalculator.calculate();
       double tReturn = Double.parseDouble(tvalue);
       return tReturn;

   }
    /**
     * 处理正常承保税优险种保费拆分成帐户保费及风险保费
     * @return
     */
    private boolean dealTaxCommision(LJAPayPersonSchema cLJAPayPersonSchema,String cContNo,String cPolNo,
    		String cPayTypeFlag)
    {   
    	int PayYear = 0;
    	String COMMISIONSN;
    	String COMMISIONSN1;
    	String tRiskPremSQL;
    	String tflag;
    	String tAgentCode;
    	String tBranchCode;
    	String tBranchAttr;
    	Reflections tReflections = new Reflections();
    	LCContSchema tLCContSchema = new LCContSchema();
    	tLCContSchema = this.queryLCCont(cContNo);
    	if(null == tLCContSchema)
    	{
    		return true;
    	}
    	LCPolSchema tLCPolSchema = new LCPolSchema();
    	tLCPolSchema = this.queryLCPol(cPolNo);
    	if(null == tLCPolSchema)
    	{
    		return true;
    	}
    	
    	//续保处理
    	LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
    	tLCRnewStateLogSchema = this.queryLCRnewPol(cPolNo);

        if (tLCRnewStateLogSchema == null) {
              tRenewCount = 0;
         }
        else
        {
          if(cPayTypeFlag.equals("1"))
           {
           tRenewCount = tLCRnewStateLogSchema.getRenewCount();
           }
          else
           {
            tRenewCount = 0;
           }
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(cLJAPayPersonSchema.getAgentCode());
        tLAAgentDB.getInfo();
        mLAAgentSchema = tLAAgentDB.getSchema();
        
        //缴费年度
        if(cLJAPayPersonSchema.getPayIntv()==0)
        {
        	PayYear = 0;
        }
        else
        {
        	//计算交费年度 = 保单生效日到交至日期
        	PayYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),
                      fDate.getString(PubFun.calDate(
                      fDate.getDate(cLJAPayPersonSchema.getCurPayToDate()), -2,"D", null)), "Y");
            if(PayYear<0)  PayYear=0;
        }
        mLACommisionSchema = new LACommisionSchema();
        COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
        System.out.println("COMMISIONSN号：" + COMMISIONSN);
        mLACommisionSchema.setCommisionSN(COMMISIONSN);
        mLACommisionSchema.setCommisionBaseNo("0000000000");
        mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
        mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
        mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
        mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
        mLACommisionSchema.setManageCom(cLJAPayPersonSchema.getManageCom());
        mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
        mLACommisionSchema.setRiskCode(tLCPolSchema.getRiskCode());
        mLACommisionSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
        mLACommisionSchema.setDutyCode(cLJAPayPersonSchema.getDutyCode());
        mLACommisionSchema.setPayPlanCode(cLJAPayPersonSchema.getPayPlanCode());
        mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
        mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
        mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
        mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
        mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
        mLACommisionSchema.setReceiptNo(cLJAPayPersonSchema.getPayNo());
        mLACommisionSchema.setTPayDate(cLJAPayPersonSchema.getPayDate());
        mLACommisionSchema.setTEnterAccDate(cLJAPayPersonSchema.getEnterAccDate());
        mLACommisionSchema.setTConfDate(cLJAPayPersonSchema.getConfDate());
        mLACommisionSchema.setTMakeDate(cLJAPayPersonSchema.getMakeDate());
        mLACommisionSchema.setCommDate(CurrentDate);
        
        //xjh Add 05/01/27 ,判断是否是批改保单
        tflag = getIncomeType(cLJAPayPersonSchema.getPayNo());
        mLACommisionSchema.setFlag(tflag);
        mLACommisionSchema.setPayYear(PayYear);
        mLACommisionSchema.setReNewCount(tRenewCount);
        mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
        mLACommisionSchema.setYears(tLCPolSchema.getYears());
        mLACommisionSchema.setPayCount(cLJAPayPersonSchema.getPayCount());
        mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
        mLACommisionSchema.setGetPolDate(tLCContSchema.getGetPolDate());

        mLACommisionSchema.setAgentCom(tLCPolSchema.getAgentCom());
        mLACommisionSchema.setAgentType(cLJAPayPersonSchema.getAgentType());
        mLACommisionSchema.setAgentCode(cLJAPayPersonSchema.getAgentCode());

        String tPolType = tLCPolSchema.getStandbyFlag2();
        mLACommisionSchema.setPolType(tPolType == null ||
                                      tPolType.equals("") ?
                                      "1" : tPolType); //0:优惠 1：正常
        mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());


        mLACommisionSchema.setP11(tLCContSchema.getAppntName());
        mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
        mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
        mLACommisionSchema.setP14(tLCPolSchema.getPrtNo());
        mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
        mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //录入日期
        mLACommisionSchema.setCustomGetPolDate(tLCContSchema.getCustomGetPolDate()); //签字日期
        mLACommisionSchema.setLastPayToDate(cLJAPayPersonSchema.getLastPayToDate());
        mLACommisionSchema.setCurPayToDate(cLJAPayPersonSchema.getCurPayToDate());
        mLACommisionSchema.setTransType("ZC");
        mLACommisionSchema.setCommDire("1");
        
        
        mLACommisionSchema.setTransMoney(cLJAPayPersonSchema.getSumActuPayMoney());
        // riskmark
        String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
        mLACommisionSchema.setScanDate(tScanDate);
        mLACommisionSchema.setOperator(mGlobalInput.Operator);
        mLACommisionSchema.setMakeDate(CurrentDate);
        mLACommisionSchema.setMakeTime(CurrentTime);
        mLACommisionSchema.setModifyDate(CurrentDate);
        mLACommisionSchema.setModifyTime(CurrentTime);
        
        String tAcType = "";
        //添加对于互动中介的处理
        if (tLCPolSchema.getSaleChnl().equals("15")) 
        {
        	tAcType = checkActive(mLACommisionSchema);
        	if("".equals(tAcType))
        	{
        		return false;
        	}
        }
        String[] tBranchTypes = AgentPubFun.switchSalChnl(
                tLCPolSchema.
                getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                tAcType);
        mLACommisionSchema.setBranchType(tBranchTypes[0]);
        mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
        mLACommisionSchema.setBranchType3(tBranchTypes[2]);
        
        String tFlag="01";
        if(mLACommisionSchema.getReNewCount()>0)
        {
            tFlag="02";
        }
        //简易险
        if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
        {
            tFlag="02";
        }
        String tConfDate = "";
        tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"ZC",mLAWageLogSchema.getEndDate(),
        		                        cLJAPayPersonSchema.getConfDate(),cLJAPayPersonSchema.getPayNo());
        
        //--查询回访成功数据
        String tVisitTime = this.queryReturnVisit(cContNo);//--查询回访日期
        String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
        String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
        String tTransState = mLACommisionSchema.getTransState();

        // add new 
        String tAgentcode =mLACommisionSchema.getAgentCode();
        String caldate = this.calCalDateForAll(tBranchTypes[0],
                tBranchTypes[1],
                tLCContSchema.getSignDate(),
                mLACommisionSchema.getGetPolDate(),
                String.valueOf(
                        mLACommisionSchema.
                        getPayCount()),
                mLACommisionSchema.getTMakeDate(),
               mLACommisionSchema.getCustomGetPolDate(),
                mLACommisionSchema.getCValiDate(),tFlag,
                tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);
        if (!caldate.equals("") && !caldate.equals("0")) 
        {
            String sql =
                    "select yearmonth from LAStatSegment where startdate<='" +
                    caldate + "' and enddate>='" + caldate +
                    "' and stattype='5'  with ur";
            ExeSQL tExeSQL = new ExeSQL();
            String tWageNo = tExeSQL.getOneValue(sql);
            mLACommisionSchema.setTConfDate(tConfDate);
            mLACommisionSchema.setWageNo(tWageNo);
            mLACommisionSchema.setCalDate(caldate);
        }

        mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode()); //直辖组
        tAgentCode = mLACommisionSchema.getAgentCode().trim();
        tBranchCode = mLAAgentSchema.getBranchCode(); //职级对应机构代码
        if (tBranchCode == null)
        {
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema;
        tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = querytLABranchGroup(tBranchCode);
        mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.getBranchSeries());
        tBranchAttr = tLABranchGroupSchema.getBranchAttr();
        mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
        mLACommisionSchema.setBranchAttr(tBranchAttr);		

        
        String tDueFeeType = judgeDueFeeType(mLACommisionSchema.getReceiptNo());
        if("0".equals(tDueFeeType))
		{
			tRiskPremSQL = "select sum(fee) from lcinsureaccfeetrace  where 1=1"
					+ " and othertype ='1' and moneytype ='RP' "
					+ " and otherno = '"+mLACommisionSchema.getPolNo()+"' with ur"; 
		}
		else
		{
			tRiskPremSQL = "select sum(fee) from lcinsureaccfeetrace  where 1=1"
					+ " and othertype ='2' and moneytype ='RP' "
					+ " and otherno='"+cLJAPayPersonSchema.getGetNoticeNo()+"' with ur"; 
		}
        ExeSQL tExeSQL = new ExeSQL();
		System.out.println("AgentWageCalSaveNewBL-->税优判断首期与续期:tRiskPremSQL:"+tRiskPremSQL);
		String tResultPrem = tExeSQL.getOneValue(tRiskPremSQL);
		if("".equals(tResultPrem)||null==tResultPrem)
		{
			tResultPrem = "0";
		}
		double tRiskPrem  = Double.parseDouble(tResultPrem);
		double tTransMoney = mLACommisionSchema.getTransMoney();
		
		mLACommisionSchema1 = new LACommisionSchema();
        COMMISIONSN1 = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
        System.out.println("COMMISIONSN号：" + COMMISIONSN1);
        tReflections.transFields(mLACommisionSchema1, mLACommisionSchema);
        mLACommisionSchema1.setCommisionSN(COMMISIONSN1);
        //帐户保费
		mLACommisionSchema.setTransState("04");
		mLACommisionSchema.setTransMoney(tTransMoney-tRiskPrem);
		mLACommisionSchema.setTransStandMoney(tTransMoney-tRiskPrem);
		if(tLCContSchema.getSaleChnl().equals("07"))
        {
            mLACommisionSchema.setF1("02");
            mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
        }
        else
        {
            mLACommisionSchema.setF1("01");
        }
		
		//风险保费
		mLACommisionSchema1.setTransMoney(tRiskPrem);
		mLACommisionSchema1.setTransState("05");
		mLACommisionSchema1.setTransStandMoney(tRiskPrem);
		if(tLCContSchema.getSaleChnl().equals("07"))
        {
            mLACommisionSchema1.setF1("02");
            mLACommisionSchema1.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
        }
        else
        {
            mLACommisionSchema1.setF1("01");
        }
		
		this.mLACommisionSet.add(mLACommisionSchema);
        this.mLACommisionSet.add(mLACommisionSchema1);
		
				
    	return true;
    }

   /**
    * 处理万能险种330801  331801
    * 区分出基本保费和额外保费
    *
   */
    private boolean dealOmniCommision(LJAPayPersonSchema tLJAPayPersonSchema,
                                 String tContNo, String PolNo,String tPayTypeFlag) {
       int PayYear= 0;
       String COMMISIONSN="";
       String COMMISIONSN1="";
       String tflag="";
       String tAgentCode="";
       String tBranchCode="";
       String tBranchAttr="";
       LCContSchema tLCContSchema = new LCContSchema();
       tLCContSchema = this.queryLCCont(tContNo);

       if (tLCContSchema == null) {
          //CError tError = new CError();
          //tError.moduleName = "AgentWageCalSaveNewBL";
          //tError.functionName = "dealOmniCommision";
          //tError.errorMessage = "没有得到足够的信息！";
          //this.mErrors.addOneError(tError);
          //return false;
         return true;
        }
      //查询保单详细信息
        LCPolSchema tLCPolSchema = new LCPolSchema();
        try{
            tLCPolSchema = this.queryLCPol(PolNo);
          }
        catch(Exception ex)
          {
            ex.getStackTrace();
          }
        if (tLCPolSchema == null) {
         // CError tError = new CError();
         // tError.moduleName = "AgentWageCalSaveNewBL";
         // tError.functionName = "dealOmniCommision";
         // tError.errorMessage = "没有得到足够的信息！";
         // this.mErrors.addOneError(tError);
         // return false;
           return true;
         }
        //续保的处理
        LCRnewStateLogSchema tLCRnewStateLogSchema = new LCRnewStateLogSchema();
        tLCRnewStateLogSchema = this.queryLCRnewPol(PolNo);

        if (tLCRnewStateLogSchema == null) {
              tRenewCount = 0;
         }
        else
        {
          if(tPayTypeFlag.equals("1"))
           {

//          	if (tRiskCode.equals("320106")){
//          		tRenewCount = 0;
//          	}
//          	else 
           tRenewCount = tLCRnewStateLogSchema.getRenewCount();
           }
          else
           {
            tRenewCount = 0;
           }
        }
        
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tLJAPayPersonSchema.getAgentCode());
        tLAAgentDB.getInfo();
        mLAAgentSchema = tLAAgentDB.getSchema();


        if (tLJAPayPersonSchema.getPayIntv() == 0) { //趸交payIntv = 0
            PayYear = 0;
        }
        else {
        //计算交费年度=保单生效日到交至日期
        PayYear = PubFun.calInterval(tLCPolSchema.getCValiDate(),
                                     fDate.getString(PubFun.calDate(
                fDate.getDate(tLJAPayPersonSchema.getCurPayToDate()), -2,
                "D", null)), "Y");
         if(PayYear<0)  PayYear=0;
        }
        mLACommisionSchema = new LACommisionSchema();
        COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
        System.out.println("COMMISIONSN号：" + COMMISIONSN);
        mLACommisionSchema.setCommisionSN(COMMISIONSN);
        mLACommisionSchema.setCommisionBaseNo("0000000000");
        mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
        mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
        mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
        mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
        mLACommisionSchema.setManageCom(tLJAPayPersonSchema.getManageCom());
        mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
        mLACommisionSchema.setRiskCode(tLCPolSchema.getRiskCode());
        mLACommisionSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
        mLACommisionSchema.setDutyCode(tLJAPayPersonSchema.getDutyCode());
        mLACommisionSchema.setPayPlanCode(tLJAPayPersonSchema.getPayPlanCode());
        mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
        mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
        mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
        mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
        mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
        mLACommisionSchema.setReceiptNo(tLJAPayPersonSchema.getPayNo());
        mLACommisionSchema.setTPayDate(tLJAPayPersonSchema.getPayDate());
        mLACommisionSchema.setTEnterAccDate(tLJAPayPersonSchema.getEnterAccDate());
        mLACommisionSchema.setTConfDate(tLJAPayPersonSchema.getConfDate());
        mLACommisionSchema.setTMakeDate(tLJAPayPersonSchema.getMakeDate());
        mLACommisionSchema.setCommDate(CurrentDate);

        //万能险 总保费=基本保费+额外保费
        //用TransState标示费用类型，01-基本保费；02-额外保费
        //在此还要区分出财务反冲的数据
        double tmprem=tLJAPayPersonSchema.getSumActuPayMoney();
        double tprem=java.lang.Math.abs(tLJAPayPersonSchema.getSumActuPayMoney());
        int    tpayintv=tLCPolSchema.getPayIntv();
        //第一版
//        double tamnt=tLCPolSchema.getAmnt();        
//        double tTransMoney = calTrans(tprem, tpayintv,tamnt);
        //暂时恢复到第一版
        //第二版
//        double tamnt1=getOminAmnt(tLCPolSchema);
        //第三版
        double tamnt1=CommonBL.getTBAmnt(tLCPolSchema);
//        double tTransMoney = calTrans(tprem, tpayintv,tamnt1);
        //第四版
        int tAppntInsuredAge =0;
        String tBirthDay = tLCPolSchema.getInsuredBirthday();
        tAppntInsuredAge = PubFun.calInterval(tBirthDay,this.currentDate, "Y");
        double tTransMoney=calTrans(tprem, tpayintv,tamnt1,tLCPolSchema.getRiskCode(),tAppntInsuredAge);
        System.out.println("保额-保费:"+tamnt1+"/"+tTransMoney);
        if(tmprem>=0)
        {
        mLACommisionSchema.setTransMoney(tTransMoney);
        mLACommisionSchema.setTransState("01");
        mLACommisionSchema.setTransStandMoney(tTransMoney);
        }
        else
        {
        mLACommisionSchema.setTransMoney(0 - java.lang.Math.abs(tTransMoney));
        mLACommisionSchema.setTransState("01");
        mLACommisionSchema.setTransStandMoney(mLACommisionSchema.getTransMoney());
        }

        mLACommisionSchema.setLastPayToDate(tLJAPayPersonSchema.getLastPayToDate());
        mLACommisionSchema.setCurPayToDate(tLJAPayPersonSchema.getCurPayToDate());
        mLACommisionSchema.setTransType("ZC");
        mLACommisionSchema.setCommDire("1");
        if(tLCContSchema.getSaleChnl().equals("07"))
        {
            mLACommisionSchema.setF1("02");
            mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
        }
        else
        {
            mLACommisionSchema.setF1("01");
        }
        //xjh Add 05/01/27 ,判断是否是批改保单
        tflag = getIncomeType(tLJAPayPersonSchema.getPayNo());
        mLACommisionSchema.setFlag(tflag);
        mLACommisionSchema.setPayYear(PayYear);
        mLACommisionSchema.setReNewCount(tRenewCount);
        mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
        mLACommisionSchema.setYears(tLCPolSchema.getYears());
        mLACommisionSchema.setPayCount(tLJAPayPersonSchema.getPayCount());
        mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
        mLACommisionSchema.setGetPolDate(tLCContSchema.getGetPolDate());

        mLACommisionSchema.setAgentCom(tLCPolSchema.getAgentCom());
        mLACommisionSchema.setAgentType(tLJAPayPersonSchema.getAgentType());
        mLACommisionSchema.setAgentCode(tLJAPayPersonSchema.getAgentCode());


        String tPolType = tLCPolSchema.getStandbyFlag2();
        mLACommisionSchema.setPolType(tPolType == null ||
                                      tPolType.equals("") ?
                                      "1" : tPolType); //0:优惠 1：正常
        mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());


        mLACommisionSchema.setP11(tLCContSchema.getAppntName());
        mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
        mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
        mLACommisionSchema.setP14(tLCPolSchema.getPrtNo());
        mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
        mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //录入日期
        mLACommisionSchema.setCustomGetPolDate(tLCContSchema.getCustomGetPolDate()); //签字日期
        // riskmark
        String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
        mLACommisionSchema.setScanDate(tScanDate);
        mLACommisionSchema.setOperator(mGlobalInput.Operator);
        mLACommisionSchema.setMakeDate(CurrentDate);
        mLACommisionSchema.setMakeTime(CurrentTime);
        mLACommisionSchema.setModifyDate(CurrentDate);
        mLACommisionSchema.setModifyTime(CurrentTime);
        String tAcType = "";
        //添加对于互动中介的处理
        if (tLCPolSchema.getSaleChnl().equals("15")) 
        {
        	tAcType = checkActive(mLACommisionSchema);
        	if("".equals(tAcType))
        	{
        		return false;
        	}
        }
        String[] tBranchTypes = AgentPubFun.switchSalChnl(
                tLCPolSchema.
                getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                tAcType);
        mLACommisionSchema.setBranchType(tBranchTypes[0]);
        mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
        mLACommisionSchema.setBranchType3(tBranchTypes[2]);
        String tFlag="01";
        if(mLACommisionSchema.getReNewCount()>0)
        {
            tFlag="02";
        }
        //简易险
        if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
        {
            tFlag="02";
        }
        String tConfDate = "";
        tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"ZC",mLAWageLogSchema.getEndDate(),
        		                        tLJAPayPersonSchema.getConfDate(),tLJAPayPersonSchema.getPayNo());
        
        //--查询回访成功数据
        String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
        String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
        String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
        String tTransState = mLACommisionSchema.getTransState();

        // add new 
        String tAgentcode =mLACommisionSchema.getAgentCode() ;
        String caldate = this.calCalDateForAll(tBranchTypes[0],
                tBranchTypes[1],
                tLCContSchema.getSignDate(),
                mLACommisionSchema.getGetPolDate(),
                String.valueOf(
                        mLACommisionSchema.
                        getPayCount()),
                mLACommisionSchema.getTMakeDate(),
               mLACommisionSchema.getCustomGetPolDate(),
                mLACommisionSchema.getCValiDate(),tFlag,
                tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);
        if (!caldate.equals("") && !caldate.equals("0")) {
            String sql =
                    "select yearmonth from LAStatSegment where startdate<='" +
                    caldate + "' and enddate>='" + caldate +
                    "' and stattype='5'  with ur";
            ExeSQL tExeSQL = new ExeSQL();
            String tWageNo = tExeSQL.getOneValue(sql);
            mLACommisionSchema.setTConfDate(tConfDate);
            mLACommisionSchema.setWageNo(tWageNo);
            mLACommisionSchema.setCalDate(caldate);
        }

        mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode()); //直辖组
        tAgentCode = mLACommisionSchema.getAgentCode().trim();
        tBranchCode = mLAAgentSchema.getBranchCode(); //职级对应机构代码
        if (tBranchCode == null) {
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema;
        tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = querytLABranchGroup(tBranchCode);
        mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.getBranchSeries());
        tBranchAttr = tLABranchGroupSchema.getBranchAttr();
        mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
        mLACommisionSchema.setBranchAttr(tBranchAttr);


        mLACommisionSchema1 = new LACommisionSchema();
        COMMISIONSN1 = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
        System.out.println("COMMISIONSN号：" + COMMISIONSN1);
        mLACommisionSchema1.setCommisionSN(COMMISIONSN1);
        mLACommisionSchema1.setCommisionBaseNo("0000000000");
        mLACommisionSchema1.setGrpContNo(tLCPolSchema.getGrpContNo());
        mLACommisionSchema1.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        mLACommisionSchema1.setContNo(tLCPolSchema.getContNo());
        mLACommisionSchema1.setPolNo(tLCPolSchema.getPolNo());
        mLACommisionSchema1.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
        mLACommisionSchema1.setManageCom(tLJAPayPersonSchema.getManageCom());
        mLACommisionSchema1.setAppntNo(tLCContSchema.getAppntNo());
        mLACommisionSchema1.setRiskCode(tLCPolSchema.getRiskCode());
        mLACommisionSchema1.setRiskVersion(tLCPolSchema.getRiskVersion());
        mLACommisionSchema1.setDutyCode(tLJAPayPersonSchema.getDutyCode());
        mLACommisionSchema1.setPayPlanCode(tLJAPayPersonSchema.getPayPlanCode());
        mLACommisionSchema1.setCValiDate(tLCPolSchema.getCValiDate());
        mLACommisionSchema1.setPayIntv(tLCPolSchema.getPayIntv());
        mLACommisionSchema1.setPayMode(tLCPolSchema.getPayMode());
        mLACommisionSchema1.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
        mLACommisionSchema1.setF5(tLCPolSchema.getInsuYearFlag());
        mLACommisionSchema1.setReceiptNo(tLJAPayPersonSchema.getPayNo());
        mLACommisionSchema1.setTPayDate(tLJAPayPersonSchema.getPayDate());
        mLACommisionSchema1.setTEnterAccDate(tLJAPayPersonSchema.getEnterAccDate());
        mLACommisionSchema1.setTConfDate(tLJAPayPersonSchema.getConfDate());
        mLACommisionSchema1.setTMakeDate(tLJAPayPersonSchema.getMakeDate());
        mLACommisionSchema1.setCommDate(CurrentDate);

        //万能险 总保费=基本保费+额外保费
        //用TransState标示费用类型，01-基本保费；02-额外保费
        double tTransMoney1 = tprem-tTransMoney;
        if(tmprem>=0)
        {
        mLACommisionSchema1.setTransMoney(tTransMoney1);
        mLACommisionSchema1.setTransState("02");
        mLACommisionSchema1.setTransStandMoney(tTransMoney1);
        }
        else
        {
        mLACommisionSchema1.setTransMoney(0 - java.lang.Math.abs(tTransMoney1));
        mLACommisionSchema1.setTransState("02");
        mLACommisionSchema1.setTransStandMoney(mLACommisionSchema1.getTransMoney());
        }
        mLACommisionSchema1.setLastPayToDate(tLJAPayPersonSchema.getLastPayToDate());
        mLACommisionSchema1.setCurPayToDate(tLJAPayPersonSchema.getCurPayToDate());
        mLACommisionSchema1.setTransType("ZC");
        mLACommisionSchema1.setCommDire("1");
        if(tLCContSchema.getSaleChnl().equals("07"))
        {
            mLACommisionSchema1.setF1("02");
            mLACommisionSchema1.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
        }
        else
        {
            mLACommisionSchema1.setF1("01");
        }
        //xjh Add 05/01/27 ,判断是否是批改保单
        tflag = getIncomeType(tLJAPayPersonSchema.getPayNo());
        mLACommisionSchema1.setFlag(tflag);
        mLACommisionSchema1.setPayYear(PayYear);
        mLACommisionSchema1.setReNewCount(tRenewCount);
        mLACommisionSchema1.setPayYears(tLCPolSchema.getPayYears());
        mLACommisionSchema1.setYears(tLCPolSchema.getYears());
        mLACommisionSchema1.setPayCount(tLJAPayPersonSchema.getPayCount());
        mLACommisionSchema1.setSignDate(tLCPolSchema.getSignDate());
        mLACommisionSchema1.setGetPolDate(tLCContSchema.getGetPolDate());

        mLACommisionSchema1.setAgentCom(tLCPolSchema.getAgentCom());
        mLACommisionSchema1.setAgentType(tLJAPayPersonSchema.getAgentType());
        mLACommisionSchema1.setAgentCode(tLJAPayPersonSchema.getAgentCode());


        tPolType = tLCPolSchema.getStandbyFlag2();
        mLACommisionSchema1.setPolType(tPolType == null ||
                                      tPolType.equals("") ?
                                      "1" : tPolType); //0:优惠 1：正常
        mLACommisionSchema1.setP1(tLCPolSchema.getManageFeeRate());


        mLACommisionSchema1.setP11(tLCContSchema.getAppntName());
        mLACommisionSchema1.setP12(tLCPolSchema.getInsuredNo());
        mLACommisionSchema1.setP13(tLCPolSchema.getInsuredName());
        mLACommisionSchema1.setP14(tLCPolSchema.getPrtNo());
        mLACommisionSchema1.setP15(tLCPolSchema.getProposalNo());
        mLACommisionSchema1.setMakePolDate(tLCPolSchema.getMakeDate()); //录入日期
        mLACommisionSchema1.setCustomGetPolDate(tLCContSchema.getCustomGetPolDate()); //签字日期
        // riskmark
        tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
        mLACommisionSchema1.setScanDate(tScanDate);
        mLACommisionSchema1.setOperator(mGlobalInput.Operator);
        mLACommisionSchema1.setMakeDate(CurrentDate);
        mLACommisionSchema1.setMakeTime(CurrentTime);
        mLACommisionSchema1.setModifyDate(CurrentDate);
        mLACommisionSchema1.setModifyTime(CurrentTime);

        tBranchTypes = AgentPubFun.switchSalChnl(
                tLCPolSchema.
                getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                tAcType);
        mLACommisionSchema1.setBranchType(tBranchTypes[0]);
        mLACommisionSchema1.setBranchType2(tBranchTypes[1]);
        mLACommisionSchema1.setBranchType3(tBranchTypes[2]);
        tFlag="01";
        if(mLACommisionSchema1.getReNewCount()>0)
        {
            tFlag="02";
        }
        //简易险
        if(tLCContSchema.getCardFlag() != null && !tLCContSchema.getCardFlag().equals("0"))
        {
            tFlag="02";
        }
        tConfDate = "";
        tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"ZC",mLAWageLogSchema.getEndDate(),
        		                       tLJAPayPersonSchema.getConfDate(),tLJAPayPersonSchema.getPayNo());
         tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
          tRiskCode = mLACommisionSchema1.getRiskCode(); // 查询出险种信息
          tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
          tTransState = mLACommisionSchema1.getTransState();

        caldate = this.calCalDateForAll(tBranchTypes[0],
                tBranchTypes[1],
                tLCContSchema.getSignDate(),
                mLACommisionSchema1.getGetPolDate(),
                String.valueOf(
                        mLACommisionSchema1.
                        getPayCount()),
                mLACommisionSchema1.getTMakeDate(),
               mLACommisionSchema1.getCustomGetPolDate(),
                mLACommisionSchema1.getCValiDate(),tFlag,
                tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema1.getGrpContNo(),tRiskCode);
        if (!caldate.equals("") && !caldate.equals("0")) {
            String sql =
                    "select yearmonth from LAStatSegment where startdate<='" +
                    caldate + "' and enddate>='" + caldate +
                    "' and stattype='5' ";
            ExeSQL tExeSQL = new ExeSQL();
            String tWageNo = tExeSQL.getOneValue(sql);
            mLACommisionSchema1.setTConfDate(tConfDate);
            mLACommisionSchema1.setWageNo(tWageNo);
            mLACommisionSchema1.setCalDate(caldate);
        }

        mLACommisionSchema1.setBranchCode(mLAAgentSchema.getBranchCode()); //直辖组
        tAgentCode = mLACommisionSchema1.getAgentCode().trim();
        tBranchCode = mLAAgentSchema.getBranchCode(); //职级对应机构代码
        if (tBranchCode == null) {
            return false;
        }
        tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = querytLABranchGroup(tBranchCode);
        mLACommisionSchema1.setBranchSeries(tLABranchGroupSchema.getBranchSeries());
        tBranchAttr = tLABranchGroupSchema.getBranchAttr();
        mLACommisionSchema1.setAgentGroup(mLAAgentSchema.getAgentGroup());
        mLACommisionSchema1.setBranchAttr(tBranchAttr);



        this.mLACommisionSet.add(mLACommisionSchema);
        this.mLACommisionSet.add(mLACommisionSchema1);

        return true;
      }
          
    /**
     * 处理犹豫期退保税优险种保费拆分成帐户保费与风险保费
     * @param cLCContSchema
     * @param cLCPolSchema
     * @param cLJAGetEndorseSchema
     * @param cContNo
     * @param cPolNo
     * @return
     */
    private boolean dealTaxEndorseCommision(LCContSchema cLCContSchema,LCPolSchema cLCPolSchema,
    		LJAGetEndorseSchema cLJAGetEndorseSchema,String cContNo,String cPolNo)
    {
    	String tPayPlanCode = cLJAGetEndorseSchema.getPayPlanCode();
    	String COMMISIONSN;
    	int PayYear = 0;
    	String tAgentCode;
    	String tAgentGroup;
    	String tBranchCode;
    	String tBranchAttr;
    	String tAcType = "";
    	Reflections tReflections = new Reflections();
    	LACommisionSchema tCopyLACommisionSchema = new LACommisionSchema();
    	tCopyLACommisionSchema.setCommisionBaseNo("0000000000");
            //WageNo 783
    	tCopyLACommisionSchema.setGrpContNo(cLCPolSchema.getGrpContNo());
    	tCopyLACommisionSchema.setGrpPolNo(cLCPolSchema.getGrpPolNo());
    	tCopyLACommisionSchema.setContNo(cLCPolSchema.getContNo());
    	tCopyLACommisionSchema.setPolNo(cLCPolSchema.getPolNo());
    	tCopyLACommisionSchema.setMainPolNo(cLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
    	tCopyLACommisionSchema.setEndorsementNo(cLJAGetEndorseSchema.getEndorsementNo()); // 新加的 批单号码
    	tCopyLACommisionSchema.setManageCom(cLJAGetEndorseSchema.
    			getManageCom());
    	tCopyLACommisionSchema.setAppntNo(cLCContSchema.getAppntNo());
    	tCopyLACommisionSchema.setRiskCode(cLJAGetEndorseSchema.getRiskCode());
    	tCopyLACommisionSchema.setRiskVersion(cLCPolSchema.getRiskVersion());
    	tCopyLACommisionSchema.setDutyCode(cLJAGetEndorseSchema.getDutyCode());
    	tCopyLACommisionSchema.setPayPlanCode(cLJAGetEndorseSchema.getPayPlanCode());
    	tCopyLACommisionSchema.setCValiDate(cLCPolSchema.getCValiDate());
    	tCopyLACommisionSchema.setPayIntv(cLCPolSchema.getPayIntv());
    	tCopyLACommisionSchema.setPayMode(cLCPolSchema.getPayMode());
    	tCopyLACommisionSchema.setF4(String.valueOf(cLCPolSchema.getInsuYear()));
    	tCopyLACommisionSchema.setF5(cLCPolSchema.getInsuYearFlag());
    	tCopyLACommisionSchema.setReceiptNo(cLJAGetEndorseSchema.getActuGetNo());
    	tCopyLACommisionSchema.setTPayDate(cLJAGetEndorseSchema.getGetDate());
    	tCopyLACommisionSchema.setTEnterAccDate(cLJAGetEndorseSchema.getEnterAccDate());
    	tCopyLACommisionSchema.setTConfDate(cLJAGetEndorseSchema.getGetConfirmDate());
    	tCopyLACommisionSchema.setTMakeDate(cLJAGetEndorseSchema.getMakeDate());
    	tCopyLACommisionSchema.setCommDate(CurrentDate);
    	tCopyLACommisionSchema.setLastPayToDate(cLCPolSchema.
                getPaytoDate());
        tCopyLACommisionSchema.setCurPayToDate(cLJAGetEndorseSchema.
                getGetDate());
        tCopyLACommisionSchema.setTransType("WT");
        tCopyLACommisionSchema.setFlag("1"); //犹豫期撤件
        tCopyLACommisionSchema.setPayYear(PayYear);
        tCopyLACommisionSchema.setReNewCount(tRenewCount);
        tCopyLACommisionSchema.setPayYears(cLCPolSchema.getPayYears());
        tCopyLACommisionSchema.setYears(cLCPolSchema.getYears());
        tCopyLACommisionSchema.setPayCount(1);
        tCopyLACommisionSchema.setSignDate(cLCPolSchema.getSignDate());
        tCopyLACommisionSchema.setGetPolDate(cLCContSchema.
                getGetPolDate());
        tCopyLACommisionSchema.setAgentCom(cLJAGetEndorseSchema.
                getAgentCom());
        tCopyLACommisionSchema.setAgentType(cLJAGetEndorseSchema.
                getAgentType());
        tCopyLACommisionSchema.setAgentCode(cLJAGetEndorseSchema.
                getAgentCode());
        String tPolType = cLCPolSchema.getStandbyFlag2();
        tCopyLACommisionSchema.setPolType(tPolType == null ||
                                      tPolType.equals("") ?
                                      "1" : tPolType);
        tCopyLACommisionSchema.setP1(cLCPolSchema.getManageFeeRate());
        tCopyLACommisionSchema.setP11(cLCContSchema.getAppntName());
        tCopyLACommisionSchema.setP12(cLCPolSchema.getInsuredNo());
        tCopyLACommisionSchema.setP13(cLCPolSchema.getInsuredName());
        tCopyLACommisionSchema.setP14(cLCPolSchema.getPrtNo()); //cg add 2004-01-13
        tCopyLACommisionSchema.setP15(cLCPolSchema.getProposalNo());
        tCopyLACommisionSchema.setMakePolDate(cLCPolSchema.getMakeDate()); //交单日期
        tCopyLACommisionSchema.setCustomGetPolDate(cLCContSchema.
                getCustomGetPolDate()); //签字日期
        String tScanDate = this.getScanDate(cLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
        tCopyLACommisionSchema.setScanDate(tScanDate);
        tCopyLACommisionSchema.setOperator(mGlobalInput.Operator);
        tCopyLACommisionSchema.setMakeDate(CurrentDate);
        tCopyLACommisionSchema.setMakeTime(CurrentTime);
        tCopyLACommisionSchema.setModifyDate(CurrentDate);
        tCopyLACommisionSchema.setModifyTime(CurrentTime);
        tCopyLACommisionSchema.setBranchCode(mLAAgentSchema.
                getBranchCode());
        
        tAgentCode = tCopyLACommisionSchema.getAgentCode().trim();
        tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
        if (tAgentGroup == null) {
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema;
        tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);

        tCopyLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                getBranchSeries());
        //添加对于互动中介的处理
        if (cLCPolSchema.getSaleChnl().equals("15")) 
        {
        	tAcType = checkActive(tCopyLACommisionSchema);
        	if("".equals(tAcType))
        	{
        		return false;
        	}
        }
        String[] tBranchTypes = AgentPubFun.switchSalChnl(
                cLCPolSchema.
                getSaleChnl(), cLCPolSchema.getSaleChnlDetail(),
                tAcType);

        tBranchCode = tCopyLACommisionSchema.getBranchCode().trim(); //所在组代码
        if (tBranchTypes == null) {
            return false;
        }
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tBranchCode);
        tLABranchGroupDB.getInfo();
        tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
        if (tBranchAttr == null) {
            return false;
        }
        tCopyLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                getBranchSeries());
        tCopyLACommisionSchema.setAgentGroup(mLAAgentSchema.
                getAgentGroup());
        tCopyLACommisionSchema.setBranchAttr(tBranchAttr);
        tCopyLACommisionSchema.setBranchType(tBranchTypes[0]);
        tCopyLACommisionSchema.setBranchType2(tBranchTypes[1]); 
        tCopyLACommisionSchema.setBranchType3(tBranchTypes[2]);
        
        if(tPayPlanCode.equals("222222"))
        {
        	COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
        	
            mLACommisionSchema = new LACommisionSchema();
            mLACommisionSchema.setCommisionSN(COMMISIONSN);
            tReflections.transFields(mLACommisionSchema, tCopyLACommisionSchema);
            mLACommisionSchema.setTransMoney(0 -
                    java.lang.Math.abs(cLJAGetEndorseSchema.getGetMoney()));
            mLACommisionSchema.setTransState("03"); //追加保单
            mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                    getTransMoney());
            if (cLCContSchema.getSaleChnl().equals("07"))
            {
                mLACommisionSchema.setF1("02"); //职团开拓
                mLACommisionSchema.setP5(mLACommisionSchema.
                                         getTransMoney()); //职团开拓实收保费
            }
            else
            {
                mLACommisionSchema.setF1("01");
            }
            
            String tSignDate = mLACommisionSchema.getSignDate();
//          (!caldate.equals("")) {
           int z = judgeWageIsCal(cPolNo, tSignDate);
           switch (z)
           {
           case 0: //判断过程出错
               return false;
           case 1: 
           { //佣金已经计算,CommDire=1 可以参加下月佣金计算就扣除了这部分佣金
        	   String tConfDate="";
        	   tConfDate = this.queryConfDate(cLCPolSchema.getGrpContNo(), "WT", mLAWageLogSchema.getEndDate(), 
        			   cLJAGetEndorseSchema.getGetConfirmDate(), cLJAGetEndorseSchema.getActuGetNo());
        	   String tVisistTime = this.queryReturnVisit(cContNo);//查询回访日期
        	   String tRiskCode = mLACommisionSchema.getRiskCode();//险种
        	   String tRiskFlag = queryRiskFlag(tRiskCode);
        	   String tTransState = mLACommisionSchema.getTransState();
        	   String tAgentcode = mLACommisionSchema.getAgentCode();
        	   String tCalDate = this.calCalDateForAll(tBranchTypes[0], tBranchTypes[1], tSignDate, cLCContSchema.getGetPolDate(),
        			   "0", cLJAGetEndorseSchema.getMakeDate(), mLACommisionSchema.getCustomGetPolDate(), mLACommisionSchema.getCValiDate(),"02", tConfDate, AFlag, this.mManageCom,
        			   tVisistTime, tRiskFlag, tTransState, tAgentcode,mLACommisionSchema.getGrpContNo(), tRiskCode);
        	   if (!tCalDate.equals("") && !tCalDate.equals("0")) 
        	   {
                   mLACommisionSchema.setCalDate(tCalDate);
               }
        	   mLACommisionSchema.setCommDire("1");
        	   mLACommisionSchema.setFlag("1");
        	   String sql =
                       "select yearmonth from LAStatSegment where startdate<='" +
                    		   tCalDate + "' and enddate>='" + tCalDate +
                       "' and stattype='5' ";
               ExeSQL tExeSQL = new ExeSQL();
               String  tWageNo = tExeSQL.getOneValue(sql);
               mLACommisionSchema.setWageNo(tWageNo); //WageNo为下一月,下月佣金可以算到
               mLACommisionSchema.setCalDate(tCalDate);
               mLACommisionSchema.setTConfDate(tConfDate);
               break;
        	   
           }
           case 2: { //佣金尚未计算，修改原来的数据，使原先的数据不参加佣金计算，那么本条数据也不参加下月的计算
               mLACommisionSchema.setCommDire("2");
               mLACommisionSchema.setFlag("1");

               String tSQL1 =
                       "select * from LACommision where PolNo = '" +
                       mLACommisionSchema.getPolNo() +
                       "' and SignDate='" + tSignDate +
                       "' order by commisionsn with ur "; //modify :2005-11-05 xiangchun 加上order by commisionsn
               System.out.println(tSQL1);
               LACommisionDB tLACommisionDB = new LACommisionDB();
               LACommisionSet dLACommisionSet = new LACommisionSet();
               dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
               LACommisionSet eLACommisionSet = new LACommisionSet();
               System.out.println(dLACommisionSet.size());
               for (int j = 1; j <= dLACommisionSet.size(); j++) {
                   LACommisionSchema tLACommisionSchema = new
                           LACommisionSchema();
                   tLACommisionSchema = dLACommisionSet.get(j);
                   //把wageno修改为新单的wageno
                   if (j == 1) {
                       mLACommisionSchema.setWageNo(
                               tLACommisionSchema.getWageNo()); //modify :2005-11-05 xiangchun
                       mLACommisionSchema.setCalDate(
                               tLACommisionSchema.getCalDate());
                   }
                   //如果如果mLAUpdateCommisionSet没有本次退保的新单数据
                   tLACommisionSchema.setCommDire("2");
                   tLACommisionSchema.setModifyDate(PubFun.getCurrentDate());
                   tLACommisionSchema.setModifyTime(PubFun.
                               getCurrentTime());
                   eLACommisionSet.add(tLACommisionSchema);
               }
               for (int k = 1; k <= mLACommisionSet.size(); k++) {
                   boolean WageNoFlag = false; //判断WageNo是否付值
                   if (mLACommisionSet.get(k).getPolNo().
                       equals(
                               mLACommisionSchema.
                               getPolNo())) {
                       if (!WageNoFlag) {
                           mLACommisionSchema.setWageNo(
                                   mLACommisionSet.get(k).getWageNo()); //modify :2005-11-05 xiangchun
                           mLACommisionSchema.setCalDate(
                                   mLACommisionSet.get(k).getCalDate());
                       }
                       mLACommisionSet.get(k).setCommDire("2");
                       WageNoFlag = true;
                       // break;                 modify :2005-10-17 xiangchun
                   }
               }

               mLAUpdateCommisionSet.add(eLACommisionSet);
               break; 
            }
          }
          mLACommisionSet.add(mLACommisionSchema);
        }
        else
        {
        	COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");

            //填充直接佣金明细表（佣金扎账表）
            mLACommisionSchema = new LACommisionSchema();
            tReflections.transFields(mLACommisionSchema, tCopyLACommisionSchema);
            mLACommisionSchema.setCommisionSN(COMMISIONSN);
            mLACommisionSchema.setTransMoney(0 -
                    java.lang.Math.abs(cLJAGetEndorseSchema.getGetMoney()));
            mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                    getTransMoney());
            mLACommisionSchema.setTransState("00"); //正常保单
            
            String tSignDate = mLACommisionSchema.getSignDate();
//          (!caldate.equals("")) {
           int z = judgeWageIsCal(cPolNo, tSignDate);
           
           switch (z)
           {
           case 0: //判断过程出错
               return false;
           case 1: 
           { //佣金已经计算,CommDire=1 可以参加下月佣金计算就扣除了这部分佣金
        	   String tConfDate="";
        	   tConfDate = this.queryConfDate(cLCPolSchema.getGrpContNo(), "WT", mLAWageLogSchema.getEndDate(), 
        			   cLJAGetEndorseSchema.getGetConfirmDate(), cLJAGetEndorseSchema.getActuGetNo());
        	   String tVisistTime = this.queryReturnVisit(cContNo);//查询回访日期
        	   String tRiskCode = mLACommisionSchema.getRiskCode();//险种
        	   String tRiskFlag = queryRiskFlag(tRiskCode);
        	   String tTransState = mLACommisionSchema.getTransState();
        	   String tAgentcode = mLACommisionSchema.getAgentCode();
        	   String tCalDate = this.calCalDateForAll(tBranchTypes[0], tBranchTypes[1], tSignDate, cLCContSchema.getGetPolDate(),
        			   "0", cLJAGetEndorseSchema.getMakeDate(), mLACommisionSchema.getCustomGetPolDate(), mLACommisionSchema.getCValiDate(),"02", tConfDate, AFlag, this.mManageCom,
        			   tVisistTime, tRiskFlag, tTransState, tAgentcode,mLACommisionSchema.getGrpContNo(), tRiskCode);
        	   if (!tCalDate.equals("") && !tCalDate.equals("0")) 
        	   {
                   mLACommisionSchema.setCalDate(tCalDate);
               }
        	   mLACommisionSchema.setCommDire("1");
        	   mLACommisionSchema.setFlag("1");
        	   String sql =
                       "select yearmonth from LAStatSegment where startdate<='" +
                    		   tCalDate + "' and enddate>='" + tCalDate +
                       "' and stattype='5' ";
               ExeSQL tExeSQL = new ExeSQL();
               String  tWageNo = tExeSQL.getOneValue(sql);
               mLACommisionSchema.setWageNo(tWageNo); //WageNo为下一月,下月佣金可以算到
               mLACommisionSchema.setCalDate(tCalDate);
               mLACommisionSchema.setTConfDate(tConfDate);
               break;
        	   
           }
           case 2: 
           { //佣金尚未计算，修改原来的数据，使原先的数据不参加佣金计算，那么本条数据也不参加下月的计算
               mLACommisionSchema.setCommDire("2");
               mLACommisionSchema.setFlag("1");

               String tSQL1 =
                       "select * from LACommision where PolNo = '" +
                       mLACommisionSchema.getPolNo() +
                       "' and SignDate='" + tSignDate +
                       "' order by commisionsn with ur "; //modify :2005-11-05 xiangchun 加上order by commisionsn
               System.out.println(tSQL1);
               LACommisionDB tLACommisionDB = new LACommisionDB();
               LACommisionSet dLACommisionSet = new LACommisionSet();
               dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
               LACommisionSet eLACommisionSet = new LACommisionSet();
               System.out.println(dLACommisionSet.size());
               for (int j = 1; j <= dLACommisionSet.size(); j++) {
                   LACommisionSchema tLACommisionSchema = new
                           LACommisionSchema();
                   tLACommisionSchema = dLACommisionSet.get(j);
                   //把wageno修改为新单的wageno
                   if (j == 1) {
                       mLACommisionSchema.setWageNo(
                               tLACommisionSchema.getWageNo()); //modify :2005-11-05 xiangchun
                       mLACommisionSchema.setCalDate(
                               tLACommisionSchema.getCalDate());
                   }
                   //如果如果mLAUpdateCommisionSet没有本次退保的新单数据
                   tLACommisionSchema.setCommDire("2");
                   tLACommisionSchema.setModifyDate(PubFun.getCurrentDate());
                   tLACommisionSchema.setModifyTime(PubFun.
                               getCurrentTime());
                   eLACommisionSet.add(tLACommisionSchema);
               }
               for (int k = 1; k <= mLACommisionSet.size(); k++) {
                   boolean WageNoFlag = false; //判断WageNo是否付值
                   if (mLACommisionSet.get(k).getPolNo().
                       equals(
                               mLACommisionSchema.
                               getPolNo())) {
                       if (!WageNoFlag) {
                           mLACommisionSchema.setWageNo(
                                   mLACommisionSet.get(k).getWageNo()); //modify :2005-11-05 xiangchun
                           mLACommisionSchema.setCalDate(
                                   mLACommisionSet.get(k).getCalDate());
                       }
                       mLACommisionSet.get(k).setCommDire("2");
                       WageNoFlag = true;
                       // break;                 modify :2005-10-17 xiangchun
                   }
               }

               mLAUpdateCommisionSet.add(eLACommisionSet);
               break; 
            }
          }
            
            String tRiskPremSQL = "select sum(fee) from lbinsureaccfeetrace where 1=1"
					  + " and moneytype = 'RP' and othertype='10' "
					  + " and otherno='"+cLJAGetEndorseSchema.getEndorsementNo()+"'"
					  +" and edorno = '"+cLJAGetEndorseSchema.getEndorsementNo()+"' with ur";
            ExeSQL tExeSQL = new ExeSQL();
    		System.out.println("AgentWageCalSaveNewBL-->犹豫期退保查询税优sql:"+tRiskPremSQL);
    		String tResultPrem = tExeSQL.getOneValue(tRiskPremSQL);
    		if("".equals(tResultPrem)||null==tResultPrem)
    		{
    			tResultPrem = "0";
    		}
    		double tRiskPrem = Double.parseDouble(tResultPrem);
    		double tTransMoney = mLACommisionSchema.getTransMoney();
    		
    		LACommisionSchema mLACommisionSchema1 = new LACommisionSchema();
    		COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
    		tReflections.transFields(mLACommisionSchema1, mLACommisionSchema);
    		mLACommisionSchema1.setCommisionSN(COMMISIONSN);
    		//帐户保费
    		mLACommisionSchema.setTransMoney(tTransMoney-tRiskPrem);
    		mLACommisionSchema.setTransState("04");
    		mLACommisionSchema.setTransStandMoney(tTransMoney-tRiskPrem);
    		if (cLCContSchema.getSaleChnl().equals("07"))
            {
                mLACommisionSchema.setF1("02"); //职团开拓
                mLACommisionSchema.setP5(mLACommisionSchema.
                                         getTransMoney()); //职团开拓实收保费
            }
            else
            {
                mLACommisionSchema.setF1("01");
            }
    		//风险保费
    		mLACommisionSchema1.setTransMoney(tRiskPrem);
    		mLACommisionSchema1.setTransState("05");
    		mLACommisionSchema1.setTransStandMoney(tRiskPrem);
    		if (cLCContSchema.getSaleChnl().equals("07"))
            {
                mLACommisionSchema1.setF1("02"); //职团开拓
                mLACommisionSchema1.setP5(mLACommisionSchema.
                                         getTransMoney()); //职团开拓实收保费
            }
            else
            {
                mLACommisionSchema1.setF1("01");
            }
    		
    		mLACommisionSet.add(mLACommisionSchema);
    		mLACommisionSet.add(mLACommisionSchema1);
        }
    		
    	return true;
    }

      /**
       * 处理万能险种  331801
       * 区分出基本保费和额外保费
       */
     private boolean dealOmniEndorseCommision(LCContSchema tLCContSchema,LCPolSchema tLCPolSchema,LJAGetEndorseSchema tLJAGetEndorseSchema,
                                      String tContNo, String PolNo)
     {
        String COMMISIONSN="";
        //计算交费年度=保单生效日到交至日期
        int  PayYear = 0;
        String tAgentCode="";
        String tAgentGroup="";
        String tBranchCode="";
        String tBranchAttr="";
        String tPayPlanCode=tLJAGetEndorseSchema.getPayPlanCode();
        if(tPayPlanCode.equals("222222"))//追加保费的犹豫期退保
        {
           COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
           //计算交费年度=保单生效日到交至日期
           PayYear = 0;
           //填充直接佣金明细表（佣金扎账表）
           mLACommisionSchema = new LACommisionSchema();
           mLACommisionSchema.setCommisionSN(COMMISIONSN);
           mLACommisionSchema.setCommisionBaseNo("0000000000");
           //WageNo 783
           mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
           mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
           mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
           mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
           mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
           mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); // 新加的 批单号码
           mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.
                   getManageCom());
           mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
           mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.
                   getRiskCode());
           mLACommisionSchema.setRiskVersion(tLCPolSchema.
                   getRiskVersion());
           mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.
                   getDutyCode());
           mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.
                   getPayPlanCode());
           mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
           mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
           mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
           mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
           mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
           mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.
                   getActuGetNo());
           mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.
                   getGetDate());
           mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.
                   getEnterAccDate());
           mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.
                   getGetConfirmDate());
           mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.
                   getMakeDate());
           // CalcDate
           mLACommisionSchema.setCommDate(CurrentDate);
           mLACommisionSchema.setTransMoney(
                   0 -
                   java.lang.Math.abs(tLJAGetEndorseSchema.getGetMoney()));
           mLACommisionSchema.setTransState("03"); //追加保单
           mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                   getTransMoney());
           mLACommisionSchema.setLastPayToDate(tLCPolSchema.
                   getPaytoDate());
           mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                   getGetDate());
           mLACommisionSchema.setTransType("WT");
           if (tLCContSchema.getSaleChnl().equals("07")) {
               mLACommisionSchema.setF1("02"); //职团开拓
               mLACommisionSchema.setP5(mLACommisionSchema.
                                        getTransMoney()); //职团开拓实收保费
           }
           else {
               mLACommisionSchema.setF1("01");
           }
           mLACommisionSchema.setFlag("1"); //犹豫期撤件
           mLACommisionSchema.setPayYear(PayYear);
           mLACommisionSchema.setReNewCount(tRenewCount);
           mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
           mLACommisionSchema.setYears(tLCPolSchema.getYears());
           mLACommisionSchema.setPayCount(1);
           mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
           mLACommisionSchema.setGetPolDate(tLCContSchema.
                   getGetPolDate());
           mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.
                   getAgentCom());
           mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                   getAgentType());
           mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                   getAgentCode());
           String tPolType = tLCPolSchema.getStandbyFlag2();
           mLACommisionSchema.setPolType(tPolType == null ||
                                         tPolType.equals("") ?
                                         "1" : tPolType);
           mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());
           mLACommisionSchema.setP11(tLCContSchema.getAppntName());
           mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
           mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
           mLACommisionSchema.setP14(tLCPolSchema.getPrtNo()); //cg add 2004-01-13
           mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
           mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //交单日期
           mLACommisionSchema.setCustomGetPolDate(tLCContSchema.
                   getCustomGetPolDate()); //签字日期
           String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
           mLACommisionSchema.setScanDate(tScanDate);
           mLACommisionSchema.setOperator(mGlobalInput.Operator);
           mLACommisionSchema.setMakeDate(CurrentDate);
           mLACommisionSchema.setMakeTime(CurrentTime);
           mLACommisionSchema.setModifyDate(CurrentDate);
           mLACommisionSchema.setModifyTime(CurrentTime);
           mLACommisionSchema.setBranchCode(mLAAgentSchema.
                   getBranchCode());
           tAgentCode = mLACommisionSchema.getAgentCode().trim();
           tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
           if (tAgentGroup == null) {
               return false;
           }
           LABranchGroupSchema tLABranchGroupSchema;
           tLABranchGroupSchema = new LABranchGroupSchema();
           tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);

           mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                   getBranchSeries());
           String tAcType = "";
//           if (tLCPolSchema.getSaleChnl().equals("03")) {
//               //如果契约渠道为中介
//               String tAgentCom = mLACommisionSchema.getAgentCom();
//               LAComDB tLAComDB = new LAComDB();
//               tLAComDB.setAgentCom(tAgentCom);
//               if (!tLAComDB.getInfo()) {
//                   LAWageActivityLogSchema tLAWageActivityLogSchema = new
//                           LAWageActivityLogSchema();
//                   tLAWageActivityLogSchema.setAgentCode(
//                           mLAAgentSchema.getAgentCode());
//                   tLAWageActivityLogSchema.setAgentGroup(
//                           mLAAgentSchema.getAgentGroup());
//                   tLAWageActivityLogSchema.setContNo(
//                           mLACommisionSchema.
//                           getContNo());
//                   tLAWageActivityLogSchema.setDescribe(
//                           "渠道类型为中介，但是没有查到机构编码为" + tAgentCom +
//                           "的中介机构!");
//                   tLAWageActivityLogSchema.setGrpContNo(
//                           mLACommisionSchema.getGrpContNo());
//                   tLAWageActivityLogSchema.setGrpPolNo(
//                           mLACommisionSchema.
//                           getGrpPolNo());
//                   tLAWageActivityLogSchema.setMakeDate(CurrentDate);
//                   tLAWageActivityLogSchema.setMakeTime(CurrentTime);
//                   tLAWageActivityLogSchema.setManageCom(
//                           mLACommisionSchema.getManageCom());
//                   tLAWageActivityLogSchema.setOperator(mGlobalInput.
//                           Operator);
//                   tLAWageActivityLogSchema.setOtherNo("");
//                   tLAWageActivityLogSchema.setOtherNoType("");
//                   tLAWageActivityLogSchema.setPolNo(
//                           mLACommisionSchema.
//                           getPolNo());
//                   tLAWageActivityLogSchema.setRiskCode(
//                           mLACommisionSchema.
//                           getRiskCode());
//
//                   String tSerialNo = PubFun1.CreateMaxNo(
//                           "WAGEACTIVITYLOG", 12);
//                   tLAWageActivityLogSchema.setSerialNo(tSerialNo);
//                   tLAWageActivityLogSchema.setWageLogType("01");
//                   tLAWageActivityLogSchema.setWageNo(mLAWageLogSchema.
//                           getWageNo());
//                   mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
//                   continue;
//               } else {
//                   tAcType = tLAComDB.getSchema().getACType();
//               }
//           }
           //添加对于互动中介的处理
           if (tLCPolSchema.getSaleChnl().equals("15")) 
           {
           	tAcType = checkActive(mLACommisionSchema);
           	if("".equals(tAcType))
           	{
           		return false;
           	}
           }
           String[] tBranchTypes = AgentPubFun.switchSalChnl(
                   tLCPolSchema.
                   getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                   tAcType);

           tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
           if (tBranchTypes == null) {
               return false;
           }
           LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
           tLABranchGroupDB.setAgentGroup(tBranchCode);
           tLABranchGroupDB.getInfo();
           tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
           if (tBranchAttr == null) {
               return false;
           }
           mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                   getBranchSeries());
           mLACommisionSchema.setAgentGroup(mLAAgentSchema.
                   getAgentGroup());
           mLACommisionSchema.setBranchAttr(tBranchAttr);
           mLACommisionSchema.setBranchType(tBranchTypes[0]);
           mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
           mLACommisionSchema.setBranchType3(tBranchTypes[2]);
           String mon = mLACommisionSchema.getGetPolDate();
           //先根据回单日期置CalDate, WAGENO
           String tConfDate = "";
           tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),
                   "WT", mLAWageLogSchema.getEndDate(),tLJAGetEndorseSchema.
                   getGetConfirmDate(),tLJAGetEndorseSchema.
                   getActuGetNo());
	        //--查询回访成功数据
           String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
           String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
           String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
           String tTransState = mLACommisionSchema.getTransState();
           // add new 
           String tAgentcode =mLACommisionSchema.getAgentCode() ;
           
           String caldate = this.calCalDateForAll(tBranchTypes[0],
                   tBranchTypes[1],
                   mLACommisionSchema.getSignDate(),
                   tLCContSchema.getGetPolDate(), "0",
                   tLJAGetEndorseSchema.
                   getMakeDate(),
                            mLACommisionSchema.getCustomGetPolDate(),
                   mLACommisionSchema.getCValiDate(), "02",
                   tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);
           if (!caldate.equals("") && !caldate.equals("0")) {
               mLACommisionSchema.setCalDate(caldate);
           }
           String tSignDate = mLACommisionSchema.getSignDate();
//          (!caldate.equals("")) {
           int z = judgeWageIsCal(PolNo, tSignDate);
           switch (z) {
           case 0: //判断过程出错
               return false;
           case 1: { //佣金已经计算,CommDire=1 可以参加下月佣金计算就扣除了这部分佣金
               mLACommisionSchema.setCommDire("1");
               mLACommisionSchema.setFlag("1");
               String sql =
                       "select yearmonth from LAStatSegment where startdate<='" +
                       caldate + "' and enddate>='" + caldate +
                       "' and stattype='5' ";
               ExeSQL tExeSQL = new ExeSQL();
               String  tWageNo = tExeSQL.getOneValue(sql);
               mLACommisionSchema.setWageNo(tWageNo); //WageNo为下一月,下月佣金可以算到
               mLACommisionSchema.setCalDate(caldate);
               mLACommisionSchema.setTConfDate(tConfDate);
               break;
           }
           case 2: { //佣金尚未计算，修改原来的数据，使原先的数据不参加佣金计算，那么本条数据也不参加下月的计算
               mLACommisionSchema.setCommDire("2");
               mLACommisionSchema.setFlag("1");

               String tSQL1 =
                       "select * from LACommision where PolNo = '" +
                       mLACommisionSchema.getPolNo() +
                       "' and SignDate='" + tSignDate +
                       "' order by commisionsn with ur "; //modify :2005-11-05 xiangchun 加上order by commisionsn
               System.out.println(tSQL1);
               LACommisionDB tLACommisionDB = new LACommisionDB();
               LACommisionSet dLACommisionSet = new LACommisionSet();
               dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
               LACommisionSet eLACommisionSet = new LACommisionSet();
               System.out.println(dLACommisionSet.size());
               for (int j = 1; j <= dLACommisionSet.size(); j++) {
                   boolean hasFound = false;
                   LACommisionSchema tLACommisionSchema = new
                           LACommisionSchema();
                   tLACommisionSchema = dLACommisionSet.get(j);
                   //把wageno修改为新单的wageno
                   if (j == 1) {
                       mLACommisionSchema.setWageNo(
                               tLACommisionSchema.getWageNo()); //modify :2005-11-05 xiangchun
                       mLACommisionSchema.setCalDate(
                               tLACommisionSchema.getCalDate());
                   }
                   for (int k = 1; k <= mLAUpdateCommisionSet.size();
                                k++) {
                       //如果mLAUpdateCommisionSet已经有了本次退保的新单数据
                       if (mLAUpdateCommisionSet.get(k).
                           getCommisionSN().equals(
                                   tLACommisionSchema.
                                   getCommisionSN())) {
                           hasFound = true;
                           mLAUpdateCommisionSet.get(k).
                                   setCommDire("2");
                           //tLACommisionSchema.gewageno如果为空，但是可能mLAUpdateCommisionSet.get(k).getWageNo()不为空，
                           //所以需要在此出重新修改wageno和caldate

                           mLACommisionSchema.setWageNo(
                                   mLAUpdateCommisionSet.get(k).
                                   getWageNo());
                           mLACommisionSchema.setCalDate(
                                   mLAUpdateCommisionSet.get(k).
                                   getCalDate());
                           // break;                 modify :2005-10-17 xiangchun
                       }
                   }
                   System.out.println(hasFound);
                   if (hasFound) {
                       continue;
                   } else {
                       //如果如果mLAUpdateCommisionSet没有本次退保的新单数据
                       tLACommisionSchema.setCommDire("2");
                       tLACommisionSchema.setModifyDate(PubFun.
                               getCurrentDate());
                       tLACommisionSchema.setModifyTime(PubFun.
                               getCurrentTime());
                       eLACommisionSet.add(tLACommisionSchema);
                   }
               }
               for (int k = 1; k <= mLACommisionSet.size(); k++) {
                   boolean WageNoFlag = false; //判断WageNo是否付值
                   if (mLACommisionSet.get(k).getPolNo().
                       equals(
                               mLACommisionSchema.
                               getPolNo())) {
                       if (!WageNoFlag) {
                           mLACommisionSchema.setWageNo(
                                   mLACommisionSet.get(k).getWageNo()); //modify :2005-11-05 xiangchun
                           mLACommisionSchema.setCalDate(
                                   mLACommisionSet.get(k).getCalDate());
                       }
                       mLACommisionSet.get(k).setCommDire("2");
                       WageNoFlag = true;
                       // break;                 modify :2005-10-17 xiangchun
                   }
               }

               mLAUpdateCommisionSet.add(eLACommisionSet);
               break; //modify :2005-10-17 xiangchun   ///////////////////////////////////////////////////是否应该为continue
           }
           }
          mLACommisionSet.add(mLACommisionSchema);
        }
        else
        {
        COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");

        //填充直接佣金明细表（佣金扎账表）
        mLACommisionSchema = new LACommisionSchema();
        mLACommisionSchema.setCommisionSN(COMMISIONSN);
        mLACommisionSchema.setCommisionBaseNo("0000000000");

        mLACommisionSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
        mLACommisionSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        mLACommisionSchema.setContNo(tLCPolSchema.getContNo());
        mLACommisionSchema.setPolNo(tLCPolSchema.getPolNo());
        mLACommisionSchema.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
        mLACommisionSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); //新加的 批单号码
        mLACommisionSchema.setManageCom(tLJAGetEndorseSchema.getManageCom());
        mLACommisionSchema.setAppntNo(tLCContSchema.getAppntNo());
        mLACommisionSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
        mLACommisionSchema.setRiskVersion(tLCPolSchema.getRiskVersion());
        mLACommisionSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
        mLACommisionSchema.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
        mLACommisionSchema.setCValiDate(tLCPolSchema.getCValiDate());
        mLACommisionSchema.setPayIntv(tLCPolSchema.getPayIntv());
        mLACommisionSchema.setPayMode(tLCPolSchema.getPayMode());
        mLACommisionSchema.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
        mLACommisionSchema.setF5(tLCPolSchema.getInsuYearFlag());
        mLACommisionSchema.setReceiptNo(tLJAGetEndorseSchema.getActuGetNo());
        mLACommisionSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
        mLACommisionSchema.setTEnterAccDate(tLJAGetEndorseSchema.getEnterAccDate());
        mLACommisionSchema.setTConfDate(tLJAGetEndorseSchema.getGetConfirmDate());
        mLACommisionSchema.setTMakeDate(tLJAGetEndorseSchema.getMakeDate());
        mLACommisionSchema.setCommDate(CurrentDate);

        /////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////
         //万能险 总保费=基本保费+额外保费
        //用TransState标示费用类型，01-基本保费；02-额外保费
        double tprem=java.lang.Math.abs(tLJAGetEndorseSchema.getGetMoney());
        int    tpayintv=tLCPolSchema.getPayIntv();
       //第一版 
//      double tamnt=tLCPolSchema.getAmnt();        
//      double tTransMoney = calTrans(tprem, tpayintv,tamnt);
        //第二版
//      double tamnt1=getOminAmnt(tLCPolSchema);
      //第三版
      double tamnt1=CommonBL.getTBAmnt(tLCPolSchema);
//      double tTransMoney = calTrans(tprem, tpayintv,tamnt1);
      //第四版
      int tAppntInsuredAge =0;
      String tBirthDay = tLCPolSchema.getInsuredBirthday();
      tAppntInsuredAge = PubFun.calInterval(tBirthDay,this.currentDate, "Y");
      double tTransMoney=calTrans(tprem, tpayintv,tamnt1,tLCPolSchema.getRiskCode(),tAppntInsuredAge);
      System.out.println("保额-保费:"+tamnt1+"/"+tTransMoney);
        mLACommisionSchema.setTransMoney(0 - java.lang.Math.abs(tTransMoney));
        mLACommisionSchema.setTransState("01");
//            mLACommisionSchema.setTransMoney(
//                    0 - java.lang.Math.abs(tLJAGetEndorseSchema.getGetMoney()));
        mLACommisionSchema.setTransStandMoney(mLACommisionSchema.
                getTransMoney());
        //////////////////////////////////////////////////////////////////////////////

        mLACommisionSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
        mLACommisionSchema.setCurPayToDate(tLJAGetEndorseSchema.
                getGetDate());
        mLACommisionSchema.setTransType("WT");
        if(tLCContSchema.getSaleChnl().equals("07"))
        {
            mLACommisionSchema.setF1("02");//职团开拓
            mLACommisionSchema.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
        }
        else
        {
            mLACommisionSchema.setF1("01");
        }
        mLACommisionSchema.setFlag("1"); //犹豫期撤件
        mLACommisionSchema.setPayYear(PayYear);
        mLACommisionSchema.setReNewCount(tRenewCount);
        mLACommisionSchema.setPayYears(tLCPolSchema.getPayYears());
        mLACommisionSchema.setYears(tLCPolSchema.getYears());
        mLACommisionSchema.setPayCount(1);
        mLACommisionSchema.setSignDate(tLCPolSchema.getSignDate());
        mLACommisionSchema.setGetPolDate(tLCContSchema.getGetPolDate());
        mLACommisionSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
        mLACommisionSchema.setAgentType(tLJAGetEndorseSchema.
                                        getAgentType());
        mLACommisionSchema.setAgentCode(tLJAGetEndorseSchema.
                                        getAgentCode());
        String tPolType = tLCPolSchema.getStandbyFlag2();
        mLACommisionSchema.setPolType(tPolType == null ||
                                      tPolType.equals("") ?
                                      "1" : tPolType);
        mLACommisionSchema.setP1(tLCPolSchema.getManageFeeRate());
        mLACommisionSchema.setP11(tLCContSchema.getAppntName());
        mLACommisionSchema.setP12(tLCPolSchema.getInsuredNo());
        mLACommisionSchema.setP13(tLCPolSchema.getInsuredName());
        mLACommisionSchema.setP14(tLCPolSchema.getPrtNo()); //cg add 2004-01-13
        mLACommisionSchema.setP15(tLCPolSchema.getProposalNo());
        mLACommisionSchema.setMakePolDate(tLCPolSchema.getMakeDate()); //交单日期
        mLACommisionSchema.setCustomGetPolDate(tLCContSchema.
                getCustomGetPolDate()); //签字日期
        String tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
        mLACommisionSchema.setScanDate(tScanDate);
        mLACommisionSchema.setOperator(mGlobalInput.Operator);
        mLACommisionSchema.setMakeDate(CurrentDate);
        mLACommisionSchema.setMakeTime(CurrentTime);
        mLACommisionSchema.setModifyDate(CurrentDate);
        mLACommisionSchema.setModifyTime(CurrentTime);
        mLACommisionSchema.setBranchCode(mLAAgentSchema.getBranchCode());
        tAgentCode = mLACommisionSchema.getAgentCode().trim();
        tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
        if (tAgentGroup == null) {
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema;
        tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);

        mLACommisionSchema.setBranchSeries(tLABranchGroupSchema.
                getBranchSeries());
        String tAcType = "";
        /**
        if (tLCPolSchema.getSaleChnl().equals("03")) {
            //如果契约渠道为中介
            String tAgentCom = mLACommisionSchema.getAgentCom();
            LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(tAgentCom);
            if (!tLAComDB.getInfo()) {
                LAWageActivityLogSchema tLAWageActivityLogSchema = new
                        LAWageActivityLogSchema();
                tLAWageActivityLogSchema.setAgentCode(
                        mLAAgentSchema.getAgentCode());
                tLAWageActivityLogSchema.setAgentGroup(
                        mLAAgentSchema.getAgentGroup());
                tLAWageActivityLogSchema.setContNo(mLACommisionSchema.
                        getContNo());
                tLAWageActivityLogSchema.setDescribe(
                        "渠道类型为中介，但是没有查到机构编码为" + tAgentCom + "的中介机构!");
                tLAWageActivityLogSchema.setGrpContNo(
                        mLACommisionSchema.getGrpContNo());
                tLAWageActivityLogSchema.setGrpPolNo(mLACommisionSchema.
                        getGrpPolNo());
                tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                tLAWageActivityLogSchema.setManageCom(
                        mLACommisionSchema.getManageCom());
                tLAWageActivityLogSchema.setOperator(mGlobalInput.
                        Operator);
                tLAWageActivityLogSchema.setOtherNo("");
                tLAWageActivityLogSchema.setOtherNoType("");
                tLAWageActivityLogSchema.setPolNo(mLACommisionSchema.
                        getPolNo());
                tLAWageActivityLogSchema.setRiskCode(mLACommisionSchema.
                        getRiskCode());

                String tSerialNo = PubFun1.CreateMaxNo(
                        "WAGEACTIVITYLOG", 12);
                tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                tLAWageActivityLogSchema.setWageLogType("01");
                tLAWageActivityLogSchema.setWageNo(mLAWageLogSchema.
                        getWageNo());
                mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                return false;
            }
            else {
                tAcType = tLAComDB.getSchema().getACType();
            }
        }
*/
        //添加对于互动中介的处理
        if (tLCPolSchema.getSaleChnl().equals("15")) 
        {
        	tAcType = checkActive(mLACommisionSchema);
        	if("".equals(tAcType))
        	{
        		return false;
        	}
        }
        String[] tBranchTypes = AgentPubFun.switchSalChnl(
                tLCPolSchema.
                getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                tAcType);

        tBranchCode = mLACommisionSchema.getBranchCode().trim(); //所在组代码
        if (tBranchTypes == null) {
            return false;
        }
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tBranchCode);
        tLABranchGroupDB.getInfo();
        tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
        if (tBranchAttr == null) {
            return false;
        }
        mLACommisionSchema.setBranchSeries(tLABranchGroupDB.
                getBranchSeries());
        mLACommisionSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
        mLACommisionSchema.setBranchAttr(tBranchAttr);
        mLACommisionSchema.setBranchType(tBranchTypes[0]);
        mLACommisionSchema.setBranchType2(tBranchTypes[1]); 
        mLACommisionSchema.setBranchType3(tBranchTypes[2]);
        String mon = mLACommisionSchema.getGetPolDate();
                 //先根据回单日期置CalDate, WAGENO
                 String tConfDate = "";
                 tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"WT",mLAWageLogSchema.getEndDate(),
                		                tLJAGetEndorseSchema.getGetConfirmDate(),tLJAGetEndorseSchema.getActuGetNo());
 		        //--查询回访成功数据
                 String tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
                 String tRiskCode = mLACommisionSchema.getRiskCode(); // 查询出险种信息
                 String tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
                 String tTransState = mLACommisionSchema.getTransState();
                 // add new 
                 String tAgentcode =mLACommisionSchema.getAgentCode() ;
                 
                 String caldate = this.calCalDateForAll(tBranchTypes[0],
                         tBranchTypes[1],
                         mLACommisionSchema.getSignDate(),
                         tLCContSchema.getGetPolDate(), "0",
                         tLJAGetEndorseSchema.
                         getMakeDate(), mLACommisionSchema.getCustomGetPolDate(),
                         mLACommisionSchema.getCValiDate(), "02",
                         tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema.getGrpContNo(),tRiskCode);
                 if (!caldate.equals("") && !caldate.equals("0")) {
                     mLACommisionSchema.setCalDate(caldate);
                 }
                 String tSignDate = mLACommisionSchema.getSignDate();
//                     if (!caldate.equals("")) {
                 int z = judgeWageIsCal(PolNo, tSignDate);
                 switch (z) {
                 case 0: //判断过程出错
                     return false;
                 case 1: { //佣金已经计算,CommDire=1 可以参加下月佣金计算就扣除了这部分佣金
                     mLACommisionSchema.setCommDire("1");
                     mLACommisionSchema.setFlag("1");
                     String sql ="select yearmonth from LAStatSegment where startdate<='" +
                             caldate + "' and enddate>='" + caldate +
                             "' and stattype='5' ";
                     ExeSQL tExeSQL = new ExeSQL();
                     String tWageNo = tExeSQL.getOneValue(sql);
                     mLACommisionSchema.setWageNo(tWageNo); //WageNo为下一月,下月佣金可以算到
                     mLACommisionSchema.setCalDate(caldate);
                     mLACommisionSchema.setTConfDate(tConfDate);
                     break;
                 }
                 case 2: { //佣金尚未计算，修改原来的数据，使原先的数据不参加佣金计算，那么本条数据也不参加下月的计算
                     mLACommisionSchema.setCommDire("2");
                     mLACommisionSchema.setFlag("1");

                     String tSQL1 =
                             "select * from LACommision where PolNo = '" +
                             mLACommisionSchema.getPolNo() +
                             "' and SignDate='" + tSignDate +
                             "' order by commisionsn with ur "; //modify :2005-11-05 xiangchun 加上order by commisionsn
                     System.out.println(tSQL1);
                     LACommisionDB tLACommisionDB = new LACommisionDB();
                     LACommisionSet dLACommisionSet = new LACommisionSet();
                     dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
                     LACommisionSet eLACommisionSet = new LACommisionSet();
                     System.out.println(dLACommisionSet.size());
                     for (int j = 1; j <= dLACommisionSet.size(); j++) {
                         boolean hasFound = false;
                         LACommisionSchema tLACommisionSchema = new
                                 LACommisionSchema();
                         tLACommisionSchema = dLACommisionSet.get(j);
                         //把wageno修改为新单的wageno
                         if (j == 1) {
                             mLACommisionSchema.setWageNo(
                                     tLACommisionSchema.getWageNo()); //modify :2005-11-05 xiangchun
                             mLACommisionSchema.setCalDate(tLACommisionSchema.getCalDate());
                             mLACommisionSchema.setTConfDate(tConfDate);
                         }
                         for (int k = 1; k <= mLAUpdateCommisionSet.size();
                                      k++) {
                             //如果mLAUpdateCommisionSet已经有了本次退保的新单数据
                             if (mLAUpdateCommisionSet.get(k).
                                 getCommisionSN().equals(
                                         tLACommisionSchema.
                                         getCommisionSN())) {
                                 hasFound = true;
                                 mLAUpdateCommisionSet.get(k).
                                         setCommDire("2");
                                 //tLACommisionSchema.gewageno如果为空，但是可能mLAUpdateCommisionSet.get(k).getWageNo()不为空，
                                 //所以需要在此出重新修改wageno和caldate
                                 mLACommisionSchema.setTConfDate(tConfDate);
                                 mLACommisionSchema.setWageNo(mLAUpdateCommisionSet.get(k).getWageNo());
                                 mLACommisionSchema.setCalDate(mLAUpdateCommisionSet.get(k).getCalDate());
                                 // break;                 modify :2005-10-17 xiangchun
                             }
                         }
                         System.out.println(hasFound);
                         if (hasFound) {
                             continue;
                         } else {
                             //如果如果mLAUpdateCommisionSet没有本次退保的新单数据
                             tLACommisionSchema.setCommDire("2");
                             tLACommisionSchema.setModifyDate(PubFun.
                                     getCurrentDate());
                             tLACommisionSchema.setModifyTime(PubFun.
                                     getCurrentTime());
                             eLACommisionSet.add(tLACommisionSchema);
                         }
                     }
                     for (int k = 1; k <= mLACommisionSet.size(); k++) {
                         boolean WageNoFlag = false; //判断WageNo是否付值
                         if (mLACommisionSet.get(k).getPolNo().
                             equals(
                                     mLACommisionSchema.
                                     getPolNo())) {
                             if (!WageNoFlag) {
                                 mLACommisionSchema.setWageNo(
                                         mLACommisionSet.get(k).getWageNo()); //modify :2005-11-05 xiangchun
                                 mLACommisionSchema.setCalDate(mLACommisionSet.get(k).getCalDate());
                                 mLACommisionSchema.setTConfDate(tConfDate);
                             }
                             mLACommisionSet.get(k).setCommDire("2");
                             WageNoFlag = true;
                             // break;                 modify :2005-10-17 xiangchun
                         }
                     }

                     mLAUpdateCommisionSet.add(eLACommisionSet);
                     break; //modify :2005-10-17 xiangchun   ///////////////////////////////////////////////////是否应该为continue
                 }
                 }
        mLACommisionSet.add(mLACommisionSchema);

        String   COMMISIONSN1 = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
        //计算交费年度=保单生效日到交至日期
        PayYear = 0;
        //填充直接佣金明细表（佣金扎账表）
        mLACommisionSchema1 = new LACommisionSchema();
        mLACommisionSchema1.setCommisionSN(COMMISIONSN1);
        mLACommisionSchema1.setCommisionBaseNo("0000000000");

        mLACommisionSchema1.setGrpContNo(tLCPolSchema.getGrpContNo());
        mLACommisionSchema1.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        mLACommisionSchema1.setContNo(tLCPolSchema.getContNo());
        mLACommisionSchema1.setPolNo(tLCPolSchema.getPolNo());
        mLACommisionSchema1.setMainPolNo(tLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
        mLACommisionSchema1.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); // 新加的 批单号码
        mLACommisionSchema1.setManageCom(tLJAGetEndorseSchema.getManageCom());
        mLACommisionSchema1.setAppntNo(tLCContSchema.getAppntNo());
        mLACommisionSchema1.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
        mLACommisionSchema1.setRiskVersion(tLCPolSchema.getRiskVersion());
        mLACommisionSchema1.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
        mLACommisionSchema1.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
        mLACommisionSchema1.setCValiDate(tLCPolSchema.getCValiDate());
        mLACommisionSchema1.setPayIntv(tLCPolSchema.getPayIntv());
        mLACommisionSchema1.setPayMode(tLCPolSchema.getPayMode());
        mLACommisionSchema1.setF4(String.valueOf(tLCPolSchema.getInsuYear()));
        mLACommisionSchema1.setF5(tLCPolSchema.getInsuYearFlag());
        mLACommisionSchema1.setReceiptNo(tLJAGetEndorseSchema.getActuGetNo());
        mLACommisionSchema1.setTPayDate(tLJAGetEndorseSchema.getGetDate());
        mLACommisionSchema1.setTEnterAccDate(tLJAGetEndorseSchema.getEnterAccDate());
        mLACommisionSchema1.setTConfDate(tLJAGetEndorseSchema.getGetConfirmDate());
        mLACommisionSchema1.setTMakeDate(tLJAGetEndorseSchema.getMakeDate());
        mLACommisionSchema1.setCommDate(CurrentDate);


        //万能险 总保费=基本保费+额外保费
        //用TransState标示费用类型，01-基本保费；02-额外保费
        double tTransMoney1 = tprem-tTransMoney;
        mLACommisionSchema1.setTransMoney(0 - java.lang.Math.abs(tTransMoney1));
        mLACommisionSchema1.setTransState("02");
        mLACommisionSchema1.setTransStandMoney(mLACommisionSchema1.getTransMoney());
        //////////////////////////////////////////////////////////////////////////////

        mLACommisionSchema1.setLastPayToDate(tLCPolSchema.getPaytoDate());
        mLACommisionSchema1.setCurPayToDate(tLJAGetEndorseSchema.
                getGetDate());
        mLACommisionSchema1.setTransType("WT");
        if(tLCContSchema.getSaleChnl().equals("07"))
        {
            mLACommisionSchema1.setF1("02");//职团开拓
            mLACommisionSchema1.setP5( mLACommisionSchema.getTransMoney());//职团开拓实收保费
        }
        else
        {
            mLACommisionSchema1.setF1("01");
        }
        mLACommisionSchema1.setFlag("1"); //犹豫期撤件
        mLACommisionSchema1.setPayYear(PayYear);
        mLACommisionSchema1.setReNewCount(tRenewCount);
        mLACommisionSchema1.setPayYears(tLCPolSchema.getPayYears());
        mLACommisionSchema1.setYears(tLCPolSchema.getYears());
        mLACommisionSchema1.setPayCount(1);
        mLACommisionSchema1.setSignDate(tLCPolSchema.getSignDate());
        mLACommisionSchema1.setGetPolDate(tLCContSchema.getGetPolDate());
        mLACommisionSchema1.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
        mLACommisionSchema1.setAgentType(tLJAGetEndorseSchema.
                                        getAgentType());
        mLACommisionSchema1.setAgentCode(tLJAGetEndorseSchema.
                                        getAgentCode());
        tPolType = tLCPolSchema.getStandbyFlag2();
        mLACommisionSchema1.setPolType(tPolType == null ||
                                      tPolType.equals("") ?
                                      "1" : tPolType);
        mLACommisionSchema1.setP1(tLCPolSchema.getManageFeeRate());
        mLACommisionSchema1.setP11(tLCContSchema.getAppntName());
        mLACommisionSchema1.setP12(tLCPolSchema.getInsuredNo());
        mLACommisionSchema1.setP13(tLCPolSchema.getInsuredName());
        mLACommisionSchema1.setP14(tLCPolSchema.getPrtNo()); //cg add 2004-01-13
        mLACommisionSchema1.setP15(tLCPolSchema.getProposalNo());
        mLACommisionSchema1.setMakePolDate(tLCPolSchema.getMakeDate()); //交单日期
        mLACommisionSchema1.setCustomGetPolDate(tLCContSchema.
                getCustomGetPolDate()); //签字日期
        tScanDate = this.getScanDate(tLCPolSchema.getPrtNo()); //caigang 2004-09-22添加，用于提取扫描日期
        mLACommisionSchema1.setScanDate(tScanDate);
        mLACommisionSchema1.setOperator(mGlobalInput.Operator);
        mLACommisionSchema1.setMakeDate(CurrentDate);
        mLACommisionSchema1.setMakeTime(CurrentTime);
        mLACommisionSchema1.setModifyDate(CurrentDate);
        mLACommisionSchema1.setModifyTime(CurrentTime);
        mLACommisionSchema1.setBranchCode(mLAAgentSchema.getBranchCode());
        tAgentCode = mLACommisionSchema1.getAgentCode().trim();
        tAgentGroup = mLAAgentSchema.getAgentGroup(); //职级对应机构代码
        if (tAgentGroup == null) {
            return false;
        }
        tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = querytLABranchGroup(tAgentGroup);

        mLACommisionSchema1.setBranchSeries(tLABranchGroupSchema.
                getBranchSeries());
        tAcType = "";

        tBranchTypes = AgentPubFun.switchSalChnl(
                tLCPolSchema.
                getSaleChnl(), tLCPolSchema.getSaleChnlDetail(),
                tAcType);

        tBranchCode = mLACommisionSchema1.getBranchCode().trim(); //所在组代码
        if (tBranchTypes == null) {
            return false;
        }
        tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tBranchCode);
        tLABranchGroupDB.getInfo();
        tBranchAttr = tLABranchGroupDB.getBranchAttr(); //职级对应机构attr
        if (tBranchAttr == null) {
            return false;
        }
        mLACommisionSchema1.setBranchSeries(tLABranchGroupDB.
                getBranchSeries());
        mLACommisionSchema1.setAgentGroup(mLAAgentSchema.getAgentGroup());
        mLACommisionSchema1.setBranchAttr(tBranchAttr);
        mLACommisionSchema1.setBranchType(tBranchTypes[0]);
        mLACommisionSchema1.setBranchType2(tBranchTypes[1]);
        mLACommisionSchema1.setBranchType3(tBranchTypes[2]);
        mon = mLACommisionSchema1.getGetPolDate();
                 //先根据回单日期置CalDate, WAGENO
        tConfDate = "";
        tConfDate = this.queryConfDate(tLCPolSchema.getGrpContNo(),"WT",mLAWageLogSchema.getEndDate(),
        		               tLJAGetEndorseSchema.getGetConfirmDate(),tLJAGetEndorseSchema.getActuGetNo());
        
        //--查询回访成功数据
         tVisitTime = this.queryReturnVisit(tContNo);//--查询回访日期
          tRiskCode = mLACommisionSchema1.getRiskCode(); // 查询出险种信息
          tRiskFlag = queryRiskFlag(tRiskCode);         // 判断是否在回访范围内
          tTransState = mLACommisionSchema1.getTransState();
         // add new 
         tAgentcode =mLACommisionSchema.getAgentCode() ;
        caldate = this.calCalDateForAll(tBranchTypes[0],
                         tBranchTypes[1],
                         mLACommisionSchema1.getSignDate(),
                         tLCContSchema.getGetPolDate(), "0",
                         tLJAGetEndorseSchema.
                         getMakeDate(), mLACommisionSchema1.getCustomGetPolDate(),
                         mLACommisionSchema1.getCValiDate(), "02",
                         tConfDate, AFlag,this.mManageCom,tVisitTime,tRiskFlag,tTransState,tAgentcode,mLACommisionSchema1.getGrpContNo(),tRiskCode);
        if (!caldate.equals("") && !caldate.equals("0")) {
                     mLACommisionSchema1.setCalDate(caldate);
                     mLACommisionSchema1.setTConfDate(tConfDate);
                 }
        tSignDate = mLACommisionSchema1.getSignDate();
//                     if (!caldate.equals("")) {
        z = judgeWageIsCal(PolNo, tSignDate);
        switch (z) {
        case 0: //判断过程出错
                     return false;
        case 1: { //佣金已经计算,CommDire=1 可以参加下月佣金计算就扣除了这部分佣金
                     mLACommisionSchema1.setCommDire("1");
                     mLACommisionSchema1.setFlag("1");
                     String sql ="select yearmonth from LAStatSegment where startdate<='" +
                             caldate + "' and enddate>='" + caldate +
                             "' and stattype='5' ";
                     ExeSQL tExeSQL = new ExeSQL();
                     String tWageNo = tExeSQL.getOneValue(sql);
                     mLACommisionSchema1.setWageNo(tWageNo); //WageNo为下一月,下月佣金可以算到
                     mLACommisionSchema1.setCalDate(caldate);
                     mLACommisionSchema1.setTConfDate(tConfDate);
                     break;
                 }
                 case 2: { //佣金尚未计算，修改原来的数据，使原先的数据不参加佣金计算，那么本条数据也不参加下月的计算
                     mLACommisionSchema1.setCommDire("2");
                     mLACommisionSchema1.setFlag("1");

                     String tSQL1 =
                             "select * from LACommision where PolNo = '" +
                             mLACommisionSchema1.getPolNo() +
                             "' and SignDate='" + tSignDate +
                             "' order by commisionsn with ur "; //modify :2005-11-05 xiangchun 加上order by commisionsn
                     System.out.println(tSQL1);
                     LACommisionDB tLACommisionDB = new LACommisionDB();
                     LACommisionSet dLACommisionSet = new LACommisionSet();
                     dLACommisionSet = tLACommisionDB.executeQuery(tSQL1);
                     LACommisionSet eLACommisionSet = new LACommisionSet();
                     System.out.println(dLACommisionSet.size());
                     for (int j = 1; j <= dLACommisionSet.size(); j++) {
                         boolean hasFound = false;
                         LACommisionSchema tLACommisionSchema = new
                                 LACommisionSchema();
                         tLACommisionSchema = dLACommisionSet.get(j);
                         //把wageno修改为新单的wageno
                         if (j == 1) {
                             mLACommisionSchema1.setWageNo(
                                     tLACommisionSchema.getWageNo()); //modify :2005-11-05 xiangchun
                             mLACommisionSchema1.setCalDate(tLACommisionSchema.getCalDate());
                         }
                         for (int k = 1; k <= mLAUpdateCommisionSet.size();
                                      k++) {
                             //如果mLAUpdateCommisionSet已经有了本次退保的新单数据
                             if (mLAUpdateCommisionSet.get(k).
                                 getCommisionSN().equals(
                                         tLACommisionSchema.
                                         getCommisionSN())) {
                                 hasFound = true;
                                 mLAUpdateCommisionSet.get(k).
                                         setCommDire("2");
                                 //tLACommisionSchema.gewageno如果为空，但是可能mLAUpdateCommisionSet.get(k).getWageNo()不为空，
                                 //所以需要在此出重新修改wageno和caldate

                                 mLACommisionSchema1.setWageNo(mLAUpdateCommisionSet.get(k).getWageNo());
                                 mLACommisionSchema1.setCalDate(mLAUpdateCommisionSet.get(k).getCalDate());
                                 // break;                 modify :2005-10-17 xiangchun
                             }
                         }
                         System.out.println(hasFound);
                         if (hasFound) {
                             continue;
                         } else {
                             //如果如果mLAUpdateCommisionSet没有本次退保的新单数据
                             tLACommisionSchema.setCommDire("2");
                             tLACommisionSchema.setModifyDate(PubFun.
                                     getCurrentDate());
                             tLACommisionSchema.setModifyTime(PubFun.
                                     getCurrentTime());
                             eLACommisionSet.add(tLACommisionSchema);
                         }
                     }
                     for (int k = 1; k <= mLACommisionSet.size(); k++) {
                         boolean WageNoFlag = false; //判断WageNo是否付值
                         if (mLACommisionSet.get(k).getPolNo().
                             equals(
                                     mLACommisionSchema1.
                                     getPolNo())) {
                             if (!WageNoFlag) {
                                 mLACommisionSchema1.setWageNo(
                                         mLACommisionSet.get(k).getWageNo()); //modify :2005-11-05 xiangchun
                                 mLACommisionSchema1.setCalDate(mLACommisionSet.get(k).getCalDate());
                             }
                             mLACommisionSet.get(k).setCommDire("2");
                             WageNoFlag = true;
                             // break;                 modify :2005-10-17 xiangchun
                         }
                     }

                     mLAUpdateCommisionSet.add(eLACommisionSet);
                     break; //modify :2005-10-17 xiangchun   ///////////////////////////////////////////////////是否应该为continue
                 }
                 }
         mLACommisionSet.add(mLACommisionSchema1);
        }
        return true;
       }
     private String getPayYears(String GrpPolNo){
    	 ExeSQL tExeSQL=new ExeSQL();
    	 String tResult=null;
    	 String tSQL="select payyears from lcpol where grppolno='"
    		 +GrpPolNo+"'  and  grpcontno<>'00000000000000000000'" +
                     " order by modifydate desc fetch first 1 rows only with ur";
    	 tResult=tExeSQL.getOneValue(tSQL);
    	 if(tResult==null){
    		 tResult="0";
    	 }
    	 return tResult;
     }
       /**
       * 查询个险万能险种的追加保费，现在只是处理了330801 331801 331201 331301险种
       *
       * @param tPrtNo String
       * @return String
       */
     private SSRS queryOmniLJAGetEndorse() {
        SSRS tSSRS = new SSRS();
        //不能根据机构去判断BranchType,因为打散的机构已备份到备份表，利用机构查会查不到该组的数据
        String sqlStr = "select ActuGetNo,EndorsementNo,FeeOperationType,FeeFinaType,PolNo,OtherNo,OtherNoType,DutyCode,PayPlanCode "
        +" from LJAGetEndorse where FeeOperationType = 'ZB' and FeeFinaType = 'ZF'"
        //+" and GrpContNo='00000000000000000000' "
        +" and MakeDate='" + mLAWageLogSchema.getEndDate()+"' "
        +" and ManageCom = '" + mManageCom + "' ";
        //增加对于互动渠道 的处理，   yangyang   2015-2-3
//        if(mBranchType.equals("5") && mBranchType2.equals("01"))
//        {
//        	sqlStr+=" and riskcode in ('331801','332701','331501','330801','331701','332001','333901','331601','334701','334801','370201') ";
//        }
//        else
//        {
//        	sqlStr+=" and RiskCode  in ('330801','331801','332701','331201','331301','331601','331501','333901','334701','334801','370201') ";
//        }
        sqlStr+=" and RiskCode in(select riskcode from lmriskapp where risktype4='4')";
        System.out.println("查询个险万能追加保费:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) 
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tExeSQL.mErrors);
          CError tError = new CError();
          tError.moduleName = "AgentWageCalSaveNewBL";
          tError.functionName = "queryOmniLJAGetEndorse";
          tError.errorMessage = "批改补退费表查询失败!";
          this.mErrors.addOneError(tError);
          return null;
        }
        return tSSRS;
      }
     /**
      * 查询保单回访信息
      *
      * @param 
      * @return String
      */
     private String queryReturnVisit(String tContno)
     {
    	 String tVisitTime = "";
    	 String sql = "select CompleteTime from ReturnVisitTable where RETURNVISITFLAG in ('1','4') and POLICYNO = '"+tContno+"' order by makedate desc fetch first 1 rows only with ur";
    	 ExeSQL tExeSQL = new ExeSQL();
    	 tVisitTime = tExeSQL.getOneValue(sql);
    	 if (tExeSQL.mErrors.needDealError() == true) 
         {
           // @@错误处理
           this.mErrors.copyAllErrors(tExeSQL.mErrors);
           CError tError = new CError();
           tError.moduleName = "AgentWageCalSaveNewBL";
           tError.functionName = "queryReturnVisit";
           tError.errorMessage = "回访数据表查询失败!";
           this.mErrors.addOneError(tError);
           return null;
         }
    	 
    	 return tVisitTime;
     }
     

     /**
      * 查询保单回访信息
      * 目前山东访后付费只作用于 个险保障期间一年期以上产品 及 保证续保产品
      * @param 传入对应的险种号信息
      * @return String
      */
     private String queryRiskFlag(String tRiskCode){
    	 ExeSQL tExeSQL = new ExeSQL();
    	 String tFlag = "";
    	 String sql = "select 'Y' from lmriskapp where riskprop='I'  and  RiskPeriod = 'L' and riskcode = '"+tRiskCode+"' "
                     +" union "
                     +" select 'Y' from lmrisk a ,lmriskapp b where a.RnewFlag='B' and a.riskcode=b.riskcode and b.riskprop='I' and b.riskcode ='"+tRiskCode+"'";
    	 tFlag=tExeSQL.getOneValue(sql);
    	 if(tFlag==null||("").equals(tFlag))
    	 {
    		 tFlag="N";
    	 }
    	 return tFlag;
    	 
     }
     public static  void main(String[] args)
     {
//   	   LCPolDB tLCPolDB=new LCPolDB();
//   	   tLCPolDB.setPolNo("21006924937");
//   	   tLCPolDB.getInfo();
//   	   LCPolSchema tLCPolSchema=new LCPolSchema();
//   	   tLCPolSchema=tLCPolDB.getSchema();
//   	  // double tamnt=getOminAmnt(tLCPolSchema);
//   	   double tamnt=CommonBL.getTBAmnt(tLCPolSchema);
//   	   System.out.println("amnt:"+tamnt);
    	 AgentWageCalSaveNewBL tAgentWageCalSaveNewBL = new AgentWageCalSaveNewBL();
  	   System.out.println(tAgentWageCalSaveNewBL.calTrans(12000, 12, 15000, "334701", 18));
     }
     
     /**
      *  银保保单对应薪资月在原来的规则基础之上，
      *  增加一条规则：需要大于保单所在机构已经计算薪资的最大薪资月
      *  
      */
     private boolean dealBankCommision()
     {
    	 //对于过往lacommision 表数据进行调整
    	 if("3".equals(this.mBranchType)&&"01".equals(this.mBranchType2))
     	{
     		String sql = "select db2inst1.DATE_FORMAT(date(startdate + 1 month),'yyyymm') from LASTATSEGMENT where stattype = '1' " 
                  +" and char(yearmonth) =( select max(wageno) from lawagehistory where 1=1 and branchtype= '3' and branchtype2 = '01' "
                  +"and managecom = '"+this.mManageCom+"' and state = '14')";
     		
     		ExeSQL tExeSQL = new ExeSQL();
     		String tMaxNo = tExeSQL.getOneValue(sql);
     		System.out.println("计算薪资月为："+tMaxNo);
     		if(tMaxNo==null||tMaxNo.equals(""))
     		{
     			// 新成立机构数据处理：直接跳过
     			return true;
     		}else{
     			
     			// 两层循环
     			for(int i = 1;i<=this.mLACommisionSet.size();i++)
     			{
                   String tWageNo = "";
     				tWageNo =mLACommisionSet.get(i).getSchema().getWageNo();    				
     				if(tWageNo!=null&&!tWageNo.equals(""))
     				{
     				 	if(tWageNo.compareTo(tMaxNo)<0)
     				 	{   				 		
     				 		mLACommisionSet.get(i).setWageNo(tMaxNo);
     				 	}   				
     				}
     				
     			}	
     			for(int j = 1;j<=this.mLAUpdateCommisionSet.size();j++)
     			{
     				String t_wageno = "";
     				t_wageno = mLAUpdateCommisionSet.get(j).getSchema().getWageNo();
     				if(t_wageno!=null&&!t_wageno.equals(""))
     				{
     					if(t_wageno.compareTo(tMaxNo)<0)
     					{	    						 
     						mLAUpdateCommisionSet.get(j).setWageNo(tMaxNo);
     					}	
     				
     			}    			
     		  }    		
     		}
     	
      }
    	 return true;
   }
    /**
     * 添加 对于互动中介的的校验 
     * 非集团交叉互动业务：销售渠道选择互动渠道并且不勾选交叉销售的业务
     * 集团交叉互动业务：销售渠道选择互动渠道并且勾选交叉销售，交叉销售渠道为：财代健或者寿代健；交叉销售业务类型必须选择：相互代理 
     * @param contno
     * @param grpcontno
     * @return
     */
   public String checkActive(LACommisionSchema tLACommisionSchema)
   {
	   String tAcType ="";
	   String crs_chnl = "";
	   if(!"00000000000000000000".equals(tLACommisionSchema.getGrpContNo()))
	   {
		   LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		   LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		   tLCGrpContDB.setGrpContNo(tLACommisionSchema.getGrpContNo());
		   if(tLCGrpContDB.getInfo())
		   {
			   tLCGrpContSchema = tLCGrpContDB.getSchema();
			   crs_chnl = tLCGrpContSchema.getCrs_SaleChnl();
		   }
	   }
	   else
	   {
		   LCContSchema tLCContSchema = new LCContSchema();
		   LCContDB tLCContDB = new LCContDB();
		   tLCContDB.setContNo(tLACommisionSchema.getContNo());
		   if(tLCContDB.getInfo())
		   {
			   tLCContSchema = tLCContDB.getSchema();
			   crs_chnl = tLCContSchema.getCrs_SaleChnl();
		   }
	   }
	   if("".equals(crs_chnl)||null==crs_chnl)
	   {
		   tAcType = "N";
	   }
	   else
	   {
		   tAcType ="2";
	   }
	   
	   return tAcType;
   }
   /**
    * 判断实收数据是首期还是续期
    * @param cPayNo
    * @return
    */
   private String judgeDueFeeType(String cPayNo)
   {
   	ExeSQL tExeSQL  = new ExeSQL();
   	String tSQL = "select DueFeeType from ljapay where payno='"+cPayNo+"' with ur";
   	return tExeSQL.getOneValue(tSQL);
   }  
}
