package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class LAWageLogQueryBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    private String mManageCom;

    /** 业务处理相关变量 */
    /** 暂交费表 */
    private LAWageLogSet mLAWageLogSet = new LAWageLogSet();
    private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();
    private GlobalInput mGI = new GlobalInput();

    public LAWageLogQueryBL()
    {}

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---getInputData---");

        //进行业务处理
        if (!queryLAWageLog())
        {
            return false;
        }
        System.out.println("---queryLAWageLog---");

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
        this.mManageCom = (String) cInputData.getObject(1);
        mLAWageLogSchema.setSchema((LAWageLogSchema) cInputData.
                                   getObjectByObjectName("LAWageLogSchema", 0));
        mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mLAWageLogSchema == null || mGI == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ProposalQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请输入查询条件!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 查询表信息
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean queryLAWageLog()
    {
        // 保单信息
        LAWageLogDB tLAWageLogDB = mLAWageLogSchema.getDB();
        String WageYear = mLAWageLogSchema.getWageYear();
        String WageMonth = mLAWageLogSchema.getWageMonth();
        //String ManageCom=mGI.ManageCom;
        String tBranchType = mLAWageLogSchema.getBranchType();
        String sqlStr = "";
        sqlStr = "select * from LAWageLog where 1=1 ";
        sqlStr = sqlStr + " and WageYear='" + WageYear + "'";
        sqlStr = sqlStr + " and WageMonth='" + WageMonth + "'";
        sqlStr = sqlStr + " and ManageCom='" + mManageCom + "'";
        if (tBranchType.equals("0"))
        {
            sqlStr += "And BranchType in ('1','2','3')";
        }
        else
        {
            sqlStr = sqlStr + " and BranchType ='" +
                     mLAWageLogSchema.getBranchType() + "'";
        }
        sqlStr = sqlStr + " and State is not null";

        System.out.println("sqlStr=" + sqlStr);

        mLAWageLogSet = tLAWageLogDB.executeQuery(sqlStr);

        if (tLAWageLogDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAWageLogDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAWageLogQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "佣金计算日志表查询失败!";
            this.mErrors.addOneError(tError);
            mLAWageLogSet.clear();
            return false;
        }
        System.out.println("LAWageLog num=" + mLAWageLogSet.size());
        mResult.clear();
        mResult.add(mLAWageLogSet);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        try
        {
            mResult.add(mLAWageLogSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageLogQueryBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
