package com.sinosoft.lis.agentcalculate;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import java.sql.Connection;

import com.sinosoft.lis.vdb.LACommisionDBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LACommisionBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public LACommisionBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LACommisionBLS Submit...");
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLACommision(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LACommisionBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLACommision(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LACommisionBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
            tLACommisionDBSet.set((LACommisionSet) mInputData.
                                  getObjectByObjectName("LACommisionSet", 0));
            System.out.println("size:" + tLACommisionDBSet.size());
            if (!tLACommisionDBSet.delete())
            {
                // @@错误处理
                //   this.mErrors.copyAllErrors(tLACommisionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LACommisionBLS";
                tError.functionName = "saveLACommisionData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            if (!tLACommisionDBSet.insert())
            {
                // @@错误处理
                //   this.mErrors.copyAllErrors(tLACommisionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LACommisionBLS";
                tError.functionName = "saveLACommisionData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACommisionBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
