package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LACommisionBSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageLogBSchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.vschema.LACommisionBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageLogBSet;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class CommisionRollBackBL{
	  /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();
    private LACommisionBSet backupLACommisionBSet = new LACommisionBSet();
    private LAWageLogBSet mLAWageLogBSet = new LAWageLogBSet();
    private String DelLAWageHistorySQL = "";
    private String DelLAWageLogSQL = "";
    private String upLAWageHistorySQL = "";
    private String upLAWageLogSQL = "";
    private String delLACommisonSQL ="";
    private String currentDate = PubFun.getCurrentDate();//当天日期
    private String currentTime = PubFun.getCurrentTime();//当时时间
    private MMap mMap = new MMap();
    private  String StartDate = "";
    private  String mStartDate = "";
    private  String YearMonth  = "";
    private  String Month = "";
    private  String currMonth = "";
    private  String currYearMonth = "";
    private  String Day = "";
    private  String endDay = "";
    private  String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20); //一条记录生成一个转储号;
    private  String mEdorType = "10";


    public static void main(String[] args)
    {
    	CommisionRollBackBL tAgentWageCalSaveUI = new CommisionRollBackBL();
        VData tVData = new VData();
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        tLAWageLogSchema.setManageCom("86110000");
        tLAWageLogSchema.setWageYear("2006");
        tLAWageLogSchema.setWageMonth("12");
        tLAWageLogSchema.setBranchType("3");
        tLAWageLogSchema.setBranchType2("01");
        tLAWageLogSchema.setStartDate("2006-12-30");
        tLAWageLogSchema.setEndDate("2006-12-30");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        tVData.addElement(tG);

        tVData.addElement(tLAWageLogSchema);
        try
        {
            tAgentWageCalSaveUI.submitData(tVData, "INSERT||AGENTWAGE");
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        System.out.println("Error:" + tAgentWageCalSaveUI.mErrors.getFirstError());

        AgentWageCalDoUI tAgentWageCalDoUI = new AgentWageCalDoUI();
        tVData = new VData();
        tLAWageLogSchema = new LAWageLogSchema();
        tLAWageLogSchema.setManageCom("86110000");
        tLAWageLogSchema.setWageYear("2006");
        tLAWageLogSchema.setWageMonth("12");
        tLAWageLogSchema.setBranchType("3");
        tLAWageLogSchema.setBranchType2("01");
        tLAWageLogSchema.setStartDate("2006-12-30");
        tLAWageLogSchema.setEndDate("2006-12-30");

        tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        tVData.addElement(tG);

        tVData.addElement(tLAWageLogSchema);

        tAgentWageCalDoUI.submitData(tVData, "INSERT||AGENTWAGE");
        System.out.println("Error:" + tAgentWageCalSaveUI.mErrors.getFirstError());

    }
    public CommisionRollBackBL()
    {

    	System.out.println("aaaa");
    }


	private boolean getInputData(VData cInputData) {
        mLAWageLogSchema = (LAWageLogSchema) cInputData.getObjectByObjectName(
                "LAWageLogSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mLAWageLogSchema == null || mGlobalInput == null) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        this.mBranchType = mLAWageLogSchema.getBranchType();
        this.mBranchType2 = mLAWageLogSchema.getBranchType2();
        this.mManageCom = mLAWageLogSchema.getManageCom();
        StartDate = mLAWageLogSchema.getStartDate();//前台传入的起期
        YearMonth  = AgentPubFun.formatDate(StartDate,"yyyyMM");//起期年月

        Month = AgentPubFun.formatDate(StartDate,"MM");  //起期月份
        currYearMonth = AgentPubFun.formatDate(CurrentDate,"yyyyMM");//起期年月
        currMonth = AgentPubFun.formatDate(CurrentDate,"MM");//当天所在月份
        Day = AgentPubFun.formatDate(StartDate,"dd");//起期日
        endDay = PubFun.calDate(StartDate, -1, "D", null);  //起期前一天
        return true;
    }
    public boolean checkData()
    {
        System.out.println("endDay" + endDay);
        //检验回退日期是否在佣金月内
        String tSQL =
                "select max(indexcalno) from lawage where ManageCom like '" +
                mManageCom + "%'" +
                " and branchtype='" + mBranchType + "' and branchtype2='" +
                mBranchType2 + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String maxMonth = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError.buildErr(this, "佣金月查询错误！");
            return false;
        }
        if (maxMonth == null || maxMonth.equals("")) { //新系统上线
            return true;
        } else {
            if (maxMonth.compareTo(YearMonth) >= 0) {
                CError.buildErr(this, "回退起期月已经算过佣金，不能再进行提数回退！");
                return false;
            }
        }
        //从回退起期以后没有进行过提数，那么也不用做回退
        tSQL = "select startdate from  lawagelog where ManageCom like '" +
               mManageCom + "%'" +
               " and branchtype='" + mBranchType + "' and branchtype2='" +
               mBranchType2 + "'" +
               " and wageno='" + YearMonth + "' ";
        tExeSQL = new ExeSQL();
        mStartDate = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError.buildErr(this, "佣金月查询错误！");
            return false;
        }

        if (mStartDate == null || mStartDate.equals("")) {
            CError.buildErr(this, "从回退起期所在月份没有提过数，所以不用做回退！");
            return false;
        }
        return true;
    }
    public boolean submitData(VData cInputData, String cOperate) {

    	if(!getInputData(cInputData))
    	{
    		return false;
    	}
    	if(!checkData())
    	{
    		return false;
    	}
    	if(!dealDate())
    	{
    		  CError tError = new CError();
              tError.moduleName = "LAAssessInputBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
    	}
    	if(!prepareOutputData())
    	{
    		return false;
    	}
    	 System.out.println("Start CommisionRollBackBL Submit...");
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, "")) {
             this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             CError tError = new CError();
             tError.moduleName = "LAAssessInputBL";
             tError.functionName = "submitData";
             tError.errorMessage = "PubSubmit保存数据失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
    	return true;
    }

    public boolean dealDate()
    {
    	//判断输入日期,和lawagelog表中的开始日期，如果大于则为修改，小于等于则删除当月lawagelog表的数据

     if(StartDate.compareTo(mStartDate)<=0)
     {
    	 //月初一号进行回退即要删除lawagelog，lawagehistory 表中数据
    	 //备份,有可能跨月进行回退，所以要循环备份
    		 LAWageLogSet tLAWageLogSet = new LAWageLogSet();
    		 LAWageLogDB tLAWageLogDB = new LAWageLogDB();
    		 String backupSQL = "select * from LAWAGELOG where wageno>='"+YearMonth+"' and branchtype='"+mBranchType+"'" +
    		 		" and branchtype2='"+mBranchType2+"' and managecom like '"+mManageCom+"%'";
    		System.out.println(backupSQL);
    		 tLAWageLogSet = tLAWageLogDB.executeQuery(backupSQL);
    		 if(tLAWageLogSet.size()>=1)
    		 {
	        	 if(!backupLAWageLog(tLAWageLogSet))
	        	 {
	        		CError.buildErr(this, "日志备份出错！");
	        		return false;
	        	 }
    		 }
       //删除
        DelLAWageLogSQL = "DELETE from LAWAGELOG where wageno>='"+YearMonth+"' and branchtype='"+mBranchType+"'" +
        		" and branchtype2='"+mBranchType2+"' and managecom like '"+mManageCom+"%'";

       DelLAWageHistorySQL = "delete from LAWAGEHISTORY where wageno>='"+YearMonth+"' and branchtype='"+mBranchType+"'" +
        		" and branchtype2='"+mBranchType2+"' and managecom like '"+mManageCom+"%'";
     }
     else//起期为月中
     {
    	 //修改当月日志，删除以后月份日志
    	 if(YearMonth.compareTo(currYearMonth)<0)
    	 {//起期月份后的月份日志均删除 ,本月的日志进行修改
    		// 备份
    		 DelLAWageHistorySQL = "delete from LAWAGEHISTORY where wageno>'"+YearMonth+"' and branchtype='"+mBranchType+"'" +
     		" and branchtype2='"+mBranchType2+"' and managecom like '"+mManageCom+"%'";

    	      DelLAWageLogSQL = "delete from LAWAGELOG where wageno>'"+YearMonth+"' and branchtype='"+mBranchType+"'" +
      		" and branchtype2='"+mBranchType2+"' and managecom like '"+mManageCom+"%'";
     	  }
    	  upLAWageHistorySQL = "update LAWAGEHISTORY set modifydate='"+endDay+"',Operator='"+mGlobalInput.Operator+"'" +
    	  		"  where  branchtype='"+mBranchType+"'" +
     		" and branchtype2='"+mBranchType2+"' and managecom like '"+mManageCom+"%' and wageno='"+YearMonth+"'";

    	  upLAWageLogSQL = "update LAWAGELOG set enddate='"+endDay+"' , modifydate= '"+currentDate+"'," +
	     		" Operator = '"+this.mGlobalInput.Operator+"' where wageno='"+YearMonth+"' and branchtype='"+mBranchType+"'" +
	     		" and branchtype2='"+mBranchType2+"' and managecom like '"+mManageCom+"%'";
            mMap.put(upLAWageHistorySQL, "UPDATE");
            mMap.put(upLAWageLogSQL, "UPDATE");
     }
     //备份LACommision 表的数据
     LACommisionDB tLACommisionDB = new LACommisionDB();
     LACommisionSet mLACommisionSet = new LACommisionSet();
     String LACommisonSQL = "select * from lacommision where tmakedate>='"+StartDate+"' and (wageno>='"+YearMonth+"' or wageno is null)and branchtype='"+mBranchType+"'" +
     		" and branchtype2='"+mBranchType2+"' and managecom like '"+mManageCom+"%'";
     mLACommisionSet = tLACommisionDB.executeQuery(LACommisonSQL);
     if(mLACommisionSet.size()>=1)
     {
	     if(!backupLACommision(mLACommisionSet))
	     {
	    	 CError.buildErr(this, "备份扎帐表出错！");
	    	 return false;
	     }
     }
     //删除LACommision 表的数据
      delLACommisonSQL = "delete from LACOMMISION where tmakedate>='"+StartDate+"' and (wageno>='"+YearMonth+"' or wageno is null) and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"'" +
     		"and managecom like '"+mManageCom+"% '";
      mMap.put(DelLAWageHistorySQL, "DELETE");
      mMap.put(DelLAWageLogSQL, "DELETE");
      mMap.put(delLACommisonSQL, "DELETE");
    	return true;
    }
    /**
     * 提数日志表备份
     * @param cLAWageLogSchema
     * @return
     */
    public boolean backupLAWageLog(LAWageLogSet cLAWageLogSet)
    {

    	//循环备份
    	for(int i=1;i<=cLAWageLogSet.size();i++)
    	{
    	LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
    	LAWageLogBSchema tLAWageLogBSchema = new LAWageLogBSchema();
    	Reflections tReflections=new Reflections();
    	tLAWageLogSchema = cLAWageLogSet.get(i);
    	tReflections.transFields(tLAWageLogBSchema,tLAWageLogSchema);
    	System.out.println("tLAWageLogBSchema."+tLAWageLogSchema.getWageNo());
    	tLAWageLogBSchema.setEdorNo(mEdorNo);
    	tLAWageLogBSchema.setEdorType(mEdorType);
    	tLAWageLogBSchema.setModifyDate(currentDate);
    	tLAWageLogBSchema.setModifyTime(currentTime);
    	tLAWageLogBSchema.setOperator(this.mGlobalInput.Operator);
    	mLAWageLogBSet.add(tLAWageLogBSchema);
    	}
    	mMap.put(mLAWageLogBSet, "INSERT");
    	return true;
    }
    /**
     * 扎帐表备份
     */
    private boolean backupLACommision(LACommisionSet cLACommisionSet)
    {
    	//循环备份
    	for(int i=1;i<=cLACommisionSet.size(); i++)
    	{
    		LACommisionSchema tLACommisionSchema = new LACommisionSchema();
    		LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();
    		tLACommisionSchema = cLACommisionSet.get(i);
    		Reflections tReflections=new Reflections();
    		tReflections.transFields(tLACommisionBSchema,tLACommisionSchema);
    		System.out.println("tLACommisionBSchema.getWageNo()"
    				+tLACommisionBSchema.getWageNo());
    		tLACommisionBSchema.setEdorNo(mEdorNo);
    		tLACommisionBSchema.setEdorType(mEdorType);
    		tLACommisionBSchema.setModifyDate(currentDate);
    		tLACommisionBSchema.setModifyTime(currentTime);
    		tLACommisionBSchema.setOperator(this.mGlobalInput.Operator);
    		backupLACommisionBSet.add(tLACommisionBSchema);
    	}
    	mMap.put(backupLACommisionBSet, "INSERT");
    	return true;
    }

    public boolean prepareOutputData()
    {
    	 try
         {
    		 mInputData = new VData();
             mInputData.add(mMap);
         }
         catch(Exception e)
         {
             CError.buildErr(this,"提交数据失败！");
             return false;
         }
         return true;
    }
}

