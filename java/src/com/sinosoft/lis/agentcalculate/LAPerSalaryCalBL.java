/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAIndexInfoSchema;
import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAIndexInfoSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
public class LAPerSalaryCalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mIndexCalNo = "";
    private LAIndexInfoSet mLAIndexInfoSet = new LAIndexInfoSet();
    private LAWageSet mLAWageSet = new LAWageSet() ;
    private MMap mMMap = new MMap();
    private VData mInputData = new VData();
    private String mOperate;
    
    public LAPerSalaryCalBL()
    {
    }

    public static void main(String[] args)
    {
        try
        {
//      PrintStream out =
//          new PrintStream(
//          new BufferedOutputStream(
//          new FileOutputStream("AgentWageGather.out")));
//      System.setOut(out);
            System.out.println("---------------");
            GlobalInput tGlobalInput = new GlobalInput();
            tGlobalInput.ManageCom = "86";
            tGlobalInput.Operator = "aa";

            VData tVData = new VData();
            LAPerSalaryCalBL AgentWageGatherBL1 = new LAPerSalaryCalBL();
            tVData.add("8611");
            tVData.add("2");
            tVData.add("01") ;
            tVData.add("200605");
            tVData.add(tGlobalInput);
//            AgentWageGatherBL1.submitData(tVData);
            if (AgentWageGatherBL1.mErrors.needDealError())
            {
                for (int i = 0; i < AgentWageGatherBL1.mErrors.getErrorCount();
                             i++)
                {
                    System.out.println(AgentWageGatherBL1.mErrors.getError(i).
                                       moduleName);
                    System.out.println(AgentWageGatherBL1.mErrors.getError(i).
                                       functionName);
                    System.out.println(AgentWageGatherBL1.mErrors.getError(i).
                                       errorMessage);
                }
            }
            System.out.println("---------------");
            System.out.println("over");
//      out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData,String cOperate)
    {
    	mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        // 进行业务处理
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AgentWageCalDoNewBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        return true;
    }

    private boolean check()
    {System.out.println("mBranchType-----"+mBranchType);
        if(mBranchType.equals("1"))//个险按支公司进行计算
        {
        	if (this.mManageCom.length() != 8) {
                buildError("check", "机构代码长度必须为8位");
                return false;

            }
        }
        else
        {
    	if (this.mManageCom.length() != 4) {
            buildError("check", "机构代码长度必须为4位");
            return false;

        }
    	}
       
//        if ((mBranchType.equals("1") && mBranchType2.equals("01"))||(mBranchType.equals("2") && mBranchType2.equals("01"))
//		//添加对于互动渠道 的处理
//        ||(mBranchType.equals("5") && mBranchType2.equals("01"))) {
//
//            //是否正在计算
////            if (!checkState()) {
////                return false;
////            }
//            //插入状态，其他人在计算完成前不能计算
////            if (!insertState()) {
////                return false;
////            }
//
//        }
        return true;
    }
    
    private boolean prepareOutputData() {
    	mMMap.put(this.mLAIndexInfoSet, "UPDATE");
    	mMMap.put(this.mLAWageSet, "UPDATE");
    	this.mInputData.add(mMMap);
        return true;
    }
    private boolean dealData()
    {

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ManageCom", mManageCom);
        tTransferData.setNameAndValue("BranchType", mBranchType);
        tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
        tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
        //AgentWageLoopCalFYC tAgentWageLoopCalFYC = new AgentWageLoopCalFYC();
        //BankRepeatFYC tBankRepeatFYC = new BankRepeatFYC();
        VData tInputData = new VData();
        tInputData.add(mGlobalInput);
        tInputData.add(tTransferData);

        if (mBranchType.equals("1"))
        {
            //2004-08-25 蔡刚添加用于佣金计算前校验
            PubCheckField checkField1 = new PubCheckField();
            VData cInputData = new VData();
//设置计算时要用到的参数值
            tTransferData = new TransferData();
            tTransferData.setNameAndValue("IndexCalNo", mIndexCalNo);
            tTransferData.setNameAndValue("BranchType", mBranchType);
            tTransferData.setNameAndValue("BranchType2",mBranchType2) ;
            tTransferData.setNameAndValue("ManageCom", mManageCom);
            LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
            LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
            String tSql =
                    "select * from lmcheckfield where riskcode = '000000'"
                    +
                    "  and fieldname = 'AgentWageGatherBL' order by serialno asc";
            tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
            cInputData.add(tTransferData);
            System.out.println(tSql);
            cInputData.add(tLMCheckFieldSet);

            if (!checkField1.submitData(cInputData, "CKBYSET"))
            {
                System.out.println("Enter Error Field!");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
                if (checkField1.mErrors.needDealError())
                {

                    this.mErrors.copyAllErrors(checkField1.mErrors);
                    return false;
                }
                else
                {
                    VData t = checkField1.getResultMess();
                    buildError("dealData", t.get(0).toString());
                    return false;
                }
            }
            else
            {
                System.out.println("Congratulation!");
            }
            System.out.println("佣金计算前校验完毕!");
        }
//        //查询已经计算过的薪资计算指标
//        String tSQL = "select * from laindexinfo where branchtype = '"+mBranchType+"' and branchtype2='"+mBranchType2+"'" +
//		" and managecom ='"+mManageCom+"' and indexcalno ='"+mIndexCalNo+"' " +
//		" and indextype ='01'";
//        LAIndexInfoSet  tLAIndexInfoBaseSet = new LAIndexInfoSet();
//        LAIndexInfoDB tLAIndexInfoBaseDB = new LAIndexInfoDB();
//        tLAIndexInfoBaseSet= tLAIndexInfoBaseDB.executeQuery(tSQL);
        
        //查询已经薪资计算的
        String tSQL = "select * from laindexinfo where branchtype = '"+mBranchType+"' and branchtype2='"+mBranchType2+"'" +
        		" and managecom ='"+mManageCom+"' and indexcalno ='"+mIndexCalNo+"' " +
        		" and indextype ='01'";
        LAIndexInfoSet  tLAIndexInfoSet = new LAIndexInfoSet();
        LAIndexInfoDB tLAIndexInfoDB = new LAIndexInfoDB();
        tLAIndexInfoSet= tLAIndexInfoDB.executeQuery(tSQL);
        if(tLAIndexInfoSet.size()>0)
        {
        	for (int i = 1; i <= tLAIndexInfoSet.size(); i++) 
        	{
				LAIndexInfoSchema tLAIndexInfoSchema = new LAIndexInfoSchema();
				tLAIndexInfoSchema= tLAIndexInfoSet.get(i);
				String tAgentCode = tLAIndexInfoSchema.getAgentCode();
				Double tTotalTax = this.caloneTaxForAll(tAgentCode,mIndexCalNo,mManageCom,"T");
				Double tPerTax = this.calPerTaxForAll(tAgentCode,mIndexCalNo,mManageCom,"S");
				tLAIndexInfoSchema.setT18(tTotalTax);
				tLAIndexInfoSchema.setT8(tPerTax);
				LAWageDB tLAWageDB = new LAWageDB();
				tLAWageDB.setAgentCode(tAgentCode);
				tLAWageDB.setIndexCalNo(mIndexCalNo);
				if(tLAWageDB.getInfo())
				{
					LAWageSchema tLAWageSchema = new LAWageSchema();
					tLAWageSchema = tLAWageDB.getSchema();
					tLAWageSchema.setK01(tPerTax);
					tLAWageSchema.setModifyDate(PubFun.getCurrentDate());
					tLAWageSchema.setModifyTime(PubFun.getCurrentTime());
					tLAWageSchema.setOperator("wyd");
					mLAWageSet.add(tLAWageSchema);
				}
				tLAIndexInfoSchema.setOperator("wyd");
				tLAIndexInfoSchema.setModifyDate(PubFun.getCurrentDate());
				tLAIndexInfoSchema.setModifyTime(PubFun.getCurrentTime());
				mLAIndexInfoSet.add(tLAIndexInfoSchema);
			}
        }
        
       

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mManageCom = (String) cInputData.get(0);
        mBranchType = (String) cInputData.get(1);
        mBranchType2 =(String)cInputData.get(2) ;
        mIndexCalNo = (String) cInputData.get(3);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 4));
        System.out.println("mManageCom"+mManageCom);
        //每次佣金计算之前  保证LATEMPMISION与LACommision同步。
//        ExeSQL tExeSQL = new ExeSQL();
//        tExeSQL.execUpdateSQL("refresh table LATEMPMISION");
        System.out.println("mManageCom");
        return true;

    }
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "AgentWageGatherBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }
    
    private Double caloneTaxForAll(String cAgentCode,String cWageNo,String cManageCom,String cReturnType)
    {
    	Double tResult = 0.0;
    	String tAgentCode = cAgentCode == null ? "" : cAgentCode;
        String tWageNo = cWageNo == null ? "" : cWageNo;
        
        Calculator tCalculator = new Calculator();
        tCalculator.setCalCode("calone");
        tCalculator.addBasicFactor("AgentCode", tAgentCode);
        tCalculator.addBasicFactor("WageNo", tWageNo);
        tCalculator.addBasicFactor("ManageCom", mManageCom);
        tCalculator.addBasicFactor("ReturnType", "T");
        
        
         tResult = Double.valueOf(tCalculator.calculate());
    	return tResult;
    }
    private Double calPerTaxForAll(String cAgentCode,String cWageNo,String cManageCom,String cReturnType)
    {
    	Double tResult = 0.0;
    	String tAgentCode = cAgentCode == null ? "" : cAgentCode;
        String tWageNo = cWageNo == null ? "" : cWageNo;
        
        Calculator tCalculator = new Calculator();
        tCalculator.setCalCode("calper");
        tCalculator.addBasicFactor("AgentCode", tAgentCode);
        tCalculator.addBasicFactor("WageNo", tWageNo);
        tCalculator.addBasicFactor("ManageCom", mManageCom);
        tCalculator.addBasicFactor("ReturnType", "S");
        
        
         tResult = Double.valueOf(tCalculator.calculate());
    	return tResult;
    } 
}
