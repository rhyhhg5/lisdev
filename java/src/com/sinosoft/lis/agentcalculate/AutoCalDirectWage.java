package com.sinosoft.lis.agentcalculate;

import java.util.Date;
import java.text.SimpleDateFormat;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: AutoCalDirectWage </p>
 * <p>Description:执行每日的提数和直接佣金的计算工作 </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company:sinosoft </p>
 * @author liujw
 * @version 1.0
 */


public class AutoCalDirectWage {

  private String mDate=null;
  private LDComSet mLDComSet = new LDComSet();

  public AutoCalDirectWage() {
  }

  public void AutoCal()
  {
//    AutoCalDirectWage tAutoCalDirectWage = new AutoCalDirectWage();

   LDComSet tLDComSet = new LDComSet();
   tLDComSet = queryManageCom(tLDComSet);

   int size = tLDComSet.size();
   Date today = new Date();
   VData tVData = new VData();
   GlobalInput tG = new GlobalInput();
   String ManageCom="";
   String Year = getFormatDate("yyyy");
   String Month = getFormatDate("MM");
   String CalDate = getFormatDate("yyyy-MM-dd");
   //循环所有管理机构，提数并计算直接佣金
   for (int i=1; i<=size; i++)
   {
     ManageCom = tLDComSet.get(i).getComCode().trim();

     if (checkData(ManageCom))
     {
       String tCalDate = getFormatDate("yyyy-MM-dd");
       String strYesterday = PubFun.calDate(tCalDate,-1,"D","");

       //准备数据
       tG.Operator = "bgauto";
       tG.ManageCom = ManageCom;
       LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
       tLAWageLogSchema.setWageYear(strYesterday.substring(0,4) );
       tLAWageLogSchema.setWageMonth(strYesterday.substring(5,7) );
       tLAWageLogSchema.setStartDate(strYesterday);
       tLAWageLogSchema.setEndDate(strYesterday);
       //tLAWageLogSchema.setBranchType("0");  //0:表示三种展业类型的数据都提
       //1--提数
       tVData.clear();
       tVData.addElement(tG);
       tVData.addElement(ManageCom);
       tVData.addElement(tLAWageLogSchema);
       tVData.addElement(""); //Branchtype
       AgentWageCalSaveUI tAgentWageCalSaveUI = new AgentWageCalSaveUI();
       try
       {
         if (!tAgentWageCalSaveUI.submitData(tVData, "INSERT||AGENTWAGE"))
         {
           System.err.println(tAgentWageCalSaveUI.mErrors .getFirstError() ) ;
           continue;
         }
       }catch(Exception e)
       {
         e.printStackTrace(System.err ) ;
         continue;
       }
       //2--计算直接佣金
       tLAWageLogSchema = new LAWageLogSchema();
       tLAWageLogSchema.setWageYear(strYesterday.substring(0,4) );
       tLAWageLogSchema.setWageMonth(strYesterday.substring(5,7) );
       tLAWageLogSchema.setManageCom(ManageCom);
       LAWageLogDB tLAWageLogDB = tLAWageLogSchema.getDB();
       LAWageLogSet tLAWageLogSet = tLAWageLogDB.query();
       tLAWageLogSchema = tLAWageLogSet.get(1);
       tVData.clear();
       tVData.addElement(tG);
       tVData.addElement(ManageCom);
       tVData.addElement(tLAWageLogSchema);
       tVData.addElement("");  //BranchType
       AgentWageCalDoUI tAgentWageCalDoUI = new AgentWageCalDoUI();
       try
       {
         if (!tAgentWageCalDoUI.submitData(tVData, "INSERT||CALWAGE"))
         {
           System.err.println(tAgentWageCalDoUI.mErrors .getFirstError() ) ;
           continue;
         }
       }catch(Exception e)
       {
         e.printStackTrace(System.err) ;
         continue;
       }
     }
    }
  }
  public void setDate(String tDate)
  {
    mDate=tDate;
  }
  public static void main(String args[])
  {
    //获得所有管理机构

    AutoCalDirectWage tAutoCalDirectWage = new AutoCalDirectWage();

    LDComSet tLDComSet = new LDComSet();
    tLDComSet = tAutoCalDirectWage.queryManageCom(tLDComSet);

    int size = tLDComSet.size();
    Date today = new Date();
    VData tVData = new VData();
    GlobalInput tG = new GlobalInput();
    String ManageCom="";
    String Year = tAutoCalDirectWage.getFormatDate("yyyy");
    String Month = tAutoCalDirectWage.getFormatDate("MM");
    String CalDate = tAutoCalDirectWage.getFormatDate("yyyy-MM-dd");
    //循环所有管理机构，提数并计算直接佣金
    for (int i=1; i<=size; i++)
    {
      ManageCom = tLDComSet.get(i).getComCode().trim();

      if (tAutoCalDirectWage.checkData(ManageCom))
      {
        String tCalDate = tAutoCalDirectWage.getFormatDate("yyyy-MM-dd");
        String strYesterday = PubFun.calDate(tCalDate,-1,"D","");

        //准备数据
        tG.Operator = "bgauto";
        tG.ManageCom = ManageCom;
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        tLAWageLogSchema.setWageYear(strYesterday.substring(0,4) );
        tLAWageLogSchema.setWageMonth(strYesterday.substring(5,7) );
        tLAWageLogSchema.setStartDate(strYesterday);
        tLAWageLogSchema.setEndDate(strYesterday);
        //tLAWageLogSchema.setBranchType("0");  //0:表示三种展业类型的数据都提
        //1--提数
        tVData.clear();
        tVData.addElement(tG);
        tVData.addElement(ManageCom);
        tVData.addElement(tLAWageLogSchema);
        tVData.addElement(""); //Branchtype
        AgentWageCalSaveUI tAgentWageCalSaveUI = new AgentWageCalSaveUI();
        try
        {
          if (!tAgentWageCalSaveUI.submitData(tVData, "INSERT||AGENTWAGE"))
          {
            System.err.println(tAgentWageCalSaveUI.mErrors .getFirstError() ) ;
            continue;
          }
        }catch(Exception e)
        {
          e.printStackTrace(System.err ) ;
          continue;
        }
        //2--计算直接佣金
        tLAWageLogSchema = new LAWageLogSchema();
        tLAWageLogSchema.setWageYear(strYesterday.substring(0,4) );
        tLAWageLogSchema.setWageMonth(strYesterday.substring(5,7) );
        tLAWageLogSchema.setManageCom(ManageCom);
        LAWageLogDB tLAWageLogDB = tLAWageLogSchema.getDB();
        LAWageLogSet tLAWageLogSet = tLAWageLogDB.query();
        tLAWageLogSchema = tLAWageLogSet.get(1);
        tVData.clear();
        tVData.addElement(tG);
        tVData.addElement(ManageCom);
        tVData.addElement(tLAWageLogSchema);
        tVData.addElement("");  //BranchType
        AgentWageCalDoUI tAgentWageCalDoUI = new AgentWageCalDoUI();
        try
        {
          if (!tAgentWageCalDoUI.submitData(tVData, "INSERT||CALWAGE"))
          {
            System.err .println(tAgentWageCalDoUI.mErrors.getFirstError());
            continue;
          }
        }catch(Exception e)
        {
          e.printStackTrace(System.err);
          continue;
        }
      }
    }
  }

  private LDComSet queryManageCom(LDComSet cLDComSet)
  {
    String tSQL = "select * from ldcom where length(trim(comcode)) = 8 order by comcode ";
    LDComDB tLDComDB = new LDComDB();
    cLDComSet = tLDComDB.executeQuery(tSQL);
    return cLDComSet;
  }
  private String getFormatDate(String cFormat)
  {
    if (mDate!=null&&!mDate.equals("") )
    {
      SimpleDateFormat sfd = new SimpleDateFormat(cFormat);
      String FormatDate = AgentPubFun.formatDate(mDate,cFormat) ;
      return FormatDate;
    }
    else
    {
      String FormatDate = "";
      Date today = new Date();
      SimpleDateFormat sfd = new SimpleDateFormat(cFormat);
      FormatDate = sfd.format(today);
      return FormatDate;
    }
  }
  private boolean checkData(String tManageCom)
  {
    Date today = new Date();
    VData tVData = new VData();
    String Year = this.getFormatDate("yyyy");
    String Month = this.getFormatDate("MM");
    String CalDate = this.getFormatDate("yyyy-MM-dd");

    String strYesterday = PubFun.calDate(CalDate,-2,"D","");

    //循环所有管理机构，提数并计算直接佣金
    FDate tFDate = new FDate();
    //准备数据
    LAWageLogSet tLAWageLogSet = new LAWageLogSet();
    LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
    tLAWageLogSchema.setWageYear(Year);
    tLAWageLogSchema.setWageMonth(Month);
    String SQL="select to_char(max(EndDate),'YYYY-MM-dd') from lawagelog where managecom='"+tManageCom+"'";
    ExeSQL tExeSQL=new ExeSQL();
    String maxEndDate=tExeSQL.getOneValue(SQL);
    if (maxEndDate==null)
      return true;
    System.out.println("数据库查询结果String maxEndDate:"+maxEndDate);
//    Date tmaxEndDate=tFDate.getDate(maxEndDate);
//    System.out.println("转化成Date tmaxEndDate:"+tmaxEndDate);
//
//    SimpleDateFormat sfd = new SimpleDateFormat("yyyy-MM-dd");
//    maxEndDate = sfd.format(tmaxEndDate);

    System.out.println("maxEndDate:"+maxEndDate);
    System.out.println("strYesterday:"+strYesterday);
    if (!maxEndDate.equals(strYesterday))
    {
      System.out.println(Year+Month+"！机构:"+tManageCom+"日期有误:"+maxEndDate);
      CError tError = new CError();
      tError.moduleName = "LARateChargeBLS";
      tError.functionName = "check()";
      tError.errorMessage = Year+Month+"！机构:"+tManageCom+"日期有误:"+maxEndDate;
      return false;
    }
    else
      return true;
  }
}