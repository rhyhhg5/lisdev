package com.sinosoft.lis.agentcalculate;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.io.*;
public class AutoCalNDDirectWage
{
  public AutoCalNDDirectWage()
  {
  }
  private String queryEndDate()
  {

   String tSQL="select max(wageno) from lawagelog where managecom='86110000' ";
   ExeSQL tExeSQL=new ExeSQL();
   String maxWageNo=tExeSQL.getOneValue(tSQL).trim() ;
   tSQL="select to_char(enddate,'yyyy-MM-dd') from lawagelog where wageno='"+maxWageNo+"' and managecom='86110000'";
   tExeSQL=new ExeSQL();
   return tExeSQL.getOneValue(tSQL).trim()  ;
  }
  public static void main(String[] args)
  {
    try
    {
    PrintStream out=
         new PrintStream(
         new BufferedOutputStream(
         new FileOutputStream("AutoCalNDDirectWage.out")));
     System.setOut(out) ;
    AutoCalNDDirectWage autoCalNDDirectWage1 = new AutoCalNDDirectWage();
    AutoCalDirectWage autoCalDirectWage1=new AutoCalDirectWage();
    String enddate=autoCalNDDirectWage1.queryEndDate();
    String FormatToday ="";
//    args=new String[1];
//    args[0]="2004-03-07";
//    if (args!=null)
//    {
//      FormatToday =args[0];
//    }
//    else
    {
      Date today=new Date();
      SimpleDateFormat sfd = new SimpleDateFormat("yyyy-MM-dd");
      FormatToday = sfd.format(today);
    }
    enddate=PubFun.calDate(enddate,1,"D",null) ;
    while (enddate.compareTo(FormatToday)<=0 )
    {
      enddate=PubFun.calDate(enddate,1,"D",null) ;
      autoCalDirectWage1.setDate(enddate) ;
      autoCalDirectWage1.AutoCal()  ;
    }
    out.close();
  }
  catch(Exception e)
  {
    e.printStackTrace() ;
  }
  }

}