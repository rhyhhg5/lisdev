package com.sinosoft.lis.agentcalculate;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;


public class ALACommisionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LACommisionSchema mLACommisionSchema = new LACommisionSchema();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    public ALACommisionBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALACommisionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALACommisionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start ALACommisionBL Submit...");
        LACommisionBLS tLACommisionBLS = new LACommisionBLS();
        tLACommisionBLS.submitData(mInputData, cOperate);
        System.out.println("End ALACommisionBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLACommisionBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLACommisionBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALACommisionBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String COMMISIONSN = "";
        String tPolNo = "";
        String tAgentGroup = "";
        String tAgentCode = "";
        String tCommisionSN = "";
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (mOperate.equals("UPDATE||MAIN"))
        {
            //担保人信息表
            int aCount = this.mLACommisionSet.size();
            System.out.println(Integer.toString(aCount));
            for (int i = 1; i <= aCount; i++)
            {
                tCommisionSN = this.mLACommisionSet.get(i).getCommisionSN().
                               trim();
                System.out.println("tCommisionSN = " + tCommisionSN);
                if (tCommisionSN == null || tCommisionSN.equals(""))
                {
                    System.out.println("go into ");
                    COMMISIONSN = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
                    this.mLACommisionSet.get(i).setCommisionSN(COMMISIONSN);
                }
                else
                {
                    LACommisionDB tLACommisionDB = new LACommisionDB();
                    tLACommisionDB.setCommisionSN(tCommisionSN);
                    if (!tLACommisionDB.getInfo())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLACommisionDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ALACommisionBL";
                        tError.functionName = "submitDat";
                        tError.errorMessage = "数据提交失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    this.mLACommisionSchema = new LACommisionSchema();
                    this.mLACommisionSchema.setSchema(this.mLACommisionSet.get(
                            i));
                    this.mLACommisionSet.get(i).setSchema(tLACommisionDB.
                            getSchema());
                    this.mLACommisionSet.get(i).setAgentCode(this.
                            mLACommisionSchema.getAgentCode());
                    this.mLACommisionSet.get(i).setWageNo(this.
                            mLACommisionSchema.getWageNo());
                    this.mLACommisionSet.get(i).setPolNo(this.
                            mLACommisionSchema.getPolNo());
                    this.mLACommisionSet.get(i).setReceiptNo(this.
                            mLACommisionSchema.getReceiptNo());
                    this.mLACommisionSet.get(i).setCalcDate(this.
                            mLACommisionSchema.getCalcDate());
                    this.mLACommisionSet.get(i).setCommDate(this.
                            mLACommisionSchema.getCommDate());
                    this.mLACommisionSet.get(i).setTransMoney(this.
                            mLACommisionSchema.getTransMoney());
                    this.mLACommisionSet.get(i).setCommDire(this.
                            mLACommisionSchema.getCommDire());
                    this.mLACommisionSet.get(i).setPolNo(this.
                            mLACommisionSchema.getPolNo());
                    this.mLACommisionSet.get(i).setMainPolNo(this.
                            mLACommisionSchema.getMainPolNo());
                    this.mLACommisionSet.get(i).setDirectWage(this.
                            mLACommisionSchema.getDirectWage());
                    //this.mLACommisionSet.get(i).setAgentCode(this.mLACommisionSet.get(i).getAgentCode());
                    //this.mLACommisionSet.get(i).setAgentGroup(this.mLACommisionSet.get(i).getAgentGroup());
                    this.mLACommisionSet.get(i).setCValiDate(this.
                            mLACommisionSchema.getCValiDate());
                    this.mLACommisionSet.get(i).setGetPolDate(this.
                            mLACommisionSchema.getGetPolDate());
                    this.mLACommisionSet.get(i).setSignDate(this.
                            mLACommisionSchema.getSignDate());
                    this.mLACommisionSet.get(i).setLastPayToDate(this.
                            mLACommisionSchema.getLastPayToDate());
                    this.mLACommisionSet.get(i).setCurPayToDate(this.
                            mLACommisionSchema.getCurPayToDate());
                    this.mLACommisionSet.get(i).setRiskCode(this.
                            mLACommisionSchema.getRiskCode());
                    this.mLACommisionSet.get(i).setPayYear(this.
                            mLACommisionSchema.getPayYear());
                    this.mLACommisionSet.get(i).setPayIntv(this.
                            mLACommisionSchema.getPayIntv());
                }
                // change : tPolNo=this.mLACommisionSet.get(i).getPolNo().trim();
                //tAgentGroup=this.mLACommisionSet.get(i).getAgentGroup().trim();
                tAgentCode = this.mLACommisionSet.get(i).getAgentCode().trim();
                /* 获得MainPolNo*/
                /*  Change:
                         String tSQL = "select a.MainPolNo "
                              +"from lcpol a "
                              +"where a.polno = '"+tPolNo+"'";

                         ExeSQL tExeSQL1 = new ExeSQL();
                         String temp = tExeSQL1.getOneValue(tSQL);
                         //System.out.println("************mainpolno"+temp);
                         this.mLACommisionSet.get(i).setMainPolNo(temp.trim());
                 */

                /* 获得agentgroup*/
                String tSQL = "select a.agentgroup  "
                              + "from LAAgent a  "
                              + "where a.agentcode = '" + tAgentCode + "'";

                ExeSQL tExeSQL2 = new ExeSQL();
                String temp = tExeSQL2.getOneValue(tSQL);
                this.mLACommisionSet.get(i).setAgentGroup(temp.trim());
                tAgentGroup = this.mLACommisionSet.get(i).getAgentGroup().trim();

                /* 获得BranchType*/
                tSQL = "select b.BranchType "
                       + "from  labranchgroup b "
                       + "where b.agentgroup = '" + tAgentGroup + "'";

                ExeSQL tExeSQL3 = new ExeSQL();
                temp = tExeSQL3.getOneValue(tSQL);
                //System.out.println("*********branchtype"+temp);
                this.mLACommisionSet.get(i).setBranchType(temp.trim());

                tSQL = "select a.branchcode  "
                       + "from LAAgent a  "
                       + "where a.agentcode = '" + tAgentCode + "'";

                ExeSQL tExeSQL4 = new ExeSQL();
                temp = tExeSQL4.getOneValue(tSQL);
                this.mLACommisionSet.get(i).setBranchCode(temp.trim());

                //tCommisionSN=this.mLACommisionSet.get(i).getCommisionSN();
                //@LACommisionDB tLACommisionDB = new LACommisionDB();

                //COMMISIONSN=PubFun1.CreateMaxNo("COMMISIONSN","SN");
                //@tLACommisionDB.setCommisionSN(COMMISIONSN);

                //String temp = ttLACommisionSchema.getCommisionSN();
                //System.out.println("!!!!!!!!!@@@"+temp);
                /*
                         if (!tLACommisionDB.getInfo())
                         {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLACommisionDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "ALACommisionBL";
                  tError.functionName = "submitDat";
                  tError.errorMessage ="数据提交失败!";
                  this.mErrors .addOneError(tError) ;
                  return false;
                         }*/
                // System.out.println("!!!!!!!!!"+COMMISIONSN);
                /*
                 ttLACommisionSchema.setSchema(tLACommisionDB.getSchema());
                 ttLACommisionSchema.setWageNo(this.mLACommisionSet.get(i).getWageNo());
                 ttLACommisionSchema.setPolNo(this.mLACommisionSet.get(i).getPolNo());
                 ttLACommisionSchema.setReceiptNo(this.mLACommisionSet.get(i).getReceiptNo());
                 ttLACommisionSchema.setCalcDate(this.mLACommisionSet.get(i).getCalcDate());
                 ttLACommisionSchema.setCommDate(this.mLACommisionSet.get(i).getCommDate());
                 ttLACommisionSchema.setTransMoney(this.mLACommisionSet.get(i).getTransMoney());
                 ttLACommisionSchema.setCommDire(this.mLACommisionSet.get(i).getCommDire());
                 ttLACommisionSchema.setDirectWage(this.mLACommisionSet.get(i).getDirectWage());
                 ttLACommisionSchema.setAgentCode(this.mLACommisionSet.get(i).getAgentCode());
                 ttLACommisionSchema.setAgentGroup(this.mLACommisionSet.get(i).getAgentGroup());

                 ttLACommisionSchema.setCValiDate(this.mLACommisionSet.get(i).getCValiDate());
                 ttLACommisionSchema.setGetPolDate(this.mLACommisionSet.get(i).getGetPolDate());
                 ttLACommisionSchema.setSignDate(this.mLACommisionSet.get(i).getSignDate());
                         ttLACommisionSchema.setLastPayToDate(this.mLACommisionSet.get(i).getLastPayToDate());
                         ttLACommisionSchema.setCurPayToDate(this.mLACommisionSet.get(i).getCurPayToDate());
                 ttLACommisionSchema.setPayCount(this.mLACommisionSet.get(i).getPayCount());
                 */
                //@LACommisionSchema ttLACommisionSchema=new LACommisionSchema();

                //@ttLACommisionSchema.setSchema(tLACommisionDB.getSchema()) ;

                //@this.mLACommisionSet.set(i,ttLACommisionSchema);
                //this.mLACommisionSet.get(i).setCommisionSN(COMMISIONSN);


                this.mLACommisionSet.get(i).setOperator(mGlobalInput.Operator);
                this.mLACommisionSet.get(i).setMakeDate(currentDate);
                this.mLACommisionSet.get(i).setMakeTime(currentTime);
                this.mLACommisionSet.get(i).setModifyDate(currentDate);
                this.mLACommisionSet.get(i).setModifyTime(currentTime);
            }
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //    this.mLACommisionSchema.setSchema((LACommisionSchema)cInputData.getObjectByObjectName("LACommisionSchema",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLACommisionSet.set((LACommisionSet) cInputData.
                                 getObjectByObjectName("LACommisionSet", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            System.out.println("hellohellohello");
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.addElement(this.mLACommisionSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALACommisionBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
