package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.Schema;

/**
 * 批处理具体执行类掊
 * @author yangyang
 *
 */
public class AgentWageOfAbstractClass {

	/**初始化公共 方法类*/
	public AgentWageCommonFunction mAgentWageCommonFunction = new AgentWageCommonFunction();
	
	/**日期处理类*/
	public FDate mFDate = new FDate();
	
	public Reflections mReflections = new Reflections();
	
	/**提取数据类型*/
	public String mExtractDataType ="";
	
	/**表名*/
	public String mTableName = "";
	
	/**提取类型*/
	public String mDataType = "";
	
	/**提交数据时封装set*/
	MMap mMMap = new MMap();
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    public void setBaseValue(String cExtractDataType,String cTableName,String cDataType)
    {
    	this.mExtractDataType = cExtractDataType;
    	this.mTableName = cTableName;
    	this.mDataType = cDataType;
    }
    
	public boolean dealData(String cSQL,String cOperator)
	{
		return true;
	}
	
	/**
	 * 根据传入的数据，返回封装后的错误记录
	 * @param cSchema  查询结果
	 * @param cErrorCode 错误代码
	 * @param cOperator  操作者
	 * @return
	 */
	public LACommisionErrorSchema dealErrorData(Schema cSchema,String cErrorCode,String cOperator)
	{
		return mAgentWageCommonFunction.dealErrorData(cSchema, mTableName, mDataType, cErrorCode, cOperator);
	}
}
