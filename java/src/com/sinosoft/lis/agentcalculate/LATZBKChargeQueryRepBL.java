package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
public class LATZBKChargeQueryRepBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mDownloadType = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LATZBKChargeQueryRepBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
    	System.out.println("................DimissionReportBL.java begin dealData()");
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "RateCommisionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 1][18];
    	   mToExcel[0][0] = "管理机构编码";
           mToExcel[0][1] = "管理机构名称";
           mToExcel[0][2] = "险种名称";
           mToExcel[0][3] = "保单号";
           mToExcel[0][4] = "投保人";
           mToExcel[0][5] = "被保险人";
           mToExcel[0][6] = "签单日期";
           mToExcel[0][7] = "保险起期";
           mToExcel[0][8] = "保险止期";
           mToExcel[0][9] = "代理人名称";
           mToExcel[0][10] = "代理机构许可证编号";
           mToExcel[0][11] = "保费收入";
           mToExcel[0][12] = "手续费比例";
           mToExcel[0][13] = "手续费金额";
           mToExcel[0][14] = "财务凭证号";
           mToExcel[0][15] = "手续费支付对象";
           mToExcel[0][16] = "手续费支付时间";
           mToExcel[0][17] = "手续费给付号";

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
    	System.out.println("...............begin inputdate");
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "RateCommisionBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mSql=StrTool.GBKToUnicode(mSql);
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        //this.mDownloadType = (String) tf.getValueByName("downloadType");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null )
        {
            CError tError = new CError();
            tError.moduleName = "RateCommisionBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
    	LATZBKChargeQueryRepBL tLATZBKChargeQueryRepBL = new
    	LATZBKChargeQueryRepBL();
    }
}
