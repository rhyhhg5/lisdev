package com.sinosoft.lis.agentcalculate;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentprint.BankWagePayBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: </p>
 *
 * @author XX
 * @version 1.0
 */
public class LAChangeWageNoUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public LAChangeWageNoUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

    	LAChangeWageNoBL tLAChangeWageNoBL = new LAChangeWageNoBL();

        if (!tLAChangeWageNoBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLAChangeWageNoBL.mErrors);

            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
