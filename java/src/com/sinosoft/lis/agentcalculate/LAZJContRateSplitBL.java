package com.sinosoft.lis.agentcalculate;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * 考核评估计算程序(考核调整)
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  Howie
 * @version 1.0
 */
public class LAZJContRateSplitBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap = new MMap();
    private VData mOutputData = new VData();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LACommisionBSet mLACommisionBSet = new LACommisionBSet();
    private String mGrpContNo = "";
    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //处理前进行验证
        if (!check())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 验证方法
     * @return boolean
     */
    private boolean check()
    {
        return true;
    }

    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try
        {
            mMap.put(this.mLACommisionSet, "UPDATE");
            mMap.put(this.mLACommisionBSet, "INSERT");
            this.mOutputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
        LACommisionSet tLACommisionSet = new LACommisionSet();        
        int tAgentCount = mLACommisionSet.size(); //需要处理的人数
        String currentdate=PubFun.getCurrentDate();
        String currenttime=PubFun.getCurrentTime();
        //循环处理每个人的考核信息
        for (int i = 1; i <= tAgentCount; i++)
        {
        	LACommisionDB tLACommisionDB=new LACommisionDB();
        	tLACommisionDB.setCommisionSN(mLACommisionSet.get(i).getCommisionSN());
        	tLACommisionDB.getInfo();
        	LACommisionSchema tLACommisionSchema = new LACommisionSchema();        	
        	tLACommisionSchema=tLACommisionDB.getSchema();
        	LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();  
        	Reflections tReflections = new Reflections();
            tReflections.transFields(tLACommisionBSchema, tLACommisionSchema);
            String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            tLACommisionBSchema.setEdorNo(mEdorNo);
            tLACommisionBSchema.setEdorType("03");
            mLACommisionBSet.add(tLACommisionBSchema);
        	
        	tLACommisionSchema.setFYCRate(mLACommisionSet.get(i).getFYCRate());
        	tLACommisionSchema.setStandFYCRate(mLACommisionSet.get(i).getFYCRate());
        	tLACommisionSchema.setFYC(mLACommisionSet.get(i).getFYCRate()*tLACommisionSchema.getTransMoney());
        	tLACommisionSchema.setDirectWage(mLACommisionSet.get(i).getFYCRate()*tLACommisionSchema.getTransMoney());
        	tLACommisionSchema.setOperator(mGlobalInput.Operator);
        	tLACommisionSchema.setModifyDate(currentdate);
        	tLACommisionSchema.setModifyTime(currenttime);
        	
        	//tLACommisionSchema = dealAssessAgent(mLAAssessSet.get(i));
            //如果更新信息成功
            if (tLACommisionSchema != null)
            {
            	tLACommisionSet.add(tLACommisionSchema);
            }
            else
            {
                CError.buildErr(this,"更新个人考核信息失败！");
                return false;
            }
        }
        mLACommisionSet = tLACommisionSet;
        return true;
    }

   

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mLACommisionSet.set((LACommisionSet) cInputData.getObjectByObjectName("LACommisionSet", 0));
        this.mGrpContNo = (String)cInputData.getObject(2);

        if (mGlobalInput == null)
        {
            // @@错误处理
            CError.buildErr(this,"没有得到足够的信息！");
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        System.out.println(sql);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try
        {
            if (conn == null)
                return 0;
            st = conn.prepareStatement(sql);
            if (st == null)
                return 0;
            rs = st.executeQuery();
            if (rs.next())
            {
                return rs.getInt(1);
            }
            return 0;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return -1;
        } finally
        {
            try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}
        }
    }


    public static void main(String[] args)
    {      
        System.out.println("add over");
    }


}
