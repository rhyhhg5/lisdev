package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.agentcharge.LAChargeCheckBL;
import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentcalculate.BalanceReturnVisit;

/**
 * <p>Title: LAACChargePayBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author 
 * @version 1.0----2009-1-13
 */
public class LAACChargePayBL {
  //错误处理类
	public CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */ 
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAChargeSet mLAChargeSet = new LAChargeSet();
  private LAChargeSet mUpLAChargeSet = new LAChargeSet();
  private LJAGetSet mLJAGetSet=new LJAGetSet();
  private LAChargeSchema mLAChargeSchema;
  private String mManageCom="";
  private String mAgentCom="";
  private String mStartDate="";
  private String mEndDate="";
  private String mGrpContNo="";
  private String mCardNo="";
  private String mWrapCode="";
  private String mContFlag = "";
  private String mContType = "";
  private String mBatchNo ="";
  private String mOperator="";
  public LAACChargePayBL() {

  }

	public static void main(String[] args)
	{
//		String mManageCom ="86321234";
//		String mContType ="0";
//		 String serialno = PubFun.getCurrentDate2()+mManageCom.substring(0, 4)+mContType;
//    	 String prefix = PubFun1.CreateMaxNo(serialno, 3);
//    	 String mBatchNo = serialno.substring(0, 12)+mManageCom.substring(4, 8)+mContType+prefix;
//    	 System.out.println("*****:"+mBatchNo);
	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //只对于结算的数据进行校验
    if(!mOperate.equals("ALLDELAY"))
    {
//  如果是江苏中介，对于其时行 校验
    	if(!checkChargePay())
    	{
    		return false;
    	}
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();

    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAACChargePayBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      this.mErrors.clearErrors();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
      this.mOperator = this.mGlobalInput.Operator;
     if(this.mOperate.equals("SELECTPAY")||this.mOperate.equals("ALLDELAY")){
    	 this.mLAChargeSet.set( (LAChargeSet) cInputData.getObjectByObjectName("LAChargeSet",0));
    	 System.out.println("*LAChargeSet get"+mLAChargeSet.size());
    	 this.mManageCom = this.mLAChargeSet.get(1).getManageCom();
         this.mContType=(String)cInputData.getObjectByObjectName("String",0);
        
     }
     else if(this.mOperate.equals("ALLPAY")){
    	 this.mManageCom=(String)cInputData.get(1);
    	 this.mAgentCom=(String)cInputData.get(2);
    	 this.mStartDate=(String)cInputData.get(3);
    	 this.mEndDate=(String)cInputData.get(4);
    	 this.mGrpContNo=(String)cInputData.get(5);
    	 this.mCardNo=(String)cInputData.get(6);
    	 this.mWrapCode=(String)cInputData.get(7);
    	 this.mContFlag=(String)cInputData.get(8);
    	 this.mContType=(String)cInputData.get(9);
     }
     else{
    	 //System.out.println("#############");
    	 CError tError = new CError();
         tError.moduleName = "LAACChargePayBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "不支持的操作类型,请验证操作类型.";
         this.mErrors.addOneError(tError);
         return false;
     }
//   对于江苏中介机构的批次号进行重新生成
     if("8632".equals(this.mManageCom.substring(0, 4)))
     {
    	 String serialno = PubFun.getCurrentDate2()+this.mManageCom.substring(0, 4);
    	 String prefix = PubFun1.CreateMaxNo(serialno, 4);
    	 this.mBatchNo = serialno.substring(0, 12)+this.mManageCom.substring(4, 8)+prefix;
    	 System.out.println("*****:"+mBatchNo);
     }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAACChargePayBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 新增，修改传入的数据校验
   */
  private boolean check(){
	  //对结算状态的校验
	  for(int k = 1 ;k<=mLAChargeSet.size();k++)
	  {
		  String tContno = mLAChargeSet.get(k).getContNo();
		  String tRiskcode = mLAChargeSet.get(k).getRiskCode();
		  ExeSQL tExeSQL1=new ExeSQL();
		  String SQL="select chargestate from lacharge where commisionsn = '"+mLAChargeSet.get(k).getCommisionSN()+"'";
		  String tchargestate=tExeSQL1.getOneValue(SQL);
		  //System.out.println(SQL+"+++++++++++++++++++++++++++++++++++"+tchargestate);
		  if(tchargestate!=null&&!tchargestate.equals(""))
		  {  
		   if(tchargestate=="1"||tchargestate.equals("1"))
		     {
			  CError tError = new CError();
              tError.moduleName = "LAACChargePayBL";
              tError.functionName = "check";
              tError.errorMessage = "保单号为"+tContno+"险种号为"+tRiskcode+"的手续费已经结算，不能重复结算！";
              this.mErrors.addOneError(tError);
              return false;
		    }
		  }
	  }

	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAACChargePayBL.dealData........."+mOperate);
    try {
    	LAChargeDB tLAChargeDB=new LAChargeDB();
        LACommisionSet tLACommisionSet=null;
        LACommisionDB tLACommisionDB=new LACommisionDB();
        BalanceReturnVisit tBalanceReturnVisit = new BalanceReturnVisit();
        String tLimit = null;
        ExeSQL tExeSQL=new ExeSQL();
        SSRS tSSRS=new SSRS();
      if (mOperate.equals("SELECTPAY")) {
        LAChargeSet tLAChargeSet ;      
        for (int i = 1; i <= mLAChargeSet.size(); i++) {
          tLAChargeSet=new LAChargeSet();          
          String tSQL="select * from lacharge where commisionsn='"
        	  +mLAChargeSet.get(i).getCommisionSN()+"' and   charge<>0 with ur";
          System.out.println(tSQL);
          tLAChargeSet=tLAChargeDB.executeQuery(tSQL);
          LAChargeSchema mLAChargeSchema=mLAChargeSet.get(i).getSchema();
          for(int j=1;j<=tLAChargeSet.size();j++){
        	  tLAChargeSet.get(j).setChargeState("1");
        	  tLAChargeSet.get(j).setBatchNo(this.mBatchNo);
        	  tLAChargeSet.get(j).setModifyDate(CurrentDate);
        	  tLAChargeSet.get(j).setModifyTime(CurrentTime);
        	  tLAChargeSet.get(j).setOperator(this.mGlobalInput.Operator);
        	 
              // 20120305 gaoy 防止并发加锁
      		  MMap tCekMap1 = null;
      		  tCekMap1 = lockLJAGet(mLAChargeSchema);
      	      if (tCekMap1 == null)
      	      {
      	        return false;
      	      }
      	      map.add(tCekMap1);
//             // --------------------------
//      	    
        	  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
              //tLimit = PubFun.getNoLimit(tLAChargeSet.get(j).getManageCom());
                  
             // tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("ACGETNO", tLimit));
        	  tLJAGetSchema.setActuGetNo(tLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setOtherNo(mLAChargeSet.get(i).getReceiptNo());
              tLJAGetSchema.setOtherNoType("AC");
              tSQL="select * from lacommision where receiptno='"
        		  +tLAChargeSet.get(j).getReceiptNo()+"' and commisionsn='"+tLAChargeSet.get(j).getCommisionSN()+"' fetch first 1 rows only ";
        	  tLACommisionSet=tLACommisionDB.executeQuery(tSQL);
        	  
        	 
        	 if (!tBalanceReturnVisit.queryVisit(tLACommisionSet.get(1))){
        		 System.out.println("yun^<>^^<>^^<>^^<>^^<>^^<>^^<>^le");
           	     CError tError = new CError();
                 tError.moduleName = "LAACChargePayBL";
                 tError.functionName = "dealData";
                 tError.errorMessage = "当前选择数据存在未回访成功的数据，结算失败！";
                 this.mErrors.addOneError(tError);
                 return false;
        	 }
              
        	  
        	  tLJAGetSchema.setOtherNo(tLAChargeSet.get(j).getReceiptNo());
        	  tLJAGetSchema.setPayMode("12");
              tLJAGetSchema.setManageCom(tLAChargeSet.get(j).getManageCom());
              tLJAGetSchema.setAgentCom(tLAChargeSet.get(j).getAgentCom());
              tLJAGetSchema.setGetNoticeNo(tLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setAgentType(tLACommisionSet.get(1).getAgentType());
              tLJAGetSchema.setAgentCode(tLACommisionSet.get(1).getAgentCode());
              tLJAGetSchema.setAgentGroup(tLACommisionSet.get(1).getAgentGroup());
              tLJAGetSchema.setAppntNo(tLACommisionSet.get(1).getAppntNo());
              tLJAGetSchema.setSumGetMoney(tLAChargeSet.get(j).getCharge());
              tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(1).getAgentCom()));
              tLJAGetSchema.setDrawerID(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setShouldDate(CurrentDate);
              tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
              tLJAGetSchema.setMakeDate(CurrentDate);
              tLJAGetSchema.setMakeTime(CurrentTime);
              tLJAGetSchema.setModifyDate(CurrentDate);
              tLJAGetSchema.setModifyTime(CurrentTime);
              this.mLJAGetSet.add(tLJAGetSchema);
              this.mUpLAChargeSet.add(tLAChargeSet);
             
          }
        }    
        map.put(this.mUpLAChargeSet, "UPDATE");
        map.put(this.mLJAGetSet, "INSERT");
      }
      
      else if(mOperate.equals("ALLPAY")){
    	  
    	 // System.out.println("@@@@@@@@@@@");
    	  String tSQL="select * from lacharge where chargetype='51"
        	  +"' and branchtype='2' and branchtype2='02' and chargestate='0' and  charge<>0 "
        	  +" and managecom='"+this.mManageCom
        	  +"' and tmakedate between '"
        	  +this.mStartDate+"' and '"+this.mEndDate+"'";
    	 // System.out.println("oooooooooooooooooooooooo");
    	  if(this.mAgentCom!=null&&!this.mAgentCom.equals("")){
    		  tSQL+=" and agentcom='"+this.mAgentCom+"'";
    	  }
    	  //System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    	  if(this.mGrpContNo!=null&&!this.mGrpContNo.equals("")){
    		  tSQL+=" and grpcontno='"+this.mGrpContNo+"'";
    	  }
    	  //System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbb");
    	  if(this.mCardNo!=null&&!this.mCardNo.equals("")){
    		  tSQL+=" and grpcontno in (select grpcontno from licertify where cardno ='"+mCardNo+"')";
    	  }
    	  //System.out.println("cccccccccccccc");
    	  if(this.mWrapCode!=null&&!this.mWrapCode.equals("")){
    		  tSQL+=" and exists  (select '1' from lacommision where f3='"+mWrapCode+"' and commisionsn=lacharge.commisionsn)";
    	  }
    	  //System.out.println("dddddddddddddddddddddddddddddd");
    	  if(this.mContFlag!=null&&!this.mContFlag.equals(""))
    	  { //System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
    		  if("0".equals(this.mContFlag))
    		  { //System.out.println("fffffffffffffffffffffffffffffffff");
    			  tSQL+=" and exists(select 1 from ljapay where payno = lacharge.receiptno and incomeno = (select contno from lacommision where commisionsn = lacharge.commisionsn) and duefeetype = '0' )";  
    		  }else if("1".equals(this.mContFlag))
    		  { System.out.println("gggggggggggggggggggggggggg");
    			  tSQL+=" and not exists(select 1 from ljapay where payno = lacharge.receiptno and incomeno = (select contno from lacommision where commisionsn = lacharge.commisionsn) and duefeetype = '0' )";  
    		  }else{
    			 // System.out.println("hhhhhhhhhhhhh");
    		  }
    	  }
    	  if(this.mContType!=null&&!this.mContType.equals(""))
    	  {
    		  //System.out.println("iiiiiiiiiiiiiiiiiiiiiiiii");
    		  if("0".equals(this.mContType))
    		  {//System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjjjj");
    			  tSQL+=" and exists(select 1 from lccont where grpcontno= lacharge.contno and ContType='2' union select 1 from lbcont where grpcontno= lacharge.contno and ContType='2' union select 1 from lpcont where grpcontno= lacharge.contno and ContType='2')" ;  
    		  }else if("1".equals(this.mContType))
    		  {//System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkk");
    			  tSQL+=" and exists(select 1 from lccont where contno= lacharge.contno and ContType='1' union select 1 from lbcont where contno= lacharge.contno and ContType='1' union select 1 from lpcont where grpcontno= lacharge.contno and ContType='1')" ; 
    		  }else{
    			  //System.out.println("lllllllllllllllllllllllllll");
    		  }
    	  }
    	  tSQL+=" with ur";
    	  //System.out.println("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
    	  System.out.println(tSQL);
    	  this.mUpLAChargeSet=tLAChargeDB.executeQuery(tSQL);
    	  for(int j=1;j<=mUpLAChargeSet.size();j++){
    		 // System.out.println("nnnnnnnnnnnnnnnnnnnnnnnnnn");
    		  LAChargeSchema mmLAChargeSchema=mUpLAChargeSet.get(j).getSchema();
    		  mmLAChargeSchema.setChargeState("1");
    		  mmLAChargeSchema.setBatchNo(this.mBatchNo);
    		  mmLAChargeSchema.setModifyDate(CurrentDate);
    		  mmLAChargeSchema.setModifyTime(CurrentTime);
    		  mmLAChargeSchema.setOperator(this.mGlobalInput.Operator);
    		  
              // 20120305 gaoy 防止并发加锁
      		  MMap tCekMap1 = null;
      		  tCekMap1 = lockLJAGet(mmLAChargeSchema);
      	      if (tCekMap1 == null)
      	      {
      	        return false;
      	      }
      	      map.add(tCekMap1);
             // --------------------------
    		  
    		  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
              //tLimit = PubFun.getNoLimit(mUpLAChargeSet.get(j).getManageCom());
             // tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("ACGETNO", tLimit));
    		  tLJAGetSchema.setActuGetNo(mUpLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setOtherNo(mUpLAChargeSet.get(j).getReceiptNo());
              tLJAGetSchema.setOtherNoType("AC");
              tSQL="select * from lacommision "
            	  +"where receiptno='" +mUpLAChargeSet.get(j).getReceiptNo()+"'"
            	  +" and  commisionsn='"+mUpLAChargeSet.get(j).getCommisionSN()+"' "
            	  +" fetch first 1 rows only ";
              tLACommisionSet=tLACommisionDB.executeQuery(tSQL);
              
        
              if (!tBalanceReturnVisit.queryVisit(tLACommisionSet.get(1))){
            	  System.out.println("yun^<>^^<>^^<>^^<>^^<>^^<>^^<>^le");
            	  CError tError = new CError();
                  tError.moduleName = "LAACChargePayBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "当前选择数据存在未回访成功的数据，结算失败！";
                  this.mErrors.addOneError(tError);
                  return false;
         	 }
              
              tLJAGetSchema.setPayMode("12");
              tLJAGetSchema.setManageCom(mUpLAChargeSet.get(j).getManageCom());
              tLJAGetSchema.setAgentCom(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setGetNoticeNo(mUpLAChargeSet.get(j).getCommisionSN());
              tLJAGetSchema.setAgentType(tLACommisionSet.get(1).getAgentType());
              tLJAGetSchema.setAgentCode(tLACommisionSet.get(1).getAgentCode());
              tLJAGetSchema.setAgentGroup(tLACommisionSet.get(1).getAgentGroup());
              tLJAGetSchema.setAppntNo(tLACommisionSet.get(1).getAppntNo());
              tLJAGetSchema.setSumGetMoney(mUpLAChargeSet.get(j).getCharge());
              tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(1).getAgentCom()));
              tLJAGetSchema.setDrawerID(tLACommisionSet.get(1).getAgentCom());
              tLJAGetSchema.setShouldDate(CurrentDate);
              tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
              tLJAGetSchema.setMakeDate(CurrentDate);
              tLJAGetSchema.setMakeTime(CurrentTime);
              tLJAGetSchema.setModifyDate(CurrentDate);
              tLJAGetSchema.setModifyTime(CurrentTime);
              this.mLJAGetSet.add(tLJAGetSchema);
              this.mLAChargeSet.add(mmLAChargeSchema);
              
    	  }
    	map.put(mLAChargeSet, "UPDATE");
    	map.put(mLJAGetSet, "INSERT");
      }
      else if (mOperate.equals("ALLDELAY")) {
          LAChargeSet tLAChargeSet ;      
          for (int i = 1; i <= mLAChargeSet.size(); i++) {
            tLAChargeSet=new LAChargeSet();          
           String SQL="select * from lacharge where commisionsn='"+mLAChargeSet.get(i).getCommisionSN()+"'  with ur";
            System.out.println(SQL);
            tLAChargeSet=tLAChargeDB.executeQuery(SQL);
            for(int j=1;j<=tLAChargeSet.size();j++){
          	  tLAChargeSet.get(j).setChargeState("X");
          	  tLAChargeSet.get(j).setModifyDate(CurrentDate);
          	  tLAChargeSet.get(j).setModifyTime(CurrentTime);
          	  tLAChargeSet.get(j).setOperator(this.mGlobalInput.Operator);
            }
            this.mUpLAChargeSet.add(tLAChargeSet);            	  
          }
          map.put(this.mUpLAChargeSet, "UPDATE");

        }
      else{
    	  System.out.println("&&&&&&&&&&&&&&&&");
    	  CError tError = new CError();
          tError.moduleName = "LAACChargePayBL";
          tError.functionName = "dealData";
          tError.errorMessage = "不支持的操作类型,请验证操作类型.";
          this.mErrors.addOneError(tError);
          return false;
      }
    }
    catch (Exception ex) {
      // @@错误处理
    	ex.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "LAACChargePayBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAACChargePayBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  private String getAgentComName(String tAgentCom){
	  String comName = "";
	  ExeSQL tExeSQL=new ExeSQL();
	  String sql=" select name from lacom where agentcom='"+tAgentCom+"' with ur ";
	  SSRS tSSRS = tExeSQL.execSQL(sql);	    	 
	  comName = tSSRS.GetText(1, 1);
	  return comName ;
  }
  private MMap lockLJAGet(LAChargeSchema tLAChargeSchema)
  {
  	
  	
      MMap tmap = null;
      /**锁定标志"TX"*/
      String tLockNoType = "XS";
      /**锁定时间*/
      String tAIS = "900";
      System.out.println("设置了15分的解锁时间");
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("LockNoKey", tLAChargeSchema.getCommisionSN());
      tTransferData.setNameAndValue("LockNoType", tLockNoType);
      tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
     
      VData tVData = new VData();
      tVData.add(tTransferData);
      tVData.add(mGlobalInput);
      LockTableActionBL tLockTableActionBL = new LockTableActionBL();
      tmap = tLockTableActionBL.getSubmitMap(tVData, null);
      
      if (tmap == null)
      {           	
          mErrors.copyAllErrors(tLockTableActionBL.mErrors);
          return null;
      }
      return tmap;
  }
  private boolean checkChargePay()
  {
	//江苏中介手续费计算时会进行校验
      if("8632".equals(mManageCom.substring(0,4))&&mUpLAChargeSet.size()>0)
      {
    	  LAChargeCheckBL tLAChargeCheckBL = new LAChargeCheckBL();
      	//手续费校验
//      	String zChargeType = "ZX";
//      	VData zVData = new VData();
//      	zVData.add(mUpLAChargeSet);
//      	zVData.add(zChargeType);
//      	zVData.add(this.mBatchNo);
//      	zVData.add(this.mOperator);
//      	if(!tLAChargeCheckBL.submitData(zVData,""))
//      	{
//      		this.mErrors.copyAllErrors(tLAChargeCheckBL.mErrors);
//      		CError tError = new CError();
//				tError.moduleName = "LAARiskChargePayBL";
//				tError.functionName = "dealData";
//				tError.errorMessage = "江苏中介手续费校验出错。";
//				this.mErrors.addOneError(tError);
//      		return false;
//      	}
  		String hChargeType = "HX";
      	VData hVData = new VData();
      	hVData.add(mUpLAChargeSet);
      	hVData.add(hChargeType);
      	hVData.add(this.mBatchNo);
      	hVData.add(this.mOperator);
      	hVData.add(this.mContType);
  		if(!tLAChargeCheckBL.submitData(hVData, ""))
  		{
  			this.mErrors.copyAllErrors(tLAChargeCheckBL.mErrors);
  			CError tError = new CError();
			tError.moduleName = "LAARiskChargePayBL";
			tError.functionName = "dealData";
			tError.errorMessage = "江苏中介手续费结算出错。";
			this.mErrors.addOneError(tError);
      		return false;
  		}
      }
      return true;
  }
  
}
