package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LPDiskImportDB;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LPDiskImportSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LPDiskImportSet;
import com.sinosoft.lis.vschema.LPEdorEspecialDataSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

/**
 * 处理团单减人（非无名单）处理类
 * @author yangyang 2017-9-22
 *
 */
public class AgentWageOfLJAGetEndorseGrpZT extends AgentWageOfAbstractClass{

	public LACommisionErrorSchema mLACommisionErrorSchema = new LACommisionErrorSchema();
	
	public LACommisionNewSchema mLACommisionNewSchema = new LACommisionNewSchema(); 
	public double mSumMoney1 = 0;
	public double mSumMoney2 = 0;
	public double mSumMoney3 = 0;
	public double mSumManageFee = 0;
	public LJAGetEndorseSchema mCurrLAGetEndorseSchema = new LJAGetEndorseSchema();
	
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfLJAGetEndorseGrpZT-->dealData:开始数据处理");
		System.out.println("AgentWageOfLJAGetEndorseGrpZT-->dealData:cSQL"+cSQL);
		LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAGetEndorseSet, cSQL);
		double tSumMoney1 = 0;
		double tSumMoney2 = 0;
		double tSumMoney3 = 0;
		double tSumManageFee = 0;
		LJAGetEndorseSchema tCurrLJAGetEndorseSchema = new LJAGetEndorseSchema();
		//最后一条数据的判断标记
		boolean tEndDataFlag = false;
		//用来判断数据是不是为空，为了防止 最后一次循环时，为空时，也打印错误语句  
		boolean tJudgeNullFlag = false;
		do
		{
			tRSWrapper.getData();
			if(!tJudgeNullFlag)
			{
				if(null==tLJAGetEndorseSet||0==tLJAGetEndorseSet.size())
				{
					System.out.println("AgentWageOfLJAGetEndorseGrpZT-->dealData do 循环没有满足条件的数据");
					return true;
				}
				tJudgeNullFlag = true;
			}
			if(null == mCurrLAGetEndorseSchema.getActuGetNo())
			{
				mCurrLAGetEndorseSchema = tLJAGetEndorseSet.get(1);
			}
			if(!tEndDataFlag&&0==tLJAGetEndorseSet.size())
			{
				tEndDataFlag = true;
				tLJAGetEndorseSet.add(mCurrLAGetEndorseSchema);
			}
			MMap tMMap = new MMap();
			LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
			LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
			for(int i =1;i<=tLJAGetEndorseSet.size();i++)
			{
				LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
				tLJAGetEndorseSchema = tLJAGetEndorseSet.get(i);
				String tNewEndorsementNo = tLJAGetEndorseSchema.getEndorsementNo();
				String tNewGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
				//添加标记，用来表示是否是不同类型的数据
				boolean tDiffentFlag = false;
				double tTransMoney = tLJAGetEndorseSchema.getGetMoney();
				//管理费
				double tManageFee = mAgentWageCommonFunction.getCTManageFee(tLJAGetEndorseSchema.getPolNo(), tLJAGetEndorseSchema.getEndorsementNo(), tLJAGetEndorseSchema.getOtherNoType());
				if(!mCurrLAGetEndorseSchema.getEndorsementNo().equals(tNewEndorsementNo)
						||!mCurrLAGetEndorseSchema.getGrpPolNo().equals(tNewGrpPolNo))
				{
					tSumMoney1 = mSumMoney1;
					tSumMoney2 = mSumMoney2;
					tSumMoney3 = mSumMoney3;
					tSumManageFee = mSumManageFee;
					tCurrLJAGetEndorseSchema = mCurrLAGetEndorseSchema;
					tDiffentFlag = true;
					//重新初始化数据，
					mCurrLAGetEndorseSchema = tLJAGetEndorseSchema;
					mSumMoney1 = 0;
					mSumMoney2 = 0;
					mSumMoney3 = 0;
					mSumManageFee = 0;
				}
				if(tEndDataFlag)
				{
					tSumMoney1 = mSumMoney1;
					tSumMoney2 = mSumMoney2;
					tSumMoney3 = mSumMoney3;
					tSumManageFee = mSumManageFee;
					tCurrLJAGetEndorseSchema = mCurrLAGetEndorseSchema;
					tDiffentFlag = true;
				}
				else
				{
					String tPremType = judgePremType(tLJAGetEndorseSchema);
					if("-0".equals(tPremType))
					{
						mLACommisionErrorSchema = dealErrorData(tLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_GRPCT01,cOperator);
						continue;
					}
					//处理非导入保费,需要计算fyc
					if("1".equals(tPremType))
					{
						mSumMoney1 += tTransMoney;
					}
					//处理非导入保费,不需要计算fyc
					if("2".equals(tPremType))
					{
						mSumMoney2 += tTransMoney;
					}
					//处理导入保费,不需要计算fyc
					if("3".equals(tPremType))
					{
						mSumMoney3 += tTransMoney;
					}
					mSumManageFee+=tManageFee;
				}
				if(tDiffentFlag)
				{
					//处理非导入保费,需要计算fyc
					if(0!=tSumMoney1)
					{
						boolean tReturnFlag = dealcutCommision(tCurrLJAGetEndorseSchema, tSumMoney1, tSumManageFee, "1", cOperator);
						if(tReturnFlag)
						{
							tLACommisionNewSet.add(mLACommisionNewSchema);
						}
						else
						{
							tLACommisionErrorSet.add(mLACommisionErrorSchema);
						}
					}
					//处理非导入保费,不需要计算fyc
					if(0!=tSumMoney2)
					{
						boolean tReturnFlag = dealcutCommision(tCurrLJAGetEndorseSchema, tSumMoney2, tSumManageFee, "2", cOperator);
						if(tReturnFlag)
						{
							tLACommisionNewSet.add(mLACommisionNewSchema);
						}
						else
						{
							tLACommisionErrorSet.add(mLACommisionErrorSchema);
						}
					}
					//处理导入保费,不需要计算fyc
					if(0!=tSumMoney3)
					{
						boolean tReturnFlag = dealcutCommision(tCurrLJAGetEndorseSchema, tSumMoney3, tSumManageFee, "3", cOperator);
						if(tReturnFlag)
						{
							tLACommisionNewSet.add(mLACommisionNewSchema);
						}
						else
						{
							tLACommisionErrorSet.add(mLACommisionErrorSchema);
						}
					}
				}
			}
			if(0<tLACommisionNewSet.size())
			{
				tMMap.put(tLACommisionNewSet, "INSERT");
			}
			if(0<tLACommisionErrorSet.size())
			{
				tMMap.put(tLACommisionErrorSet, "INSERT");
			}
			if(0==tMMap.size())
			{
				continue;
			}
			if (!mAgentWageCommonFunction.dealDataToTable(tMMap)) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "AgentWageOfLJAGetEndorseGrpZT";
				tError.functionName = "PubSubmit";
				tError.errorMessage = "数据插入到LACommisionNew中失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}while(tLJAGetEndorseSet.size()>0);
		System.out.println("AgentWageOfLJAGetEndorseGrpZT-->dealData:执行结束 ");
		return true;
	}
	/**
	 * 返回对应的保费类型，用来累计计入不同的费用中
	 * @param cLJAGetEndorseSchema
	 * @return
	 */
	private String judgePremType(LJAGetEndorseSchema cLJAGetEndorseSchema)
	{
		//判断是否为导入保费
        LPDiskImportDB tLPDiskImportDB = new LPDiskImportDB();
        tLPDiskImportDB.setGrpContNo(cLJAGetEndorseSchema.getGrpContNo());
        tLPDiskImportDB.setInsuredNo(cLJAGetEndorseSchema.getInsuredNo());
        tLPDiskImportDB.setEdorNo(cLJAGetEndorseSchema.getEndorsementNo());
        tLPDiskImportDB.setEdorType("ZT");
        LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
        tLPDiskImportSet = tLPDiskImportDB.query();
        //导入保费
        if (tLPDiskImportSet != null && tLPDiskImportSet.size() >= 1) 
        {
            LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
            tLPDiskImportSchema = tLPDiskImportSet.get(1);
            String tMoney2 = tLPDiskImportSchema.getMoney2();
            //LPDiskImportSet不为空 并且 money2 不为空，为按比例录入
            if (tMoney2 != null && !tMoney2.equals("")) 
            {
            	return "3";
            } 
        }
        //LPDiskImportSet为空或 money2 为空，为按比例录入lpedorespecialdata
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorNo(cLJAGetEndorseSchema.getEndorsementNo());
        tLPEdorEspecialDataDB.setEdorType("ZT");
        tLPEdorEspecialDataDB.setDetailType("FEERATE");
        LPEdorEspecialDataSet tLPEdorEspecialDataSet = new LPEdorEspecialDataSet();
        tLPEdorEspecialDataSet = tLPEdorEspecialDataDB.query();
        System.out.println(tLPEdorEspecialDataSet.size());
        String tEdorValue = "";
        if (null == tLPEdorEspecialDataSet|| 0==tLPEdorEspecialDataSet.size() ) 
        {
            return "2"; 
        } 
        LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema = tLPEdorEspecialDataSet.get(1);
        tEdorValue = tLPEdorEspecialDataSchema.getEdorValue();
        if (tEdorValue == null || tEdorValue.equals("")) 
        {
        	//表示此条数据有问题，不能提取
            return "-0";
        }
        return "1";
	}
	/**
	 * 根据传入的数据进行数据处理
	 * @param cLJAGetEndorseSchema   
	 * @param cTransMoney
	 * @param cManageFee
	 * @param cFYCFlag
	 * @param cOperator
	 * @return
	 */
	private boolean dealcutCommision(LJAGetEndorseSchema cLJAGetEndorseSchema,
            double cTransMoney,double cManageFee, String cFYCFlag,String cOperator) 
	{
		System.out.println("AgentWageOfLJAGetEndorseGrpZT-->dealcutCommision:开始执行");
		String tAgentCode = cLJAGetEndorseSchema.getAgentCode();
	    String tGrpPolNo = cLJAGetEndorseSchema.getGrpPolNo();
	    String tGrpContNo = cLJAGetEndorseSchema.getGrpContNo();
	    String tRiskCode = cLJAGetEndorseSchema.getRiskCode();
	    //查询业务员相关信息，
	    LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tAgentCode);
	    if(null==tLAAgentSchema)
	    {
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE,cOperator);
	    	return false;
	    }
	    String tBranchCode = tLAAgentSchema.getBranchCode();
	    //查询团队相关信息
		LABranchGroupSchema tLABranchGroupSchema= new LABranchGroupSchema();
	    tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tBranchCode);
	    if(null==tLABranchGroupSchema)
	    {
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP,cOperator);
	    	return false;
	    }
	    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
	    tLCGrpPolSchema = mAgentWageCommonFunction.queryLCGrpPol(tGrpPolNo);
	    if (tLCGrpPolSchema == null) 
	    {
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_LCGRPPOL,cOperator);
	    	return false;
	    }
	    //System.out.println("集体保单号：" + tGrpPolNo);
	    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	    tLCGrpContSchema = mAgentWageCommonFunction.queryLCGrpCont(tGrpContNo);
		if(null == tLCGrpContSchema)
		{
			mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_LCGRPCONT,cOperator);
			return false;
		}
		//对于互动的处理
	    String tSaleChnl = tLCGrpContSchema.getSaleChnl();
	    String tBranchType3 ="";
	    if("14".equals(tSaleChnl)||"15".equals(tSaleChnl))
	    {
	    	String tCrs_SaleChnl = tLCGrpContSchema.getCrs_SaleChnl();
	    	String tCrs_BussType = tLCGrpContSchema.getCrs_BussType();
	    	tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
	    }
	    if("no".equals(tBranchType3))
		{
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_ACTIVE,cOperator);
			return false;
		}
	    String[] tBranchTypes = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
	    if(null == tBranchTypes||0==tBranchTypes.length)
	    {
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_SALECHNL,cOperator);
			return false;
	    }
	    String tBranchType = tBranchTypes[0];
	    String tBranchType2 = tBranchTypes[1];
	    
		mLACommisionNewSchema = mAgentWageCommonFunction.packageGrpContDataOfLACommisionNew(tLCGrpContSchema, tLCGrpPolSchema);
		int tRenewCount = mAgentWageCommonFunction.queryLCRnewPol(tGrpPolNo,"grppolno");
		//获取最大commision
	    String tCommisionsn = mAgentWageCommonFunction.getCommisionSN();
	    mLACommisionNewSchema.setCommisionSN(tCommisionsn);
	    mLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
	    mLACommisionNewSchema.setBranchType(tBranchType);
	    mLACommisionNewSchema.setBranchType2(tBranchType2);
	    mLACommisionNewSchema.setBranchType3(tBranchType3);
	    mLACommisionNewSchema.setReNewCount(tRenewCount);
	    mLACommisionNewSchema.setAgentGroup(tLAAgentSchema.getAgentGroup());
	    mLACommisionNewSchema.setBranchCode(tBranchCode);
	    mLACommisionNewSchema.setBranchSeries(tLABranchGroupSchema.getBranchSeries());
	    mLACommisionNewSchema.setBranchAttr(tLABranchGroupSchema.getBranchAttr());
	    mLACommisionNewSchema.setP7(cManageFee);
	    mLACommisionNewSchema.setP8(0);
	    mLACommisionNewSchema.setEndorsementNo(cLJAGetEndorseSchema.getEndorsementNo());
	    mLACommisionNewSchema.setManageCom(cLJAGetEndorseSchema.getManageCom());
	    mLACommisionNewSchema.setRiskCode(tRiskCode);
	    mLACommisionNewSchema.setDutyCode(cLJAGetEndorseSchema.getDutyCode());
	    mLACommisionNewSchema.setPayPlanCode(cLJAGetEndorseSchema.getPayPlanCode());
	    mLACommisionNewSchema.setReceiptNo(cLJAGetEndorseSchema.getActuGetNo());
	    mLACommisionNewSchema.setTPayDate(cLJAGetEndorseSchema.getGetDate());
	    mLACommisionNewSchema.setTEnterAccDate(cLJAGetEndorseSchema.getEnterAccDate());
	    mLACommisionNewSchema.setTConfDate(cLJAGetEndorseSchema.getGetConfirmDate());
	    mLACommisionNewSchema.setTMakeDate(cLJAGetEndorseSchema.getMakeDate());
	    mLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
	    mLACommisionNewSchema.setTransMoney( 0 - java.lang.Math.abs(cTransMoney));
	    mLACommisionNewSchema.setTransState("00");
	    mLACommisionNewSchema.setTransStandMoney(mLACommisionNewSchema.getTransMoney());
	    mLACommisionNewSchema.setCurPayToDate(cLJAGetEndorseSchema.getGetDate());
	    mLACommisionNewSchema.setTransType("ZT");
	    mLACommisionNewSchema.setFlag("0");
	    mLACommisionNewSchema.setCommDire("1");
	    if("2".equals(cFYCFlag))
	    {
	    	mLACommisionNewSchema.setFlag("3");
		    mLACommisionNewSchema.setF2("01");;
	    }
	    else if("3".equals(cFYCFlag))
	    {
	    	mLACommisionNewSchema.setFlag("4");
		    mLACommisionNewSchema.setF2("01");;
	    }
	    if(tLCGrpPolSchema.getSaleChnl().equals("07"))
	    {
	    	mLACommisionNewSchema.setF1("02");//职团开拓
	    }
	    else
	    {
	    	mLACommisionNewSchema.setF1("01");
	    }
	    mLACommisionNewSchema.setPayYear(0);
	    mLACommisionNewSchema.setPayYears(mAgentWageCommonFunction.getPayYears(tGrpPolNo));
	    mLACommisionNewSchema.setPayCount(1);
	    mLACommisionNewSchema.setAgentCom(cLJAGetEndorseSchema.getAgentCom());
	    mLACommisionNewSchema.setAgentType(cLJAGetEndorseSchema.getAgentType());
	    mLACommisionNewSchema.setAgentCode(cLJAGetEndorseSchema.getAgentCode());
	    String tScanDate = mAgentWageCommonFunction.getScanDate(tLCGrpPolSchema.getPrtNo()); 
	    mLACommisionNewSchema.setScanDate(tScanDate);
	    mLACommisionNewSchema.setOperator(cOperator);
	    mLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
	    mLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
	    mLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
	    mLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
	    //套餐赋值
	    String  tWrapCode = "";
	    if("2".equals(tLCGrpContSchema.getCardFlag()))
	    {
	    	tWrapCode = mAgentWageCommonFunction.getWrapCode(cLJAGetEndorseSchema.getGrpContNo(), cLJAGetEndorseSchema.getRiskCode());
	    }
	    mLACommisionNewSchema.setF3(tWrapCode);
	    String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLCGrpPolSchema.getGrpContNo(),
	    		cLJAGetEndorseSchema.getFeeOperationType(),cLJAGetEndorseSchema.getMakeDate(),
	    		cLJAGetEndorseSchema.getGetConfirmDate(),cLJAGetEndorseSchema.getActuGetNo());
	    //用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
	    String tAFlag =tConfDates[0];
	    //财务确认日期 ，团单会用到此字段
	    String tConfDate = tConfDates[1];
	    //薪资月算法相关字段处理
	    String tVisitTime  = mAgentWageCommonFunction.queryReturnVisit(tGrpContNo);
	    String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
	    String tFlag = "02";
	    //计算薪资月的时候，传的paycount是0，但实际上是1，此处是为了能调用公共的薪资月方法(原来逻辑中是用一个变量，传的是0)
	    mLACommisionNewSchema.setPayCount(0);
	    String tCalDate = mAgentWageCommonFunction.calCalDateForAll(mLACommisionNewSchema, tConfDate, tFlag, tAFlag, tVisitTime, tRiskFlag);
	    mLACommisionNewSchema.setCalDate(tCalDate);
	    if(!"".equals(tCalDate)&&null!=tCalDate)
	    {
	    	String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate);
	    	mLACommisionNewSchema.setWageNo(tWageNo);
	    }
	    mLACommisionNewSchema.setPayCount(1);
	    System.out.println("AgentWageOfLJAGetEndorseGrpZT-->dealcutCommision:执行结束");
	    return true;
	}
}
