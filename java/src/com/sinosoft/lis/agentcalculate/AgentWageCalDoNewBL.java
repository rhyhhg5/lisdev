package com.sinosoft.lis.agentcalculate;

import java.util.Hashtable;

import com.sinosoft.lis.bl.LAAgentBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAssessHistoryDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LARateStandPremDB;
import com.sinosoft.lis.db.LAWageCalElementDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.db.LBGrpPolDB;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LAAssessHistorySet;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageCalElementSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LBGrpPolSet;
import com.sinosoft.lis.vschema.LBPolSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import java.util.HashMap;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import java.math.BigDecimal;
import com.sinosoft.lis.vschema.LARateStandPremSet;
import com.sinosoft.lis.db.LAContFYCRateDB;
import com.sinosoft.lis.vschema.LAContFYCRateSet;
import com.sinosoft.lis.vschema.LAContFYCRateBSet;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.vschema.LAWageActivityLogSet;
import com.sinosoft.lis.schema.LAWageActivityLogSchema;
import com.sinosoft.lis.vdb.LAWageActivityLogDBSet;
import com.sinosoft.lis.db.LAWageActivityLogDB;
import java.text.DecimalFormat;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * 注：CalType    含义
 *      00       直佣
 *      01       组提佣
 *      02       部提佣
 *      03       标准保费
 *      04       险种件数
 *      06       职团折标
 * @author caigang
 * @version 1.0
 */
public class AgentWageCalDoNewBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    /** 手续费序列号 ,手续费计算时默认为20个0*/
//    private String mBatchNo="00000000000000000000" ;

    /** 数据操作字符串 */
    private String mOperate;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String ManageCom;
    private String mBranchType;
    private String mBranchType2;
    private String FRateFORMATMODOL = "0.000000"; //浮动费率计算出来后的精确位数
    private DecimalFormat mFRDecimalFormat = new DecimalFormat(FRateFORMATMODOL); //数字转换对象
    /** 业务处理相关变量 */
    private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();
    private LAWageLogSet mLAWageLogSet = new LAWageLogSet();
    private LAWageHistorySet mLAWageHistorySet = new LAWageHistorySet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LAContFYCRateSet mLAContFYCRateSet = new LAContFYCRateSet();
    private LAContFYCRateBSet mLAContFYCRateBSet = new LAContFYCRateBSet();
    private LAChargeSet mLAChargeSet = new LAChargeSet();
    private LAWageActivityLogSet mLAWageActivityLogSet = new
            LAWageActivityLogSet();
    private MMap mMap = new MMap();

    public AgentWageCalDoNewBL() {
    }

    private boolean getInputData(VData cInputData) {
        mLAWageLogSchema = ((LAWageLogSchema) cInputData.getObjectByObjectName(
                "LAWageLogSchema", 0));
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mLAWageLogSchema == null || mGlobalInput == null) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.ManageCom = mLAWageLogSchema.getManageCom();
        this.mBranchType = mLAWageLogSchema.getBranchType();
        this.mBranchType2 = mLAWageLogSchema.getBranchType2();
        return true;

    }
  public static void main(String[] args)
  {
	  String tPrefix = PubFun.getCurrentDate2()+"86910000";
      String serialno = PubFun1.CreateMaxNo(tPrefix, 6);
      String batchNo=tPrefix+serialno;
      System.out.println(batchNo);
  }
    private boolean check() {

        return true;
    }

    private boolean prepareOutputData() {
        mMap.put(this.mLACommisionSet, "UPDATE");
        mMap.put(this.mLAContFYCRateSet, "UPDATE"); //把原来的标记置为 Y
        mMap.put(this.mLAContFYCRateBSet, "INSERT"); //备份原来的信息
        mMap.put(this.mLAWageHistorySet, "UPDATE");
        mMap.put(this.mLAWageLogSet, "UPDATE");     

        mMap.put(this.mLAChargeSet, "INSERT");
        mMap.put(this.mLAWageActivityLogSet, "INSERT");
        this.mInputData.add(mMap);
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AgentWageCalDoNewBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean dealData() {
        String wageLogSQL = "select * from lawagelog where managecom='" +
                            ManageCom + "'"
                            + " and enddate='" +
                            this.mLAWageLogSchema.getEndDate() + "'";
        if (mLAWageLogSchema.getBranchType() != null &&
            !mLAWageLogSchema.getBranchType().equals("")) {
            wageLogSQL += " and BranchType='" + mLAWageLogSchema.getBranchType() +
                    "'";
        }
        if (mLAWageLogSchema.getBranchType2() != null &&
            !mLAWageLogSchema.getBranchType2().equals("")) {
            wageLogSQL += " and BranchType2='" +
                    mLAWageLogSchema.getBranchType2() + "'";
        }
        LAWageLogDB tLAWageLogDB = new LAWageLogDB();
        LAWageLogSet tLAWageLogSet = new LAWageLogSet();
        tLAWageLogSet = tLAWageLogDB.executeQuery(wageLogSQL);
        if (tLAWageLogSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoNewBL";
            tError.functionName = "dealData";
            tError.errorMessage = "机构" + ManageCom + "的" +
                                  mLAWageLogSchema.getEndDate() +
                                  "的数据未提取，不能进行计算!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //增加团险非 标准业务的数据提取(即FYC的计算),此计算在标准业务计算之前
        //先按照非标准业务计算,可以修改原来的 数据的FYC ,其他字段不变, 然后处理当天的业务,可以处理到当天
        //数据的非标准业务的FYC 2006-02-22计算
        LAContFYCRateDB tLAContFYCRateDB = new LAContFYCRateDB();
        LAContFYCRateSet tLAContFYCRateSet = new LAContFYCRateSet();
        tLAContFYCRateDB.setFlag("N"); //Y表示已作处理了
        tLAContFYCRateSet = tLAContFYCRateDB.query();
        for (int k = 1; k <= tLAContFYCRateSet.size(); k++) {
            LAContFYCRateSchema tLAContFYCRateSchema = new LAContFYCRateSchema();
            tLAContFYCRateSchema = tLAContFYCRateSet.get(k);
            if (!dealLastDate(tLAContFYCRateSchema)) {
                return false;
            }
            //备份
            LAContFYCRateBSchema tLAContFYCRateBSchema = new
                    LAContFYCRateBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLAContFYCRateBSchema,
                                     tLAContFYCRateSchema);
            String tFYCRateEdorNo = PubFun1.CreateMaxNo("FYCRateEdorNo", 20);
            tLAContFYCRateBSchema.setEdorNo(tFYCRateEdorNo);
            tLAContFYCRateBSchema.setOriOperator(tLAContFYCRateSchema.
                                                 getOperator());
            tLAContFYCRateBSchema.setOriMakeDate(tLAContFYCRateSchema.
                                                 getMakeDate());
            tLAContFYCRateBSchema.setOriMakeTime(tLAContFYCRateSchema.
                                                 getMakeTime());
            tLAContFYCRateBSchema.setOriModifyDate(tLAContFYCRateSchema.
                    getModifyDate());
            tLAContFYCRateBSchema.setOriModifyTime(tLAContFYCRateSchema.
                    getModifyTime());
            tLAContFYCRateBSchema.setOperator(mGlobalInput.Operator);
            tLAContFYCRateBSchema.setMakeDate(CurrentDate);
            tLAContFYCRateBSchema.setMakeTime(CurrentTime);
            tLAContFYCRateBSchema.setModifyDate(CurrentDate);
            tLAContFYCRateBSchema.setModifyTime(CurrentTime);
            mLAContFYCRateBSet.add(tLAContFYCRateBSchema);

            tLAContFYCRateSchema.setFlag("Y"); //Y表示已作处理了,下次提数不再从复计算
            tLAContFYCRateSchema.setModifyDate(CurrentDate);
            tLAContFYCRateSchema.setModifyTime(CurrentTime);
            tLAContFYCRateSchema.setOperator(mGlobalInput.Operator);
            mLAContFYCRateSet.add(tLAContFYCRateSchema);
        }
        //标准业务的计算,可以处理当日的非标准业务, 处理复杂的业务,(如标保的计算),此操作必须在非标准业务处理之后,
        for (int i = 1; i <= tLAWageLogSet.size(); i++) {
            LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
            tLAWageLogSchema = tLAWageLogSet.get(i);
            if (tLAWageLogSchema.getState().equals("11")) {
                continue;
            }
	            String sql =
	                    "select commisionsn from lacommision where managecom='" +
                    ManageCom + "' and tmakedate='" +
                    tLAWageLogSchema.getEndDate()
                    + "' and BranchType='" + tLAWageLogSchema.getBranchType()
                    + "' and BranchType2='" + tLAWageLogSchema.getBranchType2()+"' with ur" ;

            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql);
            //////////////////WANGLONG 2006.12.26
            String tCaltype = "";
            if (tLAWageLogSchema.getBranchType().equals("3") &&
                tLAWageLogSchema.getBranchType2().equals("01")) {
            	tCaltype = " and caltype <>'21'";
            }
            if (tLAWageLogSchema.getBranchType().equals("5") &&
                    tLAWageLogSchema.getBranchType2().equals("01")) {
                    tCaltype = " and caltype in ('00','06')";
            }
            if (!calculate(tSSRS, tCaltype, false)) {
                return false;
            }

            tLAWageLogSchema.setState("11");
            tLAWageLogSchema.setModifyDate(CurrentDate);
            tLAWageLogSchema.setModifyTime(CurrentTime);
            tLAWageLogSchema.setOperator(mGlobalInput.Operator);
            mLAWageLogSet.add(tLAWageLogSchema);
            LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
            LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
            tLAWageHistoryDB.setManageCom(ManageCom);
            tLAWageHistoryDB.setAClass("03");
            tLAWageHistoryDB.setBranchType(tLAWageLogSchema.getBranchType());
            tLAWageHistoryDB.setBranchType2(tLAWageLogSchema.getBranchType2());
            tLAWageHistoryDB.setWageNo(tLAWageLogSchema.getWageNo());
            tLAWageHistoryDB.getInfo();
            tLAWageHistorySchema = tLAWageHistoryDB.getSchema();
            tLAWageHistorySchema.setState("11");
            tLAWageHistorySchema.setModifyDate(CurrentDate);
            tLAWageHistorySchema.setModifyTime(CurrentTime);
            tLAWageHistorySchema.setOperator(mGlobalInput.Operator);

            this.mLAWageHistorySet.add(tLAWageHistorySchema);
        }
        return true;
    }

    public boolean calculate(SSRS cSSRS, String calTypes, boolean reCalFlag) {
        SSRS tSSRS = cSSRS;
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionDB.setCommisionSN(tSSRS.GetText(i, 1));
            tLACommisionDB.getInfo();
            tLACommisionSchema = tLACommisionDB.getSchema();
            String BranchType = "";
            String BranchType2 = "";
            BranchType = tLACommisionSchema.getBranchType();
            BranchType2 = tLACommisionSchema.getBranchType2();
            ///////////////////将个销团交叉销售的渠道转换成团险直销
          if(BranchType.equals("1")&&BranchType2.equals("03")){
        	  BranchType = "2";
        	  BranchType2 = "01";
          }
          System.out.println("BranchType"+BranchType);
          System.out.println("BranchType2"+BranchType2);
          //add 添加对孤儿单判断
          String cContNo = tLACommisionSchema.getContNo();
          String cGrpContNo = tLACommisionSchema.getGrpContNo();
          String cBranchtype = tLACommisionSchema.getBranchType();
          String cBranchtype2 = tLACommisionSchema.getBranchType2();
          int cpayyear = tLACommisionSchema.getPayYear();
          int cpaycount = tLACommisionSchema.getPayCount();
          int crenewcount =tLACommisionSchema.getReNewCount();
          String cCommisionsn = tLACommisionSchema.getCommisionSN();
          String cRiskCode = tLACommisionSchema.getRiskCode();
          int orphanFlag = checkLaorphanPolicy(cCommisionsn,cpayyear,crenewcount,cContNo,cGrpContNo,cRiskCode,
        		  cBranchtype,cBranchtype2,cpaycount);
        	  tLACommisionSchema.setP6(orphanFlag);
          String tRiskCode = tLACommisionSchema.getRiskCode();
            //根据实收保费计算团险标准保费
//            if(BranchType.equals("2")&&BranchType2.equals("01"))
//            {
//                LMRiskAppSchema tLMRiskAppSchema = new LMRiskAppSchema();
//                LMRiskAppSet tLMRiskAppSet = new LMRiskAppSet();
//                LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
//                tLMRiskAppDB.setRiskCode(tRiskCode);
//                tLMRiskAppDB.getInfo();
//                tLMRiskAppSchema = tLMRiskAppDB.getSchema();
//                String tRiskType3 = tLMRiskAppSchema.getRiskType3();
//                String tCode = "";
//                //转换险种类型
//                if (tRiskType3.equals("1") || tRiskType3.equals("2")) {
//                    tCode = "01";
//                } else if (tRiskType3.equals("7")) {
//                    tCode = "02";
//                } else if (tRiskType3.equals("4")) {
//                    tCode = "03";
//                } else if (tRiskType3.equals("3")) {
//                    tCode = "05";
//                }
//
//                else if (tRiskType3.equals("5") || tRiskType3.equals("6")) {
//                    tCode = "06";
//                } else {
//                    tCode = "04";
//                }
//                String Sql_rate =
//                        "select codealias from ldcode where codetype='larisktype' and code='" +
//                        tCode + "'";
//                ExeSQL aExeSQL = new ExeSQL();
//                String tcodealias = aExeSQL.getOneValue(Sql_rate);

//                LARateStandPremSchema tLARateStandPremSchema=new LARateStandPremSchema();
//                LARateStandPremSet tLARateStandPremSet=new LARateStandPremSet();
//                LARateStandPremDB tLARateStandPremDB=new LARateStandPremDB();
//                tLARateStandPremDB.setBranchType(tLACommisionSchema.getBranchType());
//                tLARateStandPremDB.setRiskCode(tLACommisionSchema.getRiskCode());
//                tLARateStandPremDB.setsex("0");
//                tLARateStandPremDB.setAppAge("0");
//                tLARateStandPremDB.setYear("0");
//                tLARateStandPremDB.setPayIntv("0");
//                tLARateStandPremDB.setCurYear("0");
//                tLARateStandPremDB.setF01("0");
//                tLARateStandPremDB.setF02("0");
//                tLARateStandPremDB.setBranchType2(tLACommisionSchema.getBranchType2());
//                //管理机构为分公司
//                tLARateStandPremDB.setManageCom(tLACommisionSchema.getManageCom().substring(0,4));
//                tLARateStandPremSet=tLARateStandPremDB.query();
//                if ( tLARateStandPremSet.size()<=0||tLARateStandPremSet==null)
//                {//如果分公司未查询到，则按总公司计算
//                    tLARateStandPremDB=new LARateStandPremDB();
//                    tLARateStandPremDB.setBranchType(tLACommisionSchema.getBranchType());
//                    tLARateStandPremDB.setRiskCode(tLACommisionSchema.getRiskCode());
//                    tLARateStandPremDB.setsex("0");
//                    tLARateStandPremDB.setAppAge("0");
//                    tLARateStandPremDB.setYear("0");
//                    tLARateStandPremDB.setPayIntv("0");
//                    tLARateStandPremDB.setCurYear("0");
//                    tLARateStandPremDB.setF01("0");
//                    tLARateStandPremDB.setF02("0");
//                    tLARateStandPremDB.setBranchType2(tLACommisionSchema.getBranchType2());
//                                    //管理机构为总公司
//                    tLARateStandPremDB.setManageCom("86");
//                    tLARateStandPremSet=new LARateStandPremSet();
//                    tLARateStandPremSet=tLARateStandPremDB.query();
//                    if ( tLARateStandPremSet.size()<=0||tLARateStandPremSet==null)
//                    {
//                        CError tError = new CError();
//                        tError.moduleName = "AgentWageCalDoBL";
//                        tError.functionName = "dealdata";
//                        tError.errorMessage = "查询不到" +
//                                              tLACommisionSchema.getRiskCode() +
//                                              "险种的折标信息!";
//                        this.mErrors.addOneError(tError);
//                        return false;
//                    }
//                }
//                tLARateStandPremSchema=tLARateStandPremSet.get(1);
            //String tcodealias=tLARateStandPremSchema.getr;
            //double tRate = Double.parseDouble(tcodealias);
//                double tRate=tLARateStandPremSchema.getrate();
//                String  tCalType=tLARateStandPremSchema.getCalType();
//                double tTransStandMoney=0;
//                if(tCalType.equals("01"))//按实收保费计算
//                {
//                    tTransStandMoney = mulrate(tLACommisionSchema.getTransMoney(),
//                                               tRate);
//                    tTransStandMoney=round(tTransStandMoney,2);
//                }
//                else if(tCalType.equals("02"))//按管理费计算
//                {
//                    String tSql="select value(sum(fee),0) from  lcinsureaccfee  where  grppolno='"+tLACommisionSchema.getGrpPolNo()+"'";
//                    ExeSQL aExeSQL = new ExeSQL();
//                    String tMoney=aExeSQL.getOneValue(tSql);
//                    double tValue=Double.parseDouble(tMoney);
//                    tTransStandMoney = mulrate(tValue,tRate);
//                    tTransStandMoney=round(tTransStandMoney,2);
//                }
//                else
//                {
//                    CError tError = new CError();
//                    tError.moduleName = "AgentWageCalDoBL";
//                    tError.functionName = "dealdata";
//                    tError.errorMessage = tLACommisionSchema.getManageCom()+"的"
//                                          +tLACommisionSchema.getRiskCode()+"的险种折标信息的计算类型错误!";
//                    this.mErrors.addOneError(tError);
//                    return false;
//                }
//                tLACommisionSchema.setTransStandMoney(tTransStandMoney);
//            }
//           tLMRiskAppSet=tLMRiskAppDB.executeQuery(sql_risk);

            //新契约前六位是0为加费，不算佣金
            String tPayPlanCode = tLACommisionSchema.getPayPlanCode() == null ?
                                  "" : tLACommisionSchema.getPayPlanCode();
            String sql = "select * from lawagecalelement where riskcode='" +
                         tLACommisionSchema.getRiskCode() + "'";
            sql += " and branchType='" + BranchType + "'";
            sql += " and branchType2='" + BranchType2 + "' and caltype<>'91' "; //91 为特殊险种的描述,如境外旅游险种
            sql += calTypes;
            sql += " order by calorder ";
            LAWageCalElementSet tLAWageCalElementSet = new LAWageCalElementSet();
            LAWageCalElementDB tLAWageCalElementDB = new LAWageCalElementDB();
            tLAWageCalElementSet = tLAWageCalElementDB.executeQuery(sql);
            System.out.println("sql:" + sql);
            //前六位是0为加费，不算佣金
            if (tPayPlanCode.length() >= 6 &&
                tLACommisionSchema.getTransType().equals("ZC")
                    ) {
                if (tPayPlanCode.substring(0, 6).equals("000000")) {
                	 mLACommisionSet.add(tLACommisionSchema);
                    continue;
                }
            }
            //保全退保的加费部分,不算佣金
            String PAYPLANCODE_ADDFEE = com.sinosoft.lis.bq.BQ.
                                        PAYPLANCODE_ADDFEE;
            if (tPayPlanCode.equals(PAYPLANCODE_ADDFEE)) {
                continue;
            }
            if (tLAWageCalElementSet == null ||
                tLAWageCalElementSet.size() == 0) {
                continue;
            }
            if (!BranchType.equals("3")&& BranchType2.equals("01")) {
                if (!calWage1(tLAWageCalElementSet, tLACommisionSchema)) {
                    return false;
                }
            } else if (BranchType.equals("2")&&BranchType2.equals("02")) { //重新启用中介手续费的计算 miaoxz 2009-3-26 但中介仍不算佣金
                if (!calWage2(tLAWageCalElementSet,tLACommisionSchema))
                {
                   return false;
                }
                continue;
            } else if (BranchType.equals("1")&&BranchType2.equals("02")) { //重新启用中介手续费的计算 miaoxz 2009-3-26 但中介仍不算佣金
                if (!calWage1(tLAWageCalElementSet,tLACommisionSchema))
                {
                   return false;
                }
                continue;
            } else if(BranchType.equals("3")&& BranchType2.equals("01"))
            {
                if (!calWage4(tLAWageCalElementSet, tLACommisionSchema)) {
                   return false;
               }

            }else
                if (BranchType2.equals("03")) {
//                if (!calWage3(tLAWageCalElementSet, tLACommisionSchema)) {
//                    return false;
//                }
            }
            
            //目前只支持直销和中介的计算，其他的尚未实现
            else {
                continue;
            }
        }
        CalFormDrawRate tCalFormDrawRate = new CalFormDrawRate();
        VData temp = new VData();
        temp.clear();
        temp.add(mLACommisionSet);
        temp.add(""); //传入的是一个AgentGrade，提数计算的时候传空，归属的时候传入的是新的职级

        if (!tCalFormDrawRate.submitData(temp, CalFormDrawRate.CALFLAG)) {
            this.mErrors.copyAllErrors(tCalFormDrawRate.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBL";
            tError.functionName = "CalDirectWage";
            tError.errorMessage = "打折处理的时候出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        mLACommisionSet.clear();
        mLACommisionSet.set((LACommisionSet) tCalFormDrawRate.getResult().
                            getObjectByObjectName("LACommisionSet", 0));

        return true;
    }

    private boolean calWage1(LAWageCalElementSet cLAWageCalElementSet,
                             LACommisionSchema tLACommisionSchema) {
        int m = 0;
        String[] F = new String[5];
        for (int i = 1; i <= cLAWageCalElementSet.size(); i++) {
            LAWageCalElementSchema tLAWageCalElementSchema =
                    cLAWageCalElementSet.get(i);
            String calType = tLAWageCalElementSchema.getCalType();
            String calCode = tLAWageCalElementSchema.getCalCode();
            if(tLACommisionSchema.getBranchType().equals("2")
            		&& tLACommisionSchema.getBranchType2().equals("01")
            		&& !tLACommisionSchema.getGrpContNo().equals("00000000000000000000")){
            	String tResult=null;
            	ExeSQL tExeSQL=new ExeSQL();
            	String tSQL=null;
            	if(calType.equals("00")){
            		tSQL="select othersign from ldcode where codetype='GWRISKCODE'"
            			+" and codename='"+tLACommisionSchema.getRiskCode()
            			+"' and comcode='"
            			+tLACommisionSchema.getManageCom().substring(0,4)+"'";
            		tResult=tExeSQL.getOneValue(tSQL);
            		if(tResult!=null&&tResult.equals("1")){
            			calCode="GW0052";
            		}else if(tResult==null || tResult.equals("")){
            			tSQL="select othersign from ldcode where codetype='GWRISKCODE'"
                			+" and codename='"+tLACommisionSchema.getRiskCode()
                			+"' and comcode='86'";
                		tResult=tExeSQL.getOneValue(tSQL);
                		if(tResult!=null&&tResult.equals("1")){
                			calCode="GW0052";
                		}
            		}
            	}else if(calType.equals("03")){
            		tSQL="select othersign from ldcode where codetype='GSPRISKCODE'"
            			+" and codename='"+tLACommisionSchema.getRiskCode()
            			+"' and comcode='86'";
            		tResult=tExeSQL.getOneValue(tSQL);
            		if(tResult!=null&&tResult.equals("1")){
            			calCode="GSP153";
            		}
            	}
            }

            if ((calCode == null || calCode.equals("")) && !calType.equals("91")) { //91 为特殊险种,如境外旅游险种,没有计算编码,他们是为了作特殊处理的险种描述
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoNewBL";
                tError.functionName = "calWage1";
                tError.errorMessage = "佣金计算要素表中未找到计算编码!";
                this.mErrors.addOneError(tError);
                return false;
            }
            Calculator tCalculator = new Calculator(); //计算类
            System.out.println("Calcode:" + calCode);
            tCalculator.setCalCode(calCode); //添加计算编码
            tCalculator.addBasicFactor("Prem",
                                       String.valueOf(tLACommisionSchema.
                    getTransMoney())); //交易金额
            tCalculator.addBasicFactor("P7",
                                       String.valueOf(tLACommisionSchema.
                    getP7())); //帐户管理费
            //填充要素

            tCalculator.addBasicFactor("RISKCODE",
                                       tLAWageCalElementSchema.getRiskCode());
            tCalculator.addBasicFactor("TMAKEDATE",
                                       tLACommisionSchema.getTMakeDate());
            tCalculator.addBasicFactor("CONTNO",
                                       tLACommisionSchema.getContNo());
            tCalculator.addBasicFactor("SCANDATE",
                                       tLACommisionSchema.getScanDate());
            tCalculator.addBasicFactor("FLAG",
                                       tLACommisionSchema.getFlag());
            tCalculator.addBasicFactor("PAYCOUNT",
                                       String.valueOf(tLACommisionSchema.
                    getPayCount()));
            //String a=String.valueOf(tLACommisionSchema.getReNewCount() );
            tCalculator.addBasicFactor("RENEWCOUNT",
                                       String.valueOf(tLACommisionSchema.
                    getReNewCount()));
            tCalculator.addBasicFactor("GRPCONTNO",
                                       tLACommisionSchema.getGrpContNo());
            if(tLACommisionSchema.getBranchType().equals("1")&&tLACommisionSchema.getBranchType2().equals("03"))
            {
            	tCalculator.addBasicFactor("BRANCHTYPE",
                        "2");
                tCalculator.addBasicFactor("BRANCHTYPE2",
                       "01");
            }
            else
            {
            tCalculator.addBasicFactor("BRANCHTYPE",
                                       tLACommisionSchema.getBranchType());
            tCalculator.addBasicFactor("BRANCHTYPE2",
                                       tLACommisionSchema.getBranchType2());
            }
            tCalculator.addBasicFactor("BRANCHTYPE3",
                    tLACommisionSchema.getBranchType3());
            //添加套餐编码
            tCalculator.addBasicFactor("WRAPCODE",tLACommisionSchema.getF3());

            tCalculator.addBasicFactor("CALTYPE",
                                       tLAWageCalElementSchema.getCalType());
            tCalculator.addBasicFactor("COMMISIONSN",
                                       tLACommisionSchema.getCommisionSN());
            tCalculator.addBasicFactor("MANAGECOM", ManageCom);
            tCalculator.addBasicFactor("SIGNDATE",
                                       String.valueOf(tLACommisionSchema.
                    getSignDate()));
            tCalculator.addBasicFactor("YEARS",
                                       String.valueOf(tLACommisionSchema.
                    getYears()));
            tCalculator.addBasicFactor("PAYYEARS",
                                       String.valueOf(tLACommisionSchema.
                    getPayYears()));
            tCalculator.addBasicFactor("PAYYEAR",
                                       String.valueOf(tLACommisionSchema.
                    getPayYear()));
            tCalculator.addBasicFactor("PAYINTV",
                                       String.valueOf(tLACommisionSchema.
                    getPayIntv()));
            tCalculator.addBasicFactor("AGENTCOM",
                    String.valueOf(tLACommisionSchema.getAgentCom()));
            tCalculator.addBasicFactor("TRANSSTATE",
                                       String.valueOf(tLACommisionSchema.
                    getTransState()));
            tCalculator.addBasicFactor("POLTYPE", tLACommisionSchema.getPolType());
            tCalculator.addBasicFactor("WAGENO", tLACommisionSchema.getWageNo());
            tCalculator.addBasicFactor("AGENTCODE",
                                       tLACommisionSchema.getAgentCode());
//            tCalculator.addBasicFactor("VERSIONTYPE",
//                                       this.getRiskRateVersion(
//                                               tLACommisionSchema.
//                                               getScanDate() != null ?
//                                               tLACommisionSchema.getScanDate() :
//                                               tLACommisionSchema.getSignDate(),
//                                               tLACommisionSchema.getRiskCode(),
//                                               tLACommisionSchema.getBranchType(),
//                                               tLACommisionSchema.
//                                               getBranchType2()));


            System.out.println("m:" + m);
            for (int n = 0; n < m; n++) {
                System.out.println("n:" + n);
                if (F[n].equals("MANAGERATE")) { //管理费比率
                    tCalculator.addBasicFactor("ManageRate",
                                               String.valueOf(
                            tLACommisionSchema.getP1()));
                } else
                if (F[n].equals("BONUSRATE")) {
                    System.out.println("分红比率：0.7");
                    tCalculator.addBasicFactor("BonusRate",
                                               String.valueOf(
                            tLACommisionSchema.getP2()));
                }
                //可扩充
            }
            
            //得到结果
            String strWageRate = tCalculator.calculate();
            System.out.println("..................here1"+strWageRate);
            if (tCalculator.mErrors.needDealError()) {
                // @@错误处理
//              CError tError = new CError();
//              tError.moduleName = "AgentWageCalDoBL";
//              tError.functionName = "calWage1";
//              tError.errorMessage = tCalculator.mErrors.getFirstError();
//              this.mErrors.addOneError(tError);
//              return false;
                LAWageActivityLogSchema tLAWageActivityLogSchema = new
                        LAWageActivityLogSchema();
                tLAWageActivityLogSchema.setAgentCode(tLACommisionSchema.
                        getAgentCode());
                tLAWageActivityLogSchema.setAgentGroup(tLACommisionSchema.
                        getAgentGroup());
                tLAWageActivityLogSchema.setContNo(tLACommisionSchema.getContNo());
                if (calType.equals("00") || calType.equals("10")) {
                    tLAWageActivityLogSchema.setDescribe("薪资比率计算错误!");
                }
                if (calType.equals("03")) {
                    tLAWageActivityLogSchema.setDescribe("标准保费计算错误!");
                }
                if (calType.equals("06")) {
                    tLAWageActivityLogSchema.setDescribe("职团标准保费计算错误!");
                }else{
                	tLAWageActivityLogSchema.setDescribe(calType+"类型错误!");
                }

                tLAWageActivityLogSchema.setGrpContNo(tLACommisionSchema.
                        getGrpContNo());
                tLAWageActivityLogSchema.setGrpPolNo(tLACommisionSchema.
                        getGrpPolNo());
                tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                tLAWageActivityLogSchema.setManageCom(tLACommisionSchema.
                        getManageCom());
                tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
                tLAWageActivityLogSchema.setOtherNo("");
                tLAWageActivityLogSchema.setOtherNoType("");
                tLAWageActivityLogSchema.setPolNo(tLACommisionSchema.getPolNo());
                tLAWageActivityLogSchema.setRiskCode(tLACommisionSchema.
                        getRiskCode());
                String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
                tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                tLAWageActivityLogSchema.setWageLogType("01");
                tLAWageActivityLogSchema.setWageNo(tLACommisionSchema.getWageNo());
                mLAWageActivityLogSet.add(tLAWageActivityLogSchema);

            } else if (Double.parseDouble(strWageRate) == 0) {
                if (calType.equals("00") || calType.equals("10")) {
                    LAWageActivityLogSchema tLAWageActivityLogSchema = new
                            LAWageActivityLogSchema();
                    tLAWageActivityLogSchema.setAgentCode(tLACommisionSchema.
                            getAgentCode());
                    tLAWageActivityLogSchema.setAgentGroup(tLACommisionSchema.
                            getAgentGroup());
                    tLAWageActivityLogSchema.setContNo(tLACommisionSchema.
                            getContNo());
                    tLAWageActivityLogSchema.setDescribe("薪资比率计算为0!");
                    tLAWageActivityLogSchema.setGrpContNo(tLACommisionSchema.
                            getGrpContNo());
                    tLAWageActivityLogSchema.setGrpPolNo(tLACommisionSchema.
                            getGrpPolNo());
                    tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                    tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                    tLAWageActivityLogSchema.setManageCom(tLACommisionSchema.
                            getManageCom());
                    tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
                    tLAWageActivityLogSchema.setOtherNo("");
                    tLAWageActivityLogSchema.setOtherNoType("");
                    tLAWageActivityLogSchema.setPolNo(tLACommisionSchema.
                            getPolNo());
                    tLAWageActivityLogSchema.setRiskCode(tLACommisionSchema.
                            getRiskCode());

                    String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG",
                            12);
                    tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                    tLAWageActivityLogSchema.setWageLogType("01");
                    tLAWageActivityLogSchema.setWageNo(tLACommisionSchema.
                            getWageNo());
                    mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                }
            }
            //如果是退保,则提奖比例 与原来相同 ,不能重新计算,因为可能提奖比例 可能变化
            LACommisionSet tLACommisionSet = new LACommisionSet();
            boolean PolFlag = false;
            if (!tLACommisionSchema.getTransType().equals("ZC")) { //如果类型不是正常,则为退保或解约等等,应找到原来的数据
                if (mBranchType.equals("1")||(mBranchType.equals("5")&&!"00000000000000000000".equals(tLACommisionSchema.getGrpContNo()))
                		||(mBranchType.equals("7")&&!"00000000000000000000".equals(tLACommisionSchema.getGrpContNo()))) {
                    tLACommisionSet = judgeWageIsCal(tLACommisionSchema.
                            getPolNo(),
                            tLACommisionSchema.getSignDate(),tLACommisionSchema.getTransState());
                } else {
                    tLACommisionSet = judgeWageIsCal(tLACommisionSchema.
                            getGrpPolNo(),
                            tLACommisionSchema.getSignDate(),tLACommisionSchema.getTransState());
                }
                if (tLACommisionSet.size() > 0) {
                    PolFlag = true;
                } else {
                    PolFlag = false;
                }
            }
            double wageRateValue = 0;
             if (calType.equals("00")) { //直接佣金
                    if (PolFlag == true) { //如果不是正常（比如为退保），则更原来的比例一样
                        wageRateValue = tLACommisionSet.get(1).getStandFYCRate();
                    } else {
                        wageRateValue = Double.parseDouble(strWageRate);
                    }
                    if ("01".equals(tLACommisionSchema.getF2())) { //不算fyc
                    	wageRateValue = 0;
//                        tLACommisionSchema.setStandFYCRate(0);
//                        tLACommisionSchema.setDirectWage(0); //直接佣金

                    }
                    else { 
                    	  double orgphanvalue = 0;
                    	  int  orphanFlag = (int)tLACommisionSchema.getP6();
                    	   System.out.println("判断孤儿单标志####'"+orphanFlag+"'*******");
                    	   switch(orphanFlag){
                    	   case 0: {
                             break;
                    	   }
                    	   case 1:{
                    		   orgphanvalue = 0.5;
                    		   wageRateValue = mulrate(orgphanvalue,wageRateValue);
                                break;
                           	
                    	   }
                    	   case 2:{
                    		   orgphanvalue = 0.01;
                    		   wageRateValue=orgphanvalue;
                				break;
                    	   }
                    	   case 3:{
                    		   orgphanvalue = 0.5;
                    		   wageRateValue = mulrate(orgphanvalue, wageRateValue);
                               break;
                    	   }
                    	   }
                    	 
                    	
//                    	if(!checkOrphanSingleFlag(tLACommisionSchema,wageRateValue)){
//                    	   System.out.println("##########这里对孤儿单续期续保的处理#######");
//                    	}else{
//                        tLACommisionSchema.setStandFYCRate(wageRateValue);
//                        wageRateValue = mulrate(wageRateValue,
//                                                tLACommisionSchema.
//                                                getTransMoney());
//                        wageRateValue = Arith.round(wageRateValue, 2);
//                        tLACommisionSchema.setDirectWage(wageRateValue); //直接佣金
//                    	}
                    }
                    tLACommisionSchema.setStandFYCRate(wageRateValue);
                    wageRateValue = mulrate(wageRateValue,
                                            tLACommisionSchema.
                                            getTransMoney());
                    wageRateValue = Arith.round(wageRateValue, 2);
                    tLACommisionSchema.setDirectWage(wageRateValue); //直接佣金
             	
                
            } else if (calType.equals("10")) { //直接佣金,按帐户管理费计算   P7 * wageRateValue  20060226 xiangchun  add
                if (wageRateValue == -1) {
                    wageRateValue = 0;
                } else {
                    if (PolFlag == true) { //如果不是正常（比如为退保），则更原来的比例一样
                        wageRateValue = tLACommisionSet.get(1).getStandFYCRate();
                    } else {
                        wageRateValue = Double.parseDouble(strWageRate);
                    }
                    if ("01".equals(tLACommisionSchema.getF2())) { //不算fyc
                        tLACommisionSchema.setStandFYCRate(0);
                        tLACommisionSchema.setDirectWage(0); //直接佣金

                    } else {
                        tLACommisionSchema.setStandFYCRate(wageRateValue);
                        wageRateValue = mulrate(wageRateValue,
                                                tLACommisionSchema.getP7()); //直接佣金,按帐户管理费计算   P7 * wageRateValue
                        wageRateValue = Arith.round(wageRateValue, 2);
                        tLACommisionSchema.setDirectWage(wageRateValue); //直接佣金
                    }
                }
            } else if (calType.equals("03")) {

                String tbranchtype ="";
                String tbranchtype2 = "";
                tbranchtype= tLACommisionSchema.getBranchType();
                tbranchtype2 = tLACommisionSchema.getBranchType2();
                //交叉转换
                if(tbranchtype.equals("1")&&tbranchtype2.equals("03"))
                {
                	tbranchtype = "2";
                    tbranchtype2 = "01";
                }
                String triskcode = tLAWageCalElementSchema.getRiskCode();
                String tmanagecom = tLACommisionSchema.getManageCom();
                double trate = getStandPremRate(tmanagecom.substring(0,4), triskcode,
                                                tbranchtype, tbranchtype2
                                                ,tLACommisionSchema.getPayIntv()
                                                ,tLACommisionSchema.getPayYear()
                                                ,tLACommisionSchema.getPayYears());
//                if (trate == -99) {
//                    return false;
//                }
                wageRateValue = Double.parseDouble(strWageRate);
                tLACommisionSchema.setStandPremRate(trate);
                tLACommisionSchema.setStandPrem(wageRateValue);
            }
            else if (calType.equals("06")) {//职团折标

                String tbranchtype ="";
                String tbranchtype2 = "";
                tbranchtype= tLACommisionSchema.getBranchType();
                tbranchtype2 = tLACommisionSchema.getBranchType2();
                //交叉转换
                if(tbranchtype.equals("1")&&tbranchtype2.equals("03"))
                {
                    tbranchtype = "2";
                    tbranchtype2 = "01";
                }
              //  String triskcode = tLAWageCalElementSchema.getRiskCode();
              //  String tmanagecom = tLACommisionSchema.getManageCom();
                //double trate = getStandPremRate(tmanagecom, triskcode,
                //                                tbranchtype, tbranchtype2);
//                if (trate == -99) {
//                    return false;
//                }
                double RateValue = Double.parseDouble(strWageRate);
                tLACommisionSchema.setStandPremRate(RateValue); //通过计算得出的比例
                wageRateValue = mulrate(RateValue,
                                              tLACommisionSchema.getTransMoney()); //职团折标  transmoney * wageRateValue
                wageRateValue = Arith.round(wageRateValue, 2);
                tLACommisionSchema.setStandPrem(wageRateValue);
              }
//            /**
//             * 对于互动考核中用到的:互动直销业务提奖*互动开拓提奖比例
//             *	添加程序
//             */
//            if(tLACommisionSchema.getBranchType().equals("5")
//            		&& tLACommisionSchema.getBranchType2().equals("01")
//            		&& tLACommisionSchema.getBranchType3().equals("1"))
//    		{
//            	calWage5(tLAWageCalElementSchema,tLACommisionSchema);
//            }

        }


        mLACommisionSet.add(tLACommisionSchema);

        return true;
    }

    private boolean calWage2(LAWageCalElementSet cLAWageCalElementSet,
                             LACommisionSchema tLACommisionSchema) {
        int m = 0;
        String[] F = new String[5];
        for (int i = 1; i <= cLAWageCalElementSet.size(); i++) {
            LAWageCalElementSchema tLAWageCalElementSchema =
                    cLAWageCalElementSet.get(i);
            String calType = tLAWageCalElementSchema.getCalType();
            String calCode = tLAWageCalElementSchema.getCalCode();
            //团险中介佣金仿团险直销 所以逻辑先copy过来
            if(tLACommisionSchema.getBranchType().equals("2")
            		&& tLACommisionSchema.getBranchType2().equals("02")
            		&& !tLACommisionSchema.getGrpContNo().equals("00000000000000000000")){
            	String tResult=null;
            	ExeSQL tExeSQL=new ExeSQL();
            	String tSQL=null;
            	if(calType.equals("00")){
            		tSQL="select othersign from ldcode where codetype='GWRISKCODE'"
            			+" and codename='"+tLACommisionSchema.getRiskCode()
            			+"' and comcode='"
            			+tLACommisionSchema.getManageCom().substring(0,4)+"'";
            		tResult=tExeSQL.getOneValue(tSQL);
            		if(tResult!=null&&tResult.equals("1")){
            			calCode="GW0052";
            		}else if(tResult==null || tResult.equals("")){
            			tSQL="select othersign from ldcode where codetype='GWRISKCODE'"
                			+" and codename='"+tLACommisionSchema.getRiskCode()
                			+"' and comcode='86'";
                		tResult=tExeSQL.getOneValue(tSQL);
                		if(tResult!=null&&tResult.equals("1")){
                			calCode="GW0052";
                		}
            		}
            	}
//            	else if(calType.equals("03")){
//            		tSQL="select othersign from ldcode where codetype='GSPRISKCODE'"
//            			+" and codename='"+tLACommisionSchema.getRiskCode()
//            			+"' and comcode='86'";
//            		tResult=tExeSQL.getOneValue(tSQL);
//            		if(tResult!=null&&tResult.equals("1")){
//            			calCode="GSP153";
//            		}
//            	}
            }
            //copy结束
            if (calCode == null || calCode.equals("")) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoNewBL";
                tError.functionName = "calWage1";
                tError.errorMessage = "佣金计算要素表中未找到计算编码!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //重提数时针对已结算的手续费进行的校验，先不用
            if(checkChargeExist(tLACommisionSchema)){
            	continue;
            }
            Calculator tCalculator = new Calculator(); //计算类
            System.out.println("Calcode:" + calCode);
            tCalculator.setCalCode(calCode); //添加计算编码

            tCalculator.addBasicFactor("Prem",
                                       String.valueOf(tLACommisionSchema.
                    getTransMoney())); //交易金额
            tCalculator.addBasicFactor("PRTNO",
                                       String.valueOf(tLACommisionSchema.
                    getP14())); //印刷号
            //填充要素

            tCalculator.addBasicFactor("RISKCODE",
                                       tLAWageCalElementSchema.getRiskCode());
            tCalculator.addBasicFactor("TMAKEDATE",
                                       tLACommisionSchema.getTMakeDate());
            tCalculator.addBasicFactor("CONTNO",tLACommisionSchema.getContNo());
            tCalculator.addBasicFactor("SIGNDATE",tLACommisionSchema.getSignDate());

            tCalculator.addBasicFactor("SCANDATE",
                                       tLACommisionSchema.getScanDate());
            tCalculator.addBasicFactor("FLAG",
                                       tLACommisionSchema.getFlag());
            tCalculator.addBasicFactor("PAYCOUNT",
                                       String.valueOf(tLACommisionSchema.
                    getPayCount()));
            tCalculator.addBasicFactor("TRANSSTATE",
                                       String.valueOf(tLACommisionSchema.
                    getTransState()));

            tCalculator.addBasicFactor("BRANCHTYPE",
                                       tLACommisionSchema.getBranchType());
            tCalculator.addBasicFactor("BRANCHTYPE2",
                                       tLACommisionSchema.getBranchType2());
            tCalculator.addBasicFactor("CALTYPE",
                                       tLAWageCalElementSchema.getCalType());
            tCalculator.addBasicFactor("COMMISIONSN",
                                       tLACommisionSchema.getCommisionSN());
            tCalculator.addBasicFactor("AGENTCOM",
                                       tLACommisionSchema.getAgentCom());
            tCalculator.addBasicFactor("MANAGECOM", ManageCom);
            //中介渠道计算佣金提奖添加必要数据
            tCalculator.addBasicFactor("GRPCONTNO",
                    tLACommisionSchema.getGrpContNo());
			tCalculator.addBasicFactor("YEARS",
			                    String.valueOf(tLACommisionSchema.
			 getYears()));
			tCalculator.addBasicFactor("PAYYEARS",
			                    String.valueOf(tLACommisionSchema.
			 getPayYears()));
			tCalculator.addBasicFactor("PAYYEAR",
			                    String.valueOf(tLACommisionSchema.
			 getPayYear()));
			tCalculator.addBasicFactor("PAYINTV",
			                    String.valueOf(tLACommisionSchema.
			 getPayIntv()));
			tCalculator.addBasicFactor("AGENTCOM",
			 String.valueOf(tLACommisionSchema.getAgentCom()));
                       
//          添加套餐编码
            tCalculator.addBasicFactor("WRAPCODE",tLACommisionSchema.getF3());
            tCalculator.addBasicFactor("POLTYPE", tLACommisionSchema.getPolType());
            tCalculator.addBasicFactor("WAGENO", tLACommisionSchema.getWageNo());
            tCalculator.addBasicFactor("RENEWCOUNT",
                                       String.valueOf(tLACommisionSchema.
                    getReNewCount()));
            tCalculator.addBasicFactor("AGENTCODE",
                                       tLACommisionSchema.getAgentCode());
//            tCalculator.addBasicFactor("VERSIONTYPE",
//                                       this.getRiskRateVersion(
//                                               tLACommisionSchema.
//                                               getScanDate() != null ?
//                                               tLACommisionSchema.getScanDate() :
//                                               tLACommisionSchema.getSignDate(),
//                                               tLACommisionSchema.getRiskCode(),
//                                               tLACommisionSchema.getBranchType(),
//                                               tLACommisionSchema.
//                                               getBranchType2()));

            System.out.println("m:" + m);
//            for (int n = 0; n < m; n++) {
//                System.out.println("n:" + n);
//                if (F[n].equals("MANAGERATE")) { //管理费比率
//                    tCalculator.addBasicFactor("ManageRate",
//                                               String.valueOf(
//                            tLACommisionSchema.getP1()));
//                } else
//                if (F[n].equals("BONUSRATE")) {
//                    System.out.println("分红比率：0.7");
//                    tCalculator.addBasicFactor("BonusRate",
//                                               String.valueOf(
//                            tLACommisionSchema.getP2()));
//                } else
//                if (F[n].equals("YEARS")) { //保险年期
//                    tCalculator.addBasicFactor("Years",
//                                               String.valueOf(
//                            tLACommisionSchema.getYears()));
//                } else if (F[n].equals("PAYYEARS")) { //交费年期
//                    tCalculator.addBasicFactor("PayYears",
//                                               String.valueOf(
//                            tLACommisionSchema.
//                            getPayYears()));
//                } else if (F[n].equals("PAYYEAR")) { //计算交费年度=保单生效日到交至日期
//                    tCalculator.addBasicFactor("PayYear",
//                                               String.valueOf(
//                            tLACommisionSchema.
//                            getPayYear()));
//                } else if (F[n].equals("PAYINTV")) { //交费间隔
//                    tCalculator.addBasicFactor("PayIntv",
//                                               String.valueOf(
//                            tLACommisionSchema.
//                            getPayIntv()));
//                }
//                //可扩充
//            }
            if (tCalculator.mErrors.needDealError()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoBL";
                tError.functionName = "calWage1";
                tError.errorMessage = tCalculator.mErrors.getFirstError();
                this.mErrors.addOneError(tError);
                return false;
            }
            //得到结果
            String strWageRate = tCalculator.calculate();
            double wageRateValue = Double.parseDouble(strWageRate);
            if (calType.equals("00")) {
                if (wageRateValue == -1) {
                    wageRateValue = 0;
                } else {
                    wageRateValue = Double.parseDouble(strWageRate);
                    tLACommisionSchema.setStandFYCRate(wageRateValue);
                    wageRateValue = mulrate(wageRateValue,
                                            tLACommisionSchema.getTransMoney());
                    wageRateValue = Arith.round(wageRateValue, 2);
                    tLACommisionSchema.setDirectWage(wageRateValue); //直接佣金
                }
              }
            else if (calType.equals("03")) {

                String tbranchtype ="";
                String tbranchtype2 = "";
                tbranchtype= tLACommisionSchema.getBranchType();
                tbranchtype2 = tLACommisionSchema.getBranchType2();
                //交叉转换
                if(tbranchtype.equals("1")&&tbranchtype2.equals("03"))
                {
                	tbranchtype = "2";
                    tbranchtype2 = "01";
                }
                String triskcode = tLAWageCalElementSchema.getRiskCode();
                String tmanagecom = tLACommisionSchema.getManageCom();
                double trate = getStandPremRate(tmanagecom.substring(0,4), triskcode,
                                                tbranchtype, tbranchtype2
                                                ,tLACommisionSchema.getPayIntv()
                                                ,tLACommisionSchema.getPayYear()
                                                ,tLACommisionSchema.getPayYears());
//                if (trate == -99) {
//                    return false;
//                }
                wageRateValue = Double.parseDouble(strWageRate);
                tLACommisionSchema.setStandPremRate(trate);
                tLACommisionSchema.setStandPrem(wageRateValue);
            }
            else if (calType.equals("06")) {//职团折标

                String tbranchtype ="";
                String tbranchtype2 = "";
                tbranchtype= tLACommisionSchema.getBranchType();
                tbranchtype2 = tLACommisionSchema.getBranchType2();
                //交叉转换
                if(tbranchtype.equals("1")&&tbranchtype2.equals("03"))
                {
                    tbranchtype = "2";
                    tbranchtype2 = "01";
                }
              //  String triskcode = tLAWageCalElementSchema.getRiskCode();
              //  String tmanagecom = tLACommisionSchema.getManageCom();
                //double trate = getStandPremRate(tmanagecom, triskcode,
                //                                tbranchtype, tbranchtype2);
//                if (trate == -99) {
//                    return false;
//                }
                double RateValue = Double.parseDouble(strWageRate);
                tLACommisionSchema.setStandPremRate(RateValue); //通过计算得出的比例
                wageRateValue = mulrate(RateValue,
                                              tLACommisionSchema.getTransMoney()); //职团折标  transmoney * wageRateValue
                wageRateValue = Arith.round(wageRateValue, 2);
                tLACommisionSchema.setStandPrem(wageRateValue);
              }
               mLACommisionSet.add(tLACommisionSchema);
//            } else if (calType.equals("03")) { //标准保费
//                wageRateValue = Double.parseDouble(strWageRate);
//                String tbranchtype = tLACommisionSchema.getBranchType();
//                String tbranchtype2 = tLACommisionSchema.getBranchType2();
//                String triskcode = tLAWageCalElementSchema.getRiskCode();
//                String tmanagecom = tLACommisionSchema.getManageCom();
//                double trate = getStandPremRate(tmanagecom, triskcode,
//                                                tbranchtype, tbranchtype2);
//                if (trate == -99) {
//                    return false;
//                }
//                tLACommisionSchema.setStandPremRate(trate);
////                wageRateValue = mulrate(wageRateValue ,
////                                   tLACommisionSchema.getTransMoney());
////                wageRateValue=round(wageRateValue,2);
//                tLACommisionSchema.setStandPrem(wageRateValue);
//            } else if (calType.equals("04")) { //件数, 险种件数
//                tLACommisionSchema.setCalCount(wageRateValue);
//            } else
            	if (calType.equals("51")) {
                //标准手续费
                if (wageRateValue == -99) {
                    continue;
                }
                String tAgentCom = tLACommisionSchema.getAgentCom();
                double wageValue = mulrate(tLACommisionSchema.getTransMoney(),
                                           wageRateValue);
                wageValue = Arith.round(wageValue, 2);
                LAChargeSchema tLAChargeSchema = new LAChargeSchema();
                setChargeSchemaValue(tLAChargeSchema, tLACommisionSchema);
                tLAChargeSchema.setAgentCom(tAgentCom);
                tLAChargeSchema.setChargeRate(wageRateValue);
                tLAChargeSchema.setCharge(wageValue);
                tLAChargeSchema.setChargeType(calType);
                tLAChargeSchema.setBranchType(tLACommisionSchema.getBranchType());
                tLAChargeSchema.setBranchType2(tLACommisionSchema.
                                               getBranchType2());
                tLAChargeSchema.setTransType(tLACommisionSchema.getTransType());
//                tLAChargeSchema.setBatchNo(this.mBatchNo);
                mLAChargeSet.add(tLAChargeSchema);
            }
        }
        return true;
    }
    private boolean checkChargeExist(LACommisionSchema tLACommisionSchema){
    	String tSQL="select distinct 1 from lacharge where contno='"
    		+tLACommisionSchema.getContNo()+"' and polno='"
    		+tLACommisionSchema.getPolNo()+"' and receiptno='"
    		+tLACommisionSchema.getReceiptNo()+"' and managecom='"
    		+tLACommisionSchema.getManageCom()+"' and agentcom='"
    		+tLACommisionSchema.getAgentCom()+"' and transtype='"
    		+tLACommisionSchema.getTransType()
    		+"' and chargetype='51' and chargestate='1' with ur";
    	ExeSQL tExeSQL =new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL)!=null&&tExeSQL.getOneValue(tSQL).equals("1")){
    		return true;
    	}
    	return false;
    }
    private boolean calWage3(LAWageCalElementSet cLAWageCalElementSet,
                             LACommisionSchema tLACommisionSchema) {
        String tAgentCom = tLACommisionSchema.getAgentCom();
        int m = 0;
        String[] F = new String[5];
        Hashtable tHashAgentCom = new Hashtable();
        if (!putAllAgentComHash(tAgentCom, tHashAgentCom)) {
            return false;
        }

        for (int i = 1; i <= cLAWageCalElementSet.size(); i++) {
            LAWageCalElementSchema tLAWageCalElementSchema =
                    cLAWageCalElementSet.get(i);
            String calType = tLAWageCalElementSchema.getCalType();
            String calCode = tLAWageCalElementSchema.getCalCode();
            if (calCode == null || calCode.equals("")) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoNewBL";
                tError.functionName = "calWage1";
                tError.errorMessage = "佣金计算要素表中未找到计算编码!";
                this.mErrors.addOneError(tError);
                return false;
            }
            Calculator tCalculator = new Calculator(); //计算类
            System.out.println("Calcode:" + calCode);
            tCalculator.setCalCode(calCode); //添加计算编码
            tCalculator.addBasicFactor("Prem",
                                       String.valueOf(tLACommisionSchema.
                    getTransMoney())); //交易金额
            tCalculator.addBasicFactor("P7",
                                       String.valueOf(tLACommisionSchema.
                    getP7())); //帐户管理费
            //填充要素

            tCalculator.addBasicFactor("RISKCODE",
                                       tLAWageCalElementSchema.getRiskCode());
            tCalculator.addBasicFactor("TMAKEDATE",
                                       tLACommisionSchema.getTMakeDate());
            tCalculator.addBasicFactor("CONTNO",
                                       tLACommisionSchema.getContNo());
            tCalculator.addBasicFactor("SCANDATE",
                                       tLACommisionSchema.getScanDate());
            tCalculator.addBasicFactor("FLAG",
                                       tLACommisionSchema.getFlag());
            tCalculator.addBasicFactor("PAYCOUNT",
                                       String.valueOf(tLACommisionSchema.
                    getPayCount()));
            tCalculator.addBasicFactor("TRANSSTATE",
                                       String.valueOf(tLACommisionSchema.
                    getTransState()));

            tCalculator.addBasicFactor("BRANCHTYPE",
                                       tLACommisionSchema.getBranchType());

            tCalculator.addBasicFactor("BRANCHTYPE2",
                                       tLACommisionSchema.getBranchType2());
            //添加套餐编码
            tCalculator.addBasicFactor("WRAPCODE",tLACommisionSchema.getF3());
            
            tCalculator.addBasicFactor("CALTYPE",
                                       tLAWageCalElementSchema.getCalType());
            tCalculator.addBasicFactor("COMMISIONSN",
                                       tLACommisionSchema.getCommisionSN());
            tCalculator.addBasicFactor("AGENTCOM",
                                       tLACommisionSchema.getAgentCom());
            tCalculator.addBasicFactor("MANAGECOM", ManageCom);
            tCalculator.addBasicFactor("POLTYPE", tLACommisionSchema.getPolType());
            tCalculator.addBasicFactor("WAGENO", tLACommisionSchema.getWageNo());
            tCalculator.addBasicFactor("AGENTCODE",
                                       tLACommisionSchema.getAgentCode());
//            tCalculator.addBasicFactor("VERSIONTYPE",
//                                       this.getRiskRateVersion(
//                                               tLACommisionSchema.
//                                               getScanDate() != null ?
//                                               tLACommisionSchema.getScanDate() :
//                                               tLACommisionSchema.getSignDate(),
//                                               tLACommisionSchema.getRiskCode(),
//                                               tLACommisionSchema.getBranchType(),
//                                               tLACommisionSchema.
//                                               getBranchType2()));


            if (tAgentCom != null && !tAgentCom.equals("")
                && (calType.substring(0, 1).equals("2")
                    || calType.substring(0, 1).equals("3")
                    || calType.substring(0, 1).equals("4"))) {
                System.out.println("~~~~" + tHashAgentCom.size() + "~~~~~");
                switch (Integer.parseInt(calType.substring(1))) {
                case 1: //网点
                    tAgentCom = (String) tHashAgentCom.get("04");
                    break;
                case 2: //分理处
                    tAgentCom = (String) tHashAgentCom.get("03");
                    break;
                case 3: //支行
                    tAgentCom = (String) tHashAgentCom.get("02");
                    break;
                case 4: //分行
                    tAgentCom = (String) tHashAgentCom.get("01");
                    break;
                case 5: //总行
                    tAgentCom = (String) tHashAgentCom.get("00");
                    break;
                case 6: //柜员
                case 7: //留用
                    tAgentCom = tLACommisionSchema.getAgentCom();
                    break;
                    //柜员 留用 不变
                }
            }

            System.out.println("m:" + m);
            for (int n = 0; n < m; n++) {
                System.out.println("n:" + n);
                if (F[n].equals("MANAGERATE")) { //管理费比率
                    tCalculator.addBasicFactor("ManageRate",
                                               String.valueOf(
                            tLACommisionSchema.getP1()));
                } else
                if (F[n].equals("BONUSRATE")) {
                    System.out.println("分红比率：0.7");
                    tCalculator.addBasicFactor("BonusRate",
                                               String.valueOf(
                            tLACommisionSchema.getP2()));
                } else
                if (F[n].equals("YEARS")) { //保险年期
                    tCalculator.addBasicFactor("Years",
                                               String.valueOf(
                            tLACommisionSchema.getYears()));
                } else if (F[n].equals("PAYYEARS")) { //交费年期
                    tCalculator.addBasicFactor("PayYears",
                                               String.valueOf(
                            tLACommisionSchema.
                            getPayYears()));
                } else if (F[n].equals("PAYYEAR")) { //计算交费年度=保单生效日到交至日期
                    tCalculator.addBasicFactor("PayYear",
                                               String.valueOf(
                            tLACommisionSchema.
                            getPayYear()));
                } else if (F[n].equals("PAYINTV")) { //交费间隔
                    tCalculator.addBasicFactor("PayIntv",
                                               String.valueOf(
                            tLACommisionSchema.
                            getPayIntv()));
                }
                //可扩充
            }
            if (tCalculator.mErrors.needDealError()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoBL";
                tError.functionName = "calWage1";
                tError.errorMessage = tCalculator.mErrors.getFirstError();
                this.mErrors.addOneError(tError);
                return false;
            }
            //得到结果
            String strWageRate = tCalculator.calculate();
            double wageRateValue = 0;
            if (calType.equals("00")) {
                if (wageRateValue == -1) {
                    wageRateValue = 0;
                } else {
                    wageRateValue = Double.parseDouble(strWageRate);
                    tLACommisionSchema.setStandFYCRate(wageRateValue);
                    wageRateValue = mulrate(wageRateValue,
                                            tLACommisionSchema.getTransMoney());
                    wageRateValue = Arith.round(wageRateValue, 2);
                    tLACommisionSchema.setDirectWage(wageRateValue); //直接佣金
                }
            } else if (calType.equals("03")) { //标准保费
                tLACommisionSchema.setStandPremRate(wageRateValue);
                wageRateValue = mulrate(wageRateValue,
                                        tLACommisionSchema.getTransMoney());
                wageRateValue = Arith.round(wageRateValue, 2);
                tLACommisionSchema.setStandPrem(wageRateValue);
            } else if (calType.equals("04")) { //件数, 险种件数
                tLACommisionSchema.setCalCount(wageRateValue);
            } else if (!calType.substring(0, 1).equals("5")) {
                //标准手续费
                if (wageRateValue == -99) {
                    continue;
                }
                double wageValue = mulrate(tLACommisionSchema.getTransMoney(),
                                           wageRateValue);
                wageValue = Arith.round(wageValue, 2);
                LAChargeSchema tLAChargeSchema = new LAChargeSchema();
                setChargeSchemaValue(tLAChargeSchema, tLACommisionSchema);
                tLAChargeSchema.setAgentCom(tAgentCom);
                tLAChargeSchema.setChargeRate(wageRateValue);
                tLAChargeSchema.setCharge(wageValue);
                tLAChargeSchema.setChargeType(calType);
                tLAChargeSchema.setBranchType(tLACommisionSchema.getBranchType());
                tLAChargeSchema.setBranchType2(tLACommisionSchema.
                                               getBranchType2());
                tLAChargeSchema.setTransType(tLACommisionSchema.getTransType());
                mLAChargeSet.add(tLAChargeSchema);
            }

        }
        mLACommisionSet.add(tLACommisionSchema);
        return true;
    }

    private boolean calWage4(LAWageCalElementSet cLAWageCalElementSet,
                             LACommisionSchema tLACommisionSchema) {
        int m = 0;
        String[] F = new String[5];
        for (int i = 1; i <= cLAWageCalElementSet.size(); i++) {
            LAWageCalElementSchema tLAWageCalElementSchema =
                    cLAWageCalElementSet.get(i);
            String calType = tLAWageCalElementSchema.getCalType();
            String calCode = tLAWageCalElementSchema.getCalCode();
            if ((calCode == null || calCode.equals("")) && !calType.equals("91")) { //91 为特殊险种,如境外旅游险种,没有计算编码,他们是为了作特殊处理的险种描述
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoNewBL";
                tError.functionName = "calWage1";
                tError.errorMessage = "佣金计算要素表中未找到计算编码!";
                this.mErrors.addOneError(tError);
                return false;
            }

            Calculator tCalculator = new Calculator(); //计算类
            tCalculator.setCalCode(calCode); //添加计算编码
            //填充要素
            tCalculator.addBasicFactor("AGENTCOM", tLACommisionSchema.getAgentCom());//网点
            tCalculator.addBasicFactor("RISKCODE",
                                       tLAWageCalElementSchema.getRiskCode());
            tCalculator.addBasicFactor("BRANCHTYPE",
                                       tLACommisionSchema.getBranchType());
            tCalculator.addBasicFactor("BRANCHTYPE2",
                                       tLACommisionSchema.getBranchType2());
            //添加套餐编码
            tCalculator.addBasicFactor("WRAPCODE",tLACommisionSchema.getF3());
            
            tCalculator.addBasicFactor("CALTYPE",
                                       tLAWageCalElementSchema.getCalType());
            tCalculator.addBasicFactor("PAYYEARS",
                                      String.valueOf(tLACommisionSchema.
                   getPayYears()));
            tCalculator.addBasicFactor("PAYYEAR",
                                      String.valueOf(tLACommisionSchema.
                   getPayYear()));
            tCalculator.addBasicFactor("PAYINTV",
                                      String.valueOf(tLACommisionSchema.
                   getPayIntv()));
            tCalculator.addBasicFactor("TRANSSTATE",
                                                  String.valueOf(tLACommisionSchema.
                    getTransState()));
           tCalculator.addBasicFactor("TMAKEDATE",
                                      tLACommisionSchema.getTMakeDate());
           tCalculator.addBasicFactor("CONTNO",
                                      tLACommisionSchema.getContNo());
           tCalculator.addBasicFactor("SCANDATE",
                                      tLACommisionSchema.getScanDate());
           tCalculator.addBasicFactor("FLAG",
                                      tLACommisionSchema.getFlag());
           tCalculator.addBasicFactor("PAYCOUNT",
                                      String.valueOf(tLACommisionSchema.
                   getPayCount()));
           //String a=String.valueOf(tLACommisionSchema.getReNewCount() );
           tCalculator.addBasicFactor("RENEWCOUNT",
                                      String.valueOf(tLACommisionSchema.
                   getReNewCount()));
           tCalculator.addBasicFactor("GRPCONTNO",
                                      tLACommisionSchema.getGrpContNo());

           tCalculator.addBasicFactor("COMMISIONSN",
                                      tLACommisionSchema.getCommisionSN());
           tCalculator.addBasicFactor("MANAGECOM", ManageCom);
           tCalculator.addBasicFactor("SIGNDATE",
                                      String.valueOf(tLACommisionSchema.
                   getSignDate()));
           tCalculator.addBasicFactor("YEARS",
                                      String.valueOf(tLACommisionSchema.
                   getYears()));
           tCalculator.addBasicFactor("POLTYPE", tLACommisionSchema.getPolType());
           tCalculator.addBasicFactor("WAGENO", tLACommisionSchema.getWageNo());
           tCalculator.addBasicFactor("AGENTCODE",
                                      tLACommisionSchema.getAgentCode());
            System.out.println("m:" + m);
            for (int n = 0; n < m; n++) {
                System.out.println("n:" + n);
                if (F[n].equals("MANAGERATE")) { //管理费比率
                    tCalculator.addBasicFactor("ManageRate",
                                               String.valueOf(
                            tLACommisionSchema.getP1()));
                } else
                if (F[n].equals("BONUSRATE")) {
                    System.out.println("分红比率：0.7");
                    tCalculator.addBasicFactor("BonusRate",
                                               String.valueOf(
                            tLACommisionSchema.getP2()));
                }

                //可扩充
            }
            //得到结果
            String strWageRate = tCalculator.calculate();
            if (tCalculator.mErrors.needDealError()) {
                // @@错误处理
//              CError tError = new CError();
//              tError.moduleName = "AgentWageCalDoBL";
//              tError.functionName = "calWage1";
//              tError.errorMessage = tCalculator.mErrors.getFirstError();
//              this.mErrors.addOneError(tError);
//              return false;

                LAWageActivityLogSchema tLAWageActivityLogSchema = new
                        LAWageActivityLogSchema();
                tLAWageActivityLogSchema.setAgentCode(tLACommisionSchema.
                        getAgentCode());
                tLAWageActivityLogSchema.setAgentGroup(tLACommisionSchema.
                        getAgentGroup());
                tLAWageActivityLogSchema.setContNo(tLACommisionSchema.getContNo());
                if (calType.equals("00") || calType.equals("10")) {
                    tLAWageActivityLogSchema.setDescribe("薪资比率计算错误!");
                }
                if (calType.equals("03")) {
                    tLAWageActivityLogSchema.setDescribe("标准保费计算错误!");
                }
                tLAWageActivityLogSchema.setGrpContNo(tLACommisionSchema.
                        getGrpContNo());
                tLAWageActivityLogSchema.setGrpPolNo(tLACommisionSchema.
                        getGrpPolNo());
                tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                tLAWageActivityLogSchema.setManageCom(tLACommisionSchema.
                        getManageCom());
                tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
                tLAWageActivityLogSchema.setOtherNo("");
                tLAWageActivityLogSchema.setOtherNoType("");
                tLAWageActivityLogSchema.setPolNo(tLACommisionSchema.getPolNo());
                tLAWageActivityLogSchema.setRiskCode(tLACommisionSchema.
                        getRiskCode());
                String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
                tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                tLAWageActivityLogSchema.setWageLogType("01");
                tLAWageActivityLogSchema.setWageNo(tLACommisionSchema.getWageNo());
                mLAWageActivityLogSet.add(tLAWageActivityLogSchema);

            } else if (Double.parseDouble(strWageRate) == 0) {

                if (calType.equals("00") || calType.equals("10")) {
                    LAWageActivityLogSchema tLAWageActivityLogSchema = new
                            LAWageActivityLogSchema();
                    tLAWageActivityLogSchema.setAgentCode(tLACommisionSchema.
                            getAgentCode());
                    tLAWageActivityLogSchema.setAgentGroup(tLACommisionSchema.
                            getAgentGroup());
                    tLAWageActivityLogSchema.setContNo(tLACommisionSchema.
                            getContNo());
                    tLAWageActivityLogSchema.setDescribe("薪资比率计算为0!");
                    tLAWageActivityLogSchema.setGrpContNo(tLACommisionSchema.
                            getGrpContNo());
                    tLAWageActivityLogSchema.setGrpPolNo(tLACommisionSchema.
                            getGrpPolNo());
                    tLAWageActivityLogSchema.setMakeDate(CurrentDate);
                    tLAWageActivityLogSchema.setMakeTime(CurrentTime);
                    tLAWageActivityLogSchema.setManageCom(tLACommisionSchema.
                            getManageCom());
                    tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
                    tLAWageActivityLogSchema.setOtherNo("");
                    tLAWageActivityLogSchema.setOtherNoType("");
                    tLAWageActivityLogSchema.setPolNo(tLACommisionSchema.
                            getPolNo());
                    tLAWageActivityLogSchema.setRiskCode(tLACommisionSchema.
                            getRiskCode());

                    String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG",
                            12);
                    tLAWageActivityLogSchema.setSerialNo(tSerialNo);
                    tLAWageActivityLogSchema.setWageLogType("01");
                    tLAWageActivityLogSchema.setWageNo(tLACommisionSchema.
                            getWageNo());
                    mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
                }
            }
            //如果是退保,则提奖比例 与原来相同 ,不能重新计算,因为可能提奖比例 可能变化
            LACommisionSet tLACommisionSet = new LACommisionSet();
            boolean PolFlag = false;
            if (!tLACommisionSchema.getTransType().equals("ZC")) { //如果类型不是正常,则为退保或解约等等,应找到原来的数据
                if (mBranchType.equals("1")) {
                    tLACommisionSet = judgeWageIsCal(tLACommisionSchema.
                            getPolNo(),
                            tLACommisionSchema.getSignDate(),tLACommisionSchema.getTransState());
                } else {
                    tLACommisionSet = judgeWageIsCal(tLACommisionSchema.
                            getGrpPolNo(),
                            tLACommisionSchema.getSignDate(),tLACommisionSchema.getTransState());
                }
                if (tLACommisionSet.size() > 0) {
                    PolFlag = true;
                } else {
                    PolFlag = false;
                }
            }
            double wageRateValue = 0;
            if (calType.equals("00")) { //直接佣金
                if (wageRateValue == -1) {
                    wageRateValue = 0;
                } else {
                    if (PolFlag == true) { //如果不是正常（比如为退保），则更原来的比例一样
                        wageRateValue = tLACommisionSet.get(1).getStandFYCRate();
                    } else {
                        wageRateValue = Double.parseDouble(strWageRate);
                    }
                    if ("01".equals(tLACommisionSchema.getF2())) { //不算fyc
                        tLACommisionSchema.setStandFYCRate(0);
                        tLACommisionSchema.setDirectWage(0); //直接佣金

                    } else {
                        tLACommisionSchema.setStandFYCRate(wageRateValue);
                        wageRateValue = mulrate(wageRateValue,
                                                tLACommisionSchema.
                                                getTransMoney());
                        wageRateValue = Arith.round(wageRateValue, 2);
                        tLACommisionSchema.setDirectWage(wageRateValue); //直接佣金
                    }
                }
            } else if (calType.equals("10")) { //直接佣金,按帐户管理费计算   P7 * wageRateValue  20060226 xiangchun  add
                if (wageRateValue == -1) {
                    wageRateValue = 0;
                } else {
                    if (PolFlag == true) { //如果不是正常（比如为退保），则更原来的比例一样
                        wageRateValue = tLACommisionSet.get(1).getStandFYCRate();
                    } else {
                        wageRateValue = Double.parseDouble(strWageRate);
                    }
                    if ("01".equals(tLACommisionSchema.getF2())) { //不算fyc
                        tLACommisionSchema.setStandFYCRate(0);
                        tLACommisionSchema.setDirectWage(0); //直接佣金

                    } else {
                        tLACommisionSchema.setStandFYCRate(wageRateValue);
                        wageRateValue = mulrate(wageRateValue,
                                                tLACommisionSchema.getP7()); //直接佣金,按帐户管理费计算   P7 * wageRateValue
                        wageRateValue = Arith.round(wageRateValue, 2);
                        tLACommisionSchema.setDirectWage(wageRateValue); //直接佣金
                    }
                }
            }
            else if (calType.equals("03")) {

                String tbranchtype = tLACommisionSchema.getBranchType();
                String tbranchtype2 = tLACommisionSchema.getBranchType2();
                String triskcode = tLAWageCalElementSchema.getRiskCode();
                String tmanagecom = tLACommisionSchema.getManageCom();
//                double trate = getStandPremRate(tmanagecom, triskcode,
//                                                tbranchtype, tbranchtype2);
                double trate =0;//银代不计算标准保费，所以默认为0，modified by miaoxz,2009-7-27
                if (trate == -99) {
                    return false;
                }
//                if(PolFlag==true )
//                {//如果是犹豫期撤单，则跟原来的比例一样
//                    wageRateValue = tLACommisionSet.get(1).getStandPrem();
//                    trate=tLACommisionSet.get(1).getStandPremRate();
//                }
//                else
//                {
//                    wageRateValue = Double.parseDouble(strWageRate);
//                }
                wageRateValue = Double.parseDouble(strWageRate);
                tLACommisionSchema.setStandPremRate(trate);
                tLACommisionSchema.setStandPrem(wageRateValue);
            }
            else if (calType.equals("04")) { //险种件数
//                if(PolFlag==true)
//                {//如果不是正常（比如为退保），则更原来的比例一样
//                    wageRateValue = tLACommisionSet.get(1).getCalCount();
//                }
//                else
//                {
//                    wageRateValue = Double.parseDouble(strWageRate);
//                }
                wageRateValue = Double.parseDouble(strWageRate);
                tLACommisionSchema.setCalCount(wageRateValue);
            }
        }
        mLACommisionSet.add(tLACommisionSchema);

        return true;
    }

    //计算非标准业务的提奖比例
    public boolean dealLastDate(LAContFYCRateSchema tLAContFYCRateSchema) {
        String tGrpContNo = tLAContFYCRateSchema.getGrpContNo();
        String tRiskCode = tLAContFYCRateSchema.getRiskCode();
        double tRate = tLAContFYCRateSchema.getRate(); //得到提奖比例
        LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionDB tLACommisionDB = new LACommisionDB();
      //增加了银行的非标,所以增加了个单的非标处理
        if(this.mBranchType.equals("3")&&this.mBranchType2.equals("01"))//sgh2008.01.25
        	tLACommisionDB.setContNo(tGrpContNo);
        else
        tLACommisionDB.setGrpContNo(tGrpContNo);
        tLACommisionDB.setRiskCode(tRiskCode);
        tLACommisionSet = tLACommisionDB.query();
        if(tLACommisionSet.size()==0)
        {
            tLACommisionDB.setGrpContNo("00000000000000000000");
            tLACommisionDB.setContNo(tGrpContNo);
            tLACommisionDB.setRiskCode(tRiskCode);
            tLACommisionSet = tLACommisionDB.query();
        }
        for (int k = 1; k <= tLACommisionSet.size(); k++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema = tLACommisionSet.get(k);
            String BranchType = tLACommisionSchema.getBranchType();
            String BranchType2 = tLACommisionSchema.getBranchType2();
            double tTransMoney = tLACommisionSchema.getTransMoney();
            String tCommisionsn = tLACommisionSchema.getCommisionSN();
            String tAgentCode = tLACommisionSchema.getAgentCode();
            String tWageNo = tLACommisionSchema.getWageNo();
            String Tsql = "select count(1) from lawage where  indexcalno='" +
                          tWageNo + "' and agentcode='" + tAgentCode + "' ";
            int pcount = Integer.parseInt(new ExeSQL().getOneValue(Tsql));
            if (pcount > 0) {
                continue;
            }

            String sql = "select * from lawagecalelement where riskcode='" +
                         tLACommisionSchema.getRiskCode() + "'";
            sql += " and branchType='" + BranchType + "'";
            sql += " and branchType2='" + BranchType2 + "' and caltype<>'91' "; //91 为特殊险种的描述,如境外旅游险种
            sql += " order by calorder  with ur";
            LAWageCalElementSet tLAWageCalElementSet = new LAWageCalElementSet();
            LAWageCalElementDB tLAWageCalElementDB = new LAWageCalElementDB();
            tLAWageCalElementSet = tLAWageCalElementDB.executeQuery(sql);
            if (tLAWageCalElementSet == null ||
                tLAWageCalElementSet.size() == 0) {
                continue;
            }
            for (int i = 1; i <= tLAWageCalElementSet.size(); i++) {
                LAWageCalElementSchema tLAWageCalElementSchema =
                        tLAWageCalElementSet.get(i);
                String calType = tLAWageCalElementSchema.getCalType();
                String calCode = tLAWageCalElementSchema.getCalCode();
                if ((calCode == null || calCode.equals("")) &&
                    !calType.equals("91")) { //91 为特殊险种,如境外旅游险种,没有计算编码,他们是为了作特殊处理的险种描述
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalDoNewBL";
                    tError.functionName = "calWage1";
                    tError.errorMessage = "佣金计算要素表中未找到计算编码!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                System.out.println("sql:" + sql);
                String tSQL = "select MakeDate from lawage where "
                              + " IndexCalNo = '" +
                              tLACommisionSchema.getWageNo() + "'"
                              + " and agentcode = '" +
                              tLACommisionSchema.getAgentCode() + "'";
                int tFlag = 0; //1 表示此条数据已算佣金  0 表示未算
                System.out.println(tSQL);
                ExeSQL tExeSQL = new ExeSQL();
                String tMakeDate = tExeSQL.getOneValue(tSQL);
                if (tExeSQL.mErrors.needDealError()) {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveBL";
                    tError.functionName = "judgeWageIsCal";
                    tError.errorMessage = "查询指标信息表的佣金指标出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tMakeDate == null || tMakeDate.equals("")) {
                    tFlag = 0; //佣金未算
                    //月度提奖比例为 提奖比例*实际发放系数




                    double tFYCRate = calWage0(tRate, tCommisionsn);
                    if (tFYCRate == -99) {
                        return false; // -1 表示异常
                    }

                    if (calType.equals("00")) { //直接佣金
                        if ("01".equals(tLACommisionSchema.getF2())) { //不算fyc
                            tLACommisionSchema.setStandFYCRate(0);
                            tLACommisionSchema.setDirectWage(0); //直接佣金
                        } else {
                            tLACommisionSchema.setStandFYCRate(tFYCRate);
                            tFYCRate = mulrate(tFYCRate,
                                               tLACommisionSchema.getTransMoney());
                            tFYCRate = Arith.round(tFYCRate, 2);
                            tLACommisionSchema.setDirectWage(tFYCRate); //直接佣金
                        }
                    } else if (calType.equals("10")) { //直接佣金,按帐户管理费计算   P7 * wageRateValue  20060226 xiangchun  add
                        if ("01".equals(tLACommisionSchema.getF2())) { //不算fyc
                            tLACommisionSchema.setStandFYCRate(0);
                            tLACommisionSchema.setDirectWage(0); //直接佣金

                        } else {
                            tLACommisionSchema.setStandFYCRate(tFYCRate);
    //                        tLACommisionSchema.setStandFYCRate("01");
                            tFYCRate = mulrate(tFYCRate,
                                               tLACommisionSchema.getP7()); //直接佣金,按帐户管理费计算   P7 * wageRateValue
                            tFYCRate = Arith.round(tFYCRate, 2);
                            tLACommisionSchema.setDirectWage(tFYCRate); //直接佣金
                        }
                    }
    //                    double tFYC = mulrate(tFYCRate, tTransMoney);
    //                    tFYC = round(tFYC, 2);
    //                    tLACommisionSchema.setStandFYCRate(tFYCRate);
    //                    tLACommisionSchema.setFYCRate(tFYCRate);
    //                    tLACommisionSchema.setDirectWage(tFYC);
    //                    tLACommisionSchema.setFYC(tFYC);
                    tLACommisionSchema.setModifyDate(CurrentDate);
                    tLACommisionSchema.setModifyTime(CurrentTime);

                } else {
                    tFlag = 1; //佣金已算,提奖比例不再修改
                    continue;
                }
            }
            mLACommisionSet.add(tLACommisionSchema);
        }
        return true;
    }
//计算非标准业务的提奖比例  -1 表示计算错误
    public double calWage0(double cRate, String tCommisionsn) {
        Calculator tCalculator = new Calculator(); //计算类
        System.out.println("Calcode:");
        tCalculator.setCalCode("AP0152"); //添加计算编码
        String tRate=mFRDecimalFormat.format(cRate);
         System.out.println("Calcode:aaaaaaaaaaaaaaaaaaaaa"+tRate);
        tCalculator.addBasicFactor("RATE", tRate); //交易金额
        tCalculator.addBasicFactor("COMMISIONSN", tCommisionsn); //交易金额
        if (tCalculator.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBL";
            tError.functionName = "calWage1";
            tError.errorMessage = tCalculator.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return -99;
        }
        //得到结果
        String tvalue = tCalculator.calculate();
        double tReturn = Double.parseDouble(tvalue);
        return tReturn;
    }

    //计算标保比例
    private double getStandPremRate(String tmanagecom, String triskcode,
                                    String tbranchtype, String tbranchtype2
                                    ,int tPayIntv,int tPayYear,int tPayYears) {
    	double trate;
    	String tFlag=null;
    	String tResult=null;
    	ExeSQL tExeSQL= new ExeSQL();
    	String tSQL="select 1 from ldcode where codetype='GSPRISKCODE'" +
    			" and codename='"+triskcode+"' and comcode='86' " +
    			"and othersign='1'";
    	tFlag=tExeSQL.getOneValue(tSQL);
    	if(tFlag!=null&&tFlag.equals("1")){
    		tFlag="1";
    	}else{
    		tFlag="0";
    	}
    	if(tFlag.endsWith("0")){
    		tSQL="select rate from laratestandprem where managecom='"
    			+tmanagecom+"' and riskcode='"+triskcode+"' and branchtype='"
    			+tbranchtype+"' and branchtype2='"+tbranchtype2+"' fetch first 1 rows only";
    		tResult=tExeSQL.getOneValue(tSQL);
    		if(tResult!=null&&!tResult.equals("")){
    			trate=Double.parseDouble(tResult);
    		}else{
    			tSQL="select rate from laratestandprem where managecom='86' and riskcode='"
    				+triskcode+"' and branchtype='"
        			+tbranchtype+"' and branchtype2='"+tbranchtype2+"' fetch first 1 rows only";
        		tResult=tExeSQL.getOneValue(tSQL);
        		if(tResult!=null&&!tResult.equals("")){
        			trate=Double.parseDouble(tResult);
        		}else{
        			CError tError = new CError();
                  tError.moduleName = "AgentWageCalDoBL";
                  tError.functionName = "dealdata";
                  tError.errorMessage = "查询不到" +
                                        triskcode +
                                        "险种的折标信息!";
                  this.mErrors.addOneError(tError);
                  return 0;
        		}
    		}
    	}else{
    		tSQL="select rate from laratestandprem where managecom='"
    			+tmanagecom+"' and riskcode='"+triskcode+"' and branchtype='"
    			+tbranchtype+"' and branchtype2='"+tbranchtype2
    			+"' and payintv='"+tPayIntv+"' and curyear="
    			+(tPayYear+1)+" and "+tPayYears+">=int(F01) and "
    			+tPayYears+"<int(F02)  fetch first 1 rows only";
    		tResult=tExeSQL.getOneValue(tSQL);
    		if(tResult!=null&&!tResult.equals("")){
    			trate=Double.parseDouble(tResult);
    		}else{
    			tSQL="select rate from laratestandprem where managecom='86' and riskcode='"
    				+triskcode+"' and branchtype='"
        			+tbranchtype+"' and branchtype2='"+tbranchtype2
        			+"' and payintv='"+tPayIntv+"' and curyear="
        			+(tPayYear+1)+" and "+tPayYears+">=int(F01) and "
        			+tPayYears+"<int(F02) fetch first 1 rows only";
        		tResult=tExeSQL.getOneValue(tSQL);
        		if(tResult!=null&&!tResult.equals("")){
        			trate=Double.parseDouble(tResult);
        		}else{
        			CError tError = new CError();
                  tError.moduleName = "AgentWageCalDoBL";
                  tError.functionName = "dealdata";
                  tError.errorMessage = "查询不到" +
                                        triskcode +
                                        "险种的折标信息!";
                  this.mErrors.addOneError(tError);
                  return 0;
        		}
    		}
    	}
//        LARateStandPremSchema tLARateStandPremSchema = new
//                LARateStandPremSchema();
//        LARateStandPremSet tLARateStandPremSet = new LARateStandPremSet();
//        LARateStandPremDB tLARateStandPremDB = new LARateStandPremDB();
//        tLARateStandPremDB.setBranchType(tbranchtype);
//        tLARateStandPremDB.setRiskCode(triskcode);
//        tLARateStandPremDB.setsex("0");
//        tLARateStandPremDB.setAppAge("0");
//        tLARateStandPremDB.setYear("0");
//        tLARateStandPremDB.setPayIntv("0");
//        tLARateStandPremDB.setCurYear("0");
////        tLARateStandPremDB.setF01("0");
////        tLARateStandPremDB.setF02("0");
//        tLARateStandPremDB.setBranchType2(tbranchtype2);
//        //管理机构为分公司
//        tLARateStandPremDB.setManageCom(tmanagecom);
//        tLARateStandPremSet = tLARateStandPremDB.query();
//        if (tLARateStandPremSet.size() <= 0 || tLARateStandPremSet == null) { //如果分公司未查询到，则按总公司计算
//            tLARateStandPremDB = new LARateStandPremDB();
//            tLARateStandPremDB.setBranchType(tbranchtype);
//            tLARateStandPremDB.setRiskCode(triskcode);
//            tLARateStandPremDB.setsex("0");
//            tLARateStandPremDB.setYear("0");
//            if(tFlag.equals("0")){
//            	tLARateStandPremDB.setAppAge("0");
//            	tLARateStandPremDB.setPayIntv("0");
//                tLARateStandPremDB.setCurYear("0");
//            }else{
//            	tLARateStandPremDB.setPayIntv(String.valueOf(tPayIntv));
//                tLARateStandPremDB.setCurYear(tPayYear+1);
//            }
//
////            tLARateStandPremDB.setF01("0");
////            tLARateStandPremDB.setF02("0");
//            tLARateStandPremDB.setBranchType2(tbranchtype2);
//            //管理机构为总公司
//            tLARateStandPremDB.setManageCom("86");
//            tLARateStandPremSet = new LARateStandPremSet();
//            tLARateStandPremSet = tLARateStandPremDB.query();
//            if (tLARateStandPremSet.size() <= 0 || tLARateStandPremSet == null) {
//                CError tError = new CError();
//                tError.moduleName = "AgentWageCalDoBL";
//                tError.functionName = "dealdata";
//                tError.errorMessage = "查询不到" +
//                                      triskcode +
//                                      "险种的折标信息!";
//                this.mErrors.addOneError(tError);
//                return 0;
//            }
//        }
//        tLARateStandPremSchema = tLARateStandPremSet.get(1);
//        //String tcodealias=tLARateStandPremSchema.getr;
//        //double tRate = Double.parseDouble(tcodealias);
//        double tRate = tLARateStandPremSchema.getrate();
        return trate;
    }

    public String findChannelByCom(String aAgentCom) {
        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setAgentCom(aAgentCom);
        if (!tLAComDB.getInfo()) {
            //@错误处理
            this.mErrors.copyAllErrors(tLAComDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentBL";
            tError.functionName = "findChannel";
            tError.errorMessage = "得到LACom数据出错！！！";
            this.mErrors.addOneError(tError);
        }
        return tLAComDB.getChannelType();
    }

    //将网点代理机构
    private boolean putAllAgentComHash(String cAgentCom, Hashtable cHash) {
        String tSql = "";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSql =
                "Select Trim(UpAgentCom),banktype From LACom Where Trim(AgentCom) = '" +
                cAgentCom + "' with ur";
        tSSRS = tExeSQL.execSQL(tSql);
        if (tSSRS.getMaxRow() == 1) {
            String[] tResult = tSSRS.getRowData(1);
            try {
                System.out.println("--upAgentCom:" + tResult[0]);
                System.out.println("--AgentCom:" + cAgentCom + "--BankType:" +
                                   tResult[1]);
                if (tResult[1] == null || tResult[1].trim().equals("")) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalDoNewBL";
                    tError.functionName = "putAllAgentComHash";
                    tError.errorMessage = cAgentCom + "代理机构的BankType为空！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                cHash.put(tResult[1], cAgentCom);
            } catch (Exception ex) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoBL";
                tError.functionName = "putAllAgentComHash";
                tError.errorMessage = "put哈希表出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //递归
            if (!tResult[1].trim().equals("00")) { //总行
                if (!putAllAgentComHash(tResult[0], cHash)) {
                    return false;
                }
            }

        }
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBL";
            tError.functionName = "queryLAWageHistory";
            tError.errorMessage = "佣金计算历史表中未找到相关数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    private void setChargeSchemaValue(LAChargeSchema cLAChargeSchema,
                                      LACommisionSchema cLACommisionSchema) {
        cLAChargeSchema.setCommisionSN(cLACommisionSchema.getCommisionSN());
        cLAChargeSchema.setCalDate(cLACommisionSchema.getTMakeDate());
        cLAChargeSchema.setWageNo(AgentPubFun.formatDate(cLACommisionSchema.
                getSignDate(), "yyyyMM"));
        cLAChargeSchema.setContNo(cLACommisionSchema.getContNo());
        cLAChargeSchema.setGrpPolNo(cLACommisionSchema.getGrpPolNo());
        cLAChargeSchema.setGrpContNo(cLACommisionSchema.getGrpContNo());
        cLAChargeSchema.setMainPolNo(cLACommisionSchema.getMainPolNo());
        cLAChargeSchema.setPolNo(cLACommisionSchema.getPolNo());
        cLAChargeSchema.setManageCom(cLACommisionSchema.getManageCom());
        cLAChargeSchema.setRiskCode(cLACommisionSchema.getRiskCode());
        cLAChargeSchema.setReceiptNo(cLACommisionSchema.getReceiptNo());
        cLAChargeSchema.setTransMoney(cLACommisionSchema.getTransMoney());
        cLAChargeSchema.setTMakeDate(cLACommisionSchema.getTMakeDate());
        cLAChargeSchema.setOperator(cLACommisionSchema.getOperator());
        cLAChargeSchema.setChargeState("0");
        cLAChargeSchema.setMakeDate(CurrentDate);
        cLAChargeSchema.setMakeTime(CurrentTime);
        cLAChargeSchema.setModifyDate(CurrentDate);
        cLAChargeSchema.setModifyTime(CurrentTime);
    }

    /**
     * 根据扫描日期以及销售渠道获取对应的费率版本信息
     * @param tScanDate
     * @param tBranchType
     * @return
     */
    private String getRiskRateVersion(String tScanDate, String tRiskCode,
                                      String tBranchType, String tBranchType2) {
        String riskRateVersion = "";
        String sql = "select getRiskRateVersion('" + tScanDate + "','" +
                     tRiskCode +
                     "','" + tBranchType +
                     "') from ldsysvar where sysvar='onerow'";
        System.out.println(sql);
        ExeSQL tExeSQL = new ExeSQL();
        riskRateVersion = tExeSQL.getOneValue(sql);
        System.out.println("riskRateVersion:" + riskRateVersion);
        return riskRateVersion;
    }

    //double类型乘法计算
    public static double mulrate(double v1, double v2) {

        BigDecimal b1 = new BigDecimal(Double.toString(v1));

        BigDecimal b2 = new BigDecimal(Double.toString(v2));

        return b1.multiply(b2).doubleValue();

    }

//   public static double round(double v,int scale){
//   String temp="#,##0.";
//   for (int i=0;i<scale ;i++ )
//   {
//   temp+="0";
//   }
//   return Double.parseDouble(new java.text.DecimalFormat(temp).format(v));
//}
    public static double round(double v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(Double.toString(v));
        BigDecimal one = new BigDecimal("1");
        return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public VData getResult() {
        mResult = new VData();
        mResult.add(mMap);
        return mResult;
    }

    /**
     *判断所输入退保的保单  的正常保单是否计算过fyc等，（同一天不可能出现不同的提奖比例 ，如果退保和
     * 正常保单在同一天提取，则需要重新计算fyc等数据
     */
    private LACommisionSet judgeWageIsCal(String cPolNo,String  tSignDate,String tTransState)
   {
       LACommisionDB tLACommisionDB = new LACommisionDB();
       String sql =
               "select * from lacommision where polno='" +
               cPolNo + "' and TransType='ZC' and SignDate='" + tSignDate +
               "' and  TransState='"+tTransState+"' and commisionsn=(select min(commisionsn) from lacommision where polno='" +
               cPolNo + "' and TransType='ZC' and substr(PayPlanCode,1,6)<>'000000'  and  TransState='"
               +tTransState+"' )  and makedate<>'"+CurrentDate+"'  with ur";
       System.out.println("judgeWageIsCal+sql:" + sql);
       LACommisionSet tLACommisionSet = new LACommisionSet();
       tLACommisionSet = tLACommisionDB.executeQuery(sql);

       return tLACommisionSet;
   }
    private boolean calWage5(LAWageCalElementSchema cLAWageCalElementSchema,
		            LACommisionSchema tLACommisionSchema) {
		int m = 0;
		LAWageCalElementSchema tLAWageCalElementSchema =cLAWageCalElementSchema;
		String calType = tLAWageCalElementSchema.getCalType();
		String calCode = tLAWageCalElementSchema.getCalCode();
		if ((calCode == null || calCode.equals("")) && !calType.equals("91")) { //91 为特殊险种,如境外旅游险种,没有计算编码,他们是为了作特殊处理的险种描述
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "AgentWageCalDoNewBL";
			tError.functionName = "calWage1";
			tError.errorMessage = "佣金计算要素表中未找到计算编码!";
			this.mErrors.addOneError(tError);
			return false;
		}
		Calculator tCalculator = new Calculator(); //计算类
		System.out.println("Calcode:" + calCode);
		tCalculator.setCalCode(calCode); //添加计算编码
		tCalculator.addBasicFactor("BRANCHTYPE",
                tLACommisionSchema.getBranchType());
		tCalculator.addBasicFactor("BRANCHTYPE2",
                tLACommisionSchema.getBranchType2());
		tCalculator.addBasicFactor("Prem",
		                      String.valueOf(tLACommisionSchema.
		   getTransMoney())); //交易金额
		tCalculator.addBasicFactor("P7",
		                      String.valueOf(tLACommisionSchema.
		   getP7())); //帐户管理费
		//填充要素
		
		tCalculator.addBasicFactor("RISKCODE",
		                      tLAWageCalElementSchema.getRiskCode());
		tCalculator.addBasicFactor("TMAKEDATE",
		                      tLACommisionSchema.getTMakeDate());
		tCalculator.addBasicFactor("CONTNO",
		                      tLACommisionSchema.getContNo());
		tCalculator.addBasicFactor("SCANDATE",
		                      tLACommisionSchema.getScanDate());
		tCalculator.addBasicFactor("FLAG",
		                      tLACommisionSchema.getFlag());
		tCalculator.addBasicFactor("PAYCOUNT",
		                      String.valueOf(tLACommisionSchema.
		   getPayCount()));
		//String a=String.valueOf(tLACommisionSchema.getReNewCount() );
		tCalculator.addBasicFactor("RENEWCOUNT",
		                      String.valueOf(tLACommisionSchema.
		   getReNewCount()));
		tCalculator.addBasicFactor("GRPCONTNO",
		                      tLACommisionSchema.getGrpContNo());
		tCalculator.addBasicFactor("BRANCHTYPE3","2");
		//添加套餐编码
		tCalculator.addBasicFactor("WRAPCODE",tLACommisionSchema.getF3());
		
		tCalculator.addBasicFactor("CALTYPE",
		                      tLAWageCalElementSchema.getCalType());
		tCalculator.addBasicFactor("COMMISIONSN",
		                      tLACommisionSchema.getCommisionSN());
		tCalculator.addBasicFactor("MANAGECOM", ManageCom);
		tCalculator.addBasicFactor("SIGNDATE",
		                      String.valueOf(tLACommisionSchema.
		   getSignDate()));
		tCalculator.addBasicFactor("YEARS",
		                      String.valueOf(tLACommisionSchema.
		   getYears()));
		tCalculator.addBasicFactor("PAYYEARS",
		                      String.valueOf(tLACommisionSchema.
		   getPayYears()));
		tCalculator.addBasicFactor("PAYYEAR",
		                      String.valueOf(tLACommisionSchema.
		   getPayYear()));
		tCalculator.addBasicFactor("PAYINTV",
		                      String.valueOf(tLACommisionSchema.
		   getPayIntv()));
		tCalculator.addBasicFactor("AGENTCOM",
		   String.valueOf(tLACommisionSchema.getAgentCom()));
		tCalculator.addBasicFactor("TRANSSTATE",
		                      String.valueOf(tLACommisionSchema.
		   getTransState()));
		tCalculator.addBasicFactor("POLTYPE", tLACommisionSchema.getPolType());
		tCalculator.addBasicFactor("WAGENO", tLACommisionSchema.getWageNo());
		tCalculator.addBasicFactor("AGENTCODE",
		                      tLACommisionSchema.getAgentCode());
		
		//得到结果
		String strWageRate = tCalculator.calculate();
		System.out.println("..................here1"+strWageRate);
		if (tCalculator.mErrors.needDealError()) {
			// @@错误处理
			//CError tError = new CError();
			//tError.moduleName = "AgentWageCalDoBL";
			//tError.functionName = "calWage1";
			//tError.errorMessage = tCalculator.mErrors.getFirstError();
			//this.mErrors.addOneError(tError);
			//return false;
			LAWageActivityLogSchema tLAWageActivityLogSchema = new
			       LAWageActivityLogSchema();
			tLAWageActivityLogSchema.setAgentCode(tLACommisionSchema.
			       getAgentCode());
			tLAWageActivityLogSchema.setAgentGroup(tLACommisionSchema.
			       getAgentGroup());
			tLAWageActivityLogSchema.setContNo(tLACommisionSchema.getContNo());
			if (calType.equals("00") || calType.equals("10")) {
			   tLAWageActivityLogSchema.setDescribe("K1薪资比率计算错误!");
			}
			if (calType.equals("03")) {
			   tLAWageActivityLogSchema.setDescribe("K1标准保费计算错误!");
			}
			if (calType.equals("06")) {
			   tLAWageActivityLogSchema.setDescribe("K1职团标准保费计算错误!");
			}else{
				tLAWageActivityLogSchema.setDescribe(calType+"类型K1错误!");
			}
			
			tLAWageActivityLogSchema.setGrpContNo(tLACommisionSchema.
			       getGrpContNo());
			tLAWageActivityLogSchema.setGrpPolNo(tLACommisionSchema.
			       getGrpPolNo());
			tLAWageActivityLogSchema.setMakeDate(CurrentDate);
			tLAWageActivityLogSchema.setMakeTime(CurrentTime);
			tLAWageActivityLogSchema.setManageCom(tLACommisionSchema.
			       getManageCom());
			tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
			tLAWageActivityLogSchema.setOtherNo("");
			tLAWageActivityLogSchema.setOtherNoType("");
			tLAWageActivityLogSchema.setPolNo(tLACommisionSchema.getPolNo());
			tLAWageActivityLogSchema.setRiskCode(tLACommisionSchema.
			       getRiskCode());
			String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
			tLAWageActivityLogSchema.setSerialNo(tSerialNo);
			tLAWageActivityLogSchema.setWageLogType("01");
			tLAWageActivityLogSchema.setWageNo(tLACommisionSchema.getWageNo());
			mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
			
		} else if (Double.parseDouble(strWageRate) == 0) {
			if (calType.equals("00") || calType.equals("10")) {
			   LAWageActivityLogSchema tLAWageActivityLogSchema = new
			           LAWageActivityLogSchema();
			   tLAWageActivityLogSchema.setAgentCode(tLACommisionSchema.
			           getAgentCode());
			   tLAWageActivityLogSchema.setAgentGroup(tLACommisionSchema.
			           getAgentGroup());
			   tLAWageActivityLogSchema.setContNo(tLACommisionSchema.
			           getContNo());
			   tLAWageActivityLogSchema.setDescribe("K1薪资比率计算为0!");
			   tLAWageActivityLogSchema.setGrpContNo(tLACommisionSchema.
			           getGrpContNo());
			   tLAWageActivityLogSchema.setGrpPolNo(tLACommisionSchema.
			           getGrpPolNo());
			   tLAWageActivityLogSchema.setMakeDate(CurrentDate);
			   tLAWageActivityLogSchema.setMakeTime(CurrentTime);
			   tLAWageActivityLogSchema.setManageCom(tLACommisionSchema.
			           getManageCom());
			   tLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
			   tLAWageActivityLogSchema.setOtherNo("");
			   tLAWageActivityLogSchema.setOtherNoType("");
			   tLAWageActivityLogSchema.setPolNo(tLACommisionSchema.
			           getPolNo());
			   tLAWageActivityLogSchema.setRiskCode(tLACommisionSchema.
			           getRiskCode());
			
			   String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG",
			           12);
			   tLAWageActivityLogSchema.setSerialNo(tSerialNo);
			   tLAWageActivityLogSchema.setWageLogType("01");
			   tLAWageActivityLogSchema.setWageNo(tLACommisionSchema.
			           getWageNo());
			   mLAWageActivityLogSet.add(tLAWageActivityLogSchema);
			}
		}
		//得到结果
        String strWageRate1 = tCalculator.calculate();
        double wageRateValue1 =Double.parseDouble(strWageRate1);
        tLACommisionSchema.setK1(wageRateValue1);
		return true;
    }
    
    private boolean checkOrphanSingleFlag(LACommisionSchema tLACommisionSchema,double strWageRate){
    	if("1".equals(tLACommisionSchema.getBranchType()) && "01".equals(tLACommisionSchema.getBranchType2())){
    		 double orgphanvalue = 0;
    		 String tSQL = "";
    		//续期
    		if(tLACommisionSchema.getReNewCount()== 0 && tLACommisionSchema.getPayYear()>0){
    			if("00000000000000000000".equals(tLACommisionSchema.getGrpContNo()))
    	       tSQL="select 1 from lacommision  where contno='"
    		    +tLACommisionSchema.getContNo()+"' and branchtype='"
    		    +tLACommisionSchema.getBranchType()+"' and branchtype2='"
    		    +tLACommisionSchema.getBranchType2()+"' and exists (select 1 from LAOrphanPolicyB where contno='"+tLACommisionSchema.getContNo()+"' " +
    		    		" union select 1 from LAOrphanPolicy where contno='"+tLACommisionSchema.getContNo()+"' )" +
    				" and exists (select 1 from lmriskapp where riskcode = '"+tLACommisionSchema.getRiskCode()+"' and riskperiod ='L')" +
    						" and tmakedate = '"+tLACommisionSchema.getTMakeDate()+"' " +
    								"and commisionsn ='"+tLACommisionSchema.getCommisionSN()+"' with ur ";
    	      else
    	    	  tSQL="select 1 from lacommision  where grpcontno='"
    	      		    +tLACommisionSchema.getGrpContNo()+"' and branchtype='"
    	      		    +tLACommisionSchema.getBranchType()+"' and branchtype2='"
    	      		    +tLACommisionSchema.getBranchType2()+"' and exists (select 1 from LAOrphanPolicyB where contno='"+tLACommisionSchema.getGrpContNo()+"'  " +
    	      		    		" union select 1 from LAOrphanPolicy where contno='"+tLACommisionSchema.getGrpContNo()+"' )" +
    	      				" and exists (select 1 from lmriskapp where riskcode = '"+tLACommisionSchema.getRiskCode()+"' and riskperiod ='L')" +
    	      						" and tmakedate = '"+tLACommisionSchema.getTMakeDate()+"' " +
    	      								"and commisionsn ='"+tLACommisionSchema.getCommisionSN()+"' with ur ";  
    	      
    			
    	      ExeSQL ejudgeSQL = new ExeSQL();
       	      String judgeFlag =  ejudgeSQL.getOneValue(tSQL);
       	      System.out.println("+++++++++++++judgeFlag的值："+judgeFlag+"");
       	      if(judgeFlag==null ||"".equals(judgeFlag)){
       	    	  //没找到长期险的孤儿单续期保单直接跳出
       	    	  return true;
       	      }else{
       	    	 if(tLACommisionSchema.getPayCount()>1 && tLACommisionSchema.getPayCount()<6)
       	    	 {
               	   	orgphanvalue = 0.5;
               	    strWageRate = mulrate(orgphanvalue, strWageRate);
         			tLACommisionSchema.setStandFYCRate(strWageRate);
         			strWageRate = mulrate(strWageRate,
                             tLACommisionSchema.
                             getTransMoney());
         			strWageRate = Arith.round(strWageRate, 2);
                     tLACommisionSchema.setDirectWage(strWageRate); //直接佣金
                     tLACommisionSchema.setP6(1);
               	 }
     			else{
     				orgphanvalue = 0.01;
     				strWageRate = mulrate(orgphanvalue,
                               tLACommisionSchema.
                               getTransMoney());
     				strWageRate = Arith.round(strWageRate, 2);
                       tLACommisionSchema.setDirectWage(strWageRate); //直接佣金
     				tLACommisionSchema.setStandFYCRate(orgphanvalue);
     				tLACommisionSchema.setP6(1);
     			} 
       	    	  return false;
       	    	  
       	      }
    	
    		}
    		//续保
    		else if(tLACommisionSchema.getReNewCount()>0){
    		  if("00000000000000000000".equals(tLACommisionSchema.getGrpContNo()))	
    			 tSQL= "select 1 from lacommision where contno= " +
    					"'"+tLACommisionSchema.getContNo()+"' and branchtype='"+tLACommisionSchema.getBranchType()+"' and branchtype2='" +
    							""+tLACommisionSchema.getBranchType2()+"' and exists(select 1 from LAOrphanPolicyB where contno='"+tLACommisionSchema.getContNo()+"'" +
    									" union select 1 from LAOrphanPolicy where contno='"+tLACommisionSchema.getContNo()+"'  )" +
    									" and exists(select 1 from lmriskapp where riskcode = '"+tLACommisionSchema.getRiskCode()+"' and riskperiod <>'L')" +
    											" and tmakedate = '"+tLACommisionSchema.getTMakeDate()+"'" +
    													" and commisionsn ='"+tLACommisionSchema.getCommisionSN()+"' with ur ";
    		  else
     			 tSQL= "select 1 from lacommision where grpcontno= " +
     					"'"+tLACommisionSchema.getGrpContNo()+"' and branchtype='"+tLACommisionSchema.getBranchType()+"' and branchtype2='" +
     							""+tLACommisionSchema.getBranchType2()+"' and exists(select 1 from LAOrphanPolicyB where contno='"+tLACommisionSchema.getGrpContNo()+"'" +
     									" union select 1 from LAOrphanPolicy where contno='"+tLACommisionSchema.getGrpContNo()+"' )" +
     									" and exists(select 1 from lmriskapp where riskcode = '"+tLACommisionSchema.getRiskCode()+"' and riskperiod <>'L')" +
     											" and tmakedate = '"+tLACommisionSchema.getTMakeDate()+"'" +
     													" and commisionsn ='"+tLACommisionSchema.getCommisionSN()+"' with ur ";
    		  
    			 ExeSQL ejudgeSQL = new ExeSQL();
          	      String judgeFlag =  ejudgeSQL.getOneValue(tSQL);
          	      System.out.println("+++++++++++++judgeFlag的值："+judgeFlag+"");
          	      if(judgeFlag==null ||"".equals(judgeFlag)){
          	    	  //没找到短期险的孤儿单续保保单直接跳出
          	    	  return true;
          	      }else{
          	    	orgphanvalue = 0.5;
          	    	strWageRate = mulrate(orgphanvalue, strWageRate);
        			tLACommisionSchema.setStandFYCRate(strWageRate);
        			strWageRate = mulrate(strWageRate,
                            tLACommisionSchema.
                            getTransMoney());
        			strWageRate = Arith.round(strWageRate, 2);
                    tLACommisionSchema.setDirectWage(strWageRate); //直接佣金
                    tLACommisionSchema.setP6(1);
          	    	  return false;
          	    	  
          	      }
    		}	
    		
       }
    	return true;
    }
  public int checkLaorphanPolicy(String commisionsn,int payyear,int RenewCount,String Contno,String GrpContNo,String RiskCode,String BranchType,String BranchType2,int paycount){
	  String judgeSQL ="";
	  String riskSQL="";
	  LACommisionDB tLACommisionDB = new LACommisionDB();
	  tLACommisionDB.setCommisionSN(commisionsn);
      tLACommisionDB.getInfo();
      int returnFlag=0;
	  ExeSQL ejudgeSQL = new ExeSQL();
	  if("1".equals(BranchType)&&"01".equals(BranchType2)){
	   if("00000000000000000000".equals(GrpContNo)){
		  judgeSQL = "select 1 from laorphanpolicy where branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"' and contno ='"+Contno+"' union select 1 from laorphanpolicyb where contno ='"+Contno+"' and   branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"' ";
	  }else{
		  judgeSQL = "select 1 from laorphanpolicy where contno ='"+GrpContNo+"' and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'  union select 1 from laorphanpolicyb where contno ='"+GrpContNo+"' and   branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"' ";
	    }
	
	  String judgeFlag =  ejudgeSQL.getOneValue(judgeSQL);
	  if(judgeFlag==null || "".equals(judgeFlag)){
	    	  //没找到孤儿单
		      returnFlag=0;
	      }else{
	    	  //对险种的判断
	    	  riskSQL= "select 'Y' from lmriskapp where riskcode ='"+RiskCode+"'and riskperiod='L' ";
	    	  String riskFlag= ejudgeSQL.getOneValue(riskSQL);
	    	  //判断续期与续保
	    	  if((RenewCount==0 && payyear>0)&& "Y".equals(riskFlag)){
	    		  if((paycount<6 && paycount>1) ){
	    			  returnFlag=1;
	    		  }else{
	    			  returnFlag=2;
	    		  }
	    		  
	    	  }else if(RenewCount>0 && !"Y".equals(riskFlag)){
	    		  returnFlag=3;
	    	  }
	      }
	  }else{
		  returnFlag=0;
	  } 
	  return returnFlag;
  }
}

