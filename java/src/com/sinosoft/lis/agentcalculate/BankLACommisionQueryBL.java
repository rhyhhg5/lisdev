package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BankLACommisionQueryBL {
    public BankLACommisionQueryBL() {
    }
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput() ;
    private String mManageCom="";
    private String mAgentCom= "";
    private String mWageNo = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mSiteManagerCode = "";
    private String mManageName="";

    private VData mInputData = new VData();
    private String mOperate = "";
    private SSRS mSSRS1 = new SSRS();
    private XmlExport mXmlExport = null;
    private PubFun mPubFun = new PubFun();
    private ListTable mListTable = new ListTable();
    private TransferData mTransferData = new TransferData();


    /**
   * 传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {

    mOperate = cOperate;
    mInputData = (VData) cInputData;
    if (mOperate.equals("")) {
        this.bulidError("submitData", "数据不完整");
        return false;
    }

    if (!mOperate.equals("PRINT")) {
        this.bulidError("submitData", "数据不完整");
        return false;
    }

    // 得到外部传入的数据，将数据备份到本类中
      if (!getInputData(mInputData)) {
          return false;
      }

      // 进行数据查询
   if (!dealdate()) {
       return false;
   }
   System.out.println("dayin");

   //进行数据打印
   if (!getPrintData()) {
       this.bulidError("getPrintData", "查询数据失败！");
       return false;
   }
 System.out.println("dayinchenggong1232121212121");

      return true;
  }

  /**
    * 取得传入的数据
    * @return boolean
    */
   private boolean getInputData(VData cInputData)
   {


       try
       {
           mGlobalInput.setSchema((GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
           mTransferData = (TransferData) cInputData.getObjectByObjectName(
                   "TransferData", 0);
            //页面传入的数据 三个
            this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
            this.mAgentCom = (String) mTransferData.getValueByName("tAgentCom");
            this.mWageNo = (String) mTransferData.getValueByName("tWageNo");
            this.mBranchType= (String) mTransferData.getValueByName("tBranchType");
            this.mBranchType2 = (String) mTransferData.getValueByName("tBranchType2");
            this.mSiteManagerCode = (String) mTransferData.getValueByName("tSiteManagerCode");

            System.out.println(mManageCom);

       } catch (Exception ex) {
           this.mErrors.addOneError("");
           return false;
       }

       return true;

}

/**
   * 获取打印所需要的数据
   * @param cFunction String
   * @param cErrorMsg String
   */
  private void bulidError(String cFunction, String cErrorMsg) {

      CError tCError = new CError();

      tCError.moduleName = "LARiskContDetailReportBL";
      tCError.functionName = cFunction;
      tCError.errorMessage = cErrorMsg;

      this.mErrors.addOneError(tCError);

 }


 /**
   * 业务处理方法
   * @return boolean
   */

  private boolean dealdate()
  {
     System.out.println("chuli1");
     //查询数据
     if (!getAgentNow())
     {
        return false;
     }

     return true;
}

 /**
   * 查询数据
   * @return boolean
  */
  private boolean getAgentNow() {
	  //查询统一工号： 2014-11-27  解青青
//	  ExeSQL tttExeSQL = new ExeSQL();
//		  String tttGAC = "select agentcode from laagent where groupagentcode='"+mSiteManagerCode+"'";
//		  String tttAgentCode = tttExeSQL.getOneValue(tttGAC);
//		  System.out.println("mmmmmm");
//		  System.out.println(tttAgentCode);
		  
      ExeSQL tExeSQL = new ExeSQL();
      String strCon="and tmakedate >= (select startdate from  lastatsegment  WHERE STATTYPE='1' AND YEARMONTH =int("+mWageNo+") )"
                     + " and tmakedate <= (select enddate from  lastatsegment  WHERE STATTYPE='1' AND YEARMONTH =int("+mWageNo+") )" ;

       String  sql="select ManageCom,AgentCom,getUniteCode(AgentCode),(select Name from laagent where laagent.agentcode =lacommision.agentcode ),GrpContNo,AppntNo,RiskCode,TransMoney,FYCRate,FYC,(select codename from ldcode where ldcode.codetype='paymode' and ldcode.code = lacommision.PayMode),TPayDate  from lacommision  where 1=1  "
            + " and GrpContNo <> '00000000000000000000'"

            + "and managecom like '"+mManageCom+"%' "
            + " and BranchType ='" + mBranchType + "' "
            + " and BranchType2 ='" + mBranchType2 + "' "
            + strCon;
 if(mAgentCom!=null&&!mAgentCom.equals(""))
 {  sql=sql + " and AgentCom ='" + mAgentCom + "' ";}
 if(mSiteManagerCode!=null&&!mSiteManagerCode.equals(""))
	 
  {  sql=sql + " and AgentCode ='" + mSiteManagerCode + "' ";}
     sql=sql + " union all "
                + "select ManageCom,AgentCom,getUniteCode(AgentCode),(select Name from laagent where laagent.agentcode =lacommision.agentcode ),ContNo,AppntNo,RiskCode,TransMoney,FYCRate,FYC,(select codename from ldcode where ldcode.codetype='paymode' and ldcode.code = lacommision.PayMode),TPayDate  from lacommision  where 1=1  "
            + " and GrpContNo = '00000000000000000000'"
            + "and managecom like '"+mManageCom+"%' "
           + " and BranchType ='" + mBranchType + "' "
           + " and BranchType2 ='" + mBranchType2 + "' "
           + strCon;
if(mAgentCom!=null&&!mAgentCom.equals(""))
{  sql=sql + " and AgentCom ='" + mAgentCom + "' ";}
if(mSiteManagerCode!=null&&!mSiteManagerCode.equals(""))
 {  sql=sql + " and AgentCode ='" + mSiteManagerCode + "' ";}


      mSSRS1 = tExeSQL.execSQL(sql);
      System.out.println(sql);
      if (tExeSQL.mErrors.needDealError()) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "查询XML数据出错！";

          this.mErrors.addOneError(tCError);

          return false;

      }
      if (mSSRS1.getMaxRow() <= 0) {
          CError tCError = new CError();
          tCError.moduleName = "MakeXMLBL";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的信息！";

          this.mErrors.addOneError(tCError);

          return false;
      }

      return true;
  }

   /**
    *
    * @return boolean
    */
   private boolean getPrintData() {
       TextTag tTextTag = new TextTag();
       mXmlExport = new XmlExport();
       //设置模版名称
       mXmlExport.createDocument("BankLACommisionQuery.vts", "printer");

       String tMakeDate = "";
       String tMakeTime = "";

       tMakeDate = mPubFun.getCurrentDate();
       tMakeTime = mPubFun.getCurrentTime();

       System.out.print("dayin252");
      if (!getManageName()) {
           return false;
       }
       tTextTag.add("MakeDate", tMakeDate);
       tTextTag.add("MakeTime", tMakeTime);
       tTextTag.add("tName", mManageName);

       System.out.println("1212121" + tMakeDate);
       if (tTextTag.size() < 1) {
           return false;
       }

       mXmlExport.addTextTag(tTextTag);

       String[] title = {"", "", "", "", "" ,"","" ,"","","" ,"",""};

       if (!getListTable()) {
           return false;
       }
       System.out.println("111");
       mXmlExport.addListTable(mListTable, title);
       System.out.println("121");
       mXmlExport.outputDocumentToFile("c:\\", "new1");
       this.mResult.clear();

       mResult.addElement(mXmlExport);

       return true;
   }

   /**
    * 查询列表显示数据
    * @return boolean
    */
   private boolean getListTable() {
       System.out.println("dayimboiap288");
       System.out.println(mSSRS1.getMaxRow());
       if (mSSRS1.getMaxRow() > 0) {
           for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
               String Info[] = new String[12];
               Info[0] = mSSRS1.GetText(i, 1);
               Info[1] = mSSRS1.GetText(i, 2);
               Info[2] = mSSRS1.GetText(i, 3);
               Info[3] = mSSRS1.GetText(i, 4);
               Info[4] = mSSRS1.GetText(i, 5);
               Info[5] = mSSRS1.GetText(i, 6);
               Info[6] = mSSRS1.GetText(i, 7);
               Info[7] = mSSRS1.GetText(i, 8);
               Info[8] = mSSRS1.GetText(i, 9);
               Info[9] = mSSRS1.GetText(i, 10);
               Info[10] = mSSRS1.GetText(i, 11);
               Info[11] = mSSRS1.GetText(i, 12);

               mListTable.add(Info);

               System.out.println(Info[3]);
               System.out.println("dayin305");
           }
            mListTable.setName("Order");
       } else {
           CError tCError = new CError();
           tCError.moduleName = "CreateXml";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的信息！";
           this.mErrors.addOneError(tCError);
           return false;
       }
       return true;
   }

   private boolean getManageName() {

       String sql = "select name from ldcom where comcode='" + mManageCom +
                    "'";

       SSRS tSSRS = new SSRS();

       ExeSQL tExeSQL = new ExeSQL();

       tSSRS = tExeSQL.execSQL(sql);

       if (tExeSQL.mErrors.needDealError()) {

           this.mErrors.addOneError("销售单位不存在！");

           return false;

       }

       if (mManageCom.equals("86")) {
           this.mManageName = "";
       } else {
           if(mManageCom.length()>4)
           {this.mManageName = tSSRS.GetText(1, 1) + "分公司";}
           else
           {this.mManageName = tSSRS.GetText(1, 1);}
       }

       return true;
   }


   /**
    * 获取打印所需要的数据
    * @param cFunction String
    * @param cErrorMsg String
    */


   /**
    *
    * @return VData
    */
   public VData getResult() {
       return mResult;
 }

}
