package com.sinosoft.lis.agentcalculate;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.LARateStandPremSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.Reflections;

public class LARateStandPremBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap = new MMap();
    private LARateStandPremSchema mLARateStandPremSchema = new LARateStandPremSchema();
    private LARateStandPremSchema InsetLARateStandPremSchema = new LARateStandPremSchema();

    public LARateStandPremBL() {
    }

    public static void main(String[] args)
    {

    }

    public boolean submitData(VData cInputData, String cOperate)
  {
      //将操作数据拷贝到本类中
      this.mOperate = cOperate;
      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData(cInputData))
      {
          return false;
      }
      //进行业务处理
      if (!dealData())
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LARateStandPremBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LARateStandPremBL-->dealData!";
          this.mErrors.addOneError(tError);
          return false;
      }
      //准备往后台的数据
      if (!prepareOutputData())
      {
          return false;
      }
      if (this.mOperate.equals("QUERY||MAIN"))
      {
          this.submitquery();
      }
      else
      {
          PubSubmit tPubSubmit = new PubSubmit();
          System.out.println("Start LARateStandPremBL Submit...");
          if (!tPubSubmit.submitData(mInputData, mOperate))
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tPubSubmit.mErrors);
              CError tError = new CError();
              tError.moduleName = "LARateStandPremBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据提交失败!";
              this.mErrors.addOneError(tError);
              return false;
        }
      }
      mInputData = null;
      return true;
  }
  private boolean getInputData(VData cInputData)
   {
       this.mLARateStandPremSchema.setSchema((LARateStandPremSchema) cInputData.
                                    getObjectByObjectName(
                                            "LARateStandPremSchema", 0));
     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
  //     this.mContType=(String)cInputData.getObjectByObjectName("String",0);

       return true;
   }
   private boolean dealData()
   {
       String currentDate=PubFun.getCurrentDate();
       String currentTime=PubFun.getCurrentTime();
       if (mOperate.equals("INSERT||MAIN"))
       {
           String sql="select * from LARateStandPrem  where managecom='"+
                      mLARateStandPremSchema.getManageCom()+"'  and riskcode='"+
                      mLARateStandPremSchema.getRiskCode()+"' ";
            LARateStandPremDB tLARateStandPremDB=new LARateStandPremDB();
            LARateStandPremSet tLARateStandPremset=new LARateStandPremSet();
            tLARateStandPremset=tLARateStandPremDB.executeQuery(sql);
            if(tLARateStandPremset.size()>0)
            {
                CError tError = new CError();
                tError.moduleName = "LAAddressBL";
                tError.functionName = "dealData";
                tError.errorMessage = "机构为"+mLARateStandPremSchema.getManageCom()+"险种为"+mLARateStandPremSchema.getRiskCode()+"的信息已存在，不能再进行录入。";
                this.mErrors.addOneError(tError);
                return false;
            }
           mLARateStandPremSchema.setMakeDate(currentDate);
           mLARateStandPremSchema.setMakeTime(currentTime);
           mLARateStandPremSchema.setModifyDate(currentDate);
           mLARateStandPremSchema.setModifyTime(currentTime);
           mLARateStandPremSchema.setOperator(mGlobalInput.Operator);
           mMap.put(this.mLARateStandPremSchema, "INSERT");
       }
       if (mOperate.equals("UPDATE||MAIN"))
     {
         String sql="select * from LARateStandPrem  where managecom='"+
                    mLARateStandPremSchema.getManageCom()+"'  and riskcode='"+
                    mLARateStandPremSchema.getRiskCode()+"' ";
          LARateStandPremDB tLARateStandPremDB=new LARateStandPremDB();
          LARateStandPremSet tLARateStandPremset=new LARateStandPremSet();
          tLARateStandPremset=tLARateStandPremDB.executeQuery(sql);
          if(tLARateStandPremset.size()<=0||tLARateStandPremset==null)
          {
              CError tError = new CError();
              tError.moduleName = "LAAddressBL";
              tError.functionName = "dealData";
              tError.errorMessage = "没有查询到机构为"+mLARateStandPremSchema.getManageCom()+"险种为"+mLARateStandPremSchema.getRiskCode()+"的信息。";
              this.mErrors.addOneError(tError);
             return false;
          }
          LARateStandPremSchema tLARateStandPremSchema=new LARateStandPremSchema();
          tLARateStandPremSchema=tLARateStandPremset.get(1);
          InsetLARateStandPremSchema=tLARateStandPremSchema;
          Reflections tReflections = new Reflections();
          LARateStandPremBSchema tLARateStandPremBSchema = new LARateStandPremBSchema();
          tReflections.transFields(tLARateStandPremBSchema, tLARateStandPremSchema);
          tLARateStandPremBSchema.setEdorType("02");//修改备份
          String tEdorNo=com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("LARateStandPrem",20);
          tLARateStandPremBSchema.setEdorNo(tEdorNo);
          tLARateStandPremBSchema.setStartDate(currentDate);
          tLARateStandPremBSchema.setEndDate(currentDate);

          mLARateStandPremSchema.setMakeDate(tLARateStandPremSchema.getMakeDate());
          mLARateStandPremSchema.setMakeTime(tLARateStandPremSchema.getMakeTime());
          mLARateStandPremSchema.setModifyDate(currentDate);
          mLARateStandPremSchema.setModifyTime(currentTime);
          mLARateStandPremSchema.setOperator(mGlobalInput.Operator);
          mMap.put(this.InsetLARateStandPremSchema, "DELETE");
          mMap.put(this.mLARateStandPremSchema, "INSERT");
          mMap.put(tLARateStandPremBSchema, "INSERT");
     }
     if (mOperate.equals("DELETE||MAIN"))
     {
         String sql="select * from LARateStandPrem  where managecom='"+
                    mLARateStandPremSchema.getManageCom()+"'  and riskcode='"+
                    mLARateStandPremSchema.getRiskCode()+"' ";
          LARateStandPremDB tLARateStandPremDB=new LARateStandPremDB();
          LARateStandPremSet tLARateStandPremset=new LARateStandPremSet();
          tLARateStandPremset=tLARateStandPremDB.executeQuery(sql);
          if(tLARateStandPremset.size()<=0||tLARateStandPremset==null)
          {
              CError tError = new CError();
              tError.moduleName = "LAAddressBL";
              tError.functionName = "dealData";
              tError.errorMessage = "没有查询到机构为"+mLARateStandPremSchema.getManageCom()+"险种为"+mLARateStandPremSchema.getRiskCode()+"的信息。";
              this.mErrors.addOneError(tError);
             return false;
          }
          LARateStandPremSchema tLARateStandPremSchema=new LARateStandPremSchema();
          tLARateStandPremSchema=tLARateStandPremset.get(1);
          Reflections tReflections = new Reflections();
          LARateStandPremBSchema tLARateStandPremBSchema = new LARateStandPremBSchema();
          tReflections.transFields(tLARateStandPremBSchema, tLARateStandPremSchema);
          tLARateStandPremBSchema.setEdorType("01");//删除备份
          String tEdorNo=com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("LARateStandPrem",20);
          tLARateStandPremBSchema.setEdorNo(tEdorNo);
          tLARateStandPremBSchema.setStartDate(currentDate);
          tLARateStandPremBSchema.setEndDate(currentDate);
          mMap.put(this.mLARateStandPremSchema, "DELETE");
          mMap.put(tLARateStandPremBSchema, "INSERT");
     }

       return true;
   }
   private boolean submitquery()
   {
       return true;
   }
   private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
           this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARateStandPremBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
       {
           this.mResult .clear() ;
        //   this.mResult .add(mProtocalNo) ;
           return mResult;
    }

}
