/*
 * <p>ClassName: AgentWageCalSaveUI </p>
 * <p>Description: LAAgentUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-31
 */
package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentWageCalSaveUI
{

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData = new VData();
    private String mOperate;
    private String ManageCom;
    private String BranchType;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();

    public AgentWageCalSaveUI()
    {}

    public static void main(String[] args)
    {
        AgentWageCalSaveUI tAgentWageCalSaveUI = new AgentWageCalSaveUI();
        VData tVData = new VData();
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        tLAWageLogSchema.setManageCom("86110000");
        tLAWageLogSchema.setWageYear("2006");
        tLAWageLogSchema.setWageMonth("12");
        tLAWageLogSchema.setBranchType("3");
        tLAWageLogSchema.setBranchType2("01");
        tLAWageLogSchema.setStartDate("2006-12-30");
        tLAWageLogSchema.setEndDate("2006-12-30");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        tVData.addElement(tG);

        tVData.addElement(tLAWageLogSchema);
        try
        {
            tAgentWageCalSaveUI.submitData(tVData, "INSERT||AGENTWAGE");
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        System.out.println("Error:" + tAgentWageCalSaveUI.mErrors.getFirstError());

        AgentWageCalDoUI tAgentWageCalDoUI = new AgentWageCalDoUI();
        tVData = new VData();
        tLAWageLogSchema = new LAWageLogSchema();
        tLAWageLogSchema.setManageCom("86110000");
        tLAWageLogSchema.setWageYear("2006");
        tLAWageLogSchema.setWageMonth("12");
        tLAWageLogSchema.setBranchType("3");
        tLAWageLogSchema.setBranchType2("01");
        tLAWageLogSchema.setStartDate("2006-12-30");
        tLAWageLogSchema.setEndDate("2006-12-30");

        tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        tVData.addElement(tG);

        tVData.addElement(tLAWageLogSchema);

        tAgentWageCalDoUI.submitData(tVData, "INSERT||AGENTWAGE");
        System.out.println("Error:" + tAgentWageCalSaveUI.mErrors.getFirstError());

    }


    /**
     *传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        AgentWageCalSaveNewBL tAgentWageCalSaveNewBL = new AgentWageCalSaveNewBL();
        System.out.println("Start AgentWageCalSave UI Submit...");
        tAgentWageCalSaveNewBL.submitData(mInputData, mOperate);
        System.out.println("End AgentWageCalSave UI Submit...");
        if (tAgentWageCalSaveNewBL.mErrors.needDealError())
        {
            // @@错误处理
            System.out.println("Error: " +
                               tAgentWageCalSaveNewBL.mErrors.getFirstError());
            this.mErrors.copyAllErrors(tAgentWageCalSaveNewBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
//        mResult = tAgentWageCalSaveNewBL.getResult();
        //附加的特殊处理
//      DataOdditionalManageBL tDataOdditionalManageBL = new DataOdditionalManageBL();
//      mInputData.clear();
//      mInputData.add(ManageCom);
//      mInputData.add(this.mLAWageLogSchema.getEndDate());
//      System.out.println("Start DataOdditionalManage ...");
//      tDataOdditionalManageBL.submitData(mInputData);
//      System.out.println("End DataOdditionalManage ...");
//      if (tDataOdditionalManageBL.mErrors.needDealError())
//      {
//         // @@错误处理
//         this.mErrors.copyAllErrors(tDataOdditionalManageBL.mErrors);
//         CError tError = new CError();
//         tError.moduleName = "AgentWageCalSaveUI";
//         tError.functionName = "submitData";
//         tError.errorMessage = "特殊处理数据提交失败!";
//         this.mErrors.addOneError(tError) ;
//         return false;
//      }
        System.out.println("over");
//      mInputData=null;
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(mGlobalInput);
            mInputData.add(mLAWageLogSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
            mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
            mLAWageLogSchema = ((LAWageLogSchema) cInputData.
                                getObjectByObjectName("LAWageLogSchema", 0));
            System.out.println(mGlobalInput==null?"null":mGlobalInput.toString()) ;
            System.out.println(mLAWageLogSchema==null?"null":mLAWageLogSchema.toString()) ;
            if (mGlobalInput == null || mLAWageLogSchema == null )
            {
                System.out.println("has null");
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSaveUI";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
