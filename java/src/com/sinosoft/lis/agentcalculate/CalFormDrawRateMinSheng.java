package com.sinosoft.lis.agentcalculate;

import java.text.SimpleDateFormat;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LDCodeRelaDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LDCodeRelaSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LDCodeRelaSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class CalFormDrawRateMinSheng
{

    public final static String RECALFYCFLAG = "RECALFYC||AGENTWAGE";
    public final static String CALFLAG = "INSERT||AGENTWAGE";
    public CErrors mErrors = new CErrors();
    private LACommisionSet mSet = new LACommisionSet();
    private LACommisionSet mNewSet = new LACommisionSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private String mAddStartDate = "";
    private String mToday = "";
    private String mYesterday = "";
    private float rate = getRate();
    private VData tResult = new VData();
    private String mAgentGrade = ""; //组织归属处调用所需
    private String mOperate;
    private String mIncludeFlag = "";
    private String[] mStandard;
    private double mCurrentStandard = 0; //该代理人当前时间应该达到的标准
    private double mCurrDirectWage = -1;
    private int mIntv = -1;
    private boolean mSamePerson = false; //当前一笔单子和上一笔单子是不是同一个人的业务
    private boolean mCurrRebate = false; //true表示不需要打折，false表示需要的打折
    public CalFormDrawRateMinSheng()
    {
    }

    public static void main(String[] args)
    {

        LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionDB tLACommisionDB = new LACommisionDB();
        String sql = "select * from lacommision  where managecom='86321203' and agentcode='8632004536' and wageno='200408' order by AgentCode,caldate,polno,ReceiptNo,RiskCode";
        tLACommisionSet = tLACommisionDB.executeQuery(sql);
        CalFormDrawRateMinSheng tCalFormDrawRate = new CalFormDrawRateMinSheng();
        VData tVData = new VData();
        tVData.add(tLACommisionSet);
        tVData.add("");
        tCalFormDrawRate.submitData(tVData, "RECALFYC||AGENTWAGE");
        LACommisionSet s = new LACommisionSet();
        s = (LACommisionSet) (tCalFormDrawRate.getResult().firstElement());
        for (int i = 1; i <= s.size(); i++)
        {
            System.out.println(s.get(i).getDirectWage() + "----" +
                               s.get(i).getFYC());
        }
    }

    /**
     * 获取传入的数据
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData");
        this.mSet = (LACommisionSet) cInputData.getObjectByObjectName(
                "LACommisionSet", 0);
        this.mAgentGrade = (String) cInputData.getObjectByObjectName("String",
                0);
        if (mSet == null)
        {
            // @@错误处理
            System.out.println("is null");
            CError tError = new CError();
            tError.moduleName = "CalFormDrawRate";
            tError.functionName = "getInputData";
            tError.errorMessage = "未取得足够的数据。";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("not null");
        return true;
    }

    /**
     * 校验数据的正确性
     * @return
     */
    private boolean check()
    {
        // add some check operations
        System.out.println("check");
        return true;
    }

    /**
     * 进行业务处理
     * @return
     */
    private boolean dealData()
    {
        System.out.println("------------开始打折处理---------");

        String tBranchType = "";
        String tAgentCode = "";
        String tCurrAgentCode = "";
        String tFlag = "";
        String tAgentGrade = "";
        double tMoney = 0;
        int tPayYear = 0;

        String tMakeDate = "";
        String tLastMakeDate = "";

        for (int i = 1; i <= mSet.size(); i++)
        {
            LACommisionSchema tSch = new LACommisionSchema();
            System.out.println("------------calRate--------" + i + "----");

            tSch.setSchema(mSet.get(i));
            getCalDate(tSch.getTMakeDate());

            tMakeDate = tSch.getTMakeDate().trim();
            tCurrAgentCode = tSch.getAgentCode();

            if (!tAgentCode.equals(tCurrAgentCode))
            {
                mSamePerson = false;
                mCurrRebate = false;
//        mCurrDirectWage=-1;
                LAAgentDB tLAAgentDB = new LAAgentDB();
                LATreeDB tLATreeDB = new LATreeDB();
                tLAAgentDB.setAgentCode(tCurrAgentCode);
                if (!tLAAgentDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalFormDrawRate";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询代理人" + tCurrAgentCode + "的基础信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                this.mLAAgentSchema = tLAAgentDB.getSchema();

                tLATreeDB.setAgentCode(tCurrAgentCode);
                if (!tLATreeDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalFormDrawRate";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询代理人" + tCurrAgentCode + "的行政信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                this.mLATreeSchema = tLATreeDB.getSchema();
                this.mAddStartDate = this.mLATreeSchema.getStartDate();
                System.out.println("this.mAddStartDate:" + this.mAddStartDate);
            }
            else
            {
                mSamePerson = true;
            }

            if (this.mAgentGrade.trim().equals("") || this.mAgentGrade == null)
            {
                tAgentGrade = this.mLATreeSchema.getAgentGrade().trim();
            }
            else
            {
                tAgentGrade = this.mAgentGrade.trim();
            }

            System.out.println("----------tAgentGrade-----" + tAgentGrade);
            if (tAgentGrade == null || tAgentGrade.equals(""))
            {
                return false;
            }
            tPayYear = tSch.getPayYear();
            tBranchType = tSch.getBranchType();

            if (tBranchType.trim().equals("1"))
            {
                if (tPayYear == 0)
                {
                    System.out.println("---CommisionSN:" + tSch.getPolNo());
                    System.out.println("---agentcode--:" + tSch.getAgentCode());

                    if (tAgentGrade.compareTo("A01") > 0 ||
                        tSch.getCommDire().equals("2"))
                    {
                        tSch.setFYC(tSch.getDirectWage());
                        tSch.setFYCRate(tSch.getStandFYCRate());
                    }
                    else
                    {
                        System.out.println("---tCurrAgentCode--" +
                                           tCurrAgentCode);
                        System.out.println("---tAgentCode--" + tAgentCode);
                        if (tCurrAgentCode.equals(tAgentCode))
                        {
                            System.out.println("--tLastMakeDate : " +
                                               tLastMakeDate);
                            System.out.println("--tMakeDate : " + tMakeDate);
                            System.out.println("--tMoney : " + tMoney);
                            if (!tLastMakeDate.trim().equals(tMakeDate))
                            {
                                tMoney = 0;
                            }

                            tSch = doCalA(tSch, tMoney);
                            if (tSch == null)
                            {
                                buildError("dealData", "打折处理时出错");
                                return false;
                            }
                            System.out.println("----before rebate----");
                            System.out.println(tSch.getAgentCode());
                            System.out.println(tSch.getStandFYCRate());
                            System.out.println(tSch.getDirectWage());
                            System.out.println("----after rebate----");
                            System.out.println(tSch.getAgentCode());
                            System.out.println(this.mCurrRebate);
                            System.out.println(tSch.getFYCRate());
                            System.out.println(tSch.getFYC());
                            tMoney = tMoney + tSch.getDirectWage();
                        }
                        else
                        {
                            tMoney = 0;
                            tSch = doCalA(tSch, tMoney);
                            if (tSch == null)
                            {
                                buildError("dealData", "打折处理时出错");
                                return false;
                            }
                            System.out.println("----before rebate----");
                            System.out.println(tSch.getAgentCode());
                            System.out.println(tSch.getStandFYCRate());
                            System.out.println(tSch.getDirectWage());
                            System.out.println("----after rebate----");
                            System.out.println(tSch.getAgentCode());
                            System.out.println(this.mCurrRebate);
                            System.out.println(tSch.getFYCRate());
                            System.out.println(tSch.getFYC());
                            tMoney = tSch.getDirectWage();
                        }
                    }
                }
                else
                {
                    tSch.setFYC(tSch.getDirectWage());
                    tSch.setFYCRate(tSch.getStandFYCRate());
                }
            }
            //团险
            if (tBranchType.trim().equals("2"))
            {
                if (mLAAgentSchema.getInsideFlag() != null &&
                    mLAAgentSchema.getInsideFlag().equals("0"))
                {
                    tSch.setFYC(0);
                    tSch.setFYCRate(0);
                    tSch.setGrpFYC(0);
                    tSch.setGrpFYCRate(0);
                    tSch.setDepFYC(0);
                    tSch.setDepFYCRate(0);
                }
                else
                {
                    tSch.setFYC(tSch.getDirectWage());
                    tSch.setFYCRate(tSch.getStandFYCRate());
                }
            }
            //银代
            if (tBranchType.trim().equals("3"))
            {
                if (mLAAgentSchema.getInsideFlag() != null &&
                    mLAAgentSchema.getInsideFlag().equals("0"))
                {
                    tSch.setFYC(0);
                    tSch.setFYCRate(0);
                    tSch.setGrpFYC(0);
                    tSch.setGrpFYCRate(0);
                    tSch.setDepFYC(0);
                    tSch.setDepFYCRate(0);
                }
                else
                {
                    if (tAgentGrade.trim().compareTo("B01") > 0 ||
                        tSch.getCommDire().equals("2"))
                    {
                        tSch.setFYC(tSch.getDirectWage());
                        tSch.setFYCRate(tSch.getStandFYCRate());
                    }
                    else
                    {
                        tSch = DoCalB(tSch);
                    }
                }
            }

            this.mNewSet.add(tSch);
            tAgentCode = tCurrAgentCode;
            tLastMakeDate = tMakeDate.trim();
        }
        return true;
    }

    private boolean dealData1()
    {
        System.out.println("------------开始打折处理---------");

        String tBranchType = "";
        String tAgentCode = "";
        String tCurrAgentCode = "";
        String tFlag = "";
        String tAgentGrade = "";
        double tMoney = 0;
        int tPayYear = 0;

        String tCalDate = "";
        String tLastCalDate = "";

        for (int i = 1; i <= mSet.size(); i++)
        {
            LACommisionSchema tSch = new LACommisionSchema();
            System.out.println("------------calRate--------" + i + "----");

            tSch.setSchema(mSet.get(i));

            getCalDate(tSch.getCalDate());

            tCalDate = tSch.getCalDate().trim();
            tCurrAgentCode = tSch.getAgentCode();

            if (!tAgentCode.equals(tCurrAgentCode))
            {
                mSamePerson = false;
                mCurrRebate = false;
//        mCurrDirectWage=-1;
                tMoney = 0;
                LAAgentDB tLAAgentDB = new LAAgentDB();
                LATreeDB tLATreeDB = new LATreeDB();
                tLAAgentDB.setAgentCode(tCurrAgentCode);
                if (!tLAAgentDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalFormDrawRate";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询代理人" + tCurrAgentCode + "的基础信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                this.mLAAgentSchema = tLAAgentDB.getSchema();

                tLATreeDB.setAgentCode(tCurrAgentCode);
                if (!tLATreeDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "CalFormDrawRate";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询代理人" + tCurrAgentCode + "的行政信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                this.mLATreeSchema = tLATreeDB.getSchema();
                this.mAddStartDate = tLATreeDB.getStartDate();
            }
            else
            {
                mSamePerson = true;
            }

            if (this.mAgentGrade == null || this.mAgentGrade.trim().equals(""))
            {
                tAgentGrade = this.mLATreeSchema.getAgentGrade().trim();
            }
            else
            {
                tAgentGrade = this.mAgentGrade.trim();
            }

            System.out.println("----------tAgentGrade-----" + tAgentGrade);
            if (tAgentGrade == null || tAgentGrade.equals(""))
            {
                return false;
            }
            tPayYear = tSch.getPayYear();
            tBranchType = tSch.getBranchType();

            if (tBranchType.trim().equals("1"))
            {
                if (tPayYear == 0)
                {
                    System.out.println("---CommisionSN:" + tSch.getPolNo());
                    System.out.println("---agentcode--:" + tSch.getAgentCode());

                    if (tAgentGrade.compareTo("A01") > 0 ||
                        tSch.getCommDire().equals("2"))
                    {
                        tSch.setFYC(tSch.getDirectWage());
                        tSch.setFYCRate(tSch.getStandFYCRate());
                    }
                    else
                    {
                        System.out.println("---tCurrAgentCode--" +
                                           tCurrAgentCode);
                        System.out.println("---tAgentCode--" + tAgentCode);
                        if (tCurrAgentCode.equals(tAgentCode))
                        {
                            System.out.println("--tLastMakeDate : " +
                                               tLastCalDate);
                            System.out.println("--tMakeDate : " + tCalDate);
                            System.out.println("--tMoney : " + tMoney);
                            if (!tLastCalDate.trim().equals(tCalDate))
                            {
                                tMoney = 0;
                            }

                            tSch = doCalA(tSch, tMoney);
                            if (tSch == null)
                            {
                                buildError("dealData", "打折处理时出错");
                                return false;
                            }
                            System.out.println("----before rebate----");
                            System.out.println(tSch.getAgentCode());
                            System.out.println(tSch.getStandFYCRate());
                            System.out.println(tSch.getDirectWage());
                            System.out.println("----after rebate----");
                            System.out.println(tSch.getAgentCode());
                            System.out.println(this.mCurrRebate);
                            System.out.println(tSch.getFYCRate());
                            System.out.println(tSch.getFYC());
                            tMoney = tMoney + tSch.getDirectWage();
                        }
                        else
                        {
                            tMoney = 0;
                            tSch = doCalA(tSch, tMoney);
                            if (tSch == null)
                            {
                                buildError("dealData", "打折处理时出错");
                                return false;
                            }
                            System.out.println("----before rebate----");
                            System.out.println(tSch.getAgentCode());
                            System.out.println(tSch.getStandFYCRate());
                            System.out.println(tSch.getDirectWage());
                            System.out.println("----after rebate----");
                            System.out.println(tSch.getAgentCode());
                            System.out.println(this.mCurrRebate);
                            System.out.println(tSch.getFYCRate());
                            System.out.println(tSch.getFYC());
                            tMoney = tSch.getDirectWage();
                        }
                    }
                }
                else
                {
                    tSch.setFYC(tSch.getDirectWage());
                    tSch.setFYCRate(tSch.getStandFYCRate());
                }
            }
            //团险
            if (tBranchType.trim().equals("2"))
            {
                if (mLAAgentSchema.getInsideFlag() != null &&
                    mLAAgentSchema.getInsideFlag().equals("0"))
                {
                    tSch.setFYC(0);
                    tSch.setFYCRate(0);
                    tSch.setGrpFYC(0);
                    tSch.setGrpFYCRate(0);
                    tSch.setDepFYC(0);
                    tSch.setDepFYCRate(0);
                }
                else
                {
                    tSch.setFYC(tSch.getDirectWage());
                    tSch.setFYCRate(tSch.getStandFYCRate());
                }

            }
            //银代
            if (tBranchType.trim().equals("3"))
            {
                if (mLAAgentSchema.getInsideFlag() != null &&
                    mLAAgentSchema.getInsideFlag().equals("0"))
                {
                    tSch.setFYC(0);
                    tSch.setFYCRate(0);
                    tSch.setGrpFYC(0);
                    tSch.setGrpFYCRate(0);
                    tSch.setDepFYC(0);
                    tSch.setDepFYCRate(0);
                }
                else
                {
                    if (tAgentGrade.trim().compareTo("B01") > 0 ||
                        tSch.getCommDire().equals("2"))
                    {
                        tSch.setFYC(tSch.getDirectWage());
                        tSch.setFYCRate(tSch.getStandFYCRate());
                    }
                    else
                    {
                        tSch = DoCalB(tSch);
                    }
                }

            }

            this.mNewSet.add(tSch);
            tAgentCode = tCurrAgentCode;
            tLastCalDate = tCalDate.trim();
        }
        return true;
    }

    /**
     * 提交给该类，进行打折处理
     * @param cInputData
     * @return
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        if (mOperate.equals(this.CALFLAG))
        {
            if (!dealData())
            {
                return false;
            }
        }
        else
        {
            if (!dealData1())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 根据地区类型，获取打折比例
     * @return
     */
    private float getRate()
    {
        return Float.parseFloat("0.8");
    }

    /**
     * 得到判断某个管理机构的考核标准以及相关信息
     * @param intv
     * @param tManageCom
     * @return
     */
    private String[] getStandardedMoney(String tManageCom, int intv)
    {
        String tMgC = tManageCom.substring(0, 4);
        String areaType = "";
        String tEDay = "";
        float tStandard = 0;
        LDCodeRelaSet tLDCodeRelaSet = new LDCodeRelaSet();
        LDCodeRelaDB tLDCodeRelaDB = new LDCodeRelaDB();
        LDCodeRelaSchema tLDCodeRelaSchema = new LDCodeRelaSchema();
        String tSQL =
                "select * from LDCodeRela where relatype='comtoareatype' and code1='" +
                tMgC + "'";

        System.out.println("---------getStandardedMoney---" + tSQL);
        tLDCodeRelaSet = tLDCodeRelaDB.executeQuery(tSQL);
        System.out.println("tLDCodeRelaSet---" + tLDCodeRelaSet.size());
        if (tLDCodeRelaSet == null || tLDCodeRelaSet.size() == 0)
        {
            this.mErrors.copyAllErrors(tLDCodeRelaDB.mErrors);
            buildError("getStandardedMoney",
                       "查找地区类型出错，没找到" + tManageCom + "地区类型信息");
            return null;
        }
        System.out.println("not null");
        for (int i = 1; i <= tLDCodeRelaSet.size(); i++)
        {
            tLDCodeRelaSchema = tLDCodeRelaSet.get(i);
            System.out.println(tLDCodeRelaSchema.getCode1().substring(0, 4));
            System.out.println(tMgC);
            if (tLDCodeRelaSchema.getCode1().substring(0, 4).equals(tMgC))
            {
                areaType = tLDCodeRelaSchema.getCode2();
                System.out.println("----areaType---" + areaType);
                break;
            }
        }
        if (areaType == null || areaType.equals(""))
        {
            buildError("getStandardedMoney",
                       "查找地区类型出错，没找到" + tManageCom + "地区类型信息");
            return null;
        }

        rate = getRate();
        System.out.println("---------" + rate + "-------");
        // 取打折部分的标准保费
        SSRS tSSRS = new SSRS();
        tSQL =
                "select indfycsum,limitperiod from LAAgentPromRadix where areatype='"
                + areaType.trim() +
                "' and agentgrade='A01' and AssessCode='02' order by limitperiod";
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() != 3)
        {
            buildError("getStandardedMoney", "保费打折标准不是三条!");
            return null;
        }

        /**
         * result数组中存有13个数据值。result[0],result[3],result[6],result[9]中存着考核标准
         * result[1],result[2]分别保存考察3个月业绩时的起始时间和截止时间
         * result[4],result[5]分别保存考察6个月业绩时的起始时间和截止时间
         * result[7],result[8]分别保存考察9个月业绩时的起始时间和截止时间
         * result[10],result[11]分别保存考察从mYesterday前推9个月业绩时的起始时间和截止时间
         * result[12]中保存该考核标准对应的是三个月、六个月、九个月还是九个月以上的成绩
         * 分别用"0","1","2"，"3"表示之
         */
        String[] result = new String[13];
        result[0] = String.valueOf(Float.parseFloat(tSSRS.GetText(1, 1)) / rate);
        result[3] = String.valueOf(Float.parseFloat(tSSRS.GetText(2, 1)) / rate);
        result[6] = String.valueOf(Float.parseFloat(tSSRS.GetText(3, 1)) / rate);
        result[9] = String.valueOf(Float.parseFloat(tSSRS.GetText(3, 1)) / rate);
        System.out.println(mIncludeFlag);
        if (mIncludeFlag.equals("Y"))
        {
            String[] buf = this.getSEDate(this.mAddStartDate, 2);
            result[1] = buf[0];
            result[2] = buf[1];
            System.out.println(buf[0]);
            System.out.println(buf[1]);
            buf = this.getSEDate(this.mAddStartDate, 5);
            result[4] = buf[0];
            result[5] = buf[1];
            System.out.println(buf[0]);
            System.out.println(buf[1]);
            buf = this.getSEDate(this.mAddStartDate, 8);
            result[7] = buf[0];
            result[8] = buf[1];
            System.out.println(buf[0]);
            System.out.println(buf[1]);
            buf = this.getSEDate(this.mYesterday, -8);
            result[10] = buf[0];
            result[11] = buf[1];
            System.out.println(buf[0]);
            System.out.println(buf[1]);
        }
        else
        {
            String[] buf = this.getSEDate(this.mAddStartDate, 3);
            result[1] = buf[0];
            result[2] = buf[1];
            System.out.println(buf[0]);
            System.out.println(buf[1]);
            buf = this.getSEDate(this.mAddStartDate, 6);
            result[4] = buf[0];
            result[5] = buf[1];
            System.out.println(buf[0]);
            System.out.println(buf[1]);
            buf = this.getSEDate(this.mAddStartDate, 9);
            result[7] = buf[0];
            result[8] = buf[1];
            System.out.println(buf[0]);
            System.out.println(buf[1]);
            buf = this.getSEDate(this.mYesterday, -8);
            result[10] = buf[0];
            result[11] = buf[1];
            System.out.println(buf[0]);
            System.out.println(buf[1]);
        }
        if (intv > 9)
        {
            intv = 3;
        }
        else
        {
            intv = intv / 3;
        }
        this.mCurrentStandard = Double.parseDouble(result[intv * 3]);
        System.out.println("mCurrentStandard:" + mCurrentStandard);
        result[12] = String.valueOf(intv);
        return result;
    }

    /**
     *
     * @param tSch
     * @param temp
     * @return 如果需要打折，return false，如果不需要打折，return true
     */
    private boolean judgeByCalDate(String tAgentCode, String[] temp)
    {
        String timeLimit = "";
        if (temp[2].trim().compareTo(this.mYesterday) > 0)
        {
            timeLimit = this.mYesterday;
        }
        else
        {
            timeLimit = temp[2];
        }
        String timeStart = "";
        if (temp[1].trim().compareTo(this.mAddStartDate) > 0)
        {
            timeStart = temp[1];
        }
        else
        {
            timeStart = this.mAddStartDate;
        }
        String tSQL =
                "select nvl(sum(directwage),0) from lacommision where agentcode = '"
                + tAgentCode.trim()
                + "' and commdire='1' and payyear <1 and caldate >= '"
                + timeStart
                + "' and caldate <= '"
                + timeLimit + "'";
        System.out.println("---tSQL = " + tSQL);
        ExeSQL tExeSql = new ExeSQL();
        String directWageSum = tExeSql.getOneValue(tSQL);
        System.out.println("---directWageSum---" + directWageSum);
        System.out.println("---stand-----------" + temp[0]);
        if (Double.parseDouble(temp[0]) > Double.parseDouble(directWageSum))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private boolean judgeByTMakeDate(String tAgentCode, String[] temp)
    {
        String timeLimit = "";
        if (temp[2].trim().compareTo(this.mYesterday) > 0)
        {
            timeLimit = this.mYesterday;
        }
        else
        {
            timeLimit = temp[2];
        }
        String timeStart = "";
        if (temp[1].trim().compareTo(this.mAddStartDate) > 0)
        {
            timeStart = temp[1];
        }
        else
        {
            timeStart = this.mAddStartDate;
        }
        String tSQL =
                "select nvl(sum(directwage),0) from lacommision where agentcode = '"
                + tAgentCode.trim()
                + "' and commdire='1' and payyear <1 and TMakeDate >= '"
                + timeStart
                + "' and TMakeDate <= '"
                + timeLimit + "'";
        System.out.println("---tSQL = " + tSQL);
        ExeSQL tExeSql = new ExeSQL();
        String directWageSum = tExeSql.getOneValue(tSQL);
        System.out.println("---directWageSum---" + directWageSum);
        System.out.println("---stand-----------" + temp[0]);
        if (Double.parseDouble(temp[0]) > Double.parseDouble(directWageSum))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     *对扎账表中的一条信息进行打折处理
     * @param tSch
     * @param tMoney
     * @return 如果的返回null，表示出错，否则，返回打折好的扎账信息
     */

    private LACommisionSchema doCalA(LACommisionSchema tSch, double tMoney)
    {
        int tIntvl = 0;
        String tEDay = "";
        String IncludeFlag = "";
        String tSQL = "";
        String manageCom = "";
        int limitPeriod = 0;
        String tBeforeMoney = "";
        double tCurrMoney = 0;
        double tFYCRate = 0;
        double tStandFYCRate = 0;
        double tSum = 0;
        double tFYC = 0;
        double tBeforeSumMoney = 0;
        double tTransMoney = tSch.getTransMoney();

        if (mCurrRebate)
        {
            tSch.setFYCRate(tSch.getStandFYCRate());
            tSch.setFYC(tSch.getDirectWage());
            return tSch;
        }
        manageCom = tSch.getManageCom();
        tIntvl = getIntvl(tSch.getAgentCode());

        System.out.println("-----打折标志：tIntvl = " + tIntvl);

//      if (!this.mSamePerson )
        mStandard = getStandardedMoney(manageCom, tIntvl);

        if (mStandard == null)
        {
            return null;
        }

        String tOffFlag = "";
        tOffFlag = doJudge(tSch.getAgentCode(), mStandard);
        System.out.println("-----打折标志：tOffFlag = " + tOffFlag);

        if (tOffFlag.trim().equals("E"))
        {
            return null;
        }
        if (tOffFlag.trim().equals("Y"))
        {
            System.out.println("-----tStandard = " + this.mCurrentStandard);
            String[] BEDate = new String[2];
            if (Integer.parseInt(mStandard[12]) < 3)
            {
                BEDate[0] = this.mAddStartDate.trim();
                BEDate[1] = mYesterday.trim();
            }
            else
            {
                BEDate[0] = mStandard[10];
                BEDate[1] = mStandard[11];
            }
            if (!this.mOperate.equals(this.RECALFYCFLAG))
            // 计算当前日期之前的累计直接佣金
            {
                tSQL =
                        "select nvl(sum(directwage),0) from lacommision where  commdire='1' and payyear <1 and tmakedate >= '"
                        + BEDate[0]
                        + "' and tMakeDate<='" + tSch.getTMakeDate() + "'"
                        + " and ( (polno<'" + tSch.getPolNo() + "')"
                        + " or (polno='" + tSch.getPolNo() +
                        "' and receiptno<'" + tSch.getReceiptNo() + "')"
                        + " or (polno='" + tSch.getPolNo() +
                        "' and receiptno='" + tSch.getReceiptNo() +
                        "' and riskcode<'" + tSch.getRiskCode() + "'))"
                        + "  and agentcode = '" + tSch.getAgentCode() + "' "
//          + "' and tmakedate <= '"
//          + BEDate[1] + "'"
                        ;
            }
            else
            {
                tSQL =
                        "select nvl(sum(directwage),0) from lacommision where  commdire='1' and payyear <1 and caldate >= '"
                        + BEDate[0]
                        + "' and caldate<='" + tSch.getCalDate() + "'"
                        + " and ( (polno<'" + tSch.getPolNo() + "')"
                        + " or (polno='" + tSch.getPolNo() +
                        "' and receiptno<'" + tSch.getReceiptNo() + "')"
                        + " or (polno='" + tSch.getPolNo() +
                        "' and receiptno='" + tSch.getReceiptNo() +
                        "' and riskcode<'" + tSch.getRiskCode() + "'))"
                        + "  and agentcode = '" + tSch.getAgentCode() + "' "
//          + "' and caldate <= '"
//          + BEDate[1] + "'"
                        ;
            }

            System.out.println("---tSQL = " + tSQL);
            ExeSQL tExeSql = new ExeSQL();
            System.out.println("----mCurrDirectWage =before:" + mCurrDirectWage);
            String result = tExeSql.getOneValue(tSQL);
            System.out.println(result);
//      if (this.mCurrDirectWage ==-1)
            {
                System.out.println("parseDouble");
                mCurrDirectWage = Double.parseDouble(result);
            }
//      tBeforeSumMoney = mCurrDirectWage + tMoney;
            tBeforeSumMoney = mCurrDirectWage;
            System.out.println("----mCurrDirectWage =after:" + mCurrDirectWage);
            System.out.println("----" + tSch.getAgentCode() + "----tMoney = " +
                               tMoney);

            // 当前保单的直接佣金
            tCurrMoney = tSch.getDirectWage();
//      tCurrMoney=0;
            System.out.println("----tCurrMoney = " + tCurrMoney);

            tSum = tBeforeSumMoney + tCurrMoney;
            System.out.println("---tBeforeSumMoney---" + tBeforeSumMoney);
            System.out.println("----tSum = " + tSum);
            tStandFYCRate = tSch.getStandFYCRate();

            if (mCurrentStandard < tBeforeSumMoney)
            {
                tFYCRate = tStandFYCRate;
                tFYC = tSch.getDirectWage();
            }
            else
            {
                if (mCurrentStandard > tSum)
                {
                    tFYCRate = (double) (tStandFYCRate * rate);
                    tFYC = (double) (tSch.getDirectWage() * rate);
                }
                else
                {
                    if (mCurrentStandard <= tSum)
                    {
                        tFYC = (double) ((tSum - mCurrentStandard) +
                                         (tCurrMoney - (tSum - mCurrentStandard)) *
                                         rate);
                        tFYCRate = (double) (tFYC / tTransMoney);
                        this.mCurrRebate = true; //true表示该代理人不用打折了
                    }
                }
            }
        }
        else
        {
            tFYCRate = tSch.getStandFYCRate();
            tFYC = tSch.getDirectWage();
            this.mCurrRebate = true; //true表示该代理人不用打折了
        }

        //赋新值
        tSch.setFYCRate(tFYCRate);
        System.out.println("---standFYCRate = " + tSch.getStandFYCRate());
        System.out.println("---FYCRate = " + tSch.getFYCRate());
        tSch.setFYC(tFYC);
        System.out.println("----tFYC = " + tSch.getFYC());
        return tSch;
    }

    private LACommisionSchema DoCalB(LACommisionSchema tSch)
    {
        double tStandFycRate = tSch.getStandFYCRate();
        double tDirectWage = tSch.getDirectWage();
        double tFycRate = tStandFycRate * 0.5;
        double tFyc = tDirectWage * 0.5;

        tSch.setFYCRate(tFycRate);
        tSch.setFYC(tFyc);
        return tSch;
    }

    /**
     *
     * @param tAgentCode
     * @param temp
     * @param tMoney
     * @return
     */
    private String doJudge(String tAgentCode, String[] temp)
    {

        String tEmployDate = this.mAddStartDate.trim();
        String inFlag;
        int limitPeriod = Integer.parseInt(temp[12]);
        String[] buf = new String[3];
        for (int i = 0; i <= limitPeriod; i++)
        {
            buf[0] = temp[i * 3 + 0];
            buf[1] = temp[i * 3 + 1];
            buf[2] = temp[i * 3 + 2];
            if (this.mOperate.equals(this.RECALFYCFLAG))
            {
                if (this.judgeByCalDate(tAgentCode, buf))
                {
                    System.out.println("judgeByCalDate:" + true);
                    return "N";
                }
            }
            else
            {
                if (this.judgeByTMakeDate(tAgentCode, buf))
                {
                    System.out.println("judgeByTMakeDate:" + true);
                    return "N";
                }
            }

        }
        //"Y"表示需要打折，"N"表示不需要打折,"E"表示出错
        return "Y";
    }

    /**
     * 函数功能是根据基础日期BaseDate和间隔interval。
     * 求出和基础日期月份数间隔interval的目标日期
     * 如果interval是正数，是向后推，如果是负数，则是向前推
     * 如果是向后推，得到的是目标月份的自然月的最后一天，如果是向前推，得到的是目标月份的自然月
     * 的第一天。
     * 例如：基础日期是2004-02-24，间隔是4，目标日期是2004-06-30。
     * 返回的是一个二维数组，数组的第一个单元String[0]存的是开始日期，数组的第二个单元String[1]
     * 存的是截至日期，开始日期比截至日期早
     * 在本例中，String[0]="2004-02-24",String[1]="2004-06-30"
     * @param BaseDate 原始日期
     * @param interval 间隔
     * @return
     */
    private String[] getSEDate(String BaseDate, int interval)
    {
        String targetMonth = PubFun.calDate(BaseDate, interval, "M", null);
        System.out.println("targetMonth---" + targetMonth);
        targetMonth = AgentPubFun.formatDate(targetMonth, "yyyyMM");
        String tsql = "";
        String targetDate = "";
        String[] tResult = new String[2];
        ExeSQL tExeSQL = new ExeSQL();
        if (interval >= 0)
        {
            tsql = "select to_char(endDate,'yyyy-mm-dd') from lastatsegment where stattype=1 and yearmonth='" +
                   targetMonth + "'";
            targetDate = tExeSQL.getOneValue(tsql);
            System.out.println(targetDate);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                buildError("getSEDate", "查询" + targetMonth + "的自然月截止日期出错");
                return null;
            }
            System.out.println(BaseDate);
            System.out.println(targetDate);
            tResult[0] = BaseDate;
            tResult[1] = targetDate;
        }
        else
        {
            tsql = "select to_char(startdate,'yyyy-mm-dd') from lastatsegment where stattype=1 and yearmonth='" +
                   targetMonth + "'";
            targetDate = tExeSQL.getOneValue(tsql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                buildError("getSEDate", "查询" + targetMonth + "的自然月起始日期出错");
                return null;
            }
            System.out.println(targetDate);
            System.out.println(BaseDate);
            tResult[0] = targetDate;
            tResult[1] = BaseDate;
        }
        return tResult;
    }


    /**
     *
     * @param tDate
     */
    private void getCalDate(String tDate)
    {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        PubFun tPF = new PubFun();

        // 取当天日期
        java.util.Date today = new java.util.Date();
        FDate tFDate = new FDate();
        today = tFDate.getDate(tDate);
        this.mToday = tDate;
        System.out.println("----this.mToday = " + this.mToday);

        // 取前一天日期
        System.out.println(tDate);
        java.util.Date tCalDate = new java.util.Date();
        mYesterday = PubFun.calDate(tDate, -1, "D", null);
//    this.mYesterday = df.format(tCalDate);
        System.out.println("----this.mYesterday = " + this.mYesterday);
    }


    /**
     * 根据入司时间判断代理人已经入司了的月份数
     * @param tAgentCode
     * @return
     */
    private int getIntvl(String tAgentCode)
    {
        int tIntvl = 0;
        String tBDay = "";
        System.out.println("mAddStartDate" + mAddStartDate);
        String tEDay = this.mAddStartDate.substring(8);
        /**
         * 判断入司时间是不是当月的下半月，如果是，则考核从入司当月的下一个月开始
         * 考核，入司当月的绩效记入下三个月的绩效内一并进行考核
         * 否则，从入司当月开始进行考核
         */

        ExeSQL tExe = new ExeSQL();
        String tSql =
                "select varvalue from lasysvar where vartype = 'EmployLimit'";
        String tEmployLimit = tExe.getOneValue(tSql).trim();
        if (tEDay.compareTo(tEmployLimit) > 0)
        {
            mIncludeFlag = "N";
            tBDay = PubFun.calDate(mAddStartDate, 1, "M", null);
            tBDay = tBDay.substring(0, 8);
            tBDay = tBDay + "01";
        }
        else
        {
            mIncludeFlag = "Y";
            tBDay = mAddStartDate.substring(0, 8) + "01";
        }
        System.out.println("tBDay" + tBDay);
//    String sql="select to_char(FIRST_DAY('"+mToday+"'),'YYYY-MM-DD') from dual ";
//    ExeSQL tExeSQL=new ExeSQL();
//
//    System.out.println(sql) ;
//    String tToday=tExeSQL.getOneValue(sql) ;
        String tToday = mToday.substring(0, 8) + "01";
        tIntvl = PubFun.calInterval(tBDay, tToday, "M");
        System.out.println("tIntvltIntvl:" + tIntvl);
        return tIntvl;
    }

    public VData getResult()
    {
        tResult.add(mNewSet);
        return tResult;
    }

    private void buildError(String FuncName, String ErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CalFOrmDrawRate";
        cError.functionName = FuncName;
        cError.errorMessage = ErrMsg;
        this.mErrors.addOneError(cError);
    }

}
