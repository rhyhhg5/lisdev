package com.sinosoft.lis.agentcalculate;

import java.math.BigDecimal;
import java.util.HashMap;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;

/**
 * 扎账提数提奖比例计算
 * @author yangyang 2017-8-31
 *
 */
public class AgentWageOfCalAllFyc extends AgentWageOfAbstractClass{

	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfCalAllFyc-->dealData:开始执行");
		System.out.println("AgentWageOfCalAllFyc-->dealData:cSQL"+cSQL);
		LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLACommisionNewSet, cSQL);
		do
		{
			tRSWrapper.getData();
			if(null==tLACommisionNewSet||0==tLACommisionNewSet.size())
			{
				System.out.println("AgentWageOfCalFyc-->本次循环没有查询到数据，请核对提数是否有问题");
				return true;
			}
			MMap tMMap = new MMap();
			LACommisionNewSet tUpdateLACommisionNewSet = new LACommisionNewSet();
			LAChargeSet tLAChargeSet = new LAChargeSet();
			for(int i =1 ;i<=tLACommisionNewSet.size();i++)
			{
				LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
				tLACommisionNewSchema = tLACommisionNewSet.get(i);
				//不计算fyc的数据
				if(noCalFycRateData(tLACommisionNewSchema))
				{
					continue;
				}
				//后面需要修改，看是不是所有的渠道都满足，如果不为ZC，就是退保的，和ZC时的提奖比例一样
				boolean tNoZCFlag = false;
				double tNOZCFycRate = 0;
				if(!"ZC".equals(tLACommisionNewSchema.getTransType()))
				{
					tNoZCFlag = true;
					tNOZCFycRate = getNoZCFycRate(tLACommisionNewSchema);
					if(-1==tNOZCFycRate)
					{
						tNoZCFlag = false;
					}
				}
				String tBranchType = tLACommisionNewSchema.getBranchType();
				String tBranchType2 = tLACommisionNewSchema.getBranchType2();
				String tRiskCode = tLACommisionNewSchema.getRiskCode();
				String tKey = tBranchType+tBranchType2+tRiskCode;
				HashMap<String,String> tCalCodeHashMap = new HashMap<String,String>();
				tCalCodeHashMap = mAgentWageCommonFunction.getCalCode(tKey);
				String tCalCode = "";
				double tRate = 0.0;
				double tFyc = 0.0;
				if(tCalCodeHashMap.containsKey("00"))
				{
					//如果F2为01，代表是此条记录不计入薪资计算中，所以提奖比例设置为0
					if("01".equals(tLACommisionNewSchema.getF2()))
					{
						tRate = 0;
					}
					else
					{
						if(tNoZCFlag)
						{
							tRate = tNOZCFycRate;
						}
						else
						{
							tCalCode = tCalCodeHashMap.get("00");
							tRate = this.getRate(tCalCode, "00", tLACommisionNewSchema);
							if(-1==tRate)
							{
								continue;
							}
							//个险直销 有孤儿单的判断，对于孤儿单，提奖比例会进行打折
							if("1".equals(tLACommisionNewSchema.getBranchType())&&"01".equals(tLACommisionNewSchema.getBranchType2()))
							{
								String tFlag = checkLaorphanPolicy(tLACommisionNewSchema);
								tLACommisionNewSchema.setP6(tFlag);
								//孤儿单需要取折算后的提奖比例
								if("1".equals(tFlag))
								{
									tRate = getFYCRate(tLACommisionNewSchema, tRate);
								}
							}
						}
					}
					tLACommisionNewSchema.setStandFYCRate(tRate);
					tFyc = mulrate(tRate,tLACommisionNewSchema.getTransMoney());
					tFyc = Arith.round(tFyc, 2);
					tLACommisionNewSchema.setDirectWage(tFyc); //直接佣金
				}
				if(tCalCodeHashMap.containsKey("03"))
				{
					tCalCode = tCalCodeHashMap.get("03");
					//直接计算出来的是折标保费
					tRate = this.getRate(tCalCode, "03", tLACommisionNewSchema);
					if(-1==tRate)
					{
						continue;
					}
					double tStandFyc = getStandFyc(tLACommisionNewSchema);
					tLACommisionNewSchema.setStandPremRate(tStandFyc);
					tLACommisionNewSchema.setStandPrem(tRate);
				}
				if(tCalCodeHashMap.containsKey("06"))
				{
					tCalCode = tCalCodeHashMap.get("06");
					tRate = this.getRate(tCalCode, "06", tLACommisionNewSchema);
					if(-1==tRate)
					{
						continue;
					}
					tLACommisionNewSchema.setStandPremRate(tRate); //通过计算得出的比例
					tFyc = mulrate(tRate,tLACommisionNewSchema.getTransMoney()); //职团折标  transmoney * wageRateValue
					tFyc = Arith.round(tFyc, 2);
	                tLACommisionNewSchema.setStandPrem(tFyc);
				}
				if(tCalCodeHashMap.containsKey("10"))
				{
					//如果F2为01，代表是此条记录不计入薪资计算中
					if("01".equals(tLACommisionNewSchema.getF2()))
					{
						tRate = 0;
					}
					else
					{
						if(tNoZCFlag)
						{
							tRate = tNOZCFycRate;
						}
						else
						{
							tCalCode = tCalCodeHashMap.get("10");
							tRate = this.getRate(tCalCode, "10", tLACommisionNewSchema);
							if(-1==tRate)
							{
								continue;
							}
						}
					}
					tLACommisionNewSchema.setStandFYCRate(tRate);
					tFyc = mulrate(tRate,tLACommisionNewSchema.getP7()); //直接佣金,按帐户管理费计算   P7 * wageRateValue
					tFyc = Arith.round(tFyc, 2);
					tLACommisionNewSchema.setDirectWage(tFyc);
				}
				if(tCalCodeHashMap.containsKey("51"))
				{
					tCalCode = tCalCodeHashMap.get("51");
					tRate = this.getRate(tCalCode, "51", tLACommisionNewSchema);
					if(-1==tRate)
					{
						continue;
					}
					double wageValue = mulrate(tLACommisionNewSchema.getTransMoney(),tRate);
					wageValue = Arith.round(wageValue, 2);
					LAChargeSchema tLAChargeSchema = new LAChargeSchema();
					setChargeSchemaValue(tLAChargeSchema, tLACommisionNewSchema);
					tLAChargeSchema.setAgentCom(tLACommisionNewSchema.getAgentCom());
					tLAChargeSchema.setChargeRate(tRate);
					tLAChargeSchema.setCharge(wageValue);
					tLAChargeSchema.setChargeType("51");
					tLAChargeSchema.setBranchType(tLACommisionNewSchema.getBranchType());
					tLAChargeSchema.setBranchType2(tLACommisionNewSchema.getBranchType2());
					tLAChargeSchema.setTransType(tLACommisionNewSchema.getTransType());
					tLAChargeSet.add(tLAChargeSchema);
				}
				tLACommisionNewSchema.setFYCRate(tLACommisionNewSchema.getStandFYCRate());
				tLACommisionNewSchema.setFYC(tLACommisionNewSchema.getDirectWage());
				tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
				tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
				tUpdateLACommisionNewSet.add(tLACommisionNewSchema);
			}
			if(0<tLAChargeSet.size())
			{
				tMMap.put(tLAChargeSet, "INSERT");
			}
			if(0<tLACommisionNewSet.size())
			{
				tMMap.put(tLACommisionNewSet, "UPDATE");
			}
			if(0==tMMap.size())
			{
				continue;
			}
			if (!mAgentWageCommonFunction.dealDataToTable(tMMap)) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "AgentWageOfCalAllFyc";
				tError.functionName = "PubSubmit";
				tError.errorMessage = "提奖比例计算数据更新失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}while(tLACommisionNewSet.size()>0);
		System.out.println("AgentWageOfCalAllFyc-->dealData:执行结束 ");
		return true;
	}
	
	/**
	 * 根据传入的类型，计算得到的比例
	 * @param cCalCode
	 * @param cCalType
	 * @param cLACommisionNewSchema
	 * @return
	 */
	private double getRate(String cCalCode,String cCalType,LACommisionNewSchema cLACommisionNewSchema)
	{
		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode(cCalCode);
		tCalculator.addBasicFactor("AGENTCODE",cLACommisionNewSchema.getAgentCode());
		tCalculator.addBasicFactor("AGENTCOM",String.valueOf(cLACommisionNewSchema.getAgentCom()));
		tCalculator.addBasicFactor("BRANCHTYPE",cLACommisionNewSchema.getBranchType());
		tCalculator.addBasicFactor("BRANCHTYPE2",cLACommisionNewSchema.getBranchType2());
		tCalculator.addBasicFactor("BRANCHTYPE3",cLACommisionNewSchema.getBranchType3());
		tCalculator.addBasicFactor("CALTYPE",cCalType);
		tCalculator.addBasicFactor("COMMISIONSN",cLACommisionNewSchema.getCommisionSN());
		tCalculator.addBasicFactor("CONTNO",cLACommisionNewSchema.getContNo());
		tCalculator.addBasicFactor("FLAG",cLACommisionNewSchema.getFlag());
		tCalculator.addBasicFactor("GRPCONTNO",cLACommisionNewSchema.getGrpContNo());
		tCalculator.addBasicFactor("MANAGECOM",cLACommisionNewSchema.getManageCom());
		tCalculator.addBasicFactor("P7",String.valueOf(cLACommisionNewSchema.getP7()));
		tCalculator.addBasicFactor("PAYCOUNT",String.valueOf(cLACommisionNewSchema.getPayCount()));
		tCalculator.addBasicFactor("PAYINTV",String.valueOf(cLACommisionNewSchema.getPayIntv()));
		tCalculator.addBasicFactor("PAYYEAR",String.valueOf(cLACommisionNewSchema.getPayYear()));
		tCalculator.addBasicFactor("PAYYEARS",String.valueOf(cLACommisionNewSchema.getPayYears()));
		tCalculator.addBasicFactor("POLTYPE",cLACommisionNewSchema.getPolType());
		tCalculator.addBasicFactor("PRTNO",String.valueOf(cLACommisionNewSchema.getP14()));
		tCalculator.addBasicFactor("Prem",String.valueOf(cLACommisionNewSchema.getTransMoney()));
		tCalculator.addBasicFactor("RENEWCOUNT",String.valueOf(cLACommisionNewSchema.getReNewCount()));
		tCalculator.addBasicFactor("RISKCODE",cLACommisionNewSchema.getRiskCode());
		tCalculator.addBasicFactor("SCANDATE",cLACommisionNewSchema.getScanDate());
		tCalculator.addBasicFactor("SIGNDATE",String.valueOf(cLACommisionNewSchema.getSignDate()));
		tCalculator.addBasicFactor("TMAKEDATE",cLACommisionNewSchema.getTMakeDate());
		tCalculator.addBasicFactor("TRANSSTATE",String.valueOf(cLACommisionNewSchema.getTransState()));
		tCalculator.addBasicFactor("WAGENO",cLACommisionNewSchema.getWageNo());
		tCalculator.addBasicFactor("WRAPCODE",cLACommisionNewSchema.getF3());
		tCalculator.addBasicFactor("YEARS",String.valueOf(cLACommisionNewSchema.getYears()));
		String tRate = tCalculator.calculate();
		if (tCalculator.mErrors.needDealError()) 
		{
			tRate = "-1";
		}
		return Double.parseDouble(tRate);
	}
	
	//double类型乘法计算
    public static double mulrate(double v1, double v2) {

        BigDecimal b1 = new BigDecimal(Double.toString(v1));

        BigDecimal b2 = new BigDecimal(Double.toString(v2));

        return b1.multiply(b2).doubleValue();

    }
    
    private double getStandFyc(LACommisionNewSchema cLACommisionNewSchema)
    {
    	String tBranchType = cLACommisionNewSchema.getBranchType();
        String tBranchType2 = cLACommisionNewSchema.getBranchType2();
        String tRiskCode = cLACommisionNewSchema.getRiskCode();
        String tManageCom = cLACommisionNewSchema.getManageCom();
        int tPayIntv = cLACommisionNewSchema.getPayIntv();
        int tPayYear = cLACommisionNewSchema.getPayYear();
        int tPayYears = cLACommisionNewSchema.getPayYears();
        ExeSQL tExeSQL = new ExeSQL();
        double tRate =0;
    	if("1".equals(mAgentWageCommonFunction.getSpecialSign(tRiskCode)))
		{
    		String tSQL="select rate from laratestandprem where managecom=substr('"+tManageCom+"',1,4) "
    				+ " and riskcode='"+tRiskCode+"' "
					+ " and branchtype='"+tBranchType+"' "
					+ " and branchtype2='"+tBranchType2+"' "
					+ " and payintv='"+tPayIntv+"' "
					+ " and curyear="+(tPayYear+1) 
					+ " and "+tPayYears+">=int(F01) "
					+ " and "+tPayYears+"<int(F02)  fetch first 1 rows only";
        	String tResult=tExeSQL.getOneValue(tSQL);
    		if(tResult!=null&&!tResult.equals(""))
    		{
    			tRate=Double.parseDouble(tResult);
    		}
    		else
    		{
    			tSQL="select rate from laratestandprem where managecom='86' "
        				+ " and riskcode='"+tRiskCode+"' "
    					+ " and branchtype='"+tBranchType+"' "
    					+ " and branchtype2='"+tBranchType2+"' "
    					+ " and payintv='"+tPayIntv+"' "
    					+ " and curyear="+(tPayYear+1) 
    					+ " and "+tPayYears+">=int(F01) "
    					+ " and "+tPayYears+"<int(F02)  fetch first 1 rows only";
    			tResult=tExeSQL.getOneValue(tSQL);
        		if(tResult!=null&&!tResult.equals(""))
        		{
        			tRate=Double.parseDouble(tResult);
        		}
    		}
		}
    	else
    	{
    		String tSQL="select rate from laratestandprem where managecom=substr('"+tManageCom+"',1,4) "
    				+ " and riskcode='"+tRiskCode+"' "
					+ " and branchtype='"+tBranchType+"' "
					+ " and branchtype2='"+tBranchType2+"'  fetch first 1 rows only";
        	String tResult=tExeSQL.getOneValue(tSQL);
    		if(tResult!=null&&!tResult.equals(""))
    		{
    			tRate=Double.parseDouble(tResult);
    		}
    		else
    		{
    			tSQL="select rate from laratestandprem where managecom='86' "
        				+ " and riskcode='"+tRiskCode+"' "
    					+ " and branchtype='"+tBranchType+"' "
    					+ " and branchtype2='"+tBranchType2+"'  fetch first 1 rows only";
    			tResult=tExeSQL.getOneValue(tSQL);
        		if(tResult!=null&&!tResult.equals(""))
        		{
        			tRate=Double.parseDouble(tResult);
        		}
    		}
    	}
    	return tRate;
    }
    private void setChargeSchemaValue(LAChargeSchema cLAChargeSchema,LACommisionNewSchema cLACommisionNewSchema) 
    {
		cLAChargeSchema.setCommisionSN(cLACommisionNewSchema.getCommisionSN());
		cLAChargeSchema.setCalDate(cLACommisionNewSchema.getTMakeDate());
		cLAChargeSchema.setWageNo(AgentPubFun.formatDate(cLACommisionNewSchema.getSignDate(), "yyyyMM"));
		cLAChargeSchema.setContNo(cLACommisionNewSchema.getContNo());
		cLAChargeSchema.setGrpPolNo(cLACommisionNewSchema.getGrpPolNo());
		cLAChargeSchema.setGrpContNo(cLACommisionNewSchema.getGrpContNo());
		cLAChargeSchema.setMainPolNo(cLACommisionNewSchema.getMainPolNo());
		cLAChargeSchema.setPolNo(cLACommisionNewSchema.getPolNo());
		cLAChargeSchema.setManageCom(cLACommisionNewSchema.getManageCom());
		cLAChargeSchema.setRiskCode(cLACommisionNewSchema.getRiskCode());
		cLAChargeSchema.setReceiptNo(cLACommisionNewSchema.getReceiptNo());
		cLAChargeSchema.setTransMoney(cLACommisionNewSchema.getTransMoney());
		cLAChargeSchema.setTMakeDate(cLACommisionNewSchema.getTMakeDate());
		cLAChargeSchema.setOperator(cLACommisionNewSchema.getOperator());
		cLAChargeSchema.setChargeState("0");
		cLAChargeSchema.setMakeDate(PubFun.getCurrentDate());
		cLAChargeSchema.setMakeTime(PubFun.getCurrentTime());
		cLAChargeSchema.setModifyDate(PubFun.getCurrentDate());
		cLAChargeSchema.setModifyTime(PubFun.getCurrentTime());
    }
    
    /**
     * 不计算fycrate的数据判断
     * @param cLACommisionNewSchema
     * @return
     */
    public boolean noCalFycRateData(LACommisionNewSchema cLACommisionNewSchema)
    {
    	//新契约前六位是0为加费，不算佣金
        String tPayPlanCode = cLACommisionNewSchema.getPayPlanCode() == null ?
                              "" : cLACommisionNewSchema.getPayPlanCode();
        if (6<=tPayPlanCode.length()&&"ZC".equals(cLACommisionNewSchema.getTransType())) 
        {
            if ("000000".equals(tPayPlanCode.substring(0, 6))) {
                return true;
            }
        }
        //保全退保的加费部分,不算佣金
        String PAYPLANCODE_ADDFEE = com.sinosoft.lis.bq.BQ.PAYPLANCODE_ADDFEE;
        if (PAYPLANCODE_ADDFEE.equals(tPayPlanCode)) {
        	return true;
        }
        return false;
    }
    /**
     * 个险直销孤儿单判断
     * @param cLACommisionNewSchema
     * @param cRate
     * @return 直接返回折算后的提奖比例
     */
    private String checkLaorphanPolicy(LACommisionNewSchema cLACommisionNewSchema)
    {
    	ExeSQL tExeSQL = new ExeSQL();
    	String tPolicyNo = cLACommisionNewSchema.getGrpContNo();
    	//设置默认为孤儿单
    	String tJudgeResult = "1";
  	   	if("00000000000000000000".equals(tPolicyNo))
  	   	{
  	   		tPolicyNo = cLACommisionNewSchema.getContNo();
  	   	}
  	   	String tJudgeSQL = "select 1 from laorphanpolicy where branchtype='1' and branchtype2='01' and contno ='"+tPolicyNo+"' "
  	   			+ " union "
  	   			+ " select 1 from laorphanpolicyb where branchtype='1' and branchtype2='01' and contno ='"+tPolicyNo+"' with ur ";
  	
  	   	String tJudgeFlag =  tExeSQL.getOneValue(tJudgeSQL);
  	   	//孤儿单表中不存在，表示不是孤儿单
  	   	if(null==tJudgeFlag||"".equals(tJudgeFlag))
  	   	{
  	    	tJudgeResult="0";
  	    }
  	   	return tJudgeResult;
    }
    
    /**
     * 对于孤儿单提奖比例，需要进行折算
     * @return
     * 说明 ： #2772关于调整个险孤儿单续期佣金发放比例和年限的需求（需求编号：2015-289）
     */
    private double getFYCRate(LACommisionNewSchema cLACommisionNewSchema,double cRate)
    {
    	double tReturnResult = 1;
    	int tReNewCount = cLACommisionNewSchema.getReNewCount();
   		int tPayYear = cLACommisionNewSchema.getPayYear();
   		if(0==tReNewCount&&tPayYear>0)
   		{
   			int tPayCount = cLACommisionNewSchema.getPayCount();
   			if((tPayCount<6 && tPayCount>1) )
   			{
   				tReturnResult=0.5*cRate;
   			}
   			else
   			{
   				tReturnResult=0.01;
   			}
   		}
   		else if(tReNewCount>0 )
   		{
   			tReturnResult=0.5*cRate;
   		}
    	return tReturnResult;
    }
    
    /**
     * 返回此条数据之前正常的提奖比例
     * @param tLACommisionNewSchema
     * @return
     */
    private double getNoZCFycRate(LACommisionNewSchema tLACommisionNewSchema)
    {
    	double tFycRate = 0;
    	String tPolicyNo = tLACommisionNewSchema.getPolNo();
    	if(!"00000000000000000000".equals(tLACommisionNewSchema.getGrpPolNo()))
    	{
    		tPolicyNo = tLACommisionNewSchema.getGrpPolNo();
    	}
    	LACommisionDB tLACommisionDB = new LACommisionDB();
        String tSQL ="select * from lacommisionNew where polno='"+tPolicyNo+ "'"
        		+ " and TransType='ZC' and SignDate='" + tLACommisionNewSchema.getSignDate() +"' "
				+ " and TransState='"+tLACommisionNewSchema.getTransState()+"' "
				+ " and tmakedate<>'"+tLACommisionNewSchema.getTMakeDate()+"' "
				+ " and substr(PayPlanCode,1,6)<>'000000'"
				+ " order by commisionsn fetch first 1 rows only with ur";
        System.out.println("AgentWageOfCalFyc-->getNoZCFycRate-->tSQL:" + tSQL);
        LACommisionSet tLACommisionSet = new LACommisionSet();
        tLACommisionSet = tLACommisionDB.executeQuery(tSQL);
        if(null==tLACommisionSet||0==tLACommisionSet.size())
        {
        	//说明没找到之前正常的数据，有可能是和本次数据一起提取的 
        	tFycRate = -1;
        }
        else
        {
        	tFycRate = tLACommisionSet.get(1).getFYCRate();
        }
    	return tFycRate;
    }
}
