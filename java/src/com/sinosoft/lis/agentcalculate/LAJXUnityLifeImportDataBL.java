package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

import java.math.BigDecimal;
import java.sql.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:保单查询功能类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author HST
 * @version 1.0
 */
public class LAJXUnityLifeImportDataBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate;
	private MMap mMap = new MMap();
	private VData mOutputData = new VData();
	private FDate fDate = new FDate();
	private Connection con;
	private String mMakeDate;
	private String IP;
	private String url;
	private String user;
	private String passWord;
	private String port;
	private String sidName;
	

	public LAJXUnityLifeImportDataBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @throws SQLException
	 */
	public boolean submitData(VData cInputData) {
		// 将操作数据拷贝到本类中
		// this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("End AgentChargeCalSaveNewBL Submit...");
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		String tSql = "select code,codename from ldcode where codetype = 'jxxss' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS codeSSRS = new SSRS();
		codeSSRS = tExeSQL.execSQL(tSql);
		for (int i = 1; i <= codeSSRS.getMaxRow(); i++) {
			if("IP".equals(codeSSRS.GetText(i, 1))){
				IP=codeSSRS.GetText(i, 2);
			}
			if("User".equals(codeSSRS.GetText(i, 1))){
				user=codeSSRS.GetText(i, 2);
			}
			if("PassWord".equals(codeSSRS.GetText(i, 1))){
				passWord=codeSSRS.GetText(i, 2);
			}
			if("Port".equals(codeSSRS.GetText(i, 1))){
				port=codeSSRS.GetText(i, 2);
			}
			if("SidName".equals(codeSSRS.GetText(i, 1))){
				sidName = codeSSRS.GetText(i, 2);
			}
			
		}
		url = "jdbc:oracle:thin:@"+IP+":"+port+":"+sidName;
		System.out.println("url:"+url);
		if(url==null||"".equals(url)){
			System.out.println("数据库连接获取失败");
			return false;
		}
		return true;
	}

	/*
	 * 处理数据的方法
	 */
	public boolean dealData() {
		String mSQL = "select (select yearmonth from LAStatSegment where stattype= '1' and (a.tmakedate between startdate and enddate)),"
				+ "a.polno,a.mainpolno,a.managecom,a.riskcode,a.riskversion,a.dutycode,"
				+ "a.payplancode,a.cvalidate,a.payintv,a.paymode,a.receiptno,a.tpaydate,a.tenteraccdate,"
				+ "a.tconfdate,a.tmakedate,a.commdate,a.transmoney,a.transstandmoney,a.lastpaytodate,a.curpaytodate,"
				+ "a.transstate,a.directwage,a.appendwage,a.flag,a.calcdate,a.payyear,a.payyears,a.years,"
				+ "a.paycount,a.signdate,a.getpoldate,a.branchtype,a.agentcom,a.agentcode,a.p3,a.p11,a.p12,a.p13,"
				+ "a.makepoldate,a.customgetpoldate,a.riskmark,a.makedate,a.maketime,a.modifydate,a.modifytime,"
				+ "a.renewcount,a.endorsementno,current date,a.fycrate,"
				+ "(select charge from lacharge where commisionsn = a.commisionsn),"
				+ "(select chargerate from lacharge where commisionsn = a.commisionsn),"
				+ "db2inst1.getunitecode(a.agentcode),"
				+ "(case when (select grpagentcode from lccont where contno = a.contno) is not null then"
				+ "(select grpagentcode from lccont where contno = a.contno) "
				+ " when (select grpagentcode from lbcont where contno = a.contno) is not null then "
				+ " (select grpagentcode from lbcont where contno = a.contno) "
				+ " when (select grpagentcode from lcgrpcont where grpcontno = a.contno) is not null then "
				+ " (select grpagentcode from lcgrpcont where grpcontno = a.contno) "
				+ " else (select grpagentcode from lbgrpcont where grpcontno = a.contno) end),"
				+ "(case when (select actype from lacom where agentcom=a.agentcom)='06' then 'C'  "
				+ " when (select actype from lacom where agentcom=a.agentcom)='07' then 'S' else '' end)"
				+ ",NULL from lacommision a"
				+ " where  "
				+ " a.branchtype= '5' and a.branchtype2 = '01' and a.branchtype3= '2' "
				+ " and a.transstate<>'03' "
//				 + " and a.tmakedate between '2018-8-1' and '2018-11-5'"
				// 因手续费是自动结算的则认为makedate即为手续费结算日期
				+ " and exists (select '1' from ljaget where getnoticeno =a.commisionsn and OtherNoType = 'BC' and confdate is not null and shoulddate = (current date -1 days))"
				+ " and exists (select '1' from lacharge where commisionsn =a.commisionsn and chargestate = '1' )"
				+ " and exists (select '1' from lacom  where agentcom = a.agentcom and actype in('07'))"
				+ " and exists (select '1' from ldcode where codetype = 'jiaochaoMng' and trim(code) = a.managecom and othersign = 'S') with ur"
//				+ "and a.managecom in ('','','')"
				;
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println(mSQL);
		tSSRS = tExeSQL.execSQL(mSQL);
		System.out.println("----" + tSSRS.getMaxRow());

		// String tSQL = "select count(1) from t04policyperdaybaselist_jkx";
		try {
			// 加载oracle的驱动类
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out.println("找不到驱动程序类 ，加载驱动失败！");
			e.printStackTrace();
		}
		try {
			// 创建链接
			System.out.println("创建连接");
			con = DriverManager.getConnection(url,user,passWord);
			String delSQL="delete from t04policyperdaybaselist_jkx";
			PreparedStatement ps1 = con.prepareStatement(delSQL);
			ps1.executeUpdate();
			con.commit();
			System.out.println("删除完毕");
			
			String tSQL = "insert into t04policyperdaybaselist_jkx "
					+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
					+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
					+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(tSQL);
			System.out.println("生成数据");
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				ps.setString(1, tSSRS.GetText(i, 1));
				ps.setString(2, tSSRS.GetText(i, 2));
				ps.setString(3, tSSRS.GetText(i, 3));
				ps.setString(4, tSSRS.GetText(i, 4));
				ps.setString(5, tSSRS.GetText(i, 5));
				ps.setString(6, tSSRS.GetText(i, 6));
				ps.setString(7, tSSRS.GetText(i, 7));
				ps.setString(8, tSSRS.GetText(i, 8));
				ps.setDate(9,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 9))
								.getTime()));
				ps.setInt(10, Integer.parseInt(tSSRS.GetText(i, 10)));
				ps.setString(11, tSSRS.GetText(i, 11));
				ps.setString(12, tSSRS.GetText(i, 12));
				ps.setDate(13,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 13))
								.getTime()));
				ps.setDate(14,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 14))
								.getTime()));
				ps.setDate(15,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 15))
								.getTime()));
				ps.setDate(16,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 16))
								.getTime()));
				ps.setDate(17,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 17))
								.getTime()));
				ps.setDouble(18, Double.parseDouble(tSSRS.GetText(i, 18)));
				ps.setDouble(19, Double.parseDouble(tSSRS.GetText(i, 19)));
				ps.setDate(20,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 20))
								.getTime()));
				ps.setDate(21,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 21))
								.getTime()));
				ps.setString(22, tSSRS.GetText(i, 22));
				ps.setString(23, tSSRS.GetText(i, 23));
				ps.setString(24, tSSRS.GetText(i, 24));
				ps.setString(25, tSSRS.GetText(i, 25));
				if (!tSSRS.GetText(i, 26).equals("")) {
					ps.setDate(
							26,
							new java.sql.Date(fDate.getDate(
									tSSRS.GetText(i, 26)).getTime()));
				} else {
					ps.setDate(26, null);
				}

				ps.setInt(27, Integer.parseInt(tSSRS.GetText(i, 27)));
				ps.setInt(28, Integer.parseInt(tSSRS.GetText(i, 28)));
				ps.setInt(29, Integer.parseInt(tSSRS.GetText(i, 29)));
				ps.setInt(30, Integer.parseInt(tSSRS.GetText(i, 30)));
				ps.setDate(31,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 31))
								.getTime()));
				if (!tSSRS.GetText(i, 32).equals("")) {
					ps.setDate(
							32,
							new java.sql.Date(fDate.getDate(
									tSSRS.GetText(i, 32)).getTime()));
				}
				ps.setString(33, tSSRS.GetText(i, 33));
				ps.setString(34, tSSRS.GetText(i, 34));
				ps.setString(35, tSSRS.GetText(i, 35));
				ps.setBigDecimal(36, new BigDecimal(tSSRS.GetText(i, 36)));
				ps.setString(37, tSSRS.GetText(i, 37));
				ps.setString(38, tSSRS.GetText(i, 38));
				ps.setString(39, tSSRS.GetText(i, 39));
				ps.setDate(40,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 40))
								.getTime()));
				if (!tSSRS.GetText(i, 41).equals("")) {
					ps.setDate(
							41,
							new java.sql.Date(fDate.getDate(
									tSSRS.GetText(i, 41)).getTime()));
				} else {
					ps.setDate(41, null);
				}
				ps.setString(42, tSSRS.GetText(i, 42));
				ps.setDate(43,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 43))
								.getTime()));
				ps.setString(44, tSSRS.GetText(i, 44));
				ps.setDate(45,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 45))
								.getTime()));
				ps.setString(46, tSSRS.GetText(i, 46));
				ps.setInt(47, Integer.parseInt(tSSRS.GetText(i, 47)));
				ps.setString(48, tSSRS.GetText(i, 48));
				ps.setDate(49,
						new java.sql.Date(fDate.getDate(tSSRS.GetText(i, 49))
								.getTime()));
				ps.setBigDecimal(50, new BigDecimal(tSSRS.GetText(i, 50)));
				ps.setDouble(51, Double.parseDouble(tSSRS.GetText(i, 51)));
				ps.setBigDecimal(52, new BigDecimal(tSSRS.GetText(i, 52)));
				ps.setString(53, tSSRS.GetText(i, 53));
				ps.setString(54, tSSRS.GetText(i, 54));
				ps.setString(55, tSSRS.GetText(i, 55));
				if (!tSSRS.GetText(i, 56).equals("")) {
					ps.setDate(
							56,
							new java.sql.Date(fDate.getDate(
									tSSRS.GetText(i, 56)).getTime()));
				} else {
					ps.setDate(56, null);
				}
				// ps.setString(57, tSSRS.GetText(i, 57));
				// ps.setString(58, tSSRS.GetText(i, 58));
				// ps.setString(59, tSSRS.GetText(i, 59));
				// ps.setString(60, tSSRS.GetText(i, 60));

				ps.addBatch();

			}
			ps.executeBatch();
			System.out.println("开始提交");
			con.commit();
			System.out.println("提交完毕");
			ps.close();
			ps1.close();
			con.close();
			System.out.print("连接关闭");
			// Statement stmt = con.createStatement();
			// ResultSet res = stmt.executeQuery(tSQL);
			// System.out.println(res.getRow());
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("提交失败..." + e.getMessage());
			CError tError = new CError();
			tError.moduleName = "OracleConn";
			tError.functionName = "createConnection";
			tError.errorMessage = "Connect failed!  error code ="
					+ e.getErrorCode();
			this.mErrors.addOneError(tError);
			return false;
		}
		;

		return true;
	}

	public static void main(String[] args) {
		LAJXUnityLifeImportDataBL tLAJXTest = new LAJXUnityLifeImportDataBL();
		 boolean m = tLAJXTest.dealData();
		// FDate fDate = new FDate();
		// System.out.println(fDate.getDate("null").getTime());
	}

}