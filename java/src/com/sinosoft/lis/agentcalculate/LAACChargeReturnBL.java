package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageActivityLogSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageActivityLogSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LAACReCalculateBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author 
 * @version 1.0----2009-1-13
 */
public class LAACChargeReturnBL {
  //错误处理类
	public CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */ 
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAChargeSet mLAChargeSet = new LAChargeSet();
  private LAChargeSet mUpLAChargeSet = new LAChargeSet();
  private LACommisionSet mUpLACommisionSet = new LACommisionSet();
  private LAWageActivityLogSet mLAWageActivityLogSet=new LAWageActivityLogSet();
  private LJAGetSet mLJAGetSet=new LJAGetSet();
  
  private String tLimit = "";
  private String mManageCom="";
  private String mTMakeDate="";
  private String mGrpContNo="";
  private String mAgentCom="";
  private String mContNo="";
  private String mWrapCode="";
  private String mRiskCode="";
  
  private String mWageNo = "";

  public LAACChargeReturnBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();

    if (!tPubSubmit.submitData(mInputData, mOperate)) {
     // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
     CError tError = new CError();
      tError.moduleName = "LAACChargeReturnBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
     this.mErrors.addOneError(tError);
      return false;
   }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      this.mErrors.clearErrors();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
      
      this.mWageNo=AgentPubFun.formatDate(CurrentDate,"yyyyMM");
      System.out.println("mWageNo:"+mWageNo);
      
     if(this.mOperate.equals("SelectReturn")){
    	 this.mLAChargeSet.set( (LAChargeSet) cInputData.getObjectByObjectName("LAChargeSet",0));
     }
     else if(this.mOperate.equals("AllReturn")){
    	 
    	 this.mManageCom=(String)cInputData.get(1);
    	 this.mTMakeDate=(String)cInputData.get(2);
    	 this.mGrpContNo=(String)cInputData.get(3);
//    	 this.mAgentCom=(String)cInputData.get(2);
//    	 this.mContNo=(String)cInputData.get(3);
//    	 this.mRiskCode=(String)cInputData.get(4);
//    	 this.mWrapCode=(String)cInputData.get(5);
    	 
     }
     else{
    	 CError tError = new CError();
         tError.moduleName = "LAACReCalculateBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "不支持的操作类型,请验证操作类型.";
         this.mErrors.addOneError(tError);
         return false;
     }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAACReCalculateBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 新增，修改传入的数据校验
   */
  private boolean check(){
	  //---等等再写
	  
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAACReCalculateBL.dealData........."+mOperate);
    try {
    	LAChargeDB tLAChargeDB=new LAChargeDB();
    	
      if (mOperate.equals("SelectReturn")) {
    	  LAChargeSet tLAChargeSet ; 
          for(int i=1;i<=mLAChargeSet.size();i++){
        	  String tSQL = "select * from lacharge where branchtype = '2'and branchtype2 = '02' and chargestate = '1'" 
        	  		+ " and commisionsn = '"+mLAChargeSet.get(i).getCommisionSN()+"'";
        	  
        	  System.out.println(tSQL);
        	  tLAChargeSet = tLAChargeDB.executeQuery(tSQL);
        	  
        	  System.out.println("查找出来的手续费数据大小为："+tLAChargeSet.size());
        	  for(int k = 1;k<=tLAChargeSet.size();k++){
        		  
        		  //存个轨迹，避免多次反冲
        	   		LAWageActivityLogSchema mLAWageActivityLogSchema=new LAWageActivityLogSchema();  
        	   		String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
        	   		mLAWageActivityLogSchema.setSerialNo(tSerialNo);
        	   		mLAWageActivityLogSchema.setWageLogType("CR");
        	   		mLAWageActivityLogSchema.setAgentCode("0000000000");
        	   		mLAWageActivityLogSchema.setAgentGroup("0000000000");
        	   		mLAWageActivityLogSchema.setGrpContNo(tLAChargeSet.get(k).getGrpContNo());
        	   		mLAWageActivityLogSchema.setContNo(tLAChargeSet.get(k).getContNo());
        	   		mLAWageActivityLogSchema.setGrpPolNo(tLAChargeSet.get(k).getGrpPolNo());
        	   		mLAWageActivityLogSchema.setPolNo(tLAChargeSet.get(k).getPolNo()); 
        	   		mLAWageActivityLogSchema.setRiskCode(tLAChargeSet.get(k).getRiskCode());
        	   		mLAWageActivityLogSchema.setDescribe("手续费回退");
        	   		mLAWageActivityLogSchema.setOtherNo(tLAChargeSet.get(k).getCommisionSN());
        	   		mLAWageActivityLogSchema.setManageCom(tLAChargeSet.get(k).getManageCom());
        	   		mLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
        	   		mLAWageActivityLogSchema.setMakeDate(CurrentDate);
        	   		mLAWageActivityLogSchema.setMakeTime(CurrentTime);
        	   		mLAWageActivityLogSet.add(mLAWageActivityLogSchema);
        		  
        		  
        		  //冲负处理
        		  LAChargeSchema tLAChargeSchema = new LAChargeSchema();
        		  tLAChargeSchema = tLAChargeSet.get(k).getSchema();
        		  String tCommisionsn1 = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
        		  System.out.println("生成的冲负数据主键为："+tCommisionsn1);
        		  tLAChargeSchema.setCommisionSN(tCommisionsn1);
        		  tLAChargeSchema.setTransMoney(-tLAChargeSet.get(k).getTransMoney());
        		  tLAChargeSchema.setCharge(-tLAChargeSet.get(k).getCharge());
        		  
        		  tLAChargeSchema.setMakeDate(CurrentDate);
        		  tLAChargeSchema.setMakeTime(CurrentTime);
        		  tLAChargeSchema.setModifyDate(CurrentDate);
        		  tLAChargeSchema.setModifyTime(CurrentTime);
        		  tLAChargeSchema.setCalDate(CurrentDate);
        		  tLAChargeSchema.setTMakeDate(CurrentDate);
        		  tLAChargeSchema.setChargeState("1");
        		  mUpLAChargeSet.add(tLAChargeSchema);
        		  
        		  //冲负的同时结算手续费
        		  
        		  
        		  //===冲正处理
        		  LAChargeSchema tempLAChargeSchema = new LAChargeSchema();
        		  tempLAChargeSchema = tLAChargeSet.get(k).getSchema();
        		  String tCommisionsn2 = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
        		  System.out.println("生成的冲正数据主键为："+tCommisionsn2);
        		  tempLAChargeSchema.setCommisionSN(tCommisionsn2);
        		  tempLAChargeSchema.setMakeDate(CurrentDate);
        		  tempLAChargeSchema.setMakeTime(CurrentTime);
        		  tempLAChargeSchema.setModifyDate(CurrentDate);
        		  tempLAChargeSchema.setModifyTime(CurrentTime);
        		  tempLAChargeSchema.setCalDate(CurrentDate);
        		  tempLAChargeSchema.setTMakeDate(CurrentDate);
        		  tempLAChargeSchema.setChargeRate("0");
        		  tempLAChargeSchema.setCharge("0");
        		  tempLAChargeSchema.setChargeState("0");
        		  mUpLAChargeSet.add(tempLAChargeSchema);
        		 
        	  LACommisionDB tLACommisionDB = new LACommisionDB();
        	  LACommisionSet tLACommisionSet;
        	  String tempSQL = "select * from lacommision where commisionsn = '"+tLAChargeSet.get(k).getCommisionSN()+"'";
        	  System.out.println(tempSQL);
        	  tLACommisionSet=tLACommisionDB.executeQuery(tempSQL);
        	  for(int j = 1;j<=tLACommisionSet.size();j++){
        		  //冲负处理
        		  LACommisionSchema tLACommisionSchema = new LACommisionSchema();
        		  tLACommisionSchema = tLACommisionSet.get(j).getSchema();
        		  tLACommisionSchema.setCommisionSN(tCommisionsn1);
        		  tLACommisionSchema.setWageNo(mWageNo);
        		  tLACommisionSchema.setCalDate(CurrentDate);
        		  tLACommisionSchema.setTMakeDate(CurrentDate);
        		  tLACommisionSchema.setCalDate(CurrentDate);
        		  tLACommisionSchema.setMakeDate(CurrentDate);
        		  tLACommisionSchema.setMakeTime(CurrentTime);
        		  tLACommisionSchema.setModifyDate(CurrentDate);
        		  tLACommisionSchema.setMakeTime(CurrentTime);
        		  tLACommisionSchema.setTransMoney(-tLACommisionSet.get(j).getTransMoney());
        		  tLACommisionSchema.setTransStandMoney(-tLACommisionSet.get(j).getTransStandMoney());
        		  tLACommisionSchema.setDirectWage(-tLACommisionSet.get(j).getDirectWage());
        		  tLACommisionSchema.setFYC(-tLACommisionSet.get(j).getFYC());
        		  mUpLACommisionSet.add(tLACommisionSchema);

        		  //冲正处理
        		  LACommisionSchema tempLACommisionSchema = new LACommisionSchema();
        		  tempLACommisionSchema = tLACommisionSet.get(j).getSchema();
        		  tempLACommisionSchema.setCommisionSN(tCommisionsn2);
        		  tempLACommisionSchema.setWageNo(mWageNo);
        		  tempLACommisionSchema.setCalDate(CurrentDate);
        		  tempLACommisionSchema.setTMakeDate(CurrentDate);
        		  tempLACommisionSchema.setCalDate(CurrentDate);
        		  tempLACommisionSchema.setMakeDate(CurrentDate);
        		  tempLACommisionSchema.setMakeTime(CurrentTime);
        		  tempLACommisionSchema.setModifyDate(CurrentDate);
        		  tempLACommisionSchema.setMakeTime(CurrentTime);
        		  mUpLACommisionSet.add(tempLACommisionSchema);

        		  
        		  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                  tLimit = PubFun.getNoLimit(tLAChargeSet.get(k).getManageCom());
                  tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("ACGETNO", tLimit));
                  tLJAGetSchema.setOtherNo(tLAChargeSet.get(k).getReceiptNo());
                  tLJAGetSchema.setOtherNoType("AC");
            	  tLJAGetSchema.setOtherNo(tLAChargeSet.get(k).getReceiptNo());
            	  tLJAGetSchema.setPayMode("12");
                  tLJAGetSchema.setManageCom(tLAChargeSet.get(k).getManageCom());
                  tLJAGetSchema.setAgentCom(tLAChargeSet.get(k).getAgentCom());
                  tLJAGetSchema.setGetNoticeNo(tCommisionsn1);

                  System.out.println("============ljaget中GetNoticeNo为："+tCommisionsn1);

                  tLJAGetSchema.setAgentCode(tLACommisionSet.get(j).getAgentCode());
                  tLJAGetSchema.setAgentGroup(tLACommisionSet.get(j).getAgentGroup());
                  tLJAGetSchema.setAppntNo(tLACommisionSet.get(j).getAppntNo());
                  tLJAGetSchema.setSumGetMoney(-tLAChargeSet.get(k).getCharge());
                  tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(j).getAgentCom()));
                  tLJAGetSchema.setDrawerID(tLACommisionSet.get(j).getAgentCom());
                  tLJAGetSchema.setShouldDate(CurrentDate);
                  tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
                  tLJAGetSchema.setMakeDate(CurrentDate);
                  tLJAGetSchema.setMakeTime(CurrentTime);
                  tLJAGetSchema.setModifyDate(CurrentDate);
                  tLJAGetSchema.setModifyTime(CurrentTime);
                  this.mLJAGetSet.add(tLJAGetSchema);
        		   
        	  }
        	}
          }
        }
      else if(mOperate.equals("AllReturn")){
    	  LAChargeDB tempLAChargeDB=new LAChargeDB();
    	  LAChargeSet tLAChargeSet;
    	  String tSQL = "select * from lacharge where chargetype='51"
        	  +"' and branchtype='2' and branchtype2='02' and chargestate='1' and chargetype = '51'"
        	  +"  and managecom = '"+this.mManageCom+"' and grpcontno = '"+this.mGrpContNo+"'"
        	  +" and tmakedate='"+this.mTMakeDate+"'";
//    	    if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
//    	    {
//    	    	tSQL+=" and riskcode = '"+this.mRiskCode+"'" ;
//    	    }
//    	    if(this.mAgentCom!=null&&!this.mAgentCom.equals(""))
//    	    {
//    	    	tSQL+=" and agentcom = '"+this.mAgentCom+"'";
//    	    }
//    	    if(this.mWrapCode!=null&&!this.mWrapCode.equals(""))
//    	    {
//    	    	tSQL+=" and exists (select 'Y' from lacommision where f3 = '"+this.mWrapCode+"')";
//    	    }
        	tSQL += " with ur";
        	
        System.out.println("全部回退查询页面数据SQL如下："+tSQL);
        
        tLAChargeSet =tempLAChargeDB.executeQuery(tSQL); 
        System.out.println("回退数据大小为："+tLAChargeSet.size());
        for(int i = 1;i<=tLAChargeSet.size();i++)
        {
        	LAChargeSet upLAChargeSet = new LAChargeSet();
        	//冲负处理
  		String tempSQL = "select * from lacharge where branchtype = '2'and branchtype2 = '02' and chargestate = '1'" 
	  		+ " and commisionsn = '"+tLAChargeSet.get(i).getCommisionSN()+"'";
	  
	  System.out.println(tempSQL);
	  upLAChargeSet = tLAChargeDB.executeQuery(tempSQL);
	  
	  System.out.println("查找出来的手续费数据大小为："+upLAChargeSet.size());
	  for(int k = 1;k<=upLAChargeSet.size();k++){
		  
		  //存个轨迹，避免多次反冲
	   		LAWageActivityLogSchema mLAWageActivityLogSchema=new LAWageActivityLogSchema();  
	   		String tSerialNo = PubFun1.CreateMaxNo("WAGEACTIVITYLOG", 12);
	   		mLAWageActivityLogSchema.setSerialNo(tSerialNo);
	   		mLAWageActivityLogSchema.setWageLogType("02");
	   		mLAWageActivityLogSchema.setAgentCode("0000000000");
	   		mLAWageActivityLogSchema.setAgentGroup("0000000000");
	   		mLAWageActivityLogSchema.setGrpContNo(upLAChargeSet.get(k).getGrpContNo());
	   		mLAWageActivityLogSchema.setContNo(upLAChargeSet.get(k).getContNo());
	   		mLAWageActivityLogSchema.setGrpPolNo(upLAChargeSet.get(k).getGrpPolNo());
	   		mLAWageActivityLogSchema.setPolNo(upLAChargeSet.get(k).getPolNo()); 
	   		mLAWageActivityLogSchema.setRiskCode(upLAChargeSet.get(k).getRiskCode());
	   		mLAWageActivityLogSchema.setDescribe("手续费回退");
	   		mLAWageActivityLogSchema.setOtherNo(upLAChargeSet.get(k).getCommisionSN());
	   		mLAWageActivityLogSchema.setManageCom(upLAChargeSet.get(k).getManageCom());
	   		mLAWageActivityLogSchema.setOperator(mGlobalInput.Operator);
	   		mLAWageActivityLogSchema.setMakeDate(CurrentDate);
	   		mLAWageActivityLogSchema.setMakeTime(CurrentTime);
	   		mLAWageActivityLogSet.add(mLAWageActivityLogSchema);
		  
		  //冲负处理
		  LAChargeSchema tLAChargeSchema = new LAChargeSchema();
		  tLAChargeSchema = upLAChargeSet.get(k).getSchema();
		  String tCommisionsn1 = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
		  System.out.println("生成的冲负数据主键为："+tCommisionsn1);
		  tLAChargeSchema.setCommisionSN(tCommisionsn1);
		  tLAChargeSchema.setTransMoney(-upLAChargeSet.get(k).getTransMoney());
		  tLAChargeSchema.setCharge(-upLAChargeSet.get(k).getCharge());
		  
		  tLAChargeSchema.setMakeDate(CurrentDate);
		  tLAChargeSchema.setMakeTime(CurrentTime);
		  tLAChargeSchema.setModifyDate(CurrentDate);
		  tLAChargeSchema.setModifyTime(CurrentTime);
		  tLAChargeSchema.setCalDate(CurrentDate);
		  tLAChargeSchema.setTMakeDate(CurrentDate);
		  tLAChargeSchema.setChargeState("1");
		  mUpLAChargeSet.add(tLAChargeSchema);
		  
		  //冲负的同时结算手续费
		  
		  
		  //===冲正处理
		  LAChargeSchema tempLAChargeSchema = new LAChargeSchema();
		  tempLAChargeSchema = upLAChargeSet.get(k).getSchema();
		  String tCommisionsn2 = PubFun1.CreateMaxNo("COMMISIONSN", "SN");
		  System.out.println("生成的冲正数据主键为："+tCommisionsn2);
		  tempLAChargeSchema.setCommisionSN(tCommisionsn2);
		  tempLAChargeSchema.setMakeDate(CurrentDate);
		  tempLAChargeSchema.setMakeTime(CurrentTime);
		  tempLAChargeSchema.setModifyDate(CurrentDate);
		  tempLAChargeSchema.setModifyTime(CurrentTime);
		  tempLAChargeSchema.setCalDate(CurrentDate);
		  tempLAChargeSchema.setTMakeDate(CurrentDate);
		  tempLAChargeSchema.setChargeRate("0");
		  tempLAChargeSchema.setCharge("0");
		  tempLAChargeSchema.setChargeState("0");
		  mUpLAChargeSet.add(tempLAChargeSchema);
		 
	  LACommisionDB tLACommisionDB = new LACommisionDB();
	  LACommisionSet tLACommisionSet;
	  String tempSQL1 = "select * from lacommision where commisionsn = '"+upLAChargeSet.get(k).getCommisionSN()+"'";
	  System.out.println(tempSQL);
	  tLACommisionSet=tLACommisionDB.executeQuery(tempSQL1);
	  for(int j = 1;j<=tLACommisionSet.size();j++){
		  //冲负处理
		  LACommisionSchema tLACommisionSchema = new LACommisionSchema();
		  tLACommisionSchema = tLACommisionSet.get(j).getSchema();
		  tLACommisionSchema.setCommisionSN(tCommisionsn1);
		  tLACommisionSchema.setWageNo(mWageNo);
		  tLACommisionSchema.setCalDate(CurrentDate);
		  tLACommisionSchema.setTMakeDate(CurrentDate);
		  tLACommisionSchema.setCalDate(CurrentDate);
		  tLACommisionSchema.setMakeDate(CurrentDate);
		  tLACommisionSchema.setMakeTime(CurrentTime);
		  tLACommisionSchema.setModifyDate(CurrentDate);
		  tLACommisionSchema.setMakeTime(CurrentTime);
		  tLACommisionSchema.setTransMoney(-tLACommisionSet.get(j).getTransMoney());
		  tLACommisionSchema.setTransStandMoney(-tLACommisionSet.get(j).getTransStandMoney());
		  tLACommisionSchema.setDirectWage(-tLACommisionSet.get(j).getDirectWage());
		  tLACommisionSchema.setFYC(-tLACommisionSet.get(j).getFYC());
		  mUpLACommisionSet.add(tLACommisionSchema);

		  //冲正处理
		  LACommisionSchema tempLACommisionSchema = new LACommisionSchema();
		  tempLACommisionSchema = tLACommisionSet.get(j).getSchema();
		  tempLACommisionSchema.setCommisionSN(tCommisionsn2);
		  tempLACommisionSchema.setWageNo(mWageNo);
		  tempLACommisionSchema.setCalDate(CurrentDate);
		  tempLACommisionSchema.setTMakeDate(CurrentDate);
		  tempLACommisionSchema.setCalDate(CurrentDate);
		  tempLACommisionSchema.setMakeDate(CurrentDate);
		  tempLACommisionSchema.setMakeTime(CurrentTime);
		  tempLACommisionSchema.setModifyDate(CurrentDate);
		  tempLACommisionSchema.setMakeTime(CurrentTime);
		  mUpLACommisionSet.add(tempLACommisionSchema);

		  
		  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
          tLimit = PubFun.getNoLimit(tLAChargeSet.get(k).getManageCom());
          tLJAGetSchema.setActuGetNo(PubFun1.CreateMaxNo("ACGETNO", tLimit));
          tLJAGetSchema.setOtherNo(upLAChargeSet.get(k).getReceiptNo());
          tLJAGetSchema.setOtherNoType("AC");
    	  tLJAGetSchema.setOtherNo(upLAChargeSet.get(k).getReceiptNo());
    	  tLJAGetSchema.setPayMode("12");
          tLJAGetSchema.setManageCom(upLAChargeSet.get(k).getManageCom());
          tLJAGetSchema.setAgentCom(upLAChargeSet.get(k).getAgentCom());
          tLJAGetSchema.setGetNoticeNo(tCommisionsn1);

          System.out.println("============ljaget中GetNoticeNo为："+tCommisionsn1);

          tLJAGetSchema.setAgentCode(tLACommisionSet.get(j).getAgentCode());
          tLJAGetSchema.setAgentGroup(tLACommisionSet.get(j).getAgentGroup());
          tLJAGetSchema.setAppntNo(tLACommisionSet.get(j).getAppntNo());
          tLJAGetSchema.setSumGetMoney(-upLAChargeSet.get(k).getCharge());
          tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(j).getAgentCom()));
          tLJAGetSchema.setDrawerID(tLACommisionSet.get(j).getAgentCom());
          tLJAGetSchema.setShouldDate(CurrentDate);
          tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
          tLJAGetSchema.setMakeDate(CurrentDate);
          tLJAGetSchema.setMakeTime(CurrentTime);
          tLJAGetSchema.setModifyDate(CurrentDate);
          tLJAGetSchema.setModifyTime(CurrentTime);
          this.mLJAGetSet.add(tLJAGetSchema);
		   
	  }
	}
        	
        }
    	  
    		
      }     
      else{
    	  CError tError = new CError();
          tError.moduleName = "LAACChargeReturnBL";
          tError.functionName = "dealData";
          tError.errorMessage = "不支持的操作类型,请验证操作类型.";
          this.mErrors.addOneError(tError);
          return false;
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAACChargeReturnBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      
      map.put(this.mLAWageActivityLogSet, "INSERT");
      map.put(this.mUpLACommisionSet, "INSERT");
      map.put(this.mLJAGetSet, "INSERT");
      map.put(this.mUpLAChargeSet, "INSERT");
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAACChargeReturnBL";
      tError.functionName = "LAACChargeReturnBL";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  
  private String getAgentComName(String tAgentCom){
	  String comName = "";
	  ExeSQL tExeSQL=new ExeSQL();
	  String sql=" select name from lacom where agentcom='"+tAgentCom+"' with ur ";
	  SSRS tSSRS = tExeSQL.execSQL(sql);	    	 
	  comName = tSSRS.GetText(1, 1);
	  return comName ;
  }
}
