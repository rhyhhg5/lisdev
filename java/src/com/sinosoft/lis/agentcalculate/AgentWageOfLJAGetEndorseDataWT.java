package com.sinosoft.lis.agentcalculate;

import java.text.ParseException;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

public class AgentWageOfLJAGetEndorseDataWT extends AgentWageOfAbstractClass 
{
    
	
	/**
	 * 主要提数逻辑
	 * @param args
	 * @throws ParseException
	 */
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfLJAGetEndorseDataWT-->dealData:开始执行");
		System.out.println("AgentWageOfLJAGetEndorseDataWT-->dealData:cSQL:"+cSQL);
		LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAGetEndorseSet, cSQL);
		
		//用来判断数据是不是为空，为了防止 最后一次循环时，为空时，也打印错误语句  
		boolean tJudgeNullFlag = false;
		
		do 
		{
		   MMap tMMap = new MMap();
		   tRSWrapper.getData();
		   if(!tJudgeNullFlag)
		   {
			   if(null == tLJAGetEndorseSet || 0==tLJAGetEndorseSet.size())
			   {
				   System.out.println("AgentWageOfLJAPayPersonData-->dealData 没有满足条件的数据");
				   return true;
			   }
			   tJudgeNullFlag = true;
		   }
		   
		   //用来提取犹豫期退保数据插入到数据库
		   LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
		   //用来更新犹豫期退保之前 保单的更新
		   LACommisionNewSet tUpDateLACommisionNewSet = new LACommisionNewSet();
		   LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
		   for (int i = 1; i <= tLJAGetEndorseSet.size(); i++)
		   {
			   LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
			   LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
			   tLJAGetEndorseSchema = tLJAGetEndorseSet.get(i);
			   String tPolNo = tLJAGetEndorseSchema.getPolNo();
			   String tContNo = tLJAGetEndorseSchema.getContNo();
			   String tRiskCode = tLJAGetEndorseSchema.getRiskCode();
			   String tAgentCode = tLJAGetEndorseSchema.getAgentCode();
			   String tDutyCode = tLJAGetEndorseSchema.getDutyCode();
			   String tPayPlanCode = tLJAGetEndorseSchema.getPayPlanCode();
//			   //判断此犹豫期撤单件 是否已经提过数 ,如果提过,则不再提取
//			   LACommisionNewDB tLACommisionNewDB = new LACommisionNewDB();
//			   tLACommisionNewDB.setPolNo(tPolNo);
//			   tLACommisionNewDB.setTransType("WT");
//			   tLACommisionNewDB.setDutyCode(tDutyCode);
//			   tLACommisionNewDB.setPayPlanCode(tPayPlanCode);
//			   
//			   LACommisionNewSet tjudgeLACommisionSet = new LACommisionNewSet();
//			   tjudgeLACommisionSet = tLACommisionNewDB.query();
//	           if (tjudgeLACommisionSet.size() > 0) 
//	           { 
//	        	   //如果提过数了,则返回,不再提取
//	        	   LACommisionErrorSchema tLACommisionErrorSchema = 
//	        			   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_HASPROVIDED, cOperator);
//	        	   tLACommisionErrorSet.add(tLACommisionErrorSchema);
//	               continue;
//	           }
			   //查询业务员相关信息
			   LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tAgentCode);
			   if(null == tLAAgentSchema)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   LCPolSchema tLCPolSchema = new LCPolSchema();
			   tLCPolSchema = mAgentWageCommonFunction.queryLCPol(tPolNo);
			   if(null == tLCPolSchema)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCPOL, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   LCContSchema tLCContSchema = new LCContSchema();
			   tLCContSchema = mAgentWageCommonFunction.queryLCCont(tContNo);
			   if(null == tLCContSchema)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCCONT, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   String tAgentGroup = tLAAgentSchema.getAgentGroup();
			   LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
			   tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tAgentGroup);
			   if(null == tLABranchGroupSchema)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   //团队编码 
			   String tBranchCode = tLAAgentSchema.getBranchCode();
			   LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
			   tLABranchGroupDB.setAgentGroup(tBranchCode);
			   tLABranchGroupDB.getInfo();
			   String tBranchAttr = tLABranchGroupDB.getBranchAttr();
			   if(null == tBranchAttr || "".equals(tBranchAttr))
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_BRANCHATTR, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   
			   //对于互动的处理
			   String tSaleChnl = tLCContSchema.getSaleChnl();
			   String tBranchType3="";
			   //对于中介机构的校验
			   if("03".equals(tSaleChnl))
			   {
				   LAComSchema tLAComSchema = new LAComSchema();
				   tLAComSchema = mAgentWageCommonFunction.querytLACom(tLJAGetEndorseSchema.getAgentCom());
				   if(null == tLAComSchema)
				   {
					   LACommisionErrorSchema tLACommisionErrorSchema = 
							   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCOM, cOperator);
					   tLACommisionErrorSet.add(tLACommisionErrorSchema);
					   continue;
				   }
				   
			   }
			   if("15".equals(tSaleChnl)||"14".equals(tSaleChnl))
			   {
				   String tCrs_SaleChnl = tLCContSchema.getCrs_SaleChnl();
		           String tCrs_BussType = tLCContSchema.getCrs_BussType();
		           tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
		           
			   }
			   if("no".equals(tBranchType3))
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema  = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_ACTIVE, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   String [] tBranchTypes = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
			   if(null == tBranchTypes || 0 == tBranchTypes.length)
			   {
				   LACommisionErrorSchema tLACommisionErrorSchema = 
						   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_SALECHNL, cOperator);
				   tLACommisionErrorSet.add(tLACommisionErrorSchema);
				   continue;
			   }
			   String tBranchType = tBranchTypes[0];
			   String tBranchType2 = tBranchTypes[1];
			   
			   //获取保单续保次数
			   int tRenewCount = mAgentWageCommonFunction.queryLCRnewPol(tPolNo,"polno");
			   
			   //说明LACommisionNewSchema中的riskcode,Agentcom 从ljagetendorse中取，重新赋值
			   tLACommisionNewSchema = mAgentWageCommonFunction.packageContDataOfLACommisionNew(tLCContSchema, tLCPolSchema);
			   String tCommisionSn = mAgentWageCommonFunction.getCommisionSN();
			   tLACommisionNewSchema.setCommisionSN(tCommisionSn);
			   tLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
			   tLACommisionNewSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo()); 
		       tLACommisionNewSchema.setManageCom(tLJAGetEndorseSchema.getManageCom());
		       tLACommisionNewSchema.setRiskCode(tLJAGetEndorseSchema.getRiskCode());
		       tLACommisionNewSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
		       tLACommisionNewSchema.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
		       tLACommisionNewSchema.setBranchType(tBranchType);
		       tLACommisionNewSchema.setBranchType2(tBranchType2);
		       tLACommisionNewSchema.setBranchType3(tBranchType3);
		       tLACommisionNewSchema.setReceiptNo(tLJAGetEndorseSchema.getActuGetNo());
		       tLACommisionNewSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
		       tLACommisionNewSchema.setTEnterAccDate(tLJAGetEndorseSchema.getEnterAccDate());
		       tLACommisionNewSchema.setTConfDate(tLJAGetEndorseSchema.getGetConfirmDate());
		       tLACommisionNewSchema.setTMakeDate(tLJAGetEndorseSchema.getMakeDate());
		       tLACommisionNewSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
		        // setCommDate
		       tLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
		        
		       tLACommisionNewSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
		       tLACommisionNewSchema.setCurPayToDate(tLJAGetEndorseSchema.getGetDate());
		       tLACommisionNewSchema.setTransMoney(0 -java.lang.Math.abs(tLJAGetEndorseSchema.getGetMoney()));
		       tLACommisionNewSchema.setTransStandMoney(0 -java.lang.Math.abs(tLJAGetEndorseSchema.getGetMoney()));
		       tLACommisionNewSchema.setTransType("WT");
			   
		       tLACommisionNewSchema.setCommDire("1");
		       if(tLCContSchema.getSaleChnl().equals("07"))
		       {
		    	   tLACommisionNewSchema.setF1("02");//职团开拓
		    	   tLACommisionNewSchema.setP5( tLACommisionNewSchema.getTransMoney());//职团开拓实收保费
		       }
		       else
		       {
		    	   tLACommisionNewSchema.setF1("01");
		       }
		       tLACommisionNewSchema.setFlag("1");//犹豫期撤件
		       tLACommisionNewSchema.setPayYear(0);
		       tLACommisionNewSchema.setReNewCount(tRenewCount);
		       tLACommisionNewSchema.setAgentType(tLJAGetEndorseSchema.getAgentType());
		       tLACommisionNewSchema.setAgentCode(tLJAGetEndorseSchema.getAgentCode());
		        
		       //caigang 2004-09-22添加，用于提取扫描日期
		       String tScanDate = mAgentWageCommonFunction.getScanDate(tLCPolSchema.getPrtNo()); 
		       tLACommisionNewSchema.setScanDate(tScanDate);
		       //插入branchattr,branchseries,根据laagent表里的branchcode
		       tLACommisionNewSchema.setBranchSeries(tLABranchGroupDB.getBranchSeries());
		       tLACommisionNewSchema.setBranchAttr(tBranchAttr);
		       tLACommisionNewSchema.setAgentGroup(tAgentGroup);
		       tLACommisionNewSchema.setBranchCode(tLAAgentSchema.getBranchCode());
		       
		       //用来判断追加保费的犹豫期退保
		       if(tPayPlanCode.equals(AgentWageOfCodeDescribe.JUDGE_CEASE))
		       {
		    	   tLACommisionNewSchema.setTransState("03"); //追加保费
               }
               else
               {
                   tLACommisionNewSchema.setTransState("00"); //正常保单
               }
		       tLACommisionNewSchema.setTransMoney(0 -java.lang.Math.abs(tLJAGetEndorseSchema.getGetMoney()));
		       tLACommisionNewSchema.setTransStandMoney(0 -java.lang.Math.abs(tLJAGetEndorseSchema.getGetMoney()));
		        
		       tLACommisionNewSchema.setOperator(cOperator);
		       tLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
		       tLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
		       tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
		       tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
		        
		       //先判断犹豫期退保对应的首期保单 是否计算过薪资
		       int tCalNum = mAgentWageCommonFunction.judgeWageIsCal(tPolNo);
		       if(0==tCalNum)
		       {
		    	   System.out.println("首期保单未找到，有错误 ");
		    	   continue;
		       }
		       //薪资已经计算过
		       if(1==tCalNum)
		       {
		    	   //薪资月算法相关字段处理
		    	   //计算薪资月的时候，传的paycount是0，但实际上是1，此处是为了能调用公共的薪资月方法
		    	   tLACommisionNewSchema.setPayCount(0);
		    	   String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLCPolSchema.getGrpContNo(),"WT",tLACommisionNewSchema.getTMakeDate(),
		    			   tLJAGetEndorseSchema.getGetConfirmDate(),tLJAGetEndorseSchema.getActuGetNo());
		    	   //用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
		    	   String tAFlag =tConfDates[0];
		    	   //财务确认日期 ，团单会用到此字段
		    	   String tConfDate = tConfDates[1];
		    	   String tVisitTime  = mAgentWageCommonFunction.queryReturnVisit(tContNo);
		    	   String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
		    	   String tFlag = "02";
		    	   //根据薪资月函数 得到薪资月
		    	   String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, tFlag, tAFlag, tVisitTime, tRiskFlag);
		    	   if(!"".equals(tCalDate)&&null!=tCalDate)
		    	   {
		    		   tLACommisionNewSchema.setCalDate(tCalDate);
		    		   String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate).substring(0,6);
		    		   tLACommisionNewSchema.setWageNo(tWageNo);
		    		   tLACommisionNewSchema.setTConfDate(tConfDate);
		    	   }
		       }
		       //薪资未计算过
		       if(2==tCalNum)
		       {
		    	   LACommisionNewSet tZCLACommisionNewSet = new LACommisionNewSet();
		    	   tZCLACommisionNewSet = mAgentWageCommonFunction.getZCDataOfLACommisionNewSet(tPolNo);
		    	   tLACommisionNewSchema.setCommDire("2");
		    	   tLACommisionNewSchema.setCalDate(tZCLACommisionNewSet.get(1).getCalDate());
		    	   tLACommisionNewSchema.setWageNo(tZCLACommisionNewSet.get(1).getWageNo());
		    	   tUpDateLACommisionNewSet.add(tZCLACommisionNewSet);
		       }
		       tLACommisionNewSchema.setPayCount(1);
		       //拆分万能险基本保费和额外保费，税优产品的账户保费和风险保费
		       LACommisionNewSet tExtractLACommisionNewSet = new LACommisionNewSet();
               tExtractLACommisionNewSet = mAgentWageCommonFunction.dealExtractData(tLACommisionNewSchema, tLCPolSchema, "WT",tLJAGetEndorseSchema.getEndorsementNo());
               if(0>=tExtractLACommisionNewSet.size())
               {
            	   tLACommisionNewSet.add(tLACommisionNewSchema);
               }
               else
               {
            	   tLACommisionNewSet.add(tExtractLACommisionNewSet);
               }
		   }
		   if(0<tLACommisionNewSet.size())
		   {
				tMMap.put(tLACommisionNewSet, "INSERT");
		   }
		   if(0<tLACommisionErrorSet.size())
		   {
				tMMap.put(tLACommisionErrorSet, "INSERT");
		   }
		   if(0<tUpDateLACommisionNewSet.size())
		   {
			   tMMap.put(tUpDateLACommisionNewSet, "UPDATE");
		   }
		   if(0==tMMap.size())
		   {
			   continue;
		   }
		   if(!mAgentWageCommonFunction.dealDataToTable(tMMap))
		   {
			   // @@错误处理
			   CError tError = new CError();
			   tError.moduleName = "AgentWageOfLJAGetEndorseDataWT";
	           tError.functionName = "PubSubmit";
	           tError.errorMessage = "数据插入到LACommisionNew中失败!";
	           this.mErrors.addOneError(tError);
	           return false;
		   }
		} while (tLJAGetEndorseSet.size() > 0);
		System.out.println("AgentWageOfLJAGetEndorseDataWT-->dealData:执行结束 ");
		return true;
	}
}
