package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.db.LAChargeLogDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.utility.ExeSQL;

public class BankChargeGatherNewBL {
    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public CErrors mErrors = new CErrors();
    private LAChargeSet mLAChargeSet = null;
    private LAChargeBSet mLAChargeBSet = null;
    private LAWageHistorySet mLAWageHistorySet = null;
    private LARollBackTraceSet mLARollBackTraceSet = new LARollBackTraceSet();
    private LAChargeLogSchema mLAChargeLogSchema = new LAChargeLogSchema();
    private LAChargeLogSet mUPLAChargeLogSet = new LAChargeLogSet();
    private LAChargeLogSet mDELLAChargeLogSet = new LAChargeLogSet();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mCalType = "21";
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mManageCom;
    private String mBranchType;
    private String mBranchType2;
    private String mEndDate;
    private String mChargeCalNo;
    //获得存储号码
    private String mNewEdorNo = "";
    private MMap mMap = new MMap();

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Start BankChargeGatherNewBL submit...");
        mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData()) {
            if (mErrors.needDealError()) {
                System.out.println("程序异常结束原因：" + mErrors.getFirstError());
            }
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentChargeCalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;

    }

    private boolean getInputData(VData cInputData) {
        mLAChargeLogSchema = ((LAChargeLogSchema) cInputData.
                              getObjectByObjectName(
                                      "LAChargeLogSchema", 0));
        mGlobalInput = ((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mLAChargeLogSchema == null || mGlobalInput == null) {
            CError tError = new CError();
            tError.moduleName = "AgentChargeCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mManageCom = mLAChargeLogSchema.getManageCom();
        this.mBranchType = mLAChargeLogSchema.getBranchType();
        this.mBranchType2 = mLAChargeLogSchema.getBranchType2();
        this.mEndDate = mLAChargeLogSchema.getEndDate();
        return true;
    }

    private boolean prepareOutputData() {

        try {
            mMap.put(this.mLAChargeSet, "DELETE");
            mMap.put(this.mLAChargeBSet, "INSERT");
            mMap.put(this.mLARollBackTraceSet, "INSERT");
            mMap.put(this.mDELLAChargeLogSet, "DELETE");
            mMap.put(this.mUPLAChargeLogSet, "UPDATE");
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /**
     * 进行数据库操作
     * @param pmVData VData
     * @param pmOperate String
     * @return boolean
     */
    private boolean dealData() {

    	System.out.println("~~~~~~~~~~开始手续费业务处理!");
    	//    	获取手续费计算月份的止期是否已经进行过提数计算          
        LAWageLogDB tLAWageLogDB = new LAWageLogDB();
        LAWageLogSet tLAWageLogSet = new LAWageLogSet();
        tLAWageLogDB.setManageCom(mManageCom);
        tLAWageLogDB.setBranchType(mBranchType);
        tLAWageLogDB.setBranchType2(mBranchType2);
        tLAWageLogDB.setWageNo(mLAChargeLogSchema.getChargeCalNo());
        tLAWageLogDB.setWageMonth(mLAChargeLogSchema.getChargeMonth());
        tLAWageLogDB.setWageYear(mLAChargeLogSchema.getChargeYear());
        tLAWageLogSet = tLAWageLogDB.query();
        String enddate = tLAWageLogSet.get(1).getEndDate();
        System.out.println("提数止期为：" + enddate);
        System.out.println("回退止期为：" +mEndDate);
        FDate fd = new FDate();
        if (fd.getDate(mEndDate).compareTo(fd.getDate(enddate)) > 0) {
            CError tError = new CError();
            tError.moduleName = "AgentChargeCalBL";
            tError.functionName = "dealData";
            tError.errorMessage = "日期为(" + mEndDate +
                                  ")的展业类型为" +
                                  mLAChargeLogSchema.getBranchType() +
                                  "，渠道" +
                                  mLAChargeLogSchema.getBranchType2() +
                                  "业务数据未提取，不能进行此日期的手续费计算回退！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (fd.getDate(mEndDate).compareTo(fd.getDate(enddate)) == 0
            && !tLAWageLogSet.get(1).getState().equals("11")) {
            CError tError = new CError();
            tError.moduleName = "AgentChargeCalBL";
            tError.functionName = "dealData";
            tError.errorMessage = "日期为(" + mEndDate +
                                  ")的展业类型为" +
                                  mBranchType +
                                  "，渠道" +
                                  mBranchType2 +
                                  "业务数据提取未计算，不能进行此日期的手续费计算回退！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAChargeLogSet tLAChargeLogSet = new LAChargeLogSet();
        LAChargeLogDB tLAChargeLogDB = new LAChargeLogDB();
        String Year = AgentPubFun.formatDate(mEndDate,"yyyy");
        String Month = AgentPubFun.formatDate(mEndDate,"MM");
        mChargeCalNo = Year + Month;
        tLAChargeLogDB.setManageCom(mManageCom);
        tLAChargeLogDB.setChargeCalNo(mChargeCalNo);
        tLAChargeLogDB.setBranchType(mBranchType);
        tLAChargeLogDB.setBranchType2(mBranchType2);
        tLAChargeLogSet = tLAChargeLogDB.query();
        if (tLAChargeLogDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLAChargeLogDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankChargeGatherNewBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询手续费日志纪录存在与否出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAChargeLogSet == null || tLAChargeLogSet.size() < 1) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChargeGatherNewBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要回退的手续费数据！";
            System.out.println("没有查询到需要回退的手续费数据！");
            this.mErrors.addOneError(tError);
            return false;

        }
        //mUPLAChargeLogSet=tLAChargeLogSet;
        //获得转储号码
        mNewEdorNo = PubFun1.CreateMaxNo("EDORNO", 20);
        //处理代理人手续费表和手续费备份表
        if (this.dealCharge() == false) {
            return false;
        }
        //处理手续费历史表
        if (this.dealChargeLog() == false) {
            return false;
        }
        //处理回退记录信息
        dealRollBackTrace();

        return true;
    }

    /**
     * 设置手续费备份的数据
     * @return boolean
     */
    private boolean setLAChargeBSet() {
        //判断待删除的手续费数据是否存在
        if (mLAChargeSet == null || mLAChargeSet.size() < 1) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChargeGatherUI";
            tError.functionName = "setLAChargeSet";
            tError.errorMessage = "没有备份务数据！";
            System.out.println("没有手续费备份务数据！");
            this.mErrors.addOneError(tError);
            return false;
        }
        //取得直接手续费明细表（手续费扎账表）的数据数
        int iMax = mLAChargeSet.size();
        LAChargeBSet tLAChargeBSet = new LAChargeBSet();
        //获得转储号码
        String tNewEdorNo = mNewEdorNo;
        //准备备份数据
        for (int i = 1; i <= iMax; i++) {
            LAChargeSchema tLAChargeSchema = new LAChargeSchema();
            tLAChargeSchema = mLAChargeSet.get(i).getSchema();
            if (tLAChargeSchema != null) {
                LAChargeBSchema tLAChargeBSchema = setLAChargeBSchema(
                        tLAChargeSchema, tNewEdorNo);
                if (tLAChargeBSchema != null) {
                    tLAChargeBSet.add(tLAChargeBSchema);
                }
            }
        }
        //缓存备份数据
        mLAChargeBSet = tLAChargeBSet;

        return true;
    }

    /**
     * 设置备份数据
     * @param pmLAWageSchema LAChargeSchema
     * @param pmEdorNo String
     * @return LAChargeBSchema
     */
    private LAChargeBSchema setLAChargeBSchema(LAChargeSchema pmLAChargeSchema,
                                               String pmEdorNo) {
        LAChargeBSchema tLAChargeBSchema = new LAChargeBSchema();
        Reflections tReflections = new Reflections();

        //设置备份表中和手续费表中相同的项
        tReflections.transFields(tLAChargeBSchema, pmLAChargeSchema);
        //设置转储号码
        tLAChargeBSchema.setEdorNo(pmEdorNo);
        //设置转储类型“手续费回退备份”
        tLAChargeBSchema.setEdorType("02");
        tLAChargeBSchema.setTGatherEndDate(mLAChargeLogSchema.getEndDate());

        return tLAChargeBSchema;
    }

    /**
     * 处理手续费信息到备份表的数据准备
     * @return boolean
     */
    private boolean dealCharge() {
        LAChargeDB tLAChargeDB = new LAChargeDB();
        StringBuffer tSB = new StringBuffer();
        String tSql = "";
        tSB.append("SELECT * FROM LACharge WHERE ")
                .append("ChargeType ='99'  AND ")
                .append("ManageCom LIKE '")
                .append(mManageCom)
                .append("%' AND BranchType = '")
                .append(mBranchType)
                .append("' AND  BranchType2 = '")
                .append(mBranchType2)
                .append("' AND TMakeDate >='")
                .append(mEndDate)
                .append("'")
                ;
        tSql = tSB.toString();
        System.out.println("手续费查询：" + tSql);
        //查询数据库 得到结果
        mLAChargeSet = tLAChargeDB.executeQuery(tSql);

        if (mLAChargeSet == null || mLAChargeSet.size() < 1) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChargeGatherNewBL";
            tError.functionName = "checkWageType";
            tError.errorMessage = "未查询到需要计算回退的手续费数据！";
            System.out.println("未查询到需要计算回退的手续费数据！");
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("找到 " + mLAChargeSet.size() + " 条手续费数据！");
        //查询得到需要回退的手续费备份记录
        if (setLAChargeBSet() == false) {
            return false;
        }

        return true;
    }

    /**
     * 处理手续费历史表
     * @return boolean
     */
    private boolean dealChargeLog() {

        LAChargeLogDB tLAChargeLogDB = new LAChargeLogDB();
        LAChargeLogSet tUPLAChargeLogSet = new LAChargeLogSet();
        LAChargeLogSet tDELLAChargeLogSet = new LAChargeLogSet();
        
        String newEndDate = AgentPubFun.formatDate(mEndDate, "yyyy-MM-dd"); //调整日期转换格式
        String tDay = newEndDate.substring(newEndDate.lastIndexOf("-") + 1); //取停业所在月的日期
        if (tDay.equals("01"))
        {
        	String currDay=PubFun.calDate(newEndDate,-1,"D",null);
        	mChargeCalNo=AgentPubFun.formatDate(currDay, "yyyyMM");
        }
        
        StringBuffer tSB = new StringBuffer();
        String tSql = "";
        tSB.append("SELECT * FROM LAChargeLog WHERE ")
                .append("ManageCom LIKE '")
                .append(mManageCom)
                .append("%' AND BranchType = '")
                .append(mBranchType)
                .append("' AND  BranchType2 = '")
                .append(mBranchType2)
                .append("' AND  ChargeCalNo >'")
                .append(mChargeCalNo)
                .append("'")
                ;
        tSql = tSB.toString();
        System.out.println("手续费查询：" + tSql);
        //查询数据库 得到结果
        tDELLAChargeLogSet = tLAChargeLogDB.executeQuery(tSql);
        if (tLAChargeLogDB.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChargeGatherUI";
            tError.functionName = "dealChargeLog";
            tError.errorMessage = "检索不到手续费历史纪录！";
            System.out.println("检索不到手续费历史纪录！");
            this.mErrors.addOneError(tError);

            return false;
        }
        if (tDELLAChargeLogSet != null && tDELLAChargeLogSet.size() > 0) {
            mDELLAChargeLogSet = tDELLAChargeLogSet;
        }
        
        if (!tDay.equals("01")){
        	LAChargeLogSet tLAChargeLogSet = new LAChargeLogSet();
            LAChargeLogDB ttLAChargeLogDB = new LAChargeLogDB();
            String Year = AgentPubFun.formatDate(mEndDate,"yyyy");
            String Month = AgentPubFun.formatDate(mEndDate,"MM");
            mChargeCalNo = Year + Month;
            ttLAChargeLogDB.setManageCom(mManageCom);
            ttLAChargeLogDB.setChargeCalNo(mChargeCalNo);
            ttLAChargeLogDB.setBranchType(mBranchType);
            ttLAChargeLogDB.setBranchType2(mBranchType2);
            tLAChargeLogSet = ttLAChargeLogDB.query();
            
        for (int i = 1; i <= tLAChargeLogSet.size(); i++) {
            LAChargeLogSchema tLAChargeLogSchema = new LAChargeLogSchema();
            tLAChargeLogSchema = tLAChargeLogSet.get(i).getSchema();
            String tsql = "select date('" + mEndDate + "')-1 day from dual";
            ExeSQL tExeSQL = new ExeSQL();
            String tEndDate = tExeSQL.getOneValue(tsql);
            tLAChargeLogSchema.setEndDate(tEndDate);
            tUPLAChargeLogSet.add(tLAChargeLogSchema);
        }
        mUPLAChargeLogSet = tUPLAChargeLogSet;
        }
        return true;
    }

    /**
     * 进行回退记录处理
     */
    private void dealRollBackTrace() {
        //条件划分符号
        String tSign = "&";
        //回退序号
        String tIdx = PubFun1.CreateMaxNo("IDX", 20);
        //回退条件
        String tTrem = "手续费回退年月:" + mLAChargeLogSchema.getChargeCalNo();
        tTrem += tSign + "管理机构:" + mManageCom;
        tTrem += tSign + "展业机构:" + mBranchType;
        tTrem += tSign + "展业渠道:" + mBranchType2;

        LARollBackTraceSchema tLARollBackTraceSchema = new
                LARollBackTraceSchema();
        tLARollBackTraceSchema.setIdx(tIdx);
        tLARollBackTraceSchema.setEdorNo(mNewEdorNo);
        tLARollBackTraceSchema.setoperator(mGlobalInput.Operator);
        tLARollBackTraceSchema.setstate("0");
        tLARollBackTraceSchema.setConditions(tTrem);
        tLARollBackTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLARollBackTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLARollBackTraceSchema.setRollBackType("09");

        //缓存回退记录
        mLARollBackTraceSet.add(tLARollBackTraceSchema);
    }


}
