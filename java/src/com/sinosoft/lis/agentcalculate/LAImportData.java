package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.reinsure.LRListingForeastDataBL;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAssessSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.lis.vschema.LRAccountSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:保单查询功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class LAImportData
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
	private MMap mMap = new MMap();	
	private VData mOutputData = new VData();
    private Connection con;
    
    public LAImportData()
    {}

    /**
       传输数据的公共方法
     * @throws SQLException 
     */
    public boolean dealData() {
//		String tSQL = "select * from labranchgroup where branchattr in(select distinct branchattr  from lacommision where branchtype = '6' and branchtype2 = '01' and ManageCom like '86%' and WageNo = '201610')";
//        String tSQL = "select * from latreeb where managecom like '8613%' and  branchtype = '2' and branchtype2 = '02' with ur"; 
//      String tSQL = "select * from laagent where managecom like '86440700%' and  branchtype = '2' and branchtype2 = '02' with ur";	
		String tSQL = "select *  from lacommision where branchtype = '2' and branchtype2 = '02' and wageno = '201611' and managecom = '86440700' fetch first 100 rows only with ur"
			;

	    try{   
		         //加载DB2的驱动类   
            	Class.forName("com.ibm.db2.jcc.DB2Driver");   
		    }catch(ClassNotFoundException e){   
		    	System.out.println("找不到驱动程序类 ，加载驱动失败！");   
		    	e.printStackTrace() ;   
		    }   		
		try{
					//创建链接
				con = DriverManager.getConnection("jdbc:db2://10.136.1.5:50000/lis",
						"ghc"
						,"ghc&nzx71");
			}catch(SQLException e){
	            e.printStackTrace();
	            System.out.println("创建连接失败..." + e.getMessage());
	            CError tError = new CError();
	            tError.moduleName = "DBConn";
	            tError.functionName = "createConnection";
	            tError.errorMessage = "Connect failed!  error code =" +
	                                  e.getErrorCode();
	            this.mErrors.addOneError(tError);
	
	            return false;
		};
		RSWrapper tRSWrapper = new RSWrapper(con);
//		LAAgentSet tSet = new LAAgentSet();
//		LATreeBSet tSet = new LATreeBSet();
//		LABranchGroupSet tSet = new LABranchGroupSet();
//		LATreeBSet tSet = new LATreeBSet();
		LACommisionSet tSet = new LACommisionSet();
		tRSWrapper.prepareData(tSet, tSQL);
		do{
			tSet = (LACommisionSet)tRSWrapper.getData();
			mMap = new MMap();
			mMap.put(tSet, "INSERT");
			this.mOutputData.clear();
            this.mOutputData.add(this.mMap);
            PubSubmit tPubSubmit = new PubSubmit();
   	     	if(!tPubSubmit.submitData(mOutputData, "")){
   	    	 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LRListingForeastDateBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
   	     	}
		}while(tSet.size()>0);
		return true;
    }
    
	public static void main(String[] args) {
		LAImportData tLAImportData = new LAImportData();
		boolean m = tLAImportData.dealData();
		System.out.println(m);
}

}