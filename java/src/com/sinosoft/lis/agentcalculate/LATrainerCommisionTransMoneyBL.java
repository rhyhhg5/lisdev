package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATrainerWageHistoryDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAIndirectWageSchema;
import com.sinosoft.lis.schema.LATrainerWageHistorySchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAIndirectWageSet;
import com.sinosoft.lis.vschema.LATrainerWageHistorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p> 
 *
 * @author caigang
 * @version 1.0
 */
public class LATrainerCommisionTransMoneyBL {		
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private String mOperate;
    private LAIndirectWageSet mLAIndirectWageSchemaSet = new LAIndirectWageSet();
	private LATrainerWageHistorySet mLaTrainerWageHistorySet = new LATrainerWageHistorySet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    private String mManageCom="";
    private String mStartDate="";
    private String mEndDate="";
    private String mWageNo="";
    private int Error=0;

    public LATrainerCommisionTransMoneyBL() {
        try {
        } catch (Exception ex) {
            ex.printStackTrace();
        }	
    }    
    public boolean dealData(VData cInputData) {
   	    this.mManageCom=(String)cInputData.get(0);
	    this.mStartDate=(String)cInputData.get(1);
	    this.mEndDate=(String)cInputData.get(2);
	    this.mWageNo=(String)cInputData.get(3);
        //先按天循环
	    String startDate1[]=mStartDate.split("\\-");
        String endDate1[]=mEndDate.split("\\-");
        int startDateNumber = Integer.parseInt(startDate1[2]);
        int endDateNumber = Integer.parseInt(endDate1[2]); 
        int MaxNumber = endDateNumber-startDateNumber;
	    System.out.println("管理机构:"+mManageCom+"提数始期:"+mStartDate+"提数止期:"+mEndDate);
        System.out.println("需要提取"+(MaxNumber+1)+"天的数据");
        String sqlString="";
        // 查询该管理机构下所有八位管理机构
 		String manageComSql = "select comcode from ldcom where comcode like '"+mManageCom+"%' and char(length(trim(comcode)))='8' and sign='1'  order by comcode ";
		System.out.println("个险组训新单折标批处理ldcom运维提数sql：" + manageComSql);
 		SSRS tSSRS = new SSRS();
 		ExeSQL quExeSQLSQL = new ExeSQL();
 		tSSRS = quExeSQLSQL.execSQL(manageComSql);
        for(int i=0;i<=MaxNumber;i++){
        	for (int a = 1; a <= tSSRS.getMaxRow(); a++) {
    			String tManageCom = tSSRS.GetText(a, 1);
        	    sqlString="select CommisionSN from lacommision where branchtype = '1' and branchtype2 = '01' and payyear='0'"
        			+ "and tmakedate=(date('"+mStartDate+"')+"+i+" day) "
        	        +"and managecom ='"
					+ tManageCom
					+ "' and (commdire='1' or (commdire='2' and transtype = 'WT')) order by CommisionSN with ur";
			System.out.println("个险组训新单折标批处理lacommision运维提数sql：" + sqlString);
			LACommisionSet tLaCommisionSet = new LACommisionSet();
			RSWrapper tRSWrapper = new RSWrapper();
			tRSWrapper.prepareData(tLaCommisionSet, sqlString);
			boolean tQueryFlag = false;
			do {
				tRSWrapper.getData();
				if (!tQueryFlag) {
					if (tLaCommisionSet == null || tLaCommisionSet.size() == 0) {
						System.out.println("LACommisionTransMoneyTask-->dealData没有满足条件 的数据");
						break;
					}
				}
				dealSet(tLaCommisionSet, tManageCom);

			} while (tLaCommisionSet.size() > 0);
			InsertHistory(tManageCom,mWageNo);
        }
        }
		System.out.println("个险营业部组训新单期缴标准保费--->提数结束！");
        if(Error>0){
        	return false;
        }
        return true;
    }
    public boolean dealWageNoData(VData cInputData) {	
	    this.mWageNo=(String)cInputData.get(0);
	        String sqlString="";
        // 查询该薪资月下所有数据
 		if(mWageNo.equals("0")){
    	    sqlString="select CommisionSN from lacommision where branchtype = '1' and branchtype2 = '01' and payyear='0'"
        	        +"and (wageno ='' or wageno is null) and tmakedate<'2018-06-01' "
					+ "and (commdire='1' or (commdire='2' and transtype = 'WT')) order by CommisionSN with ur";

	    }else{
        	    sqlString="select CommisionSN from lacommision where branchtype = '1' and branchtype2 = '01' and payyear='0'"
        	        +"and wageno ='"
					+ mWageNo
					+ "' and tmakedate<'2018-06-01'  and (commdire='1' or (commdire='2' and transtype = 'WT')) order by CommisionSN with ur";
	    }
 		
        	System.out.println("个险组训新单折标批处理lacommision运维提数sql：" + sqlString);
			LACommisionSet tLaCommisionSet = new LACommisionSet();
			RSWrapper tRSWrapper = new RSWrapper();
			tRSWrapper.prepareData(tLaCommisionSet, sqlString);
			boolean tQueryFlag = false;
			do {
				tRSWrapper.getData();
				if (!tQueryFlag) {
					if (tLaCommisionSet == null || tLaCommisionSet.size() == 0) {
						System.out.println("LACommisionTransMoneyTask-->dealData没有满足条件 的数据");
						break;
					}
				}
				dealSet(tLaCommisionSet, "");

			} while (tLaCommisionSet.size() > 0);
        
		System.out.println("个险营业部组训新单期缴标准保费--->提数结束！");
        if(Error>0){
        	return false;
        }
        return true;
    }

	private void dealSet(LACommisionSet laCommisionSet, String tManagecom) {
		for (int i = 1; i <= laCommisionSet.size(); i++) {
			LACommisionSchema tLACommisionSchema = new LACommisionSchema();
			LAIndirectWageSchema mLAIndirectWageSchema = new LAIndirectWageSchema();
			LACommisionDB tLACommisionDB = new LACommisionDB();
			tLACommisionDB.setCommisionSN(laCommisionSet.get(i)
					.getCommisionSN());
			tLACommisionDB.getInfo();
			tLACommisionSchema = tLACommisionDB.getSchema();
			String cCommisionSN = tLACommisionSchema.getCommisionSN();
			String cRiskCode = tLACommisionSchema.getRiskCode();
			String cTmakeDate = tLACommisionSchema.getTMakeDate();
			String cTransState = tLACommisionSchema.getTransState();
			String cPayIntv = String.valueOf(tLACommisionSchema.getPayIntv());
			String cPayYears = String.valueOf(tLACommisionSchema.getPayYears());

			// 计算团队首期绩效提奖比例
			String strWageRate = this.calCalRateForAll(cRiskCode, cPayIntv,
					cTransState, cPayYears);
			double wageRateValue = 0;
			wageRateValue = Double.parseDouble(strWageRate);
			System.out.println("承保保费折标系数为：" + wageRateValue);
			double tTransMoney = tLACommisionSchema.getTransMoney();
			double tBankG = tTransMoney * wageRateValue;
			System.out.println("折标后的标准承保保费为：" + tBankG);
			// 存入间接薪资项表
			mLAIndirectWageSchema.setCommisionSN(cCommisionSN);
			mLAIndirectWageSchema.setGrpRate(wageRateValue);
			// 新单折标保费
			mLAIndirectWageSchema.setF1(tBankG);
			mLAIndirectWageSchema.setBranchType("1");
			mLAIndirectWageSchema.setBranchType2("01");
			mLAIndirectWageSchema.setOperator("it001");
			mLAIndirectWageSchema.setTMakeDate(cTmakeDate);
			mLAIndirectWageSchema.setMakeDate(currentDate);
			mLAIndirectWageSchema.setMakeTime(currentTime);
			mLAIndirectWageSchema.setModifyDate(currentDate);
			mLAIndirectWageSchema.setModifyTime(currentTime);
			mLAIndirectWageSchemaSet.add(mLAIndirectWageSchema);
		}
		if (mLAIndirectWageSchemaSet.size() == 0) {
			System.out.println("管理机构:" + tManagecom
					+ "LAIndirectWageSchemaSet没有要插入的数据");
			return;
		}
		mMap.keySet().clear();
		mMap.put(this.mLAIndirectWageSchemaSet, "INSERT");
		this.mInputData.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start AgentWageCalSaveNewBL Submit...");
		try {
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				this.Error=Error+1;
				System.out.println("组训新单保费折标计算，管理机构" + tManagecom + "插入失败！");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "AgentWageCalSaveNewBL";
				tError.functionName = "submitData";
				tError.errorMessage = "管理机构：" + tManagecom + "数据提交失败!";
				this.mErrors.addOneError(tError);
				return;
			}
			mLAIndirectWageSchemaSet.clear();
		} catch (Exception ex) {
			this.Error=Error+1;
			ex.printStackTrace();
		}

	}
	private void InsertHistory(String tManagecom ,String tWageNo) {
		// 插入日志表
		String flag = "INSERT";
		LATrainerWageHistoryDB tLATrainerWageHistoryDB = new LATrainerWageHistoryDB();
		LATrainerWageHistorySchema tLATrainerWageHistorySchema = new LATrainerWageHistorySchema();
		tLATrainerWageHistoryDB.setManageCom(tManagecom);
		tLATrainerWageHistoryDB.setAClass("03");
		tLATrainerWageHistoryDB.setBranchType("1");
		tLATrainerWageHistoryDB.setBranchType2("01");
		tLATrainerWageHistoryDB.setWageNo(tWageNo);

		if (tLATrainerWageHistoryDB.getInfo()) {
			tLATrainerWageHistorySchema = tLATrainerWageHistoryDB.getSchema();
			flag = "UPDATE";
		} else {

			tLATrainerWageHistorySchema.setWageNo(tWageNo);
			tLATrainerWageHistorySchema.setAClass("03");
			tLATrainerWageHistorySchema.setManageCom(tManagecom);
			tLATrainerWageHistorySchema.setBranchType("1");
			tLATrainerWageHistorySchema.setBranchType2("01");
			tLATrainerWageHistorySchema.setMakeDate(currentDate);
			tLATrainerWageHistorySchema.setMakeTime(currentTime);
			flag = "INSERT";
		}
		tLATrainerWageHistorySchema.setState("00");
		tLATrainerWageHistorySchema.setModifyDate(currentDate);
		tLATrainerWageHistorySchema.setModifyTime(currentTime);
		tLATrainerWageHistorySchema.setOperator("it001");
		mLaTrainerWageHistorySet.clear();
		mLaTrainerWageHistorySet.add(tLATrainerWageHistorySchema);
		mMap.keySet().clear();
		mMap.put(this.mLaTrainerWageHistorySet, flag);
		this.mInputData.add(mMap);
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start AgentWageCalSaveNewBL Submit...");
		try {
			if (!tPubSubmit.submitData(mInputData, mOperate)) {
				this.Error=Error+1;
				System.out.println("组训新单保费折标计算，管理机构" + tManagecom + "插入失败！");
				// @@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError = new CError();
				tError.moduleName = "AgentWageCalSaveNewBL";
				tError.functionName = "submitData";
				tError.errorMessage = "管理机构：" + tManagecom + "数据提交失败!";
				this.mErrors.addOneError(tError);
				return;
			}
		} catch (Exception ex) {
			this.Error=Error+1;
			ex.printStackTrace();
		}
	}
	// 新单期缴标准保费折标系数提取函数
	private String calCalRateForAll(String cRiskCode, String cPayIntv,
			String cTransState,String cPayYears) {
		String tRiskCode = cRiskCode == null ? "" : cRiskCode;
		String tPayIntv = cPayIntv == null ? "" : cPayIntv;
		String tTransState = cTransState == null ? "" : cTransState;
		String tPayYears = cPayYears == null ? "" : cPayYears;

		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("TMRate");
		tCalculator.addBasicFactor("RiskCode", tRiskCode);
		tCalculator.addBasicFactor("PayIntv", tPayIntv);
		tCalculator.addBasicFactor("TransState", tTransState);
		tCalculator.addBasicFactor("PayYears", tPayYears);
		String tResult = tCalculator.calculate();
		return tResult == null ? "" : tResult;
	}
   
}
