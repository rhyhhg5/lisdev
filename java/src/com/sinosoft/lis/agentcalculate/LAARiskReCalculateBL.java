package com.sinosoft.lis.agentcalculate;

import java.math.BigDecimal;

import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeFeeSchema;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LAChargeFeeSet;
import com.sinosoft.lis.vschema.LAChargeLogSet;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LAACReCalculateBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author 
 * @version 1.0----2009-1-13
 */
public class LAARiskReCalculateBL {
  //错误处理类
	public CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */ 
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAChargeSet mLAChargeSet = new LAChargeSet();
  private LAChargeSet mUpLAChargeSet = new LAChargeSet();
  /** 业务处理相关变量 */
  private LAChargeLogSet mLastLAChargeLogSet = new LAChargeLogSet();
  private LAChargeFeeSet mLAChargeFeeSet = new LAChargeFeeSet();
  LACommisionSet tLACommisionSet = new LACommisionSet();
  LACommisionSchema tLACommisionSchema = new LACommisionSchema();
  private String mManageCom="";
  private String mAgentCom="";
  private String mStartDate="";
  private String mEndDate="";
  private String mGrpContNo="";
  private String mCardNo="";
  private String mWrapCode="";
 // private String mContType="";
  private String mContFlag="";
  private String mRiskCode="";
  private double cSumChargeRate=0;
  /**手续费类型*/
  private String mChargeType="ChargeCalFactor";
  
  public LAARiskReCalculateBL() {

  }

//	public static void main(String[] args)
//	{
//		LaratecommisionSetBL LaratecommisionSetbl = new LaratecommisionSetBL();
//	}
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    if(!check()){
    	return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();

    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAARiskReCalculateBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      this.mErrors.clearErrors();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     if(this.mOperate.equals("SELECTREPAY")){
    	 this.mLAChargeSet.set( (LAChargeSet) cInputData.getObjectByObjectName("LAChargeSet",0));
         System.out.println("LAChargeSet get"+mLAChargeSet.size());
     }
     else if(this.mOperate.equals("ALLREPAY")){
    	 this.mManageCom=(String)cInputData.get(1);
    	 this.mAgentCom=(String)cInputData.get(2);
    	 this.mStartDate=(String)cInputData.get(3);
    	 this.mEndDate=(String)cInputData.get(4);
    	 this.mGrpContNo=(String)cInputData.get(5);
    	 this.mCardNo=(String)cInputData.get(6);
    	 this.mWrapCode=(String)cInputData.get(7);
    	 this.mContFlag=(String)cInputData.get(8);
     }
     else{
    	 CError tError = new CError();
         tError.moduleName = "LAARiskReCalculateBL";
         tError.functionName = "getInputData";
         tError.errorMessage = "不支持的操作类型,请验证操作类型.";
         this.mErrors.addOneError(tError);
         return false;
     }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAARiskReCalculateBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }
  /**
   * 新增，修改传入的数据校验
   */
  private boolean check(){
	  //不需要数据校验
	  return true;
  }
  /**
   * 业务处理主函数
   */
  private boolean dealData() {

	  System.out.println("Begin LAARiskReCalculateBL.dealData........."+mOperate);
    try {
    	double cSumCharge = 0;
    	LAChargeDB tLAChargeDB=new LAChargeDB();
    	LAChargeSchema tLAChargeSchema = new LAChargeSchema();
    	LAChargeSet tLAChargeSet = new LAChargeSet();
    	LACommisionDB tLACommisionDB = new LACommisionDB();
    	System.out.println(mLAChargeSet.size());
      if (mOperate.equals("SELECTREPAY")) {
     // LAChargeSet tLAChargeSet ;
     // String tAgentCom1="";
    //  tLAChargeSet=new LAChargeSet();
      for (int i = 1; i <= mLAChargeSet.size(); i++) {
    	  System.out.println(mLAChargeSet.size()+"hahaha");
       String commisionsn = mLAChargeSet.get(i).getCommisionSN();
       tLAChargeSet = (LAChargeSet)tLAChargeDB.executeQuery("select * from lacharge where commisionsn ='"+commisionsn+"'");
        if(tLAChargeSet.size()>0){
        	for (int j = 1; j <= tLAChargeSet.size(); j++) {
        		tLAChargeSchema = tLAChargeSet.get(j);
        	    String leSQL = "select * from lacommision where commisionsn='"+commisionsn+"'";
                tLACommisionSet = tLACommisionDB.executeQuery(leSQL);
                for(int m =1;m<=tLACommisionSet.size();m++){
              	  tLACommisionSchema = tLACommisionSet.get(m).getSchema();
              	   cSumCharge =calculate(tLACommisionSchema);
                }
                tLAChargeSchema.setCharge(cSumCharge);
                tLAChargeSchema.setChargeRate(cSumChargeRate);
                tLAChargeSchema.setModifyDate(CurrentDate);
                tLAChargeSchema.setModifyTime(CurrentTime);
                tLAChargeSchema.setCommisionSN(commisionsn);
                mUpLAChargeSet.add(tLAChargeSchema);
			}
        }
    
        
        }
 //     mLAChargeSet.add(tLAChargeSchema);
//      if (mOperate.equals("SELECTREPAY")) {
//        LAChargeSet tLAChargeSet ;     
//        for (int i = 1; i <= mLAChargeSet.size(); i++) {
//          tLAChargeSet=new LAChargeSet();          
//          String tSQL="select * from lacharge where chargetype='"
//        	  +mLAChargeSet.get(i).getChargeType()+"' and branchtype='"
//        	  +mLAChargeSet.get(i).getBranchType()+"' and branchtype2='"
//        	  +mLAChargeSet.get(i).getBranchType2()+"' and chargestate='"
//        	  +mLAChargeSet.get(i).getChargeState()+"' and managecom='"
//        	  +mLAChargeSet.get(i).getManageCom()+"' and agentcom='"
//        	  +mLAChargeSet.get(i).getAgentCom()+"' and contno='"
//        	  +mLAChargeSet.get(i).getContNo()+"' and riskcode='"
//        	  +mLAChargeSet.get(i).getRiskCode()+"' and tmakedate='"
//        	  +mLAChargeSet.get(i).getTMakeDate()+"' and receiptno='"
//        	  +mLAChargeSet.get(i).getReceiptNo()+"' and transtype='"
//        	  +mLAChargeSet.get(i).getTransType()+"' with ur";
//          tLAChargeSet=tLAChargeDB.executeQuery(tSQL);
//          tLAChargeSchema = tLAChargeSet.get(i).getSchema();
//          String commision = tLAChargeSchema.getCommisionSN();
//        
//          LACommisionDB tLACommisionDB = new LACommisionDB();
//          String leSQL = "select * from lacommision where commisionsn='"+commision+"'";
//          tLACommisionSet = tLACommisionDB.executeQuery(leSQL);
//          for(int m =1;m<tLACommisionSet.size();m++){
//        	  tLACommisionSchema = tLACommisionSet.get(m).getSchema();
//        	  String tAgentCom = tLACommisionSchema.getAgentCom();
//        	   cSumCharge =calculate(tLACommisionSchema,tAgentCom.equals(tAgentCom1));
//          }
//          tLAChargeSchema.setCharge(cSumCharge);
//          tLAChargeSchema.setChargeRate(0);
//          tLAChargeSchema.setModifyDate(CurrentDate);
//          tLAChargeSchema.setModifyTime(CurrentTime);
//          String laSql = " select * from lachargedetail where commisionsn='"
//        		  +commision+"' and branchtype='"
//        		   +tLAChargeSchema.getBranchType()+"' and branchtype2='"
//        		    +tLAChargeSchema.getBranchType2()+"' with ur";
//          tLAChargeDetailSet = tLAChargeDetailDB.executeQuery(laSql);
//          for(i=1; i<=tLAChargeDetailSet.size();i++){
//        	  mLAChargeDetailSchema = tLAChargeDetailSet.get(i);
//        	  mLAChargeDetailSchema.setModifyDate(CurrentDate);
//        	  mLAChargeDetailSchema.setModifyTime(CurrentTime);
//        	  mLAChargeDetailSchema.setChargeRate(sumcharge);
//        	  mLAChargeDetailSchema.setCharge(cSumCharge);
//        	  ml
//        	  
//          }
        
                  
         
          
          
//        //数据提取,根据TMAKEDATE查找
//          SSRS commSSRS = new SSRS();
//          commSSRS = queryLAcommision();
//          String tAgentCom1 ="";
//          for (int k = 1; k <= commSSRS.getMaxRow(); k++) {
//              LACommisionSchema tLACommisionSchema = new LACommisionSchema();
//              LACommisionDB tLACommisionDB = new LACommisionDB();
//              tLACommisionDB.setCommisionSN(commSSRS.GetText(i, 1));
//              tLACommisionDB.getInfo();
//              tLACommisionSchema = tLACommisionDB.getSchema();
//              String tAgentCom = tLACommisionSchema.getAgentCom();
//              LAChargeSchema cLAChargeSchema = new LAChargeSchema();
//              tLAChargeSchema.setAgentCom(tLACommisionSchema.getAgentCom());
//              tLAChargeSchema.setBranchType(tLACommisionSchema.getBranchType());
//              tLAChargeSchema.setBranchType2(tLACommisionSchema.getBranchType2());
//              tLAChargeSchema.setCalDate(tLACommisionSchema.getCalDate());
//              tLAChargeSchema.setCommisionSN(tLACommisionSchema.getCommisionSN());
//              tLAChargeSchema.setChargeType("15"); //互动中介的手续费类型
//              tLAChargeSchema.setContNo(tLACommisionSchema.getContNo());
//              tLAChargeSchema.setGrpContNo(tLACommisionSchema.getGrpContNo());
//              tLAChargeSchema.setPolNo(tLACommisionSchema.getPolNo());
//              tLAChargeSchema.setGrpPolNo(tLACommisionSchema.getGrpPolNo());
//              tLAChargeSchema.setManageCom(tLACommisionSchema.getManageCom());
//              tLAChargeSchema.setMainPolNo(tLACommisionSchema.getMainPolNo());
//              tLAChargeSchema.setPayCount(tLACommisionSchema.getPayCount());
//              tLAChargeSchema.setTransMoney(tLACommisionSchema.getTransMoney()); //实收保费
//              tLAChargeSchema.setTransType(tLACommisionSchema.getTransType());
//              tLAChargeSchema.setRiskCode(tLACommisionSchema.getRiskCode());
//              tLAChargeSchema.setReceiptNo(tLACommisionSchema.getReceiptNo());
//              tLAChargeSchema.setWageNo(tLACommisionSchema.getWageNo());
//              tLAChargeSchema.setChargeState("0");
//              double cSumCharge =calculate(tLACommisionSchema,tAgentCom.equals(tAgentCom1));
//              if(!tAgentCom.equals(tAgentCom1))
//              {
//              	tAgentCom1 = tAgentCom;
//              }
//              //进行手续费比例查找
//              tLAChargeSchema.setCharge(cSumCharge);
//              tLAChargeSchema.setChargeRate(0);
//              tLAChargeSchema.setMakeDate(CurrentDate);
//              tLAChargeSchema.setMakeTime(CurrentTime);
//              tLAChargeSchema.setModifyDate(CurrentDate);
//              tLAChargeSchema.setModifyTime(CurrentTime);
//              tLAChargeSchema.setOperator(mGlobalInput.Operator);
//              tLAChargeSchema.setPayIntv(tLACommisionSchema.getPayIntv());
//              tLAChargeSchema.setTMakeDate(tLACommisionSchema.getTMakeDate());
//              //手续遇计算时，默认批次号为20个0
////              String tBatchNo="00000000000000000000";
//              
////              tLAChargeSchema.setBatchNo(tBatchNo);
//              mLAChargeSet.add(tLAChargeSchema);
//            //  String tNewEdorNo = mNewEdorNo;
//              
//          } 
//          
          
//        //  double cSumCharge =calculate(tLACommisionSchema,tAgentCom.equals(tAgentCom1));
//          for(int j=1;j<=tLAChargeSet.size();j++){
//        	  String mSQL="select calcode from lawagecalelement "
//          		+" where branchtype='1' and branchtype2='02'"
//          		+" and riskcode='"+tLAChargeSet.get(j).getRiskCode()+"' and caltype='21' ";
//        	  
//          	 ExeSQL  mExeSQL = new ExeSQL();
//          	 mCalcode = mExeSQL.getOneValue(mSQL);
//          	 if(mCalcode == null || "".equals(mCalcode)){
//          		 mCalcode ="AR9901";
//          	 }
//          	  String chargeSql = "select code,codealias,othersign from ldcode where codetype ='"+this.mChargeType+"'";
//              SSRS tSSRS1 = new ExeSQL().execSQL(chargeSql);
////              for(int k=1;k<=3;k++){
////            	  if("1".equals(tSSRS1.GetText(k, 3))){
////            		  mCalcode = "AR9901";
////            	  }
////              if(mCalcode.equals("AR9901")){
////            	  
////            	  
////            	  
////            	  
////              	String updateSql=" update lachargedetail set "
////              		+"chargerate=db2inst1.getarchargerate(char(lachargedetail.branchtype),char(lachargedetail.branchtype2),"
////              		+"char((select riskcode from lacommision where commisionsn =lachargedetail.commisionsn)),char((select agentcom from lacommision where commisionsn = lachargedetail.commisionsn)),char((select contno from lacommision where commisionsn = lachargedetail.commisionsn)),char((select grpcontno from lacommision where commisionsn = lachargedetail.commisionsn)),"
////              	    +"char((select payyear from lacommision where commisionsn=lachargedetail.commisionsn)),char((select payyears from lacommision where commisionsn=lachargedetail.commisionsn)),char('99'),"
////              		+"char((select renewcount from lacommision where commisionsn=lachargedetail.commisionsn)),char((select transstate from lacommision where commisionsn=lachargedetail.commisionsn)),"
////              	    +"char((select payintv from lacommision where commisionsn = lachargedetail.commisionsn)),char((select f3 from lacommision where commisionsn=lachargedetail.commisionsn)),char((select tmakedate from lacommision where commisionsn = lachargedetail.commisionsn)),"
////              	    +"char(lachargedetail.managecom),char(lachargedetail.chargetype)),"
////              		+"modifydate=current date,modifytime=current time   "
////              		+" where commisionsn='"+tLAChargeSet.get(j).getCommisionSN()+"' and chargeflag='y' and chargetype='"+tSSRS1.GetText(k, 2)+"'" ;
////              	map.put(updateSql, "UPDATE");
////              	String updatesql="update lachargedetail set charge=ROUND(transmoney * chargerate,2),"
////                           		+"modifydate=current date,modifytime=current time "
////                           		+" where commisionsn='"+tLAChargeSet.get(j).getCommisionSN()+"'and chargestate ='0'";
////                       map.put(updatesql, "UPDATE"); 
////                       
////              	String calSQL = "select ROUND(transmoney * chargerate,2) from lachargedetail  "
////              			+" where commisionsn='"+tLAChargeSet.get(j).getCommisionSN()+"'and chargeflag ='y'  and chargetype='"+tSSRS1.GetText(k, 2)+"'";
////              	String dcharge =new ExeSQL().getOneValue(calSQL);
////              	//System.out.println("11111"+dcharge+"dsdafsafa");
////              	double dcharge1 = 0;
////              	if(dcharge != null &&!"".equals(dcharge)){
////              		 dcharge1 =Double.parseDouble(dcharge);
////              		
////              	}else{
////              		 dcharge1 =Double.parseDouble("0");
////              	}
////              	sumcharge = dcharge1 + sumcharge;
////              	
////              	//map.put(updateSql, "UPDATE");
////              }
////          }
//           // String    updateSql="update lacharge set charge=ROUND(transmoney * chargerate,2),"
//             //   		+"modifydate=current date,modifytime=current time "
//             //   		+" where commisionsn='"+tLAChargeSet.get(j).getCommisionSN()+"'and chargestate ='0'";
//          //  map.put(updateSql, "UPDATE");
//          }
      }
      
      else if(mOperate.equals("ALLREPAY")){
    	  String tSQL="select * from lacharge where chargetype='15"
        	  +"' and branchtype='1' and branchtype2='02' and chargestate='0"
        	  +"' and managecom='"+this.mManageCom
        	  +"' and tmakedate between '"
        	  +this.mStartDate+"' and '"+this.mEndDate+"'";
    	  if(this.mAgentCom!=null&&!this.mAgentCom.equals("")){
    		  tSQL+=" and agentcom='"+this.mAgentCom+"'";
    	  }
    	  if(this.mGrpContNo!=null&&!this.mGrpContNo.equals("")){
    		  tSQL+=" and grpcontno='"+this.mGrpContNo+"'";
    	  }
    	  if(this.mCardNo!=null&&!this.mCardNo.equals("")){
    		  tSQL+=" and grpcontno in (select grpcontno from licertify where cardno ='"+mCardNo+"')";
    	  }
    	  if(this.mWrapCode!=null&&!this.mWrapCode.equals("")){
    		  tSQL+=" and exists  (select '1' from lacommision where f3='"+mWrapCode+"' and commisionsn=lacharge.commisionsn)";
    	  }
    	  if(this.mContFlag!=null&&!this.mContFlag.equals(""))
    	  {
    		  if("0".equals(this.mContFlag))
    		  {
    			  tSQL+=" and exists(select 1 from ljapay where payno = lacharge.receiptno and incomeno = (select contno from lacommision where commisionsn = lacharge.commisionsn) and duefeetype = '0' )";  
    		  }else if("1".equals(this.mContFlag))
    		  {
    			  tSQL+=" and not exists(select 1 from ljapay where payno = lacharge.receiptno and incomeno = (select contno from lacommision where commisionsn = lacharge.commisionsn) and duefeetype = '0' )";  
    		  }else{
    			  
    		  }
    	  }
    	  if(this.mRiskCode != null && !this.mRiskCode.equals("")){
    		  tSQL += "and riskcode ='"+this.mRiskCode+"'";
    	  }
				tSQL += " with ur";
				System.out.println("打印sql如下：" + tSQL);
				this.mLAChargeSet = (LAChargeSet)tLAChargeDB.executeQuery(tSQL);
				for (int j = 1; j <= mLAChargeSet.size(); j++) {
					LAChargeSchema mLAChargeSchema = new LAChargeSchema();
					mLAChargeSchema = mLAChargeSet.get(j);
					String commisionsn = mLAChargeSchema.getCommisionSN();
					String leSQL = "select * from lacommision where commisionsn='"
							+ commisionsn + "'";
					tLACommisionSet = (LACommisionSet) tLACommisionDB
							.executeQuery(leSQL);
					if (tLACommisionSet.size() > 0) {
						for (int m = 1; m <= tLACommisionSet.size(); m++) {
							tLACommisionSchema = tLACommisionSet.get(m);
							cSumCharge = calculate(tLACommisionSchema);
						}
					}
					mLAChargeSchema.setCharge(cSumCharge);
					mLAChargeSchema.setChargeRate(cSumChargeRate);
					mLAChargeSchema.setModifyDate(CurrentDate);
					mLAChargeSchema.setModifyTime(CurrentTime);
					mLAChargeSchema.setCommisionSN(commisionsn);
					mUpLAChargeSet.add(mLAChargeSchema);
    	        
    		
      }
      }    	  
      else{
    	  CError tError = new CError();
          tError.moduleName = "LAARiskReCalculateBL";
          tError.functionName = "dealData";
          tError.errorMessage = "不支持的操作类型,请验证操作类型.";
          this.mErrors.addOneError(tError);
          return false;
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAARiskReCalculateBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
      map.put(this.mUpLAChargeSet, "UPDATE");
      //map.put(this.mLAChargeBSet, "UPDATE");
      map.put(this.mLastLAChargeLogSet, "UPDATE");
      map.put(this.mLAChargeFeeSet, "UPDATE");
      this.mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAARiskReCalculateBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
  public double calculate(LACommisionSchema cLACommisionSchema) {

      double sumCharge=0;
     
      String tCalCode ="";
      SSRS tSSRS = new SSRS();
      String tsql = "select CalCode from LAWageCalElement where  branchtype='1'" +
                    " and branchtype2='02' and CalType='21' " +
                    " and  RiskCode='"+cLACommisionSchema.getRiskCode()+"' ";
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tsql);
      if(tSSRS.getMaxRow()<=0){
      	tCalCode = "AR9901"; 	
      }
      else
      {
      	tCalCode = tSSRS.GetText(1, 1); 
      }
      LAChargeFeeSchema tLAChargeFeeSchema = new LAChargeFeeSchema();
      String chargeSql = "select code,code1,codealias from ldcode1 where codetype ='"+this.mChargeType+"'";
      SSRS tSSRS1 = new ExeSQL().execSQL(chargeSql);
      for(int j=1;j<=tSSRS1.getMaxRow();j++)
      {
      	Calculator tCalculator = new Calculator(); //计算类
      	System.out.println(tSSRS1.GetText(j, 1));
        tCalculator.setCalCode(tCalCode);
      	String cCodealias = tSSRS1.GetText(j, 3);
          tCalculator.addBasicFactor("BRANCHTYPE",cLACommisionSchema.getBranchType());
          tCalculator.addBasicFactor("BRANCHTYPE2",cLACommisionSchema.getBranchType2());
          tCalculator.addBasicFactor("RISKCODE",cLACommisionSchema.getRiskCode());
          tCalculator.addBasicFactor("MANAGECOM",cLACommisionSchema.getManageCom());
          tCalculator.addBasicFactor("AGENTCOM",cLACommisionSchema.getAgentCom());
          tCalculator.addBasicFactor("TRANSSTATE",cLACommisionSchema.getTransState());
          tCalculator.addBasicFactor("CONTNO",cLACommisionSchema.getContNo());
          tCalculator.addBasicFactor("GRPCONTNO", cLACommisionSchema.getGrpContNo());
          tCalculator.addBasicFactor("PAYINTV", String.valueOf(cLACommisionSchema.getPayIntv()));
          tCalculator.addBasicFactor("PAYYEARS", String.valueOf(cLACommisionSchema.getPayYears()));
          tCalculator.addBasicFactor("PAYYEAR", String.valueOf(cLACommisionSchema.getPayYear()));
          tCalculator.addBasicFactor("RENEWCOUNT", String.valueOf(cLACommisionSchema.getReNewCount()));
          tCalculator.addBasicFactor("WAGENO", cLACommisionSchema.getWageNo());
          //添加套餐编码
          tCalculator.addBasicFactor("WRAPCODE",cLACommisionSchema.getF3());
          tCalculator.addBasicFactor("TMAKEDATE",cLACommisionSchema.getTMakeDate());
      	tCalculator.addBasicFactor("CHARGETYPE", cCodealias);
      	double charge = 0;
      	double chargeRate = Double.parseDouble(tCalculator.calculate());
      	if (tCalculator.mErrors.needDealError()) {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "LAARiskAgentChargeCalNewBL";
              tError.functionName = "calWage1";
              tError.errorMessage = tCalculator.mErrors.getFirstError();
              this.mErrors.addOneError(tError);
              chargeRate = 0;
          }
      	
      	cSumChargeRate=chargeRate;
      	charge = mulrate(chargeRate,cLACommisionSchema.getTransMoney());
      	tLAChargeFeeSchema.setFirConCharge(charge);
      	tLAChargeFeeSchema.setFirConChargeRate(chargeRate);
      	//tLAChargeFeeSchema.setV(cCalChargeType,String.valueOf(chargeRate));
      	//tLAChargeFeeSchema.setV(cChargeType,String.valueOf(charge));
      	
      	sumCharge = sumCharge+charge;
      	if(charge==0)
		{
			continue;
		}
      }
		tLAChargeFeeSchema.setOperator(cLACommisionSchema.getOperator());
		tLAChargeFeeSchema.setMakeDate(this.CurrentDate);
		tLAChargeFeeSchema.setMakeTime(this.CurrentTime);
		tLAChargeFeeSchema.setModifyDate(this.CurrentDate);
		tLAChargeFeeSchema.setModifyTime(this.CurrentTime);
		tLAChargeFeeSchema.setCommisionSN(cLACommisionSchema.getCommisionSN());
		tLAChargeFeeSchema.setManageCom(cLACommisionSchema.getManageCom());
		tLAChargeFeeSchema.setAgentCom(cLACommisionSchema.getAgentCom());
		tLAChargeFeeSchema.setBranchType(cLACommisionSchema.getBranchType());
		tLAChargeFeeSchema.setBranchType2(cLACommisionSchema.getBranchType2());
		tLAChargeFeeSchema.setWageNo(cLACommisionSchema.getWageNo());
		tLAChargeFeeSchema.setTransMoney(cLACommisionSchema.getTransMoney());
		this.mLAChargeFeeSet.add(tLAChargeFeeSchema);
		return sumCharge;
  }
  
  //double类型乘法计算
  public static double mulrate(double v1, double v2) {

      BigDecimal b1 = new BigDecimal(Double.toString(v1));

      BigDecimal b2 = new BigDecimal(Double.toString(v2));

      return b1.multiply(b2).doubleValue();

  }
  
}
