package com.sinosoft.lis.agentcalculate;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author pengcheng
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAPerSalaryCalUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private String mOperat ="INSERT";


    public LAPerSalaryCalUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAPerSalaryCalBL tAgentWageGatherBL = new LAPerSalaryCalBL();
        if (!tAgentWageGatherBL.submitData(mInputData,mOperat))
        {
            this.mErrors.copyAllErrors(tAgentWageGatherBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LACalWageUI";
            tError.functionName = "submitData";
            tError.errorMessage = tAgentWageGatherBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
