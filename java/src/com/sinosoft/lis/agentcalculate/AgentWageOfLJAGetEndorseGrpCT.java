package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;

/**
 * 处理团单减少保费(解约)处理类
 * @author yangyang 2017-9-20
 *
 */
public class AgentWageOfLJAGetEndorseGrpCT extends AgentWageOfAbstractClass{

	private AgentWageOfLJAGetEndorseGrpMain mAgentWageOfLJAGetEndorseGrpMain = new AgentWageOfLJAGetEndorseGrpMain();
	
	public double mSumMoney = 0;
	public double mSumManageFee = 0;
	public double mSumAccuralFee = 0;
	public LJAGetEndorseSchema mCurrLAGetEndorseSchema = new LJAGetEndorseSchema();
	
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("AgentWageOfLJAGetEndorseGrpCT-->dealData:开始执行");
		System.out.println("AgentWageOfLJAGetEndorseGrpCT-->dealData:cSQL"+cSQL);
		mAgentWageOfLJAGetEndorseGrpMain.setBaseValue(this.mExtractDataType, this.mTableName, this.mDataType);
		LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAGetEndorseSet, cSQL);
		double tSumAccuralFee = 0;
		double tSumMoney = 0;
		double tSumManageFee = 0;
		LJAGetEndorseSchema tCurrLJAGetEndorseSchema = new LJAGetEndorseSchema();
		//最后一条数据的判断标记
		boolean tEndDataFlag = false;
		//用来判断数据是不是为空，为了防止 最后一次循环时，为空时，也打印错误语句  
		boolean tJudgeNullFlag = false;
		do
		{
			tRSWrapper.getData();
			if(!tJudgeNullFlag)
			{
				if(null==tLJAGetEndorseSet||0==tLJAGetEndorseSet.size())
				{
					System.out.println("AgentWageOfLJAGetEndorseGrpCT-->dealData 没有满足条件的数据");
					return true;
				}
				tJudgeNullFlag = true;
			}
			if(null == mCurrLAGetEndorseSchema.getActuGetNo())
			{
				mCurrLAGetEndorseSchema = tLJAGetEndorseSet.get(1);
			}
			if(!tEndDataFlag&&0==tLJAGetEndorseSet.size())
			{
				tEndDataFlag = true;
				tLJAGetEndorseSet.add(mCurrLAGetEndorseSchema);
			}
			MMap tMMap = new MMap();
			LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
			LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
			for(int i =1;i<=tLJAGetEndorseSet.size();i++)
			{
				LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
				tLJAGetEndorseSchema = tLJAGetEndorseSet.get(i);
				String tNewFeeOperationType = tLJAGetEndorseSchema.getFeeOperationType();
				String tNewFeeFinaType = tLJAGetEndorseSchema.getFeeFinaType();
				String tNewGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
				String tNewPayPlanCode = tLJAGetEndorseSchema.getPayPlanCode();
				//添加标记，用来表示是否是不同类型的数据
				boolean tDiffentFlag = false;
				double tTransMoney = tLJAGetEndorseSchema.getGetMoney();
				//管理费
				double tManageFee = mAgentWageCommonFunction.getCTManageFee(tLJAGetEndorseSchema.getPolNo(), tLJAGetEndorseSchema.getEndorsementNo(), tLJAGetEndorseSchema.getOtherNoType());
				//利息 
				double tAccuralFee = mAgentWageCommonFunction.getAccuralfee(tLJAGetEndorseSchema);
				if(!mCurrLAGetEndorseSchema.getFeeOperationType().equals(tNewFeeOperationType)
						||!mCurrLAGetEndorseSchema.getFeeFinaType().equals(tNewFeeFinaType)
						||!mCurrLAGetEndorseSchema.getGrpPolNo().equals(tNewGrpPolNo)
						||!mCurrLAGetEndorseSchema.getPayPlanCode().equals(tNewPayPlanCode))
				{
					tSumMoney = mSumMoney;
					tSumManageFee = mSumManageFee;
					tSumAccuralFee = mSumAccuralFee;
					tCurrLJAGetEndorseSchema = mCurrLAGetEndorseSchema;
					tDiffentFlag = true;
					//重新初始化数据，
					mCurrLAGetEndorseSchema = tLJAGetEndorseSchema;
					mSumMoney = tTransMoney;
					mSumManageFee = tManageFee;
					mSumAccuralFee = tAccuralFee;
				}
				else
				{
					if(tEndDataFlag)
					{
						tSumMoney = mSumMoney;
						tSumManageFee = mSumManageFee;
						tSumAccuralFee = mSumAccuralFee;
						tCurrLJAGetEndorseSchema = mCurrLAGetEndorseSchema;
						tDiffentFlag = true;
					}
					else
					{
						mSumMoney+=tTransMoney;
						mSumManageFee+=tManageFee;
						mSumAccuralFee+=tAccuralFee;
					}
				}
				if(tDiffentFlag)
				{
					boolean tReturnFlag = mAgentWageOfLJAGetEndorseGrpMain.dealData(tCurrLJAGetEndorseSchema, tSumMoney+tSumAccuralFee, tSumManageFee, tSumAccuralFee, "16", cOperator);
					if(tReturnFlag)
					{
						tLACommisionNewSet.add(mAgentWageOfLJAGetEndorseGrpMain.getmLACommisionNewSchema());
					}
					else
					{
						tLACommisionErrorSet.add(mAgentWageOfLJAGetEndorseGrpMain.getmLACommisionErrorSchema());
					}
				}
			}
			if(0<tLACommisionNewSet.size())
			{
				tMMap.put(tLACommisionNewSet, "INSERT");
			}
			if(0<tLACommisionErrorSet.size())
			{
				tMMap.put(tLACommisionErrorSet, "INSERT");
			}
			if(0==tMMap.size())
			{
				continue;
			}
			if (!mAgentWageCommonFunction.dealDataToTable(tMMap)) {
				// @@错误处理
				CError tError = new CError();
				tError.moduleName = "AgentWageOfLJAGetEndorseGrpCT";
				tError.functionName = "PubSubmit";
				tError.errorMessage = "数据插入到LACommisionNew中失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}while(tLJAGetEndorseSet.size()>0);
		System.out.println("AgentWageOfLJAGetEndorseGrpCT-->dealData:执行结束");
		return true;
	}
}
