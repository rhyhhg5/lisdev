package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAIndirectWageSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAIndirectWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p> 
 *
 * @author caigang
 * @version 1.0
 */
public class LABankManageCommision {		
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private String mOperate;
//    private LAIndirectWageSchema mLAIndirectWageSchema = new LAIndirectWageSchema();
    private LAIndirectWageSet mLAIndirectWageSchemaSet = new LAIndirectWageSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    private String mManageCom="";
    private String mStartDate="";
    private String mEndDate="";
    public LABankManageCommision() {
        try {
        } catch (Exception ex) {
            ex.printStackTrace();
        }	
    }

    //处理数据
    public boolean dealData() {    	    	
        //处理前一天的保单对新单进行间接绩效的计算
        System.out.println("*****************查询银代lacommsion表中当天的数据***************");
        String dateSql = "Select date('" + PubFun.getCurrentDate() +
        "')-1 day from dual";
        ExeSQL texeSQL = new ExeSQL();
        String tMakeDate = texeSQL.getOneValue(dateSql);
        String sql = "select CommisionSN from lacommision where branchtype = '3' and branchtype2 = '01' and payyear='0' and (commdire='1'"
        		+ "or (commdire='2' and transtype = 'WT'))";          
        sql += " and tmakedate='" + tMakeDate + "'";
        //测试用
//        sql += " and managecom = '86110000' and tmakedate between '2015-8-1' and '2015-8-30'";
        sql += " order by CommisionSN with ur ";
        System.out.println("："+sql);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        System.out.println("共有："+tSSRS.getMaxRow()+"条数据"); 
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            LAIndirectWageSchema mLAIndirectWageSchema = new LAIndirectWageSchema();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionDB.setCommisionSN(tSSRS.GetText(i, 1));
            tLACommisionDB.getInfo();
            tLACommisionSchema = tLACommisionDB.getSchema();
            String cCommisionSN = tLACommisionSchema.getCommisionSN();
            String cBranchType = tLACommisionSchema.getBranchType();
            String cBranchType2 = tLACommisionSchema.getBranchType2();
            String cRiskCode = tLACommisionSchema.getRiskCode();
            String cAgentCom = tLACommisionSchema.getAgentCom();
            String cContNo = tLACommisionSchema.getContNo();
            String cGrpContNo = tLACommisionSchema.getGrpContNo();
            String cManageCom = tLACommisionSchema.getManageCom();
            String cPayyears = tLACommisionSchema.getPayYears()+"";
            String cPayyear = tLACommisionSchema.getPayYear()+"";
            String cRenewCount = tLACommisionSchema.getReNewCount()+"";
            String cTransState = tLACommisionSchema.getTransState();
            String cPayIntv = tLACommisionSchema.getPayIntv()+"";
            String cAgentCode = tLACommisionSchema.getAgentCode();
            String cWageNo = tLACommisionSchema.getWageNo();
            String cAgentGroup = tLACommisionSchema.getAgentGroup();
            //计算团队首期绩效提奖比例
            String strWageRate = this.calCalRateForAll( cBranchType,cBranchType2,
                    cRiskCode,  cAgentCom,
                    cContNo,  cGrpContNo,
                    cManageCom,  cPayyears, cPayyear,
                    cRenewCount,
                    cTransState, cPayIntv, cAgentCode, cWageNo, cAgentGroup);
            double wageRateValue = 0;
            wageRateValue = Double.parseDouble(strWageRate);
            System.out.println("主管职级间接提奖比例为："+wageRateValue);
            double tTransMoney = tLACommisionSchema.getTransMoney();
            double tBankG = tTransMoney*wageRateValue;
            System.out.println("主管职级间接佣金为："+tBankG);
            //新单进行折标
            String strStandPrem = this.calCalStandPrem(cCommisionSN,cRiskCode,cPayyears,cPayyear,cBranchType,cBranchType2);
            double standPrem=0;
            standPrem = Double.parseDouble(strStandPrem);
                           
            //存入间接薪资项表
//            System.out.println("明细扎帐号为："+tLACommisionSchema.getCommisionSN());
            mLAIndirectWageSchema.setCommisionSN(tLACommisionSchema.getCommisionSN());
            mLAIndirectWageSchema.setBankGrpMoney(tBankG);
            mLAIndirectWageSchema.setGrpRate(wageRateValue);
            //新单折标保费
            mLAIndirectWageSchema.setF1(standPrem);
            mLAIndirectWageSchema.setBranchType(cBranchType);
            mLAIndirectWageSchema.setBranchType2(cBranchType2);
            mLAIndirectWageSchema.setOperator(tLACommisionSchema.getOperator());
            mLAIndirectWageSchema.setTMakeDate(tLACommisionSchema.getTMakeDate());
            mLAIndirectWageSchema.setMakeDate(currentDate);
            mLAIndirectWageSchema.setMakeTime(currentTime);
            mLAIndirectWageSchema.setModifyDate(currentDate);
            mLAIndirectWageSchema.setModifyTime(currentTime);
            mLAIndirectWageSchemaSet.add(mLAIndirectWageSchema);
        } 
        mMap.put(this.mLAIndirectWageSchemaSet , "INSERT");
        this.mInputData.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AgentWageCalSaveNewBL Submit...");
        try{
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
        	// @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        }catch(Exception ex)
        {
        	ex.printStackTrace();
        }
        return true;
    }
    
    public boolean dealData(VData cInputData,String operate) {
   	    this.mManageCom=(String)cInputData.get(0);
	    this.mStartDate=(String)cInputData.get(1);
	    this.mEndDate=(String)cInputData.get(2);
	    System.out.println("管理机构:"+mManageCom+"提数始期:"+mStartDate+"提数止期:"+mEndDate);
        //处理前一天的保单对新单进行间接绩效的计算
//        System.out.println("*****************查询银代lacommsion表中当天的数据***************");
//        String dateSql = "Select date('" + PubFun.getCurrentDate() +
//        "')-1 day from dual";
//        ExeSQL texeSQL = new ExeSQL();
//        String tMakeDate = texeSQL.getOneValue(dateSql);
        String sql = "select CommisionSN from lacommision where branchtype = '3' and branchtype2 = '01' and payyear='0' and (commdire='1'"
        		+ "or (commdire='2' and transtype = 'WT'))";              
        sql += " and tmakedate between '" + mStartDate + "' and '"+ mEndDate + "'";
        if(mManageCom!=null&&!mManageCom.equals("")){
        	sql +=" and managecom like '"+mManageCom+"%'";
        }
        //测试用
//        sql += " and managecom = '86110000' and tmakedate between '2015-8-1' and '2015-8-30'";
        sql += " order by CommisionSN with ur ";
        System.out.println("："+sql);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);
        System.out.println("共有："+tSSRS.getMaxRow()+"条数据"); 
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            LAIndirectWageSchema mLAIndirectWageSchema = new LAIndirectWageSchema();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionDB.setCommisionSN(tSSRS.GetText(i, 1));
            tLACommisionDB.getInfo();
            tLACommisionSchema = tLACommisionDB.getSchema();
            String cCommisionSN = tLACommisionSchema.getCommisionSN();
            String cBranchType = tLACommisionSchema.getBranchType();
            String cBranchType2 = tLACommisionSchema.getBranchType2();
            String cRiskCode = tLACommisionSchema.getRiskCode();
            String cAgentCom = tLACommisionSchema.getAgentCom();
            String cContNo = tLACommisionSchema.getContNo();
            String cGrpContNo = tLACommisionSchema.getGrpContNo();
            String cManageCom = tLACommisionSchema.getManageCom();
            String cPayyears = tLACommisionSchema.getPayYears()+"";
            String cPayyear = tLACommisionSchema.getPayYear()+"";
            String cRenewCount = tLACommisionSchema.getReNewCount()+"";
            String cTransState = tLACommisionSchema.getTransState();
            String cPayIntv = tLACommisionSchema.getPayIntv()+"";
            String cAgentCode = tLACommisionSchema.getAgentCode();
            String cWageNo = tLACommisionSchema.getWageNo();
            String cAgentGroup = tLACommisionSchema.getAgentGroup();
            //计算团队首期绩效提奖比例
            String strWageRate = this.calCalRateForAll( cBranchType,cBranchType2,
                    cRiskCode,  cAgentCom,
                    cContNo,  cGrpContNo,
                    cManageCom,  cPayyears, cPayyear,
                    cRenewCount,
                    cTransState, cPayIntv, cAgentCode, cWageNo, cAgentGroup);
            double wageRateValue = 0;
            wageRateValue = Double.parseDouble(strWageRate);
            System.out.println("主管职级间接提奖比例为："+wageRateValue);
            double tTransMoney = tLACommisionSchema.getTransMoney();
            double tBankG = tTransMoney*wageRateValue;
            System.out.println("主管职级间接佣金为："+tBankG);
            //新单进行折标
            String strStandPrem = this.calCalStandPrem(cCommisionSN,cRiskCode,cPayyears,cPayyear,cBranchType,cBranchType2);
            double standPrem=0;
            standPrem = Double.parseDouble(strStandPrem);
                           
            //存入间接薪资项表
//            System.out.println("明细扎帐号为："+tLACommisionSchema.getCommisionSN());
            mLAIndirectWageSchema.setCommisionSN(tLACommisionSchema.getCommisionSN());
            mLAIndirectWageSchema.setBankGrpMoney(tBankG);
            mLAIndirectWageSchema.setGrpRate(wageRateValue);
            //新单折标保费
            mLAIndirectWageSchema.setF1(standPrem);
            mLAIndirectWageSchema.setBranchType(cBranchType);
            mLAIndirectWageSchema.setBranchType2(cBranchType2);
            mLAIndirectWageSchema.setOperator(tLACommisionSchema.getOperator());
            mLAIndirectWageSchema.setTMakeDate(tLACommisionSchema.getTMakeDate());
            mLAIndirectWageSchema.setMakeDate(currentDate);
            mLAIndirectWageSchema.setMakeTime(currentTime);
            mLAIndirectWageSchema.setModifyDate(currentDate);
            mLAIndirectWageSchema.setModifyTime(currentTime);
            mLAIndirectWageSchemaSet.add(mLAIndirectWageSchema);
        } 
        mMap.put(this.mLAIndirectWageSchemaSet , "INSERT");
        this.mInputData.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AgentWageCalSaveNewBL Submit...");
        try{
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
        	// @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentWageCalSaveNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        }catch(Exception ex)
        {
        	ex.printStackTrace();
        }
        return true;
    }
   private String calCalRateForAll(String cBranchType, String cBranchType2,
                                   String cRiskCode, String cAgentCom,
                                   String cContNo, String cGrpContNo,
                                   String cManageCom, String cPayyears,String cPayyear,
                                   String cRenewCount,
                                   String cTransState,String cPayIntv,String cAgentCode,String cWageNo,String cAgentGroup)
   {
       String tBranchType = cBranchType == null ? "" : cBranchType;
       String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
       String tRiskCode = cRiskCode == null ? "" : cRiskCode;
       String tAgentCom = cAgentCom == null ? "" : cAgentCom;
       String tContNo = cContNo == null ? "" : cContNo;
       String tGrpContNo = cGrpContNo == null ? "" : cGrpContNo;
       String tManageCom = cManageCom == null ? "" : cManageCom;
       String tPayyears = cPayyears ==null? "": cPayyears;
       String tPayyear = cPayyear== null ? "" : cPayyear ;
       String tRenewCount = cRenewCount== null ? "" : cRenewCount ;//团险根据收费方式采取不同的佣金计算方式
       String tTransState=cTransState==null ? "" : cTransState ;
       String tPayIntv = cPayIntv==null ? "" : cPayIntv ;
       String tAgentCode = cAgentCode==null ? "" : cAgentCode ;
       String tWageNo = cWageNo==null ? "" : cWageNo ;
       String tAgentGroup =cAgentGroup ==null ? "" :cAgentGroup;
//       if (tBranchType == "") 
//       {
//           return "";
//       }
//       if (tBranchType2 == "") {
//           return "";
//       }
//       //如果是续保、续期，则不需要按照回执回销计算
//       else if (mLACommisionSchema.getPayCount()>1)
//       {
//           tFlag="02";
//       }

       Calculator tCalculator = new Calculator();
       tCalculator.setCalCode("gRate");
       tCalculator.addBasicFactor("BranchType", tBranchType);
       tCalculator.addBasicFactor("BranchType2", tBranchType2);
       tCalculator.addBasicFactor("RiskCode", tRiskCode);
       tCalculator.addBasicFactor("AgentCom", tAgentCom);
       tCalculator.addBasicFactor("ContNo", tContNo);
       tCalculator.addBasicFactor("GrpContNo", tGrpContNo);
       tCalculator.addBasicFactor("ManageCom", tManageCom);
       tCalculator.addBasicFactor("Payyears", tPayyears);
       tCalculator.addBasicFactor("Payyear", tPayyear);
       tCalculator.addBasicFactor("RenewCount", tRenewCount);
       tCalculator.addBasicFactor("TransState", tTransState);
       tCalculator.addBasicFactor("PayIntv", tPayIntv);
       tCalculator.addBasicFactor("AgentCode", tAgentCode);
       tCalculator.addBasicFactor("WageNo", tWageNo);
       tCalculator.addBasicFactor("AgentGroup", tAgentGroup);

     
       String tResult = tCalculator.calculate();
       return tResult == null ? "" : tResult;
   }
   private String calCalStandPrem(String cCommisionSN,String cRiskCode,String cPayyears,
		                    String cPayyear,String cBranchType,String cBranchType2)
   {
			String tCommisionSN = cRiskCode == null ? "" : cCommisionSN;
			String tRiskCode = cRiskCode == null ? "" : cRiskCode;
			String tPayyears = cPayyears == null ? "" : cPayyears;
			String tPayyear = cPayyear == null ? "" : cPayyear;
	   	 	String tBranchType = cBranchType == null ? "" : cBranchType;
			String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
							
			Calculator tCalculator = new Calculator();
			tCalculator.setCalCode("sPrem");
			tCalculator.addBasicFactor("CommisionSN", tCommisionSN);
			tCalculator.addBasicFactor("RiskCode", tRiskCode);	
			tCalculator.addBasicFactor("Payyears", tPayyears);
			tCalculator.addBasicFactor("Payyear", tPayyear);
			tCalculator.addBasicFactor("BranchType", tBranchType);
			tCalculator.addBasicFactor("BranchType2", tBranchType2);
			
			String tResult = tCalculator.calculate();
			return tResult == null ? "" : tResult;
   }
   
}
