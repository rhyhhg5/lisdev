/*
 * <p>ClassName: AgentWageCalDoUI </p>
 * <p>Description: LAAgentUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-31
 */
package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AgentChargeCalDoBL {

	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	private VData mInputData = new VData();
	public GlobalInput mGlobalInput = new GlobalInput();
	private LAChargeSet mLAChargeSet = new LAChargeSet();
	private LJAGetSet mLJAGetSet = new LJAGetSet();
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	private String mMakeDate;
	private MMap map = new MMap();
	private String mManageCom;
	private String mBranchType;
	private String mBranchType2;
	private String mOperate;

	public AgentChargeCalDoBL() {
	}

	public static void main(String[] args) {
		double aa = 0.000001;
		String bb = String.valueOf(aa);
		VData tVData = new VData();
		String sqlstr = "select * from LAWageLog where wageno = '200708' and managecom='86110000' and branchtype='2' and branchtype2='01'";
		LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
		LAWageLogSet tLAWageLogSet = new LAWageLogSet();
		LAWageLogDB tLAWageLogDB = tLAWageLogSchema.getDB();
		tLAWageLogSet = tLAWageLogDB.executeQuery(sqlstr);
		if (tLAWageLogSet.size() > 0) {
			tLAWageLogSchema = tLAWageLogSet.get(1);
		} else {
			System.out.println("error");
			return;
		}
		GlobalInput tG = new GlobalInput();
		tG.Operator = "ac";
		tG.ManageCom = "86";
		tVData.clear();

		// 提交
		tVData.addElement(tG);
		tVData.addElement("86110000");
		tVData.addElement(tLAWageLogSchema);
		tVData.addElement("1");
		AgentChargeCalDoBL tAgentWageCalDoUI = new AgentChargeCalDoBL();
		tAgentWageCalDoUI.submitData(tVData, "INSERT||CALWAGEAGAIN");
		System.out
				.println("Error:" + tAgentWageCalDoUI.mErrors.getFirstError());
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备往后台的数据
//		if (!prepareOutputData()) {
//			return false;
//		}
		mInputData = null;
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(mGlobalInput);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "AgentWageCalDoUI";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		System.out.println("AgentChargeCalDoBL-->dealData:开始执行");
		String cSQL = "select * from lacharge a where"
				+ " branchtype='5' and branchtype2='01' and chargestate='0' and charge<>0 "
				+ " and managecom='" + this.mManageCom
				//自动批处理9.1号之后上线
				+ "' and tmakedate between '2018-9-1' and '"+mMakeDate+"'"
				+ " and exists (select '1' from lacom where lacom.agentcom=lacharge.agentcom and actype in ('06','07')) "
				+ " and customgetpoldate is not null and customgetpoldate<>'' "
				+ " and (days("
				+ mMakeDate
				+ ")+ 1 -days(customgetpoldate))>=15 "
				;
		cSQL += " with ur";
		System.out.println("AgentChargeCalSaveBL-->dealData:cSQL" + cSQL);
		LACommisionSet tLACommisionSet = null;
		LACommisionDB tLACommisionDB = new LACommisionDB();
		LAChargeSet mUpLAChargeSet = new LAChargeSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(mUpLAChargeSet, cSQL);
		do {
			tRSWrapper.getData();
			if (null == mUpLAChargeSet || 0 == mUpLAChargeSet.size()) {
				System.out
						.println("AgentWageOfLABankManageTask-->dealData:没有满足新单折标的数据！！");
				return true;
			}
			MMap mMap = new MMap();
			System.out.println("&&&&&&&&&&&&&&&&&&");
			// add new
			for (int j = 1; j <= mUpLAChargeSet.size(); j++) {
				LAChargeSchema t_LAChargeSchema = mUpLAChargeSet.get(j)
						.getSchema();
				mUpLAChargeSet.get(j).setChargeState("1");
				mUpLAChargeSet.get(j).setModifyDate(CurrentDate);
				mUpLAChargeSet.get(j).setModifyTime(CurrentTime);
				mUpLAChargeSet.get(j).setOperator("000");

				LJAGetSchema tLJAGetSchema = new LJAGetSchema();
				tLJAGetSchema.setActuGetNo(mUpLAChargeSet.get(j)
						.getCommisionSN());
				tLJAGetSchema.setOtherNo(mUpLAChargeSet.get(j).getReceiptNo());
				if ("55".equals(mUpLAChargeSet.get(j).getChargeType())) {
					tLJAGetSchema.setOtherNoType("BC");
				}
				if ("56".equals(mUpLAChargeSet.get(j).getChargeType())) {
					tLJAGetSchema.setOtherNoType("MC");
				}
				String tSQL = "select * from lacommision "
						+ "where receiptno='"
						+ mUpLAChargeSet.get(j).getReceiptNo() + "'"
						+ " and  commisionsn='"
						+ mUpLAChargeSet.get(j).getTCommisionSN() + "' "
						+ " fetch first 1 rows only ";
				tLACommisionSet = tLACommisionDB.executeQuery(tSQL);

				// add new

				tLJAGetSchema.setPayMode("12");
				tLJAGetSchema
						.setManageCom(mUpLAChargeSet.get(j).getManageCom());
				tLJAGetSchema.setAgentCom(tLACommisionSet.get(1).getAgentCom());
				tLJAGetSchema.setGetNoticeNo(mUpLAChargeSet.get(j)
						.getCommisionSN());
				tLJAGetSchema.setAgentType(tLACommisionSet.get(1)
						.getAgentType());
				tLJAGetSchema.setAgentCode(tLACommisionSet.get(1)
						.getAgentCode());
				tLJAGetSchema.setAgentGroup(tLACommisionSet.get(1)
						.getAgentGroup());
				tLJAGetSchema.setAppntNo(tLACommisionSet.get(1).getAppntNo());
				tLJAGetSchema.setSumGetMoney(mUpLAChargeSet.get(j).getCharge());
				tLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet.get(1)
						.getAgentCom()));
				tLJAGetSchema.setDrawerID(tLACommisionSet.get(1).getAgentCom());
				tLJAGetSchema.setShouldDate(CurrentDate);
				tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
				tLJAGetSchema.setMakeDate(CurrentDate);
				tLJAGetSchema.setMakeTime(CurrentTime);
				tLJAGetSchema.setModifyDate(CurrentDate);
				tLJAGetSchema.setModifyTime(CurrentTime);
				this.mLJAGetSet.add(tLJAGetSchema);
			}
			mMap.put(mUpLAChargeSet, "UPDATE");
			mMap.put(mLJAGetSet, "INSERT");
			try {
				VData tVData = new VData();
				tVData.add(mMap);
				System.out.println("开始提交数据");
				PubSubmit tPubSubmit = new PubSubmit();
				tPubSubmit.submitData(tVData, "");
				System.out.println("数据提交完成");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} while (tLACommisionSet.size() > 0);
		System.out.println("AgentChargeCalDoBL-->dealData:执行结束 ");
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mBranchType = (String) cInputData.getObject(1);
		mBranchType2 = (String) cInputData.getObject(2);
		mManageCom = (String) cInputData.getObject(3);
		mMakeDate = (String) cInputData.getObject(4);
		if ("".equals(mBranchType)
				|| "".equals(mBranchType2) || "".equals(mManageCom)|| "".equals(mMakeDate)) {
			System.out.println("获取要素信息失败branchtype--branchtype2--mManagecom--mMakeDate:"
					+ mBranchType + "--" + mBranchType2 + "--" + mManageCom+"--"+mMakeDate);
			CError tError = new CError();
			tError.moduleName = "AgentWageCalSaveUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

	private String getAgentComName(String tAgentCom) {
		String comName = "";
		ExeSQL tExeSQL = new ExeSQL();
		String sql = " select name from lacom where agentcom='" + tAgentCom
				+ "' with ur ";
		SSRS tSSRS = tExeSQL.execSQL(sql);
		comName = tSSRS.GetText(1, 1);
		return comName;
	}

}
