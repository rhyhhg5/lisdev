/*
 * <p>ClassName: AgentWageCalSaveUI </p>
 * <p>Description: LAAgentUI类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-31
 */
package com.sinosoft.lis.agentcalculate;

import java.math.BigDecimal;

import com.sinosoft.lis.db.LAChargeLogDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeLogSchema;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAChargeLogSet;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AgentChargeCalSaveNewBL {

	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	private VData mInputData = new VData();
	private MMap mMap = new MMap();
	private LAChargeSet mLAChargeSet = new LAChargeSet();
	private LJAGetSet mLJAGetSet = new LJAGetSet();
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	private String mOperate;
	private String mManageCom;
	private String mBranchType;
	private String mBranchType2;
	private String mMakeDate;
	private GlobalInput mGlobalInput = new GlobalInput();
	private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();

	public AgentChargeCalSaveNewBL() {
	}

	public static void main(String[] args) {
		AgentChargeCalSaveNewBL tAgentWageCalSaveUI = new AgentChargeCalSaveNewBL();
		VData tVData = new VData();
		LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
		tLAWageLogSchema.setManageCom("86110000");
		tLAWageLogSchema.setWageYear("2006");
		tLAWageLogSchema.setWageMonth("12");
		tLAWageLogSchema.setBranchType("3");
		tLAWageLogSchema.setBranchType2("01");
		tLAWageLogSchema.setStartDate("2006-12-30");
		tLAWageLogSchema.setEndDate("2006-12-30");
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86";
		tVData.addElement(tG);

		tVData.addElement(tLAWageLogSchema);
		try {
			tAgentWageCalSaveUI.submitData(tVData, "INSERT||AGENTWAGE");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Error:"
				+ tAgentWageCalSaveUI.mErrors.getFirstError());

		AgentWageCalDoUI tAgentWageCalDoUI = new AgentWageCalDoUI();
		tVData = new VData();
		tLAWageLogSchema = new LAWageLogSchema();
		tLAWageLogSchema.setManageCom("86110000");
		tLAWageLogSchema.setWageYear("2006");
		tLAWageLogSchema.setWageMonth("12");
		tLAWageLogSchema.setBranchType("3");
		tLAWageLogSchema.setBranchType2("01");
		tLAWageLogSchema.setStartDate("2006-12-30");
		tLAWageLogSchema.setEndDate("2006-12-30");

		tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86";
		tVData.addElement(tG);

		tVData.addElement(tLAWageLogSchema);

		tAgentWageCalDoUI.submitData(tVData, "INSERT||AGENTWAGE");
		System.out.println("Error:"
				+ tAgentWageCalSaveUI.mErrors.getFirstError());

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备往后台的数据
		// if (!prepareOutputData()) {
		// return false;
		// }
		// PubSubmit tPubSubmit = new PubSubmit();
		// System.out.println("Start AgentChargeCalSaveNewBL Submit...");
		// try {
		// if (!tPubSubmit.submitData(mInputData, mOperate)) {
		// // @@错误处理
		// System.out.println("手续费计算提交失败");
		// this.mErrors.copyAllErrors(tPubSubmit.mErrors);
		// CError tError = new CError();
		// tError.moduleName = "AgentWageCalSaveNewBL";
		// tError.functionName = "submitData";
		// tError.errorMessage = "数据提交失败!";
		// this.mErrors.addOneError(tError);
		// return false;
		// }
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
		System.out.println("End AgentChargeCalSaveNewBL Submit...");
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	// private boolean prepareOutputData() {
	// try {
	// System.out
	// .println("Begin LAActiveChargeBL.prepareOutputData.........");
	// mInputData.clear();
	// mInputData.add(map);
	// System.out
	// .println("End LAActiveChargeBL.prepareOutputData.........");
	// } catch (Exception ex) {
	// // @@错误处理
	// System.out.println("准备提交数据时失败");
	// return false;
	// }
	// return true;
	// }

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	public boolean dealData() {
		FDate fd = new FDate();
		LAChargeLogSet tLAChargeLogSet = new LAChargeLogSet();
		LAChargeLogDB tLAChargeLogDB = new LAChargeLogDB();

		String sql = "";
		String Year = AgentPubFun.formatDate(mMakeDate, "yyyy");
		String Month = AgentPubFun.formatDate(mMakeDate, "MM");
		sql = "select * from LAChargeLog where  ChargeYear = '" + Year + "'"
				+ " and ChargeMonth='" + Month + "'" + " and ManageCom = '"
				+ mManageCom + "' ";
		sql += "And BranchType = '" + mBranchType + "'";
		sql += " And BranchType2 ='" + mBranchType2 + "'";

		System.out.println("sql:" + sql);

		tLAChargeLogSet = tLAChargeLogDB.executeQuery(sql);
		if (tLAChargeLogDB.mErrors.needDealError()) {
			this.mErrors.copyAllErrors(tLAChargeLogDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "AgentWageCalSaveNewBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询日志纪录存在是否出错！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (tLAChargeLogSet.size() == 0) {
			System.out.println("日志表中没有纪录！");
			if (!prepareLogData(Year, Month)) {
				return false;
			}
		} else {
			for (int i = 1; i <= tLAChargeLogSet.size(); i++) {
				String today = mMakeDate;
				String yesterday = PubFun.calDate(today, -1, "D", null);
				LAChargeLogSchema tLAChargeLogSchema = new LAChargeLogSchema();
				tLAChargeLogSchema = tLAChargeLogSet.get(i);
				System.out.println("tLAChargeLogSchema.getEndDate():"
						+ tLAChargeLogSchema.getEndDate());
				System.out.println("today:" + today);
				System.out.println("yesterday:" + yesterday);
				if (!tLAChargeLogSchema.getState().equals("11")) {
					CError tError = new CError();
					tError.moduleName = "ActiveAgentChargeCalNewBL";
					tError.functionName = "dealData";
					tError.errorMessage = "昨天(" + yesterday + ")的展业类型为"
							+ tLAChargeLogSchema.getBranchType() + "，渠道"
							+ tLAChargeLogSchema.getBranchType2()
							+ "数据提取后未计算，不能进行今天的提取！";
					this.mErrors.addOneError(tError);
					return false;
				}
				LAChargeLogDB mLAChargeLogDB = new LAChargeLogDB();
				mLAChargeLogDB.setManageCom(tLAChargeLogSchema.getManageCom());
				mLAChargeLogDB.setChargeCalNo(tLAChargeLogSchema
						.getChargeCalNo());
				mLAChargeLogDB.setChargeMonth(tLAChargeLogSchema
						.getChargeMonth());
				mLAChargeLogDB
						.setChargeYear(tLAChargeLogSchema.getChargeYear());
				mLAChargeLogDB
						.setBranchType(tLAChargeLogSchema.getBranchType());
				mLAChargeLogDB.setBranchType2(tLAChargeLogSchema
						.getBranchType2());
				mLAChargeLogDB.getInfo();
				mLAChargeLogDB.setModifyDate(CurrentDate);
				mLAChargeLogDB.setModifyTime(CurrentTime);
				mLAChargeLogDB.setOperator(mGlobalInput.Operator);
				mLAChargeLogDB.setState("00");
				mLAChargeLogDB.setEndDate(mMakeDate);
				if (!mLAChargeLogDB.update()) {
					return false;
				}
			}
		}
		System.out.println("AgentChargeCalSaveBL-->dealData:开始执行");
		String cSQL = 
//				"select * from lacommision where managecom like '"
//				+ mManageCom
//				+ "%' and "
//				+ " branchtype = '"
//				+ mBranchType
//				+ "' and branchtype2 = '"
//				+ mBranchType2
//				+ "' "
//				// branchtype3='2' 表示交叉销售出单，互动中介交叉销售只有相互代理
//				+ " and branchtype3= '2' and customgetpoldate>='2018-8-1' "
//				// 只有新单才有回执回销日期
//				+ " and payyear=0 and renewcount=0 "
//				+ " and agentcom is not null and agentcom <>'' "
//				+ " and customgetpoldate is not null and customgetpoldate<>'' "
//				+ " and (days("
//				+ mMakeDate
//				+ ")+ 1 -days(customgetpoldate))>=15 "
//				// 此处没有用a.tcommisionsn关联，考虑到sql效率，同时业务逻辑应是业务手续费与管理手续费成对出现，校验一个即可。
//				+ " and not exists (select '1' from lacharge a where a.commisionsn=lacommision.commisionsn)"
//				+ " union"
				// 续期续保的保单按照财务实收日期计算,需待销售扎帐批处理执行完成后再执行此批处理
				 " select * from lacommision where managecom = '"
				+ mManageCom
				+ "' and "
				+ " branchtype = '"
				+ mBranchType
				+ "' and branchtype2 = '"
				+ mBranchType2
//				+ "' and branchtype3 = '2' "
				+ " and agentcom is not null and agentcom <>'' "
				+ " and  exists(select 1 from lacommision where  lacharge.tcommisionsn = lacommision.commisionsn and branchtype3='2')"
				+ " and tmakedate = '"
				+ mMakeDate
//				+ " and (payyear>0 or renewcount>0)"
				+ " and not exists (select '1' from lacharge a where a.commisionsn=lacommision.commisionsn)";
		System.out.println("AgentChargeCalSaveBL-->dealData:cSQL" + cSQL);
		LACommisionSet tLACommisionSet = new LACommisionSet();
		LACommisionDB tLACommisionDB = new LACommisionDB();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLACommisionSet, cSQL);
		do {
			tRSWrapper.getData();
			if (null == tLACommisionSet || 0 == tLACommisionSet.size()) {
				System.out
						.println("AgentWageOfLABankManageTask-->dealData:没有满足新单折标的数据！！");
				return true;
			}
			mMap = new MMap();
			Reflections tReflections = new Reflections();
			for (int i = 1; i <= tLACommisionSet.size(); i++) {
				LACommisionSchema tLACommisionSchema = new LACommisionSchema();
				LAChargeSchema businessLAChargeSchema = new LAChargeSchema();
				LAChargeSchema manageLAChargeSchema = new LAChargeSchema();
				tLACommisionSchema = tLACommisionSet.get(i);
				if (!checkCalFlag(tLACommisionSchema)) {
					continue;
				}
				String tRiskCode = "";
				String tAgentCom = "";
				String tContNo = "";
				int payyear = 0;
				int renewcount = 0;
				int payyears = 0;
				int tPayIntv = 0;
				tRiskCode = tLACommisionSchema.getRiskCode();
				tAgentCom = tLACommisionSchema.getAgentCom();
				tContNo = tLACommisionSchema.getContNo();
				payyear = tLACommisionSchema.getPayYear();
				renewcount = tLACommisionSchema.getReNewCount();
				payyears = tLACommisionSchema.getPayYears();
				String tTransState = tLACommisionSchema.getTransState();
				String tWrapCode = tLACommisionSchema.getF3();
				String tTmakedate = tLACommisionSchema.getTMakeDate();
				String tGrpContNo = tLACommisionSchema.getGrpContNo();
				String tGrpPolNo = tLACommisionSchema.getGrpPolNo();
				String tPolNo = tLACommisionSchema.getPolNo();
				tPayIntv = tLACommisionSchema.getPayIntv();
				LAChargeSchema tLAChargeSchema = new LAChargeSchema();
				tLAChargeSchema.setAgentCom(tLACommisionSchema.getAgentCom());
				tLAChargeSchema.setBranchType(tLACommisionSchema
						.getBranchType());
				tLAChargeSchema.setBranchType2(tLACommisionSchema
						.getBranchType2());
				tLAChargeSchema.setCalDate(tLACommisionSchema.getCalDate());
				tLAChargeSchema.setTCommisionSN(tLACommisionSchema
						.getCommisionSN());// 新增字段存储commisionsn
				tLAChargeSchema.setContNo(tLACommisionSchema.getContNo());
				tLAChargeSchema.setGrpContNo(tLACommisionSchema.getGrpContNo());
				tLAChargeSchema.setPolNo(tLACommisionSchema.getPolNo());
				tLAChargeSchema.setGrpPolNo(tLACommisionSchema.getGrpPolNo());
				tLAChargeSchema.setManageCom(tLACommisionSchema.getManageCom());
				tLAChargeSchema.setMainPolNo(tLACommisionSchema.getMainPolNo());
				tLAChargeSchema.setPayCount(tLACommisionSchema.getPayCount());
				tLAChargeSchema.setTransMoney(tLACommisionSchema
						.getTransMoney()); // 实收保费
				tLAChargeSchema.setTransType(tLACommisionSchema.getTransType());
				tLAChargeSchema.setRiskCode(tLACommisionSchema.getRiskCode());
				tLAChargeSchema.setReceiptNo(tLACommisionSchema.getReceiptNo());
				tLAChargeSchema.setWageNo(tLACommisionSchema.getWageNo());
				// 因自动批处理，无须业务验证，直接置为结算状态
				tLAChargeSchema.setChargeState("1");
				tLAChargeSchema.setMakeDate(CurrentDate);
				tLAChargeSchema.setMakeTime(CurrentTime);
				tLAChargeSchema.setModifyDate(CurrentDate);
				tLAChargeSchema.setModifyTime(CurrentTime);
				tLAChargeSchema.setOperator("000");
				tLAChargeSchema.setPayIntv(tLACommisionSchema.getPayIntv());
				tLAChargeSchema.setTMakeDate(tLACommisionSchema.getTMakeDate());

				tReflections.transFields(businessLAChargeSchema,
						tLAChargeSchema);
				tReflections.transFields(manageLAChargeSchema, tLAChargeSchema);
				// 业务手续费的处理
				businessLAChargeSchema.setChargeType("55"); // 互动中介业务手续费类型
				double chargeRate = calculate(tRiskCode, tAgentCom, tContNo,
						tGrpContNo, payyear, renewcount, payyears, tTransState,
						tPayIntv, tWrapCode, tTmakedate, tGrpPolNo, tPolNo,
						"01");
				businessLAChargeSchema.setChargeRate(chargeRate);
				double charge = 0;
				charge = mulrate(chargeRate, tLACommisionSchema.getTransMoney());
				businessLAChargeSchema.setCharge(charge);
				businessLAChargeSchema.setCommisionSN(tLACommisionSchema
						.getCommisionSN());

				// 管理手续费的处理
				manageLAChargeSchema.setChargeType("56"); // 互动中介管理手续费类型
				double manageChargeRate = calculate(tRiskCode, tAgentCom,
						tContNo, tGrpContNo, payyear, renewcount, payyears,
						tTransState, tPayIntv, tWrapCode, tTmakedate,
						tGrpPolNo, tPolNo, "02");
				manageLAChargeSchema.setChargeRate(manageChargeRate);
				double manageCharge = 0;
				manageCharge = mulrate(manageChargeRate, charge);
				manageLAChargeSchema.setCharge(manageCharge);
				String manageCommisionSN = tLACommisionSchema.getCommisionSN()
						.substring(0, 9) + "1";
				manageLAChargeSchema.setCommisionSN(manageCommisionSN);
				mLAChargeSet.add(businessLAChargeSchema);
				mLAChargeSet.add(manageLAChargeSchema);

				// 生成实付数据
//				LJAGetSchema tLJAGetSchema = new LJAGetSchema();
//				LJAGetSchema manageLJAGetSchema = new LJAGetSchema();
//				LJAGetSchema businessLJAGetSchema = new LJAGetSchema();
//				String tSQL = "select * from lacommision "
//						+ "where receiptno='"
//						+ businessLAChargeSchema.getReceiptNo() + "'"
//						+ " and  commisionsn='"
//						+ businessLAChargeSchema.getTCommisionSN() + "' "
//						+ " fetch first 1 rows only ";
//				tLACommisionSet = tLACommisionDB.executeQuery(tSQL);
//				// 业务手续费实付
//				businessLJAGetSchema.setActuGetNo(manageLAChargeSchema
//						.getCommisionSN());
//				businessLJAGetSchema.setOtherNoType("BC");
//				businessLJAGetSchema.setPayMode("12");
//				businessLJAGetSchema.setManageCom(businessLAChargeSchema
//						.getManageCom());
//				businessLJAGetSchema.setAgentCom(tLACommisionSet.get(1)
//						.getAgentCom());
//				businessLJAGetSchema.setGetNoticeNo(businessLAChargeSchema
//						.getCommisionSN());
//				businessLJAGetSchema.setAgentType(tLACommisionSet.get(1)
//						.getAgentType());
//				businessLJAGetSchema.setAgentCode(tLACommisionSet.get(1)
//						.getAgentCode());
//				businessLJAGetSchema.setAgentGroup(tLACommisionSet.get(1)
//						.getAgentGroup());
//				businessLJAGetSchema.setAppntNo(tLACommisionSet.get(1)
//						.getAppntNo());
//				businessLJAGetSchema.setSumGetMoney(businessLAChargeSchema
//						.getCharge());
//				businessLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet
//						.get(1).getAgentCom()));
//				businessLJAGetSchema.setDrawerID(tLACommisionSet.get(1)
//						.getAgentCom());
//				businessLJAGetSchema.setShouldDate(CurrentDate);
//				businessLJAGetSchema.setOperator(businessLAChargeSchema
//						.getOperator());
//				businessLJAGetSchema.setMakeDate(CurrentDate);
//				businessLJAGetSchema.setMakeTime(CurrentTime);
//				businessLJAGetSchema.setModifyDate(CurrentDate);
//				businessLJAGetSchema.setModifyTime(CurrentTime);
//				// 管理手续费实付
//				manageLJAGetSchema.setActuGetNo(manageLAChargeSchema
//						.getCommisionSN());
//				manageLJAGetSchema.setOtherNoType("BC");
//				manageLJAGetSchema.setPayMode("12");
//				manageLJAGetSchema.setManageCom(manageLAChargeSchema
//						.getManageCom());
//				manageLJAGetSchema.setAgentCom(tLACommisionSet.get(1)
//						.getAgentCom());
//				manageLJAGetSchema.setGetNoticeNo(manageLAChargeSchema
//						.getCommisionSN());
//				manageLJAGetSchema.setAgentType(tLACommisionSet.get(1)
//						.getAgentType());
//				manageLJAGetSchema.setAgentCode(tLACommisionSet.get(1)
//						.getAgentCode());
//				manageLJAGetSchema.setAgentGroup(tLACommisionSet.get(1)
//						.getAgentGroup());
//				manageLJAGetSchema.setAppntNo(tLACommisionSet.get(1)
//						.getAppntNo());
//				manageLJAGetSchema.setSumGetMoney(manageLAChargeSchema
//						.getCharge());
//				manageLJAGetSchema.setDrawer(getAgentComName(tLACommisionSet
//						.get(1).getAgentCom()));
//				manageLJAGetSchema.setDrawerID(tLACommisionSet.get(1)
//						.getAgentCom());
//				manageLJAGetSchema.setShouldDate(CurrentDate);
//				manageLJAGetSchema.setOperator(manageLAChargeSchema
//						.getOperator());
//				manageLJAGetSchema.setMakeDate(CurrentDate);
//				manageLJAGetSchema.setMakeTime(CurrentTime);
//				manageLJAGetSchema.setModifyDate(CurrentDate);
//				manageLJAGetSchema.setModifyTime(CurrentTime);
//				this.mLJAGetSet.add(businessLJAGetSchema);
//				this.mLJAGetSet.add(manageLJAGetSchema);

			}
			mMap.put(this.mLAChargeSet, "INSERT");
//			mMap.put(this.mLJAGetSet, "INSERT");
			try {
				VData tVData = new VData();
				tVData.add(mMap);
				System.out.println("开始提交数据");
				PubSubmit tPubSubmit = new PubSubmit();
				tPubSubmit.submitData(tVData, "");
				System.out.println("数据提交完成");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} while (tLACommisionSet.size() > 0);
		System.out.println("AgentChargeCalSaveBL-->dealData:执行结束 ");
		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		mBranchType = (String) cInputData.getObject(1);
		mBranchType2 = (String) cInputData.getObject(2);
		mManageCom = (String) cInputData.getObject(3);
		mMakeDate = (String) cInputData.getObject(4);
		if ("".equals(mBranchType) || "".equals(mBranchType2)
				|| "".equals(mManageCom) || "".equals(mMakeDate)) {
			System.out
					.println("手续费自动计算获取要素信息失败branchtype--branchtype2--mManagecom--mMakeDate:"
							+ mBranchType
							+ "--"
							+ mBranchType2
							+ "--"
							+ mManageCom + "--" + mMakeDate);
			CError tError = new CError();
			tError.moduleName = "AgentWageCalSaveUI";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 判断此数据是否要计算手续费
	 * 
	 * @param cLACommisionSchema
	 *            要要计算的手续费
	 * @return
	 */
	private boolean checkCalFlag(LACommisionSchema cLACommisionSchema) {
		String crs_chnl = "";
		String crs_bussType = "";
		String tBranchType3 = cLACommisionSchema.getBranchType3();
		if ("N".equals(tBranchType3)) {
			return true;
		}
		if (!"00000000000000000000".equals(cLACommisionSchema.getGrpContNo())) {
			LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
			LCGrpContDB tLCGrpContDB = new LCGrpContDB();
			tLCGrpContDB.setGrpContNo(cLACommisionSchema.getGrpContNo());
			if (tLCGrpContDB.getInfo()) {
				tLCGrpContSchema = tLCGrpContDB.getSchema();
				crs_chnl = tLCGrpContSchema.getCrs_SaleChnl();
				crs_bussType = tLCGrpContSchema.getCrs_BussType();
			} else {
				LBGrpContSchema tLBGrpContSchema = new LBGrpContSchema();
				LBGrpContDB tLBGrpContDB = new LBGrpContDB();
				tLBGrpContDB.setGrpContNo(cLACommisionSchema.getGrpContNo());
				if (tLBGrpContDB.getInfo()) {
					tLBGrpContSchema = tLBGrpContDB.getSchema();
					crs_chnl = tLBGrpContSchema.getCrs_SaleChnl();
					crs_bussType = tLBGrpContSchema.getCrs_BussType();
				}
			}
		} else {
			LCContSchema tLCContSchema = new LCContSchema();
			LCContDB tLCContDB = new LCContDB();
			tLCContDB.setContNo(cLACommisionSchema.getContNo());
			if (tLCContDB.getInfo()) {
				tLCContSchema = tLCContDB.getSchema();
				crs_chnl = tLCContSchema.getCrs_SaleChnl();
				crs_bussType = tLCContSchema.getCrs_BussType();
			} else {
				LBContSchema tLBContSchema = new LBContSchema();
				LBContDB tLBContDB = new LBContDB();
				tLBContDB.setContNo(cLACommisionSchema.getContNo());
				if (tLBContDB.getInfo()) {
					tLBContSchema = tLBContDB.getSchema();
					crs_chnl = tLBContSchema.getCrs_SaleChnl();
					crs_bussType = tLBContSchema.getCrs_BussType();
				}
			}
		}
		if (!"01".equals(crs_chnl) && !"02".equals(crs_chnl)) {
			return false;
		}
		if (!"01".equals(crs_bussType)) {
			return false;
		}
		return true;
	}

	public double calculate(String cRiskCode, String cAgentCom, String cContno,
			String cGrpContNo, int cPayyear, int cRenewcount, int cPayyears,
			String tTransState, int cPayintv, String cWrapCode,
			String cTMakeDate, String cGrpPolNo, String cPolNo, String tFlag) {

		double strWageRate = 0.0;
		// LAWageCalElementSchema tLAWageCalElementSchema=new
		// LAWageCalElementSchema();
		// LAWageCalElementDB tLAWageCalElementDB=new LAWageCalElementDB();
		// tLAWageCalElementDB.setRiskCode(cRiskCode);
		// tLAWageCalElementDB.setCalType("21");
		// tLAWageCalElementDB.setBranchType("3");
		// tLAWageCalElementDB.setBranchType2("01");
		// tLAWageCalElementDB.getInfo();
		// tLAWageCalElementSchema=tLAWageCalElementDB.getSchema();
		String tGrpPolNo = cGrpPolNo;
		if ("00000000000000000000".equals(cGrpContNo)) {
			tGrpPolNo = cPolNo;
		}
		Calculator tCalculator = new Calculator(); // 计算类
		if ("01".equals(tFlag)) {
			SSRS tSSRS = new SSRS();
			String tsql = "select CalCode from LAWageCalElement where  branchtype='5'"
					+ " and branchtype2='01' and CalType='21' "
					+ " and  RiskCode='" + cRiskCode + "' ";
			ExeSQL tExeSQL = new ExeSQL();
			tSSRS = tExeSQL.execSQL(tsql);
			if (tSSRS.getMaxRow() <= 0) {
				tCalculator.setCalCode("AC9901");
			} else {
				tCalculator.setCalCode(tSSRS.GetText(1, 1));
			}
		}
		if ("02".equals(tFlag)) {
			tCalculator.setCalCode("AC9801");
		}
		// tCalculator.setCalCode(tLAWageCalElementSchema.getCalCode());
		// //添加计算编码
		// tCalculator.setCalCode("AP9901"); //添加计算编码

		tCalculator.addBasicFactor("RISKCODE", cRiskCode);
		tCalculator.addBasicFactor("CONTNO", cContno);
		tCalculator.addBasicFactor("BRANCHTYPE", this.mBranchType);
		tCalculator.addBasicFactor("BRANCHTYPE2", this.mBranchType2);
		tCalculator.addBasicFactor("AGENTCOM", cAgentCom);
		tCalculator.addBasicFactor("TRANSSTATE", tTransState);
		// 添加套餐编码
		tCalculator.addBasicFactor("WRAPCODE", cWrapCode);
		// 添加套餐编码判断是否在生效日期中
		tCalculator.addBasicFactor("TMAKEDATE", cTMakeDate);
		tCalculator.addBasicFactor("MANAGECOM", mManageCom);
		tCalculator.addBasicFactor("GRPCONTNO", cGrpContNo);
		tCalculator.addBasicFactor("RENEWCOUNT", String.valueOf(cRenewcount));
		tCalculator.addBasicFactor("PAYYEAR", String.valueOf(cPayyear));
		tCalculator.addBasicFactor("PAYYEARS", String.valueOf(cPayyears));
		tCalculator.addBasicFactor("PAYINTV", String.valueOf(cPayintv));
		tCalculator.addBasicFactor("GRPPOLNO", tGrpPolNo);

		strWageRate = Double.parseDouble(tCalculator.calculate());
		if (tCalculator.mErrors.needDealError()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ActiveAgentChargeCalNewBL";
			tError.functionName = "calWage1";
			tError.errorMessage = tCalculator.mErrors.getFirstError();
			// this.mErrors.addOneError(tError);
			return 0;
		}

		return strWageRate;
	}

	public static double mulrate(double v1, double v2) {

		BigDecimal b1 = new BigDecimal(Double.toString(v1));

		BigDecimal b2 = new BigDecimal(Double.toString(v2));

		return b1.multiply(b2).doubleValue();

	}

	private String getAgentComName(String tAgentCom) {
		String comName = "";
		ExeSQL tExeSQL = new ExeSQL();
		String sql = " select name from lacom where agentcom='" + tAgentCom
				+ "' with ur ";
		SSRS tSSRS = tExeSQL.execSQL(sql);
		comName = tSSRS.GetText(1, 1);
		return comName;
	}

	private boolean prepareLogData(String Year, String Month) {
		System.out.println("填充佣金计算日志表");
		// 填充佣金计算日志表
		LAChargeLogDB tLAChargeLogDB = new LAChargeLogDB();
		// LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
		String chargeCalNo = Year + Month;
		tLAChargeLogDB.setChargeCalNo(chargeCalNo);
		tLAChargeLogDB.setManageCom(mManageCom);
		tLAChargeLogDB.setBranchType(mBranchType);
		tLAChargeLogDB.setBranchType2(mBranchType2);
		tLAChargeLogDB.setStartDate(mMakeDate);
		tLAChargeLogDB.setEndDate(mMakeDate);
		tLAChargeLogDB.setChargeMonth(Month);
		tLAChargeLogDB.setChargeYear(Year);
		tLAChargeLogDB.setOperator("000");

		tLAChargeLogDB.setState("00");
		tLAChargeLogDB.setMakeDate(CurrentDate);
		tLAChargeLogDB.setMakeTime(CurrentTime);
		tLAChargeLogDB.setModifyDate(CurrentDate);
		tLAChargeLogDB.setModifyTime(CurrentTime);
		if (!tLAChargeLogDB.insert()) {
			return false;
		}
		return true;
	}

	public VData getResult() {
		return mResult;
	}

}
