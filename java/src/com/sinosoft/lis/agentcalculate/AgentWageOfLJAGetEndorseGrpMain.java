package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;

/**
 * 处理团单无名单增人、减人、增加保费、减少保费公共 的处理类
 * @author yangyang 2017-9-11
 *
 */
public class AgentWageOfLJAGetEndorseGrpMain extends AgentWageOfAbstractClass{

	private LACommisionErrorSchema mLACommisionErrorSchema = new LACommisionErrorSchema();
	
	private LACommisionNewSchema mLACommisionNewSchema = new LACommisionNewSchema();
	
	public boolean dealData(LJAGetEndorseSchema cLJAGetEndorseSchema,double cTransMoney,double cManageFee,double cAccuralfee,String cDataType,String cOperator)
	{
		System.out.println("AgentWageOfLJAGetEndorseGrpMain-->dealData:开始执行");
		String tAgentCode = cLJAGetEndorseSchema.getAgentCode();
	    String tGrpPolNo = cLJAGetEndorseSchema.getGrpPolNo();
	    String tGrpContNo = cLJAGetEndorseSchema.getGrpContNo();
	    String tRiskCode = cLJAGetEndorseSchema.getRiskCode();
	    //查询业务员相关信息，
	    LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tAgentCode);
	    if(null==tLAAgentSchema)
	    {
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE,cOperator);
	    	return false;
	    }
	    String tBranchCode = tLAAgentSchema.getBranchCode();
	    //查询团队相关信息
		LABranchGroupSchema tLABranchGroupSchema= new LABranchGroupSchema();
	    tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tBranchCode);
	    if(null==tLABranchGroupSchema)
	    {
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP,cOperator);
	    	return false;
	    }
	    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
	    tLCGrpPolSchema = mAgentWageCommonFunction.queryLCGrpPol(tGrpPolNo);
	    if (tLCGrpPolSchema == null) 
	    {
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_LCGRPPOL,cOperator);
	    	return false;
	    }
	    //System.out.println("集体保单号：" + tGrpPolNo);
	    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	    tLCGrpContSchema = mAgentWageCommonFunction.queryLCGrpCont(tGrpContNo);
		if(null == tLCGrpContSchema)
		{
			mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_LCGRPCONT,cOperator);
			return false;
		}
		//对于互动的处理
	    String tSaleChnl = tLCGrpContSchema.getSaleChnl();
	    String tBranchType3 ="";
	    if("14".equals(tSaleChnl)||"15".equals(tSaleChnl))
	    {
	    	String tCrs_SaleChnl = tLCGrpContSchema.getCrs_SaleChnl();
	    	String tCrs_BussType = tLCGrpContSchema.getCrs_BussType();
	    	tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
	    }
	    if("no".equals(tBranchType3))
		{
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_ACTIVE,cOperator);
			return false;
		}
	    String[] tBranchTypes = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
	    if(null == tBranchTypes||0==tBranchTypes.length)
	    {
	    	mLACommisionErrorSchema = dealErrorData(cLJAGetEndorseSchema,AgentWageOfCodeDescribe.ERRORCODE_SALECHNL,cOperator);
			return false;
	    }
	    String tBranchType = tBranchTypes[0];
	    String tBranchType2 = tBranchTypes[1];
	    
		mLACommisionNewSchema = mAgentWageCommonFunction.packageGrpContDataOfLACommisionNew(tLCGrpContSchema, tLCGrpPolSchema);
		int tRenewCount = mAgentWageCommonFunction.queryLCRnewPol(tGrpPolNo,"grppolno");
		//获取最大commision
	    String tCommisionsn = mAgentWageCommonFunction.getCommisionSN();
	    mLACommisionNewSchema.setCommisionSN(tCommisionsn);
	    mLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
	    mLACommisionNewSchema.setBranchType(tBranchType);
	    mLACommisionNewSchema.setBranchType2(tBranchType2);
	    mLACommisionNewSchema.setBranchType3(tBranchType3);
	    mLACommisionNewSchema.setReNewCount(tRenewCount);
	    mLACommisionNewSchema.setAgentGroup(tLAAgentSchema.getAgentGroup());
	    mLACommisionNewSchema.setBranchCode(tBranchCode);
	    mLACommisionNewSchema.setBranchSeries(tLABranchGroupSchema.getBranchSeries());
	    mLACommisionNewSchema.setBranchAttr(tLABranchGroupSchema.getBranchAttr());
	    mLACommisionNewSchema.setP7(cManageFee);
	    mLACommisionNewSchema.setP8(cAccuralfee);
	    mLACommisionNewSchema.setEndorsementNo(cLJAGetEndorseSchema.getEndorsementNo());
	    mLACommisionNewSchema.setManageCom(cLJAGetEndorseSchema.getManageCom());
	    mLACommisionNewSchema.setRiskCode(tRiskCode);
	    mLACommisionNewSchema.setDutyCode(cLJAGetEndorseSchema.getDutyCode());
	    mLACommisionNewSchema.setPayPlanCode(cLJAGetEndorseSchema.getPayPlanCode());
	    mLACommisionNewSchema.setReceiptNo(cLJAGetEndorseSchema.getActuGetNo());
	    mLACommisionNewSchema.setTPayDate(cLJAGetEndorseSchema.getGetDate());
	    mLACommisionNewSchema.setTEnterAccDate(cLJAGetEndorseSchema.getEnterAccDate());
	    mLACommisionNewSchema.setTConfDate(cLJAGetEndorseSchema.getGetConfirmDate());
	    mLACommisionNewSchema.setTMakeDate(cLJAGetEndorseSchema.getMakeDate());
	    mLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
	    mLACommisionNewSchema.setTransMoney(cTransMoney);
	    mLACommisionNewSchema.setTransState("00");
	    mLACommisionNewSchema.setTransStandMoney(cTransMoney);
	    mLACommisionNewSchema.setCurPayToDate(cLJAGetEndorseSchema.getGetDate());
	    mLACommisionNewSchema.setCommDire("1");
	    //无名单减人 14
	    if("14".equals(cDataType))
	    {
	    	mLACommisionNewSchema.setTransType("WJ");
	    	mLACommisionNewSchema.setFlag("0");
	    }
	    //无名单增人  13
	    else if("13".equals(cDataType))
	    {
	    	mLACommisionNewSchema.setTransType("WZ");
	    	mLACommisionNewSchema.setFlag("2");
	    }
	    else if("02".equals(cDataType))
	    {
	    	mLACommisionNewSchema.setTransType(cLJAGetEndorseSchema.getFeeOperationType());
	    	mLACommisionNewSchema.setFlag("2");
	    }
	    else if("15".equals(cDataType))
	    {
	    	mLACommisionNewSchema.setTransType(cLJAGetEndorseSchema.getFeeOperationType());
	    	mLACommisionNewSchema.setFlag("0");
	    }
	    else if("16".equals(cDataType))
	    {
	    	 //团单减少保费保全项目(不扣提奖)
	    	mLACommisionNewSchema.setTransType(cLJAGetEndorseSchema.getFeeOperationType());
	    	mLACommisionNewSchema.setFlag("0");//退一部分件
	    	mLACommisionNewSchema.setCommDire("1");//
	    	mLACommisionNewSchema.setF2("01");//不计算FYC，表示AgentWageCalDoNewBL计算提奖时不再计算此数据
	    }
	    if(tLCGrpPolSchema.getSaleChnl().equals("07"))
	    {
	    	mLACommisionNewSchema.setF1("02");//职团开拓
	    }
	    else
	    {
	    	mLACommisionNewSchema.setF1("01");
	    }
	    mLACommisionNewSchema.setPayYear(0);
	    mLACommisionNewSchema.setPayYears(mAgentWageCommonFunction.getPayYears(tGrpPolNo));
	    mLACommisionNewSchema.setPayCount(1);
	    mLACommisionNewSchema.setAgentCom(cLJAGetEndorseSchema.getAgentCom());
	    mLACommisionNewSchema.setAgentType(cLJAGetEndorseSchema.getAgentType());
	    mLACommisionNewSchema.setAgentCode(cLJAGetEndorseSchema.getAgentCode());
	    String tScanDate = mAgentWageCommonFunction.getScanDate(tLCGrpPolSchema.getPrtNo()); 
	    mLACommisionNewSchema.setScanDate(tScanDate);
	    mLACommisionNewSchema.setOperator(cOperator);
	    mLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
	    mLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
	    mLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
	    mLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
	    //套餐赋值
	    String  tWrapCode = "";
	    if("2".equals(tLCGrpContSchema.getCardFlag()))
	    {
	    	tWrapCode = mAgentWageCommonFunction.getWrapCode(cLJAGetEndorseSchema.getGrpContNo(), cLJAGetEndorseSchema.getRiskCode());
	    }
	    mLACommisionNewSchema.setF3(tWrapCode);
	    String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLCGrpPolSchema.getGrpContNo(),
	    		cLJAGetEndorseSchema.getFeeOperationType(),cLJAGetEndorseSchema.getMakeDate(),
	    		cLJAGetEndorseSchema.getGetConfirmDate(),cLJAGetEndorseSchema.getActuGetNo());
	    //用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
	    String tAFlag =tConfDates[0];
	    //财务确认日期 ，团单会用到此字段
	    String tConfDate = tConfDates[1];
	    //薪资月算法相关字段处理
	    String tVisitTime  = mAgentWageCommonFunction.queryReturnVisit(tGrpContNo);
	    String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
	    String tFlag = "02";
	    //计算薪资月的时候，传的paycount是0，但实际上是1，此处是为了能调用公共的薪资月方法(原来逻辑中是用一个变量，传的是0)
	    mLACommisionNewSchema.setPayCount(0);
	    String tCalDate = mAgentWageCommonFunction.calCalDateForAll(mLACommisionNewSchema, tConfDate, tFlag, tAFlag, tVisitTime, tRiskFlag);
	    mLACommisionNewSchema.setCalDate(tCalDate);
	    if(!"".equals(tCalDate)&&null!=tCalDate)
	    {
	    	String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate);
	    	mLACommisionNewSchema.setWageNo(tWageNo);
	    }
	    mLACommisionNewSchema.setPayCount(1);
	    System.out.println("AgentWageOfLJAGetEndorseGrpMain-->dealData:执行结束 ");
	    return true;
	}

	public LACommisionErrorSchema getmLACommisionErrorSchema() {
		return mLACommisionErrorSchema;
	}

	public LACommisionNewSchema getmLACommisionNewSchema() {
		return mLACommisionNewSchema;
	}
}
