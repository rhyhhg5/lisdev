package com.sinosoft.lis.agentcalculate;


/**
 * 定义所有目前销售扎账提数中错误代码及信息的处理
 * @author yangyang
 *
 */
public class AgentWageOfCodeDescribe {

	/**业务员查询不到对应错误的代码*/
	public static final String ERRORCODE_AGENTCODE = "E01";
	
	/**业务员查询不到对应错误的描述*/
	public static final String ERRORMESSAGE_AGENTCODE = "业务员查询不到";
	
	/**业务员所在团队查询不到对应错误的代码*/
	public static final String ERRORCODE_AGENTGROUP = "E02";
	
	/**业务员所在团队查询不到对应错误的描述*/
	public static final String ERRORMESSAGE_AGENTGROUP = "业务员所在团队查询不到";
	
	/**个人保单在保单表查询不到对应错误的代码*/
	public static final String ERRORCODE_LCCONT = "E03";
	
	/**个人保单在保单表查询对应错误的描述*/
	public static final String ERRORMESSAGE_LCCONT = "个人保单在保单表LCCONT查询不到";
	
	/**个人保单在保单险种表查询不到对应错误的代码*/
	public static final String ERRORCODE_LCPOL = "E04";
	
	/**个人保单在保单险种表查询不到对应错误的描述*/
	public static final String ERRORMESSAGE_LCPOL = "个人保单在保单险种表LCPOL查询不到";
	
	/**团体保单在保单表查询不到对应错误的代码*/
	public static final String ERRORCODE_LCGRPCONT = "05";
	
	/**团体保单在保单表查询对应错误的描述*/
	public static final String ERRORMESSAGE_LCGRPCONT = "团体保单在保单表LCGRPCONT查询不到";
	
	/**团体保单在保单险种表查询不到对应错误的代码*/
	public static final String ERRORCODE_LCGRPPOL = "E06";
	
	/**团体保单在保单险种表查询不到对应错误的描述*/
	public static final String ERRORMESSAGE_LCGRPPOL = "团体保单在保单险种表LCGRPPOL查询不到";
	
	/**互动渠道不提取数据对应错误的代码*/
	public static final String ERRORCODE_ACTIVE = "E07";
	
	/**互动渠道不提取数据对应错误的描述*/
	public static final String ERRORMESSAGE_ACTIVE = "此保单互动渠道不提取";
	
	/**保单渠道找不到展业类型对应错误的代码*/
	public static final String ERRORCODE_SALECHNL = "E08";
	
	/**保单渠道找不到展业类型对应错误的描述*/
	public static final String ERRORMESSAGE_SALECHNL = "保单渠道找不到对应的展业类型";
	
	/**保单找不到对应的中介机构的代码*/
	public static final String ERRORCODE_AGENTCOM = "E09";
	
	/**保单找不到对应的中介机构的描述*/
	public static final String ERRORMESSAGE_AGENTCOM = "保单找不到对应的中介机构";
	
	/**保单团队外部编码找不到的代码*/
	public static final String ERRORCODE_BRANCHATTR = "E10";
	
	/**保单团队外部编码找不到的描述*/
	public static final String ERRORMESSAGE_BRANCHATTR = "保单团队外部编码找不到";
	
	/**团单的保单交至日期 为空的代码*/
	public static final String ERRORCODE_CURRPAYTODATE = "E11";
	
	/**团单的保单交至日期 为空的描述*/
	public static final String ERRORMESSAGE_CURRPAYTODATE = "团单的保单交至日期 为空";
	
	/**保全类型错误代码*/
	public static final String ERRORCODE_ENDORTYPE = "E12";
	
	/**保全类型错误描述*/
	public static final String ERRORMESSAGE_ENDORTYPE = "保全类型错误";
	
	/**减人非导入保费且计算提奖时，查询对应的值 为空  对应的错误代码 */
	public static final String ERRORCODE_GRPCT01 = "E13";
	
	/**减人非导入保费且计算提奖时，查询对应的值 为空错误描述*/
	public static final String ERRORMESSAGE_GRPCT01 = "减人非导入保费且计算提奖时，查询对应的值 为空";
	
	/**扎帐提数提过后不再提取代码*/
	public static final String ERRORCODE_HASPROVIDED = "E14";
	
	/**扎帐提数提过后不再提取描述*/
	public static final String ERRORMESSAGE_HASPROVIDED = "扎帐提数提过后不再提取";
	
	/**扎账提数表*/
	public static final String TABLENAME_LACOMMISION = "LACOMMISION";
	
	/**个单实收表*/
	public static final String TABLENAME_LJAPAYPERSON = "LJAPAYPERSON";
	
	/**保全实收表*/
	public static final String TABLENAME_LJAGETENDORSE = "LJAGETENDORSE";
	
	/**团单实收表*/
	public static final String TABLENAME_LJAPAYGRP = "LJAPAYGRP";
	
	/**批处理提取天数对应参数的名称（TMakeDate） */
	public static final String WAGENO_TAMKEDATE ="TMakeDate";
		
	/**批处理提取用户信息对应的参数名称（GlobalInput） */
	public static final String WAGENO_GLOBALINPUT ="GlobalInput";
	
	/**批处理提取传递参数容器对应的参数名称（TransferData） */
	public static final String WAGENO_TRANSFERDATA ="TransferData";
	
	/**批处理提取管理机构对应参数的名称（ManageCom） */
	public static final String WAGENO_MANAGECOM ="ManageCom";
	
	/**批处理提取团单号对应参数的名称（GrpContNo‘） */
	public static final String WAGENO_GRPCONTNO ="GrpContNo";

	/**批处理提取个单号对应参数的名称（ContNo） */
	public static final String WAGENO_CONTNO ="ContNo";
	
	/**字符串返回值为空时，默认为：ISNULL*/
	public static final String RETURN_STRING = "ISNULL";
	
	/**数字返回值为空时，默认为：-001*/
	public static final int RETURN_INTEGER = -001;
	
	/**用来判断是不是团单和个单的条件*/
	public static final String JUDGE_GRPCONTNO = "00000000000000000000";
	
	/**用来判断追加保费的犹豫期退保*/
	public static final String JUDGE_CEASE = "222222";

}
