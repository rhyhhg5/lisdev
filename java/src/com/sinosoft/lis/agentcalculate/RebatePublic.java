package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 打折处理公共类</p>
 * <p>Description: 打折处理公共类</p>
 * <p>Copyright: Copyright (c) sinosoft 2005</p>
 * <p>Company: sinosoft</p>
 * @xijiahui
 * @version 1.0
 */
public abstract class RebatePublic
{
    public final static String RECALFYCFLAG = "RECALFYC||AGENTWAGE";
    public final static String CALFLAG = "INSERT||AGENTWAGE";
    public CErrors mErrors = new CErrors();
    protected LACommisionSet mSet = new LACommisionSet();
    protected LACommisionSet mNewSet = new LACommisionSet();
    protected String mAgentGrade;
    protected float rate = 1; //默认打折使用1，不打折
    protected VData tResult = new VData();
    protected String mOperate;
    public RebatePublic()
    {
    }

    //提交给该类，进行打折处理
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;
        if (!getInputData(cInputData))
            return false;
        if (!check())
            return false;
        if (mOperate.equals(this.CALFLAG))
        {
            if (!dealData())
                return false;
        }
        else
        {
            if (!dealData1())
                return false;
        }
        return true;
    }

    //校验数据的正确性
    protected abstract boolean check();

    //进行业务处理,将mSet中的数据打折后放入mNewSet
    protected abstract boolean dealData();

    protected abstract boolean dealData1();

    // 获得打折比例
    protected abstract float getRate();

    //获取传入的数据，可以在子类中覆盖
    protected boolean getInputData(VData cInputData)
    {
        System.out.println("getInputData");
        this.mSet = (LACommisionSet) cInputData.getObjectByObjectName(
                "LACommisionSet", 0);
        this.mAgentGrade = (String) cInputData.getObjectByObjectName(
                "String", 0);
        if (mSet == null)
        {
            // @@错误处理
            System.out.println("is null");
            CError tError = new CError();
            tError.moduleName = "RebatePublic";
            tError.functionName = "getInputData";
            tError.errorMessage = "未取得足够的数据。";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("not null");
        return true;
    }

    //调用此方法将结果返回调用程序
    public VData getResult()
    {
        tResult.add(mNewSet);
        return tResult;
    }

    //出错处理
    protected void buildError(String FuncName, String ErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "RebatePublic";
        cError.functionName = FuncName;
        cError.errorMessage = ErrMsg;
        this.mErrors.addOneError(cError);
    }
}
