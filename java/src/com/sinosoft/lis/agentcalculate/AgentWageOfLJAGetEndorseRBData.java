package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

public class AgentWageOfLJAGetEndorseRBData extends AgentWageOfAbstractClass 
{

	/**
	 * 主要提数逻辑
	 * 
	 * 
	 */
      public boolean dealData(String cSQL,String cOperator)
      {
    	  System.out.println("*****************犹豫期撤保保全回退AgentWageOfLJAGetEndorseRBData dealData 开始***************");
    	  LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
    	  RSWrapper tRSWrapper = new RSWrapper();
    	  tRSWrapper.prepareData(tLJAGetEndorseSet, cSQL);
		  
    	  //用来判断数据是不是为空，为了防止 最后一次循环时，为空时，也打印错误语句  
  		  boolean tJudgeNullFlag = false;
    	  
    	  do 
		  {
			  MMap tMMap = new MMap();
			  tRSWrapper.getData();
			  if(!tJudgeNullFlag)
			  {
				  if(null == tLJAGetEndorseSet || 0==tLJAGetEndorseSet.size())
				  {
					  System.out.println("AgentWageOfLJAGetEndorseWTCTData.java-->dealData 没有满足条件的数据");
					  return true;
				  }
				  tJudgeNullFlag = true;
			  }
			  LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
			  LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
			  for (int i = 1; i <= tLJAGetEndorseSet.size(); i++)
			  {
				  LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
				  LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
				  tLJAGetEndorseSchema = tLJAGetEndorseSet.get(i);
				  String tAgentCode = tLJAGetEndorseSchema.getAgentCode();
				  String tGrpContNo = tLJAGetEndorseSchema.getGrpContNo();
				  String tPolNo = tLJAGetEndorseSchema.getPolNo();
				  String tContNo = tLJAGetEndorseSchema.getContNo();
				  String tGrpPolNo = tLJAGetEndorseSchema.getGrpPolNo();
				  String tEndorsementNo = tLJAGetEndorseSchema.getEndorsementNo();
				  String tRiskCode = tLJAGetEndorseSchema.getRiskCode();
				  //查询追加类型
				  String tType = mAgentWageCommonFunction.getTransType(tEndorsementNo);
				  
				  LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tAgentCode);
				  if(null == tLAAgentSchema)
				  {
					  LACommisionErrorSchema tLACommisionErrorSchema = 
							  dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE, cOperator);
					  tLACommisionErrorSet.add(tLACommisionErrorSchema);
					  continue;
				  }
				  String tAgentGroup = tLAAgentSchema.getAgentGroup();
				  LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
				  tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tAgentGroup);
				  if(null == tLABranchGroupSchema)
				  {
					   LACommisionErrorSchema tLACommisionErrorSchema = 
							   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP, cOperator);
					   tLACommisionErrorSet.add(tLACommisionErrorSchema);
					   continue;
				  }
				  //团队编码 
				  String tBranchCode = tLAAgentSchema.getBranchCode();
				  LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
				  tLABranchGroupDB.setAgentGroup(tBranchCode);
				  tLABranchGroupDB.getInfo();
				  String tBranchAttr = tLABranchGroupDB.getBranchAttr();
				  if(null == tBranchAttr || "".equals(tBranchAttr))
				  {
					   LACommisionErrorSchema tLACommisionErrorSchema = 
							   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_BRANCHATTR, cOperator);
					   tLACommisionErrorSet.add(tLACommisionErrorSchema);
					   continue;
				  }
				  
				  //对于个单处理
				  if(tGrpContNo.equals(AgentWageOfCodeDescribe.JUDGE_GRPCONTNO))
				  {
					  if(!tType.equals("WT"))
					  {
						  LACommisionErrorSchema tLACommisionErrorSchema = 
								  dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_ENDORTYPE, cOperator);
						  tLACommisionErrorSet.add(tLACommisionErrorSchema);
						  continue;
					  }
					  LCContSchema tLCContSchema = mAgentWageCommonFunction.queryLCCont(tContNo);
					  if(null == tLCContSchema)
					  {
						  LACommisionErrorSchema tLACommisionErrorSchema = 
								  dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCCONT, cOperator);
						  tLACommisionErrorSet.add(tLACommisionErrorSchema);
						  continue;
					  }
					  LCPolSchema tLCPolSchema = mAgentWageCommonFunction.queryLCPol(tPolNo);
					  if(null == tLCPolSchema)
					  {
						  LACommisionErrorSchema tLACommisionErrorSchema = 
								  dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCPOL, cOperator);
						  tLACommisionErrorSet.add(tLACommisionErrorSchema);
						  continue;
					  }
					   
					   //对于互动的处理
					   String tSaleChnl = tLCContSchema.getSaleChnl();
					   String tBranchType3="";
					   if("15".equals(tSaleChnl)||"14".equals(tSaleChnl))
					   {
						   String tCrs_SaleChnl = tLCContSchema.getCrs_SaleChnl();
				           String tCrs_BussType = tLCContSchema.getCrs_BussType();
				           tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
				           
					   }
					   if("no".equals(tBranchType3))
					   {
						   LACommisionErrorSchema tLACommisionErrorSchema  = 
								   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_ACTIVE, cOperator);
						   tLACommisionErrorSet.add(tLACommisionErrorSchema);
						   continue;
					   }
					   if("03".equals(tSaleChnl))
					   {
						   String tAgentCom = tLJAGetEndorseSchema.getAgentCom();
						   LAComSchema tLAComSchema = mAgentWageCommonFunction.querytLACom(tAgentCom);
						   if(null == tLAComSchema)
						   {
							   LACommisionErrorSchema tLACommisionErrorSchema = 
									   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCOM, cOperator);
							   tLACommisionErrorSet.add(tLACommisionErrorSchema);
							   continue;
						   }
					   }
					   String [] tBranchTypes = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
					   if(null == tBranchTypes || 0 == tBranchTypes.length)
					   {
						   LACommisionErrorSchema tLACommisionErrorSchema = 
								   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_SALECHNL, cOperator);
						   tLACommisionErrorSet.add(tLACommisionErrorSchema);
						   continue;
					   }
					   if (!tRiskCode.equals("330801")&&!tRiskCode.equals("331801")
							   &&!tRiskCode.equals("332701")||tRiskCode.equals("333901")
							   ||tRiskCode.equals("334701")||tRiskCode.equals("334801")
							   ||tRiskCode.equals("370201"))
		               {
						   //续保次数
						   int tRnewCount = mAgentWageCommonFunction.queryLCRnewPol(tPolNo, "polno");
						   String tBranchType = tBranchTypes[0];
						   String tBranchType2 = tBranchTypes[1];
						   //获取主键值
						   String tCommisionSN = mAgentWageCommonFunction.getCommisionSN();
						   
						   //riskcode,agentcom 赋值路径不同，重新赋值
						   tLACommisionNewSchema = mAgentWageCommonFunction.packageContDataOfLACommisionNew(tLCContSchema, tLCPolSchema);
						   tLACommisionNewSchema.setCommisionSN(tCommisionSN);
						   tLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
						   tLACommisionNewSchema.setBranchType(tBranchType);
						   tLACommisionNewSchema.setBranchType2(tBranchType2);
						   tLACommisionNewSchema.setBranchType3(tBranchType3);
						   tLACommisionNewSchema.setReNewCount(tRnewCount);
						   
						   tLACommisionNewSchema.setLastPayToDate(tLCPolSchema.getPaytoDate());
						   
						   //判断薪资月日期时会用到
						   tLACommisionNewSchema.setTMakeDate(tLJAGetEndorseSchema.getMakeDate());
						   tLACommisionNewSchema.setManageCom(tLJAGetEndorseSchema.getManageCom());
						   tLACommisionNewSchema.setTransState("00");//正常保单
						   tLACommisionNewSchema.setAgentCode(tAgentCode);
						   tLACommisionNewSchema.setRiskCode(tRiskCode);
						   
						   if(tLCContSchema.getSaleChnl().equals("07"))
			                {
							   tLACommisionNewSchema.setF1("02");//职团开拓
							   tLACommisionNewSchema.setP5( tLJAGetEndorseSchema.getGetMoney());//职团开拓实收保费
			                }
			                else
			                {
			                	tLACommisionNewSchema.setF1("01");
			                }
						   tLACommisionNewSchema.setFlag("1");//犹豫期撤件
						   
						   String tScanDate = mAgentWageCommonFunction.getScanDate(tLCPolSchema.getPrtNo());
						   tLACommisionNewSchema.setScanDate(tScanDate);
					       
					       //计算薪资月的时候，传的paycount是0，但实际上是1，此处是为了能调用公共的薪资月方法
				           tLACommisionNewSchema.setPayCount(0);
				           String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLCPolSchema.getGrpContNo(),"WT",tLACommisionNewSchema.getTMakeDate(),
				                    tLJAGetEndorseSchema.getGetConfirmDate(),tLJAGetEndorseSchema.getActuGetNo());
				           //用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
				           String tAFlag =tConfDates[0];
				           //财务确认日期 ，团单会用到此字段
				           String tConfDate = tConfDates[1];
				           
				           //回访处理
					       String tVisistTime = mAgentWageCommonFunction.queryReturnVisit(tContNo);
					       String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
					       String tFlag ="02";
					       
					       //根据薪资月得到薪资月函数
					       String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, tFlag, tAFlag, tVisistTime, tRiskFlag);
					       if(!tCalDate.equals("") && !tCalDate.equals("0"))
					       {
					    	   String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate);
					    	   tLACommisionNewSchema.setTConfDate(tConfDate);
					    	   tLACommisionNewSchema.setWageNo(tWageNo);
					    	   tLACommisionNewSchema.setCalDate(tCalDate);
					       }
					       
					       tLACommisionNewSchema.setFlag("1");
						   
		               }
					   
				  }
				  //团单处理
				  else
				  {
					  if(!tType.equals("WT")||!tType.equals("CT"))
					  {
						  LACommisionErrorSchema tLACommisionErrorSchema = 
								  dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_ENDORTYPE, cOperator);
						  tLACommisionErrorSet.add(tLACommisionErrorSchema);
						  continue;
					  }
					  LCGrpContSchema tLCGrpContSchema = mAgentWageCommonFunction.queryLCGrpCont(tGrpContNo);
					  if(null == tLCGrpContSchema)
					  {
						  LACommisionErrorSchema tLACommisionErrorSchema = 
								  dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCGRPCONT, cOperator);
						  tLACommisionErrorSet.add(tLACommisionErrorSchema);
						  continue;
					  }
					  LCGrpPolSchema tLCGrpPolSchema = mAgentWageCommonFunction.queryLCGrpPol(tGrpPolNo);
					  if(null == tLCGrpPolSchema)
					  {
						  LACommisionErrorSchema tLACommisionErrorSchema = 
								  dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCGRPPOL, cOperator);
						  tLACommisionErrorSet.add(tLACommisionErrorSchema);
						  continue;
					  }
					  //对于互动的处理
					   String tSaleChnl = tLCGrpContSchema.getSaleChnl();
					   String tBranchType3="";
					   if("15".equals(tSaleChnl)||"14".equals(tSaleChnl))
					   {
						   String tCrs_SaleChnl = tLCGrpContSchema.getCrs_SaleChnl();
				           String tCrs_BussType = tLCGrpContSchema.getCrs_BussType();
				           tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
				           
					   }
					   if("no".equals(tBranchType3))
					   {
						   LACommisionErrorSchema tLACommisionErrorSchema  = 
								   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_ACTIVE, cOperator);
						   tLACommisionErrorSet.add(tLACommisionErrorSchema);
						   continue;
					   }
					   String [] tBranchTypes = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
					   if(null == tBranchTypes || 0 == tBranchTypes.length)
					   {
						   LACommisionErrorSchema tLACommisionErrorSchema = 
								   dealErrorData(tLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_SALECHNL, cOperator);
						   tLACommisionErrorSet.add(tLACommisionErrorSchema);
						   continue;
					   }
					  
					   //获取续保次数
					   int tRenewCount = mAgentWageCommonFunction.queryLCRnewPol(tGrpPolNo, "grppolno");
					   String tBranchType = tBranchTypes[0];
					   String tBranchType2 = tBranchTypes[1];
					   
					   //获取主键值
					   String tCommisionSN = mAgentWageCommonFunction.getCommisionSN();
					   
					   //封装tlacommisionnew记录contno,polno重新赋值
					   tLACommisionNewSchema = mAgentWageCommonFunction.packageGrpContDataOfLACommisionNew(tLCGrpContSchema, tLCGrpPolSchema);
					   tLACommisionNewSchema.setCommisionSN(tCommisionSN);
					   //grpzb --代表团单犹豫期撤保
					   tLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
					   tLACommisionNewSchema.setReNewCount(tRenewCount);
					   tLACommisionNewSchema.setBranchType(tBranchType);
					   tLACommisionNewSchema.setBranchType2(tBranchType2);
					   tLACommisionNewSchema.setBranchType3(tBranchType3);
					   tLACommisionNewSchema.setContNo(tContNo);
					   tLACommisionNewSchema.setPolNo(tPolNo);
					   
					   //计算管理费
					   double tManageFee = mAgentWageCommonFunction.getManageFee(tPolNo, tEndorsementNo, tLJAGetEndorseSchema.getOtherNoType());
					   tLACommisionNewSchema.setP7(tManageFee);
					   
					   tLACommisionNewSchema.setFlag("2");//正常
					   
					   tLACommisionNewSchema.setPayYears(mAgentWageCommonFunction.getPayYears(tGrpPolNo));
					   
					   
					   String tScanDate = mAgentWageCommonFunction.getScanDate(tLCGrpPolSchema.getPrtNo());
					   tLACommisionNewSchema.setScanDate(tScanDate);
					  
					   //判断薪资月日期时会用到
					   tLACommisionNewSchema.setTMakeDate(tLJAGetEndorseSchema.getMakeDate());
					   tLACommisionNewSchema.setManageCom(tLJAGetEndorseSchema.getManageCom());
					   tLACommisionNewSchema.setTransState("00");//正常保单
					   tLACommisionNewSchema.setAgentCode(tAgentCode);
					   tLACommisionNewSchema.setRiskCode(tRiskCode);
					   
					   //对于套餐而言，只适用于个险，互动，健管及团险中介
			           String tF3BranchType ="1,5,7";
			           if(tF3BranchType.indexOf(tBranchType)!=-1||("2".equals(tBranchType) && "02".equals(tBranchType2)))
			           {
			        	   if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") && 
			        			   tLCGrpContSchema.getCardFlag().equals("2"))
			        	   {
			        		   tLACommisionNewSchema.setF3(mAgentWageCommonFunction.
			        				   getWrapCode(tGrpContNo, tRiskCode));
			        	   }
			        	   else
			        	   {
			        		   tLACommisionNewSchema.setF3("");
			        	   }
			           }
			           //薪资月算法相关字段处理
			           //计算薪资月的时候，传的paycount是0，但实际上是1，此处是为了能调用公共的薪资月方法
			           tLACommisionNewSchema.setPayCount(0);
			           String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tGrpContNo, tLJAGetEndorseSchema.getFeeOperationType(), tLJAGetEndorseSchema.getMakeDate(), tLJAGetEndorseSchema.getGetConfirmDate(), 
			        		   tLJAGetEndorseSchema.getActuGetNo());
			           //用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
			           String tAFlag = tConfDates[0];
			           //财务确认日期 ，团单会用到此字段
			           String tConfDate = tConfDates[1];
			           String tFlag ="02";
			           //回访要素
			           String tVisistTime = mAgentWageCommonFunction.queryReturnVisit(tGrpContNo); //保单回访时间
			           String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode); //回访险种
			          
			           String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, "02", tAFlag, tVisistTime, tRiskFlag);
					   tLACommisionNewSchema.setCalDate(tCalDate);
					   
					   if(!tCalDate.equals("") && !tCalDate.equals("0"))
					   {
						   String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate);
						   tLACommisionNewSchema.setTConfDate(tConfDate);
						   tLACommisionNewSchema.setWageNo(tWageNo);
					   }
					   tLACommisionNewSet.add(tLACommisionNewSchema);
				  }
				  //对于团单与个单而言相同的值在这处理
				  tLACommisionNewSchema.setEndorsementNo(tEndorsementNo);
				  tLACommisionNewSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
				  tLACommisionNewSchema.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
				  tLACommisionNewSchema.setReceiptNo(tLJAGetEndorseSchema.getActuGetNo());
				  
				  tLACommisionNewSchema.setTPayDate(tLJAGetEndorseSchema.getGetDate());
				  tLACommisionNewSchema.setTEnterAccDate(tLJAGetEndorseSchema.getEnterAccDate());
				  tLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
				  
				  tLACommisionNewSchema.setTransMoney(tLJAGetEndorseSchema.getGetMoney());
				  tLACommisionNewSchema.setTransStandMoney(tLJAGetEndorseSchema.getGetMoney());
				  tLACommisionNewSchema.setCurPayToDate(tLJAGetEndorseSchema.getGetDate());
				  
				  tLACommisionNewSchema.setCommDire("1");
				  tLACommisionNewSchema.setTransType("ZC");
				  tLACommisionNewSchema.setPayYear(0);
				  tLACommisionNewSchema.setPayCount(1);
				  
				  tLACommisionNewSchema.setAgentCom(tLJAGetEndorseSchema.getAgentCom());
				  tLACommisionNewSchema.setAgentType(tLJAGetEndorseSchema.getAgentType());
				  tLACommisionNewSchema.setBranchCode(tBranchCode);
				  tLACommisionNewSchema.setBranchSeries(tLABranchGroupDB.getBranchSeries());
				  tLACommisionNewSchema.setAgentGroup(tAgentGroup);
				  tLACommisionNewSchema.setBranchAttr(tBranchAttr);
				  
				  tLACommisionNewSchema.setOperator(cOperator);
				  tLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
				  tLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
				  tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
				  tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
				  
				  tLACommisionNewSet.add(tLACommisionNewSchema);
				  
				  
			  }
			  if(0<tLACommisionNewSet.size())
			  {
					tMMap.put(tLACommisionNewSet, "INSERT");
			  }
			  if(0<tLACommisionErrorSet.size())
			  {
					tMMap.put(tLACommisionErrorSet, "INSERT");
			  }
			  if(0==tMMap.size())
			  {
					continue;
			  }
			  if (!mAgentWageCommonFunction.dealDataToTable(tMMap)) 
			  {
		        	// @@错误处理
		            CError tError = new CError();
		            tError.moduleName = "AgentWageOfLJAGetEndorseRBData";
		            tError.functionName = "PubSubmit";
		            tError.errorMessage = "数据插入到LACommisionNew中失败!";
		            this.mErrors.addOneError(tError);
		            return false;
			  }
			  
		  } while (tLJAGetEndorseSet.size()>0);
    	  System.out.println("*****************犹豫期撤保保全回退AgentWageOfLJAGetEndorseRBData dealData 结束***************");
		return true;
		
	}
	
}
