package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.vschema.LACommisionErrorSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.RSWrapper;

public class AgentWageOfLJAGetEndorseGrpDataWT extends AgentWageOfAbstractClass 
{

//	public LJAGetEndorseSchema mLastLJAGetEndorseSchema = new LJAGetEndorseSchema();
	public LJAGetEndorseSchema mCurrentLJAEndorseSchema = new LJAGetEndorseSchema();
	boolean  tEndDataFlag = false; 
	
	
	public double mSumMoney =0; 
	public String mGrpPolNo ="";
	public String mPayPlanCode ="";
	/**
	 * 主要提数逻辑
	 * @param cSQL
	 * @param cOperator
	 * @return
	 */
	public boolean dealData(String cSQL,String cOperator)
	{
		System.out.println("*****************团险犹豫期退保AgentWageOfLJAGetEndorseGrpDataWT  开始***************");
		LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
		RSWrapper tRSWrapper = new RSWrapper();
		tRSWrapper.prepareData(tLJAGetEndorseSet, cSQL);
		boolean tQueryFlag = false;
		do 
		{
			tRSWrapper.getData();
			if(!tQueryFlag)
			{
				if(null == tLJAGetEndorseSet || 0 == tLJAGetEndorseSet.size())
				{
					System.out.println("AgentWageOfLJAGetEndorseGrpDataWT-->dealData没有满足条件 的数据");
					return true;
				}
				tQueryFlag = true;
			}
			if(null != tLJAGetEndorseSet && 0 < tLJAGetEndorseSet.size())
			{
				if(mCurrentLJAEndorseSchema.getActuGetNo() == null)
				{
					mCurrentLJAEndorseSchema = tLJAGetEndorseSet.get(1);
					
				}
			}
			if(0 == tLJAGetEndorseSet.size() && !tEndDataFlag)
			{
//				mSumMoney = mSumMoney - mLastLJAGetEndorseSchema.getGetMoney();
				tLJAGetEndorseSet.add(mCurrentLJAEndorseSchema);
				tEndDataFlag = true;
			}
			dealSet(tLJAGetEndorseSet,cOperator);
			
		} while (tLJAGetEndorseSet.size() > 0);
		System.out.println("*****************团险犹豫期退保AgentWageOfLJAGetEndorseGrpDataWT  结束***************");
		return true;
	}
	
	private void dealSet(LJAGetEndorseSet cLJAGetEndorseSet,String cOperator)
	{
		double tLastSumMoney = 0;
		MMap tMMap = new MMap();
		LACommisionErrorSet tLACommisionErrorSet = new LACommisionErrorSet();
		LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
		//用于更新未计算薪资
		LACommisionNewSet tUpdateLACommisionNewSet = new LACommisionNewSet();
//      tJudgeLJAGetEndorseSchema = new LJAGetEndorseSchema();
		LJAGetEndorseSchema tJudgeLJAGetEndorseSchema = new LJAGetEndorseSchema();
		boolean tJudgeDataFlag = false;
		for (int i = 1; i <= cLJAGetEndorseSet.size(); i++) 
		{
			LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
			tLJAGetEndorseSchema = cLJAGetEndorseSet.get(i);
			String tGrpPolNoNew = tLJAGetEndorseSchema.getGrpPolNo();
			String tPayPlanCodeNew  = tLJAGetEndorseSchema.getPayPlanCode();
			double tGetMoneyNew  = tLJAGetEndorseSchema.getGetMoney();
			
			if(!mCurrentLJAEndorseSchema.getGrpPolNo().equals(tGrpPolNoNew)
					||!mCurrentLJAEndorseSchema.getPayPlanCode().equals(tPayPlanCodeNew))
				
			{
				
				tJudgeLJAGetEndorseSchema = mCurrentLJAEndorseSchema;
//				tLastSumMoney = tGetMoneyNew;
				tLastSumMoney = mSumMoney;
				
				mCurrentLJAEndorseSchema = tLJAGetEndorseSchema;
				mSumMoney = 0;
				mSumMoney+=tGetMoneyNew; 
				tJudgeDataFlag = true;
				
			}
			else
			{   
				if(tEndDataFlag)
				{
					tJudgeLJAGetEndorseSchema = mCurrentLJAEndorseSchema;
					tLastSumMoney = mSumMoney;
//					tEndDataFlag = false;
					tJudgeDataFlag = true;
				}
				else
				{
//				tJudgeLJAGetEndorseSchema = tLJAGetEndorseSchema;
				  mSumMoney += tGetMoneyNew;
				  tJudgeDataFlag = false;
				}
			}
	
			if(tJudgeDataFlag)
			{
				LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
				String tAgentCode = tJudgeLJAGetEndorseSchema.getAgentCode();
	            String tGrpContNo = tJudgeLJAGetEndorseSchema.getGrpContNo();
	            String tRiskCode = tJudgeLJAGetEndorseSchema.getRiskCode();
	            String tGrpPolNo = tJudgeLJAGetEndorseSchema.getGrpPolNo();
	            LAAgentSchema tLAAgentSchema = mAgentWageCommonFunction.queryLAAgent(tAgentCode);
	            if(null == tLAAgentSchema)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tJudgeLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTCODE, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            LCGrpContSchema tLCGrpContSchema = mAgentWageCommonFunction.queryLCGrpCont(tGrpContNo);
	            if(null == tLCGrpContSchema)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tJudgeLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCGRPCONT, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            LCGrpPolSchema tLCGrpPolSchema = mAgentWageCommonFunction.queryLCGrpPol(tGrpPolNo);
	            if(null == tLCGrpPolSchema)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tJudgeLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_LCGRPPOL, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            //团队
	            String tAgentGroup = tLAAgentSchema.getAgentGroup();
	            LABranchGroupSchema tLABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tAgentGroup);
	            if(null == tLABranchGroupSchema)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema =
	            			dealErrorData(tJudgeLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_AGENTGROUP, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            String tBranchCode = tLAAgentSchema.getBranchCode();
	            LABranchGroupSchema t2LABranchGroupSchema = mAgentWageCommonFunction.querytLABranchGroup(tBranchCode);
	            String tBranchAttr = t2LABranchGroupSchema.getBranchAttr();
	            if(null == tBranchAttr || "".equals(tBranchAttr))
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tJudgeLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_BRANCHATTR, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            
	            //添加对互动的处理
	            String tSaleChnl = tLCGrpPolSchema.getSaleChnl();
	            String tBranchType3 = "";
	            if("14".equals(tSaleChnl)||"15".equals(tSaleChnl))
	            {
	            	String tCrs_SaleChnl = tLCGrpContSchema.getCrs_SaleChnl();
	            	String tCrs_BussType = tLCGrpContSchema.getCrs_BussType();
	            	tBranchType3 = mAgentWageCommonFunction.dealActivityData(tSaleChnl, tCrs_SaleChnl, tCrs_BussType);
	            }
	            if("no".equals(tBranchType3))
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tJudgeLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_ACTIVE, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            String tBranchTypes[] = mAgentWageCommonFunction.getSaleChnlToBranchType(tSaleChnl);
	            if(null == tBranchTypes || 0 == tBranchTypes.length)
	            {
	            	LACommisionErrorSchema tLACommisionErrorSchema = 
	            			dealErrorData(tJudgeLJAGetEndorseSchema, AgentWageOfCodeDescribe.ERRORCODE_SALECHNL, cOperator);
	            	tLACommisionErrorSet.add(tLACommisionErrorSchema);
	            	continue;
	            }
	            String tBranchType = tBranchTypes[0];
	            String tBranchType2 = tBranchTypes[1];
	            
	            //获取保单续保次数
	            int PayYear = 0;
	            int tRenewCount = mAgentWageCommonFunction.queryLCRnewPol(tGrpPolNo, "grppolno");
	            //封装数据从lcgrpcont,lcgrppol中取
	            tLACommisionNewSchema = mAgentWageCommonFunction.packageGrpContDataOfLACommisionNew(tLCGrpContSchema, tLCGrpPolSchema);
	            String tCommisionSn = mAgentWageCommonFunction.getCommisionSN();
	            
	            tLACommisionNewSchema.setCommisionSN(tCommisionSn);
	            tLACommisionNewSchema.setCommisionBaseNo(this.mExtractDataType);
	            tLACommisionNewSchema.setBranchType(tBranchType);
	            tLACommisionNewSchema.setBranchType2(tBranchType2);
	            tLACommisionNewSchema.setBranchType3(tBranchType3);
	            tLACommisionNewSchema.setReNewCount(tRenewCount);
	            
	            tLACommisionNewSchema.setManageCom(tJudgeLJAGetEndorseSchema.getManageCom());
	            tLACommisionNewSchema.setRiskCode(tJudgeLJAGetEndorseSchema.getRiskCode());
	            tLACommisionNewSchema.setEndorsementNo(tJudgeLJAGetEndorseSchema.getEndorsementNo());
	            tLACommisionNewSchema.setDutyCode(tJudgeLJAGetEndorseSchema.getDutyCode());
	            tLACommisionNewSchema.setPayPlanCode(tJudgeLJAGetEndorseSchema.getPayPlanCode());
	            tLACommisionNewSchema.setReceiptNo(tJudgeLJAGetEndorseSchema.getActuGetNo());
	            
	            tLACommisionNewSchema.setTPayDate(tJudgeLJAGetEndorseSchema.getGetDate());
	            tLACommisionNewSchema.setTEnterAccDate(tJudgeLJAGetEndorseSchema.getEnterAccDate());
	            tLACommisionNewSchema.setTConfDate(tJudgeLJAGetEndorseSchema.getGetConfirmDate());
	            tLACommisionNewSchema.setTMakeDate(tJudgeLJAGetEndorseSchema.getMakeDate());
	            tLACommisionNewSchema.setCommDate(PubFun.getCurrentDate());
	            
	            tLACommisionNewSchema.setTransMoney(0-java.lang.Math.abs(tLastSumMoney));
	            tLACommisionNewSchema.setTransState("00");//正常保单
	            tLACommisionNewSchema.setTransStandMoney(0-java.lang.Math.abs(tLastSumMoney));
	            tLACommisionNewSchema.setCurPayToDate(tJudgeLJAGetEndorseSchema.getGetDate());
	            tLACommisionNewSchema.setTransType("WT");
	            
	            if("07".equals(tSaleChnl))
	            {
	            	tLACommisionNewSchema.setF1("02");//职团开拓
	            	tLACommisionNewSchema.setP5(0-java.lang.Math.abs(tLastSumMoney));//职团开拓实收保费
	            }
	            else
	            {
	            	tLACommisionNewSchema.setF1("01");
	            }
	            
	            tLACommisionNewSchema.setFlag("1");//犹豫期撤件
	            tLACommisionNewSchema.setPayYear(PayYear);
	            tLACommisionNewSchema.setPayYears(mAgentWageCommonFunction.getPayYears(tGrpPolNo));
	            tLACommisionNewSchema.setAgentCom(tJudgeLJAGetEndorseSchema.getAgentCom());
	            tLACommisionNewSchema.setAgentType(tJudgeLJAGetEndorseSchema.getAgentType());
	            tLACommisionNewSchema.setAgentCode(tJudgeLJAGetEndorseSchema.getAgentCode());
	            
	            // P7帐户管理费
	            if(tRenewCount>=1 || PayYear>=1)
	            {
	            	tLACommisionNewSchema.setP7(0);
	            }
	            else
	            {
	                String tSql =
	                        "select value(sum(fee),0) from  lcinsureaccfee  where  grppolno='" +
	                        tGrpPolNo + "' with ur ";
	                System.out.println("计算帐户管理费sql:"+tSql);
	                ExeSQL aExeSQL = new ExeSQL();
	                String tMoney = aExeSQL.getOneValue(tSql);
	                double tValue = Double.parseDouble(tMoney);
	                tLACommisionNewSchema.setP7(0 - tValue);
	            }
	           String tScanDate = mAgentWageCommonFunction.getScanDate(tLCGrpPolSchema.getPrtNo());
	           tLACommisionNewSchema.setScanDate(tScanDate);
	           tLACommisionNewSchema.setOperator(cOperator);
	           tLACommisionNewSchema.setMakeDate(PubFun.getCurrentDate());
	           tLACommisionNewSchema.setMakeTime(PubFun.getCurrentTime());
	           tLACommisionNewSchema.setModifyDate(PubFun.getCurrentDate());
	           tLACommisionNewSchema.setModifyTime(PubFun.getCurrentTime());
	           
	           tLACommisionNewSchema.setBranchCode(tBranchCode);
	           tLACommisionNewSchema.setBranchSeries(t2LABranchGroupSchema.getBranchSeries());
	           tLACommisionNewSchema.setAgentGroup(tAgentGroup);
	           tLACommisionNewSchema.setBranchAttr(tBranchAttr);
	           tLACommisionNewSchema.setCommDire("1");
	           
	           //对于套餐而言，只适用于个险，互动，健管及团险中介
	           String tF3BranchType ="1,5,7";
	           if(tF3BranchType.indexOf(tBranchType)!=-1||("2".equals(tBranchType) && "02".equals(tBranchType2)))
	           {
	        	   if(tLCGrpContSchema.getCardFlag() != null && !tLCGrpContSchema.getCardFlag().trim().equals("") && 
	        			   tLCGrpContSchema.getCardFlag().equals("2"))
	        	   {
	        		   tLACommisionNewSchema.setF3(mAgentWageCommonFunction.
	        				   getWrapCode(tGrpContNo, tJudgeLJAGetEndorseSchema.getRiskCode()));
	        	   }
	        	   else
	        	   {
	        		   tLACommisionNewSchema.setF3("");
	        	   }
	           }
	           
	           //判断犹豫期退保对应的首期保单是否计算过薪资
	           int tCalNum = mAgentWageCommonFunction.judgeWageIsCal(tGrpPolNo);
		        if(0==tCalNum)
		        {
		        	System.out.println("首期保单未找到，有错误 ");
		        	continue;
		        }
		        //薪资已经计算过
		        if(1==tCalNum)
		        {
		        	//薪资月算法相关字段处理
		        	//计算薪资月的时候，传的paycount是0，但实际上是1，此处是为了能调用公共的薪资月方法
		        	tLACommisionNewSchema.setPayCount(0);
		        	String[] tConfDates = mAgentWageCommonFunction.queryConfDate(tLCGrpPolSchema.getGrpContNo(),"WT",tLACommisionNewSchema.getTMakeDate(),
		        			tJudgeLJAGetEndorseSchema.getGetConfirmDate(),tJudgeLJAGetEndorseSchema.getActuGetNo());
		        	//用来表示是0--收费还是1--付费，个单默认为0，团单则需要判断
		        	String tAFlag =tConfDates[0];
		        	//财务确认日期 ，团单会用到此字段
		        	String tConfDate = tConfDates[1];
		        	String tVisitTime  = mAgentWageCommonFunction.queryReturnVisit(tGrpContNo);
		        	String tRiskFlag = mAgentWageCommonFunction.getVisitRiskCode(tRiskCode);
		        	String tFlag = "02";
		        	//根据薪资月函数 得到薪资月
		        	String tCalDate = mAgentWageCommonFunction.calCalDateForAll(tLACommisionNewSchema, tConfDate, tFlag, tAFlag, tVisitTime, tRiskFlag);
		        	tLACommisionNewSchema.setCalDate(tCalDate);
		        	if(!"".equals(tCalDate)&&null!=tCalDate)
		        	{
		        		tLACommisionNewSchema.setCalcDate(tCalDate);
		        		String tWageNo = mAgentWageCommonFunction.getWageNo(tCalDate).substring(0,6);
		        		tLACommisionNewSchema.setWageNo(tWageNo);
		        		tLACommisionNewSchema.setTConfDate(tConfDate);
		        	}
		        }
		      //薪资未计算过
		        if(2==tCalNum)
		        {
		        	LACommisionNewSet tZCLACommisionNewSet = new LACommisionNewSet();
		        	tZCLACommisionNewSet = mAgentWageCommonFunction.getZCDataOfLACommisionNewSet(tGrpPolNo);
		        	tLACommisionNewSchema.setCommDire("2");
		        	tLACommisionNewSchema.setCalcDate(tZCLACommisionNewSet.get(1).getCalcDate());
		        	tLACommisionNewSchema.setCalDate(tZCLACommisionNewSet.get(1).getCalDate());
		        	tLACommisionNewSchema.setWageNo(tZCLACommisionNewSet.get(1).getWageNo());
		        	tUpdateLACommisionNewSet.add(tZCLACommisionNewSet);
		        }
		        tLACommisionNewSchema.setPayCount(1);
	            tLACommisionNewSet.add(tLACommisionNewSchema);
			}
			
			
		}
		if(0<tLACommisionNewSet.size())
		{
			tMMap.put(tLACommisionNewSet, "INSERT");
		}
		if(0<tLACommisionErrorSet.size())
		{
			tMMap.put(tLACommisionErrorSet, "INSERT");
		}
		if(0<tUpdateLACommisionNewSet.size())
		{
			tMMap.put(tUpdateLACommisionNewSet, "UPDATE");
		}
		if(0==tMMap.size())
		{
			return;
		}
		if(!mAgentWageCommonFunction.dealDataToTable(tMMap))
		{
			// @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageOfLJAGetEndorseGrpDataWT";
            tError.functionName = "PubSubmit";
            tError.errorMessage = "数据插入到LACommisionNew中失败!";
            this.mErrors.addOneError(tError);
            return ;
		}
	}
	
}
	
	