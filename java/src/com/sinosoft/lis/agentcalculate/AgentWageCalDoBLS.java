/*
 * <p>ClassName: AgentWageCalDoBLS </p>
 * <p>Description: LAAgentWageCalDoBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-10-31
 */
package com.sinosoft.lis.agentcalculate;

//import com.sinosoft.lis.vbl.*;
import java.sql.Connection;

import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.vdb.LAChargeDBSet;
import com.sinosoft.lis.vdb.LACommisionDBSet;
import com.sinosoft.lis.vdb.LAWageHistoryDBSet;
import com.sinosoft.lis.vdb.LAWageLogDBSet;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class AgentWageCalDoBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    //传输数据类
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    public AgentWageCalDoBLS()
    {}

    public static void main(String[] args)
    {}

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();

        System.out.println("Start LAAgentWageCalDoBLS Submit...");
        if (!saveLAAgentWageCalDo())
        {
            System.out.println("Insert failed");
            return false;
        }
        System.out.println("End LAAgentWageCalDoBLS Submit...");
        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAgentWageCalDo()
    {
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
        LACommisionSet tLACommisionSet = new LACommisionSet();
        LAChargeSet tLAChargeSet = new LAChargeSet();
//   LAAgentSet tLAAgentSet=new LAAgentSet();
        tLAWageLogSchema = (LAWageLogSchema) mInputData.getObjectByObjectName(
                "LAWageLogSchema", 0);
        tLAWageHistorySchema = (LAWageHistorySchema) mInputData.
                               getObjectByObjectName("LAWageHistorySchema", 0);
        tLACommisionSet = (LACommisionSet) mInputData.getObjectByObjectName(
                "LACommisionSet", 0);
        tLAChargeSet = (LAChargeSet) mInputData.getObjectByObjectName(
                "LAChargeSet", 0);
//   tLAAgentSet=(LAAgentSet)mInputData.getObjectByObjectName("LAAgentSet",0) ;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBLS";
            tError.functionName = "saveLAAgentWageCalDo";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            if (tLAWageLogSchema.getBranchType().equals("0"))
            {
                //日志表
                LAWageLogDBSet tLAWageLogDBSet = new LAWageLogDBSet(conn);
                tLAWageLogSchema.setBranchType("1");
                tLAWageLogDBSet.add((new LAWageLogSchema()));
                tLAWageLogDBSet.get(1).setSchema(tLAWageLogSchema);
                tLAWageLogSchema.setBranchType("2");
                tLAWageLogDBSet.add((new LAWageLogSchema()));
                tLAWageLogDBSet.get(2).setSchema(tLAWageLogSchema);
                tLAWageLogSchema.setBranchType("3");
                tLAWageLogDBSet.add((new LAWageLogSchema()));
                tLAWageLogDBSet.get(3).setSchema(tLAWageLogSchema);
                if (!tLAWageLogDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAWageLogDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveBLS";
                    tError.functionName = "saveLAAgentWageCalSave";
                    tError.errorMessage = "日志表三条数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                //历史表
                LAWageHistoryDBSet tLAWageHistoryDBSet = new LAWageHistoryDBSet(
                        conn);
                tLAWageHistorySchema.setBranchType("1");
                tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                tLAWageHistoryDBSet.get(1).setSchema(tLAWageHistorySchema);
                tLAWageHistorySchema.setBranchType("2");
                tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                tLAWageHistoryDBSet.get(2).setSchema(tLAWageHistorySchema);
                tLAWageHistorySchema.setBranchType("3");
                tLAWageHistoryDBSet.add((new LAWageHistorySchema()));
                tLAWageHistoryDBSet.get(3).setSchema(tLAWageHistorySchema);
                if (!tLAWageHistoryDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAWageHistoryDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalSaveBLS";
                    tError.functionName = "saveLAAgentWageCalSave";
                    tError.errorMessage = "历史表三条数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            else
            {
                LAWageLogDB tLAWageLogDB = new LAWageLogDB(conn);
                tLAWageLogDB.setSchema(tLAWageLogSchema);
                if (!tLAWageLogDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAWageLogDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalDoBLS";
                    tError.functionName = "saveLAAgentWageCalDo";
                    tError.errorMessage = "日志表数据更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                System.out.println("Start update LACommision...");
                LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB(conn);
                tLAWageHistoryDB.setSchema(tLAWageHistorySchema);
                if (!tLAWageHistoryDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAWageHistoryDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalDoBLS";
                    tError.functionName = "saveLAAgentWageCalDo";
                    tError.errorMessage = "历史表数据更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            if (tLACommisionSet.size() > 0)
            {
                LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
                tLACommisionDBSet.set(tLACommisionSet);
                if (!tLACommisionDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLACommisionDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AgentWageCalDoBLS";
                    tError.functionName = "saveLAAgentWageCalDo";
                    tError.errorMessage = "直接佣金表数据更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                if (tLAChargeSet.size() > 0)
                {
                    LAChargeDBSet tLAChargeDBSet = new LAChargeDBSet(conn);
                    tLAChargeDBSet.set(tLAChargeSet);
                    if (!tLAChargeDBSet.insert())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLAChargeDBSet.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "AgentWageCalDoBLS";
                        tError.functionName = "saveLAAgentWageCalDo";
                        tError.errorMessage = "直接佣金表数据更新失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
//
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentWageCalDoBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }
}
