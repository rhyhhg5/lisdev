package com.sinosoft.lis.agentcalculate;

import java.util.HashMap;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LACommisionNewDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.lis.db.LBGrpPolDB;
import com.sinosoft.lis.db.LBPolDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LACommisionErrorSchema;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LBContSchema;
import com.sinosoft.lis.schema.LBGrpContSchema;
import com.sinosoft.lis.schema.LBGrpPolSchema;
import com.sinosoft.lis.schema.LBPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.VData;
/**
 * 此类是用来存放销售批处理中涉及到的公共方法，比如：续保次数的查询，LACommisionNew中对应保单的相关数据的赋值
 * 作用：减少相关类的代码长度，使其逻辑框架清晰，便于后期的学习修改
 * @author yangyang
 *
 */
public class AgentWageCommonFunction {
	
	private Reflections mReflections = new Reflections();
	public static void main(String[] args) {
		AgentWageCommonFunction tAgentWageCommonFunction = new AgentWageCommonFunction();
		tAgentWageCommonFunction.queryLCRnewPol("0000", "grppolno");
	}
	/**
	 * 根据保单号查询个单数据
	 * @param cContNo
	 * @return
	 */
	public LCContSchema queryLCCont(String cContNo) 
	{
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(cContNo);
        LCContSchema tLCContSchema = new LCContSchema();
        if (!tLCContDB.getInfo()) {
            LBContDB tLBContDB = new LBContDB();
            tLBContDB.setContNo(cContNo);
            if (!tLBContDB.getInfo()) {
                if (tLBContDB.mErrors.needDealError() == true) {
                    return null;
                }
                return null;
            }
            Reflections tReflections = new Reflections();
            LBContSchema tLBContSchema = new LBContSchema();
            tLBContSchema = tLBContDB.getSchema();
            tReflections.transFields(tLCContSchema, tLBContSchema);
        } else {
            tLCContSchema = tLCContDB.getSchema();
        }
        return tLCContSchema;
    }
	/**
	 * 查询集体保单表
	 * @param tGrpContNo
	 * @return 输出：如果查询时发生错误则返回null,否则返回Schema
	 */
    public LCGrpContSchema queryLCGrpCont(String cGrpContNo) 
    {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(cGrpContNo);
        LCGrpContSchema cLCGrpContSchema = new LCGrpContSchema();
        if (!tLCGrpContDB.getInfo()) {
            LBGrpContDB tLBGrpContDB = new LBGrpContDB();
            tLBGrpContDB.setGrpContNo(cGrpContNo);
            if (!tLBGrpContDB.getInfo()) {
                if (tLBGrpContDB.mErrors.needDealError() == true) {
                    return null;
                }
                return null;
            }
            Reflections tReflections = new Reflections();
            LBGrpContSchema tLBGrpContSchema = new LBGrpContSchema();
            tLBGrpContSchema = tLBGrpContDB.getSchema();
            tReflections.transFields(cLCGrpContSchema, tLBGrpContSchema);
        } else {
            cLCGrpContSchema = tLCGrpContDB.getSchema();
        }
        return cLCGrpContSchema;
    }
	/**
	 * 根据polno查询lcpol
	 * @param tPolNo
	 * @return
	 */
	public LCPolSchema queryLCPol(String cPolNo) 
	{
	    LCPolDB tLCPolDB = new LCPolDB();
	    LCPolSchema tLCPolSchema = new LCPolSchema();
	    tLCPolDB.setPolNo(cPolNo);
	    if (!tLCPolDB.getInfo()) 
	    {
	    	LBPolDB tLBPolDB = new LBPolDB();
	        tLBPolDB.setPolNo(cPolNo);
	        if (!tLBPolDB.getInfo()) 
	        {
	        	if (tLBPolDB.mErrors.needDealError() == true) 
	        	{
	                return null;
	            }
	            return null;
	        }
	        LBPolSchema tLBPolSchema = new LBPolSchema();
	        tLBPolSchema = tLBPolDB.getSchema();
	        Reflections tReflections = new Reflections();
	        tReflections.transFields(tLCPolSchema, tLBPolSchema);
	   } else
	   {
		   tLCPolSchema = tLCPolDB.getSchema();
	   }
	   return  tLCPolSchema;   
	}
	/**
	 * 查询集体保单表
	 * @param tGrpPolNo
	 * @return 输出：如果查询时发生错误则返回null,否则返回Schema
	 */
    public LCGrpPolSchema queryLCGrpPol(String cGrpPolNo) 
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(cGrpPolNo);
        LCGrpPolSchema cLCGrpPolSchema = new LCGrpPolSchema();
        if (!tLCGrpPolDB.getInfo()) {
            LBGrpPolDB tLBGrpPolDB = new LBGrpPolDB();
            tLBGrpPolDB.setGrpPolNo(cGrpPolNo);
            if (!tLBGrpPolDB.getInfo()) {
                if (tLBGrpPolDB.mErrors.needDealError() == true) {
                    return null;
                }
                return null;
            }
            LBGrpPolSchema tLBGrpPolSchema = new LBGrpPolSchema();
            tLBGrpPolSchema = tLBGrpPolDB.getSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(cLCGrpPolSchema, tLBGrpPolSchema);
        } else {
            cLCGrpPolSchema = tLCGrpPolDB.getSchema();
        }
        return cLCGrpPolSchema;
    }
    /**
     * 查询业务员
     * @param cAgentCode
     * @return 输出：如果查询时发生错误则返回null,否则返回Schema
     */
    public LAAgentSchema queryLAAgent(String cAgentCode){
    	LAAgentDB tLAAgentDB = new LAAgentDB();
    	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    	tLAAgentDB.setAgentCode(cAgentCode);
    	if(!tLAAgentDB.getInfo()){
    		return null;
    	}
    	tLAAgentSchema = tLAAgentDB.getSchema();
    	return tLAAgentSchema;
    }
    /**
     * 根据保单中的polno来获取续保次数
     * @param cPolNo  险种保单号码，个单：polno,团单：grppolno
     * @param cTableFiled  对应的表中字段：个单：polno,团单：grppolno
     * @return
     */
	public int queryLCRnewPol(String cPolNo,String cTableFiled) 
	{
        String tSql = " select nvl(max(nvl(renewcount,0)),0) from  LCRnewStateLog  where  "+cTableFiled+"='" + cPolNo +"'  "
                      +" union  "
                      +" select nvl(max(nvl(renewcount,0)),0) from  LBRnewStateLog  where  "+cTableFiled+"='" + cPolNo +"'  ";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(tSql);
        int tRenewCount = 0;
        if(0==tSSRS.getMaxRow()||null==tSSRS)
        {
        	return tRenewCount;
        }
        //上面的查询sql最多返回两条数据，至少返回一条，所以不用循环了,只用判断下查询出来的是几条即可 
        int tRenewCountOne = Integer.valueOf(tSSRS.GetText(1, 1));
        int tRenewCountTwo = 0;
        int tMaxNo = tSSRS.getMaxRow();
        if(2==tMaxNo)
        {
        	tRenewCountTwo = Integer.valueOf(tSSRS.GetText(1, 2));
        }
    	tRenewCount = 	tRenewCountOne>tRenewCountTwo?tRenewCountOne:tRenewCountTwo;
        return tRenewCount;
    }
	/**
	 * 根据传入的个人保单数据，把相关的字段信息传入到LACommisionNew中
	 * 由于这部分是公用的，所以整理成一个方法，方便查看程序的结构
	 * 所以，如果相对这一部分进行修改，需要先确认：是不是所有的提数中，此字段的提取逻辑都是一样的
	 * @param cLCContSchema
	 * @param cLCPolSchema
	 * @return
	 */
	public LACommisionNewSchema packageContDataOfLACommisionNew(LCContSchema cLCContSchema,LCPolSchema cLCPolSchema)
	{
		LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
		//目前系统逻辑字段CommisionBaseNo没什么用，建议可以存储成从哪个实收表提取的，比如：ljapaypesrson
		//此字段定义长度为：varchar(10),所以这个存储全表名时，是不行的
		//建议在具体的提取数据类中定义，但是可以在这里面添加说明 
		//ljapayperson表提取的数据，可以用：payperson来 表示
		//ljagetendorse表提取的数据，可以用：getendorse来表示
        //tLACommisionNewSchema.setCommisionBaseNo("0000000000");
        tLACommisionNewSchema.setGrpContNo(cLCPolSchema.getGrpContNo());
        tLACommisionNewSchema.setGrpPolNo(cLCPolSchema.getGrpPolNo());
        tLACommisionNewSchema.setContNo(cLCPolSchema.getContNo());
        tLACommisionNewSchema.setPolNo(cLCPolSchema.getPolNo());
        tLACommisionNewSchema.setMainPolNo(cLCPolSchema.getMainPolNo()); //新加的 主险单号 用于继续率的计算
        
        tLACommisionNewSchema.setAppntNo(cLCContSchema.getAppntNo());
        tLACommisionNewSchema.setRiskCode(cLCPolSchema.getRiskCode());
        tLACommisionNewSchema.setRiskVersion(cLCPolSchema.getRiskVersion());
        
        tLACommisionNewSchema.setCValiDate(cLCPolSchema.getCValiDate());
        tLACommisionNewSchema.setPayIntv(cLCPolSchema.getPayIntv());
        tLACommisionNewSchema.setPayMode(cLCPolSchema.getPayMode());
        tLACommisionNewSchema.setF4(String.valueOf(cLCPolSchema.getInsuYear()));
        tLACommisionNewSchema.setF5(cLCPolSchema.getInsuYearFlag());
        
        tLACommisionNewSchema.setPayYears(cLCPolSchema.getPayYears());
        tLACommisionNewSchema.setYears(cLCPolSchema.getYears());
        tLACommisionNewSchema.setSignDate(cLCPolSchema.getSignDate());
        tLACommisionNewSchema.setGetPolDate(cLCContSchema.getGetPolDate());
        tLACommisionNewSchema.setAgentCom(cLCPolSchema.getAgentCom());

        String tPolType = cLCPolSchema.getStandbyFlag2();
        tLACommisionNewSchema.setPolType(tPolType == null ||
                                      tPolType.equals("") ?
                                      "1" : tPolType); //0:优惠 1：正常
        tLACommisionNewSchema.setP1(cLCPolSchema.getManageFeeRate());
        tLACommisionNewSchema.setP11(cLCContSchema.getAppntName());
        tLACommisionNewSchema.setP12(cLCPolSchema.getInsuredNo());
        tLACommisionNewSchema.setP13(cLCPolSchema.getInsuredName());
        tLACommisionNewSchema.setP14(cLCPolSchema.getPrtNo());
        tLACommisionNewSchema.setP15(cLCPolSchema.getProposalNo());
        tLACommisionNewSchema.setMakePolDate(cLCPolSchema.getMakeDate()); //录入日期
        tLACommisionNewSchema.setCustomGetPolDate(cLCContSchema.getCustomGetPolDate()); //签字日期
		return tLACommisionNewSchema;
	}
	/**
	 * 根据传入的团体保单数据，把相关的字段信息传入到LACommisionNew中
	 * 由于这部分是公用的，所以整理成一个方法，方便查看程序的结构
	 * 所以，如果相对这一部分进行修改，需要先确认：是不是所有的提数中，此字段的提取逻辑都是一样的
	 * @param cLCContSchema
	 * @param cLCPolSchema
	 * @return
	 */
	public LACommisionNewSchema packageGrpContDataOfLACommisionNew(LCGrpContSchema cLCGrpContSchema,LCGrpPolSchema cLCGrpPolSchema)
	{
		LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
		//目前系统逻辑字段CommisionBaseNo没什么用，建议可以存储成从哪个实收表提取的，比如：ljapaypesrson
		//此字段定义长度为：varchar(10),所以这个存储全表名时，是不行的
		//建议在具体的提取数据类中定义，但是可以在这里面添加说明 
		//LJAPayGrp表提取的数据，可以用：ljapaygrp来 表示
        //tLACommisionNewSchema.setCommisionBaseNo("0000000000");
		tLACommisionNewSchema.setGrpContNo(cLCGrpPolSchema.getGrpContNo());
        tLACommisionNewSchema.setGrpPolNo(cLCGrpPolSchema.getGrpPolNo());
        tLACommisionNewSchema.setContNo(cLCGrpPolSchema.getGrpContNo());
        tLACommisionNewSchema.setPolNo(cLCGrpPolSchema.getGrpPolNo());
        tLACommisionNewSchema.setMainPolNo(cLCGrpPolSchema.getGrpPolNo()); //新加的 主险单号 用于继续率的计算
        
        tLACommisionNewSchema.setAppntNo(cLCGrpContSchema.getAppntNo());
        tLACommisionNewSchema.setRiskVersion(cLCGrpPolSchema.getRiskVersion());
        tLACommisionNewSchema.setCValiDate(cLCGrpPolSchema.getCValiDate());
        tLACommisionNewSchema.setPayIntv(cLCGrpPolSchema.getPayIntv());
        tLACommisionNewSchema.setPayMode(cLCGrpPolSchema.getPayMode());
        SSRS tSSRS = new SSRS();
        tSSRS = queryInsuYear(cLCGrpPolSchema.getGrpPolNo(), cLCGrpPolSchema.getRiskCode());
		tLACommisionNewSchema.setF4(tSSRS.GetText(1, 1));
        tLACommisionNewSchema.setF5(tSSRS.GetText(1, 2));
        tLACommisionNewSchema.setLastPayToDate(cLCGrpPolSchema.getPaytoDate());
        tLACommisionNewSchema.setSignDate(cLCGrpContSchema.getSignDate());
        tLACommisionNewSchema.setGetPolDate(cLCGrpContSchema.getGetPolDate());
        String tPolType = cLCGrpPolSchema.getStandbyFlag2();
        tLACommisionNewSchema.setPolType(tPolType == null ||
                                      tPolType.equals("") ?
                                      "1" : tPolType);
        tLACommisionNewSchema.setP1(cLCGrpPolSchema.getManageFeeRate());
//        tLACommisionNewSchema.setP2(cLCGrpPolSchema.getBonusRate());
        tLACommisionNewSchema.setP11(cLCGrpPolSchema.getGrpName());
        tLACommisionNewSchema.setP14(cLCGrpPolSchema.getPrtNo()); //cg add 2004-01-13
        tLACommisionNewSchema.setP15(cLCGrpPolSchema.getGrpProposalNo());
        tLACommisionNewSchema.setMakePolDate(cLCGrpPolSchema.getMakeDate()); //交单日期
        tLACommisionNewSchema.setCustomGetPolDate(cLCGrpContSchema.getCustomGetPolDate()); //签字日期
        tLACommisionNewSchema.setMarketType(cLCGrpContSchema.getMarketType());
		return tLACommisionNewSchema;
	}
	/**
	 * 根据传入的prtno，查询出来扫描日期 
	 * @param tPrtNo
	 * @return
	 */
    public String getScanDate(String tPrtNo) 
    {
    	String sql = "";
    	String scanDate = "";
    	ExeSQL tExeSQL = new ExeSQL();
    	//20061107
    	sql = "select MakeDate from es_doc_main where   doccode='" + tPrtNo +
    			"' and subtype in ('TB01','TB02','TB04','TB06','TB28','TB29')  with ur";
    	scanDate = tExeSQL.getOneValue(sql);
    	System.out.println(scanDate);
    	if (scanDate == null || scanDate.equals("")) {
    		return null;
    	}
    	scanDate = AgentPubFun.formatDate(scanDate, "yyyy-MM-dd");
    	return scanDate;
    }
    /**
     * 查询ljapay表imcometype字段，用来置保/批单标记
     * 可以查看：财务查询文档.xls(问你师父要)
     * @param cPayno
     * @return
     */
    public String getIncomeType(String cPayno) 
    {
        if (cPayno == null || cPayno.equals("")) {
            return "";
        }
        String tIncomeType = "";
        String tSQL = "select incometype from LJAPay where payno = '" + cPayno +
                      "' with ur ";
        ExeSQL tExeSQL = new ExeSQL();
        tIncomeType = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            return "";
        }
        return tIncomeType;
    }
    
    /**
     * 对于互动数据进行处理
     * 薪资包括：直销业务指：契约录入时，销售渠道为14-互动直销，未勾选交叉销售的业务，
     * 		     互动开拓业务指：契约录入时，销售渠道为15-互动中介，勾选了交叉销售，交叉销售类型为：01-相互代理、02-联合展业、03-渠道共享、13-互动部(销售人身险业务)、14-农网共建（互动业务）。（以上5种类型均视为互动开拓业务）
     * 手续费包括：非集团交叉互动业务：销售渠道选择互动渠道并且不勾选交叉销售的业务
     * 		        集团交叉互动业务：销售渠道选择互动渠道并且勾选交叉销售，交叉销售渠道为：财代健或者寿代健(目前系统只有这两个交叉销售渠道)；交叉销售业务类型必须选择：相互代理，才计算手续费。
     * 					        非相互代理的业务类型，不进行手续费计算。这 个判断在手续费计算里面添加的有，以后可以根据标记进行区分
     * @param cLCContSchema
     * @return
     */
    public String dealActivityData(String cSaleChnl,String cCrs_SaleChnl,String cCrs_BussType)
    {
    	String tCrs_BussTypes = "01,02,03,13,14";
    	if(null==cCrs_SaleChnl)
    	{
    		cCrs_SaleChnl = "";
    	}
    	if(null==cCrs_BussType)
    	{
    		cCrs_BussType = "";
    	}
    	//薪资：直销业务
    	if("14".equals(cSaleChnl)&&"".equals(cCrs_SaleChnl))
    	{
    		return "1";
    	}
    	//计算手续费的：非集团交叉互动业务
    	if("15".equals(cSaleChnl)&&"".equals(cCrs_SaleChnl))
    	{
    		return "N";
    	}
    	//交叉销售的，薪资中类型包括手续费，所以这一块没办法兼容
    	if("15".equals(cSaleChnl)&&!"".equals(cCrs_SaleChnl))
    	{
    		if(tCrs_BussTypes.indexOf(cCrs_BussType) != -1)
        	{
        		return "2";
        	}
    	}
    	return "no";
    }
    /**
     * 查询保单回访信息
     *
     * @param 
     * @return String
     */
    public String queryReturnVisit(String cContno)
    {
	   	 String tVisitTime = "";
	   	 String sql = "select CompleteTime from ReturnVisitTable where RETURNVISITFLAG in ('1','4') and POLICYNO = '"+cContno+"' order by makedate desc fetch first 1 rows only";
	   	 ExeSQL tExeSQL = new ExeSQL();
	   	 tVisitTime = tExeSQL.getOneValue(sql);
	   	 if (tExeSQL.mErrors.needDealError() == true) 
	        {
	          return null;
	        }
	   	 
	   	 return tVisitTime;
    }
    /**
     * songguohui
     * 查询收费的财务日期
     * 如果是保全收费则取实收表的财务确认日期，如果是保全付费则是付费表的makedate
     * 定期结算的时间是按照最终结算时的财务日期
     * 输出：如果查询时发生错误则返回null,否则返回tconfdate
     * 这个逻辑可以再确认下，看看能不能再优化或者是有未考虑到的新业务规则，因为此逻辑是很早以前（大约2010年左右吧）写的
     * @param tGrpContNo
     * @param tTransType
     * @param tMakeDate
     * @param tConfDate
     * @param tPayNo
     * @return  返回两个值 ：收付费标记和财务确认日期 
     */
    public String[]  queryConfDate(String tGrpContNo,String tTransType,String tMakeDate,String tConfDate,String tPayNo) 
    {
        String mconfdate = tConfDate;
        String tAFlag = "1";//表示付费
        ExeSQL tExeSQL = new ExeSQL();
        if(null!=tGrpContNo&&!AgentWageOfCodeDescribe.JUDGE_GRPCONTNO.equals(tGrpContNo)&&!"".equals(tGrpContNo))//个单不做处理
        {
	        if("ZC".equals(tTransType)||"ZT".equals(tTransType))
	        {
	        	//首先判断是不是保全付费的情况
	        	String tGetSQL="select distinct paymode from ljaget where 1=1 "
        			+ " and actugetno in ( select actugetno from ljagetendorse where 1=1"
        			+ " and grpcontno='"+tGrpContNo+"' and actugetno='"+tPayNo+"' and endorsementno is not null) with ur";
	        	String tPayMode = tExeSQL.getOneValue(tGetSQL);
           		if(null!=tPayMode&&!"".equals(tPayMode))
           		{
           			if("B".equals(tPayMode))//定期结算中的白条
            		{
            			 mconfdate = "";
            			 tAFlag = "0";
            		}
            		else if("J".equals(tPayMode))//已经定期结算
            		{
            			String ttSQL="select actuno,makedate from LJAEdorBalDetail where btactuno='" +tPayNo+"' with ur";
            			SSRS tSSRS=new SSRS();
            			tSSRS=tExeSQL.execSQL(ttSQL);
            			if(tSSRS.getMaxRow()>0)
            			{
                			mconfdate=tSSRS.GetText(1,2);
            				System.out.println("财务确认日期"+mconfdate);
            			}
            			else
            			{
        					mconfdate="";
            			}
        				tAFlag = "0";
            		}
           		}
           		else
           		{
//           		首先判断是契约收费还是保全收费
                	String tEndorseSQL = "select distinct endorsementno from ljapaygrp where 1=1"
                					+" and grpcontno='"+tGrpContNo+"'" 
                					+" and makedate='"+tMakeDate+"' "
        							+" and payno = '"+tPayNo+"' with ur";
                	String tEndorseMentNo = tExeSQL.getOneValue(tEndorseSQL);
                	if(null!=tEndorseMentNo&&!"".equals(tEndorseMentNo))//为空的时候表示是契约收费,不为空表示是保全的工单号
                	{
                		String tInComeTypeSQL = "select distinct incometype from ljapay where 1=1"
                							+ " and incomeno='"+tEndorseMentNo+"' with ur ";
                		String tIncometype = tExeSQL.getOneValue(tInComeTypeSQL);
                		if(null!=tIncometype&&!"".equals(tIncometype))
                		{
                			if("B".equals(tIncometype))//定期结算中的白条
                    		{
                    			 mconfdate = "";
                    			 tAFlag = "0";
                    		}
                    		else if("J".equals(tIncometype))//已经定期结算
                    		{
                    			String tSQL="select incometype,confdate from ljapay where 1=1"
                    					 +" and getnoticeno in (" 
                    					 +" select actuno from LJAEdorBalDetail where btactuno='" +tPayNo+"') with ur";
                    			SSRS tSSRS=new SSRS();
                    			tSSRS=tExeSQL.execSQL(tSQL);
                    			if(tSSRS.getMaxRow()>0)
                    			{
                    				if(tSSRS.GetText(1,1).equals("13"))
                    				{
                    					mconfdate=tSSRS.GetText(1,2);
                        				System.out.println("财务确认日期"+mconfdate);
                    				}else
                    				{
                    					mconfdate="";
                    				}

                    			}
                    			else
                    			{
                    				tSQL="select othernotype,confdate from ljaget where actugetno in (" +
                					"select actuno from LJAEdorBalDetail where btactuno='" +tPayNo+"') with ur";
                    				tSSRS=tExeSQL.execSQL(tSQL);
                    				if(0<tSSRS.getMaxRow()&&"13".equals(tSSRS.GetText(1,1))
                    						&&null!=tSSRS.GetText(1,2)&&!"".equals(tSSRS.GetText(1,2)))
                    				{
                        				mconfdate=tSSRS.GetText(1,2);
                        				System.out.println("财务确认日期"+mconfdate);
                        			}
                    				else
                    				{
                        				mconfdate = "";
                        			}

                    			}
//                    			String SQL = "select confdate,MAX(INComeno) from ljapay where getnoticeno in " +
//            	        		" (select distinct actuno from ljaedorbaldetail where btactuno in " +
//            	        		" (select distinct actugetno from ljagetendorse where grpcontno in" +
//            	        		" (select grpcontno from LCGrpBalPlan where GrpContno = '"+tGrpContNo+"'))) " +
//            	        		" GROUP BY ConfDate,INComeno " +
//            	        		" order by confdate desc with ur ";
//            			          mconfdate = tExeSQL.getOneValue(SQL);
                    			tAFlag = "0";
                    		}
                		}
               	 	}
           		}
	        }
	        else
	        {
	        	String AddSQL = "select max(GETCONFIRMDATE) from ljagetendorse where 1=1 " 
        			   			+" and FeeFinaType in (select code from ldcode where codetype='feefinatype2')" 
    			   				+" and grpcontno = '"+tGrpContNo+"' "
		   						+" and feeoperationtype='"+tTransType+"' "
   								+" and makedate='"+tMakeDate+"' with ur";
	        	String maxConfdate = tExeSQL.getOneValue(AddSQL);
	        	if(null!=maxConfdate&&!"".equals(maxConfdate))//表示是保全加费
	        	{
	        		mconfdate = maxConfdate;
	        		tAFlag = "0";
	        	}
	        	else //付费
	        	{
	        		tAFlag = "1";
	        	}
	        }
        }
        String[]  tConfDates = {tAFlag,mconfdate};
        return tConfDates; 
    }
    /**
     * 薪资月函数参数包装，薪资月逻辑有点复杂，可以汇总下
     * @param cLACommisionNewSchema
     * @param cConfDate  只有团单才校验，个单不校验此日期 
     * @param cFlag  01的时候才校验回执回销日期 
     * @param cAFlag 团单的时候才校验，为0时表示是收费，需要校验财务确认日期 
     * @param cVisistTime   回访日期 
     * @param cRiskFlag    是否为回访的险种
     * @return
     */
    public String calCalDateForAll(LACommisionNewSchema cLACommisionNewSchema,String cConfDate,String cFlag,String cAFlag,String cVisistTime,String cRiskFlag)
    {
    	String tGetPolDate = cLACommisionNewSchema.getGetPolDate() ==null? "":cLACommisionNewSchema.getGetPolDate();
    	String tSignDate = cLACommisionNewSchema.getSignDate()==null? "":cLACommisionNewSchema.getSignDate();
    	String tTMakeDate = cLACommisionNewSchema.getTMakeDate()==null? "":cLACommisionNewSchema.getTMakeDate();
    	String tCustomGetPolDate = cLACommisionNewSchema.getCustomGetPolDate()==null? "":cLACommisionNewSchema.getCustomGetPolDate();
    	String tBranchType = cLACommisionNewSchema.getBranchType();
    	String tBranchType2 = cLACommisionNewSchema.getBranchType2();
    	String tPayCount = String.valueOf(cLACommisionNewSchema.getPayCount());
    	String tConfDate = cConfDate ==null? "": cConfDate;
    	String tFlag = cFlag== null ? "" : cFlag ;
    	String tAFlag = cAFlag== null ? "" : cAFlag ;//团险根据收费方式采取不同的佣金计算方式
    	String tManageCom=cLACommisionNewSchema.getManageCom();
    	String tVisistTime = cVisistTime==null ? "" : cVisistTime ;
    	String tRiskFlag = cRiskFlag==null ? "" : cRiskFlag ;
    	String tTransState = cLACommisionNewSchema.getTransState();
    	String tAgentCode = cLACommisionNewSchema.getAgentCode();
    	String tGrpcontno = cLACommisionNewSchema.getGrpContNo();//用来判断是否个团的，实际上可以用团个标记来替换，后面如果要改的话，需要改对应的函数 及配置
    	String tRiskCode = cLACommisionNewSchema.getRiskCode();
    	String tCValiDate = cLACommisionNewSchema.getCValiDate();//此日期 没有用，函数 里面也没有使用，不知道当时是有什么样的业务
    	
		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("wageno");
		tCalculator.addBasicFactor("GetPolDate", tGetPolDate);
		tCalculator.addBasicFactor("SignDate", tSignDate);
		tCalculator.addBasicFactor("TMakeDate", tTMakeDate);
		tCalculator.addBasicFactor("CustomGetPolDate", tCustomGetPolDate);
		tCalculator.addBasicFactor("BranchType", tBranchType);
		tCalculator.addBasicFactor("BranchType2", tBranchType2);
		tCalculator.addBasicFactor("ValiDate", tCValiDate);
		tCalculator.addBasicFactor("PayCount", tPayCount);
		tCalculator.addBasicFactor("Flag", tFlag);
		tCalculator.addBasicFactor("ConfDate", tConfDate);
		tCalculator.addBasicFactor("AFlag", tAFlag);
		tCalculator.addBasicFactor("ManageCom", tManageCom);
		tCalculator.addBasicFactor("VisistTime", tVisistTime);
		tCalculator.addBasicFactor("RiskFlag", tRiskFlag);
		tCalculator.addBasicFactor("TransState", tTransState);
		tCalculator.addBasicFactor("AgentCode", tAgentCode);
		tCalculator.addBasicFactor("GroupAgentCode", tGrpcontno);
		tCalculator.addBasicFactor("cRiskCode", tRiskCode);
//		tCalculator.addBasicFactor("VisitFlag", "Y");
		String tVisitFlag = "Y";
		//用来兼容校验回访日期中：2655 需求，对于个险河北回访日期的数据进行不校验回访日期：
		if("8613".equals(cLACommisionNewSchema.getManageCom().substring(0,4))
				&&"1".equals(cLACommisionNewSchema.getBranchType())&&"01".equals(cLACommisionNewSchema.getBranchType2()))
		{
			tVisitFlag = judgeVisitFlag(cLACommisionNewSchema.getReceiptNo(), cLACommisionNewSchema.getContNo(),cLACommisionNewSchema.getGrpContNo());
		}
		tCalculator.addBasicFactor("VisitFlag", tVisitFlag);
		String tResult = tCalculator.calculate();
		if("0".equals(tResult))
		{
			tResult = null;
		}
		return tResult == null ? "" : tResult;
    }
    /**
     * 把字符串日期格式：YYYY-MM-DD或YYYY-M-D,转化在YYYYMMDD
     * @param tCalDate
     * @return
     */
    public String getWageNo(String tCalDate)
    {
    	if(-1==tCalDate.indexOf("-"))
    	{
    		return null;
    	}
    	String[] tDates = tCalDate.split("-");
    	String tYear = tDates[0];
    	String tMonth = tDates[1];
//    	String tDay = tDates[2];
    	if(1==tMonth.length())
    	{
    		tMonth = "0"+tMonth;
    	}
//    	if(1==tDay.length())
//    	{
//    		tDay = "0"+tMonth;
//    	}
    	String tWageNo = tYear.trim()+tMonth.trim();
    	return tWageNo;
    }
    /**
     * 根据团队代码，返回团队信息
     * @param tAgentGroup
     * @return
     */
    public LABranchGroupSchema querytLABranchGroup(String tAgentGroup) 
    {
        String sqlStr = "select * from LABranchGroup where AgentGroup='" +
                        tAgentGroup + "' with ur ";
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sqlStr);
        if (tLABranchGroupDB.mErrors.needDealError() == true) {
            return null;
        }
        if (tLABranchGroupSet.size() > 0) {
            tLABranchGroupSchema = tLABranchGroupSet.get(1);
            return tLABranchGroupSchema;
        } else {
            return null;
        }
    }
    /**
     * 查询中介机构
     * @param tAgentCom
     * @return lacom
     */
    public LAComSchema querytLACom(String tAgentCom){
    	String tSql ="select * from lacom where agentcom ='"+tAgentCom+"' with ur ";
    	LAComSchema tLAComSchema = new LAComSchema();
    	LAComSet tLAComSet = new LAComSet();
    	LAComDB tLAComDB = new LAComDB();
    	tLAComSet = tLAComDB.executeQuery(tSql);
    	if(tLAComDB.mErrors.needDealError() == true){
    		return null;
    	}
    	if(tLAComSet.size() > 0){
    		tLAComSchema = tLAComSet.get(1);
    		return tLAComSchema;
    	}else{
    	    return null;
    	}
    }
    /**
     * 万能险基本保费算法
     * @param cCalCode  算法代码
     * @param cPrem     保费
     * @param cPayIntv  缴费方式 
     * @param cAmnt     保额 
     * @param cInsuredAge 被保人年龄
     * @return
     */
    public double calTrans(String cCalCode,double cPrem, int cPayIntv,double cAmnt,int cInsuredAge) 
    {
       Calculator tCalculator = new Calculator(); //计算类
       String tPrem=String.valueOf(cPrem);
       String tPayIntv=String.valueOf(cPayIntv);
       String tAmnt=String.valueOf(cAmnt);
       
       tCalculator.setCalCode(cCalCode); //添加计算编码
       tCalculator.addBasicFactor("Prem", tPrem); //
       tCalculator.addBasicFactor("PayIntv", tPayIntv); //
       tCalculator.addBasicFactor("Amnt", tAmnt); //
       tCalculator.addBasicFactor("InsuredAge", String.valueOf(cInsuredAge)); //
       if (tCalculator.mErrors.needDealError()) {
            return -99;
       }
       //得到结果
       String tvalue = tCalculator.calculate();
       double tReturn = Double.parseDouble(tvalue);
       return tReturn;
   }
    
    /**
     * 获取计算万能险基本保费时用到的基本保额 
     * @param aLCPolSchema LCPolSchema 
     * @return double
     * 2010-5-25 计算初始保费要用投保时保额 CQ号：
     */
    public double getTBAmnt(LCPolSchema aLCPolSchema)
    {
    	ExeSQL tExeSQL = new ExeSQL();
    	double tAmnt = aLCPolSchema.getAmnt();
    	
//    	添加对于湖南相关保单的配置  这一块会在初始化时写到内存里面
//    	String sqlcheck = "select count(1) from lpedorespecialdata where edorvalue='1' and edortype = 'WN' and detailtype = 'ULIFEE' and edorno = '" + aLCPolSchema.getContNo() + "' with ur";
//     	String tCount = tExeSQL.getOneValue(sqlcheck);
//     	if(!"0".equals(tCount))
//     	{
//     		return tAmnt;
//     	}	     	
    	
    	try 
    	{
			String tContNo = aLCPolSchema.getContNo();
			String sql = "select edoracceptno from LPEdorItem a where ContNo = '" + tContNo + "' and EdorType = 'BA' "
				       + "and exists (select 1 from LPEdorApp where EdorAcceptNo = a.EdorAcceptNo and EdorState = '0') " 
				       + "order by edoracceptno ";
			String firstEdorNo = tExeSQL.getOneValue(sql);
			if(!firstEdorNo.equals(""))
			{
				sql = "select Amnt from LPPol where EdorNo = '" + firstEdorNo  + "' and PolNo = '" + aLCPolSchema.getPolNo() 
				    + "' and EdorType = 'BA' ";
				String sAmnt = tExeSQL.getOneValue(sql);
				if("".equals(sAmnt)||null==sAmnt)
				{
					sAmnt = "0";
				}
				tAmnt = Double.parseDouble(sAmnt);
			}
			
		} 
    	catch (RuntimeException e)
    	{
			e.printStackTrace();
			return tAmnt;
		}
    	return tAmnt;
    }
    /**
     * 查询保险期间
     * 此逻辑可以了 解核心的业务后，看能不能优化下
     * @param tGrpPolNo
     * @param tRiskCode
     * @return
     */
    public SSRS queryInsuYear(String tGrpPolNo, String tRiskCode) 
    {
    	String sqlStr =
                "select distinct insuyear,insuyearflag  " 
                +"  from lcpol  where grppolno='"+tGrpPolNo+"' " 
                +" and riskcode='" + tRiskCode +"' "
                +" union select distinct insuyear,insuyearflag "
                +" from lbpol where grppolno='"+tGrpPolNo+"' " 
                +" and riskcode='" + tRiskCode +"' "
                +" fetch first 1 rows only with ur ";
        System.out.println("sqlStr:" + sqlStr);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(sqlStr);
        if (tExeSQL.mErrors.needDealError() == true) {
            return null;
        }
        return tSSRS;
    }
    /**
	 * 判断此条犹豫期退保数据对应的首期保单数据是否已经计算过薪资
	 * @param cPolNo
	 * @return  true 未算过，false 计算过
	 */
	/**
	 * 判断此条犹豫期退保数据对应的首期保单数据是否已经计算过薪资
	 * @param cPolNo
	 * @return 0--找不到之前的首期保单，1--已经计算过薪资的，2--未计算过薪资的
	 */
    public int judgeWageIsCal(String cPolNo)
	{
		ExeSQL tExeSQL = new ExeSQL();
        String sql =
                "select wageno,branchtype,agentcode from LACommisionNew where polno='" +
                cPolNo + "' and TransType='ZC' " +
                " and commisionsn=(select max(commisionsn) from LACommisionNew where polno='" +
                cPolNo + "' and TransType='ZC' ) with ur";
        System.out.println("AgentWageCommonFunction-->judgeWageIsCal+sql:sql:" + sql);
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if(0>=tSSRS.getMaxRow()) 
        {
        	return 0;
        }
        String tWageNo = tSSRS.GetText(1, 1);
        String tAgentCode = tSSRS.GetText(1, 3);
        if (null == tWageNo || "".equals(tWageNo)) {
            return 2;
        }
        String tSQL = "select MakeDate from lawage where "
                      + " IndexCalNo = '" + tWageNo + "'"
                      + " and agentcode = '" + tAgentCode + "' with ur " ;

        System.out.println(tSQL);
        String tMakeDate = tExeSQL.getOneValue(tSQL);
        if (tMakeDate == null || tMakeDate.equals("")) {
            return 2;
        }  
        return 1;
	}
	
	/**
	 * 对于未计算过的犹豫期退保数据对应的首期保单数据，更新其状态，使其不参与薪资计算中
	 * @param cPolNo
	 * @return
	 */
	public LACommisionNewSet getZCDataOfLACommisionNewSet(String cPolNo)
	{
		LACommisionNewSet tReturnLACommisionNewSet = new LACommisionNewSet();
		LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
		String sql ="select * from LACommisionNew a where polno='"+cPolNo+"' "
		 		+ " and TransType='ZC' and not exists(select 1 from lawage b where a.wageno= b.indexcalno and a.agentcode = b.agentcode )" 
	            + " with ur";
		LACommisionNewDB tLACommisionNewDB = new LACommisionNewDB();
		tLACommisionNewSet = tLACommisionNewDB.executeQuery(sql);
		for(int i = 1; i <= tLACommisionNewSet.size();i++)
		{
			LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
			tLACommisionNewSchema = tLACommisionNewSet.get(i);
			//不计入到佣金中
			tLACommisionNewSchema.setCommDire("2");
			tReturnLACommisionNewSet.add(tLACommisionNewSchema);
		}
		return tReturnLACommisionNewSet;
	}
	/**
     * 获取缴费期数，
     * 感觉 这个有点不准确，需要了解核心的业务，再修改此点
     * @param GrpPolNo
     * @return
     */
    public String getPayYears(String cGrpPolNo)
    {
    	ExeSQL tExeSQL=new ExeSQL();
   	 	String tResult=null;
   	 	String tSQL="select payyears from lcpol where grppolno='"
   	 			+cGrpPolNo+"'  and  grpcontno<>'00000000000000000000'" +
                    " order by modifydate desc fetch first 1 rows only with ur";
   	 	tResult=tExeSQL.getOneValue(tSQL);
   	 	if(tResult==null)
   	 	{
   	 		tResult="0";
   	 	}
   	 	return tResult;
	}
    
    /**
     * 连接数据库，执行增删改
     * @param cSchemaSet  需要处理的数据
     * @param cOperator   对于数据库执行的操作
     * @return
     */
    public boolean dealDataToTable(MMap cMMap)
    {
    	VData tVData = new VData();
    	tVData.add(cMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		return tPubSubmit.submitData(tVData, "");
    }

	
	/**
	 * 根据传入的保单渠道，返回销售对应的渠道
	 * @param cSalechnl
	 * @return
	 */
	public String[] getSaleChnlToBranchType(String cSalechnl) 
	{
		if(AgentWageInitialize.getmSaleChnlToBranchType().containsKey(cSalechnl))
		{
			return AgentWageInitialize.getmSaleChnlToBranchType().get(cSalechnl);
		}
		return null;
	}

	/**
	 * 根据传入的险种代码，返回回访标志
	 * @param cRiskCode
	 * @return
	 */
	public String getVisitRiskCode(String cRiskCode) 
	{
		if(AgentWageInitialize.getmVisitRiskCode().containsKey(cRiskCode))
		{
			return AgentWageInitialize.getmVisitRiskCode().get(cRiskCode);
		}
		return "null";
	}

	/**
	 * 根据传入的险种，返回算法代码
	 * @param cRiskCode
	 * @return
	 */
	public String getUniversalLifeIns(String cRiskCode) 
	{
		if(AgentWageInitialize.getmUniversalLifeIns().containsKey(cRiskCode))
		{
			return AgentWageInitialize.getmUniversalLifeIns().get(cRiskCode);
		}
		return AgentWageOfCodeDescribe.RETURN_STRING;
	}

	/**
	 * 根据传入的保单号，返回次数
	 * @param cContNo
	 * @return
	 */
	public Integer getHuiNanSpecialPolicy(String cContNo) 
	{
		if(AgentWageInitialize.getmHuiNanSpecialPolicy().containsKey(cContNo))
		{
			return AgentWageInitialize.getmHuiNanSpecialPolicy().get(cContNo);
		}
		return AgentWageOfCodeDescribe.RETURN_INTEGER;
	}
	
	/**
	 * 返回已经提取过的数据量
	 * @return
	 */
	public int getUseNo() 
	{
		return AgentWageInitialize.getmUseNo();
	}
	
	/**
	 * 返回生成扎账数据的最大值 
	 * @return
	 */
	public int getInitializeCommisionsn() 
	{
		return AgentWageInitialize.getmInitializeCommisionsn();
	}

	/**
	 * 根据传入的展业类型||渠道||险种，返回对应的fyc计算代码
	 * @param cRiskCode
	 * @return
	 */
	public String getCalFycCode(String cBranchtypeRiskCode) 
	{
		if(AgentWageInitialize.getmCalFycCode().containsKey(cBranchtypeRiskCode))
		{
			return AgentWageInitialize.getmCalFycCode().get(cBranchtypeRiskCode);
		}
		return AgentWageOfCodeDescribe.RETURN_STRING;
	}

	/**
	 * 根据传入的展业类型||渠道||险种，返回对应的标准保费计算代码
	 * @param cBranchtypeRiskCode
	 * @return
	 */
	public String getCalStandPremCode(String cBranchtypeRiskCode) 
	{
		if(AgentWageInitialize.getmCalStandPremCode().containsKey(cBranchtypeRiskCode))
		{
			return AgentWageInitialize.getmCalStandPremCode().get(cBranchtypeRiskCode);
		}
		return AgentWageOfCodeDescribe.RETURN_STRING;
	}

	/**
	 * 根据传入的展业类型||渠道||险种，返回对应的职团标准保费计算代码
	 * @param cBranchtypeRiskCode
	 * @return
	 */
	public String getCalStandPremGroupCode(String cBranchtypeRiskCode) 
	{
		if(AgentWageInitialize.getmCalStandPremGroupCode().containsKey(cBranchtypeRiskCode))
		{
			return AgentWageInitialize.getmCalStandPremGroupCode().get(cBranchtypeRiskCode);
		}
		return AgentWageOfCodeDescribe.RETURN_STRING;
	}
	
	/**
	 * 根据传入的展业类型||渠道||险种，返回对应的按管理费来计算fyc的计算代码
	 * @param cBranchtypeRiskCode
	 * @return
	 */
	public String getCalSpecialFycCode(String cBranchtypeRiskCode) 
	{
		if(AgentWageInitialize.getmCalSpecialFycCode().containsKey(cBranchtypeRiskCode))
		{
			return AgentWageInitialize.getmCalSpecialFycCode().get(cBranchtypeRiskCode);
		}
		return AgentWageOfCodeDescribe.RETURN_STRING;
	}
	/**
	 * 根据传入的展业类型||渠道||险种，返回对应的团险手续费比例的计算代码
	 * @param cBranchtypeRiskCode
	 * @return
	 */
	public String getCalChargeCode(String cBranchtypeRiskCode) 
	{
		if(AgentWageInitialize.getmCalChargeCode().containsKey(cBranchtypeRiskCode))
		{
			return AgentWageInitialize.getmCalChargeCode().get(cBranchtypeRiskCode);
		}
		return AgentWageOfCodeDescribe.RETURN_STRING;
	}
	
	/**
	 * 根据传入的错误代码，返回对应的错误描述
	 * @param cErrorCode
	 * @return
	 */
	public String getErrorMessage(String cErrorCode) 
	{
		if(AgentWageInitialize.getmErrorDescribe().containsKey(cErrorCode))
		{
			return AgentWageInitialize.getmErrorDescribe().get(cErrorCode);
		}
		return AgentWageOfCodeDescribe.RETURN_STRING;
	}
	
	/**
	 * 根据传入的险种代码，返回对应的值 
	 * @param cErrorCode
	 * @return
	 */
	public String getSpecialSign(String cRiskCode) 
	{
		if(AgentWageInitialize.getmStandPremFycRiskCode().containsKey(cRiskCode))
		{
			return AgentWageInitialize.getmStandPremFycRiskCode().get(cRiskCode);
		}
		return AgentWageOfCodeDescribe.RETURN_STRING;
	}
	
	/**
     * 得到commisionsn的最大值 
     * @return
     */
    public String getCommisionSN()
    {
    	AgentWageInitialize.setmUseNo(1);
        int tNo =(AgentWageInitialize.getmInitializeCommisionsn()+AgentWageInitialize.getmUseNo())*10;
        String tCommisionsn = PubFun.LCh(String.valueOf(tNo), "0", 10);
        return tCommisionsn;
    }
    /**
     * 根据传入不同的schema，封装不同的数据
     * @param cSchema   传入的源来数据
     * @param cTableName  传入查询的表名
     * @param cDataTYpe   传入查询时的数据类型，这个是定义好的
     * @param cErrorCode  传入错误代码，这个也 是定义好的  ldcode 表中  codetype ='lacommisionerror'
     * @param cOperator   主要是用来标记是系统 提数还是手工补提
     * @return
     */
    public LACommisionErrorSchema dealErrorData(Schema cSchema,String cTableName,String cDataTYpe,String cErrorCode,String cOperator)
    {
    	LACommisionErrorSchema tLACommisionErrorSchema = new LACommisionErrorSchema();
    	tLACommisionErrorSchema.setSourceTableName(cTableName);
    	String tCommisionSN = getCommisionSN();
    	tLACommisionErrorSchema.setCommisionSN(PubFun.getCurrentDate2()+tCommisionSN);
    	tLACommisionErrorSchema.setDataType(cDataTYpe);
    	tLACommisionErrorSchema.setErrorCode(cErrorCode);
    	tLACommisionErrorSchema.setErrorMessage(this.getErrorMessage(cErrorCode));
    	if(AgentWageOfCodeDescribe.TABLENAME_LJAPAYPERSON.equals(cTableName))
    	{
    		LJAPayPersonSchema tLJAPayPersonSchema = (LJAPayPersonSchema)cSchema;
    		tLACommisionErrorSchema.setPolNo(tLJAPayPersonSchema.getPolNo());
    		tLACommisionErrorSchema.setPayNo(tLJAPayPersonSchema.getPayNo());
    		tLACommisionErrorSchema.setDutyCode(tLJAPayPersonSchema.getDutyCode());
    		tLACommisionErrorSchema.setPayPlanCode(tLJAPayPersonSchema.getPayPlanCode());
    		tLACommisionErrorSchema.setPayType(tLJAPayPersonSchema.getPayType());
    		tLACommisionErrorSchema.setAgentCode(tLJAPayPersonSchema.getAgentCode());
    		tLACommisionErrorSchema.setTMakeDate(tLJAPayPersonSchema.getMakeDate());
    	}
    	if(AgentWageOfCodeDescribe.TABLENAME_LJAPAYGRP.equals(cTableName))
    	{
    		LJAPayGrpSchema tLJAPayGrpSchema = (LJAPayGrpSchema)cSchema;
    		tLACommisionErrorSchema.setGrpPolNo(tLJAPayGrpSchema.getGrpPolNo());
    		tLACommisionErrorSchema.setPayNo(tLJAPayGrpSchema.getPayNo());
    		tLACommisionErrorSchema.setPayType(tLJAPayGrpSchema.getPayType());
    		tLACommisionErrorSchema.setTMakeDate(tLJAPayGrpSchema.getMakeDate());
    	}
    	if(AgentWageOfCodeDescribe.TABLENAME_LJAGETENDORSE.equals(cTableName))
    	{
    		LJAGetEndorseSchema tLJAGetEndorseSchema = (LJAGetEndorseSchema)cSchema;
    		tLACommisionErrorSchema.setActugetNo(tLJAGetEndorseSchema.getActuGetNo());
    		tLACommisionErrorSchema.setEndorsementNo(tLJAGetEndorseSchema.getEndorsementNo());
    		tLACommisionErrorSchema.setFeeOperationType(tLJAGetEndorseSchema.getFeeOperationType());
    		tLACommisionErrorSchema.setFeeFinaType(tLJAGetEndorseSchema.getFeeFinaType());;
    		tLACommisionErrorSchema.setPolNo(tLJAGetEndorseSchema.getPolNo());
    		tLACommisionErrorSchema.setOtherNo(tLJAGetEndorseSchema.getOtherNo());
    		tLACommisionErrorSchema.setDutyCode(tLJAGetEndorseSchema.getDutyCode());
    		tLACommisionErrorSchema.setPayPlanCode(tLJAGetEndorseSchema.getPayPlanCode());
    		tLACommisionErrorSchema.setTMakeDate(tLJAGetEndorseSchema.getMakeDate());
    	}
    	tLACommisionErrorSchema.setOperator(cOperator);
    	tLACommisionErrorSchema.setMakeDate(PubFun.getCurrentDate());
    	tLACommisionErrorSchema.setMakeTime(PubFun.getCurrentTime());
    	tLACommisionErrorSchema.setModifyDate(PubFun.getCurrentDate());
    	tLACommisionErrorSchema.setModifyTime(PubFun.getCurrentTime());
    	return tLACommisionErrorSchema;
    }
    
    /**
     *  根据传入的参数，返回对应的万能险拆分，包括基本保费和额外保费，及税优的风险保费和账户保费
     * @param cLACommisionNewSchema   此schema是需要返回时用到一些基本要素
     * @param cLCPolSchema  用来计算基本保费和额外保费用到的
     * @param cPayType      判断是否为追加保费，追加保费不区分
     * @param cGetNoticeNo  税优计算账户保费和风险保费时要用到的
     * @return
     */
    public LACommisionNewSet dealExtractData(LACommisionNewSchema cLACommisionNewSchema,LCPolSchema cLCPolSchema,String cPayType,String cGetNoticeNo)
    {
    	LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
    	String tRiskCode = cLACommisionNewSchema.getRiskCode();
        String tCalCode = getUniversalLifeIns(tRiskCode);
        if(!AgentWageOfCodeDescribe.RETURN_STRING.equals(tCalCode)&&!"ZB".equals(cPayType))
        {
        	//判断是税优产品，并且非团单
        	if("Y".equals(tCalCode)&&AgentWageOfCodeDescribe.JUDGE_GRPCONTNO.equals(cLACommisionNewSchema.getGrpContNo()))
        	{
        		String tRiskPremSQL="";
        		//犹豫期退保 
        		if("WT".equals(cPayType))
        		{
        			tRiskPremSQL = "select sum(fee) from lbinsureaccfeetrace where 1=1"
							  + " and moneytype = 'RP' and othertype='10' "
							  + " and edorno='"+cGetNoticeNo+"' "
					  		  + " and otherno ='"+cGetNoticeNo+"' with ur";
        		}
        		else
        		{
        			String tDueFeeType =judgeDueFeeType(cLACommisionNewSchema.getReceiptNo());
        			//首期
        			if("0".equals(tDueFeeType))
        			{
        				tRiskPremSQL = "select sum(fee) from lcinsureaccfeetrace  where 1=1"
        						+ " and othertype ='1' and moneytype ='RP' "
        						+ " and otherno = '"+cLACommisionNewSchema.getPolNo()+"' with ur"; 
        			}
        			else
        			{
        				tRiskPremSQL = "select sum(fee) from lcinsureaccfeetrace  where 1=1"
        						+ " and othertype ='2' and moneytype ='RP' "
        						+ " and otherno='"+cGetNoticeNo+"' with ur"; 
        			}
        		}
        		ExeSQL tExeSQL = new ExeSQL();
        		System.out.println("AgentWageCommonFunction-->dealExtractData:tRiskPremSQL:"+tRiskPremSQL);
        		String tResultPrem = tExeSQL.getOneValue(tRiskPremSQL);
        		if("".equals(tResultPrem)||null==tResultPrem)
        		{
        			tResultPrem = "0";
        		}
        		double tRiskPrem = Double.parseDouble(tResultPrem);
        		double tTransMoney = cLACommisionNewSchema.getTransMoney();
        		String tCommisionsn = getCommisionSN();
        		LACommisionNewSchema tAccountPremLACommisionNewSchema = new LACommisionNewSchema();
        		LACommisionNewSchema tRiskPremLACommisionNewSchema = new LACommisionNewSchema();
        		mReflections.transFields(tAccountPremLACommisionNewSchema,cLACommisionNewSchema);
        		mReflections.transFields(tRiskPremLACommisionNewSchema,cLACommisionNewSchema);
        		tRiskPremLACommisionNewSchema.setCommisionSN(tCommisionsn);
    			tAccountPremLACommisionNewSchema.setTransMoney(tTransMoney-tRiskPrem);
    			tAccountPremLACommisionNewSchema.setTransState("04");
    			tAccountPremLACommisionNewSchema.setTransStandMoney(tTransMoney-tRiskPrem);
    			tRiskPremLACommisionNewSchema.setTransMoney(tRiskPrem);
    			tRiskPremLACommisionNewSchema.setTransState("05");
    			tRiskPremLACommisionNewSchema.setTransStandMoney(tRiskPrem);
        		tLACommisionNewSet.add(tAccountPremLACommisionNewSchema);
        		tLACommisionNewSet.add(tRiskPremLACommisionNewSchema);
        	}
        	else if(!"N".equals(tCalCode))
        	{
        		//额外保费主键生成
        		String tCommisionsn = getCommisionSN();
        		LACommisionNewSchema tBasePremLACommisionNewSchema = new LACommisionNewSchema();
        		LACommisionNewSchema tExtraPremLACommisionNewSchema = new LACommisionNewSchema();
        		mReflections.transFields(tBasePremLACommisionNewSchema,cLACommisionNewSchema);
        		mReflections.transFields(tExtraPremLACommisionNewSchema,cLACommisionNewSchema);
        		tExtraPremLACommisionNewSchema.setCommisionSN(tCommisionsn);
        		double tmprem=cLACommisionNewSchema.getTransMoney();//tLJAPayPersonSchema.getSumActuPayMoney();
        		double tprem=java.lang.Math.abs(tmprem);
        		int    tpayintv=cLCPolSchema.getPayIntv();
        		int tAppntInsuredAge =0;
                String tBirthDay = cLCPolSchema.getInsuredBirthday();
                tAppntInsuredAge = PubFun.calInterval(tBirthDay,cLACommisionNewSchema.getTMakeDate(), "Y");
        		double tamnt1=cLCPolSchema.getAmnt();
        		if(AgentWageOfCodeDescribe.RETURN_INTEGER!=getHuiNanSpecialPolicy(cLCPolSchema.getContNo()))
        		{
        			tamnt1 = getTBAmnt(cLCPolSchema);
        		}
        		double tTransMoney = calTrans(tCalCode, tprem, tpayintv, tamnt1, tAppntInsuredAge);
        		double tTransMoney1 = tprem-tTransMoney;
        		if(tmprem>=0)
                {
        			tBasePremLACommisionNewSchema.setTransMoney(tTransMoney);
        			tBasePremLACommisionNewSchema.setTransState("01");
        			tBasePremLACommisionNewSchema.setTransStandMoney(tTransMoney);
        			tExtraPremLACommisionNewSchema.setTransMoney(tTransMoney1);
        			tExtraPremLACommisionNewSchema.setTransState("02");
        			tExtraPremLACommisionNewSchema.setTransStandMoney(tTransMoney1);
                }
                else
                {
                	tBasePremLACommisionNewSchema.setTransMoney(0 - java.lang.Math.abs(tTransMoney));
                	tBasePremLACommisionNewSchema.setTransState("01");
                	tBasePremLACommisionNewSchema.setTransStandMoney(cLACommisionNewSchema.getTransMoney());
                	tExtraPremLACommisionNewSchema.setTransMoney(0 - java.lang.Math.abs(tTransMoney1));
                	tExtraPremLACommisionNewSchema.setTransState("02");
                	tExtraPremLACommisionNewSchema.setTransStandMoney(cLACommisionNewSchema.getTransMoney());
                }
        		tLACommisionNewSet.add(tBasePremLACommisionNewSchema);
        		tLACommisionNewSet.add(tExtraPremLACommisionNewSchema);
        	}
        }
        return tLACommisionNewSet;
    }
    /**
     * 根据传入的展业类型||渠道||险种，返回对应的fyc计算代码和计算类型
     * @param cBranchtypeRiskCode
     * @return
     */
    public HashMap<String,String> getCalCode(String cBranchtypeRiskCode)
    {
    	HashMap<String,String> tCalCodeHashMap = new HashMap<String,String>();
    	String tCalCode = getCalFycCode(cBranchtypeRiskCode);
    	if(!AgentWageOfCodeDescribe.RETURN_STRING.equals(tCalCode))
    	{
    		tCalCodeHashMap.put("00", tCalCode);
    	}
    	tCalCode = getCalStandPremCode(cBranchtypeRiskCode);
    	if(!AgentWageOfCodeDescribe.RETURN_STRING.equals(tCalCode))
    	{
    		tCalCodeHashMap.put("03", tCalCode);
    	}
    	tCalCode = getCalStandPremGroupCode(cBranchtypeRiskCode);
    	if(!AgentWageOfCodeDescribe.RETURN_STRING.equals(tCalCode))
    	{
    		tCalCodeHashMap.put("06", tCalCode);
    	}
    	tCalCode = getCalSpecialFycCode(cBranchtypeRiskCode);
    	if(!AgentWageOfCodeDescribe.RETURN_STRING.equals(tCalCode))
    	{
    		tCalCodeHashMap.put("10", tCalCode);
    	}
    	tCalCode = getCalChargeCode(cBranchtypeRiskCode);
    	if(!AgentWageOfCodeDescribe.RETURN_STRING.equals(tCalCode))
    	{
    		tCalCodeHashMap.put("51", tCalCode);
    	}
    	return tCalCodeHashMap;
    }
    /**
     * 根据传入的参数 ，返回对应的管理费
     * @param cPolno
     * @param cEndorsementNo
     * @param cOtherNoType
     * @return
     */
    public double getManageFee(String cPolNo,String cOtherNo,String cOtherNoType)
    {
    	String tSql = "select sum(fee) from lcinsureaccclassfee where  PolNo='"+cPolNo+"' "
    			+ " and otherno='"+cOtherNo+"' and othertype='"+cOtherNoType+"'  with ur ";
        ExeSQL tExeSQL = new ExeSQL();
        String tManageFee=tExeSQL.getOneValue(tSql);
        if(null==tManageFee||"".equals(tManageFee))
        {
        	tManageFee="0";
        }
        return Double.parseDouble(tManageFee);
    }
    /**
     * 根据传入的参数 ，返回对应的解约的管理费
     * @param cPolno
     * @param cEndorsementNo
     * @param cOtherNoType
     * @return
     */
    public double getCTManageFee(String cPolNo,String cOtherNo,String cOtherNoType)
    {
    	String tSql = "select sum(money) from lbinsureacctrace  where  moneytype='TM' "
    			+ " and PolNo='"+cPolNo+"' "
    			+ " and otherno='"+cOtherNo+"' and othertype='"+cOtherNoType+"'  with ur ";
        ExeSQL tExeSQL = new ExeSQL();
        String tManageFee=tExeSQL.getOneValue(tSql);
        if(null==tManageFee||"".equals(tManageFee))
        {
        	tManageFee="0";
        }
        return Double.parseDouble(tManageFee);
    }
    /**
     * 得到利息 ，由于有月息的情况，所以只用polno来关联
     * 2017-3-22 yangyang 需求：3290
     * @param cPolNo
     * @param cOtherNo
     * @param cOtherNoType
     * @return
     */
    /**
     * 得到利息 ，由于有月息的情况，所以根据险种来进行分别处理
     * 2017-3-22 yangyang 需求：3290
     * @param cLJAGetEndorseSchema
     * @return
     */
    public double getAccuralfee(LJAGetEndorseSchema cLJAGetEndorseSchema) 
    {
    	String tRiskCode = cLJAGetEndorseSchema.getRiskCode();
    	String tPolNo = cLJAGetEndorseSchema.getPolNo();
    	String tSql ="";
    	if("370201".equals(tRiskCode))
    	{
    		String tPayPlanCode = cLJAGetEndorseSchema.getPayPlanCode();
    		tSql = "select sum(money) from Lbinsureacctrace where  1=1 "
				+ " and PolNo='"+tPolNo+"'"
				+ " and payplancode='"+tPayPlanCode+"'"
				+ " and moneytype='LX' with ur ";
    	}
    	else
    	{
    		String tEndorsementNo = cLJAGetEndorseSchema.getEndorsementNo();
    		String tOtherNoType = cLJAGetEndorseSchema.getOtherNoType();
    		tSql = "select sum(money) from Lbinsureacctrace where  PolNo='"
    	        	+tPolNo+"' and otherno='"+tEndorsementNo
    	        	+"' and othertype='"+tOtherNoType+"' and moneytype='LX' with ur ";
    	}
        ExeSQL tExeSQL = new ExeSQL();
        String tvalue=tExeSQL.getOneValue(tSql);
        if(tvalue==null||tvalue.equals(""))
        {
            tvalue="0";
        }
        return  Double.parseDouble(tvalue);
    }
    /**
     * 根据保单号和险种，得到对应的套餐编码 
     * @param cGrpContNo
     * @param cRiskCode
     * @return
     */
    public String getWrapCode(String cGrpContNo,String cRiskCode)
    {
    	 String tWrapCodeSQL = "select distinct riskwrapcode from lccontplanrisk "
                 +"where grpcontno='" +cGrpContNo+ "' and riskcode='" +cRiskCode+"' with ur ";
         ExeSQL tExeSQL = new ExeSQL();
         String tWrapCode = tExeSQL.getOneValue(tWrapCodeSQL);
         return tWrapCode;
    }
    /**
     * 根据团单号获取保单的最大交至日期
     * @param cGrpPolNo
     * @return
     */
    public String getGrpCurPaytoDate(String cGrpPolNo){
    	String tSql = "", tReturn = "";
        ExeSQL tExeSQL = new ExeSQL();
        tSql =
                "SELECT Max(CurPaytoDate) FROM LJAPayPerson "
                + "WHERE GrpPolNo = '" + cGrpPolNo + "' with ur ";
        tReturn = tExeSQL.getOneValue(tSql);
        if (tExeSQL.mErrors.needDealError()) {
            return null;
        }
        if (tReturn == null || tReturn.equals("")) {
            return null;
        }
        return tReturn;
    }
    
    /**
     * //先算契约管理费 ，如果契约为0 ，则算增人管理费  团单契约：b.incometype='1'  保全：b.incometype='3'
     * @param cEndorsementNo,cGrpPolNo,cPayType,cGetNoticeNo,cPayNo
     * @return
     */
    public String getCManageFee(String cEndorsementNo,String cGrpPolNo,String cPayType,String cGetNoticeNo,String cPayNo) 
    {
    	String tMoney = "0";
    	String sqlStr  = "";
    	// 如果是增人
    	if((null != cGetNoticeNo && !"".equals(cGetNoticeNo))
    			&&(null != cEndorsementNo && !"".equals(cEndorsementNo)))
    	{
    		sqlStr =" select sum(value(c.fee,0)) from lcinsureaccclassfee c where otherno = polno and polno in "
    				+"(select b.polno from  ljagetendorse b where endorsementno='" +cEndorsementNo +"' "
    						+ "and  feeoperationtype ='NI'  and grppolno='" +cGrpPolNo+ "') with ur ";
		}
    	else
    	{
    		//如果是新单
    		sqlStr ="select sum(value(fee,0)) from lcinsureaccfee where polno in "
    				+ "(select polno from LJAPayPerson where grppolno='" + cGrpPolNo +"' "
    						+ "and PayNo='" +cPayNo+ "' and PayType='" +cPayType+"' "
    								+ "and exists (select 1 from ljapay where payno =ljapayperson.payno and incometype = '1'))"
    								+ " with ur" ;
    	}
	    ExeSQL aExeSQL = new ExeSQL();
	    tMoney = aExeSQL.getOneValue(sqlStr);
	    System.out.println("sqlStr:" + sqlStr + "|");
	    if(null==tMoney || "".equals(tMoney))
	    {
	    	tMoney="0";
	    }
    	return tMoney;
    }
    
    /***
     * 查询追加类型
     * @param tEndorsementNo
     * @return
     */
    public String getTransType(String cEndorsementNo)
    {
    	if (cEndorsementNo == null || cEndorsementNo.equals("")) 
    	{
            return "W";
        }
        String tIncomeNo = "";
        String tSQL = "select edortype From lpedoritem where edorno =(select innersource from lgwork where workno ='"
                      + cEndorsementNo + "')  with ur ";
        ExeSQL tExeSQL = new ExeSQL();
        tIncomeNo = tExeSQL.getOneValue(tSQL);
        if (tExeSQL.mErrors.needDealError()) 
        {
            return "W";
        }
        return tIncomeNo;
    	
    }
    
    /**
     * 2655 需求，对于个险河北回访日期的数据进行不校验回访日期：
     * 1)、2015-4-1日前的历史保单万能追加保费数据和涉及到的保全数据 2)、非历史保单中通过保全来收取的追加保费的数据
     * @param cReceiptNo   
     * @param cContNo 保单号
     * @return
     */
    private String judgeVisitFlag(String cReceiptNo,String cContNo,String cGrpContNo)
    {
    	ExeSQL tExeSQL  = new ExeSQL();
 	   	String  tJudgeSql ="";
    	String tHistoryFlag  = "";
    	if(AgentWageOfCodeDescribe.JUDGE_GRPCONTNO.equals(cGrpContNo))
    	{
    		tHistoryFlag ="select 1 from lacommisionNew where contno ='"+cContNo+"' and tmakedate <'2015-4-1' with ur";
    		tJudgeSql="select  1  from lpedoritem where edorno in(select incomeno from ljapay where payno ='"+cReceiptNo+"')  and edortype='ZB' and edorstate='0'";
    	}
    	else
    	{
    		tHistoryFlag ="select 1 from lacommisionNew where grpcontno ='"+cGrpContNo+"' and tmakedate <'2015-4-1' with ur";  
    		tJudgeSql="select  1  from lpgrpedoritem where edorno in(select incomeno from ljapay where payno ='"+cReceiptNo+"')  and edortype='ZB' and edorstate='0'";
    	}
    	String tResult = tExeSQL.getOneValue(tHistoryFlag);
    	if("1".equals(tResult))
    	{
    		return "N";
    	}
 	   	tResult = tExeSQL.getOneValue(tJudgeSql);
 	   	if("1".equals(tResult))
 	   	{
 	   		return "N";
 	   	}
 	   	return "Y";
    }
    
    /**
     * 判断实收数据是首期还是续期
     * @param cPayNo
     * @return
     */
    private String judgeDueFeeType(String cPayNo)
    {
    	ExeSQL tExeSQL  = new ExeSQL();
    	String tSQL = "select DueFeeType from ljapay where payno='"+cPayNo+"' with ur";
    	return tExeSQL.getOneValue(tSQL);
    }
}
