package com.sinosoft.lis.agentcalculate;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LARateStandPremUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LARateStandPremUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LARateStandPremBL tLARateStandPremBL=new LARateStandPremBL();
        try
        {
            if (!tLARateStandPremBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLARateStandPremBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LARateStandPremUI";
                tError.functionName = "submitData";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
      //  mResult=LARateStandPremBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
