package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.db.LACommisionConfigDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionConfigSchema;
import com.sinosoft.lis.vschema.LACommisionConfigSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 把所有的提数给汇总一起，按顺序 进行
 * @author yangyang
 *
 */
public class AgentWageOfFillData {

	/**接收登陆的信息*/
	public GlobalInput mGlobalInput = new GlobalInput();
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
	 
	/**管理机构*/
	public String mManageCom;
	
	/**提数日期*/
	public String mTMakeDate;
	
	/**操作类型*/
	public String mOperator;
	
	/**存储对应的查询条件，为了满足后台逻辑*/
	public String mWhereSQL;
	
	/**团单号，为了运维功能，漏提数据时，可以根据保单号来提取*/
	public String mGrpContNo;
	
	/**个单号，为了运维功能，漏提数据时，可以根据保单号来提取*/
	public String mContNo;
	
	/**用来存储所有扎账提数具体处理类的信息*/
	private LACommisionConfigSet mLACommisionConfigSet = new LACommisionConfigSet();

	/**前台传入要处理的配置信息记录*/
	public String mConfigWhereSql;
	
	/**薪资月*/
	public String mWageNo;
	/**
	 * 调用初始化类，把需要初始化的数据放入到内存中
	 */
	private void init(String cExtractDataTypes)
	{
		initLACommsionConfig(cExtractDataTypes);
		AgentWageInitialize tAgentWageInitialize = new AgentWageInitialize();
	}
	
	public AgentWageOfFillData()
	{
	}
	
	public boolean submitData(VData cVData,String cOperator)
	{
		if(!getInputData(cVData,cOperator))
		{
			return false;
		}
		ExeSQL tExeSQL = new ExeSQL();
		if("update".equals(mOperator))
		{
			String tUpdateSQL = "update lawagenewlog set enddate = enddate -1 days,state ='11' where state ='00' and wageno ='"+mWageNo+"'";
			tExeSQL.execUpdateSQL(tUpdateSQL);
			return true;
		}
		if("all".equals(mOperator))
	    {
			
			String tJudgeSQL = "select count from lawagenewlog where (enddate + 1 days <>'"+mTMakeDate+"' or state ='00')"
							 + " and wageno = (select max(wageno) from lawagenewlog ) with ur";
			String tCount = tExeSQL.getOneValue(tJudgeSQL);
			if(!"0".equals(tCount))
			{
				System.out.println("此日期之前的数据没有执行完成 或此日期已经提过数据，请检查！");
				CError tError = new CError();
				tError.moduleName = "AgentWageOfFillData";
	            tError.functionName = "submitData";
	            tError.errorMessage="此日期之前的数据没有执行完成 或此日期已经提过数据，请检查！";
				mErrors.addOneError(tError);
				return false;
			}
			String tWageNo = getWageNo(mTMakeDate);
			//说明是新的月份，需要向日志表中插入一条数据
	    	if(mTMakeDate.endsWith("-01"))
	    	{
	    		if(!insertLog(tWageNo,mTMakeDate))
	    		{
	    			System.out.println("AgentWageOfFillData-->薪资月"+tWageNo+"插入(insert)日期表日志表中失败,请查明原因!");
	    		}
	    	}
	    	else
	    	{
	    		if(!updateLog(tWageNo,mTMakeDate))
				{
	    			System.out.println("AgentWageOfFillData-->薪资月"+tWageNo+"更新（update）日期表日志表失败,请查明原因!");
				}
	    	}
	    }
		init(mConfigWhereSql);
		for(int i =1;i<=mLACommisionConfigSet.size();i++)
		{
			LACommisionConfigSchema tLACommisionConfigSchema = new LACommisionConfigSchema();
			tLACommisionConfigSchema = mLACommisionConfigSet.get(i);
			String tClassAP = tLACommisionConfigSchema.getClassAP();
			String tExtractSQL = getRealSQL(tLACommisionConfigSchema.getExtractSQL());
			String tExtractDataType = tLACommisionConfigSchema.getExtractDataType();
			String tTableName = tLACommisionConfigSchema.getSourceTableName();
			String tDataType = tLACommisionConfigSchema.getDataType();
			AgentWageOfAbstractClass tAgentWageOfAbstractClass;
				try {
					tAgentWageOfAbstractClass = (AgentWageOfAbstractClass)Class.forName(tClassAP).newInstance();
					//本来想用反射，不过就这几个类，再写个反射，有点不划算，直接 写个set方法得了
					tAgentWageOfAbstractClass.setBaseValue(tExtractDataType,tTableName, tDataType);
					if(!tAgentWageOfAbstractClass.dealData(tExtractSQL,cOperator))
					{
						CError tError = new CError();
						tError.moduleName = "AgentWageOfFillData";
			            tError.functionName = "submitData";
			            tError.errorMessage="AgentWageOfFillData-->"+tClassAP+"类执行报错，请查明原因";
						mErrors.addOneError(tError);
						System.out.println("AgentWageOfFillData-->"+tClassAP+"类执行报错，请查明原因");
						return false;
					}
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		    if("all".equals(mOperator))
		    {
		    	if(!updataLog(tLACommisionConfigSchema))
		    	{
		    		CError tError = new CError();
					tError.moduleName = "AgentWageOfFillData";
		            tError.functionName = "submitData";
		            tError.errorMessage="AgentWageOfFillData-->"+tClassAP+"类执行完成，但更新日志表出错，请查明原因";
					mErrors.addOneError(tError);
		    		System.out.println("AgentWageOfFillData-->"+tClassAP+"类执行完成，但更新日志表出错，请查明原因");
		    		return false;
		    	}
		    }
		}
		System.out.println("AgentWageOfFillData类已经执行完成,"+PubFun.getCurrentDate()+PubFun.getCurrentTime());
		return true;
	}
	/**
	 * 处理前面传来的参数 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData,String cOperator)
	{
		mOperator = cOperator;
		mGlobalInput =  (GlobalInput)cInputData.getObjectByObjectName(AgentWageOfCodeDescribe.WAGENO_GLOBALINPUT, 0);
		TransferData tTransferData = new TransferData();
		tTransferData = (TransferData)cInputData.getObjectByObjectName(AgentWageOfCodeDescribe.WAGENO_TRANSFERDATA, 0);
		mTMakeDate = (String) tTransferData.getValueByName(AgentWageOfCodeDescribe.WAGENO_TAMKEDATE);
		mWageNo = (String) tTransferData.getValueByName("WageNo");
		mConfigWhereSql = (String) tTransferData.getValueByName("configwhereSql");
		String tWhereSQL ="1 ";
		mManageCom = (String) tTransferData.getValueByName(AgentWageOfCodeDescribe.WAGENO_MANAGECOM);
		mGrpContNo = (String) tTransferData.getValueByName(AgentWageOfCodeDescribe.WAGENO_GRPCONTNO);
		mContNo = (String) tTransferData.getValueByName(AgentWageOfCodeDescribe.WAGENO_CONTNO);
		if(!"".equals(mManageCom)&&null!=mManageCom)
		{
			tWhereSQL+= " and managecom ='"+mManageCom+"' ";
		}
		if(!"".equals(mGrpContNo)&&null!=mGrpContNo)
		{
			tWhereSQL+= " and grpcontno ='"+mGrpContNo+"' ";
		}
		if(!"".equals(mContNo)&&null!=mContNo)
		{
			tWhereSQL+= " and contno ='"+mContNo+"' ";
		}
		mWhereSQL = tWhereSQL;
		return true;
	}
	
	/**
	 * 把要执行的提数类相关信息放入到内存中
	 */
	private void initLACommsionConfig(String cExtractDataType)
	{
		String sql = "select * from LACommisionConfig where state ='1' and extractdatatype in ("+cExtractDataType+") order by orderno with ur";
		System.out.println("AgentWageOfFillData-->initLACommsionConfig:sql"+sql);
		LACommisionConfigDB LACommisionConfigDB = new LACommisionConfigDB();
		mLACommisionConfigSet = LACommisionConfigDB.executeQuery(sql);
		System.out.println("AgentWageOfFillData-->initLACommsionConfig:扎账提数佣金sql配置类始化成功");
	}
	
	/**
	 * 把sql中的变量给替换出来
	 * @param cSQL
	 * @return
	 */
	private String getRealSQL(String cSQL)
	{
		 PubCalculator tPubCalculator = new PubCalculator();
         tPubCalculator.addBasicFactor("TMakeDate",mTMakeDate);
         tPubCalculator.addBasicFactor("WhereSQL", mWhereSQL);
         tPubCalculator.setCalSql(cSQL);
         String tRealSql = tPubCalculator.calculateEx();
         return tRealSql;
	}
	
	/**
	 * 根据配置表，更新日志表状态为完成
	 * @param tLACommisionConfigSchema
	 * @return
	 */
	private boolean updataLog(LACommisionConfigSchema tLACommisionConfigSchema)
	{
		String tSourceTableName = tLACommisionConfigSchema.getSourceTableName();
		String tDataType = tLACommisionConfigSchema.getDataType();
		String tUpdateSQL = "update lawagenewlog set state ='11' ,operator='"+mOperator+"',modifydate = current date,modifytime = current time "
							+ " where SourceTableName ='"+tSourceTableName+"' "
							+ " and datatype ='"+tDataType+"'"
							+ " and enddate = '"+mTMakeDate+"'";
		ExeSQL tExeSQL = new ExeSQL();
		return tExeSQL.execUpdateSQL(tUpdateSQL);
	}
	/**
     * 把字符串日期格式：YYYY-MM-DD或YYYY-M-D,转化成 对应的薪资月
     * @param tCalDate
     * @return
     */
    public String getWageNo(String tCalDate)
    {
    	if(-1==tCalDate.indexOf("-"))
    	{
    		return null;
    	}
    	String[] tDates = tCalDate.split("-");
    	String tYear = tDates[0];
    	String tMonth = tDates[1];
//    	String tDay = tDates[2];
    	if(1==tMonth.length())
    	{
    		tMonth = "0"+tMonth;
    	}
//    	if(1==tDay.length())
//    	{
//    		tDay = "0"+tMonth;
//    	}
    	String tWageNo = tYear.trim()+tMonth.trim();
    	return tWageNo;
    }
    /**
     * 向日志 表中插入新的日志 数据，以配置表中有效的配置为基础插入
     * @param cWageNo   要插入的薪资月
     * @param cStartDate  开始提取数据的日期，应该为每月1号
     * @return
     */
    private boolean insertLog(String cWageNo,String cStartDate)
    {
    	String tInsertSQL ="insert into lawagenewlog select '"+cWageNo+"',SourceTableName,datatype,"
    				+ " '"+cStartDate+"','"+cStartDate+"','00',"
    				+ " '','sys',current date ,current time ,current date,current time "
    				+ "  from LACommisionConfig where state ='1' ";
    	ExeSQL tExeSQL = new ExeSQL();
    	return tExeSQL.execUpdateSQL(tInsertSQL);
    }
    /**
     * 更新日志 表的状态及提取终止日期
     * @param cWageNo  要更新日志数据对应的薪资月
     * @param cEndDate 当天要提数的数据日期，应为昨天  
     * @return
     */
    private boolean updateLog(String cWageNo,String cEndDate)
    {
    	String tUpdateSQL ="update lawagenewlog set state ='00',"
    					+ " enddate = '"+cEndDate+"' ,modifydate = current date,modifytime= current time "
    					+ " where wageno ='"+cWageNo+"'";
    	ExeSQL tExeSQL = new ExeSQL();
    	return tExeSQL.execUpdateSQL(tUpdateSQL);
    }
	public static void main(String[] args) {
		GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.ManageCom = "86";
    	tGlobalInput.Operator = "yy";
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue(AgentWageOfCodeDescribe.WAGENO_TAMKEDATE, "2017-9-29");
		tTransferData.setNameAndValue(AgentWageOfCodeDescribe.WAGENO_MANAGECOM, "86420000");
		tTransferData.setNameAndValue(AgentWageOfCodeDescribe.WAGENO_CONTNO, "034992925000001");
		tVData.add(tTransferData);
		tVData.add(tGlobalInput);
		AgentWageOfFillData tAgentWageOfFillData = new AgentWageOfFillData();
		tAgentWageOfFillData.submitData(tVData, "sys");
		
		
	}
} 
