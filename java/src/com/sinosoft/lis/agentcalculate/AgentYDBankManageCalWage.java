package com.sinosoft.lis.agentcalculate;

import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LACommisionNewSchema;
import com.sinosoft.lis.schema.LAIndirectWageSchema;
import com.sinosoft.lis.vschema.LACommisionNewSet;
import com.sinosoft.lis.vschema.LAIndirectWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.RSWrapper;

public class AgentYDBankManageCalWage extends AgentWageOfAbstractClass {
	
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String tsql="select * from lacommisionnew where tmakedate ='2017-9-25' and 1= 1 and branchtype ='3' and branchtype2 ='01' and commdire ='1' and payyear = '0' with ur";
		String tOperator = "sys";
		AgentYDBankManageCalWage tAgentYDBankManageCalWage =new AgentYDBankManageCalWage();
		tAgentYDBankManageCalWage.dealData(tsql, tOperator);
		
	}

	public boolean dealData(String cSQL, String cOperator) {
		LACommisionNewSet tLACommisionNewSet = new LACommisionNewSet();
		RSWrapper tRSWrapper = new RSWrapper();
		boolean tJudgeFlag = false;
		tRSWrapper.prepareData(tLACommisionNewSet, cSQL);
		do {
		    MMap mMap = new MMap();
			tRSWrapper.getData();
			if(!tJudgeFlag){
				if (tLACommisionNewSet == null || tLACommisionNewSet.size() == 0) {
					System.out
							.println("AgentYDBankManageCalWage-->dealData:没有银代的数据！！");
					return true;
				}
				tJudgeFlag=true;
			}
			System.out.println("数据量"+tLACommisionNewSet.size());

			LAIndirectWageSet mLAIndirectWageSchemaSet = new LAIndirectWageSet();
			System.out.println("需要插入的数据量"+mLAIndirectWageSchemaSet.size());
			for (int i = 1; i <= tLACommisionNewSet.size(); i++) {
				LACommisionNewSchema tLACommisionNewSchema = new LACommisionNewSchema();
	            LAIndirectWageSchema mLAIndirectWageSchema = new LAIndirectWageSchema();
				tLACommisionNewSchema = tLACommisionNewSet.get(i);
				String cCommisionSN = tLACommisionNewSchema.getCommisionSN();
				String cBranchType = tLACommisionNewSchema.getBranchType();
				String cBranchType2 = tLACommisionNewSchema.getBranchType2();
				String cRiskCode = tLACommisionNewSchema.getRiskCode();
				String cAgentCom = tLACommisionNewSchema.getAgentCom();
				String cContNo = tLACommisionNewSchema.getContNo();
				String cGrpContNo = tLACommisionNewSchema.getGrpContNo();
				String cManageCom = tLACommisionNewSchema.getManageCom();
				String cPayyears = tLACommisionNewSchema.getPayYears() + "";
				String cPayyear = tLACommisionNewSchema.getPayYear() + "";
				String cRenewCount = tLACommisionNewSchema.getReNewCount() + "";
				String cTransState = tLACommisionNewSchema.getTransState();
				String cPayIntv = tLACommisionNewSchema.getPayIntv() + "";
				String cAgentCode = tLACommisionNewSchema.getAgentCode();
				String cWageNo = tLACommisionNewSchema.getWageNo();
				String cAgentGroup = tLACommisionNewSchema.getAgentGroup();
				// 计算团队首期绩效提奖比例
				String strWageRate = this.calCalRateForAll(cBranchType,
						cBranchType2, cRiskCode, cAgentCom, cContNo,
						cGrpContNo, cManageCom, cPayyears, cPayyear,
						cRenewCount, cTransState, cPayIntv, cAgentCode,
						cWageNo, cAgentGroup);
				double wageRateValue = 0;
				wageRateValue = Double.parseDouble(strWageRate);
				System.out.println("主管职级间接提奖比例为：" + wageRateValue);
				double tTransMoney = tLACommisionNewSchema.getTransMoney();
				double tBankG = tTransMoney * wageRateValue;
				System.out.println("主管职级间接佣金为：" + tBankG);
				// 新单进行折标
				String strStandPrem = this.calCalStandPrem(cCommisionSN,
						cRiskCode, cPayyears, cPayyear, cBranchType,
						cBranchType2);
				double standPrem = 0;
				standPrem = Double.parseDouble(strStandPrem);
	            //存入间接薪资项表
//	            System.out.println("明细扎帐号为："+tLACommisionNewSchema.getCommisionSN());
	            mLAIndirectWageSchema.setCommisionSN(tLACommisionNewSchema.getCommisionSN());
	            mLAIndirectWageSchema.setBankGrpMoney(tBankG);
	            mLAIndirectWageSchema.setGrpRate(wageRateValue);
	            //新单折标保费
	            mLAIndirectWageSchema.setF1(standPrem);
	            mLAIndirectWageSchema.setBranchType(cBranchType);
	            mLAIndirectWageSchema.setBranchType2(cBranchType2);
	            mLAIndirectWageSchema.setOperator(cOperator);
	            mLAIndirectWageSchema.setTMakeDate(tLACommisionNewSchema.getTMakeDate());
	            mLAIndirectWageSchema.setMakeDate(currentDate);
	            mLAIndirectWageSchema.setMakeTime(currentTime);
	            mLAIndirectWageSchema.setModifyDate(currentDate);
	            mLAIndirectWageSchema.setModifyTime(currentTime);
	            mLAIndirectWageSchemaSet.add(mLAIndirectWageSchema);
			}
			mMap.put(mLAIndirectWageSchemaSet, "INSERT");
			try{
				if(!mAgentWageCommonFunction.dealDataToTable(mMap)){
		        	// @@错误处理
		            CError tError = new CError();
		            tError.moduleName = "AgentYDBankOfManageCalWage";
		            tError.functionName = "PubSubmit";
		            tError.errorMessage = "数据插入到LAIndirectWage中失败!";
		            this.mErrors.addOneError(tError);	
		            return false;
				}
			
			}catch(Exception ex){
				ex.printStackTrace();
			}

		} while (0<tLACommisionNewSet.size());
		return true;
	}

	private String calCalRateForAll(String cBranchType, String cBranchType2,
			String cRiskCode, String cAgentCom, String cContNo,
			String cGrpContNo, String cManageCom, String cPayyears,
			String cPayyear, String cRenewCount, String cTransState,
			String cPayIntv, String cAgentCode, String cWageNo,
			String cAgentGroup) {
		String tBranchType = cBranchType == null ? "" : cBranchType;
		String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
		String tRiskCode = cRiskCode == null ? "" : cRiskCode;
		String tAgentCom = cAgentCom == null ? "" : cAgentCom;
		String tContNo = cContNo == null ? "" : cContNo;
		String tGrpContNo = cGrpContNo == null ? "" : cGrpContNo;
		String tManageCom = cManageCom == null ? "" : cManageCom;
		String tPayyears = cPayyears == null ? "" : cPayyears;
		String tPayyear = cPayyear == null ? "" : cPayyear;
		String tRenewCount = cRenewCount == null ? "" : cRenewCount;// 团险根据收费方式采取不同的佣金计算方式
		String tTransState = cTransState == null ? "" : cTransState;
		String tPayIntv = cPayIntv == null ? "" : cPayIntv;
		String tAgentCode = cAgentCode == null ? "" : cAgentCode;
		String tWageNo = cWageNo == null ? "" : cWageNo;
		String tAgentGroup = cAgentGroup == null ? "" : cAgentGroup;
		// if (tBranchType == "")
		// {
		// return "";
		// }
		// if (tBranchType2 == "") {
		// return "";
		// }
		// //如果是续保、续期，则不需要按照回执回销计算
		// else if (mLACommisionSchema.getPayCount()>1)
		// {
		// tFlag="02";
		// }

		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("gRate");
		tCalculator.addBasicFactor("BranchType", tBranchType);
		tCalculator.addBasicFactor("BranchType2", tBranchType2);
		tCalculator.addBasicFactor("RiskCode", tRiskCode);
		tCalculator.addBasicFactor("AgentCom", tAgentCom);
		tCalculator.addBasicFactor("ContNo", tContNo);
		tCalculator.addBasicFactor("GrpContNo", tGrpContNo);
		tCalculator.addBasicFactor("ManageCom", tManageCom);
		tCalculator.addBasicFactor("Payyears", tPayyears);
		tCalculator.addBasicFactor("Payyear", tPayyear);
		tCalculator.addBasicFactor("RenewCount", tRenewCount);
		tCalculator.addBasicFactor("TransState", tTransState);
		tCalculator.addBasicFactor("PayIntv", tPayIntv);
		tCalculator.addBasicFactor("AgentCode", tAgentCode);
		tCalculator.addBasicFactor("WageNo", tWageNo);
		tCalculator.addBasicFactor("AgentGroup", tAgentGroup);

		String tResult = tCalculator.calculate();
		return tResult == null ? "" : tResult;
	}

	private String calCalStandPrem(String cCommisionSN, String cRiskCode,
			String cPayyears, String cPayyear, String cBranchType,
			String cBranchType2) {
		String tCommisionSN = cRiskCode == null ? "" : cCommisionSN;
		String tRiskCode = cRiskCode == null ? "" : cRiskCode;
		String tPayyears = cPayyears == null ? "" : cPayyears;
		String tPayyear = cPayyear == null ? "" : cPayyear;
		String tBranchType = cBranchType == null ? "" : cBranchType;
		String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;

		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("sPrem");
		tCalculator.addBasicFactor("CommisionSN", tCommisionSN);
		tCalculator.addBasicFactor("RiskCode", tRiskCode);
		tCalculator.addBasicFactor("Payyears", tPayyears);
		tCalculator.addBasicFactor("Payyear", tPayyear);
		tCalculator.addBasicFactor("BranchType", tBranchType);
		tCalculator.addBasicFactor("BranchType2", tBranchType2);

		String tResult = tCalculator.calculate();
		return tResult == null ? "" : tResult;
	}
}
