package com.sinosoft.lis.bank;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 业务数据转换到银行系统，银行代收</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class BatchSendBankBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 返回数据的容器 */
    private VData mResult = new VData();

    /** 提交数据的容器 */
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //业务数据
    /** 提数开始日期 */
    private String startDate = "";

    /** 提数结束日期 */
    private String endDate = "";

    /** 银行编码 */
    private String bankCode = "";

    private LJSPaySet outLJSPaySet = new LJSPaySet();
    
    private LJSPaySet oLJSPaySet = new LJSPaySet();

    public BatchSendBankBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("---End dealData---");

        //银行代收
        if (mOperate.equals("GETMONEY"))
        {
            //准备往后台的数据
            if (!prepareOutputData())
            {
                return false;
            }
            System.out.println("---End prepareOutputData---");

            System.out.println("Start PubSubmit BLS Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("End PubSubmit BLS Submit...");
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            TransferData tTransferData = (TransferData) mInputData
                    .getObjectByObjectName("TransferData", 0);
            startDate = (String) tTransferData.getValueByName("startDate");
            endDate = (String) tTransferData.getValueByName("endDate");
            bankCode = (String) tTransferData.getValueByName("bankCode");

            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 获取交费日期在设置的日期区间内的应收总表记录
     * @param startDate
     * @param endDate
     * @return
     */
    private LJSPaySet getLJSPayByPaydate(String startDate, String endDate,
            String bankCode)
    {
        String tSql = "";
        String tSql2 = "";
        String tSql3 = "";

        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("MaxSendToBankCount");

        //不指定开始时间
        if (startDate.equals(""))
        {
            //规则：最早应收日期<=结束时间，最晚交费日期>结束时间
            //     银行编码匹配，机构编码向下匹配，不在途，有账号
            //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
            String sql = "select * from LJSPay where StartPayDate <= '"
                    + endDate
                    + "'"
                    + " and PayDate >= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0 "
                    + " and (BankAccNo <> '') and (AccName <> '') and bankcode not in ('489102','999100') and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7701','7703','7704') and a.bankcode=LJSPay.bankcode) ";
            
            tSql = sql + " and OtherNoType not in ('2','1','3') with ur ";
            //续期划帐,提取所有续期续保数据
            tSql2 = "select * from LJSPay where StartPayDate <= '"
                    + endDate
                    + "'"
                    + " and  PayDate >= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0 "
                    + " and (BankAccNo <> '') and (AccName <> '') and 0=(select COUNT(enteraccdate) from ljtempfee where  tempfeeno=LJSPay.getnoticeno and tempfeetype='2' ) "
                    + " and OtherNoType='2' and bankcode not in ('489102','999100') and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7701','7703','7704') and a.bankcode=LJSPay.bankcode)  with ur ";
            tSql3 = sql + " and OtherNoType = '1' " 
            	    	+ " and exists ( select grpcontno from lcgrppol where riskcode  in ('162601','162801')  "            		
            	    	+ " and exists (select 1 from lcrnewstatelog where grpcontno = lcgrppol.grpcontno and state = '4') and grpcontno = ljspay.otherno)"
                    + " and 0=(select COUNT(enteraccdate) from ljtempfee where  tempfeeno=LJSPay.getnoticeno ) "
        		        + " and bankcode not in ('489102','999100') and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7701','7703','7704') and a.bankcode=LJSPay.bankcode) with ur ";

         }
        //指定开始时间和结束时间
        else
        {
            //规则：最早应收日期>=开始时间，最早应收日期<=结束时间，最晚交费日期>结束时间
            //     银行编码匹配，机构编码向下匹配，不在途，有账号
            //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
            String sql = "select * from LJSPay where StartPayDate >= '"
                    + startDate
                    + "'"
                    + " and StartPayDate <= '"
                    + endDate
                    + "'"
                    + " and PayDate >= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank = '0' or CanSendBank is null ) and sumduepaymoney<>0 "
                    + " and (BankAccNo <> '') and (AccName <> '') ";

            tSql = sql
                    + " and OtherNoType not in ('2','1','3') and bankcode not in ('489102','999100') and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7701','7703','7704') and a.bankcode=LJSPay.bankcode) with ur ";
            
            tSql2 = "select * from LJSPay where StartPayDate <= '"
                    + endDate
                    + "' and StartPayDate >='"
                    + startDate
                    + "'"
                    + " and  PayDate >= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0 "
                    + " and (BankAccNo <> '') and (AccName <> '') and 0=(select COUNT(enteraccdate) from ljtempfee where  tempfeeno=LJSPay.getnoticeno and tempfeetype='2' ) "
                    + " and OtherNoType='2' and bankcode not in ('489102','999100') and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7701','7703','7704') and a.bankcode=LJSPay.bankcode) with ur ";
        tSql3 = sql + " and OtherNoType = '1' " 
            	    	+ " and exists ( select grpcontno from lcgrppol where riskcode  in ('162601','162801')  "            		
            	    	+ " and exists (select 1 from lcrnewstatelog where grpcontno = lcgrppol.grpcontno and state = '4') and grpcontno = ljspay.otherno)"
                    + " and 0=(select COUNT(enteraccdate) from ljtempfee where  tempfeeno=LJSPay.getnoticeno ) "
        		        + " and bankcode not in ('489102','999100') and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7701','7703','7704') and a.bankcode=LJSPay.bankcode) with ur ";
     
        }
        System.out.println(tSql);
        System.out.println(tSql2);
        System.out.println(tSql3);

        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSql);
        tLJSPaySet.add(tLJSPayDB.executeQuery(tSql2));
        tLJSPaySet.add(tLJSPayDB.executeQuery(tSql3));

        return tLJSPaySet;
    }

    private LJSPaySet getLJSPay(String startDate, String endDate)
    {
        LJSPaySet tLJSPaySet = new LJSPaySet();

        //获取银行信息，校验是否是银联
        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(bankCode);
        if (!tLDBankDB.getInfo())
        {
            CError.buildErr(this, "获取银行信息（LDBank）失败");
            return null;
        }

        //普通银行
        if (tLDBankDB.getBankUniteFlag() == null
                || tLDBankDB.getBankUniteFlag().equals("0"))
        {
            tLJSPaySet = getLJSPayByPaydate(startDate, endDate, bankCode);
        }
        //银联
        else if (tLDBankDB.getBankUniteFlag().equals("1"))
        {
            LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();

            //unitegroupcode 1代表支持代收 2代表支持代付 3代表支持代收代付
            String bankSql = "select * from ldbankunite where bankunitecode='"
                    + bankCode + "' and unitegroupcode in ('1','3') ";
            System.out.println(bankSql);
            LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB
                    .executeQuery(bankSql);

            if (tLDBankUniteSet.size() == 0)
            {
                CError.buildErr(this, "获取银联相关银行信息（LDBankUnite）失败");
                return null;
            }

            for (int i = 0; i < tLDBankUniteSet.size(); i++)
            {
                tLJSPaySet.add(getLJSPayByPaydate(startDate, endDate,
                        tLDBankUniteSet.get(i + 1).getBankCode()));
            }
        }

        return tLJSPaySet;
    }

    /**
     * 修改应收表银行在途标志,记录发送银行次数
     * @param tLJSPaySet
     * @param flag 校验标识 false标识校验未通过
     * @return
     */
    private LJSPaySet modifyBankFlag(LJSPaySet tLJSPaySet, boolean flag)
    {
        String bankOnTheWay = "";
        String canSend = "";
        if(flag == true){
            bankOnTheWay = "1";
            canSend = "8";
        } else {
            bankOnTheWay = "0";
            canSend = "1";
        }
        for (int i = 0; i < tLJSPaySet.size(); i++)
        {
            LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i + 1);
            tLJSPaySchema.setBankOnTheWayFlag(bankOnTheWay);
            tLJSPaySchema.setCanSendBank(canSend);
            tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
            tLJSPaySchema.setOperator(mGlobalInput.Operator);
            tLJSPaySet.set(i + 1, tLJSPaySchema);
        }

        return tLJSPaySet;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            //银行代收（应收总表LJSPay）

            // if (mOperate.equals("GETMONEY")) {

            //总应收表处理（获取交费日期在设置的日期区间内的记录；获取银行在途标志为N的记录）
            LJSPaySet tLJSPaySet = getLJSPay(startDate, endDate);
            if (tLJSPaySet == null)
            {
                throw new NullPointerException("总应收表处理失败！");
            }
            if (tLJSPaySet.size() == 0)
            {
                throw new NullPointerException("总应收表无数据！");
            }
            if (!CheckLJSpay(tLJSPaySet, bankCode))
            {
                return false;
            }
            System.out.println("---End getLJSPayByPaydate---");

            //修改应收表银行在途标志,记录发送银行次数
            tLJSPaySet = modifyBankFlag(tLJSPaySet, true);
            System.out.println("---End modifyBankFlag---");

            System.out.println("---End getBankLog---");

            outLJSPaySet.set(tLJSPaySet);

        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "数据处理错误:" + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            map.put(outLJSPaySet, "UPDATE");
            map.put(oLJSPaySet, "UPDATE");
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public boolean CheckLJSpay(LJSPaySet tLJSPaySet, String bankcode)
    {
        String failInfo = "";
        LJSPaySet oLJSPaySet = new LJSPaySet();
        LDCodeDB tLDCodeDB = new LDCodeDB();
        String sql = "select * from ldcode where codetype='accnocheckS' and code='"
                + bankcode + "'";
        LDCodeSet tLDCodeSet = tLDCodeDB.executeQuery(sql);
        if (tLDCodeSet != null && tLDCodeSet.size() > 0)
        {
            String checkReg = tLDCodeSet.get(1).getCodeName();
            for (int row = 1; row <= tLJSPaySet.size(); row++)
            {
                String accno = tLJSPaySet.get(row).getBankAccNo();
                if (!accno.trim().matches(checkReg))
                {
                    oLJSPaySet.add(tLJSPaySet.get(row));
                }
            }
        }
        for (int i = 1; i <= oLJSPaySet.size(); i++)
        {
            if (!failInfo.equals(""))
            {
                failInfo += "、" + oLJSPaySet.get(i).getOtherNo();
            }
            else
            {
                failInfo = oLJSPaySet.get(i).getOtherNo();
            }
            tLJSPaySet.remove(oLJSPaySet.get(i));
        }
        if (!failInfo.equals(""))
        {
            failInfo = "业务号" + failInfo + "的帐号格式错误，已被锁定";
        }
        this.oLJSPaySet.set(modifyBankFlag(oLJSPaySet,false));
        this.mResult.add(failInfo);
        return true;
    }
}
