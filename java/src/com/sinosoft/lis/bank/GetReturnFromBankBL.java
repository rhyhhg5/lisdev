package com.sinosoft.lis.bank;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.taskservice.LLClaimFinishTask;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.text.DecimalFormat;
import com.sinosoft.lis.finfee.AfterTempFeeBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到业务系统</p>
 * <p>Description: 转换标记（ConvertFlag）说明：
 * 初始（null）
 * 扣款失败（1）
 * 重复交费（2）
 * 首期直接交费（5）
 * 催收交费且已经交费核销（3）
 * 首期事后交费（6）
 * 事后选银行转账（9）
 * 正常催收且已经在柜台交费（4）
 * 续期催收（7）</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class GetReturnFromBankBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();

  //业务数据
  /** 处理总金额 */
  private String totalMoney = "";
  /** 处理总笔数 */
  private int sumNum = 0;
  /** 批次号 */
  private String serialNo = "";
  /** 银行编码 */
  private String bankCode = "";
  /** 输入银行编码 */
 // private String insbankcode = "";
   /** 输入银行编码 */
  private String getbankcode = "";
  /** 输入银行账户 */
  private String getbankaccno = "";
  /** 输入银行账户自动匹配标记 */
  private String autobankaccno = "";
  /** 银联标记，用来传递保存银联编码 */
  private String bankUniteFlag = "";
  private Reflections mReflections = new Reflections();
  private GlobalInput mGlobalInput = new GlobalInput();

//  private LYReturnFromBankSchema inLYReturnFromBankSchema = new LYReturnFromBankSchema();
  private TransferData inTransferData = new TransferData();
  private LYReturnFromBankSet inLYReturnFromBankSet = new LYReturnFromBankSet();

  private LYDupPaySet outLYDupPaySet = new LYDupPaySet();
  private LYBankLogSchema outLYBankLogSchema = new LYBankLogSchema();
  private LYReturnFromBankSet outDelLYReturnFromBankSet = new LYReturnFromBankSet();
  private LJSPaySet outDelLJSPaySet = new LJSPaySet();
  private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();
  private LJSPaySet outLJSPaySet = new LJSPaySet();
  private LYReturnFromBankBSet outLYReturnFromBankBSet = new LYReturnFromBankBSet();
  private LYSendToBankSet outDelLYSendToBankSet = new LYSendToBankSet();
  private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeSet outUpdateLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeClassSet outUpdateLJTempFeeClassSet = new LJTempFeeClassSet();
  private LYBankUniteLogSchema outLYBankUniteLogSchema = new LYBankUniteLogSchema();
  private LJSPayBSet outLJSPayBSet = new LJSPayBSet();
  private LJSPayPersonBSet outLJSPayPersonBSet = new LJSPayPersonBSet();

  private LJTempFeeSet mTempFeeSetForAfterDeal = new LJTempFeeSet();

  private LOPRTManagerSet outLOPRTManagerSet = new LOPRTManagerSet();
  
  // 银联文件合并标记
  private String mergeFlag = "";

  public GetReturnFromBankBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代收
    if (mOperate.equals("GETMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start GetReturnFromBank BLS Submit...");
      GetReturnFromBankBLS tGetReturnFromBankBLS = new GetReturnFromBankBLS();
      if(tGetReturnFromBankBLS.submitData(mInputData, cOperate) == false)	{
        // @@错误处理
        this.mErrors.copyAllErrors(tGetReturnFromBankBLS.mErrors);
        mResult.clear();
        return false;
      }
      System.out.println("End GetReturnFromBank BLS Submit...");

      //如果有需要处理的错误，则返回
      if (tGetReturnFromBankBLS.mErrors .needDealError())  {
        this.mErrors.copyAllErrors(tGetReturnFromBankBLS.mErrors ) ;
      }
    }

    if (!claimFinish())
    {
        //return false;
    }
    
    if(!afterSubmit())
    {
        return false;
    }

    return true;
  }

  /**
   * 处理财务处理结束后的操作。
   * 本操作在支票确认和银行转帐回盘时都会调用，
   * 请注意实现AfterTempFeeBL接口时要保障实现后的类在这两个地方都能使用
   * @return boolean
   */
  private boolean afterSubmit()
  {
      String tempFeeType = null;
      MMap map = new MMap();
      //注意，一条LJSPay可能对应多个LJTempFee纪录，所以进行处理时要注意相关性
      for(int i = 1; i <= mTempFeeSetForAfterDeal.size(); i++)
      {
          try
          {
              LJTempFeeSchema tLJTempFeeSchema
                  = mTempFeeSetForAfterDeal.get(i).getSchema();
              //按TempFeeType编码调用不同的类
              tempFeeType = tLJTempFeeSchema.getTempFeeType();

              String className = "com.sinosoft.lis.finfee.AfterTempFee" +
                                 tempFeeType + "BL";
              Class confirmClass = Class.forName(className);
              AfterTempFeeBL tAfterTempFeeBL = (AfterTempFeeBL)
                                          confirmClass.newInstance();
              VData data = new VData();
              data.add(mGlobalInput);
              data.add(tLJTempFeeSchema);

              MMap tMMap = tAfterTempFeeBL.getSubmitMMap(data, "");
              if(tMMap == null)
              {
                  mErrors.copyAllErrors(tAfterTempFeeBL.mErrors);
                  return false;
              }
              map.add(tMMap);
          }
          catch(ClassNotFoundException ex)
          {
              System.out.println(tempFeeType + "暂收后处理类!");
          }
          catch(NullPointerException ex)
          {
              ex.printStackTrace();
          }
          catch(Exception ex)
          {
              ex.printStackTrace();
              return false;
          }
      }

      if(map == null || map.size() == 0)
      {
          return true;
      }

      VData data = new VData();
      data.add(map);
      PubSubmit p = new PubSubmit();
      if(!p.submitData(data, ""))
      {
          CError tError = new CError();
          tError.moduleName = "TempFeeBL";
          tError.functionName = "afterSubmit";
          tError.errorMessage = "收费成功，但后续处理失败";
          mErrors.addOneError(tError);
          System.out.println(tError.errorMessage);
          return false;
      }

      return true;
  }

  private boolean claimFinish()
  {
      
      for (int i = 1; i <= mTempFeeSetForAfterDeal.size(); i++)
      {
          if (!mTempFeeSetForAfterDeal.get(i).getTempFeeType().equals("Y") && !mTempFeeSetForAfterDeal.get(i).getTempFeeType().equals("9"))
          {
              continue;
          }
          try
          {
              System.out.println("---成功调到---claimFinish---");
              LLClaimFinishTask tLLClaimFinishTask = new LLClaimFinishTask(
                      mGlobalInput.ManageCom);
              tLLClaimFinishTask.run();
          }
          catch (Exception ex)
          {
              CError tError = new CError();
              tError.moduleName = "GetReturnFromBankBL";
              tError.functionName = "claimFinish";
              tError.errorMessage = "返回处理成功，但理赔预付回收核销失败";
              mErrors.addOneError(tError);
              System.out.println(tError.errorMessage);
              ex.printStackTrace();
              return false;
          }
      }
      return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
//      inLYReturnFromBankSchema = (LYReturnFromBankSchema)mInputData.getObjectByObjectName("LYReturnFromBankSchema", 0);
      inTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);

      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }

  /**
   * 获取交费日期在设置的日期区间内的总应付表记录
   * @param startDate
   * @param endDate
   * @return
   */
  private LJSPaySet getLJSPayByPaydate(String startDate, String endDate) {
    String tSql = "select * from LJSPay where PayDate >= '" + startDate
                + "' and PayDate <= '" + endDate + "'"
                + " and BankOnTheWayFlag = 0";

    LJSPayDB tLJSPayDB = new LJSPayDB();
    LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSql);

    if (tLJSPayDB.mErrors.getErrorCount()>0) return null;
    else return tLJSPaySet;
  }

  /**
   * 生成送银行表数据
   * @param tLJSPaySet
   * @return
   */
  private LYReturnFromBankSet getReturnFromBank(LJSPaySet tLJSPaySet) {
    int i;
    serialNo = PubFun1.CreateMaxNo("1", 20);
    LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();

    for (i=0; i<tLJSPaySet.size(); i++) {
      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i + 1);

      //生成送银行表数据
      LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();
      tLYReturnFromBankSet.add(tLYReturnFromBankSchema);
    }

    return tLYReturnFromBankSet;
  }

  /**
   * 获取银行返回数据
   * @param inLYReturnFromBankSchema
   * @return
   */
  private LYReturnFromBankSet getLYReturnFromBank(String serialNo, String bankCode) {
	  
	  	System.out.println("\n获取发盘数据");
	  	
		LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();

		// 由于深圳银联的平安、深发回盘文件合并，导致程序处理有问题
		// 在此增加多文件返回处理时，银行合并的相关配置
		String codeSql = "select code,codealias from ldcode1 where codetype='bankunitedeal' "
				+ "and code=(select bankcode from lybanklog where serialno='"
				+ serialNo + "') " + "and code1='" + bankCode + "' with ur";
		ExeSQL tExeSql = new ExeSQL();
		// 获取合并银行编码
		SSRS codeSSRS = tExeSql.execSQL(codeSql);

		if (codeSSRS.getMaxRow() > 0) {
			// 多文件回盘，有合并银行
			System.out.println("!!------有合并银行了");

			// 获取合并银行编码下的银行
			String uniteSql = "select code1 from ldcode1 where codetype='bankunitedeal' "
					+ "and code='"
					+ codeSSRS.GetText(1, 1)
					+ "' "
					+ "and codealias='" + codeSSRS.GetText(1, 2) + "' with ur";
			SSRS uniteSSRS = tExeSql.execSQL(uniteSql);

			// 获取两个银行的回盘数据
			String reSql = "select * from lyreturnfrombank where serialno='"
					+ serialNo + "' and bankcode in (";
			for (int row = 1; row <= uniteSSRS.getMaxRow(); row++) {
				reSql = reSql + "'" + uniteSSRS.GetText(row, 1) + "',";
			}
			if (reSql.lastIndexOf(',') != -1) {
				reSql = reSql.substring(0, reSql.length() - 1) + ")";
				LYReturnFromBankDB tReturnDB = new LYReturnFromBankDB();
				System.out.println(reSql);
				tLYReturnFromBankSet = tReturnDB.executeQuery(reSql);
				if (tLYReturnFromBankSet.size() != 0) {
					// 校验金额是否相等，用于判断银行是否合并后进行的回盘
					String whereSql = reSql.replace(" * ", " sum(paymoney) ");
					String checkSql = "select 1 from lybankunitelog where serialno='"
							+ serialNo
							+ "' and bankcode='"
							+ bankCode
							+ "' and totalmoney=(" + whereSql + ") with ur";
					ExeSQL tExeSQL = new ExeSQL();
					SSRS tSSRS = tExeSQL.execSQL(checkSql);
					if (tSSRS.MaxRow != 0) {
						mergeFlag = reSql;
						return tLYReturnFromBankSet;
					}
				}
			}
		}

		// 没有合并银行，多单文件返回
		LYReturnFromBankDB tLYReturnFromBankDB = new LYReturnFromBankDB();
		tLYReturnFromBankDB.setSerialNo(serialNo);
		tLYReturnFromBankDB.setBankCode(bankCode);
		tLYReturnFromBankSet = tLYReturnFromBankDB.query();

		return tLYReturnFromBankSet;
  }

  /**
   * 获取银行扣款成功标志信息
   * @param tLYReturnFromBankSchema
   * @return
   */
  private String getBankSuccFlag(LYReturnFromBankSchema tLYReturnFromBankSchema) {
    try {
      LDBankSchema tLDBankSchema = new LDBankSchema();
      
      ExeSQL tExeSQL = new ExeSQL();
      String bankSql = "select 1 from ldcode where codetype='bankunitecode' and code='" + bankCode + "'";
      SSRS tSSRS = tExeSQL.execSQL(bankSql);
      
      if(tSSRS != null && tSSRS.getMaxRow() > 0) {
    	  tLDBankSchema.setBankCode(bankCode);
      }
      else{
          //银联标记为空，表示普通银行
          if (bankUniteFlag.equals("")) {
            tLDBankSchema.setBankCode(tLYReturnFromBankSchema.getBankCode());
          }
          else {
            tLDBankSchema.setBankCode(bankUniteFlag);
          }
      }
      tLDBankSchema.setSchema(tLDBankSchema.getDB().query().get(1));

      return tLDBankSchema.getAgentPaySuccFlag();
    }
    catch (Exception e) {
      e.printStackTrace();
      throw new NullPointerException("获取银行扣款成功标志信息失败！(getBankSuccFlag) " + e.getMessage());
    }
  }

  /**
   * 校验银行扣款成功标志
   * @param bankSuccFlag
   * @param tLYReturnFromBankSchema
   * @return
   */
  private boolean verifyBankSuccFlag(String bankSuccFlag, LYReturnFromBankSchema tLYReturnFromBankSchema) {
    int i;
    boolean passFlag = false;

    String[] arrSucc = PubFun.split(bankSuccFlag, ";");
    String tSucc = tLYReturnFromBankSchema.getBankSuccFlag();

    for (i=0; i<arrSucc.length; i++) {
      if(arrSucc[i].equals("") && tSucc==null){
          passFlag = true;
          break;
      }
      if (arrSucc[i].equals(tSucc)) {
        passFlag = true;
        break;
      }
    }

    return passFlag;
  }

  /**
   * 校验银行扣款成功与否
   */
  private void verifyBankSucc() {
    int i;
    String bankSuccFlag = getBankSuccFlag(inLYReturnFromBankSet.get(1));

    for (i=0; i<inLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i+1);
      //此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
      if (tLYReturnFromBankSchema.getConvertFlag() != null) continue;

      //如果扣款失败，则设置转换标记为1
      if (!verifyBankSuccFlag(bankSuccFlag, tLYReturnFromBankSchema)) {
        inLYReturnFromBankSet.get(i+1).setConvertFlag("1");
      }else if(verifyBankSuccFlag(bankSuccFlag, tLYReturnFromBankSchema)){
        inLYReturnFromBankSet.get(i+1).setPayType("S");
      }
    }
  }

  /**
   * 校验财务总金额
   * @param tLYReturnFromBankSet
   * @param totalMoney
   * @return
   */
  private boolean confirmTotalMoney(LYReturnFromBankSet tLYReturnFromBankSet, String totalMoney) {
    double sumMoney = 0;
    double fTotalMoney = Double.valueOf(totalMoney).doubleValue();

    for (int i=0; i<tLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet.get(i + 1);
      //此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
      if (tLYReturnFromBankSchema.getConvertFlag() != null) continue;

      sumMoney = sumMoney + tLYReturnFromBankSchema.getPayMoney();
      //转换精度
      sumMoney = Double.parseDouble((new DecimalFormat("0.00")).format(sumMoney));
      sumNum = sumNum + 1;
    }
    System.out.println("输入金额：" + fTotalMoney );
    System.out.println("数据金额：" + sumMoney);
    return (fTotalMoney == sumMoney);
  }

  /**
   * 记录入重复交费记录表
   * @param tLYReturnFromBankSchema
   */
  private void setLYDupPay(LYReturnFromBankSchema tLYReturnFromBankSchema) {
    LYDupPaySchema tLYDupPaySchema = new LYDupPaySchema();
    mReflections.transFields(tLYDupPaySchema, tLYReturnFromBankSchema);
    tLYDupPaySchema.setDataType("S");
    outLYDupPaySet.add(tLYDupPaySchema);
  }

  /**
   * 校验每笔金额
   */
  private void verifyUnitMoney() {
    for (int i=0; i<inLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i+1);
      //此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
      if (tLYReturnFromBankSchema.getConvertFlag() != null) continue;

      LJSPaySchema tLJSPaySchema = new LJSPaySchema();
      tLJSPaySchema.setGetNoticeNo(tLYReturnFromBankSchema.getPayCode());
      tLJSPaySchema.setSchema(tLJSPaySchema.getDB().query().get(1));

      //扣款小于应收表中的信息，则转入重复交费表（设置转换标记为2），否则通过
      if (tLJSPaySchema.getSumDuePayMoney() > tLYReturnFromBankSchema.getPayMoney()) {
        setLYDupPay(tLYReturnFromBankSchema);
        tLYReturnFromBankSchema.setConvertFlag("2");
      }
    }
  }

  /**
   * 校验暂交费表
   * @param tLYReturnFromBankSchema
   * @return
   */
  private boolean verifyLJTempFee(LYReturnFromBankSchema tLYReturnFromBankSchema) {
    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();

    tLJTempFeeSchema.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
    LJTempFeeSet tLJTempFeeSet = tLJTempFeeSchema.getDB().query();

    if (tLJTempFeeSet.size() > 0) return true;
    else return false;
  }

  /**
   * 通过银行返回数据获取应收表数据
   * @param tLYReturnFromBankSchema
   * @return
   */
  private LJSPaySchema getLJSPayByLYReturnFromBank(LYReturnFromBankSchema tLYReturnFromBankSchema) {
    try {
      LJSPaySchema tLJSPaySchema = new LJSPaySchema();

      tLJSPaySchema.setGetNoticeNo(tLYReturnFromBankSchema.getPayCode());
      return tLJSPaySchema.getDB().query().get(1);
    }
    catch (Exception e) {
      //总应收表无数据，表示扣款失败，并且已经用其它方式交费
      return null;
    }
  }

  private void setLJTempFeeCont(LYReturnFromBankSchema tLYReturnFromBankSchema) throws Exception
  {
    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);
    String tLimit = PubFun.getNoLimit(tLJSPaySchema.getManageCom());
    String serNo = PubFun1.CreateMaxNo("SERIALNO",tLimit);

    if (tLJSPaySchema.getOtherNoType().equals("1") ||tLJSPaySchema.getOtherNoType().equals("5")) //集体保单号
    {
      LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
      LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();
      tLJSPayGrpDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
      tLJSPayGrpSet=tLJSPayGrpDB.query();
      for (int i=1;i<=tLJSPayGrpSet.size();i++)
      {
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
        tLJSPayGrpSchema=tLJSPayGrpSet.get(i);
        tLJTempFeeSchema.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
        tLJTempFeeSchema.setOtherNo(tLJSPayGrpSchema.getGrpPolNo());
        //首期直接交费
        if (tLYReturnFromBankSchema.getDoType().equals("2")) {
          tLJTempFeeSchema.setTempFeeType("1");
          tLJTempFeeSchema.setOtherNoType("0");
        }
        else if (tLYReturnFromBankSchema.getDoType().equals("1")) {
          //首期事后交费
          if (tLYReturnFromBankSchema.getNoType().equals("6")) {
            tLJTempFeeSchema.setTempFeeType("1");
            tLJTempFeeSchema.setOtherNoType("0");
          }
          //正常续期催收
          else if (tLYReturnFromBankSchema.getNoType().equals("2")) {
            tLJTempFeeSchema.setTempFeeType("2");
            tLJTempFeeSchema.setOtherNoType("1");
            tLJTempFeeSchema.setOtherNo(tLJSPayGrpSchema.getGrpContNo());
          }
        }
        tLJTempFeeSchema.setRiskCode(tLJSPayGrpSchema.getRiskCode());
        tLJTempFeeSchema.setPayIntv(tLJSPayGrpSchema.getPayIntv());
        tLJTempFeeSchema.setPayMoney(tLJSPayGrpSchema.getSumDuePayMoney());
        tLJTempFeeSchema.setPayDate(tLYReturnFromBankSchema.getSendDate());
        tLJTempFeeSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
        if(autobankaccno!=null && "00".equals(autobankaccno)){
        	tLJTempFeeSchema.setManageCom(tLYReturnFromBankSchema.getComCode());
        }else{
        tLJTempFeeSchema.setManageCom(getManageComFromLDFinBank());
        }
        tLJTempFeeSchema.setPolicyCom(tLYReturnFromBankSchema.getComCode());
        tLJTempFeeSchema.setAgentGroup(tLJSPayGrpSchema.getAgentGroup());
        tLJTempFeeSchema.setAgentCode(tLJSPayGrpSchema.getAgentCode());
        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setSerialNo(serNo);
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setCashier(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
        outLJTempFeeSet.add(tLJTempFeeSchema);
        System.out.println("add--outLJTempFeeSet");
        mTempFeeSetForAfterDeal.add(tLJTempFeeSchema);
     }
    }
    else if (tLJSPaySchema.getOtherNoType().equals("2") ||tLJSPaySchema.getOtherNoType().equals("6")) //个人保单号
    {
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = new SSRS();
      String tSql="select riskcode,polno,sum(SumDuePayMoney),contno from LJSPayPerson where GetNoticeNo='"+tLJSPaySchema.getGetNoticeNo()+"' group by riskcode,polno,contno with ur";
      tSSRS = tExeSQL.execSQL(tSql);
      for (int i=1;i<=tSSRS.MaxRow;i++)
      {
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        LJTempFeeSchema tNewLJTempFeeSchema=new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
        tLJTempFeeSchema.setOtherNo(tSSRS.GetText(i,2));
        //首期直接交费
        if (tLYReturnFromBankSchema.getDoType().equals("2")) {
          tLJTempFeeSchema.setTempFeeType("1");
          tLJTempFeeSchema.setOtherNoType("0");
        }
        else if (tLYReturnFromBankSchema.getDoType().equals("1")) {
          //首期事后交费
          if (tLYReturnFromBankSchema.getNoType().equals("6")) {
            tLJTempFeeSchema.setTempFeeType("1");
            tLJTempFeeSchema.setOtherNoType("0");
          }
          //正常续期催收
          else if (tLYReturnFromBankSchema.getNoType().equals("2")) {
            tLJTempFeeSchema.setTempFeeType("2");
            tLJTempFeeSchema.setOtherNoType("0");
            tLJTempFeeSchema.setOtherNo(tSSRS.GetText(i,4));
          }
        }

        LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
        LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
        tLJSPayPersonDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
        tLJSPayPersonDB.setPolNo(tSSRS.GetText(i,2));
        tLJSPayPersonSet=tLJSPayPersonDB.query();
        LJSPayPersonSchema tLJSPayPersonSchema=tLJSPayPersonSet.get(1);
        tLJTempFeeSchema.setRiskCode(tSSRS.GetText(i,1));
        tLJTempFeeSchema.setPayMoney(tSSRS.GetText(i,3));
        tLJTempFeeSchema.setPayDate(tLYReturnFromBankSchema.getSendDate());
        tLJTempFeeSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
        tLJTempFeeSchema.setPayIntv(tLJSPayPersonSchema.getPayIntv());
        if(autobankaccno!=null && "00".equals(autobankaccno)){
        	tLJTempFeeSchema.setManageCom(tLYReturnFromBankSchema.getComCode());
        }else{
        tLJTempFeeSchema.setManageCom(getManageComFromLDFinBank());
        }
        tLJTempFeeSchema.setPolicyCom(tLYReturnFromBankSchema.getComCode());
        tLJTempFeeSchema.setAgentGroup(tLJSPayPersonSchema.getAgentGroup());
        tLJTempFeeSchema.setAgentCode(tLJSPayPersonSchema.getAgentCode());
        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setSerialNo(serNo);
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setCashier(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
        if(outLJTempFeeSet.size()>0&& tLJTempFeeSchema.getTempFeeType().equals("2")){
            for(int n=1;n<=outLJTempFeeSet.size();n++){
                if(StrTool.cTrim(outLJTempFeeSet.get(n).getRiskCode()).equals(tLJTempFeeSchema.getRiskCode())
                  &&StrTool.cTrim(outLJTempFeeSet.get(n).getTempFeeType()).equals(tLJTempFeeSchema.getTempFeeType())
                  &&StrTool.cTrim(outLJTempFeeSet.get(n).getTempFeeNo()).equals(tLJTempFeeSchema.getTempFeeNo())     ){
                    tNewLJTempFeeSchema=outLJTempFeeSet.get(n);
                    double Paymoney=tNewLJTempFeeSchema.getPayMoney()+tLJTempFeeSchema.getPayMoney();
                    System.out.println("Paymoney"+Paymoney);
                    outLJTempFeeSet.remove(tNewLJTempFeeSchema);
                    tLJTempFeeSchema.setRiskCode(tSSRS.GetText(i,1));
                    tLJTempFeeSchema.setOtherNo(tSSRS.GetText(i,4));
                    if(tLJSPayPersonSchema.getGrpContNo().equals("00000000000000000000")){
                        tLJTempFeeSchema.setOtherNoType("0");
                    }else{
                        tLJTempFeeSchema.setOtherNoType("1");
                    }
                    tLJTempFeeSchema.setPayMoney(Paymoney);
                }
            }
        }

        outLJTempFeeSet.add(tLJTempFeeSchema);
        System.out.println("add--outLJTempFeeSet");
        mTempFeeSetForAfterDeal.add(tLJTempFeeSchema);
     }
    }
   
    //为暂交费分类表设置数据
    tLJTempFeeClassSchema.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
    tLJTempFeeClassSchema.setPayMode("4");
    tLJTempFeeClassSchema.setPayMoney(tLYReturnFromBankSchema.getPayMoney());
    tLJTempFeeClassSchema.setPayDate(tLYReturnFromBankSchema.getSendDate());
    tLJTempFeeClassSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
    tLJTempFeeClassSchema.setConfFlag("0");
    tLJTempFeeClassSchema.setSerialNo(serNo);
    tLJTempFeeClassSchema.setOperator(this.mGlobalInput.Operator);
    tLJTempFeeClassSchema.setCashier(this.mGlobalInput.Operator);
    tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
    tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
    tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
    tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
    //tLJTempFeeClassSchema.setInsBankCode(insbankcode);
	tLJTempFeeClassSchema.setInsBankCode(getbankcode);
	if(autobankaccno!=null && "00".equals(autobankaccno)){
		tLJTempFeeClassSchema.setInsBankAccNo(getbankaccnomain(tLJTempFeeClassSchema));
		tLJTempFeeClassSchema.setManageCom(tLYReturnFromBankSchema.getComCode());
	}else{
    tLJTempFeeClassSchema.setInsBankAccNo(getbankaccno);
    tLJTempFeeClassSchema.setManageCom(getManageComFromLDFinBank());
	}
	
    tLJTempFeeClassSchema.setPolicyCom(tLYReturnFromBankSchema.getComCode());
    tLJTempFeeClassSchema.setBankCode(tLYReturnFromBankSchema.getBankCode());
    tLJTempFeeClassSchema.setBankAccNo(tLYReturnFromBankSchema.getAccNo());
    tLJTempFeeClassSchema.setAccName(tLYReturnFromBankSchema.getAccName());
    tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
    tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
    outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
  }

  /**
   * 转入暂交费表
   * @param tLYReturnFromBankSchema
   */
  private void setLJTempFee(LYReturnFromBankSchema tLYReturnFromBankSchema) throws Exception{
    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);

    tLJTempFeeSchema.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
    //首期直接交费
    if (tLYReturnFromBankSchema.getDoType().equals("2")) {
      tLJTempFeeSchema.setTempFeeType("1");
      tLJTempFeeSchema.setOtherNoType("0");
    }
    else if (tLYReturnFromBankSchema.getDoType().equals("1")) {
      //首期事后交费
      if (tLYReturnFromBankSchema.getNoType().equals("6")) {
        tLJTempFeeSchema.setTempFeeType("1");
        tLJTempFeeSchema.setOtherNoType("0");
      }
      //正常续期催收
      else if (tLYReturnFromBankSchema.getNoType().equals("2")) {
        tLJTempFeeSchema.setTempFeeType("2");
        tLJTempFeeSchema.setOtherNoType("0");
      }
    }

    tLJTempFeeSchema.setOtherNo(tLJSPaySchema.getOtherNo());
    tLJTempFeeSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
    tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
    tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
    tLJTempFeeSchema.setConfFlag("0");
    //流水号
    String tLimit = PubFun.getNoLimit(tLJSPaySchema.getManageCom());
    String serNo = PubFun1.CreateMaxNo("SERIALNO",tLimit);
    tLJTempFeeSchema.setSerialNo(serNo);
    if(autobankaccno!=null && "00".equals(autobankaccno)){
    	tLJTempFeeSchema.setManageCom(tLYReturnFromBankSchema.getComCode());
    }else{
    tLJTempFeeSchema.setManageCom(getManageComFromLDFinBank());
    }
    tLJTempFeeSchema.setPolicyCom(tLYReturnFromBankSchema.getComCode());
    tLJTempFeeSchema.setAgentGroup(tLJSPaySchema.getAgentGroup());
    tLJTempFeeSchema.setAgentCode(tLJSPaySchema.getAgentCode());

    tLJTempFeeSchema.setRiskCode(tLJSPaySchema.getRiskCode());
    tLJTempFeeSchema.setPayMoney(tLYReturnFromBankSchema.getPayMoney());
    tLJTempFeeSchema.setPayDate(tLYReturnFromBankSchema.getSendDate());
    tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
    tLJTempFeeSchema.setCashier(mGlobalInput.Operator);
    tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
    tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
    tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
    tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());

    //为暂交费分类表设置数据
    mReflections.transFields(tLJTempFeeClassSchema, tLJTempFeeSchema);
    tLJTempFeeClassSchema.setPayMode("4");
    tLJTempFeeClassSchema.setBankCode(tLYReturnFromBankSchema.getBankCode());
    tLJTempFeeClassSchema.setBankAccNo(tLYReturnFromBankSchema.getAccNo());
    tLJTempFeeClassSchema.setAccName(tLYReturnFromBankSchema.getAccName());

    tLJTempFeeClassSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
    tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
    tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
    //tLJTempFeeClassSchema.setInsBankCode(insbankcode);
	tLJTempFeeClassSchema.setInsBankCode(getbankcode);
  
	if(autobankaccno!=null && "00".equals(autobankaccno)){
		tLJTempFeeClassSchema.setInsBankAccNo(getbankaccnomain(tLJTempFeeClassSchema));
		tLJTempFeeClassSchema.setManageCom(tLYReturnFromBankSchema.getComCode());
	}else{
    tLJTempFeeClassSchema.setInsBankAccNo(getbankaccno);
    tLJTempFeeClassSchema.setManageCom(getManageComFromLDFinBank());
	}
	tLJTempFeeClassSchema.setConfFlag("0");

    //流水号
    tLJTempFeeClassSchema.setSerialNo(serNo);
    tLJTempFeeClassSchema.setPolicyCom(tLYReturnFromBankSchema.getComCode());
    outLJTempFeeSet.add(tLJTempFeeSchema);
    System.out.println("add--outLJTempFeeSet");
    mTempFeeSetForAfterDeal.add(tLJTempFeeSchema);
    outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
  }

  /**
   * 更新暂交费表，适用于事后选择银行转账
   * @param tLYReturnFromBankSchema
   */
  private void updateLJTempFee(LYReturnFromBankSchema tLYReturnFromBankSchema) throws Exception{
    LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);

    //核销暂交费分类表，设置到帐日期
    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
    tLJTempFeeClassDB.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
    tLJTempFeeClassDB.setPayMode("4");  //银行转账
    LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.query();

    for (int i=0; i<tLJTempFeeClassSet.size(); i++) {
      LJTempFeeClassSchema tLJTempFeeClassSchema = tLJTempFeeClassSet.get(i + 1);

      //修改到帐日期
      tLJTempFeeClassSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
      tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
      tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
      tLJTempFeeClassSchema.setCashier(mGlobalInput.Operator);
      //tLJTempFeeClassSchema.setInsBankCode(insbankcode);
	  tLJTempFeeClassSchema.setInsBankCode(getbankcode);
  	if(autobankaccno!=null && "00".equals(autobankaccno)){
		tLJTempFeeClassSchema.setInsBankAccNo(getbankaccnomain(tLJTempFeeClassSchema));
		tLJTempFeeClassSchema.setManageCom(tLYReturnFromBankSchema.getComCode());
	}else{
    tLJTempFeeClassSchema.setInsBankAccNo(getbankaccno);
    tLJTempFeeClassSchema.setManageCom(getManageComFromLDFinBank());
	}
   
      tLJTempFeeClassSchema.setPolicyCom(tLYReturnFromBankSchema.getComCode());
	}

    //校验该暂交费号是否已经全部到帐，是则设置暂交费表的到帐日期，否则不设置
    LJTempFeeClassDB tLJTempFeeClassDB2 = new LJTempFeeClassDB();
    String strSql = "select * from LJTempFeeClass where TempFeeNo='"
                  + tLYReturnFromBankSchema.getPayCode() + "' and PayMode<>'4'";
    LJTempFeeClassSet tLJTempFeeClassSet2 = tLJTempFeeClassDB2.executeQuery(strSql);

    boolean isAllEnterAcc = true;
    for (int j=0; j<tLJTempFeeClassSet2.size(); j++) {
      if (tLJTempFeeClassSet2.get(j+1).getEnterAccDate() == null) {
        isAllEnterAcc = false;
        break;
      }
    }

    if (isAllEnterAcc) {
      LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
      tLJTempFeeDB.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
      LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();

      for (int i=0; i<tLJTempFeeSet.size(); i++) {
        LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i + 1);

        //修改到帐日期
        tLJTempFeeSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
        tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setCashier(mGlobalInput.Operator);
        if(autobankaccno!=null && "00".equals(autobankaccno)){
        	tLJTempFeeSchema.setManageCom(tLYReturnFromBankSchema.getComCode());
        }else{
        tLJTempFeeSchema.setManageCom(getManageComFromLDFinBank());
        }
        tLJTempFeeSchema.setPolicyCom(tLYReturnFromBankSchema.getComCode());

        mTempFeeSetForAfterDeal.add(tLJTempFeeSchema);
      }

      outUpdateLJTempFeeSet.add(tLJTempFeeSet);
    }

    outUpdateLJTempFeeClassSet.add(tLJTempFeeClassSet);
  }

  /**
   * 校验应收表
   */
  private void verifyLJSPay() {
    try {
      for (int i=0; i<inLYReturnFromBankSet.size(); i++) {
        //遍历每一条返回盘记录
        LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i + 1);
       
        //此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
        if (tLYReturnFromBankSchema.getConvertFlag() != null) continue;

        //校验是否首期直接交费
        if (tLYReturnFromBankSchema.getDoType().equals("2")) {
          //记录暂交费表，和暂交费分类表
          setLJTempFee(tLYReturnFromBankSchema);
          tLYReturnFromBankSchema.setConvertFlag("5");
          continue;
        }

        //属于应收表中的催收交费
        if (tLYReturnFromBankSchema.getDoType().equals("1")) {
          //查询应收表
          LJSPaySchema tLJSPaySchema = new LJSPaySchema();
          tLJSPaySchema.setGetNoticeNo(tLYReturnFromBankSchema.getPayCode());
          LJSPaySet tLJSPaySet = tLJSPaySchema.getDB().query();

          //应收表不存在，表示已经交费核销，记录入重复交费记录表
          if (tLJSPaySet.size() == 0) {
            setLYDupPay(tLYReturnFromBankSchema);
            tLYReturnFromBankSchema.setConvertFlag("3");
            continue;
          }

          //应收表记录存在
          if (tLJSPaySet.size() > 0) {
            //GetNoticeNo是LJSPay表主键，所以应该只查询出一条
            tLJSPaySchema = tLJSPaySet.get(1);
            
            //首期事后交费
            if (tLYReturnFromBankSchema.getNoType().equals("6")) {
              outDelLJSPaySet.add(tLJSPaySchema);
              tLYReturnFromBankSchema.setConvertFlag("6");
            }

            //事后选银行转账
            if (tLYReturnFromBankSchema.getNoType().equals("9")||tLYReturnFromBankSchema.getNoType().equals("16")||tLYReturnFromBankSchema.getNoType().equals("5")) {
              outDelLJSPaySet.add(tLJSPaySchema);
              tLYReturnFromBankSchema.setConvertFlag("9");

              //更新暂交费表
              updateLJTempFee(tLYReturnFromBankSchema);
            }

            //保全
            if (tLYReturnFromBankSchema.getNoType().equals("10") ||
                    tLYReturnFromBankSchema.getNoType().equals("3"))
            {
                outDelLJSPaySet.add(tLJSPaySchema);
                tLYReturnFromBankSchema.setConvertFlag("9");

                //更新暂交费表
                updateLJTempFee(tLYReturnFromBankSchema);
            }
            //理赔预付回收
            if (tLYReturnFromBankSchema.getNoType().equals("Y"))
            {                
                tLYReturnFromBankSchema.setConvertFlag("9");               
                updateLJTempFee(tLYReturnFromBankSchema);
            }
            //续期催收
            if (tLYReturnFromBankSchema.getNoType().equals("2")) {
              tLYReturnFromBankSchema.setConvertFlag("7");

              //生成续期划帐成功通知书
              setLOPRTManager(tLYReturnFromBankSchema, "48");
              setLJSPayBState(tLJSPaySchema.getSchema());
            }

            //正常催收
            if (tLYReturnFromBankSchema.getNoType().equals("2") || tLYReturnFromBankSchema.getNoType().equals("6")) {
              //校验暂交费表，有数据表示已经在柜台交费，转入重复交费表
              if (verifyLJTempFee(tLYReturnFromBankSchema)) {
                setLYDupPay(tLYReturnFromBankSchema);
                tLYReturnFromBankSchema.setConvertFlag("4");
              }
              //记录暂交费表
              else
              {
                setLJTempFeeCont(tLYReturnFromBankSchema);
              }
            }
          }
        }
      }
    }
    catch(Exception e) {
      throw new NullPointerException("银行返回盘数据校验失败！(verifyLJSPay) " + e.getMessage());
    }
  }

  /**
   * 变更应收备份数据状态
   * @param tLJSPaySchema LJSPaySchema
   */
  private void setLJSPayBState(LJSPaySchema tLJSPaySchema)
  {
      if(tLJSPaySchema == null || tLJSPaySchema.getGetNoticeNo() == null
          || tLJSPaySchema.getGetNoticeNo().equals("")){
           return;
       }
       LJSPayBDB tLJSPayBDB = new LJSPayBDB();
       tLJSPayBDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
       LJSPayBSet tLJSPayBSet = tLJSPayBDB.query();
       for(int i = 1; i <= tLJSPayBSet.size(); i++)
       {
           tLJSPayBSet.get(i).setDealState("4");
       }
       outLJSPayBSet.add(tLJSPayBSet);

       LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
       tLJSPayPersonBDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
       LJSPayPersonBSet set = tLJSPayPersonBDB.query();
       for(int i = 1; i <= set.size(); i++)
       {
           set.get(i).setDealState("4");
       }
       outLJSPayPersonBSet.add(set);
  }

  /**
     * 生成打印管理表数据
     * @param tLJSPaySchema
     * @param tEdorNo
     * @param type
     * @return
     */
    private void setLOPRTManager(LYReturnFromBankSchema tLYReturnFromBankSchema, String type) {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

        String mLimit = PubFun.getNoLimit(mGlobalInput.ManageCom);
        String serNo = PubFun1.CreateMaxNo("PRTSEQNO", mLimit);
        tLOPRTManagerSchema.setPrtSeq(serNo);

        tLOPRTManagerSchema.setOtherNo(tLYReturnFromBankSchema.getPolNo());
        tLOPRTManagerSchema.setOtherNoType("00"); //个人保单号
        tLOPRTManagerSchema.setCode(type);
        tLOPRTManagerSchema.setManageCom(tLYReturnFromBankSchema.getComCode());
        tLOPRTManagerSchema.setAgentCode(tLYReturnFromBankSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

        outLOPRTManagerSet.add(tLOPRTManagerSchema);
    }

  /**
   * 备份银行返回盘记录表到B表
   */
  private void getLYReturnFromBankB() {
    int i;

    for (i=0; i<inLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i + 1);

      LYReturnFromBankBSchema tLYReturnFromBankBSchema = new LYReturnFromBankBSchema();
      mReflections.transFields(tLYReturnFromBankBSchema, tLYReturnFromBankSchema);
      //因为没有设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
      tLYReturnFromBankBSchema.setRemark(mGlobalInput.Operator);
      tLYReturnFromBankBSchema.setModifyDate(PubFun.getCurrentDate());
      tLYReturnFromBankBSchema.setModifyTime(PubFun.getCurrentTime());
      tLYReturnFromBankBSchema.setBankUnSuccReason(getUnSuccReason(tLYReturnFromBankSchema));
      outLYReturnFromBankBSet.add(tLYReturnFromBankBSchema);
    }
  }
  /**
   * 获取失败原因
   */
  private String getUnSuccReason(LYReturnFromBankSchema tLYReturnFromBankSchema){
      try{
          LDCode1DB tLDCode1DB=new LDCode1DB();
          tLDCode1DB.setCodeType("bankerror");
          if (bankUniteFlag.equals("")) {
              tLDCode1DB.setCode(tLYReturnFromBankSchema.getBankCode());
          }else {
              tLDCode1DB.setCode(bankUniteFlag);
          }
          tLDCode1DB.setCode1(tLYReturnFromBankSchema.getBankSuccFlag());
          return tLDCode1DB.query().get(1).getCodeName();
       }catch(Exception e){
       }
       return "";
  }

  /**
   * 获取发送盘数据，用于删除
   */
  private void getLYSendToBank() {
	  if("".equals(mergeFlag)){
		  LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();

		    tLYSendToBankSchema.setSerialNo(serialNo);
		    ExeSQL tExeSQL = new ExeSQL();
		    String bankSql = "select 1 from ldcode where codetype='bankunitecode' and code='" + bankCode + "'";
		    SSRS tSSRS = tExeSQL.execSQL(bankSql);
		      
		    if(tSSRS != null && tSSRS.getMaxRow() > 0) {
		    }else{
		        tLYSendToBankSchema.setBankCode(bankCode);
		    }
		    
		    outDelLYSendToBankSet = tLYSendToBankSchema.getDB().query();
	  } else {
		  // 银联合并
		  // 改的时间比较紧，简单写了，直接取得回盘数据的提取sql改为发盘的
		  String sql = mergeFlag.replace(" lyreturnfrombank ", " lysendtobank "); 
		  System.out.println(sql);
		  LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
		  outDelLYSendToBankSet = tLYSendToBankDB.executeQuery(sql);
	  }
    
  }

  /**
   * 获取应收表数据，修改银行在途标志
   */
  private void getLJSPay() {
    int i;

    for (i=0; i<inLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i + 1);

      //如果DoType＝2，表示首期银行直接交费，应收表中无数据，故不做银行在途标志的修改
      if (tLYReturnFromBankSchema.getDoType().equals("2")) return;

      String convertFlag = tLYReturnFromBankSchema.getConvertFlag();
      if ((convertFlag != null) && (convertFlag.equals("1"))) {
        LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);
        //总应收表无数据，表示扣款失败，并且已经用其它方式交费
        if (tLJSPaySchema == null) continue;

        tLJSPaySchema.setBankOnTheWayFlag("0");
        //扣款失败，待确认
        if (!tLJSPaySchema.getOtherNoType().equals("2"))

        {
            if(!(tLYReturnFromBankSchema.getComCode().equals("86110000") || tLYReturnFromBankSchema.getComCode().equals("8611")))
                tLJSPaySchema.setCanSendBank("1");
        }else{ 
           /* if(!(tLYReturnFromBankSchema.getComCode().equals("86110000") || tLYReturnFromBankSchema.getComCode().equals("8611")))
                tLJSPaySchema.setCanSendBank("1");*/
        	//功能 #3634新需求续期都不锁
        	tLJSPaySchema.setCanSendBank("0");
      }
        tLJSPaySchema.setApproveDate(PubFun.getCurrentDate());
        tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
        outLJSPaySet.add(tLJSPaySchema);
      }else if (StrTool.cTrim(tLYReturnFromBankSchema.getPayType()).equals("S")){
          LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);
          //总应收表无数据，表示扣款失败，并且已经用其它方式交费
          if (tLJSPaySchema == null) continue;
          //扣款成功
          tLJSPaySchema.setBankOnTheWayFlag("3");
          tLJSPaySchema.setApproveDate(PubFun.getCurrentDate());
          tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
          tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
          outLJSPaySet.add(tLJSPaySchema);
      }
    }
  }

  /**
   * 生成银行日志数据
   * @param tLYSendToBankSchema
   * @return
   */
  private void getLYBankLog() throws Exception {
    //获取日志记录
    LYBankLogDB tLYBankLogDB = new LYBankLogDB();
    tLYBankLogDB.setSerialNo(serialNo);
    if (!tLYBankLogDB.getInfo()) {
      throw new Exception("获取银行日志表（LDBankLog）数据失败");
    }
    outLYBankLogSchema = tLYBankLogDB.getSchema();

//    LDBankDB tLDBankDB = new LDBankDB();
//    tLDBankDB.setBankCode(outLYBankLogSchema.getBankCode());
//    if (!tLDBankDB.getInfo()) {
//      throw new Exception("获取银行表（LDBank）数据失败");
//    }

    //银联处理日志方式
    if (!bankUniteFlag.equals("")) {
      LYBankUniteLogDB tLYBankUniteLogDB = new LYBankUniteLogDB();
      tLYBankUniteLogDB.setSerialNo(serialNo);
      tLYBankUniteLogDB.setBankCode(bankCode);
      if (!tLYBankUniteLogDB.getInfo()) {
        throw new Exception("获取银联日志表（LDBankUniteLog）数据失败");
      }

      tLYBankUniteLogDB.setDealState("1");
      tLYBankUniteLogDB.setModifyDate(PubFun.getCurrentDate());
      tLYBankUniteLogDB.setModifyTime(PubFun.getCurrentTime());
      outLYBankUniteLogSchema = tLYBankUniteLogDB.getSchema();

      //修改日志
      outLYBankLogSchema.setAccTotalMoney(outLYBankLogSchema.getAccTotalMoney() + outLYBankUniteLogSchema.getAccTotalMoney()); //财务确认总金额
      outLYBankLogSchema.setBankSuccMoney(outLYBankLogSchema.getBankSuccMoney() + outLYBankUniteLogSchema.getBankSuccMoney()); //银行成功总金额
      outLYBankLogSchema.setBankSuccNum(outLYBankLogSchema.getBankSuccNum() + outLYBankUniteLogSchema.getBankSuccNum()); //银行成功总数量

      //校验所有银联下银行的总金额，是否都已经处理完成，是，则该银联处理完成
      String sql = "select sum(TotalNum), sum(TotalMoney) from LYBankUniteLog where SerialNo='" + serialNo + "' and DealState='1'";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS s = tExeSQL.execSQL(sql);
      double num = 0;
      double money = 0;
      if (s.GetText(1, 2).equals("null")) {
        num = outLYBankUniteLogSchema.getTotalNum();
        money = outLYBankUniteLogSchema.getTotalMoney();
      }
      else {
        num = Double.parseDouble(s.GetText(1, 1)) + outLYBankUniteLogSchema.getTotalNum();
        money = Double.parseDouble(s.GetText(1, 2)) + outLYBankUniteLogSchema.getTotalMoney();
      }

      //如果银联的所有银行都已经回盘，则设置处理状态和回盘文件名
      if (num == outLYBankLogSchema.getTotalNum() && money == outLYBankLogSchema.getTotalMoney()) {
        outLYBankLogSchema.setDealState("1"); //处理状态
        outLYBankLogSchema.setInFile("银联文件，请在银联日志表LYBankUniteLog中查询");
      }
    }
    else {
      //修改日志
      outLYBankLogSchema.setAccTotalMoney(totalMoney); //财务确认总金额
      outLYBankLogSchema.setBankSuccMoney(totalMoney); //银行成功总金额
      outLYBankLogSchema.setBankSuccNum(sumNum + ""); //银行成功总数量
      outLYBankLogSchema.setDealState("1"); //处理状态
    }

    outLYBankLogSchema.setTransOperator(mGlobalInput.Operator);
    outLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    outLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
	  System.out.println("---dealData--处理---");
    try {
      //银行代收
      if (mOperate.equals("GETMONEY")) {
        //获取批次号、总金额、银行编码
        serialNo = (String) inTransferData.getValueByName("serialNo");
        //insbankcode =(String)inTransferData.getValueByName("insbankcode");
		getbankcode =(String)inTransferData.getValueByName("getbankcode");
        getbankaccno =(String)inTransferData.getValueByName("getbankaccno");
        System.out.print("getbankaccno2:"+getbankaccno);
        autobankaccno =(String)inTransferData.getValueByName("autobankaccno");
        System.out.print("autobankaccno:"+autobankaccno);
        totalMoney = (String) inTransferData.getValueByName("totalMoney");
        bankCode = (String) inTransferData.getValueByName("bankCode");
        bankUniteFlag = (String) inTransferData.getValueByName("bankUniteFlag");

        //获取银行返回数据，设置备份表
        ExeSQL tExeSQL = new ExeSQL();
        String bankSql = "select 1 from ldcode where codetype='bankunitecode' and code='" + bankCode + "'";
        SSRS tSSRS = tExeSQL.execSQL(bankSql);
        
        if(tSSRS != null && tSSRS.getMaxRow() > 0) {
        	inLYReturnFromBankSet = getLYReturnFromBank(serialNo, null);
        }
        else{
            inLYReturnFromBankSet = getLYReturnFromBank(serialNo, bankCode);
        }
        if (inLYReturnFromBankSet.size() == 0) throw new NullPointerException("无银行返回数据！");
        outDelLYReturnFromBankSet.set(inLYReturnFromBankSet);
        System.out.println("---End getLYReturnFromBank---");

        //校验银行扣款成功与否
        verifyBankSucc();
        System.out.println("---End verifyBankSucc---");
       
        //校验财务总金额，比较回盘表成功总金额和界面录入的总金额
        if (!confirmTotalMoney(inLYReturnFromBankSet, totalMoney))
          throw new NullPointerException("财务总金额确认失败！请与银行对帐！");
        System.out.println("---End confirmTotalMoney---");

        //add by yingxl 2008-04-18
        for(int i=0;i<outDelLYReturnFromBankSet.size();i++){
            String sql="update ljtempfee set state='1' where tempfeeno='"
                       +outDelLYReturnFromBankSet.get(i+1).getPayCode()+"'";
            boolean abool= new ExeSQL().execUpdateSQL(sql) ;
        }
        //end add
        
        //校验每笔金额，比较LJSPay中的金额
        verifyUnitMoney();
        System.out.println("---End verifyUnitMoney---");

        //校验应收表
        verifyLJSPay();
        System.out.println("---End verifyLJSPay---");

        //备份返回盘数据到返回盘B表
        getLYReturnFromBankB();
        System.out.println("---End getLYReturnFromBankB---");

        //获取发送盘数据，用于删除
        getLYSendToBank();
        System.out.println("---End getLYSendToBank---");

        //获取应收表数据，修改银行在途标志
        getLJSPay();
        System.out.println("---End getLJSPay---");

        //记录日志，设置总金额、总数量、处理状态
        getLYBankLog();
        System.out.println("---End verifyUnitMoney---");
      }
    }
    catch(Exception e) {
      e.printStackTrace();
      // @@错误处理
      CError.buildErr(this, "数据处理错误: " + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    mInputData = new VData();

    try {
      mInputData.add(outLYDupPaySet);
      mInputData.add(outLYBankLogSchema);
      mInputData.add(outDelLYReturnFromBankSet);
      mInputData.add(outDelLJSPaySet);
      mInputData.add(outLJTempFeeSet);
      mInputData.add(outLJSPaySet);
      mInputData.add(outLYReturnFromBankBSet);
      mInputData.add(outDelLYSendToBankSet);
      mInputData.add(outLJTempFeeClassSet);
      mInputData.add(outUpdateLJTempFeeSet);
      mInputData.add(outUpdateLJTempFeeClassSet);

      mInputData.add(outLOPRTManagerSet);
      mInputData.add(outLYBankUniteLogSchema);
      mInputData.add(outLJSPayBSet);
      mInputData.add(outLJSPayPersonBSet);
    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  public String getManageComFromLDFinBank() throws Exception{
      String outString=new String();
      String sqlStr="select * from LDFinBank where bankcode='"+getbankcode+"' and bankaccno='"+getbankaccno+"' and finflag='S' order by managecom  with ur";
      System.out.println("查询ldfinbank中的managecom:"+sqlStr);
      LDFinBankDB tLDFinBankDB=new LDFinBankDB();
      LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(sqlStr);
      
      if(tLDFinBankSet.size()==0)
          throw new Exception("该本方银行帐户在ldfinbank表中没有对应数据！");
      LDFinBankSchema tLDFinBankSchema=tLDFinBankSet.get(1);
      outString=tLDFinBankSchema.getManageCom();
      return outString;
  }
  public String getbankaccnomain(LJTempFeeClassSchema mLJTempFeeClassSchema){
		 String mybankaccno="";
		 String mybankcode=mLJTempFeeClassSchema.getBankCode().substring(1, 2);
		 System.out.println("银行编码:"+mybankcode);
		 String mysql="select bankaccno from ldfinbank where finflag='S' and bankcode='"+mybankcode+"' and managecom='"+mLJTempFeeClassSchema.getManageCom()+"'";
		 System.out.println("本方银行帐号查询sql:"+mysql);
		  LDFinBankDB tLDFinBankDB=new LDFinBankDB();
		  if (mybankcode!=null&&!"".equals(mybankcode)){
	      LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(mysql);
	      LDFinBankSchema tLDFinBankSchemat=tLDFinBankSet.get(1);
	      mybankaccno=tLDFinBankSchemat.getBankAccNo();
		  }
		 return mybankaccno;
	 }
  public static void main(String[] args) {
    GetReturnFromBankBL getReturnFromBankBL1 = new GetReturnFromBankBL();
    TransferData transferData1 = new TransferData();
    transferData1.setNameAndValue("totalMoney", "10000");
    transferData1.setNameAndValue("serialNo", "00000000000000001600");
    transferData1.setNameAndValue("bankCode", "9931");
    transferData1.setNameAndValue("bankUniteFlag", "");
    
    GlobalInput tGlobalInput = new GlobalInput(); 

    VData inVData = new VData();
    inVData.add(transferData1);
    inVData.add(tGlobalInput);

    if (!getReturnFromBankBL1.submitData(inVData, "GETMONEY")) {
    }
  }
}
