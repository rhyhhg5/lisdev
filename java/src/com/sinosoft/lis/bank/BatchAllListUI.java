package com.sinosoft.lis.bank;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:邮储代收代付汇总清单
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author 亓莹莹
 * @version 1.0
 */
public class BatchAllListUI
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private TransferData mDealElement = new TransferData();
    private GlobalInput mGlobalInput = new GlobalInput();

    public BatchAllListUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("---进入UI");
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            if (!getInputData(cInputData))
            {
                return false;
            }
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }
            BatchAllListBL tBatchAllListBl = new BatchAllListBL();

            System.out.println("Start YCAllListUI UI Submit ...");

            if (!tBatchAllListBl.submitData(cInputData, cOperate))
            {
                if (tBatchAllListBl.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tBatchAllListBl.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData", "YCAllListUI发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tBatchAllListBl.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PrintBillYFRightUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try {
            vData.clear();
            vData.add(mDealElement);
            vData.add(mGlobalInput);
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mDealElement = (TransferData) cInputData.getObjectByObjectName( "TransferData", 0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName( "GlobalInput", 0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BatchAllListUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        String tBillNo = "00000000000000000079";
        String tBankCode = "0102";
        tG.ManageCom = "8694";
        VData tVData = new VData();
        tVData.addElement(tBillNo);
        tVData.addElement(tBankCode);
        tVData.addElement(tG);
        
    }
}
