package com.sinosoft.lis.bank;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class GetReturnFromBankBLS {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  //业务数据
  private LYDupPaySet inLYDupPaySet = new LYDupPaySet();
  private LYBankLogSchema inLYBankLogSchema = new LYBankLogSchema();
  private LYReturnFromBankSet inDelLYReturnFromBankSet = new LYReturnFromBankSet();
  private LJSPaySet inDelLJSPaySet = new LJSPaySet();
  private LJTempFeeSet inLJTempFeeSet = new LJTempFeeSet();
  private LJSPaySet inLJSPaySet = new LJSPaySet();
  private LYReturnFromBankBSet inLYReturnFromBankBSet = new LYReturnFromBankBSet();
  private LYSendToBankSet inDelLYSendToBankSet = new LYSendToBankSet();
  private LJTempFeeClassSet inLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeSet inUpdateLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeClassSet inUpdateLJTempFeeClassSet = new LJTempFeeClassSet();

  private LOPRTManagerSet inLOPRTManagerSet = new LOPRTManagerSet();
  private LYBankUniteLogSchema inLYBankUniteLogSchema = new LYBankUniteLogSchema();
  private LJSPayBSet inLJSPayBSet = new LJSPayBSet();
  private LJSPayPersonBSet inLJSPayPersonBSet = new LJSPayPersonBSet();

  public GetReturnFromBankBLS() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    boolean tReturn = false;

    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate =cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    //System.out.println("---End getInputData---");

    //信息保存
    if(this.mOperate.equals("GETMONEY")) {
      tReturn = save(cInputData);
    }

    if (tReturn)
      System.out.println("Save sucessful");
    else
      System.out.println("Save failed");

    return tReturn;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      inLYDupPaySet = (LYDupPaySet)mInputData.getObjectByObjectName("LYDupPaySet", 0);
      inLYBankLogSchema = (LYBankLogSchema)mInputData.getObjectByObjectName("LYBankLogSchema", 1);
      inDelLYReturnFromBankSet = (LYReturnFromBankSet)mInputData.getObjectByObjectName("LYReturnFromBankSet", 2);
      inDelLJSPaySet = (LJSPaySet)mInputData.getObjectByObjectName("LJSPaySet", 3);
      inLJTempFeeSet = (LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet", 4);
      inLJSPaySet = (LJSPaySet)mInputData.getObjectByObjectName("LJSPaySet", 5);
      inLYReturnFromBankBSet = (LYReturnFromBankBSet)mInputData.getObjectByObjectName("LYReturnFromBankBSet", 6);
      inDelLYSendToBankSet = (LYSendToBankSet)mInputData.getObjectByObjectName("LYSendToBankSet", 7);
      inLJTempFeeClassSet = (LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet", 8);
      inUpdateLJTempFeeSet = (LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet", 9);
      inUpdateLJTempFeeClassSet = (LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet", 10);
      inLOPRTManagerSet = (LOPRTManagerSet)mInputData.getObjectByObjectName("LOPRTManagerSet", 11);
      inLYBankUniteLogSchema = (LYBankUniteLogSchema)mInputData.getObjectByObjectName("LYBankUniteLogSchema", 12);
      inLJSPayBSet = (LJSPayBSet) mInputData.getObjectByObjectName("LJSPayBSet", 0);
      inLJSPayPersonBSet = (LJSPayPersonBSet) mInputData.getObjectByObjectName("LJSPayPersonBSet", 0);

//      System.out.println("inLYDupPaySet:" + inLYDupPaySet.encode());
//      System.out.println("inLYBankLogSet:" + inLYBankLogSet.encode());
//      System.out.println("inLYReturnFromBankSet:" + inDelLYReturnFromBankSet.encode());
//      System.out.println("inDelLJSPaySet:" + inDelLJSPaySet.encode());
//      System.out.println("inLJTempFeeSet:" + inLJTempFeeSet.encode());
//      System.out.println("inLJSPaySet:" + inLJSPaySet.encode());
//      System.out.println("inLYReturnFromBankBSet:" + inLYReturnFromBankBSet.encode());
//      System.out.println("inDelLYSendToBankSet:" + inDelLYSendToBankSet.encode());
//      System.out.println("inLJTempFeeClassSet:" + inLJTempFeeClassSet.encode());
//      System.out.println("inUpdateLJTempFeeSet:" + inUpdateLJTempFeeSet.encode());
//      System.out.println("inUpdateLJTempFeeClassSet:" + inUpdateLJTempFeeClassSet.encode());
    }
    catch (Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GetReturnFromBankBLS";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  //保存操作
  private boolean save(VData mInputData) {
    int i;
    boolean tReturn =true;
    System.out.println("Start Save...");

    //建立数据库连接
    Connection conn=DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GetReturnFromBankBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try {
      //开始事务，锁表
      conn.setAutoCommit(false);

      //生成重复交费数据，插入重复交费记录表
      LYDupPayDBSet tLYDupPayDBSet = new LYDupPayDBSet(conn);
      tLYDupPayDBSet.set(inLYDupPaySet);
      if (!tLYDupPayDBSet.insert()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYDupPay Insert Failed");
        return false;
      }
      System.out.println("End 生成重复交费数据 ...");

      //记录银行业务日志，插入新日志数据
      LYBankLogSet tLYBankLogSet = new LYBankLogSet();
      tLYBankLogSet.add(inLYBankLogSchema);
      LYBankLogDBSet tLYBankLogDBSet = new LYBankLogDBSet(conn);
      tLYBankLogDBSet.set(tLYBankLogSet);
      if (!tLYBankLogDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYBankLog Update Failed");
        return false;
      }
      System.out.println("End 记录银行业务日志 ...");

      //记录银行业务日志，插入新日志数据
      LYBankUniteLogSet tLYBankUniteLogSet = new LYBankUniteLogSet();
      tLYBankUniteLogSet.add(inLYBankUniteLogSchema);
      LYBankUniteLogDBSet tLYBankUniteLogDBSet = new LYBankUniteLogDBSet(conn);
      tLYBankUniteLogDBSet.set(tLYBankUniteLogSet);
      if (!tLYBankUniteLogDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYBankUniteLog Update Failed");
        return false;
      }
      System.out.println("End 记录银行业务日志 ...");


      //删除应收表数据（首期事后交费）
      LJSPayDBSet tDelLJSPayDBSet = new LJSPayDBSet(conn);
      tDelLJSPayDBSet.set(inDelLJSPaySet);
      if (!tDelLJSPayDBSet.delete()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJSPay Delete Failed");
        return false;
      }
      System.out.println("End 删除应收表数据（首期事后交费） ...");

      //新增暂交费数据
      LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet(conn);
      tLJTempFeeDBSet.set(inLJTempFeeSet);
      if (!tLJTempFeeDBSet.insert()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJTempFee Insert Failed");
        return false;
      }
      System.out.println("End 新增暂交费数据 ...");

      //新增暂交费分类表数据
      LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet(conn);
      tLJTempFeeClassDBSet.set(inLJTempFeeClassSet);
      if (!tLJTempFeeClassDBSet.insert()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJTempFeeClass Insert Failed");
        return false;
      }
      System.out.println("End 新增暂交费分类表数据 ...");

      //修改暂交费数据
      LJTempFeeDBSet tUpdateLJTempFeeDBSet = new LJTempFeeDBSet(conn);
      tUpdateLJTempFeeDBSet.set(inUpdateLJTempFeeSet);
      if (!tUpdateLJTempFeeDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJTempFee Update Failed");
        return false;
      }
      System.out.println("End 修改暂交费数据 ...");

      //修改暂交费分类表数据
      LJTempFeeClassDBSet tUpdateLJTempFeeClassDBSet = new LJTempFeeClassDBSet(conn);
      tUpdateLJTempFeeClassDBSet.set(inUpdateLJTempFeeClassSet);
      if (!tUpdateLJTempFeeClassDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJTempFeeClass Update Failed");
        return false;
      }
      System.out.println("End 修改暂交费分类表数据 ...");

      //修改总应收表的银行在途标志
      LJSPayDBSet tLJSPayDBSet = new LJSPayDBSet(conn);
      tLJSPayDBSet.set(inLJSPaySet);
      if (!tLJSPayDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJSPay Update Failed");
        return false;
      }
      System.out.println("End 修改总应收表的银行在途标志 ...");

      //备份返回盘数据
      LYReturnFromBankBDBSet tLYReturnFromBankBDBSet = new LYReturnFromBankBDBSet(conn);
      tLYReturnFromBankBDBSet.set(inLYReturnFromBankBSet);
      if (!tLYReturnFromBankBDBSet.insert()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYReturnFromBankB Insert Failed");
        return false;
      }
      System.out.println("End 备份返回盘数据 ...");

      //删除返回盘数据
      LYReturnFromBankDBSet tLYReturnFromBankDBSet = new LYReturnFromBankDBSet(conn);
      tLYReturnFromBankDBSet.set(inDelLYReturnFromBankSet);
      if (!tLYReturnFromBankDBSet.delete()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYReturnFromBank Delete Failed");
        return false;
      }
      System.out.println("End 删除返回盘数据 ...");

      //删除发送盘数据
      LYSendToBankDBSet tLYSendToBankDBSet = new LYSendToBankDBSet(conn);
      tLYSendToBankDBSet.set(inDelLYSendToBankSet);
      if (!tLYSendToBankDBSet.delete()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYSendToBank Insert Failed");
        return false;
      }
      System.out.println("End 删除发送盘数据 ...");

      //插入打印管理表数据
      LOPRTManagerDBSet tLOPRTManagerDBSet = new LOPRTManagerDBSet(conn);
      tLOPRTManagerDBSet.set(inLOPRTManagerSet);
      if (!tLOPRTManagerDBSet.insert()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LOPRTManager Insert Failed");
        return false;
      }
      System.out.println("End 插入打印管理表数据 ...");

      //插入打印管理表数据
      LJSPayBDBSet tLJSPayBDBSet = new LJSPayBDBSet(conn);
      tLJSPayBDBSet.set(inLJSPayBSet);
      if (!tLJSPayBDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("tLJSPayB update Failed");
        return false;
      }

      //插入打印管理表数据
      LJSPayPersonBDBSet tLJSPayPersonBDBSet = new LJSPayPersonBDBSet(conn);
      tLJSPayPersonBDBSet.set(inLJSPayPersonBSet);
      if (!tLJSPayPersonBDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("tLJSPayPersonB update Failed");
        return false;
      }

      conn.commit();
      conn.close();
      System.out.println("End Committed");
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GetReturnFromBankBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try{ conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }

  public static void main(String[] args) {
    GetReturnFromBankBLS getReturnFromBankBLS1 = new GetReturnFromBankBLS();
  }
}
