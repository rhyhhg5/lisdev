package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDBankUniteSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * @author 张成轩
 * @version 1.0
 */
public class BatchSendBaoRongXml
{
    private String mUrl = "";

    private String feeType = "";

    private String ubank = "";
    
    private String filePath = "";
    private String bankaccno = "";

    private MMap map = new MMap();

    private VData mInputData = new VData();

    /**
     * @param mUrl 报文存放路径
     * @param feeType 收付费标识
     * @param ubank 银行编码
     */
    public boolean submitData(String mUrl, String feeType, String ubank)
    {
        this.mUrl = mUrl + "/";
        this.feeType = feeType;
        this.ubank = ubank;

        if (!dealDate())
        {
            System.out.println("获取数据失败");
            return false;
        }

        if (!prepareOutputData())
        {
            System.out.println("准备更新日志数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE"))
        {
            //错误处理
            System.out.println("更新发盘日志失败");
            return false;
        }

        return true;
    }

    public boolean dealDate()
    {
        String sql = "select * from lybanklog a where a.dealstate is null and a.senddate is null and a.bankcode in ("
                + ubank
                + ") and logtype='"
                + feeType
                + "' "
                + "and (select count(*) from lysendtobank where serialno=a.serialno)>0 with ur";
        System.out.println(sql);
        LYBankLogDB tLyBankLogDB = new LYBankLogDB();
        LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
        if (tLYBankLogSet == null)
        {
            System.out.println("未查到发送银行数据");
            return false;
        }
        for (int i = 1; i <= tLYBankLogSet.size(); i++)
        {
            LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
            String serialno = tLYBankLogSchema.getSerialNo();
            sql = "select * from lysendtobank where serialno='" + serialno
                    + "' with ur";
            System.out.println(sql);
            LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
            LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB
                    .executeQuery(sql);
            
            // 判断批次下是否有异常的付费数据（并发数据）
            if("F".equals(feeType) && !checkLJFIGet(serialno, tLYBankLogSchema)){
            	// 若有问题，停止该批次的发送，并发送邮件
            	continue;
            }
            // 判断批次下是否有异常的收费数据（并发数据）
            if("S".equals(feeType) && !checkLysendtobank(serialno, tLYBankLogSchema)){
            	// 若有问题，停止该批次的发送，并发送邮件
            	continue;
            }
            if (!createXml(tLYBankLogSchema, tLYSendToBankSet))
            {
            	continue;
            }
        }
        return true;
    }

    public boolean createXml(LYBankLogSchema tLYBankLogSchema,
            LYSendToBankSet tLYSendToBankSet)
    {
        String bankUniteCode = tLYBankLogSchema.getBankCode();
        String type = tLYBankLogSchema.getLogType();
        //因根据大行分批次，无论是大行下的哪个小行，归集账号都是一样的
    	LYSendToBankSchema tLYSendToBankSchema1 = tLYSendToBankSet.get(1);
    	//查大行
        String bank = uniteBankCode(tLYSendToBankSchema1.getBankCode(), bankUniteCode);
        Element root = null;
        if (bankUniteCode.equals("7701") )
        {
            root = new Element("Root");   
        }
        Document doc = new Document(root);

        Element ele_INFO = new Element("Head");
        //指令类型
        if (type.equals("S"))
        {
            setText(ele_INFO, "CommandCode", "11210");//同行代收
        }
        if(type.equals("F"))
        {
        	if(bank.equals("308"))
        	{
        		setText(ele_INFO, "CommandCode", "10220");//综合代付
        	}
        	else
        	{
        		setText(ele_INFO, "CommandCode", "11220");//同行代付
        	}
        }
       
        setText(ele_INFO, "EnterpriseNum", "");//企业编号
        
        if (type.equals("S"))
        {
        	setText(ele_INFO, "CorpBankCode", bank); //发起方银行渠道代码
        }
        if(type.equals("F"))
        {
	        if(bank.equals("308"))
	        {
	        	setText(ele_INFO, "CorpBankCode", "102"); //发起方银行渠道代码
	        }
	        else
	        {
	        	setText(ele_INFO, "CorpBankCode", bank); //发起方银行渠道代码
	        }
        }
        //根据大行查询归集账号
        
    	
    	String sqlbankname="select bankname from ldfinbank  where finbankcode='"+bank+"' with ur ";
    	String bankname = new ExeSQL().getOneValue(sqlbankname);
    	System.out.println(bankname);
    	
        setText(ele_INFO, "TransSeqID", PubFun.getCurrentDate2()
                + PubFun.getCurrentTime2());//时间戳
        setText(ele_INFO, "VerifyCode", "");//验签码
        setText(ele_INFO, "ZipType", "");//压缩方式
        setText(ele_INFO, "RespCode", "");//响应编码
        setText(ele_INFO, "RespInfo", "");//响应信息
       
        
        root.addContent(ele_INFO);

        Element ele_TransReq = new Element("TransReq");
        

        setText(ele_TransReq, "TotalNum", Long.toString(tLYBankLogSchema.getTotalNum()));//总笔数 Y
        setText(ele_TransReq, "TotalAmt",(new DecimalFormat("0.00")).format(Double.valueOf((tLYBankLogSchema.getTotalMoney()))));//总金额 Y
        //setText(ele_TransReq, "TotalAmt",Long.toString((long) tLYBankLogSchema.getTotalMoney()));//总金额 Y
        setText(ele_TransReq, "ReqSeqID", tLYBankLogSchema.getSerialNo());//批次号 Y
        setText(ele_TransReq, "AccountingFlag", "");//记账标记
        
        Element ele_CorpToBank = new Element("CorpToBank");
        setText(ele_CorpToBank, "EncryptFlag", "");//是否对文件关键字段加密
        setText(ele_CorpToBank, "SourceDataMD5", "");//如使用文件内容加密，加密前原始数据需签名后发送给银行进行确认
        setText(ele_CorpToBank, "SourceTotalAmount", "");//当EncryptFlag为加密时需传该节点原始总金额，不加密时可传空
        ele_TransReq.addContent(ele_CorpToBank);
        Element ele_TransParam = new Element("TransParam");
        
        if(type.equals("S"))
        {   
        	
        	//查子银行名称
        	String uniteBankName=uniteBankName(tLYSendToBankSchema1.getBankCode(), bankUniteCode);
        	String sqlbankaccno="select bankaccno from ldfinbank  where finbankcode='"+bank+"' and finflag='S' with ur ";
        	String bankaccno = new ExeSQL().getOneValue(sqlbankaccno);
        	setText(ele_TransParam, "RecAct", bankaccno);//企业方账户   Y
            setText(ele_TransParam, "RecArea", "");//企业方区域代码
            setText(ele_TransParam, "RecAreaName", "");//企业方区域名称
            setText(ele_TransParam, "CorpBankCode", bank); //企业方银行代码 Y
        	
        	
         for (int i = 1; i <= tLYSendToBankSet.size(); i++)
         {
        	 
        	LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(i);
        	
            Element ele_DetailRecord = new Element("DetailRecord");
            setText(ele_DetailRecord, "PayName", tLYSendToBankSchema.getAccName());//客户方户名
            setText(ele_DetailRecord, "PayAct", tLYSendToBankSchema.getAccNo());//客户方账号  Y
            setText(ele_DetailRecord, "PayBnk", uniteBankName);//客户方银行开户行名
            String area ="select codealias  from  ldcode where codetype='baoRong'  "
            		+ "	and code='"+tLYSendToBankSchema.getComCode()+"' with ur ";
            String PayArea = new ExeSQL().getOneValue(area);
            setText(ele_DetailRecord, "PayArea", PayArea);//客户方区域代码 Y
            
            setText(ele_DetailRecord, "PayAreaName", "");//客户方区域名称
            setText(ele_DetailRecord, "PayBankCode", bank);//客户方银行代码  Y
            setText(ele_DetailRecord, "PayBankName", bankname);//客户方银行名称
            
            setText(ele_DetailRecord, "RecName", "");//企业方户名 
            
            setText(ele_DetailRecord, "RecAct", bankaccno);//企业方账户 Y
            setText(ele_DetailRecord, "RecBnk", "");//企业方银行开户行名
            
            setText(ele_DetailRecord, "RecBankCode", bank);//企业方银行代码 Y
            setText(ele_DetailRecord, "RecBankName", bankname);//企业方银行名称
            setText(ele_DetailRecord, "RecArea", "");//企业方区域代码 
            setText(ele_DetailRecord, "RecAreaName", "");//企业方区域名称

            setText(ele_DetailRecord, "CardType", "0");//卡折标记 Y 0 卡  1 折
            setText(ele_DetailRecord, "SameCity", "");//是否同城
            setText(ele_DetailRecord, "SameBnk", "");//是否同行 
            setText(ele_DetailRecord, "PayDate",  PubFun.getCurrentDate());//交易日期
            setText(ele_DetailRecord, "PayTime",  PubFun.getCurrentTime());//交易时间
            setText(ele_DetailRecord, "PayAmount", (new DecimalFormat("0.00")).format(Double.valueOf(tLYSendToBankSchema.getPayMoney())));//交易金额   Y
            setText(ele_DetailRecord, "PayCur", "CNY");//客户方币种 Y  CNY 人民币  HKD 港币 USD 美元
            setText(ele_DetailRecord, "RecCur", "CNY");//企业方币种
            setText(ele_DetailRecord, "CertType", tLYSendToBankSchema.getIDType());//证件类型
            setText(ele_DetailRecord, "CertNum", tLYSendToBankSchema.getIDNo());//证件号
            setText(ele_DetailRecord, "CreditCardSecCode", "");//信用卡验证码
            setText(ele_DetailRecord, "CreditCardValidity", "");//信用卡有效期
            setText(ele_DetailRecord, "Usage", "");//用途
            setText(ele_DetailRecord, "PostScript", tLYSendToBankSchema.getPayCode());//流水号，批次内唯一  Y
            setText(ele_DetailRecord, "Memo", "");//备注
            setText(ele_DetailRecord, "ReqReserve", "");//对账码
            setText(ele_DetailRecord, "CNAPSCode", "");//联行号
            setText(ele_DetailRecord, "CNAPSName", "");//开户行名称
            setText(ele_DetailRecord, "IsPrivate", "");//公私标记 
            setText(ele_DetailRecord, "SourceNote", tLYSendToBankSchema.getPayCode());//原始单据号
            setText(ele_DetailRecord, "ORGCode", "");//机构号
            setText(ele_DetailRecord, "Extend1", "");//扩展字段1
            setText(ele_DetailRecord, "Extend2", "");//扩展字段2
            ele_TransParam.addContent(ele_DetailRecord);
        
            
         }
        }else{
        	
        	//查子银行名称
        	String uniteBankName=uniteBankName(tLYSendToBankSchema1.getBankCode(), bankUniteCode);
        	//根据大行查归集账号
        	if(bank.equals("308"))
        	{
        		String sqlbankaccno="select bankaccno from ldfinbank  where finbankcode='102' and finflag='F' with ur";
        		bankaccno = new ExeSQL().getOneValue(sqlbankaccno);
        	}
        	else
        	{
            	String sqlbankaccno="select bankaccno from ldfinbank  where finbankcode='"+bank+"' and finflag='F' with ur";
            	bankaccno = new ExeSQL().getOneValue(sqlbankaccno);
        	}
        	setText(ele_TransParam, "PayAct", bankaccno);//企业方账户   Y
	        setText(ele_TransParam, "PayArea", "");//企业方区域代码
	        
	        setText(ele_TransParam, "PayAreaName", "");//企业方区域名称
	        if(bank.equals("308"))
	        {
	        	setText(ele_TransParam, "CorpBankCode", "102"); //企业方银行代码 Y
	        }
	        else
	        {
	        	setText(ele_TransParam, "CorpBankCode", bank); //企业方银行代码 Y
	        }
	     for (int i = 1; i <= tLYSendToBankSet.size(); i++)
	     {
	    	LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(i);
	    	
	        Element ele_DetailRecord = new Element("DetailRecord");
	        setText(ele_DetailRecord, "PayName", "");//企业方户名
	        setText(ele_DetailRecord, "PayAct", bankaccno);//企业方账号  Y
	        if(bank.equals("308"))
	        {
	        	setText(ele_DetailRecord, "PayBankCode", "102");//企业方银行代码Y
	        }
	        else
	        {
	        	setText(ele_DetailRecord, "PayBankCode", bank);//企业方银行代码Y
	        }
	        setText(ele_DetailRecord, "PayBankName", bankname);//企业方银行名称
	        setText(ele_DetailRecord, "PayBnk", "");//企业方银行开户行名
	        setText(ele_DetailRecord, "PayArea", "");//企业方区域代码
	        setText(ele_DetailRecord, "PayAreaName", "");//企业方区域名称
	        setText(ele_DetailRecord, "RecName", tLYSendToBankSchema.getAccName());//客户方户名Y
	        setText(ele_DetailRecord, "RecAct", tLYSendToBankSchema.getAccNo());//客户方账户Y
	        setText(ele_DetailRecord, "RecBnk", uniteBankName);//客户方银行开户行名
	        setText(ele_DetailRecord, "RecBankCode", bank);//客户方银行代码 Y
	        setText(ele_DetailRecord, "RecBankName", bankname);//客户方银行名称
	        String area ="select codealias  from  ldcode where codetype='baoRong'  "
            		+ "	and code='"+tLYSendToBankSchema.getComCode()+"' with ur ";
            String PayArea = new ExeSQL().getOneValue(area);
	        setText(ele_DetailRecord, "RecArea", PayArea);//客户方区域代码  Y
	        
	        setText(ele_DetailRecord, "RecAreaName", "");//客户方区域名称
	
	        setText(ele_DetailRecord, "CardType", "0");//卡折标记 Y 0 卡  1 折 
	        setText(ele_DetailRecord, "SameCity", "");//是否同城
	        setText(ele_DetailRecord, "SameBnk", "");//是否同行 
	        setText(ele_DetailRecord, "PayDate", PubFun.getCurrentDate());//交易日期
	        setText(ele_DetailRecord, "PayTime", PubFun.getCurrentTime());//交易时间
	        setText(ele_DetailRecord, "PayAmount", (new DecimalFormat("0.00")).format(Double.valueOf(tLYSendToBankSchema.getPayMoney())));//交易金额  
	        setText(ele_DetailRecord, "PayCur", "CNY");//企业方币种 Y  CNY 人民币  HKD 港币 USD 美元
	        setText(ele_DetailRecord, "RecCur", "CNY");//客户方币种
	        setText(ele_DetailRecord, "CertType", tLYSendToBankSchema.getIDType());//证件类型
	        setText(ele_DetailRecord, "CertNum", tLYSendToBankSchema.getIDNo());//证件号
	        setText(ele_DetailRecord, "CreditCardSecCode", "");//信用卡验证码
	        setText(ele_DetailRecord, "CreditCardValidity", "");//信用卡有效期
	        setText(ele_DetailRecord, "Usage", "");//用途
	        setText(ele_DetailRecord, "PostScript", tLYSendToBankSchema.getPayCode());//流水号，批次内唯一  Y
	        setText(ele_DetailRecord, "Memo", "人保健康赔付");//备注
	        setText(ele_DetailRecord, "ReqReserve", "");//对账码
	        setText(ele_DetailRecord, "CNAPSCode", "");//联行号
	        setText(ele_DetailRecord, "CNAPSName", "");//开户行名称
	        setText(ele_DetailRecord, "IsPrivate", "");//公私标记 
	        setText(ele_DetailRecord, "SourceNote", tLYSendToBankSchema.getPayCode());//原始单据号
	        setText(ele_DetailRecord, "ORGCode", "");//机构号
	        setText(ele_DetailRecord, "Extend1", "");//扩展字段1
	        setText(ele_DetailRecord, "Extend2", "");//扩展字段2
	        ele_TransParam.addContent(ele_DetailRecord);
	      }
	     
  }
        ele_TransReq.addContent(ele_TransParam);
    	root.addContent(ele_TransReq);


        XMLOutputter XMLOut = new XMLOutputter();
        try
        {
        	this.filePath = this.mUrl + tLYBankLogSchema.getSerialNo() + ".xml";
        	
            XMLOut.setEncoding("GBK");
            XMLOut.output(doc, new FileOutputStream(this.mUrl
                    + tLYBankLogSchema.getSerialNo() + ".xml"));
            String sendStr = readtxt(this.mUrl + tLYBankLogSchema.getSerialNo()
                    + ".xml");
            int length=0;
    		try {
    			length = sendStr.getBytes("GBK").length;
    		} catch (UnsupportedEncodingException e1) {
    			e1.printStackTrace();
    		}
    		String lengthStr = String.valueOf(length);
    		int s = 8 - lengthStr.length();
    		for (int i = 0; i < s ; i++) {
    			lengthStr = " "+lengthStr;
    		}
    		sendStr = "Content-Length:"+lengthStr+"\r"+"\n"+sendStr;
            System.out.println("发送报文内容" + sendStr);

            if (!sendStr.equals(""))
            {
                if (bankUniteCode.equals("7701"))
                {
                    
                    String payReq = SendBank(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        saveReq(payReq, tLYBankLogSchema);
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema);
                    }
                }
				
                
            }

        }
        catch (Exception ex)
        {
            System.out.println("生成xml失败");
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    public String SendBank(String xml) throws IOException
    {
    	String info = "";
		String backInfo="";
		Socket socket = null;
		OutputStreamWriter os = null;
		java.io.InputStream is=null;
		byte[] bytes = new byte[2048];
		try
		{
			// 1.创建客户端Socket，指定服务器地址和端口
			
			String sqlurl="select codename from ldcode1 where codetype='BRBatchSendBank' and code='BatchServerUrl' with ur ";
			String url=new ExeSQL().getOneValue(sqlurl);
			String sqlPort="select codename from ldcode1 where codetype='BRBatchSendBank' and code='BatchServerPort' with ur ";
			int port = Integer.parseInt(new ExeSQL().getOneValue(sqlPort).trim());
			System.out.println(url);
			System.out.println(port);
			socket = new Socket(url, port);
			socket.setSoTimeout(10000);
			// 2.获取输出流，向服务器端发送信息
			os = new OutputStreamWriter(socket.getOutputStream(),"GBK");// 字节输出流
			
//			socket.shutdownOutput();// 关闭输出流
			// 3.获取输入流，并读取服务器端的响应信息
			is = socket.getInputStream();	
			os.write(xml);
			os.flush();

			int tmpOffSet = 0;
			int contentLength=-1;
			int contentStartIndex=-1;
            byte tmpByteLt = "<".getBytes("GBK")[0];
			while(true){
				
				int tmpReadLen = is.read(bytes, tmpOffSet, bytes.length - tmpOffSet);
				if(tmpReadLen<=0){
					break;
				}
				if(contentLength == -1){
					for(int i=tmpOffSet;i<tmpReadLen;i++){
						if(bytes[i]== tmpByteLt){
							if(contentStartIndex<0){
								contentStartIndex = i;
							}
							String tmpHeadStr = new String(bytes,0,i,"GBK");
							String[] tmpKeyValues = tmpHeadStr.split("\n");
							for(int j=0;j<tmpKeyValues.length;j++){
								String[] tmpEach = tmpKeyValues[j].split(":");
								if(tmpEach[0].toLowerCase().equals("content-length")){
									contentLength = Integer.valueOf(tmpEach[1].trim());
								}
							}
							break;
						}
					}
				}
				tmpOffSet += tmpReadLen;
				
				if (contentLength > 0 && contentStartIndex > 0)
                {
                    if (bytes.length < contentLength + contentStartIndex)
                    {
                        byte[] tmpUnionInfo = new byte[contentLength + contentStartIndex];//一次性分配内存
        				System.arraycopy(bytes, 0, tmpUnionInfo, 0, tmpOffSet);
        				bytes = null;
        				bytes = tmpUnionInfo;
                    }
                    
                    int tmpToReadLen = contentLength + contentStartIndex - tmpOffSet;
                    if (tmpToReadLen <= 0)
                    {
                    	break;
                    }
                }
				
				
			}
			if((backInfo=new String(bytes,"GBK").trim()) != null)
				{
					System.out.println("我是客户端，服务器说：" + backInfo);
					
				}
		
	}catch(Exception e) {
		e.printStackTrace();
	} finally {
        if (os != null) {  
            try {  
            	os.close();  
            } catch (Exception e) {  
            }  
        }//关闭Socket输出流  
        if (is != null) {  
        	try {  
        		is.close();  
        	} catch (Exception e) {  
        	}  
        }//关闭Socket输入流  
        if (socket != null) {  
        	try {  
        		socket.close();  
        	} catch (IOException e) {  
        	}  
        }  
    } 
		info=backInfo.substring(23, backInfo.length()).trim();
		return info;
	}
    
    /**
     * 添加子节点
     * @param element 
     * @param name 节点名称
     * @param value 节点值
     */
    private void setText(Element element, String name, String value)
    {

        Element ele = new Element(name);
        ele.addContent(value);
        element.addContent(ele);
    }

    private String uniteBankCode(String bankcode, String bankunitecode)
    {
        String sql = "select * from ldbankunite where bankcode='" + bankcode
                + "' and bankunitecode='" + bankunitecode + "' with ur";
        System.out.println(sql);
        LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
        LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.executeQuery(sql);
        LDBankUniteSchema tLDBankUniteSchema = tLDBankUniteSet.get(1);
        return tLDBankUniteSchema.getUniteBankCode();
    }
    private String uniteBankName(String bankcode, String bankunitecode)
    {
    	String sql = "select * from ldbankunite where bankcode='" + bankcode
    			+ "' and bankunitecode='" + bankunitecode + "' with ur";
    	System.out.println(sql);
    	LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
    	LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.executeQuery(sql);
    	LDBankUniteSchema tLDBankUniteSchema = tLDBankUniteSet.get(1);
    	return tLDBankUniteSchema.getUniteBankName();
    }
    

  
    /**
     * 获取报文省市节点值
     * @param bank 银行代码
     * @param comCode 机构
     * @param othertype 1为省 2为市
     * @return String
     */
    private String otherInfo(String bank, String comCode, int othertype)
    {
        if (othertype == 1)
        {
            if (bank.equals("04105840"))
            {
                return "广东";
            }
            else
            {
                if (comCode.length() >= 4)
                {
                    String sql = "select codename from ldcode where codetype='ComProvince' and code='"
                            + comCode.substring(0, 4) + "'";
                    String com = new ExeSQL().getOneValue(sql);
                    if (com != null)
                    {
                        return com;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "北京";
                }
            }
        }
        else if (othertype == 2 && bank.equals("04105840"))
        {
            return "深圳";
        }
        return "";
    }

    /**
     * 获取身份证节点值
     * @param bank 银行代码
     * @param type 收付费类型
     * @param flag 1为证件类型 2为证件号
     * @return String
     */
    private String getID(LYSendToBankSchema tLYSendToBankSchema, String bank,
            String type, int flag)
    {
        if (type.equals("S")
                && (bank.equals("307") || bank.equals("302")
                        || bank.equals("301") || bank.equals("303")
                        || bank.equals("305") || bank.equals("04105840")))
        {
            if (flag == 1)
            {
                return "0";
            }
            else
            {
                return tLYSendToBankSchema.getIDNo();
            }
        }
        return "";
    }

    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    /**
     * 存储、读取返回报文
     * @param payReq 返回报分字符串
     */
    private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema)
    {
        try
        {
            FileWriter fw = new FileWriter(this.mUrl
                    + tLYBankLogSchema.getSerialNo() + "rq.xml");
            fw.write(payReq);
            fw.close();
            updateLYBankLog(payReq, tLYBankLogSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 连接失败更新日志
     */
    private void errBankLog(LYBankLogSchema tLYBankLogSchema)
    {
        System.out.println("获取响应信息失败");
        tLYBankLogSchema.setOutFile("获取响应信息失败");
        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        this.map.put(tLYBankLogSchema, "UPDATE");
    }

    /**
     * 读取返回报文,更新日志表
     * @param req 返回报分字符串
     */
    private void updateLYBankLog(String req, LYBankLogSchema tLYBankLogSchema)
    {
        int iStart = req.indexOf("<RespCode>");
        if (iStart != -1)
        {
            int end = req.indexOf("</RespCode>");
            String signedMsg = req.substring(iStart + 10, end);
            int signedMsg1=Integer.parseInt(signedMsg);
            System.out.println("响应标记：" + signedMsg1);
            if (signedMsg1>=0)
            {
                tLYBankLogSchema.setOutFile("成功发送#" + this.filePath);
                tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());
                tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                System.out.println("成送发送");
            }
            else
            {
                int s_MSG = req.indexOf("<RespInfo>");
                if (s_MSG != -1)
                {
                    int e_MSG = req.indexOf("</RespInfo>");
                    String signedMSG = req.substring(s_MSG + 10, e_MSG);
                    System.out.println("响应标记：" + signedMSG);
                    if (signedMSG.length() > 50)
                    {
                        tLYBankLogSchema.setOutFile("发送失败,"
                                + signedMSG.substring(0, 40));
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                    else
                    {
                        tLYBankLogSchema.setOutFile(signedMSG);
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                }
                else
                {
                    tLYBankLogSchema.setOutFile("发送失败");
                    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    System.out.println("--发送失败--s_MSG=-1---");
                }
            }
        }
        this.map.put(tLYBankLogSchema, "UPDATE");
        return;
    }

    /**
     * 将发送报文转换为字符串
     * @param path 生成报文路径
     */
    private String readtxt(String path)
    {
        String str = "";
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(path));

            String r = br.readLine();
            while (r != null)
            {
                str += r;
                r = br.readLine();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
        //str = str.replaceAll("<SIGNED_MSG />", "<SIGNED_MSG></SIGNED_MSG>");
        return str;
    }
    
    /**
     * 校验批次下是否有异常的付费数据，对于异常数据会发送邮件
     * 
     * @return
     */
    private boolean checkLJFIGet(String serialno, LYBankLogSchema tLYBankLogSchema){
    	// 校验是否有异常付费数据
    	String sql = "select paycode,polno from lysendtobank where serialno='" 
    			+ serialno 
    			+ "' and exists (select 1 from ljfiget where actugetno=lysendtobank.paycode)"
		        + "  union all select paycode,polno from lysendtobank ly where serialno='"
		        + serialno 
		        + "' and exists (select 1 from lysendtobank  where paycode=ly.paycode and serialno <>ly.serialno )";
		        
    	ExeSQL tEx = new ExeSQL();
    	SSRS tSSRS = tEx.execSQL(sql);
    	if(tSSRS.getMaxRow() == 0){
    		return true;
    	} else if(!"1".equals(tLYBankLogSchema.getOthFlag())){
    		// 批次没有发送过邮件
    		// 发送邮件
//    		MailSender tMailSender = new MailSender("zhangchengxuan", "zhangcx3");
    		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
					.GetText(1, 2), "picchealth");
    		// 设置邮件发送信息
    		String sendInf = "集中代收付" + serialno + "批次出现异常付费数据，已停止该批次的发送，请核查批次内信息。";
    		for(int row = 1; row <= tSSRS.getMaxRow(); row++){
    			sendInf = sendInf + "付费号：" + tSSRS.GetText(row, 1) + "，业务号:" + tSSRS.GetText(row, 2) + "。";
    		}
    		tMailSender.setSendInf("集中代收付异常批次", sendInf, null);
    		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
    		tMailSender.setToAddress("7701");
    		if (!tMailSender.sendMail()) {
    			tMailSender.getErrorMessage();
    		}
    		// 更新日志表输出信息
    		tLYBankLogSchema.setOutFile("批次下存在异常付费数据，发送失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
            // 已发送邮件标记
            tLYBankLogSchema.setOthFlag("1");
            this.map.put(tLYBankLogSchema, "UPDATE");
    		return false;
    	} else {
    		return false;
    	}
    }
    
    /**
     * 校验批次下是否有异常的付费数据，对于异常数据会发送邮件
     * 
     * @return
     */
    private boolean checkLysendtobank(String serialno, LYBankLogSchema tLYBankLogSchema){
    	// 校验是否有异常付费数据
    	String sql = "select paycode,polno from lysendtobank sb where serialno='" 
    			+ serialno 
    			+ "' and  exists (select 1 from lysendtobank  where paycode=sb.paycode and serialno <>sb.serialno )"
    			+ " union all select paycode,polno from lysendtobank where serialno='"
    			+ serialno 
    			+ "' and  exists (select 1 from ljtempfee  where paycode=ljtempfee.tempfeeno and confmakedate is not null )" 
    			+ " union all select paycode,polno from lysendtobank sb where serialno='"
    			+ serialno 
    			+ "' and notype='9' and exists (select 1 from lccont where prtno=sb.polno and appntname<>sb.accname) "
    			+ " union select paycode,polno from lysendtobank sb where serialno='"
    			+ serialno 
    			+ "' and exists (select 1 from ljtempfeeclass where sb.paycode=tempfeeno and paymode<>'4' ) "
    			;
    	ExeSQL tEx = new ExeSQL();
    	SSRS tSSRS = tEx.execSQL(sql);
    	if(tSSRS.getMaxRow() == 0){
    		return true;
    	} else if(!"1".equals(tLYBankLogSchema.getOthFlag())){
    		// 批次没有发送过邮件
    		// 发送邮件
//    		MailSender tMailSender = new MailSender("zhangchengxuan", "zhangcx3");
    		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
					.GetText(1, 2), "picchealth");
    		// 设置邮件发送信息
    		String sendInf = "集中代收付" + serialno + "批次出现异常收费数据，已停止该批次的发送，请核查批次内信息。";
    		for(int row = 1; row <= tSSRS.getMaxRow(); row++){
    			sendInf = sendInf + "收费号：" + tSSRS.GetText(row, 1) + "，业务号:" + tSSRS.GetText(row, 2) + "。";
    		}
    		tMailSender.setSendInf("集中代收付异常批次", sendInf, null);
    		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
    		tMailSender.setToAddress("7701");
    		if (!tMailSender.sendMail()) {
    			tMailSender.getErrorMessage();
    		}
    		// 更新日志表输出信息
    		tLYBankLogSchema.setOutFile("批次下存在异常收费数据，发送失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
            // 已发送邮件标记
            tLYBankLogSchema.setOthFlag("1");
            this.map.put(tLYBankLogSchema, "UPDATE");
    		return false;
    	} else {
    		return false;
    	}
    }
    
    public static void main(String[] arr)
    {
    	LYBankLogSchema banklog = new LYBankLogSchema();
    	LYBankLogDB banklogDB = new LYBankLogDB();
    	banklogDB.setSerialNo("77011020000000122182");
    	if(banklogDB.getInfo()){
    		banklog = banklogDB.getSchema();
    	}
    	System.out.println(banklog.getBankCode());
    	LYSendToBankSet sendtobank = new LYSendToBankSet();
    	LYSendToBankDB	sendtobankDB = new LYSendToBankDB();
    	String sql = "select * from lysendtobank where serialno='77011020000000122182' with ur";
    	
    	sendtobank = sendtobankDB.executeQuery(sql);
    	
    	BatchSendBaoRongXml bscx = new BatchSendBaoRongXml();
    	bscx.filePath = "E:\\";
    	bscx.mUrl = "E:\\";
    	bscx.createXml(banklog, sendtobank);
    }
}
