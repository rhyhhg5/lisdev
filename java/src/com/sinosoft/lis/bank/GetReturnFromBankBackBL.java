package com.sinosoft.lis.bank;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 回退文件返回
 * </p>
 * <p>
 * Copyright: Copyright (c) 2012
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author zhangjl
 * @version 1.0
 */

public class GetReturnFromBankBackBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();
	/** 传出数据的容器 */
	private VData mResult = new VData();
	/** 提交数据的容器 */
	private MMap map = new MMap();
	/** 数据操作字符串 */
	private String mOperate;
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	// 业务数据
	private TransferData transferData1 = new TransferData();
	private GlobalInput inGlobalInput = new GlobalInput();
	
    //统一更新日期
    private String mCurrentDate = PubFun.getCurrentDate();

    // 统一更新时间
    private String mCurrentTime = PubFun.getCurrentTime();

	/** 返回批次号 */
	private String serialNo = "";

	public GetReturnFromBankBackBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"READ"和""
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData())
			return false;
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData())
			return false;
		System.out.println("---End dealData---");

		// 文件读取标识
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mInputData, "READ")) {
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			transferData1 = (TransferData) mInputData.getObjectByObjectName(
					"TransferData", 0);
			inGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);

			serialNo = (String) transferData1.getValueByName("serialNo");
		} catch (Exception e) {
			// @@错误处理
			CError.buildErr(this, "接收数据失败");
			return false;
		}

		return true;
	}

	private boolean dealData() {
		

		ExeSQL tExeSQL = new ExeSQL();
		String tsql2 = "select bankcode " + "from LYBankLog "
				+ "where serialNo='" + serialNo + "' ";
		String bankcode = tExeSQL.getOneValue(tsql2);
		// "7003".equals(bankcode);
		// bankcode == null || bankcode.equals("7003")
		if (bankcode == null || bankcode.equals("")) {
			CError.buildErr(this, "查询批次信息异常，请尝试重新选择批次进行回退");
			return false;
		}
		
		if (bankcode.equals("7703") || bankcode.equals("7705")
				|| bankcode.equals("7706")) {
			CError.buildErr(this, "该批次为总公司转账批次，不能进行回退操作");
			return false;
		}
		
		if (bankcode.equals("9995")) {
			CError.buildErr(this, "深圳银联批次不能进行回退操作");
			return false;
		}

		String tsql1 = "select 1 " + "from LYBankLog " + "where serialNo='"
				+ serialNo + "' and (InFile is not null or returndate is not null)";
		String b = tExeSQL.getOneValue(tsql1);
		if (b == null || b.equals("")) {
			CError.buildErr(this, "该批次尚未进行文件返回操作，不能进行批次回退");
			return false;

		}
		
		String tsql = "select 1 " + "from LYBankLog " + "where serialNo='"
				+ serialNo + "' and dealstate is null";
		String a = tExeSQL.getOneValue(tsql);
		//  ||   |    &&   &
		if (a == null || a.equals("")) {
			CError.buildErr(this, "该批次已进行返回处理操作，不能进行批次回退");
			return false;
		}


		try {
			// 获取银行文件数据
			if (mOperate.equals("READ")) {
	            
				serialNo = (String) transferData1.getValueByName("serialNo");
				String sql = "update LYBankLog " 
					    + "set InFile= null"  + ", "
						+ "ReturnDate= null"  + ", "
						+ "returnoperator=null " + ", "
						+ "ModifyDate='" + mCurrentDate + "', " 
                        + "ModifyTime='" + mCurrentTime + "' " 
						+ " where serialNo='" + serialNo + "' ";
				System.out.println(sql);
				map.put(sql, "UPDATE");

				String sql1 = "delete from LYReturnFromBank "
						+ "where serialNo='" + serialNo + "' ";
				System.out.println(sql1);
				map.put(sql1, "DELETE");
			}
		} catch (Exception e) {
			// @@错误处理
			System.out.println(e.getMessage());
			CError.buildErr(this, "数据处理错误:" + e.getMessage());
			return false;
		}

		return true;

	}

	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception e) {
			buildError("prepareOutputData", "准备输往后台数据时出错");
			System.out.println("prepareOutputData, 准备输往后台数据时出错");
		}

		return true;
	}

	private void buildError(String funcName, String errMes) {
		CError tError = new CError();

		tError.moduleName = "LGAutoDeliver";
		tError.functionName = funcName;
		tError.errorMessage = errMes;

		this.mErrors.addOneError(tError);
	}

	public VData getResult() {
		return mResult;
	}
}
