package com.sinosoft.lis.bank;

import java.util.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.finfee.OperFinFeeGetBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class PayReturnFromBankBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();

  //业务数据
  private String totalMoney = "";
  private int sumNum = 0;
  private String serialNo = "";
  private String getbankcode="";
  private String getbankaccno="";
  private String autobankaccno="";
  private LYReturnFromBankSchema inLYReturnFromBankSchema = new LYReturnFromBankSchema();
  private TransferData inTransferData = new TransferData();
  private LYReturnFromBankSet inLYReturnFromBankSet = new LYReturnFromBankSet();
  private GlobalInput mGlobalInput = new GlobalInput();
  private String comCode = "";
      
  private LYDupGetSet outLYDupGetSet = new LYDupGetSet();
  private LYBankLogSet outLYBankLogSet = new LYBankLogSet();
  private LYReturnFromBankSet outDelLYReturnFromBankSet = new LYReturnFromBankSet();
  private LJFIGetSet outLJFIGetSet = new LJFIGetSet();
  private LJAGetSet outLJAGetSet = new LJAGetSet();
  private LYReturnFromBankBSet outLYReturnFromBankBSet = new LYReturnFromBankBSet();
  private LYSendToBankSet outDelLYSendToBankSet = new LYSendToBankSet();

  private LJAGetDrawSet outLJAGetDrawSet = new LJAGetDrawSet();
  private LJAGetEndorseSet outLJAGetEndorseSet = new LJAGetEndorseSet();
  private LJAGetTempFeeSet outLJAGetTempFeeSet = new LJAGetTempFeeSet();
  private LJAGetClaimSet outLJAGetClaimSet = new LJAGetClaimSet();
  private LJAGetOtherSet outLJAGetOtherSet = new LJAGetOtherSet();
  private LJABonusGetSet outLJABonusGetSet = new LJABonusGetSet();
  private LLCasePayAdvancedTraceSet outLLCasePayAdvancedTraceSet = new LLCasePayAdvancedTraceSet();
//理赔批案表
  private LLRegisterSet mLLRegisterSet = new LLRegisterSet();
//理赔案件表
  private LLCaseSet mLLCaseSet = new LLCaseSet();
  private LLPrepaidClaimSet mLLPrepaidClaimSet = new LLPrepaidClaimSet();
  public PayReturnFromBankBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代收
    if (mOperate.equals("PAYMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start PayReturnFromBank BLS Submit...");
      PayReturnFromBankBLS tPayReturnFromBankBLS = new PayReturnFromBankBLS();
      if(tPayReturnFromBankBLS.submitData(mInputData, cOperate) == false)	{
        // @@错误处理
        this.mErrors.copyAllErrors(tPayReturnFromBankBLS.mErrors);
        mResult.clear();
        return false;
      }
      System.out.println("End PayReturnFromBank BLS Submit...");

      //如果有需要处理的错误，则返回
      if (tPayReturnFromBankBLS.mErrors .needDealError())  {
        this.mErrors.copyAllErrors(tPayReturnFromBankBLS.mErrors ) ;
      }
    }
    //银行代付
    else if (mOperate.equals("PAYMONEY")) {
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      inLYReturnFromBankSchema = (LYReturnFromBankSchema)mInputData.getObjectByObjectName("LYReturnFromBankSchema", 0);
      inTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);

      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
      comCode = mGlobalInput.ComCode;
      System.out.println("登录机构：" + comCode);
      if(comCode.length() == 2) {
          comCode = comCode + "000000";
      } else if(comCode.length() == 4) {
          comCode = comCode + "0000";
      }
      System.out.println("付费机构：" + comCode);
    }
    catch (Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PayReturnFromBankBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 获取交费日期在设置的日期区间内的总应付表记录
   * @param startDate
   * @param endDate
   * @return
   */
  private LJAGetSet getLJAGetByPaydate(String startDate, String endDate) {
    String tSql = "select * from LJAGet where PayDate >= '" + startDate
                + "' and PayDate <= '" + endDate + "'"
                + " and BankOnTheWayFlag = 0";

    LJAGetDB tLJAGetDB = new LJAGetDB();
    LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(tSql);

    if (tLJAGetDB.mErrors.getErrorCount()>0) return null;
    else return tLJAGetSet;
  }

  /**
   * 生成送银行表数据
   * @param tLJAGetSet
   * @return
   */
  private LYReturnFromBankSet getReturnFromBank(LJAGetSet tLJAGetSet) {
    int i;
    serialNo = PubFun1.CreateMaxNo("1", 20);
    LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();

    for (i=0; i<tLJAGetSet.size(); i++) {
      LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

      //生成送银行表数据
      LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();

      tLYReturnFromBankSet.add(tLYReturnFromBankSchema);

    }

    return tLYReturnFromBankSet;
  }

  /**
   * 获取银行返回数据
   * @param inLYReturnFromBankSchema
   * @return
   */
  private LYReturnFromBankSet getLYReturnFromBank(LYReturnFromBankSchema inLYReturnFromBankSchema) {
    LYReturnFromBankSet tLYReturnFromBankSet = inLYReturnFromBankSchema.getDB().query();

    return tLYReturnFromBankSet;
  }

  /**
   * 获取银行返款成功标志信息
   * @param tLYReturnFromBankSchema
   * @return
   */
  private String getBankSuccFlag(LYReturnFromBankSchema tLYReturnFromBankSchema) {
    LDBankSchema tLDBankSchema = new LDBankSchema();

    tLDBankSchema.setBankCode(tLYReturnFromBankSchema.getBankCode());
    tLDBankSchema.setSchema(tLDBankSchema.getDB().query().get(1));

    return tLDBankSchema.getAgentGetSuccFlag();
  }

  /**
   * 校验银行返款成功标志
   * @param bankSuccFlag
   * @param tLYReturnFromBankSchema
   * @return
   */
  private boolean verifyBankSuccFlag(String bankSuccFlag, LYReturnFromBankSchema tLYReturnFromBankSchema) {
    int i;
    boolean passFlag = false;

    String[] arrSucc = PubFun.split(bankSuccFlag, ";");
    String tSucc = tLYReturnFromBankSchema.getBankSuccFlag();

    for (i=0; i<arrSucc.length; i++) {
       if(arrSucc[i].equals("") && tSucc==null){
           passFlag = true;
           break; 
       }
      if (arrSucc[i].equals(tSucc)) {
        passFlag = true;
        break;
      }
    }

    return passFlag;
  }

  /**
   * 校验银行扣款成功与否
   */
  private void verifyBankSucc() {
    int i;
    String bankSuccFlag = getBankSuccFlag(inLYReturnFromBankSet.get(1));

    for (i=0; i<inLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i + 1);
      if (tLYReturnFromBankSchema.getConvertFlag() != null) continue;

      if (!verifyBankSuccFlag(bankSuccFlag, tLYReturnFromBankSchema)) {
        tLYReturnFromBankSchema.setConvertFlag("1");
        inLYReturnFromBankSet.set(i+1, tLYReturnFromBankSchema);
      }
    }
  }

  /**
   * 校验财务总金额
   * @param tLYReturnFromBankSet
   * @param totalMoney
   * @return
   */
  private boolean confirmTotalMoney(LYReturnFromBankSet tLYReturnFromBankSet, String totalMoney) {
    int i;
    double sumMoney = 0;
    double fTotalMoney = Double.valueOf(totalMoney).doubleValue();

    for (i=0; i<tLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet.get(i + 1);
      if (tLYReturnFromBankSchema.getConvertFlag() != null) continue;

      sumMoney = sumMoney + tLYReturnFromBankSchema.getPayMoney();
      sumMoney = PubFun.setPrecision(sumMoney, "0.00");
      sumNum = sumNum + 1;
    }

    System.out.println("输入金额：" + fTotalMoney + "\n银行返还金额：" + sumMoney);
    return (fTotalMoney == sumMoney);
  }

  /**
   * 记录入重复交费记录表
   * @param tLYReturnFromBankSchema
   */
  private void setLYDupGet(LYReturnFromBankSchema tLYReturnFromBankSchema) {
    Reflections tReflections = new Reflections();
    LYDupGetSchema tLYDupGetSchema = new LYDupGetSchema();
    tReflections.transFields(tLYDupGetSchema, tLYReturnFromBankSchema);
    tLYDupGetSchema.setDataType("F");
    outLYDupGetSet.add(tLYDupGetSchema);
  }

  /**
   * 校验每笔金额
 * @throws Exception 
   */
  private void verifyUnitMoney() throws Exception {
    int i;

    for (i=0; i<inLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i + 1);
      if (tLYReturnFromBankSchema.getConvertFlag() != null) continue;

      LJAGetSchema tLJAGetSchema = new LJAGetSchema();
      tLJAGetSchema.setActuGetNo(tLYReturnFromBankSchema.getPayCode());
      tLJAGetSchema.setSchema(tLJAGetSchema.getDB().query().get(1));
	  tLJAGetSchema.setInsBankCode(getbankcode);
	   if("00".equals(autobankaccno)){
       	System.out.println("302行:"+getbankaccnomain(tLJAGetSchema));
       	tLJAGetSchema.setInsBankAccNo(getbankaccnomain(tLJAGetSchema));
       }else{
       	tLJAGetSchema.setInsBankAccNo(getbankaccno);
       }
      //付款不等于实付表中的信息，则转入重复给付表，否则通过
      if (tLJAGetSchema.getSumGetMoney() != tLYReturnFromBankSchema.getPayMoney()) {
        setLYDupGet(tLYReturnFromBankSchema);
        tLYReturnFromBankSchema.setConvertFlag("2");
      }

      inLYReturnFromBankSet.set(i+1, tLYReturnFromBankSchema);
    }
  }

  /**
   * 通过银行返回数据获取应收表数据
   * @param tLYReturnFromBankSchema
   * @return
   */
  private LJAGetSchema getLJAGetByLYReturnFromBank(LYReturnFromBankSchema tLYReturnFromBankSchema) {
    LJAGetSchema tLJAGetSchema = new LJAGetSchema();

    tLJAGetSchema.setActuGetNo(tLYReturnFromBankSchema.getPayCode());
    return tLJAGetSchema.getDB().query().get(1);
  }

  /**
   * 转入财务付费表
   * @param tLYReturnFromBankSchema
   */
  private void setLJFIGet(LYReturnFromBankSchema tLYReturnFromBankSchema) throws Exception {
    LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
    LJAGetSchema tLJAGetSchema = getLJAGetByLYReturnFromBank(tLYReturnFromBankSchema);

    tLJFIGetSchema.setActuGetNo(tLYReturnFromBankSchema.getPayCode());
    tLJFIGetSchema.setPayMode("4");
    tLJFIGetSchema.setOtherNo(tLYReturnFromBankSchema.getPolNo());
   // tLJFIGetSchema.setOtherNoType("2");
    tLJFIGetSchema.setGetMoney(tLYReturnFromBankSchema.getPayMoney());
    tLJFIGetSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
    tLJFIGetSchema.setConfDate(PubFun.getCurrentDate());
    tLJFIGetSchema.setShouldDate(PubFun.getCurrentDate());
    tLJFIGetSchema.setOperator(mGlobalInput.Operator);
    tLJFIGetSchema.setMakeDate(PubFun.getCurrentDate());
    tLJFIGetSchema.setMakeTime(PubFun.getCurrentTime());
    tLJFIGetSchema.setModifyDate(PubFun.getCurrentDate());
    tLJFIGetSchema.setModifyTime(PubFun.getCurrentTime());

    tLJFIGetSchema.setBankCode(tLYReturnFromBankSchema.getBankCode());
    tLJFIGetSchema.setConfMakeDate(PubFun.getCurrentDate());
    String tSerialNo = PubFun1.CreateMaxNo("ReturnFromBank", 20);
    tLJFIGetSchema.setSerialNo(tSerialNo);
    LJAGetDB tLJAGetDB = new LJAGetDB();
    tLJAGetDB.setActuGetNo(tLYReturnFromBankSchema.getPayCode());
    if (tLJAGetDB.getInfo()) {
      tLJFIGetSchema.setSaleChnl(tLJAGetDB.getSaleChnl());
      System.out.println("autobankaccno1111111111:"+autobankaccno);
      if(!"00".equals(autobankaccno)){
          String tmanagecomt = comCode;
          String sqlStr1="select * from LDFinBank where managecom='"+comCode+"' and bankaccno='"+getbankaccno+"' and bankcode='"+getbankcode+"' and finflag='F' order by managecom with ur";     
          System.out.println("用户校验查询ldfinbank:"+sqlStr1);      
          LDFinBankDB tLDFinBankDB=new LDFinBankDB();
          LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(sqlStr1);          
          if(tLDFinBankSet.size()==0)
              throw new Exception("该本方银行账户在ldfinbank表中无数据！");
 //     String tmanagecomt=getManageComFromLDFinBank(tLJAGetDB.getActuGetNo());
      System.out.println("tmanagecomt22222222222222:"+tmanagecomt);
          if(!"000".equals(tmanagecomt)){
             tLJFIGetSchema.setManageCom(tmanagecomt);
            }
             else{
        	  tLJFIGetSchema.setManageCom(tLJAGetDB.getManageCom());
          }
         }
      else{
          tLJFIGetSchema.setManageCom(tLJAGetDB.getManageCom());  
      }
      tLJFIGetSchema.setAgentCom(tLJAGetDB.getAgentCom());
      tLJFIGetSchema.setAgentType(tLJAGetDB.getAgentType());
      tLJFIGetSchema.setAgentGroup(tLJAGetDB.getAgentGroup());
      tLJFIGetSchema.setAgentCode(tLJAGetDB.getAgentCode());
      tLJFIGetSchema.setOtherNoType(tLJAGetDB.getOtherNoType());
      if((tLJAGetDB.getOtherNoType().equals("5"))||(tLJAGetDB.getOtherNoType().equals("C"))||(tLJAGetDB.getOtherNoType().equals("F"))){
    	  String qljagetclaim="select * from Ljagetclaim where actugetno='"+tLJAGetDB.getActuGetNo()+"' with ur"; 
    	  LJAGetClaimDB tLJAGetClaimDB=new LJAGetClaimDB();
          LJAGetClaimSet tLJAGetClaimSet=tLJAGetClaimDB.executeQuery(qljagetclaim);
          LJAGetClaimSchema tLJAGetClaimSchema=new LJAGetClaimSchema();
          tLJAGetClaimSchema=tLJAGetClaimSet.get(1);
          tLJFIGetSchema.setPolicyCom(tLJAGetClaimSchema.getManageCom());
          
    	  
      }else{
           tLJFIGetSchema.setPolicyCom(tLJAGetDB.getManageCom());
      }
      tLJAGetSchema.setManageCom(tLJFIGetSchema.getPolicyCom());
    }
    outLJFIGetSet.add(tLJFIGetSchema);
  }

  /**
   * 校验实付表
   */
  private void verifyLJAGet() throws Exception{
    int i;

    for (i=0; i<inLYReturnFromBankSet.size(); i++) {
      //遍历每一条返回盘记录
      LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i + 1);
      if (tLYReturnFromBankSchema.getConvertFlag() != null) continue;

      //转入财务付费表
      setLJFIGet(tLYReturnFromBankSchema);
    }
  }

  /**
   * 备份银行返回盘记录表到B表
   */
  private void getLYReturnFromBankB() {
    int i;
    Reflections tReflections = new Reflections();

    for (i=0; i<inLYReturnFromBankSet.size(); i++) {
      LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i + 1);

      LYReturnFromBankBSchema tLYReturnFromBankBSchema = new LYReturnFromBankBSchema();
      tReflections.transFields(tLYReturnFromBankBSchema, tLYReturnFromBankSchema);
      tLYReturnFromBankBSchema.setBankUnSuccReason(getUnSuccReason(tLYReturnFromBankSchema));
      outLYReturnFromBankBSet.add(tLYReturnFromBankBSchema);
    }
  }
  /**
   * 获取失败原因
   */
  private String getUnSuccReason(LYReturnFromBankSchema tLYReturnFromBankSchema){
      try{
         LDCode1DB tLDCode1DB=new LDCode1DB();
         tLDCode1DB.setCodeType("bankerror");
         
         String serNo = tLYReturnFromBankSchema.getSerialNo();
         String sql = "select bankcode from lybanklog where serialno='" + serNo + "'";
         ExeSQL tExeSQL = new ExeSQL();
         SSRS tSSRS = tExeSQL.execSQL(sql);
         
         tLDCode1DB.setCode(tSSRS.GetText(1, 1));
         tLDCode1DB.setCode1(tLYReturnFromBankSchema.getBankSuccFlag());
         return tLDCode1DB.query().get(1).getCodeName();
      }catch(Exception e){
      }
      return "";
  }

  /**
   * 获取发送盘数据，用于删除
   */
  private void getLYSendToBank() {
    LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();

    tLYSendToBankSchema.setSerialNo(serialNo);
    outDelLYSendToBankSet = tLYSendToBankSchema.getDB().query();
  }

  /**
   * 获取实付表数据，修改银行在途标志、财务到帐日期、财务确认日期
   */
  private void getLJAGet() throws Exception {
    try {
      for (int i=0; i<inLYReturnFromBankSet.size(); i++) {
        LYReturnFromBankSchema tLYReturnFromBankSchema = inLYReturnFromBankSet.get(i + 1);

        //获取实付总表数据
        LJAGetSchema tLJAGetSchema = getLJAGetByLYReturnFromBank(tLYReturnFromBankSchema);
        tLJAGetSchema.setBankOnTheWayFlag("0");
        tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
        tLJAGetSchema.setInsBankCode(getbankcode);
        if("00".equals(autobankaccno)){
        	System.out.println("449行:"+getbankaccnomain(tLJAGetSchema));
        	tLJAGetSchema.setInsBankAccNo(getbankaccnomain(tLJAGetSchema));
        }else{
        	tLJAGetSchema.setInsBankAccNo(getbankaccno);
        }
        //如果不能正常核销
        if (tLYReturnFromBankSchema.getConvertFlag() != null ) {
              tLJAGetSchema.setCanSendBank("1");
              outLJAGetSet.add(tLJAGetSchema);
              continue;
        }

        //核销
        tLJAGetSchema.setEnterAccDate(tLYReturnFromBankSchema.getBankDealDate());
        tLJAGetSchema.setConfDate(PubFun.getCurrentDate());
        outLJAGetSet.add(tLJAGetSchema);

        String OtherNoType = tLJAGetSchema.getOtherNoType();

        //更新给付表(生存领取_实付)   LJAGetDraw
        if (OtherNoType.equals("1") || OtherNoType.equals("2") || OtherNoType.equals("0")) {
          updateLJAGetDraw(tLJAGetSchema);
        }

        //更新批改补退费表(实收/实付) LJAGetEndorse
        if (OtherNoType.equals("3")) {
          updateLJAGetEndorse(tLJAGetSchema);
        }

        //更新暂交费退费实付表 LJAGetTempFee
        if (OtherNoType.equals("4")) {
          updateLJAGetTempFee(tLJAGetSchema);
        }

        //更新赔付实付表 LJAGetClaim
        if (OtherNoType.equals("5")||OtherNoType.equals("Y")) {
          updateLJAGetClaim(tLJAGetSchema);
        }

        //更新其他退费实付表 LJAGetOther
        if(OtherNoType.equals("6")||OtherNoType.equals("8")) {
          updateLJAGetOther(tLJAGetSchema);
        }

        //更新红利给付实付表 LJABonusGet
        if(OtherNoType.equals("7")) {
          updateLJABonusGet(tLJAGetSchema);
        }
        
//      更新社保意外险平台实付表 LLCasePayAdvancedTrace
        if (OtherNoType.equals("D")) {
          updateLLCasePayAdvancedTrace(tLJAGetSchema);
        }
      }
    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * 查询给付表(生存领取_实付)
   * @param LJAGetBL
   * @return
   */
  private void updateLJAGetDraw(LJAGetSchema mLJAGetSchema) throws Exception {
    String sqlStr = "select * from LJAGetDraw where ActuGetNo='" + mLJAGetSchema.getActuGetNo() + "'";
    System.out.println("查询给付表(生存领取_实付):"+sqlStr);
    LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
    LJAGetDrawSet tLJAGetDrawSet = tLJAGetDrawDB.executeQuery(sqlStr);

    if (tLJAGetDrawSet.size()==0) throw new Exception("给付表无数据，不能核销！");

    for (int n=1; n<=tLJAGetDrawSet.size(); n++) {
      LJAGetDrawSchema tLJAGetDrawSchema = tLJAGetDrawSet.get(n);
      tLJAGetDrawSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJAGetDrawSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetDrawSchema.setModifyDate(PubFun.getCurrentDate());
      tLJAGetDrawSchema.setModifyTime(PubFun.getCurrentTime());
      outLJAGetDrawSet.add(tLJAGetDrawSchema);
    }
  }

  /**
   * 查询批改补退费表(实收/实付)
   * @param LJAGetSchema
   * @return
   */
  private void updateLJAGetEndorse(LJAGetSchema mLJAGetSchema) throws Exception {
    String sqlStr="select * from LJAGetEndorse where ActuGetNo='"+mLJAGetSchema.getActuGetNo()+"'";
    System.out.println("查询给付表(生存领取_实付):"+sqlStr);
    LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
    LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(sqlStr);

    if (tLJAGetEndorseSet.size()==0) throw new Exception("批改补退费表无数据，不能核销！");

    for (int n=1; n<=tLJAGetEndorseSet.size(); n++) {
      LJAGetEndorseSchema tLJAGetEndorseSchema = tLJAGetEndorseSet.get(n);
      tLJAGetEndorseSchema.setGetConfirmDate(mLJAGetSchema.getConfDate());
      tLJAGetEndorseSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
      tLJAGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
      outLJAGetEndorseSet.add(tLJAGetEndorseSchema);
    }
  }

  /**
   * 查询其他退费实付表
   * @param LJAGetSchema
   * @return
   */
  private void updateLJAGetOther(LJAGetSchema mLJAGetSchema) throws Exception {
    String sqlStr="select * from LJAGetOther where ActuGetNo='"+mLJAGetSchema.getActuGetNo()+"'";
    System.out.println("其他退费实付表:"+sqlStr);
    LJAGetOtherDB tLJAGetOtherDB = new LJAGetOtherDB();
    LJAGetOtherSet tLJAGetOtherSet = tLJAGetOtherDB.executeQuery(sqlStr);

    if (tLJAGetOtherSet.size()==0) throw new Exception("其他退费实付表无数据，不能核销！");

    for (int n=1; n<=tLJAGetOtherSet.size(); n++) {
      LJAGetOtherSchema tLJAGetOtherSchema = tLJAGetOtherSet.get(n);
      tLJAGetOtherSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJAGetOtherSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetOtherSchema.setModifyDate(PubFun.getCurrentDate());
      tLJAGetOtherSchema.setModifyTime(PubFun.getCurrentTime());
      outLJAGetOtherSet.add(tLJAGetOtherSchema);
    }
  }

 /**
   * 赔付实付表
   * @param LJAGetSchema
   * @return
   */
  private void updateLJAGetClaim(LJAGetSchema mLJAGetSchema) throws Exception {
    String sqlStr="select * from LJAGetClaim where ActuGetNo='"+mLJAGetSchema.getActuGetNo()+"'";
    System.out.println("赔付实付表:"+sqlStr);
    LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
    LJAGetClaimSet tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
    setLLCaseFalg(mLJAGetSchema.getOtherNo(),tLJAGetClaimSet);
    if(mLJAGetSchema.getOtherNoType().equals("Y"))
    {
      LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
      tLLPrepaidClaimDB.setPrepaidNo(mLJAGetSchema.getOtherNo());
      if(tLLPrepaidClaimDB.getInfo())
      {
          LLPrepaidClaimSchema tLLPrepaidClaimSchema = tLLPrepaidClaimDB.getSchema();
          tLLPrepaidClaimSchema.setRgtState("06");
          this.mLLPrepaidClaimSet.add(tLLPrepaidClaimSchema);
          
      }
    }
    if (tLJAGetClaimSet.size()==0) throw new Exception("赔付实付表无数据，不能核销！");

    for (int n=1; n<=tLJAGetClaimSet.size(); n++) {
      LJAGetClaimSchema tLJAGetClaimSchema = tLJAGetClaimSet.get(n);
      tLJAGetClaimSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJAGetClaimSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
      tLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());
      outLJAGetClaimSet.add(tLJAGetClaimSchema);
    }
  }
  
  public boolean setLLCaseFalg(String strRgtNo,LJAGetClaimSet aLJAGetClaimSet)
  {
      LLRegisterDB tLLRegisterDB = new LLRegisterDB();
      tLLRegisterDB.setRgtNo(strRgtNo);
      if(tLLRegisterDB.getInfo()){
          tLLRegisterDB.setRgtState("04");
          this.mLLRegisterSet.add(tLLRegisterDB.getSchema());
      }
      String tRgtNo = "";
      String tCaseNo = "";
      for (int k=1;k<=aLJAGetClaimSet.size();k++){
          String tfeeFinaType=aLJAGetClaimSet.get(k).getFeeFinaType();
          if(tfeeFinaType!=null && (tfeeFinaType.equals("JJ") || tfeeFinaType.equals("ZJ"))){
              continue;
          }else{
              LLCaseDB tLLCaseDB = new LLCaseDB();
              tLLCaseDB.setCaseNo(aLJAGetClaimSet.get(k).getOtherNo());
              if(tLLCaseDB.getInfo()){
                  tRgtNo = tLLCaseDB.getRgtNo();
                  tCaseNo = tLLCaseDB.getCaseNo();
                  tLLCaseDB.setRgtState("12");
                  this.mLLCaseSet.add(tLLCaseDB.getSchema());
              }
          }
      }
      //团单下最后一个人做完给付确认应更新团单案件的状态为“04”－所有个人给付完毕
      if(strRgtNo.substring(0,1).equals("C")&&!tRgtNo.equals(tCaseNo)){
          LLRegisterDB tsLLRegisterDB = new LLRegisterDB();
          tsLLRegisterDB.setRgtNo(tRgtNo);
          if (tsLLRegisterDB.getInfo()) {
              if ("1".equals(tsLLRegisterDB.getRgtClass())) {
                  boolean tFlag = true;
                  LLCaseDB tmpLLCaseDB = new LLCaseDB();
                  LLCaseSet tmpLLCaseSet = new LLCaseSet();
                  tmpLLCaseDB.setRgtNo(tsLLRegisterDB.getRgtNo());
                  tmpLLCaseSet.set(tmpLLCaseDB.query());
                  if (tmpLLCaseSet != null && tmpLLCaseSet.size() != 0) {
                      for (int m = 1; m <= tmpLLCaseSet.size(); m++) {
                          if (tmpLLCaseSet.get(m).getCaseNo().equals(tCaseNo))
                              continue;
                          if (!("12".equals(tmpLLCaseSet.get(m).getRgtState()) || "14".equals(tmpLLCaseSet.get(m).getRgtState()))) {
                              tFlag = false;
                              break;
                          }
                      }
                  }
                  if (tFlag) {
                      tsLLRegisterDB.setRgtState("04");
                      this.mLLRegisterSet.add(tsLLRegisterDB.getSchema());
                  }
              }
          }
         }
      return true;
  }


 /**
   * 暂交费退费实付表
   * @param LJAGetSchema
   * @return
   */
  private void updateLJAGetTempFee(LJAGetSchema mLJAGetSchema) throws Exception {
    String sqlStr="select * from LJAGetTempFee where ActuGetNo='"+mLJAGetSchema.getActuGetNo()+"'";
    System.out.println("暂交费退费实付表:"+sqlStr);
    LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
    LJAGetTempFeeSet tLJAGetTempFeeSet = tLJAGetTempFeeDB.executeQuery(sqlStr);

    if (tLJAGetTempFeeSet.size()==0) throw new Exception("暂交费退费实付表无数据，不能核销！");

    for (int n=1; n<=tLJAGetTempFeeSet.size(); n++) {
      LJAGetTempFeeSchema tLJAGetTempFeeSchema = tLJAGetTempFeeSet.get(n);
      tLJAGetTempFeeSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJAGetTempFeeSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
      tLJAGetTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
      outLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
    }
  }

 /**
   * 红利给付实付表
   * @param LJAGetSchema
   * @return
   */
  private void updateLJABonusGet(LJAGetSchema mLJAGetSchema) throws Exception {
    String sqlStr="select * from LJABonusGet where ActuGetNo='"+mLJAGetSchema.getActuGetNo()+"'";
    System.out.println("红利给付实付表:"+sqlStr);
    LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB();
    LJABonusGetSet tLJABonusGetSet = tLJABonusGetDB.executeQuery(sqlStr);

    if (tLJABonusGetSet.size()==0) throw new Exception("红利给付实付表无数据，不能核销！");

    for (int n=1; n<=tLJABonusGetSet.size(); n++) {
      LJABonusGetSchema tLJABonusGetSchema = tLJABonusGetSet.get(n);
      tLJABonusGetSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJABonusGetSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJABonusGetSchema.setModifyDate(PubFun.getCurrentDate());
      tLJABonusGetSchema.setModifyTime(PubFun.getCurrentTime());
      outLJABonusGetSet.add(tLJABonusGetSchema);
    }
  }

  private void updateLLCasePayAdvancedTrace(LJAGetSchema mLJAGetSchema) throws Exception {
	    String sqlStr="select * from LLCasePayAdvancedTrace where ActuGetNo='"+mLJAGetSchema.getActuGetNo()+"'";
	    System.out.println("社保意外险平台实付表:"+sqlStr);
	    LLCasePayAdvancedTraceDB tLLCasePayAdvancedTraceDB = new LLCasePayAdvancedTraceDB();
	    LLCasePayAdvancedTraceSet tLLCasePayAdvancedTraceSet = tLLCasePayAdvancedTraceDB.executeQuery(sqlStr);
	    //setLLCaseFalg(mLLCasePayAdvancedTraceSchema.getOtherNo(),tLLCasePayAdvancedTraceSet);
	    if (tLLCasePayAdvancedTraceSet.size()==0) throw new Exception("社保意外险平台实付表无数据，不能核销！");

	    for (int n=1; n<=tLLCasePayAdvancedTraceSet.size(); n++) {
	      LLCasePayAdvancedTraceSchema tLLCasePayAdvancedTraceSchema = tLLCasePayAdvancedTraceSet.get(n);
	      tLLCasePayAdvancedTraceSchema.setDealState("02");
	      tLLCasePayAdvancedTraceSchema.setModifyDate(PubFun.getCurrentDate());
	      tLLCasePayAdvancedTraceSchema.setModifyTime(PubFun.getCurrentTime());
	      outLLCasePayAdvancedTraceSet.add(tLLCasePayAdvancedTraceSchema);
	    }
	  }

  /**
   * 生成银行日志数据
   * @param tLYSendToBankSchema
   * @return
   */
  private LYBankLogSet getLYBankLog() {
    LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
    LYBankLogSet tLYBankLogSet = new LYBankLogSet();

    //获取日志记录
    tLYBankLogSchema.setSerialNo(serialNo);
    tLYBankLogSet.set(tLYBankLogSchema.getDB().query());

    if (tLYBankLogSet.size() > 0) {
      tLYBankLogSchema.setSchema(tLYBankLogSet.get(1));

      //修改日志
      tLYBankLogSchema.setAccTotalMoney(totalMoney);                 //财务确认总金额
      tLYBankLogSchema.setBankSuccMoney(totalMoney);                 //银行成功总金额
      tLYBankLogSchema.setBankSuccNum(sumNum + "");                  //银行成功总数量
      tLYBankLogSchema.setDealState("1");                            //处理状态
      tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
      tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());

      tLYBankLogSet.clear();
      tLYBankLogSet.add(tLYBankLogSchema);
    }

    return tLYBankLogSet;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    int i;

    try {
      //银行代付
      if (mOperate.equals("PAYMONEY")) {
        //获取银行返回数据
        inLYReturnFromBankSet = getLYReturnFromBank(inLYReturnFromBankSchema);
        if (inLYReturnFromBankSet.size() == 0) throw new NullPointerException("无银行返回数据！");
        System.out.println("---End getLYReturnFromBank---");

        //记录批次号、总金额、备份银行返回盘表
        serialNo = inLYReturnFromBankSet.get(1).getSerialNo();
        totalMoney = (String)inTransferData.getValueByName("totalMoney");
        outDelLYReturnFromBankSet.set(inLYReturnFromBankSet);
        getbankcode =(String)inTransferData.getValueByName("getbankcode");
        getbankaccno =(String)inTransferData.getValueByName("getbankaccno");
        autobankaccno =(String)inTransferData.getValueByName("autobankaccno");
        System.out.println("autobankaccnoBL:"+autobankaccno);
      
        //校验银行付款成功与否
        verifyBankSucc();
        System.out.println("---End verifyBankSucc---");

        //校验财务总金额
        if (!confirmTotalMoney(inLYReturnFromBankSet, totalMoney))
          throw new NullPointerException("财务总金额确认失败！请与银行对帐！");
        System.out.println("---End confirmTotalMoney---");

        //校验每笔金额
        verifyUnitMoney();
        System.out.println("---End verifyUnitMoney---");

        //校验实付表，转入财务付费表
        verifyLJAGet();
        System.out.println("---End verifyLJAGet---");

        //备份返回盘数据到返回盘B表
        getLYReturnFromBankB();
        System.out.println("---End getLYReturnFromBankB---");

        //获取发送盘数据，用于删除
        getLYSendToBank();
        System.out.println("---End getLYSendToBank---");

        //获取实付表数据，修改银行在途标志、财务到帐日期、财务确认日期
        getLJAGet();
        System.out.println("---End getLJAGet---");

        //记录日志
        outLYBankLogSet = getLYBankLog();
        if (outLYBankLogSet.size() == 0) throw new NullPointerException("无银行日志数据！");
        System.out.println("---End verifyUnitMoney---");
      }

      //银行代付,团单下最后一个人做完给付确认应更新团单案件的状态为“04”－所有个人给付完毕
      else if (mOperate.equals("CLAIM")) {
          //记录批次号、总金额、备份银行返回盘表
          serialNo = inLYReturnFromBankSchema.getSerialNo();
          LYReturnFromBankBDB tLYReturnFromBankBDB=new LYReturnFromBankBDB();
          tLYReturnFromBankBDB.setSerialNo(serialNo);
          LYReturnFromBankBSet tLYReturnFromBankBSet=tLYReturnFromBankBDB.query();
          try {
              for (int j=0; j<tLYReturnFromBankBSet.size(); j++) {
                LYReturnFromBankBSchema tLYReturnFromBankBSchema = tLYReturnFromBankBSet.get(j+1);
                LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                tLJAGetSchema.setActuGetNo(tLYReturnFromBankBSchema.getPayCode());
                tLJAGetSchema= tLJAGetSchema.getDB().query().get(1);
              
                String OtherNoType = tLJAGetSchema.getOtherNoType();

                //更新赔付实付表 LJAGetClaim
                if (OtherNoType.equals("5") && !StrTool.cTrim(tLJAGetSchema.getConfDate()).equals("")) {
                    String sqlStr="select * from LJAGetClaim where ActuGetNo='"+tLJAGetSchema.getActuGetNo()+"' with ur";
                    LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
                    LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
                    tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
                    String tRgtNo = "";
                    String tCaseNo = "";
                    for (int k=1;k<=tLJAGetClaimSet.size();k++){
                        String tfeeFinaType=tLJAGetClaimSet.get(k).getFeeFinaType();
                        if(tfeeFinaType!=null && (tfeeFinaType.equals("JJ") || tfeeFinaType.equals("ZJ"))){
                            continue;
                        }else{
                            LLCaseDB tLLCaseDB = new LLCaseDB();
                            tLLCaseDB.setCaseNo(tLJAGetClaimSet.get(k).getOtherNo());
                            if(tLLCaseDB.getInfo()){
                                tRgtNo = tLLCaseDB.getRgtNo();
                                tCaseNo = tLLCaseDB.getCaseNo();
                            }
                        }
                    }
                    //团单下最后一个人做完给付确认应更新团单案件的状态为“04”－所有个人给付完毕
                    if(tLJAGetSchema.getOtherNo().substring(0,1).equals("C")&&!tRgtNo.equals(tCaseNo)){
                        LLRegisterDB tsLLRegisterDB = new LLRegisterDB();
                        tsLLRegisterDB.setRgtNo(tRgtNo);
                        if (tsLLRegisterDB.getInfo()) {
                            if ("1".equals(tsLLRegisterDB.getRgtClass())) {
                                boolean tFlag = true;
                                LLCaseDB tmpLLCaseDB = new LLCaseDB();
                                LLCaseSet tmpLLCaseSet = new LLCaseSet();
                                tmpLLCaseDB.setRgtNo(tsLLRegisterDB.getRgtNo());
                                tmpLLCaseSet.set(tmpLLCaseDB.query());
                                if (tmpLLCaseSet != null && tmpLLCaseSet.size() != 0) {
                                    for (int m = 1; m <= tmpLLCaseSet.size(); m++) {
                                        if (tmpLLCaseSet.get(m).getCaseNo().equals(tCaseNo))
                                            continue;
                                        if (!("12".equals(tmpLLCaseSet.get(m).getRgtState()) || "14".equals(tmpLLCaseSet.get(m).getRgtState()))) {
                                            tFlag = false;
                                            break;
                                        }
                                    }
                                }
                                if (tFlag) {
                                    tsLLRegisterDB.setRgtState("04");
                                    tsLLRegisterDB.update();
                                }
                            }
                        }
                    }
                }//end if (OtherNoType.equals("5"))
              }
            }
            catch (Exception e) {
              throw e;
            }
      }
    }
    catch(Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PayReturnFromBankBL";
      tError.functionName = "dealData";
      tError.errorMessage = "数据处理错误! " + e.getMessage();
      this.mErrors .addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    mInputData = new VData();

    try {
      mInputData.add(outLYDupGetSet);
      mInputData.add(outLYBankLogSet);
      mInputData.add(outDelLYReturnFromBankSet);
      mInputData.add(outLJFIGetSet);
      mInputData.add(outLJAGetSet);
      mInputData.add(outLYReturnFromBankBSet);
      mInputData.add(outDelLYSendToBankSet);

      mInputData.add(outLJAGetDrawSet);
      mInputData.add(outLJAGetEndorseSet);
      mInputData.add(outLJAGetTempFeeSet);
      mInputData.add(outLJAGetClaimSet);
      mInputData.add(outLJAGetOtherSet);
      mInputData.add(outLJABonusGetSet);
      mInputData.add(outLLCasePayAdvancedTraceSet);
      mInputData.add(this.mLLCaseSet);
      mInputData.add(this.mLLPrepaidClaimSet);
      mInputData.add(this.mLLRegisterSet);   
    }
    catch(Exception ex) {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="PayReturnFromBankBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  /**
   * 从LDFinBank表中获取数据
   * @return 管理机构
   */
  public String getManageComFromLDFinBank(String actugetno) throws Exception{
      
      String outString=new String();
      
      String sqlStr1="select * from LDFinBank where bankcode='"+getbankcode+"' and bankaccno='"+getbankaccno+"' and finflag='F' order by managecom with ur";     
      System.out.println("查询ldfinbank中的managecom:"+sqlStr1);      
      LDFinBankDB tLDFinBankDB=new LDFinBankDB();
      LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(sqlStr1);
      LDFinBankSchema tLDFinBankSchemat=tLDFinBankSet.get(1);
      if(tLDFinBankSet.size()==0)
          throw new Exception("该本方银行帐户在ldfinbank表中没有对应数据！");
      if(tLDFinBankSet.size()==1){
          return tLDFinBankSchemat.getManageCom();
      }
      if(tLDFinBankSet.size()>1){
          System.out.println("actugetno:"+actugetno);
          String qljagetclaim="select * from Ljagetclaim where actugetno='"+actugetno+"' with ur";        
              
              String ljagetclaimoutString=new String();
              LJAGetClaimDB tLJAGetClaimDB=new LJAGetClaimDB();
              LJAGetClaimSet tLJAGetClaimSet=tLJAGetClaimDB.executeQuery(qljagetclaim);
              LJAGetEndorseSet tLJAGetEndorseSet=new LJAGetEndorseSet();
              LJSGetDrawSet tLJSGetDrawSet=new LJSGetDrawSet();
              if(tLJAGetClaimSet.size()==0){
                    String qljagetendorse="select * from Ljagetendorse where actugetno='"+actugetno+"' with ur";     
                    LJAGetEndorseDB tLJAGetEndoeseDB=new LJAGetEndorseDB();
                     tLJAGetEndorseSet=tLJAGetEndoeseDB.executeQuery(qljagetendorse);
                    if(tLJAGetEndorseSet.size()==0){
                        String qljagetdraw="select * from ljsgetdraw where getnoticeno='"+actugetno+"' with ur";     
                       LJSGetDrawDB tLJSGetDrawDB=new LJSGetDrawDB();
                       tLJSGetDrawSet=tLJSGetDrawDB.executeQuery(qljagetdraw);
                       if(tLJSGetDrawSet.size()==0)
                           return "000";
                       //throw new Exception("该实付号在Ljagetclaim表或Ljagetendorse表中没有对应数据！");
                    }
              }
              if(tLJAGetClaimSet.size()>=1){
                   ljagetclaimoutString=tLJAGetClaimSet.get(1).getManageCom();
                      System.out.println("查询Ljagetclaim中的managecom:"+ljagetclaimoutString);
              } if(tLJAGetEndorseSet.size()>=1){
                  ljagetclaimoutString=tLJAGetEndorseSet.get(1).getManageCom();
                  System.out.println("查询Ljagetendorse中的managecom:"+ljagetclaimoutString);
              }else if(tLJSGetDrawSet.size()>=1){
                  ljagetclaimoutString=tLJSGetDrawSet.get(1).getManageCom();
                  System.out.println("查询LJSGetDraw中的managecom:"+ljagetclaimoutString);
              }
              
              String sqlStr2="select * from LDFinBank where bankcode='"+getbankcode+"' and bankaccno='"+getbankaccno+"' and finflag='F' and managecom='"+ljagetclaimoutString+"' order by managecom with ur";
              System.out.println("查询ldfinbank中的managecom2:"+sqlStr2);
              tLDFinBankSet = tLDFinBankDB.executeQuery(sqlStr2);
              
              LDFinBankSchema tLDFinBankSchema=tLDFinBankSet.get(1);
              outString=tLDFinBankSchema.getManageCom();
      }
      return outString;
  }
 public String getbankaccnomain(LJAGetSchema mLJAGetSchema) throws Exception{
	 String mybankaccno="";
	 String mybankcode=mLJAGetSchema.getBankCode().substring(0, 2);
	 System.out.println("银行编码:"+mybankcode);
	 String mysql="select * from ldfinbank where finflag='F' and bankcode='"+mybankcode+"' and managecom='"+mLJAGetSchema.getManageCom()+"' with ur";
	 System.out.println("本方银行帐号查询sql:"+mysql);
	  LDFinBankDB tLDFinBankDB=new LDFinBankDB();
      LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(mysql);
      if(tLDFinBankSet.size()==0){
    	  throw new Exception("该本方银行帐户在ldfinbank表中没有对应数据！");
      }
      LDFinBankSchema tLDFinBankSchemat=tLDFinBankSet.get(1);
      mybankaccno=tLDFinBankSchemat.getBankAccNo();
      System.out.println("996行:"+mybankaccno);
	 return mybankaccno;
 }
  public static void main(String[] args) {
    PayReturnFromBankBL payReturnFromBankBL1 = new PayReturnFromBankBL();
  
  }
}
