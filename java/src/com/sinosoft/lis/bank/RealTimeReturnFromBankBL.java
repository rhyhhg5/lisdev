package com.sinosoft.lis.bank;

import java.io.IOException;

import org.xml.sax.SAXException;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.finfee.AfterTempFeeBL;
import com.sinosoft.lis.pubfun.*;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.taskservice.LLClaimFinishTask;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class RealTimeReturnFromBankBL
{

    /** 提交数据的容器 */
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    private LJSPaySet outLJSPaySet = new LJSPaySet();

    private LYSendToBankSet outDelLYSendToBankSet = new LYSendToBankSet();

    private LJSPayBSet outLJSPayBSet = new LJSPayBSet();

    private LJSPayPersonBSet outLJSPayPersonBSet = new LJSPayPersonBSet();

    private LJTempFeeClassSet outUpdateLJTempFeeClassSet = new LJTempFeeClassSet();

    private LJTempFeeSet outUpdateLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeSet mTempFeeSetForAfterDeal = new LJTempFeeSet();

    private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();

    private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();

    private LJSPaySet outDelLJSPaySet = new LJSPaySet();

    private LYDupPaySet outLYDupPaySet = new LYDupPaySet();

    private Reflections mReflections = new Reflections();

    private LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();

    private LYBankLogSchema outLYBankLogSchema = new LYBankLogSchema();

    private String path = "";

    private String uBankCode = "";

    private LJFIGetSet outLJFIGetSet = new LJFIGetSet();

    private LJAGetSet outLJAGetSet = new LJAGetSet();

    private LYDupGetSet outLYDupGetSet = new LYDupGetSet();

    private LJAGetDrawSet outLJAGetDrawSet = new LJAGetDrawSet();

    private LJAGetEndorseSet outLJAGetEndorseSet = new LJAGetEndorseSet();

    private LJAGetTempFeeSet outLJAGetTempFeeSet = new LJAGetTempFeeSet();

    // 理赔批案表
    private LLRegisterSet mLLRegisterSet = new LLRegisterSet();

    private LJAGetClaimSet outLJAGetClaimSet = new LJAGetClaimSet();

    // 理赔案件表
    private LLCaseSet mLLCaseSet = new LLCaseSet();

    private LJAGetOtherSet outLJAGetOtherSet = new LJAGetOtherSet();

    private LJABonusGetSet outLJABonusGetSet = new LJABonusGetSet();

    private LYBankLogSet outLYBankLogSet = new LYBankLogSet();

    public RealTimeReturnFromBankBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * 
     * @param cInputData
     *            传入的数据,Document对象
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(String path, String uBankCode)
    {
        this.path = path;
        this.uBankCode = uBankCode;
        // 进行业务处理
        if (!dealData())
            return false;
        System.out.println("---End dealData---");

        //如果类型为 代收
        if ("R".equals(tLYReturnFromBankSet.get(1).getDealType()))
        {

            // 准备往后台的数据
            if (!prepareOutputData())
                return false;

            System.out.println("---End prepareOutputData---");
            System.out.println("Start GetReturnFromBank BLS Submit...");

            BatchGetReturnBLS tBatchGetReturnBLS = new BatchGetReturnBLS();
            if (tBatchGetReturnBLS.submitData(mInputData) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tBatchGetReturnBLS.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("End GetReturnFromBank BLS Submit...");

            // 如果有需要处理的错误，则返回
            if (tBatchGetReturnBLS.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tBatchGetReturnBLS.mErrors);
            }
            
            //理赔预付回收核销 2012-1-30
            if (!claimFinish())  
            {
                //return false;
            }
            
            if (!afterSubmit())
            {
                //return false;  //去掉是为保证日志表不会更新为程序处理异常
            }

        }

        return true;
    }

    /**
     * 处理财务处理结束后的操作。
     * 本操作在支票确认和银行转帐回盘时都会调用，
     * 请注意实现AfterTempFeeBL接口时要保障实现后的类在这两个地方都能使用
     * @return boolean
     */
    private boolean afterSubmit()
    {
        String tempFeeType = null;
        MMap map = new MMap();

        //注意，一条LJSPay可能对应多个LJTempFee纪录，所以进行处理时要注意相关性
        for (int i = 1; i <= mTempFeeSetForAfterDeal.size(); i++)
        {
            try
            {
                LJTempFeeSchema tLJTempFeeSchema = mTempFeeSetForAfterDeal.get(
                        i).getSchema();
                //按TempFeeType编码调用不同的类
                tempFeeType = tLJTempFeeSchema.getTempFeeType();

                String className = "com.sinosoft.lis.finfee.AfterTempFee"
                        + tempFeeType + "BL";
                Class confirmClass = Class.forName(className);
                AfterTempFeeBL tAfterTempFeeBL = (AfterTempFeeBL) confirmClass
                        .newInstance();
                VData data = new VData();

                GlobalInput mGlobalInput = new GlobalInput();
                mGlobalInput.Operator = "sys";
                mGlobalInput.ComCode = "86";
                mGlobalInput.ManageCom = "86";
                data.add(mGlobalInput);
                data.add(tLJTempFeeSchema);

                MMap tMMap = tAfterTempFeeBL.getSubmitMMap(data, "");
                if (tMMap == null)
                {
                    mErrors.copyAllErrors(AfterTempFeeBL.mErrors);
                    //return false;
                    continue;
                }
                map.add(tMMap);
            }
            catch (ClassNotFoundException ex)
            {
                System.out.println(tempFeeType + "暂收后处理类!");
            }
            catch (NullPointerException ex)
            {
                ex.printStackTrace();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                //return false;
            }
        }

        if (map == null || map.size() == 0)
        {
            return true;
        }

        VData data = new VData();
        data.add(map);
        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "BatchReturnFromBankBL";
            tError.functionName = "afterSubmit";
            tError.errorMessage = "收费成功，但后续处理失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    
    private boolean claimFinish()
    {
        for (int i = 1; i <= mTempFeeSetForAfterDeal.size(); i++)
        {
            if (!mTempFeeSetForAfterDeal.get(i).getTempFeeType().equals("Y") && !mTempFeeSetForAfterDeal.get(i).getTempFeeType().equals("9"))
            {
                continue;
            }
            try
            {
                LLClaimFinishTask tLLClaimFinishTask = new LLClaimFinishTask(
                        "86");
                tLLClaimFinishTask.run();
            }
            catch (Exception ex)
            {
                CError tError = new CError();
                tError.moduleName = "BatchReturnFromBankBL";
                tError.functionName = "claimFinish";
                tError.errorMessage = "收费成功，但理赔预付回收核销失败";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                ex.printStackTrace();
                return false;
            }
        }
        return true;
    }

    /**a
     * 根据前面的输入数据，进行逻辑处理
     * 
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        RealTimeReadFile tBatchReadFile = new RealTimeReadFile();

        outLJFIGetSet.clear();
        outLJAGetSet.clear();
        tLYReturnFromBankSet.clear();
        outLJTempFeeSet.clear();
        outLJTempFeeClassSet.clear();

        //将数据存入数据库
        tLYReturnFromBankSet = tBatchReadFile.xmlToDatabase(path, uBankCode);
        if (tLYReturnFromBankSet.size() == 0)
        {
            System.out.println("tLYReturnFromBankSet为空");
            return false;
        }

        String sql = "select distinct 1 from lyreturnfrombank where serialno= '"
                + tLYReturnFromBankSet.get(1).getSerialNo() + "' with ur";
        System.out.println(sql);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow() != 0)
        {
            System.out.println(tLYReturnFromBankSet.get(1).getSerialNo()
                    + "批次已回盘");
            return false;
        }

        if ("R".equals(tLYReturnFromBankSet.get(1).getDealType()))
        {

            try
            {
                //生成银行日志数据
                outLYBankLogSchema.setSchema(getLYBankLog());

                //校验银行扣款成功与否
                verifyBankSucc();
                System.out.println("---End verifyBankSucc---");

                //校验每笔金额，比较LJSPay中的金额
                verifyUnitMoney();
                System.out.println("---End verifyUnitMoney---");

                //校验应收表
                if (!verifyLJSPay())
                {
                    return false;
                }
                System.out.println("---End verifyLJSPay---");

                //获取发送盘数据，用于删除
                getLYSendToBank();
                System.out.println("---End getLYSendToBank---");

                //获取应收表数据，修改银行在途标志
                getLJSPay();
                System.out.println("---End getLJSPay---");

                //记录日志，设置总金额、总数量、处理状态
                outLYBankLogSet = getLYBankLog1();

            }
            catch (Exception e)
            {
                //@@错误处理
                e.printStackTrace();
                CError.buildErr(this, "数据处理错误:" + e.getMessage());
                return false;
            }
            System.out.println("---End verifyUnitMoney---");
        }

        else if ("F".equals(tLYReturnFromBankSet.get(1).getDealType()))
        {

            try
            {
                //生成银行日志数据
                outLYBankLogSchema.setSchema(getLYBankLog());

                //校验银行付款成功与否
                verifyPayBankSucc();
                System.out.println("---End verifyBankSucc---");

                //校验每笔金额
                verifyPayUnitMoney();
                System.out.println("---End verifyUnitMoney---");

                //校验实付表，转入财务付费表
                if (!verifyLJAGet())
                {
                    return false;
                }
                System.out.println("---End verifyLJAGet---");

                //获取发送盘数据，用于删除
                getLYSendToBank();
                System.out.println("---End getLYSendToBank---");

                //获取实付表数据，修改银行在途标志、财务到帐日期、财务确认日期

                if (!getLJAGet())
                {
                    return false;
                }
                System.out.println("---End getLJAGet---");

            }
            catch (Exception e)
            {
                //@@错误处理
                System.out.println(e.getMessage());
                CError.buildErr(this, "数据处理错误:" + e.getMessage());
                return false;
            }
            //记录日志
            outLYBankLogSet = getPayLYBankLog();
            if (outLYBankLogSet.size() == 0)
                throw new NullPointerException("无银行日志数据！");
            System.out.println("---End verifyUnitMoney---");
        }
        return true;
    }

    /**
     * 生成银行日志数据
     * 
     * @param tLYSendToBankSchema
     * @return
     */
    private LYBankLogSet getLYBankLog1() throws Exception
    {

        LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
        LYBankLogSet tLYBankLogSet = new LYBankLogSet();

        //获取日志记录
        tLYBankLogSchema.setSerialNo(tLYReturnFromBankSet.get(1).getSerialNo());
        tLYBankLogSet.set(tLYBankLogSchema.getDB().query());

        if (tLYBankLogSet.size() > 0)
        {
            tLYBankLogSchema.setSchema(tLYBankLogSet.get(1));

            //修改日志
            tLYBankLogSchema.setAccTotalMoney(tLYBankLogSchema
                    .getAccTotalMoney()); // 财务确认总金额
            tLYBankLogSchema.setBankSuccMoney(tLYBankLogSchema
                    .getBankSuccMoney()); // 银行成功总金额
            tLYBankLogSchema.setBankSuccNum(tLYBankLogSchema.getBankSuccNum()
                    + ""); // 银行成功总数量
            tLYBankLogSchema.setDealState("1"); // 处理状态
            tLYBankLogSchema.setInFile("回盘成功#" + this.path);
            tLYBankLogSchema.setTransOperator("sys");
            tLYBankLogSchema.setReturnOperator("sys");
            tLYBankLogSchema.setReturnDate(PubFun.getCurrentDate());
            //tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());

            String ComCode = tLYBankLogSchema.getComCode();
            System.out.println("575行 管理机构为  " + ComCode);
            int len = ComCode.length();
            for (int i = 0; i < 8 - len; i++)
            {
                ComCode = ComCode + "0";
            }
            tLYBankLogSchema.setComCode(ComCode);

            tLYBankLogSchema.setDealState("1"); //处理状态
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());

            tLYBankLogSet.clear();
            tLYBankLogSet.add(tLYBankLogSchema);
        }

        return tLYBankLogSet;
    }

    /**
     * 校验每笔金额
     */
    private void verifyPayUnitMoney()
    {

        for (int i = 0; i < tLYReturnFromBankSet.size(); i++)
        {
            LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet
                    .get(i + 1);
            //此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
            if (tLYReturnFromBankSchema.getConvertFlag() != null)
                continue;

            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            tLJAGetSchema.setActuGetNo(tLYReturnFromBankSchema.getPayCode());
            tLJAGetSchema.setSchema(tLJAGetSchema.getDB().query().get(1));
            //扣款小于应收表中的信息，则转入重复交费表（设置转换标记为2），否则通过
            if (tLJAGetSchema.getSumGetMoney() != tLYReturnFromBankSchema
                    .getPayMoney())
            {
                setLYDupGet(tLYReturnFromBankSchema);
                tLYReturnFromBankSchema.setConvertFlag("2");
            }
        }
    }

    /**
     * 校验实付表
     */
    private boolean verifyLJAGet()
    {
        int i;

        for (i = 0; i < tLYReturnFromBankSet.size(); i++)
        {
            //遍历每一条返回盘记录
            LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet
                    .get(i + 1);

            if (tLYReturnFromBankSchema.getConvertFlag() != null)
                continue;

            //转入财务付费表
            try
            {
                if (!setLJFIGet(tLYReturnFromBankSchema))
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * 转入财务付费表
     * @param tLYReturnFromBankSchema
     */
    private boolean setLJFIGet(LYReturnFromBankSchema tLYReturnFromBankSchema)
            throws Exception
    {

        LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
        LJAGetSchema tLJAGetSchema = getLJAGetByLYReturnFromBank(tLYReturnFromBankSchema);
        if (tLJAGetSchema == null)
        {
            mErrors.addOneError("获取应付数据失败！");
            return false;
        }
        tLJFIGetSchema.setActuGetNo(tLYReturnFromBankSchema.getPayCode());
        tLJFIGetSchema.setPayMode("4");
        LJAGetSchema tempLJAGetSchema = new LJAGetSchema();
        tempLJAGetSchema.setActuGetNo(tLYReturnFromBankSchema.getPayCode());
        tempLJAGetSchema.setSchema(tLJAGetSchema.getDB().query().get(1));
        tLJFIGetSchema.setOtherNoType(tempLJAGetSchema.getOtherNoType());
        tLJFIGetSchema.setOtherNo(tempLJAGetSchema.getOtherNo());

        tLJFIGetSchema.setAgentCode(tLYReturnFromBankSchema.getAgentCode());

        String comecode = tLYReturnFromBankSchema.getComCode();
        for (int i = comecode.length(); i < 8; i++)
        {
            comecode = comecode + "0";
        }
        // String bankCode = getFinBankCode(tLYReturnFromBankSchema.getBankCode());
        String sql = "select * from ldfinbank where finbankcode='7705' and managecom = '86000000' and finflag = 'F'";
        LDFinBankDB tLDFinBankDB = new LDFinBankDB();
        LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(sql);

        tLJFIGetSchema.setManageCom(tLDFinBankSet.get(1).getManageCom());

        tLJFIGetSchema.setPolicyCom(comecode);
        tLJFIGetSchema.setBankAccNo(tLYReturnFromBankSchema.getAccNo());
        tLJFIGetSchema.setGetMoney(tLYReturnFromBankSchema.getPayMoney());
        tLJFIGetSchema.setEnterAccDate(tLYReturnFromBankSchema.getModifyDate());
        tLJFIGetSchema.setAccName(tLYReturnFromBankSchema.getAccName());
        tLJFIGetSchema.setConfDate(PubFun.getCurrentDate());
        tLJFIGetSchema.setConfMakeTime(PubFun.getCurrentTime());
        tLJFIGetSchema.setShouldDate(PubFun.getCurrentDate());
        tLJFIGetSchema.setOperator("sys");
        tLJFIGetSchema.setMakeDate(PubFun.getCurrentDate());
        tLJFIGetSchema.setMakeTime(PubFun.getCurrentTime());
        tLJFIGetSchema.setModifyDate(PubFun.getCurrentDate());
        tLJFIGetSchema.setModifyTime(PubFun.getCurrentTime());

        tLJFIGetSchema.setBankCode(tLYReturnFromBankSchema.getBankCode());
        tLJFIGetSchema.setConfMakeDate(PubFun.getCurrentDate());
        String tSerialNo = PubFun1.CreateMaxNo("ReturnFromBank", 20);
        tLJFIGetSchema.setSerialNo(tSerialNo);
        outLJFIGetSet.add(tLJFIGetSchema);
        return true;
    }

    /**
     * 获取发送盘数据，用于删除
     */
    private void getLYSendToBank()
    {
        LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();

        tLYSendToBankSchema.setSerialNo(tLYReturnFromBankSet.get(1)
                .getSerialNo());

        System.out.println("删除发盘文件 ：  "
                + tLYReturnFromBankSet.get(1).getSerialNo());

        outDelLYSendToBankSet = tLYSendToBankSchema.getDB().query();
    }

    /**
     * 获取应收表数据，修改银行在途标志
     */
    private void getLJSPay()
    {

        int i;

        for (i = 0; i < tLYReturnFromBankSet.size(); i++)
        {
            LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet
                    .get(i + 1);

            String convertFlag = tLYReturnFromBankSchema.getConvertFlag();
            if ((convertFlag != null) && (convertFlag.equals("1")))
            {
                LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);
                // 总应收表无数据，表示扣款失败，并且已经用其它方式交费
                if (tLJSPaySchema == null)
                {
                    continue;
                }

                tLJSPaySchema.setBankOnTheWayFlag("0");
                tLJSPaySchema.setCanSendBank("1");

                tLJSPaySchema.setApproveDate(PubFun.getCurrentDate());
                tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
                tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
                outLJSPaySet.add(tLJSPaySchema);
            }
            else if (StrTool.cTrim(tLYReturnFromBankSchema.getPayType())
                    .equals("R"))
            {
                LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);
                // 总应收表无数据，表示扣款失败，并且已经用其它方式交费
                if (tLJSPaySchema == null)
                {
                    continue;
                }
                // 扣款成功
                tLJSPaySchema.setBankOnTheWayFlag("3");
                tLJSPaySchema.setApproveDate(PubFun.getCurrentDate());
                tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
                tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
                outLJSPaySet.add(tLJSPaySchema);
            }
        }
    }

    /**
     * 校验应收表
     */
    private boolean verifyLJSPay()
    {
        try
        {

            for (int i = 0; i < tLYReturnFromBankSet.size(); i++)
            {
                System.out.println("得到 DoType 类型 是 ："
                        + tLYReturnFromBankSet.get(1).getDoType());

                // 遍历每一条返回盘记录
                LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet
                        .get(i + 1);

                // 此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
                if (tLYReturnFromBankSchema.getConvertFlag() != null)
                    continue;
                System.out.println("校验应收表开始。。。。。。。");
                // 校验是否首期直接交费
                if (tLYReturnFromBankSchema.getDoType().equals("2"))
                {
                    // 记录暂交费表，和暂交费分类表
                    setLJTempFee(tLYReturnFromBankSchema);
                    tLYReturnFromBankSchema.setConvertFlag("5");
                    continue;
                }
                // 属于应收表中的催收交费
                if (tLYReturnFromBankSchema.getDoType().equals("1"))
                {
                    // 查询应收表
                    System.out.println("校验催收交费。。。。。。");
                    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
                    tLJSPaySchema.setGetNoticeNo(tLYReturnFromBankSchema
                            .getPayCode());
                    LJSPaySet tLJSPaySet = tLJSPaySchema.getDB().query();
                    System.out.println("745hang::"
                            + tLJSPaySet.get(1).getOtherNoType());

                    // 应收表不存在，表示已经交费核销，记录入重复交费记录表
                    if (tLJSPaySet.size() == 0)
                    {
                        setLYDupPay(tLYReturnFromBankSchema);
                        tLYReturnFromBankSchema.setConvertFlag("3");

                        continue;
                    }
                    System.out.println("===="
                            + tLJSPaySet.get(1).getOtherNoType());
                    // 应收表记录存在
                    if (tLJSPaySet.size() > 0)
                    {
                        // GetNoticeNo是LJSPay表主键，所以应该只查询出一条
                        tLJSPaySchema = tLJSPaySet.get(1);
                        //首期事后交费
                        if (tLJSPaySet.get(1).getOtherNoType().equals("6"))
                        {

                            outDelLJSPaySet.add(tLJSPaySchema);
                            tLYReturnFromBankSchema.setConvertFlag("6");
                        }

                        //事后选银行转账
                        if (tLJSPaySet.get(1).getOtherNoType().equals("9")
                                || tLJSPaySet.get(1).getOtherNoType().equals(
                                        "16"))
                        {

                            outDelLJSPaySet.add(tLJSPaySchema);

                            tLYReturnFromBankSchema.setConvertFlag("9");

                            //更新暂交费表
                            updateLJTempFee(tLYReturnFromBankSchema);
                        }

                        //保全
                        if (tLJSPaySet.get(1).getOtherNoType().equals("10")
                                || tLYReturnFromBankSchema.getNoType().equals(
                                        "3"))
                        {

                            outDelLJSPaySet.add(tLJSPaySchema);
                            tLYReturnFromBankSchema.setConvertFlag("9");

                            //更新暂交费表
                            updateLJTempFee(tLYReturnFromBankSchema);
                        }

                        //理赔预付回收
                        if (tLJSPaySet.get(1).getOtherNoType().equals("Y")
                                || tLYReturnFromBankSchema.getNoType().equals(
                                        "Y"))
                        {
                            tLYReturnFromBankSchema.setConvertFlag("9");
                            //outDelLJSPaySet.add(tLJSPaySchema);  //删了后续核销不了了
                            //更新暂交费表
                            updateLJTempFee(tLYReturnFromBankSchema);
                        }
                        
                        //续期催收
                        if (tLJSPaySet.get(1).getOtherNoType().equals("2"))
                        {

                            tLYReturnFromBankSchema.setConvertFlag("7");

                            //生成续期划帐成功通知书
                            //                          setLOPRTManager(tLYReturnFromBankSchema, "48");
                            setLJSPayBState(tLJSPaySchema.getSchema());
                        }

                        //正常催收
                        if (tLJSPaySet.get(1).getOtherNoType().equals("2")
                                || tLJSPaySet.get(1).getOtherNoType().equals(
                                        "6"))
                        {
                            //校验暂交费表，有数据表示已经在柜台交费，转入重复交费表
                            if (verifyLJTempFee(tLYReturnFromBankSchema))
                            {
                                setLYDupPay(tLYReturnFromBankSchema);
                                tLYReturnFromBankSchema.setConvertFlag("4");
                            }
                            //记录暂交费表
                            else
                            {
                                System.out.println("812行::"
                                        + tLYReturnFromBankSchema.getPolNo());
                                System.out.println("记录暂交费表！！！");
                                if (!setLJTempFeeCont(tLYReturnFromBankSchema))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("银行返回盘数据校验失败！(verifyLJSPay) ");
            mErrors.addOneError("解析回盘数据出错。");
            return false;
        }
        return true;
    }

    private boolean setLJTempFeeCont(
            LYReturnFromBankSchema tLYReturnFromBankSchema) throws Exception
    {

        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();

        LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);

        System.out.println("----得到LJSPAY 的OTHERNOTYPE  ： "
                + tLJSPaySchema.getOtherNoType());

        String tLimit = PubFun.getNoLimit(tLJSPaySchema.getManageCom());

        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

        if (tLJSPaySchema.getOtherNoType().equals("1")
                || tLJSPaySchema.getOtherNoType().equals("5")) // 集体保单号
        {

            LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
            LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();

            tLJSPayGrpDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());

            tLJSPayGrpSet = tLJSPayGrpDB.query();
            for (int i = 1; i <= tLJSPayGrpSet.size(); i++)
            {

                LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();

                tLJSPayGrpSchema = tLJSPayGrpSet.get(i);
                tLJTempFeeSchema.setTempFeeNo(tLYReturnFromBankSchema
                        .getPayCode());
                tLJTempFeeSchema.setOtherNo(tLJSPayGrpSchema.getGrpPolNo());
                // 首期直接交费
                if (tLYReturnFromBankSchema.getDoType().equals("2"))
                {
                    tLJTempFeeSchema.setTempFeeType("18");
                    tLJTempFeeSchema.setOtherNoType("0");
                }
                else if (tLYReturnFromBankSchema.getDoType().equals("1"))
                {
                    // 首期事后交费

                    if (tLYReturnFromBankSchema.getNoType().equals("6"))
                    {
                        tLJTempFeeSchema.setTempFeeType("18");
                        tLJTempFeeSchema.setOtherNoType("0");
                    }
                    // 正常续期催收
                    else if (tLYReturnFromBankSchema.getNoType().equals("2"))
                    {

                        tLJTempFeeSchema.setTempFeeType("18");
                        tLJTempFeeSchema.setOtherNoType("1");
                        tLJTempFeeSchema.setOtherNo(tLJSPayGrpSchema
                                .getGrpContNo());
                    }

                    else if (tLYReturnFromBankSchema.getNoType().equals("9"))
                    {
                        tLJTempFeeSchema.setTempFeeType("9");
                    }
                    else if (tLYReturnFromBankSchema.getNoType().equals("10"))
                    {
                        tLJTempFeeSchema.setTempFeeType("10");
                    }
                }

                tLJTempFeeSchema.setRiskCode("000000");
                tLJTempFeeSchema.setPayIntv(tLJSPayGrpSchema.getPayIntv());
                tLJTempFeeSchema.setPayMoney(tLJSPayGrpSchema
                        .getSumDuePayMoney());
                tLJTempFeeSchema.setPayDate(tLYReturnFromBankSchema
                        .getSendDate());
                tLJTempFeeSchema.setEnterAccDate(tLYReturnFromBankSchema
                        .getBankDealDate());
                String com = tLYReturnFromBankSchema.getComCode();
                for (int j = com.length(); j < 8; j++)
                {
                    com = com + "0";
                }
                tLJTempFeeSchema.setManageCom(com);
                tLJTempFeeSchema.setPolicyCom(com);
                tLJTempFeeSchema
                        .setAgentGroup(tLJSPayGrpSchema.getAgentGroup());
                tLJTempFeeSchema.setAgentCode(tLJSPayGrpSchema.getAgentCode());
                tLJTempFeeSchema.setConfFlag("0");
                //serNo = "B"+serNo;
                tLJTempFeeSchema.setSerialNo(serNo);
                tLJTempFeeSchema.setOperator("sys");
                tLJTempFeeSchema.setCashier("sys");
                tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
                outLJTempFeeSet.add(tLJTempFeeSchema);
            }
        }
        else if (tLJSPaySchema.getOtherNoType().equals("2")
                || tLJSPaySchema.getOtherNoType().equals("6")) // 个人保单号
        {
            System.out.println("----------> 进来了！！！");
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = new SSRS();
            String tSql = "select riskcode,polno,sum(SumDuePayMoney),contno from LJSPayPerson where GetNoticeNo='"
                    + tLJSPaySchema.getGetNoticeNo()
                    + "' group by riskcode,polno,contno with ur";
            tSSRS = tExeSQL.execSQL(tSql);

            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {

                System.out.println("  <----->   ********    <----->");
                LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();

                LJTempFeeSchema tNewLJTempFeeSchema = new LJTempFeeSchema();

                tLJTempFeeSchema.setTempFeeNo(tLYReturnFromBankSchema
                        .getPayCode());
                System.out
                        .println("---------->  tLYReturnFromBankSchema.getPayCode"
                                + tLYReturnFromBankSchema.getPayCode());
                tLJTempFeeSchema.setOtherNo(tSSRS.GetText(i, 2));
                System.out.println("----------> 925行 ：" + tSSRS.GetText(i, 2));

                tLJTempFeeSchema.setRiskCode("000000");
                // 首期直接交费
                if (tLYReturnFromBankSchema.getDoType().equals("2"))
                {

                    tLJTempFeeSchema.setTempFeeType("1");
                    tLJTempFeeSchema.setOtherNoType("0");
                }
                else if (tLYReturnFromBankSchema.getDoType().equals("1"))
                {
                    // 首期事后交费
                    if (tLJSPaySchema.getOtherNoType().equals("6"))
                    {
                        System.out.println("----------> 首期事后缴费 ："
                                + tLJSPaySchema.getOtherNoType());
                        tLJTempFeeSchema.setTempFeeType("1");
                        tLJTempFeeSchema.setOtherNoType("0");
                    }
                    // 正常续期催收
                    else if (tLJSPaySchema.getOtherNoType().equals("2"))
                    {
                        System.out.println("----------> 正常续期催收 ："
                                + tLJSPaySchema.getOtherNoType());
                        tLJTempFeeSchema.setTempFeeType("2");
                        tLJTempFeeSchema.setOtherNoType("0");
                        tLJTempFeeSchema.setOtherNo(tSSRS.GetText(i, 4));
                        tLJTempFeeSchema.setRiskCode(tSSRS.GetText(i, 1));
                    }
                    else if (tLJSPaySchema.getOtherNoType().equals("2"))
                    {
                        System.out.println("---------->===");
                        tLJTempFeeSchema.setTempFeeType("2");
                        tLJTempFeeSchema.setOtherNoType("0");
                        tLJTempFeeSchema.setOtherNo(tSSRS.GetText(i, 4));
                        tLJTempFeeSchema.setRiskCode(tSSRS.GetText(i, 1));
                    }
                    else if (tLJSPaySchema.getOtherNoType().equals("10"))
                    {
                        System.out.println("----------> &&&&&&");
                        tLJTempFeeSchema.setTempFeeType("10");
                        tLJTempFeeSchema.setOtherNoType("0");
                    }
                }

                System.out.println("相应数据的插入。。。");
                LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
                LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
                tLJSPayPersonDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
                System.out.println("----------> 961行 ："
                        + tLJSPaySchema.getGetNoticeNo());
                tLJSPayPersonDB.setPolNo(tSSRS.GetText(i, 2));
                System.out.println("----------> 963行 ：" + tSSRS.GetText(i, 2));
                tLJSPayPersonSet = tLJSPayPersonDB.query();
                LJSPayPersonSchema tLJSPayPersonSchema = tLJSPayPersonSet
                        .get(1);
                //              tLJTempFeeSchema.setRiskCode(tSSRS.GetText(i, 1));
                tLJTempFeeSchema.setPayMoney(tSSRS.GetText(i, 3));
                tLJTempFeeSchema.setPayDate(tLYReturnFromBankSchema
                        .getSendDate());
                tLJTempFeeSchema.setEnterAccDate(tLYReturnFromBankSchema
                        .getBankDealDate());
                tLJTempFeeSchema.setPayIntv(tLJSPayPersonSchema.getPayIntv());

                //String bankCode = getFinBankCode(tLYReturnFromBankSchema
                //.getBankCode());
                String sql = "select * from ldfinbank where finbankcode='7705' and finflag = 'S' and managecom = '86000000'";
                LDFinBankDB tLDFinBankDB = new LDFinBankDB();
                LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(sql);

                tLJTempFeeSchema.setManageCom(tLDFinBankSet.get(1)
                        .getManageCom());

                tLJTempFeeSchema.setPolicyCom(tLYReturnFromBankSchema
                        .getComCode());
                tLJTempFeeSchema.setAgentGroup(tLJSPayPersonSchema
                        .getAgentGroup());
                tLJTempFeeSchema.setAgentCode(tLJSPayPersonSchema
                        .getAgentCode());
                tLJTempFeeSchema.setConfFlag("0");
                tLJTempFeeSchema.setSerialNo(serNo);
                tLJTempFeeSchema.setOperator("sys");
                tLJTempFeeSchema.setCashier("sys");
                tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());

                if (outLJTempFeeSet.size() > 0
                        && tLJTempFeeSchema.getTempFeeType().equals("2"))
                {

                    for (int n = 1; n <= outLJTempFeeSet.size(); n++)
                    {

                        if (StrTool.cTrim(outLJTempFeeSet.get(n).getRiskCode())
                                .equals(tLJTempFeeSchema.getRiskCode())

                                && StrTool
                                        .cTrim(
                                                outLJTempFeeSet.get(n)
                                                        .getTempFeeType())
                                        .equals(
                                                tLJTempFeeSchema
                                                        .getTempFeeType())

                                && StrTool
                                        .cTrim(
                                                outLJTempFeeSet.get(n)
                                                        .getTempFeeNo())
                                        .equals(tLJTempFeeSchema.getTempFeeNo()))
                        {

                            tNewLJTempFeeSchema = outLJTempFeeSet.get(n);

                            double Paymoney = tNewLJTempFeeSchema.getPayMoney()
                                    + tLJTempFeeSchema.getPayMoney();

                            System.out.println("Paymoney" + Paymoney);

                            outLJTempFeeSet.remove(tNewLJTempFeeSchema);

                            tLJTempFeeSchema.setRiskCode(tSSRS.GetText(i, 1));
                            tLJTempFeeSchema.setOtherNo(tSSRS.GetText(i, 4));

                            if (tLJSPayPersonSchema.getGrpContNo().equals(
                                    "00000000000000000000"))
                            {

                                tLJTempFeeSchema.setOtherNoType("0");

                            }
                            else
                            {

                                tLJTempFeeSchema.setOtherNoType("1");

                            }

                            tLJTempFeeSchema.setPayMoney(Paymoney);
                        }
                    }
                }
                mTempFeeSetForAfterDeal.add(tLJTempFeeSchema);
                outLJTempFeeSet.add(tLJTempFeeSchema);
            }
        }
        // 为暂交费分类表设置数据
        tLJTempFeeClassSchema
                .setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
        tLJTempFeeClassSchema.setPayMode("4");
        tLJTempFeeClassSchema
                .setPayMoney(tLYReturnFromBankSchema.getPayMoney());
        tLJTempFeeClassSchema.setPayDate(tLYReturnFromBankSchema.getSendDate());
        tLJTempFeeClassSchema.setEnterAccDate(tLYReturnFromBankSchema
                .getBankDealDate());
        tLJTempFeeClassSchema.setConfFlag("0");
        tLJTempFeeClassSchema.setSerialNo(serNo);
        tLJTempFeeClassSchema.setOperator("sys");
        tLJTempFeeClassSchema.setCashier("sys");
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());

        //String bankCode = getFinBankCode(tLYReturnFromBankSchema.getBankCode());
        //设置本方银行账号
        String sql = "select * from ldfinbank where finbankcode='7705' and Managecom = '86000000' and finflag = 'S'";
        LDFinBankDB tLDFinBankDB = new LDFinBankDB();
        LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(sql);
        if (tLDFinBankSet.size() == 0)
        {
            mErrors.addOneError("本方银行未配置，请先配置再进行处理");
            return false;
        }
        String InsBankAccNo = tLDFinBankSet.get(1).getBankAccNo();
        String bankcode = tLDFinBankSet.get(1).getBankCode();

        tLJTempFeeClassSchema.setInsBankAccNo(InsBankAccNo);
        tLJTempFeeClassSchema.setInsBankCode(bankcode);

        tLJTempFeeClassSchema.setManageCom(tLDFinBankSet.get(1).getManageCom());
        tLJTempFeeClassSchema
                .setPolicyCom(tLYReturnFromBankSchema.getComCode());
        tLJTempFeeClassSchema
                .setBankCode(tLYReturnFromBankSchema.getBankCode());
        tLJTempFeeClassSchema.setBankAccNo(tLYReturnFromBankSchema.getAccNo());
        tLJTempFeeClassSchema.setAccName(tLYReturnFromBankSchema.getAccName());
        tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
        outLJTempFeeClassSet.add(tLJTempFeeClassSchema);

        return true;
    }

    /**
     * 校验暂交费表
     * 
     * @param tLYReturnFromBankSchema
     * @return
     */
    private boolean verifyLJTempFee(
            LYReturnFromBankSchema tLYReturnFromBankSchema)
    {
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();

        tLJTempFeeSchema.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
        LJTempFeeSet tLJTempFeeSet = tLJTempFeeSchema.getDB().query();

        if (tLJTempFeeSet.size() > 0)
            return true;
        else
            return false;
    }

    /**
     * 变更应收备份数据状态
     * 
     * @param tLJSPaySchema
     *            LJSPaySchema
     */
    private void setLJSPayBState(LJSPaySchema tLJSPaySchema)
    {
        if (tLJSPaySchema == null || tLJSPaySchema.getGetNoticeNo() == null
                || tLJSPaySchema.getGetNoticeNo().equals(""))
        {
            return;
        }
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
        LJSPayBSet tLJSPayBSet = tLJSPayBDB.query();
        for (int i = 1; i <= tLJSPayBSet.size(); i++)
        {
            tLJSPayBSet.get(i).setDealState("4");
        }
        outLJSPayBSet.add(tLJSPayBSet);

        LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
        tLJSPayPersonBDB.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
        LJSPayPersonBSet set = tLJSPayPersonBDB.query();
        for (int i = 1; i <= set.size(); i++)
        {
            set.get(i).setDealState("4");
        }
        outLJSPayPersonBSet.add(set);
    }

    /**
     * 更新暂交费表，适用于事后选择银行转账
     * 
     * @param tLYReturnFromBankSchema
     */
    private void updateLJTempFee(LYReturnFromBankSchema tLYReturnFromBankSchema)
            throws Exception
    {
        System.out.println("更新暂交费表   开始 ！！！");
        // 核销暂交费分类表，设置到帐日期
        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        tLJTempFeeClassDB.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
        tLJTempFeeClassDB.setPayMode("4"); // 银行转账
        LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.query();

        for (int i = 0; i < tLJTempFeeClassSet.size(); i++)
        {
            LJTempFeeClassSchema tLJTempFeeClassSchema = tLJTempFeeClassSet
                    .get(i + 1);

            // 修改到帐日期
            tLJTempFeeClassSchema.setEnterAccDate(tLYReturnFromBankSchema
                    .getBankDealDate());
            tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
            tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
            tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
            tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
            tLJTempFeeClassSchema.setOperator("sys");
            tLJTempFeeClassSchema.setCashier("sys");
            tLJTempFeeClassSchema.setPayMode("4");

            //String bankCode = getFinBankCode(tLYReturnFromBankSchema
            //         .getBankCode());
            String SQL = "select * from ldfinbank where finbankcode='7705' and finflag = 'S' and managecom = '86000000'";
            LDFinBankDB tLDFinBankDB = new LDFinBankDB();
            LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(SQL);
            if (tLDFinBankSet.size() == 0)
                System.out.println("---查询银行失败！---");
            tLJTempFeeClassSchema.setManageCom(tLDFinBankSet.get(1)
                    .getManageCom());
            //增加归集账户  dongjian
            tLJTempFeeClassSchema.setInsBankAccNo(tLDFinBankSet.get(1)
                    .getBankAccNo());
            tLJTempFeeClassSchema.setInsBankCode(tLDFinBankSet.get(1)
                    .getBankCode());

            tLJTempFeeClassSchema.setPolicyCom(tLYReturnFromBankSchema
                    .getComCode());
        }

        // 校验该暂交费号是否已经全部到帐，是则设置暂交费表的到帐日期，否则不设置
        LJTempFeeClassDB tLJTempFeeClassDB2 = new LJTempFeeClassDB();
        String strSql = "select * from LJTempFeeClass where TempFeeNo='"
                + tLYReturnFromBankSchema.getPayCode() + "' and PayMode<>'4'";

        LJTempFeeClassSet tLJTempFeeClassSet2 = tLJTempFeeClassDB2
                .executeQuery(strSql);

        boolean isAllEnterAcc = true;
        for (int j = 0; j < tLJTempFeeClassSet2.size(); j++)
        {
            if (tLJTempFeeClassSet2.get(j + 1).getEnterAccDate() == null)
            {
                isAllEnterAcc = false;
                break;
            }
        }

        if (isAllEnterAcc)
        {
            LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
            tLJTempFeeDB.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
            LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();

            for (int i = 0; i < tLJTempFeeSet.size(); i++)
            {
                LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i + 1);

                // 修改到帐日期
                tLJTempFeeSchema.setEnterAccDate(tLYReturnFromBankSchema
                        .getBankDealDate());
                tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setOperator("sys");
                tLJTempFeeSchema.setCashier("sys");

                String SQL = "select * from ldfinbank where finbankcode='7705' and finflag = 'S' and managecom = '86000000'";
                LDFinBankDB tLDFinBankDB = new LDFinBankDB();
                LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(SQL);
                tLJTempFeeSchema.setManageCom(tLDFinBankSet.get(1)
                        .getManageCom());

                tLJTempFeeSchema.setPolicyCom(tLYReturnFromBankSchema
                        .getComCode());

                mTempFeeSetForAfterDeal.add(tLJTempFeeSchema);
            }

            outUpdateLJTempFeeSet.add(tLJTempFeeSet);
        }

        outUpdateLJTempFeeClassSet.add(tLJTempFeeClassSet);
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean preparePayOutputData()
    {
        mInputData = new VData();

        try
        {
            mInputData.add(outLYBankLogSchema);
            mInputData.add(tLYReturnFromBankSet);
            mInputData.add(outLYDupGetSet);
            mInputData.add(outLYBankLogSet);
            mInputData.add(outLJFIGetSet);
            mInputData.add(outLJAGetSet);
            mInputData.add(outDelLYSendToBankSet);
            mInputData.add(tLYReturnFromBankSet);

            mInputData.add(outLJAGetDrawSet);
            mInputData.add(outLJAGetEndorseSet);
            mInputData.add(outLJAGetTempFeeSet);
            mInputData.add(outLJAGetClaimSet);
            mInputData.add(outLJAGetOtherSet);
            mInputData.add(outLJABonusGetSet);
            mInputData.add(this.mLLCaseSet);
            mInputData.add(this.mLLRegisterSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PayReturnFromBankBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 通过银行返回数据获取应收表数据
     * 
     * @param tLYReturnFromBankSchema
     * @return
     */
    private LJSPaySchema getLJSPayByLYReturnFromBank(
            LYReturnFromBankSchema tLYReturnFromBankSchema)
    {
        try
        {
            LJSPaySchema tLJSPaySchema = new LJSPaySchema();

            tLJSPaySchema.setGetNoticeNo(tLYReturnFromBankSchema.getPayCode());
            return tLJSPaySchema.getDB().query().get(1);
        }
        catch (Exception e)
        {
            // 总应收表无数据，表示扣款失败，并且已经用其它方式交费
            return null;
        }
    }

    /**
     * 转入暂交费表
     *      TempFeeType  = 18 为银保通类型
     * @param tLYReturnFromBankSchema
     */
    private void setLJTempFee(LYReturnFromBankSchema tLYReturnFromBankSchema)
            throws Exception
    {
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        LJSPaySchema tLJSPaySchema = getLJSPayByLYReturnFromBank(tLYReturnFromBankSchema);

        tLJTempFeeSchema.setTempFeeNo(tLYReturnFromBankSchema.getPayCode());
        // 首期直接交费
        if (tLYReturnFromBankSchema.getDoType().equals("2"))
        {
            tLJTempFeeSchema.setTempFeeType("18");
            tLJTempFeeSchema.setOtherNoType("0");
        }
        else if (tLYReturnFromBankSchema.getDoType().equals("1"))
        {
            // 首期事后交费
            if (tLJSPaySchema.getOtherNoType().equals("6"))
            {
                tLJTempFeeSchema.setTempFeeType("18");
                tLJTempFeeSchema.setOtherNoType("0");
            }
            // 正常续期催收
            else if (tLJSPaySchema.getOtherNoType().equals("2"))
            {
                tLJTempFeeSchema.setTempFeeType("18");
                tLJTempFeeSchema.setOtherNoType("1");
            }
        }

        tLJTempFeeSchema.setOtherNo(tLJSPaySchema.getOtherNo());
        tLJTempFeeSchema.setEnterAccDate(tLYReturnFromBankSchema
                .getBankDealDate());
        tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setConfFlag("0");
        // 流水号
        String tLimit = PubFun.getNoLimit(tLJSPaySchema.getManageCom());
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        //serNo = "B"+serNo;
        tLJTempFeeSchema.setSerialNo(serNo);
        String com = tLYReturnFromBankSchema.getComCode();
        for (int k = com.length(); k < 8; k++)
        {
            com = com + "0";
        }
        tLJTempFeeSchema.setManageCom(com);

        //<*****************************>                            <****************************>
        tLJTempFeeSchema.setTempFeeType("4");

        tLJTempFeeSchema.setPolicyCom(tLYReturnFromBankSchema.getComCode());
        tLJTempFeeSchema.setAgentGroup(tLJSPaySchema.getAgentGroup());
        tLJTempFeeSchema.setAgentCode(tLJSPaySchema.getAgentCode());
        //险种编码
        tLJTempFeeSchema.setRiskCode("000000");
        tLJTempFeeSchema.setPayMoney(tLYReturnFromBankSchema.getPayMoney());
        tLJTempFeeSchema.setPayDate(tLYReturnFromBankSchema.getSendDate());
        tLJTempFeeSchema.setOperator("sys");
        tLJTempFeeSchema.setCashier("sys");
        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());

        // 为暂交费分类表设置数据
        mReflections.transFields(tLJTempFeeClassSchema, tLJTempFeeSchema);
        tLJTempFeeClassSchema.setPayMode("4");
        tLJTempFeeClassSchema
                .setBankCode(tLYReturnFromBankSchema.getBankCode());
        tLJTempFeeClassSchema.setBankAccNo(tLYReturnFromBankSchema.getAccNo());
        tLJTempFeeClassSchema.setAccName(tLYReturnFromBankSchema.getAccName());

        tLJTempFeeClassSchema.setEnterAccDate(tLYReturnFromBankSchema
                .getBankDealDate());
        tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setConfFlag("0");

        // 流水号
        tLJTempFeeClassSchema.setSerialNo(serNo);
        String Mancom = tLYReturnFromBankSchema.getComCode();
        for (int k = Mancom.length(); k < 8; k++)
        {
            Mancom = Mancom + "0";
        }
        tLJTempFeeClassSchema.setManageCom(Mancom);
        tLJTempFeeClassSchema.setPolicyCom(Mancom);
        outLJTempFeeSet.add(tLJTempFeeSchema);
        outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
    }

    /**
     * 生成银行日志数据
     * @param tLYSendToBankSchema
     * @return
     */
    private LYBankLogSet getPayLYBankLog()
    {
        LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
        LYBankLogSet tLYBankLogSet = new LYBankLogSet();

        //获取日志记录
        tLYBankLogSchema.setSerialNo(tLYReturnFromBankSet.get(1).getSerialNo());
        tLYBankLogSet.set(tLYBankLogSchema.getDB().query());

        if (tLYBankLogSet.size() > 0)
        {
            tLYBankLogSchema.setSchema(tLYBankLogSet.get(1));
            //修改日志
            tLYBankLogSchema.setAccTotalMoney(tLYBankLogSchema
                    .getAccTotalMoney()); // 财务确认总金额
            tLYBankLogSchema.setBankSuccMoney(tLYBankLogSchema
                    .getBankSuccMoney()); // 银行成功总金额
            tLYBankLogSchema.setBankSuccNum(tLYBankLogSchema.getBankSuccNum()
                    + ""); // 银行成功总数量
            tLYBankLogSchema.setInFile("回盘成功#" + this.path);
            tLYBankLogSchema.setDealState("1"); // 处理状态
            tLYBankLogSchema.setTransOperator("sys");
            tLYBankLogSchema.setReturnOperator("sys");
            tLYBankLogSchema.setReturnDate(PubFun.getCurrentDate());
            //tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());

            String ComCode = tLYBankLogSchema.getComCode();
            System.out.println("1306行 管理机构为  " + ComCode);

            int len = ComCode.length();
            for (int i = 0; i < 8 - len; i++)
            {
                ComCode = ComCode + "0";
            }

            tLYBankLogSchema.setComCode(ComCode);

            tLYBankLogSchema.setDealState("1"); //处理状态
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());

            tLYBankLogSet.clear();
            tLYBankLogSet.add(tLYBankLogSchema);
        }

        return tLYBankLogSet;
    }

    /**
     * 更新银行日志数据
     * 
     * @param tLYSendToBankSchema
     * @return
     */
    private LYBankLogSchema getLYBankLog()
    {
        // 获取日志记录
        LYBankLogDB tLYBankLogDB = new LYBankLogDB();

        System.out.println("批次号为:" + tLYReturnFromBankSet.get(1).getSerialNo());
        tLYBankLogDB.setSerialNo(tLYReturnFromBankSet.get(1).getSerialNo());

        if (!tLYBankLogDB.getInfo())
        {
            CError.buildErr(this, "获取银行日志数据失败");
            return null;
        }
        LYBankLogSchema tLYBankLogSchema = tLYBankLogDB.getSchema();
        tLYBankLogSchema.setInFile("回盘成功#" + this.path);
        tLYBankLogSchema.setSerialNo(tLYReturnFromBankSet.get(1).getSerialNo());
        tLYBankLogSchema.setReturnDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setReturnOperator("sys");
        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());

        return tLYBankLogSchema;
    }

    /**
     * 校验每笔金额
     */
    private boolean verifyUnitMoney()
    {
        try
        {
            for (int i = 0; i < tLYReturnFromBankSet.size(); i++)
            {
                LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet
                        .get(i + 1);
                // 此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
                if (tLYReturnFromBankSchema.getConvertFlag() != null)
                    continue;

                LJSPaySchema tLJSPaySchema = new LJSPaySchema();
                tLJSPaySchema.setGetNoticeNo(tLYReturnFromBankSchema
                        .getPayCode());
                System.out.println("====="
                        + tLYReturnFromBankSchema.getPayCode());
                tLJSPaySchema.setSchema(tLJSPaySchema.getDB().query().get(1));

                // 扣款小于应收表中的信息，则转入重复交费表（设置转换标记为2），否则通过
                if (tLJSPaySchema.getSumDuePayMoney() != tLYReturnFromBankSchema
                        .getPayMoney())
                {
                    setLYDupPay(tLYReturnFromBankSchema);
                    tLYReturnFromBankSchema.setConvertFlag("2");
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            try
            {
                throw e;
            }
            catch (Exception e1)
            {
                e1.printStackTrace();
                System.out.println("校验每笔金额失败！");
            }
        }
        return true;
    }

    /**
     * 记录入重复交费记录表
     * 
     * @param tLYReturnFromBankSchema
     */
    private void setLYDupPay(LYReturnFromBankSchema tLYReturnFromBankSchema)
    {
        LYDupPaySchema tLYDupPaySchema = new LYDupPaySchema();
        mReflections.transFields(tLYDupPaySchema, tLYReturnFromBankSchema);
        tLYDupPaySchema.setDataType("R");
        outLYDupPaySet.add(tLYDupPaySchema);
    }

    /**
     * 记录入重复交费记录表
     * @param tLYReturnFromBankSchema
     */
    private void setLYDupGet(LYReturnFromBankSchema tLYReturnFromBankSchema)
    {
        Reflections tReflections = new Reflections();
        LYDupGetSchema tLYDupGetSchema = new LYDupGetSchema();
        tReflections.transFields(tLYDupGetSchema, tLYReturnFromBankSchema);
        tLYDupGetSchema.setDataType("F");
        outLYDupGetSet.add(tLYDupGetSchema);
    }

    /**
     * 校验银行扣款成功与否
     */
    private void verifyBankSucc()
    {
        int i;
        ExeSQL bankExeSQL = new ExeSQL();
        String tserialno = tLYReturnFromBankSet.get(1).getSerialNo();
        String sersql = "select AgentPaySuccFlag from ldbank where bankcode =(select bankcode from lybanklog where serialno='"
                + tserialno + "' fetch first 1 rows only)";
        String SuccBankCode = bankExeSQL.getOneValue(sersql);
        System.out.println("=====" + SuccBankCode);
        if (SuccBankCode == null || "".equals(SuccBankCode))
        {
            throw new NullPointerException("无银行成功标记！");
        }

        for (i = 0; i < tLYReturnFromBankSet.size(); i++)
        {
            System.out.println("======" + "失循环校验了");
            LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet
                    .get(i + 1);
            // 此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
            if (tLYReturnFromBankSchema.getConvertFlag() != null)
                continue;

            // 如果扣款失败，则设置转换标记为1
            if (!verifyBankSuccFlag(SuccBankCode, tLYReturnFromBankSchema))
            {
                System.out.println("======" + "失败了");
                tLYReturnFromBankSet.get(i + 1).setConvertFlag("1");

            }
            else
            {

                tLYReturnFromBankSet.get(i + 1).setPayType("R");
            }
        }
    }

    /**
     * 校验银行扣款成功标志
     * 
     * @param bankSuccFlag
     * @param tLYReturnFromBankSchema
     * @return
     */
    private boolean verifyBankSuccFlag(String bankSuccFlag,
            LYReturnFromBankSchema tLYReturnFromBankSchema)
    {
        int i;
        boolean passFlag = false;
        String[] arrSucc = PubFun.split(bankSuccFlag, ";");
        String tSucc = tLYReturnFromBankSchema.getBankSuccFlag();
        for (i = 0; i < arrSucc.length; i++)
        {
            if (arrSucc[i].equals("") && tSucc == null)
            {
                passFlag = true;
                break;
            }
            if (arrSucc[i].equals(tSucc))
            {
                passFlag = true;
                break;
            }
        }
        System.out.println("------返回的值是" + passFlag);
        return passFlag;
    }

    private boolean prepareOutputData()
    {
        mInputData = new VData();
        try
        {
            mInputData.add(outLYBankLogSet);
            mInputData.add(tLYReturnFromBankSet);
            mInputData.add(outLYDupPaySet);
            mInputData.add(outLYBankLogSchema);
            mInputData.add(outDelLJSPaySet);
            mInputData.add(outLJTempFeeSet);
            mInputData.add(outLJSPaySet);
            mInputData.add(outDelLYSendToBankSet);
            mInputData.add(outLJTempFeeClassSet);
            mInputData.add(outUpdateLJTempFeeSet);
            mInputData.add(outUpdateLJTempFeeClassSet);
            mInputData.add(outLJSPayBSet);
            mInputData.add(outLJSPayPersonBSet);
            mInputData.add(outLYBankLogSet);

        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    /**
     * 校验银行扣款成功与否
     */
    private void verifyPayBankSucc()
    {
        int i;
        ExeSQL bankExeSQL = new ExeSQL();
        String tserialno = tLYReturnFromBankSet.get(1).getSerialNo();
        String sersql = "select AgentPaySuccFlag from ldbank where bankcode =(select bankcode from lybanklog where serialno='"
                + tserialno + "' fetch first 1 rows only)";
        String SuccBankCode = bankExeSQL.getOneValue(sersql);
        System.out.println("---" + SuccBankCode);
        if (SuccBankCode == null || "".equals(SuccBankCode))
        {
            throw new NullPointerException("无银行成功标记！");
        }

        for (i = 0; i < tLYReturnFromBankSet.size(); i++)
        {
            LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet
                    .get(i + 1);
            System.out.println("---" + "执行循环了");
            // 此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
            /*if (tLYReturnFromBankSchema.getConvertFlag() != null)
             continue;
             */
            // 如果扣款失败，则设置转换标记为1
            if (!verifyBankSuccFlag(SuccBankCode, tLYReturnFromBankSchema))
            {
                tLYReturnFromBankSet.get(i + 1).setConvertFlag("1");

            }
            else
            {
                System.out.println("----" + "付费成功了");
                tLYReturnFromBankSet.get(i + 1).setPayType("F");
            }
        }
    }

    /**
     * 通过银行返回数据获取应收表数据
     * 
     * @param tLYReturnFromBankSchema
     * @return
     */
    private LJAGetSchema getLJAGetByLYReturnFromBank(
            LYReturnFromBankSchema tLYReturnFromBankSchema)
    {
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();

        tLJAGetSchema.setActuGetNo(tLYReturnFromBankSchema.getPayCode());

        LJAGetSet tLJAGetSet = tLJAGetSchema.getDB().query();
        if (tLJAGetSet.size() == 0)
        {
            return null;
        }
        return tLJAGetSchema.getDB().query().get(1);

    }

    /**
     * 获取实付表数据，修改银行在途标志、财务到帐日期、财务确认日期
     */
    private boolean getLJAGet() throws Exception
    {
        try
        {
            for (int i = 0; i < tLYReturnFromBankSet.size(); i++)
            {
                LYReturnFromBankSchema tLYReturnFromBankSchema = tLYReturnFromBankSet
                        .get(i + 1);
                // 获取实付总表数据
                LJAGetSchema tLJAGetSchema = getLJAGetByLYReturnFromBank(tLYReturnFromBankSchema);
                tLJAGetSchema.setBankOnTheWayFlag("0");
                tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());

                //设置本方银行账号
                String sql = "select * from ldfinbank where finbankcode='7705' and Managecom = '86000000' and finflag = 'F'";
                LDFinBankDB tLDFinBankDB = new LDFinBankDB();
                LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(sql);
                if (tLDFinBankSet.size() == 0)
                {
                    mErrors.addOneError("本方银行未配置，请先配置再进行处理");
                    return false;
                }
                String InsBankAccNo = tLDFinBankSet.get(1).getBankAccNo();
                String bankcode = tLDFinBankSet.get(1).getBankCode();

                tLJAGetSchema.setOperator("sys");
                tLJAGetSchema.setInsBankAccNo(InsBankAccNo);
                tLJAGetSchema.setInsBankCode(bankcode);

                // 如果不能正常核销
                if (tLYReturnFromBankSchema.getConvertFlag() != null)
                {
                    tLJAGetSchema.setCanSendBank("1");
                    outLJAGetSet.add(tLJAGetSchema);
                    continue;
                }

                // 核销
                tLJAGetSchema.setEnterAccDate(tLYReturnFromBankSchema
                        .getBankDealDate());
                tLJAGetSchema.setConfDate(PubFun.getCurrentDate());
                outLJAGetSet.add(tLJAGetSchema);

                String OtherNoType = tLJAGetSchema.getOtherNoType();

                // 更新给付表(生存领取_实付) LJAGetDraw
                if (OtherNoType.equals("1") || OtherNoType.equals("2")
                        || OtherNoType.equals("0"))
                {
                    updateLJAGetDraw(tLJAGetSchema);
                }

                // 更新批改补退费表(实收/实付) LJAGetEndorse
                if (OtherNoType.equals("3"))
                {
                    updateLJAGetEndorse(tLJAGetSchema);
                }

                // 更新暂交费退费实付表 LJAGetTempFee
                if (OtherNoType.equals("4"))
                {
                    updateLJAGetTempFee(tLJAGetSchema);
                }

                // 更新赔付实付表 LJAGetClaim
                if (OtherNoType.equals("5"))
                {
                    updateLJAGetClaim(tLJAGetSchema);
                }

                // 更新其他退费实付表 LJAGetOther
                if (OtherNoType.equals("6") || OtherNoType.equals("8"))
                {
                    updateLJAGetOther(tLJAGetSchema);
                }

                // 更新红利给付实付表 LJABonusGet
                if (OtherNoType.equals("7"))
                {
                    updateLJABonusGet(tLJAGetSchema);
                }
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return true;
    }

    public String getbankaccnomain(LJAGetSchema mLJAGetSchema)
    {
        String mybankaccno = "";
        String mybankcode = mLJAGetSchema.getBankCode().substring(0, 2);
        System.out.println("银行编码:" + mybankcode);
        String mysql = "select * from ldfinbank where finflag='F' and bankcode='"
                + mybankcode
                + "' and managecom='"
                + mLJAGetSchema.getManageCom() + "' with ur";
        System.out.println("本方银行帐号查询sql:" + mysql);
        LDFinBankDB tLDFinBankDB = new LDFinBankDB();
        LDFinBankSet tLDFinBankSet = tLDFinBankDB.executeQuery(mysql);
        if (tLDFinBankSet.size() == 0)
        {
            return null;
        }
        LDFinBankSchema tLDFinBankSchemat = tLDFinBankSet.get(1);
        mybankaccno = tLDFinBankSchemat.getBankAccNo();
        System.out.println("996行:" + mybankaccno);
        return mybankaccno;
    }

    /**
     * 查询给付表(生存领取_实付)
     * 
     * @param LJAGetBL
     * @return
     */
    private void updateLJAGetDraw(LJAGetSchema mLJAGetSchema) throws Exception
    {
        String sqlStr = "select * from LJAGetDraw where ActuGetNo='"
                + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("查询给付表(生存领取_实付):" + sqlStr);
        LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
        LJAGetDrawSet tLJAGetDrawSet = tLJAGetDrawDB.executeQuery(sqlStr);

        if (tLJAGetDrawSet.size() == 0)
            throw new Exception("给付表无数据，不能核销！");

        for (int n = 1; n <= tLJAGetDrawSet.size(); n++)
        {
            LJAGetDrawSchema tLJAGetDrawSchema = tLJAGetDrawSet.get(n);
            tLJAGetDrawSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJAGetDrawSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJAGetDrawSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetDrawSchema.setModifyTime(PubFun.getCurrentTime());
            outLJAGetDrawSet.add(tLJAGetDrawSchema);
        }
    }

    /**
     * 查询批改补退费表(实收/实付)
     * 
     * @param LJAGetSchema
     * @return
     */
    private void updateLJAGetEndorse(LJAGetSchema mLJAGetSchema)
            throws Exception
    {
        String sqlStr = "select * from LJAGetEndorse where ActuGetNo='"
                + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("查询给付表(生存领取_实付):" + sqlStr);
        LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
        LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB
                .executeQuery(sqlStr);

        if (tLJAGetEndorseSet.size() == 0)
            throw new Exception("批改补退费表无数据，不能核销！");

        for (int n = 1; n <= tLJAGetEndorseSet.size(); n++)
        {
            LJAGetEndorseSchema tLJAGetEndorseSchema = tLJAGetEndorseSet.get(n);
            tLJAGetEndorseSchema.setGetConfirmDate(mLJAGetSchema.getConfDate());
            tLJAGetEndorseSchema.setEnterAccDate(mLJAGetSchema
                    .getEnterAccDate());
            tLJAGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());
            outLJAGetEndorseSet.add(tLJAGetEndorseSchema);
        }
    }

    /**
     * 暂交费退费实付表
     * 
     * @param LJAGetSchema
     * @return
     */
    private void updateLJAGetTempFee(LJAGetSchema mLJAGetSchema)
            throws Exception
    {
        String sqlStr = "select * from LJAGetTempFee where ActuGetNo='"
                + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("暂交费退费实付表:" + sqlStr);
        LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
        LJAGetTempFeeSet tLJAGetTempFeeSet = tLJAGetTempFeeDB
                .executeQuery(sqlStr);

        if (tLJAGetTempFeeSet.size() == 0)
            throw new Exception("暂交费退费实付表无数据，不能核销！");

        for (int n = 1; n <= tLJAGetTempFeeSet.size(); n++)
        {
            LJAGetTempFeeSchema tLJAGetTempFeeSchema = tLJAGetTempFeeSet.get(n);
            tLJAGetTempFeeSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJAGetTempFeeSchema.setEnterAccDate(mLJAGetSchema
                    .getEnterAccDate());
            tLJAGetTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
            outLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
        }
    }

    /**
     * 赔付实付表
     * 
     * @param LJAGetSchema
     * @return
     */
    private void updateLJAGetClaim(LJAGetSchema mLJAGetSchema) throws Exception
    {
        String sqlStr = "select * from LJAGetClaim where ActuGetNo='"
                + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("赔付实付表:" + sqlStr);
        LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
        LJAGetClaimSet tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
        setLLCaseFalg(mLJAGetSchema.getOtherNo(), tLJAGetClaimSet);
        if (tLJAGetClaimSet.size() == 0)
            throw new Exception("赔付实付表无数据，不能核销！");

        for (int n = 1; n <= tLJAGetClaimSet.size(); n++)
        {
            LJAGetClaimSchema tLJAGetClaimSchema = tLJAGetClaimSet.get(n);
            tLJAGetClaimSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJAGetClaimSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJAGetClaimSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetClaimSchema.setModifyTime(PubFun.getCurrentTime());
            outLJAGetClaimSet.add(tLJAGetClaimSchema);
        }
    }

    public boolean setLLCaseFalg(String strRgtNo, LJAGetClaimSet aLJAGetClaimSet)
    {
        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
        tLLRegisterDB.setRgtNo(strRgtNo);
        if (tLLRegisterDB.getInfo())
        {
            tLLRegisterDB.setRgtState("04");
            this.mLLRegisterSet.add(tLLRegisterDB.getSchema());
        }
        String tRgtNo = "";
        String tCaseNo = "";
        for (int k = 1; k <= aLJAGetClaimSet.size(); k++)
        {
            String tfeeFinaType = aLJAGetClaimSet.get(k).getFeeFinaType();
            if (tfeeFinaType != null
                    && (tfeeFinaType.equals("JJ") || tfeeFinaType.equals("ZJ")))
            {
                continue;
            }
            else
            {
                LLCaseDB tLLCaseDB = new LLCaseDB();
                tLLCaseDB.setCaseNo(aLJAGetClaimSet.get(k).getOtherNo());
                if (tLLCaseDB.getInfo())
                {
                    tRgtNo = tLLCaseDB.getRgtNo();
                    tCaseNo = tLLCaseDB.getCaseNo();
                    tLLCaseDB.setRgtState("12");
                    this.mLLCaseSet.add(tLLCaseDB.getSchema());
                }
            }
        }
        // 团单下最后一个人做完给付确认应更新团单案件的状态为“04”－所有个人给付完毕
        if (strRgtNo.substring(0, 1).equals("C") && !tRgtNo.equals(tCaseNo))
        {
            LLRegisterDB tsLLRegisterDB = new LLRegisterDB();
            tsLLRegisterDB.setRgtNo(tRgtNo);
            if (tsLLRegisterDB.getInfo())
            {
                if ("1".equals(tsLLRegisterDB.getRgtClass()))
                {
                    boolean tFlag = true;
                    LLCaseDB tmpLLCaseDB = new LLCaseDB();
                    LLCaseSet tmpLLCaseSet = new LLCaseSet();
                    tmpLLCaseDB.setRgtNo(tsLLRegisterDB.getRgtNo());
                    tmpLLCaseSet.set(tmpLLCaseDB.query());
                    if (tmpLLCaseSet != null && tmpLLCaseSet.size() != 0)
                    {
                        for (int m = 1; m <= tmpLLCaseSet.size(); m++)
                        {
                            if (tmpLLCaseSet.get(m).getCaseNo().equals(tCaseNo))
                                continue;
                            if (!("12"
                                    .equals(tmpLLCaseSet.get(m).getRgtState()) || "14"
                                    .equals(tmpLLCaseSet.get(m).getRgtState())))
                            {
                                tFlag = false;
                                break;
                            }
                        }
                    }
                    if (tFlag)
                    {
                        tsLLRegisterDB.setRgtState("04");
                        this.mLLRegisterSet.add(tsLLRegisterDB.getSchema());
                    }
                }
            }
        }
        return true;
    }

    /**
     * 查询其他退费实付表
     * 
     * @param LJAGetSchema
     * @return
     */
    private void updateLJAGetOther(LJAGetSchema mLJAGetSchema) throws Exception
    {
        String sqlStr = "select * from LJAGetOther where ActuGetNo='"
                + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("其他退费实付表:" + sqlStr);
        LJAGetOtherDB tLJAGetOtherDB = new LJAGetOtherDB();
        LJAGetOtherSet tLJAGetOtherSet = tLJAGetOtherDB.executeQuery(sqlStr);

        if (tLJAGetOtherSet.size() == 0)
            throw new Exception("其他退费实付表无数据，不能核销！");

        for (int n = 1; n <= tLJAGetOtherSet.size(); n++)
        {
            LJAGetOtherSchema tLJAGetOtherSchema = tLJAGetOtherSet.get(n);
            tLJAGetOtherSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJAGetOtherSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJAGetOtherSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetOtherSchema.setModifyTime(PubFun.getCurrentTime());
            outLJAGetOtherSet.add(tLJAGetOtherSchema);
        }
    }

    /**
     * 红利给付实付表
     * 
     * @param LJAGetSchema
     * @return
     */
    private void updateLJABonusGet(LJAGetSchema mLJAGetSchema) throws Exception
    {
        String sqlStr = "select * from LJABonusGet where ActuGetNo='"
                + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("红利给付实付表:" + sqlStr);
        LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB();
        LJABonusGetSet tLJABonusGetSet = tLJABonusGetDB.executeQuery(sqlStr);

        if (tLJABonusGetSet.size() == 0)
            throw new Exception("红利给付实付表无数据，不能核销！");

        for (int n = 1; n <= tLJABonusGetSet.size(); n++)
        {
            LJABonusGetSchema tLJABonusGetSchema = tLJABonusGetSet.get(n);
            tLJABonusGetSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJABonusGetSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJABonusGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLJABonusGetSchema.setModifyTime(PubFun.getCurrentTime());
            outLJABonusGetSet.add(tLJABonusGetSchema);
        }
    }

    public String getFinBankCode(String bankCode)
    {
        String finBankCode = "01";
        String bankSql = "select * from ldbankunite where bankcode='"
                + bankCode + "' with ur";
        System.out.println(bankSql);
        LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
        LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.executeQuery(bankSql);
        if (tLDBankUniteSet.get(1).getBankUniteCode().equals("7705"))
        {
            finBankCode = "01";
        }
        else if (tLDBankUniteSet.get(1).getBankUniteCode().equals("7706"))
        {
            finBankCode = "01";
        }
        System.out.println("ldfinbank中bankCode:" + bankCode);
        return finBankCode;
    }

    public static void main(String[] args) throws SAXException, IOException
    {
        RealTimeReturnFromBankBL tBatchReturnFromBankBL = new RealTimeReturnFromBankBL();
        tBatchReturnFromBankBL
                .submitData(
                        "D:\\batchsendbank\\return\\20111214195625\\a.xml",
                        "7705");
    }

}
