package com.sinosoft.lis.bank;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.utility.ExeSQL;

/**
 * 获取医保交易的报文
 * @author 王硕
 */
public class GetParamToM {
	/**
	 * 业务周期号
	 */
	private static String busiCode = "";
	/**
	 * 获取交易的参数固定信息
	 * @param type 交易类型 
	 * @return 业务周期号
	 * @throws Exception 
	 */
	public static String registToMedical() throws Exception{
		SendToPreMacMedical sToPM = new SendToPreMacMedical();
		String prarms = getInputStr();
		String busicode= sToPM.send(prarms, "");
		if("".equals(busicode))throw new Exception("签到失败");
		String busiCodeTemp = getDelimitedStr(busicode,"\\^")[2];
		busiCode = getDelimitedStr(busiCodeTemp,"\\|")[0];
		return busiCode;
	} 
	
	private static String getInputStr(){
		
		String tradeType = getParamter("tradetype","签到");
		String medicalCode = getParamter("medicalcode",null);
		String operatorCode = getParamter("operatorcode",null);
		String centerCode = getParamter("centercode",null);
		String onlineSign = getParamter("onlinesign",null);
		//业务编号^医疗机构编号^操作员编号^业务周期号^医院交易流水号^中心编码^入参^ 联机标志^
		String params = tradeType+"^"+medicalCode+"^"+operatorCode+"^"+busiCode+"^^"+centerCode+"^^"+onlineSign+"^";
		System.out.println(params);
		return params;
	}
	
	private static String getParamter(String codename,String othersign){
		String requestSQL = "select code from ldcode where codetype = 'lnmedicalinf' and codename = '"+codename+"'";
		if(othersign != null){
			requestSQL +=" and othersign = '"+othersign+"' ";
		}else{
			requestSQL +=" and othersign is null ";
		}
		requestSQL += "with ur";
		
		ExeSQL tExeSQL = new ExeSQL(); 
		String parameter = tExeSQL.getOneValue(requestSQL);
		return parameter;
	}
	/**
	 * 获取业务周期号
	 * @return
	 */
	public static String getBusiCode(){
		return busiCode;
	}
	/**
	 * 把字符串转换成数组
	 * @param originalStr 原字符串
	 * @param matchingStr 分隔字符
	 * @return String[]
	 */
	public static String[] getDelimitedStr(String originalStr , String matchingStr) {
		  return originalStr.split(matchingStr);
	 }
	public static String dateFormat(String dateStr){
		StringBuffer strB = new StringBuffer();
		
		
		
		return strB.toString();
	}  
	
	/**
	   * 得到现在时间
	   * 
	   * @return 字符串 yyyyMMdd HHmmss
	 * @throws ParseException 
	   */
	public static String getStringDate(String dateStr) throws ParseException {
//	   Date currentTime = new Date();
	   SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	   Date date = formatter.parse(dateStr);
	   SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	   String dateString = sdf.format(date);
	   return dateString;
	}

	public static String setDate(String dateStr) throws ParseException {
//		   Date currentTime = new Date();
		   String getdate=dateStr.substring(0, 8);
		   SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		   Date date = formatter.parse(getdate);
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		   String dateString = sdf.format(date);
		   return dateString;
		}
	
	public static String setTime(String dateStr) throws ParseException {
//		   Date currentTime = new Date();
		   String gettime=dateStr.substring(8);
		   SimpleDateFormat formatter = new SimpleDateFormat("HHmmss");
		   Date date = formatter.parse(gettime);
		   SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		   String dateString = sdf.format(date);
		   return dateString;
		   
		}


	public static void main(String args[]){
		try {
//			System.out.println(GetParamToM.registToMedical());
			System.out.println(GetParamToM.getStringDate("2013-5-5"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
