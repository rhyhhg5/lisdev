package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class RealTimeReturnXml
{
    private String mUrl = "";

    private MMap map = new MMap();

    private VData mInputData = new VData();

    

    public boolean submitData(String payReq, LYBankLogSchema tLYBankLogSchema)
    {

        if (!dealDate(payReq,tLYBankLogSchema))
        {
            System.out.println("获取数据失败");
            return false;
        }

        if (!prepareOutputData())
        {
            System.out.println("准备更新日志数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE"))
        {
            //错误处理
            System.out.println("更新日志失败");
            return false;
        }

        return true;
    }

    public boolean dealDate(String payReq, LYBankLogSchema tLYBankLogSchema)
    {
        
        createXml(payReq, tLYBankLogSchema);

        return true;
    }

    public void createXml(String payReq, LYBankLogSchema tLYBankLogSchema)
    {
        String bankUniteCode = tLYBankLogSchema.getBankCode();
        saveReq(payReq, tLYBankLogSchema, bankUniteCode);

    }

    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema,
            String bankCode)
    {
        System.out.println("成功了！！！！");
        RealTimeReturnFromBankBL tBatchReturnFromBankBL = new RealTimeReturnFromBankBL();
        try
        {
            if (!tBatchReturnFromBankBL.submitData(payReq, bankCode))
            {
                errBankLog(tLYBankLogSchema, 2);
            }
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, 2);
            ex.printStackTrace();
        }
        return true;
    }

    private void errBankLog(LYBankLogSchema tLYBankLogSchema, int flag)
    {
        if (flag == 1)
        {
            System.out.println("获取响应信息失败");
            tLYBankLogSchema.setInFile("获取响应信息失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            System.out.println("回盘处理程序出现异常");
            tLYBankLogSchema.setInFile("回盘处理程序出现异常");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        this.map.put(tLYBankLogSchema, "UPDATE");
    }

    
    /**
     * newFolder
     * 新建报文存放文件夹，以便对回盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] arr)
    {
    }
}
