package com.sinosoft.lis.bank;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Notebook</p>
 *
 * <p>Description: TeamNotebook</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LDCodeInterfaceUI {
    public LDCodeInterfaceUI() {

    }
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /**
     * 向BL传递的接口
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        LDCodeInterfaceBL tLDCodeInterfaceUIBL = new LDCodeInterfaceBL();
        if (!tLDCodeInterfaceUIBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLDCodeInterfaceUIBL.mErrors);
            return false;
        }
        return true;
    }

}
