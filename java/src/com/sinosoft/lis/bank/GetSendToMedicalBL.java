package com.sinosoft.lis.bank;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDMedicalComDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBankAccSchema;
import com.sinosoft.lis.schema.LCBankAuthSchema;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 业务数据转换到银行系统，银行代收</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class GetSendToMedicalBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 返回数据的容器 */
  private VData mResult = new VData();
  /** 提交数据的容器 */
  private MMap map = new MMap();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /** 前台传入的公共变量 */
  private GlobalInput mGlobalInput = new GlobalInput();

  //业务数据
  /** 提数开始日期 */
  private String startDate = "";
  /** 提数结束日期 */
  private String endDate = "";
  /** 银行编码 */
  private String medicalCode = "";
  /** 提取数据范围类型，如果值是：ALLXQ，则只提取续期数据 */
  private String typeFlag = "";
  /** 总金额 */
  private double totalMoney = 0;
  /** 总笔数 */
  private int sumNum = 0;

  private  String flag="0";
  /** 批次号 */
  private String serialNo = "";
  /** 批次最大显示返回信息 */
	private String manage = "";

  private LJSPaySet outLJSPaySet = new LJSPaySet();
  private LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();
  private LYBankLogSet outLYBankLogSet = new LYBankLogSet();
//  private LDBankSchema outLDBankSchema = new LDBankSchema();

  public GetSendToMedicalBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代收
    if (mOperate.equals("GETMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start PubSubmit BLS Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        mResult.clear();
        return false;
      }
      System.out.println("End PubSubmit BLS Submit...");
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
      startDate = (String)tTransferData.getValueByName("startDate");
      endDate = (String)tTransferData.getValueByName("endDate");
      medicalCode = (String)tTransferData.getValueByName("MedicalCode");
      typeFlag = (String)tTransferData.getValueByName("typeFlag");

      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }

  /**
   * 获取交费日期在设置的日期区间内的应收总表记录
   * @param startDate
   * @param endDate
   * @return
   */
 
  private LJSPaySet getLJSPayByPaydate(String startDate, String endDate, String bankCode) {
    String tSql = "";

    //获取最大发送银行次数，在LDSysVar表中设置
    String maxSendToBankCount = "0";
    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    tLDSysVarDB.setSysVar("MaxSendToBankCount");
    if (tLDSysVarDB.getInfo()) {
      maxSendToBankCount = tLDSysVarDB.getSysVarValue();
    }

    //不指定开始时间
    if (startDate.equals("")) {
      //规则：最早应收日期<=结束时间，最晚交费日期>结束时间
      //     银行编码匹配，机构编码向下匹配，不在途，有账号
      //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
      String sql = "select * from LJSPay where StartPayDate <= '" + endDate + "'"
                 + " and PayDate >= '" + endDate + "'"
                 + " and BankCode = '" + bankCode + "'"
                 + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
                 + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                 + " and (CanSendBank = '0' or CanSendBank is null or CanSendBank = '2')  and sumduepaymoney<>0 "
                 + " and (BankAccNo is not null) ";
//
      //非续期划帐，要求发送次数小于最大限制
      tSql = sql + " and SendBankCount <= " + maxSendToBankCount;
    }
    //指定开始时间和结束时间
    else {
      //规则：最早应收日期>=开始时间，最早应收日期<=结束时间，最晚交费日期>结束时间
      //     银行编码匹配，机构编码向下匹配，不在途，有账号
      //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
      String sql = "select * from LJSPay where StartPayDate >= '" + startDate  + "'"
           + " and StartPayDate <= '" + endDate + "'"
           + " and PayDate >= '" + endDate + "'"
           + " and BankCode = '" + bankCode + "'"
           + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
           + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
           + " and (CanSendBank = '0' or CanSendBank is null or CanSendBank = '2') and sumduepaymoney<>0 "
           + " and (BankAccNo is not null) ";

      tSql = sql + " and SendBankCount <= " + maxSendToBankCount;
    }
    System.out.println(tSql);

    LJSPayDB tLJSPayDB = new LJSPayDB();
    LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSql);
    return tLJSPaySet;
  }

  private LJSPaySet getLJSPay(String startDate, String endDate) {
    LJSPaySet tLJSPaySet = new LJSPaySet();

    //获取银行信息，校验是否是银联
    LDMedicalComDB tLDMedicalDB = new LDMedicalComDB();
    tLDMedicalDB.setMedicalComCode(medicalCode);
    if (!tLDMedicalDB.getInfo()) {
      CError.buildErr(this, "获取银行信息（LDBank）失败");
      return null;
    }

    //普通银行
    if (tLDMedicalDB.getMedicalUniteFlag()==null || tLDMedicalDB.getMedicalUniteFlag().equals("0")) {
      tLJSPaySet = getLJSPayByPaydate(startDate, endDate, medicalCode);
    }
    //银联
    else if (tLDMedicalDB.getMedicalUniteFlag().equals("1")){
      LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
      tLDBankUniteDB.setBankUniteCode(medicalCode);
      LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.query();

      if (tLDBankUniteSet.size()==0) {
        CError.buildErr(this, "获取银联相关银行信息（LDBankUnite）失败");
        return null;
      }

      for (int i=0; i<tLDBankUniteSet.size(); i++) {
        tLJSPaySet.add(getLJSPayByPaydate(startDate, endDate, tLDBankUniteSet.get(i+1).getBankCode()));
      }
    }

    return tLJSPaySet;
  }

  /**
   * 校验银行授权
   * @param tLJSPaySet
   */
  private LJSPaySet verifyBankAuth(LJSPaySet tLJSPaySet) {
    int i;
    LJSPaySet bankAuthLJSPaySet = new LJSPaySet();

    for (i=0; i<tLJSPaySet.size(); i++) {
      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i+1);

      LCBankAuthSchema tLCBankAuthSchema = new LCBankAuthSchema();
      tLCBankAuthSchema.setPolNo(tLJSPaySchema.getOtherNo());  //保单号
      tLCBankAuthSchema.setPayGetFlag("0");                    //收付费标志(0---表示收费)
      tLCBankAuthSchema.setPayValidFlag("1");                  //正常交费/领取代收开通标志(1 -- 开通银行代收)
      tLCBankAuthSchema.setBankCode(medicalCode);                 //匹配银行编码

      //找到需要处理的数据
      if (tLCBankAuthSchema.getDB().query().size() > 0) {
        bankAuthLJSPaySet.add(tLJSPaySchema);
      }
    }

    if (bankAuthLJSPaySet.size() > 0) return bankAuthLJSPaySet;
    else return null;
  }

  /**
   * 获取银行账号信息
   * @param tLJSPaySchema
   * @return
   */
  private LCBankAccSchema getBankAcc(LJSPaySchema tLJSPaySchema) {
    try {
      LCBankAccSchema tLCBankAccSchema = new LCBankAccSchema();
      tLCBankAccSchema.setBankCode(medicalCode);
      tLCBankAccSchema.setBankAccNo(tLJSPaySchema.getBankAccNo());

      tLCBankAccSchema.setSchema(tLCBankAccSchema.getDB().query().get(1));

      return tLCBankAccSchema;
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * 生成送银行表数据
   * @param tLJSPaySet
   * @return
   */
  private LYSendToBankSet getSendToBank(LJSPaySet tLJSPaySet) {
    //总金额
    double dTotalMoney = 0;
    //生成批次号，要在循环外生成
    serialNo = "YB"+PubFun1.CreateMaxNo("1", 18);
    LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();

    for (int i=0; i<tLJSPaySet.size(); i++) {
      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i + 1);

      //获取银行账号信息，账户信息不一定存在于账户表中了
//      LCBankAccSchema tLCBankAccSchema = getBankAcc(tLJSPaySchema);
//      if (tLCBankAccSchema == null) return null;
      //校验该笔数据是否已经提盘
        LYSendToBankDB OldLYSendToBankDB = new LYSendToBankDB();
        LYSendToBankSet OldLYSendToBankSet = new LYSendToBankSet();
        OldLYSendToBankDB.setPayCode(tLJSPaySchema.getGetNoticeNo());
        OldLYSendToBankDB.setBankCode(tLJSPaySchema.getBankCode());
        OldLYSendToBankDB.setAccName(tLJSPaySchema.getAccName());
        OldLYSendToBankDB.setAccNo(tLJSPaySchema.getBankAccNo());
        OldLYSendToBankSet=OldLYSendToBankDB.query();
        if(OldLYSendToBankSet.size()>0){
            flag="1";
        }
        ExeSQL tExeSQL=  new ExeSQL();
      //生成送银行表数据
      LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
      //设置统一的批次号
      tLYSendToBankSchema.setSerialNo(serialNo);
      //收费标记
      tLYSendToBankSchema.setDealType("G");
      if(!"2".equals(tLJSPaySchema.getOtherNoType())){
    	  tLYSendToBankSchema.setPayType("1");
      }else{
    	  tLYSendToBankSchema.setPayType(tLJSPaySchema.getOtherNoType());
      }
      tLYSendToBankSchema.setPayCode(tLJSPaySchema.getGetNoticeNo());
      tLYSendToBankSchema.setBankCode(tLJSPaySchema.getBankCode());
//      tLYSendToBankSchema.setAccType(tLCBankAccSchema.getAccType());
      tLYSendToBankSchema.setAccName(tLJSPaySchema.getAccName());
      tLYSendToBankSchema.setAccNo(tLJSPaySchema.getBankAccNo());
     
      if(!"2".equals(tLJSPaySchema.getOtherNoType())){
    	  tLYSendToBankSchema.setPolNo(tLJSPaySchema.getOtherNo());
      }else{
    	  SSRS tSSRS1=tExeSQL.execSQL("select prtno  from lccont  where contno = ( select otherno from ljspay where getnoticeno = '"+tLJSPaySchema.getGetNoticeNo()+"') with ur");
    	  tLYSendToBankSchema.setPolNo(tSSRS1.GetText(1,1));
      }
      tLYSendToBankSchema.setNoType(tLJSPaySchema.getOtherNoType());
      tLYSendToBankSchema.setComCode(tLJSPaySchema.getManageCom());
      tLYSendToBankSchema.setAgentCode(tLJSPaySchema.getAgentCode());
      tLYSendToBankSchema.setPayMoney(tLJSPaySchema.getSumDuePayMoney());
      tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
      tLYSendToBankSchema.setDoType("1");
      //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
      tLYSendToBankSchema.setRemark(mGlobalInput.Operator);
      tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
      tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
     SSRS tSSRS=tExeSQL.execSQL("select idno,idtype from ldperson where customerno='"+tLJSPaySchema.getAppntNo()+"'");
     if(tSSRS.getMaxRow()>0){
           tLYSendToBankSchema.setIDNo(tSSRS.GetText(1,1));
           tLYSendToBankSchema.setIDType(tSSRS.GetText(1,2));
     }else{
      tLYSendToBankSchema.setIDNo("");
     }
     
     //查找保单对应保险公司的险种编码
			String condition = "";
			if (!"2".equals(tLJSPaySchema.getOtherNoType())) {
				condition = "prtno = '" + tLJSPaySchema.getOtherNo() + "'";
			} else {
				condition = "contno = '" + tLJSPaySchema.getOtherNo() + "'";
			}
			String getRiskCode = "select codealias from ldcode  where codetype = 'medicaloutrisk' and code = (select riskcode from lcpol where "
					+ condition + " and polno = mainpolno  fetch first 1 rows only)  with ur";
     
     SSRS tSSRS2 = tExeSQL.execSQL(getRiskCode);
     if(tSSRS2.getMaxRow()>0){
    	 tLYSendToBankSchema.setRiskCode(tSSRS2.GetText(1, 1));
     }else{
    	 tLYSendToBankSchema.setRiskCode(tLJSPaySchema.getRiskCode());
     }
     
      tLYSendToBankSet.add(tLYSendToBankSchema);

      //累加总金额和总数量
      dTotalMoney = dTotalMoney + tLJSPaySchema.getSumDuePayMoney();
      //转换精度
      dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
      sumNum = sumNum + 1;
    }
    totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));

    return tLYSendToBankSet;
  }

  /**
   * 修改应收表银行在途标志,记录发送银行次数
   * @param tLJSPaySet
   * @return
   */
  private LJSPaySet modifyBankFlag(LJSPaySet tLJSPaySet) {
    for (int i=0; i<tLJSPaySet.size(); i++) {
      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i+1);

      tLJSPaySchema.setBankOnTheWayFlag("1");
      //记录发送银行次数
      tLJSPaySchema.setSendBankCount(tLJSPaySchema.getSendBankCount() + 1);
      tLJSPaySet.set(i+1, tLJSPaySchema);
    }

    return tLJSPaySet;
  }

  /**
   * 生成银行日志表数据
   * @return
   */
  private LYBankLogSchema getBankLog() {
    LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

    tLYBankLogSchema.setSerialNo(serialNo);
    tLYBankLogSchema.setBankCode(medicalCode);
    tLYBankLogSchema.setLogType("G");
    tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
//    tLYBankLogSchema.setTotalMoney(157574.57f);
//    String t = (new DecimalFormat("0.000000")).format(tLYBankLogSchema.getTotalMoney());
    tLYBankLogSchema.setTotalMoney(totalMoney);
    tLYBankLogSchema.setTotalNum(sumNum);
    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
    tLYBankLogSchema.setComCode(mGlobalInput.ComCode);

    return tLYBankLogSchema;
  }

  /**
   * 更新银行表独立序号
   * @return
   */
  private LDBankSchema getLDBank() {
    LDBankDB tLDBankDB = new LDBankDB();
    tLDBankDB.setBankCode(medicalCode);
    if (!tLDBankDB.getInfo()) {
      CError.buildErr(this, "获取银行信息（LDBank）失败");
      return null;
    }

    if (tLDBankDB.getSeqNo()==null || tLDBankDB.getSeqNo().equals("")) {
      tLDBankDB.setSeqNo("0");
    }
    else {
      tLDBankDB.setSeqNo(String.valueOf(Integer.parseInt(tLDBankDB.getSeqNo()) + 1));
    }

    return tLDBankDB.getSchema();
  }


  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      //银行代收（应收总表LJSPay）

      if (mOperate.equals("GETMONEY")) {

        //总应收表处理（获取交费日期在设置的日期区间内的记录；获取银行在途标志为N的记录）
        LJSPaySet tLJSPaySet = getLJSPay(startDate, endDate);
        
     // zcx 增加对批次大小的限制
        checkLJSpaySize(tLJSPaySet);
        
        if (tLJSPaySet == null) throw new NullPointerException("总应收表处理失败！");
        if (tLJSPaySet.size() == 0) throw new NullPointerException("总应收表无数据！");
        System.out.println("---End getLJSPayByPaydate---");

        //生成送银行表数据
        LYSendToBankSet tLYSendToBankSet = getSendToBank(tLJSPaySet);
        if(flag.equals("1")){
            CError.buildErr(this, "接收数据失败");
           return false;
        }
        if (tLYSendToBankSet == null) throw new Exception("生成送银行表数据失败！");
        System.out.println("---End getSendToBank---");

        //修改应收表银行在途标志,记录发送银行次数
        tLJSPaySet = modifyBankFlag(tLJSPaySet);
        System.out.println("---End modifyBankFlag---");

        //生成银行日志表数据
        LYBankLogSchema tLYBankLogSchema = getBankLog();
        System.out.println("---End getBankLog---");

        //更新银行表独立序号
//        outLDBankSchema.setSchema(getLDBank());
//        System.out.println("---End getLDBank---");

        outLJSPaySet.set(tLJSPaySet);
        outLYSendToBankSet.set(tLYSendToBankSet);
        outLYBankLogSet.add(tLYBankLogSchema);
      }
    }
    catch(Exception e) {
      // @@错误处理
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      map.put(outLJSPaySet, "UPDATE");
      map.put(outLYSendToBankSet, "INSERT");
      map.put(outLYBankLogSet, "INSERT");
//      map.put(outLDBankSchema, "UPDATE");

      mInputData.clear();
      mInputData.add(map);
    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  public boolean CheckLJSpay(LJSPaySet tLJSPaySet){
    LJSPaySet tempLJSPaySet=new LJSPaySet();
    for (int i = 1; i <= tLJSPaySet.size(); i++) {
        if(tLJSPaySet.size()>0){
            LJSPaySchema tLJSPaySchema=tLJSPaySet.get(i);
            String Accno=tLJSPaySchema.getBankAccNo();
            if(!IsNumeric(Accno)){
                tempLJSPaySet.add(tLJSPaySchema);
                tLJSPaySchema.setCanSendBank("1");
                this.map.put(tLJSPaySchema, "UPDATE");
            }
            LDCode1DB tLDCode1DB=new LDCode1DB();
            String sql="select * from ldcode1 where codetype='bankaccnocheck' and code='"+tLJSPaySchema.getBankCode()+"'";
            LDCode1Set tLDCode1Set= tLDCode1DB.executeQuery(sql);
            if(tLDCode1Set!=null && tLDCode1Set.size()>0){
                LDCode1Schema tLDCode1Schema=tLDCode1Set.get(1);
                if(Integer.parseInt(StrTool.cTrim(tLDCode1Schema.getCode1()))<StrTool.cTrim(tLJSPaySchema.getBankAccNo()).length()){
                    tempLJSPaySet.add(tLJSPaySchema);
                    tLJSPaySchema.setCanSendBank("1");
                    this.map.put(tLJSPaySchema, "UPDATE");
                }
            }
        }
    }
    String failInfo="";
    for (int i = 1; i <= tempLJSPaySet.size(); i++) {
        if(!failInfo.equals("")){
            failInfo+="、"+tempLJSPaySet.get(i).getOtherNo();
        }else{
            failInfo=tempLJSPaySet.get(i).getOtherNo();
        }
        tLJSPaySet.remove(tempLJSPaySet.get(i));
    }
    if(!failInfo.equals("")){
        failInfo="业务号"+failInfo+"的帐号位数过长，已被锁定";
    }
    this.mResult.add(failInfo);
    return true;
  }

  //数字判断2
  public boolean IsNumeric(String s)
  {
      for (int i=0; i<s.length(); i++) {
          char c = s.charAt(i);
          if (c!=' ') {
              if (c<'0' || '9'<c) {
                  return false ;
              }
          }
  }
  return true;
}
  
	private void checkLJSpaySize(LJSPaySet tLJSPaySet){
		String sql = "select code from ldcode where codetype='sendbankcount'";
		ExeSQL tExeSQL = new ExeSQL();
		String count = tExeSQL.getOneValue(sql);
		if (count == null || count.equals("") || count.equals("0")) {
			return;
		}
		int maxCount;
		try {
			maxCount = Integer.parseInt(count);
		} catch (Exception ex) {
			return;
		}
		if (tLJSPaySet.size() > maxCount) {
			this.manage = "待发送数据已超过" + maxCount + "笔，本次未全部提取，请再次进行提取";
			tLJSPaySet.removeRange(maxCount + 1, tLJSPaySet.size());
		}
		return;
	}
  
	public String getManage() {
		return this.manage;
	}

	public String getSerialNo() {
		return this.serialNo;
	}
  
}

