package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sinosoft.lis.db.LDMedicalComDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToConfirmDB;
import com.sinosoft.lis.db.LYSendToSettleDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToConfirmSchema;
import com.sinosoft.lis.schema.LYSendToSettleSchema;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToConfirmSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行文件转换到数据模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class MedicalDealReturnD{
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 提交数据的容器 */
  private MMap map = new MMap();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();

  //业务数据
  private TransferData inTransferData = new TransferData();
  private GlobalInput inGlobalInput = new GlobalInput();
  
  private LYBankLogSchema outLYBankLogSchema = new LYBankLogSchema();
  private LYReturnFromBankSet outLYReturnFromBankSet = new LYReturnFromBankSet();
  private LYSendToConfirmSet outLYSendToConfirmSet = new LYSendToConfirmSet();
  private LYSendToSettleSet outLYSettleSet = new LYSendToSettleSet();
  /** 返回批次号 */
  private String serialNo = "";
  private String DealType = "";
  /** 选择医保机构编码 */
  private String medicalCode = "";
  
  private String returnStr = "";
  
  private String fileFullName = "";
  protected Document dataDoc = null;
  protected Document resultDoc = null;

  public MedicalDealReturnD() {
  }
  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"READ"和""
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean  submitData(String fileName,String MedicalCode,String serialno,String returnStr,String DealType) {

	  
	//初始化全局变量
	this.medicalCode = MedicalCode;
  	String returnPath = getFileUrl();
  	String folder = PubFun.getCurrentDate2()+ "/"; 
  	String filePath = returnPath + "/" + folder ;
  	this.returnStr = returnStr;
  	
  	if(!newFolder(filePath)){
  		return false;
  	}
  	this.fileFullName =	 filePath + fileName.substring(fileName.lastIndexOf("/"), fileName.indexOf(".xml"))+".z";
  	this.DealType = DealType;
  	this.serialNo = serialno;
	  

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //文件读取标识
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start PubSubmit BLS Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        mResult.clear();
        return false;
      }
      System.out.println("End PubSubmit BLS Submit...");

    return true;
  }
  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  public boolean dealData() {
    try {
    	
    	
    	if (!writeReturnFile(fileFullName,returnStr))throw new Exception("读取银行返回文件失败");	
    	
        if (!readBankFile(fileFullName)) throw new Exception("读取银行返回文件失败");

        //转换xml，公共部分
        if (!xmlTransform()) throw new Exception("转换xml失败");

        //将数据存入数据库，普通银行、银联汇总和银联明细各不相同
        if(!xmlToDatabase())throw new Exception("处理回盘数据失败");
    }
    catch(Exception e) {
      // @@错误处理
      System.out.println(e.getMessage());
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return false;
    }
    return true;
  }

  /**
   * 获取xsl文件路径
   * @return
   */
  public String[] getXslPath() throws Exception {
	  
    LDMedicalComDB tLDMedicalDB = new LDMedicalComDB();
    String[] xslPath;
    tLDMedicalDB.setMedicalComCode(medicalCode);
    if (!tLDMedicalDB.getInfo()) throw new Exception("获取银行XSL描述信息失败！");
    xslPath = PubFun.split(tLDMedicalDB.getChkReceiveF(), ",");
    return xslPath;
  }
  /**
   * 读取银行返回文件
   * @param fileName
   * @return
   */
  protected boolean readBankFile(String fileName) {
    //Declare the document
    dataDoc = buildDocument();
    resultDoc = buildDocument();

    try {
    	
      //读入文件到BUFFER中以提高处理效率
      BufferedReader in = new BufferedReader(
          new FileReader(fileName));

      //将所有文本以行为单位读入到VECTOR中
      String strLine = "";

      //创建根标签
      Element dataRoot = dataDoc.createElement("BANKDATA");

      //循环获取每一行
      while(true) {
        strLine = in.readLine();
        if (strLine == null) break;
        strLine = strLine.trim();
        //去掉空行
        if (strLine.length() < 3) continue;
        System.out.println(strLine);
        //System.out.println("strLen: " + strLine.length());

        //Create the element to hold the row
        Element rowEl = dataDoc.createElement("ROW");

        Element columnEl = dataDoc.createElement("COLUMN");
        columnEl.appendChild(dataDoc.createTextNode(strLine));
        rowEl.appendChild(columnEl);

        //Add the row element to the root
        dataRoot.appendChild(rowEl);
      }

      //Add the root to the document
      dataDoc.appendChild(dataRoot);
//      NodeList tables = dataDoc.getDocumentElement().getChildNodes();
      //System.out.println("tables.getLength():" + tables.getLength());

      //显示XML信息，调试用
      displayDocument(dataDoc);

      in.close();
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      // @@错误处理
      CError.buildErr(this, "读取银行返回文件失败");
      return false;
    }

    return true;
  }
  
  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }


  public static int num = 0;
  public void displayDocument(Node d) {
    num += 2;

    if (d.hasChildNodes()) {
      NodeList nl = d.getChildNodes();

      for (int i=0; i<nl.getLength(); i++) {
        Node n = nl.item(i);

        for (int j=0; j<num; j++) {
          System.out.print(" ");
        }
        if (n.getNodeValue() == null) {
          System.out.println("<" + n.getNodeName() + ">");
        }
        else {
          System.out.println(n.getNodeValue());
        }

        displayDocument(n);

        num -= 2;
//        System.out.println("num:" + num);

        if (n.getNodeValue() == null) {
          for (int j=0; j<num; j++) {
            System.out.print(" ");
          }
          System.out.println("</" + n.getNodeName() + ">");
        }

      }

    }
  }
  
  private boolean getBankLog(){
	  LYBankLogDB bankLogDB = new LYBankLogDB();
	  
	  bankLogDB.setSerialNo(serialNo);
	  if(bankLogDB.getInfo()){
		  outLYBankLogSchema = bankLogDB.getSchema();
		  
		  outLYBankLogSchema.setReturnDate(PubFun.getCurrentDate());
		  outLYBankLogSchema.setInFile("回盘成功#"+fileFullName);
		  outLYBankLogSchema.setReturnOperator("sys");
		  outLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
		  outLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
		  return true;
	  }else{
		  // @@错误处理
		    CError.buildErr(this, "获取银行日志失败！");
			return false;
	  }
	  
	  
  }
  
  public String getFileUrl(){
	  
	  LDSysVarSchema tLDSysVarSchema = new LDSysVarSchema();

      tLDSysVarSchema.setSysVar("ReFromMedicalPath");
      tLDSysVarSchema = tLDSysVarSchema.getDB().query().get(1);
      return tLDSysVarSchema.getSysVarValue();
  }
  public boolean writeReturnFile(String fileName,String returnData){
	  try {
		//保存医保返回全报文
		  String returnFileName = fileName + ".o";
		  string2File(returnData,returnFileName);
		  //存储可以由系统解析的回盘文件
		  returnData = getDelimitedStr(returnData,"\\^")[2];
		  //按$分行
		  String eachData =  returnData.replaceAll("\\$", "\n");
		  System.out.println(eachData);
		  string2File(eachData,fileName);
		  
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
	      // @@错误处理
	    CError.buildErr(this, "存储医保返回文件失败");
		return false;
	}
	  return true;
  }
  /** 
   * 将字符串写入指定文件(当指定的父路径中文件夹不存在时，会最大限度去创建，以保证保存成功！) 
   * 
   * @param res 原字符串 
   * @param filePath 文件路径 
   * @return 成功标记 
   */ 
  public  boolean string2File(String res, String filePath) { 
          boolean flag = true; 
          BufferedReader bufferedReader = null; 
          BufferedWriter bufferedWriter = null; 
          try { 
                  File distFile = new File(filePath); 
                  if (!distFile.getParentFile().exists()) distFile.getParentFile().mkdirs(); 
                  bufferedReader = new BufferedReader(new StringReader(res)); 
                  bufferedWriter = new BufferedWriter(new FileWriter(distFile)); 
                  char buf[] = new char[1024];         //字符缓冲区 
                  int len; 
                  while ((len = bufferedReader.read(buf)) != -1) { 
                          bufferedWriter.write(buf, 0, len); 
                  } 
                  bufferedWriter.flush(); 
                  bufferedReader.close(); 
                  bufferedWriter.close(); 
          } catch (IOException e) { 
                  e.printStackTrace(); 
                  flag = false; 
                  return flag; 
          } finally { 
                  if (bufferedReader != null) { 
                          try { 
                                  bufferedReader.close(); 
                          } catch (IOException e) { 
                                  e.printStackTrace(); 
                          } 
                  } 
          } 
          return flag; 
  }
  public String getReturnFilePath(){
	  return this.fileFullName;
  }
  
  /**
   * newFolder
   * 新建报文存放文件夹，以便对发盘报文查询
   * @return boolean
   */
  public static boolean newFolder(String folderPath)
  {
      String filePath = folderPath.toString();
      File myFilePath = new File(filePath);
      try
      {
          if (myFilePath.isDirectory())
          {
              System.out.println("目录已存在");
              return true;
          }
          else
          {
              myFilePath.mkdir();
              System.out.println("新建目录成功");
              return true;
          }
      }
      catch (Exception e)
      {
          System.out.println("新建目录失败");
          e.printStackTrace();
          return false;
      }
  }
  /**
	 * 把字符串转换成数组
	 * @param originalStr 原字符串
	 * @param matchingStr 分隔字符
	 * @return String[]
	 */
	  public  String[] getDelimitedStr(String originalStr , String matchingStr) {
		  return originalStr.split(matchingStr);
	 }
	  
	  /**
	   * 创建一个xml文档对象DOM
	   * @return
	   */
	  private Document buildDocument() {
	    try {
	      //Create the document builder
	      DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
	      DocumentBuilder docbuilder = dbfactory.newDocumentBuilder();

	      //Create the new document(s)
	      return docbuilder.newDocument();
	    }
	    catch (Exception e) {
	      System.out.println("Problem creating document: " + e.getMessage());
	      return null;
	    }
	  }
	  
	  /**
	   * Simple sample code to show how to run the XSL processor
	   * from the API.
	   */
	  public boolean xmlTransform() {
	    try {
	      String[] arrPath = getXslPath();

	      //非银联或银联的汇总文件，使用第一个描述
	      String xslPath = "";
	      xslPath = arrPath[0];
	      System.out.println("xslPath:" + xslPath);

	      File fStyle = new File(xslPath);
	      Source source = new DOMSource(dataDoc);//源文件
	      Result result = new DOMResult(resultDoc);//目标文件
	      Source style = new StreamSource(fStyle);//转换文件

	      //Create the Transformer
	      TransformerFactory transFactory = TransformerFactory.newInstance();
	      Transformer transformer = transFactory.newTransformer(style);

	      //Transform the Document
	      transformer.transform(source, result);

	      System.out.println("Transform Success!");
	    }
	    catch(Exception e) {
	      e.printStackTrace();
	      System.out.println(e.getMessage());
	      // @@错误处理
	      CError.buildErr(this, "Xml处理失败");
	      return false;
	    }

	    return true;
	  }
	  
  public boolean valiAccInfo(LYSendToSettleSchema send,LYSendToSettleSchema retu){
	  String oldPayCode = send.getPayCode();
	  String newPayCode = retu.getPayCode();
	  if(!oldPayCode.equals(newPayCode)){
		  CError.buildErr(this, "缴费通知书号错误！");
		  return false;
	  }
	  
	  String oldAccNo = send.getAccNo();
	  String newAccNo = retu.getAccNo();
	  if(!oldAccNo.equals(newAccNo)){
		  CError.buildErr(this, "医保个人编号不一致！");
		  return false;
	  }
	  
	  /*String oldAccName = send.getAccName();
	  String newAccName = retu.getAccName();
	  if(!oldAccName.equals(newAccName)){
		  CError.buildErr(this, "医保个人编号不一致！");
		  return false;
	  }*/
	  return true;
  }

  /**
   * 将数据存入数据库
   * @param tLYReturnFromBankSet
   * @return
   */
  private boolean xmlToDatabase() {
    try {
      displayDocument(resultDoc);

      //get all rows
      NodeList rows = resultDoc.getDocumentElement().getChildNodes();

      for(int i=0; i<rows.getLength(); i++){
        //For each row, get the row element and name
        Element thisRow = (Element)rows.item(i);

        //Get the columns for thisRow
        NodeList columns = thisRow.getChildNodes();
        LYSendToSettleSchema tSeToSettle = new LYSendToSettleSchema();
        for(int j=0; j<columns.getLength(); j++){
          Element thisColumn = (Element)columns.item(j);
          String colName = thisColumn.getNodeName();
          String colValue = thisColumn.getFirstChild().getNodeValue();
          System.out.println("colName:" + colName + ":" + colValue);
          tSeToSettle.setV(colName, colValue);
        }
        
        LYSendToSettleDB tSeToSettleDB = new LYSendToSettleDB();
        tSeToSettleDB.setPayCode(tSeToSettle.getPayCode());
        tSeToSettleDB.setSerialNo(serialNo);
        tSeToSettleDB.setDealType(DealType);
        if(!tSeToSettleDB.getInfo()){
        	// @@错误处理
            CError.buildErr(this, "未查询到此结算信息！");
            return false;
        }
        
        LYSendToSettleSchema tSeToSettleOld = tSeToSettleDB.getSchema();
        tSeToSettle.setSerialNo(serialNo);
        tSeToSettle.setDealType(DealType);
        tSeToSettle.setModifyDate(PubFun.getCurrentDate());
        tSeToSettle.setModifyTime(PubFun.getCurrentTime());
        tSeToSettle.setAgentCode(tSeToSettleOld.getAgentCode());
        tSeToSettle.setAccName(tSeToSettleOld.getAccName());
        tSeToSettle.setComCode(tSeToSettleOld.getComCode());
        tSeToSettle.setConvertFlag(tSeToSettleOld.getConvertFlag());
        tSeToSettle.setDealType(tSeToSettleOld.getDealType());
        tSeToSettle.setDoType(tSeToSettleOld.getDoType());
        tSeToSettle.setGetPolDate(tSeToSettleOld.getGetPolDate());
        tSeToSettle.setIDNo(tSeToSettleOld.getIDNo());
        tSeToSettle.setIDType(tSeToSettleOld.getIDType());
        tSeToSettle.setName(tSeToSettleOld.getName());
        tSeToSettle.setMakeDate(tSeToSettleOld.getMakeDate());
        tSeToSettle.setMakeTime(tSeToSettleOld.getMakeTime());
        tSeToSettle.setMedicalCode(tSeToSettleOld.getMedicalCode());
        tSeToSettle.setNoType(tSeToSettleOld.getNoType());
        tSeToSettle.setPayMoney(tSeToSettleOld.getPayMoney());
        tSeToSettle.setPolNo(tSeToSettleOld.getPolNo());
        tSeToSettle.setPayType(tSeToSettleOld.getPayType());
        tSeToSettle.setSendDate(tSeToSettleOld.getSendDate());
        tSeToSettle.setSendTime(tSeToSettleOld.getSendTime());
        tSeToSettle.setOperator("sys");
//        tSeToSettle.setDealDate(PubFun.getCurrentDate());
//        tSeToSettle.setDealTime(PubFun.getCurrentTime());
        
        if(!valiAccInfo(tSeToSettleOld,tSeToSettle)){
          	return false;
          }
        
      //更新代收回盘表的状态
        LYSendToConfirmDB tSettleDB = new LYSendToConfirmDB();
        tSettleDB.setPolNo(tSeToSettle.getPolNo());
        
        LYSendToConfirmSet  tLYSendtoConfrimSet= new LYSendToConfirmSet();
        
        String sql = "select * from lysendtoconfirm where " +
        		"(ConfirmState <> '2') " +
        		"and succflag = '0' " +
        		"and  paycode = '"+tSeToSettle.getPayCode()+"'  " +
        		"and dealtype = 'C'" +
        		" and medicalcode = '"+medicalCode+"' with ur" ;
        System.out.println(sql);
        tLYSendtoConfrimSet = tSettleDB.executeQuery(sql);
        if(tLYSendtoConfrimSet == null || tLYSendtoConfrimSet.size()==0){
      	// @@错误处理
            CError.buildErr(this, "获取确认表数据失败！");
      	    return false;
        }
        LYSendToConfirmSchema tSendToConfirm = tLYSendtoConfrimSet.get(1);
        if("0".equals(tSeToSettle.getSuccFlag())){
      	  tSeToSettle.setRemark("succ");
      	  tSeToSettle.setDealState("1");
      	  tSendToConfirm.setConfirmState("2");
      	  tSendToConfirm.setRemark("settle");
        }else{
      	  tSeToSettle.setRemark("fail");
      	  tSeToSettle.setDealState("0");
      	  tSendToConfirm.setConfirmState("1");
      	  tSendToConfirm.setRemark("fail");
        }
        tSendToConfirm.setModifyDate(PubFun.getCurrentDate());
        tSendToConfirm.setModifyTime(PubFun.getCurrentTime());
        
        outLYSendToConfirmSet.add(tSendToConfirm);
        outLYSettleSet.add(tSeToSettle);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      // @@错误处理
      CError.buildErr(this, "Xml转入数据库处理失败: " + e.getMessage());
      return false;
    }
    if(!getBankLog()){
    	  return false;
      }
    return true;
  }


  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      map.put(outLYSendToConfirmSet, "UPDATE");
      map.put(outLYSettleSet, "UPDATE");
      map.put(outLYBankLogSchema, "UPDATE");
      mInputData.clear();
      mInputData.add(map);
    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }

    return true;
  }

  
  public static void main(String[] args) {
	  MedicalDealReturnD mdr = new MedicalDealReturnD();
	  
	  String path = "E:\\test\\Medical\\test[0].z" ;
	  
//	  mdr.dealData("test[0].z","G","88210001","");
  }
}


