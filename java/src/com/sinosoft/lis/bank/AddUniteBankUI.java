package com.sinosoft.lis.bank;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AddUniteBankUI {
	
	
	
	 /** 传入数据的容器 */
	  private VData mInputData = new VData();
	  /** 传出数据的容器 */
	  private VData mResult = new VData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /** 错误处理类 */
	  public CErrors mErrors = new CErrors();
	  
	  public AddUniteBankUI()
	  {
		  
	  }
	  
	  /**
	     传输数据的公共方法
	     */
	  public boolean submitData(VData cInputData,String cOperate)
	  {
		  //数据操作字符串拷贝到本类中
		  this.mInputData = (VData)cInputData.clone();
		  this.mOperate = cOperate;
		  
		  System.out.println("---AddUniteBank BL BEGIN---");
		  AddUniteBankBL tAddUniteBankBL = new AddUniteBankBL();
		  
		  if(tAddUniteBankBL.submitData(mInputData,mOperate) == false)
		  {
			  //@@错误处理
		      this.mErrors.copyAllErrors(tAddUniteBankBL.mErrors);
		      mResult.clear();
		      mResult.add(mErrors.getFirstError());
		      return false;
		  }
		  else
		  {
			  mResult = tAddUniteBankBL.getResult();
		  }
		  System.out.println("---AddUniteBank BL END---");
		  
		  return true;
	  }
	  
	  
	  /**
	   * 数据输出方法，供外界获取数据处理结果
	   * @return 包含有数据查询结果字符串的VData对象
	   */
	  public VData getResult() {
	    return mResult;
	  }

}
