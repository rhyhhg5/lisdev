package com.sinosoft.lis.bank;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import java.io.*;
import java.lang.reflect.*;
import java.text.DecimalFormat;

import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到文件模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class MedicalDealSTBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    //业务数据
    private LYSendToSettleSet inLYSendToSettleSet = new LYSendToSettleSet();

    private LYBankLogSet inLYBankLogSet = new LYBankLogSet();

    private TransferData inTransferData = new TransferData();

    private LDMedicalComSchema outLDMedicalSchema = new LDMedicalComSchema();

    private String fileName = "";

    private String filePath = "";

    private String bankCode = "";

    private String DealType = "";
    
    private String paramStr = "";

    public MedicalDealSTBLS()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"WRITE"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = true;

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
            return false;
        System.out.println("---End getInputData---");

        //信息保存
        if (this.mOperate.equals("WRITE"))
        {
            //获取文件名和文件路径
            fileName = (String) inTransferData.getValueByName("fileName")
                    + ".xml";
            filePath = fileName.substring(0, fileName.lastIndexOf("/"));
            if(!newFolder(filePath)){
            	return false;
            }
            bankCode = (String) inTransferData.getValueByName("bankCode");
            DealType = (String) inTransferData.getValueByName("DealType");
            //生成送银行文件
            if (!writeBankFile(fileName)){
            	CError.buildErr(this, "读取发盘数据失败");
            	return false;
            }

          //签到
            try {
				GetParamToM.registToMedical();
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println(e1.getMessage());
				CError.buildErr(this, "签到失败");
//				this.mErrors.copyAllErrors(CError);
				return false;
			}
            
            //转换xml
            if (!xmlTransform()){
            	CError.buildErr(this, "转换报文失败");
            	return false;
            }
            
            //保存银行操作日志
         	if(!save()){
         		CError.buildErr(this, "保存发盘文件失败");
				return false;
         	}
            
          //发送到前置机
			SendToPreMacMedical stpmm = new SendToPreMacMedical();
			String response = stpmm.send(paramStr, bankCode);
			if ("".equals(response) || response == null) {
				CError.buildErr(this, "发送前置机处理失败");
				return false;
			} else {// 回盘处理
				String serialno = "";
				if (inLYBankLogSet != null) {
					serialno = inLYBankLogSet.get(1).getSerialNo();
					MedicalDealReturnD rfdcBL = new MedicalDealReturnD();
					if (!rfdcBL.submitData(fileName, bankCode, serialno,
							response, DealType)) {
						CError.buildErr(this, "回盘数据处理失败");
						return false;
					}
				} else {
					return false;
				}
			}

            //更新银行表独立序号
            outLDMedicalSchema.setSchema(getLDMedical());
            System.out.println("---End getLDBank---");

            //保存银行操作日志
//            tReturn = save();
        }


        return tReturn;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            inLYSendToSettleSet = (LYSendToSettleSet) mInputData
                    .getObjectByObjectName("LYSendToSettleSet", 0);
            inLYBankLogSet = (LYBankLogSet) mInputData.getObjectByObjectName(
                    "LYBankLogSet", 0);
            inTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 生成送银行文件
     * @param fileName
     * @return
     */
    private boolean writeBankFile(String fileName)
    {
        OutputStreamWriter out = null;

        try
        {
            //TODO:本地测试
            //fileName = "E:/B093101S00000000000000001315(2007-07-25).xml";
            System.out.println("Start creating file ......");
            System.out.println("File Name : " + fileName);
            
//            out = new PrintWriter(new FileWriter(new File(fileName)), true);
            FileOutputStream fos = new FileOutputStream(new File(fileName)); 
            out = new OutputStreamWriter(new BufferedOutputStream(fos),"gbk");  
            out.write("<?xml version=\"1.0\" encoding=\"GBK\"?>");
            out.write("<?xml-stylesheet type=\"text/xsl\" href=\"BankDataFormat.xsl\"?>");
            out.write("");

            // 生成文件头信息
            out.write("<!--");
            out.write(" * <p>FileName: " + fileName + " </p>");
            out.write(" * <p>Description: 业务系统数据转银行系统文件 </p>");
            out.write(" * <p>Copyright: Copyright (c) 2002</p>");
            out.write(" * <p>Company: sinosoft </p>");
            out.write(" * @Author：Minim's WriteToFileBLS");
            out.write(" * @CreateDate：" + PubFun.getCurrentDate());
            out.write("-->");
            out.write("");

            out.write("<BANKDATA>");

            //增加银行独立序号
            LDMedicalComDB tLDMedicalDB = new LDMedicalComDB();
            tLDMedicalDB.setMedicalComCode(bankCode);
            if (!tLDMedicalDB.getInfo())
            {
                CError.buildErr(this, "获取银行信息（LDBank）失败");
                return false;
            }
            for (int i = 0; i < inLYSendToSettleSet.size(); i++)
            {
                LYSendToSettleSchema tLYSendToSettleSchema = inLYSendToSettleSet
                        .get(i + 1);

                Class c = tLYSendToSettleSchema.getClass();
                Field f[] = c.getDeclaredFields();
                
                LJSPayDB tLJSPayDB=new LJSPayDB();
                tLJSPayDB.setGetNoticeNo(tLYSendToSettleSchema.getPayCode());
                
                out.write("  <ROW>");
                out.write("    <SeqNo>" + tLDMedicalDB.getSeqNo() + "</SeqNo>");
                out.write("    <BankName>" + tLDMedicalDB.getMedicalComName() + "</BankName>");
                out.write("    <DealType>" + this.DealType + "</DealType>");
                if (!StrTool.cTrim(tLYSendToSettleSchema.getPayCode()).equals("") && tLJSPayDB.getInfo()){
                    LAComDB tLAComDB=new LAComDB();
                    tLAComDB.setAgentCom(tLJSPayDB.getAgentCom());
                    if(!StrTool.cTrim(tLJSPayDB.getAgentCom()).equals("") && tLAComDB.getInfo()){
                       out.write("    <BankCom>" + tLAComDB.getBusiLicenseCode() + "</BankCom>");
                    }
                }
                //增加银联代码
                if (tLDMedicalDB.getMedicalUniteFlag() != null
                        && tLDMedicalDB.getMedicalUniteFlag().equals("1"))
                {
                    LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
                    tLDBankUniteDB.setBankUniteCode(bankCode);
                    tLDBankUniteDB.setBankCode(tLYSendToSettleSchema
                            .getMedicalCode());
                    if (!tLDBankUniteDB.getInfo())
                    {
                        CError.buildErr(this, "获取银联信息（LDBankUnite）失败");
                        return false;
                    }
                    out.write("    <UniteBankCode>"
                            + tLDBankUniteDB.getUniteBankCode()
                            + "</UniteBankCode>");
                    out.write("    <UniteBankName>"
                            + tLDBankUniteDB.getUniteBankName()
                            + "</UniteBankName>");
                }

                for (int elementsNum = 0; elementsNum < f.length; elementsNum++)
                {
                    out.write("    <" + f[elementsNum].getName() + ">");
                    
                    if (f[elementsNum].getName().equals("PayMoney"))
                    {
                        //            double d = Double.parseDouble(tLYSendToBankSchema.getV(f[elementsNum].getName()));
                        //            d = Double.parseDouble((new DecimalFormat("0000000000.00")).format(d));
                        out.write((new DecimalFormat("0.00")).format(Double
                                .parseDouble(tLYSendToSettleSchema
                                        .getV(f[elementsNum].getName()))));
                    }
                    else
                    {
                    	//yukun 2018-03-28 新增
                        //如果是沈阳医保的就需要把tempfeeno转换成lccontsub表的symedicaltempfeeno
                    	if (f[elementsNum].getName().equals("PayCode")){
                        	String sql = "select lcs.symedicaltempfeeno "
                        			+" from lccont lc,lccontsub lcs,ljtempfee lj "
                        			+" where "
                        			+" lc.conttype = '1' "
                        			+" and lc.appflag = '1' "
                        			+" and lc.stateflag = '1' "
                        			+" and lc.prtno = lcs.prtno "
                        			+" and lc.contno = lj.otherno "
                        			+" and lj.tempfeetype = '1' "
                        			+" and lj.tempfeeno = '"+tLYSendToSettleSchema.getV(f[elementsNum].getName())+"' "
                        			+" fetch first 1 rows only "
                        			+" with ur ";
                        	System.out.println(sql); 
                      	  	ExeSQL tExeSQL = new ExeSQL();
                            SSRS tSSRS = tExeSQL.execSQL(sql);
                            if("".equals(tSSRS)||null==tSSRS||tSSRS.getMaxRow()<0){
                            	System.out.println("errInof:lccontsub表的symedicaltempfeeno为空！");
                                // @@错误处理
                                CError.buildErr(this, "lccontsub表的symedicaltempfeeno为空！");
                            }
                            out.write(tSSRS.GetText(1,1));
                    	}else{
                        out.write(tLYSendToSettleSchema.getV(f[elementsNum]
                                .getName()));
                        }
                    }

                    out.write("</" + f[elementsNum].getName() + ">");
                }

                out.write("  </ROW>");
                out.write("");

                //out.println(tLYSendToBankSchema.encode());
            }
            out.write("</BANKDATA>");

            out.write("");
            out.write("<!-- Create BankFile Success! -->");
            out.close();
            fos.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Create File Error!");
            return false;
        }

        return true;
    }

    /**
     * 获取xsl文件路径
     * @return
     */
    public String[] getXslPath()throws Exception 
    {
        LDMedicalComDB tLDMedicalDB = new LDMedicalComDB();
        String[] xslPath;
        tLDMedicalDB.setMedicalComCode(bankCode);
        if(!tLDMedicalDB.getInfo()) throw new Exception("获取结算XSL描述信息失败！");
        	String path = tLDMedicalDB.getChkSendF();
        	System.out.println("LDMedicalDB.getChkSendF()："+path);
        	xslPath = PubFun.split(tLDMedicalDB.getChkSendF(), ",");
        	System.out.println("xslPath："+xslPath[0]);
        return xslPath;
    }

    /**
     * Simple sample code to show how to run the XSL processor
     * from the API.
     */
    public boolean xmlTransform()
    {
        // Have the XSLTProcessorFactory obtain a interface to a
        // new XSLTProcessor object.
        try
        {
            System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
            "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
    System.setProperty("javax.xml.parsers.SAXParserFactory",
            "org.apache.xerces.jaxp.SAXParserFactoryImpl");
            //获取转换文件数组
//测试            
			String[] xslPath = getXslPath();
//            String[] xslPath = {"",""};
            //加载源数据（XML文件）
//            fileName = "E:\\test\\Medical\\test.xml";
            File fSource = new File(fileName);
            
            
            //循环每个转换文件，生成目标文件
            String allFileName = "";
            TransformerFactory transFactory = TransformerFactory.newInstance();
            for (int i = 0; i < xslPath.length; i++)
            {
                //沈阳医保本地测试
            	LYSendToSettleSchema tLYSendToSettleSchema = inLYSendToSettleSet.get(1);
            	String tPolNo = tLYSendToSettleSchema.getPolNo();
            	if("SYW".equals(tPolNo.substring(0, 3))){
            		xslPath[i] = "D:\\lisdev\\ui\\bank\\SendToBankFile\\xsl\\send_lnSyMedical_settle.xsl";
            	}else{
            		xslPath[i] = "D:\\lisdev\\ui\\bank\\SendToBankFile\\xsl\\send_lnMedical_settle.xsl";
            	}
            	
                System.out.println("xslPath:" + xslPath[i]);
                File fStyle = new File(xslPath[i]);
                Source style = new StreamSource(fStyle);
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fSource),"gbk"));
                Source source = new StreamSource(reader);
                
                String f = fileName.substring(0, fileName.lastIndexOf("."))
                        + "[" + i + "]" + ".z";
//                if(bankCode!=null && bankCode.trim().equals("0437")){
//                    f = fileName.substring(0, fileName.lastIndexOf("."))
//                    + "[" + i + "]" + ".txt";
//                }
                //纪录银联的多个文件名
                if (allFileName.equals(""))
                {
                    allFileName = allFileName
                            + f.substring(f.lastIndexOf("/") + 1);
                }
                else
                {
                    allFileName = allFileName + ","
                            + f.substring(f.lastIndexOf("/") + 1);
                }
                
                //Create the Transformer
                Transformer transformer = transFactory.newTransformer(style);
                //Transform the Document
                FileOutputStream fos = new FileOutputStream(f); 
                BufferedOutputStream bos=new BufferedOutputStream(fos);
                OutputStreamWriter out = new OutputStreamWriter(bos,"gbk"); 
                Result result = new StreamResult(out);
                transformer.transform(source, result);
                fos.close();
                bos.close();
                out.close();
                reader.close();
                paramStr = readText(f);
                System.out.println(paramStr);
            }
            inLYBankLogSet.get(1).setOutFile("成功发盘#"+allFileName);

            System.out.println("Transform Success!");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("errInof:"+e.getMessage());
            // @@错误处理
            CError.buildErr(this, "Xml处理失败");
            return false;
        }

        return true;
    }

    /**
     * 保存操作
     * @param mInputData
     * @return
     */
    private boolean save()
    {
        boolean tReturn = true;

        //建立数据库连接
        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError.buildErr(this, "数据库连接失败");
            return false;
        }

        try
        {
            //开始事务，锁表
            conn.setAutoCommit(false);

            //记录银行操作日志
            LYBankLogDBSet tLYBankLogDBSet = new LYBankLogDBSet(conn);
            tLYBankLogDBSet.set(inLYBankLogSet);
            if (!tLYBankLogDBSet.update())
            {
                try
                {
                    conn.rollback();
                }
                catch (Exception e)
                {
                }
                conn.close();
                System.out.println("LYBankLog Insert Failed");
                return false;
            }


            conn.commit();
            conn.close();
            System.out.println("End Committed");
        }
        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace();
            CError.buildErr(this, "数据库提交失败");
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {
            }
            tReturn = false;
        }

        return tReturn;
    }

    /**
     * 更新银行表独立序号
     * @return
     */
    private LDMedicalComSchema getLDMedical()
    {
        LDMedicalComDB tLDMedicalDB = new LDMedicalComDB();
        tLDMedicalDB.setMedicalComCode(bankCode);
        if (!tLDMedicalDB.getInfo())
        {
            CError.buildErr(this, "获取银行信息（LDBank）失败");
            return null;
        }

        if (tLDMedicalDB.getSeqNo() == null || tLDMedicalDB.getSeqNo().equals(""))
        {
            tLDMedicalDB.setSeqNo("0");
        }
        else
        {
            tLDMedicalDB.setSeqNo(String.valueOf(Integer.parseInt(tLDMedicalDB
                    .getSeqNo()) + 1));
        }

        return tLDMedicalDB.getSchema();
    }
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
    public String readText(String path){
		StringBuffer inputStr= new  StringBuffer();
		try {
			//读入文件到BUFFER中以提高处理效率
			BufferedReader in = new BufferedReader(new FileReader(path));
			while(true){
				String srtLine = in.readLine();
			    if(srtLine == null)break;
			    inputStr.append(srtLine.trim());
			}
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return inputStr.toString();
	}
    public static void main(String[] args)
    {
        MedicalDealSTBLS writeToFileBLS1 = new MedicalDealSTBLS();
    }
}
