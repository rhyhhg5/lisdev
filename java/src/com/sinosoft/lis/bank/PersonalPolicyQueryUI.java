package com.sinosoft.lis.bank;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PersonalPolicyQueryUI {
	
	public CErrors mErrors = new CErrors();
	
	public PersonalPolicyQueryUI()
    {
        System.out.println("---PersonalPolicyQueryUI---");
    }
	public boolean submitData(VData cInputData)
    {
		PersonalPolicyQueryBL bl = new PersonalPolicyQueryBL();
        if(!bl.submitData(cInputData))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {

    }

}
