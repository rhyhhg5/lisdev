package com.sinosoft.lis.bank;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 亓莹莹
 * function ://邮储代收代付成功失败清单
 * @version 1.0
 * @date 2011-3-01
 */

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.f1print.PremBankPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class YCSuccessListBL {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private PremBankPubFun mPremBankPubFun = new PremBankPubFun();
    //接收的数据
    private String mFlag = "" ;
    private String mTFFlag = "" ;
    private String mComCode = "" ;// 管理机构  comCode
    private VData mInputData = new VData();
    private TransferData mTransferData = new TransferData();
    private int mCount = 0 ;
    //定义需要用到的数据
    private String mSerialNo = "" ;//批次号
    private String mBankCode = "" ;//银行代码
    private String mBankName = "" ;//银行名称
    private String mAccNo = "" ;//业务员编号
    private String mAccName = "" ;//业务员名称
    private String mAccType = "" ;//业务员组别
    private String mPayCode = "" ;//银行账号
    private String mName = "" ;//银行户名
    private String mPolNo = "" ;//缴费通知书
    private String mDealType = "" ;//类别
    private String mPayMoney = "" ; //金额
    private String mSumDuePayMoney = "";//总金额---类型不正确
    private String mSumCount = "";//总笔数
    
    public YCSuccessListBL() 
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
        if (!cOperate.equals("PRINT")) 
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) 
        {
            return false;
        }
        mResult.clear();
        if(!getPrintData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData) 
    {
        mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
        System.out.println("mTransferData------------"+mTransferData);
        mSerialNo = (String) mTransferData.getValueByName("SerialNo");
        System.out.println("mSerialNo-----------"+mSerialNo);
        mBankCode = (String) mTransferData.getValueByName("BankCode");
        mFlag = (String) mTransferData.getValueByName("Flag");
        mComCode = (String) mTransferData.getValueByName("ManageCom");
        mTFFlag = (String) mTransferData.getValueByName("TFFlag");
        mSumCount = (String)mTransferData.getValueByName("SumCount");
        mSumDuePayMoney = (String)mTransferData.getValueByName("SumDuePayMoney");
        System.out.println("mFlag---------------------"+mFlag);
        System.out.println("-------BL-----mTFFlag---------------------"+mTFFlag);
        System.out.println("SumDuePayMoney-----------------"+mSumDuePayMoney);
        System.out.println("comcode------------------------"+mComCode);
        return true;
    }

    public VData getResult() 
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) 
    {
        CError cError = new CError();
        cError.moduleName = "PrintBillRightBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    //查询出首期或者是续期的银行代收正确清单
    private boolean getPrintData() 
    {
        //定义报表相关的类
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        if("T".equals(mTFFlag)) {
            xmlexport.createDocument("YCSuccess.vts","printer");
        }
        if("F".equals(mTFFlag)){
            xmlexport.createDocument("YCFail.vts","printer");
        }
                               
        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");
        //打印成功的清单
        String query_sql = "";
        if("T".equals(mTFFlag))
        {
            query_sql = "select a.agentcode,a.accno,a.accname,a.paycode,a.dealtype,a.paymoney,a.comcode,a.polno"
                +" from lyreturnfrombank a where a.serialno = '"+mSerialNo+"'" 
                +" and a.banksuccflag = '0000'"
                +" and a.comcode like '"+mComCode+"%' union all "
                +"select b.agentcode,b.accno,b.accname,b.paycode,b.dealtype,b.paymoney,b.comcode,b.polno"
                +" from lyreturnfrombankb b where b.serialno = '"+mSerialNo+"'" 
                +" and b.banksuccflag = '0000'"
                +" and b.comcode like '"+mComCode+"%'";
        }
        //失败清单
        if("F".equals(mTFFlag))
        {
            query_sql = "select a.agentcode,a.accno,a.accname,a.paycode,a.dealtype,a.paymoney,a.comcode,a.polno,a.banksuccflag"
                +" from lyreturnfrombank a where a.serialno = '"+mSerialNo+"'" 
                +" and a.banksuccflag != '0000'"
                +" and a.comcode like '"+mComCode+"%' union all "
                +"select b.agentcode,b.accno,b.accname,b.paycode,b.dealtype,b.paymoney,b.comcode,b.polno,b.banksuccflag"
                +" from lyreturnfrombankb b where b.serialno = '"+mSerialNo+"'" 
                +" and b.banksuccflag != '0000'"
                +" and b.comcode like '"+mComCode+"%'";
           
        }
        
        System.out.println("query_sql------------------------"+query_sql);
        ExeSQL main_exesql = new ExeSQL();
        SSRS main_ssrs = main_exesql.execSQL(query_sql);
        System.out.println("main_ssrs---------------------"+main_ssrs.getMaxRow());
        if(main_ssrs.getMaxRow()>0)
        {
            for(int i = 1 ; i <= main_ssrs.getMaxRow() ; i++)
            {
                System.out.println("------------for循环执行");
                System.out.println("----------i--------"+i);
                String[] cols = null;
                if("T".equals(mTFFlag)){
                    cols = new String[10];
                }
                if("F".equals(mTFFlag)){
                    cols = new String[11];
                }
                cols[0] = StrTool.cTrim(main_ssrs.GetText(i, 1)); //业务员编码 agentCode
                cols[3] = StrTool.cTrim(main_ssrs.GetText(i, 2)); //银行账号    accno
                cols[4] = StrTool.cTrim(main_ssrs.GetText(i, 3)); //银行户名    accname
                cols[5] = StrTool.cTrim(main_ssrs.GetText(i, 4));//缴费通知书、暂收收据号   payCode
                cols[7] = StrTool.cTrim(main_ssrs.GetText(i, 6)); //金额   payMoney
                cols[8] = StrTool.cTrim(main_ssrs.GetText(i, 7)); //管理机构   comCode
                cols[9] = StrTool.cTrim(main_ssrs.GetText(i, 8)); //管理机构   comCode
                if("F".equals(mTFFlag)){
                    cols[10] = StrTool.cTrim(main_ssrs.GetText(i, 9));
                    System.out.println("cols[9]----------------------"+cols[9]);
                }
                System.out.println("cols[0]----------------------"+cols[0]);
                System.out.println("cols[3]----------------------"+cols[3]);
                System.out.println("cols[4]----------------------"+cols[4]);
                System.out.println("cols[5]----------------------"+cols[5]);
                System.out.println("cols[7]----------------------"+cols[7]);
                System.out.println("cols[8]----------------------"+cols[8]);
                //查询失败原因
                if("F".equals(mTFFlag)){
                    ExeSQL exeError = new ExeSQL();
                    if(!(StrTool.cTrim(cols[10]).equals("") && cols[0] != null)){
                        String errorReason_sql = "select codename from ldcode1 where code='7703' and code1='"+cols[10]+"' and codetype='bankerror'";
                        SSRS errorReason_ssrs = exeError.execSQL(errorReason_sql);
                        if(errorReason_ssrs.getMaxRow()==0){
                            cols[10]="无";
                        }else{
                            cols[10]=errorReason_ssrs.GetText(1, 1);
                            System.out.println("-------------------失败原因-------------------"+cols[9]);
                        }
                    }
                }
                
                
                //查询业务员名称和业务员组别
                ExeSQL exesql = new ExeSQL();
                if (!(StrTool.cTrim(cols[0]).equals("") || cols[0] == null))
                {
                    System.out.println("查询出代理人的详细信息！！！");
                    //根据代理人的编码查询出代理人的姓名
                    String AgentName_sql = "Select Name,AgentGroup from LAAgent Where AgentCode = '" + cols[0] + "'";
                    SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
                    //查询出代理人组别的名称
                    String AgentGroup_sql = "Select Name from LABranchGroup Where AgentGroup = '"+ AgentName_ssrs.GetText(1, 2) + "'";
                    SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);
                    if (AgentName_ssrs.getMaxRow() == 0)
                    {
                        cols[1] = " ";
                    }
                    else
                    {
                        cols[1] = AgentName_ssrs.GetText(1, 1);
                        System.out.println("代理人的姓名是" + AgentName_ssrs.GetText(1, 1));
                    }
                    if (AgentGroup_ssrs.getMaxRow() == 0)
                    {
                        cols[2] = "无";
                    }
                    else
                    {
                        cols[2] = AgentGroup_ssrs.GetText(1, 1);
                        System.out.println("代理人组别是" + AgentGroup_ssrs.GetText(1, 1));
                       
                    }
                }
                //查询类别
                ExeSQL exesqltype = new ExeSQL();
                SSRS typeSSRS = new SSRS();
                //代付"YF".equals(mFlag)
                String Type_sql = "";
                if("YF".equals(mFlag)){
                    Type_sql ="Select case OtherNoType when '3' then '保全' when '4' then '新契约' when '5' then '理赔'  when '10' then '保全' else '其他' end from LJAGet where ActuGetNo = '"
                       + cols[5] + "'";
                }
                //代收"YS".equals(mFlag)
                if("YS".equals(mFlag)){
                    //代收失败
                    if("F".equals(mTFFlag))
                    {
                        Type_sql = "select case othernoType when '1'  then '新契约' when '2' then '续期' "+
                        "when '4' then '保全' when '7' then '保全' when '8' then '新契约' "+
                        "when '6' then '契约' when '10' then '契约' when '16' then '契约' "+
                        "when 'a' then '契约' " +
                        "else '其他' end from ljspay where getnoticeno='"+cols[5]+"' ";
                    }
                    //代收成功
                    if("T".equals(mTFFlag))
                    {
                        Type_sql = "select case tempfeetype when '1'  then '新契约' when '2' then '续期' "+
                        "when '4' then '保全' when '7' then '保全' when '8' then '新契约' "+
                        "when '6' then '契约' when '10' then '契约' when '16' then '契约' "+
                        "when 'a' then '契约' " +
                        "else '其他' end from ljtempfee where tempfeeno='"+cols[5]+"' ";
                    }
                    
                }
                
                typeSSRS = exesqltype.execSQL(Type_sql);
                if (typeSSRS.getMaxRow()==0)
                {
                   cols[6] = "暂无";
                }
                else
                {
                   cols[6] = typeSSRS.GetText(1, 1);
                }
                
                mCount = mCount + 1 ;
                System.out.println("----------------------------一次循环");
                alistTable.add(cols);
            }
            //根据银行代码查询银行名称
            ExeSQL exesqlBankName = new ExeSQL();
            SSRS bankNameSSRS = new SSRS();
            String query_bankName = "select bankname from ldbank where bankcode= '"+ mBankCode +"'";
            bankNameSSRS = exesqlBankName.execSQL(query_bankName);
            System.out.println("--------------bankNameSSRS--------------"+bankNameSSRS.getMaxRow());
            if(bankNameSSRS.getMaxRow()==0)
            {
                mBankName = " ";
            }
            else{
                mBankName = bankNameSSRS.GetText(1, 1);
            }
            //SF标志
            String tSF = "";
            if("YS".equals(mFlag))
            {
                if("T".equals(mTFFlag))
                {
                    tSF = "邮 储 代 收 正 确";
                }
                if("F".equals(mTFFlag))
                {
                    tSF = "邮 储 代 收 失 败";
                }
            }
            if("YF".equals(mFlag))
            {
                if("T".equals(mTFFlag)){
                    tSF = "邮 储 代 付 正 确";
                }
                if("F".equals(mTFFlag))
                {
                    tSF = "邮 储 代 付 失 败";
                }
            }
            
            if (mCount > 0) 
            {
                System.out.println("开始执行最外部分的循环");
                
                if("T".equals(mTFFlag)){
                    String[] b_col = new String[10];
                    xmlexport.addDisplayControl("displayinfo");
                    xmlexport.addListTable(alistTable, b_col);
                }
                if("F".equals(mTFFlag)){
                    String[] b_col = new String[11];
                    xmlexport.addDisplayControl("displayinfo");
                    xmlexport.addListTable(alistTable, b_col);
                }
                //日期
                String date_sql = "select startdate from lybanklog where serialno = '"+mSerialNo+"'";
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS2 = tExeSQL.execSQL(date_sql);
                String startDate = tSSRS2.GetText(1,1);
                
                texttag.add("BillNo", mSerialNo);
                texttag.add("StartDate", startDate);
                texttag.add("Date", PubFun.getCurrentDate());
                texttag.add("BankCode", mBankCode);
                texttag.add("BankName", mBankName);
                texttag.add("AccNo", mBankName);
                texttag.add("SumDuePayMoney", mSumDuePayMoney);
                texttag.add("SumCount", mSumCount);
                System.out.println("---------BL---------------------------tSF------------"+tSF);
                texttag.add("SF", tSF);
                System.out.println("texttag---------------"+texttag);
                if (texttag.size() > 0) 
                {
                    xmlexport.addTextTag(texttag);
                }
                xmlexport.outputDocumentToFile("d:\\","YCSuccessListBL"); //输出xml文档到文件
                mResult.clear();
                mResult.addElement(xmlexport);
                System.out.println("mResult-----------"+mResult);
                System.out.println("xmlExport-----------"+xmlexport);
                return true;
            } else 
            {
                CError tError = new CError();
                tError.moduleName = "YCSuccessListBL";
                tError.functionName = "submitData";
                tError.errorMessage = "实付表中没有相应的数据信息！";
                this.mErrors.addOneError(tError);
                return false;
                
            }
        }else
        {
            CError tError = new CError();
            tError.moduleName = "YCSuccessBL";
            tError.functionName = "submitData";
            tError.errorMessage = "实付表中没有相应的数据信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
       
   }

}
