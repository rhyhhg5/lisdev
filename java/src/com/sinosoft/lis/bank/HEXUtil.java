package com.sinosoft.lis.bank;

/** 十六进制相关处理助手类 */
public class HEXUtil {
	/**
	 * 十六进制字符串转换为byte数组
	 * @param hexStr	十六进制字符串
	 * @return			目标byte数组
	 */
	public static byte[] toBytes(String hexStr){
		char[] chars = hexStr.toCharArray();
		byte[] res = new byte[chars.length >> 1];
		for (int i = 0, j = 0, len = chars.length; j < len; ++j){
			byte val = (byte)Character.digit(chars[j], 16);
			if ((j & 1) == 0)
				res[i] = (byte)(val << 4);
			else
				res[i++] |= val;
		}
		return res;
	}
	
	private static char[] hexChars = "0123456789abcdef".toCharArray();
	/**
	 * byte数组转换为十六进制字符串
	 * @param buf	byte数组
	 * @return		十六进制字符串
	 */
	public static String toString(byte[] buf){
		if (buf == null)
			return "";
		char[] chars = new char[buf.length << 1];
		for (int i = 0, j = 0, len = buf.length; i < len; ++i){
			chars[j++] = hexChars[fixByte(buf[i]) >> 4];
			chars[j++] = hexChars[fixByte(buf[i]) & 0x0f];
		}
		return new String(chars);
	}
	private static int fixByte(byte b){
		return b < 0 ? 256 + b : b;
	}
}
