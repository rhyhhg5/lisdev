package com.sinosoft.lis.bank;

import com.sinosoft.utility.ExeSQL;

public class RealTimeProcess
{
    //private static Log log = LogFactory.getLog(BatchProcess.class);

    private static String url = "";

    private static String pathCer = "";

    private static String pathPfx = "";

    private static String PfxPass = "";

    public RealTimeProcess(String bankflag, String type)
    {
        String sqlurl = "select codename from ldcode1 where codetype='RealTimeSendBank' and code='RealTimeServerUrl' and code1='"
                + bankflag + "'";
        url = new ExeSQL().getOneValue(sqlurl);

        sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='CerPathR'";
        pathCer = new ExeSQL().getOneValue(sqlurl);
        //pathCer = "E:\\lisdev\\ui\\pdscert\\gzdsf.cer";
        
        sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PfxPath" + type + "'";
        pathPfx = new ExeSQL().getOneValue(sqlurl);
        //pathPfx = "E:\\lisdev\\ui\\pdscert\\ORA@TEST1.pfx";

        sqlurl = "select codename from ldcode1 where codetype='RealTimeSendBank' and code='PfxPass' and code1='"
            + type + bankflag + "'";
        PfxPass = new ExeSQL().getOneValue(sqlurl);

        if (url == null || url.equals("") || pathCer == null
                || pathCer.equals("") || pathPfx == null || pathPfx.equals(""))
        {
            System.out.println("数据发送准备数据不全");
        }
    }

	
	/**
	 * 验证签名信息
	 * @throws CryptException 
	 */
	/*private boolean verifySign(String strXML) {
		//签名(代收付系统JDK环境使用GBK编码，商户使用签名包时需传送GBK编码集)
		com.gnete.security.crypt.Crypt crypt = new com.gnete.security.crypt.Crypt("gbk");
		String pathCer = "D:/ORA/ora/cert/ORA@TEST1.cer";
		int iStart = strXML.indexOf("<SIGNED_MSG>");
		if (iStart != -1) {
			int end = strXML.indexOf("</SIGNED_MSG>");
			String signedMsg = strXML.substring(iStart+12, end);
			System.out.println(signedMsg);
			String strMsg = strXML.substring(0, iStart) + strXML.substring(end+13);
			//调用签名包验签接口(原文,签名,公钥路径)
			try {
				if (crypt.verify(strMsg,signedMsg, pathCer)){
					System.out.println("verify ok");
					return true;
				}
				else {
					System.out.println("verify error");
					return false;
				}
			} catch (CryptException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return true;
	}*/
	
	/**
	 * 数据签名
	 * comment here
	 * @param strData
	 * @return
	 * @throws CryptException 
	 * @since gnete-ora 0.0.0.1
	 */
	/*private String signMsg(String strData) {
		//签名(代收付系统JDK环境使用GBK编码，商户使用签名包时需传送GBK编码集)
		com.gnete.security.crypt.Crypt  crypt = new com.gnete.security.crypt.Crypt("gbk");
		String strMsg = strData.replaceAll("<SIGNED_MSG></SIGNED_MSG>", "");
		//调用签名包签名接口(原文,私钥路径,密码)
		String signedMsg = "";
		try {
			signedMsg = crypt.sign(strMsg, pathPfx, PfxPass);
		} catch (CryptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  strData.replaceAll("<SIGNED_MSG></SIGNED_MSG>", "<SIGNED_MSG>"+signedMsg+"</SIGNED_MSG>");
	}*/
	

    /**
     * 请求
     * comment here
     * @since gnete-ora 0.0.0.1
     */
    public String PayReq(String strSendData)
    {
        System.out.println("---7705---BatchProcess---发送报文 https实时---");
        /*strSendData = this.signMsg(strSendData);*/
        
        String xmlLeft = strSendData.substring(0, strSendData
                .indexOf("<SIGNED_MSG></SIGNED_MSG>"));
        String xmlRight = strSendData.substring(strSendData
                .indexOf("<SIGNED_MSG></SIGNED_MSG>") + 25);
        String xmlKey = "";
        try
        {
            byte[] signed = new FileKeyManager(pathPfx, PfxPass, pathCer)
                    .signeMsg((xmlLeft + xmlRight).getBytes("GBK"));
            xmlKey = HEXUtil.toString(signed);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println("签证信息赋值错误");
            return "";
        }
        strSendData = xmlLeft + "<SIGNED_MSG>" + xmlKey + "</SIGNED_MSG>" + xmlRight;
        System.out.println("发送报文内容" + strSendData);
        

        BatchSendXml tBatchSendXml = new BatchSendXml();
        String strResp = tBatchSendXml.send(strSendData, "7705", url);
        
        //请求报文
        /*HttpClient httpClient = new HttpClient();
        //url
        PostMethod postMethod = new PostMethod(url);
        //设置编码
        httpClient.getParams().setParameter(
                HttpMethodParams.HTTP_CONTENT_CHARSET, "GBK");*/

        /*postMethod.setRequestBody(strSendData);

        String strResp = "";
        try
        {
            long start = System.currentTimeMillis();
            //执行getMethod
            int statusCode = httpClient.executeMethod(postMethod);
            //失败
            if (statusCode != HttpStatus.SC_OK)
            {
                log.error("Method failed: " + postMethod.getStatusLine());
                //读取内容 
                byte[] responseBody = postMethod.getResponseBody();
                //处理内容
                strResp = new String(responseBody, "GBK");
                log.error(strResp);
            }
            else
            {
                //              //读取内容 
                byte[] responseBody = postMethod.getResponseBody();
                //              
                strResp = new String(responseBody, "GBK");
                System.out.println("服务器返回:" + strResp);
                //验签
                if (this.verifySign(strResp))
                {
                    log.info("验签正确，处理服务器返回的报文");
                }
            }
            System.out.println("cost:" + (System.currentTimeMillis() - start));
        }
        catch (HttpException e)
        {
            //发生致命的异常，可能是协议不对或者返回的内容有问题
            log.error("Please check your provided http address!", e);
            e.printStackTrace();
            return "";
        }
        catch (IOException e)
        {
            //发生网络异常
            log.error(e);
            return "";
        }
        catch (Exception e)
        {
            log.error(e);
            return "";
        }
        finally
        {
            //释放连接
            postMethod.releaseConnection();
        }*/
        return strResp;
    }

    public static void main(String[] args)
    {
//        BatchProcess tp = new BatchProcess("7706", "S");
//        tp
//                .PayReq("<?xml version=\"1.0\" encoding=\"GBK\"?><GZELINK><INFO><TRX_CODE>200001</TRX_CODE><VERSION>04</VERSION><DATA_TYPE>2</DATA_TYPE><REQ_SN>aaaaaaaaaaaaaa</REQ_SN><USER_NAME>YSCS002</USER_NAME><USER_PASS>111111</USER_PASS><SIGNED_MSG></SIGNED_MSG></INFO><BODY><QUERY_TRANS><QUERY_SN>449871043010095EOD</QUERY_SN></QUERY_TRANS></BODY></GZELINK>");
    }
}
