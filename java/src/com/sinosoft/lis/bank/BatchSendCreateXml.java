package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDBankUniteSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * @author 张成轩
 * @version 1.0
 */
public class BatchSendCreateXml
{
    private String mUrl = "";

    private String feeType = "";

    private String ubank = "";
    
    private String filePath = "";

    private MMap map = new MMap();

    private VData mInputData = new VData();

    /**
     * @param mUrl 报文存放路径
     * @param feeType 收付费标识
     * @param ubank 银行编码
     */
    public boolean submitData(String mUrl, String feeType, String ubank)
    {
        this.mUrl = mUrl + "/";
        this.feeType = feeType;
        this.ubank = ubank;

        if (!dealDate())
        {
            System.out.println("获取数据失败");
            return false;
        }

        if (!prepareOutputData())
        {
            System.out.println("准备更新日志数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE"))
        {
            //错误处理
            System.out.println("更新发盘日志失败");
            return false;
        }

        return true;
    }

    public boolean dealDate()
    {
        String sql = "select * from lybanklog a where a.dealstate is null and a.senddate is null and a.bankcode in ("
                + ubank
                + ") and logtype='"
                + feeType
                + "' "
                + "and (select count(*) from lysendtobank where serialno=a.serialno)>0 with ur";
        System.out.println(sql);
        LYBankLogDB tLyBankLogDB = new LYBankLogDB();
        LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
        if (tLYBankLogSet == null)
        {
            System.out.println("未查到发送银行数据");
            return false;
        }
        for (int i = 1; i <= tLYBankLogSet.size(); i++)
        {
            LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
            String serialno = tLYBankLogSchema.getSerialNo();
            sql = "select * from lysendtobank where serialno='" + serialno
                    + "' with ur";
            System.out.println(sql);
            LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
            LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB
                    .executeQuery(sql);
            
            // 判断批次下是否有异常的付费数据（并发数据）
            if("F".equals(feeType) && !checkLJFIGet(serialno, tLYBankLogSchema)){
            	// 若有问题，停止该批次的发送，并发送邮件
            	continue;
            }
            // 判断批次下是否有异常的收费数据（并发数据）
            if("S".equals(feeType) && !checkLysendtobank(serialno, tLYBankLogSchema)){
            	// 若有问题，停止该批次的发送，并发送邮件
            	continue;
            }
            if (!createXml(tLYBankLogSchema, tLYSendToBankSet))
            {
            	continue;
            }
        }
        return true;
    }

    public boolean createXml(LYBankLogSchema tLYBankLogSchema,
            LYSendToBankSet tLYSendToBankSet)
    {
        String bankUniteCode = tLYBankLogSchema.getBankCode();
        String type = tLYBankLogSchema.getLogType();

        String sql = "select codename from ldcode1 where codetype='BatchSendBank' and code='UserName' and code1='"
                + type + bankUniteCode + "'";
        String userName = new ExeSQL().getOneValue(sql);
        sql = "select codename from ldcode1 where codetype='BatchSendBank' and code='UserPass' and code1='"
                + type + bankUniteCode + "'";
        String userPass = new ExeSQL().getOneValue(sql);
        sql = "select codename from ldcode1 where codetype='BatchSendBank' and code='MerchantID' and code1='"
                + type + bankUniteCode + "'";
        String merchantID = new ExeSQL().getOneValue(sql);

        Element root = null;
        if (bankUniteCode.equals("7705") || bankUniteCode.equals("7709"))
        {
            root = new Element("GZELINK");
        }
        else if (bankUniteCode.equals("7706"))
        {
            root = new Element("AIPG");
        }

        Document doc = new Document(root);

        Element ele_INFO = new Element("INFO");
        if (type.equals("S"))
        {
            setText(ele_INFO, "TRX_CODE", "100001");
        }
        else
        {
            setText(ele_INFO, "TRX_CODE", "100002");
        }
        if (bankUniteCode.equals("7705") || bankUniteCode.equals("7709"))
        {
            setText(ele_INFO, "VERSION", "04");
        }
        else
        {
            setText(ele_INFO, "VERSION", "03");
        }

        setText(ele_INFO, "DATA_TYPE", "2");
        setText(ele_INFO, "LEVEL", "5");
        setText(ele_INFO, "USER_NAME", userName);
        setText(ele_INFO, "USER_PASS", userPass);
        setText(ele_INFO, "REQ_SN", tLYBankLogSchema.getSerialNo());
        setText(ele_INFO, "SIGNED_MSG", "");
        root.addContent(ele_INFO);

        Element ele_BODY = new Element("BODY");
        Element ele_TRANS_SUM = new Element("TRANS_SUM");
        if (type.equals("S"))
        {
            setText(ele_TRANS_SUM, "BUSINESS_CODE", "10600");
        }
        else
        {
            setText(ele_TRANS_SUM, "BUSINESS_CODE", "00600");
        }
        setText(ele_TRANS_SUM, "MERCHANT_ID", merchantID);
        if (bankUniteCode.equals("7706"))
        {
            setText(ele_TRANS_SUM, "SETTDAY", "");
        }
        setText(ele_TRANS_SUM, "SUBMIT_TIME", PubFun.getCurrentDate2()
                + PubFun.getCurrentTime2());
        setText(ele_TRANS_SUM, "TOTAL_ITEM", Integer.toString(tLYSendToBankSet
                .size()));
        double sum = 0;
        for (int i = 1; i <= tLYSendToBankSet.size(); i++)
        {
            sum = Arith.add(sum, tLYSendToBankSet.get(i).getPayMoney());
        }
        //setText(ele_TRANS_SUM, "TOTAL_SUM", Long.toString(Math.round(sum * 100)));//一分钱问题。。害人啊
        setText(ele_TRANS_SUM, "TOTAL_SUM", Long.toString((long) Arith.mul(
                sum, 100)));//最后权衡还是使用精确类计算，放弃四舍五入
        ele_BODY.addContent(ele_TRANS_SUM);

        Element ele_TRANS_DETAILS = new Element("TRANS_DETAILS");
        DecimalFormat df = new DecimalFormat("0000");
        for (int i = 1; i <= tLYSendToBankSet.size(); i++)
        {
            LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(i);
            Element ele_TRANS_DETAIL = new Element("TRANS_DETAIL");
            setText(ele_TRANS_DETAIL, "SN", df.format(i));
            setText(ele_TRANS_DETAIL, "E_USER_CODE", "");
            String bank = uniteBankCode(tLYSendToBankSchema.getBankCode(),
                    bankUniteCode);
            setText(ele_TRANS_DETAIL, "BANK_CODE", bank);
            setText(ele_TRANS_DETAIL, "ACCOUNT_TYPE", "");
            setText(ele_TRANS_DETAIL, "ACCOUNT_NO", tLYSendToBankSchema
                    .getAccNo());
            setText(ele_TRANS_DETAIL, "ACCOUNT_NAME", tLYSendToBankSchema
                    .getAccName());
            setText(ele_TRANS_DETAIL, "PROVINCE", otherInfo(bank,
                    tLYSendToBankSchema.getComCode(), 1));
            setText(ele_TRANS_DETAIL, "CITY", otherInfo(bank,
                    tLYSendToBankSchema.getComCode(), 2));
            setText(ele_TRANS_DETAIL, "BANK_NAME", "");
            setText(ele_TRANS_DETAIL, "ACCOUNT_PROP", "0");
            setText(ele_TRANS_DETAIL, "AMOUNT", Long.toString((long) Arith
                    .mul(tLYSendToBankSchema.getPayMoney(), 100)));//一分钱问题。。害人啊
            setText(ele_TRANS_DETAIL, "CURRENCY", "");
            setText(ele_TRANS_DETAIL, "PROTOCOL", "");
            setText(ele_TRANS_DETAIL, "PROTOCOL_USERID", "");
            setText(ele_TRANS_DETAIL, "ID_TYPE", getID(tLYSendToBankSchema,
                    bank, type, 1));
            setText(ele_TRANS_DETAIL, "ID", getID(tLYSendToBankSchema, bank,
                    type, 2));
            setText(ele_TRANS_DETAIL, "TEL", "");
            if (bankUniteCode.equals("7705") || bankUniteCode.equals("7709"))
            {
                setText(ele_TRANS_DETAIL, "RECKON_ACCOUNT", "");
            }
            setText(ele_TRANS_DETAIL, "CUST_USERID", "");
            if (bankUniteCode.equals("7706"))
            {
                setText(ele_TRANS_DETAIL, "SETTACCT", "");
            }
            setText(ele_TRANS_DETAIL, "REMARK", tLYSendToBankSchema
                    .getPayCode());
            if (bankUniteCode.equals("7705") || bankUniteCode.equals("7709"))
            {
                setText(ele_TRANS_DETAIL, "RESERVE1", "");
                setText(ele_TRANS_DETAIL, "RESERVE2", "");
            }
            ele_TRANS_DETAILS.addContent(ele_TRANS_DETAIL);
        }
        ele_BODY.addContent(ele_TRANS_DETAILS);
        root.addContent(ele_BODY);

        XMLOutputter XMLOut = new XMLOutputter();
        try
        {
        	this.filePath = this.mUrl + tLYBankLogSchema.getSerialNo() + ".xml";
            XMLOut.setEncoding("GBK");
            XMLOut.output(doc, new FileOutputStream(this.mUrl
                    + tLYBankLogSchema.getSerialNo() + ".xml"));
            String sendStr = readtxt(this.mUrl + tLYBankLogSchema.getSerialNo()
                    + ".xml");
            System.out.println("发送报文内容" + sendStr);

            if (!sendStr.equals(""))
            {
                if (bankUniteCode.equals("7705"))
                {
                    BatchProcess tBatchProcess = new BatchProcess(
                            bankUniteCode, type);
                    String payReq = tBatchProcess.PayReq(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        saveReq(payReq, tLYBankLogSchema);
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema);
                    }
                }
				else if(bankUniteCode.equals("7709")){
                	BatchProcessHigh tBatchProcessHigh = new BatchProcessHigh(
                			bankUniteCode, type);
                	String payReq = tBatchProcessHigh.PayReq(sendStr);
                	 if (!payReq.equals(""))
                     {
                         System.out.println("--返回请求响应--");
                         saveReq(payReq, tLYBankLogSchema);
                     }
                     else
                     {
                         errBankLog(tLYBankLogSchema);
                     }
                }
                else if (bankUniteCode.equals("7706"))
                {
                    BatchProcessCom tBatchProcessCom = new BatchProcessCom(
                            bankUniteCode, type);
                    String payReq = tBatchProcessCom.send(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        saveReq(payReq, tLYBankLogSchema);
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            System.out.println("生成xml失败");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 添加子节点
     * @param element 
     * @param name 节点名称
     * @param value 节点值
     */
    private void setText(Element element, String name, String value)
    {

        Element ele = new Element(name);
        ele.addContent(value);
        element.addContent(ele);
    }

    private String uniteBankCode(String bankcode, String bankunitecode)
    {
        String sql = "select * from ldbankunite where bankcode='" + bankcode
                + "' and bankunitecode='" + bankunitecode + "' with ur";
        System.out.println(sql);
        LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
        LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.executeQuery(sql);
        LDBankUniteSchema tLDBankUniteSchema = tLDBankUniteSet.get(1);
        return tLDBankUniteSchema.getUniteBankCode();
    }

    /**
     * 获取报文省市节点值
     * @param bank 银行代码
     * @param comCode 机构
     * @param othertype 1为省 2为市
     * @return String
     */
    private String otherInfo(String bank, String comCode, int othertype)
    {
        if (othertype == 1)
        {
            if (bank.equals("04105840"))
            {
                return "广东";
            }
            else
            {
                if (comCode.length() >= 4)
                {
                    String sql = "select codename from ldcode where codetype='ComProvince' and code='"
                            + comCode.substring(0, 4) + "'";
                    String com = new ExeSQL().getOneValue(sql);
                    if (com != null)
                    {
                        return com;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "北京";
                }
            }
        }
        else if (othertype == 2 && bank.equals("04105840"))
        {
            return "深圳";
        }
        return "";
    }

    /**
     * 获取身份证节点值
     * @param bank 银行代码
     * @param type 收付费类型
     * @param flag 1为证件类型 2为证件号
     * @return String
     */
    private String getID(LYSendToBankSchema tLYSendToBankSchema, String bank,
            String type, int flag)
    {
        if (type.equals("S")
                && (bank.equals("307") || bank.equals("302")
                        || bank.equals("301") || bank.equals("303")
                        || bank.equals("305") || bank.equals("04105840")))
        {
            if (flag == 1)
            {
                return "0";
            }
            else
            {
                return tLYSendToBankSchema.getIDNo();
            }
        }
        return "";
    }

    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    /**
     * 存储、读取返回报文
     * @param payReq 返回报分字符串
     */
    private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema)
    {
        try
        {
            FileWriter fw = new FileWriter(this.mUrl
                    + tLYBankLogSchema.getSerialNo() + "rq.xml");
            fw.write(payReq);
            fw.close();
            updateLYBankLog(payReq, tLYBankLogSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 连接失败更新日志
     */
    private void errBankLog(LYBankLogSchema tLYBankLogSchema)
    {
        System.out.println("获取响应信息失败");
        tLYBankLogSchema.setOutFile("获取响应信息失败");
        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        this.map.put(tLYBankLogSchema, "UPDATE");
    }

    /**
     * 读取返回报文,更新日志表
     * @param req 返回报分字符串
     */
    private void updateLYBankLog(String req, LYBankLogSchema tLYBankLogSchema)
    {
        int iStart = req.indexOf("<RET_CODE>");
        if (iStart != -1)
        {
            int end = req.indexOf("</RET_CODE>");
            String signedMsg = req.substring(iStart + 10, end);
            System.out.println("响应标记：" + signedMsg);
            if (signedMsg.equals("0000"))
            {
                tLYBankLogSchema.setOutFile("成功发送#" + this.filePath);
                tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());
                tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                System.out.println("成送发送");
            }
            else
            {
                int s_MSG = req.indexOf("<ERR_MSG>");
                if (s_MSG != -1)
                {
                    int e_MSG = req.indexOf("</ERR_MSG>");
                    String signedMSG = req.substring(s_MSG + 10, e_MSG);
                    System.out.println("响应标记：" + signedMSG);
                    if (signedMSG.length() > 50)
                    {
                        tLYBankLogSchema.setOutFile("发送失败,"
                                + signedMSG.substring(0, 40));
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                    else
                    {
                        tLYBankLogSchema.setOutFile(signedMSG);
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                }
                else
                {
                    tLYBankLogSchema.setOutFile("发送失败");
                    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    System.out.println("--发送失败--s_MSG=-1---");
                }
            }
        }
        this.map.put(tLYBankLogSchema, "UPDATE");
        return;
    }

    /**
     * 将发送报文转换为字符串
     * @param path 生成报文路径
     */
    private String readtxt(String path)
    {
        String str = "";
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(path));

            String r = br.readLine();
            while (r != null)
            {
                str += r;
                r = br.readLine();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
        str = str.replaceAll("<SIGNED_MSG />", "<SIGNED_MSG></SIGNED_MSG>");
        return str;
    }
    
    /**
     * 校验批次下是否有异常的付费数据，对于异常数据会发送邮件
     * 
     * @return
     */
    private boolean checkLJFIGet(String serialno, LYBankLogSchema tLYBankLogSchema){
    	// 校验是否有异常付费数据
    	String sql = "select paycode,polno from lysendtobank where serialno='" 
    			+ serialno 
    			+ "' and exists (select 1 from ljfiget where actugetno=lysendtobank.paycode)"
		        + "  union all select paycode,polno from lysendtobank ly where serialno='"
		        + serialno 
		        + "' and exists (select 1 from lysendtobank  where paycode=ly.paycode and serialno <>ly.serialno )";
		        
    	ExeSQL tEx = new ExeSQL();
    	SSRS tSSRS = tEx.execSQL(sql);
    	if(tSSRS.getMaxRow() == 0){
    		return true;
    	} else if(!"1".equals(tLYBankLogSchema.getOthFlag())){
    		// 批次没有发送过邮件
    		// 发送邮件
//    		MailSender tMailSender = new MailSender("zhangchengxuan", "zhangcx3");
    		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
					.GetText(1, 2), "picchealth");
    		// 设置邮件发送信息
    		String sendInf = "集中代收付" + serialno + "批次出现异常付费数据，已停止该批次的发送，请核查批次内信息。";
    		for(int row = 1; row <= tSSRS.getMaxRow(); row++){
    			sendInf = sendInf + "付费号：" + tSSRS.GetText(row, 1) + "，业务号:" + tSSRS.GetText(row, 2) + "。";
    		}
    		tMailSender.setSendInf("集中代收付异常批次", sendInf, null);
    		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
    		tMailSender.setToAddress("7705/7706");
    		if (!tMailSender.sendMail()) {
    			tMailSender.getErrorMessage();
    		}
    		// 更新日志表输出信息
    		tLYBankLogSchema.setOutFile("批次下存在异常付费数据，发送失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
            // 已发送邮件标记
            tLYBankLogSchema.setOthFlag("1");
            this.map.put(tLYBankLogSchema, "UPDATE");
    		return false;
    	} else {
    		return false;
    	}
    }
    
    /**
     * 校验批次下是否有异常的付费数据，对于异常数据会发送邮件
     * 
     * @return
     */
    private boolean checkLysendtobank(String serialno, LYBankLogSchema tLYBankLogSchema){
    	// 校验是否有异常付费数据
    	String sql = "select paycode,polno from lysendtobank sb where serialno='" 
    			+ serialno 
    			+ "' and  exists (select 1 from lysendtobank  where paycode=sb.paycode and serialno <>sb.serialno )"
    			+ " union all select paycode,polno from lysendtobank where serialno='"
    			+ serialno 
    			+ "' and  exists (select 1 from ljtempfee  where paycode=ljtempfee.tempfeeno and confmakedate is not null )" 
    			+ " union all select paycode,polno from lysendtobank sb where serialno='"
    			+ serialno 
    			+ "' and notype='9' and exists (select 1 from lccont where prtno=sb.polno and appntname<>sb.accname) "
    			+ " union select paycode,polno from lysendtobank sb where serialno='"
    			+ serialno 
    			+ "' and exists (select 1 from ljtempfeeclass where sb.paycode=tempfeeno and paymode<>'4' ) "
    			;
    	ExeSQL tEx = new ExeSQL();
    	SSRS tSSRS = tEx.execSQL(sql);
    	if(tSSRS.getMaxRow() == 0){
    		return true;
    	} else if(!"1".equals(tLYBankLogSchema.getOthFlag())){
    		// 批次没有发送过邮件
    		// 发送邮件
//    		MailSender tMailSender = new MailSender("zhangchengxuan", "zhangcx3");
    		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
					.GetText(1, 2), "picchealth");
    		// 设置邮件发送信息
    		String sendInf = "集中代收付" + serialno + "批次出现异常收费数据，已停止该批次的发送，请核查批次内信息。";
    		for(int row = 1; row <= tSSRS.getMaxRow(); row++){
    			sendInf = sendInf + "收费号：" + tSSRS.GetText(row, 1) + "，业务号:" + tSSRS.GetText(row, 2) + "。";
    		}
    		tMailSender.setSendInf("集中代收付异常批次", sendInf, null);
    		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
    		tMailSender.setToAddress("7705/7706");
    		if (!tMailSender.sendMail()) {
    			tMailSender.getErrorMessage();
    		}
    		// 更新日志表输出信息
    		tLYBankLogSchema.setOutFile("批次下存在异常收费数据，发送失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
            // 已发送邮件标记
            tLYBankLogSchema.setOthFlag("1");
            this.map.put(tLYBankLogSchema, "UPDATE");
    		return false;
    	} else {
    		return false;
    	}
    }
    
    public static void main(String[] arr)
    {
    	LYBankLogSchema banklog = new LYBankLogSchema();
    	LYBankLogDB banklogDB = new LYBankLogDB();
    	banklogDB.setSerialNo("77052387000000149967");
    	if(banklogDB.getInfo()){
    		banklog = banklogDB.getSchema();
    	}
    	System.out.println(banklog.getBankCode());
    	LYSendToBankSet sendtobank = new LYSendToBankSet();
    	LYSendToBankDB	sendtobankDB = new LYSendToBankDB();
    	String sql = "select * from lysendtobank where serialno='77052387000000149967' with ur";
    	sendtobank = sendtobankDB.executeQuery(sql);
    	
    	BatchSendCreateXml bscx = new BatchSendCreateXml();
    	bscx.filePath = "E:\\";
    	bscx.mUrl = "E:\\";
    	bscx.createXml(banklog, sendtobank);
    }
}
