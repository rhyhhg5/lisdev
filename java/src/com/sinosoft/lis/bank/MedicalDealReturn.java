package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sinosoft.lis.db.LDMedicalComDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.lis.vschema.LYSendToConfirmSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行文件转换到数据模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public abstract class MedicalDealReturn {
  /** 传入数据的容器 */
	protected VData mInputData = new VData();
  /** 传出数据的容器 */
  protected VData mResult = new VData();
  /** 提交数据的容器 */
  protected MMap map = new MMap();
  /** 数据操作字符串 */
  protected String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  protected LYReturnFromBankSet outLYReturnFromBankSet = new LYReturnFromBankSet();

  //回盘文件全路径
  protected String fileFullName = "";

  protected String DealType = "";
  /** 选择银行编码 */
  protected String medicalCode = "";
  protected String serialNo = "";

  protected Document dataDoc = null;
  protected Document resultDoc = null;


  public MedicalDealReturn() {
  }

  /**
   * 创建一个xml文档对象DOM
   * @return
   */
  private Document buildDocument() {
    try {
      //Create the document builder
      DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docbuilder = dbfactory.newDocumentBuilder();

      //Create the new document(s)
      return docbuilder.newDocument();
    }
    catch (Exception e) {
      System.out.println("Problem creating document: " + e.getMessage());
      return null;
    }
  }

  /**
   * 获取xsl文件路径
   * @return
   */
  public String[] getXslPath() throws Exception {
	  
    LDMedicalComDB tLDMedicalDB = new LDMedicalComDB();
    String[] xslPath;
    tLDMedicalDB.setMedicalComCode(medicalCode);
    if (!tLDMedicalDB.getInfo()) throw new Exception("获取银行XSL描述信息失败！");
    if(StrTool.cTrim(this.DealType).equals("G")){
      xslPath = PubFun.split(tLDMedicalDB.getAgentGetReceiveF(), ",");
    }else if(StrTool.cTrim(this.DealType).equals("P")){
      xslPath = PubFun.split(tLDMedicalDB.getAgentPayReceiveF(), ",");
    }else if(StrTool.cTrim(this.DealType).equals("C")){
      xslPath = PubFun.split(tLDMedicalDB.getAgentGetSavePath(), ",");
    }else{
      xslPath = PubFun.split(tLDMedicalDB.getAgentPaySavePath(), ",");
    }
    return xslPath;
  }


  /**
   * Simple sample code to show how to run the XSL processor
   * from the API.
   */
  public boolean xmlTransform() {
    try {
      String[] arrPath = getXslPath();

      //非银联或银联的汇总文件，使用第一个描述
      String xslPath = "";
      xslPath = arrPath[0];
      System.out.println("xslPath:" + xslPath);

      File fStyle = new File(xslPath);
      Source source = new DOMSource(dataDoc);//源文件
      Result result = new DOMResult(resultDoc);//目标文件
      Source style = new StreamSource(fStyle);//转换文件

      //Create the Transformer
      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer(style);

      //Transform the Document
      transformer.transform(source, result);

      System.out.println("Transform Success!");
    }
    catch(Exception e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      // @@错误处理
      CError.buildErr(this, "Xml处理失败");
      return false;
    }

    return true;
  }


  /**
   * 将数据存入数据库
   * @param tLYReturnFromBankSet
   * @return
   */
  protected boolean xmlToDatabase() {
	  return true;
  }



  /**
   * 读取银行返回文件
   * @param fileName
   * @return
   */
  protected boolean readBankFile(String fileName) {
    //Declare the document
    dataDoc = buildDocument();
    resultDoc = buildDocument();

    try {
    	
      //读入文件到BUFFER中以提高处理效率
      BufferedReader in = new BufferedReader(
          new FileReader(fileName));

      //将所有文本以行为单位读入到VECTOR中
      String strLine = "";

      //创建根标签
      Element dataRoot = dataDoc.createElement("BANKDATA");

      //循环获取每一行
      while(true) {
        strLine = in.readLine();
        if (strLine == null) break;
        strLine = strLine.trim();
        //去掉空行
        if (strLine.length() < 3) continue;
        System.out.println(strLine);
        //System.out.println("strLen: " + strLine.length());

        //Create the element to hold the row
        Element rowEl = dataDoc.createElement("ROW");

        Element columnEl = dataDoc.createElement("COLUMN");
        columnEl.appendChild(dataDoc.createTextNode(strLine));
        rowEl.appendChild(columnEl);

        //Add the row element to the root
        dataRoot.appendChild(rowEl);
      }

      //Add the root to the document
      dataDoc.appendChild(dataRoot);
//      NodeList tables = dataDoc.getDocumentElement().getChildNodes();
      //System.out.println("tables.getLength():" + tables.getLength());

      //显示XML信息，调试用
      displayDocument(dataDoc);

      in.close();
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      // @@错误处理
      CError.buildErr(this, "读取银行返回文件失败");
      return false;
    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  public LYReturnFromBankSet dealData(String fileName,String MedicalCode,String serialno,String returnStr,String DealType) {
    try {
    	this.medicalCode = MedicalCode;
    	String returnPath = getFileUrl();
    	String folder = PubFun.getCurrentDate2()+ "/"; 
    	String filePath = returnPath  + folder ;
    	if(!newFolder(filePath)){
    		return null;
    	}
    	this.fileFullName =	 filePath + fileName.substring(fileName.lastIndexOf("/"), fileName.indexOf(".xml"))+".z";
    	this.DealType = DealType;
    	this.serialNo = serialno;
    	
    	
    	if (!writeReturnFile(fileFullName,returnStr))throw new Exception("读取银行返回文件失败");	
    	
        if (!readBankFile(fileFullName)) throw new Exception("读取银行返回文件失败");

        //转换xml，公共部分
        if (!xmlTransform()) throw new Exception("转换xml失败");

        //将数据存入数据库，普通银行、银联汇总和银联明细各不相同
        if(!xmlToDatabase())throw new Exception("处理回盘数据失败");
    }
    catch(Exception e) {
      // @@错误处理
      System.out.println(e.getMessage());
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return null;
    }

    return outLYReturnFromBankSet;
  }


  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }


  public static int num = 0;
  public void displayDocument(Node d) {
    num += 2;

    if (d.hasChildNodes()) {
      NodeList nl = d.getChildNodes();

      for (int i=0; i<nl.getLength(); i++) {
        Node n = nl.item(i);

        for (int j=0; j<num; j++) {
          System.out.print(" ");
        }
        if (n.getNodeValue() == null) {
          System.out.println("<" + n.getNodeName() + ">");
        }
        else {
          System.out.println(n.getNodeValue());
        }

        displayDocument(n);

        num -= 2;
//        System.out.println("num:" + num);

        if (n.getNodeValue() == null) {
          for (int j=0; j<num; j++) {
            System.out.print(" ");
          }
          System.out.println("</" + n.getNodeName() + ">");
        }

      }

    }
  }
  public String getFileUrl(){
	  
	  LDSysVarSchema tLDSysVarSchema = new LDSysVarSchema();

      tLDSysVarSchema.setSysVar("ReFromMedicalPath");
      tLDSysVarSchema = tLDSysVarSchema.getDB().query().get(1);
      return tLDSysVarSchema.getSysVarValue();
  }
  public boolean writeReturnFile(String fileName,String returnData){
	  try {
		//保存医保返回全报文
		  String returnFileName = fileName + ".o";
		  string2File(returnData,returnFileName);
		  //存储可以由系统解析的回盘文件
		  returnData = getDelimitedStr(returnData,"\\^")[2];
		  //按$分行
		  String eachData =  returnData.replaceAll("\\$", "\n");
		  System.out.println(eachData);
		  string2File(eachData,fileName);
		  
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
	      // @@错误处理
	    CError.buildErr(this, "存储医保返回文件失败");
		return false;
	}
	  return true;
  }
  /** 
   * 将字符串写入指定文件(当指定的父路径中文件夹不存在时，会最大限度去创建，以保证保存成功！) 
   * 
   * @param res 原字符串 
   * @param filePath 文件路径 
   * @return 成功标记 
   */ 
  public  boolean string2File(String res, String filePath) { 
          boolean flag = true; 
          BufferedReader bufferedReader = null; 
          BufferedWriter bufferedWriter = null; 
          try { 
                  File distFile = new File(filePath); 
                  if (!distFile.getParentFile().exists()) distFile.getParentFile().mkdirs(); 
                  bufferedReader = new BufferedReader(new StringReader(res)); 
                  bufferedWriter = new BufferedWriter(new FileWriter(distFile)); 
                  char buf[] = new char[1024];         //字符缓冲区 
                  int len; 
                  while ((len = bufferedReader.read(buf)) != -1) { 
                          bufferedWriter.write(buf, 0, len); 
                  } 
                  bufferedWriter.flush(); 
                  bufferedReader.close(); 
                  bufferedWriter.close(); 
          } catch (IOException e) { 
                  e.printStackTrace(); 
        	      // @@错误处理
          	    CError.buildErr(this, "把报文存入文件失败！");
                  flag = false; 
                  return flag; 
          } finally { 
                  if (bufferedReader != null) { 
                          try { 
                                  bufferedReader.close(); 
                          } catch (IOException e) { 
                                  e.printStackTrace(); 
                          } 
                  } 
          } 
          return flag; 
  }
  public String getReturnFilePath(){
	  return this.fileFullName;
  }
  
  /**
   * newFolder
   * 新建报文存放文件夹，以便对发盘报文查询
   * @return boolean
   */
  public static boolean newFolder(String folderPath)
  {
      String filePath = folderPath.toString();
      File myFilePath = new File(filePath);
      try
      {
          if (myFilePath.isDirectory())
          {
              System.out.println("目录已存在");
              return true;
          }
          else
          {
              myFilePath.mkdir();
              System.out.println("新建目录成功");
              return true;
          }
      }
      catch (Exception e)
      {
          System.out.println("新建目录失败");
          e.printStackTrace();
          return false;
      }
  }
  /**
	 * 把字符串转换成数组
	 * @param originalStr 原字符串
	 * @param matchingStr 分隔字符
	 * @return String[]
	 */
	  public  String[] getDelimitedStr(String originalStr , String matchingStr) {
		  return originalStr.split(matchingStr);
	 }
	 
}


