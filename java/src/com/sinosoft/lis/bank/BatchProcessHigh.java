package com.sinosoft.lis.bank;

import com.sinosoft.utility.ExeSQL;

public class BatchProcessHigh {

    //private static Log log = LogFactory.getLog(BatchProcess.class);

    private static String url = "";

    private static String pathCer = "";

    private static String pathPfx = "";

    private static String PfxPass = "";

    public BatchProcessHigh(String bankflag, String type)
    {
        String sqlurl = "select codename from ldcode1 where codetype='BatchSendBank' and code='BatchServerUrl' and code1='"
                + bankflag + "'";
        url = new ExeSQL().getOneValue(sqlurl);
        //url="F:\\test\\";

        sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='CerPath'";
        pathCer = new ExeSQL().getOneValue(sqlurl);

        sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PfxPath" + type + "'";
        pathPfx = new ExeSQL().getOneValue(sqlurl);
       // pathPfx="F:\\test\\";

        sqlurl = "select codename from ldcode1 where codetype='BatchSendBank' and code='PfxPass' and code1='"
            + type + bankflag + "'";
        PfxPass = new ExeSQL().getOneValue(sqlurl);

        if (url == null || url.equals("") || pathCer == null
                || pathCer.equals("") || pathPfx == null || pathPfx.equals(""))
        {
            System.out.println("数据发送准备数据不全");
        }
    }
    /**
     * 数据签名
     * comment here
     * @param strData
     * @return
     * @since gnete-ora 0.0.0.1
     */
    private String signMsg(String strData)
    {
        String strRnt = "";
        //签名
        Crypt crypt = new Crypt();
        String strMsg = strData.replaceAll("<SIGNED_MSG></SIGNED_MSG>", "");
        System.out.println("签名原文:" + strMsg);
        System.out.println(pathPfx);
        if (crypt.SignMsg(strMsg, pathPfx, PfxPass))
        {
            String signedMsg = crypt.getLastSignMsg();
            strRnt = strData.replaceAll("<SIGNED_MSG></SIGNED_MSG>",
                    "<SIGNED_MSG>" + signedMsg + "</SIGNED_MSG>");
            System.out.println("签名信息:" + signedMsg);
            System.out.println("请求交易报文:" + strRnt);
        }
        else
        {
            strRnt = strData;
        }
        return strRnt;
    }

    /**
     * 请求
     * comment here
     * @since gnete-ora 0.0.0.1
     */
    public String PayReq(String strSendhigh)
    {
        System.out.println("---7709---BatchProcess---发送报文---");
        String xmlLeft = strSendhigh.substring(0, strSendhigh
                .indexOf("<SIGNED_MSG></SIGNED_MSG>"));
        String xmlRight = strSendhigh.substring(strSendhigh
                .indexOf("<SIGNED_MSG></SIGNED_MSG>") + 25);
        String xmlKey = "";
        try
        {
            byte[] signed = new FileKeyManager(pathPfx, PfxPass, pathCer)
                    .signeMsg((xmlLeft + xmlRight).getBytes("GBK"));
            xmlKey = HEXUtil.toString(signed);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println("签证信息赋值错误");
            return "";
        }
        strSendhigh = xmlLeft + "<SIGNED_MSG>" + xmlKey + "</SIGNED_MSG>" + xmlRight;
        System.out.println("发送报文内容" + strSendhigh);
        System.out.println("url : " + url);
        //ADD　by JL End
        BatchSendXml tBatchSendXml = new BatchSendXml();
        String strResp = tBatchSendXml.send(strSendhigh, "7709", url);
        return strResp;
    }

    public static void main(String[] args)
    {
    }
}
