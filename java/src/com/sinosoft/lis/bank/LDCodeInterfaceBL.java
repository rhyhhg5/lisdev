package com.sinosoft.lis.bank;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LDCodeInterfaceSet;
import com.sinosoft.lis.schema.LDCodeInterfaceSchema;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: Notebook</p>
 *
 * <p>Description: TeamNotebook</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LDCodeInterfaceBL {
    public LDCodeInterfaceBL() {
    }

    /** 传入参数 */
    private VData mInputData;
    /** 传入操作符 */
    private String mOperate;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 报错存储对象 */
    public CErrors mErrors = new CErrors();
    /** 最后保存结果 */
    private VData mResult = new VData();
    /** 最后递交Map */
    private MMap map = new MMap();
    /** Schema*/
    private LDCodeInterfaceSchema mLDCodeInterfaceSchema = null;
    /** Set*/
    private LDCodeInterfaceSet mLDCodeInterfaceSet = null;

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        //System.out.println("into BriefCardSignBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            System.out.println("ERROR When getInputData!");
            return false;
        }
        if (!checkData()) {
            System.out.println("ERROR When CheckInputData!");
            return false;
        }
        if (!dealData()) {
            System.out.println("ERROR When DealInputData!");
            return false;
        }
        if (!prepareOutputData()) {
            System.out.println("ERROR When prepareOutputData!");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mInputData, this.mOperate)) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        //System.out.println("BriefCardSignBL finished...");
        return true;
    }

    /**
     * 获取数据
     *
     * @return boolean
     */
    private boolean getInputData() {
        //System.out.println("into BriefCardSignBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLDCodeInterfaceSet = (LDCodeInterfaceSet) mInputData.
                              getObjectByObjectName("LDCodeInterfaceSet",
                0);

        return true;
    }

    /**
     * 效验数据
     *
     * @return boolean
     */
    private boolean checkData() {
        //System.out.println("into BriefCardSignBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为空，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }

        if (mLDCodeInterfaceSet == null) {
            String str = "为空的传入数据!";
            buildError("checkData", str);
            return false;
        }
        return true;
    }

    /**
     * 处理数据
     *
     * @return boolean
     */
    private boolean dealData() {

        if (this.mOperate.equals("INSERT||MAIN")) {
            //当操作为插入时
            for(int i = 1; i <= mLDCodeInterfaceSet.size(); i++) {
                mLDCodeInterfaceSchema = mLDCodeInterfaceSet.get(i);
                mLDCodeInterfaceSchema.setOperator(this.mGlobalInput.Operator);
                mLDCodeInterfaceSchema.setComCode(this.mGlobalInput.ComCode);
                PubFun.fillDefaultField(mLDCodeInterfaceSchema);
                mLDCodeInterfaceSet.set(i, mLDCodeInterfaceSchema);
            }
             map.put(mLDCodeInterfaceSet, "INSERT");
        } else if (this.mOperate.equals("UPDATE||MAIN")) {
            //当操作为更新时，加入最近更新时间
            for(int i = 1; i <= mLDCodeInterfaceSet.size(); i++) {
                mLDCodeInterfaceSchema = mLDCodeInterfaceSet.get(i);
                mLDCodeInterfaceSchema.setOperator(this.mGlobalInput.Operator);
                mLDCodeInterfaceSchema.setComCode(this.mGlobalInput.ComCode);
                mLDCodeInterfaceSchema.setModifyDate(PubFun.getCurrentDate());
                mLDCodeInterfaceSchema.setModifyTime(PubFun.getCurrentTime());
                mLDCodeInterfaceSet.set(i, mLDCodeInterfaceSchema);
            }
            map.put(mLDCodeInterfaceSet, "UPDATE");
        } else if (this.mOperate.equals("DELETE||MAIN")) {
            //当操作为删除时，直接将动作装入map
            map.put(mLDCodeInterfaceSet, "DELETE");
        }
        //System.out.println("into BriefCardSignBL.dealData()...");
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        //System.out.println("into BriefCardSignBL.prepareOutputData()...");
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLDCodeInterfaceSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLDCodeInterfaceSet);
            MMap tMMap = new MMap();
            tMMap = (MMap) mInputData.getObjectByObjectName("MMap", 0);
            if(tMMap == null) {
                System.out.println("Data package error!");
            }
            System.out.println(tMMap.getAllObjectByObjectName("LDCodeInterfaceSet", 0).toString());
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDOccupationBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefCardSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调试用主函数
     * @param args String[]
     */
    public static void main(String[] args) {
        LDCodeInterfaceSchema tLDCodeInterfaceSchema = new
                LDCodeInterfaceSchema();

        LDCodeInterfaceSet LSet = new LDCodeInterfaceSet();
        tLDCodeInterfaceSchema.setSysName("SysName");
        tLDCodeInterfaceSchema.setCodeName("fff");
        tLDCodeInterfaceSchema.setCodeTypeNmae("2");
        tLDCodeInterfaceSchema.setCode("ddd");
        tLDCodeInterfaceSchema.setInterfaceCode("4");
        tLDCodeInterfaceSchema.setCodeType("fff");
        LSet.add(tLDCodeInterfaceSchema);
        GlobalInput tGi = new GlobalInput();
        tGi.ManageCom = "86110000";
        tGi.Operator = "endor";
        VData tVData = new VData();
        tVData.add(tGi);
        tVData.add(LSet);
        LDCodeInterfaceBL LB = new LDCodeInterfaceBL();
        if (LB.submitData(tVData, "DELETE||MAIN")) {
            System.out.println("跑通了！");
        } else {
            System.out.println("没跑通！");
        }
    }
}
