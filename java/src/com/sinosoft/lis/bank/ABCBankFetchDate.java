package com.sinosoft.lis.bank;

import com.sinosoft.lis.operfee.NewIndiDueFeeMultiUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.taskservice.BatchSendDataTask;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ABCBankFetchDate
{
	private String startDate;
	private String endDate;
	private String flag;

	public ABCBankFetchDate()
	{

	}

	public boolean CreatDate(String flag)
	{

		this.flag = flag;
		if (!getInputData())
		{

			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData())
		{
			return false;
		}
		System.out.println("---End dealData---");
		return true;
	}

	private boolean getInputData()
	{
		try
		{

			startDate = "2012-1-1"; // 提取数据的其实日期
			endDate = PubFun.getCurrentDate();
		} catch (Exception e)
		{
			// @@错误处理
			CError.buildErr(this, "接收数据失败");
			return false;
		}
		return true;
	}

	private boolean dealData()
	{
		ExeSQL tExeSQL = new ExeSQL();
		try
		{
			// 调用数据提取类
			{
				System.out.println("---农行调用数据提取类---7708");
				getSendbankData("7708", flag);
			}
		} catch (Exception ex)
		{
			System.out.println("---农行调用数据提取类报错---7708");
			ex.printStackTrace();
			return false;
		}

		SSRS tSSRS2 = new SSRS();

		try
		{
			System.out.println("---调用数据审核类---7708");
			tSSRS2 = tExeSQL
					.execSQL("select substr(comcode,1,4) from ldcom where sign='1' and comcode not in ('86','86000000')  group by substr(comcode,1,4) ");
			if (flag.equals("S"))
			{
				// 按机构分别生成批次
				for (int j = 1; j <= tSSRS2.MaxRow; j++)
				{
					
					try
					{
						{
							getSendBankConf("7708", "S", tSSRS2.GetText(j, 1), "1");
							getSendBankConf("7708", "S", tSSRS2.GetText(j, 1), "2");
						}
					} catch (Exception ex)
					{
						System.out.println("---农行调用数据提取类报错---7708");
						ex.printStackTrace();
					}

					

				}
			} else{
				
				for (int j = 1; j <= tSSRS2.MaxRow; j++)
				{
					
					try
					{
						{
							getSendBankConf("7708", "F", tSSRS2.GetText(j, 1), "1");
							getSendBankConf("7708", "F", tSSRS2.GetText(j, 1), "2");
						}
					} catch (Exception ex)
					{
						System.out.println("---农行调用数据提取类报错---7708");
						ex.printStackTrace();
					}

					

				}
			}

		} catch (Exception ex)
		{
			System.out.println("---数据审核类报错---TT----");
			ex.printStackTrace();
		}

		return true;
	}

	private boolean getSendbankData(String bankcode, String flag)
	{
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = "86";
		tGlobalInput.ComCode = "86";
		tGlobalInput.Operator = "sys";

		if ("S".equals(flag))
		{
			LCPolSchema tLCPolSchema = new LCPolSchema(); // 个人保单表
			System.out.println("StartDate:" + startDate);
			tLCPolSchema.setGetStartDate(startDate); // 将判断条件设置在起领日期字段中
			tLCPolSchema.setPayEndDate(endDate); // 将判断条件设置在终交日期字段中

			TransferData transferData2 = new TransferData();
			transferData2.setNameAndValue("bankCode", bankcode);
			VData tVData2 = new VData();
			tVData2.add(tLCPolSchema);
			tVData2.add(tGlobalInput);
			tVData2.add(transferData2);
			NewIndiDueFeeMultiUI tNewIndiDueFeeMultiUI = new NewIndiDueFeeMultiUI();
			tNewIndiDueFeeMultiUI.submitData(tVData2, "INSERT");

			if (tNewIndiDueFeeMultiUI.mErrors.needDealError())
			{
				System.out.print("催收处理失败，不能生成银行发送数据，原因是:");

				for (int n = 0; n < tNewIndiDueFeeMultiUI.mErrors
						.getErrorCount(); n++)
				{
					System.out
							.print(tNewIndiDueFeeMultiUI.mErrors.getError(n).errorMessage
									+ "|");
				}
			} else
			{
				System.out.println("催收处理成功！");
			}

			// 生成银行数据
			System.out.println("\n---BatchSendBankSave Start---");
			ABCBankFetchDateUI tABCBankFetchDateUI = new ABCBankFetchDateUI();

			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("startDate", startDate);
			transferData1.setNameAndValue("endDate", endDate);
			transferData1.setNameAndValue("bankCode", bankcode);

			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(tGlobalInput);

			if (!tABCBankFetchDateUI.submitData(tVData, "GETMONEY"))// 收费
			{
				VData rVData = tABCBankFetchDateUI.getResult();
				System.out.println("提取数据失败：" + (String) rVData.get(0));
				return false;
			} else
			{
				System.out.println("提取数据成功 ^-^ ");
			}
		} else if ("F".equals(flag))
		{
			// 生成银行数据
			System.out.println("\n---BatchSendBankSave Start---");
			ABCBankFetchDateUI tABCBankFetchDateUI = new ABCBankFetchDateUI();

			TransferData transferData1 = new TransferData();
			transferData1.setNameAndValue("startDate", startDate);
			transferData1.setNameAndValue("endDate", endDate);
			transferData1.setNameAndValue("bankCode", bankcode);

			VData tVData = new VData();
			tVData.add(transferData1);
			tVData.add(tGlobalInput);

			if (!tABCBankFetchDateUI.submitData(tVData, "PAYMONEY"))// 付费
			{
				VData rVData = tABCBankFetchDateUI.getResult();
				System.out.println("提取数据失败：" + (String) rVData.get(0));
			} else
			{
				System.out.println("提取数据成功 ^-^ ");
			}
		}
		return true;
	}

	private boolean getSendBankConf(String bankcode, String flag,
			String managecom, String banktype)
	{
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ManageCom = managecom;
		tGlobalInput.ComCode = managecom;
		tGlobalInput.Operator = "sys";

		System.out.print("---SendBankConfSave Start---");

		String buttonflag = "Yes";

		// flag 为 1时 为代收
		if ("S".equals(flag))
		{
			VData tVData = new VData();
			tVData.add(new LJSPaySet());
			tVData.add(tGlobalInput);
			tVData.add(buttonflag);
			tVData.add(bankcode);
			tVData.add(banktype);

			ABCBankFetchDateConf tABCBankFetchDateConf = new ABCBankFetchDateConf();
			if (!tABCBankFetchDateConf.submitData(tVData, "WRITE"))
			{
				VData rData = tABCBankFetchDateConf.getResult();
				//System.out.println("处理失败，原因是:" + (String) rData.get(0));
				return false;
			} else
			{
				System.out.println("数据确认成功 ^-^ ");
			}
		} else if ("F".equals(flag))
		{
			VData tVData = new VData();
			tVData.add(new LJAGetSet());
			tVData.add(tGlobalInput);
			tVData.add(buttonflag);
			tVData.add(bankcode);
			tVData.add(banktype);
			ABCBankFetchDateConfF tABCBankFetchDateConfF = new ABCBankFetchDateConfF();
			if (!tABCBankFetchDateConfF.submitData(tVData, "WRITE"))
			{
				VData rData = tABCBankFetchDateConfF.getResult();
				System.out.println("处理失败，原因是:" + (String) rData.get(0));
				return false;
			} else
			{
				System.out.println("数据确认成功 ^-^ ");
			}
		}
		return true;
	}

	public static void main(String[] args)
	{
		BatchSendDataTask tBatchSendDataTask = new BatchSendDataTask();
		tBatchSendDataTask.run();
	}
}
