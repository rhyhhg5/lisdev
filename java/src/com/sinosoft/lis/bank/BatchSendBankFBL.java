package com.sinosoft.lis.bank;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BatchSendBankFBL
{

    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 返回数据的容器 */
    private VData mResult = new VData();

    /** 提交数据的容器 */
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //业务数据
    /** 提数开始日期 */
    private String startDate = "";

    /** 提数结束日期 */
    private String endDate = "";

    /** 银行编码 */
    private String bankCode = "";

    private LJAGetSet outLJAGetSet = new LJAGetSet();
    
    private LJAGetSet oLJAGetSet = new LJAGetSet();

    public BatchSendBankFBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
            return false;
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
            return false;
        System.out.println("---End dealData---");

        //银行代收
        if (mOperate.equals("PAYMONEY"))
        {
            //准备往后台的数据
            if (!prepareOutputData())
                return false;
            System.out.println("---End prepareOutputData---");

            System.out.println("Start PubSubmit BLS Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("End PubSubmit BLS Submit...");
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            TransferData tTransferData = (TransferData) mInputData
                    .getObjectByObjectName("TransferData", 0);
            startDate = (String) tTransferData.getValueByName("startDate");
            endDate = (String) tTransferData.getValueByName("endDate");
            bankCode = (String) tTransferData.getValueByName("bankCode");

            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 获取交费日期在设置的日期区间内的应付总表记录
     * @param startDate
     * @param endDate
     * @return
     */
    private LJAGetSet getLJAGetByPaydate(String startDate, String endDate,
            String bankCode)
    {
        String tSql = "";
        String tSql2 = "";

        //不指定开始时间
        if (startDate.equals(""))
        {
            //规则：最早应收日期<=结束时间，最晚交费日期>结束时间
            //     银行编码匹配，机构编码向下匹配，不在途，有账号
            //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
        	//#2191 社保通查勘费报销问题：按原始需求，仅支持现金结算
            tSql = "select * from LJAGet where "
                    + "  ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank = '0' or CanSendBank is null )  and SumGetMoney<>0 "
                    + " and (BankAccNo <> '') and (AccName <> '') "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7701') and a.bankcode=LJAGet.bankcode) "
                    + " and othernotype not in ('3','5','23') with ur ";

            tSql2 = "select * from LJAGet where "
                    + "  ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank = '0' or CanSendBank is null )  and SumGetMoney<>0 "
                    + " and (BankAccNo <> '') and (AccName <> '') "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7701') and a.bankcode=LJAGet.bankcode) "
                    + " and othernotype = '5' and Exists (Select 1 From Llcase b Where Otherno = b.Caseno) "
                    + " and othernotype <>'23'"
                    + " union select * from LJAGet where "
                    + "  ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank = '0' or CanSendBank is null )  and SumGetMoney<>0 "
                    + " and (BankAccNo <> '') and (AccName <> '') "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7701') and a.bankcode=LJAGet.bankcode) "
                    + " and othernotype = '5' and exists (select 1 from llregister lr,llcase lc,llhospcase lh where lr.rgtno=lc.rgtno and lc.caseno=lh.caseno and lr.rgtno=ljaget.otherno and lh.casetype='04' and lr.mngcom like '8695%')"
                    + " and ManageCom like '8695%' "
                    + " and othernotype <>'23' with ur";
        }
        else
        {
            tSql = "select * from LJAGet where "
                    + " ShouldDate >= '"
                    + startDate
                    + "'"
                    + " and ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank is null or  CanSendBank = '0')"
                    + " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7701') and a.bankcode=LJAGet.bankcode) "
                    + " and othernotype not in ('3','5','23') with ur ";

            tSql2 = "select * from LJAGet where "
                    + " ShouldDate >= '"
                    + startDate
                    + "'"
                    + " and ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank is null or  CanSendBank = '0')"
                    + " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7701') and a.bankcode=LJAGet.bankcode) "
                    + " and othernotype = '5' and Exists (Select 1 From Llcase b Where Otherno = b.Caseno) "
                    + " and othernotype <>'23'"
                    + " union  select * from LJAGet where "
                    + " ShouldDate >= '"
                    + startDate
                    + "'"
                    + " and ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank is null or  CanSendBank = '0')"
                    + " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7701') and a.bankcode=LJAGet.bankcode) "
                    + " and othernotype = '5' and exists (select 1 from llregister lr,llcase lc,llhospcase lh where lr.rgtno=lc.rgtno and lc.caseno=lh.caseno and lr.rgtno=ljaget.otherno and lh.casetype='04' and lr.mngcom like '8695%')"
                    + " and ManageCom like '8695%' "
                    + " and othernotype <>'23' with ur ";
        }

        System.out.println(tSql);

        LJAGetDB tLJAGetDB = new LJAGetDB();
        LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(tSql);
        tLJAGetSet.add(tLJAGetDB.executeQuery(tSql2));

        return tLJAGetSet;
    }

    private LJAGetSet getLJAGet(String startDate, String endDate)
    {
        LJAGetSet tLJAGetSet = new LJAGetSet();

        //获取银行信息，校验是否是银联
        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(bankCode);
        if (!tLDBankDB.getInfo())
        {
            CError.buildErr(this, "获取银行信息（LDBank）失败");
            return null;
        }

        //普通银行
        if (tLDBankDB.getBankUniteFlag() == null
                || tLDBankDB.getBankUniteFlag().equals("0"))
        {
            tLJAGetSet = getLJAGetByPaydate(startDate, endDate, bankCode);
        }
        //银联
        else if (tLDBankDB.getBankUniteFlag().equals("1"))
        {
            //LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
            //tLDBankUniteDB.setBankUniteCode(bankCode);  //需要区分代收代付
            //LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.query();

            //        unitegroupcode 1代表支持代收 2代表支持代付 3代表支持代收代付
//            String bankSql = "select * from ldbankunite where bankunitecode='"
//                    + bankCode + "' and unitegroupcode in ('2','3') ";
//            System.out.println(bankSql);
//            LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB
//                    .executeQuery(bankSql);
//
//            if (tLDBankUniteSet.size() == 0)
//            {
//                CError.buildErr(this, "获取银联相关银行信息（LDBankUnite）失败");
//                return null;
//            }
           String ljagetsql="select distinct bankcode from LJAGet where "
        	   			 +"ShouldDate >= '"+startDate+"'"
        	   			 +"and ShouldDate <= '"+endDate+"'"
        	   			 +"and exists(select bankcode from ldbankunite b where  b.bankunitecode='"+bankCode+"'  and b.unitegroupcode in ('2','3') and b.bankcode=LJAGet.bankcode)"
        	   			 +"and ManageCom like '86%'"
        	   			 +"and PayMode='4'"
        	   			 +"and EnterAccDate is null and ConfDate is null"
        	   			 +"and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
        	   			 +"and (CanSendBank is null or  CanSendBank = '0')"
        	   			 +"and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
        	   			 +"and bankcode not in ('489102','999100') "
        	   			 +"and not exists (select bankcode from ldbankunite a where a.bankunitecode in ('7703','7704','7701') and a.bankcode=LJAGet.bankcode) "
        	   			 +"and othernotype not in ('3','23') with ur ";
           LJAGetSet inLJAGetSet = new LJAGetSet();
           LJAGetDB  inLJAGetDB=new LJAGetDB();
           inLJAGetSet=inLJAGetDB.executeQuery(ljagetsql);
            for (int i = 0; i < inLJAGetSet.size(); i++)
            {
                tLJAGetSet.add(getLJAGetByPaydate(startDate, endDate,
                		inLJAGetSet.get(i + 1).getBankCode()));
            }
        }

        return tLJAGetSet;
    }

    /**
     * 修改应收表银行在途标志,记录发送银行次数
     * @param tLJSPaySet
     * @return
     */
    private LJAGetSet modifyBankFlag(LJAGetSet tLJAGetSet, boolean flag)
    {
        String bankOnTheWay = "";
        String canSend = "";
        if(flag == true){
            bankOnTheWay = "1";
            canSend = "8";
        } else {
            bankOnTheWay = "0";
            canSend = "1";
        }
        for (int i = 0; i < tLJAGetSet.size(); i++)
        {
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

            tLJAGetSchema.setBankOnTheWayFlag(bankOnTheWay);
            tLJAGetSchema.setCanSendBank(canSend);
            tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
            tLJAGetSchema.setOperator(mGlobalInput.Operator);
            //记录发送银行次数
            tLJAGetSet.set(i + 1, tLJAGetSchema);
        }

        return tLJAGetSet;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            if (mOperate.equals("PAYMONEY"))
            {

                LJAGetSet tLJAGetSet = getLJAGet(startDate, endDate);
                if (tLJAGetSet == null) {
                    throw new NullPointerException("总应付表处理失败！");
                }
                if (tLJAGetSet.size() == 0) {
                    throw new NullPointerException("总应付表无数据！");
                }
                if (!CheckLJAGet(tLJAGetSet, bankCode))
                {
                    return false;
                }
                System.out.println("---End getLJSPayByPaydate---");

                //修改应收表银行在途标志,记录发送银行次数
                tLJAGetSet = modifyBankFlag(tLJAGetSet ,true);
                System.out.println("---End modifyBankFlag---");

                outLJAGetSet.set(tLJAGetSet);
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "数据处理错误:" + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            map.put(oLJAGetSet, "UPDATE");
            map.put(outLJAGetSet, "UPDATE");

            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public boolean CheckLJAGet(LJAGetSet tLJAGetSet, String bankcode)
    {
        String failInfo = "";
        LJAGetSet oLJAGetSet = new LJAGetSet();
        LDCodeDB tLDCodeDB = new LDCodeDB();
        String sql = "select * from ldcode where codetype='accnocheckF' and code='"
                + bankcode + "'";
        LDCodeSet tLDCodeSet = tLDCodeDB.executeQuery(sql);
        if (tLDCodeSet != null && tLDCodeSet.size() > 0)
        {
            String checkReg = tLDCodeSet.get(1).getCodeName();
            for (int row = 1; row <= tLJAGetSet.size(); row++)
            {
                String accno = tLJAGetSet.get(row).getBankAccNo();
                if (!accno.trim().matches(checkReg))
                {
                    oLJAGetSet.add(tLJAGetSet.get(row));
                }
            }
        }
        for (int i = 1; i <= oLJAGetSet.size(); i++)
        {
            if (!failInfo.equals(""))
            {
                failInfo += "、" + oLJAGetSet.get(i).getOtherNo();
            }
            else
            {
                failInfo = oLJAGetSet.get(i).getOtherNo();
            }
            tLJAGetSet.remove(oLJAGetSet.get(i));
        }
        if (!failInfo.equals(""))
        {
            failInfo = "业务号" + failInfo + "的帐号格式错误，已被锁定";
        }
        this.oLJAGetSet.set(modifyBankFlag(oLJAGetSet, false));
        this.mResult.add(failInfo);
        return true;
    }

    public static void main(String[] args)
    {

    }
}