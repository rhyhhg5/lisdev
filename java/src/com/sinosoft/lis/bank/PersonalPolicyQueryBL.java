package com.sinosoft.lis.bank;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;

public class PersonalPolicyQueryBL {
	
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();
	private GlobalInput mGI = null;
	
	private String mSql1 = null;
	private String mSql2 = null;
	private String mSql3 = null;

	private String mOutXmlPath = null;

	private String startDate = "";
	private String endDate = "";
	private String manageCom = "";
	private String idNo="";

	//private String mCurDate = PubFun.getCurrentDate();

	SSRS tSSRS = new SSRS();

	SSRS tComSSRS = new SSRS();
	SSRS tComSSRS2 = new SSRS();
	SSRS tComSSRS3 = new SSRS();

	public boolean submitData(VData cInputData) {
		
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		return true;
	}
	
	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);

		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "PersonalPolicyQueryBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		startDate = (String) tf.getValueByName("StartDate");
		endDate = (String) tf.getValueByName("EndDate");
		manageCom = (String) tf.getValueByName("ManageCom");
		idNo=(String) tf.getValueByName("IDNo");

		if (startDate == null || endDate == null || mOutXmlPath == null
				|| manageCom == null||idNo==null) {
			CError tError = new CError();
			tError.moduleName = "PersonalPolicyQueryBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		return true;
	}
	
	private boolean checkData() {
		return true;
	}
	
	private boolean dealData() {
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println("BL->dealDate()");
		//投保人
		mSql1 = "select "
				+ "lcc.managecom, "
				+ "(select name from ldcom where comcode = lcc.managecom),"
				+ "lcp.riskcode,"
				+ "(select riskname from lmriskapp where riskcode = lcp.riskcode), "
				+ "lcp.prem, lcc.signdate, lcc.contno, lcc.AppntName, lcc.AppntIDNo, "
				+ "lcc.AccName, lcc.BankAccNo "
				+ "from lccont lcc, lcpol lcp where 1=1 "
				+ "and lcc.contno = lcp.contno "
				+ "and lcc.managecom like '"+ manageCom + "%'" 
				+ "and lcc.signdate between '" + startDate + "' and '" + endDate + "' "
				+ "and lcc.AppntIDNo like '" + idNo + "%' with ur";
		//被保人
		mSql2="select "
				+ "lcc.managecom, "
				+ "(select name from ldcom where comcode = lcc.managecom), "
				+ "lcp.riskcode, "
				+ "(select riskname from lmriskapp where riskcode = lcp.riskcode), "
				+ "lcp.prem, lcc.signdate, lcc.contno, lcc.InsuredName, lcc.InsuredIDNo "
				+ "from lccont lcc, lcpol lcp where 1=1 "
				+ "and lcc.contno = lcp.contno "
				+ "and lcc.managecom like '"+ manageCom + "%' "
				+ "and lcc.signdate between '" + startDate + "' and '" + endDate + "' "
				+ "and lcc.InsuredIDNo like '" + idNo + "%' with ur";
		//受益人
		mSql3="select "
				+ "lcc.managecom, "
				+ "(select name from ldcom where comcode = lcc.managecom), "
				+ "lcp.riskcode, "
				+ "(select riskname from lmriskapp where riskcode = lcp.riskcode), "
				+ "lcp.prem, lcc.signdate, lcc.contno, lcb.Name, lcb.IDNo "
				+ "from lccont lcc, lcpol lcp, LCBnf lcb where 1=1 "
				+ "and lcc.contno = lcp.contno "
				+ "and lcc.contno = lcb.contno "
				+ "and lcc.managecom like '"+ manageCom + "%' "
				+ "and lcc.signdate between '" + startDate + "' and '" + endDate + "' "
				+ "and lcb.IDNo like '" + idNo + "%' with ur ";
		System.out.println(mSql1);
		System.out.println(mSql2);
		System.out.println(mSql3);
		System.out.println(mOutXmlPath);
		tComSSRS = tExeSQL.execSQL(mSql1);
		tComSSRS2= tExeSQL.execSQL(mSql2);
		tComSSRS3= tExeSQL.execSQL(mSql3);

		if (tExeSQL.mErrors.needDealError()) {
			System.out.println(tExeSQL.mErrors.getErrContent());

			CError tError = new CError();
			tError.moduleName = "PersonalPolicyQueryBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		if (tComSSRS.getMaxRow() == 0 && tComSSRS2.getMaxRow() == 0 && tComSSRS3.getMaxRow() == 0) {

			CError tError = new CError();
			tError.moduleName = "PersonalPolicyQueryBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有符合条件的数据，请重新输入条件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		String[][] mToExcel = new String[tComSSRS.getMaxRow() + 10][11];

		mToExcel[0][0] = "管理机构";
		mToExcel[0][1] = "管理机构名称";
		mToExcel[0][2] = "险种编码";
		mToExcel[0][3] = "险种名称";
		mToExcel[0][4] = "保费金额";
		mToExcel[0][5] = "签单日期";
		mToExcel[0][6] = "保单号";
		mToExcel[0][7] = "投保人姓名";
		mToExcel[0][8] = "投保人身份证号";
		mToExcel[0][9] = "缴费账户户名";
		mToExcel[0][10] = "缴费账户号";
		
		String[][] mToExcel2=new String[tComSSRS2.getMaxRow() + 10][9];
		
		mToExcel2[0][0] = "管理机构";
		mToExcel2[0][1] = "管理机构名称";
		mToExcel2[0][2] = "险种编码";
		mToExcel2[0][3] = "险种名称";
		mToExcel2[0][4] = "保费金额";
		mToExcel2[0][5] = "签单日期";
		mToExcel2[0][6] = "保单号";
		mToExcel2[0][7] = "被保人姓名";
		mToExcel2[0][8] = "被保人身份证号";
		
		String[][] mToExcel3=new String[tComSSRS2.getMaxRow() + 10][9];
		mToExcel3[0][0] = "管理机构";
		mToExcel3[0][1] = "管理机构名称";
		mToExcel3[0][2] = "险种编码";
		mToExcel3[0][3] = "险种名称";
		mToExcel3[0][4] = "保费金额";
		mToExcel3[0][5] = "签单日期";
		mToExcel3[0][6] = "保单号";
		mToExcel3[0][7] = "受益人姓名";
		mToExcel3[0][8] = "受益人身份证号";

		try {
			//投保人
			System.out.println(tComSSRS.getMaxRow());
			for (int comnum = 1; comnum <= tComSSRS.getMaxRow(); comnum++) {

				mToExcel[comnum][0] = tComSSRS.GetText(comnum, 1);
				mToExcel[comnum][1] = tComSSRS.GetText(comnum, 2);
				mToExcel[comnum][2] = tComSSRS.GetText(comnum, 3);
				mToExcel[comnum][3] = tComSSRS.GetText(comnum, 4);
				mToExcel[comnum][4] = tComSSRS.GetText(comnum, 5);
				mToExcel[comnum][5] = tComSSRS.GetText(comnum, 6);
				mToExcel[comnum][6] = tComSSRS.GetText(comnum, 7);
				mToExcel[comnum][7] = tComSSRS.GetText(comnum, 8);
				mToExcel[comnum][8] = tComSSRS.GetText(comnum, 9);
				mToExcel[comnum][9] = tComSSRS.GetText(comnum, 10);
				mToExcel[comnum][10] = tComSSRS.GetText(comnum, 11);
			}
			//被保人
			System.out.println(tComSSRS2.getMaxRow());
			for (int comnum = 1; comnum <= tComSSRS2.getMaxRow(); comnum++) {

				mToExcel2[comnum][0] = tComSSRS2.GetText(comnum, 1);
				mToExcel2[comnum][1] = tComSSRS2.GetText(comnum, 2);
				mToExcel2[comnum][2] = tComSSRS2.GetText(comnum, 3);
				mToExcel2[comnum][3] = tComSSRS2.GetText(comnum, 4);
				mToExcel2[comnum][4] = tComSSRS2.GetText(comnum, 5);
				mToExcel2[comnum][5] = tComSSRS2.GetText(comnum, 6);
				mToExcel2[comnum][6] = tComSSRS2.GetText(comnum, 7);
				mToExcel2[comnum][7] = tComSSRS2.GetText(comnum, 8);
				mToExcel2[comnum][8] = tComSSRS2.GetText(comnum, 9);
			}
			//受益人
			System.out.println(tComSSRS3.getMaxRow());
			for (int comnum = 1; comnum <= tComSSRS3.getMaxRow(); comnum++) {

				mToExcel3[comnum][0] = tComSSRS3.GetText(comnum, 1);
				mToExcel3[comnum][1] = tComSSRS3.GetText(comnum, 2);
				mToExcel3[comnum][2] = tComSSRS3.GetText(comnum, 3);
				mToExcel3[comnum][3] = tComSSRS3.GetText(comnum, 4);
				mToExcel3[comnum][4] = tComSSRS3.GetText(comnum, 5);
				mToExcel3[comnum][5] = tComSSRS3.GetText(comnum, 6);
				mToExcel3[comnum][6] = tComSSRS3.GetText(comnum, 7);
				mToExcel3[comnum][7] = tComSSRS3.GetText(comnum, 8);
				mToExcel3[comnum][8] = tComSSRS3.GetText(comnum, 9);
			}
			
		} catch (Exception ex) {
			CError tError = new CError();
			tError.moduleName = "PersonalPolicyQueryBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询信息失败！";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		try {

			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { "投保人信息","被保人信息","受益人信息" };
			t.addSheetGBK(sheetName);
			t.setData(0, mToExcel);
			t.setData(1, mToExcel2);
			t.setData(2, mToExcel3);
			t.write(mOutXmlPath);
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}
	
}
