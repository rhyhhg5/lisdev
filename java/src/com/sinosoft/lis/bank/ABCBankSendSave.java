package com.sinosoft.lis.bank;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ABCBankSendSave
{

	private MMap mmap = new MMap();

	private String DealType = null;

	private String ComCode = null;

	private String BankCode = null;

	private PubSubmit tPubSubmit = new PubSubmit();

	private int limite = 0;

	// 传入数据的容器
	private VData mInputData = new VData();

	private boolean ReturnFileSave(TransferData transferData)
	{

		try
		{
			DealType = (String) transferData.getValueByName("DealType");
			ComCode = (String) transferData.getValueByName("ComCode");
			BankCode = (String) transferData.getValueByName("BankCode");
		} catch (Exception e)
		{
			CError.buildErr(this, "接收数据失败");
			return false;
		}

		return true;

	}

	public boolean dealData(TransferData transferData)
	{

		ReturnFileSave(transferData);

		String sql_limit = "select code1 from ldcode1 where codetype='batchsendlimite' and code='limite'";
		ExeSQL limiteSql = new ExeSQL();
		String limiteCont = limiteSql.getOneValue(sql_limit);
		try
		{
			limite = Integer.parseInt(limiteCont);
		} catch (NumberFormatException e)
		{
			e.printStackTrace();
		}

		// 生成LYSendToBank
		if ("S".equals(DealType))
		{
			SendBankConfBL tSendBankConf = new SendBankConfBL();
			// 查询出cansendbank状态为9的数据
			LJSPayDB tLJSPayDB = new LJSPayDB();
			String sql1 = "select * from ljspay a where cansendbank = '9' and BankOnTheWayFlag = '1' and  bankcode in "
					+ "(select bankcode from ldbankunite where bankunitecode='"
					+ BankCode
					+ "' ) and "
					+ "not exists(select 1 from lysendtobank where a.getnoticeno = paycode ) and managecom like  '"
					+ ComCode + "%'";
			LJSPaySet tLJSPaySet = new LJSPaySet();
			// 测试用
			LJSPaySet tLJSPaySet1 = tLJSPayDB.executeQuery(sql1);
			System.out.println("总长度" + tLJSPaySet1.size());
			int tStartS = 1;
			tLJSPaySet = tLJSPayDB.executeQuery(sql1, tStartS, limite);
			System.out.println("第一个收费批次的数据量" + tLJSPaySet.size());
			do
			{
				LYBankLogSet outLYBankLogSet = new LYBankLogSet();
				LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();
				if (tLJSPaySet != null && tLJSPaySet.size() > 0)
				{
					LYSendToBankSet tLYSendToBankSet = tSendBankConf
							.getSendToBank(tLJSPaySet, BankCode);
					if (tLYSendToBankSet.size() != 0)
					{
						String serialno = tLYSendToBankSet.get(1).getSerialNo();
						LYBankLogSchema tLYBankLogSchema = tSendBankConf
								.getBankLog();
						tLYBankLogSchema.setSerialNo(serialno);
						tLYBankLogSchema.setBankCode(BankCode);
						tLYBankLogSchema.setComCode(ComCode);

						// 提交数据
						outLYSendToBankSet.set(tLYSendToBankSet);
						outLYBankLogSet.add(tLYBankLogSchema);
						MMap map = new MMap();
						map.put(outLYSendToBankSet, "INSERT");
						map.put(outLYBankLogSet, "INSERT");
						mInputData.clear();
						mInputData.add(map);
						tPubSubmit.submitData(mInputData, "");
					} else
					{
						System.out.println("没有中公司批量代收费数据！！！");
						return false;
					}
				}
				tLJSPaySet = tLJSPayDB.executeQuery(sql1, tStartS, limite);

			} while (tLJSPaySet != null && tLJSPaySet.size() > 0);

		} else
		{
			SendBankConfFBL tSendBankConf = new SendBankConfFBL();
			// 查询出cansendbank状态为9的数据
			LJAGetDB tLJAGetDB = new LJAGetDB();
			String sql2 = "select * from ljaget a where cansendbank = '9' and BankOnTheWayFlag = '1' "
					+ "and bankcode in ( select bankcode from ldbankunite where bankunitecode='"
					+ BankCode
					+ "')"
					+ "and not exists(select 1 from lysendtobank where a.actugetno = paycode ) and managecom like '"
					+ ComCode + "%' ";
			LJAGetSet tLJAGetSet = new LJAGetSet();
			// 测试用
			LJAGetSet tLJAGetSet1 = tLJAGetDB.executeQuery(sql2);
			System.out.println("总长度" + tLJAGetSet1.size());
			int tStartF = 1;
			tLJAGetSet = tLJAGetDB.executeQuery(sql2, tStartF, limite);
			System.out.println("第一个付费批次的数据量" + tLJAGetSet.size());
			do
			{
				LYBankLogSet outLYBankLogSet1 = new LYBankLogSet();
				LYSendToBankSet outLYSendToBankSet1 = new LYSendToBankSet();
				LYSendToBankSet tLYSendToBankSet = tSendBankConf.getSendToBank(
						tLJAGetSet, BankCode);
				if (tLYSendToBankSet.size() != 0)
				{
					String serialno = tLYSendToBankSet.get(1).getSerialNo();

					LYBankLogSchema tLYBankLogSchema = tSendBankConf
							.getBankLog();
					tLYBankLogSchema.setSerialNo(serialno);
					tLYBankLogSchema.setBankCode(BankCode);
					tLYBankLogSchema.setComCode(ComCode);

					outLYSendToBankSet1.set(tLYSendToBankSet);
					outLYBankLogSet1.add(tLYBankLogSchema);
					MMap map = new MMap();
					map.put(outLYSendToBankSet1, "INSERT");
					map.put(outLYBankLogSet1, "INSERT");

					mInputData.clear();
					mInputData.add(map);
					PubSubmit ttPubSubmit = new PubSubmit();
					if (!ttPubSubmit.submitData(mInputData, ""))
					{
						System.out.println("数据提交失败！");
						return false;
					}
				} else
				{
					System.out.println("没有总公司批量代付费数据！！！");
					return false;
				}
				tLJAGetSet = tLJAGetDB.executeQuery(sql2, tStartF, limite);
			} while (tLJAGetSet.size() > 0 && tLJAGetSet != null);

		}
		return true;
	}

	// 为函数增加一个参数 2010-11-18
	public LYSendToBankSet setYingDaiRemark(LYSendToBankSet tLYSendToBankSet)
	{
		LYSendToBankSet oLYSendToBankSet = new LYSendToBankSet();

		for (int i = 1; i <= tLYSendToBankSet.size(); i++)
		{
			LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
			LYSendToBankSchema oLYSendToBankSchema = tLYSendToBankSet.get(i);
			tLYSendToBankSchema = getNoType(oLYSendToBankSchema);

			oLYSendToBankSet.add(tLYSendToBankSchema);

		}

		mmap.put(oLYSendToBankSet, "UPDATE");

		mInputData.clear();
		mInputData.add(mmap);

		PubSubmit ttPubSubmit = new PubSubmit();
		if (!ttPubSubmit.submitData(mInputData, ""))
		{
			System.out.println("数据提交失败！");
			return null;
		}

		return oLYSendToBankSet;
	}

	public LYSendToBankSchema getNoType(LYSendToBankSchema tLYSendToBankSchema)
	{
		String tNoType = "";
		String flag = "";
		String tContno = "";
		String tDealType = tLYSendToBankSchema.getDealType();
		String tOthernotype = tLYSendToBankSchema.getNoType();
		String tPolno = tLYSendToBankSchema.getPolNo();

		if ("2".equals(tOthernotype))
		{
			tNoType = "01";
			tContno = tPolno;
		} else if ("9".equals(tOthernotype) || "16".equals(tOthernotype))
		{
			tNoType = "02";
			tContno = tPolno;
		} else if ("10".equals(tOthernotype))
		{
			tContno = getContno(tOthernotype, tPolno);
			if ("S".equals(tDealType))
			{
				tNoType = "08";
			} else
			{
				tNoType = getLJAGetEndorseType(tPolno);
			}
		} else if ("5".equals(tOthernotype) || "C".equals(tOthernotype)
				|| "F".equals(tOthernotype))
		{
			tContno = getContno(tOthernotype, tPolno);
			tNoType = "07";
		}
		if (tNoType != "")
		{
			tLYSendToBankSchema.setNoType(tNoType);
		}
		if (tContno != "")
		{
			tLYSendToBankSchema.setName(tContno);
		}
		tLYSendToBankSchema.setRemark(flag);

		return tLYSendToBankSchema;
	}

	public String getContno(String tOthernotype, String tPolno)
	{
		String tContno = "";
		if ("10".equals(tOthernotype))
		{
			String endorsementnosql = "select contno from lpedormain where edorno='"
					+ tPolno + "' fetch first 1 rows only ";
			ExeSQL contnoSQL = new ExeSQL();
			tContno = contnoSQL.getOneValue(endorsementnosql);
		} else if ("5".equals(tOthernotype) || "C".equals(tOthernotype)
				|| "F".equals(tOthernotype))
		{
			String claimnosql = "select contno from ljagetclaim where otherno='"
					+ tPolno + "' fetch first 1 rows only ";
			ExeSQL contnoSQL = new ExeSQL();
			tContno = contnoSQL.getOneValue(claimnosql);
		}
		return tContno;
	}

	public String getLJAGetEndorseType(String tEndorsementno)
	{
		String tType = "";
		String sql = " select FeeOperationType from ljagetendorse where endorsementno = '"
				+ tEndorsementno + "' fetch first 1 rows only";
		ExeSQL tTypeSQL = new ExeSQL();
		tType = tTypeSQL.getOneValue(sql);
		if ("WT".equals(tType))
		{
			tType = "04";
		} else if ("CT".equals(tType))
		{
			tType = "05";
		} else if ("MJ".equals(tType))
		{
			tType = "06";
		} else
		{
			tType = "03";
		}
		return tType;
	}

	public static void main(String args[])
	{

		SendBankSave a = new SendBankSave();
		TransferData b = new TransferData();
		b.setNameAndValue("DealType", "S");
		b.setNameAndValue("ComCode", "86");
		b.setNameAndValue("BankCode", "7705");
		a.dealData(b);
	}

}
