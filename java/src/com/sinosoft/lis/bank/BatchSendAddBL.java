package com.sinosoft.lis.bank;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDBankUniteSchema;
import com.sinosoft.lis.vschema.LDBankSet;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BatchSendAddBL {

     /** 传入数据的容器 */
      private VData mInputData = new VData();
      /** 返回数据的容器 */
      private VData mResult = new VData();
      /** 提交数据的容器 */
      private MMap map = new MMap();
      /** 数据操作字符串 */
      private String mOperate;
      /** 错误处理类 */
      public  CErrors mErrors = new CErrors();
      /** 前台传入的公共变量 */
      private GlobalInput mGlobalInput = new GlobalInput();

      private String bankUniteCode = "";
      /** 银行编码 */
      private String bankCode = "";
      
      private String bankType = "";
      
      private String upBankUniteCode = "";
      /** 银行编码 */
      private String upBankCode = "";
      
      private String upBankType = "";
      
      private LDBankUniteSet tLDBankUniteSet = new LDBankUniteSet();
      
      private LDBankSet tLDBankSet = new LDBankSet();

      public BatchSendAddBL() {
      }
      
      /**
       * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
       * @param cInputData 传入的数据,VData对象
       * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
       * @return 布尔值（true--提交成功, false--提交失败）
       */
      public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mInputData = (VData)cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) return false;
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData()) return false;
        System.out.println("---End dealData---");

          //准备往后台的数据
          if (!prepareOutputData()) return false;
          System.out.println("---End prepareOutputData---");

          System.out.println("Start PubSubmit BLS Submit...");
          PubSubmit tPubSubmit = new PubSubmit();
          if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            mResult.clear();
            return false;
          }
          System.out.println("End PubSubmit BLS Submit...");

        return true;
      }

      /**
       * 将外部传入的数据分解到本类的属性中
       * @param: 无
       * @return: boolean
       */
      private boolean getInputData()    {
        try {
          TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
          bankUniteCode = (String)tTransferData.getValueByName("UniteBankCode");
          bankCode = (String)tTransferData.getValueByName("BankCode");
          bankType = (String)tTransferData.getValueByName("BankType");
          upBankCode = (String)tTransferData.getValueByName("upBankCode");
          upBankType = (String)tTransferData.getValueByName("upBankType");
          upBankUniteCode = (String)tTransferData.getValueByName("UniteBankCode3");
          mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
        }
        catch (Exception e) {
          // @@错误处理
          CError.buildErr(this, "接收数据失败");
          return false;
        }

        return true;
      }

      /**
       * 根据前面的输入数据，进行逻辑处理
       * @return 如果在处理过程中出错，则返回false,否则返回true
       */
      private boolean dealData() {
        try {
             System.out.println("mOperate:" + mOperate);
             
             if(mOperate.equals("INSERT")){
                 if(!addUniteBank()){
                     return false;
                 }
             } else if(mOperate.equals("UPDATE")){
                 if(!upUniteBank()){
                     return false;
                 }
             } else {
                 CError.buildErr(this, "操作字符异常");
                 return false;
             }
        }
        catch(Exception e) {
          // @@错误处理
          CError.buildErr(this, "数据处理错误:" + e.getMessage());
          return false;
        }

        return true;
      }

      private boolean addUniteBank(){
          LDBankDB tLDBankDB = new LDBankDB();
          tLDBankDB.setBankCode(bankCode);
          if (!tLDBankDB.getInfo())
          {
              CError.buildErr(this, "获取银行信息（LDBank）失败");
              return false;
          }
          
          LDBankDB tUniteBank = new LDBankDB();
          tUniteBank.setBankCode(bankUniteCode);
          if (!tUniteBank.getInfo())
          {
              CError.buildErr(this, "获取银行信息（LDBank）失败");
              return false;
          }
          
          LDCode1DB tLDCode1DB = new LDCode1DB();
          tLDCode1DB.setCodeType("BatchBank");
          tLDCode1DB.setCode(bankUniteCode);
          tLDCode1DB.setCode1(bankType);
          if (!tLDCode1DB.getInfo())
          {
              CError.buildErr(this, bankUniteCode + "不支持该银行");
              return false;
          }
          
          LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
          String sql = "select * from ldbankunite where bankcode='" + bankCode + "' and bankunitecode in ('7703','7704','7705','7706','7707','7708','7709')";
          LDBankUniteSet tUnite = tLDBankUniteDB.executeQuery(sql);
          if (tUnite.size() != 0)
          {
              CError.buildErr(this, "该银行已配置在" + tUnite.get(1).getBankUniteName() + "下");
              return false;
          }
          
          LDBankSchema tLDBankSchema = tLDBankDB.getSchema();
          tLDBankSchema.setCanSendFlag("1");
          tLDBankSet.add(tLDBankSchema);
          
          LDBankUniteSchema tLDBankUniteSchema = new LDBankUniteSchema();
          tLDBankUniteSchema.setBankUniteCode(bankUniteCode);
          tLDBankUniteSchema.setBankUniteName(tUniteBank.getBankName());
          tLDBankUniteSchema.setBankCode(bankCode);
          tLDBankUniteSchema.setUniteBankCode(tLDCode1DB.getCodeAlias());
          tLDBankUniteSchema.setUniteBankName(tLDBankDB.getBankName());
          tLDBankUniteSchema.setComCode(tLDBankDB.getComCode());
          tLDBankUniteSchema.setUniteGroupCode(tLDCode1DB.getOtherSign());
          tLDBankUniteSchema.setOperator(mGlobalInput.Operator);
          tLDBankUniteSchema.setMakeDate(PubFun.getCurrentDate());
          tLDBankUniteSchema.setMakeTime(PubFun.getCurrentTime());
          tLDBankUniteSchema.setModifyDate(PubFun.getCurrentDate());
          tLDBankUniteSchema.setModifyTime(PubFun.getCurrentTime());
          tLDBankUniteSet.add(tLDBankUniteSchema);
          return true;
      }

      private boolean upUniteBank(){
          LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
          tLDBankUniteDB.setBankCode(upBankCode);
          tLDBankUniteDB.setBankUniteCode(upBankUniteCode);
          if (!tLDBankUniteDB.getInfo())
          {
              CError.buildErr(this, "获取银行信息（LDBank）失败");
              return false;
          }
          
          LDCode1DB tLDCode1DB = new LDCode1DB();
          tLDCode1DB.setCodeType("BatchBank");
          tLDCode1DB.setCode(upBankUniteCode);
          tLDCode1DB.setCode1(upBankType);
          if (!tLDCode1DB.getInfo())
          {
              CError.buildErr(this, upBankUniteCode + "不支持该银行");
              return false;
          }
          
          LDBankUniteSchema tLDBankUniteSchema = tLDBankUniteDB.getSchema();
          tLDBankUniteSchema.setUniteBankCode(tLDCode1DB.getCodeAlias());
          tLDBankUniteSchema.setUniteGroupCode(tLDCode1DB.getOtherSign());
          tLDBankUniteSchema.setOperator(mGlobalInput.Operator);
          tLDBankUniteSchema.setModifyDate(PubFun.getCurrentDate());
          tLDBankUniteSchema.setModifyTime(PubFun.getCurrentTime());
          tLDBankUniteSet.add(tLDBankUniteSchema);
          return true;
      }
      
      /**
       * 准备往后层输出所需要的数据
       * @return 如果准备数据时发生错误则返回false,否则返回true
       */
      private boolean prepareOutputData() {
        try {
          map.put(tLDBankSet, "UPDATE");
          map.put(tLDBankUniteSet, mOperate);
          mInputData.clear();
          mInputData.add(map);
        }
        catch(Exception ex) {
          // @@错误处理
          CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
          return false;
        }

        return true;
      }

      /**
       * 数据输出方法，供外界获取数据处理结果
       * @return 包含有数据查询结果字符串的VData对象
       */
      public VData getResult() {
        return mResult;
      }
      public static void main(String[] args) {
       
}
}