package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.SAXException;

import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class GoldenUnionReadFile {


    public static int num = 0;

    //金联文件到数据的转换
    public LYReturnFromBankSet fileToDatabase(String FileName, String uBankCode,String SerialNo)
    {
    	
    	File myFile=new File(FileName);
    	 List detailList = new ArrayList();
    	 if(!myFile.exists())
         { 
             System.err.println("Can't Find " + FileName);
         }

         try 
         {
             BufferedReader in = new BufferedReader(new FileReader(myFile));
             String str;
            
             while ((str = in.readLine()) != null) 
             {
                   System.out.println(str);
                   detailList.add(str);
             }
             in.close();
         } 
         catch (IOException e) 
         {
             e.getStackTrace();
         }
    	
        try
        {
        	
        	 LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
             String dealType = "";
             String serialNo = "";

             if (uBankCode.equals("7707"))
             {
                 serialNo = SerialNo;
                 System.out.println("批次号" + serialNo);
                 String sql = "select logtype from lybanklog where serialno='"
                         + serialNo + "' with ur";
                 System.out.println(sql);
                 ExeSQL tExeSQL = new ExeSQL();
                 SSRS tSSRS = tExeSQL.execSQL(sql);
                 dealType = tSSRS.GetText(1, 1);

             }
         	
        	if(detailList.size() > 0){
        		
        		//以下为明细信息
        		for( int i = 1;i<detailList.size();i++){
        			String detileString[] = detailList.get(i).toString().split("[|]");
            		//回执状态
            		String bankSuccState = detileString[6].toString();
            		System.out.println("回执状态"+bankSuccState);
            		//回执描述
            		String bkdescrit = detileString[7].toString();
            		System.out.println("回执描述"+bkdescrit);
            		//paycode
            		String tPayCode = detileString[8].toString();
            		System.out.println("业务号"+tPayCode);
            		
            		LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
                    tLYSendToBankDB.setSerialNo(SerialNo);
                    tLYSendToBankDB.setPayCode(tPayCode);

                    LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
                    tLYSendToBankSet = tLYSendToBankDB.query();

                    if (tLYSendToBankSet == null || tLYSendToBankSet.size() == 0)
                    {
                        throw new Exception("从发送盘表获取数据失败！");
                    }
                    //如果返回处理错误，则标明错误信息            		
            		 if(bankSuccState != null ){
                         bankSuccState = getNewSucc(bkdescrit,uBankCode,bankSuccState);
                     }
            		
                     if (tLYSendToBankSet.size() == 1
                             || tLYReturnFromBankSet.size() == 0)
                     {
                         LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
                         tLYSendToBankSchema = tLYSendToBankSet.get(1);

                         if (tLYSendToBankSchema == null
                                 || "".equals(tLYSendToBankSchema))
                         {
                             throw new Exception("从发送盘表获取数据失败！");
                         }
                         
                         LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();
                         tLYReturnFromBankSchema.setSerialNo(serialNo);
                         tLYReturnFromBankSchema.setDealType(dealType);
                         tLYReturnFromBankSchema.setPayCode(tPayCode);
                         tLYReturnFromBankSchema.setBankSuccFlag(bankSuccState);

                         System.out.println("成功标记" + tLYReturnFromBankSchema.getBankSuccFlag());
                         
                         tLYReturnFromBankSchema.setDoType("1");
                         tLYReturnFromBankSchema.setModifyDate(PubFun.getCurrentDate());
                         tLYReturnFromBankSchema.setModifyTime(PubFun.getCurrentTime());
                         tLYReturnFromBankSchema.setBankDealDate(PubFun.getCurrentDate());
                         tLYReturnFromBankSchema.setPayCode(tLYSendToBankSchema
                                 .getPayCode());
                         tLYReturnFromBankSchema.setPolNo(tLYSendToBankSchema
                                 .getPolNo());
                         tLYReturnFromBankSchema.setNoType(tLYSendToBankSchema
                                 .getNoType());
                         tLYReturnFromBankSchema.setBankCode(tLYSendToBankSchema
                                 .getBankCode());
                         tLYReturnFromBankSchema.setComCode(tLYSendToBankSchema
                                 .getComCode());
                         tLYReturnFromBankSchema.setAgentCode(tLYSendToBankSchema
                                 .getAgentCode());
                         tLYReturnFromBankSchema.setName(tLYSendToBankSchema
                                 .getName());
                         tLYReturnFromBankSchema.setSendDate(tLYSendToBankSchema.getSendDate());
                         tLYReturnFromBankSchema.setAccNo(tLYSendToBankSchema.getAccNo());
                         tLYReturnFromBankSchema.setAccName(tLYSendToBankSchema.getAccName());
                         tLYReturnFromBankSchema.setPayMoney(tLYSendToBankSchema.getPayMoney());

                         tLYReturnFromBankSet.add(tLYReturnFromBankSchema);
                     }
                 }
        			
        		}
        	
            return tLYReturnFromBankSet;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError.buildErr(this, "file转入数据库处理失败: " + ex.getMessage());
        }

        return null;
    }

    private boolean checkSet(LYSendToBankSchema tSe, LYReturnFromBankSet tRe)
    {
        for (int rRow = 1; rRow <= tRe.size(); rRow++)
        {
            if (tSe.getPayCode().equals(tRe.get(rRow).getPayCode()))
            {
                return false;
            }
        }
        return true;
    }
    
    private String getNewSucc(String succMan,String bankCode,String bankSuccState)
    {
        try {
            if (succMan == null || succMan.trim().equals("")){
                return "90";
            } else {
            	// 为解决[账户名]:的问题，每次都会插入新数据 
    			if(succMan.lastIndexOf("]:") != -1){
    				succMan = succMan.substring(succMan.lastIndexOf("]:") + 3);
    			}
                String querySql = "select code1 from ldcode1 where codetype='bankerror' and code='" 
                        + bankCode + "' and code1='" + bankSuccState + "'";
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = tExeSQL.execSQL(querySql);
                if(tSSRS == null || tSSRS.MaxRow == 0){
                    
                    LDCode1DB tLDCode1DB = new LDCode1DB();
                    tLDCode1DB.setCodeType("bankerror");
                    tLDCode1DB.setCode(bankCode);
                    tLDCode1DB.setCode1(bankSuccState);
                    tLDCode1DB.setCodeName(succMan.trim());
                    tLDCode1DB.insert();
                    return bankSuccState;
                } else {
                    return bankSuccState;
                }
            }
        }
        catch (RuntimeException e) {
            e.printStackTrace();
        }
        return "90";
    }

}
