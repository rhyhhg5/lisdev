package com.sinosoft.lis.bank;

import java.io.*;

import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class ABCBankCreadXml
{

	private String mUrl = "";

	private String feeType = "";

	private String ubank = "";

	private String filePath = "";

	private MMap map = new MMap();
	/** 传入数据的容器 */
	private VData mInputData = new VData();
	
    /** 传出数据的容器 */
    private VData mResult = new VData();
	
	/** 错误处理类 */
	private CErrors mErrors = new CErrors();
	/** 前台传入的公共变量 */
    private GlobalInput inGlobalInput = new GlobalInput();
	/** 生成文件路径 */
	private String mPath = "";
	/** 生成文件名 */
    private String fileName = "";
	// 业务数据
	private LYBankLogSchema mLYBankLogSchema = new LYBankLogSchema();
	
	private LYSendToBankSchema inLYSendToBankSchema = new LYSendToBankSchema();

	private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();
	
    private TransferData inTransferData = new TransferData();
    private TransferData inTransferDataftp = new TransferData();
    

    private LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();

    private LYBankLogSet outLYBankLogSet = new LYBankLogSet();

	/**
	 * @param mUrl
	 *            报文存放路径
	 * @param feeType
	 *            收付费标识
	 * @param ubank
	 *            银行编码
	 */
	public boolean submitData(String mUrl, String feeType, String ubank)
	{
		this.mUrl = mUrl + "/";
		this.feeType = feeType;
		this.ubank = ubank;
		
		if (!dealDate())
		{
			System.out.println("获取数据失败");
			return false;
		}

		if (!prepareOutputData())
		{
			System.out.println("准备更新日志数据失败");
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "UPDATE"))
		{
			// 错误处理
			System.out.println("更新发盘日志失败");
			return false;
		}

		return true;
	}


	//判断是否有待发送给农行的数据
	public boolean dealDate()
	{
		String sql = "select * from lybanklog a where a.dealstate is null and a.senddate is null and a.bankcode in ("
				+ ubank
				+ ") and logtype='"
				+ feeType
				+ "' "
				+ "and (select count(*) from lysendtobank where serialno=a.serialno)>0 with ur";
		System.out.println(sql);
		LYBankLogDB tLyBankLogDB = new LYBankLogDB();
		LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
		if (tLYBankLogSet == null)
		{
			System.out.println("未查到发送银行数据");
			return false;
		}
		if(!getFTPInfo()){
			System.out.println("获取ftp信息失败");
			return false;
		}
		for (int i = 1; i <= tLYBankLogSet.size(); i++)
		{
			LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
			String serialno = tLYBankLogSchema.getSerialNo();
			sql = "select * from lysendtobank where serialno='" + serialno
					+ "' with ur";
			System.out.println(sql);
			LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
			LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB
					.executeQuery(sql);

			// 判断批次下是否有异常的付费数据（并发数据）
			if ("F".equals(feeType)
					&& !checkLJFIGet(serialno, tLYBankLogSchema))
			{
				// 若有问题，停止该批次的发送，并发送邮件
				continue;
			}
			// 判断批次下是否有异常的收费数据（并发数据）
			if ("S".equals(feeType)
					&& !checkLysendtobank(serialno, tLYBankLogSchema))
			{
				// 若有问题，停止该批次的发送，并发送邮件
				continue;
			}
			if (!createXml(tLYBankLogSchema, tLYSendToBankSet))
			{
				continue;
			}
			
		}
		return true;
	}

	//创建发送报文
	public boolean createXml(LYBankLogSchema tLYBankLogSchema,
			LYSendToBankSet tLYSendToBankSet)
	{
		
		//生成银行文件数据---任伟
		String feeType=tLYBankLogSchema.getLogType();
		try
        {
            //生成银行文件数据
            if (feeType.equals("S"))
            {
                //获取发送银行数据信息
                if (tLYSendToBankSet.size() == 0)
                    throw new NullPointerException("无发送银行数据！");

                //生成银行日志数据,构建文件名
                LYBankLogSet tLYBankLogSet = getLYBankLog(tLYBankLogSchema);
                if (tLYBankLogSet.size() == 0)
                    throw new NullPointerException("无银行日志数据！");

                //生成发送文件全路径
                fileName = this.mUrl + fileName;
                System.out.println("fileName="+fileName);
                ubank = tLYBankLogSet.get(1).getBankCode();
                
                outLYSendToBankSet.set(tLYSendToBankSet);
                outLYBankLogSet.set(tLYBankLogSet);
                inTransferData.removeByName("fileName");
                inTransferData.removeByName("bankCode");
                inTransferData.removeByName("DealType");
                inTransferData.setNameAndValue("fileName", fileName);
                inTransferData.setNameAndValue("bankCode", ubank);
                inTransferData.setNameAndValue("DealType", feeType);
            }
            else if(feeType.equals("F")){
            	
            	if (tLYSendToBankSet.size() == 0)
                    throw new NullPointerException("无发送银行数据！");

                //生成银行日志数据,构建文件名
                LYBankLogSet tLYBankLogSet = getLYBankLog(tLYBankLogSchema);
                if (tLYBankLogSet.size() == 0)
                    throw new NullPointerException("无银行日志数据！");

                //生成发送文件全路径
                fileName = this.mUrl + fileName;
                System.out.println("fileName="+fileName);
                ubank = tLYBankLogSet.get(1).getBankCode();
                
                outLYSendToBankSet.set(tLYSendToBankSet);
                outLYBankLogSet.set(tLYBankLogSet);
                inTransferData.removeByName("fileName");
                inTransferData.removeByName("bankCode");
                inTransferData.removeByName("DealType");
                inTransferData.setNameAndValue("fileName", fileName);
                inTransferData.setNameAndValue("bankCode", ubank);
                inTransferData.setNameAndValue("DealType", feeType);
            }

        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "数据处理错误" + e.getMessage());
            return false;
        }
		
        //生成文件标识
        if (feeType.equals("S"))
        {
        	String cOperate = "WRITE";
            //准备往后台的数据
            if (!OutputData())
                return false;
            System.out.println("---End prepareOutputData---");

            System.out.println("Start ABCBankCreateFileBL Submit...");
            ABCBankCreateFileBL tABCBankCreateFileBL = new ABCBankCreateFileBL();
            if (tABCBankCreateFileBL.submitData(mInputData, cOperate) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tABCBankCreateFileBL.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("End ABCBankCreateFileBL Submit...");

            //如果有需要处理的错误，则返回
            if (tABCBankCreateFileBL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tABCBankCreateFileBL.mErrors);
            }
        }else if(feeType.equals("F")){

        	String cOperate = "WRITE";
            //准备往后台的数据
            if (!OutputData())
                return false;
            System.out.println("---End prepareOutputData---");

            System.out.println("Start ABCBankCreateFileBL Submit...");
            ABCBankCreateFileBL tABCBankCreateFileBL = new ABCBankCreateFileBL();
            if (tABCBankCreateFileBL.submitData(mInputData, cOperate) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tABCBankCreateFileBL.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("End ABCBankCreateFileBL Submit...");

            //如果有需要处理的错误，则返回
            if (tABCBankCreateFileBL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tABCBankCreateFileBL.mErrors);
            }
        
        }
		//结束-----任伟
		
		String bankUniteCode = tLYBankLogSchema.getBankCode();
		String type = tLYBankLogSchema.getLogType();
		// TODO 生成数据包

		Element root = null;
		if (bankUniteCode.equals("7708") && type.equals("S"))
		{
			root = new Element("ap");
		} else if (bankUniteCode.equals("7708") && type.equals("F"))
		{
			root = new Element("ap");
		}
		
		Document doc = new Document(root);
		
		if(bankUniteCode.equals("7708") && type.equals("S"))
		{
		Element ele_INFO1_1 = new Element("Version");
		ele_INFO1_1.setText("");
		root.addContent(ele_INFO1_1);

		Element ele_INFO1_2 = new Element("Sign");
		ele_INFO1_2.setText("1");
		root.addContent(ele_INFO1_2);

		Element ele_INFO1_3 = new Element("ProductID");
		ele_INFO1_3.setText("ICC");
		root.addContent(ele_INFO1_3);

		Element ele_INFO1_4 = new Element("AuthNo");
		ele_INFO1_4.setText("");
		root.addContent(ele_INFO1_4);

		Element ele_INFO1_5 = new Element("OpNo");
		ele_INFO1_5.setText("9999");
		root.addContent(ele_INFO1_5);

		Element ele_INFO1_6 = new Element("CorpNo");
		ele_INFO1_6.setText("6637502024694406");
		root.addContent(ele_INFO1_6);

		Element ele_INFO1_7 = new Element("ChannelType");
		ele_INFO1_7.setText("ERP");
		root.addContent(ele_INFO1_7);

		Element ele_INFO1_8 = new Element("TransCode");
		ele_INFO1_8.setText("");
		root.addContent(ele_INFO1_8);

		Element ele_INFO1_9 = new Element("CCTransCode");
		ele_INFO1_9.setText("IBAF05");
		root.addContent(ele_INFO1_9);

		Element ele_INFO1_10 = new Element("ReqSeqNo");
		ele_INFO1_10.setText(tLYBankLogSchema.getSerialNo());
		root.addContent(ele_INFO1_10);

		Element ele_INFO1_11 = new Element("ReqDate");
		ele_INFO1_11.setText(PubFun.getCurrentDate2());
		root.addContent(ele_INFO1_11);

		Element ele_INFO1_12 = new Element("ReqTime");
		ele_INFO1_12.setText(PubFun.getCurrentTime2());
		root.addContent(ele_INFO1_12);

		Element ele_INFO1_13 = new Element("Amt");
		ele_INFO1_13.setText(Double.toString(tLYBankLogSchema.getTotalMoney()));
		root.addContent(ele_INFO1_13);

		Element ele_INFO1_14 = new Element("FileFlag");
		ele_INFO1_14.setText("1");
		root.addContent(ele_INFO1_14);

		Element ele_INFO15 = new Element("Cmp");
		setText(ele_INFO15, "DbAccNo", "171101040009911");
		setText(ele_INFO15, "DbProv", "11");
		setText(ele_INFO15, "DbCur", "01");
		setText(ele_INFO15, "DbLogAccNo", "");
		setText(ele_INFO15, "SumNum",
				Integer.toString(tLYBankLogSchema.getTotalNum()));
		setText(ele_INFO15, "BatchFileName",fileName.substring(fileName.lastIndexOf("/")+ 1)+".DS");
		root.addContent(ele_INFO15);

		Element ele_INFO16 = new Element("Corp");
		setText(ele_INFO16, "Postscript", "无合约代收");
		setText(ele_INFO16, "DbAccName", "绳舟柔字鹁擒于慊官送");
		setText(ele_INFO16, "NVoucherType", "99010006");
		setText(ele_INFO16, "NFAccNo", "11171101948700186");
		root.addContent(ele_INFO16);
		}
		else if(bankUniteCode.equals("7708") && type.equals("F"))
		{
			Element ele_INFO1_1 = new Element("Version");
			ele_INFO1_1.setText("");
			root.addContent(ele_INFO1_1);

			Element ele_INFO1_2 = new Element("Sign");
			ele_INFO1_2.setText("");
			root.addContent(ele_INFO1_2);

			Element ele_INFO1_3 = new Element("ProductID");
			ele_INFO1_3.setText("ICC");
			root.addContent(ele_INFO1_3);

			Element ele_INFO1_4 = new Element("AuthNo");
			ele_INFO1_4.setText("");
			root.addContent(ele_INFO1_4);

			Element ele_INFO1_5 = new Element("OpNo");
			ele_INFO1_5.setText("9999");
			root.addContent(ele_INFO1_5);

			Element ele_INFO1_6 = new Element("CorpNo");
			ele_INFO1_6.setText("6637502024694406");
			root.addContent(ele_INFO1_6);

			Element ele_INFO1_7 = new Element("ChannelType");
			ele_INFO1_7.setText("ERP");
			root.addContent(ele_INFO1_7);

			Element ele_INFO1_8 = new Element("TransCode");
			ele_INFO1_8.setText("");
			root.addContent(ele_INFO1_8);

			Element ele_INFO1_9 = new Element("CCTransCode");
			ele_INFO1_9.setText("IBAF04");
			root.addContent(ele_INFO1_9);

			Element ele_INFO1_10 = new Element("ReqSeqNo");
			ele_INFO1_10.setText(tLYBankLogSchema.getSerialNo());
			root.addContent(ele_INFO1_10);

			Element ele_INFO1_11 = new Element("ReqDate");
			ele_INFO1_11.setText(PubFun.getCurrentDate2());
			root.addContent(ele_INFO1_11);

			Element ele_INFO1_12 = new Element("ReqTime");
			ele_INFO1_12.setText(PubFun.getCurrentTime2());
			root.addContent(ele_INFO1_12);

			Element ele_INFO1_13 = new Element("Amt");
			ele_INFO1_13.setText(Double.toString(tLYBankLogSchema.getTotalMoney()));
			root.addContent(ele_INFO1_13);

			Element ele_INFO1_14 = new Element("FileFlag");
			ele_INFO1_14.setText("1");
			root.addContent(ele_INFO1_14);

			Element ele_INFO15 = new Element("Cmp");
			setText(ele_INFO15, "DbAccNo", "171101040009911");
			setText(ele_INFO15, "DbProv", "11");
			setText(ele_INFO15, "DbCur", "01");
			setText(ele_INFO15, "DbLogAccNo", "");
			setText(ele_INFO15, "SumNum",
					Integer.toString(tLYBankLogSchema.getTotalNum()));
			setText(ele_INFO15, "BatchFileName",fileName.substring(fileName.lastIndexOf("/")+ 1)+".DS");
			root.addContent(ele_INFO15);

			Element ele_INFO16 = new Element("Corp");
			setText(ele_INFO16, "Postscript", "无合约代收");
			setText(ele_INFO16, "DbAccName", "绳舟柔字鹁擒于慊官送");
			setText(ele_INFO16, "NVoucherType", "99020003");
			setText(ele_INFO16, "NFAccNo", "11171101948700221");
			root.addContent(ele_INFO16);
		}

		XMLOutputter XMLOut = new XMLOutputter();
		try
		{
			this.filePath = this.mUrl + tLYBankLogSchema.getSerialNo() + ".xml";
			XMLOut.setEncoding("GBK");
			XMLOut.output(doc, new FileOutputStream(this.mUrl
					+ tLYBankLogSchema.getSerialNo() + ".xml"));
			String sendStr = readtxt(this.mUrl + tLYBankLogSchema.getSerialNo()
					+ ".xml");
			int a = sendStr.getBytes("GBK").length;
			System.out.println("a的长度" + a);
			sendStr = String.format("%-7s", String.valueOf("0" + a)) + sendStr;
			System.out.println("发送报文内容" + sendStr);

			if (!sendStr.equals(""))
			{
				if (bankUniteCode.equals("7708"))
				{
					String payReq = SendBank(sendStr);
					if (!payReq.equals(""))
					{
						System.out.println("--返回请求响应--");
						saveReq(payReq, tLYBankLogSchema);
					} else
					{
						errBankLog(tLYBankLogSchema);
					}
				}
			}

		} catch (Exception ex)
		{
			System.out.println("生成xml失败");
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	public String SendBank(String xml) throws IOException
	{
		// System.out.println("开始发送");
		// String sendStr = xml;
		// String netAddress = "10.253.33.18";
		// int PORT_NUM = 3837;
		// String receStr = "";
		//
		// DatagramSocket datagramSocket;
		// DatagramPacket datagramPacket;
		// try
		// {
		//
		// *//*** 发送数据 ***//*
		// // 初始化datagramSocket,注意与前面Server端实现的差别
		// datagramSocket = new DatagramSocket();
		// // 使用DatagramPacket(byte buf[], int length, InetAddress address, int
		// // port)函数组装发送UDP数据报
		// System.out.println("开始发送的字符：" + sendStr);
		// byte[] buf = sendStr.getBytes();
		// InetAddress address = InetAddress.getByName(netAddress);
		// datagramPacket = new DatagramPacket(buf, buf.length, address,
		// PORT_NUM);
		// // 发送数据
		// datagramSocket.send(datagramPacket);
		// System.out.println("发送完了");
		// *//*** 接收数据 ***//*
		// byte[] receBuf = new byte[1024];
		// DatagramPacket recePacket = new DatagramPacket(receBuf,
		// receBuf.length);
		// datagramSocket.receive(recePacket);
		//
		// receStr = new String(recePacket.getData(), 0,
		// recePacket.getLength());
		// System.out.println("返回的包:" + receStr);
		// System.out.println(recePacket.getPort());
		//
		// } catch (SocketException e)
		// {
		// e.printStackTrace();
		// } catch (UnknownHostException e)
		// {
		// e.printStackTrace();
		// } catch (IOException e)
		// {
		// e.printStackTrace();
		// } finally
		// {
		//
		// * // 关闭socket if (datagramSocket != null) { datagramSocket.close();
		// * }
		//
		// }
		// return receStr;

		/*
		 * 向服务器端发送数据
		 */
		String info = null;
		String backInfo="";
		try
		{
			// 1.创建客户端Socket，指定服务器地址和端口
			Socket socket = new Socket("10.252.126.137", 3837);
			// 2.获取输出流，向服务器端发送信息
			OutputStream os = socket.getOutputStream();// 字节输出流
			PrintWriter pw = new PrintWriter(os);// 将输出流包装为打印流
			pw.write(xml);
			pw.flush();
			socket.shutdownOutput();// 关闭输出流
			// 3.获取输入流，并读取服务器端的响应信息
			InputStream is = socket.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			while ((info = br.readLine()) != null)
			{
				System.out.println("我是客户端，服务器说：" + info);
				backInfo=backInfo+info;
			}
			// 4.关闭资源
			br.close();
			is.close();
			pw.close();
			os.close();
			socket.close();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		info=backInfo;
		return info;
	}

	/**
	 * 添加子节点
	 * 
	 * @param element
	 * @param name
	 *            节点名称
	 * @param value
	 *            节点值
	 */
	private void setText(Element element, String name, String value)
	{

		Element ele = new Element(name);
		ele.addContent(value);
		element.addContent(ele);
	}

	private String uniteBankCode(String bankcode, String bankunitecode)
	{
		String sql = "select * from ldbankunite where bankcode='" + bankcode
				+ "' and bankunitecode='" + bankunitecode + "' with ur";
		System.out.println(sql);
		LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
		LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.executeQuery(sql);
		LDBankUniteSchema tLDBankUniteSchema = tLDBankUniteSet.get(1);
		return tLDBankUniteSchema.getUniteBankCode();
	}

	/**
	 * 获取报文省市节点值
	 * 
	 * @param bank
	 *            银行代码
	 * @param comCode
	 *            机构
	 * @param othertype
	 *            1为省 2为市
	 * @return String
	 */
	private String otherInfo(String bank, String comCode, int othertype)
	{
		if (othertype == 1)
		{
			if (bank.equals("04105840"))
			{
				return "广东";
			} else
			{
				if (comCode.length() >= 4)
				{
					String sql = "select codename from ldcode where codetype='ComProvince' and code='"
							+ comCode.substring(0, 4) + "'";
					String com = new ExeSQL().getOneValue(sql);
					if (com != null)
					{
						return com;
					} else
					{
						return "";
					}
				} else
				{
					return "北京";
				}
			}
		} else if (othertype == 2 && bank.equals("04105840"))
		{
			return "深圳";
		}
		return "";
	}

	/**
	 * 获取身份证节点值
	 * 
	 * @param bank
	 *            银行代码
	 * @param type
	 *            收付费类型
	 * @param flag
	 *            1为证件类型 2为证件号
	 * @return String
	 */
	private String getID(LYSendToBankSchema tLYSendToBankSchema, String bank,
			String type, int flag)
	{
		if (type.equals("S")
				&& (bank.equals("307") || bank.equals("302")
						|| bank.equals("301") || bank.equals("303")
						|| bank.equals("305") || bank.equals("04105840")))
		{
			if (flag == 1)
			{
				return "0";
			} else
			{
				return tLYSendToBankSchema.getIDNo();
			}
		}
		return "";
	}

	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex)
		{
			// @@错误处理
			System.out.println(ex.getMessage());
			CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
			return false;
		}
		return true;
	}

	/**
	 * 存储、读取返回报文
	 * 
	 * @param payReq
	 *            返回报分字符串
	 */
	private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema)
	{
		try
		{
			FileWriter fw = new FileWriter(this.mUrl
					+ tLYBankLogSchema.getSerialNo() + "rq.xml");
			fw.write(payReq);
			fw.close();
			updateLYBankLog(payReq, tLYBankLogSchema);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 连接失败更新日志
	 */
	private void errBankLog(LYBankLogSchema tLYBankLogSchema)
	{
		System.out.println("获取响应信息失败");
		tLYBankLogSchema.setOutFile("获取响应信息失败");
		tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
		tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
		this.map.put(tLYBankLogSchema, "UPDATE");
	}

	/**
	 * 读取返回报文,更新日志表
	 * 
	 * @param req
	 *            返回报分字符串
	 */
	private void updateLYBankLog(String req, LYBankLogSchema tLYBankLogSchema)
	{
		int iStart = req.indexOf("<RespCode>");
		if (iStart != -1)
		{
			int end = req.indexOf("</RespCode>");
			String signedMsg = req.substring(iStart + 10, end);
			System.out.println("响应标记：" + signedMsg);
			if (signedMsg.equals("0000"))
			{
				tLYBankLogSchema.setOutFile("成功发送#" + this.filePath);
				tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());
				tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
				tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
				System.out.println("成送发送");
			} else
			{
				int s_MSG = req.indexOf("<RespInfo>");
				if (s_MSG != -1)
				{
					int e_MSG = req.indexOf("</RespInfo>");
					String signedMSG = req.substring(s_MSG + 10, e_MSG);
					System.out.println("响应标记：" + signedMSG);
					if (signedMSG.length() > 50)
					{
						tLYBankLogSchema.setOutFile("发送失败,"
								+ signedMSG.substring(0, 40));
						tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
						tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
					} else
					{
						tLYBankLogSchema.setOutFile(signedMSG);
						tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
						tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
					}
				} else
				{
					tLYBankLogSchema.setOutFile("发送失败");
					tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
					tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
					System.out.println("--发送失败--s_MSG=-1---");
				}
			}
		}
		this.map.put(tLYBankLogSchema, "UPDATE");
		return;
	}

	/**
	 * 将发送报文转换为字符串
	 * 
	 * @param path
	 *            生成报文路径
	 */
	private String readtxt(String path)
	{
		String str = "";
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(path));

			String r = br.readLine();
			while (r != null)
			{
				str += r;
				r = br.readLine();
			}
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return "";
		}
		str = str.replace("<?xml version=\"1.0\" encoding=\"GBK\"?>", "");
		return str;
	}

	/**
	 * 校验批次下是否有异常的付费数据，对于异常数据会发送邮件
	 * 
	 * @return
	 */
	private boolean checkLJFIGet(String serialno,
			LYBankLogSchema tLYBankLogSchema)
	{
		// 校验是否有异常付费数据
		String sql = "select paycode,polno from lysendtobank where serialno='"
				+ serialno
				+ "' and exists (select 1 from ljfiget where actugetno=lysendtobank.paycode)"
				+ "  union all select paycode,polno from lysendtobank ly where serialno='"
				+ serialno
				+ "' and exists (select 1 from lysendtobank  where paycode=ly.paycode and serialno <>ly.serialno )";

		ExeSQL tEx = new ExeSQL();
		SSRS tSSRS = tEx.execSQL(sql);
		if (tSSRS.getMaxRow() == 0)
		{
			return true;
		} else if (!"1".equals(tLYBankLogSchema.getOthFlag()))
		{
			// 批次没有发送过邮件
			// 发送邮件
			// MailSender tMailSender = new MailSender("zhangchengxuan",
			// "zhangcx3");
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1),
					tMSSRS.GetText(1, 2), "picchealth");
			// 设置邮件发送信息
			String sendInf = "集中代收付" + serialno
					+ "批次出现异常付费数据，已停止该批次的发送，请核查批次内信息。";
			for (int row = 1; row <= tSSRS.getMaxRow(); row++)
			{
				sendInf = sendInf + "付费号：" + tSSRS.GetText(row, 1) + "，业务号:"
						+ tSSRS.GetText(row, 2) + "。";
			}
			tMailSender.setSendInf("集中代收付异常批次", sendInf, null);
			// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
			tMailSender.setToAddress("7705/7706");
			if (!tMailSender.sendMail())
			{
				tMailSender.getErrorMessage();
			}
			// 更新日志表输出信息
			tLYBankLogSchema.setOutFile("批次下存在异常付费数据，发送失败");
			tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
			tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
			// 已发送邮件标记
			tLYBankLogSchema.setOthFlag("1");
			this.map.put(tLYBankLogSchema, "UPDATE");
			return false;
		} else
		{
			return false;
		}
	}

	/**
	 * 校验批次下是否有异常的付费数据，对于异常数据会发送邮件
	 * 
	 * @return
	 */
	private boolean checkLysendtobank(String serialno,
			LYBankLogSchema tLYBankLogSchema)
	{
		// 校验是否有异常付费数据
		String sql = "select paycode,polno from lysendtobank sb where serialno='"
				+ serialno
				+ "' and  exists (select 1 from lysendtobank  where paycode=sb.paycode and serialno <>sb.serialno )"
				+ " union all select paycode,polno from lysendtobank where serialno='"
				+ serialno
				+ "' and  exists (select 1 from ljtempfee  where paycode=ljtempfee.tempfeeno and confmakedate is not null )"
				+ " union all select paycode,polno from lysendtobank sb where serialno='"
				+ serialno
				+ "' and notype='9' and exists (select 1 from lccont where prtno=sb.polno and appntname<>sb.accname) "
				+ " union select paycode,polno from lysendtobank sb where serialno='"
				+ serialno
				+ "' and exists (select 1 from ljtempfeeclass where sb.paycode=tempfeeno and paymode<>'4' ) ";
		ExeSQL tEx = new ExeSQL();
		SSRS tSSRS = tEx.execSQL(sql);
		if (tSSRS.getMaxRow() == 0)
		{
			return true;
		} else if (!"1".equals(tLYBankLogSchema.getOthFlag()))
		{
			// 批次没有发送过邮件
			// 发送邮件
			// MailSender tMailSender = new MailSender("zhangchengxuan",
			// "zhangcx3");
			String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1),
					tMSSRS.GetText(1, 2), "picchealth");
			// 设置邮件发送信息
			String sendInf = "集中代收付" + serialno
					+ "批次出现异常收费数据，已停止该批次的发送，请核查批次内信息。";
			for (int row = 1; row <= tSSRS.getMaxRow(); row++)
			{
				sendInf = sendInf + "收费号：" + tSSRS.GetText(row, 1) + "，业务号:"
						+ tSSRS.GetText(row, 2) + "。";
			}
			tMailSender.setSendInf("集中代收付异常批次", sendInf, null);
			// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
			tMailSender.setToAddress("7705/7706");
			if (!tMailSender.sendMail())
			{
				tMailSender.getErrorMessage();
			}
			// 更新日志表输出信息
			tLYBankLogSchema.setOutFile("批次下存在异常收费数据，发送失败");
			tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
			tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
			// 已发送邮件标记
			tLYBankLogSchema.setOthFlag("1");
			this.map.put(tLYBankLogSchema, "UPDATE");
			return false;
		} else
		{
			return false;
		}
	}

	public static void main(String[] arr)
	{

		LYBankLogSchema banklog = new LYBankLogSchema();
		LYBankLogDB banklogDB = new LYBankLogDB();
		banklogDB.setSerialNo("77052387000000149967");
		if (banklogDB.getInfo())
		{
			banklog = banklogDB.getSchema();
		}
		System.out.println(banklog.getBankCode());
		LYSendToBankSet sendtobank = new LYSendToBankSet();
		LYSendToBankDB sendtobankDB = new LYSendToBankDB();
		String sql = "select * from lysendtobank where serialno='77052387000000149967' with ur";
		sendtobank = sendtobankDB.executeQuery(sql);

		ABCBankCreadXml bscx = new ABCBankCreadXml();
		//bscx.dealABCData();
		bscx.filePath = "E:\\";
		bscx.mUrl = "E:\\";
		bscx.createXml(banklog, sendtobank);


		/*
		 * String a = "姜丽\"ds\""; System.out.println(a);
		 */

	}
	
	/*
	 * 任伟       开始
	 */
	
	
    /**
     * 获取发送银行数据信息
     * @param tLYSendToBankSchema
     * @return
     */
    private LYSendToBankSet getLYSendToBank(LYSendToBankSchema tLYSendToBankSchema)
    {
        LYSendToBankSet tLYSendToBankSet = tLYSendToBankSchema.getDB().query();
        //由于知BankCode，只能重新查询一次，无奈!
        
        if (tLYSendToBankSet != null && tLYSendToBankSet.size() > 0)
        {
            String bankCode = tLYSendToBankSet.get(1).getBankCode();
            if (bankCode != null && bankCode.equals("7708"))
            {
                String sql = "select * from lysendtobank where serialno ='"
                        + tLYSendToBankSchema.getSerialNo()
                        + "' and bankcode ='7708' order by accno ";
                System.out.println("--WriteToFileBL line 148:" + sql);
                tLYSendToBankSet = tLYSendToBankSchema.getDB()
                        .executeQuery(sql);
            }
        }
        return tLYSendToBankSet;
    }
	
    /**
     * 生成银行日志数据
     * @param tLYBankLogSchema2
     * @return
     */
    private LYBankLogSet getLYBankLog(LYBankLogSchema tLYBankLogSchema2)
    {
    	LYBankLogSet tLYBankLogSet = new LYBankLogSet();

        //获取日志记录
        LYBankLogDB tLYBankLogDB = new LYBankLogDB();
        tLYBankLogDB.setSerialNo(tLYBankLogSchema2.getSerialNo());
        if (!tLYBankLogDB.getInfo())
        {
            CError.buildErr(this, "获取银行日志数据失败");
            return null;
        }
        LYBankLogSchema tLYBankLogSchema = tLYBankLogDB.getSchema();

        //构建文件名，未加后缀名
        if(tLYBankLogSchema.getLogType().equals("S")){
        	fileName = "ABC" + tLYBankLogSchema.getBankCode() + "S"
                    + tLYBankLogSchema.getSerialNo() + "("
                    + PubFun.getCurrentDate() + ")";
        }else if(tLYBankLogSchema.getLogType().equals("F")){
        	fileName = "ABC" + tLYBankLogSchema.getBankCode() + "F"
                    + tLYBankLogSchema.getSerialNo() + "("
                    + PubFun.getCurrentDate() + ")";
        }
        
        System.out.println(tLYBankLogSchema.getBankCode()+"========================");
        
        System.out.println("文件名称："+fileName);
        tLYBankLogSet.add(tLYBankLogSchema);

        return tLYBankLogSet;
    }
    
    /**
     * 获取文件路径
     * @return
     */
    /*private String getFilePath()
    {
        LDSysVarSchema tLDSysVarSchema = new LDSysVarSchema();

        tLDSysVarSchema.setSysVar("SendToBankFilePath");
        tLDSysVarSchema = tLDSysVarSchema.getDB().query().get(1);
        return "E:/renwei/code/";
        //return tLDSysVarSchema.getSysVarValue();
    }*/
    
    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean OutputData()
    {
        mInputData = new VData();

        try
        {
        	mInputData.clear();
            mInputData.add(outLYSendToBankSet);
            mInputData.add(outLYBankLogSet);
            mInputData.add(inTransferData);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }

        return true;
    }
    /**
     * 将银行返回接收结果更新入库
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean writeMess(){
    	
		return false;
    }
    
    /**
     * 获取ftp信息
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean getFTPInfo(){
    	String sqlFTPName = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPName' and Code1='7708' ";
    	String FTPName = new ExeSQL().getOneValue(sqlFTPName);
    	
    	String sqlFTPPass = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPPass' and Code1='7708' ";
    	String FTPPass = new ExeSQL().getOneValue(sqlFTPPass);
    	
    	String sqlFTPIP = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPIP' and Code1='7708' ";
    	String FTPIP = new ExeSQL().getOneValue(sqlFTPIP);
    	
    	String sqlFTPPort = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPPort' and Code1='7708' ";
    	String FTPPort = new ExeSQL().getOneValue(sqlFTPPort);
    	
    	String sqlFTPPath = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPPath' and Code1='7708' ";
    	String FTPPath = new ExeSQL().getOneValue(sqlFTPPath);
    	
    	inTransferData.setNameAndValue("FTPName", FTPName);
    	inTransferData.setNameAndValue("FTPPass", FTPPass);
    	inTransferData.setNameAndValue("FTPIP", FTPIP);
    	inTransferData.setNameAndValue("FTPPort", FTPPort);
    	inTransferData.setNameAndValue("FTPPath", FTPPath);
    	
    	/*mInputData = new VData();

        try
        {
        	mInputData.clear();
            mInputData.add(inTransferDataftp);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }*/

        return true;
    	
    }
    
	/*
	 * 任伟     结束
	 */

}
