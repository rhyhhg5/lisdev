package com.sinosoft.lis.bank;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LDBankUniteSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.vschema.LDBankSet;

public class AddUniteBankBL {

	  /** 传入数据的容器 */
	  private VData mInputData = new VData();
	  /** 返回数据的容器 */
	  private VData mResult = new VData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /** 错误处理类 */
	  public  CErrors mErrors = new CErrors();
	  /** 前台传入的公共变量 */
	  private GlobalInput mGlobalInput = new GlobalInput();
	  
	  private LDBankUniteSet outLDBankUniteSet = new LDBankUniteSet();
	  
	  private LDBankSet outLDBankSet = new LDBankSet();
	  
	  
	  /** 业务数据 */
	  String UniteBankCode = "";
	  String BankCode = "";
	  String ManageCom = "";
	  String BankName = "";
	  String BankCodeName ="";
	  String UniteBankCodeName ="";
	  String uniteBankCode = "";
	  
	  
	  public AddUniteBankBL()
	  {
		  
	  }
	  
	  /**
	   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	   * @param cInputData 传入的数据,VData对象
	   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
	   * @return 布尔值（true--提交成功, false--提交失败）
	   */
	  public boolean submitData(VData cInputData, String cOperate)
	  {
		  //将操作数据拷贝到本类中
		  this.mInputData = (VData)cInputData.clone();
		  this.mOperate = cOperate;
		  
		  //得到外部传入的数据,将数据备份到本类中
		  if (!getInputData()) return false;
		  System.out.println("---End getInputData---");
		  
		  //进行业务处理
		  if(!dealData()) return false;
		  System.out.println("---End dealData---");
		  
		  //准备往后台传数据
		  if (!prepareOutputData()) return false;
	      System.out.println("---End prepareOutputData---");

	      System.out.println("Start GetReturnFromBank BLS Submit...");
	      AddUniteBankBLS tAddUniteBankBLS = new AddUniteBankBLS();
	      if(tAddUniteBankBLS.submitData(mInputData, cOperate) == false)
	      {
	    	  //@@错误处理
	          this.mErrors.copyAllErrors(tAddUniteBankBLS.mErrors);
	          mResult.clear();
	          return false;
	      }
	      //如果有需要处理的错误，则返回
	      if (tAddUniteBankBLS.mErrors .needDealError())  {
	        this.mErrors.copyAllErrors(tAddUniteBankBLS.mErrors ) ;
	      }
		  
		  return true;
	  }
	  
	  
	  /**
	   * 将外部传入的数据分解到本类的属性中
	   * @param: 无
	   * @return: boolean
	   */
	  private boolean getInputData()
	  {
		  try
		  {
			  TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
			  
			  UniteBankCode = (String)tTransferData.getValueByName("UniteBankCode");
			  UniteBankCodeName = (String)tTransferData.getValueByName("UniteBankCodeName");
			  BankCode = (String)tTransferData.getValueByName("BankCode");
			  BankCodeName = (String)tTransferData.getValueByName("BankCodeName");
			  ManageCom = (String)tTransferData.getValueByName("ManageCom");
			  
			  mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
			  
		  }catch(Exception e)
		  {
			  //@@错误处理
			  CError.buildErr(this, "接收数据失败");
		      return false;
		  }
		  return true;
	  }
	  /**
	   * 根据前面的输入数据，进行逻辑处理
	   * @return 如果在处理过程中出错，则返回false,否则返回true
	   */
	  public boolean dealData()
	  {
		  try
		  {
			  //生成银联关联表（LDBANKUNITE）
			  outLDBankUniteSet = getLDBankUnite();
			  System.out.println("---End getLDBankUnite---");
			  //修改银行发盘权限（LDBANK） by dongjian 2010-12-07
			  outLDBankSet = getLDBank();
			  System.out.println("---End getLDBank---");
		  }
		  catch(Exception e)
		  {
			  //@@错误处理
		      CError.buildErr(this, "数据处理错误:" + e.getMessage());
		      return false;
		  }
		  
		  return true;
	  }
	  
	  /**
	   * 生成银联表数据
	   * @return
	   */
	  private LDBankUniteSet getLDBankUnite()
	  {
		  LDBankUniteSchema  tLDBankUniteSchema = new LDBankUniteSchema();
		  LDBankUniteSet tLDBankUniteSet = new LDBankUniteSet();
		  
		  uniteBankCode = PubFun1.CreateMaxNo("1", 3);
		  
		  tLDBankUniteSchema.setBankUniteCode(UniteBankCode);
		  tLDBankUniteSchema.setBankCode(BankCode);
		  tLDBankUniteSchema.setComCode(ManageCom);
		  tLDBankUniteSchema.setBankUniteName(UniteBankCodeName);
		  tLDBankUniteSchema.setUniteBankName(BankCodeName);
		  tLDBankUniteSchema.setMakeDate(PubFun.getCurrentDate());
		  tLDBankUniteSchema.setMakeTime(PubFun.getCurrentTime());
		  tLDBankUniteSchema.setModifyDate(PubFun.getCurrentDate());
		  tLDBankUniteSchema.setModifyTime(PubFun.getCurrentTime());
		  tLDBankUniteSchema.setOperator("001");
		  tLDBankUniteSchema.setUniteBankCode(uniteBankCode);
		  
		  tLDBankUniteSet.add(tLDBankUniteSchema);
		  
		  return tLDBankUniteSet;
	  }
	  /*
	   * 修改银行发盘权限 dongjian  for 邮保通
	   * */
	  private LDBankSet getLDBank()
	  {
//		  ExeSQL tExeSQL = new ExeSQL();
//	      SSRS tSSRS = new SSRS();
		  LDBankSchema  tLDBankSchema = new LDBankSchema();
		  LDBankSet tLDBankSet = new LDBankSet();
		  
//		  String tSql = "select * from ldbank where bankcode ='"+ BankCode+"' with ur";
//		  tSSRS = tExeSQL.execSQL(tSql);
		  
		  tLDBankSchema.setBankCode(BankCode);
		  try
		  {
			  tLDBankSchema = tLDBankSchema.getDB().query().get(1);
		  }
		  catch 
		  (Exception e) {
		      System.out.println("LDBank表中没有"+BankCode+"的相关信息！");
		      return null;
		  }
		  tLDBankSchema.setCanSendFlag("1");
//		  if("7703".endsWith(uniteBankCode))
//		  {
//			  tLDBankSchema.setAgentPaySuccFlag("0000;");
//			  tLDBankSchema.setAgentGetSuccFlag("0000;");
//		  }
		  
		  
		  tLDBankSet.add(tLDBankSchema);
		  
		  return tLDBankSet;
	  }
	  
	  /**
	   * 准备往后层输出所需要的数据
	   * @return 如果准备数据时发生错误则返回false,否则返回true
	   */
	  private boolean prepareOutputData() {
	    mInputData = new VData();

	    try {
	    	mInputData.add(outLDBankUniteSet);
	    	mInputData.add(outLDBankSet);
	     
	    }
	    catch(Exception ex) {
	      // @@错误处理
	      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
	      return false;
	    }

	    return true;
	  }
	  
	  
	  
	  /**
	   * 数据输出方法，供外界获取数据处理结果
	   * @return 包含有数据查询结果字符串的VData对象
	   */
	  public VData getResult() {
	    return mResult;
	  }
	  
}
