package com.sinosoft.lis.bank;

import java.sql.Connection;

import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.vdb.LJTempFeeClassDBSet;
import com.sinosoft.lis.vdb.LJTempFeeDBSet;
import com.sinosoft.lis.vdb.LYBankLogDBSet;
import com.sinosoft.lis.vdb.LYSendToSettleDBSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class SettledConfBLS {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  //业务数据
  private LYBankLogSchema inLYBankLogSchema = new LYBankLogSchema();
  private LJTempFeeSet inLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeClassSet inLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet inUpdateLJTempFeeClassSet = new LJTempFeeClassSet();

  private LYSendToSettleSet inLYSendToSettleSet = new LYSendToSettleSet();

  public SettledConfBLS() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    boolean tReturn = false;

    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate =cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    //System.out.println("---End getInputData---");

    //信息保存
    if(this.mOperate.equals("GETMONEY")) {
      tReturn = save(cInputData);
    }

    if (tReturn)
      System.out.println("Save sucessful");
    else
      System.out.println("Save failed");

    return tReturn;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      inLYBankLogSchema = (LYBankLogSchema)mInputData.getObjectByObjectName("LYBankLogSchema", 0);
      inLJTempFeeSet = (LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet", 1);
      inLJTempFeeClassSet = (LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet", 2);
      inLYSendToSettleSet = (LYSendToSettleSet) mInputData.getObjectByObjectName("LYSendToSettleSet", 3);

    }
    catch (Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GetReturnFromBankBLS";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  //保存操作
  private boolean save(VData mInputData) {
    int i;
    boolean tReturn =true;
    System.out.println("Start Save...");

    //建立数据库连接
    Connection conn=DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GetReturnFromBankBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try {
      //开始事务，锁表
      conn.setAutoCommit(false);


      //记录银行业务日志，插入新日志数据
      LYBankLogSet tLYBankLogSet = new LYBankLogSet();
      tLYBankLogSet.add(inLYBankLogSchema);
      LYBankLogDBSet tLYBankLogDBSet = new LYBankLogDBSet(conn);
      tLYBankLogDBSet.set(tLYBankLogSet);
      if (!tLYBankLogDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYBankLog Update Failed");
        return false;
      }
      System.out.println("End 记录银行业务日志 ...");




      //新增暂交费数据
      LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet(conn);
      tLJTempFeeDBSet.set(inLJTempFeeSet);
      if (!tLJTempFeeDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJTempFee Insert Failed");
        return false;
      }
      System.out.println("End 新增暂交费数据 ...");

      //新增暂交费分类表数据
      LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet(conn);
      tLJTempFeeClassDBSet.set(inLJTempFeeClassSet);
      if (!tLJTempFeeClassDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJTempFeeClass Insert Failed");
        return false;
      }
      System.out.println("End 新增暂交费分类表数据 ...");






      //插入打印管理表数据
      LYSendToSettleDBSet tLYSendToSettleDBSet = new LYSendToSettleDBSet(conn);
      tLYSendToSettleDBSet.set(inLYSendToSettleSet);
      if (!tLYSendToSettleDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("tLJSPayPersonB update Failed");
        return false;
      }

      conn.commit();
      conn.close();
      System.out.println("End Committed");
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GetReturnFromBankBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try{ conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }

  public static void main(String[] args) {
  }
}
