package com.sinosoft.lis.bank;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToSettleDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDMedicalComSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYBankUniteLogSchema;
import com.sinosoft.lis.schema.LYSendToSettleSchema;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.vschema.LJSPayPersonBSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.vschema.LYDupPaySet;
import com.sinosoft.lis.vschema.LYReturnFromBankBSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到业务系统</p>
 * <p>Description: 转换标记（ConvertFlag）说明：
 * 初始（null）
 * 扣款失败（1）
 * 重复交费（2）
 * 首期直接交费（5）
 * 催收交费且已经交费核销（3）
 * 首期事后交费（6）
 * 事后选银行转账（9）
 * 正常催收且已经在柜台交费（4）
 * 续期催收（7）</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class SettledConfBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();

  //业务数据
  /** 处理总金额 */
  private String totalMoney = "";
  /** 处理总笔数 */
  private int sumNum = 0;
  /** 批次号 */
  private String serialNo = "";
  /** 银行编码 */
  private String bankCode = "";
   /** 输入银行编码 */
  private String getbankcode = "";
  /** 输入银行账户 */
  private String getbankaccno = "";
  /** 输入银行账户自动匹配标记 */
  private String autobankaccno = "";
  private GlobalInput mGlobalInput = new GlobalInput();

//  private LYReturnFromBankSchema inLYReturnFromBankSchema = new LYReturnFromBankSchema();
  private TransferData inTransferData = new TransferData();
  private LYSendToSettleSet inLYSendToSettleSet = new LYSendToSettleSet();

  private LYBankLogSchema outLYBankLogSchema = new LYBankLogSchema();
  private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();
  

  


  public SettledConfBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代收
    if (mOperate.equals("GETMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start GetReturnFromBank BLS Submit...");
      SettledConfBLS tSettledConfBLS = new SettledConfBLS();
      if(tSettledConfBLS.submitData(mInputData, cOperate) == false)	{
        // @@错误处理
        this.mErrors.copyAllErrors(tSettledConfBLS.mErrors);
        mResult.clear();
        return false;
      }
      System.out.println("End GetReturnFromBank BLS Submit...");

      //如果有需要处理的错误，则返回
      if (tSettledConfBLS.mErrors .needDealError())  {
        this.mErrors.copyAllErrors(tSettledConfBLS.mErrors ) ;
      }
    }


    return true;
  }





  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
//      inLYReturnFromBankSchema = (LYReturnFromBankSchema)mInputData.getObjectByObjectName("LYReturnFromBankSchema", 0);
      inTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);

      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }


  
  /**
   * 获取银行返回数据
   * @param inLYReturnFromBankSchema
   * @return
   */
  private LYSendToSettleSet getLYSendToSettle(String serialNo, String bankCode) {
	  
	  	System.out.println("\n获取发盘数据");
	  	
	  	LYSendToSettleSet tLYSendToSettleSet = new LYSendToSettleSet();
	  	

		// 没有合并银行，多单文件返回
		LYSendToSettleDB tLYSendToSettleDB = new LYSendToSettleDB();
		tLYSendToSettleDB.setSerialNo(serialNo);
		tLYSendToSettleDB.setMedicalCode(bankCode);
		tLYSendToSettleSet = tLYSendToSettleDB.query();
		for (int i = 1; i <= tLYSendToSettleSet.size(); i++) {
			LYSendToSettleSchema tLYSendToSettleSchema =  tLYSendToSettleSet.get(i);
			tLYSendToSettleSchema.setInsBankCode(getbankcode);
			tLYSendToSettleSchema.setInsAccNo(getbankaccno);
			tLYSendToSettleSchema.setConfDate(PubFun.getCurrentDate());
			tLYSendToSettleSchema.setConfMoney(totalMoney);
			tLYSendToSettleSchema.setDealState("2");
            tLYSendToSettleSchema.setModifyDate(PubFun.getCurrentDate());
			tLYSendToSettleSchema.setModifyTime(PubFun.getCurrentTime());
			tLYSendToSettleSet.set(i, tLYSendToSettleSchema);
		}

		return tLYSendToSettleSet;
  }

  /**
   * 获取银行扣款成功标志信息
   * @param tLYSendToSettleSchema
   * @return
   */
  private String getBankSuccFlag(LYSendToSettleSchema tLYSendToSettleSchema) {
    try {
      LDMedicalComSchema tLDMedicalSchema = new LDMedicalComSchema();
      
      tLDMedicalSchema.setMedicalComCode(tLYSendToSettleSchema.getMedicalCode());
      tLDMedicalSchema.setSchema(tLDMedicalSchema.getDB().query().get(1));

      return tLDMedicalSchema.getAgentPaySuccFlag();
    }
    catch (Exception e) {
      e.printStackTrace();
      throw new NullPointerException("获取结算成功标志信息失败！(getBankSuccFlag) " + e.getMessage());
    }
  }

  /**
   * 校验银行扣款成功标志
   * @param bankSuccFlag
   * @param tLYSendToSettleSchema
   * @return
   */
  private boolean verifyBankSuccFlag(String bankSuccFlag, LYSendToSettleSchema tLYSendToSettleSchema) {
    int i;
    boolean passFlag = false;

    String[] arrSucc = PubFun.split(bankSuccFlag, ";");
    String tSucc = tLYSendToSettleSchema.getSuccFlag();

    for (i=0; i<arrSucc.length; i++) {
      if(arrSucc[i].equals("") && tSucc==null){
          passFlag = true;
          break;
      }
      if (arrSucc[i].equals(tSucc)) {
        passFlag = true;
        break;
      }
    }

    return passFlag;
  }

  /**
   * 校验银行扣款成功与否
   */
  private void verifyBankSucc() {
    int i;
    //查看医保机构返回成功标志
    String bankSuccFlag = getBankSuccFlag(inLYSendToSettleSet.get(1));

    for (i=0; i<inLYSendToSettleSet.size(); i++) {
      LYSendToSettleSchema tLYSendToSettleSchema = inLYSendToSettleSet.get(i+1);
      //此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
      if (tLYSendToSettleSchema.getConvertFlag() != null) continue;

      //如果扣款失败，则设置转换标记为1
      if (!verifyBankSuccFlag(bankSuccFlag, tLYSendToSettleSchema)) {
        inLYSendToSettleSet.get(i+1).setConvertFlag("1");
      }
    }
  }

  /**
   * 校验财务总金额
   * @param tLYSendToSettleSet
   * @param totalMoney
   * @return
   */
  private boolean confirmTotalMoney(LYSendToSettleSet tLYSendToSettleSet, String totalMoney) {
    double sumMoney = 0;
    double fTotalMoney = Double.valueOf(totalMoney).doubleValue();

    for (int i=0; i<tLYSendToSettleSet.size(); i++) {
      LYSendToSettleSchema tLYSendToSettleSchema = tLYSendToSettleSet.get(i + 1);
      //此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
      if (tLYSendToSettleSchema.getConvertFlag() != null) continue;

      sumMoney = sumMoney + tLYSendToSettleSchema.getPayMoney();
      //转换精度
      sumMoney = Double.parseDouble((new DecimalFormat("0.00")).format(sumMoney));
      sumNum = sumNum + 1;
    }
    System.out.println("输入金额：" + fTotalMoney );
    System.out.println("数据金额：" + sumMoney);
    return (fTotalMoney == sumMoney);
  }


  /**
   * 校验每笔金额
   */
  private boolean verifyUnitMoney() {
    for (int i=0; i<inLYSendToSettleSet.size(); i++) {
      LYSendToSettleSchema tLYSendToSettleSchema = inLYSendToSettleSet.get(i+1);
      //此时ConvertFlag应为空，否则表示该数据为某类特殊情况数据，不参与正常逻辑处理，具体参数值参见上面类说明
      if (tLYSendToSettleSchema.getConvertFlag() != null) continue;

      LJTempFeeClassSchema tLJTempFeeClassSchema=new LJTempFeeClassSchema();
      tLJTempFeeClassSchema.setTempFeeNo(tLYSendToSettleSchema.getPayCode());
      tLJTempFeeClassSchema.setSchema(tLJTempFeeClassSchema.getDB().query().get(1));
//    扣款小于应收表中的信息，则转入重复交费表（设置转换标记为2），否则通过
      if(tLJTempFeeClassSchema.getPayMoney()!=tLYSendToSettleSchema.getPayMoney())
    	  throw new NullPointerException("缴费通知书号："+tLYSendToSettleSchema.getPayCode()+"的结算金额与收费金额不等！请与医保局对帐！");     
    }  
    return true;
  }





  /**
   * 生成银行日志数据
   * @param tLYSendToBankSchema
   * @return
   */
  private void getLYBankLog() throws Exception {
    //获取日志记录
    LYBankLogDB tLYBankLogDB = new LYBankLogDB();
    tLYBankLogDB.setSerialNo(serialNo);
    if (!tLYBankLogDB.getInfo()) {
      throw new Exception("获取银行日志表（LDBankLog）数据失败");
    }
    outLYBankLogSchema = tLYBankLogDB.getSchema();


    //修改日志
    outLYBankLogSchema.setAccTotalMoney(totalMoney); //财务确认总金额
   	outLYBankLogSchema.setBankSuccMoney(totalMoney); //银行成功总金额
   	outLYBankLogSchema.setBankSuccNum(sumNum + ""); //银行成功总数量
//   	outLYBankLogSchema.setDealState("1"); //处理状态

    outLYBankLogSchema.setTransOperator(mGlobalInput.Operator);
    outLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    outLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
  }
  
  private void getLJTempFee() throws Exception{
	  for (int i = 1; i <= inLYSendToSettleSet.size(); i++) {
		  
		  LYSendToSettleSchema mLYSendToSettleSchema= inLYSendToSettleSet.get(i);
		  
		  LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
		  LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
		  tLJTempFeeClassSchema.setTempFeeNo(mLYSendToSettleSchema.getPayCode());
		  tLJTempFeeClassSchema.setSchema(tLJTempFeeClassSchema.getDB().query().get(1));
		  
		  tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
		  tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
		  tLJTempFeeClassSchema.setInsBankAccNo(mLYSendToSettleSchema.getInsAccNo());
		  tLJTempFeeClassSchema.setInsBankCode(mLYSendToSettleSchema.getInsBankCode());
		  
		  tLJTempFeeSchema.setTempFeeNo(mLYSendToSettleSchema.getPayCode());
		  tLJTempFeeSchema.setSchema(tLJTempFeeSchema.getDB().query().get(1));
		  tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
		  tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
		  
		  outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
		  outLJTempFeeSet.add(tLJTempFeeSchema);
		  
	}
	  
	  
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
	  System.out.println("---dealData--处理---");
    try {
      //银行代收
      if (mOperate.equals("GETMONEY")) {
        //获取批次号、总金额、银行编码
        serialNo = (String) inTransferData.getValueByName("serialNo");
		getbankcode =(String)inTransferData.getValueByName("getbankcode");
        getbankaccno =(String)inTransferData.getValueByName("getbankaccno");
        System.out.print("getbankaccno2:"+getbankaccno);
        autobankaccno =(String)inTransferData.getValueByName("autobankaccno");
        System.out.print("autobankaccno:"+autobankaccno);
        totalMoney = (String) inTransferData.getValueByName("totalMoney");
        bankCode = (String) inTransferData.getValueByName("bankCode");
        //取结算数据
        inLYSendToSettleSet = getLYSendToSettle(serialNo, bankCode);
        if (inLYSendToSettleSet.size() == 0) throw new NullPointerException("无结算成功数据！");
        System.out.println("---End getLYSendToSettleSet---");

        //校验银行扣款成功与否
        verifyBankSucc();
        System.out.println("---End verifyBankSucc---");

        //校验每笔金额，比较LJSPay中的金额
        verifyUnitMoney();
        System.out.println("---End verifyUnitMoney---");
       
        //校验财务总金额，比较回盘表成功总金额和界面录入的总金额
        if (!confirmTotalMoney(inLYSendToSettleSet, totalMoney))
          throw new NullPointerException("财务总金额确认失败！请与医保局对帐！");
        System.out.println("---End confirmTotalMoney---");

                
        //删除结算失败的数据
        deleteUnSuccRecode();

        //记录日志，设置总金额、总数量、处理状态
        getLYBankLog();
        System.out.println("---End verifyUnitMoney---");
        //获取LJTempFee
        getLJTempFee();
        System.out.println("---End getLJTempFee---");
      }
    }
    catch(Exception e) {
      e.printStackTrace();
      // @@错误处理
      CError.buildErr(this, "数据处理错误: " + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    mInputData = new VData();

    try {
      mInputData.add(outLYBankLogSchema);
      mInputData.add(outLJTempFeeSet);
      mInputData.add(outLJTempFeeClassSet);
      mInputData.add(inLYSendToSettleSet);

    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  /**
   * 删除结算失败记录
   */
  public void deleteUnSuccRecode(){
	  LYSendToSettleSet outLYSendToSettleSet = new LYSendToSettleSet();
	  outLYSendToSettleSet.set(inLYSendToSettleSet);
	  System.out.println(outLYSendToSettleSet.size());
	  for (int i = 1; i <= outLYSendToSettleSet.size(); i++) {
		  LYSendToSettleSchema tLYSendToSettleSchema = outLYSendToSettleSet.get(i);
		if(tLYSendToSettleSchema.getConvertFlag() != null){
			inLYSendToSettleSet.remove(tLYSendToSettleSchema);
		}
	}
  }

  public static void main(String[] args) {
    SettledConfBL getReturnFromBankBL1 = new SettledConfBL();
    TransferData transferData1 = new TransferData();
    transferData1.setNameAndValue("totalMoney", "10000");
    transferData1.setNameAndValue("serialNo", "00000000000000001600");
    transferData1.setNameAndValue("bankCode", "9931");
    transferData1.setNameAndValue("bankUniteFlag", "");
    
    GlobalInput tGlobalInput = new GlobalInput(); 

    VData inVData = new VData();
    inVData.add(transferData1);
    inVData.add(tGlobalInput);

    if (!getReturnFromBankBL1.submitData(inVData, "GETMONEY")) {
    }
  }
}
