package com.sinosoft.lis.bank;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LBContSet;
import com.sinosoft.lis.vschema.LCContSet;

/**
 * 银行接口特殊信息提取类
 * 
 * @author 
 */
public class SendMedicalQueryJS {

	/**
	 * 获取客户社保编号
	 * 
	 * @param tLYSendToBankSchema
	 * @return
	 */
	public String getSecurityCode(LYSendToBankSchema tLYSendToBankSchema) {
		return "";
	}

	/**
	 * 获取收费类型
	 * 
	 * @param tLYSendToBankSchema
	 * @return
	 */
	public String getType(LYSendToBankSchema tLYSendToBankSchema) {
		String type = tLYSendToBankSchema.getNoType();
		
		if ("9".equals(type) || "16".equals(type)) {
			return "首期";
		} else if ("2".equals(type)) {
			return "续期";
		} else {
			return "犹豫期退保";
		}
	}
	
	public String getAgentCode(LYSendToBankSchema tLYSendToBankSchema) {
		String agentcode = tLYSendToBankSchema.getAgentCode();
		String name="";
		LAAgentDB tlaagent =  new LAAgentDB();
		tlaagent.setAgentCode(agentcode);
		if(tlaagent.getInfo()){
			name=tlaagent.query().get(1).getName();
		}
		return name;
	}
     public String getStatus(LYSendToBankSchema tLYSendToBankSchema){
	 String polno =tLYSendToBankSchema.getPolNo();
     LCContDB tlccont = new LCContDB();  
	 LBContDB tlbcont = new LBContDB();
     String status = "";
     String a1="select * from lccont where prtno='"+polno+"'";
//     String b1="select * from lbcont where prtno='"+polno+"'";
	 LCContSet  a = tlccont.executeQuery(a1);
//	 LBContSet b = tlbcont.query();
	 if(a.size()>0){
	 status="有效";
	 }else{
	 status= "无效";
	 }
     return status;
     }	
}
