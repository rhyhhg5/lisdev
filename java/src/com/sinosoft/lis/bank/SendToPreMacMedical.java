package com.sinosoft.lis.bank;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import com.sinosoft.utility.ExeSQL;

/**
 * 报文发送类
 * 
 * @author 王硕
 */
public class SendToPreMacMedical {

	private static String pcUrl = "";
	private static String dllUrl = "";
	
	public SendToPreMacMedical(){
		String sqlurl = "select codename from ldcode1 where codetype='SendToMedical' and code ='PCIP'";
		ExeSQL tExeSQL= new ExeSQL();
		pcUrl = tExeSQL.getOneValue(sqlurl);
		System.out.println("医保转账业务前置机地址："+pcUrl);
		String dllSQL = "select codename from ldcode1 where codetype='SendToMedical' and code='BatchServerUrl' with ur";
		dllUrl = tExeSQL.getOneValue(dllSQL);
		System.out.println("医保转账业务DLL存放路径："+dllUrl);
	}
	/**
	 * 将报文发送至前置机
	 * 
	 * @param xml 请求报文
	 * @param bank 第三方银行
	 * @param url 第三方银行地址
	 * @return 返回报文
	 */
	public String send(String xml, String bank) {
		// 请求报文
		HttpClient httpClient = new HttpClient();
		// url
		PostMethod postMethod = new PostMethod(pcUrl);
		// 设置编码
		httpClient.getParams().setParameter(
				HttpMethodParams.HTTP_CONTENT_CHARSET, "GBK");

		postMethod.addParameter("bank", bank);
		postMethod.addParameter("xml", xml);
		postMethod.addParameter("dllUrl", dllUrl);

		String strResp = "";
		try {
			long start = System.currentTimeMillis();
			// 执行getMethod
			int statusCode = httpClient.executeMethod(postMethod);
			// 失败
			if (statusCode != HttpStatus.SC_OK) {
				System.out.println("发送前置机失败，错误代码："+statusCode);
				return "";
			} else {
				// 读取内容
				byte[] responseBody = postMethod.getResponseBody();
				strResp = new String(responseBody, "GBK");
				System.out.println("服务器返回:" + strResp);
			}
			System.out.println("耗时:" + (System.currentTimeMillis() - start));
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		} finally {
			// 释放连接
			postMethod.releaseConnection();
		}
		return strResp;
	}
	
	public static void main(String args[]){
		
		new SendToPreMacMedical().send("21212","sad");
	}
}
