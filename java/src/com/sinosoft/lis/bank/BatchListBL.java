package com.sinosoft.lis.bank;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BatchListBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String mSql = null;

    private String mOutXmlPath = null;

    private String startDate = "";

    private String endDate = "";

    private String bankType = "";

    private String prtFlag = "";

    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();

    SSRS tComSSRS = new SSRS();

    public BatchListBL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        mSql = "select rfb.comcode,count(*),sum(paymoney) "
                + "from lyreturnfrombank rfb, lybanklog bl "
                + "where bl.bankcode in (";

        if (bankType.trim().equals("1"))
        {
            mSql = mSql + "'7705'";
        }
        else if (bankType.trim().equals("2"))
        {
            mSql = mSql + "'7706'";
        }
        else if (bankType.trim().equals("3"))
        {
            mSql = mSql + "'7703'";
        }
        else if (bankType.trim().equals("4"))
        {
            mSql = mSql + "'7707'";
        }
		 else if (bankType.trim().equals("5"))
        {
            mSql = mSql + "'7709'";
        }
        else if (bankType.trim().equals("6"))
        {
            mSql = mSql + "'7705','7706'";
        }
        else if (bankType.trim().equals("7"))
        {
            mSql = mSql + "'7705','7706','7707'";
        }
        else if (bankType.trim().equals("8"))
        {
        	mSql = mSql + "'7701'";
        }
        else if (bankType.trim().equals("9"))
        {
            mSql = mSql + "'7701','7703','7705','7706','7707','7709'";
        }

        mSql = mSql + ") and bl.logtype = '" + prtFlag + "' "
                + "and ( rfb.banksuccflag = '0000' or rfb.banksuccflag = '00' or rfb.banksuccflag = 'S0000' ) "
                + "and rfb.serialno = bl.serialno " + "and makedate between '"
                + startDate + "' and '" + endDate + "' and rfb.comcode like '" + mGI.ManageCom +"%'"
                + "group by rfb.comcode order by rfb.comcode with ur";
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tComSSRS = tExeSQL.execSQL(mSql);

        if (tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "BatchListBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        if (tComSSRS.getMaxRow() == 0)
        {

            CError tError = new CError();
            tError.moduleName = "BatchListBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有符合条件的数据，请重新输入条件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        String[][] mToExcel = new String[10000][30];
        
        String bankName = "";
        if (bankType.trim().equals("1"))
        {
            bankName = "银联";
        }
        else if (bankType.trim().equals("2"))
        {
            bankName = "通联";
        }
        else if (bankType.trim().equals("3"))
        {
            bankName = "邮储联";
        }
        else if (bankType.trim().equals("4"))
        {
            bankName = "金联";
        }
		else if (bankType.trim().equals("5"))
        {
            bankName = "银联高费率渠道";
        }
        else if (bankType.trim().equals("6"))
        {
            bankName = "银联/通联";
        }
        else if (bankType.trim().equals("7"))
        {
            bankName = "银联/通联/金联";
        }
        else if (bankType.trim().equals("8"))
        {
        	bankName = "保融";
        }
        else if (bankType.trim().equals("9"))
        {
            bankName = "银联/通联/邮储联/金联/银联高费率渠道/保融";
        }
        
        if (prtFlag.equals("S"))
        {
            mToExcel[0][0] = "代收成功笔数查询清单";
        }
        else if (prtFlag.equals("F"))
        {
            mToExcel[0][0] = "代付成功笔数查询清单";
        }else
        {
        	mToExcel[0][0] = "实时代收成功笔数查询清单";
        }	
        mToExcel[2][0] = "查询时间：" + startDate.replaceAll("-", "") + "-" + endDate.replaceAll("-", "");
        mToExcel[2][3] = "第三方银行：" + bankName;
        
        mToExcel[3][0] = "管理机构代码";
        mToExcel[3][1] = "机构代码";
        mToExcel[3][2] = "机构名称";
        if (prtFlag.equals("S")||prtFlag.equals("R"))
        {
            mToExcel[3][3] = "代收日期";
        }
        else
        {
            mToExcel[3][3] = "代付日期";
        }

        mToExcel[3][4] = "成功笔数";
        mToExcel[3][5] = "成功金额";
        
        int printNum = 3;
        int sumNum = 0; //支公司合计笔数
        double sumMoney = 0;//支公司合计金额
        int sumComNum = 0; //分公司合计笔数
        double sumComMoney = 0; //分公司合计金额
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");//格式化设置 
        String polCom = tComSSRS.GetText(1, 1).substring(0, 4) + "0000";
        
        System.out.println(tComSSRS.getMaxRow());
        for (int comnum = 1; comnum <= tComSSRS.getMaxRow(); comnum++)
        {
            sumNum = sumNum + Integer.parseInt(tComSSRS.GetText(comnum, 2));
            sumMoney = sumMoney + Double.parseDouble(tComSSRS.GetText(comnum, 3));
            
            System.out.println("--------开始对机构循环" + comnum);
            mSql = "select rfb.comcode,(select codename from licodetrans where codetype='ManageCom' and code=rfb.comcode),makedate,"
                    + "count(1),sum(paymoney) "
                    + "from lyreturnfrombank rfb, lybanklog bl "
                    + "where bl.bankcode in (";
            
            if (bankType.trim().equals("1"))
            {
                mSql = mSql + "'7705'";
            }
            else if (bankType.trim().equals("2"))
            {
                mSql = mSql + "'7706'";
            }
            else if (bankType.trim().equals("3"))
            {
                mSql = mSql + "'7703'";
            }
            else if (bankType.trim().equals("4"))
            {
                mSql = mSql + "'7707'";
            }
			else if (bankType.trim().equals("5"))
            {
                mSql = mSql + "'7709'";
            }
            else if (bankType.trim().equals("6"))
            {
                mSql = mSql + "'7705','7706'";
            }
            else if (bankType.trim().equals("7"))
            {
                mSql = mSql + "'7705','7706','7707'";
            }
            else if (bankType.trim().equals("8"))
            {
            	mSql = mSql + "'7701'";
            }
            else if (bankType.trim().equals("9"))
            {
                mSql = mSql + "'7701','7703','7705','7706','7707','7709'";
            }
            
            mSql = mSql + ") and bl.logtype = '" + prtFlag + "' "
                    + "and (rfb.banksuccflag = '0000' or rfb.banksuccflag = '00' or rfb.banksuccflag = 'S0000' ) "
                    + "and rfb.serialno = bl.serialno "
                    + "and makedate between '" + startDate + "' and '"
                    + endDate + "' and rfb.comcode='"
                    + tComSSRS.GetText(comnum, 1) + "'"
                    + "group by rfb.comcode,makedate order by makedate with ur";
            
            tSSRS = tExeSQL.execSQL(mSql);
            
            if(!polCom.equals(tSSRS.GetText(tSSRS.getMaxRow(), 1).substring(0, 4) + "0000")){
                mToExcel[printNum + 1][0] = "分公司合计：";
                mToExcel[printNum + 1][4] = sumComNum + "";
                mToExcel[printNum + 1][5] = decimalFormat.format(sumComMoney);
                sumComNum = 0;
                sumComMoney = 0;
                printNum = printNum + 2;
            }
            System.out.println(polCom + " " + tSSRS.GetText(tSSRS.getMaxRow(), 1).substring(0, 4) + "0000");
            polCom = tSSRS.GetText(tSSRS.getMaxRow(), 1).substring(0, 4) + "0000";
            
            for (int row = 1; row <= tSSRS.getMaxRow(); row++)
            {
                mToExcel[row + printNum][0] = tSSRS.GetText(row, 1).substring(0, 4) + "0000";
                for (int col = 1; col <= tSSRS.getMaxCol(); col++)
                {
                    mToExcel[row + printNum][col] = tSSRS.GetText(row, col);
                }
            }
            mToExcel[printNum + tSSRS.getMaxRow() + 1][0] = "支公司合计：";
            mToExcel[printNum + tSSRS.getMaxRow() + 1][4] = tComSSRS.GetText(comnum, 2);
            mToExcel[printNum + tSSRS.getMaxRow() + 1][5] = tComSSRS.GetText(comnum, 3);
            sumComNum = sumComNum + Integer.parseInt(tComSSRS.GetText(comnum, 2));
            sumComMoney = sumComMoney + Double.parseDouble(tComSSRS.GetText(comnum, 3));
            BigDecimal a= BigDecimal.valueOf(sumComMoney);
            System.out.println("格式输出:" + a);
            printNum = printNum + tSSRS.getMaxRow() + 1;
        }
        
        mToExcel[printNum + 1][0] = "分公司合计：";
        mToExcel[printNum + 1][4] = sumComNum + "";
        mToExcel[printNum + 1][5] = decimalFormat.format(sumComMoney);
        mToExcel[printNum + 3][0] = "总合计：";
        mToExcel[printNum + 3][4] = sumNum + "";
        mToExcel[printNum + 3][5] = decimalFormat.format(sumMoney);
        mToExcel[printNum + 5][0] = "制表单位：" + getName();
        mToExcel[printNum + 5][3] = "制表日期：" + mCurDate;
        
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch (Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName(
                "TransferData", 0);

        if (mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "BatchListBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        startDate = (String) tf.getValueByName("StartDate");
        endDate = (String) tf.getValueByName("EndDate");
        bankType = (String) tf.getValueByName("BankType");
        prtFlag = (String) tf.getValueByName("PrtFlag");

        if (startDate == null || endDate == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "BatchListBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
    }

    private String getName()
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select name from ldcom where comcode='" + mGI.ManageCom + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tSSRS.getMaxRow() == 0)
        {
            tRtValue = "";
        }
        else
            tRtValue = tSSRS.GetText(1, 1);
        return tRtValue;
    }

    public static void main(String[] args)
    {
    }
}
