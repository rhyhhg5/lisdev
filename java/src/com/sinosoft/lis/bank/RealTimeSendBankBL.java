package com.sinosoft.lis.bank;

import java.io.File;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 业务数据转换到银行系统，银行代收
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author
 * @version 1.0
 */

public class RealTimeSendBankBL {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 返回数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 前台传入的公共变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	// 业务数据
	/** 应收号 */
	private String getNoticeNo = "";

	/** 银联编码 */
	private String bankCode = "";

	private String uniteBankCode = "";

	private LJSPaySet mLJSPaySet = new LJSPaySet();

	/** 总笔数 */
	int sumNum = 0;

	/** 批次号 */
	String serialNo = "";

	/** 总金额 */
	private double totalMoney = 0;

	private String mUrl = "";

	private PubSubmit tPubSubmit = new PubSubmit();

	private String mSendPath = null;

	public RealTimeSendBankBL() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");

		// 置发盘数据
		if (!setBankDate()) {
			return false;
		}
		// 发盘回盘
		//发盘文件存放路径
		if (!getSendUrlName()) {
			return false;
		}
		
		if (!dealSendReturnXml())
        {
            return false;
        }

		return true;
	}

	private boolean setBankDate() {
		LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();
		LYBankLogSet outLYBankLogSet = new LYBankLogSet();
		LYSendToBankSet tLYSendToBankSet = getSendToBank(mLJSPaySet, bankCode);
		if (tLYSendToBankSet.size() != 0) {
			String serialno = tLYSendToBankSet.get(1).getSerialNo();

			LYBankLogSchema tLYBankLogSchema = getBankLog();
			tLYBankLogSchema.setSerialNo(serialno);
			tLYBankLogSchema.setBankCode(bankCode);
			tLYBankLogSchema.setComCode(mGlobalInput.ComCode);

			// 提交数据
			outLYSendToBankSet.set(tLYSendToBankSet);
			outLYBankLogSet.add(tLYBankLogSchema);

			MMap tmap = new MMap();
			tmap.put(outLYSendToBankSet, "INSERT");
			tmap.put(outLYBankLogSet, "INSERT");

			mInputData.clear();
			mInputData.add(tmap);

			tPubSubmit.submitData(mInputData, "");
		} else {
			System.out.println("没有中公司批量代收费数据！！！");
			return false;
		}
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			TransferData tTransferData = (TransferData) mInputData
					.getObjectByObjectName("TransferData", 0);
			getNoticeNo = (String) tTransferData.getValueByName("getNoticeNo");
			bankCode = (String) tTransferData.getValueByName("bankCode");
			uniteBankCode = (String) tTransferData
					.getValueByName("uniteBankCode");
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);
		} catch (Exception e) {
			// @@错误处理
			CError.buildErr(this, "接收数据失败");
			return false;
		}

		return true;
	}

	/**
	 * 获取交费日期在设置的日期区间内的应收总表记录
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	private LJSPaySet getLJSPayByPaydate() {
		String tSql = "";

		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("MaxSendToBankCount");

		// 规则：最早应收日期<=结束时间，最晚交费日期>结束时间
		// 银行编码匹配，机构编码向下匹配，不在途，有账号
		// 如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
		String sql = "select * from LJSPay where 1=1"
				+ " and BankCode = '"
				+ uniteBankCode
				+ "'"
				+ " and getnoticeno = '"
				+ getNoticeNo
				+ "'"
				+ " and ManageCom like '"
				+ mGlobalInput.ComCode
				+ "%'"
				+ " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null) "
				+ " and CanSendBank = 'p' and sumduepaymoney<>0 "
				+ " and (BankAccNo <> '') and (AccName <> '') and bankcode not in ('489102','999100') and bankcode not in (select bankcode from ldbankunite a where a.bankunitecode in ('7704') and a.bankcode=bankcode) ";

		tSql = sql + " and OtherNoType not in ('2','1','3') ";

		System.out.println(tSql);

		LJSPayDB tLJSPayDB = new LJSPayDB();
		LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSql);

		return tLJSPaySet;
	}

	private LJSPaySet getLJSPay() {
		LJSPaySet tLJSPaySet = new LJSPaySet();

		// 获取银行信息，校验是否是银联
		LDBankDB tLDBankDB = new LDBankDB();
		tLDBankDB.setBankCode(uniteBankCode);
		if (!tLDBankDB.getInfo()) {
			CError.buildErr(this, "获取银行信息（LDBank）失败");
			return null;
		}

		LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();

		 //unitegroupcode 1代表支持代收 2代表支持代付 3代表支持代收代付
		String bankSql = "select * from ldbankunite where bankunitecode in ('7705','7706','7709','7701','7703') and (UniteGroupCode in ('1','3','') or UniteGroupCode is null) and bankcode='"
				+ uniteBankCode + "' ";
		System.out.println(bankSql);
		LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.executeQuery(bankSql);

		if (tLDBankUniteSet.size() == 0) {
			CError.buildErr(this, "获取银联相关银行信息（LDBankUnite）失败");
			return null;
		}

		tLJSPaySet.add(getLJSPayByPaydate());

		return tLJSPaySet;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		try {
			// 银行代收（应收总表LJSPay）

			// 总应收表处理（获取交费日期在设置的日期区间内的记录；获取银行在途标志为N的记录）
			mLJSPaySet = getLJSPay();
			if (mLJSPaySet == null) {
				throw new NullPointerException("总应收表处理失败！");
			}
			if (mLJSPaySet.size() == 0) {
				throw new NullPointerException("总应收表无数据！");
			}
			if (!CheckLJSpay(mLJSPaySet, bankCode)) {
				return false;
			}
			System.out.println("---End getBankLog---");

		} catch (Exception e) {
			// @@错误处理
			CError.buildErr(this, "数据处理错误:" + e.getMessage());
			return false;
		}

		return true;
	}


	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult() {
		return mResult;
	}

	public boolean CheckLJSpay(LJSPaySet tLJSPaySet, String bankcode) {
		String failInfo = "";
		LJSPaySet oLJSPaySet = new LJSPaySet();
		LDCodeDB tLDCodeDB = new LDCodeDB();
		String sql = "select * from ldcode where codetype='accnocheckS' and code='"
				+ bankcode + "'";
		LDCodeSet tLDCodeSet = tLDCodeDB.executeQuery(sql);
		if (tLDCodeSet != null && tLDCodeSet.size() > 0) {
			String checkReg = tLDCodeSet.get(1).getCodeName();
			for (int row = 1; row <= tLJSPaySet.size(); row++) {
				String accno = tLJSPaySet.get(row).getBankAccNo();
				if (!accno.trim().matches(checkReg)) {
					oLJSPaySet.add(tLJSPaySet.get(row));
				}
			}
		}
		for (int i = 1; i <= oLJSPaySet.size(); i++) {
			if (!failInfo.equals("")) {
				failInfo += "、" + oLJSPaySet.get(i).getOtherNo();
			} else {
				failInfo = oLJSPaySet.get(i).getOtherNo();
			}
			tLJSPaySet.remove(oLJSPaySet.get(i));
		}
		if (!failInfo.equals("")) {
			failInfo = "业务号" + failInfo + "的银行帐号格式错误，无法进行收费";
			CError.buildErr(this, failInfo);
			return false;
		}
		this.mResult.add(failInfo);
		return true;
	}

	/**
	 * 生成银行日志表数据
	 * 
	 * @return
	 */
	public LYBankLogSchema getBankLog() {
		LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

		tLYBankLogSchema.setBankCode(bankCode);
		tLYBankLogSchema.setLogType("R");
		tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
		tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
		tLYBankLogSchema.setTotalMoney(totalMoney);
		tLYBankLogSchema.setTotalNum(sumNum);
		tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
		tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
		tLYBankLogSchema.setComCode(mGlobalInput.ComCode);

		return tLYBankLogSchema;
	}

	/**
	 * 生成送银行表数据
	 * 
	 * @param tLJSPaySet
	 * @return
	 */
	public LYSendToBankSet getSendToBank(LJSPaySet tLJSPaySet, String tBankCode) {
		// 总金额
		double dTotalMoney = 0;

		serialNo = PubFun1.CreateMaxNo("1", 15);
		serialNo = "R" + tBankCode + serialNo;
		System.out.println(serialNo);

		LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();

		for (int i = 0; i < tLJSPaySet.size(); i++) {
			LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i + 1);

			// 校验该笔数据是否已经提盘
			LYSendToBankDB OldLYSendToBankDB = new LYSendToBankDB();
			OldLYSendToBankDB.setPayCode(tLJSPaySchema.getGetNoticeNo());
			OldLYSendToBankDB.setBankCode(tLJSPaySchema.getBankCode());
			OldLYSendToBankDB.setAccName(tLJSPaySchema.getAccName());
			OldLYSendToBankDB.setAccNo(tLJSPaySchema.getBankAccNo());
			// 生成送银行表数据
			LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
			// 设置统一的批次号
			tLYSendToBankSchema.setSerialNo(serialNo);
			// 收费标记
			tLYSendToBankSchema.setDealType("R");
			tLYSendToBankSchema.setPayCode(tLJSPaySchema.getGetNoticeNo());
			tLYSendToBankSchema.setBankCode(tLJSPaySchema.getBankCode());
			tLYSendToBankSchema.setAccName(tLJSPaySchema.getAccName());
			tLYSendToBankSchema.setAccNo(tLJSPaySchema.getBankAccNo());

			tLYSendToBankSchema.setPolNo(tLJSPaySchema.getOtherNo());
			tLYSendToBankSchema.setNoType(tLJSPaySchema.getOtherNoType());
			tLYSendToBankSchema.setComCode(tLJSPaySchema.getManageCom());
			tLYSendToBankSchema.setAgentCode(tLJSPaySchema.getAgentCode());
			tLYSendToBankSchema.setPayMoney(tLJSPaySchema.getSumDuePayMoney());
			tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
			tLYSendToBankSchema.setDoType("1");
			tLYSendToBankSchema.setRemark(mGlobalInput.Operator);
			tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
			tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
			System.out.println("tLJSPaySchema:::::"
					+ tLJSPaySchema.getAppntNo());
			SSRS tSSRS = new ExeSQL()
					.execSQL("select idno,idtype from ldperson where customerno='"
							+ tLJSPaySchema.getAppntNo() + "'");
			if (tSSRS.getMaxRow() > 0) {
				tLYSendToBankSchema.setIDNo(tSSRS.GetText(1, 1));
				tLYSendToBankSchema.setIDType(tSSRS.GetText(1, 2));
			} else {
				tLYSendToBankSchema.setIDNo("");
			}
			tLYSendToBankSchema.setRiskCode(tLJSPaySchema.getRiskCode());
			tLYSendToBankSet.add(tLYSendToBankSchema);

			// 累加总金额和总数量
			dTotalMoney = dTotalMoney + tLJSPaySchema.getSumDuePayMoney();
			// 转换精度
			dTotalMoney = Double.parseDouble((new DecimalFormat("0.00"))
					.format(dTotalMoney));
			sumNum = sumNum + 1;
		}
		totalMoney = Double.parseDouble((new DecimalFormat("0.00"))
				.format(dTotalMoney));

		return tLYSendToBankSet;
	}

	/**
	 * getUrlName 获取文件存放路径
	 * 
	 * @return boolean
	 */
	private boolean getSendUrlName() {
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";// 发盘报文的存放路径
//		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPathBenDi'";
		mUrl = new ExeSQL().getOneValue(sqlurl);
		System.out.println("发盘报文的存放路径:" + mUrl);// 调试用－－－－－
		if (mUrl == null || mUrl.equals("")) {
			CError.buildErr("getFileUrlName", "获取文件存放路径出错");
			return false;
		}
		return true;
	}

	/**
	 * dealData 数据逻辑处理
	 * 
	 * @return boolean
	 */
	private boolean dealSendReturnXml() {
		mSendPath = mUrl + PubFun.getCurrentDate2() + "R";
		if (!newFolder(mSendPath)) {
			mSendPath = mUrl;
		}
		System.out.println("文件存放路径" + mSendPath);
		RealTimeSendCreateXml tBatchSendCreateXml = new RealTimeSendCreateXml();
		// 由于通联银联、代收代付需要不同时间段的批处理进行处理
		tBatchSendCreateXml.submitData(mSendPath, "R", serialNo);
		return true;
	}

	/**
	 * newFolder 新建报文存放文件夹，以便对发盘报文查询
	 * 
	 * @return boolean
	 */
	public static boolean newFolder(String folderPath) {
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try {
			if (myFilePath.isDirectory()) {
				System.out.println("目录已存在");
				return true;
			} else {
				myFilePath.mkdir();
				System.out.println("新建目录成功");
				return true;
			}
		} catch (Exception e) {
			System.out.println("新建目录失败");
			e.printStackTrace();
			return false;
		}
	}

	public String getSerialNo() {
		return serialNo;
	}
}
