package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ABCBankReturnXml {
	private String mUrl = "";

	private String feeType = "";

	private String ubank = "";

	private String filePath = "";

	private MMap map = new MMap();
	/** 传入数据的容器 */
	private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
	/** 错误处理类 */
	private CErrors mErrors = new CErrors();
	/** 前台传入的公共变量 */
    private GlobalInput inGlobalInput = new GlobalInput();
    
    private TransferData inTransferData = new TransferData();
	/** 生成文件路径 */
	private String mPath = "";
	
	private String flag="";
    
    public boolean submitData(String mUrl, String ubank)
    {
    	this.mPath = mUrl;
		this.ubank = ubank;

		/*if(!newFolder(mPath)){
            mPath = mUrl;
        }*/
        if (!dealDate())
        {
            System.out.println("获取数据失败");
            return false;
        }
        
        if (!prepareOutputData())
        {
            System.out.println("准备更新日志数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE"))
        {
            //错误处理
            System.out.println("更新日志失败");
            return false;
        }
        
        return true;
    }

	private boolean dealDate() {
		String sql = "select * from lybanklog a where a.dealstate is null and a.senddate is not null and a.bankcode in ('"+ ubank+ "') "
					+ "and (select count(*) from lysendtobank where serialno=a.serialno)>0 with ur";
		System.out.println(sql);
        LYBankLogDB tLyBankLogDB = new LYBankLogDB();
        LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
        if (tLYBankLogSet == null)
        {
            System.out.println("未查到待回盘银行数据");
            return false;
        }
        /*if(!connect()){
        	System.out.println("ftp登录失败！");
        	return false;
        }*/
        
        if(!getFTPInfo()){
			System.out.println("获取ftp信息失败");
			return false;
		}
        
        for (int i = 1; i <= tLYBankLogSet.size(); i++)
        {
            LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
            String serialno = tLYBankLogSchema.getSerialNo();
            sql = "select * from lysendtobank where serialno='" + serialno
                    + "' with ur";
            System.out.println(sql);
            LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
            LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB
                    .executeQuery(sql);
            
            createXml(tLYBankLogSchema, tLYSendToBankSet);
        }
        /*if(!disconnect()){
        	System.out.println("ftp断开失败");
        	return false;
        }*/
        return true;
	}
	
	//生成查询代收代付回盘报文
	public void createXml(LYBankLogSchema tLYBankLogSchema,
            LYSendToBankSet tLYSendToBankSet)
    {
        String bankUniteCode = tLYBankLogSchema.getBankCode();
        String type = tLYBankLogSchema.getLogType();

        //TODO 查询时代收代付是否有差异，待改
        Element root = null;
        if (bankUniteCode.equals("7708") && type.equals("S"))
		{
			root = new Element("ap");
		} else if (bankUniteCode.equals("7708") && type.equals("F"))
		{
			root = new Element("ap");
		}

        Document doc = new Document(root);
        Element ele_INFO1 = new Element("CCTransCode");
        ele_INFO1.setText("IBAQ06");
        root.addContent(ele_INFO1);
        
        Element ele_INFO2 = new Element("ChannelType");
        ele_INFO2.setText("0");
        root.addContent(ele_INFO2);
        
        Element ele_INFO3 =new Element("CorpNo");
        ele_INFO3.setText("6637502024694406");
        root.addContent(ele_INFO3);
        
        Element ele_INFO4 =new Element("OpNo");
        ele_INFO4.setText("9999");
        root.addContent(ele_INFO4);
        
        Element ele_INFO5 =new Element("AuthNo");
        ele_INFO5.setText("001");
        root.addContent(ele_INFO5);
        
        Element ele_INFO6 =new Element("ReqSeqNo");
        ele_INFO6.setText("20180529140750857001");
        root.addContent(ele_INFO6);
        
        Element ele_INFO7 =new Element("ReqDate");
        ele_INFO7.setText(PubFun.getCurrentDate2());
        root.addContent(ele_INFO7);
        
        Element ele_INFO8 =new Element("ReqTime");
        ele_INFO8.setText(PubFun.getCurrentTime2());
        root.addContent(ele_INFO8);
        
        Element ele_INFO9 =new Element("Sign");
        ele_INFO9.setText("");
        root.addContent(ele_INFO9);
        
        Element ele_INFO = new Element("Corp");
        setText(ele_INFO, "CustomNo", tLYBankLogSchema.getSerialNo());
		root.addContent(ele_INFO);

        XMLOutputter XMLOut = new XMLOutputter();
        try
        {
            XMLOut.setEncoding("GBK");
            XMLOut.output(doc, new FileOutputStream(this.mPath
                    + tLYBankLogSchema.getSerialNo() + ".xml"));
            String sendStr = readtxt(this.mPath + tLYBankLogSchema.getSerialNo()
                    + ".xml");
			int a = sendStr.getBytes("GBK").length;
			System.out.println("a的长度" + a);
			sendStr = String.format("%-7s", String.valueOf("0" + a)) + sendStr;
            System.out.println("发送查询报文内容" + sendStr);

            if (!sendStr.equals(""))
            {
                if (bankUniteCode.equals("7708"))
                {
                	String payReq = SendBank(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        saveReq(payReq, tLYBankLogSchema, bankUniteCode);
                        
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema, 1);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            System.out.println(tLYBankLogSchema.getSerialNo() + "批次程序处理异常");
            ex.printStackTrace();
        }
    }
	
	private boolean setText(Element element, String name, String value)
    {

        Element ele = new Element(name);
        ele.addContent(value);
        element.addContent(ele);
        return true;
    }
	
	private String readtxt(String path)
    {
        String str = "";
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(path));

            String r = br.readLine();
            while (r != null)
            {
                str += r;
                r = br.readLine();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
        str = str.replaceAll("<?xml version=\"1.0\" encoding=\"GBK\"?>", "");
        return str;
    }
	
	private void errBankLog(LYBankLogSchema tLYBankLogSchema, int flag)
    {
        if (flag == 1)
        {
            System.out.println("获取响应信息失败");
            tLYBankLogSchema.setInFile("获取响应信息失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            System.out.println("回盘处理程序出现异常");
            tLYBankLogSchema.setInFile("回盘处理程序出现异常");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        this.map.put(tLYBankLogSchema, "UPDATE");
    }
	
	
	//读取报文内容判断查询的批次号对应的银行处理结果是否成功
	private boolean checkReq(String req, LYBankLogSchema tLYBankLogSchema)
    {
        int res_start = req.indexOf("<RespSource>");
        int iStart = req.indexOf("<RespCode>");
        if(res_start != -1&&iStart != -1){
        	int end = req.indexOf("</RespCode>");
        	int res_end = req.indexOf("</RespSource>");
        	String respSource = req.substring(res_start+12,res_end);
        	System.out.println("RespSource:"+ respSource);
        	String respCode = req.substring(iStart+10, end);
        	System.out.println("RespCode:" + respCode);
        	if("0".equals(respSource)&&"0000".equals(respCode)){
        		int stateStart = req.indexOf("<状态>");
        		if (stateStart!=-1){
        			int stateEnd = req.indexOf("</状态>");
        			String stateMsg = req.substring(stateStart+4, stateEnd);
        			System.out.println("处理状态：" + stateMsg);
        			if ("5".equals(stateMsg)){
        				return true;
        			}
        			else{
        				int desStart = req.indexOf("<RxtInfo>");
        				if(desStart!=-1){
        					int desEnd = req.indexOf("</RxtInfo>");
                    		String description = req.substring(desStart+9,desEnd);
                    		System.out.println("状态描述："+description);
                    		if(description.length()>50){
                    			tLYBankLogSchema.setInFile(description.substring(0, 40));
                    			tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                                tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    		}else{
                    			tLYBankLogSchema.setInFile(description);
                    			tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                                tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    		}
        				}
        				this.map.put(tLYBankLogSchema, "UPDATE");
        			}
        		}
        	}else if("2".equals(respSource)&&"9999".equals(respCode)){
        		tLYBankLogSchema.setInFile("未知状态");
    			tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        	}else{
        		int s_MSG = req.indexOf("<RespInfo>");
                if (s_MSG != -1)
                {
                    int e_MSG = req.indexOf("</RespInfo>");
                    System.out.println("截去字符串的起始位置:" + (s_MSG+10) + ";终止位置:" + e_MSG);
                    String signedMSG = req.substring(s_MSG + 10, e_MSG);
                    System.out.println("响应标记：" + signedMSG);
                    if (signedMSG.length() > 50)
                    {
                        tLYBankLogSchema.setInFile(signedMSG.substring(0, 40));
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                    else
                    {
                        tLYBankLogSchema.setInFile(signedMSG);
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                    this.map.put(tLYBankLogSchema, "UPDATE");
                }
        	}
        }
        return false;
    }
	
	
	
	private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema,
            String bankCode)
    {
        String tUrl = this.mPath + tLYBankLogSchema.getSerialNo() + "rq.xml";
        try
        {	
        	//输出返回报文内容生成xml
        	//ABCBankReturnXml read = new ABCBankReturnXml();
        	//filePath = read.readInfo(payReq);
            FileWriter fw = new FileWriter(tUrl);
            fw.write(payReq);
            fw.close();
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, 2);
            ex.printStackTrace();
            return false;
        }
        if (!checkReq(payReq, tLYBankLogSchema))
        {
            return false;
        }
        ABCBankReturnXml read = new ABCBankReturnXml();
    	filePath = read.readInfo(payReq);
    	if("fail".equals(filePath)){
    		return false;
    	}
        System.out.println("成功了！！！！");
        ABCBankReadBackFileBL tABCBankReadBackFileBL = new ABCBankReadBackFileBL();
        try
        {
        	String cOperate = "DownLoad";
            if (!tABCBankReadBackFileBL.submitData( mInputData,tLYBankLogSchema, this.mPath, filePath, cOperate))
            {
                errBankLog(tLYBankLogSchema, 2);
            }
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, 2);
            ex.printStackTrace();
        }
        return true;
    }
	
	/*private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }*/
	
	//发送报文并返回报文内容
	public String SendBank(String xml) throws IOException
	{
		String info = null;
		String backInfo="";
		try
		{
			// 1.创建客户端Socket，指定服务器地址和端口
			Socket socket = new Socket("10.252.126.137", 3837);
			// 2.获取输出流，向服务器端发送信息
			OutputStream os = socket.getOutputStream();// 字节输出流
			PrintWriter pw = new PrintWriter(os);// 将输出流包装为打印流
			pw.write(xml);
			pw.flush();
			socket.shutdownOutput();// 关闭输出流
			// 3.获取输入流，并读取服务器端的响应信息
			InputStream is = socket.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			
			while ((info = br.readLine()) != null)
			{
				
				backInfo=backInfo+info;
				System.out.println("我是客户端，服务器说：" + info);
			}
			// 4.关闭资源
			br.close();
			is.close();
			pw.close();
			os.close();
			socket.close();
		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		info=backInfo;
		return info;
	}
	private String readInfo(String backInfo) {
		String flag="fail";
		int iStart = backInfo.indexOf("<RespSource>");
        if (iStart != -1)
        {
        	//判断查询是否成功
        	int end = backInfo.indexOf("</RespSource>");
            String signedMsg = backInfo.substring(iStart + 12, end);
            System.out.println("查询结果(0为成功，非零为失败)：" + signedMsg);
            if("0".equals(signedMsg)){
            	//查询成功后判断文件是否存在
            	int s_FileFlag = backInfo.indexOf("<FileFlag>");
            	if (s_FileFlag != -1){
            		int e_FileFlag = backInfo.indexOf("</FileFlag>");
                    System.out.println("截取字符串的起始位置:" + (s_FileFlag+10) + ";终止位置:" + e_FileFlag);
                    String file = backInfo.substring(s_FileFlag + 10, e_FileFlag);
                    if("1".equals(file)){
                    	//文件存在，获取文件名或路径
                    	int s_FileName = backInfo.indexOf("<BatchFileName>");
                    	if(s_FileName!=-1){
                    		int e_FileName = backInfo.indexOf("</BatchFileName>");
                    		System.out.println("截取字符串的起始位置:" + (s_FileName+15) + ";终止位置:" + e_FileName);
                            String filePath = backInfo.substring(s_FileName + 15, e_FileName);
                            System.out.println("filePath:"+filePath);
                            return filePath;
                    	}
                    	else{
                    		return flag;
                    	}
                    }
                    else{
                    	return flag;
                    }
            	}
            	else{
            		return flag;
            	}
            }
            else{
            	return flag;
            }
        }
        else{
        	return flag;
        }
	}
	
	/**
     * 获取ftp信息
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean getFTPInfo(){
    	String sqlFTPName = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPName' and Code1='7708' ";
    	String FTPName = new ExeSQL().getOneValue(sqlFTPName);
    	
    	String sqlFTPPass = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPPass' and Code1='7708' ";
    	String FTPPass = new ExeSQL().getOneValue(sqlFTPPass);
    	
    	String sqlFTPIP = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPIP' and Code1='7708' ";
    	String FTPIP = new ExeSQL().getOneValue(sqlFTPIP);
    	
    	String sqlFTPPort = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPPort' and Code1='7708' ";
    	String FTPPort = new ExeSQL().getOneValue(sqlFTPPort);
    	
    	String sqlFTPPath = "select Codename from ldcode1 where Codetype='ABCBankFTP' and Code='FTPPath' and Code1='7708' ";
    	String FTPPath = new ExeSQL().getOneValue(sqlFTPPath);
    	
    	inTransferData.setNameAndValue("FTPName", FTPName);
    	inTransferData.setNameAndValue("FTPPass", FTPPass);
    	inTransferData.setNameAndValue("FTPIP", FTPIP);
    	inTransferData.setNameAndValue("FTPPort", FTPPort);
    	inTransferData.setNameAndValue("FTPPath", FTPPath);
    	
    	mInputData = new VData();

        try
        {
        	mInputData.clear();
            mInputData.add(inTransferData);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }

        return true;
    	
    }
	
	/**
     * newFolder
     * 新建报文存放文件夹，以便对查询和回盘报文查询
     * @return boolean
     *//*
	public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdirs();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }*/

	/**
     * connect
     * 登录ftp
     * @return boolean
     */
	/*public boolean connect(){
		String hostname="10.252.126.137";//FTP服务器hostname  
		int port=21;//FTP服务器端口 
		String username="sff";
		String password="ADMIN!1234";
		//String pathname="D:\\ABCBankErp\\data\\";//FTP服务器上的相对路径
	    boolean flag = false;
	    FTPClient ftpClient = new FTPClient();
	    try {
	      //连接FTP服务器
	      ftpClient.connect(hostname, port);
	      //登录FTP服务器
	      ftpClient.login(username, password);
	      //验证FTP服务器是否登录成功
	      int replyCode = ftpClient.getReplyCode();
	      if(!FTPReply.isPositiveCompletion(replyCode)){
	        return flag;
	      }
	      flag=true;
	    }catch(Exception e) {
		      e.printStackTrace();
		      }
	    return flag;
	}*/
	
	/**
     * disconnect
     * 断开ftp
     * @return boolean
     */
	/*public boolean disconnect(){
		boolean flag = false;
		FTPClient ftpClient = new FTPClient();
		
		if(ftpClient.isConnected()){
	        try {
	          ftpClient.logout();
	          flag=true;
	        } catch (IOException e) {
	          
	        }
	      }
		return flag;
	}*/
	
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }
    
	public static void main(String[] args){
		ABCBankReturnXml abc = new ABCBankReturnXml();
		abc.dealDate();
	}
}
