package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDBankUniteSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * @author 
 * @version 1.0
 */
public class RealTimeSendCreateXml
{
    private String mUrl = "";

    private String feeType = "";

    private String serialNo = "";
    
    private String filePath = "";

    private MMap map = new MMap();

    private VData mInputData = new VData();
    
    private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();

	private PubSubmit tPubSubmit = new PubSubmit();
    /**
     * @param mUrl 报文存放路径
     * @param feeType 收付费标识
     * @param ubank 银行编码
     */
    public boolean submitData(String mUrl, String feeType, String ubank)
    {
        this.mUrl = mUrl + "/";
        this.feeType = feeType;
        this.serialNo = ubank;

        if (!dealDate())
        {
            System.out.println("获取数据失败");
            return false;
        }

        return true;
    }

    public boolean dealDate()
    {
        String sql = "select * from lybanklog a where a.dealstate is null and a.senddate is null and a.serialno ='"
                + serialNo
                + "' and logtype='"
                + feeType
                + "' "
                + "and (select count(*) from lysendtobank where serialno=a.serialno)>0 with ur";
        System.out.println(sql);
        LYBankLogDB tLyBankLogDB = new LYBankLogDB();
        LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
        if (tLYBankLogSet == null)
        {
            System.out.println("未查到发送银行数据");
            return false;
        }
        for (int i = 1; i <= tLYBankLogSet.size(); i++)
        {
            LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
            String serialno = tLYBankLogSchema.getSerialNo();
            sql = "select * from lysendtobank where serialno='" + serialno
                    + "' with ur";
            System.out.println(sql);
            LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
            mLYSendToBankSet = tLYSendToBankDB
                    .executeQuery(sql);
          
            // 判断批次下是否有异常的收费数据（并发数据）
            if("R".equals(feeType) && !checkLysendtobank(serialno, tLYBankLogSchema)){
            	// 若有问题，停止该批次的发送，并发送邮件
            	errBankLog(tLYBankLogSchema);
            	continue;
            }
            if (!createXml(tLYBankLogSchema, mLYSendToBankSet))
            {
            	errBankLog(tLYBankLogSchema);
            	continue;
            }
        }
        return true;
    }

    public boolean createXml(LYBankLogSchema tLYBankLogSchema,
            LYSendToBankSet tLYSendToBankSet)
    {
        String bankUniteCode = tLYBankLogSchema.getBankCode();
        String type = tLYBankLogSchema.getLogType();
        String sql = "select codename from ldcode1 where codetype='RealTimeSendBank' and code='UserName' and code1='"
                + type + bankUniteCode + "'";
        String userName = new ExeSQL().getOneValue(sql);
        sql = "select codename from ldcode1 where codetype='RealTimeSendBank' and code='UserPass' and code1='"
                + type + bankUniteCode + "'";
        String userPass = new ExeSQL().getOneValue(sql);
        sql = "select codename from ldcode1 where codetype='RealTimeSendBank' and code='MerchantID' and code1='"
                + type + bankUniteCode + "'";
        String merchantID = new ExeSQL().getOneValue(sql);

        Element root = null;
        if (bankUniteCode.equals("7705"))
        {
            root = new Element("GZELINK");
        }
        else if (bankUniteCode.equals("7706"))
        {
            root = new Element("AIPG");
        }
        

        Document doc = new Document(root);

        Element ele_INFO = new Element("INFO");
        if (type.equals("R"))
        {
            setText(ele_INFO, "TRX_CODE", "100004");
        }
        if (bankUniteCode.equals("7705"))
        {
            setText(ele_INFO, "VERSION", "04");
        }
        else
        {
            setText(ele_INFO, "VERSION", "03");
        }

        setText(ele_INFO, "DATA_TYPE", "2");
        setText(ele_INFO, "LEVEL", "0");
        setText(ele_INFO, "USER_NAME", userName);
        setText(ele_INFO, "USER_PASS", userPass);
        setText(ele_INFO, "REQ_SN", tLYBankLogSchema.getSerialNo());
        setText(ele_INFO, "SIGNED_MSG", "");
        root.addContent(ele_INFO);

        Element ele_BODY = new Element("BODY");
        Element ele_TRANS_SUM = new Element("TRANS_SUM");
        if (type.equals("R"))
        {
            setText(ele_TRANS_SUM, "BUSINESS_CODE", "10600");
        }
        
        setText(ele_TRANS_SUM, "MERCHANT_ID", merchantID);
        if (bankUniteCode.equals("7706"))
        {
            setText(ele_TRANS_SUM, "SETTDAY", "");
        }
        setText(ele_TRANS_SUM, "SUBMIT_TIME", PubFun.getCurrentDate2()
                + PubFun.getCurrentTime2());
        setText(ele_TRANS_SUM, "TOTAL_ITEM", Integer.toString(tLYSendToBankSet
                .size()));
        double sum = 0;
        for (int i = 1; i <= tLYSendToBankSet.size(); i++)
        {
            sum = Arith.add(sum, tLYSendToBankSet.get(i).getPayMoney());
        }
        //setText(ele_TRANS_SUM, "TOTAL_SUM", Long.toString(Math.round(sum * 100)));//一分钱问题。。害人啊
        setText(ele_TRANS_SUM, "TOTAL_SUM", Long.toString((long) Arith.mul(
                sum, 100)));//最后权衡还是使用精确类计算，放弃四舍五入
        ele_BODY.addContent(ele_TRANS_SUM);

        Element ele_TRANS_DETAILS = new Element("TRANS_DETAILS");
        DecimalFormat df = new DecimalFormat("0000");
        for (int i = 1; i <= tLYSendToBankSet.size(); i++)
        {
            LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(i);
            Element ele_TRANS_DETAIL = new Element("TRANS_DETAIL");
            setText(ele_TRANS_DETAIL, "SN", df.format(i));
            setText(ele_TRANS_DETAIL, "E_USER_CODE", "");
            String bank = uniteBankCode(tLYSendToBankSchema.getBankCode(),
                    bankUniteCode);
            String bsql="select bankunitecode  from ldbankunite where bankcode="+tLYSendToBankSchema.getBankCode()+"";
        	String bcode = new ExeSQL().getOneValue(bsql);
        	if (bcode.equals("7703")){
        		setText(ele_TRANS_DETAIL, "BANK_CODE", "403");
        	}
        	else
        	{
        		setText(ele_TRANS_DETAIL, "BANK_CODE", bank);
        	}
            setText(ele_TRANS_DETAIL, "ACCOUNT_TYPE", "");
            setText(ele_TRANS_DETAIL, "ACCOUNT_NO", tLYSendToBankSchema
                    .getAccNo());
            setText(ele_TRANS_DETAIL, "ACCOUNT_NAME", tLYSendToBankSchema
                    .getAccName());
            setText(ele_TRANS_DETAIL, "PROVINCE", otherInfo(bank,
                    tLYSendToBankSchema.getComCode(), 1));
            setText(ele_TRANS_DETAIL, "CITY", otherInfo(bank,
                    tLYSendToBankSchema.getComCode(), 2));
            setText(ele_TRANS_DETAIL, "BANK_NAME", "");
            setText(ele_TRANS_DETAIL, "ACCOUNT_PROP", "0");
            setText(ele_TRANS_DETAIL, "AMOUNT", Long.toString((long) Arith
                    .mul(tLYSendToBankSchema.getPayMoney(), 100)));//一分钱问题。。害人啊
            setText(ele_TRANS_DETAIL, "CURRENCY", "");
            setText(ele_TRANS_DETAIL, "PROTOCOL", "");
            setText(ele_TRANS_DETAIL, "PROTOCOL_USERID", "");
            setText(ele_TRANS_DETAIL, "ID_TYPE", getID(tLYSendToBankSchema,
                    bank, type, 1));
            setText(ele_TRANS_DETAIL, "ID", getID(tLYSendToBankSchema, bank,
                    type, 2));
            setText(ele_TRANS_DETAIL, "TEL", "");
            if (bankUniteCode.equals("7705"))
            {
                setText(ele_TRANS_DETAIL, "RECKON_ACCOUNT", "");
            }
            setText(ele_TRANS_DETAIL, "CUST_USERID", "");
            if (bankUniteCode.equals("7706"))
            {
                setText(ele_TRANS_DETAIL, "SETTACCT", "");
            }
            setText(ele_TRANS_DETAIL, "REMARK", tLYSendToBankSchema
                    .getPayCode());
            if (bankUniteCode.equals("7705"))
            {
                setText(ele_TRANS_DETAIL, "RESERVE1", "");
                setText(ele_TRANS_DETAIL, "RESERVE2", "");
            }
            ele_TRANS_DETAILS.addContent(ele_TRANS_DETAIL);
        }
        ele_BODY.addContent(ele_TRANS_DETAILS);
        root.addContent(ele_BODY);

        XMLOutputter XMLOut = new XMLOutputter();
        try
        {
        	this.filePath = this.mUrl + tLYBankLogSchema.getSerialNo() + ".xml";
            XMLOut.setEncoding("GBK");
            XMLOut.output(doc, new FileOutputStream(this.mUrl
                    + tLYBankLogSchema.getSerialNo() + ".xml"));
            String sendStr = readtxt(this.mUrl + tLYBankLogSchema.getSerialNo()
                    + ".xml");
            System.out.println("发送报文内容" + sendStr);

            if (!sendStr.equals(""))
            {
                if (bankUniteCode.equals("7705"))
                {
                    RealTimeProcess tBatchProcess = new RealTimeProcess(
                            bankUniteCode, type);
                    String payReq = tBatchProcess.PayReq(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        if(!updateLog(tLYBankLogSchema))return false;
                        saveReq(payReq, tLYBankLogSchema);
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema);
                    }
                }
                else if (bankUniteCode.equals("7706"))
                {
                    BatchProcessCom tBatchProcessCom = new BatchProcessCom(
                            bankUniteCode, type);
                    String payReq = tBatchProcessCom.send(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        saveReq(payReq, tLYBankLogSchema);
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            System.out.println("生成xml失败");
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean updateLog(LYBankLogSchema tLYBankLogSchema) {
    	tLYBankLogSchema.setOutFile(this.filePath);
    	tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());
    	LYBankLogDB tLYBankLogDB = new LYBankLogDB();
    	tLYBankLogDB.setSchema(tLYBankLogSchema);
    	tLYBankLogDB.update();
		return true;
	}

	/**
     * 添加子节点
     * @param element 
     * @param name 节点名称
     * @param value 节点值
     */
    private void setText(Element element, String name, String value)
    {

        Element ele = new Element(name);
        ele.addContent(value);
        element.addContent(ele);
    }

    private String uniteBankCode(String bankcode, String bankunitecode)
    {
    	
        String sql = "select * from ldbankunite where bankcode='" + bankcode
                + "' and bankunitecode in ('7705','7706','7709','7701','7703') with ur";
        System.out.println(sql);
        LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
        LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.executeQuery(sql);
        LDBankUniteSchema tLDBankUniteSchema = tLDBankUniteSet.get(1);
        return tLDBankUniteSchema.getUniteBankCode();
    }

    /**
     * 获取报文省市节点值
     * @param bank 银行代码
     * @param comCode 机构
     * @param othertype 1为省 2为市
     * @return String
     */
    private String otherInfo(String bank, String comCode, int othertype)
    {
        if (othertype == 1)
        {
            if (bank.equals("04105840"))
            {
                return "广东";
            }
            else
            {
                if (comCode.length() >= 4)
                {
                    String sql = "select codename from ldcode where codetype='ComProvince' and code='"
                            + comCode.substring(0, 4) + "'";
                    String com = new ExeSQL().getOneValue(sql);
                    if (com != null)
                    {
                        return com;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return "北京";
                }
            }
        }
        else if (othertype == 2 && bank.equals("04105840"))
        {
            return "深圳";
        }
        return "";
    }

    /**
     * 获取身份证节点值
     * @param bank 银行代码
     * @param type 收付费类型
     * @param flag 1为证件类型 2为证件号
     * @return String
     */
    private String getID(LYSendToBankSchema tLYSendToBankSchema, String bank,
            String type, int flag)
    {
        if (type.equals("R")
                && (bank.equals("307") || bank.equals("302")
                        || bank.equals("301") || bank.equals("303")
                        || bank.equals("305") || bank.equals("04105840")))
        {
            if (flag == 1)
            {
                return "0";
            }
            else
            {
                return tLYSendToBankSchema.getIDNo();
            }
        }
        return "";
    }


    /**
     * 存储、读取返回报文
     * @param payReq 返回报分字符串
     */
    private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema)
    {
        try
        {
        	String refile = this.mUrl + tLYBankLogSchema.getSerialNo() + "rq.xml";
            FileWriter fw = new FileWriter(refile);
            fw.write(payReq);
            fw.close();
            RealTimeReturnXml tRealTimeReturnXml = new RealTimeReturnXml();
            tRealTimeReturnXml.submitData(refile,tLYBankLogSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 连接失败更新日志
     */
    private void errBankLog(LYBankLogSchema tLYBankLogSchema)
    {
        System.out.println("获取响应信息失败");
        MMap tmap = new MMap();
        tmap.put(tLYBankLogSchema, "DELETE");
        tmap.put(mLYSendToBankSet, "DELETE");
       
		mInputData.clear();
		mInputData.add(tmap);

		tPubSubmit.submitData(mInputData, "");
    }

    /**
     * 将发送报文转换为字符串
     * @param path 生成报文路径
     */
    private String readtxt(String path)
    {
        String str = "";
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(path));

            String r = br.readLine();
            while (r != null)
            {
                str += r;
                r = br.readLine();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
        str = str.replaceAll("<SIGNED_MSG />", "<SIGNED_MSG></SIGNED_MSG>");
        return str;
    }
    
    
    /**
     * 校验批次下是否有异常的付费数据，对于异常数据会发送邮件
     * 
     * @return
     */
    private boolean checkLysendtobank(String serialno, LYBankLogSchema tLYBankLogSchema){
    	// 校验是否有异常付费数据
    	String sql = "select paycode,polno from lysendtobank sb where serialno='" 
    			+ serialno 
    			+ "' and  exists (select 1 from lysendtobank  where paycode=sb.paycode and serialno <>sb.serialno )"
    			+ " union all select paycode,polno from lysendtobank where serialno='"
    			+ serialno 
    			+ "' and  exists (select 1 from ljtempfee  where paycode=ljtempfee.tempfeeno and confmakedate is not null )" ;
    	ExeSQL tEx = new ExeSQL();
    	SSRS tSSRS = tEx.execSQL(sql);
    	if(tSSRS.getMaxRow() == 0){
    		return true;
    	} else if(!"1".equals(tLYBankLogSchema.getOthFlag())){
    		// 批次没有发送过邮件
    		// 发送邮件
    		MailSender tMailSender = new MailSender("itoperation", "11111111","picchealth");
    		// 设置邮件发送信息
    		String sendInf = "实时代收" + serialno + "批次出现异常收费数据，已停止该批次的发送，请核查批次内信息。";
    		for(int row = 1; row <= tSSRS.getMaxRow(); row++){
    			sendInf = sendInf + "收费号：" + tSSRS.GetText(row, 1) + "，业务号:" + tSSRS.GetText(row, 2) + "。";
    		}
    		tMailSender.setSendInf("实时代收异常批次", sendInf, null);
    		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
    		tMailSender.setToAddress("Real7705");
    		if (!tMailSender.sendMail()) {
    			tMailSender.getErrorMessage();
    		}
    		// 更新日志表输出信息
    		tLYBankLogSchema.setOutFile("批次下存在异常收费数据，发送失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
            // 已发送邮件标记
            tLYBankLogSchema.setOthFlag("1");
            this.map.put(tLYBankLogSchema, "UPDATE");
    		return false;
    	} else {
    		return false;
    	}
    }
    
    
    
    public static void main(String[] arr)
    {
    }
}
