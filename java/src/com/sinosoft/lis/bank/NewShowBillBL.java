package com.sinosoft.lis.bank;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

/**
 * <p>Title: 查询批次号程序</p>
 * <p>Description: 根据条件查询出要打印的批次号</p>
 * <p>Copyright: Copyright (c) 2003-04-02
 * <p>Company: Sinosoft</p>
 * @author 刘岩松
 * @version 1.0
 * @function show BillNo that was selected from LYBankLog Business Logic Layer
 */
public class NewShowBillBL
{
  public  CErrors mErrors = new CErrors();
  private VData mInputData ;
  private VData mResult = new VData();
  private String mOperate;
  private String strStartDate;
  private String strEndDate;
  private String strBankCode;
  private String strFlag;
  private GlobalInput mG = new GlobalInput();
  private String t_ComCode;
  private String strTFFlag = "";
  private String strXQFlag = "";
  private PremBankPubFun mPremBankPubFun = new PremBankPubFun ();


  public NewShowBillBL() {}

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return: true or false
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;
    if (!getInputData(cInputData))
      return false;
    if(!queryData())
      return false;
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }
  /****
   * @function :receive parameter and checkdate
   * @receive :receive parameter from jsp
   * @checkdate:judge whether startDate before EndDate
   * @return true or false
   ****/
  private boolean getInputData(VData cInputData)
  {
    if (mOperate.equals("SHOW"))
    {
      strStartDate=(String)cInputData.get(0);//开始日期
      strEndDate = (String)cInputData.get(1);//结束日期
      strBankCode = (String)cInputData.get(2);//银行代码（可以是空）
      strFlag = (String)cInputData.get(3);//查询的标准“YF”or“YS”
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      t_ComCode = mG.ManageCom;
      strTFFlag = (String)cInputData.get(4);//正确还是错误清单
      strXQFlag = (String)cInputData.get(5);//首期还是续期的标志
      //trim所以的传入变量
      strStartDate = strStartDate.trim();
      strEndDate=strEndDate.trim();
      //strBankCode = strBankCode.trim();
      strFlag=strFlag.trim();
      t_ComCode=t_ComCode.trim() ;
      strTFFlag = strTFFlag.trim();
      strXQFlag = strXQFlag.trim();
//      System.out.println("strStartDate"+strStartDate);
//      System.out.println("strEndDate"+strEndDate);
//      System.out.println("strBankCode"+strBankCode);
//      System.out.println("strFlag"+strFlag);
//      System.out.println("strTFFlag"+strTFFlag);
//      System.out.println("strXQFlag"+strXQFlag);
//      System.out.println("t_ComCode"+t_ComCode);
    }
    return true;
  }

  /**
   * @function select BillNo from LYBankLog
   * @execute different SQL bansd on different OperateFlag
   * return true or false
   * */
  private boolean queryData()
  {
    //注意事项：管理机构要以登陆的管理机构为准。与界面上录入的管理机构是无关的。
    LYBankLogSet tLYBankLogSet = new LYBankLogSet();
    String main_sql = getMainSqL(strFlag,strXQFlag,strBankCode);
    System.out.println("主查询语句是"+main_sql);
    ExeSQL mainexesql = new ExeSQL();
    SSRS mainssrs = new SSRS();
    mainssrs = mainexesql.execSQL(main_sql);
    System.out.println("查询的结果是"+mainssrs.getMaxRow());
    if(mainssrs.getMaxRow()>0)
    {
      for(int m_count=1;m_count<=mainssrs.getMaxRow();m_count++)
      {
        System.out.println("LYBankLog中的记录是"+m_count);
        System.out.println("批次号码是"+mainssrs.GetText(m_count,1));
        System.out.println("查找出该批次号码所对应的银行返回的件数和金额合计");
        String sec_sql = "";
        if("7703".equals(strBankCode))
        {	
        	System.out.println("邮宝通清单打印");
        	if("F".equals(strTFFlag))
        	{
        	sec_sql = "select serialno,bankcode,sum(paymoney) ,count (*) from lyreturnfrombankb a"
                +" where serialno ='"+mainssrs.GetText(m_count,1)+"' and banksuccflag='1111' "
                //+" and 0=(select count(*) from ljspay b where b.getnoticeno = a.paycode and (b.cansendbank is null or b.cansendbank ='0') )"
                //+" and 0=(select count(*) from ljaget b where b.actugetno = a.paycode and (b.cansendbank is null or b.cansendbank ='0') )"
                +" group by serialno,bankcode union all "
                +" select serialno,bankcode,sum(paymoney) ,count (*) from lyreturnfrombank a "
                +" where serialno  ='"+mainssrs.GetText(m_count,1)+"' and banksuccflag='1111' "
                //+" and 0=(select count(*) from ljspay b where b.getnoticeno = a.paycode and (b.cansendbank is null or b.cansendbank ='0') )"
                //+" and 0=(select count(*) from ljaget b where b.actugetno = a.paycode and (b.cansendbank is null or b.cansendbank ='0') )"
                +" group by serialno,bankcode ";
        
        	}
        	else
        	{
        	sec_sql = "select serialno,bankcode,sum(paymoney) ,count (*) from lyreturnfrombankb "
                +" where serialno ='"+mainssrs.GetText(m_count,1)+"' and banksuccflag='0000' group by serialno,bankcode union all "
                +" select serialno,bankcode,sum(paymoney) ,count (*) from lyreturnfrombank "
                +" where serialno  ='"+mainssrs.GetText(m_count,1)+"' and banksuccflag='0000' group by serialno,bankcode ";
        	}
        }
        else
        {
        	if("T".equals(strTFFlag))
        	{
        		sec_sql = "select serialno,bankcode,sum(paymoney) ,count (*) from lyreturnfrombankb "
                +" where serialno ='"+mainssrs.GetText(m_count,1)+"' and comcode like '"+t_ComCode+"%' and paytype = '"+strFlag.substring(1, 2)+"' group by serialno,bankcode union all "
                +" select serialno,bankcode,sum(paymoney) ,count (*) from lyreturnfrombank "
                +" where serialno  ='"+mainssrs.GetText(m_count,1)+"' and comcode like '"+t_ComCode+"%' and paytype = '"+strFlag.substring(1, 2)+"' group by serialno,bankcode ";
        	}
        	else
        	{
        		sec_sql = "select serialno,bankcode,sum(paymoney) ,count (*) from lyreturnfrombankb "
                    +" where serialno ='"+mainssrs.GetText(m_count,1)+"' and comcode like '"+t_ComCode+"%' and paytype is null group by serialno,bankcode union all "
                    +" select serialno,bankcode,sum(paymoney) ,count (*) from lyreturnfrombank "
                    +" where serialno  ='"+mainssrs.GetText(m_count,1)+"' and comcode like '"+t_ComCode+"%' and paytype is null group by serialno,bankcode ";
        	}
        }
        System.out.println("查询的语句是"+sec_sql);
        ExeSQL secexesql = new ExeSQL();
        SSRS secssrs = secexesql.execSQL(sec_sql);
        int mCount = 0;
        double mSumMoney = 0.00;
        if(secssrs.getMaxRow()>0)
        {
          LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

          for(int s_count=1;s_count<=secssrs.getMaxRow();s_count++)
          {
            mSumMoney = mSumMoney + Double.parseDouble(secssrs.GetText(s_count,3));
            mCount = mCount + Integer.parseInt(secssrs.GetText(s_count,4));
            System.out.println("批次是"+Integer.parseInt(secssrs.GetText(s_count,4)));
          }
          tLYBankLogSchema.setSerialNo(secssrs.GetText(1,1));
          tLYBankLogSchema.setBankCode(secssrs.GetText(1,2));
          tLYBankLogSchema.setTotalMoney(mSumMoney);

          tLYBankLogSchema.setTotalNum(mCount);
          tLYBankLogSet.add(tLYBankLogSchema);
          System.out.println("批次号码是"+tLYBankLogSchema.getSerialNo());
          System.out.println("银行代码是"+tLYBankLogSchema.getBankCode());
          System.out.println("金额合计"+tLYBankLogSchema.getTotalMoney());
          System.out.println("数量合计"+tLYBankLogSchema.getTotalNum());
        }
      }
    }
    if(tLYBankLogSet.size()<=0)
    {
      CError tError = new CError();
      tError.moduleName = "PEfficiencyBL";
      tError.functionName = "getDutyGetClmInfo";
      tError.errorMessage = "该时间段内没有发生银行返回盘的记录信息，请您从新录入查询条件！" ;
      this.mErrors.addOneError(tError) ;
      return false;
    }
    mResult.add(tLYBankLogSet);
    return true;
  }
  /****************************************************************************************
   * Name:getMainSqL
   * Parameter:
   * strFlag----判断是“YS”代收还是“YF”代付
   * tXQFlag----记录是首期还是续期的标志(0---首期，1---续期)
   * tBankCode---记录银行代码
   * Return value :主查询语句(查询出符合条件的批次)
   *
   */
  private String getMainSqL(String strFlag,String tXQFlag,String tBankCode )
  {
    String main_sql = "" ;
    String SF_flag = "";
    if(strFlag.equals("YS"))
    {
      SF_flag = "S" ;
    }
    if(strFlag.equals("YF"))
    {
      SF_flag = "F" ;
    }
    String addsql = " and LogType = '"+SF_flag+"' and ReturnDate is not null";
    if(!((tBankCode==null)||(tBankCode.equals(""))))
    {
      addsql = addsql + " and BankCode = '"+tBankCode+"' ";
    }
    System.out.println("tXQFlag");
//    main_sql=" select serialno from lybanklog where 1=1 and ComCode like '"+t_ComCode
//            +"%' and StartDate >='"+strStartDate+"'and StartDate <= '"+strEndDate
//            +"'" +addsql ;
    main_sql=" select serialno from lybanklog where 1=1 and StartDate >='"+strStartDate+"'and StartDate <= '"+strEndDate
    +"'" +addsql ;
    System.out.println("查询的语句是"+main_sql);
    return main_sql ;
  }
  public static void main(String[] args)
  {
    String aStartDate="2004-7-1";                //开始日期
    String aEndDate="2004-7-9";                  //结束日期
    String aBankCode = "";
    String aFlag = "YS";
    GlobalInput tG = new GlobalInput();
    tG.ManageCom="8611";
    String aTFFlag = "F";//1是正确2是错误
    String aXQFlag = "S";

    VData tVData = new VData();
    tVData.addElement(aStartDate);
    tVData.addElement(aEndDate);
    tVData.addElement(aBankCode);
    tVData.addElement(aFlag);
    tVData.addElement(aTFFlag);
    tVData.addElement(aXQFlag);
    tVData.addElement(tG);

    NewShowBillBL tNewShowBillBL = new NewShowBillBL();
    tNewShowBillBL.submitData(tVData,"SHOW");
  }
}