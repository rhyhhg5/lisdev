package com.sinosoft.lis.bank;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到文件模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class MedicalDealSBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    //业务数据
    private LYSendToBankSchema inLYSendToBankSchema = new LYSendToBankSchema();

    private TransferData inTransferData = new TransferData();

    private String fileName = "";

    private GlobalInput inGlobalInput = new GlobalInput();

    private String bankCode = "";

    private String DealType = "";

    private LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();

    private LYBankLogSet outLYBankLogSet = new LYBankLogSet();

    public MedicalDealSBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"WRITE"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
            return false;
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
            return false;
        System.out.println("---End dealData---");

        //生成文件标识
        if (mOperate.equals("WRITE"))
        {
            //准备往后台的数据
            if (!prepareOutputData())
                return false;
            System.out.println("---End prepareOutputData---");

            System.out.println("Start WriteToFile BLS Submit...");
            MedicalDealSBLS tMedicalDealSBLS = new MedicalDealSBLS();
            if (tMedicalDealSBLS.submitData(mInputData, cOperate) == false)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tMedicalDealSBLS.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("End WriteToFile BLS Submit...");

            //如果有需要处理的错误，则返回
            if (tMedicalDealSBLS.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tMedicalDealSBLS.mErrors);
            }
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            inLYSendToBankSchema = (LYSendToBankSchema) mInputData
                    .getObjectByObjectName("LYSendToBankSchema", 0);
            TransferData tTransferData = new TransferData();
            tTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);
            DealType = (String) tTransferData.getValueByName("DealType");
            inGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 获取发送银行数据信息
     * @param tLYSendToBankSchema
     * @return
     */
    private LYSendToBankSet getLYSendToBank(
            LYSendToBankSchema tLYSendToBankSchema)
    {
        LYSendToBankSet tLYSendToBankSet = tLYSendToBankSchema.getDB().query();
        return tLYSendToBankSet;
    }

    /**
     * 生成银行日志数据
     * @param tLYSendToBankSchema
     * @return
     */
    private LYBankLogSet getLYBankLog(LYSendToBankSchema tLYSendToBankSchema)
    {
        LYBankLogSet tLYBankLogSet = new LYBankLogSet();

        //获取日志记录
        LYBankLogDB tLYBankLogDB = new LYBankLogDB();
        tLYBankLogDB.setSerialNo(tLYSendToBankSchema.getSerialNo());
        if (!tLYBankLogDB.getInfo())
        {
            CError.buildErr(this, "获取银行日志数据失败");
            return null;
        }
        LYBankLogSchema tLYBankLogSchema = tLYBankLogDB.getSchema();

        //构建文件名，未加后缀名
        fileName = "M" + tLYBankLogSchema.getBankCode() + tLYBankLogSchema.getLogType()
                + tLYBankLogSchema.getSerialNo() + "("
                + PubFun.getCurrentDate() + ")";
        System.out.println(tLYSendToBankSchema.getBankCode()+"========================");
        System.out.println("zb__fileName :" + fileName);
        //修改日志
        tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setSendOperator(inGlobalInput.Operator);
        
        tLYBankLogSchema.setOutFile(fileName + ".z");
        tLYBankLogSet.add(tLYBankLogSchema);

        return tLYBankLogSet;
    }

    private String getFilePath()
    {
        LDSysVarSchema tLDSysVarSchema = new LDSysVarSchema();

        tLDSysVarSchema.setSysVar("SendToMedicalPath");
        tLDSysVarSchema = tLDSysVarSchema.getDB().query().get(1);
        return tLDSysVarSchema.getSysVarValue();
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            //生成银行文件数据
            if (mOperate.equals("WRITE"))
            {
                //获取发送银行数据信息
                LYSendToBankSet tLYSendToBankSet = getLYSendToBank(inLYSendToBankSchema);
                if (tLYSendToBankSet.size() == 0)
                    throw new NullPointerException("无发送银行数据！");

                //生成银行日志数据,构建文件名
                LYBankLogSet tLYBankLogSet = getLYBankLog(inLYSendToBankSchema);
                if (tLYBankLogSet.size() == 0)
                    throw new NullPointerException("无银行日志数据！");

                String folder = PubFun.getCurrentDate2() + "/"; 
                //生成发送文件全路径
                fileName = getFilePath() + folder + fileName;
                bankCode = tLYBankLogSet.get(1).getBankCode();

                outLYSendToBankSet.set(tLYSendToBankSet);
                outLYBankLogSet.set(tLYBankLogSet);
                inTransferData.setNameAndValue("fileName", fileName);
                inTransferData.setNameAndValue("bankCode", bankCode);
                inTransferData.setNameAndValue("DealType", DealType);
            }

        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "数据处理错误" + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mInputData = new VData();

        try
        {
            mInputData.add(outLYSendToBankSet);
            mInputData.add(outLYBankLogSet);
            mInputData.add(inTransferData);
            mInputData.add(inGlobalInput);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        mResult.add(fileName);
        return mResult;
    }

    public static void main(String[] args)
    {
        MedicalDealSBL writeToFileBL1 = new MedicalDealSBL();
    }
}
