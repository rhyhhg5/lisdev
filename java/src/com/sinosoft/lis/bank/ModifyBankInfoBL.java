package com.sinosoft.lis.bank;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 修改客户的银行信息</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class ModifyBankInfoBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /** 全局基础数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  /** 额外传递的参数 */
  //private TransferData mTransferData = null;

  /** 传入的业务数据 */
  private LCContSet inLCContSet = new LCContSet();

  /** 传出的业务数据 */
  private LCContSet outLCContSet = new LCContSet();
  private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJSPaySet outLJSPaySet = new LJSPaySet();

  public ModifyBankInfoBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //需要传到后台处理
    if (mOperate.equals("INSERT")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start ModifyBankInfo BLS Submit...");
      ModifyBankInfoBLS tModifyBankInfoBLS = new ModifyBankInfoBLS();
      if(tModifyBankInfoBLS.submitData(mInputData, cOperate) == false)	{
        //@@错误处理
        this.mErrors.copyAllErrors(tModifyBankInfoBLS.mErrors);
        mResult.clear();
        mResult.add(mErrors.getFirstError());
        return false;
      }	else {
        mResult = tModifyBankInfoBLS.getResult();
      }
      System.out.println("End ModifyBankInfo BLS Submit...");
    }
    //不需要传到后台处理
    else if (mOperate.equals("")) {
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);

      //mTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);

      inLCContSet = (LCContSet)mInputData.getObjectByObjectName("LCContSet", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ModifyBankInfoBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 修改保单表
   * @param inLCContSchema
   */
  private void modifyLCCont(LCContSchema inLCContSchema) {
    //搜索出该印刷号对应的所有投保单
    LCContSchema tLCContSchema = new LCContSchema();
    tLCContSchema.setPrtNo(inLCContSchema.getPrtNo());
    tLCContSchema.setAppFlag("0");
    LCContSet tLCContSet = tLCContSchema.getDB().query();

    //修改该印刷号对应的所有投保单的银行信息
    for (int i=0; i<tLCContSet.size(); i++) {
      /*Lis5.3 upgrade get
      tLCContSet.get(i+1).setBankCode(inLCContSchema.getBankCode());
      tLCContSet.get(i+1).setBankAccNo(inLCContSchema.getBankAccNo());
      tLCContSet.get(i+1).setAccName(inLCContSchema.getAccName());
    */
    }

    outLCContSet = tLCContSet;
  }

	/**
	 * 修改暂交费分类表
	 * @param inLCContSchema
	*/
	private void modifyLJTempFeeClass(LCContSchema inLCContSchema) {
		//搜索出暂交费分类表中交费方式为银行转账（4），且未到帐的数据
		LJTempFeeClassDB tLJTempFeeClassDB= new LJTempFeeClassDB();
		String strSql = "select * from LJTempFeeClass where PayMode='4' and EnterAccDate is null "
			+ " and TempFeeNo in (select TempFeeNo from LJTempFee where OtherNo = '"
			+ inLCContSchema.getPrtNo() + "' and EnterAccDate is null)";
		LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(strSql);

		LJTempFeeClassSchema tLJTempFeeClassSchema;
		//如果未到帐，则修改分类表的银行信息
		for (int j = 0; j < tLJTempFeeClassSet.size(); j++) {
			tLJTempFeeClassSchema = tLJTempFeeClassSet.get(j+1);
			tLJTempFeeClassSchema.setBankCode(inLCContSchema.getBankCode());
			tLJTempFeeClassSchema.setBankAccNo(inLCContSchema.getBankAccNo());
			tLJTempFeeClassSchema.setAccName(inLCContSchema.getAccName());
			outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
		}
	}

	/**
	 * 修改应收总表
	 * @param inLCContSchema
	 */
	private void modifyLJSPay(LCContSchema inLCContSchema) throws Exception {
		LJSPayDB tLJSPayDB = new LJSPayDB();
		tLJSPayDB.setOtherNo(inLCContSchema.getPrtNo());
		LJSPaySet tLJSPaySet = tLJSPayDB.query();
		LJSPaySchema mLJSPaySchema;
		if (tLJSPaySet.size() > 0)
		{
			mLJSPaySchema = tLJSPaySet.get(1);
			if (mLJSPaySchema.getBankOnTheWayFlag().equals("1"))
			{
				throw new Exception("该印刷号有财务数据发往银行途中，不能修改银行信息！");
			}
			if (mLJSPaySchema.getCanSendBank() == null || !mLJSPaySchema.getCanSendBank().equals("1"))
			{
				throw new Exception("该印刷号的财务数据未锁定，不能修改银行信息！");
			}
			mLJSPaySchema.setBankCode(inLCContSchema.getBankCode());
			mLJSPaySchema.setAccName(inLCContSchema.getAccName());
			mLJSPaySchema.setBankAccNo(inLCContSchema.getBankAccNo());
			
			outLJSPaySet.add(mLJSPaySchema);
		}
	}

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      if (mOperate.equals("INSERT")) {
        LCContSchema inLCContSchema = inLCContSet.get(1);

        //修改保单表，暂时不修改投保单表，另外修改
//        modifyLCCont(inLCContSchema);

        //修改暂交费分类表
        modifyLJTempFeeClass(inLCContSchema);

        //修改应收总表
        modifyLJSPay(inLCContSchema);
      }
      else if (mOperate.equals("")) {
      }
    }
    catch(Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "ModifyBankInfoBL";
      tError.functionName = "dealData";
      tError.errorMessage = "数据处理错误! " + e.getMessage();
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();

//      mInputData.add(outLCContSet);
      mInputData.add(outLJTempFeeClassSet);
      mInputData.add(outLJSPaySet);
    }
    catch(Exception ex) {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="ModifyBankInfoBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错! ";
      this.mErrors .addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 主函数，测试用
   */
  public static void main(String[] args) {
    ModifyBankInfoBL ModifyBankInfoBL1 = new ModifyBankInfoBL();
  }
}
