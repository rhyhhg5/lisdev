package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LDMedicalComDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYBankUniteLogSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.schema.LYSendToConfirmSchema;
import com.sinosoft.lis.schema.LYSendToSettleSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.lis.vschema.LYSendToConfirmSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行文件转换到数据模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class MedicalDealReturnS extends MedicalDealReturn{


  public MedicalDealReturnS() {
  }

  /**
   * 将数据存入数据库
   * @param tLYReturnFromBankSet
   * @return
   */
  protected boolean xmlToDatabase() {
    try {
      displayDocument(resultDoc);

      //get all rows
      NodeList rows = resultDoc.getDocumentElement().getChildNodes();

      for(int i=0; i<rows.getLength(); i++){
        //For each row, get the row element and name
        Element thisRow = (Element)rows.item(i);

        //Get the columns for thisRow
        NodeList columns = thisRow.getChildNodes();
        LYReturnFromBankSchema tReFromBank = new LYReturnFromBankSchema();
        for(int j=0; j<columns.getLength(); j++){
          Element thisColumn = (Element)columns.item(j);

          String colName = thisColumn.getNodeName();
          String colValue = thisColumn.getFirstChild().getNodeValue();
          System.out.println("colName:" + colName + ":" + colValue);
          
          tReFromBank.setV(colName, colValue);
          tReFromBank.setSerialNo(serialNo);
          tReFromBank.setDealType(DealType);
          tReFromBank.setDoType("1");
          tReFromBank.setModifyDate(PubFun.getCurrentDate());
          tReFromBank.setModifyTime(PubFun.getCurrentTime());
        }
        
        LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
        tLYSendToBankDB.setSerialNo(tReFromBank.getSerialNo());
        tLYSendToBankDB.setAccNo(tReFromBank.getAccNo());
        if("P".equals(DealType)){
        	tLYSendToBankDB.setPolNo(tReFromBank.getPayCode());
        }else{
        	tLYSendToBankDB.setPayCode(tReFromBank.getPayCode());
        }
        
        tLYSendToBankDB.setPayMoney(tReFromBank.getPayMoney());

        LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
        tLYSendToBankSet = tLYSendToBankDB.query();

        if (tLYSendToBankSet == null || tLYSendToBankSet.size() == 0)
        {
            throw new Exception("从发送盘表获取数据失败！");
        }
        else if (tLYSendToBankSet.size() == 1)
        {
            LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
            tLYSendToBankSchema = tLYSendToBankSet.get(1);

            if (tLYSendToBankSchema == null
                    || "".equals(tLYSendToBankSchema))
            {
                throw new Exception("从发送盘表获取数据失败！");
            }

            tReFromBank.setPayCode(tLYSendToBankSchema
                    .getPayCode());
            tReFromBank.setPolNo(tLYSendToBankSchema
                    .getPolNo());
            tReFromBank.setNoType(tLYSendToBankSchema
                    .getNoType());
            tReFromBank.setBankCode(tLYSendToBankSchema
                    .getBankCode());
            tReFromBank.setComCode(tLYSendToBankSchema
                    .getComCode());
            tReFromBank.setAgentCode(tLYSendToBankSchema
                    .getAgentCode());
            tReFromBank.setName(tLYSendToBankSchema
                    .getName());
            tReFromBank.setSendDate(tLYSendToBankSchema.getSendDate());
            tReFromBank.setPayMoney(tLYSendToBankSchema.getPayMoney());
            tReFromBank.setPayType(tLYSendToBankSchema.getPayType());

            outLYReturnFromBankSet.add(tReFromBank); 
        }else{
        	throw new Exception("一条回盘数据对应多条发盘数据！");
        }
        
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      // @@错误处理
      CError.buildErr(this, "Xml转入数据库处理失败: " + e.getMessage());
      return false;
    }
    return true;
  }


  
  /**
   * 获取xsl文件路径
   * @return
   */
  public String[] getXslPath() throws Exception {
	  
	    LDMedicalComDB tLDMedicalDB = new LDMedicalComDB();
	    String[] xslPath;
	    tLDMedicalDB.setMedicalComCode(medicalCode);
	    if (!tLDMedicalDB.getInfo()) throw new Exception("获取银行XSL描述信息失败！");
	    if(StrTool.cTrim(this.DealType).equals("G")){
	      xslPath = PubFun.split(tLDMedicalDB.getAgentGetReceiveF(), ",");
	    }else if(StrTool.cTrim(this.DealType).equals("P")){
	      xslPath = PubFun.split(tLDMedicalDB.getAgentPayReceiveF(), ",");
	    }else if(StrTool.cTrim(this.DealType).equals("C")){
	      xslPath = PubFun.split(tLDMedicalDB.getAgentGetSavePath(), ",");
	    }else{
	      xslPath = PubFun.split(tLDMedicalDB.getAgentPaySavePath(), ",");
	    }
	    return xslPath;
	  }
  
  public static void main(String[] args) {
	  MedicalDealReturnS mdr = new MedicalDealReturnS();
	  
//	  String path = "E:\\test\\Medical\\test[0].z" ;
	  
//	  mdr.dealData("test[0].z","G","88210001","");
	  
	  mdr.writeReturnFile("E:\\test\\Medical\\returnfile\\a.z", "9001-20130826102224-088^^9001-00999001-20130822552191|$sdfdsfs$123456789^0|||^");
  }
}


