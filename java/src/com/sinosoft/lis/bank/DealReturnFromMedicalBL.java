package com.sinosoft.lis.bank;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToSettleDB;
import com.sinosoft.lis.db.LYReturnFromBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDMedicalComSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYBankUniteLogSchema;
import com.sinosoft.lis.schema.LYSendToSettleSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.vschema.LJSPayPersonBSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.vschema.LYDupPaySet;
import com.sinosoft.lis.vschema.LYReturnFromBankBSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.VData;

public class DealReturnFromMedicalBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();

  //业务数据
  /** 处理总金额 */
  private String totalMoney = "";
  /** 处理总笔数 */
  private int sumNum = 0;
  /** 批次号 */
  private String serialNo = "";
  /** 银行编码 */
  private String bankCode = "";
   /** 输入银行编码 */
  private String getbankcode = "";
  /** 输入银行账户 */
  private String getbankaccno = "";
  private GlobalInput mGlobalInput = new GlobalInput();

//  private LYReturnFromBankSchema inLYReturnFromBankSchema = new LYReturnFromBankSchema();
  private TransferData inTransferData = new TransferData();
  private LYSendToSettleSet inLYSendToSettleSet = new LYSendToSettleSet();
  private LYReturnFromBankSet inLYReturnFromBankSet = new LYReturnFromBankSet();
  private LYBankLogSchema outLYBankLogSchema = new LYBankLogSchema();
  private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();
  

  public DealReturnFromMedicalBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代收
    if (mOperate.equals("GETMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");
    }
    PubSubmit tPubSubmit = new PubSubmit();

	// 如果有需要处理的错误，则返回
	if (!tPubSubmit.submitData(this.mInputData, "")) {
		// @@错误处理
		CError.buildErr(this, "--数据提交失败--");
		mErrors.addOneError("数据提交失败");
		return false;
	}
    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
//      inLYReturnFromBankSchema = (LYReturnFromBankSchema)mInputData.getObjectByObjectName("LYReturnFromBankSchema", 0);
      inTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);

      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }


  
  /**
   * 获取银行返回数据
   * @param inLYReturnFromBankSchema
   * @return
   */
  private LYSendToSettleSet getLYSendToSettle(String serialNo, String bankCode) {
	  
	  	System.out.println("\n获取发盘数据");
		  
		  LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
		  LYSendToSettleSet tLYSendToSettleSet = new LYSendToSettleSet();
		  
		  LYReturnFromBankDB tLYReturnFromBankDB = new LYReturnFromBankDB();
		  tLYReturnFromBankDB.setSerialNo(serialNo);
		  tLYReturnFromBankDB.setBankCode(bankCode);
		  tLYReturnFromBankSet = tLYReturnFromBankDB.query();
		 for(int i =1;i<=tLYReturnFromBankSet.size();i++){
		  LYReturnFromBankSchema  tLYReturnFromBankSchema = tLYReturnFromBankSet.get(i);
		  System.out.println(tLYReturnFromBankSchema.getAccName());
		  LYSendToSettleSchema tLYSendToSettleSchema = new LYSendToSettleSchema();
		  tLYSendToSettleSchema.setPayCode(tLYReturnFromBankSchema.getPayCode());
		  tLYSendToSettleSchema.setAccName(tLYReturnFromBankSchema.getAccName());
		    tLYSendToSettleSchema.setAccNo(tLYReturnFromBankSchema.getAccNo());
		    tLYSendToSettleSchema.setAgentCode(tLYReturnFromBankSchema.getAgentCode());
		    tLYSendToSettleSchema.setComCode(tLYReturnFromBankSchema.getComCode());
		    tLYSendToSettleSchema.setDealType(tLYReturnFromBankSchema.getDealType());
		    tLYSendToSettleSchema.setDoType(tLYReturnFromBankSchema.getDoType());
		    tLYSendToSettleSchema.setMedicalCode(tLYReturnFromBankSchema.getBankCode());
		    tLYSendToSettleSchema.setSerialNo(tLYReturnFromBankSchema.getSerialNo());
		    tLYSendToSettleSchema.setRiskCode(tLYReturnFromBankSchema.getRiskCode());
		    tLYSendToSettleSchema.setRemark(tLYReturnFromBankSchema.getBankSuccFlag());
		    tLYSendToSettleSchema.setSendDate(tLYReturnFromBankSchema.getSendDate());
		    tLYSendToSettleSchema.setPolNo(tLYReturnFromBankSchema.getPolNo());
			tLYSendToSettleSchema.setConfDate(PubFun.getCurrentDate());
			tLYSendToSettleSchema.setPayMoney(tLYReturnFromBankSchema.getPayMoney());
			tLYSendToSettleSchema.setConfMoney(totalMoney);
	        tLYSendToSettleSchema.setModifyDate(PubFun.getCurrentDate());
			tLYSendToSettleSchema.setModifyTime(PubFun.getCurrentTime());
			tLYSendToSettleSchema.setMakeDate(PubFun.getCurrentDate());
			tLYSendToSettleSchema.setMakeTime(PubFun.getCurrentTime());
			tLYSendToSettleSchema.setOperator(mGlobalInput.Operator);
		if(tLYReturnFromBankSchema.getBankSuccFlag().equals("Y")){
			tLYSendToSettleSchema.setSuccFlag("0");
			tLYSendToSettleSchema.setDealState("2");
			tLYSendToSettleSchema.setInsBankCode(getbankcode);
			tLYSendToSettleSchema.setInsAccNo(getbankaccno);
		}else{
			tLYSendToSettleSchema.setSuccFlag("N");
			tLYSendToSettleSchema.setDealState("0");
			}
			tLYSendToSettleSet.add(tLYSendToSettleSchema);
		 }

		return tLYSendToSettleSet;
  }
  
  private LYReturnFromBankSet getLYReturnFromBank(String serialNo,String bankCode){
	  
	  LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
	 
	  
	  LYReturnFromBankDB tLYReturnFromBankDB = new LYReturnFromBankDB();
	  tLYReturnFromBankDB.setSerialNo(serialNo);
	  tLYReturnFromBankDB.setBankCode(bankCode);
	  tLYReturnFromBankSet = tLYReturnFromBankDB.query();
	  return tLYReturnFromBankSet;
  }

private boolean confirmTotalMoney (LYReturnFromBankSet tLYReturnFromBankSet,String totalMoney){
	double sumMoney = 0;
	double fTotalMoney = Double.valueOf(totalMoney).doubleValue();
	
	for(int i =0;i<tLYReturnFromBankSet.size();i++){
		
		LYReturnFromBankSchema  tLYReturnFromBankSchema = tLYReturnFromBankSet.get(i+1);
		if(!tLYReturnFromBankSchema.getBankSuccFlag().equals("Y")){ 
			continue;
		}
	sumMoney = sumMoney +tLYReturnFromBankSchema.getPayMoney();
	sumNum = sumNum + 1;
	
	}
	return (fTotalMoney==sumMoney);
	
}

  /**
   * 生成银行日志数据
   * @param tLYSendToBankSchema
   * @return
   */
  private void getLYBankLog() throws Exception {
    //获取日志记录
    LYBankLogDB tLYBankLogDB = new LYBankLogDB();
    tLYBankLogDB.setSerialNo(serialNo);
    if (!tLYBankLogDB.getInfo()) {
      throw new Exception("获取银行日志表（LDBankLog）数据失败");
    }
    outLYBankLogSchema = tLYBankLogDB.getSchema();


    //修改日志
    outLYBankLogSchema.setAccTotalMoney(totalMoney); //财务确认总金额
   	outLYBankLogSchema.setBankSuccMoney(totalMoney); //银行成功总金额
   	outLYBankLogSchema.setBankSuccNum(sumNum + ""); //银行成功总数量
    outLYBankLogSchema.setTransOperator(mGlobalInput.Operator);
    outLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    outLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
  }
  
  private void getLJTempFee() throws Exception{
	  for (int i = 1; i <= inLYReturnFromBankSet.size(); i++) {
		  
		  LYReturnFromBankSchema mLYSendToSettleSchema= inLYReturnFromBankSet.get(i);
		  if("Y".equals(mLYSendToSettleSchema.getBankSuccFlag())){
			  LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
			  String paycode=mLYSendToSettleSchema.getPayCode();
			  LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
			  LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.executeQuery("select * from ljtempfeeclass where tempfeeno = '"+paycode+"'");
			  tLJTempFeeClassSchema.setSchema(tLJTempFeeClassSet.get(1));
			  
			  tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
			  tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
			  tLJTempFeeClassSchema.setInsBankAccNo(getbankaccno);
			  tLJTempFeeClassSchema.setInsBankCode(getbankcode);
			  
			  outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
		  }
		  
		  
	}
	  
	  
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
	  System.out.println("---dealData--处理---");
    try {
      //银行代收
      if (mOperate.equals("GETMONEY")) {
        //获取批次号、总金额、银行编码
        serialNo = (String) inTransferData.getValueByName("serialNo");
		getbankcode =(String)inTransferData.getValueByName("getbankcode");
        getbankaccno =(String)inTransferData.getValueByName("getbankaccno");
        System.out.print("getbankaccno2:"+getbankaccno);
        totalMoney = (String) inTransferData.getValueByName("totalMoney");
        bankCode = (String) inTransferData.getValueByName("bankCode");
        //取结算数据
       
        inLYReturnFromBankSet = getLYReturnFromBank(serialNo,bankCode);
        if (inLYReturnFromBankSet.size() == 0) throw new NullPointerException("无结算成功数据！");
        System.out.println("---End getLYReturnFromBankSet---");
        inLYSendToSettleSet = getLYSendToSettle(serialNo, bankCode);
       
        //校验财务总金额，比较回盘表成功总金额和界面录入的总金额
        if (!confirmTotalMoney(inLYReturnFromBankSet, totalMoney)){
        	
        	 CError.buildErr(this, "财务总金额确认失败！请与医保局对帐！");
        	return false;
        }
          
        System.out.println("---End confirmTotalMoney---");

        //记录日志，设置总金额、总数量、处理状态
        getLYBankLog();
        System.out.println("---End verifyUnitMoney---");
      
        getLJTempFee();
        System.out.println("---End getLJTempFee---");
      }
    }
    catch(Exception e) {
      e.printStackTrace();
      // @@错误处理
      CError.buildErr(this, "数据处理错误: " + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    mInputData = new VData();
    MMap tMap = new MMap();
    try {
    	tMap.put(outLYBankLogSchema, "UPDATE");
		tMap.put(inLYSendToSettleSet, "INSERT");
		tMap.put(outLJTempFeeClassSet, "UPDATE");
		
    	mInputData.add(tMap);
   
    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
}
