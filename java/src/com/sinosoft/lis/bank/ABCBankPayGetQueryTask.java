package com.sinosoft.lis.bank;

import java.io.File;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 业务数据转换到银行系统,银行代付
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author Minim
 * @version 1.0
 */

public class ABCBankPayGetQueryTask implements Runnable{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private String mUrl = null;

	private String mPath = null;

	public void run() {
		String flag = "SF";
		System.out.println("flag="+flag);
		System.out.println("---ABCBankPayGetQueryTask.java开始---");
		submitData();
		System.out.println("---ABCBankPayGetQueryTask.java正常结束---");
	}

	private boolean submitData() {
		if (!getUrlName())
		{
			return false;
		}

		if (!dealData())
		{
			return false;
		}
		return true;
		
	}

	private boolean getUrlName() {
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchReturnPath'";//查询报文的存放路径
        mUrl = new ExeSQL().getOneValue(sqlurl);
        System.out.println("查询报文的存放路径:" + mUrl);//调试用－－－－－
        if (mUrl == null || mUrl.equals(""))
        {
            buildError("getFileUrlName", "获取文件存放路径出错");
            return false;
        }
        //mUrl="E:/renwei/code/ReturnFromBankFile/";
        return true;
	}
	
	private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ABCBankPayGetQueryTask";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
	
    /**
     * dealData
     * 数据逻辑处理
     * @return boolean
     */
    private boolean dealData()
    {
        mPath = mUrl + "ABCBankReturn/" + PubFun.getCurrentDate2().substring(0,6)+ "/" + PubFun.getCurrentDate2().substring(6,8)+ "/";
        if(!newFolder(mPath)){
            mPath = mUrl;
        }
        System.out.println("文件存放初始路径" + mPath);
        ABCBankReturnXml tABCBankReturnXml = new ABCBankReturnXml();
        tABCBankReturnXml.submitData(mPath,"7708");
        return true;
    }
    /**
     * newFolder
     * 新建报文存放文件夹，以便对发盘报文查询
     * @return boolean
     */
    public static boolean newFolder(String folderPath)
    {
        String filePath = folderPath.toString();
        File myFilePath = new File(filePath);
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
                return true;
            }
            else
            {
                myFilePath.mkdirs();
                System.out.println("新建目录成功");
                return true;
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
            return false;
        }
    }
   /* public static void main(String args[]){
    	System.out.println(PubFun.getCurrentDate2().substring(6,8));
    	//PubFun.getCurrentDate2().substring(0,6)+ "/" + PubFun.getCurrentDate2().substring(6,2)+
    }*/

}
