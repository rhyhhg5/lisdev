package com.sinosoft.lis.bank;

import com.sinosoft.lis.schema.LYSendToBankSchema;

/**
 * 银行接口特殊信息提取类
 * 
 * @author 张成轩
 */
public class SendBankQuery {

	/**
	 * 获取客户社保编号
	 * 
	 * @param tLYSendToBankSchema
	 * @return
	 */
	public String getSecurityCode(LYSendToBankSchema tLYSendToBankSchema) {
		return "";
	}

	/**
	 * 获取收费类型
	 * 
	 * @param tLYSendToBankSchema
	 * @return
	 */
	public String getType(LYSendToBankSchema tLYSendToBankSchema) {
		String type = tLYSendToBankSchema.getNoType();
		if ("9".equals(type) || "16".equals(type)) {
			return "首期";
		} else if ("2".equals(type)) {
			return "续期";
		} else {
			return "其它";
		}
	}
}
