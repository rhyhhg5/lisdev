package com.sinosoft.lis.bank;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LDBankSet;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class NewBatchSendAddBL {

     /** 传入数据的容器 */
      private VData mInputData = new VData();
      /** 返回数据的容器 */
      /** 提交数据的容器 */
      private MMap map = new MMap();
      /** 数据操作字符串 */
      private String mOperate;
      /** 错误处理类 */
      public  CErrors mErrors = new CErrors();
      /** 前台传入的公共变量 */
      private GlobalInput mGlobalInput = new GlobalInput();
      
      private String upBankCode = "";
      private String upBankCodeName = "";
      private String UniteBankCode3 = "";
      private String UniteBankCodeName = "";
      private String UniteBankCode4 = "";
      private String UniteBankCodeName4 = "";
      
      private String upBankCodeSrc = "";
      private String upBankCodeNameSrc = "";
      private String UniteBankCode3Src = "";
      private String UniteBankCodeNameSrc = "";
      private String UniteBankCode4Src = "";
      private String UniteBankCodeName4Src = "";

      private LDBankUniteSet tLDBankUniteSet = new LDBankUniteSet();
      

      public NewBatchSendAddBL() {
      }
      
      /**
       * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
       * @param cInputData 传入的数据,VData对象
       * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
       * @return 布尔值（true--提交成功, false--提交失败）
       */
      public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mInputData = (VData)cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData()) return false;
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData()) return false;
        System.out.println("---End dealData---");

          //准备往后台的数据
          if (!prepareOutputData()) return false;
          System.out.println("---End prepareOutputData---");

          System.out.println("Start PubSubmit BLS Submit...");
          PubSubmit tPubSubmit = new PubSubmit();
          if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
          }
          System.out.println("End PubSubmit BLS Submit...");

        return true;
      }

      /**
       * 将外部传入的数据分解到本类的属性中
       * @param: 无
       * @return: boolean
       */
      private boolean getInputData()    {
        try {
          TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
          upBankCodeSrc = (String)tTransferData.getValueByName("upBankCodeSrc");
          upBankCodeNameSrc = (String)tTransferData.getValueByName("upBankCodeNameSrc");
          UniteBankCode3Src = (String)tTransferData.getValueByName("UniteBankCode3Src");
          UniteBankCodeNameSrc = (String)tTransferData.getValueByName("UniteBankCodeNameSrc");
          UniteBankCode4Src = (String)tTransferData.getValueByName("UniteBankCode4Src");
          UniteBankCodeName4Src = (String)tTransferData.getValueByName("UniteBankCodeName4Src");

          upBankCode = (String)tTransferData.getValueByName("upBankCode");
          upBankCodeName = (String)tTransferData.getValueByName("upBankCodeName");
          UniteBankCode3 = (String)tTransferData.getValueByName("UniteBankCode3");
          UniteBankCodeName = (String)tTransferData.getValueByName("UniteBankCodeName");
          UniteBankCode4 = (String)tTransferData.getValueByName("UniteBankCode4");
          UniteBankCodeName4 = (String)tTransferData.getValueByName("UniteBankCodeName4");
          mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
        }
        catch (Exception e) {
          // @@错误处理
          CError.buildErr(this, "接收数据失败");
          return false;
        }

        return true;
      }

      /**
       * 根据前面的输入数据，进行逻辑处理
       * @return 如果在处理过程中出错，则返回false,否则返回true
       */
      private boolean dealData() {
        try {
             System.out.println("mOperate:" + mOperate);
             
             if(mOperate.equals("UPDATE")){
                     if(!upUniteBank()){
                         
                         CError.buildErr(this, this.mErrors.getErrContent());
                         return false;
                 }
             } else {
                 CError.buildErr(this, "操作字符异常");
                 return false;
             }
        }
        catch(Exception e) {
          // @@错误处理
          CError.buildErr(this, "数据处理错误:" + e.getMessage());
          return false;
        }

        return true;
      }


      private boolean upUniteBank(){
    	  String sql="select * from lysendtobank where  bankcode='"+upBankCodeSrc+"'";
    	  ExeSQL exesql = new ExeSQL();
    	  SSRS r=exesql.execSQL(sql);
    	  if(r.getMaxRow()>0){
    		  this.mErrors.addOneError("银行已在途，请回盘之后进行修改");
              return false;
    	  }
    	  String sqlUpPerson ="update ldbankunite set BankUniteCode = '" + UniteBankCode3+"',BankUniteName='"+UniteBankCodeName+"',UniteGroupCode='"+UniteBankCode4+ "' where BankUniteCode = '" + UniteBankCode3Src+"'and BankCode='"+upBankCodeSrc+"'";
			MMap tmap = new MMap();
			tmap.put(sqlUpPerson, "UPDATE");
          VData mInputData = new VData();
			mInputData.clear();
			mInputData.add(tmap);
			PubSubmit p = new PubSubmit();
	        if (!p.submitData(mInputData, ""))
	        {
	        	CError.buildErr(this, "提交数据失败");
	          
	            return false;
	        }
	        return true;
    	  /*LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
    	  tLDBankUniteDB.setBankCode(upBankCodeSrc);
    	  tLDBankUniteDB.setBankUniteCode(UniteBankCode3Src);
    	  LDBankUniteSet tLDBankUniteset = new LDBankUniteSet();
    	  LDBankUniteSchema tLDBankUniteSchema = new LDBankUniteSchema();
    	  tLDBankUniteset = tLDBankUniteDB.query();
    	  if(tLDBankUniteset.size()>0){
    		  tLDBankUniteSchema = tLDBankUniteset.get(1);
    	  }
    	  tLDBankUniteSchema.setBankUniteCode(UniteBankCode3);
    	  tLDBankUniteSchema.setBankCode(upBankCode);
    	  tLDBankUniteSchema.setUniteGroupCode(UniteBankCode4);
    	  System.out.println(upBankCodeSrc +"SRC BAnkcode    " +UniteBankCode3 +"UP unite");
    	  System.out.println(UniteBankCode3Src + "SRC UNITE   " +  upBankCode +"IP Bank"  +UniteBankCode4);
    	  System.out.println();
    	  System.out.println();
    	  
          tLDBankUniteSet.add(tLDBankUniteSchema);
          return true;*/
      }
      
      /**
       * 准备往后层输出所需要的数据
       * @return 如果准备数据时发生错误则返回false,否则返回true
       */
      private boolean prepareOutputData() {
        try {
          map.put(tLDBankUniteSet, mOperate);
          mInputData.clear();
          mInputData.add(map);
        }
        catch(Exception ex) {
          // @@错误处理
          CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
          return false;
        }

        return true;
      }

      /**
       * 数据输出方法，供外界获取数据处理结果
       * @return 包含有数据查询结果字符串的VData对象
       */
      public static void main(String[] args) {
       
}
}