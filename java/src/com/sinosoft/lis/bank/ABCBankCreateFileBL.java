package com.sinosoft.lis.bank;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import java.sql.*;

import com.sinosoft.lis.pubfun.*;

import java.io.*;
import java.lang.reflect.*;
import java.text.DecimalFormat;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.apache.commons.net.ftp.FTPClient;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到文件模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class ABCBankCreateFileBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务数据
    private LYSendToBankSet inLYSendToBankSet = new LYSendToBankSet();

    private LYBankLogSet inLYBankLogSet = new LYBankLogSet();

    private TransferData inTransferData = new TransferData();

    private LDBankSchema outLDBankSchema = new LDBankSchema();

    private String fileName = "";

    private String filePath = "";

    private String bankCode = "";

    private String DealType = "";
    
    private String FTPName = "";
    
    private String FTPPass = "";
    
    private String FTPIP = "";
    
    private int FTPPort = 21;
    
    private String FTPPath = "";
    
    protected final Logger cLogger = Logger.getLogger(getClass());

    public ABCBankCreateFileBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"WRITE"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;

        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
            return false;
        System.out.println("---End getInputData---");

        //信息保存
        if (this.mOperate.equals("WRITE"))
        {
            //获取文件名和文件路径
            fileName = (String) inTransferData.getValueByName("fileName")
                    + ".xml";
            filePath = fileName.substring(0, fileName.lastIndexOf("/"));
            bankCode = (String) inTransferData.getValueByName("bankCode");
            DealType = (String) inTransferData.getValueByName("DealType");
            //生成送银行文件
            if (!writeBankFile(fileName))
                return false;

            //转换xml
            if (!xmlTransform())
                return false;

            //更新银行表独立序号
            outLDBankSchema.setSchema(getLDBank());
            System.out.println("---End getLDBank---");

            //保存银行操作日志
            tReturn = save();
        }

        if (tReturn)
            System.out.println("Save sucessful");
        else
            System.out.println("Save failed");

        return tReturn;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            inLYSendToBankSet = (LYSendToBankSet) mInputData
                    .getObjectByObjectName("LYSendToBankSet", 0);
            inLYBankLogSet = (LYBankLogSet) mInputData.getObjectByObjectName(
                    "LYBankLogSet", 0);
            inTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 生成送银行文件
     * @param fileName
     * @return
     */
    private boolean writeBankFile(String fileName)
    {
        OutputStreamWriter out = null;
        //序列号
        int j=0;
        try
        {
            //TODO:本地测试
            System.out.println("Start creating file ......");
            System.out.println("File Name : " + fileName);
            
//            out = new PrintWriter(new FileWriter(new File(fileName)), true);
            FileOutputStream fos = new FileOutputStream(new File(fileName)); 
            out = new OutputStreamWriter(new BufferedOutputStream(fos),"gbk"); 
            out.write("<?xml version=\"1.0\" encoding=\"GBK\"?>");
            	out
                	.write("<?xml-stylesheet type=\"text/xsl\" href=\"send_abcnonghang.xsl\"?>");
        	out.write("");

            // 生成文件头信息
            out.write("<!--");
            out.write(" * <p>FileName: " + fileName + " </p>");
            out.write(" * <p>Description: 业务系统数据转银行系统文件 </p>");
            out.write(" * <p>Copyright: Copyright (c) 2002</p>");
            out.write(" * <p>Company: sinosoft </p>");
            out.write(" * @Author：Minim's ABCBankCreateFileBL");
            out.write(" * @CreateDate：" + PubFun.getCurrentDate());
            out.write("-->");
            out.write("");

            out.write("<BANKDATA>");

            //增加银行独立序号
            LDBankDB tLDBankDB = new LDBankDB();
            tLDBankDB.setBankCode(bankCode);
            if (!tLDBankDB.getInfo())
            {
                CError.buildErr(this, "获取银行信息（LDBank）失败");
                return false;
            }
            for (int i = 0; i < inLYSendToBankSet.size(); i++)
            {
            	j=i + 1;
                LYSendToBankSchema tLYSendToBankSchema = inLYSendToBankSet
                        .get(i + 1);

                Class c = tLYSendToBankSchema.getClass();
                Field f[] = c.getDeclaredFields();
                
                LJSPayDB tLJSPayDB=new LJSPayDB();
                tLJSPayDB.setGetNoticeNo(tLYSendToBankSchema.getPayCode());
                
                out.write("  <ROW>");
                out.write("    <XuLie>" +j+"</XuLie>");
                out.write("    <SeqNo>" + tLDBankDB.getSeqNo() + "</SeqNo>");
                out.write("    <BankName>" + tLDBankDB.getBankName() + "</BankName>");
                out.write("    <DealType>" + this.DealType + "</DealType>");
                if (!StrTool.cTrim(tLYSendToBankSchema.getPayCode()).equals("") && tLJSPayDB.getInfo()){
                    LAComDB tLAComDB=new LAComDB();
                    tLAComDB.setAgentCom(tLJSPayDB.getAgentCom());
                    if(!StrTool.cTrim(tLJSPayDB.getAgentCom()).equals("") && tLAComDB.getInfo()){
                       out.write("    <BankCom>" + tLAComDB.getBusiLicenseCode() + "</BankCom>");
                    }
                }
                //增加银联代码
                if (tLDBankDB.getBankUniteFlag() != null
                        && tLDBankDB.getBankUniteFlag().equals("1"))
                {
                    LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
                    tLDBankUniteDB.setBankUniteCode(bankCode);
                    tLDBankUniteDB.setBankCode(tLYSendToBankSchema
                            .getBankCode());
                    if (!tLDBankUniteDB.getInfo())
                    {
                        CError.buildErr(this, "获取银联信息（LDBankUnite）失败");
                        return false;
                    }
                    out.write("    <UniteBankCode>"
                            + tLDBankUniteDB.getUniteBankCode()
                            + "</UniteBankCode>");
                    out.write("    <UniteBankName>"
                            + tLDBankUniteDB.getUniteBankName()
                            + "</UniteBankName>");
                }

                for (int elementsNum = 0; elementsNum < f.length; elementsNum++)
                {
                    out.write("    <" + f[elementsNum].getName() + ">");

                    if (f[elementsNum].getName().equals("PayMoney"))
                    {
                        //            double d = Double.parseDouble(tLYSendToBankSchema.getV(f[elementsNum].getName()));
                        //            d = Double.parseDouble((new DecimalFormat("0000000000.00")).format(d));
                        out.write((new DecimalFormat("0.00")).format(Double
                                .parseDouble(tLYSendToBankSchema
                                        .getV(f[elementsNum].getName()))));
                    }
                    else
                    {
                        out.write(tLYSendToBankSchema.getV(f[elementsNum]
                                .getName()));
                    }

                    out.write("</" + f[elementsNum].getName() + ">");
                }

                out.write("  </ROW>");
                out.write("");

            }
            out.write("</BANKDATA>");

            out.write("");
            out.write("<!-- Create BankFile Success! -->");
            out.close();
            fos.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Create File Error!");
            return false;
        }

        return true;
    }

    /**
     * 获取xsl文件路径
     * @return
     */
    public String[] getXslPath()
    {
        LDBankDB tLDBankDB = new LDBankDB();
        String[] xslPath;
        tLDBankDB.setBankCode(bankCode);
        tLDBankDB.getInfo();
        if (StrTool.cTrim(this.DealType).equals("F"))
        {
            xslPath = PubFun.split(tLDBankDB.getAgentGetSendF(), ",");
        }
        else
        {
            xslPath = PubFun.split(tLDBankDB.getAgentPaySendF(), ",");
        }
        return xslPath;
    }

    /**
     * Simple sample code to show how to run the XSL processor
     * from the API.
     */
    
    
    public boolean xmlTransform()
    {
        // Have the XSLTProcessorFactory obtain a interface to a
        // new XSLTProcessor object.
        try
        {
            System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
            "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
    System.setProperty("javax.xml.parsers.SAXParserFactory",
            "org.apache.xerces.jaxp.SAXParserFactoryImpl");
            //获取转换文件数组
            String[] xslPath = getXslPath();
            //加载源数据（XML文件）
            File fSource = new File(fileName);
            
            //循环每个转换文件，生成目标文件
            String allFileName = "";
            TransformerFactory transFactory = TransformerFactory.newInstance();
            for (int i = 0; i < xslPath.length; i++)
            {
                // TODO:本地测试
            	//xslPath[i] = "E:/renwei/code/lisdev/ui/bank/SendToBankFile/xsl/send_abcnonghang.xsl";
                System.out.println("xslPath:" + xslPath[i]);
                File fStyle = new File(xslPath[i]);
                Source style = new StreamSource(fStyle);
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fSource),"gbk"));
                Source source = new StreamSource(reader);
                
                String f = fileName.substring(0, fileName.lastIndexOf(".")) + ".DS";
                System.out.println("DS文件名："+f);

                //纪录银联的多个文件名
                if (allFileName.equals(""))
                {
                    allFileName = allFileName
                            + f.substring(f.lastIndexOf("/") + 1);
                }
                else
                {
                    allFileName = allFileName + ","
                            + f.substring(f.lastIndexOf("/") + 1);
                }
                
                //Create the Transformer
                Transformer transformer = transFactory.newTransformer(style);
                //Transform the Document
                System.out.println("DS文件名："+f);
                FileOutputStream fos = new FileOutputStream(f); 
                System.out.println("filename:"+fileName);
                BufferedOutputStream bos=new BufferedOutputStream(fos);
                OutputStreamWriter out = new OutputStreamWriter(bos,"gbk"); 
                Result result = new StreamResult(out);
                System.out.println("DS文件名："+f);
                transformer.transform(source, result);
                fos.close();
                bos.close();
                out.close();
                reader.close();
            }
            inLYBankLogSet.get(1).setOutFile(fileName);
            //ABCBankCreateFileBL up = new ABCBankCreateFileBL();
            testUpload(filePath+"/", allFileName);
            System.out.println("Transform Success!");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("errInof:"+e.getMessage());
            // @@错误处理
            CError.buildErr(this, "Xml处理失败");
            return false;
        }

        return true;
    }
    
   
    /**
     * 保存操作
     * @param mInputData
     * @return
     */
    private boolean save()
    {
        boolean tReturn = true;

        //建立数据库连接
        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError.buildErr(this, "数据库连接失败");
            return false;
        }

        try
        {
            //开始事务，锁表
            conn.setAutoCommit(false);

            //记录银行操作日志
            LYBankLogDBSet tLYBankLogDBSet = new LYBankLogDBSet(conn);
            tLYBankLogDBSet.set(inLYBankLogSet);
            if (!tLYBankLogDBSet.update())
            {
                try
                {
                    conn.rollback();
                }
                catch (Exception e)
                {
                }
                conn.close();
                System.out.println("LYBankLog Insert Failed");
                return false;
            }

            //记录银行操作日志
            LDBankDB tLDBankDB = new LDBankDB(conn);
            tLDBankDB.setSchema(outLDBankSchema);
            if (!tLDBankDB.update())
            {
                try
                {
                    conn.rollback();
                }
                catch (Exception e)
                {
                }
                conn.close();
                System.out.println("LDBank Update Failed");
                return false;
            }

            conn.commit();
            conn.close();
            System.out.println("End Committed");
        }
        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace();
            CError.buildErr(this, "数据库提交失败");
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {
            }
            tReturn = false;
        }

        return tReturn;
    }

    /**
     * 更新银行表独立序号
     * @return
     */
    private LDBankSchema getLDBank()
    {
        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(bankCode);
        if (!tLDBankDB.getInfo())
        {
            CError.buildErr(this, "获取银行信息（LDBank）失败");
            return null;
        }

        if (tLDBankDB.getSeqNo() == null || tLDBankDB.getSeqNo().equals(""))
        {
            tLDBankDB.setSeqNo("0");
        }
        else
        {
            tLDBankDB.setSeqNo(String.valueOf(Integer.parseInt(tLDBankDB
                    .getSeqNo()) + 1));
        }

        return tLDBankDB.getSchema();
    }
    
    //DS文件上传ftp
        public  void testUpload(String filePath,String fileName) {  
            FTPClient ftpClient = new FTPClient();  
            FileInputStream fis = null; 
      	  System.out.println("filePath:"+filePath);
      	System.out.println("fileName:"+fileName);
      	  String username  = (String) inTransferData.getValueByName("FTPName");
      	  String password = (String) inTransferData.getValueByName("FTPPass");
      	  String hostname = (String) inTransferData.getValueByName("FTPIP");
      	  int port = Integer.parseInt((String) inTransferData.getValueByName("FTPPort"));
      	  String pathname = (String) inTransferData.getValueByName("FTPPath");
            
            try {  
            	System.out.println("hostname:port="+hostname+port);
                ftpClient.connect(hostname,port);  
                System.out.println("hostname:port="+hostname+port);
                ftpClient.login(username, password);  
       
               File srcFile = new File(filePath+fileName); 
               System.out.println("srcFile:"+srcFile);
                fis = new FileInputStream(srcFile); 
               //设置上传目录  
               ftpClient.changeWorkingDirectory(pathname); 
               System.out.println("pathname:"+pathname);
               //ftpClient.setBufferSize(1024);  
               //ftpClient("GBK");  
                //设置文件类型（二进制）  
                ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);  
                ftpClient.storeFile(fileName, fis);  
            } catch (IOException e) {  
                e.printStackTrace();  
                throw new RuntimeException("FTP客户端出错！", e);  
            } finally {  
               try {  
                    ftpClient.disconnect();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                   throw new RuntimeException("关闭FTP连接发生异常！", e);  
                }  
            }  
        }  
       

    
   /* protected BufferedReader getFtpFile(String string, Source style) throws Exception {
		cLogger.info("Into Balance.getFtpFile()...");
		
		String mIP = "10.252.126.137";
		if ((null == mIP) || mIP.equals("")) {
			mIP = "10.252.126.137";
		}
		
		String mPort = "21";
		if ((null == mPort) || mPort.equals("")) {
			mPort = "21";
		}
		
		String mFilePath = "D:\\农业银行\\data";
		if ((null == mFilePath) || "".equals(mFilePath)) {
			mFilePath = "./";
		}
		mFilePath = mFilePath.replace('\\', '/');
		if (!mFilePath.endsWith("/")) {
			mFilePath = mFilePath + "/";
		}
		
		String mUserName = "sff";
		if ((null == mUserName) || "".equals(mUserName)) {
			mUserName = " ";
		}
		
		String mPassword = "ADMIN!1234";
		if ((null == mPassword) || "".equals(mPassword)) {
			mPassword = " ";
		}

		String mPathName = mFilePath + style;
		
		BufferedReader mBufReader = null;
		
		//ftp到银行
		FTPClient mFTPClient = new FTPClient();
		
		mFTPClient.setDefaultPort(Integer.parseInt(mPort));
		try {
			//建立ftp连接
			mFTPClient.connect(mIP);
			int tReplyCode = mFTPClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(tReplyCode)) {
				throw new BaseException("ftp连接失败！" + mIP + ": " + tReplyCode);
			}
			cLogger.info("ftp连接成功！" + mIP);
			
			//登录
			if (!mFTPClient.login(mUserName, mPassword)) {
				throw new BaseException("ftp登录失败！" + mUserName + ":" + mPassword);
			}
			cLogger.info("ftp登录成功！");
			mFTPClient.enterLocalPassiveMode();
			cLogger.error("ftp被动模式！");
			InputStream mRemoteIs = 
				mFTPClient.retrieveFileStream(mPathName);
			if (null == mRemoteIs) {
				throw new BaseException("未找到对账文件！" + (mPathName));
			}
			cLogger.info("ftp下载数据成功！");
			
			byte[] tBytes = IOTrans.InputStreamToBytes(mRemoteIs);
			Reader tBalanceReader = new InputStreamReader(
						new ByteArrayInputStream(tBytes));
			mBufReader = new BufferedReader(tBalanceReader);
			
			//退出登陆
			mFTPClient.logout();
			cLogger.info("ftp退出成功！");
		} finally {
			if (mFTPClient.isConnected()) {
				try {
					mFTPClient.disconnect();
					cLogger.info("ftp连接断开！");
				} catch(IOException ex) {
					cLogger.warn("服务端连接已断开！", ex);
				}
			}
		}
		
		cLogger.info("Out Balance.getFtpFile()!");
		return mBufReader;
	}*/

    public static void main(String[] args)
    {
        //WriteToFileBLS writeToFileBLS1 = new WriteToFileBLS();
    }
}

