package com.sinosoft.lis.bank;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * function ://总公司批量代收代付成功失败清单
 * @version 1.0
 */

import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BatchSendListByDateBL {
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	// 接收的数据
	private String mFlag = "";
	private String mSuccFlag = "";
	private String mComCode = "";// 管理机构 comCode
	private String mOutXmlPath = "";
	private String mStartDate = "";
	private String mEndDate = "";
	private String mBankCode = "";
	private TransferData mTransferData = new TransferData();
	private int mCount = 0;
	private double sumMoney = 0.0;

	// 定义需要用到的数据

	public BatchSendListByDateBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		if (!cOperate.equals("PRINT")) {
			buildError("submitData", "不支持的操作字符串");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		mResult.clear();
		if (!getPrintData()) {
			return false;
		}
		return true;
	}

	private boolean getInputData(VData cInputData) {
		mTransferData = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mStartDate = (String) mTransferData.getValueByName("StartDate");
		mEndDate = (String) mTransferData.getValueByName("EndDate");
		mBankCode = (String) mTransferData.getValueByName("BankCode");
		mFlag = (String) mTransferData.getValueByName("Flag");
		mComCode = (String) mTransferData.getValueByName("ManageCom");
		mSuccFlag = (String) mTransferData.getValueByName("SuccFlag");
		mOutXmlPath = (String) mTransferData.getValueByName("OutXmlPath");
		return true;
	}

	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "PrintBillRightBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	// 查询出首期或者是续期的银行代收正确清单
	private boolean getPrintData() {
		// 打印成功的清单
		String query_sql = "";
		if ("S".equals(mSuccFlag)) {
			if("7707".equals(mBankCode)){
				
				query_sql = "select a.agentcode,a.bankcode,a.accno,a.accname,a.paycode,a.dealtype,a.paymoney,a.comcode,a.polno"
						+ " from lyreturnfrombank a where exists (select 1 from lybanklog where logtype='" + mFlag + "' and serialno=a.serialno and bankcode='"
						+ mBankCode
						+ "' and startdate between '"
						+ mStartDate
						+ "' and '"
						+ mEndDate
						+ "')"
						+ " and a.banksuccflag = '00'"
						+ " and a.comcode like '"
						+ mComCode + "%'";
				
			} 
			if("7701".equals(mBankCode)){
				
				query_sql = "select a.agentcode,a.bankcode,a.accno,a.accname,a.paycode,a.dealtype,a.paymoney,a.comcode,a.polno"
						+ " from lyreturnfrombank a where exists (select 1 from lybanklog where logtype='" + mFlag + "' and serialno=a.serialno and bankcode='"
						+ mBankCode
						+ "' and startdate between '"
						+ mStartDate
						+ "' and '"
						+ mEndDate
						+ "')"
						+ " and a.banksuccflag = 'S0000'"
						+ " and a.comcode like '"
						+ mComCode + "%'";
				
			}
			else {
				
				query_sql = "select a.agentcode,a.bankcode,a.accno,a.accname,a.paycode,a.dealtype,a.paymoney,a.comcode,a.polno"
						+ " from lyreturnfrombank a where exists (select 1 from lybanklog where logtype='" + mFlag + "' and serialno=a.serialno and bankcode='"
						+ mBankCode
						+ "' and startdate between '"
						+ mStartDate
						+ "' and '"
						+ mEndDate
						+ "')"
						+ " and a.banksuccflag = '0000'"
						+ " and a.comcode like '"
						+ mComCode + "%'";
				
			}
		}
		// 失败清单
		if ("F".equals(mSuccFlag)) {
			if("7707".equals(mBankCode)){
				query_sql = "select a.agentcode,a.bankcode,a.accno,a.accname,a.paycode,a.dealtype,a.paymoney,a.comcode,a.polno,a.banksuccflag"
						+ " from lyreturnfrombank a where exists (select 1 from lybanklog where logtype='" + mFlag + "' and serialno=a.serialno and bankcode='"
						+ mBankCode
						+ "' and startdate between '"
						+ mStartDate
						+ "' and '"
						+ mEndDate
						+ "')"
						+ " and (a.banksuccflag != '00' or a.banksuccflag is null)"
						+ " and a.comcode like '" + mComCode + "%'";
			} 
			if("7701".equals(mBankCode)){
				query_sql = "select a.agentcode,a.bankcode,a.accno,a.accname,a.paycode,a.dealtype,a.paymoney,a.comcode,a.polno,a.banksuccflag"
						+ " from lyreturnfrombank a where exists (select 1 from lybanklog where logtype='" + mFlag + "' and serialno=a.serialno and bankcode='"
						+ mBankCode
						+ "' and startdate between '"
						+ mStartDate
						+ "' and '"
						+ mEndDate
						+ "')"
						+ " and (a.banksuccflag != 'S0000' or a.banksuccflag is null)"
						+ " and a.comcode like '" + mComCode + "%'";
			}
			else {
				query_sql = "select a.agentcode,a.bankcode,a.accno,a.accname,a.paycode,a.dealtype,a.paymoney,a.comcode,a.polno,a.banksuccflag"
						+ " from lyreturnfrombank a where exists (select 1 from lybanklog where logtype='" + mFlag + "' and serialno=a.serialno and bankcode='"
						+ mBankCode
						+ "' and startdate between '"
						+ mStartDate
						+ "' and '"
						+ mEndDate
						+ "')"
						+ " and (a.banksuccflag != '0000' or a.banksuccflag is null)"
						+ " and a.comcode like '" + mComCode + "%'";
			}
			
		}

		System.out.println("query_sql------------------------" + query_sql);
		ExeSQL main_exesql = new ExeSQL();
		SSRS main_ssrs = main_exesql.execSQL(query_sql);
		System.out.println("main_ssrs---------------------"
				+ main_ssrs.getMaxRow());

		if (main_ssrs.getMaxRow() > 0) {
			ExeSQL exeBank = new ExeSQL();
			String bank_exesql = "select bankname from ldbank where bankcode='"
					+ mBankCode + "'";
			SSRS bankSSRS = exeBank.execSQL(bank_exesql);

			String[][] cols = new String[main_ssrs.getMaxRow() + 10][15];

			cols[2][0] = "查询起期：" + mStartDate;
			cols[2][3] = "查询止期：" + mEndDate;

			cols[3][0] = "银行代码：" + mBankCode;
			cols[3][3] = "银行名称：" + bankSSRS.GetText(1, 1);
			cols[3][6] = "下载日期：" + PubFun.getCurrentDate();

			cols[5][0] = "业务员编码";
			cols[5][1] = "业务员名称";
			cols[5][2] = "业务员组别";
			cols[5][3] = "银行编码";
			cols[5][4] = "银行账号";
			cols[5][5] = "银行户名";
			cols[5][6] = "缴费通知书号/暂收收据号";
			cols[5][7] = "类别";
			cols[5][8] = "金额";
			cols[5][9] = "管理机构";
			cols[5][10] = "业务号码";

			if ("F".equals(mSuccFlag)) {
				cols[5][11] = "失败原因";
			}

			for (int i = 1; i <= main_ssrs.getMaxRow(); i++) {
				System.out.println("------------for循环执行");
				System.out.println("----------i--------" + i);

				cols[i + 5][0] = StrTool.cTrim(main_ssrs.GetText(i, 1)); // 业务员编码
																			// agentCode
				cols[i + 5][3] = StrTool.cTrim(main_ssrs.GetText(i, 2)); // 银行编码
																			// bankcode
				cols[i + 5][4] = StrTool.cTrim(main_ssrs.GetText(i, 3)); // 银行账号
																			// accno
				cols[i + 5][5] = StrTool.cTrim(main_ssrs.GetText(i, 4));// 银行户名
																		// accname
				cols[i + 5][6] = StrTool.cTrim(main_ssrs.GetText(i, 5)); // 缴费通知书、暂收收据号
																			// payCode
				cols[i + 5][8] = StrTool.cTrim(main_ssrs.GetText(i, 7)); // 金额
																			// payMoney
				cols[i + 5][9] = StrTool.cTrim(main_ssrs.GetText(i, 8)); // 管理机构
																			// comCode
				cols[i + 5][10] = StrTool.cTrim(main_ssrs.GetText(i, 9)); // 业务号码

				try {
					double money = Double.parseDouble(cols[i + 5][8]);
					sumMoney = Arith.add(sumMoney, money);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				
				if ("F".equals(mSuccFlag)) {
					cols[i + 5][11] = StrTool.cTrim(main_ssrs.GetText(i, 10));
					System.out.println("cols[i+4][10]----------------------"
							+ cols[i + 5][11]);
				}
				// 查询失败原因
				if ("F".equals(mSuccFlag)) {
					ExeSQL exeError = new ExeSQL();
					if (!(StrTool.cTrim(cols[i + 5][10]).equals("") && cols[i + 5][0] != null)) {
						String errorReason_sql = "select codename from ldcode1 where code='"
								+ mBankCode
								+ "' and code1='"
								+ cols[i + 5][11]
								+ "' and codetype='bankerror'";
						SSRS errorReason_ssrs = exeError
								.execSQL(errorReason_sql);
						if (errorReason_ssrs.getMaxRow() == 0) {
							cols[i + 5][11] = "无";
						} else {
							cols[i + 5][11] = errorReason_ssrs.GetText(1, 1);
							System.out
									.println("-------------------失败原因-------------------"
											+ cols[i + 5][11]);
						}
					}
				}

				// 查询业务员名称和业务员组别
				ExeSQL exesql = new ExeSQL();
				if (!(StrTool.cTrim(cols[i + 5][0]).equals("") || cols[i + 5][0] == null)) {
					System.out.println("查询出代理人的详细信息！！！");
					// 根据代理人的编码查询出代理人的姓名
					String AgentName_sql = "Select Name,AgentGroup from LAAgent Where AgentCode = '"
							+ cols[i + 5][0] + "'";
					SSRS AgentName_ssrs = exesql.execSQL(AgentName_sql);
					// 查询出代理人组别的名称
					String AgentGroup_sql = "Select Name from LABranchGroup Where AgentGroup = '"
							+ AgentName_ssrs.GetText(1, 2) + "'";
					SSRS AgentGroup_ssrs = exesql.execSQL(AgentGroup_sql);
					if (AgentName_ssrs.getMaxRow() == 0) {
						cols[i + 5][1] = " ";
					} else {
						cols[i + 5][1] = AgentName_ssrs.GetText(1, 1);
						System.out.println("代理人的姓名是"
								+ AgentName_ssrs.GetText(1, 1));
					}
					if (AgentGroup_ssrs.getMaxRow() == 0) {
						cols[i + 5][2] = "无";
					} else {
						cols[i + 5][2] = AgentGroup_ssrs.GetText(1, 1);
						System.out.println("代理人组别是"
								+ AgentGroup_ssrs.GetText(1, 1));

					}
					
					//将AgentCode转换为GroupAgentCode
			        //统一工号修改
			        String SQL = "select getUniteCode("+cols[i + 5][0]+") from dual where 1=1 with ur";
			        ExeSQL aExeSQL = new ExeSQL();
			        SSRS aSSRS = new SSRS();
			        aSSRS = aExeSQL.execSQL(SQL);
				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
				  		cols[i + 5][0] = aSSRS.GetText(1, 1);
				  	}

				}
				// 查询类别
				ExeSQL exesqltype = new ExeSQL();
				SSRS typeSSRS = new SSRS();
				// 代付"F".equals(mFlag)
				String Type_sql = "";
				if ("F".equals(mFlag)) {
					Type_sql = "Select case OtherNoType when '3' then '保全' when '4' then '新契约' when '5' then '理赔'  when '10' then '保全' else '其他' end from LJAGet where ActuGetNo = '"
							+ cols[i + 5][6] + "'";
				}
				// 代收"S".equals(mFlag)
				if ("S".equals(mFlag)||"R".equals(mFlag)) {
					// 代收失败
					if ("F".equals(mSuccFlag)) {
						Type_sql = "select case othernoType when '1'  then '新契约' when '2' then '续期' "
								+ "when '4' then '保全' when '7' then '保全' when '8' then '新契约' "
								+ "when '6' then '契约' when '10' then '契约' when '16' then '契约' "
								+ "when 'a' then '契约' "
								+ "else '其他' end from ljspay where getnoticeno='"
								+ cols[i + 5][6] + "' ";
					}
					// 代收成功
					if ("S".equals(mSuccFlag)) {
						Type_sql = "select case tempfeetype when '1'  then '新契约' when '2' then '续期' "
								+ "when '4' then '保全' when '7' then '保全' when '8' then '新契约' "
								+ "when '6' then '契约' when '10' then '契约' when '16' then '契约' "
								+ "when 'a' then '契约' "
								+ "else '其他' end from ljtempfee where tempfeeno='"
								+ cols[i + 5][6] + "' ";
					}

				}

				typeSSRS = exesqltype.execSQL(Type_sql);
				if (typeSSRS.getMaxRow() == 0) {
					cols[i + 5][7] = "暂无";
				} else {
					cols[i + 5][7] = typeSSRS.GetText(1, 1);
				}

				mCount = mCount + 1;
				System.out.println("----------------------------一次循环");
			}
			String tSF = "";
			if (mBankCode.equals("7703")) {
				if ("S".equals(mFlag)) {
					if ("S".equals(mSuccFlag)) {
						tSF = "总公司邮储代收正确清单";
					}
					if ("F".equals(mSuccFlag)) {
						tSF = "总公司邮储代收失败清单";
					}
				}
				if ("F".equals(mFlag)) {
					if ("S".equals(mSuccFlag)) {
						tSF = "总公司邮储代付正确清单";
					}
					if ("F".equals(mSuccFlag)) {
						tSF = "总公司邮储代付失败清单";
					}
				}
			} else {
				if ("S".equals(mFlag)) {
					if ("S".equals(mSuccFlag)) {
						tSF = "总公司批量代收正确清单";
					}
					if ("F".equals(mSuccFlag)) {
						tSF = "总公司批量代收失败清单";
					}
				}
				if ("F".equals(mFlag)) {
					if ("S".equals(mSuccFlag)) {
						tSF = "总公司批量代付正确清单";
					}
					if ("F".equals(mSuccFlag)) {
						tSF = "总公司批量代付失败清单";
					}
				}
				if ("R".equals(mFlag)) {
					if ("S".equals(mSuccFlag)) {
						tSF = "总公司实时代收正确清单";
					}
					if ("F".equals(mSuccFlag)) {
						tSF = "总公司实时代收失败清单";
					}
				}
			}

			cols[0][0] = tSF;
			DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");//格式化设置  
	        cols[mCount + 7][0] = "总金额：" + decimalFormat.format(sumMoney);
			System.out.println("总金额：" + decimalFormat.format(sumMoney));
			cols[mCount + 8][0] = "总笔数：" + mCount;
			
			try {
				System.out.println(mOutXmlPath);
				WriteToExcel t = new WriteToExcel("");
				t.createExcelFile();
				String[] sheetName = { PubFun.getCurrentDate() };
				t.addSheet(sheetName);
				t.setData(0, cols);
				t.write(mOutXmlPath);
				System.out.println("生成文件完成");
			} catch (Exception ex) {
				ex.toString();
				ex.printStackTrace();
			}
		} else {
			CError tError = new CError();
            tError.errorMessage = "未发现相关数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
		}
		return true;
	}
}
