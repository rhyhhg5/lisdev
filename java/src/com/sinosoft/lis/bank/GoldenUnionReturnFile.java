package com.sinosoft.lis.bank;

import java.io.File;
import java.io.FileWriter;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class GoldenUnionReturnFile {
	

    private String mUrl = "";

    private MMap map = new MMap();

    private VData mInputData = new VData();

    public boolean submitData(String mUrl)
    {

    	this.mUrl = mUrl;
        if (!dealDate())
        {
            System.out.println("获取数据失败");
            return false;
        }

        if (!prepareOutputData())
        {
            System.out.println("准备更新日志数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE"))
        {
            //错误处理
            System.out.println("更新日志失败");
            return false;
        }

        return true;
    }

    /**
     * dealData
     * 数据逻辑处理
     * @return boolean
     */
    public boolean dealDate()
    {
    	//查询待回盘的银行数据
        String sql = "select * from lybanklog a where a.dealstate is null and a.senddate is not null and a.outfile is not null and returndate is null and a.bankcode = '7707' with ur";
        System.out.println(sql);
        LYBankLogDB tLyBankLogDB = new LYBankLogDB();
        LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
        if (tLYBankLogSet == null)
        {
            System.out.println("未查到待回盘银行数据");
            return false;
        }
        for (int i = 1; i <= tLYBankLogSet.size(); i++)
        {
            LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
            String serialno = tLYBankLogSchema.getSerialNo();
            sql = "select * from lysendtobank where serialno='" + serialno
                    + "' with ur";
            System.out.println(sql);
            LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
            LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB
                    .executeQuery(sql);
            createStr(tLYBankLogSchema, tLYSendToBankSet);
        }
        return true;
    }

    public void createStr(LYBankLogSchema tLYBankLogSchema,
            LYSendToBankSet tLYSendToBankSet)
    {
    	//获取发盘时返回的批次号和文件名
    	String SerialNo = tLYBankLogSchema.getSerialNo();
    	String LogType = tLYBankLogSchema.getLogType();
        String str = null;
        String batchId = "";
        String fileName = SerialNo;
        //获取银行返回的信息
        str = "select batchId from lybanklog  where serialno='" + SerialNo + "' with ur";
        batchId = new ExeSQL().getOneValue(str);
        // 获取发送服务器的地址，其实真正的地址写在了前置机的hession类里了
		String sqlurl1 = "select codename from ldcode1 where codetype='GoldenReturnBank' and code='GoldenUnionServerUrl' and code1='7707'";
		String url = new ExeSQL().getOneValue(sqlurl1);
        //发送到第三方银行并返回提回信息
    	String payReq = send(batchId,"7707",fileName,LogType);
    	System.out.println("前置机返回来的回盘信息为：" + payReq);
    	
    	if (!payReq.equals("") && !payReq.equals(null) && !payReq.startsWith("False"))
        {
            System.out.println("--返回请求响应--");
            saveReq(payReq, tLYBankLogSchema, "7707");
        }
        else if (payReq.equals("") && payReq.equals(null)){
        	System.out.println("银行返回回盘的信息为空，请核实原因");
            errBankLog(tLYBankLogSchema,"银行返回回盘的信息为空，请核实原因");
            
        }  else {
        	payReq = payReq.substring(5, payReq.length());
        	errBankLog(tLYBankLogSchema,payReq);
        }
    }
    
    /**
	 * 将批次号发送至前置机
	 * 
	 * @param batchId 发盘返回的批次号
	 * @param bank 第三方银行编码
	 * @param fileName 发盘的文件名
	 * @return 返回提回文件
	 */
    public String send(String batchId, String bank,String fileName,String LogType) {
		// 请求报文
		HttpClient httpClient = new HttpClient();
		System.out.println("给前置机传的收付费标志："+LogType);
		// pcUrl前置机地址
		String sqlurl = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code ='PCIP'";
		String pcUrl = new ExeSQL().getOneValue(sqlurl);
		//pcUrl = "http://localhost:8080/jldsf/GoldenUnionSendServlet";
		PostMethod postMethod = new PostMethod(pcUrl);
		// 设置编码
		httpClient.getParams().setParameter(
				HttpMethodParams.HTTP_CONTENT_CHARSET, "GBK");

		postMethod.addParameter("bank", bank);
		postMethod.addParameter("batchId", batchId);
		postMethod.addParameter("fileName", fileName);
		//flag等于2代表回盘
		postMethod.addParameter("flag", "2");
		postMethod.addParameter("LogType", LogType);

		String strResp = "";
		try {
			long start = System.currentTimeMillis();
			// 执行getMethod先发送到前置机、再由前置机发送到第三方银行
			int statusCode = httpClient.executeMethod(postMethod);
			// 失败
			if (statusCode != HttpStatus.SC_OK) {
				return "False连接前置机失败";
			} else {
				// 读取内容
				byte[] responseBody = postMethod.getResponseBody();
				strResp = new String(responseBody, "GBK");
				System.out.println("服务器返回:" + strResp);
			}
			System.out.println("耗时:" + (System.currentTimeMillis() - start));
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		} finally {
			// 释放连接
			postMethod.releaseConnection();
		}
		return strResp;
	}


    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    /**
	 * 保存提回文件并进行数据读取处理
	 */
    private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema,
            String bankCode)
    {
        String tUrl = this.mUrl +"/"+ tLYBankLogSchema.getSerialNo() + "rq.txt";
        try
        {
            FileWriter fw = new FileWriter(tUrl);
            fw.write(payReq);
            fw.close();
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, "保存提回文件异常");
            ex.printStackTrace();
            return false;
        }
        System.out.println("成功了！！！！");
        GoldenUnionReturnFromBankBL tGoldenUnionReturnFromBankBL = new GoldenUnionReturnFromBankBL();
        try
        {
            if (!tGoldenUnionReturnFromBankBL.submitData(tUrl, bankCode,tLYBankLogSchema.getSerialNo()))
            {
                errBankLog(tLYBankLogSchema, "");
            }
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, "");
            ex.printStackTrace();
        }
        return true;
    }


    private void errBankLog(LYBankLogSchema tLYBankLogSchema, String err)
    {
        if (!err.equals(null) && !"".equals(err))
        {
            System.out.println(err);
            tLYBankLogSchema.setInFile(err);
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        } else{  
            System.out.println("回盘处理程序出现异常");
            tLYBankLogSchema.setInFile("回盘处理程序出现异常");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        this.map.put(tLYBankLogSchema, "UPDATE");
    }

    //TODO
    //紧急处理时使用，模拟回盘
    public boolean GoldenUnionReturn(String req, String serialNo)
    {
        String sql = "select * from lybanklog where serialno='" + serialNo
                + "' with ur";
        System.out.println(sql);
        LYBankLogDB spLYBankLogDB = new LYBankLogDB();
        LYBankLogSet spLYBankLogSet = spLYBankLogDB.executeQuery(sql);
        if (spLYBankLogSet.size() == 0)
        {
            System.out.println("批次号错误！");
            return false;
        }
        LYBankLogSchema spLYBankLogSchema = spLYBankLogSet.get(1);
        String bankCode = spLYBankLogSchema.getBankCode();

        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchReturnPath'";//发盘报文的存放路径
        this.mUrl = new ExeSQL().getOneValue(sqlurl) + "special";
        System.out.println("发盘报文的存放路径:" + mUrl);
        File myFilePath = new File(mUrl.toString());
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
        }
        spLYBankLogSchema.setSerialNo(serialNo);
        if (!saveReq(req, spLYBankLogSchema, bankCode))
        {
            return false;
        }
        return true;
    }

    public static void main(String[] arr)
    {
    	GoldenUnionReturnFile tGoldenUnionReturnFile = new GoldenUnionReturnFile();
    	tGoldenUnionReturnFile.GoldenUnionReturn("111111", "00000000000000094990");
    }


}
