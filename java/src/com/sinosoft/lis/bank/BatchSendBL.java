package com.sinosoft.lis.bank;

import com.sinosoft.lis.operfee.NewIndiDueFeeMultiUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.taskservice.TaskThread;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BatchSendBL extends TaskThread
{
    private String startDate = "";

    private String endDate = "";

    public boolean submitData()
    {
        if (!getInputData())
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("---End dealData---");
        return true;
    }

    private boolean getInputData()
    {
        try
        {
            startDate = "2012-1-1";
            endDate = PubFun.getCurrentDate();
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL
                .execSQL("select bankcode from ldbank where operator='sys' and comcode='86'");
        try
        {
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                System.out.println("---调用数据提取类---" + tSSRS.GetText(i, 1));
                getSendbankData(tSSRS.GetText(i, 1), "1");
                getSendbankData(tSSRS.GetText(i, 1), "2");
            }
        }
        catch (Exception ex)
        {
            System.out.println("---数据提取类报错---TT----");
            ex.printStackTrace();
        }

        SSRS tSSRS2 = new SSRS();

        try
        {
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                System.out.println("---调用数据审核类---" + tSSRS.GetText(i, 1));
                tSSRS2 = tExeSQL
                        .execSQL("select substr(comcode,1,4) from ldbankunite where bankunitecode='"
                                + tSSRS.GetText(i, 1)
                                + "' group by substr(comcode,1,4)");
                for (int j = 1; j <= tSSRS2.MaxRow; j++)
                {
                    getSendBankConf(tSSRS.GetText(i, 1), "1", tSSRS2.GetText(j,
                            1), "1");
                    getSendBankConf(tSSRS.GetText(i, 1), "2", tSSRS2.GetText(j,
                            1), "1");
                    getSendBankConf(tSSRS.GetText(i, 1), "1", tSSRS2.GetText(j,
                            1), "2");
                    getSendBankConf(tSSRS.GetText(i, 1), "2", tSSRS2.GetText(j,
                            1), "2");
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println("---数据审核类报错---TT----");
            ex.printStackTrace();
        }

        return true;
    }

    private boolean getSendbankData(String bankcode, String flag)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "sys";

        if ("1".equals(flag))
        {
            LCPolSchema tLCPolSchema = new LCPolSchema(); // 个人保单表
            System.out.println("StartDate:" + startDate);
            tLCPolSchema.setGetStartDate(startDate); //将判断条件设置在起领日期字段中
            tLCPolSchema.setPayEndDate(endDate); //将判断条件设置在终交日期字段中

            TransferData transferData2 = new TransferData();
            transferData2.setNameAndValue("bankCode", bankcode);
            VData tVData2 = new VData();
            tVData2.add(tLCPolSchema);
            tVData2.add(tGlobalInput);
            tVData2.add(transferData2);
            NewIndiDueFeeMultiUI tNewIndiDueFeeMultiUI = new NewIndiDueFeeMultiUI();
            tNewIndiDueFeeMultiUI.submitData(tVData2, "INSERT");

            if (tNewIndiDueFeeMultiUI.mErrors.needDealError())
            {
                System.out.print("催收处理失败，不能生成银行发送数据，原因是:");

                for (int n = 0; n < tNewIndiDueFeeMultiUI.mErrors
                        .getErrorCount(); n++)
                {
                    System.out
                            .print(tNewIndiDueFeeMultiUI.mErrors.getError(n).errorMessage
                                    + "|");
                }
            }
            else
            {
                System.out.println("催收处理成功！");
            }

            //生成银行数据
            System.out.println("\n---BatchSendBankSave Start---");
            BatchSendBankUI tBatchSendBankUI = new BatchSendBankUI();

            TransferData transferData1 = new TransferData();
            transferData1.setNameAndValue("startDate", startDate);
            transferData1.setNameAndValue("endDate", endDate);
            transferData1.setNameAndValue("bankCode", bankcode);

            VData tVData = new VData();
            tVData.add(transferData1);
            tVData.add(tGlobalInput);

            if (!tBatchSendBankUI.submitData(tVData, "GETMONEY"))
            {
                VData rVData = tBatchSendBankUI.getResult();
                System.out.println("提取数据失败：" + (String) rVData.get(0));
            }
            else
            {
                System.out.println("提取数据成功 ^-^ ");
            }
        }
        else if ("2".equals(flag))
        {
            //生成银行数据
            System.out.println("\n---BatchSendBankSave Start---");
            BatchSendBankFUI tBatchSendBankFUI = new BatchSendBankFUI();

            TransferData transferData1 = new TransferData();
            transferData1.setNameAndValue("startDate", startDate);
            transferData1.setNameAndValue("endDate", endDate);
            transferData1.setNameAndValue("bankCode", bankcode);

            VData tVData = new VData();
            tVData.add(transferData1);
            tVData.add(tGlobalInput);

            if (!tBatchSendBankFUI.submitData(tVData, "PAYMONEY"))
            {
                VData rVData = tBatchSendBankFUI.getResult();
                System.out.println("提取数据失败：" + (String) rVData.get(0));
            }
            else
            {
                System.out.println("提取数据成功 ^-^ ");
            }
        }
        return true;
    }

    private boolean getSendBankConf(String bankcode, String flag,
            String managecom, String banktype)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = managecom;
        tGlobalInput.ComCode = managecom;
        tGlobalInput.Operator = "sys";

        System.out.print("---SendBankConfSave Start---");

        String buttonflag = "Yes";

        //flag 为 1时 为代收
        if ("1".equals(flag))
        {
            VData tVData = new VData();
            tVData.add(new LJSPaySet());
            tVData.add(tGlobalInput);
            tVData.add(buttonflag);
            tVData.add(bankcode);
            tVData.add(banktype);

            SendBankConfUI tSendBankConfUI = new SendBankConfUI();
            if (!tSendBankConfUI.submitData(tVData, "WRITE"))
            {
                VData rData = tSendBankConfUI.getResult();
                System.out.println("处理失败，原因是:" + (String) rData.get(0));
                return false;
            }
            else
            {
                System.out.println("数据确认成功 ^-^ ");
            }
        }
        else if ("2".equals(flag))
        {
            VData tVData = new VData();
            tVData.add(new LJAGetSet());
            tVData.add(tGlobalInput);
            tVData.add(buttonflag);
            tVData.add(bankcode);
            tVData.add(banktype);

            SendBankConfFUI tSendBankConfFUI = new SendBankConfFUI();
            if (!tSendBankConfFUI.submitData(tVData, "WRITE"))
            {
                VData rData = tSendBankConfFUI.getResult();
                System.out.println("处理失败，原因是:" + (String) rData.get(0));
                return false;
            }
            else
            {
                System.out.println("数据确认成功 ^-^ ");
            }
        }
        return true;
    }

    public static void main(String[] args)
    {
        BatchSendBL tBatchSendBL = new BatchSendBL();
        tBatchSendBL.submitData();
    }

}
