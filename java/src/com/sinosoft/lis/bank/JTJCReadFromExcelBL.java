package com.sinosoft.lis.bank;


import java.io.IOException;



import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.f1j.util.F1Exception;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 银行接口Excel格式读取类 需要配置相应的配置xml，具体格式看程序吧。。
 * 
 * @author zyy
 * 
 */
public class JTJCReadFromExcelBL {

	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 错误处理类 */
	private CErrors mErrors = new CErrors();

	/** 前台传入的公共变量 */
	private GlobalInput mGlobalInput = new GlobalInput();
	 private TransferData mTransferData = null;
	/** 读取文件路径 */
	 private String mFilePath = "";

	/** 格式内容数组 */
	private String mOperate;
	private String serialno="";
	// 业务数据
	private LYBankLogSchema mLYBankLogSchema = new LYBankLogSchema();
	private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();
	private LYReturnFromBankSet mLYReturnFromBankSet = new LYReturnFromBankSet();
    private String fileName = "";
    private BookModelImpl book = new BookModelImpl();
	/**
	 * 入口函数，sino都知道
	 * 
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		 this.mOperate=cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("---End getInputData---");
      
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");
		System.out.println("tttttt==="+this.mLYReturnFromBankSet.size());
		if (!prepareOutputData()) {
			return false;
		}
		System.out.println("---End prepareOutputData---");

		PubSubmit tPubSubmit = new PubSubmit();

		// 如果有需要处理的错误，则返回
		if (!tPubSubmit.submitData(this.mInputData, "")) {
			// @@错误处理
			CError.buildErr(this, "--数据提交失败--");
			mErrors.addOneError("数据提交失败");
			return false;
		}

		System.out.println("---End ReadFromExcel---");

		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	 private boolean getInputData(VData cInputData) {
		try {
			 mTransferData = (TransferData) cInputData.
             getObjectByObjectName("TransferData", 0);
            mGlobalInput = (GlobalInput) cInputData.
            getObjectByObjectName("GlobalInput", 1);
            System.out.println("操作人员****:"+mGlobalInput.Operator);
            this.fileName=(String)mTransferData.getValueByIndex(0);
			//mPath =(String)mTransferData.getValueByIndex(2);
			serialno=(String)mTransferData.getValueByIndex(1);
			System.out.println("文件名:"+fileName);
			//System.out.println("路径:"+mPath);
			if (mLYBankLogSchema == null) {
				// @@错误处理
				CError.buildErr(this, "--接收数据失败--");
				mErrors.addOneError("接收数据失败");
				return false;
			}
		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			CError.buildErr(this, "接收数据失败");
			mErrors.addOneError("接收数据失败");
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		try {
			// 获取LYBankLog表信息
			mLYBankLogSchema.setSerialNo(serialno);
			LYBankLogDB mLYBankLogDB = mLYBankLogSchema.getDB();
			if (!mLYBankLogDB.getInfo()) {
				// @@错误处理
				CError.buildErr(this, "获取批次信息失败");
				mErrors.addOneError("获取批次信息失败");
				return false;
			}
			mLYBankLogSchema = mLYBankLogDB.getSchema();

			// 获取LYSendToBank表信息
			LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
			tLYSendToBankDB.setSerialNo(mLYBankLogDB.getSerialNo());
			tLYSendToBankDB.setDealType("F");
			mLYSendToBankSet = tLYSendToBankDB.query();

			if (mLYSendToBankSet == null || mLYSendToBankSet.size() == 0) {
				// @@错误处理
				CError.buildErr(this, "获取银行发盘明细数据失败");
				mErrors.addOneError("获取银行发盘明细数据失败");
				return false;
			}
		        
			 //获取银行文件数据
	        if (mOperate.equals("READ")) {
	          //获取返回文件名称
	         fileName = fileName.replace('\\', '/');
	          //读取银行返回文件，公共部分
	          if (!readBankFile(fileName)){
	          throw new Exception("读取银行编码文件失败");
	          }
	        }
	        if(!checkSchema()){
	        	 return false;
	         }
		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			CError.buildErr(this, "数据处理错误" + ex.getMessage());
			return false;
		}
		return true;
	}
	  /**
     * 读取银行编码文件
     * @param fileName
     * @return
     */
    private boolean readBankFile(String fileName) {
        try{
        	mFilePath=fileName;
            book.initWorkbook();//初始
            book.read(mFilePath,new ReadParams());
            int tSheetNums = book.getNumSheets();//读去sheet的个数，就是一个excel包含几个sheet
            System.out.println("tSheetNums==="+tSheetNums);
            LYBankBean tLYBankBean=new LYBankBean();
            for( int sheetNum=0;sheetNum<tSheetNums;sheetNum++ ){
                book.setSheet(sheetNum);
                int tLastCol = book.getLastCol();//总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
                int tLastRow = book.getLastRow();//总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
                System.out.println("tLastCol==="+tLastCol);
                System.out.println("tLastRow==="+tLastRow);
              //按列循环
                for(int  j=1;j<=tLastRow;j++ ){
                	tLYBankBean.setBankaccno(book.getText(j,2));
                	System.out.println("Bankaccno==="+tLYBankBean.getBankaccno());
                	tLYBankBean.setBankaccname(book.getText(j,3));
                	System.out.println("Bankaccname==="+tLYBankBean.getBankaccname());
                	tLYBankBean.setAccno(book.getText(j,4));
                	System.out.println("Accno==="+tLYBankBean.getAccno());
                    tLYBankBean.setAccname(book.getText(j,5));
                    System.out.println("Accname==="+tLYBankBean.getAccname());
                    tLYBankBean.setPaymoney(Double.parseDouble(book.getText(j,6)));
                   // tLYBankBean.setPaymoney(Double.parseDouble("".equals(book.getText(j,6).toString())?"0.00":book.getText(j,6).toString()));
                    System.out.println("Paymoney==="+tLYBankBean.getPaymoney());
                    tLYBankBean.setState(book.getText(j,8));
                    System.out.println("State==="+tLYBankBean.getState());
                    tLYBankBean.setRemark(book.getText(j,9));
                    System.out.println("Remark==="+tLYBankBean.getRemark());
                    if(!(checkBankData(tLYBankBean)))
                    {
                    	return false;
                    }
                    String BankSuccFlag="";
                    LYReturnFromBankSchema tLYReturnFromBankSchema=new LYReturnFromBankSchema();
                    tLYReturnFromBankSchema.setPayCode(tLYBankBean.getRemark());
                    System.out.println("paycode==="+tLYReturnFromBankSchema.getPayCode());
            		tLYReturnFromBankSchema.setPayMoney(tLYBankBean.getPaymoney());
            		tLYReturnFromBankSchema.setAccNo(tLYBankBean.getBankaccno());
            		tLYReturnFromBankSchema.setAccName(tLYBankBean.getBankaccname());
            		BankSuccFlag=tLYBankBean.getState();
            		if(BankSuccFlag.equals("处理成功")){
            			tLYReturnFromBankSchema.setBankSuccFlag("00");//付费成功
            			
            		}
            		else{
            			tLYReturnFromBankSchema.setBankSuccFlag("01");//付费失败
            			
            		}
            		mLYReturnFromBankSet.add(tLYReturnFromBankSchema);
                }
            }
            
        }catch(F1Exception e){
            e.printStackTrace();
            //mCErrors.addOneError("处理一线分摊录入数据错误，原因：" + e.getMessage());
            return false;
        }catch(IOException ioe){
            ioe.printStackTrace();
            //mCErrors.addOneError("IO错误，原因：" + ioe.getMessage());
            return false;
        }
        return true;
           
        }
	private boolean checkBankData(LYBankBean tLYBankBean) {
		String paycode =tLYBankBean.getRemark();
		String accNo =tLYBankBean.getBankaccno();
		double payMoney =tLYBankBean.getPaymoney();

		for (int row = 0; row < mLYSendToBankSet.size(); row++) {

			LYSendToBankSchema tLYSendToBankSchema = mLYSendToBankSet
					.get(row + 1);

			if (!paycode.equals(tLYSendToBankSchema.getPayCode())) {
				continue;
			}

			if (!accNo.equals(tLYSendToBankSchema.getAccNo())) {
				CError.buildErr(this, "付费号：" + paycode + ",客户帐号与发盘数据不匹配，请核查");
				mErrors.addOneError("付费号：" + paycode + ",客户帐号与发盘数据不匹配，请核查");
				return false;
			}
			if (payMoney != tLYSendToBankSchema.getPayMoney()) {
				CError.buildErr(this, "付费号：" + paycode + ",金额与发盘数据不匹配，请核查");
				mErrors.addOneError("付费号：" + paycode + ",金额与发盘数据不匹配，请核查");
				return false;
			}
			return true;
		}
		CError.buildErr(this, "未找到回盘文件中的付费号：" + paycode + "的相关发盘数据，请核查");
		mErrors.addOneError("未找到回盘文件中的付费号：" + paycode + "的相关发盘数据，请核查");
		return false;
	}

	/**
	 * 校验Set准确性
	 * 
	 * @return
	 */
	private boolean checkSchema() {
		if (mLYReturnFromBankSet.size() != mLYSendToBankSet.size()) {
			CError.buildErr(this, "发盘数据与回盘文件中数据量不一致");
			mErrors.addOneError("发盘数据与回盘文件中数据量不一致");
			return false;
		}
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return
	 */
	private boolean prepareOutputData() {
		try {
			System.out.println("SSSSSS==="+this.mLYReturnFromBankSet.size());
			mInputData = new VData();
			mInputData.add(this.mGlobalInput);
			MMap tMap = new MMap();
			System.out.println("operator==="+mGlobalInput.Operator);
			mLYBankLogSchema.setInFile(fileName);
			System.out.println("DDDDD==="+this.mLYBankLogSchema.getInFile());
			mLYBankLogSchema.setSendOperator(mGlobalInput.Operator);
			mLYBankLogSchema.setReturnDate(PubFun.getCurrentDate());
			System.out.println("CCCCCC==="+this.mLYBankLogSchema.getReturnDate());
			mLYBankLogSchema.setReturnOperator(mGlobalInput.Operator);
			System.out.println("BBBBBB==="+this.mLYBankLogSchema.getReturnOperator());
			mLYBankLogSchema.setTransDate(PubFun.getCurrentDate());
			mLYBankLogSchema.setTransOperator(mGlobalInput.Operator);
			System.out.println("AAAAAAA==="+this.mLYBankLogSchema.getTransOperator());
			mLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
			mLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
			System.out.println("MMMMM==="+this.mLYReturnFromBankSet.size());
			LYSendToBankSet lyset=new LYSendToBankSet();
			for (int row = 1; row <= mLYReturnFromBankSet.size(); row++) {
				System.out.println("YYYYYY==="+this.mLYReturnFromBankSet.size());
				LYReturnFromBankSchema ttLYReturnFromBankSchema = mLYReturnFromBankSet
						.get(row);
				String payCode = ttLYReturnFromBankSchema.getPayCode();
				System.out.println("IIIII==="+ttLYReturnFromBankSchema.getPayCode());
				//根据回盘表付费号查询发盘表是否存在数据，有则生成回盘表，无则返回。
				LYSendToBankDB ly = new LYSendToBankDB();
				ly.setPayCode(payCode);
				ly.setDealType("F");
				lyset = ly.query();
				System.out.println("PPPPPPP==="+lyset.size());
			
			if(lyset.size()>0){
					System.out.println("VVVVVVVVV==="+lyset.size());
					LYSendToBankSchema tLYSendToBankSetSchema = lyset
							.get(1);
					System.out.println("OOOOOOOOO==="+tLYSendToBankSetSchema.getPayCode());
					ttLYReturnFromBankSchema.setAccName(tLYSendToBankSetSchema.getAccName());
					ttLYReturnFromBankSchema.setAccNo(tLYSendToBankSetSchema.getAccNo());
					ttLYReturnFromBankSchema.setAgentCode(tLYSendToBankSetSchema.getAgentCode());
					ttLYReturnFromBankSchema.setBankCode(tLYSendToBankSetSchema.getBankCode());
					ttLYReturnFromBankSchema.setBankDealDate(PubFun.getCurrentDate());
					ttLYReturnFromBankSchema.setBankDealTime(PubFun.getCurrentTime());
					ttLYReturnFromBankSchema.setComCode(tLYSendToBankSetSchema.getComCode());
					ttLYReturnFromBankSchema.setConvertFlag(ttLYReturnFromBankSchema.getConvertFlag());
					ttLYReturnFromBankSchema.setDataType(tLYSendToBankSetSchema.getDataType());
					if(ttLYReturnFromBankSchema.getBankSuccFlag().equals("00")){
						ttLYReturnFromBankSchema.setPayType(tLYSendToBankSetSchema.getDealType());
					}
					else{
						ttLYReturnFromBankSchema.setPayType(tLYSendToBankSetSchema.getPayType());
					}
					ttLYReturnFromBankSchema.setDealType(tLYSendToBankSetSchema.getDealType());
					ttLYReturnFromBankSchema.setDoType(tLYSendToBankSetSchema.getDoType());
					ttLYReturnFromBankSchema.setModifyDate(PubFun.getCurrentDate());
					ttLYReturnFromBankSchema.setModifyTime(PubFun.getCurrentTime());
					ttLYReturnFromBankSchema.setName(tLYSendToBankSetSchema.getName());
					ttLYReturnFromBankSchema.setNoType(tLYSendToBankSetSchema.getNoType());
					ttLYReturnFromBankSchema.setPayCode(tLYSendToBankSetSchema.getPayCode());
					ttLYReturnFromBankSchema.setPolNo(tLYSendToBankSetSchema.getPolNo());
					ttLYReturnFromBankSchema.setPayMoney(tLYSendToBankSetSchema.getPayMoney());
					ttLYReturnFromBankSchema.setRemark(tLYSendToBankSetSchema.getRemark());
					ttLYReturnFromBankSchema.setRiskCode(tLYSendToBankSetSchema.getRiskCode());
					ttLYReturnFromBankSchema.setSendDate(tLYSendToBankSetSchema.getSendDate());
					ttLYReturnFromBankSchema.setSerialNo(tLYSendToBankSetSchema.getSerialNo());
					ttLYReturnFromBankSchema.setVertifyFlag(tLYSendToBankSetSchema.getVertifyFlag());
					
				}
			
			}
			tMap.put(mLYBankLogSchema, "UPDATE");
			tMap.put(mLYReturnFromBankSet, "INSERT");
			mInputData.add(tMap);

		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	/**
	 * 获取异常信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {

		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "cwad";

		LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
		tLYBankLogSchema.setSerialNo("10000000000000112921");

		VData v = new VData();
		v.add(tLYBankLogSchema);
		v.add("E:\\zzzzzzzzzzzzz\\回盘文件.xls");
		v.add(tGlobalInput);

		JTJCReadFromExcelBL tWriteToExcelBL = new JTJCReadFromExcelBL();
		if (tWriteToExcelBL.submitData(v, "")) {
		} else {
			System.out.println(tWriteToExcelBL.getErrors().getFirstError());
		}
	}
}
