package com.sinosoft.lis.bank;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.io.*;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;




import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行文件转换到数据模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class ABCBankReadBackFileBL {
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 提交数据的容器 */
  private MMap map = new MMap();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  //业务数据
  //private TransferData inTransferData = new TransferData();
  private GlobalInput inGlobalInput = new GlobalInput();
  private String fileName = "";

  private LYReturnFromBankSet outLYReturnFromBankSet = new LYReturnFromBankSet();
  private LYBankLogSchema outLYBankLogSchema = new LYBankLogSchema();
  private LYBankUniteLogSchema outLYBankUniteLogSchema = new LYBankUniteLogSchema();
  /** 返回批次号 */
  private String serialNo = "";
  private String DealType = "";
  /** 选择银行编码 */
  private String bankCode = "";
  /** 银联文件标记，HZ－汇总文件，MX－明细文件 */
  private String bankUniteFlag = "";
  /** 银联子银行代码，在银联明显日志中需要纪录，在汇总回盘文件中没有该信息 */
  private String bankInUniteCode = "";

  private Document dataDoc = null;
  private Document resultDoc = null;

  private Reflections mReflections = new Reflections();
  private String mPath="";
  private String filePath="";
  private  VData mInputData = null;
  private TransferData inTransferData = new TransferData();
 

  public ABCBankReadBackFileBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"READ"和""
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData ,LYBankLogSchema mLYBankLogSchema, String mPath,String filePath, String cOperate) {
    //将操作数据拷贝到本类中
    this.serialNo = mLYBankLogSchema.getSerialNo();
    this.mPath=mPath;
    this.filePath=filePath;
    this.mOperate = cOperate;
    this.outLYBankLogSchema = mLYBankLogSchema;
    this.mInputData = (VData) cInputData.clone();

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //核销
    ABCBankReturnSave mABCBankReturnSave =  new ABCBankReturnSave();
    try
    {
        if (!mABCBankReturnSave.submitData(fileName, outLYReturnFromBankSet))
        {
            errBankLog(outLYBankLogSchema, 2);
        }
    }
    catch (Exception ex)
    {
        errBankLog(outLYBankLogSchema, 2);
        ex.printStackTrace();
    }

    return true;
  }

  
  private void errBankLog(LYBankLogSchema tLYBankLogSchema, int flag)
  {
      if (flag == 1)
      {
          System.out.println("获取响应信息失败");
          tLYBankLogSchema.setInFile("获取响应信息失败");
          tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
          tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
      }
      else
      {
          System.out.println("回盘处理程序出现异常");
          tLYBankLogSchema.setInFile("回盘处理程序出现异常");
          tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
          tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
      }
      this.map.put(tLYBankLogSchema, "UPDATE");
  }

  
  /**
   * 更新银行日志数据
   * @param tLYSendToBankSchema
   * @return
   */
  private LYBankLogSchema getLYBankLog() {
    //获取日志记录
    LYBankLogDB tLYBankLogDB = new LYBankLogDB();
    tLYBankLogDB.setSerialNo(serialNo);
    if (!tLYBankLogDB.getInfo()) {
      CError.buildErr(this, "获取银行日志数据失败");
      return null;
    }
    LYBankLogSchema tLYBankLogSchema = tLYBankLogDB.getSchema();

    if (tLYBankLogSchema.getDealState() != null)throw new NullPointerException(
            "该批次号的银行返回文件已经处理过了，请核实……");

      tLYBankLogSchema.setInFile(fileName);

    tLYBankLogSchema.setReturnDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setReturnOperator(inGlobalInput.Operator);
    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());

    return tLYBankLogSchema;
  }

  /**
   * 创建一个xml文档对象DOM
   * @return
   */
  private Document buildDocument() {
    try {
      //Create the document builder
      DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docbuilder = dbfactory.newDocumentBuilder();

      //Create the new document(s)
      return docbuilder.newDocument();
    }
    catch (Exception e) {
      System.out.println("Problem creating document: " + e.getMessage());
      return null;
    }
  }

  /**
   * 获取xsl文件路径
   * @return
   */
  public String getXslPath() throws Exception {
    LDBankDB tLDBankDB = new LDBankDB();
    String xslPath;
    tLDBankDB.setBankCode(bankCode);
    if (!tLDBankDB.getInfo()) throw new Exception("获取银行XSL描述信息失败！");
    if(StrTool.cTrim(this.DealType).equals("F")){
      xslPath = tLDBankDB.getAgentGetReceiveF();
    }else{
      xslPath = tLDBankDB.getAgentPayReceiveF();
    }
    return xslPath;
  }


  /**
   * Simple sample code to show how to run the XSL processor
   * from the API.
   */
  public boolean xmlTransform() {
    try {
      String xslPath = getXslPath();

      //非银联或银联的汇总文件，使用第一个描述
      //String xslPath = "";
      /*if (bankUniteFlag.equals("") || bankUniteFlag.equals("HZ")) {
        xslPath = arrPath[0];
      }
      else {
        xslPath = arrPath[1];
      }*/
      //xslPath="E:\\renwei\\code\\lisdev\\ui\\bank\\ReturnFromBankFile\\xsl\\return_abcnonghang.xsl";
      System.out.println("xslPath:" + xslPath);

      File fStyle = new File(xslPath);
      Source source = new DOMSource(dataDoc);
      Result result = new DOMResult(resultDoc);
      Source style = new StreamSource(fStyle);

      //Create the Transformer
      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer(style);
      System.out.println("xslPath:"+xslPath);
      //Transform the Document
      transformer.transform(source, result);

      System.out.println("Transform Success!");
    }
    catch(Exception e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      // @@错误处理
      CError.buildErr(this, "Xml处理失败");
      return false;
    }

    return true;
  }

  /**
   * 从发送盘表中获取数据
   * @param tLYReturnFromBankSchema LYReturnFromBankSchema 当前处理的返回盘数据纪录
   * @param tLYReturnFromBankSet LYReturnFromBankSet 所有返回盘数据
   * @return boolean
   */
  private boolean prepareReturnData(LYReturnFromBankSchema tLYReturnFromBankSchema, LYReturnFromBankSet tLYReturnFromBankSet) {
    try {
      LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();

      boolean enough = false;
      String strSql = "select * from lysendtobank where serialno='" + tLYReturnFromBankSchema.getSerialNo() + "'";

      if (tLYReturnFromBankSchema.getAccNo() != null) {
          if(StrTool.cTrim(bankCode).equals("023101")){
              strSql = strSql + " and (AccNo='" + tLYReturnFromBankSchema.getAccNo() + "' or accno='"+tLYReturnFromBankSchema.getAccNo().substring(0, tLYReturnFromBankSchema.getAccNo().length()-1)+"') ";
              enough = true;
          }else{
            strSql = strSql + " and AccNo='" + tLYReturnFromBankSchema.getAccNo() + "'";
            enough = true;
          }
      }
      if (tLYReturnFromBankSchema.getPayCode() != null) {
          if(StrTool.cTrim(bankCode).equals("999100")){
              strSql = strSql + " and PayCode like '%" + tLYReturnFromBankSchema.getPayCode() + "%'";
              enough = true;
          } else {
        strSql = strSql + " and PayCode='" + tLYReturnFromBankSchema.getPayCode() + "'";
        enough = true;
          }
      }
      if(!StrTool.cTrim(bankCode).equals("0433") && !StrTool.cTrim(bankCode).equals("034201")&& !StrTool.cTrim(bankCode).equals("034404")&& !StrTool.cTrim(bankCode).equals("034406")&& !StrTool.cTrim(bankCode).equals("489102")){
          strSql = strSql + " and paymoney=" + tLYReturnFromBankSchema.getPayMoney();
      }
      strSql = strSql + " order by modifydate";

      if (!enough) {
        throw new Exception("获取返回数据不足，必须有账号或者收据号！");
      }

      System.out.println("strSql: " + strSql);
      LYSendToBankSet tLYSendToBankSet = tLYSendToBankSchema.getDB().executeQuery(strSql);
      
      if(StrTool.cTrim(bankCode).equals("0301") && tLYSendToBankSet.size() == 0){
          String tSql="select * from lysendtobank where serialno='" + tLYReturnFromBankSchema.getSerialNo() + "' and paymoney=" + tLYReturnFromBankSchema.getPayMoney()+" with ur";
          LYSendToBankSet ttLYSendToBankSet = tLYSendToBankSchema.getDB().executeQuery(tSql);
          if(ttLYSendToBankSet.size()!=0){
              for (int i=0; i<ttLYSendToBankSet.size(); i++) {
                  String tSendAccNo=StrTool.cTrim(ttLYSendToBankSet.get(i+1).getAccNo());
                  String tReturnAccno=StrTool.cTrim(tLYReturnFromBankSchema.getAccNo());
                  if(tSendAccNo.length()>tReturnAccno.length()){
                      int tLen=tReturnAccno.length();
                      for(int j=0;j<tSendAccNo.length()-tLen;j++){
                          tReturnAccno="0"+tReturnAccno;
                      }
                  }
                  if(tSendAccNo.equals(tReturnAccno)){
                      tLYSendToBankSet.add(ttLYSendToBankSet.get(i+1));
                  }
              }
          }
      }
      if (tLYSendToBankSet.size() == 0) {
        throw new Exception("发送盘表无数据，无法与返回数据匹配！");
      }
      else if (tLYSendToBankSet.size() == 1) {
        tLYSendToBankSchema = tLYSendToBankSet.get(1);
      }
      //确保同一账号，同一金额的多条数据能够被正确取出
      else {
        for (int i=0; i<tLYSendToBankSet.size(); i++) {
          boolean isExist = false;

          for (int j=0; j<tLYReturnFromBankSet.size(); j++) {
            //PayCode对应应收总表的getnoticeno,唯一
            if (tLYSendToBankSet.get(i+1).getPayCode().equals(tLYReturnFromBankSet.get(j+1).getPayCode())) {
              isExist = true;
              break;
            }
          }

          if (!isExist) {
            tLYSendToBankSchema = tLYSendToBankSet.get(i+1);
            break;
          }
        }
      }

      //获取数据
      String BankDealDate = PubFun.getCurrentDate2();//tLYReturnFromBankSchema.getBankDealDate();
      String BankSuccFlag = tLYReturnFromBankSchema.getBankSuccFlag();
      mReflections.transFields(tLYReturnFromBankSchema, tLYSendToBankSchema);

      tLYReturnFromBankSchema.setBankDealDate(BankDealDate);
      tLYReturnFromBankSchema.setBankSuccFlag(BankSuccFlag);
      //因为没有设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
      tLYReturnFromBankSchema.setRemark(inGlobalInput.Operator);
      tLYReturnFromBankSchema.setModifyDate(PubFun.getCurrentDate());
      tLYReturnFromBankSchema.setModifyTime(PubFun.getCurrentTime());
    }
    catch (Exception e) {
      e.printStackTrace();
      // @@错误处理
      CError.buildErr(this, "从发送盘表中获取数据失败" + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * 将数据存入数据库
   * @param tLYReturnFromBankSet
   * @return
   */
  private LYReturnFromBankSet xmlToDatabase() {
    try {
      String innerBankCode = "";
      LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
      displayDocument(resultDoc);

      //get all rows
      NodeList rows = resultDoc.getDocumentElement().getChildNodes();

      for(int i=0; i<rows.getLength(); i++){
        //For each row, get the row element and name
        Element thisRow = (Element)rows.item(i);

        //Get the columns for thisRow
        NodeList columns = thisRow.getChildNodes();
        LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();

        for(int j=0; j<columns.getLength()-1; j++){
          Element thisColumn = (Element)columns.item(j);

          String colName = thisColumn.getNodeName();
          String colValue = thisColumn.getFirstChild().getNodeValue();
          //System.out.println("colName:" + colName + ":" + colValue);

          //根据标签名往数据库的同名字段里插入数据
          //如果是银联汇总文件，不处理返回盘表，只纪录日志明细表
            tLYReturnFromBankSchema.setV(colName, colValue);
        }
        Element thisColumn = (Element)columns.item(columns.getLength()-2);
        String bankSuccFlag = thisColumn.getFirstChild().getNodeValue();
        
        if(bankSuccFlag != null && !bankSuccFlag.equals("0000")){
        	Element thisColumnInfo = (Element)columns.item(columns.getLength()-1);
            String bankSuccMan = thisColumnInfo.getFirstChild().getNodeValue();;
            bankSuccFlag = getNewSucc(bankSuccMan,"7708");
            tLYReturnFromBankSchema.setBankSuccFlag(bankSuccFlag);
        }
        tLYReturnFromBankSchema.setBankDealDate(PubFun.getCurrentDate());
        tLYReturnFromBankSchema.setSerialNo(serialNo);
        tLYReturnFromBankSchema.setModifyDate(PubFun.getCurrentDate());
        tLYReturnFromBankSchema.setModifyTime(PubFun.getCurrentTime());
        //如果金额小数点后面多于两位，则只保留两位小数//creair
        String sMoney = new java.text.DecimalFormat("#.00").format(tLYReturnFromBankSchema.getPayMoney());
        System.out.println("first:"+tLYReturnFromBankSchema.getPayMoney());
        tLYReturnFromBankSchema.setPayMoney(sMoney);
        System.out.println("second:"+tLYReturnFromBankSchema.getPayMoney());
        //TODO 貌似有问题
        //从发送盘表中获取数据
        if (!prepareReturnData(tLYReturnFromBankSchema, tLYReturnFromBankSet)) throw new Exception("从发送盘表获取数据失败！");
        tLYReturnFromBankSet.add(tLYReturnFromBankSchema);
      }

      if (tLYReturnFromBankSet.size() == 0)throw new NullPointerException("将数据存入数据库失败");

      //如果是银联明细文件，要和银联汇总文件数据的笔数和金额进行匹配

        int totalNum = tLYReturnFromBankSet.size();
        double totalMoney = 0;
        for (int i=0; i<tLYReturnFromBankSet.size(); i++) {
          totalMoney = totalMoney + tLYReturnFromBankSet.get(i+1).getPayMoney();
        }


          LYBankLogDB tLYBankLogDB = new LYBankLogDB();
          tLYBankLogDB.setSerialNo(serialNo);
          tLYBankLogDB.setBankCode(bankCode);
          if (!tLYBankLogDB.getInfo()) {
            throw new Exception("获取银行提盘信息失败");
          }

          int totalNumS = tLYReturnFromBankSet.size();
          double totalMoneyS = 0;
          for (int i=0; i<tLYReturnFromBankSet.size(); i++) {
        	  totalMoneyS = totalMoneyS + tLYReturnFromBankSet.get(i+1).getPayMoney();
          }

          if (totalNumS != tLYBankLogDB.getTotalNum()) {
            throw new Exception("回盘文件的账户总数与发盘文件的账户总数不符!");
          }

          if(Math.abs(totalMoney-tLYBankLogDB.getTotalMoney())>0.00001) {
            throw new Exception("回盘文件的总金额与发盘文件的总金额不符!");
        }

      return tLYReturnFromBankSet;
    }
    catch (Exception e) {
      e.printStackTrace();
      // @@错误处理
      if (!bankUniteFlag.equals("")) {
        CError.buildErr(this, "Xml转入数据库处理失败，请确认银联回盘格式 " + e.getMessage());
      }
      else {
        CError.buildErr(this, "Xml转入数据库处理失败: " + e.getMessage());
      }
      return null;
    }
  }
  /**
   * 读取银行返回文件
   * @param fileName
   * @return
   */
  private boolean readBankFile(String fileName) {
      //ABCBankCreateFileBL download = new ABCBankCreateFileBL();
//    download.getFtpFile("D:\\农业银行\\data", style);
    //Declare the document
    dataDoc = buildDocument();
    resultDoc = buildDocument();

    try {
      //读入文件到BUFFER中以提高处理效率
      BufferedReader in = new BufferedReader(
          new FileReader(fileName));

      //将所有文本以行为单位读入到VECTOR中
      String strLine = "";

      //创建根标签
      Element dataRoot = dataDoc.createElement("BANKDATA");

      //循环获取每一行
	     int j =0;
      while(true) {
        strLine = in.readLine();
        if (strLine == null) break;
        strLine = strLine.trim();
        //去掉空行
        if (strLine.length() < 3) continue;
        System.out.println(strLine);
        //System.out.println("strLen: " + strLine.length());

        //Create the element to hold the row
        Element rowEl = dataDoc.createElement("ROW");

        Element columnEl = dataDoc.createElement("COLUMN");
         if ("9995".equals(bankCode) && "MX".equals(bankUniteFlag) && j==0){

         } else {
        	 //Add the row element to the root
        columnEl.appendChild(dataDoc.createTextNode(strLine));
        rowEl.appendChild(columnEl);
        dataRoot.appendChild(rowEl);
      }
       j++;
      }

      //Add the root to the document
      dataDoc.appendChild(dataRoot);
//      NodeList tables = dataDoc.getDocumentElement().getChildNodes();
      //System.out.println("tables.getLength():" + tables.getLength());

      //显示XML信息，调试用
      displayDocument(dataDoc);

      in.close();
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      // @@错误处理
      CError.buildErr(this, "读取银行返回文件失败");
      return false;
    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      //获取银行文件数据
      if (mOperate.equals("DownLoad")) {
        //获取返回文件名称
    	  //从ftp上获取银行文件
    	//this.filePath=filePath.substring(filePath.lastIndexOf("/")+1);
    	if(!downFile(filePath, this.mPath)){
    		System.out.println("文件下载失败！");
    		return false;
    	}
    	bankCode = outLYBankLogSchema.getBankCode();
    	DealType= new ExeSQL().getOneValue("select dealtype from lysendtobank where serialno='"+serialNo+"' ");
    	
    	//读取银行返回文件，公共部分
        fileName=this.mPath +filePath;
        System.out.println("fileName:"+fileName);
     
        if (!readBankFile(fileName)) throw new Exception("读取银行返回文件失败");

        //转换xml，公共部分
        if (!xmlTransform()) throw new Exception("转换xml失败");
        
        this.fileName = fileName.substring(0, fileName.lastIndexOf(".")+3);//上生产是将该行代码注释掉。

        //将数据存入数据库，普通银行、银联汇总和银联明细各不相同
        if(DealType.equalsIgnoreCase("S") && bankCode.equals("7708")){
        	outLYReturnFromBankSet = xmlToDatabase();
            if (outLYReturnFromBankSet==null) return false;
            //生成银行日志数据
            outLYBankLogSchema.setSchema(getLYBankLog());
        }else if(DealType.equalsIgnoreCase("F") && bankCode.equals("7708")){
        	outLYReturnFromBankSet = xmlToDatabaseF();
            if (outLYReturnFromBankSet==null) return false;
            //生成银行日志数据
            outLYBankLogSchema.setSchema(getLYBankLog());
        }
        
      }
    }
    catch(Exception e) {
      // @@错误处理
        System.out.println(e.getMessage());
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return false;
    }

    return true;
  }



  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
	  ABCBankReadBackFileBL abc = new ABCBankReadBackFileBL();
	  abc.dealData();
  }

  public static int num = 0;
  public void displayDocument(Node d) {
    num += 2;

    if (d.hasChildNodes()) {
      NodeList nl = d.getChildNodes();

      for (int i=0; i<nl.getLength(); i++) {
        Node n = nl.item(i);

        for (int j=0; j<num; j++) {
          System.out.print(" ");
        }
        if (n.getNodeValue() == null) {
          System.out.println("<" + n.getNodeName() + ">");
        }
        else {
          System.out.println(n.getNodeValue());
        }

        displayDocument(n);

        num -= 2;
//        System.out.println("num:" + num);

        if (n.getNodeValue() == null) {
          for (int j=0; j<num; j++) {
            System.out.print(" ");
          }
          System.out.println("</" + n.getNodeName() + ">");
        }

      }

    }
  }
  
/*  *//**
   * 将数据存入数据库
   * @param tLYReturnFromBankSet
   * @return
   */
  private LYReturnFromBankSet xmlToDatabaseF() {
    try {
      String innerBankCode = "";
      LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
      displayDocument(resultDoc);

      //get all rows
      NodeList rows = resultDoc.getDocumentElement().getChildNodes();

      for(int i=0; i<rows.getLength(); i++){
        //For each row, get the row element and name
        Element thisRow = (Element)rows.item(i);

        //Get the columns for thisRow
        NodeList columns = thisRow.getChildNodes();
        LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();

        for(int j=0; j<columns.getLength(); j++){
          Element thisColumn = (Element)columns.item(j);

          String colName = thisColumn.getNodeName();
          String colValue = thisColumn.getFirstChild().getNodeValue();
          //System.out.println("colName:" + colName + ":" + colValue);

          //根据标签名往数据库的同名字段里插入数据
          //如果是银联汇总文件，不处理返回盘表，只纪录日志明细表
          if (bankUniteFlag.equals("HZ")) {
            outLYBankUniteLogSchema.setV(colName, colValue);
          }
          else {
            tLYReturnFromBankSchema.setV(colName, colValue);
          }
        }

        //如果是银联汇总文件，不处理返回盘表，只纪录日志明细表
        if (bankUniteFlag.equals("HZ")) {
//          //银行PICCH深圳银联的汇总文件没有行别信息，所以暂时限制不能同时导入两个汇总文件
//          String sql = "select count(1) from lybankunitelog where serialno='" + serialNo + "' and dealstate is null";
//          ExeSQL e = new ExeSQL();
//          SSRS s = e.execSQL(sql);
//          if (Integer.parseInt(s.GetText(1, 1)) > 0) {
//            throw new Exception("已经存在一个银行的汇总信息没有确认，请先处理完毕");
//          }
//          //--------------------------------------------------------------------

          outLYBankUniteLogSchema.setSerialNo(serialNo);
          outLYBankUniteLogSchema.setBankUniteCode(bankCode);

          //从银联表中获取内部银行代码
//          outLYBankUniteLogSchema.setBankCode(getInnerBankCode(bankCode, outLYBankUniteLogSchema.getBankCode()));
          outLYBankUniteLogSchema.setBankCode(bankInUniteCode);

          outLYBankUniteLogSchema.setComCode(inGlobalInput.ComCode);
          outLYBankUniteLogSchema.setLogType("S");
          outLYBankUniteLogSchema.setMakeDate(PubFun.getCurrentDate());
          outLYBankUniteLogSchema.setInFile(fileName);
          outLYBankUniteLogSchema.setReturnDate(PubFun.getCurrentDate());
          outLYBankUniteLogSchema.setReturnOperator(inGlobalInput.Operator);
          outLYBankUniteLogSchema.setModifyDate(PubFun.getCurrentDate());
          outLYBankUniteLogSchema.setModifyTime(PubFun.getCurrentTime());
          return tLYReturnFromBankSet;
        }
        //银联明细文件处理
        else if (bankUniteFlag.equals("MX")) {
//          LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
//          tLDBankUniteDB.setBankUniteCode(bankCode);
//          tLDBankUniteDB.setUniteBankCode(tLYReturnFromBankSchema.getBankCode());
//          innerBankCode = tLDBankUniteDB.query().get(1).getBankCode();
          innerBankCode = bankInUniteCode;

          LYBankUniteLogDB tLYBankUniteLogDB = new LYBankUniteLogDB();
          tLYBankUniteLogDB.setSerialNo(serialNo);
          tLYBankUniteLogDB.setBankCode(innerBankCode);
          if (!tLYBankUniteLogDB.getInfo()) {
            throw new Exception("获取银联日志信息（LYBankUniteLog）失败");
          }

//          //银行PICCH深圳银联的汇总文件没有行别信息，所以暂时限制不能同时导入两个汇总文件
//          String sql = "select count(1) from lybankunitelog where serialno='" + serialNo + "' and dealstate is null";
//          ExeSQL e = new ExeSQL();
//          SSRS s = e.execSQL(sql);
//          if (Integer.parseInt(s.GetText(1, 1)) > 0) {
//            throw new Exception("已经存在一个银行（" + s.GetText(1, 2) + "）的汇总信息没有确认，请先处理完毕");
//          }
//          sql = "select TransDate from lybankunitelog where serialno='" + serialNo + "' and dealstate is null";
//          s = e.execSQL(sql);
//          tLYReturnFromBankSchema.setBankDealDate(s.GetText(1, 1));
//          //--------------------------------------------------------------------


          //因为银联明细文件中没有处理日期，因此需要从汇总文件中获取
          tLYReturnFromBankSchema.setBankDealDate(tLYBankUniteLogDB.getTransDate());
        }

        tLYReturnFromBankSchema.setSerialNo(serialNo);
        tLYReturnFromBankSchema.setModifyDate(PubFun.getCurrentDate());
        tLYReturnFromBankSchema.setModifyTime(PubFun.getCurrentTime());
        //如果金额小数点后面多于两位，则只保留两位小数//creair
        String sMoney = new java.text.DecimalFormat("#.00").format(tLYReturnFromBankSchema.getPayMoney());
        System.out.println("first:"+tLYReturnFromBankSchema.getPayMoney());
        tLYReturnFromBankSchema.setPayMoney(sMoney);
        System.out.println("second:"+tLYReturnFromBankSchema.getPayMoney());
        
        //从发送盘表中获取数据
        if (!prepareReturnData(tLYReturnFromBankSchema, tLYReturnFromBankSet)) throw new Exception("从发送盘表获取数据失败！");
        tLYReturnFromBankSet.add(tLYReturnFromBankSchema);
      }

      if (tLYReturnFromBankSet.size() == 0)throw new NullPointerException("将数据存入数据库失败");

      //如果是银联明细文件，要和银联汇总文件数据的笔数和金额进行匹配
      if (bankUniteFlag.equals("MX")) {
        LYBankUniteLogDB tLYBankUniteLogDB = new LYBankUniteLogDB();
        tLYBankUniteLogDB.setSerialNo(serialNo);
        tLYBankUniteLogDB.setBankCode(innerBankCode);
        if (!tLYBankUniteLogDB.getInfo()) {
          throw new Exception("获取银联信息失败");
        }

        int totalNum = tLYReturnFromBankSet.size();
        double totalMoney = 0;
        for (int i=0; i<tLYReturnFromBankSet.size(); i++) {
          totalMoney = totalMoney + tLYReturnFromBankSet.get(i+1).getPayMoney();
        }

        if (totalNum != tLYBankUniteLogDB.getTotalNum()) {
          throw new Exception("银联明细文件的划款成功账户数与汇总文件不符");
        }

        if(Math.abs(totalMoney-tLYBankUniteLogDB.getTotalMoney())>0.00001)  {
            throw new Exception("银联明细文件的划款成功金额与汇总文件不符");
          }
      }else{
          LYBankLogDB tLYBankLogDB = new LYBankLogDB();
          tLYBankLogDB.setSerialNo(serialNo);
          tLYBankLogDB.setBankCode(bankCode);
          if (!tLYBankLogDB.getInfo()) {
            throw new Exception("获取银行提盘信息失败");
          }

          int totalNum = tLYReturnFromBankSet.size();
          double totalMoney = 0;
          for (int i=0; i<tLYReturnFromBankSet.size(); i++) {
            totalMoney = totalMoney + tLYReturnFromBankSet.get(i+1).getPayMoney();
          }

          if (totalNum != tLYBankLogDB.getTotalNum()) {
            throw new Exception("回盘文件的账户总数与发盘文件的账户总数不符!");
          }

          if(Math.abs(totalMoney-tLYBankLogDB.getTotalMoney())>0.00001) {
            throw new Exception("回盘文件的总金额与发盘文件的总金额不符!");
        }
      }

      return tLYReturnFromBankSet;
    }
    catch (Exception e) {
      e.printStackTrace();
      // @@错误处理
      if (!bankUniteFlag.equals("")) {
        CError.buildErr(this, "Xml转入数据库处理失败，请确认银联回盘格式 " + e.getMessage());
      }
      else {
        CError.buildErr(this, "Xml转入数据库处理失败: " + e.getMessage());
      }
      return null;
    }
  }
  
  //从Windows ftp服务器下载文件 fileName要下载的文件名 localPath云盘路径
  public boolean downFile(String filename, String localpath){
	  inTransferData = (TransferData) mInputData.getObjectByObjectName(
              "TransferData", 0);
	  
	  String username  = (String) inTransferData.getValueByName("FTPName");
	  String password = (String) inTransferData.getValueByName("FTPPass");
	  String hostname = (String) inTransferData.getValueByName("FTPIP");
	  int port = Integer.parseInt((String) inTransferData.getValueByName("FTPPort"));
	  String pathname = (String) inTransferData.getValueByName("FTPPath");
	  System.out.println("username:"+username);
	  System.out.println("password:"+password);
	  System.out.println("hostname:"+hostname);
	  System.out.println("port:"+port);
	  System.out.println("pathname:"+pathname);
	  
	    /*String hostname="10.252.126.137";//FTP服务器hostname  
		int port=21;//FTP服务器端口 
		String username="sff";
		String password="ADMIN!1234";
		String pathname="D:\\ABCBankErp\\data\\";//FTP服务器上的相对路径
*/	    boolean flag = false;
	    FTPClient ftpClient = new FTPClient();
	    try {
	      //连接FTP服务器
	      ftpClient.connect(hostname, port);
	      //登录FTP服务器
	      ftpClient.login(username, password);
	      //验证FTP服务器是否登录成功
	      int replyCode = ftpClient.getReplyCode();
	      if(!FTPReply.isPositiveCompletion(replyCode)){
	        return flag;
	      }
	      //切换FTP目录
	      ftpClient.changeWorkingDirectory(pathname);
          File localFile = new File(localpath + filename);
          OutputStream os = new FileOutputStream(localFile);
          ftpClient.retrieveFile(filename, os);
          os.close();
	      ftpClient.logout();
	      flag = true;
	    } catch (Exception e) {
	      e.printStackTrace();
	    } finally{
	      if(ftpClient.isConnected()){
	        try {
	          ftpClient.logout();
	        } catch (IOException e) {
	          
	        }
	      }
	    }
	    return flag;
	}

  private String getNewSucc(String succMan,String bankCode)
  {
      try {
          if (succMan == null || succMan.trim().equals("")){
              return "3999";
          } else {
          	// 为解决[账户名]:的问题，每次都会插入新数据 
  			if(succMan.lastIndexOf(",") != -1){
  				succMan = succMan.substring(0,succMan.indexOf(",[") );
  			}
              String querySql = "select code1 from ldcode1 where codetype='bankerror' and code='" 
                      + bankCode + "' and codename='" + succMan.trim() + "'";
              ExeSQL tExeSQL = new ExeSQL();
              SSRS tSSRS = tExeSQL.execSQL(querySql);
              if(tSSRS == null || tSSRS.MaxRow == 0){
                  querySql = "select max(int(code1))+1 from ldcode1 where codetype='bankerror' and code='" 
                      + bankCode + "'";
                  tSSRS = tExeSQL.execSQL(querySql);
                  
                  if(tSSRS.MaxRow == 0 || tSSRS.GetText(1, 1) == null || tSSRS.GetText(1, 1).trim().equals("")){
                      return "3999";
                  }
                  
                  LDCode1DB tLDCode1DB = new LDCode1DB();
                  tLDCode1DB.setCodeType("bankerror");
                  tLDCode1DB.setCode(bankCode);
                  tLDCode1DB.setCode1(tSSRS.GetText(1, 1));
                  tLDCode1DB.setCodeName(succMan.trim());
                  tLDCode1DB.insert();
                  return tSSRS.GetText(1, 1);
              } else {
                  return tSSRS.GetText(1, 1);
              }
          }
      }
      catch (RuntimeException e) {
          e.printStackTrace();
      }
      return "3999";
  }
  
}
