package com.sinosoft.lis.bank;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.SAXException;

import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BatchReadBaoRongFile
{

    public static int num = 0;

    
    public LYReturnFromBankSet xmlToDatabase(String path, String uBankCode)
    {
        SAXBuilder builder = new SAXBuilder(false);
        try
        {
            LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
            String dealType = "";
            Document doc = builder.build(path);
            Element root = doc.getRootElement();
            List detailList = null;
            String serialNo = "";

            if (uBankCode.equals("7701"))
            {
                Element transReq = root.getChild("TransResp");
                serialNo = transReq.getChildText("ReqSeqID");
                System.out.println("批次号" + serialNo);
                String sql = "select logtype from lybanklog where serialno='"
                        + serialNo + "' with ur";
                System.out.println(sql);
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = tExeSQL.execSQL(sql);
                dealType = tSSRS.GetText(1, 1);
                detailList = transReq.getChildren("DetailRecord");

            }
            
            for (Iterator iter = detailList.iterator(); iter.hasNext();)
            {
                Element detail = (Element) iter.next();
                String accNo = "";
                String accName="";
                if (dealType.equals("S"))
                {
                	accName=detail.getChildText("PayName");
                    accNo = detail.getChildText("PayAct");
                }
                else if (dealType.equals("F"))
                {
                	accName=detail.getChildText("RecName");
                    accNo = detail.getChildText("RecAct");
                }

               
                String payMoney = detail.getChildText("PayAmount");
                String bankSuccFlag = detail.getChildText("PayInfoCode");
                
                // 成功标识  2成功 3失败
                String bankSuccMan = detail.getChildText("Memo");
                System.out.println(bankSuccMan);
                String payCode = detail.getChildText("PostScript");

                LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();
                tLYReturnFromBankSchema.setSerialNo(serialNo);
                tLYReturnFromBankSchema.setDealType(dealType);
                tLYReturnFromBankSchema.setAccNo(accNo);
                tLYReturnFromBankSchema.setPayCode(payCode);
                tLYReturnFromBankSchema.setAccName(accName);
                tLYReturnFromBankSchema.setPayMoney(Double.valueOf(payMoney).doubleValue());
                tLYReturnFromBankSchema.setBankSuccFlag(bankSuccFlag);

                System.out.println("成功标记" + tLYReturnFromBankSchema.getBankSuccFlag());
                
                tLYReturnFromBankSchema.setDoType("1");
                tLYReturnFromBankSchema.setModifyDate(PubFun.getCurrentDate());
                tLYReturnFromBankSchema.setModifyTime(PubFun.getCurrentTime());
                tLYReturnFromBankSchema
                        .setBankDealDate(PubFun.getCurrentDate());

                LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
                tLYSendToBankDB.setSerialNo(tLYReturnFromBankSchema
                        .getSerialNo());
                tLYSendToBankDB.setAccNo(tLYReturnFromBankSchema.getAccNo());
                tLYSendToBankDB
                        .setPayCode(tLYReturnFromBankSchema.getPayCode());
                tLYSendToBankDB.setPayMoney(tLYReturnFromBankSchema
                        .getPayMoney());

                LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
                tLYSendToBankSet = tLYSendToBankDB.query();

                if (tLYSendToBankSet == null || tLYSendToBankSet.size() == 0)
                {
                    throw new Exception("从发送盘表获取数据失败！");
                }
                else if (tLYSendToBankSet.size() == 1
                        || tLYReturnFromBankSet.size() == 0)
                {
                    LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
                    tLYSendToBankSchema = tLYSendToBankSet.get(1);

                    if (tLYSendToBankSchema == null
                            || "".equals(tLYSendToBankSchema))
                    {
                        throw new Exception("从发送盘表获取数据失败！");
                    }

                    tLYReturnFromBankSchema.setPayCode(tLYSendToBankSchema
                            .getPayCode());
                    tLYReturnFromBankSchema.setPolNo(tLYSendToBankSchema
                            .getPolNo());
                    tLYReturnFromBankSchema.setNoType(tLYSendToBankSchema
                            .getNoType());
                    tLYReturnFromBankSchema.setBankCode(tLYSendToBankSchema
                            .getBankCode());
                    tLYReturnFromBankSchema.setComCode(tLYSendToBankSchema
                            .getComCode());
                    tLYReturnFromBankSchema.setAgentCode(tLYSendToBankSchema
                            .getAgentCode());
                    tLYReturnFromBankSchema.setName(tLYSendToBankSchema
                            .getName());
                    tLYReturnFromBankSchema.setSendDate(tLYSendToBankSchema.getSendDate());

                    tLYReturnFromBankSet.add(tLYReturnFromBankSchema);
                }
                else
                {
                    //增加了remake字段，不再使用，但若返回的paycode为空，可以对帐号金额相同数据的处理
                    for (int row = 1; row <= tLYSendToBankSet.size(); row++)
                    {
                        LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
                        tLYSendToBankSchema = tLYSendToBankSet.get(row);
                        if (checkSet(tLYSendToBankSchema, tLYReturnFromBankSet))
                        {
                            tLYReturnFromBankSchema
                                    .setPayCode(tLYSendToBankSchema
                                            .getPayCode());
                            tLYReturnFromBankSchema
                                    .setPolNo(tLYSendToBankSchema.getPolNo());
                            tLYReturnFromBankSchema
                                    .setNoType(tLYSendToBankSchema.getNoType());
                            tLYReturnFromBankSchema
                                    .setBankCode(tLYSendToBankSchema
                                            .getBankCode());
                            tLYReturnFromBankSchema
                                    .setComCode(tLYSendToBankSchema
                                            .getComCode());
                            tLYReturnFromBankSchema
                                    .setAgentCode(tLYSendToBankSchema
                                            .getAgentCode());
                            tLYReturnFromBankSchema.setName(tLYSendToBankSchema
                                    .getName());
                            tLYReturnFromBankSchema.setSendDate(tLYSendToBankSchema.getSendDate());
                            tLYReturnFromBankSet.add(tLYReturnFromBankSchema);
                            break;
                        }
                    }
                }
            }
            return tLYReturnFromBankSet;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError.buildErr(this, "Xml转入数据库处理失败: " + ex.getMessage());
        }

        return null;
    }

    private boolean checkSet(LYSendToBankSchema tSe, LYReturnFromBankSet tRe)
    {
        for (int rRow = 1; rRow <= tRe.size(); rRow++)
        {
            if (tSe.getPayCode().equals(tRe.get(rRow).getPayCode()))
            {
                return false;
            }
        }
        return true;
    }
    //添加新的错误类型
    private String getNewSucc(String succMan,String bankCode)
    {
        try {
            if (succMan == null || succMan.trim().equals("")){
                return "3";
            } else {
            	// 为解决[账户名]:的问题，每次都会插入新数据 
//    			if(succMan.lastIndexOf("]:") != -1){
//    				succMan = succMan.substring(succMan.lastIndexOf("]:") + 3);
//    				System.out.println(succMan);
//    			}
                String querySql = "select code1 from ldcode1 where codetype='bankerror' and code='" 
                        + bankCode + "' and codename='" + succMan.trim() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = tExeSQL.execSQL(querySql);
                if(tSSRS == null || tSSRS.MaxRow == 0){
//                    querySql = "select max(int(code1))+1 from ldcode1 where codetype='bankerror' and code='" 
//                        + bankCode + "'";
//                    tSSRS = tExeSQL.execSQL(querySql);
                    
                    if(tSSRS.MaxRow == 0 || tSSRS.GetText(1, 1) == null || tSSRS.GetText(1, 1).trim().equals("")){
                        return "3";
                    }
                    
                    LDCode1DB tLDCode1DB = new LDCode1DB();
                    tLDCode1DB.setCodeType("bankerror");
                    tLDCode1DB.setCode(bankCode);
                    tLDCode1DB.setCode1(tSSRS.GetText(1, 1));
                    tLDCode1DB.setCodeName(succMan.trim());
                    tLDCode1DB.insert();
                    return tSSRS.GetText(1, 1);
                } else {
                    return tSSRS.GetText(1, 1);
                }
            }
        }
        catch (RuntimeException e) {
            e.printStackTrace();
        }
        return "3";
    }

    public static void main(String[] args) throws SAXException, IOException
    {

        try
        {
            BatchReadBaoRongFile tBatchReadFile = new BatchReadBaoRongFile();
            LYReturnFromBankSet tLYReturnFromBankSet = tBatchReadFile
                    .xmlToDatabase(
                            "C:\\77011030000000121049rq.xml",
                            "7701");
            System.out.println(tLYReturnFromBankSet.get(1).getPayCode());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
