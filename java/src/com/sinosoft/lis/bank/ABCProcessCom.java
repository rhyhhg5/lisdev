package com.sinosoft.lis.bank;

import com.sinosoft.utility.ExeSQL;

public class ABCProcessCom {
	
	private static String url = "";

    private static String pfxPath = "";

    private static String pfxPassword = "";

    private static String cerPath = "";
    
    public ABCProcessCom(String bankflag, String type)
    {
        String sqlurl = "select codename from ldcode1 where codetype='BatchSendBank' and code='BatchServerUrl' and code1='"
                + bankflag + "'";
        url = new ExeSQL().getOneValue(sqlurl);

        sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='CerTlPath'";
        cerPath = new ExeSQL().getOneValue(sqlurl);

        sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='PfxTlPath"
                + type + "'";
        pfxPath = new ExeSQL().getOneValue(sqlurl);

        sqlurl = "select codename from ldcode1 where codetype='BatchSendBank' and code='PfxPass' and code1='"
                + type + bankflag + "'";
        pfxPassword = new ExeSQL().getOneValue(sqlurl);

        if (url == null || url.equals("") || cerPath == null
                || cerPath.equals("") || pfxPath == null || pfxPath.equals(""))
        {
            System.out.println("数据发送准备数据不全");
        }
    }

    /**
     * @param reqPackage
     * @return
     */
    public String send(String xml)
    {
        System.out.println("---7706---BatchProcessCom---发送报文---");
        //byte[] postData;
        String xmlLeft = xml.substring(0, xml
                .indexOf("<SIGNED_MSG></SIGNED_MSG>"));
        String xmlRight = xml.substring(xml
                .indexOf("<SIGNED_MSG></SIGNED_MSG>") + 25);
        String xmlKey = "";
        try
        {
            byte[] signed = new FileKeyManager(pfxPath, pfxPassword, cerPath)
                    .signeMsg((xmlLeft + xmlRight).getBytes("GBK"));
            xmlKey = HEXUtil.toString(signed);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println("签证信息赋值错误");
            return "";
        }
        xml = xmlLeft + "<SIGNED_MSG>" + xmlKey + "</SIGNED_MSG>" + xmlRight;
        System.out.println("发送报文内容" + xml);
        
        BatchSendXml tBatchSendXml = new BatchSendXml();
        String respText = tBatchSendXml.send(xml, "7706", url);
        return respText;
    }
    public static void main(String[] args) throws Exception
    {
        //BatchProcessCom tBatchPressCom = new BatchProcessCom("7705", "S");
//        tBatchPressCom
//                .send("<?xml version=\"1.0\" encoding=\"GBK\"?><AIPG><INFO><TRX_CODE>100002</TRX_CODE><VERSION>04</VERSION><DATA_TYPE>2</DATA_TYPE><USER_NAME>134520000102</USER_NAME><USER_PASS>111111</USER_PASS><REQ_SN>134520000120111214141354953</REQ_SN><SIGNED_MSG></SIGNED_MSG><LEVEL>5</LEVEL></INFO><BODY><TRANS_SUM><BUSINESS_CODE>00600</BUSINESS_CODE><MERCHANT_ID>1345200001</MERCHANT_ID><SUBMIT_TIME/><TOTAL_ITEM>5</TOTAL_ITEM><TOTAL_SUM>35</TOTAL_SUM></TRANS_SUM><TRANS_DETAILS><TRANS_DETAIL><SN>0005</SN><BANK_CODE>102</BANK_CODE><ACCOUNT_NO>9558803602980322785</ACCOUNT_NO><ACCOUNT_NAME>&#31532;5&#20154;</ACCOUNT_NAME><ACCOUNT_PROP>0</ACCOUNT_PROP><AMOUNT>5</AMOUNT><CURRENCY></CURRENCY></TRANS_DETAIL><TRANS_DETAIL><SN>0006</SN><BANK_CODE>102</BANK_CODE><ACCOUNT_NO>9558803602980322786</ACCOUNT_NO><ACCOUNT_NAME>&#31532;6&#20154;</ACCOUNT_NAME><ACCOUNT_PROP>0</ACCOUNT_PROP><AMOUNT>6</AMOUNT><CURRENCY></CURRENCY></TRANS_DETAIL><TRANS_DETAIL><SN>0007</SN><BANK_CODE>102</BANK_CODE><ACCOUNT_NO>9558803602980322787</ACCOUNT_NO><ACCOUNT_NAME>&#31532;7&#20154;</ACCOUNT_NAME><ACCOUNT_PROP>0</ACCOUNT_PROP><AMOUNT>7</AMOUNT><CURRENCY></CURRENCY></TRANS_DETAIL><TRANS_DETAIL><SN>0008</SN><BANK_CODE>102</BANK_CODE><ACCOUNT_NO>9558803602980322788</ACCOUNT_NO><ACCOUNT_NAME>&#31532;8&#20154;</ACCOUNT_NAME><ACCOUNT_PROP>0</ACCOUNT_PROP><AMOUNT>8</AMOUNT><CURRENCY></CURRENCY></TRANS_DETAIL><TRANS_DETAIL><SN>0009</SN><BANK_CODE>102</BANK_CODE><ACCOUNT_NO>9558803602980322789</ACCOUNT_NO><ACCOUNT_NAME>&#31532;9&#20154;</ACCOUNT_NAME><ACCOUNT_PROP>0</ACCOUNT_PROP><AMOUNT>9</AMOUNT><CURRENCY></CURRENCY></TRANS_DETAIL></TRANS_DETAILS></BODY></AIPG>");
    }
}
