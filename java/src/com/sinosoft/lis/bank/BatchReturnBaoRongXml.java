package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDBankUniteSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class BatchReturnBaoRongXml
{
    private String mUrl = "";
    private String bankaccno = "";

    private MMap map = new MMap();

    private VData mInputData = new VData();

    public boolean submitData(String mUrl)
    {
        this.mUrl = mUrl + "/";

        if (!dealDate())
        {
            System.out.println("获取数据失败");
            return false;
        }

        if (!prepareOutputData())
        {
            System.out.println("准备更新日志数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE"))
        {
            //错误处理
            System.out.println("更新日志失败");
            return false;
        }

        return true;
    }

    public boolean dealDate()
    {
        String sql = "select * from lybanklog a where a.dealstate is null and a.senddate is not null and a.bankcode in ('7701') with ur";
        System.out.println(sql);
        LYBankLogDB tLyBankLogDB = new LYBankLogDB();
        LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
        if (tLYBankLogSet == null)
        {
            System.out.println("未查到待回盘银行数据");
            return false;
        }
        for (int i = 1; i <= tLYBankLogSet.size(); i++)
        {
            LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
            String serialno = tLYBankLogSchema.getSerialNo();
            sql = "select * from lysendtobank where serialno='" + serialno
                    + "' with ur";
            System.out.println(sql);
            LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
            LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB
                    .executeQuery(sql);
            createXml(tLYBankLogSchema, tLYSendToBankSet);
        }
        return true;
    }

    public void createXml(LYBankLogSchema tLYBankLogSchema,
            LYSendToBankSet tLYSendToBankSet)
    {
        String bankUniteCode = tLYBankLogSchema.getBankCode();
        String type = tLYBankLogSchema.getLogType();
        LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(1);
        String bank = uniteBankCode(tLYSendToBankSchema.getBankCode(), bankUniteCode);
        
       
        Element root = null;
        if (bankUniteCode.equals("7701"))
        {
            root = new Element("Root");
        }

        Document doc = new Document(root);

        Element ele_INFO = new Element("Head");
        if (type.equals("S"))
        {
            setText(ele_INFO, "CommandCode", "21210");//同行代收查询
        }
        if(type.equals("F"))
        {
        	if(bank.equals("308"))
        	{
        		setText(ele_INFO, "CommandCode", "20220");//综合代付查询
        	}
        	else
        	{
        		setText(ele_INFO, "CommandCode", "21220");//同行代付查询 
        	}
            
        }

        setText(ele_INFO, "EnterpriseNum", "");
        if(type.equals("S"))
        {
        	setText(ele_INFO, "CorpBankCode", bank); //发起方银行渠道代码
        }
        if(type.equals("F"))
        {
        	if(bank.equals("308"))
        	{
        		setText(ele_INFO, "CorpBankCode", "102"); //发起方银行渠道代码
        	}
        	else
        	{
        		setText(ele_INFO, "CorpBankCode", bank); //发起方银行渠道代码
        	}
        }
        setText(ele_INFO, "TransSeqID", PubFun.getCurrentDate2()
                + PubFun.getCurrentTime2());//时间戳
        setText(ele_INFO, "VerifyCode", "");//验签码
        setText(ele_INFO, "ZipType", "");//压缩方式
        setText(ele_INFO, "RespCode", "");//响应编码
        setText(ele_INFO, "RespInfo", "");//响应信息
        root.addContent(ele_INFO);
        if (type.equals("S"))
        {
        	
        	String sqlbankaccno="select bankaccno from ldfinbank  where finbankcode='"+bank+"' and finflag='S' with ur ";
        	String bankaccno = new ExeSQL().getOneValue(sqlbankaccno);
            Element ele_TransReq = new Element("TransReq");
            setText(ele_TransReq, "ReqSeqID", tLYBankLogSchema.getSerialNo());//批次号 Y
            setText(ele_TransReq, "BgnDate", tLYBankLogSchema.getSendDate());//交易提交日期 Y
            setText(ele_TransReq, "RecAct", bankaccno);//企业方账户 Y
            setText(ele_TransReq, "RecArea", "");//区域代码
//            double sum = 0;
//            for (int i = 1; i <= tLYSendToBankSet.size(); i++)
//            {
//                sum = Arith.add(sum, tLYSendToBankSet.get(i).getPayMoney());
//            }
            //DecimalFormat df = new DecimalFormat("0.00");
            setText(ele_TransReq, "TotalNum", Long.toString(tLYBankLogSchema.getTotalNum()));//总笔数 Y
            //setText(ele_TransReq, "TotalAmount", df.format(Long.toString((long)tLYBankLogSchema.getTotalMoney())));//总金额 Y
            setText(ele_TransReq, "TotalAmount", (new DecimalFormat("0.00")).format(Double.valueOf(tLYBankLogSchema.getTotalMoney())));//总金额 Y
            Element ele_CorpToBank = new Element("CorpToBank");
            setText(ele_CorpToBank, "EncryptFlag", "");//是否对文件关键字段加密
            root.addContent(ele_CorpToBank);
            root.addContent(ele_TransReq);
        }
        else
        {
        	if(bank.equals("308"))
        	{
        		String sqlbankaccno="select bankaccno from ldfinbank  where finbankcode='102' and finflag='F' with ur";
        		bankaccno = new ExeSQL().getOneValue(sqlbankaccno);
        	}
        	else
        	{
            	String sqlbankaccno="select bankaccno from ldfinbank  where finbankcode='"+bank+"' and finflag='F' with ur";
            	bankaccno = new ExeSQL().getOneValue(sqlbankaccno);
        	}
        	Element ele_TransReq = new Element("TransReq");
            setText(ele_TransReq, "ReqSeqID", tLYBankLogSchema.getSerialNo());
            setText(ele_TransReq, "BgnDate", tLYBankLogSchema.getSendDate());
            setText(ele_TransReq, "PayAct", bankaccno);//企业方账户 Y
            setText(ele_TransReq, "PayArea", "");

            setText(ele_TransReq, "TotalNum", Long.toString(tLYBankLogSchema.getTotalNum()));
            setText(ele_TransReq, "TotalAmount", (new DecimalFormat("0.00")).format(Double.valueOf(tLYBankLogSchema.getTotalMoney())));
            
            Element ele_CorpToBank = new Element("CorpToBank");
            setText(ele_CorpToBank, "EncryptFlag", "");
            root.addContent(ele_CorpToBank);
           
            root.addContent(ele_TransReq);
        }

        XMLOutputter XMLOut = new XMLOutputter();
        try
        {
            XMLOut.setEncoding("GBK");
            XMLOut.output(doc, new FileOutputStream(this.mUrl
                    + tLYBankLogSchema.getSerialNo() + ".xml"));
            String sendStr = readtxt(this.mUrl + tLYBankLogSchema.getSerialNo()
                    + ".xml");
            int length=0;
    		try {
    			length = sendStr.getBytes("GBK").length;
    		} catch (UnsupportedEncodingException e1) {
    			e1.printStackTrace();
    		}
    		String lengthStr = String.valueOf(length);
    		int s = 8 - lengthStr.length();
    		for (int i = 0; i < s ; i++) {
    			lengthStr = " "+lengthStr;
    		}
    		sendStr = "Content-Length:"+lengthStr+"\r"+"\n"+sendStr;
            
            System.out.println("发送查询报文内容" + sendStr);

            if (!sendStr.equals(""))
            {
                if (bankUniteCode.equals("7701"))
                {
                	String payReq = SendBank(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        saveReq(payReq, tLYBankLogSchema, "7701");
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema, 1);
                    }
                }
				
            }

        }
        catch (Exception ex)
        {
            System.out.println(tLYBankLogSchema.getSerialNo() + "批次程序处理异常");
            ex.printStackTrace();
        }
    }
    public String SendBank(String xml) throws IOException
    {
    	String info = "";
		String backInfo="";
		Socket socket = null;
		OutputStreamWriter os = null;
		java.io.InputStream is=null;
		byte[] bytes = new byte[2048];
		try
		{
			// 1.创建客户端Socket，指定服务器地址和端口
			
			String sqlurl="select codename from ldcode1 where codetype='BRBatchSendBank' and code='BatchServerUrl' with ur ";
			String url=new ExeSQL().getOneValue(sqlurl);
			String sqlPort="select codename from ldcode1 where codetype='BRBatchSendBank' and code='BatchServerPort' with ur ";
			int port = Integer.parseInt(new ExeSQL().getOneValue(sqlPort).trim());
			System.out.println(url);
			System.out.println(port);
			socket = new Socket(url, port);
			socket.setSoTimeout(10000);
			// 2.获取输出流，向服务器端发送信息
			os = new OutputStreamWriter(socket.getOutputStream(),"GBK");// 字节输出流
			
//			socket.shutdownOutput();// 关闭输出流
			// 3.获取输入流，并读取服务器端的响应信息
			is = socket.getInputStream();	
			os.write(xml);
			os.flush();

			int tmpOffSet = 0;
			int contentLength=-1;
			int contentStartIndex=-1;
            byte tmpByteLt = "<".getBytes("GBK")[0];
			while(true){
				
				int tmpReadLen = is.read(bytes, tmpOffSet, bytes.length - tmpOffSet);
				if(tmpReadLen<=0){
					break;
				}
				if(contentLength == -1){
					for(int i=tmpOffSet;i<tmpReadLen;i++){
						if(bytes[i]== tmpByteLt){
							if(contentStartIndex<0){
								contentStartIndex = i;
							}
							String tmpHeadStr = new String(bytes,0,i,"GBK");
							String[] tmpKeyValues = tmpHeadStr.split("\n");
							for(int j=0;j<tmpKeyValues.length;j++){
								String[] tmpEach = tmpKeyValues[j].split(":");
								if(tmpEach[0].toLowerCase().equals("content-length")){
									contentLength = Integer.valueOf(tmpEach[1].trim());
								}
							}
							break;
						}
					}
				}
				tmpOffSet += tmpReadLen;
				
				if (contentLength > 0 && contentStartIndex > 0)
                {
                    if (bytes.length < contentLength + contentStartIndex)
                    {
                        byte[] tmpUnionInfo = new byte[contentLength + contentStartIndex];//一次性分配内存
        				System.arraycopy(bytes, 0, tmpUnionInfo, 0, tmpOffSet);
        				bytes = null;
        				bytes = tmpUnionInfo;
                    }
                    
                    int tmpToReadLen = contentLength + contentStartIndex - tmpOffSet;
                    if (tmpToReadLen <= 0)
                    {
                    	break;
                    }
                }
				
				
			}
			if((backInfo=new String(bytes,"GBK").trim()) != null)
				{
					System.out.println("我是客户端，服务器说：" + backInfo);
					
				}
		
	}catch(Exception e) {
		e.printStackTrace();
	} finally {
        if (os != null) {  
            try {  
            	os.close();  
            } catch (Exception e) {  
            }  
        }//关闭Socket输出流  
        if (is != null) {  
        	try {  
        		is.close();  
        	} catch (Exception e) {  
        	}  
        }//关闭Socket输入流  
        if (socket != null) {  
        	try {  
        		socket.close();  
        	} catch (IOException e) {  
        	}  
        }  
    } 
		info=backInfo.substring(23, backInfo.length()).trim();
		return info;
	}
    

    private boolean setText(Element element, String name, String value)
    {

        Element ele = new Element(name);
        ele.addContent(value);
        element.addContent(ele);
        return true;
    }
    private String uniteBankCode(String bankcode, String bankunitecode)
    {
        String sql = "select * from ldbankunite where bankcode='" + bankcode
                + "' and bankunitecode='" + bankunitecode + "' with ur";
        System.out.println(sql);
        LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
        LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.executeQuery(sql);
        LDBankUniteSchema tLDBankUniteSchema = tLDBankUniteSet.get(1);
        return tLDBankUniteSchema.getUniteBankCode();
    }
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema,
            String bankCode)
    {
        String tUrl = this.mUrl + tLYBankLogSchema.getSerialNo() + "rq.xml";
        try
        {
            FileWriter fw = new FileWriter(tUrl);
            fw.write(payReq);
            fw.close();
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, 2);
            ex.printStackTrace();
            return false;
        }
        if (!checkReq(payReq, tLYBankLogSchema))
        {
            return false;
        }
        System.out.println("成功了！！！！");
        BatchReturnBaoRongFromBankBL tBatchReturnBaoRongFromBankBL = new BatchReturnBaoRongFromBankBL();
        try
        {
            if (!tBatchReturnBaoRongFromBankBL.submitData(tUrl, bankCode))
            {
                errBankLog(tLYBankLogSchema, 2);
            }
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, 2);
            ex.printStackTrace();
        }
        return true;
    }

    private boolean checkReq(String req, LYBankLogSchema tLYBankLogSchema)
    {
        int iStart = req.indexOf("<RespCode>");
        if (iStart != -1)
        {
            int end = req.indexOf("</RespCode>");
            String signedMsg = req.substring(iStart + 10, end);
            int signedMsg1=Integer.parseInt(signedMsg);
            System.out.println("响应标记：" + signedMsg1);
            if (signedMsg1>=0)
            {
                return true;
            }
            else
            {
                int s_MSG = req.indexOf("<RespInfo>");
                if (s_MSG != -1)
                {
                    int e_MSG = req.indexOf("</RespInfo>");
                    System.out.println("截去字符串的起始位置:" + (s_MSG+10) + ";终止位置:" + e_MSG);
                    String signedMSG = req.substring(s_MSG + 10, e_MSG);
                    System.out.println("响应标记：" + signedMSG);
                    if (signedMSG.length() > 50)
                    {
                        tLYBankLogSchema.setInFile(signedMSG.substring(0, 40));
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                    else
                    {
                        tLYBankLogSchema.setInFile(signedMSG);
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                    this.map.put(tLYBankLogSchema, "UPDATE");
                }
            }
        }
        return false;

    }

    private void errBankLog(LYBankLogSchema tLYBankLogSchema, int flag)
    {
        if (flag == 1)
        {
            System.out.println("获取响应信息失败");
            tLYBankLogSchema.setInFile("获取响应信息失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            System.out.println("回盘处理程序出现异常");
            tLYBankLogSchema.setInFile("回盘处理程序出现异常");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        this.map.put(tLYBankLogSchema, "UPDATE");
    }

    private String readtxt(String path)
    {
        String str = "";
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(path));

            String r = br.readLine();
            while (r != null)
            {
                str += r;
                r = br.readLine();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
        //str = str.replaceAll("<SIGNED_MSG />", "<SIGNED_MSG></SIGNED_MSG>");
        return str;
    }

    //紧急处理时使用，模拟回盘
    public boolean batchReturn(String req, String serialNo)
    {
        String sql = "select * from lybanklog where serialno='" + serialNo
                + "' with ur";
        System.out.println(sql);
        LYBankLogDB spLYBankLogDB = new LYBankLogDB();
        LYBankLogSet spLYBankLogSet = spLYBankLogDB.executeQuery(sql);
        if (spLYBankLogSet.size() == 0)
        {
            System.out.println("批次号错误！");
            return false;
        }
        LYBankLogSchema spLYBankLogSchema = spLYBankLogSet.get(1);
        String bankCode = spLYBankLogSchema.getBankCode();

        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchReturnPath'";//发盘报文的存放路径
        this.mUrl = new ExeSQL().getOneValue(sqlurl) + "special";
        System.out.println("发盘报文的存放路径:" + mUrl);
        File myFilePath = new File(mUrl.toString());
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
        }
        spLYBankLogSchema.setSerialNo(serialNo);
        if (!saveReq(req, spLYBankLogSchema, bankCode))
        {
            return false;
        }
        return true;
    }

//    public static void main(String[] arr)
//    {
//        BatchReturnBaoRongXml tBatchReturnXml = new BatchReturnBaoRongXml();
//        tBatchReturnXml.batchReturn("111111", "00000000000000094990");
//    }
    public static void main(String[] arr)
    {
    	LYBankLogSchema banklog = new LYBankLogSchema();
    	LYBankLogDB banklogDB = new LYBankLogDB();
    	banklogDB.setSerialNo("77013080000000122171");
    	if(banklogDB.getInfo()){
    		banklog = banklogDB.getSchema();
    	}
    	System.out.println(banklog.getBankCode());
    	LYSendToBankSet sendtobank = new LYSendToBankSet();
    	LYSendToBankDB	sendtobankDB = new LYSendToBankDB();
    	String sql = "select * from lysendtobank where serialno='77013080000000122171' with ur";
    	
    	sendtobank = sendtobankDB.executeQuery(sql);
    	
    	BatchReturnBaoRongXml bscx = new BatchReturnBaoRongXml();
//    	bscx.filePath = "E:\\";
    	bscx.mUrl = "E:\\";
    	bscx.createXml(banklog, sendtobank);
    }

}
