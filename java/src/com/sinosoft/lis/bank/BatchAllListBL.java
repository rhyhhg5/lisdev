package com.sinosoft.lis.bank;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author 亓莹莹
 * function ://邮储代收代付汇总清单
 * @version 1.0
 * @date 2011-3-1
 */

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BatchAllListBL 
{
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //接收的数据
    private String mFlag = "" ;
    private String mComCode = "" ;// 管理机构  comCode
    private String mSFFlag = "";//代收代付
    private TransferData mTransferData = new TransferData();
    //定义需要用到的数据
    private String mSerialNo = "" ;//批次号
    private String mBankCode = "" ;//银行代码
    private String mBankName = "" ;//银行名称
    private double mSuccSumDuePayMoney = 0.0;//成功总金额
    private int mSuccSumCount = 0;//成功总笔数
    private double mFailSumDuePayMoney =0.0; //失败总金额
    private int mFailSumCount = 0;//失败总笔数
    
    public BatchAllListBL() 
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
        
        if (!cOperate.equals("PRINT")) 
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) 
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if(!getPrintData())
        {
            return false;
        }
        
        return true;
    }

    private boolean getInputData(VData cInputData) 
    {
        mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
        System.out.println("mTransferData--"+mTransferData);
        mSerialNo = (String) mTransferData.getValueByName("SerialNo");
        System.out.println("mSerialNo--"+mSerialNo);
        mBankCode = (String) mTransferData.getValueByName("BankCode");
        mFlag = (String) mTransferData.getValueByName("Flag");
        mComCode = (String) mTransferData.getValueByName("ManageCom");
        System.out.println("serialNo--"+mSerialNo);
        System.out.println("bankcode--"+mBankCode);
        System.out.println("flag--"+mFlag);
        if("S".equals(mFlag)){
            mSFFlag = "S";
        }
        if("F".equals(mFlag)){
            mSFFlag = "F";
        }
        return true;
    }

    public VData getResult() 
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) 
    {
        CError cError = new CError();
        cError.moduleName = "BatchAllListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    //查询出首期或者是续期的银行代收正确清单
    private boolean getPrintData() 
    {
       
        //定义报表相关的类
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("BatchSendAll.vts", "printer");
        ListTable alistTable = new ListTable();
        alistTable.setName("INFO");

        ExeSQL exesqlBankName = new ExeSQL();
        SSRS bankNameSSRS = new SSRS();
        String query_bankName = "select bankname from ldbank where bankcode= '"+ mBankCode +"'";
        bankNameSSRS = exesqlBankName.execSQL(query_bankName);
        if(bankNameSSRS.getMaxRow()==0)
        {
            mBankName = " ";
            
        }
        else
        {
            mBankName = bankNameSSRS.GetText(1, 1);
        }
        
        /**
         *计算总金额和总笔数
         */
        //数据库中banksuccflag的值为空，无法判断是成功还是失败
        String query_succ = "";
        String query_fail = "";
        if("7707".equals(mBankCode))
        {
        	query_succ = "select sum(paymoney),count(*),serialno,banksuccflag from lyreturnfrombank"
                    +" where serialno = '"+mSerialNo+"' and banksuccflag='00'"
                    +" and dealtype='"+mSFFlag+"'";
                    if(!(mComCode.equals(""))||mComCode!=null)
                    {
                        query_succ = query_succ +" and comcode like '"+mComCode+"%'";
                    }
                    query_succ = query_succ+" group by serialno, banksuccflag union all"
                            +" select sum(paymoney),count(*),serialno,banksuccflag from lyreturnfrombankb"
                            +" where serialno = '"+mSerialNo+"' and banksuccflag='00'"
                            +" and dealtype='"+mSFFlag+"' ";
                    if(!(mComCode.equals(""))||mComCode!=null)
                    {
                        query_succ = query_succ +" and comcode like '"+mComCode+"%'";
                    }
                    query_succ = query_succ + " group by serialno,banksuccflag";
                
                 query_fail = "select sum(paymoney),count(*),serialno from lyreturnfrombank"
                    +" where serialno = '"+mSerialNo+"' and (banksuccflag<>'00' or banksuccflag is null )"
                    +" and dealtype='"+mSFFlag+"'";
                    if(!(mComCode.equals(""))||mComCode!=null)
                    {
                        query_fail = query_fail +" and comcode like '"+mComCode+"%'";
                    }
                    query_fail = query_fail+" group by serialno union all"
                            +" select sum(paymoney),count(*),serialno from lyreturnfrombankb"
                            +" where serialno = '"+mSerialNo+"' and (banksuccflag<>'00' or banksuccflag is null)"
                            +" and dealtype='"+mSFFlag+"'";
                    if(!(mComCode.equals(""))||mComCode!=null)
                    {
                        query_fail = query_fail +" and comcode like '"+mComCode+"%'";
                    }
                    query_fail = query_fail + " group by serialno";
          }
        if("7701".equals(mBankCode))
        {
    		query_succ = "select sum(paymoney),count(*),serialno,banksuccflag from lyreturnfrombank"
    				+" where serialno = '"+mSerialNo+"' and banksuccflag='S0000'"
    				+" and dealtype='"+mSFFlag+"'";
    		if(!(mComCode.equals(""))||mComCode!=null)
    		{
    			query_succ = query_succ +" and comcode like '"+mComCode+"%'";
    		}
    		query_succ = query_succ+" group by serialno, banksuccflag union all"
    				+" select sum(paymoney),count(*),serialno,banksuccflag from lyreturnfrombankb"
    				+" where serialno = '"+mSerialNo+"' and banksuccflag='S0000'"
    				+" and dealtype='"+mSFFlag+"' ";
    		if(!(mComCode.equals(""))||mComCode!=null)
    		{
    			query_succ = query_succ +" and comcode like '"+mComCode+"%'";
    		}
    		query_succ = query_succ + " group by serialno,banksuccflag";
    		
    		query_fail = "select sum(paymoney),count(*),serialno from lyreturnfrombank"
    				+" where serialno = '"+mSerialNo+"' and (banksuccflag<>'S0000' or banksuccflag is null )"
    				+" and dealtype='"+mSFFlag+"'";
    		if(!(mComCode.equals(""))||mComCode!=null)
    		{
    			query_fail = query_fail +" and comcode like '"+mComCode+"%'";
    		}
    		query_fail = query_fail+" group by serialno union all"
    				+" select sum(paymoney),count(*),serialno from lyreturnfrombankb"
    				+" where serialno = '"+mSerialNo+"' and (banksuccflag<>'S0000' or banksuccflag is null)"
    				+" and dealtype='"+mSFFlag+"'";
    		if(!(mComCode.equals(""))||mComCode!=null)
    		{
    			query_fail = query_fail +" and comcode like '"+mComCode+"%'";
    		}
    		query_fail = query_fail + " group by serialno";
    	}
        
        else{
        	
        	   query_succ = "select sum(paymoney),count(*),serialno,banksuccflag from lyreturnfrombank"
        	            +" where serialno = '"+mSerialNo+"' and banksuccflag='0000'"
        	            +" and dealtype='"+mSFFlag+"'";
        	            if(!(mComCode.equals(""))||mComCode!=null)
        	            {
        	                query_succ = query_succ +" and comcode like '"+mComCode+"%'";
        	            }
        	            query_succ = query_succ+" group by serialno, banksuccflag union all"
        	                    +" select sum(paymoney),count(*),serialno,banksuccflag from lyreturnfrombankb"
        	                    +" where serialno = '"+mSerialNo+"' and banksuccflag='0000'"
        	                    +" and dealtype='"+mSFFlag+"' ";
        	            if(!(mComCode.equals(""))||mComCode!=null)
        	            {
        	                query_succ = query_succ +" and comcode like '"+mComCode+"%'";
        	            }
        	            query_succ = query_succ + " group by serialno,banksuccflag";
        	        
        	         query_fail = "select sum(paymoney),count(*),serialno from lyreturnfrombank"
        	            +" where serialno = '"+mSerialNo+"' and (banksuccflag<>'0000' or banksuccflag is null )"
        	            +" and dealtype='"+mSFFlag+"'";
        	            if(!(mComCode.equals(""))||mComCode!=null)
        	            {
        	                query_fail = query_fail +" and comcode like '"+mComCode+"%'";
        	            }
        	            query_fail = query_fail+" group by serialno union all"
        	                    +" select sum(paymoney),count(*),serialno from lyreturnfrombankb"
        	                    +" where serialno = '"+mSerialNo+"' and (banksuccflag<>'0000' or banksuccflag is null)"
        	                    +" and dealtype='"+mSFFlag+"'";
        	            if(!(mComCode.equals(""))||mComCode!=null)
        	            {
        	                query_fail = query_fail +" and comcode like '"+mComCode+"%'";
        	            }
        	            query_fail = query_fail + " group by serialno";
        	
        }
      
        
        ExeSQL succExeSql = new ExeSQL();
        SSRS succSSRS = new SSRS();
        succSSRS = succExeSql.execSQL(query_succ);
        System.out.println("---成功----是否查出--"+succSSRS.getMaxRow());
        if(succSSRS.getMaxRow()==0)
        {   
            mSuccSumDuePayMoney = 0.0 ;
            mSuccSumCount = 0 ;
            
        }else
        {
            System.out.println("--成功----sql---");
            String strSuccSumDuePayMoney = succSSRS.GetText(1, 1);
            mSuccSumDuePayMoney = Double.parseDouble(strSuccSumDuePayMoney);
            String strSuccSumCount = succSSRS.GetText(1, 2);
            mSuccSumCount = Integer.parseInt(strSuccSumCount);
        }
        SSRS failSSRS = new SSRS();
        failSSRS = succExeSql.execSQL(query_fail);
        System.out.println(query_fail);
        System.out.println("---失败--是否查出--"+failSSRS.getMaxRow());
        if(failSSRS.getMaxRow()==0)
        {
            mFailSumDuePayMoney = 0.0 ;
            mFailSumCount = 0 ;
            
        }else{
            System.out.println("-----失败----sql----");
            String strSuccSumDuePayMoney = failSSRS.GetText(1, 1);
            System.out.println("--金额--" + failSSRS.GetText(1, 1));
            mFailSumDuePayMoney = Double.parseDouble(strSuccSumDuePayMoney);
            String strSuccSumCount = failSSRS.GetText(1, 2);
            System.out.println("--笔数--" + failSSRS.GetText(1, 2));
            mFailSumCount = Integer.parseInt(strSuccSumCount);
        }
        
        /*************************************************************************
         * 
         */
        
        String tFlag = "";
        texttag.add("BillNo", mSerialNo);
        texttag.add("Date", PubFun.getCurrentDate());
        texttag.add("BankCode", mBankCode);
        texttag.add("BankName", mBankName);
        texttag.add("SuccSumDuePayMoney", mSuccSumDuePayMoney);
        texttag.add("SuccSumCount", mSuccSumCount);
        texttag.add("FailSumDuePayMoney", mFailSumDuePayMoney);
        texttag.add("FailSumCount", mFailSumCount);
        if("S".equals(mFlag))
        {
            tFlag = "总公司集中代收";
        }
        if("F".equals(mFlag))
        {
            tFlag = "总公司集中代付";
        }
        texttag.add("F",tFlag);
        System.out.println("texttag------"+texttag);
        System.out.println("xmlexport-----"+xmlexport);
        xmlexport.addDisplayControl("displayinfo");
        String[] b_col = new String[9];
        xmlexport.addListTable(alistTable, b_col);
        if (texttag.size() > 0) 
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("d:\\","YCAllListBL"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
   }

}

