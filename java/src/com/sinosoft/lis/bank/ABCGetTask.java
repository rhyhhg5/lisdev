package com.sinosoft.lis.bank;

import java.io.File;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.taskservice.BatchSendBankTrans;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

public class ABCGetTask
{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private String mUrl = null;

	private String mPath = null;

	public void run()
	{
		// 提盘
		System.out.println("---ABCBankFetchDate开始---");
		ABCBankFetchDate tABCBankFinDate = new ABCBankFetchDate();
		boolean b = tABCBankFinDate.CreatDate("F");
		if (b == false)
		{
			System.out.println("---ABCBankFetchDate异常结束---");
			return;
		}
		System.out.println("---ABCBankFetchDate正常结束---");

		// 发盘
		System.out.println("---BatchSendBankTrans开始---");
		submitData();
		System.out.println("---BatchSendBankTrans正常结束---");

	}

	public boolean submitData()
	{

		if (!getUrlName())
		{
			return false;
		}

		if (!dealData())
		{
			return false;
		}
		return true;
	}

	/**
	 * dealData 数据逻辑处理
	 * 
	 * @return boolean
	 */
	private boolean dealData()
	{
		mPath = mUrl + "ABCBankF/" + PubFun.getCurrentDate2().substring(0, 4)
				+ "/" + PubFun.getCurrentDate2().substring(4, 6) + "/"
				+ PubFun.getCurrentDate2().substring(6, 8);
		if (!newFolder(mPath))
		{
			mPath = mUrl;
		}
		System.out.println("文件存放路径" + mPath);
		ABCBankCreadXml tABCBankCreadXml = new ABCBankCreadXml();
		// 由于通联银联、代收代付需要不同时间段的批处理进行处理
		tABCBankCreadXml.submitData(mPath, "F", "'7708'");
		return true;
	}

	/**
	 * newFolder 新建报文存放文件夹，以便对发盘报文查询
	 * 
	 * @return boolean
	 */
	public static boolean newFolder(String folderPath)
	{
		String filePath = folderPath.toString();
		File myFilePath = new File(filePath);
		try
		{
			if (myFilePath.isDirectory())
			{
				System.out.println("目录已存在");
				return true;
			} else
			{
				myFilePath.mkdirs();
				System.out.println("新建目录成功");
				return true;
			}
		} catch (Exception e)
		{
			System.out.println("新建目录失败");
			e.printStackTrace();
			return false;
		}
	}

	public boolean getSendData()
	{
		try
		{
			BatchSendBL tBatchSendBL = new BatchSendBL();
			tBatchSendBL.submitData();
		} catch (Exception ex)
		{
			System.out.println("-----提取数据程序报错！--TT-----");
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * getUrlName 获取文件存放路径
	 * 
	 * @return boolean
	 */
	private boolean getUrlName()
	{
		String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchSendPath'";// 发盘报文的存放路径
		mUrl = new ExeSQL().getOneValue(sqlurl);
		//mUrl = "D:\\ABC\\";
		System.out.println("发盘报文的存放路径:" + mUrl);// 调试用－－－－－
		if (mUrl == null || mUrl.equals(""))
		{
			buildError("getFileUrlName", "获取文件存放路径出错");
			return false;
		}
		return true;
	}

	private void buildError(String szFunc, String szErrMsg)
	{
		CError cError = new CError();
		cError.moduleName = "BatchSendBankTrans";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] arr)
	{
		BatchSendBankTrans tBatchSendBankTrans = new BatchSendBankTrans();
		tBatchSendBankTrans.run();
	}

}
