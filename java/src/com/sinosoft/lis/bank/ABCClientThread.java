package com.sinosoft.lis.bank;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class ABCClientThread extends Thread
{

	public ABCClientThread()
	{
	}

	@Override
	public void run()
	{
		DatagramSocket ds = null;
		try
		{
			ds = new DatagramSocket(10000);
		} catch (SocketException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 定义服务，监视端口上面的发送端口，注意不是send本身端口

		byte[] buf = new byte[1024];// 接受内容的大小，注意不要溢出

		DatagramPacket dp = new DatagramPacket(buf, 0, buf.length);// 定义一个接收的包

		try
		{
			ds.receive(dp);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// 将接受内容封装到包中

		String data = new String(dp.getData(), 0, dp.getLength());// 利用getData()方法取出内容

		System.out.println(data);// 打印内容

		ds.close();// 关闭资源
	}
}

/*
 * InputStream inputStream = socket.getInputStream();
 * System.out.println("发送报文JL" + inputStream.toString()); InputStreamReader
 * inputStreamReader = new InputStreamReader( inputStream); BufferedReader br =
 * new BufferedReader(inputStreamReader); try { //
 * 信息的格式：(login||logout||say),发送人,收发人,信息体 while (true) { String msg =
 * br.readLine(); System.out.println("返回报文JL" + msg); if (msg.equals(null)) {
 * break; }
 * 
 * switch (str[0]) { case "say": System.out.println(str[2] + " 对   " + str[1] +
 * " say: " + str[3]); break; default: break; }
 * 
 * } } catch (Exception e) { e.printStackTrace(); } } catch (IOException e1) {
 * e1.printStackTrace(); } }
 */

