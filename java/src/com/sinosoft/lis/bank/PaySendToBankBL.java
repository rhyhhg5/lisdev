package com.sinosoft.lis.bank;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.text.DecimalFormat;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 业务数据转换到银行系统,银行代付</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class PaySendToBankBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 返回数据的容器 */
  private VData mResult = new VData();
  /** 提交数据的容器 */
  private MMap map = new MMap();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /** 前台传入的公共变量 */
  private GlobalInput mGlobalInput = new GlobalInput();

  //业务数据
  /** 提数开始日期 */
  private String startDate = "";
  /** 提数结束日期 */
  private String endDate = "";
  /** 银行编码 */
  private String bankCode = "";
  /** 判断是否为大连 */
  private String bankUniteCode="";
  /** 总金额 */
  private double totalMoney = 0;
  /** 总笔数 */
  private int sumNum = 0;
  /** 批次号 */
  private String serialNo = "";
  /** 批次最大显示返回信息 */
  private String manage = "";
  
  private ExeSQL mExeSQL = new ExeSQL();;

  private LJAGetSet outLJAGetSet = new LJAGetSet();
  private LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();
  private LYBankLogSet outLYBankLogSet = new LYBankLogSet();

  public PaySendToBankBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代付
    if (mOperate.equals("PAYMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start PubSubmit BLS Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        mResult.clear();
        return false;
      }
      System.out.println("End PubSubmit BLS Submit...");
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()  {
    try {
      TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
      startDate = (String)tTransferData.getValueByName("startDate");
      endDate = (String)tTransferData.getValueByName("endDate");
      bankCode = (String)tTransferData.getValueByName("bankCode");
      bankUniteCode = (String)tTransferData.getValueByName("bankCode");
      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }
  
  private LJAGetSet getLJAGetSet(String startDate, String endDate) {
      LJAGetSet tLJAGetSet = new LJAGetSet();

      //获取银行信息，校验是否是银联
      LDBankDB tLDBankDB = new LDBankDB();
      tLDBankDB.setBankCode(bankCode);
      if (!tLDBankDB.getInfo()) {
        CError.buildErr(this, "获取银行信息（LDBank）失败");
        return null;
      }

      //普通银行
      if (tLDBankDB.getBankUniteFlag()==null || tLDBankDB.getBankUniteFlag().equals("0")) {
        tLJAGetSet = getLJAGetByPaydate(startDate, endDate, bankCode);
      }
      //银联
      else if (tLDBankDB.getBankUniteFlag().equals("1")){
        LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
        tLDBankUniteDB.setBankUniteCode(bankCode);
        LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.query();

        if (tLDBankUniteSet.size()==0) {
          CError.buildErr(this, "获取银联相关银行信息（LDBankUnite）失败");
          return null;
        }

        for (int i=0; i<tLDBankUniteSet.size(); i++) {
          tLJAGetSet.add(getLJAGetByPaydate(startDate, endDate, tLDBankUniteSet.get(i+1).getBankCode()));
        }
      }

      return tLJAGetSet;
    }


  /**
   * 获取交费日期在设置的日期区间内的总应付表记录
   * @param startDate
   * @param endDate
   * @return
   */
  private LJAGetSet getLJAGetByPaydate(String startDate, String endDate,String bankCode) {
    String tSql = "";
    String tSql2=" and (select count(*) from ljaget a,ljagetclaim b,lccontplanrisk c,lcgrpcont d " +
            "where a.actugetno=b.actugetno and b.grpcontno=c.grpcontno " +
            "and c.riskwrapcode in ('BCGS08','BCGS09','BCGS10') and d.cardflag = '4' and a.actugetno = ljaget.actugetno)+" +
            "(select count(*) from ljaget a,ljagetclaim b,lccontplanrisk c,lbgrpcont d " +
            "where a.actugetno=b.actugetno and b.grpcontno=c.grpcontno " +
            "and c.riskwrapcode in ('BCGS08','BCGS09','BCGS10') and d.cardflag = '4' and a.actugetno = ljaget.actugetno)+" +
            "(select count(*) from ljaget a,ljsgetendorse b,lccontplanrisk c,lcgrpcont d " +
            "where a.otherno=b.endorsementno and b.grpcontno=c.grpcontno " +
            "and c.riskwrapcode in ('BCGS08','BCGS09','BCGS10') and d.cardflag = '4' and a.actugetno = ljaget.actugetno)>0";
    
    String tSql3=" and (select count(*) from ljaget a,ljagetclaim b,lccontplanrisk c,lcgrpcont d " +
    "where a.actugetno=b.actugetno and b.grpcontno=c.grpcontno " +
    "and c.riskwrapcode in ('BCGS08','BCGS09','BCGS10') and d.cardflag = '4' and a.actugetno = ljaget.actugetno)+" +
    "(select count(*) from ljaget a,ljagetclaim b,lccontplanrisk c,lbgrpcont d " +
    "where a.actugetno=b.actugetno and b.grpcontno=c.grpcontno " +
    "and c.riskwrapcode in ('BCGS08','BCGS09','BCGS10') and d.cardflag = '4' and a.actugetno = ljaget.actugetno)+" +
    "(select count(*) from ljaget a,ljsgetendorse b,lccontplanrisk c,lcgrpcont d " +
    "where a.otherno=b.endorsementno and b.grpcontno=c.grpcontno " +
    "and c.riskwrapcode in ('BCGS08','BCGS09','BCGS10') and d.cardflag = '4' and a.actugetno = ljaget.actugetno)=0";
    
    //#2191 社保通查勘费报销问题：按原始需求，仅支持现金结算
    if (startDate.equals("")) {
      tSql = "select * from LJAGet where "
//           + " and StartGetDate <= '" + endDate + "'"
           + " ShouldDate <= '" + endDate + "'"
           + " and BankCode = '" + bankCode + "'"
           + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
           + " and PayMode='4'"
           + " and EnterAccDate is null and ConfDate is null"
           + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
           + " and (CanSendBank is null or  CanSendBank = '0')"
           + " and (BankAccNo is not null) and sumgetmoney<>0"
      	   + " and othernotype <>'23'";
    }
    else {
      tSql = "select * from LJAGet where "
//           + " and StartGetDate <= '" + endDate + "'"
           + " ShouldDate >= '" + startDate + "'"
           + " and ShouldDate <= '" + endDate + "'"
           + " and BankCode = '" + bankCode + "'"
           + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
           + " and PayMode='4'"
           + " and EnterAccDate is null and ConfDate is null"
           + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
           + " and (CanSendBank is null or  CanSendBank = '0')"
           + " and (BankAccNo is not null) and sumgetmoney<>0"
      	   + " and othernotype <>'23'";
    }
    
    /*if (bankUniteCode.equals("999100") || bankUniteCode.equals("489102")) {
        tSql = tSql + tSql2;
    } else if(bankCode.length()>2 && bankCode.substring(1, 4).equals("8691")) {
        tSql = tSql + tSql3;
    }*/
    
    System.out.println(tSql);

    LJAGetDB tLJAGetDB = new LJAGetDB();
    LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(tSql);

    if (tLJAGetDB.mErrors.getErrorCount()>0) return null;
    else return tLJAGetSet;
  }

  /**
   * 校验银行授权
   * @param tLJAGetSet
   */
  private LJAGetSet verifyBankAuth(LJAGetSet tLJAGetSet) {
    int i;
    LJAGetSet bankAuthLJAGetSet = new LJAGetSet();

    for (i=0; i<tLJAGetSet.size(); i++) {
      LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i+1);

      LCBankAuthSchema tLCBankAuthSchema = new LCBankAuthSchema();
      tLCBankAuthSchema.setPolNo(tLJAGetSchema.getOtherNo());  //保单号
      tLCBankAuthSchema.setPayGetFlag("0");                    //收付费标志(0---表示收费)
      tLCBankAuthSchema.setPayValidFlag("1");                  //正常交费/领取代付开通标志(1 -- 开通银行代付)
      tLCBankAuthSchema.setBankCode(bankCode);                 //匹配银行编码

      //找到需要处理的数据
      if (tLCBankAuthSchema.getDB().query().size() > 0) {
        bankAuthLJAGetSet.add(tLJAGetSchema);
      }
    }

    if (bankAuthLJAGetSet.size() > 0) return bankAuthLJAGetSet;
    else return null;
  }

  /**
   * 获取银行账号信息
   * @param tLJAGetSchema
   * @return
   */
  private LCBankAccSchema getBankAcc(LJAGetSchema tLJAGetSchema) {
    try {
      LCBankAccSchema tLCBankAccSchema = new LCBankAccSchema();
      tLCBankAccSchema.setBankCode(bankCode);
      tLCBankAccSchema.setBankAccNo(tLJAGetSchema.getBankAccNo());

      tLCBankAccSchema.setSchema(tLCBankAccSchema.getDB().query().get(1));

      return tLCBankAccSchema;
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * 生成送银行表数据
   * @param tLJAGetSet
   * @return
   */
  private LYSendToBankSet getSendToBank(LJAGetSet tLJAGetSet) {
    //总金额
    double dTotalMoney = 0;
    serialNo = PubFun1.CreateMaxNo("1", 20);
    LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();

    for (int i=0; i<tLJAGetSet.size(); i++) {
      LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

      //获取银行账号信息
//      LCBankAccSchema tLCBankAccSchema = getBankAcc(tLJAGetSchema);
//      if (tLCBankAccSchema == null) return null;

      //生成送银行表数据
      LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
      tLYSendToBankSchema.setSerialNo(serialNo);
      tLYSendToBankSchema.setDealType("F");
      tLYSendToBankSchema.setPayCode(tLJAGetSchema.getActuGetNo());
      tLYSendToBankSchema.setBankCode(tLJAGetSchema.getBankCode());
//      tLYSendToBankSchema.setAccType(tLCBankAccSchema.getAccType());
      tLYSendToBankSchema.setAccName(tLJAGetSchema.getAccName());
      tLYSendToBankSchema.setAccNo(tLJAGetSchema.getBankAccNo());
      tLYSendToBankSchema.setPolNo(tLJAGetSchema.getOtherNo());
      //代付没有首续期之分，为了满足商业银行需要，先全部设置成0
      tLYSendToBankSchema.setNoType("0");
      tLYSendToBankSchema.setComCode(tLJAGetSchema.getManageCom());
      tLYSendToBankSchema.setAgentCode(tLJAGetSchema.getAgentCode());
      tLYSendToBankSchema.setPayMoney(tLJAGetSchema.getSumGetMoney());
      tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
      tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
      tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
      if(!StrTool.cTrim(tLJAGetSchema.getDrawerID()).equals("")){
      tLYSendToBankSchema.setIDType("0");
      tLYSendToBankSchema.setIDNo(tLJAGetSchema.getDrawerID());
      }
      if(!StrTool.cTrim(tLJAGetSchema.getDrawer()).equals("")){
      tLYSendToBankSchema.setName(tLJAGetSchema.getDrawer());
      }
      //    银联需要银联表银行代码，添加到bankunitecode
      SSRS tSSRS=new ExeSQL().execSQL("select unitebankcode from ldbankunite where bankcode = '"+tLJAGetSchema.getBankCode()+"'");
      if(tSSRS.getMaxRow()>0){
          tLYSendToBankSchema.setBankUniteCode((tSSRS.GetText(1,1)));
      }
      tLYSendToBankSet.add(tLYSendToBankSchema);

      //累加总金额和总数量
      dTotalMoney = dTotalMoney + tLJAGetSchema.getSumGetMoney();
      //转换精度
      dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
      sumNum = sumNum + 1;
    }
    totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));

    return tLYSendToBankSet;
  }

  /**
   * 修改实付表银行在途标志
   * @param tLJAGetSet
   * @return
   */
  private LJAGetSet modifyBankFlag(LJAGetSet tLJAGetSet) {
    int i;

    for (i=0; i<tLJAGetSet.size(); i++) {
      LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i+1);

      tLJAGetSchema.setBankOnTheWayFlag("1");
      tLJAGetSchema.setSendBankCount(tLJAGetSchema.getSendBankCount() + 1);
      tLJAGetSet.set(i+1, tLJAGetSchema);
    }

    return tLJAGetSet;
  }

  /**
   * 生成银行日志表数据
   * @return
   */
  private LYBankLogSchema getBankLog() {
    LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

    tLYBankLogSchema.setSerialNo(serialNo);
    tLYBankLogSchema.setBankCode(bankCode);
    tLYBankLogSchema.setLogType("F");
    tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setTotalMoney(String.valueOf(totalMoney));
    tLYBankLogSchema.setTotalNum(sumNum);
    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
    tLYBankLogSchema.setComCode(mGlobalInput.ComCode);

    return tLYBankLogSchema;
  }


  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      //银行代付
      if (mOperate.equals("PAYMONEY")) {
        //总实付表处理（获取应付日期在设置的日期区间内的记录；获取银行在途标志为N的记录）
          
        LJAGetSet tLJAGetSet = getLJAGetSet(startDate, endDate);
        if(!CheckLJAGet(tLJAGetSet)){
            return false;
        }
        // zcx 增加对批次大小的限制
        CheckSetSize(tLJAGetSet);
        
        if (tLJAGetSet == null) throw new NullPointerException("总实付表处理失败！");
        if (tLJAGetSet.size() == 0) throw new NullPointerException("总实付表无数据！");
        //System.out.println("tLJAGetSet:" + tLJAGetSet.encode());
        System.out.println("---End getLJAGetByPaydate---");
        //校验银行授权
//        tLJAGetSet = verifyBankAuth(tLJAGetSet);
//        if (tLJAGetSet == null) throw new NullPointerException("校验银行授权处理失败！无数据！");
//        System.out.println("---End verifyBankAuth---");

        //生成送银行表数据
        LYSendToBankSet tLYSendToBankSet = getSendToBank(tLJAGetSet);
        if (tLYSendToBankSet == null) throw new Exception("生成送银行表数据失败！");
        //System.out.println("tLYSendToBankSet:" + tLYSendToBankSet.encode());
        System.out.println("---End getSendToBank---");

        //修改实付表银行在途标志
        tLJAGetSet = modifyBankFlag(tLJAGetSet);
        System.out.println("---End modifyBankFlag---");

        //生成银行日志表数据
        LYBankLogSchema tLYBankLogSchema = getBankLog();
        //System.out.println("tLYBankLogSchema:" + tLYBankLogSchema.encode());
        System.out.println("---End getBankLog---");

        outLJAGetSet.set(tLJAGetSet);
        outLYSendToBankSet.set(tLYSendToBankSet);
        outLYBankLogSet.add(tLYBankLogSchema);
      }
    }
    catch(Exception e) {
      // @@错误处理
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      map.put(outLJAGetSet, "UPDATE");
      map.put(outLYSendToBankSet, "INSERT");
      map.put(outLYBankLogSet, "INSERT");

      mInputData.clear();
      mInputData.add(map);
    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  public boolean CheckLJAGet(LJAGetSet tLJAGetSet){
  	LJAGetSet tempLJAGetSet=new LJAGetSet();
    int flag=0;
    boolean Bankerror=false;
    String BugReason="";
    SSRS tSSRS = new SSRS();
    for (int i = 1; i <= tLJAGetSet.size(); i++) {
        if(tLJAGetSet.size()>0){
            LJAGetSchema tLJAGetSchema=tLJAGetSet.get(i);
            String Accno=tLJAGetSet.get(i).getBankAccNo();
            String Bankcode=tLJAGetSet.get(i).getBankCode();
            tSSRS = mExeSQL.execSQL("select 1 from ldcode1 where codetype='bankaccnumcheck' and code='"+Bankcode+"' and code1='1'");
            if(tSSRS.getMaxRow()>0){
            	if(!IsNumericAcc(Accno)){
                    tempLJAGetSet.add(tLJAGetSchema);
                    tLJAGetSchema.setCanSendBank("1");
                    this.map.put(tLJAGetSchema, "UPDATE");
                }
            }
            else if (!"809101".equals(Bankcode)){
            	System.out.println(Bankcode);
	            if(!IsNumeric(Accno)){
	                tempLJAGetSet.add(tLJAGetSchema);
	                tLJAGetSchema.setCanSendBank("1");
	                this.map.put(tLJAGetSchema, "UPDATE");
	            }
            }
//            if(StrTool.cTrim(tLJAGetSet.get(i).getBankCode()).equals("0412")){
//                Bankerror=true;
//                if(StrTool.cTrim(tLJAGetSet.get(i).getDrawer()).equals("")||
//                    StrTool.cTrim(tLJAGetSet.get(i).getDrawerID()).equals("")    ){
//                   if(StrTool.cTrim(tLJAGetSet.get(i).getOtherNoType()).equals("10")){
//                   BugReason=BugReason+"保全号码:"+tLJAGetSet.get(i).getOtherNo()+"<br>";
//                   }else{
//                    BugReason=BugReason+"理赔号码:"+tLJAGetSet.get(i).getOtherNo()+"<br>";
//                   }
//               }else{
//                   flag++;
//               }
//            }
            if("9912".equals(bankCode))
            {
                System.out.println("---------天津银联银行账号校验！----------");
                if( 30<StrTool.cTrim(tLJAGetSchema.getBankAccNo()).length())
                {
                    tempLJAGetSet.add(tLJAGetSchema);
                    tLJAGetSchema.setCanSendBank("1");
                    this.map.put(tLJAGetSchema, "UPDATE");
                }
            }
            else
            {
            LDCode1DB tLDCode1DB=new LDCode1DB();
            String sql="select * from ldcode1 where codetype='bankaccnocheck' and code='"+tLJAGetSchema.getBankCode()+"'";
            LDCode1Set tLDCode1Set= tLDCode1DB.executeQuery(sql);
            if(tLDCode1Set!=null && tLDCode1Set.size()>0){
                LDCode1Schema tLDCode1Schema=tLDCode1Set.get(1);
                if(Integer.parseInt(StrTool.cTrim(tLDCode1Schema.getCode1()))<StrTool.cTrim(tLJAGetSchema.getBankAccNo()).length()){
                    tempLJAGetSet.add(tLJAGetSchema);
                    tLJAGetSchema.setCanSendBank("1");
                    this.map.put(tLJAGetSchema, "UPDATE");
                }
            }
            }
        }
    }
    if(flag!=tLJAGetSet.size()&&Bankerror){
        CError.buildErr(this, BugReason+"没有提供领取人姓名或领取人证件号码,不能进行提盘!");
        return false;
    }
    String failInfo="";
    for (int i = 1; i <= tempLJAGetSet.size(); i++) {
        if(!failInfo.equals("")){
            failInfo+="、"+tempLJAGetSet.get(i).getOtherNo();
        }else{
            failInfo=tempLJAGetSet.get(i).getOtherNo();
        }
        tLJAGetSet.remove(tempLJAGetSet.get(i));
    }
    if(!failInfo.equals("")){
        failInfo="业务号"+failInfo+"的帐号位数过长，已被锁定";
    }
    this.mResult.add(failInfo);
    return true;
  }
  //数字判断2
  public boolean IsNumeric(String s)
  {
      for (int i=0; i<s.length(); i++) {
          char c = s.charAt(i);
          if (c!=' ') {
              if (c<'0' || '9'<c) {
                  return false ;
              }
          }
  }
  return true;
}
  public boolean IsNumericAcc(String s)
  {
      for (int i=0; i<s.length(); i++) {
          char c = s.charAt(i);
          if (c!=' ') {
              if (c<'0' || '9'<c) {
            	  if(c=='-'){
            		  continue;
            	  }
                  return false ;
              }
          }
  }
  return true;
}
  
	private void CheckSetSize(LJAGetSet tLJAGetSet) {
		String sql = "select code from ldcode where codetype='sendbankcount'";
		ExeSQL tExeSQL = new ExeSQL();
		String count = tExeSQL.getOneValue(sql);
		if (count == null || count.equals("") || count.equals("0")) {
			return;
		}
		int maxCount;
		try {
			maxCount = Integer.parseInt(count);
		} catch (Exception ex) {
			return;
		}
		if (tLJAGetSet.size() > maxCount) {
			this.manage = "待发送数据已超过" + maxCount + "笔，本次未全部提取，请再次进行提取";
			tLJAGetSet.removeRange(maxCount + 1, tLJAGetSet.size());
		}
		return;
	}

	public String getManage() {
		return this.manage;
	}

	public String getSerialNo() {
		return this.serialNo;
	}
	
  public static void main(String[] args) {
    PaySendToBankBL paySendToBankBL1 = new PaySendToBankBL();
    LJAGetSet tLJAGetSet = new LJAGetSet();
    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    tLJAGetSchema.setBankCode("809101");
    tLJAGetSet.add(tLJAGetSchema);
    paySendToBankBL1.CheckLJAGet(tLJAGetSet);
  }
}
