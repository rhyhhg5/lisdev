package com.sinosoft.lis.bank;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LDMedicalComDB;
import com.sinosoft.lis.db.LYSendToConfirmDB;
import com.sinosoft.lis.db.LYSendToSettleDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBankAccSchema;
import com.sinosoft.lis.schema.LCBankAuthSchema;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.schema.LYSendToConfirmSchema;
import com.sinosoft.lis.schema.LYSendToSettleSchema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToConfirmSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 业务数据转换到银行系统，银行代收</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class SendToSettleBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 返回数据的容器 */
  private VData mResult = new VData();
  /** 提交数据的容器 */
  private MMap map = new MMap();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /** 前台传入的公共变量 */
  private GlobalInput mGlobalInput = new GlobalInput();

  //业务数据
  /** 提数开始日期 */
  private String startDate = "";
  /** 提数结束日期 */
  private String endDate = "";
  /** 银行编码 */
  private String medicalCode = "";
  /** 提取数据范围类型，如果值是：ALLXQ，则只提取续期数据 */
  private String typeFlag = "";
  /** 总金额 */
  private double totalMoney = 0;
  /** 总笔数 */
  private int sumNum = 0;

  private  String flag="0";
  /** 批次号 */
  private String serialNo = "";
  /** 批次最大显示返回信息 */
	private String manage = "";

  private LYSendToConfirmSet outLYSendToConfirmSet = new LYSendToConfirmSet();
  private LYSendToSettleSet outLYSendToSettleSet = new LYSendToSettleSet();
  private LYBankLogSet outLYBankLogSet = new LYBankLogSet();
//  private LDBankSchema outLDBankSchema = new LDBankSchema();

  public SendToSettleBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代收
    if (mOperate.equals("GETMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start PubSubmit BLS Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        mResult.clear();
        return false;
      }
      //调用处理类发送给医保
      TransferData transferData1 = new TransferData();
      transferData1.setNameAndValue("DealType", "D");
      VData inVData = new VData();
      inVData.add(outLYSendToSettleSet);
	  inVData.add(transferData1);
	  inVData.add(mGlobalInput);
      MedicalDealSTBL tMedicalDealSTBL = new MedicalDealSTBL();
      if(!tMedicalDealSTBL.submitData(inVData, "WRITE")){
    	// @@错误处理
          this.mErrors.copyAllErrors(tMedicalDealSTBL.mErrors);
          mResult.clear();
          mResult.add(mErrors.getFirstError());
          return false;
      }else{
    	  mResult = tMedicalDealSTBL.getResult();
      }
      System.out.println("End PubSubmit BLS Submit...");
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
      medicalCode = (String)tTransferData.getValueByName("MedicalCode");
      typeFlag = (String)tTransferData.getValueByName("typeFlag");

      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }

  /**
   * 获取交费日期在设置的日期区间内的应收总表记录
   * @param startDate
   * @param endDate
   * @return
   */
  private LYSendToConfirmSet getLYSendToConfirm(String bankCode) {


    //不指定开始时间
      //规则：最早应收日期<=结束时间，最晚交费日期>结束时间
      //     银行编码匹配，机构编码向下匹配，不在途，有账号
      //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
      String sql = "select lrb.* from lysendToConfirm lrb "
					+" left join lccont lcc   on lrb.polno = lcc.prtno "
					+"  where lcc.contno is not null  "
					+"   and lrb.Medicalcode = '"+bankCode+"'  "
					+"   and current date > lcc.CustomGetPolDate + 9 day  "
					+"   and lcc.CustomGetPolDate is not null  "
					+"   and lrb.comcode like '"+mGlobalInput.ComCode+"%' "
					+"   and lrb.Dealtype = 'C'  "
					+"   and lrb.ConfirmState = '1'  "
					+"   and lrb.succflag = '0'"
					+"   and not exists (select 1 from lysendtosettle where paycode = lrb.paycode and  succflag = '0')"
					+" with ur";
							
    
    System.out.println(sql);

    LYSendToConfirmDB tLYSendToConfirmDB = new LYSendToConfirmDB();
    LYSendToConfirmSet tLYSendToConfirmSet = tLYSendToConfirmDB.executeQuery(sql);
    return tLYSendToConfirmSet;
  }

  private LYSendToConfirmSet getLYSendToConfirmSet() {
	  LYSendToConfirmSet tLYSendToConfirmSet = new LYSendToConfirmSet();

    //获取银行信息，校验是否是银联
    LDMedicalComDB tLDMedicalDB = new LDMedicalComDB();
    tLDMedicalDB.setMedicalComCode(medicalCode);
    if (!tLDMedicalDB.getInfo()) {
      CError.buildErr(this, "获取银行信息（LDBank）失败");
      return null;
    }

    //普通银行
    tLYSendToConfirmSet = getLYSendToConfirm(medicalCode);

    return tLYSendToConfirmSet;
  }

  /**
   * 校验银行授权
   * @param tLJSPaySet
   */
  private LJSPaySet verifyBankAuth(LJSPaySet tLJSPaySet) {
    int i;
    LJSPaySet bankAuthLJSPaySet = new LJSPaySet();

    for (i=0; i<tLJSPaySet.size(); i++) {
      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i+1);

      LCBankAuthSchema tLCBankAuthSchema = new LCBankAuthSchema();
      tLCBankAuthSchema.setPolNo(tLJSPaySchema.getOtherNo());  //保单号
      tLCBankAuthSchema.setPayGetFlag("0");                    //收付费标志(0---表示收费)
      tLCBankAuthSchema.setPayValidFlag("1");                  //正常交费/领取代收开通标志(1 -- 开通银行代收)
      tLCBankAuthSchema.setBankCode(medicalCode);                 //匹配银行编码

      //找到需要处理的数据
      if (tLCBankAuthSchema.getDB().query().size() > 0) {
        bankAuthLJSPaySet.add(tLJSPaySchema);
      }
    }

    if (bankAuthLJSPaySet.size() > 0) return bankAuthLJSPaySet;
    else return null;
  }

  /**
   * 获取银行账号信息
   * @param tLJSPaySchema
   * @return
   */
  private LCBankAccSchema getBankAcc(LJSPaySchema tLJSPaySchema) {
    try {
      LCBankAccSchema tLCBankAccSchema = new LCBankAccSchema();
      tLCBankAccSchema.setBankCode(medicalCode);
      tLCBankAccSchema.setBankAccNo(tLJSPaySchema.getBankAccNo());

      tLCBankAccSchema.setSchema(tLCBankAccSchema.getDB().query().get(1));

      return tLCBankAccSchema;
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * 生成送银行表数据
   * @param tLYSendToConfirmSet
   * @return
   */
  private LYSendToSettleSet getSendToBank(LYSendToConfirmSet tLYSendToConfirmSet) {
    //总金额
    double dTotalMoney = 0;
    //生成批次号，要在循环外生成
    serialNo = PubFun1.CreateMaxNo("1", 20);
    LYSendToSettleSet tLYSendToSettleSet = new LYSendToSettleSet();

    for (int i=0; i<tLYSendToConfirmSet.size(); i++) {
      LYSendToConfirmSchema tLYSendToConfirmSchema =  tLYSendToConfirmSet.get(i + 1);

      //获取银行账号信息，账户信息不一定存在于账户表中了
      //校验该笔数据是否已经提盘
//        LYSendToSettleDB OldLYSendToSettleDB = new LYSendToSettleDB();
//        LYSendToSettleSet OldLYSendToSettleSet = new LYSendToSettleSet();
//        OldLYSendToSettleDB.setPayCode(tLYSendToConfirmSchema.getPayCode());
//        OldLYSendToSettleDB.setMedicalCode(tLYSendToConfirmSchema.getMedicalCode());
//        OldLYSendToSettleDB.setAccName(tLYSendToConfirmSchema.getAccName());
//        OldLYSendToSettleDB.setAccNo(tLYSendToConfirmSchema.getAccNo());
//        OldLYSendToSettleSet=OldLYSendToSettleDB.query();
//        if(OldLYSendToSettleSet.size()>0){
//            flag="1";
//        }

      //生成送银行表数据
      LYSendToSettleSchema tLYSendToSettleSchema = new LYSendToSettleSchema();
      //设置统一的批次号
      tLYSendToSettleSchema.setSerialNo(serialNo);
      //收费标记
      tLYSendToSettleSchema.setDealType("D");
      tLYSendToSettleSchema.setPayCode(tLYSendToConfirmSchema.getPayCode());
      tLYSendToSettleSchema.setPayType(tLYSendToConfirmSchema.getPayType());
      tLYSendToSettleSchema.setMedicalCode(tLYSendToConfirmSchema.getMedicalCode());
//      tLYSendToBankSchema.setAccType(tLCBankAccSchema.getAccType());
      tLYSendToSettleSchema.setAccName(tLYSendToConfirmSchema.getAccName());
      tLYSendToSettleSchema.setAccNo(tLYSendToConfirmSchema.getAccNo());

      //因为改为前台录入财务数据，保单表中不一定有数据，所以不再从中取信息
      tLYSendToSettleSchema.setNoType(tLYSendToConfirmSchema.getNoType());
      tLYSendToSettleSchema.setComCode(tLYSendToConfirmSchema.getComCode());
      tLYSendToSettleSchema.setAgentCode(tLYSendToConfirmSchema.getAgentCode());
      tLYSendToSettleSchema.setPayMoney(tLYSendToConfirmSchema.getPayMoney());
      tLYSendToSettleSchema.setSendDate(PubFun.getCurrentDate());
      tLYSendToSettleSchema.setSendTime(PubFun.getCurrentTime());
      tLYSendToSettleSchema.setDoType("1");
      tLYSendToSettleSchema.setPolNo(tLYSendToConfirmSchema.getPolNo());
      //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
      tLYSendToSettleSchema.setRemark(mGlobalInput.Operator);
      tLYSendToSettleSchema.setOperator(mGlobalInput.Operator);
      tLYSendToSettleSchema.setModifyDate(PubFun.getCurrentDate());
      tLYSendToSettleSchema.setModifyTime(PubFun.getCurrentTime());
      tLYSendToSettleSchema.setMakeDate(PubFun.getCurrentDate());
      tLYSendToSettleSchema.setMakeTime(PubFun.getCurrentTime());
     // String IDNo=new ExeSQL().getOneValue("select idno from ldperson where customerno='"+tLJSPaySchema.getAppntNo()+"'");
      tLYSendToSettleSchema.setIDNo(tLYSendToConfirmSchema.getIDNo());
      tLYSendToSettleSchema.setIDType(tLYSendToConfirmSchema.getIDType());
      tLYSendToSettleSchema.setGetPolDate(tLYSendToConfirmSchema.getGetPolDate());
      tLYSendToSettleSchema.setRiskCode(tLYSendToConfirmSchema.getRiskCode());
      tLYSendToSettleSet.add(tLYSendToSettleSchema);

      //累加总金额和总数量
      dTotalMoney = dTotalMoney + tLYSendToSettleSchema.getPayMoney();
      //转换精度
      dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
      sumNum = sumNum + 1;
    }
    totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));

    return tLYSendToSettleSet;
  }

  /**
   * 修改应收表银行在途标志,记录发送银行次数
   * @param tLYSendToConfirmSet
   * @return
   */
  private LYSendToConfirmSet modifyBankFlag(LYSendToConfirmSet tLYSendToConfirmSet) {
    for (int i=0; i<tLYSendToConfirmSet.size(); i++) {
    	LYSendToConfirmSchema tLYSendToConfirmSchema = tLYSendToConfirmSet.get(i+1);
    	tLYSendToConfirmSchema.setRemark("1");
      //记录发送银行次数
      tLYSendToConfirmSet.set(i+1, tLYSendToConfirmSchema);
    }

    return tLYSendToConfirmSet;
  }

  /**
   * 生成银行日志表数据
   * @return
   */
  private LYBankLogSchema getBankLog() {
    LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

    tLYBankLogSchema.setSerialNo(serialNo);
    tLYBankLogSchema.setBankCode(medicalCode);
    tLYBankLogSchema.setLogType("D");
    tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
//    tLYBankLogSchema.setTotalMoney(157574.57f);
//    String t = (new DecimalFormat("0.000000")).format(tLYBankLogSchema.getTotalMoney());
    tLYBankLogSchema.setTotalMoney(totalMoney);
    tLYBankLogSchema.setTotalNum(sumNum);
    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
    tLYBankLogSchema.setComCode(mGlobalInput.ComCode);

    return tLYBankLogSchema;
  }

  /**
   * 更新银行表独立序号
   * @return
   */
  private LDBankSchema getLDBank() {
    LDBankDB tLDBankDB = new LDBankDB();
    tLDBankDB.setBankCode(medicalCode);
    if (!tLDBankDB.getInfo()) {
      CError.buildErr(this, "获取银行信息（LDBank）失败");
      return null;
    }

    if (tLDBankDB.getSeqNo()==null || tLDBankDB.getSeqNo().equals("")) {
      tLDBankDB.setSeqNo("0");
    }
    else {
      tLDBankDB.setSeqNo(String.valueOf(Integer.parseInt(tLDBankDB.getSeqNo()) + 1));
    }

    return tLDBankDB.getSchema();
  }


  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      //银行代收（应收总表LJSPay）

      if (mOperate.equals("GETMONEY")) {

        //总应收表处理（获取交费日期在设置的日期区间内的记录；获取银行在途标志为N的记录）
        LYSendToConfirmSet tLYSendToConfirmSet = getLYSendToConfirmSet();
        
     // zcx 增加对批次大小的限制
        checkLYSendToConfirmSize(tLYSendToConfirmSet);
        
        if (tLYSendToConfirmSet == null) throw new NullPointerException("总应收表处理失败！");
        if (tLYSendToConfirmSet.size() == 0) throw new NullPointerException("总应收表无数据！");
        System.out.println("---End getLJSPayByPaydate---");

        //生成送银行表数据
        LYSendToSettleSet tLYSendToSettleSet = getSendToBank(tLYSendToConfirmSet);
        if(flag.equals("1")){
            CError.buildErr(this, "接收数据失败");
           return false;
        }
        if (tLYSendToConfirmSet == null) throw new Exception("生成送银行表数据失败！");
        System.out.println("---End getSendToBank---");

        //修改应收表银行在途标志,记录发送银行次数
        tLYSendToConfirmSet = modifyBankFlag(tLYSendToConfirmSet);
        System.out.println("---End modifyBankFlag---");

        //生成银行日志表数据
        LYBankLogSchema tLYBankLogSchema = getBankLog();
        System.out.println("---End getBankLog---");

        outLYSendToConfirmSet.set(tLYSendToConfirmSet);
        outLYSendToSettleSet.set(tLYSendToSettleSet);
        outLYBankLogSet.add(tLYBankLogSchema);
      }
    }
    catch(Exception e) {
      // @@错误处理
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      map.put(outLYSendToConfirmSet, "UPDATE");
      map.put(outLYSendToSettleSet, "INSERT");
      map.put(outLYBankLogSet, "INSERT");

      mInputData.clear();
      mInputData.add(map);
    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  public boolean CheckLJSpay(LJSPaySet tLJSPaySet){
    LJSPaySet tempLJSPaySet=new LJSPaySet();
    for (int i = 1; i <= tLJSPaySet.size(); i++) {
        if(tLJSPaySet.size()>0){
            LJSPaySchema tLJSPaySchema=tLJSPaySet.get(i);
            String Accno=tLJSPaySchema.getBankAccNo();
            if(!IsNumeric(Accno)){
                tempLJSPaySet.add(tLJSPaySchema);
                tLJSPaySchema.setCanSendBank("1");
                this.map.put(tLJSPaySchema, "UPDATE");
            }
            LDCode1DB tLDCode1DB=new LDCode1DB();
            String sql="select * from ldcode1 where codetype='bankaccnocheck' and code='"+tLJSPaySchema.getBankCode()+"'";
            LDCode1Set tLDCode1Set= tLDCode1DB.executeQuery(sql);
            if(tLDCode1Set!=null && tLDCode1Set.size()>0){
                LDCode1Schema tLDCode1Schema=tLDCode1Set.get(1);
                if(Integer.parseInt(StrTool.cTrim(tLDCode1Schema.getCode1()))<StrTool.cTrim(tLJSPaySchema.getBankAccNo()).length()){
                    tempLJSPaySet.add(tLJSPaySchema);
                    tLJSPaySchema.setCanSendBank("1");
                    this.map.put(tLJSPaySchema, "UPDATE");
                }
            }
        }
    }
    String failInfo="";
    for (int i = 1; i <= tempLJSPaySet.size(); i++) {
        if(!failInfo.equals("")){
            failInfo+="、"+tempLJSPaySet.get(i).getOtherNo();
        }else{
            failInfo=tempLJSPaySet.get(i).getOtherNo();
        }
        tLJSPaySet.remove(tempLJSPaySet.get(i));
    }
    if(!failInfo.equals("")){
        failInfo="业务号"+failInfo+"的帐号位数过长，已被锁定";
    }
    this.mResult.add(failInfo);
    return true;
  }

  //数字判断2
  public boolean IsNumeric(String s)
  {
      for (int i=0; i<s.length(); i++) {
          char c = s.charAt(i);
          if (c!=' ') {
              if (c<'0' || '9'<c) {
                  return false ;
              }
          }
  }
  return true;
}
  
	private void checkLYSendToConfirmSize(LYSendToConfirmSet tLYSendToConfirmSet){
		String sql = "select code from ldcode where codetype='sendbankcount'";
		ExeSQL tExeSQL = new ExeSQL();
		String count = tExeSQL.getOneValue(sql);
		if (count == null || count.equals("") || count.equals("0")) {
			return;
		}
		int maxCount;
		try {
			maxCount = Integer.parseInt(count);
		} catch (Exception ex) {
			return;
		}
		if (tLYSendToConfirmSet.size() > maxCount) {
			this.manage = "待发送数据已超过" + maxCount + "笔，本次未全部提取，请再次进行提取";
			tLYSendToConfirmSet.removeRange(maxCount + 1, tLYSendToConfirmSet.size());
		}
		return;
	}
  
	public String getManage() {
		return this.manage;
	}

	public String getSerialNo() {
		return this.serialNo;
	}
  
  public static void main(String[] args) {
	  LJSPaySchema tLJSPaySchmea = new LJSPaySchema();
	  tLJSPaySchmea.setGetNoticeNo("1");
	  LJSPaySchema tLJSPaySchmea2 = new LJSPaySchema();
	  tLJSPaySchmea.setGetNoticeNo("2");
	  LJSPaySchema tLJSPaySchmea3 = new LJSPaySchema();
	  tLJSPaySchmea.setGetNoticeNo("3");
	  LJSPaySet tLJSPay = new LJSPaySet();
	  tLJSPay.add(tLJSPaySchmea); 
	  tLJSPay.add(tLJSPaySchmea2);
	  tLJSPay.add(tLJSPaySchmea3);
	  
	 SendToSettleBL tPaySendToBankBL = new SendToSettleBL();
	 System.out.println(tLJSPay.size());
	 System.out.println(tLJSPay.get(tLJSPay.size()).getGetNoticeNo());
  }
}
