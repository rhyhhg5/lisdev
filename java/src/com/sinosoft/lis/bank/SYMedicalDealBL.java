package com.sinosoft.lis.bank;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToSettleSchema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 业务数据转换到银行系统，银行代收</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class SYMedicalDealBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 返回数据的容器 */
  private VData mResult = new VData();
  /** 提交数据的容器 */
  private MMap map = new MMap();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /** 前台传入的公共变量 */
  private GlobalInput mGlobalInput = new GlobalInput();

  //业务数据
  /** 提数开始日期 */
  private String startDate = "";
  /** 提数结束日期 */
  private String endDate = "";
  /** 银行编码 */
  private String medicalCode = "";
  /** 提取数据范围类型，如果值是：ALLXQ，则只提取续期数据 */
  private String typeFlag = "";
  /** 总金额 */
  private double totalMoney = 0;
  /** 总笔数 */
  private int sumNum = 0;

  private  String flag="0";
  /** 批次号 */
  private String serialNo = "";
  /** 批次最大显示返回信息 */
	private String manage = "";

  private LYSendToSettleSet outLYSendToSettleSet = new LYSendToSettleSet();
  private LYBankLogSet outLYBankLogSet = new LYBankLogSet();
//  private LDBankSchema outLDBankSchema = new LDBankSchema();

  public SYMedicalDealBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代收
    if (mOperate.equals("GETMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start PubSubmit BLS Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        mResult.clear();
        return false;
      }
      //调用处理类发送给医保
      TransferData transferData1 = new TransferData();
      transferData1.setNameAndValue("DealType", "D");
      VData inVData = new VData();
      inVData.add(outLYSendToSettleSet);
	  inVData.add(transferData1);
	  inVData.add(mGlobalInput);
      MedicalDealSTBL tMedicalDealSTBL = new MedicalDealSTBL();
      if(!tMedicalDealSTBL.submitData(inVData, "WRITE")){
    	// @@错误处理
          this.mErrors.copyAllErrors(tMedicalDealSTBL.mErrors);
          mResult.clear();
          mResult.add(mErrors.getFirstError());
          return false;
      }else{
    	  mResult = tMedicalDealSTBL.getResult();
      }
      System.out.println("End PubSubmit BLS Submit...");
    }
    
    //结算确认
    if(!MakeSettle()) return false;
    System.out.println("---End SYYBJS---");

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
      medicalCode = (String)tTransferData.getValueByName("MedicalCode");  
      typeFlag = (String)tTransferData.getValueByName("typeFlag");

      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }


  /**
   * 生成送银行表数据
   * @param tLYSendToConfirmSet
   * @return
   */
  private boolean getSendToBank() {
//	  String sql = " select lj.tempfeeno,lc.appntname,lc.bankaccno,lc.appntidno,lc.appntidtype,lc.prtno,lp.riskcode,lc.getpoldate,lc.managecom,lc.agentcode,lj.paymoney "
//	  		 	+" from lccont lc,lcpol lp,ljtempfee lj "
//	  		 	+" where lc.prtno like 'SYW%' "
//	  		 	+" and lc.conttype = '1' "
//	  		 	+" and lc.appflag = '1' "
//	  		 	+" and lc.paymode = '8' "
//	  		 	+" and lc.stateflag = '1' "
//	  		 	+" and lc.contno = lj.otherno "
//	  		 	+" and lc.contno = lp.contno "
//	  		 	+" and lc.prtno = lp.prtno "
//	  		 	+" and lp.riskcode= lj.riskcode "
//	  		 	+" and lc.getpoldate is not null "
//	  		 	+" and lc.customgetpoldate is not null " 
//	  		 	+" and current date > customgetpoldate + 45 day "
//	  		 	+" and not exists (select 1 from lpedoritem where contno = lc.contno and edortype in ('CT','WT')) "
//	  		 	+" and not exists (select 1 from LYSendToSettle where polno = lc.prtno) "
//	  		 	+" fetch first 1 rows only "
//	  		 	+" with ur ";
	  
//	  String sql = "select lj.tempfeeno,lc.appntname,lc.bankaccno,lc.appntidno,lc.appntidtype,lc.prtno, "
////			  +" (select codealias from ldcode where codetype = 'medicaloutrisk' and code in (select riskcode from lcpol where prtno = lc.prtno)) riskcode,
//			  +" '1004' riskcode,lc.getpoldate,lc.managecom,lc.agentcode,ljc.paymoney "
//			  +" from lccont lc,lccontsub lcs,ljtempfee lj,ljtempfeeclass ljc "
//			  +" where lc.prtno like 'SYW%' "
//			  +" and lc.conttype = '1' "
//			  +" and lc.appflag = '1' "
//			  +" and lc.paymode = '8' "
//			  +" and lc.stateflag = '1' "
//			  +" and lc.prtno = lcs.prtno "
//			  +" and lc.contno = lj.otherno "
//			  +" and lj.tempfeeno = ljc.tempfeeno "
//			  +" and lc.getpoldate is not null "
//			  +" and lc.customgetpoldate is not null "
//			  +" and current date > customgetpoldate + 45 day "
//			  +" and not exists (select 1 from lpedoritem where contno = lc.contno and edortype in ('CT','WT')) "
//			  +" and not exists (select 1 from LYSendToSettle where polno = lc.prtno) "
//			  +" fetch first 1 rows only "
//			  +" with ur";
	  
	  String sql = "select (select tempfeeno from ljtempfee where otherno = lc.contno fetch first 1 rows only) tempfeeno,lc.appntname,lc.bankaccno,lc.appntidno,lc.appntidtype,lc.prtno, "
//			  +" (select codealias from ldcode where codetype = 'medicaloutrisk' and code in (select riskcode from lcpol where prtno = lc.prtno)) riskcode,
			  +" '1004' riskcode,lc.getpoldate,lc.managecom,lc.agentcode,(select paymoney from ljtempfeeclass where paymode = '8' and tempfeeno in (select tempfeeno from ljtempfee where otherno = lc.contno)) paymoney "
			  +" from lccont lc,lccontsub lcs "
			  +" where lc.prtno like 'SYW%' "
			  +" and lc.conttype = '1' "
			  +" and lc.appflag = '1' "
			  +" and lc.paymode = '8' "
			  +" and lc.stateflag = '1' "
			  +" and lc.prtno = lcs.prtno "
			  +" and lc.getpoldate is not null "
			  +" and lc.customgetpoldate is not null "
			  +" and current date > lc.customgetpoldate + 45 day "
			  +" and not exists (select 1 from lpedoritem where contno = lc.contno and edortype in ('CT','WT')) "
			  +" and not exists (select 1 from LYSendToSettle where polno = lc.prtno) "
			  +" with ur ";
	  System.out.println(sql); 
	  ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(sql);
      if(null==tSSRS||"".equals(tSSRS)||tSSRS.getMaxRow()<0){
    	  CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
    	  return false;
      }
      //总金额
	  double dTotalMoney = 0;
      //生成批次号，要在循环外生成
	  serialNo = PubFun1.CreateMaxNo("1", 20);
	  LYSendToSettleSet tLYSendToSettleSet = new LYSendToSettleSet();
	  for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
		  //生成送银行表数据
		  LYSendToSettleSchema tLYSendToSettleSchema = new LYSendToSettleSchema();
		  //设置统一的批次号
		  tLYSendToSettleSchema.setSerialNo(serialNo);
		  //收费标记
		  tLYSendToSettleSchema.setDealType("D");
		  tLYSendToSettleSchema.setPayCode(tSSRS.GetText(i, 1));
		  tLYSendToSettleSchema.setPayType("1");
		  tLYSendToSettleSchema.setName(tSSRS.GetText(i, 2));
		  tLYSendToSettleSchema.setMedicalCode("88210001");
		  tLYSendToSettleSchema.setAccName(tSSRS.GetText(i, 2));
		  tLYSendToSettleSchema.setAccNo(tSSRS.GetText(i, 3));
		  tLYSendToSettleSchema.setIDNo(tSSRS.GetText(i, 4));
		  tLYSendToSettleSchema.setIDType(tSSRS.GetText(i, 5));
		  tLYSendToSettleSchema.setPolNo(tSSRS.GetText(i, 6));
		  //因为改为前台录入财务数据，保单表中不一定有数据，所以不再从中取信息
		  tLYSendToSettleSchema.setNoType("9");
		  tLYSendToSettleSchema.setRiskCode(tSSRS.GetText(i, 7));
		  tLYSendToSettleSchema.setGetPolDate(tSSRS.GetText(i, 8));
		  tLYSendToSettleSchema.setComCode(tSSRS.GetText(i, 9));
		  tLYSendToSettleSchema.setAgentCode(tSSRS.GetText(i, 10));
		  tLYSendToSettleSchema.setPayMoney(tSSRS.GetText(i, 11));
		  tLYSendToSettleSchema.setSendDate(PubFun.getCurrentDate());
		  tLYSendToSettleSchema.setSendTime(PubFun.getCurrentTime());
		  tLYSendToSettleSchema.setDealDate(PubFun.getCurrentDate());
		  tLYSendToSettleSchema.setDealTime(PubFun.getCurrentTime());
		  tLYSendToSettleSchema.setSuccFlag("");
		  tLYSendToSettleSchema.setFailReason("");
		  tLYSendToSettleSchema.setDealState("0");
		  tLYSendToSettleSchema.setVertifyFlag("");
		  tLYSendToSettleSchema.setConfDate("");
		  tLYSendToSettleSchema.setConfMoney("");
		  tLYSendToSettleSchema.setInsBankCode("04");
		  //归集账号
		  tLYSendToSettleSchema.setInsAccNo("06-130001040023898");
		  tLYSendToSettleSchema.setRemark("SYJS");
		  tLYSendToSettleSchema.setConvertFlag("");
		  tLYSendToSettleSchema.setDoType("1");
		  //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
		  tLYSendToSettleSchema.setOperator(mGlobalInput.Operator);
		  tLYSendToSettleSchema.setModifyDate(PubFun.getCurrentDate());
		  tLYSendToSettleSchema.setModifyTime(PubFun.getCurrentTime());
		  tLYSendToSettleSchema.setMakeDate(PubFun.getCurrentDate());
		  tLYSendToSettleSchema.setMakeTime(PubFun.getCurrentTime());
		  tLYSendToSettleSet.add(tLYSendToSettleSchema);

		  //累加总金额和总数量
		  dTotalMoney = dTotalMoney + tLYSendToSettleSchema.getPayMoney();
		  //转换精度
		  dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
		  sumNum = sumNum + 1;
		  }
	  totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
	  //判断提数长度，超过500自动截取
	  checkLYSendToConfirmSize(tLYSendToSettleSet);
	  
	  outLYSendToSettleSet.set(tLYSendToSettleSet);
	  return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      //银行代收（应收总表LJSPay）
      if (mOperate.equals("GETMONEY")) {
    	  //生成送银行表数据
    	  if(!getSendToBank()){
    		  CError.buildErr(this, "生成送银行表数据失败");
    		  return false;
    		  }
    	  if(flag.equals("1")){
    		  CError.buildErr(this, "接收数据失败");
    		  return false;
    		  }
    	  }
      //生成银行日志表数据
      LYBankLogSchema tLYBankLogSchema = getBankLog();
      outLYBankLogSet.add(tLYBankLogSchema);
      System.out.println("---End getBankLog---");
    }
    catch(Exception e) {
      // @@错误处理
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return false;
    }

    return true;
  }
  
  /**
   * 生成银行日志表数据
   * @return
   */
  private LYBankLogSchema getBankLog() {
    LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

    tLYBankLogSchema.setSerialNo(serialNo);
    tLYBankLogSchema.setBankCode(medicalCode);
    tLYBankLogSchema.setLogType("D");
    tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
//    tLYBankLogSchema.setTotalMoney(157574.57f);
//    String t = (new DecimalFormat("0.000000")).format(tLYBankLogSchema.getTotalMoney());
    tLYBankLogSchema.setTotalMoney(totalMoney);
    tLYBankLogSchema.setTotalNum(sumNum);
    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
    tLYBankLogSchema.setComCode(mGlobalInput.ComCode);

    return tLYBankLogSchema;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      map.put(outLYSendToSettleSet, "INSERT");
      map.put(outLYBankLogSet, "INSERT");
      
      mInputData.clear();
      mInputData.add(map);
    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }
    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  public boolean CheckLJSpay(LJSPaySet tLJSPaySet){
    LJSPaySet tempLJSPaySet=new LJSPaySet();
    for (int i = 1; i <= tLJSPaySet.size(); i++) {
        if(tLJSPaySet.size()>0){
            LJSPaySchema tLJSPaySchema=tLJSPaySet.get(i);
            String Accno=tLJSPaySchema.getBankAccNo();
            if(!IsNumeric(Accno)){
                tempLJSPaySet.add(tLJSPaySchema);
                tLJSPaySchema.setCanSendBank("1");
                this.map.put(tLJSPaySchema, "UPDATE");
            }
            LDCode1DB tLDCode1DB=new LDCode1DB();
            String sql="select * from ldcode1 where codetype='bankaccnocheck' and code='"+tLJSPaySchema.getBankCode()+"'";
            LDCode1Set tLDCode1Set= tLDCode1DB.executeQuery(sql);
            if(tLDCode1Set!=null && tLDCode1Set.size()>0){
                LDCode1Schema tLDCode1Schema=tLDCode1Set.get(1);
                if(Integer.parseInt(StrTool.cTrim(tLDCode1Schema.getCode1()))<StrTool.cTrim(tLJSPaySchema.getBankAccNo()).length()){
                    tempLJSPaySet.add(tLJSPaySchema);
                    tLJSPaySchema.setCanSendBank("1");
                    this.map.put(tLJSPaySchema, "UPDATE");
                }
            }
        }
    }
    String failInfo="";
    for (int i = 1; i <= tempLJSPaySet.size(); i++) {
        if(!failInfo.equals("")){
            failInfo+="、"+tempLJSPaySet.get(i).getOtherNo();
        }else{
            failInfo=tempLJSPaySet.get(i).getOtherNo();
        }
        tLJSPaySet.remove(tempLJSPaySet.get(i));
    }
    if(!failInfo.equals("")){
        failInfo="业务号"+failInfo+"的帐号位数过长，已被锁定";
    }
    this.mResult.add(failInfo);
    return true;
  }

  //数字判断2
  public boolean IsNumeric(String s)
  {
      for (int i=0; i<s.length(); i++) {
          char c = s.charAt(i);
          if (c!=' ') {
              if (c<'0' || '9'<c) {
                  return false ;
              }
          }
  }
  return true;
}
  
	private void checkLYSendToConfirmSize(LYSendToSettleSet tLYSendToSettleSet){
		String sql = "select code from ldcode where codetype='sendbankcount'";
		ExeSQL tExeSQL = new ExeSQL();
		String count = tExeSQL.getOneValue(sql);
		if (count == null || count.equals("") || count.equals("0")) {
			return;
		}
		int maxCount;
		try {
			maxCount = Integer.parseInt(count);
		} catch (Exception ex) {
			return;
		}
		if (tLYSendToSettleSet.size() > maxCount) {
			this.manage = "待发送数据已超过" + maxCount + "笔，本次未全部提取，请再次进行提取";
			tLYSendToSettleSet.removeRange(maxCount + 1, tLYSendToSettleSet.size());
		}
		return;
	}
	
	private boolean MakeSettle(){
		String sql = "select distinct log.bankcode, log.serialno, log.makedate, log.returndate "
				+" from lybanklog log "
				+" left join lysendtosettle set "
				+" on log.serialno = set.serialno where "
				+" set.dealtype = 'D' "
				+" and set.dealstate = '1' "
				+" and log.BankCode = '88210001' "
				+" and log.returndate = current date "
				+" order by log.returndate desc "
				+" fetch first 3000 rows only with ur ";
		
		System.out.println(sql); 
		ExeSQL tExeSQL = new ExeSQL();
	    SSRS tSSRS = tExeSQL.execSQL(sql);
		
		SettledConfUI getReturnFromBankUI1 = new SettledConfUI();
		TransferData transferData1 = new TransferData();
		//这两个字段需要查出来
//		transferData1.setNameAndValue("totalMoney", request.getParameter("TotalMoney"));
//		transferData1.setNameAndValue("serialNo", request.getParameter("serialNo"));
		//测试先写死一个
		transferData1.setNameAndValue("bankCode", tSSRS.GetText(1, 1));
		transferData1.setNameAndValue("getbankcode", request.getParameter("getbankcode"));
		transferData1.setNameAndValue("getbankaccno", request.getParameter("getbankaccno"));
		//LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();
		//tLYReturnFromBankSchema.setSerialNo(request.getParameter("serialNo"));

//		GlobalInput tGlobalInput = new GlobalInput(); 
//		tGlobalInput = (GlobalInput)session.getValue("GI");
		    
		VData tVData = new VData();
		tVData.add(transferData1);
		tVData.add(mGlobalInput);
		  
		String Content = "";
		String FlagStr = "";

		if (!getReturnFromBankUI1.submitData(tVData, "GETMONEY")) {
			VData rVData = getReturnFromBankUI1.getResult();
		    CError.buildErr(this, "在准备往后层处理所需要的数据时出错，原因是：" + (String)rVData.get(0));
		    return false;
		  	}
		return true;
	}
  
	public String getManage() {
		return this.manage;
	}

	public String getSerialNo() {
		return this.serialNo;
	}
  
  public static void main(String[] args) {
	  LJSPaySchema tLJSPaySchmea = new LJSPaySchema();
	  tLJSPaySchmea.setGetNoticeNo("1");
	  LJSPaySchema tLJSPaySchmea2 = new LJSPaySchema();
	  tLJSPaySchmea.setGetNoticeNo("2");
	  LJSPaySchema tLJSPaySchmea3 = new LJSPaySchema();
	  tLJSPaySchmea.setGetNoticeNo("3");
	  LJSPaySet tLJSPay = new LJSPaySet();
	  tLJSPay.add(tLJSPaySchmea); 
	  tLJSPay.add(tLJSPaySchmea2);
	  tLJSPay.add(tLJSPaySchmea3);
	  
	 SYMedicalDealBL tPaySendToBankBL = new SYMedicalDealBL();
	 System.out.println(tLJSPay.size());
	 System.out.println(tLJSPay.get(tLJSPay.size()).getGetNoticeNo());
  }
}
