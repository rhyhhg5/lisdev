package com.sinosoft.lis.bank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Enumeration;

public class Crypt
{
    private String lastResult;

    private String lastSignMsg;

    public Crypt()
    {
    }

    public boolean SignMsg(String TobeSigned, String KeyFile, String PassWord)
    {
        boolean result;
        FileInputStream fiKeyFile;
        result = false;
        fiKeyFile = null;
        try
        {
            lastSignMsg = "";
            KeyStore ks = KeyStore.getInstance("PKCS12");
            fiKeyFile = new FileInputStream(KeyFile);
            ks.load(fiKeyFile, PassWord.toCharArray());
            Enumeration myEnum = ks.aliases();
            String keyAlias = null;
            RSAPrivateCrtKey prikey = null;
            while (myEnum.hasMoreElements())
            {
                keyAlias = (String) myEnum.nextElement();
                if (ks.isKeyEntry(keyAlias))
                {
                    prikey = (RSAPrivateCrtKey) ks.getKey(keyAlias, PassWord
                            .toCharArray());
                    break;
                }
            }
            if (prikey == null)
            {
                System.out.println("没有找到匹配私钥");
                result = false;
            }
            else
            {
                Signature sign = Signature.getInstance("SHA1withRSA");
                sign.initSign(prikey);
                sign.update(TobeSigned.getBytes());
                byte signed[] = sign.sign();
                byte sign_asc[] = new byte[signed.length * 2];
                Hex2Ascii(signed.length, signed, sign_asc);
                lastResult = new String(sign_asc);
                lastSignMsg = lastResult;
                result = true;
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            System.out.println("找不到证书");
            result = false;
        }
        catch (UnrecoverableKeyException e)
        {
            e.printStackTrace();
            System.out.println("没有找到匹配私钥");
            result = false;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            
            result = false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("签名失败");
            result = false;
        }
        try
        {
            if (!fiKeyFile.equals(null))
                fiKeyFile.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("证书文件关闭失败");
            result = false;
        }
        try
        {
            if (!fiKeyFile.equals(null))
                fiKeyFile.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("证书文件关闭失败");
            result = false;
        }
        return result;
    }

    public boolean VerifyMsg(String TobeVerified, String PlainText,
            String CertFile)
    {
        boolean result;
        FileInputStream certfile;
        result = false;
        certfile = null;
        try
        {
            certfile = new FileInputStream(CertFile);
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate x509cert = (X509Certificate) cf
                    .generateCertificate(certfile);
            RSAPublicKey pubkey = (RSAPublicKey) x509cert.getPublicKey();
            Signature verify = Signature.getInstance("SHA1withRSA");
            verify.initVerify(pubkey);
            byte signeddata[] = new byte[TobeVerified.length() / 2];
            Ascii2Hex(TobeVerified.length(), TobeVerified.getBytes(),
                    signeddata);
            verify.update(PlainText.getBytes());
            if (verify.verify(signeddata))
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        catch (CertificateException e)
        {
            e.printStackTrace();
            System.out.println("证书解析错误");
            result = false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("验签失败");
            result = false;
        }
        try
        {
            if (!certfile.equals(null))
                certfile.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.out.println("验签失败");
            result = false;
        }
        try
        {
            if (!certfile.equals(null))
                certfile.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.out.println("验签失败");
            result = false;
        }
        return result;
    }

    public String getLastSignMsg()
    {
        return lastSignMsg;
    }

    private static void Hex2Ascii(int len, byte data_in[], byte data_out[])
    {
        byte temp1[] = new byte[1];
        byte temp2[] = new byte[1];
        int i = 0;
        int j = 0;
        for (; i < len; i++)
        {
            temp1[0] = data_in[i];
            temp1[0] = (byte) (temp1[0] >>> 4);
            temp1[0] = (byte) (temp1[0] & 0xf);
            temp2[0] = data_in[i];
            temp2[0] = (byte) (temp2[0] & 0xf);
            if (temp1[0] >= 0 && temp1[0] <= 9)
                data_out[j] = (byte) (temp1[0] + 48);
            else if (temp1[0] >= 10 && temp1[0] <= 15)
                data_out[j] = (byte) (temp1[0] + 87);
            if (temp2[0] >= 0 && temp2[0] <= 9)
                data_out[j + 1] = (byte) (temp2[0] + 48);
            else if (temp2[0] >= 10 && temp2[0] <= 15)
                data_out[j + 1] = (byte) (temp2[0] + 87);
            j += 2;
        }

    }

    private static void Ascii2Hex(int len, byte data_in[], byte data_out[])
    {
        byte temp1[] = new byte[1];
        byte temp2[] = new byte[1];
        int i = 0;
        for (int j = 0; i < len; j++)
        {
            temp1[0] = data_in[i];
            temp2[0] = data_in[i + 1];
            if (temp1[0] >= 48 && temp1[0] <= 57)
            {
                temp1[0] -= 48;
                temp1[0] = (byte) (temp1[0] << 4);
                temp1[0] = (byte) (temp1[0] & 0xf0);
            }
            else if (temp1[0] >= 97 && temp1[0] <= 102)
            {
                temp1[0] -= 87;
                temp1[0] = (byte) (temp1[0] << 4);
                temp1[0] = (byte) (temp1[0] & 0xf0);
            }
            if (temp2[0] >= 48 && temp2[0] <= 57)
            {
                temp2[0] -= 48;
                temp2[0] = (byte) (temp2[0] & 0xf);
            }
            else if (temp2[0] >= 97 && temp2[0] <= 102)
            {
                temp2[0] -= 87;
                temp2[0] = (byte) (temp2[0] & 0xf);
            }
            data_out[j] = (byte) (temp1[0] | temp2[0]);
            i += 2;
        }
    }
}
