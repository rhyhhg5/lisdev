package com.sinosoft.lis.bank;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDBankUniteDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class BatchSendBankBaoRongFBL
{

    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 返回数据的容器 */
    private VData mResult = new VData();

    /** 提交数据的容器 */
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 前台传入的公共变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    //业务数据
    /** 提数开始日期 */
    private String startDate = "";

    /** 提数结束日期 */
    private String endDate = "";

    /** 银行编码 */
    private String bankCode = "";

    private LJAGetSet outLJAGetSet = new LJAGetSet();
    
    private LJAGetSet oLJAGetSet = new LJAGetSet();

    public BatchSendBankBaoRongFBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
            return false;
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
            return false;
        System.out.println("---End dealData---");

        //银行代收
        if (mOperate.equals("PAYMONEY"))
        {
            //准备往后台的数据
            if (!prepareOutputData())
                return false;
            System.out.println("---End prepareOutputData---");

            System.out.println("Start PubSubmit BLS Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("End PubSubmit BLS Submit...");
        }

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            TransferData tTransferData = (TransferData) mInputData
                    .getObjectByObjectName("TransferData", 0);
            startDate = (String) tTransferData.getValueByName("startDate");
            endDate = (String) tTransferData.getValueByName("endDate");
            bankCode = (String) tTransferData.getValueByName("bankCode");

            mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 获取交费日期在设置的日期区间内的应付总表记录
     * @param startDate
     * @param endDate
     * @return
     */
    private LJAGetSet getLJAGetByPaydate(String startDate, String endDate,
            String bankCode)
    {
        String tSql = "";
        String tSql2 = "";

       
            tSql = "select * from LJAGet where "
                    + " ShouldDate >= '"
                    + startDate
                    + "'"
                    + " and ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank is null or  CanSendBank = '0')"
                    + " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706','7709') and a.bankcode=bankcode) "
                    + " and othernotype not in ('3','5','23') with ur ";

            tSql2 = "select * from LJAGet where "
                    + " ShouldDate >= '"
                    + startDate
                    + "'"
                    + " and ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank is null or  CanSendBank = '0')"
                    + " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706','7709') and a.bankcode=bankcode) "
                    + " and othernotype = '5' and Exists (Select 1 From Llcase b Where Otherno = b.Caseno) "
                    + " and othernotype <>'23'"
                    + " union  select * from LJAGet where "
                    + " ShouldDate >= '"
                    + startDate
                    + "'"
                    + " and ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and BankCode = '"
                    + bankCode
                    + "'"
                    + " and ManageCom like '"
                    + mGlobalInput.ComCode
                    + "%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank is null or  CanSendBank = '0')"
                    + " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0 "
                    + " and bankcode not in ('489102','999100') "
                    + " and not exists (select 1 from ldbankunite a where a.bankunitecode in ('7703','7704','7705','7706','7709') and a.bankcode=bankcode) "
                    + " and othernotype = '5' and exists (select 1 from llregister lr,llcase lc,llhospcase lh where lr.rgtno=lc.rgtno and lc.caseno=lh.caseno and lr.rgtno=ljaget.otherno and lh.casetype='04' and lr.mngcom like '8695%')"
                    + " and ManageCom like '8695%' "
                    + " and othernotype <>'23' with ur";
       

        System.out.println(tSql);
        System.out.println(tSql2);
        LJAGetDB tLJAGetDB = new LJAGetDB();
        LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(tSql);
        tLJAGetSet.add(tLJAGetDB.executeQuery(tSql2));

        return tLJAGetSet;
    }

     private LJAGetSet getLJAGet(String startDate, String endDate)
    {
        LJAGetSet tLJAGetSet = new LJAGetSet();

        //获取银行信息，校验是否是银联
        LDBankDB tLDBankDB = new LDBankDB();
        tLDBankDB.setBankCode(bankCode);
        if (!tLDBankDB.getInfo())
        {
            CError.buildErr(this, "获取银行信息（LDBank）失败");
            return null;
        }

            //        unitegroupcode 1代表支持代收 2代表支持代付 3代表支持代收代付
            String bankSql = "select distinct bankcode from LJAGet where "
                    + " ShouldDate >= '"
                    + startDate
                    + "'"
                    + " and ShouldDate <= '"
                    + endDate
                    + "'"
                    + " and exists (select 1 from ldbankunite ld where bankunitecode = '"+bankCode+"'  and ld.bankcode=ljaget.bankcode and unitegroupcode in('2','3'))"
                    + " and ManageCom like '86%'"
                    + " and PayMode='4'"
                    + " and EnterAccDate is null and ConfDate is null"
                    + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                    + " and (CanSendBank is null or  CanSendBank = '0')"
                    + " and (BankAccNo <> '') and (AccName <> '') and sumgetmoney<>0  and othernotype not in('3','23') "
                    ;   
            System.out.println(bankSql);
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(bankSql);

            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                tLJAGetSet.add(getLJAGetByPaydate(startDate, endDate,tSSRS.GetText(i, 1)));
            }

        return tLJAGetSet;
    }

    /**
     * 修改应收表银行在途标志,记录发送银行次数
     * @param tLJSPaySet
     * @return
     */
    private LJAGetSet modifyBankFlag(LJAGetSet tLJAGetSet, boolean flag)
    {
        String bankOnTheWay = "";
        String canSend = "";
        if(flag == true){
            bankOnTheWay = "1";
            canSend = "8";
        } else {
            bankOnTheWay = "0";
            canSend = "1";
        }
        for (int i = 0; i < tLJAGetSet.size(); i++)
        {
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

            tLJAGetSchema.setBankOnTheWayFlag(bankOnTheWay);
            tLJAGetSchema.setCanSendBank(canSend);
            tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
            tLJAGetSchema.setOperator(mGlobalInput.Operator);
            //记录发送银行次数
            tLJAGetSet.set(i + 1, tLJAGetSchema);
        }

        return tLJAGetSet;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            if (mOperate.equals("PAYMONEY"))
            {

                LJAGetSet tLJAGetSet = getLJAGet(startDate, endDate);
                if (tLJAGetSet == null) {
                    throw new NullPointerException("总应付表处理失败！");
                }
                if (tLJAGetSet.size() == 0) {
                    throw new NullPointerException("总应付表无数据！");
                }
                if (!CheckLJAGet(tLJAGetSet, bankCode))
                {
                    return false;
                }
                System.out.println("---End getLJSPayByPaydate---");

                //修改应收表银行在途标志,记录发送银行次数
                tLJAGetSet = modifyBankFlag(tLJAGetSet ,true);
                System.out.println("---End modifyBankFlag---");

                outLJAGetSet.set(tLJAGetSet);
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "数据处理错误:" + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            map.put(oLJAGetSet, "UPDATE");
            map.put(outLJAGetSet, "UPDATE");

            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public boolean CheckLJAGet(LJAGetSet tLJAGetSet, String bankcode)
    {
        String failInfo = "";
        LJAGetSet oLJAGetSet = new LJAGetSet();
        LDCodeDB tLDCodeDB = new LDCodeDB();
        String sql = "select * from ldcode where codetype='accnocheckF' and code='"
                + bankcode + "'";
        LDCodeSet tLDCodeSet = tLDCodeDB.executeQuery(sql);
        if (tLDCodeSet != null && tLDCodeSet.size() > 0)
        {
            String checkReg = tLDCodeSet.get(1).getCodeName();
            for (int row = 1; row <= tLJAGetSet.size(); row++)
            {
                String accno = tLJAGetSet.get(row).getBankAccNo();
                if (!accno.trim().matches(checkReg))
                {
                    oLJAGetSet.add(tLJAGetSet.get(row));
                }
            }
        }
        for (int i = 1; i <= oLJAGetSet.size(); i++)
        {
            if (!failInfo.equals(""))
            {
                failInfo += "、" + oLJAGetSet.get(i).getOtherNo();
            }
            else
            {
                failInfo = oLJAGetSet.get(i).getOtherNo();
            }
            tLJAGetSet.remove(oLJAGetSet.get(i));
        }
        if (!failInfo.equals(""))
        {
            failInfo = "业务号" + failInfo + "的帐号格式错误，已被锁定";
        }
        this.oLJAGetSet.set(modifyBankFlag(oLJAGetSet, false));
        this.mResult.add(failInfo);
        return true;
    }

    public static void main(String[] args)
    {

    }
}