package com.sinosoft.lis.bank;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ABCBankFetchDateConfF
{

	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 返回数据的容器 */
	private VData mResult = new VData();

	String flag = "0";

	/** 提交数据的容器 */
	private MMap map = new MMap();

	/** 数据操作字符串 */
	private String mOperate;

	/** 确定标记 */
	private String tbuttontype = "";

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	/** 前台传入的公共变量 */
	GlobalInput tG = new GlobalInput();

	/** 银行编码 */
	String bankCode = "";

	/** 提取数据范围类型，如果值是：ALLXQ，则只提取续期数据 */

	/** 总金额 */
	private double totalMoney = 0;

	/** 总笔数 */
	private int sumNum = 0;

	// private String flag="0";
	/** 批次号 */
	String serialNo = "";

	String banktype = "";

	private LJAGetSet inLJAGetSet = new LJAGetSet();

	private LJAGetSet outLJAGetSet = new LJAGetSet();

	private TransferData transferData = new TransferData();

	private String bankUniteCode = "";

	public ABCBankFetchDateConfF()
	{
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中

		if (!getInputData(cInputData))
			return false;
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData())
			return false;
		System.out.println("---End dealData---");

		// 准备往后台的数据
		if (!prepareOutputData())
			return false;
		System.out.println("---End prepareOutputData---");

		System.out.println("Start PubSubmit BLS Submit...");
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			mResult.clear();
			return false;
		}
		System.out.println("End PubSubmit BLS Submit...");

		ABCBankSendSave tABCBankSendSave = new ABCBankSendSave();
		if (tbuttontype.equals("Yes"))
		{
			if (!tABCBankSendSave.dealData(transferData))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData(VData mInputData)
	{
		try
		{

			tbuttontype = (String) mInputData.get(2);
			bankUniteCode = (String) mInputData.get(3);
			banktype = (String) mInputData.get(4);

			String bankSQL = "";
			if (banktype == null || banktype == "")
			{
				System.out.println("银行类型错误");
				return false;
			} else if (banktype.equals("1"))
			{
				bankSQL = " and unitebankcode <> '104' ";
			} else if (banktype.equals("2"))
			{
				bankSQL = " and unitebankcode = '104' ";
			} else
			{
				System.out.println("银行类型错误");
				return false;
			}

			tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
					0);

			if (tbuttontype.equals("Yes"))
			{
				String tSql = "select * from ljaget a where ManageCom like '"
						+ tG.ComCode
						+ "%'  and cansendbank = '8' and exists ( select 1 from ldbankunite where bankunitecode='"
						+ bankUniteCode + "' and bankcode=a.bankcode "
						+ bankSQL + ") with ur";
				System.out.println(tSql);
				LJAGetDB tLJAGetDB = new LJAGetDB();
				inLJAGetSet.set(tLJAGetDB.executeQuery(tSql));
			} else
			{
				inLJAGetSet.set((LJAGetSet) mInputData.getObjectByObjectName(
						"LJAGetSet", 0));
			}

			transferData.setNameAndValue("DealType", "F");
			transferData.setNameAndValue("ComCode", tG.ComCode);
			transferData.setNameAndValue("BankCode", bankUniteCode);

		} catch (Exception e)
		{
			e.printStackTrace();
			// @@错误处理
			CError.buildErr(this, "接收数据失败");
			return false;
		}

		return true;
	}

	private LJAGetSet getLJAGetSet(LJAGetSet tinLJAGet)
	{
		LJAGetSet tLJAGetSet = new LJAGetSet();
		if (tinLJAGet.size() > 0)
		{
			for (int i = 1; i <= tinLJAGet.size(); i++)
			{
				LJAGetSchema tLJAGetSchema = tinLJAGet.get(i);
				tLJAGetSchema = (tLJAGetSchema.getDB().query()).get(1);
				tLJAGetSet.add(tLJAGetSchema);
			}
		}

		return tLJAGetSet;
	}

	/**
	 * 生成送银行表数据
	 * 
	 * @param tLJSPaySet
	 * @return
	 */
	public LYSendToBankSet getSendToBank(LJAGetSet tLJAGetSet, String tBankCode)
	{
		// 总金额
		double dTotalMoney = 0;
		// 总笔数
		sumNum = 0;
		// 生成批次号，要在循环外生成
		StringBuffer mSeriaPrefix = new StringBuffer();
		String mGetMerchantNo = "select codename from ldcode1 where codetype='BatchSendBank' and code = 'UserName' and code1 = 'F"
				+ tBankCode + "' with ur ";
		ExeSQL mExeSql = new ExeSQL();
		SSRS tNoSSRS = mExeSql.execSQL(mGetMerchantNo);
		if (tNoSSRS.getMaxRow() > 0)
		{
			String xMerchantNo = tNoSSRS.GetText(1, 1);
			String xLastFourNo = xMerchantNo.substring(
					xMerchantNo.length() - 4, xMerchantNo.length());

			mSeriaPrefix.append(tBankCode);
			mSeriaPrefix.append(xLastFourNo);
		}

		serialNo = PubFun1.CreateMaxNo("1", 12);
		serialNo = mSeriaPrefix.toString() + serialNo;
		System.out.println(serialNo);

		LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();

		for (int i = 0; i < tLJAGetSet.size(); i++)
		{
			LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

			// 生成送银行表数据
			LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
			// 设置统一的批次号
			tLYSendToBankSchema.setSerialNo(serialNo);
			// 收费标记
			tLYSendToBankSchema.setDealType("F");
			tLYSendToBankSchema.setPayCode(tLJAGetSchema.getActuGetNo());
			tLYSendToBankSchema.setBankCode(tLJAGetSchema.getBankCode());
			tLYSendToBankSchema.setAccName(tLJAGetSchema.getAccName());
			tLYSendToBankSchema.setAccNo(tLJAGetSchema.getBankAccNo());
			tLYSendToBankSchema.setPolNo(tLJAGetSchema.getOtherNo());
			tLYSendToBankSchema.setNoType(tLJAGetSchema.getOtherNoType());
			if (tLJAGetSchema.getOtherNoType().equals("4"))
			{
				System.out.println(tLJAGetSchema.getOtherNoType()
						+ "-----------------------");
				String tljtempfee = "select * from ljtempfee where tempfeeno ='"
						+ tLJAGetSchema.getOtherNo() + "' ";
				LJTempFeeDB tLjtempfeeDB = new LJTempFeeDB();
				LJTempFeeSet tLJTempFeeSet = tLjtempfeeDB
						.executeQuery(tljtempfee);
				LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(1);
				tLYSendToBankSchema.setComCode(tLJTempFeeSchema.getPolicyCom());

			} else
			{
				tLYSendToBankSchema.setComCode(tLJAGetSchema.getManageCom());
			}
			tLYSendToBankSchema.setAgentCode(tLJAGetSchema.getAgentCode());
			tLYSendToBankSchema.setPayMoney(tLJAGetSchema.getSumGetMoney());
			tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
			tLYSendToBankSchema.setDoType("0");
			// 因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
			tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
			tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
			if (!StrTool.cTrim(tLJAGetSchema.getDrawerID()).equals(""))
			{
				tLYSendToBankSchema.setIDType("0");
				tLYSendToBankSchema.setIDNo(tLJAGetSchema.getDrawerID());
			}
			if (!StrTool.cTrim(tLJAGetSchema.getDrawer()).equals(""))
			{
				tLYSendToBankSchema.setName(tLJAGetSchema.getDrawer());
			}

			tLYSendToBankSet.add(tLYSendToBankSchema);

			// 累加总金额和总数量
			dTotalMoney = dTotalMoney + tLJAGetSchema.getSumGetMoney();
			// 转换精度
			dTotalMoney = Double.parseDouble((new DecimalFormat("0.00"))
					.format(dTotalMoney));
			sumNum = sumNum + 1;
		}
		totalMoney = Double.parseDouble((new DecimalFormat("0.00"))
				.format(dTotalMoney));

		return tLYSendToBankSet;
	}

	/**
	 * 修改应收表银行在途标志,记录发送银行次数
	 * 
	 * @param tLJSPaySet
	 * @return
	 */
	private LJAGetSet modifyBankFlag(LJAGetSet tLJAGetSet)
	{
		for (int i = 0; i < tLJAGetSet.size(); i++)
		{
			System.out.println("modifyBankFlag:");

			LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i + 1);

			if (tbuttontype.equals("Yes"))
			{
				tLJAGetSchema.setBankOnTheWayFlag("1");
				tLJAGetSchema.setCanSendBank("9");
				tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
				tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
				tLJAGetSchema.setOperator(tG.Operator);
			} else
			{
				tLJAGetSchema.setBankOnTheWayFlag("0");
				tLJAGetSchema.setCanSendBank("0");
				tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
				tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
				tLJAGetSchema.setOperator(tG.Operator);
			}

			// 记录发送银行次数
			tLJAGetSchema
					.setSendBankCount(tLJAGetSchema.getSendBankCount() + 1);
			tLJAGetSet.set(i + 1, tLJAGetSchema);
		}

		return tLJAGetSet;
	}

	/**
	 * 生成银行日志表数据
	 * 
	 * @return
	 */
	public LYBankLogSchema getBankLog()
	{
		LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

		tLYBankLogSchema.setBankCode(bankCode);
		tLYBankLogSchema.setLogType("F");
		tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
		tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
		tLYBankLogSchema.setTotalMoney(totalMoney);
		tLYBankLogSchema.setTotalNum(sumNum);
		tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
		tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
		tLYBankLogSchema.setComCode(tG.ComCode);

		return tLYBankLogSchema;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData()
	{
		try
		{

			LJAGetSet tLJAGetSet = getLJAGetSet(inLJAGetSet);

			if (!CheckLJAGet(tLJAGetSet))
			{
				return false;
			}
			// System.out.println("tLJAGetSet.size()sssss"+inLJAGetSet.size());
			if (tLJAGetSet == null)
				throw new NullPointerException("总应付表处理失败！");
			if (tLJAGetSet.size() == 0)
				throw new NullPointerException("总应付表无数据！");
			System.out.println("---End getLJAGetByPaydate---");

			// 修改应收表银行在途标志,记录发送银行次数
			modifyBankFlag(tLJAGetSet);
			System.out.println("---End modifyBankFlag---");
			// 生成银行日志表数据
			// LYBankLogSchema tLYBankLogSchema = getBankLog();
			System.out.println("---End getBankLog---");
			outLJAGetSet.set(tLJAGetSet);

			// outLYBankLogSet.add(tLYBankLogSchema);

		} catch (Exception e)
		{
			// @@错误处理
			CError.buildErr(this, "数据处理错误:" + e.getMessage());
			return false;
		}

		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return 如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData()
	{
		try
		{
			map.put(outLJAGetSet, "UPDATE");
			System.out.println("执行了后台的插入动作！！！");
			// map.put(outLYBankLogSet, "INSERT");

			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex)
		{
			// @@错误处理
			CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
			return false;
		}

		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult()
	{
		return mResult;
	}

	public boolean CheckLJAGet(LJAGetSet tLJAGetSet)
	{
		LJAGetSet tempLJAGetSet = new LJAGetSet();

		for (int i = 1; i <= tLJAGetSet.size(); i++)
		{
			if (tLJAGetSet.size() > 0)
			{
				LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i);
				System.out.println(tLJAGetSchema.getOtherNo());
				tLJAGetSchema.setCanSendBank("9");
				tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
				tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
				tLJAGetSchema.setOperator(tG.Operator);
				this.map.put(tLJAGetSchema, "UPDATE");
				tempLJAGetSet.add(tLJAGetSchema);

			}
		}

		return true;
	}

	// 数字判断2
	public boolean IsNumeric(String s)
	{
		for (int i = 0; i < s.length(); i++)
		{
			char c = s.charAt(i);
			if (c != ' ')
			{
				if (c < '0' || '9' < c)
				{
					return false;
				}
			}
		}
		return true;
	}

	public static void main(String args[])
	{
		SendBankConfFBL sbcb = new SendBankConfFBL();
		LJAGetSet tLJAGetSet = new LJAGetSet();
		sbcb.getSendToBank(tLJAGetSet, "7705");
	}

	public LJAGetSet getInLJLJAGetSet()
	{
		return inLJAGetSet;
	}

	public void setInLJLJAGetSet(LJAGetSet inLJLJAGetSet)
	{
		this.inLJAGetSet = inLJLJAGetSet;
	}

}
