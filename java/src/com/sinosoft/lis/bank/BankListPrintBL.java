package com.sinosoft.lis.bank;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BankListPrintBL {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null;

	private String mSql = null;

	private String mOutXmlPath = null;

	private String startDate = "";

	private String endDate = "";

	private String manageCom = "";

	private String mCurDate = PubFun.getCurrentDate();

	SSRS tSSRS = new SSRS();

	SSRS tComSSRS = new SSRS();

	public BankListPrintBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 校验操作是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println("BL->dealDate()");
		mSql = "select distinct substr(comcode,1,4) from lyreturnfrombank "
				+ "where exists (select 1 from lybanklog where serialno=lyreturnfrombank.serialno "
				+ "and bankcode in ('7701','7703','7705','7706','7707','7709') and returndate between '"
				+ startDate + "' and '" + endDate + "') and comcode like '"
				+ manageCom + "%'";
		System.out.println(mSql);
		System.out.println(mOutXmlPath);
		tComSSRS = tExeSQL.execSQL(mSql);

		if (tExeSQL.mErrors.needDealError()) {
			System.out.println(tExeSQL.mErrors.getErrContent());

			CError tError = new CError();
			tError.moduleName = "BankListPrintBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		if (tComSSRS.getMaxRow() == 0) {

			CError tError = new CError();
			tError.moduleName = "BankListPrintBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有符合条件的数据，请重新输入条件";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		String[][] mToExcel = new String[tComSSRS.getMaxRow() + 10][16];

		mToExcel[0][0] = "全系统代收付统计表";
		mToExcel[2][0] = "统计时间：" + startDate + "至" + endDate;
		mToExcel[3][8] = "单位：元";

		mToExcel[5][0] = "分公司名称";
		mToExcel[5][1] = "保融代收";
		mToExcel[5][2] = "银联代收";
		mToExcel[5][3] = "通联代收";
		mToExcel[5][4] = "邮储代收";
		mToExcel[5][5] = "金联代收";
		mToExcel[5][6] = "银联实时代收";	
		mToExcel[5][7] = "银联高费率渠道代收";		
		mToExcel[5][8] = "代收合计";
		mToExcel[5][9] = "保融代付";
		mToExcel[5][10] = "银联代付";
		mToExcel[5][11] = "通联代付";
		mToExcel[5][12] = "邮储代付";
		mToExcel[5][13] = "金联代付";
		mToExcel[5][14] = "银联高费率渠道代付";
		mToExcel[5][15] = "代付合计";

		try {
			System.out.println(tComSSRS.getMaxRow());
			for (int comnum = 1; comnum <= tComSSRS.getMaxRow(); comnum++) {
				String com = tComSSRS.GetText(comnum, 1);
				System.out.println("comCode:--" + com);

				mToExcel[5 + comnum][0] = getName(com);
				mToExcel[5 + comnum][1] = getMoeny(com, "7701", "S");
				mToExcel[5 + comnum][2] = getMoeny(com, "7705", "S");
				mToExcel[5 + comnum][3] = getMoeny(com, "7706", "S");
				mToExcel[5 + comnum][4] = getMoeny(com, "7703", "S");
				mToExcel[5 + comnum][5] = getMoeny(com, "7707", "S");
				mToExcel[5 + comnum][6] = getMoeny(com, "7705", "R");				
				mToExcel[5 + comnum][7] = getMoeny(com, "7709", "S");				
				mToExcel[5 + comnum][8] = getMoeny(com, "", "S");
				mToExcel[5 + comnum][9] = getMoeny(com, "7701", "F");
				mToExcel[5 + comnum][10] = getMoeny(com, "7705", "F");
				mToExcel[5 + comnum][11] = getMoeny(com, "7706", "F");
				mToExcel[5 + comnum][12] = getMoeny(com, "7703", "F");
				mToExcel[5 + comnum][13] = getMoeny(com, "7707", "F");
				mToExcel[5 + comnum][14] = getMoeny(com, "7709", "F");
				mToExcel[5 + comnum][15] = getMoeny(com, "", "F");
			}
			mToExcel[7 + tComSSRS.getMaxRow()][0] = "合计";
			mToExcel[7 + tComSSRS.getMaxRow()][1] = getMoeny("", "7701", "S");
			mToExcel[7 + tComSSRS.getMaxRow()][2] = getMoeny("", "7705", "S");
			mToExcel[7 + tComSSRS.getMaxRow()][3] = getMoeny("", "7706", "S");
			mToExcel[7 + tComSSRS.getMaxRow()][4] = getMoeny("", "7703", "S");
			mToExcel[7 + tComSSRS.getMaxRow()][5] = getMoeny("", "7707", "S");
			mToExcel[7 + tComSSRS.getMaxRow()][6] = getMoeny("", "7705", "R");
			mToExcel[7 + tComSSRS.getMaxRow()][7] = getMoeny("", "7709", "S");
			mToExcel[7 + tComSSRS.getMaxRow()][8] = getMoeny("", "", "S");
			mToExcel[7 + tComSSRS.getMaxRow()][9] = getMoeny("", "7701", "F");
			mToExcel[7 + tComSSRS.getMaxRow()][10] = getMoeny("", "7705", "F");
			mToExcel[7 + tComSSRS.getMaxRow()][11] = getMoeny("", "7706", "F");
			mToExcel[7 + tComSSRS.getMaxRow()][12] = getMoeny("", "7703", "F");
			mToExcel[7 + tComSSRS.getMaxRow()][13] = getMoeny("", "7707", "F");
			mToExcel[7 + tComSSRS.getMaxRow()][14] = getMoeny("", "7709", "F");
			mToExcel[7 + tComSSRS.getMaxRow()][15] = getMoeny("", "", "F");
		} catch (Exception ex) {
			CError tError = new CError();
			tError.moduleName = "BankListPrintBL";
			tError.functionName = "dealData";
			tError.errorMessage = "查询信息失败！";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		mToExcel[9 + tComSSRS.getMaxRow()][0] = "制表单位："
				+ getName(mGI.ManageCom);
		mToExcel[9 + tComSSRS.getMaxRow()][4] = "制表日期：" + mCurDate;

		try {
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	private String getMoeny(String manageCom, String bankCode, String type)
			throws Exception {
		String tbankCode = bankCode;
		if (bankCode.equals("")) {
			bankCode = "ybl.bankcode in ('7701','7703','7706','7705','7707','7709')";
		} else {
			bankCode = "ybl.bankcode ='" + bankCode + "'";
		}
		if (manageCom.equals("")) {
			manageCom = this.manageCom;
		}
		if(tbankCode.equals("")&&type.equals("S")){
			type = type+"','R";
		}
		ExeSQL tExeSQL = new ExeSQL();
		String sql = "select sum(yrfb.paymoney) from lyreturnfrombank yrfb "
				+ "inner join lybanklog ybl on ybl.serialno=yrfb.serialno "
				+ "where " + bankCode + " and ( yrfb.banksuccflag='0000' or yrfb.banksuccflag='00' or yrfb.banksuccflag='S0000')  "
				+ "and ybl.logtype in ('" + type + "') and yrfb.comcode like '"
				+ manageCom + "%' and returndate between '" + startDate
				+ "' and '" + endDate + "'";
		String meney = tExeSQL.getOneValue(sql);
		if (tExeSQL.mErrors.needDealError()) {
			throw new Exception("查询信息失败!");
		}
		if (meney == null || meney.equals("")) {
			meney = "0";
		}
		return meney;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);

		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "BankListPrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		startDate = (String) tf.getValueByName("StartDate");
		endDate = (String) tf.getValueByName("EndDate");
		manageCom = (String) tf.getValueByName("ManageCom");

		if (startDate == null || endDate == null || mOutXmlPath == null
				|| manageCom == null) {
			CError tError = new CError();
			tError.moduleName = "BankListPrintBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		return true;
	}

	private String getName(String manageCom) {
		String tSQL = "";
		String tRtValue = "";
		tSQL = "select name from ldcom where comcode='" + manageCom + "'";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(tSQL);
		if (tSSRS.getMaxRow() == 0) {
			tRtValue = "";
		} else
			tRtValue = tSSRS.GetText(1, 1);
		return tRtValue;
	}

	public static void main(String[] args) {
	}
}
