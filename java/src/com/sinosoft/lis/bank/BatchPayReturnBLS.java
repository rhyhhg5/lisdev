package com.sinosoft.lis.bank;

import java.sql.Connection;

import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.vdb.LJABonusGetDBSet;
import com.sinosoft.lis.vdb.LJAGetClaimDBSet;
import com.sinosoft.lis.vdb.LJAGetDBSet;
import com.sinosoft.lis.vdb.LJAGetDrawDBSet;
import com.sinosoft.lis.vdb.LJAGetEndorseDBSet;
import com.sinosoft.lis.vdb.LJAGetOtherDBSet;
import com.sinosoft.lis.vdb.LJAGetTempFeeDBSet;
import com.sinosoft.lis.vdb.LJFIGetDBSet;
import com.sinosoft.lis.vdb.LLCaseDBSet;
import com.sinosoft.lis.vdb.LLRegisterDBSet;
import com.sinosoft.lis.vdb.LYBankLogDBSet;
import com.sinosoft.lis.vdb.LYDupGetDBSet;
import com.sinosoft.lis.vdb.LYReturnFromBankBDBSet;
import com.sinosoft.lis.vdb.LYReturnFromBankDBSet;
import com.sinosoft.lis.vdb.LYSendToBankDBSet;
import com.sinosoft.lis.vschema.LJABonusGetSet;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.vschema.LJAGetDrawSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAGetOtherSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJAGetTempFeeSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.lis.vschema.LLCaseSet;
import com.sinosoft.lis.vschema.LLRegisterSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYDupGetSet;
import com.sinosoft.lis.vschema.LYReturnFromBankBSet;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class BatchPayReturnBLS {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 传入数据的容器 */
  private VData mInputData = new VData();

  //业务数据
  private LYDupGetSet inLYDupGetSet = new LYDupGetSet();
  private LYBankLogSet inLYBankLogSet = new LYBankLogSet();
  private LYReturnFromBankSet inDelLYReturnFromBankSet = new LYReturnFromBankSet();
  private LYReturnFromBankSet inLYReturnFromBankSet = new LYReturnFromBankSet();
  private LJFIGetSet inLJFIGetSet = new LJFIGetSet();
  private LJAGetSet inLJAGetSet = new LJAGetSet();
  private LYReturnFromBankBSet inLYReturnFromBankBSet = new LYReturnFromBankBSet();
  private LYSendToBankSet inDelLYSendToBankSet = new LYSendToBankSet();
 
  private LYBankLogSchema inLYBankLogSchema = new LYBankLogSchema();
  private LJAGetDrawSet inLJAGetDrawSet = new LJAGetDrawSet();
  private LJAGetEndorseSet inLJAGetEndorseSet = new LJAGetEndorseSet();
  private LJAGetTempFeeSet inLJAGetTempFeeSet = new LJAGetTempFeeSet();
  private LJAGetClaimSet inLJAGetClaimSet = new LJAGetClaimSet();
  private LJAGetOtherSet inLJAGetOtherSet = new LJAGetOtherSet();
  private LJABonusGetSet inLJABonusGetSet = new LJABonusGetSet();

  public BatchPayReturnBLS() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData) {
    boolean tReturn = false;

    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    //this.mOperate =cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    //System.out.println("---End getInputData---");

    //信息保存
    //if(this.mOperate.equals("PAYMONEY")) {
      tReturn = save(cInputData);
    //}

    if (tReturn)
      System.out.println("Save sucessful");
    else
      System.out.println("Save failed");

    return tReturn;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()    {
    try {
      inLYBankLogSchema  = (LYBankLogSchema)mInputData.getObjectByObjectName("LYBankLogSchema", 0);
      inLYReturnFromBankSet = (LYReturnFromBankSet)mInputData.getObjectByObjectName("LYReturnFromBankSet", 1);  
      inLYDupGetSet = (LYDupGetSet)mInputData.getObjectByObjectName("LYDupGetSet", 2);
      inLYBankLogSet = (LYBankLogSet)mInputData.getObjectByObjectName("LYBankLogSet", 3);
      //inDelLYReturnFromBankSet = (LYReturnFromBankSet)mInputData.getObjectByObjectName("LYReturnFromBankSet", 0);
      inLJFIGetSet = (LJFIGetSet)mInputData.getObjectByObjectName("LJFIGetSet", 4);
      inLJAGetSet = (LJAGetSet)mInputData.getObjectByObjectName("LJAGetSet", 5);
      inDelLYSendToBankSet = (LYSendToBankSet)mInputData.getObjectByObjectName("LYSendToBankSet", 6);
      inLYReturnFromBankSet = (LYReturnFromBankSet)mInputData.getObjectByObjectName("LYReturnFromBankSet", 7);


      inLJAGetDrawSet = (LJAGetDrawSet)mInputData.getObjectByObjectName("LJAGetDrawSet", 8);
      inLJAGetEndorseSet = (LJAGetEndorseSet)mInputData.getObjectByObjectName("LJAGetEndorseSet", 9);
      inLJAGetTempFeeSet = (LJAGetTempFeeSet)mInputData.getObjectByObjectName("LJAGetTempFeeSet", 10);
      //inmyLJFIGetSet     = (LJFIGetSet)mInputData.getObjectByObjectName("LJFIGetSet",11);
      inLJAGetClaimSet = (LJAGetClaimSet)mInputData.getObjectByObjectName("LJAGetClaimSet", 11);
      inLJAGetOtherSet = (LJAGetOtherSet)mInputData.getObjectByObjectName("LJAGetOtherSet", 12);
      inLJABonusGetSet = (LJABonusGetSet)mInputData.getObjectByObjectName("LJABonusGetSet", 13);


    }
    catch (Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PayReturnFromBankBLS";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  //保存操作
  private boolean save(VData mInputData) {
    boolean tReturn =true;
    System.out.println("Start Save...");

    //建立数据库连接
    Connection conn=DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PayReturnFromBankBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try {
      //开始事务，锁表
      conn.setAutoCommit(false);

      //生成重复给付数据，插入重复给付记录表
      LYDupGetDBSet tLYDupGetDBSet = new LYDupGetDBSet(conn);
      tLYDupGetDBSet.set(inLYDupGetSet);
      if (!tLYDupGetDBSet.insert()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYDupGet Insert Failed");
        return false;
      }
      System.out.println("End 生成重复给付数据 ...");
      
      //生成回盘记录表
      LYReturnFromBankDBSet tinLYReturnFromBankDBSet = new LYReturnFromBankDBSet();
      tinLYReturnFromBankDBSet.set(inLYReturnFromBankSet);
      if(!tinLYReturnFromBankDBSet.insert())
      {
          try{ conn.rollback() ;} catch(Exception e){}
          conn.close();
          System.out.println("LYReturnFromBank Insert Failed");
          return false;
      }

      //记录银行业务日志，插入新日志数据
      LYBankLogDBSet tLYBankLogDBSet = new LYBankLogDBSet(conn);
      tLYBankLogDBSet.set(inLYBankLogSet);
      if (!tLYBankLogDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYBankLog Update Failed");
        return false;
      }
      System.out.println("End 记录银行业务日志 ...");

      //转为财务付费表
      LJFIGetDBSet tLJFIGetDBSet = new LJFIGetDBSet(conn);
      tLJFIGetDBSet.set(inLJFIGetSet);
      if (!tLJFIGetDBSet.insert()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJFIGet Insert Failed");
        return false;
      }
      System.out.println("End 转为财务付费表 ...");

      //修改总实付表的银行在途标志
      LJAGetDBSet tLJAGetDBSet = new LJAGetDBSet(conn);
      tLJAGetDBSet.set(inLJAGetSet);
      if (!tLJAGetDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJAGet Update Failed");
        return false;
      }
      System.out.println("End 修改总实付表的银行在途标志 ...");

      //备份返回盘数据
      LYReturnFromBankBDBSet tLYReturnFromBankBDBSet = new LYReturnFromBankBDBSet(conn);
      tLYReturnFromBankBDBSet.set(inLYReturnFromBankBSet);
      if (!tLYReturnFromBankBDBSet.insert()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYReturnFromBankB Insert Failed");
        return false;
      }
      System.out.println("End 备份返回盘数据 ...");

      //删除返回盘数据
      LYReturnFromBankDBSet tLYReturnFromBankDBSet = new LYReturnFromBankDBSet(conn);
      tLYReturnFromBankDBSet.set(inDelLYReturnFromBankSet);
      if (!tLYReturnFromBankDBSet.delete()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYReturnFromBank Delete Failed");
        return false;
      }
      System.out.println("End 删除返回盘数据 ...");
      
      //删除LJFIGET数据
//      LJFIGetDBSet myLJFIGetDBSet = new LJFIGetDBSet(conn);
//      myLJFIGetDBSet.set(inmyLJFIGetSet);
//      if (!myLJFIGetDBSet.delete()) {
//        try{ conn.rollback() ;} catch(Exception e){}
//        conn.close();
//        System.out.println("LYReturnFromBank Delete Failed");
//        return false;
//      }
//      System.out.println("End 删除返回盘数据 ...");

      //删除发送盘数据
      LYSendToBankDBSet tLYSendToBankDBSet = new LYSendToBankDBSet(conn);
      tLYSendToBankDBSet.set(inDelLYSendToBankSet);
      if (!tLYSendToBankDBSet.delete()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LYSendToBank Insert Failed");
        return false;
      }
      System.out.println("End 删除发送盘数据 ...");

      //更新给付表
      LJAGetDrawDBSet tLJAGetDrawDBSet = new LJAGetDrawDBSet(conn);
      tLJAGetDrawDBSet.set(inLJAGetDrawSet);
      if (!tLJAGetDrawDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJAGetDraw Insert Failed");
        return false;
      }
      System.out.println("End 更新给付表 ...");

      //更新批改补退费表
      LJAGetEndorseDBSet tLJAGetEndorseDBSet = new LJAGetEndorseDBSet(conn);
      tLJAGetEndorseDBSet.set(inLJAGetEndorseSet);
      if (!tLJAGetEndorseDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJAGetEndorse Insert Failed");
        return false;
      }
      System.out.println("End 更新批改补退费表 ...");

      //更新暂交费退费实付表
      LJAGetTempFeeDBSet tLJAGetTempFeeDBSet = new LJAGetTempFeeDBSet(conn);
      tLJAGetTempFeeDBSet.set(inLJAGetTempFeeSet);
      if (!tLJAGetTempFeeDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJAGetTempFee Insert Failed");
        return false;
      }
      System.out.println("End 更新暂交费退费实付表 ...");

      //更新赔付实付表
      LJAGetClaimDBSet tLJAGetClaimDBSet = new LJAGetClaimDBSet(conn);
      tLJAGetClaimDBSet.set(inLJAGetClaimSet);
      if (!tLJAGetClaimDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJAGetClaim Insert Failed");
        return false;
      }
      System.out.println("End 更新赔付实付表 ...");

      //更新其他退费实付表
      LJAGetOtherDBSet tLJAGetOtherDBSet = new LJAGetOtherDBSet(conn);
      tLJAGetOtherDBSet.set(inLJAGetOtherSet);
      if (!tLJAGetOtherDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJAGetOther Insert Failed");
        return false;
      }
      System.out.println("End 更新其他退费实付表 ...");

      //更新红利给付实付表
      LJABonusGetDBSet tLJABonusGetDBSet = new LJABonusGetDBSet(conn);
      tLJABonusGetDBSet.set(inLJABonusGetSet);
      if (!tLJABonusGetDBSet.update()) {
        try{ conn.rollback() ;} catch(Exception e){}
        conn.close();
        System.out.println("LJABonusGet Insert Failed");
        return false;
      }
      System.out.println("End 更新红利给付实付表 ...");
      
      LLCaseDBSet mLLCaseDBSet=new LLCaseDBSet(conn);
      mLLCaseDBSet.set((LLCaseSet)mInputData.getObjectByObjectName("LLCaseSet",0));
      if(mLLCaseDBSet!=null)
      {
         if (!mLLCaseDBSet.update())
         {
                  // @@错误处理
          this.mErrors.copyAllErrors(mLLCaseDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "OperFinFeeGetBLS";
          tError.functionName = "save";
          tError.errorMessage = "更新 赔付实付表失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback() ;
          conn.close();
          return false;
        }
      }
      
      LLRegisterDBSet mLLRegisterDBSet=new LLRegisterDBSet(conn);
      mLLRegisterDBSet.set((LLRegisterSet)mInputData.getObjectByObjectName("LLRegisterSet",0));
      if(mLLRegisterDBSet!=null)
      {
         if (!mLLRegisterDBSet.update())
         {
                  // @@错误处理
          this.mErrors.copyAllErrors(mLLRegisterDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "OperFinFeeGetBLS";
          tError.functionName = "save";
          tError.errorMessage = "更新 赔付实付表失败!";
          this.mErrors .addOneError(tError) ;
          conn.rollback() ;
          conn.close();
          return false;
        }
      }
      
      conn.commit();
      conn.close();
      System.out.println("End Committed");
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "PayReturnFromBankBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try{ conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }

  public static void main(String[] args) {
    //PayReturnFromBankBLS payReturnFromBankBLS1 = new PayReturnFromBankBLS();
  }
}