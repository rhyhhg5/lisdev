package com.sinosoft.lis.bank;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.Signature;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Enumeration;

/**
 * 使用文件证书进行签名、验签的代理类
 */
public class FileKeyManager  {

	private static final String ALGORITHM = "SHA1withRSA";
	
	String m_strSigneFilePath, m_strVerifyFilePath, m_strSigneFilePassword;
	String lastErrMsg;
	
	/** 获取签名文件证书路径 */
	public String getSigneFilePath() {
		return m_strSigneFilePath;
	}

	/** 设置签名文件证书路径 */
	public void setSigneFilePath(String signeFilePath) {
		m_strSigneFilePath = signeFilePath;
	}

	/** 获取签名文件证书密码 */
	public String getSigneFilePassword() {
		return m_strSigneFilePassword;
	}

	/** 设置签名文件证书密码 */
	public void setSigneFilePassword(String signeFilePassword) {
		m_strSigneFilePassword = signeFilePassword;
	}
	
	/** 获取验签文件证书路径 */
	public String getVerifyFilePath() {
		return m_strVerifyFilePath;
	}

	/** 设置验签文件证书路径 */
	public void setVerifyPath(String verifyFilePath) {
		m_strVerifyFilePath = verifyFilePath;
	}

	/**
	 * 获取上一次错误信息
	 */
	public String getLastErrMsg(){
		return lastErrMsg;
	}
	
	/**
	 * 初始化文件证书管理类
	 * @param signeFilePath		签名证书路径
	 * @param signeFilePassword	签名证书密码
	 * @param verifyFilePath	验签证书路径
	 */
	public FileKeyManager(String signeFilePath, String signeFilePassword, String verifyFilePath){
		setSigneFilePath(signeFilePath);
		setSigneFilePassword(signeFilePassword);
		setVerifyPath(verifyFilePath);
	}
	
	/**
	 * 对给定数据进行签名
	 * @param srcMsg		给定数据
	 * @return				签名摘要
	 * @throws Exception
	 */
	public byte[] signeMsg(byte[] srcMsg) throws Exception {
		byte[] result = null;
		FileInputStream fiKeyFile = null;
		try {
			this.lastErrMsg = "";
			KeyStore ks = KeyStore.getInstance("PKCS12");
			fiKeyFile = new FileInputStream(getSigneFilePath());
			ks.load(fiKeyFile, getSigneFilePassword().toCharArray());
			Enumeration myEnum = ks.aliases();
			String keyAlias = null;
			RSAPrivateCrtKey prikey = null;
			// keyAlias = (String) myEnum.nextElement();
			/* IBM JDK必须使用While循环取最后一个别名，才能得到个人私钥别名 */
			while (myEnum.hasMoreElements()) {
				keyAlias = (String) myEnum.nextElement();
				if (ks.isKeyEntry(keyAlias)) {
					prikey = (RSAPrivateCrtKey) ks.getKey(keyAlias, getSigneFilePassword().toCharArray());
					break;
				}
			}
			if (prikey == null) {
				this.lastErrMsg = "Error Description: ER_PRIKEY_CANNOT_FOUND（没有找到匹配私钥）";
				result = null;
			} else {
				Signature sign = Signature.getInstance(ALGORITHM);
				sign.initSign(prikey);
				sign.update(srcMsg);
				result = sign.sign();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			this.lastErrMsg = "Error Description: ER_FIND_CERT_FAILED（找不到证书）";
			result = null;
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
			this.lastErrMsg = "Error Description: ER_PRIKEY_CANNOT_FOUND（没有找到匹配私钥） | Exception:"
					+ e.getMessage();
			result = null;
		} catch (IOException e) {
			e.printStackTrace();
			this.lastErrMsg = "Error Description: ER_PRIKEY_CANNOT_FOUND（没有找到匹配私钥） | Exception:"
					+ e.getMessage();
			result = null;
		} catch (Exception e) {
			e.printStackTrace();
			this.lastErrMsg = "Error Description: ER_SIGN_ERROR（签名失败）"
					+ e.toString() + "| Exception:" + e.getMessage();
			result = null;
		} finally {
			try {
				if (fiKeyFile == null) {
					fiKeyFile.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				this.lastErrMsg = "Error Description: ER_CLOSEFILE_ERROR（证书文件关闭失败）";
				result = null;
			}
		}
		return result;
	}

	/**
	 * 验证签名
	 * @param srcMsg	待验签原文（不包含签名摘要）
	 * @param digest	签名摘要
	 * @return			验签结果
	 * @throws Exception
	 */
	public boolean verifyMsg(byte[] srcMsg, byte[] digest) throws Exception {
		boolean result = false;
		FileInputStream certfile = null;
		try {
			certfile = new FileInputStream(getVerifyFilePath());
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			X509Certificate x509cert = (X509Certificate) cf
					.generateCertificate(certfile);
			RSAPublicKey pubkey = (RSAPublicKey) x509cert.getPublicKey();

			Signature verify = Signature.getInstance(ALGORITHM);
			verify.initVerify(pubkey);
			verify.update(srcMsg);
			if (verify.verify(digest)) {
				result = true;
			} else {
				this.lastErrMsg = "Error Description:ER_VERIFY_ERROR（验签失败）";
				result = false;
			}

		} catch (CertificateException e) {
			e.printStackTrace();
			this.lastErrMsg = "Error Description: ER_CERT_PARSE_ERROR（证书解析错误）";
			result = false;
		} catch (Exception e) {
			e.printStackTrace();
			this.lastErrMsg = "Error Description: ER_VERIFY_ERROR（验签失败）";
			result = false;
		} finally {
			try {
				if (certfile == null) {
					certfile.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				this.lastErrMsg = "Error Description: ER_VERIFY_ERROR（验签失败）";
				result = false;
			}
		}
		return result;
	}

}
