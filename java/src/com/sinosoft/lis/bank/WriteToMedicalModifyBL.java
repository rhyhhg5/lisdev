package com.sinosoft.lis.bank;

import java.lang.reflect.Method;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * 银行接口Excel格式生成类 需要配置相应的配置xml，具体格式看程序吧。。
 * 
 * @author 
 * 
 */
public class WriteToMedicalModifyBL {

	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 错误处理类 */
	private CErrors mErrors = new CErrors();

	/** 前台传入的公共变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 生成文件路径 */
	private String mPath = "";
	
	/** 生成文件名 */
	private String mFileName = "";

	/** 格式表头数组 */
	private String[] mTitleArr;

	/** 格式内容数组 */
	private String[] mDataArr;

	// 业务数据
	private LYBankLogSchema mLYBankLogSchema = new LYBankLogSchema();

	private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();

	/**
	 * 入口函数，sino都知道
	 * 
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");

		if (!prepareOutputData()) {
			return false;
		}
		System.out.println("---End prepareOutputData---");

		PubSubmit tPubSubmit = new PubSubmit();

		// 如果有需要处理的错误，则返回
		if (!tPubSubmit.submitData(this.mInputData, "")) {
			// @@错误处理
			CError.buildErr(this, "--数据提交失败--");
			mErrors.addOneError("数据提交失败");
			return false;
		}

		System.out.println("---End ReadFromExcel---");

		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			mLYBankLogSchema = (LYBankLogSchema) mInputData
					.getObjectByObjectName("LYBankLogSchema", 0);
			mPath = (String) mInputData.getObjectByObjectName("String", 0);
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);
			if (mLYBankLogSchema == null
					|| mLYBankLogSchema.getSerialNo() == null
					|| "".equals(mLYBankLogSchema.getSerialNo())) {
				// @@错误处理
				CError.buildErr(this, "接收数据失败");
				mErrors.addOneError("接收数据失败");
				return false;
			}
			if (mPath == null || "".equals(mPath)) {
				// @@错误处理
				CError.buildErr(this, "接收数据失败");
				mErrors.addOneError("接收数据失败");
				return false;
			}
			if (mGlobalInput == null) {
				// @@错误处理
				CError.buildErr(this, "接收数据失败");
				mErrors.addOneError("接收数据失败");
				return false;
			}
		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			CError.buildErr(this, "接收数据失败");
			mErrors.addOneError("接收数据失败");
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		try {
			// 获取LYBankLog表信息
			LYBankLogDB mLYBankLogDB = mLYBankLogSchema.getDB();
			if (!mLYBankLogDB.getInfo()) {
				// @@错误处理
				CError.buildErr(this, "获取批次信息失败");
				mErrors.addOneError("获取批次信息失败");
				return false;
			}

			mLYBankLogSchema = mLYBankLogDB.getSchema();

			// 获取LYSendToBank表信息
			LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
			tLYSendToBankDB.setSerialNo(mLYBankLogDB.getSerialNo());

			mLYSendToBankSet = tLYSendToBankDB.query();

			if (mLYSendToBankSet == null || mLYSendToBankSet.size() == 0) {
				// @@错误处理
				CError.buildErr(this, "获取发盘明细数据失败");
				mErrors.addOneError("获取发盘明细数据失败");
				return false;
			}

			// 获取格式配置路径
			String xmlPath = getXMLPath(mLYBankLogDB.getBankCode(),
					mLYBankLogDB.getLogType());

			if (xmlPath == null || "".equals(xmlPath)) {
				// @@错误处理
				CError.buildErr(this, "获取格式失败");
				mErrors.addOneError("获取格式失败");
				return false;
			}

			// 读取格式文件，获取格式信息
			if (!readXML(xmlPath)) {
				return false;
			}

			// 创建发盘Excel文件
			if (!creatExcel(mPath)) {
				return false;
			}

		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			CError.buildErr(this, "数据处理错误" + ex.getMessage());
			mErrors.addOneError("数据处理错误");
			return false;
		}
		return true;
	}

	/**
	 * 获取银行格式路径
	 * 
	 * @param bankCode
	 *            银行编码
	 * @param type
	 *            收付费类型
	 * @return
	 */
	private String getXMLPath(String bankCode, String type) {

		
		String sql = "select agentpaysendf from ldmedicalcom where medicalcomcode='"
				+ bankCode + "'";

		ExeSQL tExeSQL = new ExeSQL();
		String xmlpath = tExeSQL.getOneValue(sql);
//		xmlpath = "F:\\picch\\ui\\bank\\SendToBankFile\\xsl\\send_tcyibao.xml";
		return xmlpath;
	}

	/**
	 * 解析格式xml配置文件
	 * 
	 * @param xmlPath
	 *            格式路径 路径需要配置在LDBank表中
	 * @return
	 */
	private boolean readXML(String xmlPath) {

		System.out.println("--开始读取格式文件--" + xmlPath);
		SAXBuilder builder = new SAXBuilder(false);

		try {

			Document doc = builder.build(xmlPath);
			Element root = doc.getRootElement();

			// Excel 表头配置信息
			Element title = root.getChild("TITLE");
			List titleCol = title.getChildren();

			mTitleArr = new String[titleCol.size()];

			for (int col = 0; col < titleCol.size(); col++) {
				Element eCol = (Element) titleCol.get(col);
				mTitleArr[col] = eCol.getText();
				System.out.println("title-" + col + "-" + mTitleArr[col]);
			}

			// Excel 内容配置信息
			Element row = root.getChild("ROW");
			List dateCol = row.getChildren();

			mDataArr = new String[dateCol.size()];

			for (int col = 0; col < dateCol.size(); col++) {
				Element eCol = (Element) dateCol.get(col);
				mDataArr[col] = eCol.getText();
				System.out.println("date-" + col + "-" + mDataArr[col]);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			CError.buildErr(this, "格式读取处理失败: " + ex.getMessage());
			mErrors.addOneError("格式读取处理失败");
			return false;
		}
		return true;
	}

	/**
	 * 根据配置信息，创建Excel发盘文件
	 * 
	 * @param path
	 *            生成文件路径
	 * @return
	 */
	private boolean creatExcel(String path) {
		
		mFileName = "B" + mLYBankLogSchema.getBankCode() + "S" + mLYBankLogSchema.getSerialNo() + "("+ PubFun.getCurrentDate() + ").xls";
		
		System.out.println("--开始生成Excel发盘--路径--" + path + mFileName);

		String[][] mToExcel = new String[mLYSendToBankSet.size() + 1][mDataArr.length];

		for (int col = 0; col < mTitleArr.length && col < mDataArr.length; col++) {
			mToExcel[0][col] = mTitleArr[col];
		}
		for (int row = 0; row < mLYSendToBankSet.size(); row++) {

			LYSendToBankSchema tLYSendToBankSchema = mLYSendToBankSet
					.get(row + 1);

			for (int col = 0; col < mTitleArr.length && col < mDataArr.length; col++) {

				mToExcel[row + 1][col] = transXML(mDataArr[col],
						tLYSendToBankSchema);
				
				if("AgentCode".equals(mDataArr[col])){
					//统一工号，修改业务原编码
					//将AgentCode转换为GroupAgentCode
			        //统一工号修改
			        String SQL = "select getUniteCode("+tLYSendToBankSchema.getAgentCode()+") from dual where 1=1 with ur";
			        ExeSQL aExeSQL = new ExeSQL();
			        SSRS aSSRS = new SSRS();
			        aSSRS = aExeSQL.execSQL(SQL);
				  	if (aSSRS != null && aSSRS.getMaxRow() > 0) {
				  		mToExcel[row + 1][0] = aSSRS.GetText(1, 1);
				  	}
				}
							
			}	
		}

		try {
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate2() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(path + mFileName);
			System.out.println("--生成文件完毕--");
		} catch (Exception ex) {
			ex.printStackTrace();
			CError.buildErr(this, "生成发盘文件失败: " + ex.getMessage());
			mErrors.addOneError("生成发盘文件失败");
			return false;
		}
		return true;
	}

	/**
	 * 匹配转换发盘信息 <br>
	 * 其中： <br>
	 * str--指定信息 <br>
	 * oth--特殊信息 会调取SendMedicalQuery对应函数进行获取 <br>
	 * 其他--直接取LYSendToBank相应字段值 <br>
	 * 
	 * @param value
	 *            配置字段信息
	 * @param tLYSendToBankSchema
	 * @return
	 */
	private String transXML(String value, LYSendToBankSchema tLYSendToBankSchema) {

		if (value.length() > 3 && "str:".equals(value.substring(0, 4))) {
			// 直接取 str: 后面的信息
			return value.substring(4);
		} else

		if (value.length() > 3 && ("oth:".equals(value.substring(0, 4)) || "oth:".equals(value.substring(0, 4)))) {
			try {
				// 使用反射机制调用
				// 调用的类
				Class clas = Class
						.forName("com.sinosoft.lis.bank.SendMedicalQuery");

				// 设置函数的参数类型 下面获取函数的时候会用到
				Class[] parameter = new Class[1];
				parameter[0] = LYSendToBankSchema.class;

				// 调用的函数 两个参数--函数名，函数参数列表
				Method method = clas.getMethod("get" + value.substring(4),
						parameter);

				// 调用函数的时候参数的值
				LYSendToBankSchema[] parameteres = new LYSendToBankSchema[1];
				parameteres[0] = tLYSendToBankSchema;

				Object obj = method.invoke(clas.newInstance(), parameteres);
				// 调用函数
				return obj == null ? null : obj.toString();

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		return tLYSendToBankSchema.getV(value);
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			mInputData.add(this.mGlobalInput);
			MMap tMap = new MMap();

			mLYBankLogSchema.setOutFile(mFileName);
			mLYBankLogSchema.setSendDate(PubFun.getCurrentDate());
			mLYBankLogSchema.setSendOperator(mGlobalInput.Operator);
			mLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
			mLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());

			tMap.put(mLYBankLogSchema, "UPDATE");
			mInputData.add(tMap);

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 获取异常信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}
}
