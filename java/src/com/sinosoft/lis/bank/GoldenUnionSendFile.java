package com.sinosoft.lis.bank;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;

import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GoldenUnionSendFile {

	private String mUrl = "";

    private String feeType = "";

    private String ubank = "";
    
    private String filePath = "";
    
    private String reqStr = null;
    
    private String YJ = "";

    private MMap map = new MMap();

    private VData mInputData = new VData();
    
    private LDCodeSchema ldcodeSchema = new LDCodeSchema();
    
    /**
     * @param mUrl 报文存放路径
     * @param feeType 收付费标识
     * @param ubank 银行编码
     */
    public boolean submitData(String mUrl, String feeType, String ubank,String YJ){
    	this.mUrl = mUrl;
        this.feeType = feeType;
        this.ubank = ubank;
        this.YJ = YJ;

        if (!dealDate()){
            System.out.println("获取数据失败");
            return false;
        }

        if (!prepareOutputData()){
            System.out.println("准备更新日志数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE")){
            //错误处理
            System.out.println("更新发盘日志失败");
            return false;
        }

        return true;
    }
    
    private boolean prepareOutputData(){
        try{
            mInputData.clear();
            mInputData.add(map);
        }catch (Exception ex){
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }
    
    public boolean dealDate(){
    	String sql = "";
    	if("F".equals(feeType)){
    		
    		if("YJ".equals(YJ)){
    		 sql = "select * from lybanklog a where (a.dealstate is null or a.dealstate='') and a.senddate is null and a.bankcode in ("
                     + ubank
                     + ") and a.logtype='"
                     + feeType
                     + "' "
                     + "and a.serialno in (select lys.serialno from lysendtobank lys , ljaget lja where lys.paycode=lja.actugetno and lja.othernotype = '24') "
                     + "and (select count(*) from lysendtobank where serialno=a.serialno)>0 with ur";
    		
    	}else {
    		  sql = "select * from lybanklog a where (a.dealstate is null or a.dealstate='') and a.senddate is null and a.bankcode in ("
                      + ubank
                      + ") and logtype='"
                      + feeType
                      + "' "
                      + "and a.serialno not in (select lys.serialno from lysendtobank lys , ljaget lja where lys.paycode=lja.actugetno and lja.othernotype ='24') "
                      + "and (select count(*) from lysendtobank where serialno=a.serialno)>0 with ur";
    	} 
    	} else {
    		
    		sql = "select * from lybanklog a where (a.dealstate is null or a.dealstate='') and a.senddate is null and a.bankcode in ("
                    + ubank
                    + ") and logtype='"
                    + feeType
                    + "' "
                    + "and (select count(*) from lysendtobank where serialno=a.serialno)>0 with ur";
    	}
       
        System.out.println(sql);
        LYBankLogDB tLyBankLogDB = new LYBankLogDB();
        LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
        if (tLYBankLogSet == null){
            System.out.println("未查到发送银行数据");
            return false;
        }
        for (int i = 1; i <= tLYBankLogSet.size(); i++){
            LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
            String serialno = tLYBankLogSchema.getSerialNo();
            sql = "select * from lysendtobank where serialno='"+serialno+"' with ur";
            System.out.println(sql);
            LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
            LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB.executeQuery(sql);
            
            // 判断批次下是否有异常的付费数据（并发数据）
            if("F".equals(feeType) && !checkLJFIGet(serialno, tLYBankLogSchema)){
            	// 若有问题，停止该批次的发送，并发送邮件
            	continue;
            }
            // 判断批次下是否有异常的收费数据（并发数据）
            if("S".equals(feeType) && !checkLysendtobank(serialno, tLYBankLogSchema)){
            	// 若有问题，停止该批次的发送，并发送邮件
            	continue;
            }
            if (!createFile(tLYBankLogSchema, tLYSendToBankSet, feeType)){
            	continue;
            }
        }
        return true;
    }
   /**
    * 每天的发盘批次号不能重复
    * @return 汇总批次号
    */
    public String getRandom(){
    	
    	String sqlurl = "select * from ldcode where codetype ='RandomDate' with ur";
    	System.out.println(sqlurl);
    	LDCodeDB ldcodeDB = new LDCodeDB();
    	LDCodeSet ldcodeSet = ldcodeDB.executeQuery(sqlurl);
    	LDCodeSet tldcodeSet = new LDCodeSet();
    	String returnString = "";
    	if(ldcodeSet.size()==0){
    		ldcodeSchema.setCode("1");
    		ldcodeSchema.setCodeType("RandomDate");
    		ldcodeSchema.setCodeAlias(PubFun.getCurrentDate());
    		ldcodeSchema.setCodeName("1");
    		tldcodeSet.add(ldcodeSchema);
            MMap map = new MMap();
            map.put(tldcodeSet, "INSERT");
            VData tVData = new VData();
            tVData.add(map);
            System.out.println("Start PubSubmit BLS Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if(!tPubSubmit.submitData(tVData, "INSERT")){
            	System.out.println("提交数据库出错...");
            	return "";
            }
            returnString="1";
    	} else {
    	for(int i = 1;i <= ldcodeSet.size();i++){
    		
    		ldcodeSchema = ldcodeSet.get(i);
    		String code =ldcodeSchema.getCodeName();
    		String date = ldcodeSchema.getCodeAlias();
    		if(PubFun.getCurrentDate().equals(date)){
    			
    			ldcodeSchema.setCodeAlias(PubFun.getCurrentDate());
        		ldcodeSchema.setCodeName(String.valueOf(Integer.parseInt(code) + 1));
        		ldcodeSchema.setCodeType("RandomDate");
        		ldcodeSchema.setCode("1");
        		tldcodeSet.add(ldcodeSchema);
                MMap map = new MMap();
                map.put(tldcodeSet, "UPDATE");
                
                VData tVData = new VData();
                tVData.add(map);
                System.out.println("Start PubSubmit BLS Submit...");
                PubSubmit tPubSubmit = new PubSubmit();
                if(!tPubSubmit.submitData(tVData, "UPDATE")){
                	System.out.println("提交数据库出错...");
                	return "";
                }
    		} else {
    			ldcodeSchema.setCodeAlias(PubFun.getCurrentDate());
        		ldcodeSchema.setCodeName("1");
        		ldcodeSchema.setCodeType("RandomDate");
        		ldcodeSchema.setCode("1");
        		tldcodeSet.add(ldcodeSchema);
                MMap map = new MMap();
                map.put(tldcodeSet, "UPDATE");
                VData tVData = new VData();
                tVData.add(map);
                System.out.println("Start PubSubmit BLS Submit...");
                PubSubmit tPubSubmit = new PubSubmit();
                if(!tPubSubmit.submitData(tVData, "UPDATE")){
                	System.out.println("提交数据库出错...");
                	return "";
                }
    		}
    		
    		
		    String sqlurl1 = "select * from ldcode where codetype ='RandomDate' with ur";
		    ldcodeSet.clear();
    		ldcodeSet = ldcodeDB.executeQuery(sqlurl1);
    		returnString = ldcodeSet.get(1).getCodeName();
    		
    	}
    	
    	}
    	
    	return returnString;
    }
    
    /**
     * 获取银行行名
     * @return 银行行名
     */
    public String getBankName(String bankcode){
    	
    	String sql="select unitebankname from ldbankunite where bankunitecode ='7707' and bankcode ='"+bankcode +"'";
    	String bankname = new ExeSQL().getOneValue(sql);
    	return bankname;
    }
    /**
     * 获取企业编码
     * @return 企业编码
     */
    public String getCompanyID(){
    	
    	String sql="select codename from ldcode1 where codetype ='CompanyID' and code ='7707'";
    	String CompanyID = new ExeSQL().getOneValue(sql);
    	return CompanyID;
    }
    /**
     * 获取费项代码
     * @return 费项代码
     */
    public String getExpenditureID(String type){
    	
    	String sql="select codename from ldcode1 where codetype ='ExpenditureID' and code ='7707' and  code1 ='"+type +"'";
    	String ExpenditureID = new ExeSQL().getOneValue(sql);
    	return ExpenditureID;
    }
    /**
     * 获取开户行号
     * @return 开户行号
     */
    public String getBankAccID(){
    	
    	String sql="select codename from ldcode1 where codetype ='BankAccID' and code ='7707'";
    	String BankAccID = new ExeSQL().getOneValue(sql);
    	return BankAccID;
    }
    /**
     * 获取账号
     * @return 账号
     */
    public String getBankaccNo(String type){
    	
    	String sql="select codename from ldcode1 where codetype ='BankaccNo' and code ='7707' and code1 ='"+type +"'";
    	String BankaccNo = new ExeSQL().getOneValue(sql);
    	return BankaccNo;
    }
    
    //对于double型的数据，超过10位非科学记数法表示，正常显示
    public static String formatFloatNumber(double value) {
        if(value != 0.00){
            java.text.DecimalFormat df = new java.text.DecimalFormat("########.00");
            return df.format(value);
        }else{
            return "0.00";
        }

    }
    
    private boolean createFile(LYBankLogSchema tLYBankLogSchema,LYSendToBankSet tLYSendToBankSet, String feeType){
    	
        this.filePath = this.mUrl + "/"+ tLYBankLogSchema.getSerialNo() + ".txt";
        System.out.println("全部路径为："+filePath);
        //以下为测试用
        File file = new File(filePath);
        FileWriter fw = null;
        StringBuffer sbf = new StringBuffer();
        String str = null;
        String RandomBatchID = getRandom();
        
        try{
        	fw = new FileWriter(file, true);
        	
        	if("S".equals(feeType)){
        		//--【汇总行信息】--业务类型|企业编码|费项代码|总笔数|总金额|开户行号|账号|批次序号
        		fw.append("40502|").append(getCompanyID()+"|")
          	  .append(getExpenditureID(feeType)+"|")
          	  .append(tLYBankLogSchema.getTotalNum()+"|")
          	  .append(formatFloatNumber(tLYBankLogSchema.getTotalMoney())+"|")
          	  .append(getBankAccID()+"|").append(getBankaccNo(feeType)+"|")
          	  .append( RandomBatchID).append("\r\n");
          	
          	sbf.append("40502|").append(getCompanyID()+"|")
               .append(getExpenditureID(feeType)+"|")
    	       .append(tLYBankLogSchema.getTotalNum()+"|")
    	       .append(formatFloatNumber(tLYBankLogSchema.getTotalMoney())+"|")
    	       .append(getBankAccID()+"|").append(getBankaccNo(feeType)+"|")
    	       .append( RandomBatchID).append("\r\n");
          	//--【明细】-----明细序号|行号|行名|对方账号|户名|金额(元)|协议号|备注|手机号|流水号|省份|城市|分支机构
          	for(int i = 1; i<=tLYSendToBankSet.size(); i++){
          		LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(i);
          		
  				fw.append(i+"|").append(" |")
  				  .append(getBankName(tLYSendToBankSchema.getBankCode()) + "|")
  				  .append(tLYSendToBankSchema.getAccNo()+"|")
  				  .append(tLYSendToBankSchema.getAccName()+"|")
  				  .append(formatFloatNumber(tLYSendToBankSchema.getPayMoney())+"|")
  				  
  				  .append(" |").append(tLYSendToBankSchema.getPayCode()+"|").append(" |")
  				  .append(tLYSendToBankSchema.getPayCode()+"|").append(" |").append(" |").append(" ").append("|");
  				
  				sbf.append(i+"|").append(" |")
				  .append(getBankName(tLYSendToBankSchema.getBankCode()) + "|")
				  .append(tLYSendToBankSchema.getAccNo()+"|")
				  .append(tLYSendToBankSchema.getAccName()+"|")
				  .append(formatFloatNumber(tLYSendToBankSchema.getPayMoney())+"|")
				  .append(" |").append(tLYSendToBankSchema.getPayCode()+"|").append(" |")
				  .append(tLYSendToBankSchema.getPayCode()+"|").append(" |").append(" |").append(" ").append("|");
  				
  				if(i != tLYSendToBankSet.size()){
  					fw.write("\r\n");
  					sbf.append("\r\n");
  				}
  				
  				fw.flush();
  			}
        		
        	}
        	if("F".equals(feeType)){
        		if("YJ".equals(YJ)){

            		//--【汇总行信息】--业务类型|企业编码|费项代码|总笔数|总金额|开户行号|账号|批次序号
            	fw.append("40501|").append(getCompanyID()+"|")
            	  .append(getExpenditureID(feeType)+"|")//金联的付费
              	  .append(tLYBankLogSchema.getTotalNum()+"|")
              	  .append(formatFloatNumber(tLYBankLogSchema.getTotalMoney())+"|")
              	  .append(getBankAccID()+"|").append(getBankaccNo("YJF")+"|")
              	  .append( RandomBatchID).append("\r\n");
              	
            	sbf.append("40501|").append(getCompanyID()+"|")
              	  .append(getExpenditureID(feeType)+"|")
             	   .append(tLYBankLogSchema.getTotalNum()+"|")
             	   .append(formatFloatNumber(tLYBankLogSchema.getTotalMoney())+"|")
             	   .append(getBankAccID()+"|").append(getBankaccNo("YJF")+"|")
             	   .append( RandomBatchID).append("\r\n");
        		}else{

            		//--【汇总行信息】--业务类型|企业编码|费项代码|总笔数|总金额|开户行号|账号|批次序号
            	fw.append("40501|").append(getCompanyID()+"|")
            	  .append(getExpenditureID(feeType)+"|")//金联的付费
              	  .append(tLYBankLogSchema.getTotalNum()+"|")
              	  .append(formatFloatNumber(tLYBankLogSchema.getTotalMoney())+"|")
              	  .append(getBankAccID()+"|").append(getBankaccNo(feeType)+"|")
              	  .append( RandomBatchID).append("\r\n");
              	
            	sbf.append("40501|").append(getCompanyID()+"|")
              	  .append(getExpenditureID(feeType)+"|")
             	   .append(tLYBankLogSchema.getTotalNum()+"|")
             	   .append(formatFloatNumber(tLYBankLogSchema.getTotalMoney())+"|")
             	   .append(getBankAccID()+"|").append(getBankaccNo(feeType)+"|")
             	   .append( RandomBatchID).append("\r\n");
        		}
/*
        		//--【汇总行信息】--业务类型|企业编码|费项代码|总笔数|总金额|开户行号|账号|批次序号
        	fw.append("40501|").append(getCompanyID()+"|")
        	  .append(getExpenditureID(feeType)+"|")//金联的付费
          	  .append(tLYBankLogSchema.getTotalNum()+"|")
          	  .append(formatFloatNumber(tLYBankLogSchema.getTotalMoney())+"|")
          	  .append(getBankAccID()+"|").append(getBankaccNo(feeType)+"|")
          	  .append( RandomBatchID).append("\r\n");
          	
        	sbf.append("40501|").append(getCompanyID()+"|")
          	  .append(getExpenditureID(feeType)+"|")
         	   .append(tLYBankLogSchema.getTotalNum()+"|")
         	   .append(formatFloatNumber(tLYBankLogSchema.getTotalMoney())+"|")
         	   .append(getBankAccID()+"|").append(getBankaccNo(feeType)+"|")
         	   .append( RandomBatchID).append("\r\n");*/
          	  //--【明细】
        	//--明细序号|行号|行名|对方账号|户名|金额(元)|备注|手机号|流水号|省份|城市|分支机构
          	for(int i = 1; i<=tLYSendToBankSet.size(); i++){
          		LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(i);
          		
  				fw.append(i+"|").append(" |")
  				  .append(getBankName(tLYSendToBankSchema.getBankCode()) + "|")
  				  .append(tLYSendToBankSchema.getAccNo()+"|")
  				  .append(tLYSendToBankSchema.getAccName()+"|")
  				  .append(formatFloatNumber(tLYSendToBankSchema.getPayMoney())+"|")
  				  .append("备注|").append(" |").append(tLYSendToBankSchema.getPayCode()+"|").append(" |")
  				  .append(" | ").append("|");
  				
  				sbf.append(i+"|").append(" |")
				  .append(getBankName(tLYSendToBankSchema.getBankCode()) + "|")
				  .append(tLYSendToBankSchema.getAccNo()+"|")
				  .append(tLYSendToBankSchema.getAccName()+"|")
				  .append(formatFloatNumber(tLYSendToBankSchema.getPayMoney())+"|")
				  .append("备注|").append(" |").append(tLYSendToBankSchema.getPayCode()+"|").append(" |")
				  .append(" | ").append("|");
  				
  				if(i != tLYSendToBankSet.size()){
  					fw.write("\r\n");
  					sbf.append("\r\n");
  				}
  				
  				fw.flush();
  			}
        	}
        	
        	str = sbf.toString();
        	
        	System.out.println("发送的字符串为:" + str);
            System.out.println("开始发送文件:" + filePath);
			if (!"".equals(filePath)) {

				// 获取发送服务器的地址,需要配置= =
				String sqlurl = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code='GoldenUnionServerUrl' and code1='7707'";
				String url = new ExeSQL().getOneValue(sqlurl);
				reqStr = sendFile(filePath, "7707", url, str, feeType,YJ);
				
			}
			if(!reqStr.equals("") && !reqStr.equals(null) && !reqStr.startsWith("False")){
				//更新银行日志表
				updateLYBankLog(reqStr, tLYBankLogSchema);
			} else if(reqStr.equals("") || reqStr.equals(null) ){
				System.out.println("发送失败银行返回的批次号为空");
				//更新银行日志表为发送失败
				updateLYBankLog1(reqStr, tLYBankLogSchema);
				return false;
			} else {
				reqStr = reqStr.substring(5, reqStr.length());
				System.out.println("发送失败");
				//更新银行日志表为发送失败
				updateLYBankLog1(reqStr, tLYBankLogSchema);
				return false;
			}
            
        }catch(Exception e){
        	System.out.println("生成报送文件失败");
        	e.printStackTrace();
        	return false;
        }finally{
        	try{
				fw.close();
			}catch (IOException e) {
				e.printStackTrace();
			}
        }
        
    	return true;
    }
    
    
    /**
     * 更新日志表
     */
    private void updateLYBankLog1(String req, LYBankLogSchema tLYBankLogSchema)
    {
    		 tLYBankLogSchema.setOutFile("发送失败"+req);
             tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
             tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
             System.out.println("--发送失败--s_MSG=-1---");
             this.map.put(tLYBankLogSchema, "UPDATE");
    }
    
    /**
     * 更新正确日志表
     */
    private void updateLYBankLog(String req, LYBankLogSchema tLYBankLogSchema)
    {
    	tLYBankLogSchema.setOutFile("成功发送#" + this.filePath);
        tLYBankLogSchema.setSendDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        //更新批次号
        tLYBankLogSchema.setBatchID(req);
        System.out.println("成送发送");
        this.map.put(tLYBankLogSchema, "UPDATE");
             
    }
    
    /**
     * 校验批次下是否有异常的收费数据，对于异常数据会发送邮件
     * 
     * @return
     */
    private boolean checkLysendtobank(String serialno, LYBankLogSchema tLYBankLogSchema){
    	// 校验是否有异常付费数据
    	String sql = "select paycode,polno from lysendtobank sb where serialno='" 
    			+ serialno 
    			+ "' and  exists (select 1 from lysendtobank  where paycode=sb.paycode and serialno <>sb.serialno )"
    			+ " union all select paycode,polno from lysendtobank where serialno='"
    			+ serialno 
    			+ "' and  exists (select 1 from ljtempfee  where paycode=ljtempfee.tempfeeno and confmakedate is not null )" 
    			+ " union all select paycode,polno from lysendtobank sb where serialno='"
    			+ serialno 
    			+ "' and notype='9' and exists (select 1 from lccont where prtno=sb.polno and appntname<>sb.accname) "
    			+ " union select paycode,polno from lysendtobank sb where serialno='"
    			+ serialno 
    			+ "' and exists (select 1 from ljtempfeeclass where sb.paycode=tempfeeno and paymode<>'4' ) "
    			;
    	ExeSQL tEx = new ExeSQL();
    	SSRS tSSRS = tEx.execSQL(sql);
    	if(tSSRS.getMaxRow() == 0){
    		return true;
    	}else {
    		// 批次没有发送过邮件
    		// 发送邮件
    		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
					.GetText(1, 2), "picchealth");
    		// 设置邮件发送信息
    		String sendInf = "金联万家:" + serialno + "批次出现异常收费数据，已停止该批次的发送，请核查批次内信息。";
    		for(int row = 1; row <= tSSRS.getMaxRow(); row++){
    			sendInf = sendInf + "收费号：" + tSSRS.GetText(row, 1) + "，业务号:" + tSSRS.GetText(row, 2) + "。";
    		}
    		tMailSender.setSendInf("金联万家异常批次", sendInf, null);
    		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
    		tMailSender.setToAddress("7707");
    		if (!tMailSender.sendMail()) {
    			tMailSender.getErrorMessage();
    		}
    		// 更新日志表输出信息
    		tLYBankLogSchema.setOutFile("批次下存在异常收费数据，发送失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
            // 已发送邮件标记
            tLYBankLogSchema.setOthFlag("1");
            this.map.put(tLYBankLogSchema, "UPDATE");
    		return false;
    	}
    }
    
    /**
     * 校验批次下是否有异常的付费数据，对于异常数据会发送邮件
     * 
     * @return
     */
    private boolean checkLJFIGet(String serialno, LYBankLogSchema tLYBankLogSchema){
    	// 校验是否有异常付费数据
    	String sql = "select paycode,polno from lysendtobank where serialno='" 
    			+ serialno 
    			+ "' and exists (select 1 from ljfiget where actugetno=lysendtobank.paycode)"
    			+ "  union all select paycode,polno from lysendtobank ly where serialno='"
    			+ serialno 
    			+ "' and exists (select 1 from lysendtobank  where paycode=ly.paycode and serialno <>ly.serialno )";
    	ExeSQL tEx = new ExeSQL();
    	SSRS tSSRS = tEx.execSQL(sql);
    	if(tSSRS.getMaxRow() == 0){
    		return true;
    	}else {
    		// 批次没有发送过邮件
    		// 发送邮件
    		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
			SSRS tMSSRS = new ExeSQL().execSQL(tSQLMailSql);
			MailSender tMailSender = new MailSender(tMSSRS.GetText(1, 1), tMSSRS
					.GetText(1, 2), "picchealth");
    		// 设置邮件发送信息
    		String sendInf = "金联万家" + serialno + "批次出现异常付费数据，已停止该批次的发送，请核查批次内信息。";
    		for(int row = 1; row <= tSSRS.getMaxRow(); row++){
    			sendInf = sendInf + "付费号：" + tSSRS.GetText(row, 1) + "，业务号:" + tSSRS.GetText(row, 2) + "。";
    		}
    		tMailSender.setSendInf("金联万家异常批次", sendInf, null);
    		// 通过数据库配置发送，会自动查ldcode1表中的sendmail配置的数据
    		tMailSender.setToAddress("7707");
    		if (!tMailSender.sendMail()) {
    			tMailSender.getErrorMessage();
    		}
    		// 更新日志表输出信息
    		tLYBankLogSchema.setOutFile("批次下存在异常付费数据，发送失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
            // 已发送邮件标记
            tLYBankLogSchema.setOthFlag("1");
            this.map.put(tLYBankLogSchema, "UPDATE");
    		return false;
    	}
    }
    
    /**
	 * 将文件发送至前置机
	 * 
	 * @param filepath 发盘路径
	 * @param bankcode 第三方银行
	 * @param url 第三方银行地址
	 * @param str 发送的字符串
	 * @param feeType 收付費标识
	 * @return String 返回的批次号
	 */
    private String sendFile(String filepath,String bankcode,String url,String str, String feeType,String YJ){
    	
    	// 请求
    	HttpClient httpClient = new HttpClient();
    	// 前置机url
    	String sqlurl = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code ='PCIP'";
    	// 金联密码
    	String pcUrl1 = "";
    	// 金联用户名
    	String pcUrl2  = "";
    	if ("S".equals(feeType)){
    		// 金联密码
    		pcUrl1 = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code = 'UserPass' and code1='S7707'";
        	// 金联用户名
    		pcUrl2 = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code = 'MerchantID' and code1='S7707'";
    	} 
    	if ("F".equals(feeType)){
    		// 金联密码
    		pcUrl1 = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code = 'UserPass' and code1='F7707'";
        	// 金联用户名
    		pcUrl2 = "select codename from ldcode1 where codetype='GoldenUnionSendBank' and code = 'MerchantID' and code1='F7707'";
    	}
    	
    	String UserPass = new ExeSQL().getOneValue(pcUrl1);
    	String MerchantID = new ExeSQL().getOneValue(pcUrl2);
    	
    	String pcUrl = new ExeSQL().getOneValue(sqlurl);
    	PostMethod postMethod = new PostMethod(pcUrl);
    	// 设置编码
    	httpClient.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "GBK");
    	
    	postMethod.addParameter("bankcode", bankcode);
    	postMethod.addParameter("url", url);
    	postMethod.addParameter("filepath", filepath);
    	postMethod.addParameter("str",str);
    	postMethod.addParameter("feeType",feeType);
    	postMethod.addParameter("UserPass",UserPass);
    	postMethod.addParameter("MerchantID",MerchantID);
    	postMethod.addParameter("YJ",YJ);
    	
    	
    	String strResp = "";
    	try{
    		
    		long start = System.currentTimeMillis();
    		System.out.println("开始时间:"+ start);
    		// 执行getMethod
    		int statusCode = httpClient.executeMethod(postMethod);
    		// 失败
    		byte[] responseBody = postMethod.getResponseBody();
    		if(statusCode != HttpStatus.SC_OK) {
    			System.out.println("前置机连接失败！返回的连接状态为 ："+statusCode);
    			return "False前置机连接失败";
    		}else{
    			
    			// 读取内容
    			strResp = new String(responseBody, "GBK");
    			if(strResp!=""){
    			  System.out.println("服务器返回:" + strResp);
    			} else{
    				System.out.println("上传失败！服务器返回："+strResp);
    			}
    			
    			
    		}
    		System.out.println("耗时:" + (System.currentTimeMillis() - start));
    	}catch(Exception e){
    		e.printStackTrace();
    		return "";
    	}finally{
    		// 释放连接
    		postMethod.releaseConnection();
    	}
    	return strResp;
    }
    
    
   public static void main (String args[]){
	   GoldenUnionSendFile a = new GoldenUnionSendFile();
	   String aq = a.formatFloatNumber(12345678555555559.01);
	   System.out.println(aq);
	   System.out.println(12345678555555559.01);
   }
}
