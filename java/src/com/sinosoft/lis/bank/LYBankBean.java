/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.bank;
public class LYBankBean
{
	/** 收款账号 */
	private String bankaccno;
	/** 收款单位名称 */
	private String bankaccname;
	/** 付款账号/卡号 */
	private String accno;
	/** 付款单位名称 */
	private String accname;
	public double getPaymoney() {
		return paymoney;
	}
	public void setPaymoney(double paymoney) {
		this.paymoney = paymoney;
	}
	/** 金额 */
	private double paymoney;
	/** 网上银行状态 */
	private String state;
	/** 用户备注 */
	private String remark;
	public String getBankaccno() {
		return bankaccno;
	}
	public void setBankaccno(String bankaccno) {
		this.bankaccno = bankaccno;
	}
	public String getBankaccname() {
		return bankaccname;
	}
	public void setBankaccname(String bankaccname) {
		this.bankaccname = bankaccname;
	}
	public String getAccno() {
		return accno;
	}
	public void setAccno(String accno) {
		this.accno = accno;
	}
	public String getAccname() {
		return accname;
	}
	public void setAccname(String accname) {
		this.accname = accname;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
	
	

	