package com.sinosoft.lis.bank;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYReturnFromBankSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LYReturnFromBankSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * 银行接口Excel格式读取类 需要配置相应的配置xml，具体格式看程序吧。。
 * 
 * @author 张成轩
 * 
 */
public class ReadFromExcelBL {

	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 错误处理类 */
	private CErrors mErrors = new CErrors();

	/** 前台传入的公共变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 读取文件路径 */
	private String mPath = "";

	/** 格式内容数组 */
	private int[] mDataArr;

	// 业务数据
	private LYBankLogSchema mLYBankLogSchema = new LYBankLogSchema();

	private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();

	private LYReturnFromBankSet mLYReturnFromBankSet = new LYReturnFromBankSet();

	/**
	 * 入口函数，sino都知道
	 * 
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");

		if (!prepareOutputData()) {
			return false;
		}
		System.out.println("---End prepareOutputData---");

		PubSubmit tPubSubmit = new PubSubmit();

		// 如果有需要处理的错误，则返回
		if (!tPubSubmit.submitData(this.mInputData, "")) {
			// @@错误处理
			CError.buildErr(this, "--数据提交失败--");
			mErrors.addOneError("数据提交失败");
			return false;
		}

		System.out.println("---End ReadFromExcel---");

		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			mLYBankLogSchema = (LYBankLogSchema) mInputData
					.getObjectByObjectName("LYBankLogSchema", 0);
			mPath = (String) mInputData.getObjectByObjectName("String", 0);
			if (mLYBankLogSchema == null) {
				// @@错误处理
				CError.buildErr(this, "--接收数据失败--");
				mErrors.addOneError("接收数据失败");
				return false;
			}
			mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
					"GlobalInput", 0);
		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			CError.buildErr(this, "接收数据失败");
			mErrors.addOneError("接收数据失败");
			return false;
		}
		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		try {
			// 获取LYBankLog表信息
			LYBankLogDB mLYBankLogDB = mLYBankLogSchema.getDB();
			if (!mLYBankLogDB.getInfo()) {
				// @@错误处理
				CError.buildErr(this, "获取批次信息失败");
				mErrors.addOneError("获取批次信息失败");
				return false;
			}
			mLYBankLogSchema = mLYBankLogDB.getSchema();

			// 获取LYSendToBank表信息
			LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
			tLYSendToBankDB.setSerialNo(mLYBankLogDB.getSerialNo());

			mLYSendToBankSet = tLYSendToBankDB.query();

			if (mLYSendToBankSet == null || mLYSendToBankSet.size() == 0) {
				// @@错误处理
				CError.buildErr(this, "获取银行发盘明细数据失败");
				mErrors.addOneError("获取银行发盘明细数据失败");
				return false;
			}

			// 获取格式配置路径
			String xmlPath = getXMLPath(mLYBankLogDB.getBankCode(),
					mLYBankLogDB.getLogType());

			if (xmlPath == null || "".equals(xmlPath)) {
				// @@错误处理
				CError.buildErr(this, "获取银行格式失败");
				mErrors.addOneError("获取银行格式失败");
				return false;
			}

			// 读取格式文件，获取格式信息
			if (!readXML(xmlPath)) {
				return false;
			}

			// 读取回盘Excel文件
			if (!readExcel(mPath)) {
				return false;
			}

			if (!checkSchema()) {
				return false;
			}

		} catch (Exception ex) {
			// @@错误处理
			ex.printStackTrace();
			CError.buildErr(this, "数据处理错误" + ex.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * 获取银行格式路径
	 * 
	 * @param bankCode
	 *            银行编码
	 * @param type
	 *            收付费类型
	 * @return
	 */
	private String getXMLPath(String bankCode, String type) {

		String sqlPath = "F".equals(type) ? "agentgetreceivef"
				: "agentpayreceivef";

		String sql = "select " + sqlPath + " from ldbank where bankcode='"
				+ bankCode + "'";

		ExeSQL tExeSQL = new ExeSQL();
		String xmlpath = tExeSQL.getOneValue(sql);
		return xmlpath;
	}

	/**
	 * 解析格式xml配置文件
	 * 
	 * @param xmlPath
	 *            格式路径 路径需要配置在LDBank表中
	 * @return
	 */
	private boolean readXML(String xmlPath) {

		System.out.println("--开始读取格式文件--" + xmlPath);
		SAXBuilder builder = new SAXBuilder(false);
		System.out.println("deal");
		try {
System.out.println("任性");
			Document doc = builder.build(xmlPath);
			Element root = doc.getRootElement();
			System.out.println("--开始处理--" + doc);
			// Excel 内容配置信息
			mDataArr = new int[5];
				System.out.println("处理到这啦:"+doc.getRootElement());
			mDataArr[0] = Integer.parseInt(root.getChild("Title").getText());
			mDataArr[1] = Integer.parseInt(root.getChild("AccNo").getText());
			mDataArr[2] = Integer.parseInt(root.getChild("PayMoney").getText());
			System.out.println("熊猫:"+mDataArr[2]);
			mDataArr[3] = Integer.parseInt(root.getChild("PayCode").getText());
			mDataArr[4] = Integer.parseInt(root.getChild("BankSuccFlag")
					.getText());
			System.out.println("要结束啦:"+mDataArr[4]);
		} catch (Exception ex) {
			ex.printStackTrace();
			CError.buildErr(this, "银行格式读取处理失败: " + ex.getMessage());
			mErrors.addOneError("银行格式读取处理失败");
			return false;
		}
		return true;
	}

	/**
	 * 根据配置信息，读取Excel回盘文件
	 * 
	 * @param path
	 *            回盘文件路径
	 * @return
	 */
	private boolean readExcel(String path) {
		try {

			System.out.println("--回盘文件路径--" + path);

			BookModelImpl book = new BookModelImpl();
			book.read(path, new ReadParams());

			int tSheetNums = book.getNumSheets();
			System.out.println("sheet--" + tSheetNums);

			for (int sheetNum = 0; sheetNum < tSheetNums; sheetNum++) {
				book.setSheet(sheetNum);

				int tLastCol = book.getLastCol();
				int tLastRow = book.getLastRow();

				System.out.println("列数--" + tLastCol);
				System.out.println("行数--" + tLastRow);
				// 按列循环

				int title = mDataArr[0];
				int accNo = mDataArr[1];
				int payMoney = mDataArr[2];
				int payCode = mDataArr[3];
				int bankSuccFlag = mDataArr[4];

				for (int j = 0; j <= tLastRow; j++) {
					if (j < title && title != -1) {
						continue;
					}
					LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();
					if (accNo != -1) {
						tLYReturnFromBankSchema.setAccNo(book.getText(j,
								accNo - 1).trim());
					}

					if (payMoney != -1) {
						tLYReturnFromBankSchema.setPayMoney(book.getText(j,
								payMoney - 1).trim());
					}

					tLYReturnFromBankSchema.setPayCode(book.getText(j,
							payCode - 1).trim());

					tLYReturnFromBankSchema.setBankSuccFlag(book.getText(j,
							bankSuccFlag - 1).trim());

					if (!checkBankData(tLYReturnFromBankSchema)) {
						CError.buildErr(this, "获取回盘信息与发盘信息不一致，请检查批次回盘信息是否正常");
						mErrors.addOneError("获取回盘信息与发盘信息不一致，请检查批次回盘信息是否正常");
						return false;
					}

					mLYReturnFromBankSet.add(tLYReturnFromBankSchema);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			CError.buildErr(this, "获取回盘信息失败: " + ex.getMessage());
			mErrors.addOneError("获取回盘信息失败");
			return false;
		}

		return true;
	}

	private boolean checkBankData(LYReturnFromBankSchema tLYReturnFromBankSchema) {

		int indexAccNo = mDataArr[1];
		int indexPayMoney = mDataArr[2];

		String paycode = tLYReturnFromBankSchema.getPayCode();
		String accNo = tLYReturnFromBankSchema.getAccNo();
		double payMoney = tLYReturnFromBankSchema.getPayMoney();

		for (int row = 0; row < mLYSendToBankSet.size(); row++) {

			LYSendToBankSchema tLYSendToBankSchema = mLYSendToBankSet
					.get(row + 1);

			if (!paycode.equals(tLYSendToBankSchema.getPayCode())) {
				continue;
			}

			if (indexAccNo != -1
					&& !accNo.equals(tLYSendToBankSchema.getAccNo())) {
				CError.buildErr(this, "收/付费号：" + paycode + ",客户帐号与发盘数据不匹配，请核查");
				mErrors.addOneError("收/付费号：" + paycode + ",客户帐号与发盘数据不匹配，请核查");
				return false;
			}

			if (indexPayMoney != -1
					&& payMoney != tLYSendToBankSchema.getPayMoney()) {
				CError.buildErr(this, "收/付费号：" + paycode + ",金额与发盘数据不匹配，请核查");
				mErrors.addOneError("收/付费号：" + paycode + ",金额与发盘数据不匹配，请核查");
				return false;
			}
			return true;
		}
		CError.buildErr(this, "未找到收/付费号：" + paycode + "的相关发盘数据，请核查");
		mErrors.addOneError("未找到收/付费号：" + paycode + "的相关发盘数据，请核查");
		return false;
	}

	/**
	 * 校验Set准确性
	 * 
	 * @return
	 */
	private boolean checkSchema() {
		if (mLYReturnFromBankSet.size() != mLYSendToBankSet.size()) {
			CError.buildErr(this, "发盘数据与回盘文件中数据量不一致");
			mErrors.addOneError("发盘数据与回盘文件中数据量不一致");
			return false;
		}
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 
	 * @return
	 */
	private boolean prepareOutputData() {
		try {
			mInputData = new VData();
			mInputData.add(this.mGlobalInput);
			MMap tMap = new MMap();

			mLYBankLogSchema.setInFile(mPath);
			mLYBankLogSchema.setReturnDate(PubFun.getCurrentDate());
			mLYBankLogSchema.setReturnOperator(mGlobalInput.Operator);
			mLYBankLogSchema.setTransDate(PubFun.getCurrentDate());
			mLYBankLogSchema.setTransOperator(mGlobalInput.Operator);
			mLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
			mLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());

			for (int row = 1; row <= mLYReturnFromBankSet.size(); row++) {

				LYReturnFromBankSchema tLYReturnFromBankSchema = mLYReturnFromBankSet
						.get(row);
				String payCode = tLYReturnFromBankSchema.getPayCode();

				for (int sRow = 1; sRow <= mLYSendToBankSet.size(); sRow++) {

					LYSendToBankSchema tLYSendToBankSetSchema = mLYSendToBankSet
							.get(row);

					if (!payCode.equals(tLYSendToBankSetSchema.getPayCode())) {
						continue;
					}
					tLYReturnFromBankSchema.setAccName(tLYSendToBankSetSchema.getAccName());
					tLYReturnFromBankSchema.setAccNo(tLYSendToBankSetSchema.getAccNo());
					tLYReturnFromBankSchema.setAccType(tLYSendToBankSetSchema.getAccType());
					tLYReturnFromBankSchema.setAgentCode(tLYSendToBankSetSchema.getAgentCode());
					tLYReturnFromBankSchema.setBankCode(tLYSendToBankSetSchema.getBankCode());
					tLYReturnFromBankSchema.setBankDealDate(PubFun.getCurrentDate());
					tLYReturnFromBankSchema.setBankDealTime(PubFun.getCurrentTime());
					tLYReturnFromBankSchema.setComCode(tLYSendToBankSetSchema.getComCode());
					tLYReturnFromBankSchema.setConvertFlag(tLYSendToBankSetSchema.getConvertFlag());
					tLYReturnFromBankSchema.setDataType(tLYSendToBankSetSchema.getDataType());
					tLYReturnFromBankSchema.setDealType(tLYSendToBankSetSchema.getDealType());
					tLYReturnFromBankSchema.setDoType(tLYSendToBankSetSchema.getDoType());
					tLYReturnFromBankSchema.setModifyDate(PubFun.getCurrentDate());
					tLYReturnFromBankSchema.setModifyTime(PubFun.getCurrentTime());
					tLYReturnFromBankSchema.setName(tLYSendToBankSetSchema.getName());
					tLYReturnFromBankSchema.setNoType(tLYSendToBankSetSchema.getNoType());
					tLYReturnFromBankSchema.setPayType(tLYSendToBankSetSchema.getPayType());
					tLYReturnFromBankSchema.setPayCode(tLYSendToBankSetSchema.getPayCode());
					tLYReturnFromBankSchema.setPolNo(tLYSendToBankSetSchema.getPolNo());
					tLYReturnFromBankSchema.setPayMoney(tLYSendToBankSetSchema.getPayMoney());
					tLYReturnFromBankSchema.setRemark(tLYSendToBankSetSchema.getRemark());
					tLYReturnFromBankSchema.setRiskCode(tLYSendToBankSetSchema.getRiskCode());
					tLYReturnFromBankSchema.setSendDate(tLYSendToBankSetSchema.getSendDate());
					tLYReturnFromBankSchema.setSerialNo(tLYSendToBankSetSchema.getSerialNo());
					tLYReturnFromBankSchema.setVertifyFlag(tLYSendToBankSetSchema.getVertifyFlag());
					break;
				}
			}

			tMap.put(mLYBankLogSchema, "UPDATE");
			tMap.put(mLYReturnFromBankSet, "INSERT");
			mInputData.add(tMap);

		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	/**
	 * 获取异常信息
	 * 
	 * @return
	 */
	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] arr) {

		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "cwad";

		LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
		tLYBankLogSchema.setSerialNo("10000000000000112921");

		VData v = new VData();
		v.add(tLYBankLogSchema);
		v.add("E:\\zzzzzzzzzzzzz\\a.xls");
		v.add(tGlobalInput);

		ReadFromExcelBL tWriteToExcelBL = new ReadFromExcelBL();
		if (tWriteToExcelBL.submitData(v, "")) {
		} else {
			System.out.println(tWriteToExcelBL.getErrors().getFirstError());
		}
	}
}
