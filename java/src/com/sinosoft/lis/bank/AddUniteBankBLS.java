package com.sinosoft.lis.bank;

import java.sql.Connection;

import com.sinosoft.lis.vdb.LDBankUniteDBSet;
import com.sinosoft.lis.vschema.LDBankUniteSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vdb.LDBankDBSet;
import com.sinosoft.lis.vschema.LDBankSet;

public class AddUniteBankBLS {
	
	  //错误处理类，每个需要错误处理的类中都放置该类
	  public  CErrors mErrors=new CErrors();
	  /** 传入数据的容器 */
	  private VData mInputData = new VData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  
	  private LDBankUniteSet inLDBankUniteSet = new LDBankUniteSet();
	  
	  private LDBankSet inLDBankSet = new LDBankSet();
	  
	  public AddUniteBankBLS()
	  {
		  
	  }
	  
	  /**
	   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	   * @param cInputData 传入的数据,VData对象
	   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
	   * @return 布尔值（true--提交成功, false--提交失败）
	   */
	  public boolean submitData(VData cInputData, String cOperate) {
	    boolean tReturn = false;

	    //将操作数据拷贝到本类中
	    this.mInputData = (VData)cInputData.clone();
	    this.mOperate =cOperate;

	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData()) return false;
	    //System.out.println("---End getInputData---");

	    //信息保存
	    //if(this.mOperate.equals("PAYMONEY")) {
	      tReturn = save(cInputData);
	   // }

	    if (tReturn)
	      System.out.println("Save sucessful");
	    else
	      System.out.println("Save failed");

	    return tReturn;
	  }
	  
	  //保存操作
	  private boolean save(VData mInputData)
	  {
		  boolean tReturn = true;
		  System.out.println("Start Save...");
		  
		  //建立数据库连接
		  Connection conn=DBConnPool.getConnection();
		    if (conn == null) {
		      // @@错误处理
		      CError tError = new CError();
		      tError.moduleName = "PayReturnFromBankBLS";
		      tError.functionName = "saveData";
		      tError.errorMessage = "数据库连接失败!";
		      this.mErrors .addOneError(tError) ;
		      return false;
		    }
		    
		    try
		    {
		    	//开始事务，锁表
		        conn.setAutoCommit(false);
		        
		        //生成银联表数据
		        LDBankUniteDBSet tLDBankUniteDBSet = new LDBankUniteDBSet();
		        LDBankDBSet tLDBankDBSet = new LDBankDBSet();
		        tLDBankUniteDBSet.set(inLDBankUniteSet);
		        tLDBankDBSet.set(inLDBankSet);
		        if(!tLDBankUniteDBSet.insert())
		        {
		        	try{conn.rollback() ;} catch(Exception e){}
		        	conn.close();
		        	System.out.println("LDBankUnite Insert Failed");
		        	return false;
		        }
		        System.out.println("End 生成银联表关联数据 ...");
		        //dongjian 2010-12-07
		        if(!tLDBankDBSet.update())
		        {
		        	try{conn.rollback() ;} catch(Exception e){}
		        	conn.close();
		        	System.out.println("LDBank update Failed");
		        	return false;
		        }
		        System.out.println("End 修改银行发盘权限 ...");
		        
		        conn.commit();
		        conn.close();
		        System.out.println("End Committed");
		    }catch(Exception ex)
		    {
		    	//@@错误处理
		        CError tError = new CError();
		        tError.moduleName = "AddUniteBankBLS";
		        tError.functionName = "submitData";
		        tError.errorMessage = ex.toString();
		        this.mErrors .addOneError(tError);
		        try{ conn.rollback() ;} catch(Exception e){}
		        tReturn=false;
		    }
		  
		  return tReturn;
	  }
	  
	  /**
	   * 将外部传入的数据分解到本类的属性中
	   * @param: 无
	   * @return: boolean
	   */
	  private boolean getInputData()
	  {
		  try
		  {
			  inLDBankUniteSet = (LDBankUniteSet)mInputData.getObjectByObjectName("LDBankUniteSet", 0);
			  inLDBankSet = (LDBankSet)mInputData.getObjectByObjectName("LDBankSet", 0);
			  
		  }catch(Exception e)
		  {
			  //@@错误处理
		      CError tError = new CError();
		      tError.moduleName = "PayReturnFromBankBLS";
		      tError.functionName = "getInputData";
		      tError.errorMessage = "接收数据失败!!";
		      this.mErrors .addOneError(tError) ;
		      return false;
		  }
		  return true;
	  }

}
