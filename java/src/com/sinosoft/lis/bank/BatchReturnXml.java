package com.sinosoft.lis.bank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class BatchReturnXml
{
    private String mUrl = "";

    private MMap map = new MMap();

    private VData mInputData = new VData();

    public boolean submitData(String mUrl)
    {
        this.mUrl = mUrl + "/";

        if (!dealDate())
        {
            System.out.println("获取数据失败");
            return false;
        }

        if (!prepareOutputData())
        {
            System.out.println("准备更新日志数据失败");
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "UPDATE"))
        {
            //错误处理
            System.out.println("更新日志失败");
            return false;
        }

        return true;
    }

    public boolean dealDate()
    {
        String sql = "select * from lybanklog a where a.dealstate is null and a.senddate is not null and a.bankcode in ('7705','7706','7709') with ur";
        System.out.println(sql);
        LYBankLogDB tLyBankLogDB = new LYBankLogDB();
        LYBankLogSet tLYBankLogSet = tLyBankLogDB.executeQuery(sql);
        if (tLYBankLogSet == null)
        {
            System.out.println("未查到待回盘银行数据");
            return false;
        }
        for (int i = 1; i <= tLYBankLogSet.size(); i++)
        {
            LYBankLogSchema tLYBankLogSchema = tLYBankLogSet.get(i);
            String serialno = tLYBankLogSchema.getSerialNo();
            sql = "select * from lysendtobank where serialno='" + serialno
                    + "' with ur";
            System.out.println(sql);
            LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
            LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB
                    .executeQuery(sql);
            createXml(tLYBankLogSchema, tLYSendToBankSet);
        }
        return true;
    }

    public void createXml(LYBankLogSchema tLYBankLogSchema,
            LYSendToBankSet tLYSendToBankSet)
    {
        String bankUniteCode = tLYBankLogSchema.getBankCode();
        String type = tLYBankLogSchema.getLogType();
        String sql = "select codename from ldcode1 where codetype='BatchSendBank' and code='UserName' and code1='"
                + type + bankUniteCode + "'";
        String userName = new ExeSQL().getOneValue(sql);
        sql = "select codename from ldcode1 where codetype='BatchSendBank' and code='UserPass' and code1='"
                + type + bankUniteCode + "'";
        String userPass = new ExeSQL().getOneValue(sql);
        sql = "select codename from ldcode1 where codetype='BatchSendBank' and code='MerchantID' and code1='"
                + type + bankUniteCode + "'";
        String merchantID = new ExeSQL().getOneValue(sql);

        Element root = null;
        if (bankUniteCode.equals("7705") || bankUniteCode.equals("7709"))
        {
            root = new Element("GZELINK");
        }
        else if (bankUniteCode.equals("7706"))
        {
            root = new Element("AIPG");
        }

        Document doc = new Document(root);

        Element ele_INFO = new Element("INFO");
        if (bankUniteCode.equals("7705") || bankUniteCode.equals("7709"))
        {
            setText(ele_INFO, "TRX_CODE", "200001");
        }
        else
        {
            setText(ele_INFO, "TRX_CODE", "200004");
        }

        setText(ele_INFO, "VERSION", "03");
        setText(ele_INFO, "DATA_TYPE", "2");
        setText(ele_INFO, "REQ_SN", PubFun.getCurrentDate2()
                + PubFun.getCurrentTime2());
        setText(ele_INFO, "USER_NAME", userName);
        setText(ele_INFO, "USER_PASS", userPass);
        setText(ele_INFO, "SIGNED_MSG", "");
        root.addContent(ele_INFO);
        if (bankUniteCode.equals("7705") || bankUniteCode.equals("7709"))
        {
            Element ele_BODY = new Element("BODY");
            Element QUERY_TRANS = new Element("QUERY_TRANS");
            setText(QUERY_TRANS, "QUERY_SN", tLYBankLogSchema.getSerialNo());
            setText(QUERY_TRANS, "QUERY_REMARK", "000000000000351");
            ele_BODY.addContent(QUERY_TRANS);
            root.addContent(ele_BODY);
        }
        else
        {
            Element QTRANSREQ = new Element("QTRANSREQ");
            setText(QTRANSREQ, "MERCHANT_ID", merchantID);
            setText(QTRANSREQ, "QUERY_SN", tLYBankLogSchema.getSerialNo());
            setText(QTRANSREQ, "STATUS", "2");
            setText(QTRANSREQ, "TYPE", "1");
            setText(QTRANSREQ, "START_DAY", "");
            setText(QTRANSREQ, "END_DAY", "");
            root.addContent(QTRANSREQ);
        }

        XMLOutputter XMLOut = new XMLOutputter();
        try
        {
            XMLOut.setEncoding("GBK");
            XMLOut.output(doc, new FileOutputStream(this.mUrl
                    + tLYBankLogSchema.getSerialNo() + ".xml"));
            String sendStr = readtxt(this.mUrl + tLYBankLogSchema.getSerialNo()
                    + ".xml");
            System.out.println("发送查询报文内容" + sendStr);

            if (!sendStr.equals(""))
            {
                if (bankUniteCode.equals("7705"))
                {
                    BatchProcess tBatchProcess = new BatchProcess(
                            bankUniteCode, type);
                    String payReq = tBatchProcess.PayReq(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        saveReq(payReq, tLYBankLogSchema, "7705");
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema, 1);
                    }
                }
				else if(bankUniteCode.equals("7709")){
                	BatchProcessHigh tBatchProcessHigh = new BatchProcessHigh(
                			bankUniteCode, type);
                	String payReq = tBatchProcessHigh.PayReq(sendStr);
                	 if (!payReq.equals(""))
                     {
                         System.out.println("--返回请求响应--");
                         saveReq(payReq, tLYBankLogSchema,"7709");
                     }
                     else
                     {
                         errBankLog(tLYBankLogSchema,1);
                     }
                }
                else if (bankUniteCode.equals("7706"))
                {
                    BatchProcessCom tBatchProcessCom = new BatchProcessCom(
                            bankUniteCode, type);
                    String payReq = tBatchProcessCom.send(sendStr);
                    if (!payReq.equals(""))
                    {
                        System.out.println("--返回请求响应--");
                        saveReq(payReq, tLYBankLogSchema, "7706");
                    }
                    else
                    {
                        errBankLog(tLYBankLogSchema, 1);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            System.out.println(tLYBankLogSchema.getSerialNo() + "批次程序处理异常");
            ex.printStackTrace();
        }
    }

    private boolean setText(Element element, String name, String value)
    {

        Element ele = new Element(name);
        ele.addContent(value);
        element.addContent(ele);
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    private boolean saveReq(String payReq, LYBankLogSchema tLYBankLogSchema,
            String bankCode)
    {
        String tUrl = this.mUrl + tLYBankLogSchema.getSerialNo() + "rq.xml";
        try
        {
            FileWriter fw = new FileWriter(tUrl);
            fw.write(payReq);
            fw.close();
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, 2);
            ex.printStackTrace();
            return false;
        }
        if (!checkReq(payReq, tLYBankLogSchema))
        {
            return false;
        }
        System.out.println("成功了！！！！");
        BatchReturnFromBankBL tBatchReturnFromBankBL = new BatchReturnFromBankBL();
        try
        {
            if (!tBatchReturnFromBankBL.submitData(tUrl, bankCode))
            {
                errBankLog(tLYBankLogSchema, 2);
            }
        }
        catch (Exception ex)
        {
            errBankLog(tLYBankLogSchema, 2);
            ex.printStackTrace();
        }
        return true;
    }

    private boolean checkReq(String req, LYBankLogSchema tLYBankLogSchema)
    {
        int iStart = req.indexOf("<RET_CODE>");
        if (iStart != -1)
        {
            int end = req.indexOf("</RET_CODE>");
            String signedMsg = req.substring(iStart + 10, end);
            System.out.println("响应标记：" + signedMsg);
            if (signedMsg.equals("0000"))
            {
                return true;
            }
            else
            {
                int s_MSG = req.indexOf("<ERR_MSG>");
                if (s_MSG != -1)
                {
                    int e_MSG = req.indexOf("</ERR_MSG>");
                    System.out.println("截去字符串的起始位置:" + (s_MSG+9) + ";终止位置:" + e_MSG);
                    String signedMSG = req.substring(s_MSG + 9, e_MSG);
                    System.out.println("响应标记：" + signedMSG);
                    if (signedMSG.length() > 50)
                    {
                        tLYBankLogSchema.setInFile(signedMSG.substring(0, 40));
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                    else
                    {
                        tLYBankLogSchema.setInFile(signedMSG);
                        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
                        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
                    }
                    this.map.put(tLYBankLogSchema, "UPDATE");
                }
            }
        }
        return false;

    }

    private void errBankLog(LYBankLogSchema tLYBankLogSchema, int flag)
    {
        if (flag == 1)
        {
            System.out.println("获取响应信息失败");
            tLYBankLogSchema.setInFile("获取响应信息失败");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        else
        {
            System.out.println("回盘处理程序出现异常");
            tLYBankLogSchema.setInFile("回盘处理程序出现异常");
            tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        }
        this.map.put(tLYBankLogSchema, "UPDATE");
    }

    private String readtxt(String path)
    {
        String str = "";
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(path));

            String r = br.readLine();
            while (r != null)
            {
                str += r;
                r = br.readLine();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
        str = str.replaceAll("<SIGNED_MSG />", "<SIGNED_MSG></SIGNED_MSG>");
        return str;
    }

    //紧急处理时使用，模拟回盘
    public boolean batchReturn(String req, String serialNo)
    {
        String sql = "select * from lybanklog where serialno='" + serialNo
                + "' with ur";
        System.out.println(sql);
        LYBankLogDB spLYBankLogDB = new LYBankLogDB();
        LYBankLogSet spLYBankLogSet = spLYBankLogDB.executeQuery(sql);
        if (spLYBankLogSet.size() == 0)
        {
            System.out.println("批次号错误！");
            return false;
        }
        LYBankLogSchema spLYBankLogSchema = spLYBankLogSet.get(1);
        String bankCode = spLYBankLogSchema.getBankCode();

        String sqlurl = "select sysvarvalue from LDSYSVAR  where Sysvar='BatchReturnPath'";//发盘报文的存放路径
        this.mUrl = new ExeSQL().getOneValue(sqlurl) + "special";
        System.out.println("发盘报文的存放路径:" + mUrl);
        File myFilePath = new File(mUrl.toString());
        try
        {
            if (myFilePath.isDirectory())
            {
                System.out.println("目录已存在");
            }
            else
            {
                myFilePath.mkdir();
                System.out.println("新建目录成功");
            }
        }
        catch (Exception e)
        {
            System.out.println("新建目录失败");
            e.printStackTrace();
        }
        spLYBankLogSchema.setSerialNo(serialNo);
        if (!saveReq(req, spLYBankLogSchema, bankCode))
        {
            return false;
        }
        return true;
    }

    public static void main(String[] arr)
    {
        BatchReturnXml tBatchReturnXml = new BatchReturnXml();
        tBatchReturnXml.batchReturn("111111", "00000000000000094990");
    }
}
