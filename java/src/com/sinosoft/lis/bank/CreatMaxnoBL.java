package com.sinosoft.lis.bank;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CreatMaxnoBL
{
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private VData mResult = new VData();
    private String tNotype="" ;
    private String tLimit="";
    private String MaxNo="";
    public CreatMaxnoBL(){}
    
    public boolean submitData(VData cInputData, String cOperate)
    {
        
        if(!getInputData(cInputData))
        {
            return false;
        }
        
        if(!dealData())
        {
            return false;
        }
        return true;
    }
    
    
    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {	
    	  
    	if ("".equals(tLimit)){
    		
    		MaxNo= PubFun1.CreateMaxNo(tNotype , null);
    	}else {
    		tLimit= PubFun.getNoLimit(tLimit);
    		MaxNo= PubFun1.CreateMaxNo(tNotype, tLimit);		
    	}
        
    	mResult.add(0,MaxNo);
    	return true ;
    }
    
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "QYMaintenanceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在接受数据时没有得到TransferData的数据!";
            this.mErrors.addOneError(tError);

            return false;
        }
       
        tNotype = (String)mTransferData.getValueByName("NoType");
        tLimit = (String)mTransferData.getValueByName("Limit");
        System.out.println("业务号码类型为：" + tNotype);
        System.out.println("限制条件为" + tLimit); 
       return true;
    }
    
    public VData getResult()
    {
        return this.mResult;
    }
}
