/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.userMan;

import com.sinosoft.lis.db.LDSpotUWRateDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.llcase.ClaimUserOperateBL;
import com.sinosoft.lis.menumang.LDUserSortUI;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDSpotUWRateSchema;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LDSpotUWRateSet;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.lis.vschema.LDUserTOMenuGrpSet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 用户查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author DingZhong
 * @version 1.0
 */
public class LDUserManBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mDeletor;

    /** 业务处理相关变量 */
    /** 用户的相关信息*/
    LDUserSchema mLDUserSchema = new LDUserSchema();
    LDUserTOMenuGrpSet mLDUserToMenuGrpSet = new LDUserTOMenuGrpSet();
    String mOperator; //指示进行本操作的操作员
    String mUnderStr;

    String mResultStr = "";
    int mResultNum = 0;

    public LDUserManBL()
    {
        // just for debug
    }

    public static void main(String[] args)
    {
        LDUserSchema userSchema = new LDUserSchema();
        userSchema.setUserCode("dong");
        userSchema.setComCode("86");
        userSchema.setUserName("dong");
        VData tData = new VData();
        tData.add(userSchema);
        tData.add("001");

        LDUserManBL tLDUserManBL = new LDUserManBL();
        tLDUserManBL.submitData(tData,"query");


    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        // 判断操作是不是查询
        if (cOperate.compareTo("query") != 0
            && cOperate.compareTo("insert") != 0
            && cOperate.compareTo("update") != 0
            && cOperate.compareTo("delete") != 0
            && cOperate.compareTo("unLock") != 0)
        {
            System.out.println("Operate is not permitted");
            return false;
        }

        System.out.println("start BL submit...");

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---getInputData---");

        if (mOperate.equals("query"))
        {
            if (!queryData())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        if(mOperate.equals("update")&&mLDUserSchema.getUserState().trim().equals("1"))//将用户状态更新为无效时
        {
        	LLClaimUserDB tLLClaimUserDB=new LLClaimUserDB();
//        	tLLClaimUserDB.setUserCode(mLDUserSchema.getUserCode());
        	LLClaimUserSet tLLClaimUserSet=tLLClaimUserDB.executeQuery("select * from llclaimuser where usercode='"+mLDUserSchema.getUserCode()+"' with ur");
        	if(tLLClaimUserSet!=null&&tLLClaimUserSet.size()>0) 
        	{	
	        	LLClaimUserSchema tLLClaimUserSchema=tLLClaimUserSet.get(1);
	        	tLLClaimUserSchema.setStateFlag("9");//设置为其他无效情况
	        	tLLClaimUserSchema.setRemark("无");//因为设置为非有效时需要录入备注，所以这里设置为空
	        	LDSpotUWRateDB tLDSpotUWRateDB=new LDSpotUWRateDB();
	        	LDSpotUWRateSet tLDSpotUWRateSet=tLDSpotUWRateDB.executeQuery("select * from LDSpotUWRate where usercode='"+mLDUserSchema.getUserCode()+"' with ur");
	        	LDSpotUWRateSchema tLDSpotUWRateSchema=tLDSpotUWRateSet.get(1);
	        	
	        	ClaimUserOperateBL tClaimUserOperateBL=new ClaimUserOperateBL();
	        	VData tVData=new VData();
	        	tVData.addElement(tLLClaimUserSchema);
	//        	tVData.addElement(new LDSpotUWRateSchema());
	        	tVData.addElement(tLDSpotUWRateSchema);
	        	tVData.addElement(cInputData.getObjectByObjectName("GlobalInput", 0));
	        	tVData.addElement(new LDCode1Set());
	        	
	        	if(!tClaimUserOperateBL.submitData(tVData, "UPDATE"))
	        	{   this.mErrors.copyAllErrors(tClaimUserOperateBL.mErrors);
	        		CError tError = new CError();
	                tError.moduleName = "LDUserManBL";
	                tError.functionName = "submitData";
//	                tError.errorMessage = "自动将理赔权限修改为失效时失败!";
	                this.mErrors.addOneError(tError);
	                return false;
	        	}
	        	mLDUserSchema.setClaimPopedom("3");
        	}
        }
        //进行校验
        if (!checkData())
        {
            return false;
        }
        
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("After prepareOutputData");

        System.out.println("Start LDUser BL Submit...");

        LDUserManBLS tLDUserManBLS = new LDUserManBLS();

        System.out.println("tLDuserManBLs.submit...");
        String str = (String) mInputData.getObjectByObjectName("String", 0);
        System.out.println("deletor is " + str);
        tLDUserManBLS.submitData(mInputData, cOperate);

        System.out.println("End LDUserMan BL Submit...");

        //如果有需要处理的错误，则返回
        if (tLDUserManBLS.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLDUserManBLS.mErrors);
            return false;
        }

        mInputData = null;

        return true;
    }
    //在删除用户时校验该用户是否具有核赔权限、账单输入权限
    private boolean checkData()
    {
    	if(mOperate.equals("delete"))
        {
//        	LDUserDB tLDUserDB = new LDUserDB();
//            tLDUserDB.setUserCode(mLDUserSchema.getUserCode());
        	ExeSQL tExeSQL=new ExeSQL();
        	String tClaimPopedom=tExeSQL.getOneValue("select ClaimPopedom from lduser where usercode='"+mLDUserSchema.getUserCode()+"'");
//            LDUserSet tLDUserSet=tLDUserDB.executeQuery("select * from ");
//            LDUserSchema tLDUserSchema=tLDUserDB.getSchema();
            if(tClaimPopedom.trim().equals("1")||tClaimPopedom.trim().equals("2"))
            {
            	CError tError = new CError();
                tError.moduleName = "LDUserManBL";
                tError.functionName = "checkData";
                tError.errorMessage = "用户具有核赔权限或者账单输入权限，请先置为无核赔权限再删除。";
                this.mErrors.addOneError(tError);
                return false;
            }	
        }
    	return true;
    }

    private boolean dealData()
    {
        if(mOperate.equals("unLock")){
            LDUserDB tLDUserDB = new LDUserDB();
            tLDUserDB.setUserCode(mLDUserSchema.getUserCode());
            if(tLDUserDB.getInfo()){
                // 开始进行密码加密
               String plainPwd = mLDUserSchema.getPassword();
               LisIDEA tIdea = new LisIDEA();
               String encryptPwd = tIdea.encryptString(plainPwd);
                tLDUserDB.setPassword(encryptPwd);
                tLDUserDB.setUserState("0");
                tLDUserDB.setLoginFailCount(0);
                tLDUserDB.setModifydate(PubFun.getCurrentDate());
                tLDUserDB.setModifytime(PubFun.getCurrentTime());
                tLDUserDB.setModifyOperator(mOperator);
  //              tLDUserDB.setOperator(mOperator); 不修改操作员，防止是超级用户，则会修改创建者
                tLDUserDB.update();
            }else{
                return false;
            }
        }
        return true;
    }


    private boolean queryData()
    {
        /* */
        //String ope ="";
        //TransferData userData=(TransferData)mInputData.getObjectByObjectName("TransferData",0);
        //System.out.println("hihihi  in java , in queryData() ... ");

        //String operator = (String) mInputData.getObjectByObjectName(
                    //"String", 0);

        //ope = (String)userData.getValueByName("Ope"); //得到操作人
        System.out.println("hihihi  in java ...mOperator。。。。 "+mOperator);

        LDUserSortUI userSort = new LDUserSortUI();
        String underStr=userSort.getAllUnderLing(mOperator); //得到所有下级用户
        System.out.println("hihihi  in java ... "+underStr);
        String usercode = mLDUserSchema.getUserCode();
        System.out.println(usercode.length());
        String username = mLDUserSchema.getUserName();
        String comcode = mLDUserSchema.getComCode();
        LDUserDB tLDUserDB = new LDUserDB();
        LDUserSet tLDUserSet = new LDUserSet();
        String strSql ="";
        String wherePart = " where 1 = 1 ";

        if ((usercode != null) && (!usercode.trim().equals("")))
        {
            wherePart = wherePart + " and  usercode = '" + usercode + "' ";
            System.out.println(wherePart);
        }

        if ((username != null) && (!username.trim().equals("")))
        {
            wherePart = wherePart + " and  username = '" + username + "' ";
        }

        if ((comcode != null) && (!comcode.trim().equals("")))
        {
            wherePart = wherePart + " and  comcode like '" + comcode + "%' ";
        }

        if(mOperator.equals("001")||mOperator.equals("it001"))
        {
            strSql = "select * from lduser " + wherePart ;// + " and usercode in (" +underStr + ")";
        }
        else
        {
            strSql = "select * from lduser " + wherePart + " and usercode in (" + underStr + ")";
        }
        System.out.println("zb strSql: "+strSql);
        tLDUserSet = tLDUserDB.executeQuery(strSql);
System.out.println("tLDUserSet.encode():  "+tLDUserSet.encode());

        System.out.println("tLDUserSet size :" + tLDUserSet.size());

        if (tLDUserSet == null)
        {
            return false;
        }

        LisIDEA tIdea = new LisIDEA();
        for (int i = 1; i <= tLDUserSet.size(); i++)
        {
            String encryptPwd = ""+tLDUserSet.get(i).getPassword();

            if (encryptPwd.length() != 16)
            {
                continue;
            }

            String decryptPwd = tIdea.decryptString(encryptPwd);
            System.out.println(encryptPwd);
            System.out.println(decryptPwd);
            tLDUserSet.get(i).setPassword(decryptPwd);
        }

        String tReturn = tLDUserSet.encode();
        tReturn = "0|" + tLDUserSet.size() + "^" + tReturn;
        System.out.println("tReturn-------****" + tReturn);

        mResult.clear();
        mResult.add(tReturn);

        return true;

    }


    public VData getResult()
    {
        return mResult;
    }

    public int getResultNum()
    {
        return mResultNum;
    }

    public String getResultStr()
    {
        String resultStr = "";
        for (int i = 1; i <= mResultNum; i++)
        {

        }
        return resultStr;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //得到增加更新时的操作员或删除时的当前操作员
        mOperator = (String) cInputData.getObjectByObjectName("String", 0);
        mUnderStr = (String) cInputData.getObjectByObjectName("String", 3);

        // 检验查询条件
        mLDUserSchema = (LDUserSchema) cInputData.getObjectByObjectName(
                "LDUserSchema", 0);

        if (mLDUserSchema == null)
        {
            System.out.println("cant get userschema");
            return false;
        }

        String curDate = PubFun.getCurrentDate();
        String curTime = PubFun.getCurrentTime();

        if (mOperate.compareTo("insert") == 0)
        {
            System.out.println("insert operate");
            mLDUserSchema.setMakeTime(curTime);
            mLDUserSchema.setMakeDate(curDate);
        }
        System.out.println("come here");
        System.out.println("password is:" + mLDUserSchema.getPassword());
        // decrypt password if possible

//      System.out.println("comeHere");

        mLDUserToMenuGrpSet = (LDUserTOMenuGrpSet) cInputData.
                              getObjectByObjectName("LDUserTOMenuGrpSet", 0);

        System.out.println("completed get input data");
        return true;
    }

    /**
     * 查询符合条件的用户的信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryUser()
    {

        return true;
    }

    /**
     * 查询符合条件的菜单的信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryRemainMenu()
    {

        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        mResult.clear();
        mInputData.clear();
        try
        {
            mInputData.add(mOperator);
            mInputData.add(mLDUserSchema);
            mInputData.add(mLDUserToMenuGrpSet);
            mInputData.add(mUnderStr);
  System.out.println("try...............: "+mUnderStr);

            System.out.println("prepareOutput deletor : " + mOperator);

            String str = (String) mInputData.getObjectByObjectName("String", 0);
            System.out.println("deletor is : " + str);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println("BL excetion happend");

            CError tError = new CError();
            tError.moduleName = "MenuQueryBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
