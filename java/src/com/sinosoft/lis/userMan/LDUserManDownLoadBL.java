package com.sinosoft.lis.userMan;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.menumang.LDUserSortUI;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LDSpotUWRateDB;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.db.LLClaimUserDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.llcase.ClaimUserOperateBL;
import com.sinosoft.lis.menumang.LDUserSortUI;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDSpotUWRateSchema;
import com.sinosoft.lis.schema.LDUserSchema;
import com.sinosoft.lis.schema.LLClaimUserSchema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LDSpotUWRateSet;
import com.sinosoft.lis.vschema.LDUserSet;
import com.sinosoft.lis.vschema.LDUserTOMenuGrpSet;
import com.sinosoft.lis.vschema.LLClaimUserSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import java.io.*;

public class LDUserManDownLoadBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGI = null;
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mDeletor;
    private String mSql = null;
    private String mOutXmlPath = null;

    /** 业务处理相关变量 */
    /** 用户的相关信息*/
    LDUserSchema mLDUserSchema = new LDUserSchema();
    LDUserTOMenuGrpSet mLDUserToMenuGrpSet = new LDUserTOMenuGrpSet();
    String mOperator; //指示进行本操作的操作员
    String mUnderStr;

    String mResultStr = "";
    int mResultNum = 0;

    public LDUserManDownLoadBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {

        /* */
        //String ope ="";
        //TransferData userData=(TransferData)mInputData.getObjectByObjectName("TransferData",0);
        //System.out.println("hihihi  in java , in queryData() ... ");

        //String operator = (String) mInputData.getObjectByObjectName(
                    //"String", 0);

        //ope = (String)userData.getValueByName("Ope"); //得到操作人
        System.out.println("hihihi  in java ...mOperator。。。。 "+mOperator);

        LDUserSortUI userSort = new LDUserSortUI();
        String underStr=userSort.getAllUnderLing(mOperator); //得到所有下级用户
        System.out.println("hihihi  in java ... "+underStr);
        String usercode = mLDUserSchema.getUserCode();
        System.out.println(usercode.length());
        String username = mLDUserSchema.getUserName();
        String comcode = mLDUserSchema.getComCode();
        LDUserDB tLDUserDB = new LDUserDB();
        LDUserSet tLDUserSet = new LDUserSet();
        String strSql ="";
        String wherePart = " where 1 = 1 ";

        if ((usercode != null) && (!usercode.trim().equals("")))
        {
            wherePart = wherePart + " and  usercode = '" + usercode + "' ";
            System.out.println(wherePart);
        }

        if ((username != null) && (!username.trim().equals("")))
        {
            wherePart = wherePart + " and  username = '" + username + "' ";
        }

        if ((comcode != null) && (!comcode.trim().equals("")))
        {
            wherePart = wherePart + " and  comcode like '" + comcode + "%' ";
        }

        if(mOperator.equals("001")||mOperator.equals("it001"))
        {
            strSql = "select username,usercode," +
            		"(case userstate when '0' then '有效' when '1' " +
            		"then '无效' when 'L' then '锁定' else '其它' end)," +
            		"userdescription,comcode,(select name from ldcom where comcode=a.comcode)," +
            		"UWPopedom,ClaimPopedom,OtherPopedom,PopUWFlag,ValidStartDate,ValidEndDate," +
            		"CertifyFlag,EdorPopedom,AgentCom,operator from lduser a " + wherePart ;// + " and usercode in (" +underStr + ")";
        }
        else
        {
            strSql = "select username,usercode,(" +
            		"case userstate when '0' then '有效' when '1' " +
            		"then '无效' when 'L' then '锁定' else '其它' end)," +
            		"userdescription,comcode,(select name from ldcom where comcode=a.comcode)," +
            		"UWPopedom,ClaimPopedom,OtherPopedom,PopUWFlag,ValidStartDate,ValidEndDate," +
            		"CertifyFlag,EdorPopedom,AgentCom,operator from lduser a " + wherePart + " and usercode in (" + underStr + ")";
        }
        System.out.println("zb strSql: "+strSql);
       

    

    
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(strSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(strSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LDUserManDownLoadBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 1][16];

        mToExcel[0][0] = "用户姓名";
        mToExcel[0][1] = "用户编码";
        mToExcel[0][2] = "用户状态";
        mToExcel[0][3] = "用户描述";
        mToExcel[0][4] = "管理机构编码";
        mToExcel[0][5] = "管理机构名称";
        mToExcel[0][6] = "核保权限";
        mToExcel[0][7] = "核赔权限";
        mToExcel[0][8] = "其它权限";
        mToExcel[0][9] = "首席核保标志";
        mToExcel[0][10] = "有效开始日期";
        mToExcel[0][11] = "有效结束日期";
        mToExcel[0][12] = "单证管理员标志";
        mToExcel[0][13] = "保全权限";
        mToExcel[0][14] = "代理机构";
        mToExcel[0][15] = "用户上级";


        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData cInputData)
    {
        //得到增加更新时的操作员或删除时的当前操作员
        mOperator = (String) cInputData.getObjectByObjectName("String", 0);
        mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        // 检验查询条件
        mLDUserSchema = (LDUserSchema) cInputData.getObjectByObjectName(
                "LDUserSchema", 0);

        if (mLDUserSchema == null)
        {
            System.out.println("cant get userschema");
            return false;
        }

        String curDate = PubFun.getCurrentDate();
        String curTime = PubFun.getCurrentTime();

        TransferData tf = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);

       

        
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");

        mLDUserToMenuGrpSet = (LDUserTOMenuGrpSet) cInputData.
                              getObjectByObjectName("LDUserTOMenuGrpSet", 0);

        System.out.println("completed get input data");
        return true;
    }


    public static void main(String[] args)
    {
        
    }
}
