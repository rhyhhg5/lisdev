/**
 * 2011-8-10
 */
package com.sinosoft.lis.wiitb;

import org.apache.log4j.Logger;
import org.jdom.Document;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.lis.cbcheck.GrpUWSendPrintUI;
import com.sinosoft.lis.certifybusiness.CertifyDiskImportLog;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBMissionSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflow.tb.GrpTbWorkFlowUI;
import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.utility.SSRS;
/**
 * @author LY
 *
 */
public class PubBriGrpBatchImportSYAPI
{
    private final static Logger mLogger = Logger.getLogger(PubBriGrpBatchImportAPI.class);

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mBatchNo = null;

    private String mMsgType = null;

    private Document mInXmlDoc = null;

    public PubBriGrpBatchImportSYAPI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "参数[导入文件名（BatchNo）]不存在。");
            return false;
        }

        mInXmlDoc = (Document) mTransferData.getValueByName("InXmlDoc");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "参数[报文数据（InXmlDoc）]不存在。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        if ("Import".equals(mOperate))
        {
            if (!importDatas())
            {
                return false;
            }
        }

        return true;
    }

    private boolean importDatas()
    {
        IXmlMsgColls tMsgCols = null;

        // 报文归档
        //        MsgXmlArchive.xmlArchive(cInXmlDoc);
        // --------------------

        // 解析报文
        MsgXmlParse tMsgXmlParse = new MsgXmlParse();
        tMsgCols = tMsgXmlParse.deal(mInXmlDoc);
        if (tMsgCols == null)
        {
            buildError("importDatas", tMsgXmlParse.getErrInfo());
            return false;
        }
        // --------------------

        // 校验报文基本信息完整性
        if (!chkXmlBaseInfo(tMsgCols))
        {
            return false;
        }
        // --------------------

        // 创建批次任务
        //        if (!createBatchMission(tMsgCols))
        //        {
        //            return false;
        //        }
        // --------------------

        // 对应业务处理调度
        if (!dealTask(tMsgCols))
        {
            return false;
        }
        // --------------------

        // 处理结果封装
        //        Document tOutDoc = null;
        //        MsgXmlPack tMsgXmlPack = new MsgXmlPack();
        //        tOutDoc = tMsgXmlPack.deal(tMsgCols);
        // --------------------

        return true;
    }

    private boolean chkXmlBaseInfo(IXmlMsgColls cMsgCols)
    {
        // 校验报文头
        IXmlMsgColls[] tLMsgHeadInfo = cMsgCols.getBodyByFlag("MsgHead");
        if (tLMsgHeadInfo == null || tLMsgHeadInfo.length != 1)
        {
            buildError("chkXmlBaseInfo", "报文头信息格式不符合约定，请核实。");
            return false;
        }

        IXmlMsgColls tMsgHeadInfo = tLMsgHeadInfo[0];

        String tMsgHeadBatchNo = tMsgHeadInfo.getTextByFlag("BatchNo");
        if (tMsgHeadBatchNo == null || !tMsgHeadBatchNo.equals(mBatchNo))
        {
            buildError("chkXmlBaseInfo", "批次号与文件中批次号不符，请核对批次文件名称与报文内容批次信息是否一致。");
            return false;
        }

        mMsgType = tMsgHeadInfo.getTextByFlag("MsgType");
        if (mMsgType == null || "".equals(mMsgType))
        {
            buildError("chkXmlBaseInfo", "报文类型为空，请核查！");
            return false;
        }

        BriGrpCommonCheckBL tChkCommTool = new BriGrpCommonCheckBL();
        if (!tChkCommTool.checkMsgHead(tMsgHeadInfo))
        {
            buildError("chkXmlBaseInfo", tChkCommTool.getErrInfo());
            return false;
        }
        // --------------------

        // 校验正文数据内容
        IXmlMsgColls[] tLMsgBody = cMsgCols.getBodyByFlag("PolicyInfo");
        if (tLMsgBody == null || tLMsgBody.length <= 0)
        {
            buildError("chkXmlBaseInfo", "报文中不存在业务数据信息或业务信息节点命名不符合约定规范，请核实。");
            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealTask(IXmlMsgColls cMsgCols)
    {
        String tCurBatchNo = mBatchNo;

        String tStrSql = null;

        // 锁定批次
        LWMissionSchema tMissionInfo = null;

        tStrSql = " select * from LWMission lwm "
                + " where 1 = 1 and lwm.ProcessId = '0000000012' and lwm.ActivityId = '0000012100' "
                + " and MissionProp1 = '" + tCurBatchNo + "' ";
        LWMissionSet tMissionSet = new LWMissionDB().executeQuery(tStrSql);
        if (tMissionSet == null || tMissionSet.size() == 0)
        {
            buildError("dealTask", "未找到该导入批次信息。");
            return false;
        }
        else if (tMissionSet.size() != 1)
        {
            buildError("dealTask", "该导入批次信息出现冗余。");
            return false;
        }
        else
        {
            tMissionInfo = tMissionSet.get(1); // 获取当前批次信息
        }
        tStrSql = null;

        String tActivityStatus = tMissionInfo.getActivityStatus();
        if (!"1".equals(tActivityStatus))
        {
            buildError("dealTask", "该导入批次信息已经被锁定，可能正在运行，请稍候尝试。");
            return false;
        }

        LWMissionDB tMissionDB = new LWMissionDB();
        tMissionDB.setMissionID(tMissionInfo.getMissionID());
        tMissionDB.setSubMissionID(tMissionInfo.getSubMissionID());
        tMissionDB.setActivityID(tMissionInfo.getActivityID());
        if (!tMissionDB.getInfo())
        {
            buildError("dealTask", "该导入批次信息未找到。");
            return false;
        }
        tMissionDB.setActivityStatus("0"); // 暂时用“0”代表锁定
        tMissionDB.update(); // 即时更新，防止并发导入 
        // --------------------

        // 创建日志记录器
        CertifyDiskImportLog tImportLog = null;

        try
        {
            tImportLog = new CertifyDiskImportLog(mGlobalInput, tCurBatchNo);
        }
        catch (Exception e)
        {
            String tStrErr = "创建日志失败。";
            buildError("getInputData", tStrErr);
            e.printStackTrace();
            return false;
        }
        // --------------------

        // 获取明细任务清单
        IXmlMsgColls[] tMsgBodyDatas = cMsgCols.getBodyByFlag("PolicyInfo");

        for (int i = 0; i < tMsgBodyDatas.length; i++)
        {
            IXmlMsgColls tSubMsgBodyData = tMsgBodyDatas[i];

            if (!dealSubTask(tSubMsgBodyData, tImportLog))
            {
                continue;
            }
        }
        // --------------------

        // 所有子任务执行完毕，设置批次任务状态
        Reflections tReflections = new Reflections();

        LBMissionSchema tBMissionInfo = new LBMissionSchema();
        tReflections.transFields(tBMissionInfo, tMissionInfo);

        String serialNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
        tBMissionInfo.setSerialNo(serialNo);
        tBMissionInfo.setActivityStatus("3"); //节点任务执行完毕
        tBMissionInfo.setLastOperator(mGlobalInput.Operator);
        tBMissionInfo.setMakeDate(PubFun.getCurrentDate());
        tBMissionInfo.setMakeTime(PubFun.getCurrentTime());
        tBMissionInfo.setModifyDate(PubFun.getCurrentDate());
        tBMissionInfo.setModifyTime(PubFun.getCurrentTime());

        MMap tMissionMMap = new MMap();
        tMissionMMap.put(tBMissionInfo, SysConst.DELETE_AND_INSERT);
        tMissionMMap.put(tMissionInfo, SysConst.DELETE);

        VData tempVData = new VData();
        tempVData.add(tMissionMMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tempVData, ""))
        {
            String error = "[" + tCurBatchNo + "]更新批次导入完成状态时，出现异常。请稍候重试。";
            buildError("dealTask", error);
        }
        // --------------------

        // 关闭日志记录器
        if (!tImportLog.logEnd())
        {
            buildError("dealTask", "关闭日志记录器时，出现异常。");
            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealSubTask(IXmlMsgColls cMsgCols, CertifyDiskImportLog cImportLog)
    {
        MMap tMMap = null;
        MMap tTmpMap = null;

        IXmlMsgColls tSubMsgBodyData = cMsgCols;

        String tPrtNo = tSubMsgBodyData.getTextByFlag("PrtNo");
        String tOperator = "Create";
        String tPeoples3=tSubMsgBodyData.getTextByFlag("Peoples3");
        
        if (!(tPrtNo.matches("[A-Za-z0-9]*")))
        {
        	String tStrErr = "印刷号格式有误，必须为字母或数字！";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }

            return false;
        }
        
        // 对待处理保单进行校验
        if (checkExists(tPrtNo))
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单已存在，不可再次导入！";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }

            return false;
        }

        if (Integer.parseInt(tPeoples3)<3)
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单的被保险人数小于3！";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }

            return false;
        }
        
        if (checkTempfee(tPrtNo))
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单已有财务确认数据，不可再次导入！";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }

        // 校验首次投保，需要进行扫描，首次投保原则：投保单位名称完全一致并且投保产品编码为工伤险产品
        IXmlMsgColls[] tSubGrpInfo = tSubMsgBodyData.getBodyByFlag("GrpAppntInfo");
        if (tSubGrpInfo == null || tSubGrpInfo.length != 1)
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单团体单位信息有误！";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }

        String tGrpName = tSubGrpInfo[0].getTextByFlag("GrpName");
        if (!checkScan(tGrpName, tPrtNo))
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单为首次导入，需先扫描投保书！";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }
        // --------------------

        VData tVData = new VData();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", mBatchNo);
        tTransferData.setNameAndValue("PrtNo", tPrtNo);
        tTransferData.setNameAndValue("XmlMsgDatas", tSubMsgBodyData);
        tTransferData.setNameAndValue("ImportLog", cImportLog);
        tTransferData.setNameAndValue("MsgType", mMsgType);

        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        ParseBriWiiGrpContBL tParseBriWiiGrpContBL = new ParseBriWiiGrpContBL();
        tTmpMap = tParseBriWiiGrpContBL.getSubmitMap(tVData, tOperator);
        if (tTmpMap == null)
        {
            return false;
        }

        tMMap = new MMap();
        tMMap.add(tTmpMap);
        tTmpMap = null;

        if (!submit(tMMap))
        {
            return false;
        }
        tMMap = null;

        // 发送缴费通知书
        sendPayNotice(tPrtNo);
        // --------------------

        String tStrSql = " select * from LCGrpCont where PrtNo = '" + tPrtNo + "' ";
        LCGrpContSet tGrpContInfoSet = new LCGrpContDB().executeQuery(tStrSql);
        if (tGrpContInfoSet == null || tGrpContInfoSet.size() == 0)
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单，未找到导入数据。";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }
        else if (tGrpContInfoSet.size() > 1)
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单，导入数据出现冗余。";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }
        
        LCGrpContSchema tGrpContInfo = tGrpContInfoSet.get(1);
        //江苏中介校验
        /* 获取销售渠道 */
    	String jSaleChnl = new ExeSQL().getOneValue("select salechnl from lcgrpcont where prtno = '"+tPrtNo+"'");
    	/* 获取保单号 */
    	String jGrpContNo = new ExeSQL().getOneValue("select grpcontno from lcgrpcont where prtno = '"+tPrtNo+"'");
    	/* 获取管理机构 */
    	String jManageCom = new ExeSQL().getOneValue("select managecom from lcgrpcont where prtno = '"+tPrtNo+"'");
    	/* 截取管理机构前四位 */
    	String jSubManageCom = jManageCom.substring(0,4);
        String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
			  	    + "and code = '"+jSaleChnl+"'";
        SSRS tSSRS = new ExeSQL().execSQL(tSql);
        if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(jSubManageCom)){
        	ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
  	  	  	TransferData tJSTransferData = new TransferData();
  	  	  	tJSTransferData.setNameAndValue("ContNo",jGrpContNo);
  	  	  	VData tJSVData = new VData();
  	  	  	tJSVData.add(tJSTransferData);
  	  	  	if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
	  	  	  	String tStrErr = "印刷号为【" + tPrtNo + "】的保单，导入数据时发生错误：" + tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
	            if (!cImportLog.errLog(tPrtNo, tStrErr)){
	                mErrors.copyAllErrors(cImportLog.mErrors);
	                return false;
	            }
	            return false;
  	  	  	}
        }//End JS Check
        
        // 生成工作流
        if (!dealMission(tGrpContInfo, cImportLog))
        {
            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealMission(LCGrpContSchema cGrpContInfo, CertifyDiskImportLog cImportLog)
    {
        VData tVData = new VData();

        String tPrtNo = cGrpContInfo.getPrtNo();
        String tManageCom = cGrpContInfo.getPrtNo();
        String tInputDate = cGrpContInfo.getInputDate();
        String tOperator = cGrpContInfo.getOperator();

        // 参数要素
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PrtNo", tPrtNo);
        tTransferData.setNameAndValue("ManageCom", tManageCom);
        tTransferData.setNameAndValue("InputDate", tInputDate);
        tTransferData.setNameAndValue("Operator", tOperator);

        tVData.add(tTransferData);
        // --------------------

        // 封装公共变量参数
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = tManageCom;
        tGI.Operator = tOperator;
        tVData.add(tGI);
        // --------------------

        GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
        if (!tGrpTbWorkFlowUI.submitData(tVData, "0000012199"))
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单，创建录入起始工作流节点失败。";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }

        String tStrSql = " select * from LWMission lwm where lwm.ActivityId = '0000012101' and ProcessId = '0000000012' "
                + " and lwm.MissionProp1 = '" + tPrtNo + "' ";

        LWMissionSet tMissionSet = new LWMissionDB().executeQuery(tStrSql);
        if (tMissionSet == null || tMissionSet.size() == 0)
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单，未找到录入工作流。";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }
        else if (tMissionSet.size() != 1)
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单，录入工作流出现冗余。";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }

        LWMissionSchema tMissionInfo = tMissionSet.get(1);

        String tProcessID = tMissionInfo.getProcessID();
        String tActivityID = tMissionInfo.getActivityID();
        String tMissionID = tMissionInfo.getMissionID();
        String tSubMissionID = tMissionInfo.getSubMissionID();

        TransferData tMissionTransferData = new TransferData();
        tMissionTransferData.setNameAndValue("ProcessID", tProcessID);
        tMissionTransferData.setNameAndValue("ActivityID", tActivityID);
        tMissionTransferData.setNameAndValue("MissionID", tMissionID);
        tMissionTransferData.setNameAndValue("SubMissionID", tSubMissionID);

        tMissionTransferData.setNameAndValue("GrpContNo", cGrpContInfo.getGrpContNo());
        tMissionTransferData.setNameAndValue("PrtNo", cGrpContInfo.getPrtNo());
        tMissionTransferData.setNameAndValue("SaleChnl", cGrpContInfo.getSaleChnl());
        tMissionTransferData.setNameAndValue("ManageCom", cGrpContInfo.getManageCom());
        tMissionTransferData.setNameAndValue("AgentCode", cGrpContInfo.getAgentCode());
        tMissionTransferData.setNameAndValue("AgentGroup", cGrpContInfo.getAgentGroup());
        tMissionTransferData.setNameAndValue("GrpName", cGrpContInfo.getGrpName());
        tMissionTransferData.setNameAndValue("CValiDate", cGrpContInfo.getCValiDate());
        tMissionTransferData.setNameAndValue("GrpNo", cGrpContInfo.getAppntNo());
        tMissionTransferData.setNameAndValue("UWDate", cGrpContInfo.getUWDate());
        tMissionTransferData.setNameAndValue("UWTime", cGrpContInfo.getUWTime());

        VData tMissionVData = new VData();
        tMissionVData.add(tGI);
        tMissionVData.add(tMissionTransferData);

        GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
        if (!tTbWorkFlowUI.submitData(tMissionVData, tActivityID))
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单，签单工作流流转失败。";
            if (!cImportLog.errLog(tPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(cImportLog.mErrors);
                return false;
            }
            return false;
        }

        return true;
    }

    private void sendPayNotice(String cPrtNo)
    {
        String tStrSql = " select * from LCGrpCont where PrtNo = '" + cPrtNo + "' ";

        LCGrpContSchema tGrpContInfo = null;
        LCGrpContDB tGrpContDB = new LCGrpContDB();
        LCGrpContSet tGrpContSet = tGrpContDB.executeQuery(tStrSql);
        if (tGrpContSet == null || tGrpContSet.size() <= 0)
        {
            return;
        }
        tGrpContInfo = tGrpContSet.get(1);

        String tGrpContNo = tGrpContInfo.getGrpContNo();
        String tPayMode = tGrpContInfo.getPayMode();

        // 只有非银行转帐方式，才发送缴费通知书
        if (tPayMode != null && !tPayMode.equals("04"))
        {
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema.setOtherNo(tGrpContNo);
            tLOPRTManagerSchema.setOtherNoType("01");
            tLOPRTManagerSchema.setCode("57");
            //校验是否发过首期交费通知书 ，如果发过则不再重发
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
            if (tLOPRTManagerDB.query().size() <= 0)
            {
                VData tempVData = new VData();
                tempVData.add(tLOPRTManagerSchema);
                tempVData.add(mGlobalInput);

                GrpUWSendPrintUI tGrpUWSendPrintUI = new GrpUWSendPrintUI();
                if (!tGrpUWSendPrintUI.submitData(tempVData, "INSERT"))
                {
                    System.out.println(tGrpUWSendPrintUI.mErrors.getErrContent());
                    System.out.println("未成功打印缴费通知书");
                    return;
                }
            }
        }
        // --------------------
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap cMMap)
    {
        VData data = new VData();
        data.add(cMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("系统超时，请稍后尝试重新提交，谢谢。");
            buildError("submitData", "系统超时，请稍后尝试重新提交，谢谢。");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    //是否已有签单数据
    private boolean checkExists(String aPrtNo)
    {
        String tStrSql = "select 1 from lcgrpcont where prtno = '" + aPrtNo + "' " + " union all "
                + " select 1 from lbgrpcont where prtno = '" + aPrtNo + "' ";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            return true;
        }
        return false;
    }

    /**
     * 校验是否存在银行数据
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkTempfee(String aPrtNo)
    {
        String tSql = "select 1 from ljtempfee where otherno = '" + aPrtNo
                + "' and enteraccdate is not null and confdate is null ";
        String tBankFlag = new ExeSQL().getOneValue(tSql);
        if (!"1".equals(tBankFlag))
        {
            return false;
        }
        return true;
    }

    private boolean checkScan(String tGrpName, String tPrtNo)
    {
        String tSql = " select 1 from LCGrpCont lgc inner join LCGrpPol lgp on lgp.GrpContNo = lgc.GrpContNo where lgc.CardFlag = '4' "
//        	    +" and lgp.RiskCode in ('560501', '460201') "
        	    +" and exists (select 1 from LICertifyImportLog where cardno=lgc.prtno and errortype='0' and batchno like 'GS%') "
                + " and lgc.GrpName = '" + tGrpName + "' ";
        String tResultFlag = new ExeSQL().getOneValue(tSql);
        if (!"1".equals(tResultFlag))
        {
            String tStrSql = " select 1 from Es_Doc_Main where SubType = 'TB09' and DocCode = '" + tPrtNo + "' ";
            String tScanFlag = new ExeSQL().getOneValue(tStrSql);
            if (!"1".equals(tScanFlag))
                return false;
            return true;
        }
        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        mLogger.error(szFunc + ":" + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "BriGrpBatchImportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
