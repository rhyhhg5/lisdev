package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BriGrpSignBL {
	
	/**存放结果*/
    private VData mVResult = new VData();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private TransferData mTransferData = new TransferData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mGrpContNo;

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println(">>>>>>submitData");
        //将操作数据拷贝到本类中
        //  mInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }
        LCGrpContSignBL tLCGrpContSignBL = new LCGrpContSignBL();

        boolean tSignResult = false;
        try
        {
            tSignResult = tLCGrpContSignBL.submitData(mInputData, null);
        }
        catch (Exception ex)
        {
            buildError("submitData", "签单异常失败" + ex.getMessage());
            return false;
        }

        if (!tSignResult)
        {
            if (!tLCGrpContSignBL.mErrors.needDealError())
            {
                CError.buildErr(this, "部分签单完成");
            }
            else
            {
                this.mErrors.copyAllErrors(tLCGrpContSignBL.mErrors);
            }
            mTransferData.setNameAndValue("FinishFlag", "0");
            // return false;
        }
        else
        {
            String tTmpProposalGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
            String tPrtNo = new ExeSQL().getOneValue("select PrtNo from LCGrpCont where ProposalGrpContNo = '"
                    + tTmpProposalGrpContNo + "'");

            if (tPrtNo == null || tPrtNo.equals(""))
            {
                buildError("submitData", "签单后查询保单印刷号失败！");
                return false;
            }

            // 自动回执回销保单
//            if (!addGetPolDate(tPrtNo))
//            {
//                return false;
//            }
            // --------------------
            if(!addPrintCountDate(tPrtNo)){
            	return false;
            }
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("PrtNo", tPrtNo);

            VData vData = new VData();

            vData.addElement(mGlobalInput);
            vData.addElement(tTransferData);

            mTransferData.setNameAndValue("FinishFlag", "1");
        }
        this.mVResult.add(mTransferData);
        String tProposalGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = null;
        if (tProposalGrpContNo != null && !tProposalGrpContNo.equals(""))
        {
            tLCGrpContDB.setProposalGrpContNo(tProposalGrpContNo);
            tLCGrpContSet = tLCGrpContDB.query();
            if (tLCGrpContSet == null || tLCGrpContSet.size() <= 0 || tLCGrpContSet.size() > 1)
            {
                buildError("submitData", "签单后查询保单信息失败！");
                return false;
            }
            LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);
            for (int i = 1; i <= tLCGrpContSchema.getFieldCount(); i++)
            {
                mTransferData.removeByName(tLCGrpContSchema.getFieldName(i - 1));
                mTransferData.setNameAndValue(tLCGrpContSchema.getFieldName(i - 1), tLCGrpContSchema.getV(i - 1));
            }
        }

        return tSignResult;
        //return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        this.mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            return false;
        }
        this.mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        if (this.mGrpContNo == null)
        {
            buildError("getInputData", "GrpContNo为null！");
            return false;
        }
        return true;
    }
    private boolean addGetPolDate(String cPrtNo)
    {
        MMap tMMap = new MMap();

        String tStrSql = "select * from LCGrpCont where PrtNo = '" + cPrtNo + "'";

        LCGrpContSchema tGrpContInfo = null;

        LCGrpContSet tGrpContInfoSet = new LCGrpContDB().executeQuery(tStrSql);
        if (tGrpContInfoSet == null || tGrpContInfoSet.size() != 1)
        {
            buildError("addGetPolDate", "获取签单后保单信息失败。");
            return false;
        }
        tGrpContInfo = tGrpContInfoSet.get(1);

        String tGrpContNo = tGrpContInfo.getGrpContNo();

        String tCutDate = PubFun.getCurrentDate();
        String tCutTime = PubFun.getCurrentTime();

        int iDelayDays = 10;
        String tCustomGetPolDate = PubFun.calDate(tCutDate, iDelayDays, "D", null);

        StringBuffer tStrBSql = null;

        // 处理团单数据
        tStrBSql = new StringBuffer();
        tStrBSql.append(" update LCGrpCont set ");
        tStrBSql.append(" CustomGetPolDate = '" + tCustomGetPolDate + "',");
        tStrBSql.append(" GetPolDate = '" + tCutDate + "',");
        tStrBSql.append(" ModifyDate = '" + tCutDate + "',");
        tStrBSql.append(" ModifyTime = '" + tCutTime + "',");
        tStrBSql.append(" Operator = '" + this.mGlobalInput.Operator + "' ");
        tStrBSql.append(" where GrpContNo = '" + tGrpContNo + "'");
        tMMap.put(tStrBSql.toString(), SysConst.UPDATE);
        tStrBSql = null;
        // --------------------

        // 处理分单数据
        tStrBSql = new StringBuffer();
        tStrBSql.append(" update LCCont set ");
        tStrBSql.append(" CustomGetPolDate = '" + tCustomGetPolDate + "',");
        tStrBSql.append(" GetPolDate = '" + tCutDate + "',");
        tStrBSql.append(" ModifyDate = '" + tCutDate + "',");
        tStrBSql.append(" ModifyTime = '" + tCutTime + "',");
        tStrBSql.append(" Operator = '" + this.mGlobalInput.Operator + "' ");
        tStrBSql.append(" where GrpContNo = '" + tGrpContNo + "'");
        tMMap.put(tStrBSql.toString(), SysConst.UPDATE);
        tStrBSql = null;
        // --------------------

        this.mVResult.add(tMMap);

        return true;
    }
    
    private boolean addPrintCountDate(String cPrtNo)
    {
        MMap tMMap = new MMap();

        String tStrSql = "select * from LCGrpCont where PrtNo = '" + cPrtNo + "'";

        LCGrpContSchema tGrpContInfo = null;

        LCGrpContSet tGrpContInfoSet = new LCGrpContDB().executeQuery(tStrSql);
        if (tGrpContInfoSet == null || tGrpContInfoSet.size() != 1)
        {
            buildError("addGetPolDate", "获取签单后保单信息失败。");
            return false;
        }
        tGrpContInfo = tGrpContInfoSet.get(1);

        String tGrpContNo = tGrpContInfo.getGrpContNo();

        String tCutDate = PubFun.getCurrentDate();
        String tCutTime = PubFun.getCurrentTime();


        StringBuffer tStrBSql = null;

        // 处理团单数据
        tStrBSql = new StringBuffer();
        tStrBSql.append(" update LCGrpCont set ");
        tStrBSql.append(" printcount = 1,");
        tStrBSql.append(" ModifyDate = '" + tCutDate + "',");
        tStrBSql.append(" ModifyTime = '" + tCutTime + "',");
        tStrBSql.append(" Operator = '" + this.mGlobalInput.Operator + "' ");
        tStrBSql.append(" where GrpContNo = '" + tGrpContNo + "'");
        tMMap.put(tStrBSql.toString(), SysConst.UPDATE);
        tStrBSql = null;
        // --------------------
        if(!submit(tMMap)){
        	return false;
        }
        
        this.mVResult.add(tMMap);

        return true;
    }

    public VData getResult()
    {
        return this.mVResult;
    }

    public TransferData getReturnTransferData()
    {
        return this.mTransferData;
    }

    public CErrors getErrors()
    {
        return this.mErrors;
    }
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap aMMap)
    {
        VData data = new VData();
        data.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("系统超时，请稍后尝试重新提交，谢谢。");
            buildError("submitData", "系统超时，请稍后尝试重新提交，谢谢。");
            return false;
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpContSignAfterInitService";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    
    public static void main(String[] args){
    	BriGrpSignBL tBriGrpSignBL = new BriGrpSignBL();
    	
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.Operator = "ly";
    	tGlobalInput.ManageCom = "86910000";
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("GrpContNo", "1400007936");
    	
    	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    	tLCGrpContDB.setGrpContNo("1400007936");
    	LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
    	
    	VData tVData = new VData();
    	tVData.add(tGlobalInput);
    	tVData.add(tTransferData);
    	tVData.add(tLCGrpContSet);
    	
    	tBriGrpSignBL.submitData(tVData, "test");
    }
}
