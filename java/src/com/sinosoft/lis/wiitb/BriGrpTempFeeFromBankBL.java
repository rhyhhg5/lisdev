package com.sinosoft.lis.wiitb;


import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BriGrpTempFeeFromBankBL {
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
    
    /**团单号*/
//    private String cGrpContNo = "";
    
    /**印刷号*/
    private String cPrtNo = "";
    
    
    public boolean submitData(VData cInputData, String cOperate)
    {
    	if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        return true;
    }
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }
    private boolean getInputData(VData cInputData, String cOperate)
    {

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }
        
//        cGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
//        if (cGrpContNo == null || "".equals(cGrpContNo))
//        {
//            buildError("getInputData", "生成暂收数据时，未获取到团单号码。");
//            return false;
//        }
        
        cPrtNo = (String) mTransferData.getValueByName("PrtNo");
        if (cPrtNo == null || "".equals(cPrtNo))
        {
            buildError("getInputData", "生成暂收数据时，未获取到团单印刷号码。");
            return false;
        }

        return true;
    }
    private boolean checkData()
    {
        return true;
    }
    private boolean dealData()
    {
    	MMap tTempFeeMap = null;
    	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    	String tSql = "select * from lcgrpcont where prtno = '"+cPrtNo+"' ";
    	LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(tSql);
    	if (tLCGrpContSet == null || tLCGrpContSet.size() != 1)
        {
            buildError("dealData", "生成暂收数据时，获取保单数据失败。");
            return false;
        }
    	tLCGrpContSchema = tLCGrpContSet.get(1);
    	if(!"1".equals(tLCGrpContSchema.getAppFlag())  //保单未签单时处理
    			&& "".equals(StrTool.cTrim(tLCGrpContSchema.getSignDate()))//保单未签单时处理
    			&& "4".equals(tLCGrpContSchema.getCardFlag())){//工伤险保单
    		if("4".equals(tLCGrpContSchema.getPayMode())){//4代表银行转账
        		tTempFeeMap = dealTempFeeDataOfBank(tLCGrpContSchema);
        		if(tTempFeeMap == null){
        			buildError("dealData", "生成暂收数据时，获取险种数据失败。");
                    return false;
        		}
        		
            	mMap.add(tTempFeeMap);
        	}else{
        		System.out.print("保单[印刷号码："+cPrtNo+"]缴费方式不是银行转账方式，不生成暂收数据！");
        	}
    	}else{
    		System.out.print("保单[印刷号码："+cPrtNo+"]已签单，不生成暂收数据！");
    	}
    	
        return true;
    }
    /**
     * 产生银行发盘暂收数据。
     * @param cContInfo
     * @return
     */
    private MMap dealTempFeeDataOfBank(LCGrpContSchema cContInfo)
    {
        MMap tMMap = new MMap();
        String LCGrpPolSql = "select * from lcgrppol where grpcontno = '"+cContInfo.getGrpContNo()+"'";
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
        tLCGrpPolSet = tLCGrpPolDB.executeQuery(LCGrpPolSql);
        if(tLCGrpPolSet == null || tLCGrpPolSet.size()<=0){
        	buildError("dealTempFeeDataOfBank", "生成暂收数据时，未获取到险种。");
        	return null;
        }else{
        	String prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO", cContInfo.getPrtNo());
            String serNo = PubFun1.CreateMaxNo("SERIALNO", cContInfo.getManageCom());
            
//            GregorianCalendar Calendar = new GregorianCalendar();
//            Calendar.setTime((new FDate()).getDate(PubFun.getCurrentDate()));
//            Calendar.add(Calendar.DATE, 0);
            
            LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
            double sumPrem = 0;
            for(int i=1;i<=tLCGrpPolSet.size();i++){
            	LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            	LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                tLJTempFeeSchema.setTempFeeNo(prtSeq);
                tLJTempFeeSchema.setTempFeeType("1");//团体保单新单收费
                tLJTempFeeSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());

                tLJTempFeeSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
//                tLJTempFeeSchema.setAPPntName(cContInfo.getappnt);
                tLJTempFeeSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
                tLJTempFeeSchema.setPayDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setPayMoney(tLCGrpPolSchema.getPrem());
                sumPrem +=tLCGrpPolSchema.getPrem();
                // --------------------
                tLJTempFeeSchema.setManageCom(tLCGrpPolSchema.getManageCom());
                tLJTempFeeSchema.setOtherNo(tLCGrpPolSchema.getPrtNo());
                tLJTempFeeSchema.setOtherNoType("5");//团体保单首期缴费
                tLJTempFeeSchema.setPolicyCom(tLCGrpPolSchema.getManageCom());
                tLJTempFeeSchema.setSerialNo(serNo);
                tLJTempFeeSchema.setConfFlag("0");//未核销
                tLJTempFeeSchema.setEnterAccDate("");
                tLJTempFeeSchema.setConfMakeDate("");
                tLJTempFeeSchema.setConfDate("");
                tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
                tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
                
                tLJTempFeeSet.add(tLJTempFeeSchema);
                
            }
            tMMap.put(tLJTempFeeSet, SysConst.INSERT);

            LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
            tLJTempFeeClassSchema.setTempFeeNo(prtSeq);
            tLJTempFeeClassSchema.setPayMode("4");
            tLJTempFeeClassSchema.setPayDate(PubFun.getCurrentDate());
            tLJTempFeeClassSchema.setPayMoney(sumPrem);
            tLJTempFeeClassSchema.setManageCom(cContInfo.getManageCom());
            tLJTempFeeClassSchema.setPolicyCom(cContInfo.getManageCom());

            String BankCode = cContInfo.getBankCode();
            String BankAccNo = cContInfo.getBankAccNo();
            String AccName = cContInfo.getAccName();
            tLJTempFeeClassSchema.setBankCode(BankCode);
            tLJTempFeeClassSchema.setBankAccNo(BankAccNo);
            tLJTempFeeClassSchema.setAccName(AccName);

            tLJTempFeeClassSchema.setSerialNo(serNo);
            tLJTempFeeClassSchema.setConfFlag("0");//未核销
            tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
            tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
            tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
            tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
            tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
            tLJTempFeeClassSchema.setEnterAccDate("");
            tLJTempFeeClassSchema.setConfMakeDate("");
            tLJTempFeeClassSchema.setConfDate("");

            tMMap.put(tLJTempFeeClassSchema, SysConst.INSERT);
            
            LJSPaySchema tLJSPaySchema = new LJSPaySchema();
            tLJSPaySchema.setGetNoticeNo(prtSeq);
            tLJSPaySchema.setOtherNo(cContInfo.getPrtNo());
            tLJSPaySchema.setOtherNoType("5");//团体保单首期缴费
            tLJSPaySchema.setAppntNo(cContInfo.getAppntNo());
            tLJSPaySchema.setSumDuePayMoney(sumPrem);
//            tLJSPaySchema.setPayDate(aPayDate);
            tLJSPaySchema.setBankOnTheWayFlag("0");
            tLJSPaySchema.setBankSuccFlag("0");
            tLJSPaySchema.setApproveDate(cContInfo.getApproveDate());
            tLJSPaySchema.setSerialNo(serNo);
            tLJSPaySchema.setOperator(mGlobalInput.Operator);
            tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
            tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
            tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
            tLJSPaySchema.setManageCom(cContInfo.getManageCom());
            
            tLJSPaySchema.setAgentCom(cContInfo.getAgentCom());
            tLJSPaySchema.setAgentType(cContInfo.getAgentType());
            tLJSPaySchema.setBankCode(BankCode);
            tLJSPaySchema.setBankAccNo(BankAccNo);
            tLJSPaySchema.setRiskCode("000000");
            tLJSPaySchema.setAgentCode(cContInfo.getAgentCode());
            tLJSPaySchema.setAgentGroup(cContInfo.getAgentGroup());
            tLJSPaySchema.setAccName(AccName);
            tLJSPaySchema.setCanSendBank("0");
            
            tLJSPaySchema.setStartPayDate(PubFun.getCurrentDate());
            tLJSPaySchema.setPayDate(PubFun.calDate(PubFun.getCurrentDate(),12,"M",null));
            
            tMMap.put(tLJSPaySchema, SysConst.INSERT);
        }

        return tMMap;
    }
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriGrpTempFeeFromBank";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    public static void main(String[] args){
    	BriGrpTempFeeFromBankBL tBriGrpTempFeeFromBankBL = new BriGrpTempFeeFromBankBL();
    	GlobalInput mGlobalInput = new GlobalInput();
    	mGlobalInput.Operator = "gzh";
    	TransferData mTransferData = new TransferData();
    	mTransferData.setNameAndValue("GrpContNo", "1400007895");
    	VData tVData = new VData();
        tVData.addElement(mTransferData);
        tVData.addElement(mGlobalInput);
        if(tBriGrpTempFeeFromBankBL.submitData(tVData,"test")){
        	tVData.add(tBriGrpTempFeeFromBankBL.mMap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(tVData, "DELETE&INSERT"))
            {
                System.out.println("保存数据失败了！");
            }
        }
        
    }
}
