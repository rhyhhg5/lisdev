/**
 * 2012-3-7
 */
package com.sinosoft.lis.wiitb.excimport;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BriGrpParseGuideInUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public BriGrpParseGuideInUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            BriGrpParseGuideIn tBriGrpParseGuideIn = new BriGrpParseGuideIn();
            if (!tBriGrpParseGuideIn.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tBriGrpParseGuideIn.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
