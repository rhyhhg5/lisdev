/**
 * 2012-3-7
 */
package com.sinosoft.lis.wiitb.excimport;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.tb.LCPolImpInfo;
import com.sinosoft.lis.wiitb.PubBriGrpBatchImportSYAPI;
import com.sinosoft.lis.wiitb.XmlFileUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XMLPathTool;

/**
 * @author LY
 *
 */
public class BriGrpParseGuideIn
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public CErrors logErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /**内存文件暂存*/
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String FileName;

    private String XmlFileName;

    private String mPreFilePath;

    private String FilePath = "";

    private String ParseRootPath = "/DATASET/BATCHNO";

    private String ParsePath = "CONTTABLE/ROW";

    //配置文件Xml节点描述
    private String ImportFileName;

    private String ConfigFileName;

    private String mBatchNo = "";

    private String mPrtNo = "";

    private String mContID = null;

    private org.w3c.dom.Document m_doc = null;

    private LCPolImpInfo m_LCPolImpInfo = new LCPolImpInfo();

    private String[] m_strDataFiles = null;

    private boolean ShowSchedule = false;

    //    boolean mIsBPO = false;

    public BriGrpParseGuideIn()
    {
        bulidDocument();
    }

    public BriGrpParseGuideIn(String fileName)
    {
        bulidDocument();
        FileName = fileName;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        getInputData();
        if (!checkData())
        {
            return false;
        }
        System.out.println("开始时间:" + PubFun.getCurrentTime());
        try
        {
            if (!parseVts())
            {
                CError tError = new CError();
                tError.moduleName = "TSParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = this.mErrors.getFirstError();
                logErrors.addOneError(tError);
                return false;
            }

            for (int nIndex = 0; nIndex < m_strDataFiles.length; nIndex++)
            {
                XmlFileName = m_strDataFiles[nIndex];
                if (!ParseXml())
                {
                    CError tError = new CError();
                    tError.moduleName = "TSParseGuideIn";
                    tError.functionName = "checkData";
                    tError.errorMessage = this.mErrors.getFirstError();
                    logErrors.addOneError(tError);
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "导入文件格式有误!";
            logErrors.addOneError(tError);
        }

        mErrors = logErrors;
        System.out.println("结束时间:" + PubFun.getCurrentTime());
        if (mErrors.getErrorCount() > 0)
        {
            return false;
        }

        return true;
    }

    /**
     * 得到传入数据
     */
    private void getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        mPreFilePath = (String) mTransferData.getValueByName("FilePath");
        System.out.println(mPreFilePath);
    }

    /**
     * 校验传输数据
     *
     * @return boolean
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无操作员信息，请重新登录!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无导入文件信息，请重新导入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FileName = (String) mTransferData.getValueByName("FileName");
            System.out.println("FileName: " + FileName);
        }
        return true;
    }

    /**
     * 得到生成文件路径
     *
     * @return boolean
     */
    private boolean getFilePath()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("WiiXmlImportDir");
        if (!tLDSysVarDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "缺少文件导入路径!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FilePath = tLDSysVarDB.getSysVarValue();
        }

        return true;
    }

    /**
     * 检验文件是否存在
     *
     * @return boolean
     */
    private boolean checkXmlFileName()
    {
        File tFile = new File(XmlFileName);
        if (!tFile.exists())
        {
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("WiiXmlImportDir");
            if (!tLDSysVarDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "缺少文件导入路径!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                FilePath = tLDSysVarDB.getSysVarValue();
            }

            File tFile1 = new File(FilePath);
            if (!tFile1.exists())
            {
                tFile1.mkdirs();
            }
            XmlFileName = FilePath + XmlFileName;
            File tFile2 = new File(XmlFileName);
            if (!tFile2.exists())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "请上传相应的数据文件到指定路径" + FilePath + "!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        //得到批次号
        XMLPathTool tXPT = new XMLPathTool(XmlFileName);
        if (!getBatchNo(tXPT))
        {
            return false;
        }

        return true;
    }

    /**
     * 检查导入配置文件是否存在
     *
     * @return boolean
     */
    private boolean checkImportConfig()
    {
        this.getFilePath();

        String filePath = mPreFilePath + FilePath;
        //String filePath = "E:/v.1.1/ui/temp/";
        File tFile1 = new File(filePath);
        if (!tFile1.exists())
        {
            //初始化创建目录
            tFile1.mkdirs();
        }

        ConfigFileName = filePath + "BriGrpImport.xml";
        File tFile2 = new File(ConfigFileName);
        if (!tFile2.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "请上传配置文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 初始化上传文件
     *
     * @return boolean
     */
    private boolean initImportFile()
    {
        this.getFilePath();
        ImportFileName = mPreFilePath + FilePath + FileName;
        //ImportFileName = "E:/v.1.1/ui/temp/" + FileName;

        File tFile = new File(ImportFileName);
        if (!tFile.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "未上传文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-----导入文件");
        return true;
    }

    /**
     * 解析excel并转换成xml文件
     *
     * @return boolean
     * @throws Exception
     */
    private boolean parseVts() throws Exception
    {
        //初始化导入文件
        if (!this.initImportFile())
        {
            return false;
        }
        //检查导入配置文件是否存在
        if (!this.checkImportConfig())
        {
            return false;
        }

        BriGrpPolVTSParser lcpvp = new BriGrpPolVTSParser();

        if (!lcpvp.setFileName(ImportFileName))
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }
        if (!lcpvp.setConfigFileName(ConfigFileName))
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }
        //转换excel到xml
        if (!lcpvp.transform())
        {
            mErrors.copyAllErrors(lcpvp.mErrors);
            return false;
        }

        // 得到生成的XML文件名列表
        m_strDataFiles = lcpvp.getDataFiles();
        return true;
    }

    /**
     * 解析xml
     *
     * @return boolean
     */
    private boolean ParseXml()
    {
        if (!checkXmlFileName())
        {
            return false;
        }
        this.mErrors.clearErrors();

        //得到保单的传入信息
        //        XMLPathTool tXPT = new XMLPathTool(XmlFileName);
        //        NodeList nodeList = tXPT.parseN(ParsePath);

        // 转换报文格式
        Document tInXmlDoc = chgXml(XmlFileName);
        if (tInXmlDoc == null)
        {
            return false;
        }
        // --------------------

        // 数据导入
        if (!importBriGrpPolicy(tInXmlDoc))
        {
            return false;
        }
        // --------------------

        //解析完删除XML文件
        File tFile = new File(XmlFileName);
        if (tFile.exists())
        {
            //            tXPT = null;
            if (!tFile.delete())
            {
                System.out.println("删除Ｘｍｌ文件失败！");
            }
            else
            {
                System.out.println("删除Ｘｍｌ文件成功！");
            }
        }

        return true;
    }

    /**
     * getBatchNo
     * 得到批次信息BatchNo、BPOBatchNo
     * @return boolean
     */
    private boolean getBatchNo(XMLPathTool tXPT)
    {
        try
        {
            //批次号
            mBatchNo = tXPT.parseX(ParseRootPath).getFirstChild().getNodeValue();

            if (mBatchNo == null || mBatchNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "TSParseGuideIn";
                tError.functionName = "ParseXml";
                tError.errorMessage = "没有获得批次号";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "TSParseGuideIn";
            tError.functionName = "getBatchNo";
            tError.errorMessage = "获取批次号信息出现异常。";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    private boolean importBriGrpPolicy(Document cInXmlDoc)
    {
        // 调用承保导入接口，处理报文
        VData tVData = new VData();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", mBatchNo);
        tTransferData.setNameAndValue("InXmlDoc", cInXmlDoc);

        tVData.add(tTransferData);
        tVData.add(mGlobalInput);

        PubBriGrpBatchImportSYAPI tBusLogicBL = new PubBriGrpBatchImportSYAPI();
        if (!tBusLogicBL.submitData(tVData, "Import"))
        {
            buildError("importBriGrpPolicy", tBusLogicBL.mErrors.getFirstError());
            return false;
        }
        // --------------------

        return true;
    }

    private Document chgXml(String cXmlFileName)
    {
        // 读取文件，转换成报文数据Docment
        Document tInXmlDoc = loadFile4Xml(cXmlFileName);
        if (tInXmlDoc == null)
        {
            return null;
        }
        // --------------------

        // 将报文数据转换成工伤险所需报文
        Element tEleChgRoot = new Element("DataSet");

        Element tEleChgHead = new Element("MsgHead");
        tEleChgRoot.addContent(tEleChgHead);

        Element tEleChgHeadBatchNo = new Element("BatchNo");
        tEleChgHeadBatchNo.setText(mBatchNo);
        tEleChgHead.addContent(tEleChgHeadBatchNo);

        Element tEleChgHeadSendDate = new Element("SendDate");
        tEleChgHeadSendDate.setText(PubFun.getCurrentDate());
        tEleChgHead.addContent(tEleChgHeadSendDate);

        Element tEleChgHeadSendTime = new Element("SendTime");
        tEleChgHeadSendTime.setText(PubFun.getCurrentTime());
        tEleChgHead.addContent(tEleChgHeadSendTime);

        Element tEleChgHeadBranchCode = new Element("BranchCode");
        tEleChgHeadBranchCode.setText("SYGSSys");
        tEleChgHead.addContent(tEleChgHeadBranchCode);

        Element tEleChgHeadSendOperator = new Element("SendOperator");
        tEleChgHeadSendOperator.setText("SYGSSys");
        tEleChgHead.addContent(tEleChgHeadSendOperator);

        Element tEleChgHeadMsgType = new Element("MsgType");
        tEleChgHeadMsgType.setText("WIIPI99999");
        tEleChgHead.addContent(tEleChgHeadMsgType);

        Element tEleChgPolicy = new Element("PolicyInfo");
        tEleChgRoot.addContent(tEleChgPolicy);

        Element tEleRoot = tInXmlDoc.getRootElement();

        List tDataList = tEleRoot.getChild("CONTTABLE").getChildren("ROW");
        if (tDataList == null || tDataList.size() == 0)
        {
            buildError("chgXml", "未找到保单数据，请核查数据导入文件");
            return null;
        }

        for (int i = 0; i < tDataList.size(); i++)
        {
            Element tEleChgPolicyItem = new Element("Item");
            tEleChgPolicy.addContent(tEleChgPolicyItem);

            // 转换保单数据
            Element tEleData = (Element) tDataList.get(i);

            String tDataPrtNo = tEleData.getChildTextTrim("PRTNO");
            System.out.println("[" + tDataPrtNo + "] 开始解析");

            Element tElePolicy = tEleData.getChild("POLICYTABLE");

            String tPrtNo = tElePolicy.getChildText("PrtNo");
            Element tEleChgPolicyItemPrtNo = new Element("PrtNo");
            tEleChgPolicyItemPrtNo.setText(tPrtNo);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemPrtNo);

            String tManageCom = tElePolicy.getChildText("ManageCom");
            Element tEleChgPolicyItemManageCom = new Element("ManageCom");
            tEleChgPolicyItemManageCom.setText(tManageCom);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemManageCom);

            String tSaleChnl = tElePolicy.getChildText("SaleChnl");
            Element tEleChgPolicyItemSaleChnl = new Element("SaleChnl");
            tEleChgPolicyItemSaleChnl.setText(tSaleChnl);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemSaleChnl);

            String tMarketType = tElePolicy.getChildText("MarketType");
            Element tEleChgPolicyItemMarketType = new Element("MarketType");
            tEleChgPolicyItemMarketType.setText(tMarketType);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemMarketType);

            String tAgentCom = tElePolicy.getChildText("AgentCom");
            Element tEleChgPolicyItemAgentCom = new Element("AgentCom");
            tEleChgPolicyItemAgentCom.setText(tAgentCom);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemAgentCom);
            //add by zjd 集团统一工号保存老的工号修改
            ExeSQL tExeSQL=new ExeSQL();
            String tAgentCode = tExeSQL.getOneValue("select getAgentCode('"+tElePolicy.getChildText("AgentCode")+"') from dual ");
            if("".equals(tAgentCode) || tAgentCode==null){
            	tAgentCode=tElePolicy.getChildText("AgentCode");
            }
            Element tEleChgPolicyItemtAgentCodem = new Element("AgentCode");
            tEleChgPolicyItemtAgentCodem.setText(tAgentCode);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemtAgentCodem);

            String tAgentName = tElePolicy.getChildText("AgentName");
            Element tEleChgPolicyItemAgentName = new Element("AgentName");
            tEleChgPolicyItemAgentName.setText(tAgentName);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemAgentName);

            String tPolApplyDate = tElePolicy.getChildText("PolApplyDate");
            Element tEleChgPolicyItemPolApplyDate = new Element("PolApplyDate");
            tEleChgPolicyItemPolApplyDate.setText(tPolApplyDate);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemPolApplyDate);

            String tHandlerName = tElePolicy.getChildText("HandlerName");
            Element tEleChgPolicyItemHandlerName = new Element("HandlerName");
            tEleChgPolicyItemHandlerName.setText(tHandlerName);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemHandlerName);

            String tFirstTrialOperator = tElePolicy.getChildText("FirstTrialOperator");
            Element tEleChgPolicyItemFirstTrialOperator = new Element("FirstTrialOperator");
            tEleChgPolicyItemFirstTrialOperator.setText(tFirstTrialOperator);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemFirstTrialOperator);

            String tPayMode = tElePolicy.getChildText("PayMode");
            Element tEleChgPolicyItemPayMode = new Element("PayMode");
            tEleChgPolicyItemPayMode.setText(tPayMode);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemPayMode);

            String tPayIntv = tElePolicy.getChildText("PayIntv");
            Element tEleChgPolicyItemPayIntv = new Element("PayIntv");
            tEleChgPolicyItemPayIntv.setText(tPayIntv);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemPayIntv);

            String tPremScope = tElePolicy.getChildText("PremScope");
            Element tEleChgPolicyItemPremScope = new Element("PremScope");
            tEleChgPolicyItemPremScope.setText(tPremScope);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemPremScope);

            String tBankCode = tElePolicy.getChildText("BankCode");
            Element tEleChgPolicyItemBankCode = new Element("BankCode");
            tEleChgPolicyItemBankCode.setText(tBankCode);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemBankCode);

            String tBankAccNo = tElePolicy.getChildText("BankAccNo");
            Element tEleChgPolicyItemBankAccNo = new Element("BankAccNo");
            tEleChgPolicyItemBankAccNo.setText(tBankAccNo);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemBankAccNo);

            String tAccName = tElePolicy.getChildText("AccName");
            Element tEleChgPolicyItemAccName = new Element("AccName");
            tEleChgPolicyItemAccName.setText(tAccName);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemAccName);

            String tCValiDate = tElePolicy.getChildText("CValiDate");
            Element tEleChgPolicyItemCValiDate = new Element("CValiDate");
            tEleChgPolicyItemCValiDate.setText(tCValiDate);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemCValiDate);

            String tCInValiDate = tElePolicy.getChildText("CInValiDate");
            Element tEleChgPolicyItemCInValiDate = new Element("CInValiDate");
            tEleChgPolicyItemCInValiDate.setText(tCInValiDate);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemCInValiDate);

            String tRemark = tElePolicy.getChildText("Remark");
            Element tEleChgPolicyItemRemark = new Element("Remark");
            tEleChgPolicyItemRemark.setText(tRemark);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemRemark);

            String tPeoples3 = tElePolicy.getChildText("Peoples3");
            Element tEleChgPolicyItemPeoples3 = new Element("Peoples3");
            tEleChgPolicyItemPeoples3.setText(tPeoples3);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemPeoples3);

            String tOnWorkPeoples = tElePolicy.getChildText("OnWorkPeoples");
            Element tEleChgPolicyItemOnWorkPeoples = new Element("OnWorkPeoples");
            tEleChgPolicyItemOnWorkPeoples.setText(tOnWorkPeoples);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemOnWorkPeoples);

            String tOffWorkPeoples = tElePolicy.getChildText("OffWorkPeoples");
            Element tEleChgPolicyItemOffWorkPeoples = new Element("OffWorkPeoples");
            tEleChgPolicyItemOffWorkPeoples.setText(tOffWorkPeoples);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemOffWorkPeoples);

            String tOtherPeoples = tElePolicy.getChildText("OtherPeoples");
            Element tEleChgPolicyItemOtherPeoples = new Element("OtherPeoples");
            tEleChgPolicyItemOtherPeoples.setText(tOtherPeoples);
            tEleChgPolicyItem.addContent(tEleChgPolicyItemOtherPeoples);

            Element tChgPolicyItemGrpAppntInfo = new Element("GrpAppntInfo");
            tEleChgPolicyItem.addContent(tChgPolicyItemGrpAppntInfo);

            String tCustomerNo = "";//tElePolicy.getChildText("CustomerNo");
            Element tChgPolicyItemGrpAppntInfoCustomerNo = new Element("CustomerNo");
            tChgPolicyItemGrpAppntInfoCustomerNo.setText(tCustomerNo);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoCustomerNo);

            String tGrpName = tElePolicy.getChildText("GrpName");
            Element tChgPolicyItemGrpAppntInfoGrpName = new Element("GrpName");
            tChgPolicyItemGrpAppntInfoGrpName.setText(tGrpName);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoGrpName);

            String tOrgancomCode = tElePolicy.getChildText("OrgancomCode");
            Element tChgPolicyItemGrpAppntInfoOrgancomCode = new Element("OrgancomCode");
            tChgPolicyItemGrpAppntInfoOrgancomCode.setText(tOrgancomCode);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoOrgancomCode);

            String tOtherCertificates = tElePolicy.getChildText("OtherCertificates");
            Element tChgPolicyItemGrpAppntInfoOtherCertificates = new Element("OtherCertificates");
            tChgPolicyItemGrpAppntInfoOtherCertificates.setText(tOtherCertificates);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoOtherCertificates);

            String tGrpAddress = tElePolicy.getChildText("GrpAddress");
            Element tChgPolicyItemGrpAppntInfoGrpAddress = new Element("GrpAddress");
            tChgPolicyItemGrpAppntInfoGrpAddress.setText(tGrpAddress);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoGrpAddress);

            String tGrpZipCode = tElePolicy.getChildText("GrpZipCode");
            Element tChgPolicyItemGrpAppntInfoGrpZipCode = new Element("GrpZipCode");
            tChgPolicyItemGrpAppntInfoGrpZipCode.setText(tGrpZipCode);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoGrpZipCode);

            String tLinkMan1 = tElePolicy.getChildText("LinkMan1");
            Element tChgPolicyItemGrpAppntInfoLinkMan1 = new Element("LinkMan1");
            tChgPolicyItemGrpAppntInfoLinkMan1.setText(tLinkMan1);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoLinkMan1);

            String tPhone1 = tElePolicy.getChildText("Phone1");
            Element tChgPolicyItemGrpAppntInfoPhone1 = new Element("Phone1");
            tChgPolicyItemGrpAppntInfoPhone1.setText(tPhone1);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoPhone1);

            String tGrpLevel = tElePolicy.getChildText("GrpLevel");
            Element tChgPolicyItemGrpAppntInfoGrpLevel = new Element("GrpLevel");
            tChgPolicyItemGrpAppntInfoGrpLevel.setText(tGrpLevel);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoGrpLevel);

            String tWrapFlag = tElePolicy.getChildText("WrapFlag");
            Element tChgPolicyItemGrpAppntInfoWrapFlag = new Element("WrapFlag");
            tChgPolicyItemGrpAppntInfoWrapFlag.setText(tWrapFlag);
            tChgPolicyItemGrpAppntInfo.addContent(tChgPolicyItemGrpAppntInfoWrapFlag);
            
            String tCounty = tElePolicy.getChildText("County");
            Element tChgPolicyItemGrpAppntInfoCounty = new Element("County");
            tChgPolicyItemGrpAppntInfoCounty.setText(tCounty);
            tEleChgPolicyItem.addContent(tChgPolicyItemGrpAppntInfoCounty);
            
            String tCustomerBankCode = tElePolicy.getChildText("CustomerBankCode");
            Element tChgPolicyItemGrpAppntInfoCustomerBankCode = new Element("CustomerBankCode");
            tChgPolicyItemGrpAppntInfoCustomerBankCode.setText(tCustomerBankCode);
            tEleChgPolicyItem.addContent(tChgPolicyItemGrpAppntInfoCustomerBankCode);
            
            String tAuthorizeCode = tElePolicy.getChildText("AuthorizeCode");
            Element tChgPolicyItemGrpAppntInfoAuthorizeCode = new Element("AuthorizeCode");
            tChgPolicyItemGrpAppntInfoAuthorizeCode.setText(tAuthorizeCode);
            tEleChgPolicyItem.addContent(tChgPolicyItemGrpAppntInfoAuthorizeCode);
            //共保信息
            String tCoInsuranceFlag = tElePolicy.getChildText("CoInsuranceFlag");
            Element tChgPolicyItemCoInsuranceFlag = new Element("CoInsuranceFlag");
            tChgPolicyItemCoInsuranceFlag.setText(tCoInsuranceFlag);
            tEleChgPolicyItem.addContent(tChgPolicyItemCoInsuranceFlag);
            
            String tCoAgentCom = tElePolicy.getChildText("CoAgentCom");
            Element tChgPolicyItemCoAgentCom = new Element("CoAgentCom");
            tChgPolicyItemCoAgentCom.setText(tCoAgentCom);
            tEleChgPolicyItem.addContent(tChgPolicyItemCoAgentCom);
            
            String tCoAgentComName = tElePolicy.getChildText("CoAgentComName");
            Element tChgPolicyItemCoAgentComName = new Element("CoAgentComName");
            tChgPolicyItemCoAgentComName.setText(tCoAgentComName);
            tEleChgPolicyItem.addContent(tChgPolicyItemCoAgentComName);
            
            String tRate = tElePolicy.getChildText("Rate");
            Element tChgPolicyItemRate = new Element("Rate");
            tChgPolicyItemRate.setText(tRate);
            tEleChgPolicyItem.addContent(tChgPolicyItemRate);
            
            String tCoAgentCom1 = tElePolicy.getChildText("CoAgentCom1");
            Element tChgPolicyItemCoAgentCom1 = new Element("CoAgentCom1");
            tChgPolicyItemCoAgentCom1.setText(tCoAgentCom1);
            tEleChgPolicyItem.addContent(tChgPolicyItemCoAgentCom1);
            
            String tCoAgentComName1 = tElePolicy.getChildText("CoAgentComName1");
            Element tChgPolicyItemCoAgentComName1 = new Element("CoAgentComName1");
            tChgPolicyItemCoAgentComName1.setText(tCoAgentComName1);
            tEleChgPolicyItem.addContent(tChgPolicyItemCoAgentComName1);
            
            String tRate1 = tElePolicy.getChildText("Rate1");
            Element tChgPolicyItemRate1 = new Element("Rate1");
            tChgPolicyItemRate1.setText(tRate1);
            tEleChgPolicyItem.addContent(tChgPolicyItemRate1);
        }

        Document tDocChgData = new Document(tEleChgRoot);
        // --------------------

        return tDocChgData;
    }

    private Document loadFile4Xml(String cXmlFileName)
    {
        String tFileName = cXmlFileName;

        Document tXmlDoc = null;
        tXmlDoc = XmlFileUtil.xmlFile2Doc(tFileName);
        if (tXmlDoc == null)
        {
            buildError("loadFile4Xml", "解析Xml文件失败。");
            return null;
        }

        return tXmlDoc;
    }

    public String getExtendFileName(String aFileName)
    {
        File tFile = new File(aFileName);
        String aExtendFileName = "";
        String name = tFile.getName();
        for (int i = name.length() - 1; i >= 0; i--)
        {
            if (i < 1)
            {
                i = 1;
            }
            if (name.substring(i - 1, i).equals("."))
            {
                aExtendFileName = name.substring(i, name.length());
                System.out.println("ExtendFileName;" + aExtendFileName);
                return aExtendFileName;
            }
        }
        return aExtendFileName;
    }

    /**
     * 字符串替换
     *
     * @param s1 String
     * @param OriginStr String
     * @param DestStr String
     * @return java.lang.String
     */
    public static String replace(String s1, String OriginStr, String DestStr)
    {
        s1 = s1.trim();
        int mLenOriginStr = OriginStr.length();
        for (int j = 0; j < s1.length(); j++)
        {
            int befLen = s1.length();
            if (s1.substring(j, j + 1) == null || s1.substring(j, j + 1).trim().equals(""))
            {
                continue;
            }
            else
            {
                if (OriginStr != null && DestStr != null)
                {
                    if (j + mLenOriginStr <= s1.length())
                    {

                        if (s1.substring(j, j + mLenOriginStr).equals(OriginStr))
                        {

                            OriginStr = s1.substring(j, j + mLenOriginStr);

                            String startStr = s1.substring(0, j);
                            String endStr = s1.substring(j + mLenOriginStr, s1.length());

                            s1 = startStr + DestStr + endStr;

                            j = j + s1.length() - befLen;
                            if (j < 0)
                            {
                                j = 0;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return s1;
    }

    /**
     * 得到日志显示结果
     *
     * @return com.sinosoft.utility.VData
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
    }

    /**
     * Build a instance document object for function transfromNode()
     */
    private void bulidDocument()
    {
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware(true);

        try
        {
            m_doc = dfactory.newDocumentBuilder().newDocument();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * 创建错误日志
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ParseGuideIn";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        CErrors tCErrors = new CErrors();
        tCErrors.addOneError(cError);
    }
}
