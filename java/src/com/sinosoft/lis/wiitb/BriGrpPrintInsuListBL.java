/**
 * 
 */
package com.sinosoft.lis.wiitb;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * @author zhangyang
 *
 */
public class BriGrpPrintInsuListBL implements PrintService {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mOperate = "";

    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private LCGrpContSchema mGrpContInfo = null;

    private String mLoadFlag;

    private String mflag = null;
    
    public BriGrpPrintInsuListBL(){}
    
    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        try
        {
            if (!getInputData(cInputData))
            {
                return false;
            }
            mResult.clear();

            // 准备所有要打印的数据
            if (!getPrintData())
            {
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", ex.toString());
            return false;
        }
    }

    public static void main(String[] args)
    {
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        
        mLOPRTManagerSchema.setSchema((LOPRTManagerSchema) cInputData
                .getObjectByObjectName("LOPRTManagerSchema", 0));

        TransferData tTransferData = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        if (tTransferData != null)
        {
            this.mLoadFlag = (String) tTransferData.getValueByName("LoadFlag");
        }
        //只赋给schema一个prtseq

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        mGrpContInfo = loadGrpContInfo(mLOPRTManagerSchema.getOtherNo());
        if (mGrpContInfo == null)
        {
            buildError("getInputData", "未找到团体合同信息。");
            return false;
        }

        mflag = mOperate;
        
        String tStrPrtSeq = PubFun1.CreateMaxNo("PRTSEQNO", null);
        if (tStrPrtSeq == null || tStrPrtSeq.equals(""))
        {
            buildError("dealPrintManager", "生成打印流水号失败。");
            return false;
        }

        mLOPRTManagerSchema.setPrtSeq(tStrPrtSeq);

        mLOPRTManagerSchema.setOtherNoType("16");

        mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("1");

        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());

        return true;
    }

    //得到返回值
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BriGrpPrintInsuListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    // 准备所有要打印的数据
    private boolean getPrintData()
    {
        XmlExport xmlExport = new XmlExport();
        TextTag tTextTag = new TextTag();

        xmlExport.createDocument("BriGrpPrintInsuList", "");

        // 预设pdf接口节点。
        if (!dealPdfBaseInfo(tTextTag))
        {
            return false;
        }
        // ------------------------------

        // 处理团体保单层信息。
        if (!dealGrpContInfo(tTextTag))
        {
            return false;
        }
        // ------------------------------
        
        if (tTextTag.size() > 0)
        {
            xmlExport.addTextTag(tTextTag);
        }

        // 处理卡折清单。
        if (!dealInsuListInfo(xmlExport))
        {
            return false;
        }
        // ------------------------------

        // 置文件终止节点。
        if (!setEndNode(xmlExport))
        {
            return false;
        }
        // ------------------------------

        //xmlExport.outputDocumentToFile("e:\\", "CCSFirstPay");

        mResult.clear();
        mResult.addElement(xmlExport);

        // 处理打印轨迹。
        mLOPRTManagerSchema.setStateFlag("1");
        mLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setExeOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema
                .setPrintTimes(mLOPRTManagerSchema.getPrintTimes() + 1);
        mResult.add(mLOPRTManagerSchema);
        // ------------------------------

        return true;
    }

    /**
     * 产生PDF打印相关基本信息。
     * @param cTextTag
     * @return
     */
    private boolean dealPdfBaseInfo(TextTag cTextTag)
    {
        TextTag tTmpTextTag = cTextTag;

        // 单证类型标志。
        tTmpTextTag.add("JetFormType", "GSL01");
        // -------------------------------

        // 四位管理机构代码。
        String sqlusercom = "select comcode from lduser where usercode='"
                + mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600")
                || comcode.equals("86000000"))
        {
            comcode = "86";
        }
        else if (comcode.length() >= 4)
        {
            comcode = comcode.substring(0, 4);
        }
        else
        {
            buildError("dealGrpContInfo", "操作员机构查询出错!");
            return false;
        }
        String printcom = "select codename from ldcode where codetype='pdfprintcom' and code='"
                + comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        tTmpTextTag.add("ManageComLength4", printcode);
        // -------------------------------

        // 客户端IP。
        tTmpTextTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        // -------------------------------

        // 预览标志。
        if (mflag.equals("batch"))
        {
            tTmpTextTag.add("previewflag", "0");
        }
        else
        {
            tTmpTextTag.add("previewflag", "1");
        }
        // -------------------------------

        return true;
    }

    /**
     * 处理团体保单层信息。
     * @param cTextTag
     * @return
     */
    private boolean dealGrpContInfo(TextTag cTextTag)
    {
        TextTag tTmpTextTag = cTextTag;

        tTmpTextTag.add("GrpContNo", mGrpContInfo.getGrpContNo());
        tTmpTextTag.add("PrtNo", mGrpContInfo.getPrtNo());
        tTmpTextTag.add("GrpName", mGrpContInfo.getGrpName());

        String tManageCom = mGrpContInfo.getManageCom();
        LDComDB tComInfo = new LDComDB();
        tComInfo.setComCode(tManageCom);
        if (!tComInfo.getInfo())
        {
            buildError("dealGrpContInfo", "管理机构[" + tManageCom + "]信息未找到。");
            return false;
        }
        tTmpTextTag.add("ManageCom", mGrpContInfo.getManageCom());
        
        tTmpTextTag.add("Today", getDate(PubFun.getCurrentDate()));
        return true;
    }

    private boolean dealInsuListInfo(XmlExport cXmlExport)
    {
        SSRS tGrpInsuInfoList = null;
        String tStrSql = " select "
                + " lcid.Name, CodeName('sex', lcid.Sex) Sex, lcid.BirthDay, "
                + " CodeName('idtype', lcid.IDType) IDType, lcid.IDNo, varchar(sum(lcp.Prem)) Prem, "
                + " lcid.BankCode, lcid.AccName, lcid.BankAccNo "
                + " from lcgrpcont lgc  "
                + " inner join lcinsured lcid on lcid.GrpContNo = lgc.GrpContNo "
                + " inner join lcpol lcp on lcp.GrpContNo = lgc.GrpContNo and lcp.InsuredNo = lcid.InsuredNo "
                + " where 1 = 1 "
                + " and lgc.GrpContNo = '" + mGrpContInfo.getGrpContNo() + "' "
                + " group by lcid.Name,lcid.Sex,lcid.BirthDay,lcid.IDType,lcid.IDNo,lcid.BankCode,lcid.AccName,lcid.BankAccNo "
                + " union all "
                + " select "
                + " lcid.Name, CodeName('sex', lcid.Sex) Sex, lcid.BirthDay, "
                + " CodeName('idtype', lcid.IDType) IDType, lcid.IDNo, varchar(sum(lcp.Prem)) Prem, "
                + " lcid.BankCode, lcid.AccName, lcid.BankAccNo "
                + " from lbgrpcont lgc  "
                + " inner join lbinsured lcid on lcid.GrpContNo = lgc.GrpContNo "
                + " inner join lbpol lcp on lcp.GrpContNo = lgc.GrpContNo and lcp.InsuredNo = lcid.InsuredNo "
                + " where 1 = 1 " 
                + " and lgc.GrpContNo = '" + mGrpContInfo.getGrpContNo() + "' "
                + " group by lcid.Name,lcid.Sex,lcid.BirthDay,lcid.IDType,lcid.IDNo,lcid.BankCode,lcid.AccName,lcid.BankAccNo "
                ;
        
        tGrpInsuInfoList = new ExeSQL().execSQL(tStrSql);

        if (tGrpInsuInfoList == null || tGrpInsuInfoList.MaxRow == 0)
        {
            buildError("dealCardListInfo", "查询卡折清单出错。");
            return false;
        }

        String[] tGrpInsuListInfoTitle = new String[10];

        tGrpInsuListInfoTitle[0] = "Id";
        tGrpInsuListInfoTitle[1] = "Name";
        tGrpInsuListInfoTitle[2] = "Sex";
        tGrpInsuListInfoTitle[3] = "BirthDay";
        tGrpInsuListInfoTitle[4] = "IDType";
        tGrpInsuListInfoTitle[5] = "IDNo";
        tGrpInsuListInfoTitle[6] = "Prem";
        tGrpInsuListInfoTitle[7] = "BankCode";
        tGrpInsuListInfoTitle[8] = "AccName";
        tGrpInsuListInfoTitle[9] = "BankAccNo";

        ListTable tListTable = new ListTable();
        tListTable.setName("GrpInsuListInfo");

        String[] oneInsuInfo = null;

        for (int i = 1; i <= tGrpInsuInfoList.MaxRow; i++)
        {
            String[] tTmpCardInfo = tGrpInsuInfoList.getRowData(i);

            oneInsuInfo = new String[10];
            oneInsuInfo[0] = String.valueOf(i);
            oneInsuInfo[1] = tTmpCardInfo[0];
            oneInsuInfo[2] = tTmpCardInfo[1];
            oneInsuInfo[3] = tTmpCardInfo[2];
            oneInsuInfo[4] = tTmpCardInfo[3];
            oneInsuInfo[5] = tTmpCardInfo[4];
            oneInsuInfo[6] = tTmpCardInfo[5];
            oneInsuInfo[7] = tTmpCardInfo[6];
            oneInsuInfo[8] = tTmpCardInfo[7];
            oneInsuInfo[9] = tTmpCardInfo[8];

            tListTable.add(oneInsuInfo);
        }

        cXmlExport.addListTable(tListTable, tGrpInsuListInfoTitle);

        return true;
    }

    /**
     * 设置文件终止节点。
     * @param cXmlExport
     * @return
     */
    private boolean setEndNode(XmlExport cXmlExport)
    {
        ListTable tEndTable = new ListTable();

        tEndTable.setName("END");
        String[] tEndTitle = new String[0];

        cXmlExport.addListTable(tEndTable, tEndTitle);

        return true;
    }

    /**
     * 根据团体合同号，加载团体保单信息。
     * @param cGrpContNo
     * @return
     */
    private LCGrpContSchema loadGrpContInfo(String cGrpContNo)
    {
        LCGrpContDB tGrpContInfo = new LCGrpContDB();
        tGrpContInfo.setGrpContNo(cGrpContNo);
        if (!tGrpContInfo.getInfo())
        {
            return null;
        }

        return tGrpContInfo.getSchema();
    }

    public CErrors getErrors()
    {
        return null;
    }
    
    private String getDate(String cDate)
    {
        Date tDate = new FDate().getDate(cDate);
        if (tDate == null)
        {
            System.out.println("[" + cDate + "]日期转换失败");
            return cDate;
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        return df.format(tDate);
    }
}
