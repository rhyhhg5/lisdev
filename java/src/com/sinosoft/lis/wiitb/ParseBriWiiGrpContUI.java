/**
 * 2011-8-11
 */
package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ParseBriWiiGrpContUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public ParseBriWiiGrpContUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            ParseBriWiiGrpContBL tLogicBL = new ParseBriWiiGrpContBL();
            if (!tLogicBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tLogicBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
