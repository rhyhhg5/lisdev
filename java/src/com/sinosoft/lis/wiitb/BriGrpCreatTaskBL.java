package com.sinosoft.lis.wiitb;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBriGrpImportBatchInfoSchema;
import com.sinosoft.lis.schema.LCBriGrpImportDetailSchema;
import com.sinosoft.lis.vschema.LCBriGrpImportDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BriGrpCreatTaskBL {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
	
    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;
    
    private IXmlMsgColls mIXmlMsgColls = null;
    
    private String mBatchNo = null;
    
    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
	
	public boolean submitData(VData cInputData, String cOperate)
    {
    	if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }
    	
    	if (!submit())
        {
            return false;
        }

        return true;
    }
	private MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }
	private boolean getInputData(VData cInputData, String cOperate)
    {

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }
        
        mIXmlMsgColls = (IXmlMsgColls)cInputData.getObjectByObjectName("XmlMsgColls", 0); 
        if(mIXmlMsgColls == null){
        	buildError("getInputData", "获取报文数据失败。");
            return false;
        }
        
        return true;
    }
    private boolean checkData()
    {
        return true;
    }
    private boolean dealData()
    {
    	IXmlMsgColls[] iXmlMsgHead = mIXmlMsgColls.getBodyByFlag("MsgHead");
    	if(iXmlMsgHead == null){
        	buildError("dealData", "获取报文头结点数据失败。");
            return false;
        }
    	if(iXmlMsgHead.length != 1){
        	buildError("dealData", "报文头结点只能包含一个，请核查。");
            return false;
        }
    	mBatchNo =  iXmlMsgHead[0].getTextByFlag("BatchNo");
    	if(!isValidBatchNo(mBatchNo)){
    		String tStrErr = "已存在该批次数据，请核查！";
    		buildError("dealData", tStrErr);
            return false;
    	}
//    	获取保单数据
    	IXmlMsgColls[] iXmlPolicyInfos = mIXmlMsgColls.getBodyByFlag("PolicyInfo");
    	if(iXmlPolicyInfos == null || iXmlPolicyInfos.length == 0){
        	buildError("dealData", "获取报文保单数据失败。");
            return false;
        }
//    	生成主表信息
    	creatImportBatchInfo(iXmlMsgHead[0],iXmlPolicyInfos.length);
//    	生成明细信息
    	if(!creatImportDetail(iXmlPolicyInfos)){
    		return false;
    	}
    	
        return true;
    }
    
    private void creatImportBatchInfo(IXmlMsgColls aXmlMsgHead,int aTaskCount){
    	
    	LCBriGrpImportBatchInfoSchema tLCBriGrpImportBatchInfoSchema = new LCBriGrpImportBatchInfoSchema();
    	
    	tLCBriGrpImportBatchInfoSchema.setBatchNo(aXmlMsgHead.getTextByFlag("BatchNo"));
    	tLCBriGrpImportBatchInfoSchema.setSendDate(aXmlMsgHead.getTextByFlag("SendDate"));
    	tLCBriGrpImportBatchInfoSchema.setSendTime(aXmlMsgHead.getTextByFlag("SendTime"));
    	tLCBriGrpImportBatchInfoSchema.setBranchCode(aXmlMsgHead.getTextByFlag("BranchCode"));
    	tLCBriGrpImportBatchInfoSchema.setSendOperator(aXmlMsgHead.getTextByFlag("SendOperator"));
    	tLCBriGrpImportBatchInfoSchema.setMsgType(aXmlMsgHead.getTextByFlag("MsgType"));
    	tLCBriGrpImportBatchInfoSchema.setTaskCount(aTaskCount);
    	tLCBriGrpImportBatchInfoSchema.setImportState("00");
    	tLCBriGrpImportBatchInfoSchema.setImportStartDate(PubFun.getCurrentDate());
    	tLCBriGrpImportBatchInfoSchema.setImportEndDate("");
    	tLCBriGrpImportBatchInfoSchema.setOperator(mGlobalInput.Operator);
    	tLCBriGrpImportBatchInfoSchema.setMakeDate(PubFun.getCurrentDate());
    	tLCBriGrpImportBatchInfoSchema.setMakeTime(PubFun.getCurrentTime());
    	tLCBriGrpImportBatchInfoSchema.setModifyDate(PubFun.getCurrentDate());
    	tLCBriGrpImportBatchInfoSchema.setModifyTime(PubFun.getCurrentTime());
    	tLCBriGrpImportBatchInfoSchema.setBatchConfState("00");
    	tLCBriGrpImportBatchInfoSchema.setBatchConfDate("");
    	
    	mMap.put(tLCBriGrpImportBatchInfoSchema, SysConst.INSERT);
    }
    private boolean creatImportDetail(IXmlMsgColls[] aXmlPolicyInfos){
    	
    	LCBriGrpImportDetailSet tLCBriGrpImportDetailSet = new LCBriGrpImportDetailSet();
    	String tPrtNo = "";
    	for(int i=0;i<aXmlPolicyInfos.length;i++){
    		
    		IXmlMsgColls aXmlPolicyInfo = aXmlPolicyInfos[i];
    		LCBriGrpImportDetailSchema tLCBriGrpImportDetailSchema = new LCBriGrpImportDetailSchema();
        	String tBussNo = aXmlPolicyInfo.getTextByFlag("PrtNo");
        	tLCBriGrpImportDetailSchema.setBatchNo(mBatchNo);//批次号
        	if(tPrtNo.equals(tBussNo)){
        		buildError("creatImportDetail", "该批次多个保单中存在相同的印刷号码！");
                return false;
        	}else{
        		tPrtNo = tBussNo;
        	}
        	tLCBriGrpImportDetailSchema.setBussNo(tBussNo);//业务号码 印刷号
        	tLCBriGrpImportDetailSchema.setImportState("00");//导入状态 00-待处理   	01-处理中  02-处理完毕  03-异常终止
        	tLCBriGrpImportDetailSchema.setImportDate(PubFun.getCurrentDate());//导入日期
        	tLCBriGrpImportDetailSchema.setConfState("00");//任务确认状态 00-未确认  01-确认中 02-已确认 03-异常终止
        	tLCBriGrpImportDetailSchema.setConfDate("");//任务确认时间
        	tLCBriGrpImportDetailSchema.setDealFlag("00");//处理标志 00 - 未承保    	01 - 已承保
        	tLCBriGrpImportDetailSchema.setDealRemark("");//处理说明
        	tLCBriGrpImportDetailSchema.setOperator(mGlobalInput.Operator);//操作员
        	tLCBriGrpImportDetailSchema.setMakeDate(PubFun.getCurrentDate());//入机日期
        	tLCBriGrpImportDetailSchema.setMakeTime(PubFun.getCurrentTime());//入机时间
        	tLCBriGrpImportDetailSchema.setModifyDate(PubFun.getCurrentDate());//最后一次修改日期
        	tLCBriGrpImportDetailSchema.setModifyTime(PubFun.getCurrentTime());//最后一次修改时间
        	tLCBriGrpImportDetailSchema.setEnableFlag("00");//是否有效标志 00-待处理  01-有效 02-无效
        	tLCBriGrpImportDetailSchema.setGrpContNo("");//保单合同号
        	tLCBriGrpImportDetailSchema.setGrpName("");//投保单位名称
        	tLCBriGrpImportDetailSchema.setOrgancomCode("");//组织机构代码
        	tLCBriGrpImportDetailSchema.setOtherCertificates("");//保险证编码/社保登记编号
        	tLCBriGrpImportDetailSchema.setCValiDate("");//保单生效日期
        	tLCBriGrpImportDetailSchema.setCInValidate("");//保单满期日期
        	tLCBriGrpImportDetailSchema.setPolApplyDate("");//投保单申请日期
        	tLCBriGrpImportDetailSchema.setSignDate("");//保单签发日期
        	tLCBriGrpImportDetailSchema.setConfMakeDate("");//财务到帐日期
        	tLCBriGrpImportDetailSchema.setPrem("");//保单保费
        	
        	tLCBriGrpImportDetailSet.add(tLCBriGrpImportDetailSchema);
    	}
    	mMap.put(tLCBriGrpImportDetailSet, SysConst.INSERT);
    	return true;
    }
//	校验批次是否已经存在
	private boolean isValidBatchNo(String aBatchNo){
		String sql = "select count(1) from LCBriGrpImportBatchInfo where BatchNo='"+ aBatchNo + "'";
		ExeSQL tExeSQL = new ExeSQL();
		int count = Integer.parseInt(tExeSQL.getOneValue(sql));
		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriGrpTempFeeFromBank";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public MMap getMMap(){
    	return mMap;
    }
    
    public CErrors getCErrors(){
    	return mErrors;
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
}
