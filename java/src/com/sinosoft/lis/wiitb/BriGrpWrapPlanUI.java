/**
 * 2011-3-21
 */
package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BriGrpWrapPlanUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public BriGrpWrapPlanUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            BriGrpWrapPlanBL tLogicBL = new BriGrpWrapPlanBL();
            if (!tLogicBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tLogicBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
