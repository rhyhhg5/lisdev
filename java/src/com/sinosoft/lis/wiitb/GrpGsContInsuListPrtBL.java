package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.f1print.PDFPrintBatchManagerBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 团体工伤险被保险人清单打印
 * 
 * @author LY
 *
 */
public class GrpGsContInsuListPrtBL 
{
	private String mOperate;

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
    
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private TransferData mTransferData = new TransferData();
    
    private String mCode = null;
    
    public GrpGsContInsuListPrtBL(){}
    
    /**
     * 传输数据的公共方法
     * 
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        cInputData = (VData) cInputData.clone();

        this.mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean getInputData(VData cInputData)
    {
        mLOPRTManagerSchema = (LOPRTManagerSchema)cInputData.getObjectByObjectName(
                "LOPRTManagerSchema", 0);
        if (mLOPRTManagerSchema == null)
        {
            buildError("getInputData", "未找到打印清单。");
            return false;
        }

        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }
        
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }
        
        mCode = (String) mTransferData.getValueByName("Code");
        if (mCode == null || mCode.equals(""))
        {
            buildError("getInputData", "打印类型出错。");
            return false;
        }
        
        return true;
    }

    /**
     * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (!prepareOutputData())
        {
            return false;
        }

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(mLOPRTManagerSet);
        PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
        if (!tPDFPrintBatchManagerBL.submitData(tVData, mOperate))
        {
            mErrors.copyAllErrors(tPDFPrintBatchManagerBL.mErrors);
            return false;
        }
        
        return true;
    }

    private boolean prepareOutputData()
    {
        String tOtherNoType = "16";
        mLOPRTManagerSchema.setOtherNoType(tOtherNoType);
        mLOPRTManagerSchema.setCode(mCode);
        mLOPRTManagerSet.add(mLOPRTManagerSchema);
        
        return true;
    }

    public void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpSubContBatchPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
