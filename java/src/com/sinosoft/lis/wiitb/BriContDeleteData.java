package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title:团单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class BriContDeleteData {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCContSchema mLCContSchema = new LCContSchema();
//    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    public BriContDeleteData() {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }
    	
        return true;
    }
    private MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }
    
    private boolean getInputData(VData cInputData, String cOperate)
    {
    	 //全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null) {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        //团体保单实例
        mLCContSchema.setSchema((LCContSchema) mInputData.
                                   getObjectByObjectName("LCContSchema", 0));

        if (mLCContSchema == null) {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));

            return false;
        }

        System.out.println("完成数据解析 结果为 " + mLCContSchema.getPrtNo() + "  " +
                           mLCContSchema.getContNo());
        return true;
    }
    private boolean checkData()
    {
        return true;
    }
    private boolean dealData()
    {
//    	获取删除保单数据
    	MMap delContMMap = dealContData();
    	if(delContMMap == null){
    		buildError("dealData","获取待删除保单数据失败！");
    	}
    	mMap.add(delContMMap);
    	
        return true;
    }

    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * @return boolean
     */
    private MMap dealContData() {
    	
    	MMap ContMMap = new MMap();
        
        String tContNo = mLCContSchema.getContNo();
        String tPrtNo = mLCContSchema.getPrtNo();
        if(tContNo == null || "".equals(tContNo)){
        	this.mErrors.addOneError(new CError("新单删除时，团单号码为空！"));
            return null;
        }
        if(tPrtNo == null || "".equals(tPrtNo)){
        	this.mErrors.addOneError(new CError("新单删除时，印刷号为空！"));
            return null;
        }
        String tSql = "";
        Reflections tReflections = new Reflections();
        
        LCPolDB tLCPolDB = new LCPolDB();
//        tLCPolDB.setContNo(tContNo);
        tSql = "select * from lcpol where contno = '"+tContNo+"' ";
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(tSql);
        if(tLCPolSet != null && tLCPolSet.size()>0){
        	
        	LCDutySet tLCDutySet = new LCDutySet();
        	LCInsuredRelatedSet tLCInsuredRelatedSet = new LCInsuredRelatedSet();
        	LCPremToAccSet tLCPremToAccSet = new LCPremToAccSet();
        	LCGetToAccSet tLCGetToAccSet = new LCGetToAccSet();
        	
        	for(int i=1;i<=tLCPolSet.size();i++){
        		
        		LCPolSchema tLCPolSchema = tLCPolSet.get(i);
        		String tPolNo = tLCPolSchema.getPolNo();
        		
        		LCInsuredRelatedDB tLCInsuredRelatedDB = new LCInsuredRelatedDB();
//        		tLCInsuredRelatedDB.setPolNo(tPolNo);
        		tSql = "select * from LCInsuredRelated where polno = '"+tPolNo+"' ";
        		LCInsuredRelatedSet tempLCInsuredRelatedSet = tLCInsuredRelatedDB.executeQuery(tSql);
        		tLCInsuredRelatedSet.add(tempLCInsuredRelatedSet);
        		
        		LCDutyDB tLCDutyDB = new LCDutyDB();
//        		tLCDutyDB.setPolNo(tPolNo);
        		tSql = "select * from LCDuty where polno = '"+tPolNo+"' ";
        		LCDutySet tempLCDutySet = tLCDutyDB.executeQuery(tSql);
        		tLCDutySet.add(tempLCDutySet);
        		
        		LCPremToAccDB tLCPremToAccDB = new LCPremToAccDB();
//        		tLCPremToAccDB.setPolNo(tPolNo);
        		tSql = "select * from LCPremToAcc where polno = '"+tPolNo+"' ";
        		LCPremToAccSet tempLCPremToAccSet = tLCPremToAccDB.executeQuery(tSql);
        		tLCPremToAccSet.add(tempLCPremToAccSet);
        		
        		LCGetToAccDB tLCGetToAccDB = new LCGetToAccDB();
//        		tLCGetToAccDB.setPolNo(tPolNo);
        		tSql = "select * from LCPremToAcc where polno = '"+tPolNo+"' ";
        		LCGetToAccSet tempLCGetToAccSet = tLCGetToAccDB.executeQuery(tSql);
        		tLCGetToAccSet.add(tempLCGetToAccSet);
        		
        	}
        	
        	LOBInsuredRelatedSet tLOBInsuredRelatedSet = new LOBInsuredRelatedSet();
        	LOBInsuredRelatedSchema tLOBInsuredRelatedSchema = new LOBInsuredRelatedSchema();
        	tLOBInsuredRelatedSet.add(tLOBInsuredRelatedSchema);
        	tReflections.transFields(tLOBInsuredRelatedSet,tLCInsuredRelatedSet);
        	ContMMap.put(tLOBInsuredRelatedSet, SysConst.DELETE_AND_INSERT);
            ContMMap.put(tLCInsuredRelatedSet, SysConst.DELETE);
            
            LOBDutySet tLOBDutySet = new LOBDutySet();
        	LOBDutySchema tLOBDutySchema = new LOBDutySchema();
        	tLOBDutySet.add(tLOBDutySchema);
        	tReflections.transFields(tLOBDutySet,tLCDutySet);
        	ContMMap.put(tLOBDutySet, SysConst.DELETE_AND_INSERT);
            ContMMap.put(tLCDutySet, SysConst.DELETE);
            
            LOBPremToAccSet tLOBPremToAccSet = new LOBPremToAccSet();
        	LOBPremToAccSchema tLOBPremToAccSchema = new LOBPremToAccSchema();
        	tLOBPremToAccSet.add(tLOBPremToAccSchema);
        	tReflections.transFields(tLOBPremToAccSet,tLCPremToAccSet);
        	ContMMap.put(tLOBPremToAccSet, SysConst.DELETE_AND_INSERT);
            ContMMap.put(tLCPremToAccSet, SysConst.DELETE);
            
            LOBGetToAccSet tLOBGetToAccSet = new LOBGetToAccSet();
        	LOBGetToAccSchema tLOBGetToAccSchema = new LOBGetToAccSchema();
        	tLOBGetToAccSet.add(tLOBGetToAccSchema);
        	tReflections.transFields(tLOBGetToAccSet,tLCGetToAccSet);
        	ContMMap.put(tLOBGetToAccSet, SysConst.DELETE_AND_INSERT);
            ContMMap.put(tLCGetToAccSet, SysConst.DELETE);
        }
        
        
        LCGetDB tLCGetDB = new LCGetDB();
//        tLCGetDB.setContNo(tContNo);
        tSql = "select * from LCGet from contno = '"+tContNo+"' ";
        LCGetSet tLCGetSet = tLCGetDB.executeQuery(tSql);        
        LOBGetSet tLOBGetSet = new LOBGetSet();
        LOBGetSchema tLOBGetSchema = new LOBGetSchema();
        tLOBGetSet.add(tLOBGetSchema);
        tReflections.transFields(tLOBGetSet,tLCGetSet);        
        ContMMap.put(tLOBGetSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCGetSet, SysConst.DELETE);
        
        LCPremDB tLCPremDB = new LCPremDB();
//        tLCPremDB.setContNo(tContNo);
        tSql = "select * from LCPrem where contno = '"+tContNo+"' ";
        LCPremSet tLCPremSet = tLCPremDB.executeQuery(tSql);        
        LOBPremSet tLOBPremSet = new LOBPremSet();
        LOBPremSchema tLOBPremSchema = new LOBPremSchema();
        tLOBPremSet.add(tLOBPremSchema);
        tReflections.transFields(tLOBPremSet,tLCPremSet);        
        ContMMap.put(tLOBPremSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCPremSet, SysConst.DELETE);
        
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
//        tLCInsuredDB.setContNo(tContNo);
        tSql = "select * from LCInsured where contno = '"+tContNo+"' ";
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.executeQuery(tSql);        
        LOBInsuredSet tLOBInsuredSet = new LOBInsuredSet();
        LOBInsuredSchema tLOBInsuredSchema = new LOBInsuredSchema();
        tLOBInsuredSet.add(tLOBInsuredSchema);
        tReflections.transFields(tLOBInsuredSet,tLCInsuredSet);        
        ContMMap.put(tLOBInsuredSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCInsuredSet, SysConst.DELETE);
        
        LOBPolSet tLOBPolSet = new LOBPolSet();
        LOBPolSchema tLOBPolSchema = new LOBPolSchema();
        tLOBPolSet.add(tLOBPolSchema);
        tReflections.transFields(tLOBPolSet,tLCPolSet);        
        ContMMap.put(tLOBPolSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCPolSet, SysConst.DELETE);
        
        LCContDB tLCContDB = new LCContDB();
//        tLCContDB.setContNo(tContNo);
        tSql = "select * from LCCont where contno = '"+tContNo+"' ";
        LCContSet tLCContSet = tLCContDB.executeQuery(tSql);        
        LOBContSet tLOBContSet = new LOBContSet();
        LOBContSchema tLOBContSchema = new LOBContSchema();
        tLOBContSet.add(tLOBContSchema);
        tReflections.transFields(tLOBContSet,tLCContSet);        
        ContMMap.put(tLOBContSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCContSet, SysConst.DELETE);
        
        LCAppntDB tLCAppntDB = new LCAppntDB();
//        tLCAppntDB.setContNo(tContNo);
        tSql = "select * from LCAppnt where contno = '"+tContNo+"' ";
        LCAppntSet tLCAppntSet = tLCAppntDB.executeQuery(tSql);        
        LOBAppntSet tLOBAppntSet = new LOBAppntSet();
        LOBAppntSchema tLOBAppntSchema = new LOBAppntSchema();
        tLOBAppntSet.add(tLOBAppntSchema);
        tReflections.transFields(tLOBAppntSet,tLCAppntSet);        
        ContMMap.put(tLOBAppntSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCAppntSet, SysConst.DELETE);
        
        LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
//        tLCInsureAccFeeDB.setContNo(tContNo);
        tSql = "select * from LCInsureAccFee where contno = '"+tContNo+"' ";
        LCInsureAccFeeSet tLCInsureAccFeeSet = tLCInsureAccFeeDB.executeQuery(tSql);        
        LOBInsureAccFeeSet tLOBInsureAccFeeSet = new LOBInsureAccFeeSet();
        LOBInsureAccFeeSchema tLOBInsureAccFeeSchema = new LOBInsureAccFeeSchema();
        tLOBInsureAccFeeSet.add(tLOBInsureAccFeeSchema);
        tReflections.transFields(tLOBInsureAccFeeSet,tLCInsureAccFeeSet);        
        ContMMap.put(tLOBInsureAccFeeSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCInsureAccFeeSet, SysConst.DELETE);
        
        LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
//        tLCInsureAccClassFeeDB.setContNo(tContNo);
        tSql = "select * from LCInsureAccClassFee where contno = '"+tContNo+"' ";
        LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = tLCInsureAccClassFeeDB.executeQuery(tSql);        
        LOBInsureAccClassFeeSet tLOBInsureAccClassFeeSet = new LOBInsureAccClassFeeSet();
        LOBInsureAccClassFeeSchema tLOBInsureAccClassFeeSchema = new LOBInsureAccClassFeeSchema();
        tLOBInsureAccClassFeeSet.add(tLOBInsureAccClassFeeSchema);
        tReflections.transFields(tLOBInsureAccClassFeeSet,tLCInsureAccClassFeeSet);        
        ContMMap.put(tLOBInsureAccClassFeeSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCInsureAccClassFeeSet, SysConst.DELETE);
        
        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
//        tLCInsureAccClassDB.setContNo(tContNo);
        tSql = "select * from LCInsureAccClass where contno = '"+tContNo+"' ";
        LCInsureAccClassSet tLCInsureAccClassSet = tLCInsureAccClassDB.executeQuery(tSql);        
        LOBInsureAccClassSet tLOBInsureAccClassSet = new LOBInsureAccClassSet();
        LOBInsureAccClassSchema tLOBInsureAccClassSchema = new LOBInsureAccClassSchema();
        tLOBInsureAccClassSet.add(tLOBInsureAccClassSchema);
        tReflections.transFields(tLOBInsureAccClassSet,tLCInsureAccClassSet);        
        ContMMap.put(tLOBInsureAccClassSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCInsureAccClassSet, SysConst.DELETE);
        
        LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
//        tLCInsureAccDB.setContNo(tContNo);
        tSql = "select * from LCInsureAcc where contno = '"+tContNo+"' ";
        LCInsureAccSet tLCInsureAccSet = tLCInsureAccDB.executeQuery(tSql);        
        LOBInsureAccSet tLOBInsureAccSet = new LOBInsureAccSet();
        LOBInsureAccSchema tLOBInsureAccSchema = new LOBInsureAccSchema();
        tLOBInsureAccSet.add(tLOBInsureAccSchema);
        tReflections.transFields(tLOBInsureAccSet,tLCInsureAccSet);        
        ContMMap.put(tLOBInsureAccSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCInsureAccSet, SysConst.DELETE);
        
        LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
//        tLCInsureAccTraceDB.setContNo(tContNo);
        tSql = "select * from LCInsureAccTrace where contno = '"+tContNo+"' ";
        LCInsureAccTraceSet tLCInsureAccTraceSet = tLCInsureAccTraceDB.executeQuery(tSql);        
        LOBInsureAccTraceSet tLOBInsureAccTraceSet = new LOBInsureAccTraceSet();
        LOBInsureAccTraceSchema tLOBInsureAccTraceSchema = new LOBInsureAccTraceSchema();
        tLOBInsureAccTraceSet.add(tLOBInsureAccTraceSchema);
        tReflections.transFields(tLOBInsureAccTraceSet,tLCInsureAccTraceSet);        
        ContMMap.put(tLOBInsureAccTraceSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCInsureAccTraceSet, SysConst.DELETE);
        
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
//        tLCInsuredListDB.setContNo(tContNo);
        tSql = "select * from LCInsuredList where contno = '"+tContNo+"' ";
        LCInsuredListSet tLCInsuredListSet = tLCInsuredListDB.executeQuery(tSql);        
        LBInsuredListSet tLBInsuredListSet = new LBInsuredListSet();
        LBInsuredListSchema tLBInsuredListSchema = new LBInsuredListSchema();
        tLBInsuredListSet.add(tLBInsuredListSchema);
        tReflections.transFields(tLBInsuredListSet,tLCInsuredListSet);
        String SerialNo = PubFun1.CreateMaxNo("SerialNo", 20);
        for(int i=1;i<=tLBInsuredListSet.size();i++){
        	tLBInsuredListSet.get(i).setSerialNo(SerialNo);
        }
        ContMMap.put(tLBInsuredListSet, SysConst.DELETE_AND_INSERT);
        ContMMap.put(tLCInsuredListSet, SysConst.DELETE);

        return ContMMap;
    }
    
    
    /**
     * 操作结果
     * @return VData
     */
    public MMap getMMap() {
        return mMap;
    }
    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GroupContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    
    public static void main(String[] args){
    	BriContDeleteData tBriGrpContDeleteBL = new BriContDeleteData();
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.ManageCom = "86110000";
    	tGlobalInput.Operator = "ly";
    	
    	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    	tLCGrpContDB.setPrtNo("99000400001");
    	LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
    	
    	VData tVData = new VData();
    	tVData.add(tGlobalInput);
    	tVData.add(tLCGrpContSet.getObj(1));
    	tBriGrpContDeleteBL.submitData(tVData, "test");
    }
}
