/**
 * 2012-3-23
 */
package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class GrpGSNoPrtPrintBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mGrpContNo = null;

    public GrpGSNoPrtPrintBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        if (mGrpContNo == null || mGrpContNo.equals(""))
        {
            buildError("getInputData", "获取保单合同号失败。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tMMap = new MMap();

        String tStrSql = "select * from LCGrpCont where GrpContNo = '" + mGrpContNo + "'";

        LCGrpContSchema tGrpContInfo = null;

        LCGrpContSet tGrpContInfoSet = new LCGrpContDB().executeQuery(tStrSql);
        if (tGrpContInfoSet == null || tGrpContInfoSet.size() != 1)
        {
            buildError("addGetPolDate", "获取签单后保单信息失败。");
            return false;
        }
        tGrpContInfo = tGrpContInfoSet.get(1);

        String tGrpContNo = tGrpContInfo.getGrpContNo();

        String tCutDate = PubFun.getCurrentDate();
        String tCutTime = PubFun.getCurrentTime();

        int iDelayDays = 0;
        String tCustomGetPolDate = PubFun.calDate(tCutDate, iDelayDays, "D", null);

        StringBuffer tStrBSql = null;

        // 处理团单数据
        tStrBSql = new StringBuffer();
        tStrBSql.append(" update LCGrpCont set ");
        tStrBSql.append(" PrintCount = 1, ");
        tStrBSql.append(" CustomGetPolDate = '" + tCustomGetPolDate + "', ");
        tStrBSql.append(" GetPolDate = '" + tCutDate + "', ");
        tStrBSql.append(" ModifyDate = '" + tCutDate + "', ");
        tStrBSql.append(" ModifyTime = '" + tCutTime + "', ");
        tStrBSql.append(" Operator = '" + this.mGlobalInput.Operator + "' ");
        tStrBSql.append(" where GrpContNo = '" + tGrpContNo + "' ");
        tMMap.put(tStrBSql.toString(), SysConst.UPDATE);
        tStrBSql = null;
        // --------------------

        // 处理分单数据
        tStrBSql = new StringBuffer();
        tStrBSql.append(" update LCCont set ");
        tStrBSql.append(" CustomGetPolDate = '" + tCustomGetPolDate + "', ");
        tStrBSql.append(" GetPolDate = '" + tCutDate + "', ");
        tStrBSql.append(" ModifyDate = '" + tCutDate + "', ");
        tStrBSql.append(" ModifyTime = '" + tCutTime + "', ");
        tStrBSql.append(" Operator = '" + this.mGlobalInput.Operator + "' ");
        tStrBSql.append(" where GrpContNo = '" + tGrpContNo + "' ");
        tMMap.put(tStrBSql.toString(), SysConst.UPDATE);
        tStrBSql = null;
        // --------------------

        mMap.add(tMMap);

        return true;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GrpGSNoPrtPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
}
