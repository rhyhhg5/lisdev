/**
 * 2011-3-10
 */
package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCExtendSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpContSubSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.tb.ExtendBL;
import com.sinosoft.lis.vschema.LCGrpAppntSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LDGrpSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BriGrpContBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private LCGrpContSchema mGrpContInfo = null;

    private LCGrpAppntSchema mGrpAppntInfo = null;

    private LDGrpSchema mGrpInfo = null;

    private LCGrpAddressSchema mGrpAddressInfo = null;
    
    private LCGrpContSubSchema mGrpContSub = null;

    private String mCurDate = PubFun.getCurrentDate();

    private String mCurTime = PubFun.getCurrentTime();

    //by gzh 20130408 对综合开拓数据处理
    private LCExtendSchema mLCExtendSchema = new LCExtendSchema();
    
    public BriGrpContBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mGrpContInfo = (LCGrpContSchema) cInputData.getObjectByObjectName("LCGrpContSchema", 0);
        if (mGrpContInfo == null)
        {
            buildError("getInputData", "未获取到保单基本信息。");
            return false;
        }

        mGrpAppntInfo = (LCGrpAppntSchema) cInputData.getObjectByObjectName("LCGrpAppntSchema", 0);
        if (mGrpAppntInfo == null)
        {
            buildError("getInputData", "未获取到投保单位信息。");
            return false;
        }

        mGrpAddressInfo = (LCGrpAddressSchema) cInputData.getObjectByObjectName("LCGrpAddressSchema", 0);
        if (mGrpAddressInfo == null)
        {
            buildError("getInputData", "未获取到投保单位联系信息。");
            return false;
        }
        
        mGrpContSub = (LCGrpContSubSchema) cInputData.getObjectByObjectName("LCGrpContSubSchema", 0);
        
        mLCExtendSchema = (LCExtendSchema) cInputData.getObjectByObjectName("LCExtendSchema", 0);
        if(mLCExtendSchema == null ){
        	mLCExtendSchema = new LCExtendSchema();
        }

        return true;
    }

    private boolean checkData()
    {
        // 检查保单信息完整性
        String tPrtNo = mGrpContInfo.getPrtNo();
        if (tPrtNo == null || tPrtNo.equals(""))
        {
            buildError("checkData", "印刷号不能为空。");
            return false;
        }

        String tStrSql = " select 1 from LCGrpCont lgc where lgc.PrtNo = '" + tPrtNo + "' "
                + " and lgc.SignDate is not null and (lgc.CardFlag is null or lgc.CardFlag != '4') " + " union all "
                + " select 1 from LBGrpCont lgc where lgc.PrtNo = '" + tPrtNo + "' "
                + " and lgc.SignDate is not null and (lgc.CardFlag is null or lgc.CardFlag != '4') ";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            buildError("checkData", "该印刷号保单已存在。");
            return false;
        }

        String tManageCom = mGrpContInfo.getManageCom();
        if (tManageCom == null || tManageCom.equals(""))
        {
            buildError("checkData", "管理机构不能为空。");
            return false;
        }

        String tSaleChnl = mGrpContInfo.getSaleChnl();
        if (tSaleChnl == null || tSaleChnl.equals(""))
        {
            buildError("checkData", "销售渠道不能为空。");
            return false;
        }
        
        if(tSaleChnl != null && !"".equals(tSaleChnl)){
        	//验证销售渠道是否正确
            String  sqlSaleChnl = " select 1 from ldcode where codetype in ('salechnl','lcsalechnl') and code = '"+tSaleChnl+"'";
            String resultSaleChnl = new ExeSQL().getOneValue(sqlSaleChnl);
        	if (resultSaleChnl == null|| resultSaleChnl.equals("")){
            	  String tStrErr = "导入的合同销售渠道为" + tSaleChnl + "：销售渠道不存在 请核实。";
                  buildError("checkData", tStrErr);
                  return false;
            }
        }

        String tAgentCode = mGrpContInfo.getAgentCode();
        if (tAgentCode == null || tAgentCode.equals(""))
        {
            buildError("checkData", "业务员代码不能为空。");
            return false;
        }

        String tPolApplyDate = mGrpContInfo.getPolApplyDate();
        if (tPolApplyDate == null || tPolApplyDate.equals(""))
        {
            buildError("checkData", "保单申请日期不能为空。");
            return false;
        }

        String tHandlerName = mGrpContInfo.getHandlerName();
        if (tHandlerName == null || tHandlerName.equals(""))
        {
            buildError("checkData", "业务经办人不能为空。");
            return false;
        }

        String tFirstTrialOperator = mGrpContInfo.getFirstTrialOperator();
        if (tFirstTrialOperator == null || tFirstTrialOperator.equals(""))
        {
            buildError("checkData", "初审人员不能为空。");
            return false;
        }

        int tPeoples3 = mGrpContInfo.getPeoples3();
        if (tPeoples3 <= 0)
        {
            buildError("checkData", "被保险人数少于1人。");
            return false;
        }

        double tPremScope = mGrpContInfo.getPremScope();
        if (tPremScope < 0)
        {
            buildError("checkData", "保费合计小于0。");
            return false;
        }

        String tCValiDate = mGrpContInfo.getCValiDate();
        if (tCValiDate == null || tCValiDate.equals(""))
        {
            buildError("checkData", "保险责任生效日期不能为空。");
            return false;
        }

        String tCInValiDate = mGrpContInfo.getCInValiDate();
        if (tCInValiDate == null || tCInValiDate.equals(""))
        {
            buildError("checkData", "保险责任终止日期不能为空。");
            return false;
        }
        // --------------------

        // 投保单位信息
        String tGrpName = mGrpAppntInfo.getName();
        if (tGrpName == null || tGrpName.equals(""))
        {
            buildError("checkData", "投保单位不能为空。");
            return false;
        }

        // 暂不要求必填
        //        String tOrgancomCode = mGrpAppntInfo.getOrganComCode();
        //        if (tOrgancomCode == null || tOrgancomCode.equals(""))
        //        {
        //            buildError("checkData", "投保单位组织机构代码不能为空。");
        //            return false;
        //        }

        // 暂不要求必填
        //        String tOtherCertificates = mGrpAppntInfo.getOtherCertificates();
        //        if (tOtherCertificates == null || tOtherCertificates.equals(""))
        //        {
        //            buildError("checkData", "投保单位其他证件不能为空。");
        //            return false;
        //        }

        String tGrpAddress = mGrpAddressInfo.getGrpAddress();
        if (tGrpAddress == null || tGrpAddress.equals(""))
        {
            buildError("checkData", "投保单位地址不能为空。");
            return false;
        }

        String tGrpZipCode = mGrpAddressInfo.getGrpZipCode();
        if (tGrpZipCode == null || tGrpZipCode.equals(""))
        {
            buildError("checkData", "投保单位邮政编码不能为空。");
            return false;
        }

        String tLinkMan1 = mGrpAddressInfo.getLinkMan1();
        if (tLinkMan1 == null || tLinkMan1.equals(""))
        {
            buildError("checkData", "投保单位联系人不能为空。");
            return false;
        }

        String tPhone1 = mGrpAddressInfo.getPhone1();
        if (tPhone1 == null || tPhone1.equals(""))
        {
            buildError("checkData", "投保单位联系人电话不能为空。");
            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Create".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = createGrpCont();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
//          by gzh 20130408 对综合开拓数据处理
            if (!"".equals(StrTool.cTrim(mLCExtendSchema.getAssistSalechnl())))
            {
                tTmpMap = null;
                tTmpMap = delExtendInfo(mLCExtendSchema);
                if (tTmpMap == null)
                {
                    return false;
                }
                mMap.add(tTmpMap);
            }else{
            	String tSql = "delete from LCExtend where prtno = '"+mLCExtendSchema.getPrtNo()+"'";
            	mMap.put(tSql, SysConst.DELETE);
            }
        }

        return true;
    }

    private MMap createGrpCont()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 获取数据库保单信息
        String tPrtNo = mGrpContInfo.getPrtNo();
        LCGrpContSchema tGrpContInfo = loadGrpContInfo(tPrtNo);
        if (tGrpContInfo == null)
        {
            tGrpContInfo = new LCGrpContSchema();
        }
        else
        {
            // 清除相关保单信息数据
            String tTmpContNo = tGrpContInfo.getGrpContNo();
            tTmpMap = cleanGrpCont(tTmpContNo);
            if (tTmpMap == null)
            {
                return null;
            }
            tMMap.add(tTmpMap);
            tTmpMap = null;
        }
        // --------------------

        // 校验是否信息可以变更
        if (!chkChgKeyDatas(tGrpContInfo))
        {
            return null;
        }
        // --------------------

        // 保单信息
        if (!dealGrpContInfo(tGrpContInfo))
        {
            return null;
        }
        // --------------------

        // 处理销售信息
        if (!dealSaleInfo())
        {
            return null;
        }
        // --------------------

        // 投保单位信息
        if (!dealGrpAppntInfo())
        {
            return null;
        }
        // --------------------

        // 同步数据库事务
        tMMap.put(mGrpInfo, SysConst.DELETE_AND_INSERT);
        tMMap.put(mGrpContInfo, SysConst.DELETE_AND_INSERT);
        tMMap.put(mGrpAppntInfo, SysConst.DELETE_AND_INSERT);
        tMMap.put(mGrpAddressInfo, SysConst.DELETE_AND_INSERT);
        tMMap.put(mGrpContSub, SysConst.DELETE_AND_INSERT);
        // --------------------

        // 同步保单相关表数据
        tTmpMap = syncOtherPolicyInfo();
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 防并发控制
        tTmpMap = lockCont(mGrpContInfo);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    /**
     * 校验生效日期调整，如保单已存在被保人信息，不能进行修改
     * @param cGrpContInfo
     * @return
     */
    private boolean chkChgKeyDatas(LCGrpContSchema cGrpContInfo)
    {
        String tCValidate = cGrpContInfo.getCValiDate();
        String tCInValidate = cGrpContInfo.getCInValiDate();
        int tPayIntv = cGrpContInfo.getPayIntv();

        if ((tCValidate != null && !tCValidate.equals(mGrpContInfo.getCValiDate()))
                || (tCInValidate != null && !tCInValidate.equals(mGrpContInfo.getCInValiDate()))
                || (tPayIntv != mGrpContInfo.getPayIntv()))
        {
            String tPrtNo = mGrpContInfo.getPrtNo();
            String tStrSql = "select 1 from LCCont where PrtNo = '" + tPrtNo + "'";
            String tResult = new ExeSQL().getOneValue(tStrSql);
            if ("1".equals(tResult))
            {
                buildError("chkChgValidateDatas", "已存在被保人，不能调整生效日期、终止日期、缴费频次，如需调整，请先删除被保人后，重试修改操作。");
                return false;
            }
        }

        int tPeoples3 = cGrpContInfo.getPeoples3();
        if (tPeoples3 != mGrpContInfo.getPeoples3())
        {
            String tPrtNo = mGrpContInfo.getPrtNo();
            String tStrSql = "select 1 from LCCont where PrtNo = '" + tPrtNo + "' and PolType = '1' ";
            String tResult = new ExeSQL().getOneValue(tStrSql);
            if ("1".equals(tResult))
            {
                buildError("chkChgValidateDatas", "该保单为无名单，不能调整被保险人数，如需调整，请先删除被保人后，重试修改操作。");
                return false;
            }
        }

        return true;
    }

    private boolean dealGrpContInfo(LCGrpContSchema cGrpContInfo)
    {
        if (cGrpContInfo == null)
        {
            return false;
        }

        cGrpContInfo.setPrtNo(mGrpContInfo.getPrtNo());
        cGrpContInfo.setManageCom(mGrpContInfo.getManageCom());

        cGrpContInfo.setMarketType(mGrpContInfo.getMarketType());

        cGrpContInfo.setPayMode(mGrpContInfo.getPayMode());
        cGrpContInfo.setPayIntv(mGrpContInfo.getPayIntv());
        cGrpContInfo.setPremScope(mGrpContInfo.getPremScope());

        cGrpContInfo.setBankCode(mGrpContInfo.getBankCode());
        cGrpContInfo.setBankAccNo(mGrpContInfo.getBankAccNo());
        cGrpContInfo.setAccName(mGrpContInfo.getAccName());

        cGrpContInfo.setCValiDate(mGrpContInfo.getCValiDate());
        cGrpContInfo.setCInValiDate(mGrpContInfo.getCInValiDate());

        cGrpContInfo.setSaleChnl(mGrpContInfo.getSaleChnl());
        cGrpContInfo.setAgentCom(mGrpContInfo.getAgentCom());
        cGrpContInfo.setAgentCode(mGrpContInfo.getAgentCode());

        cGrpContInfo.setPeoples3(mGrpContInfo.getPeoples3());
        cGrpContInfo.setOnWorkPeoples(mGrpContInfo.getOnWorkPeoples());
        cGrpContInfo.setOffWorkPeoples(mGrpContInfo.getOffWorkPeoples());
        cGrpContInfo.setOtherPeoples(mGrpContInfo.getOtherPeoples());

        cGrpContInfo.setAppFlag("0");
        cGrpContInfo.setPolApplyDate(mGrpContInfo.getPolApplyDate());
        cGrpContInfo.setHandlerName(mGrpContInfo.getHandlerName());
        cGrpContInfo.setFirstTrialOperator(mGrpContInfo.getFirstTrialOperator());
        
        //添加集团交叉数据
        cGrpContInfo.setCrs_SaleChnl(mGrpContInfo.getCrs_SaleChnl());
        cGrpContInfo.setCrs_BussType(mGrpContInfo.getCrs_BussType());
        cGrpContInfo.setGrpAgentCode(mGrpContInfo.getGrpAgentCode());
        cGrpContInfo.setGrpAgentCom(mGrpContInfo.getGrpAgentCom());
        cGrpContInfo.setGrpAgentName(mGrpContInfo.getGrpAgentName());
        cGrpContInfo.setGrpAgentIDNo(mGrpContInfo.getGrpAgentIDNo());
        cGrpContInfo.setCoInsuranceFlag(mGrpContInfo.getCoInsuranceFlag());
        
        cGrpContInfo.setRemark(mGrpContInfo.getRemark());

        String tInputOperator = cGrpContInfo.getInputOperator();
        if (tInputOperator == null || tInputOperator.equals(""))
        {
            cGrpContInfo.setInputOperator(mGlobalInput.Operator);
        }

        String tInputDate = cGrpContInfo.getInputDate();
        if (tInputDate == null || tInputDate.equals(""))
        {
            cGrpContInfo.setInputDate(PubFun.getCurrentDate());
        }

        String tInputTime = cGrpContInfo.getInputTime();
        if (tInputTime == null || tInputTime.equals(""))
        {
            cGrpContInfo.setInputTime(PubFun.getCurrentTime());
        }

        String tApproveFlag = "0";
        cGrpContInfo.setApproveFlag(tApproveFlag);
        //        cGrpContInfo.setApproveCode(mGlobalInput.Operator);
        //        cGrpContInfo.setApproveDate(mCurDate);
        //        cGrpContInfo.setApproveTime(mCurTime);

        String tUWFlag = "0";
        cGrpContInfo.setUWFlag(tUWFlag);
        //        cGrpContInfo.setUWOperator(mGlobalInput.Operator);
        //        cGrpContInfo.setUWDate(PubFun.getCurrentDate());
        //        cGrpContInfo.setUWTime(PubFun.getCurrentTime());

        // 团体工伤险业务标志
        String tCardFlag = "4";
        cGrpContInfo.setCardFlag(tCardFlag);
        // ---------------------------

        String tContPrintType = "0";
        cGrpContInfo.setContPrintType(tContPrintType);

        cGrpContInfo.setStateFlag("0");

        String tMakeDate = cGrpContInfo.getMakeDate();
        if (tMakeDate == null || tMakeDate.equals(""))
        {
            cGrpContInfo.setMakeDate(mCurDate);
        }

        String tMakeTime = cGrpContInfo.getMakeTime();
        if (tMakeTime == null || tMakeTime.equals(""))
        {
            cGrpContInfo.setMakeTime(mCurTime);
        }

        cGrpContInfo.setOperator(mGlobalInput.Operator);
        cGrpContInfo.setModifyDate(mCurDate);
        cGrpContInfo.setModifyTime(mCurTime);

        // 置换为处理后的数据信息
        mGrpContInfo = cGrpContInfo;
        // --------------------

        return true;
    }

    /**
     * 处理保单层业务员相关信息。
     * @return
     */
    private boolean dealSaleInfo()
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mGrpContInfo.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            String tStrErr = "代理人编码输入错误。";
            buildError("dealSaleInfo", tStrErr);
            return false;
        }

        if (tLAAgentDB.getManageCom() == null)
        {
            String tStrErr = "代理人编码对应数据库中的管理机构为空！";
            buildError("dealSaleInfo", tStrErr);
            return false;
        }

        if (!tLAAgentDB.getManageCom().equals(mGrpContInfo.getManageCom()))
        {
            String tStrErr = "录入的管理机构和数据库中代理人编码对应的管理机构不符合！";
            buildError("dealSaleInfo", tStrErr);
            return false;
        }

        if (mGrpContInfo.getAgentCom() != null && !mGrpContInfo.getAgentCom().equals(""))
        {
            LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(mGrpContInfo.getAgentCom());
            if (!tLAComDB.getInfo())
            {
                String tStrErr = "代理机构编码输入错误。";
                buildError("dealSaleInfo", tStrErr);
                return false;
            }

            if (tLAComDB.getManageCom() == null)
            {
                String tStrErr = "代理机构编码对应数据库中的管理机构为空！";
                buildError("dealSaleInfo", tStrErr);
                return false;
            }

            if (!tLAComDB.getManageCom().equals(mGrpContInfo.getManageCom()))
            {
                String tStrErr = "录入的管理机构和数据库中代理机构编码对应的管理机构不符合！";
                buildError("dealSaleInfo", tStrErr);
                return false;
            }
        }

        mGrpContInfo.setAgentGroup(tLAAgentDB.getAgentGroup());
        mGrpContInfo.setSaleChnlDetail(tLAAgentDB.getBranchType2());

        return true;
    }

    /**
     * 处理投保人信息。
     * @return
     */
    private boolean dealGrpAppntInfo()
    {
        String tGrpAppntName = mGrpAppntInfo.getName();
        if (tGrpAppntName == null || tGrpAppntName.equals(""))
        {
            String tStrErr = "获取投保机构信息失败。";
            buildError("dealGrpAppntInfo", tStrErr);
            return false;
        }

        String tGrpCustomerNo = mGrpAppntInfo.getCustomerNo();

        if (tGrpCustomerNo != null && !tGrpCustomerNo.equals(""))
        {
            String tTmpStrSql = "select 1 from LDGrp where CustomerNo = '" + tGrpCustomerNo + "' and GrpName = '"
                    + tGrpAppntName + "'";
            String tTmpResult = new ExeSQL().getOneValue(tTmpStrSql);
            if (!"1".equals(tTmpResult))
            {
                buildError("dealGrpAppntInfo", "填写客户号对应单位名称与系统中不符，如确认投保单位名称无误，请尝试将客户号删除后，再次尝试保存操作。");
                return false;
            }
        }

        LDGrpSchema tGrpInfo = null;

        if (tGrpCustomerNo != null && !tGrpCustomerNo.equals(""))
        {
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(tGrpCustomerNo);
            if (!tLDGrpDB.getInfo())
            {
                buildError("dealGrpAppntInfo", "客户号[" + tGrpCustomerNo + "]在核心中未找到对应数据信息，请修改后，重新尝试操作。");
                return false;
            }
            tGrpInfo = tLDGrpDB.getSchema();
        }
        else
        {
            // 处理投保单位信息。
            String tStrGrpSql = " select * from LDGrp ldg where ldg.GrpName = '" + tGrpAppntName
                    + "' order by ldg.CustomerNo ";
            LDGrpSet tLDGrpSet = new LDGrpDB().executeQuery(tStrGrpSql);
            if (tLDGrpSet != null && tLDGrpSet.size() > 0)
            {
                tGrpInfo = tLDGrpSet.get(1);
            }
        }

        if (tGrpInfo != null)
        {
        	mGrpInfo = new LDGrpSchema();
        	
            String tCustomerNo = tGrpInfo.getCustomerNo();
            String tGrpName = tGrpInfo.getGrpName();

            tGrpInfo.setTaxpayerType(mGrpAppntInfo.getTaxpayerType());
            tGrpInfo.setTaxNo(mGrpAppntInfo.getTaxNo());
            tGrpInfo.setCustomerBankCode(mGrpAppntInfo.getCustomerBankCode());
            tGrpInfo.setCustomerBankAccNo(mGrpAppntInfo.getCustomerBankAccNo());
            tGrpInfo.setOrganComCode(mGrpAppntInfo.getOrganComCode());
            tGrpInfo.setUnifiedSocialCreditNo(mGrpAppntInfo.getUnifiedSocialCreditNo());


            mGrpAppntInfo.setCustomerNo(tCustomerNo);
            mGrpAppntInfo.setName(tGrpName);

            mGrpContInfo.setAppntNo(tCustomerNo);
            mGrpContInfo.setGrpName(tGrpName);
            mGrpInfo=tGrpInfo;
        }
        else
        {
            String tLimit = "SN";
            String tCustomerNo = PubFun1.CreateMaxNo("GRPNO", tLimit);
            if (!tCustomerNo.equals(""))
            {
                mGrpInfo = new LDGrpSchema();

                mGrpAppntInfo.setCustomerNo(tCustomerNo);
                mGrpAppntInfo.setName(tGrpAppntName);

                mGrpInfo.setCustomerNo(mGrpAppntInfo.getCustomerNo());
                mGrpInfo.setGrpName(mGrpAppntInfo.getName());

                mGrpInfo.setOperator(mGlobalInput.Operator);
                mGrpInfo.setMakeDate(mCurDate);
                mGrpInfo.setMakeTime(mCurTime);
                mGrpInfo.setModifyDate(mCurDate);
                mGrpInfo.setModifyTime(mCurTime);
                mGrpInfo.setTaxpayerType(mGrpAppntInfo.getTaxpayerType());
                mGrpInfo.setTaxNo(mGrpAppntInfo.getTaxNo());
                mGrpInfo.setCustomerBankCode(mGrpAppntInfo.getCustomerBankCode());
                mGrpInfo.setCustomerBankAccNo(mGrpAppntInfo.getCustomerBankAccNo());
                mGrpInfo.setOrganComCode(mGrpAppntInfo.getOrganComCode());
                mGrpInfo.setUnifiedSocialCreditNo(mGrpAppntInfo.getUnifiedSocialCreditNo());

                mGrpContInfo.setAppntNo(tCustomerNo);
                mGrpContInfo.setGrpName(tGrpAppntName);
            }
            else
            {
                String tStrErr = "客户号码生成失败。";
                buildError("dealGrpAppntInfo", tStrErr);
                return false;
            }
        }

        // 由于团体合同号生成规则要求根据客户号进行生成，因此该处理在投保单位处理中进行处理。
        String tProposalGrpContNo = mGrpContInfo.getProposalGrpContNo();
        String tGrpContNo = null;
        if (tProposalGrpContNo == null || tProposalGrpContNo.equals(""))
        {
            // 生成团体投保单流水号ProposalGrpContNo
            tProposalGrpContNo = PubFun1.CreateMaxNo("ProGrpContNo", mGrpAppntInfo.getCustomerNo());
            System.out.println("新生成团体保单号：" + tProposalGrpContNo);

            tGrpContNo = tProposalGrpContNo;
        }
        else
        {
            tGrpContNo = mGrpContInfo.getGrpContNo();
        }

        if (tProposalGrpContNo == null || tProposalGrpContNo.equals(""))
        {
            String tStrErr = "团体投保单号生成失败。";
            buildError("dealGrpAppntInfo", tStrErr);
            return false;
        }

        if (tGrpContNo == null || tGrpContNo.equals(""))
        {
            String tStrErr = "团体合同单号生成失败。";
            buildError("dealGrpAppntInfo", tStrErr);
            return false;
        }

        mGrpContInfo.setProposalGrpContNo(tProposalGrpContNo);
        mGrpContInfo.setGrpContNo(tGrpContNo);

        mGrpAppntInfo.setGrpContNo(tGrpContNo);
        // --------------------------

        mGrpAppntInfo.setPrtNo(mGrpContInfo.getPrtNo());

        mGrpAppntInfo.setOperator(mGlobalInput.Operator);
        mGrpAppntInfo.setMakeDate(mCurDate);
        mGrpAppntInfo.setMakeTime(mCurTime);
        mGrpAppntInfo.setModifyDate(mCurDate);
        mGrpAppntInfo.setModifyTime(mCurTime);

        // 处理投保单位地址信息。每次修改无论信息是否变化，均生成新地址信息。
        try
        {
            String tStrGrpAddrSql = " select integer(nvl(max(integer(lgad.AddressNo)), 0) + 1) from LCGrpAddress lgad "
                    + " where 1 = 1 " + " and lgad.CustomerNo = '" + mGrpAppntInfo.getCustomerNo() + "' ";
            String tAddressNo = new ExeSQL().getOneValue(tStrGrpAddrSql);

            mGrpAddressInfo.setCustomerNo(mGrpAppntInfo.getCustomerNo());
            mGrpAddressInfo.setAddressNo(tAddressNo);

            mGrpAddressInfo.setOperator(mGlobalInput.Operator);
            mGrpAddressInfo.setMakeDate(PubFun.getCurrentDate());
            mGrpAddressInfo.setMakeTime(PubFun.getCurrentTime());
            mGrpAddressInfo.setModifyDate(PubFun.getCurrentDate());
            mGrpAddressInfo.setModifyTime(PubFun.getCurrentTime());

            mGrpAppntInfo.setAddressNo(mGrpAddressInfo.getAddressNo());
            mGrpContInfo.setAddressNo(tAddressNo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            String tStrErr = "客户地址信息生成失败。";
            buildError("dealGrpAppntInfo", tStrErr);
            return false;
        }
        // --------------------------------

        return true;
    }

    /**
     * 
     * @return
     */
    private MMap syncOtherPolicyInfo()
    {
        MMap tMMap = new MMap();
        StringBuffer tStrSql = null;

        String tGrpContNo = mGrpContInfo.getGrpContNo();
        if (tGrpContNo == null || tGrpContNo.equals(""))
        {
            return new MMap();
        }

        // 团单层数据
        tStrSql = new StringBuffer();
        tStrSql.append(" update LCGrpPol set ");

        tStrSql.append(" ManageCom = '" + mGrpContInfo.getManageCom() + "', ");
        tStrSql.append(" CValidate = '" + mGrpContInfo.getCValiDate() + "', ");
        tStrSql.append(" SaleChnl = '" + mGrpContInfo.getSaleChnl() + "', ");
        tStrSql.append(" AgentCom = '" + mGrpContInfo.getAgentCom() + "', ");
        tStrSql.append(" AgentCode = '" + mGrpContInfo.getAgentCode() + "', ");
        tStrSql.append(" AgentGroup = '" + mGrpContInfo.getAgentGroup() + "', ");
        tStrSql.append(" PayIntv = " + mGrpContInfo.getPayIntv() + ", ");
        tStrSql.append(" PayMode = '" + mGrpContInfo.getPayMode() + "', ");
        tStrSql.append(" ApproveFlag = '" + mGrpContInfo.getApproveFlag() + "', ");
        tStrSql.append(" UwFlag = '" + mGrpContInfo.getUWFlag() + "', ");

        tStrSql.append(" CustomerNo = '" + mGrpAppntInfo.getCustomerNo() + "', ");
        tStrSql.append(" AddressNo = '" + mGrpAppntInfo.getAddressNo() + "', ");
        tStrSql.append(" GrpName = '" + mGrpAppntInfo.getName() + "', ");

        
//      更新的集团交叉信息
        
        /*tStrSql.append(" Crs_SaleChnl = '" + mGrpContInfo.getCrs_SaleChnl() + "', ");
        tStrSql.append(" Crs_BussType = '" + mGrpContInfo.getCrs_BussType() + "', ");
        tStrSql.append(" GrpAgentCode = '" + mGrpContInfo.getGrpAgentCode() + "', ");
        tStrSql.append(" GrpAgentCom = '" + mGrpContInfo.getGrpAgentCom() + "', ");
        tStrSql.append(" GrpAgentName = '" + mGrpContInfo.getGrpAgentName() + "', ");
        tStrSql.append(" GrpAgentIDNo = '" + mGrpContInfo.getGrpAgentIDNo()+ "', ");*/
        
        
        tStrSql.append(" Operator = '" + mGrpContInfo.getOperator() + "', ");
        tStrSql.append(" ModifyDate = '" + mGrpContInfo.getModifyDate() + "', ");
        tStrSql.append(" ModifyTime = '" + mGrpContInfo.getModifyTime() + "' ");

        
             
        tStrSql.append(" where 1 = 1 ");
        tStrSql.append(" and GrpContNo = '" + tGrpContNo + "' ");
        tMMap.put(tStrSql.toString(), SysConst.UPDATE);
        // --------------------

        // 分单层数据，有分单数据不能调整：生效日期、失效日期、缴费频次
        // LCCont
        tStrSql = new StringBuffer();
        tStrSql.append(" update LCCont set ");

        tStrSql.append(" ManageCom = '" + mGrpContInfo.getManageCom() + "', ");
        tStrSql.append(" SaleChnl = '" + mGrpContInfo.getSaleChnl() + "', ");
        tStrSql.append(" AgentCom = '" + mGrpContInfo.getAgentCom() + "', ");
        tStrSql.append(" AgentCode = '" + mGrpContInfo.getAgentCode() + "', ");
        tStrSql.append(" AgentGroup = '" + mGrpContInfo.getAgentGroup() + "', ");
        tStrSql.append(" PayMode = '" + mGrpContInfo.getPayMode() + "', ");
        tStrSql.append(" ApproveFlag = '" + mGrpContInfo.getApproveFlag() + "', ");
        tStrSql.append(" UwFlag = '" + mGrpContInfo.getUWFlag() + "', ");

        tStrSql.append(" AppntNo = '" + mGrpAppntInfo.getCustomerNo() + "', ");
        tStrSql.append(" AppntName = '" + mGrpAppntInfo.getName() + "', ");

        tStrSql.append(" Operator = '" + mGrpContInfo.getOperator() + "', ");
        tStrSql.append(" ModifyDate = '" + mGrpContInfo.getModifyDate() + "', ");
        tStrSql.append(" ModifyTime = '" + mGrpContInfo.getModifyTime() + "' ");

        tStrSql.append(" where 1 = 1 ");
        tStrSql.append(" and GrpContNo = '" + tGrpContNo + "' ");
        tMMap.put(tStrSql.toString(), SysConst.UPDATE);

        // LCPol
        tStrSql = new StringBuffer();
        tStrSql.append(" update LCPol set ");

        tStrSql.append(" ManageCom = '" + mGrpContInfo.getManageCom() + "', ");
        tStrSql.append(" SaleChnl = '" + mGrpContInfo.getSaleChnl() + "', ");
        tStrSql.append(" AgentCom = '" + mGrpContInfo.getAgentCom() + "', ");
        tStrSql.append(" AgentCode = '" + mGrpContInfo.getAgentCode() + "', ");
        tStrSql.append(" AgentGroup = '" + mGrpContInfo.getAgentGroup() + "', ");
        tStrSql.append(" PayMode = '" + mGrpContInfo.getPayMode() + "', ");
        tStrSql.append(" ApproveFlag = '" + mGrpContInfo.getApproveFlag() + "', ");
        tStrSql.append(" UwFlag = '" + mGrpContInfo.getUWFlag() + "', ");

        tStrSql.append(" AppntNo = '" + mGrpAppntInfo.getCustomerNo() + "', ");
        tStrSql.append(" AppntName = '" + mGrpAppntInfo.getName() + "', ");

        tStrSql.append(" Operator = '" + mGrpContInfo.getOperator() + "', ");
        tStrSql.append(" ModifyDate = '" + mGrpContInfo.getModifyDate() + "', ");
        tStrSql.append(" ModifyTime = '" + mGrpContInfo.getModifyTime() + "' ");

        tStrSql.append(" where 1 = 1 ");
        tStrSql.append(" and GrpContNo = '" + tGrpContNo + "' ");
        tMMap.put(tStrSql.toString(), SysConst.UPDATE);

        // LCDuty

        // LCPrem

        // LCGet

        // --------------------

        return tMMap;
    }

    /**
     * 清除保单填写前数据，部分数据删除，暂时不考虑备份OB表
     * @param cContNo
     * @return
     */
    private MMap cleanGrpCont(String cContNo)
    {
        MMap tMMap = new MMap();
        String tStrSql = null;

        // 保单信息 LCGrpCont
        tStrSql = " select * from LCGrpCont where GrpContNo = '" + cContNo + "' ";
        LCGrpContDB tGrpContDB = new LCGrpContDB();
        LCGrpContSet tGrpContInfos = tGrpContDB.executeQuery(tStrSql);
        tMMap.put(tGrpContInfos, SysConst.DELETE);
        tStrSql = null;
        // --------------------

        // 投保单位 LCGrpAppnt
        tStrSql = " select * from LCGrpAppnt where GrpContNo = '" + cContNo + "' ";
        LCGrpAppntDB tGrpAppntDB = new LCGrpAppntDB();
        LCGrpAppntSet tGrpAppntInfos = tGrpAppntDB.executeQuery(tStrSql);
        tMMap.put(tGrpAppntInfos, SysConst.DELETE);
        tStrSql = null;
        // --------------------

        // 暂时不删除投保单位地址信息，新地址按顺序递增AddressNo，投保单位地址 LCGrpAddress
        //        tStrSql = " select * from LCGrpAddress lgad "
        //                + " where lgad.CustomerNo in (select lga.CustomerNo from LCGrpAppnt lga where lga.GrpContNo = '"
        //                + cContNo
        //                + "') and lgad.AddressNo in (select lga.AddressNo from LCGrpAppnt lga where lga.GrpContNo = '"
        //                + cContNo + "')";
        //        LCGrpAddressDB tLCGrpAddrDB = new LCGrpAddressDB();
        //        LCGrpAddressSet tGrpAddrInfos = tLCGrpAddrDB.executeQuery(tStrSql);
        //        tMMap.put(tGrpAddrInfos, SysConst.DELETE);
        //        tStrSql = null;
        // --------------------

        return tMMap;
    }

    /**
     * 获取保单信息。
     * @param cPrtNo
     * @return
     */
    private LCGrpContSchema loadGrpContInfo(String cPrtNo)
    {
        String tStrSql = "select * from LCGrpCont where PrtNo = '" + cPrtNo + "' ";
        LCGrpContDB tGrpContInfoDB = new LCGrpContDB();
        LCGrpContSet tGrpContInfos = tGrpContInfoDB.executeQuery(tStrSql);

        if (tGrpContInfos == null || tGrpContInfos.size() == 0)
        {
            return null;
        }
        LCGrpContSchema tGrpContInfo = tGrpContInfos.get(1);

        System.out.println("GrpContNo：" + tGrpContInfo.getGrpContNo());
        System.out.println("ProposalGrpContNo：" + tGrpContInfo.getProposalGrpContNo());

        return tGrpContInfo;
    }

    /**
     * 锁定签单动作。
     * @param cLCContSchema
     * @return
     */
    private MMap lockCont(LCGrpContSchema cGrpContInfo)
    {
        MMap tMMap = null;

        // 锁定标志为：“BG”
        String tLockNoType = "BG";
        // -----------------------

        // 锁定有效时间
        String tAIS = "60";
        // -----------------------

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cGrpContInfo.getPrtNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            String tErrInfo = tLockTableActionBL.mErrors.getFirstError();
            buildError("lockCont", tErrInfo);
            return null;
        }

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + " : " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("系统超时，请稍后尝试重新提交，谢谢。");
            buildError("submitData", "系统超时，请稍后尝试重新提交，谢谢。");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
    
    /**
     * 处理综合开拓数据。
     * @param cGrpContNo
     * @return
     */
    private MMap delExtendInfo(LCExtendSchema aLCExtendSchema)
    {
        MMap tMMap = null;

        ExtendBL tExtendBL = new ExtendBL();

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(aLCExtendSchema);

        tMMap = tExtendBL.submitData(tVData, "");

        if (tMMap == null)
        {
            buildError("delCoInsuranceInfo", "共保要素处理失败。");
            return null;
        }

        return tMMap;
    }
}
