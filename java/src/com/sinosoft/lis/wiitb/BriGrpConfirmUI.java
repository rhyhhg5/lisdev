package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BriGrpConfirmUI {
	/** �������� */
    public CErrors mErrors = new CErrors();

    public BriGrpConfirmUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	BriGrpConfirmBL tBriGrpConfirmBL = new BriGrpConfirmBL();
            if (!tBriGrpConfirmBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tBriGrpConfirmBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
