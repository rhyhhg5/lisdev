package com.sinosoft.lis.wiitb;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LCBriGrpImportDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCBriGrpImportDetailSchema;
import com.sinosoft.lis.vschema.LCBriGrpImportDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BriGrpDownLoadBL {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
	
    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;
    
    private String mBatchNo = null;
    
    private Element root = null;
    
    private Document Doc = null;
    
    private String mExportPath = null;
    
    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
	
	public boolean submitData(VData cInputData, String cOperate)
    {
    	if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }
    	
//    	if (!submit())
//        {
//            return false;
//        }

        return true;
    }
	private MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }
        if(!save()){
        	return null;
        }

        return mMap;
    }
	private boolean getInputData(VData cInputData, String cOperate)
    {

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }
        
        mBatchNo = (String)mTransferData.getValueByName("BatchNo"); 
        if(mBatchNo == null || "".equals(mBatchNo)){
        	buildError("getInputData", "获取批次号码失败。");
            return false;
        }
        mExportPath = (String)mTransferData.getValueByName("ExportPath"); 
        if(mExportPath == null || "".equals(mExportPath)){
        	buildError("getInputData", "获取生成文件路径失败。");
            return false;
        }
        return true;
    }
    private boolean checkData()
    {
    	return true;
    }
    private boolean dealData()
    {
    	LCBriGrpImportDetailDB tLCBriGrpImportDetailDB = new LCBriGrpImportDetailDB();
    	tLCBriGrpImportDetailDB.setBatchNo(mBatchNo);
    	tLCBriGrpImportDetailDB.setEnableFlag("01");
    	LCBriGrpImportDetailSet tLCBriGrpImportDetailSet = tLCBriGrpImportDetailDB.query();
    	if(tLCBriGrpImportDetailSet==null || tLCBriGrpImportDetailSet.size()<=0){
    		buildError("dealData", "该批次没有需回送的数据。");
            return false;
    	}
    	root = new Element("DataSet");
    	Doc = new Document(root);    	
    	BuildMsgHead();//生成MsgHead
    	BuildQryPolicyInfo(tLCBriGrpImportDetailSet);//生成PolicyInfo
//    	XMLOutputter XMLOut = new XMLOutputter();              
    	// 输出 user.xml 文件；        
//    	XMLOut.output(Doc, new FileOutputStream("user.xml"));
    	
    	return true;
    }
    
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriGrpTempFeeFromBank";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public MMap getMMap(){
    	return mMap;
    }
    
    public CErrors getCErrors(){
    	return mErrors;
    }
    
    private boolean save() {
    	
		try {
			FileOutputStream tFos = new FileOutputStream(mExportPath + mBatchNo+"_return.xml");
			output(Doc, tFos);
			tFos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return true;
	}
    private void BuildMsgHead(){  
    	
		Element elements = new Element("MsgHead");                      
		elements.addContent(new Element("BatchNo").setText(mBatchNo));           
		elements.addContent(new Element("SendDate").setText(PubFun.getCurrentDate()));           
		elements.addContent(new Element("SendTime").setText(PubFun.getCurrentTime()));   
		elements.addContent(new Element("BranchCode").setText("001"));           
		elements.addContent(new Element("SendOperator").setText(mGlobalInput.Operator));           
		elements.addContent(new Element("MsgType").setText("WIIQP00001")); 
		
		root.addContent(elements);        
    }
    private void BuildQryPolicyInfo(LCBriGrpImportDetailSet aLCBriGrpImportDetailSet){  
    	
		Element elements = new Element("QryPolicyInfo"); 
		for(int i=1;i<=aLCBriGrpImportDetailSet.size();i++){
			
			LCBriGrpImportDetailSchema aLCBriGrpImportDetailSchema = aLCBriGrpImportDetailSet.get(i);
			
			Element aItem = new Element("Item");
			aItem.addContent(new Element("PrtNo").setText(aLCBriGrpImportDetailSchema.getBussNo())); 
			aItem.addContent(new Element("ImpBatchNo").setText(mBatchNo)); 
			aItem.addContent(new Element("DealFlag").setText(aLCBriGrpImportDetailSchema.getDealFlag())); 
			aItem.addContent(new Element("DealInfo").setText(aLCBriGrpImportDetailSchema.getDealRemark())); 
			aItem.addContent(new Element("GrpContNo").setText(aLCBriGrpImportDetailSchema.getGrpContNo())); 
			aItem.addContent(new Element("CValiDate").setText(aLCBriGrpImportDetailSchema.getCValiDate())); 
			aItem.addContent(new Element("GrpName").setText(aLCBriGrpImportDetailSchema.getGrpName())); 
			aItem.addContent(new Element("OrgancomCode").setText(aLCBriGrpImportDetailSchema.getOrgancomCode())); 
			aItem.addContent(new Element("OtherCertificates").setText(aLCBriGrpImportDetailSchema.getOtherCertificates())); 
			aItem.addContent(new Element("CInValidate").setText(aLCBriGrpImportDetailSchema.getCInValidate())); 
			aItem.addContent(new Element("PolApplyDate").setText(aLCBriGrpImportDetailSchema.getPolApplyDate())); 
			aItem.addContent(new Element("SignDate").setText(aLCBriGrpImportDetailSchema.getSignDate())); 
			aItem.addContent(new Element("ConfMakeDate").setText(aLCBriGrpImportDetailSchema.getConfMakeDate())); 
			aItem.addContent(new Element("Prem").setText(String.valueOf(format(aLCBriGrpImportDetailSchema.getPrem())))); 
			
			elements.addContent(aItem);
		}
		root.addContent(elements);        
    }
    /**
	 * 将Document输出到指定的输出流。
	 * 输出格式：GBK编码，去首尾空格，各层标签缩进3空格。
	 * 注意：此方法不自动关闭流，如有需要，请在调用后手动关闭。
	 */
	public void output(Document pXmlDoc, OutputStream pOs) throws IOException {
		XMLOutputter mXMLOutputter = new XMLOutputter();
		mXMLOutputter.setEncoding("UTF-8");
		mXMLOutputter.setTrimText(true);
		mXMLOutputter.setIndent("   ");
		mXMLOutputter.setNewlines(true);
		mXMLOutputter.output(pXmlDoc, pOs);
	}
	/**
     * 格式化浮点型数据
     * @param dValue double
     * @return String
     */
    private static String format(double dValue)
    {
        return new DecimalFormat("0.00").format(dValue);
    }
}
