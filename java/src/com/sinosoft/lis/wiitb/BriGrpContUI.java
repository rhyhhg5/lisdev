/**
 * 2011-3-10
 */
package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BriGrpContUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public BriGrpContUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            BriGrpContBL tLogicBL = new BriGrpContBL();
            if (!tLogicBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tLogicBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
