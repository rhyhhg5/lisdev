/**
 * 2011-8-10
 */
package com.sinosoft.lis.wiitb;

import org.apache.log4j.Logger;
import org.jdom.Document;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.lis.brieftb.ContInputAgentcomChkBL;
import com.sinosoft.lis.certifybusiness.CertifyDiskImportLog;
import com.sinosoft.lis.db.LCBriGrpImportBatchInfoDB;
import com.sinosoft.lis.db.LCBriGrpImportDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBriGrpImportDetailSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCBriGrpImportDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class PubBriGrpBatchImportAPI
{
    private final static Logger mLogger = Logger.getLogger(PubBriGrpBatchImportAPI.class);

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mBatchNo = null;
    
    private String mMsgType = null;

    private Document mInXmlDoc = null;
    
    public PubBriGrpBatchImportAPI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "参数[导入文件名（BatchNo）]不存在。");
            return false;
        }

        mInXmlDoc = (Document) mTransferData.getValueByName("InXmlDoc");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "参数[报文数据（InXmlDoc）]不存在。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        if ("Import".equals(mOperate))
        {
            if (!importDatas())
            {
                return false;
            }
        }

        return true;
    }

    private boolean importDatas()
    {
        IXmlMsgColls tMsgCols = null;

        // 报文归档
        //        MsgXmlArchive.xmlArchive(cInXmlDoc);
        // --------------------

        // 解析报文
        MsgXmlParse tMsgXmlParse = new MsgXmlParse();
        tMsgCols = tMsgXmlParse.deal(mInXmlDoc);
        if (tMsgCols == null)
        {
            buildError("importDatas", tMsgXmlParse.getErrInfo());
            return false;
        }
        // --------------------

        // 校验报文基本信息完整性
        if (!chkXmlBaseInfo(tMsgCols))
        {
            return false;
        }
        // --------------------

        // 创建批次任务
        if (!createBatchMission(tMsgCols))
        {
            return false;
        }
        // --------------------

        // 对应业务处理调度
        if (!dealTask(tMsgCols))
        {
            return false;
        }
        // --------------------

        // 处理结果封装
        //        Document tOutDoc = null;
        //        MsgXmlPack tMsgXmlPack = new MsgXmlPack();
        //        tOutDoc = tMsgXmlPack.deal(tMsgCols);
        // --------------------

        return true;
    }

    private boolean createBatchMission(IXmlMsgColls cMsgCols)
    {
        // 判断该批次是否已经产生任务清单，如已经产生，则不进行重新创建。
        LCBriGrpImportBatchInfoDB tImportBatchInfo = new LCBriGrpImportBatchInfoDB();
        tImportBatchInfo.setBatchNo(mBatchNo);
        if (tImportBatchInfo.getInfo())
        {
            mLogger.info("[" + mBatchNo + "]报文已经产生对应任务清单。");
            return true;
        }
        // --------------------

        VData tVData = new VData();

        TransferData tTransferData = new TransferData();
        tVData.add(tTransferData);

        tVData.add(cMsgCols);
        tVData.add(mGlobalInput);

        BriGrpCreatTaskBL tBriGrpCreatTaskBL = new BriGrpCreatTaskBL();
        if (!tBriGrpCreatTaskBL.submitData(tVData, null))
        {
            buildError("createBatchMission", tBriGrpCreatTaskBL.mErrors.getFirstError());
            return false;
        }

        return true;
    }

    private boolean chkXmlBaseInfo(IXmlMsgColls cMsgCols)
    {
        // 校验报文头
        IXmlMsgColls[] tLMsgHeadInfo = cMsgCols.getBodyByFlag("MsgHead");
        if (tLMsgHeadInfo == null || tLMsgHeadInfo.length != 1)
        {
            buildError("chkXmlBaseInfo", "报文头信息格式不符合约定，请核实。");
            return false;
        }

        IXmlMsgColls tMsgHeadInfo = tLMsgHeadInfo[0];

        String tMsgHeadBatchNo = tMsgHeadInfo.getTextByFlag("BatchNo");
        if (tMsgHeadBatchNo == null || !tMsgHeadBatchNo.equals(mBatchNo))
        {
            buildError("chkXmlBaseInfo", "批次号与文件中批次号不符，请核对批次文件名称与报文内容批次信息是否一致。");
            return false;
        }
        
        mMsgType = tMsgHeadInfo.getTextByFlag("MsgType");
        if (mMsgType == null || "".equals(mMsgType))
        {
            buildError("chkXmlBaseInfo", "报文类型为空，请核查！");
            return false;
        }

        BriGrpCommonCheckBL tChkCommTool = new BriGrpCommonCheckBL();
        if (!tChkCommTool.checkMsgHead(tMsgHeadInfo))
        {
            buildError("chkXmlBaseInfo", tChkCommTool.getErrInfo());
            return false;
        }
        // --------------------

        // 校验正文数据内容
        IXmlMsgColls[] tLMsgBody = cMsgCols.getBodyByFlag("PolicyInfo");
        if (tLMsgBody == null || tLMsgBody.length <= 0)
        {
            buildError("chkXmlBaseInfo", "报文中不存在业务数据信息或业务信息节点命名不符合约定规范，请核实。");
            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealTask(IXmlMsgColls cMsgCols)
    {
        String tCurBatchNo = mBatchNo;

        LCBriGrpImportBatchInfoDB tBatchTaskInfo = new LCBriGrpImportBatchInfoDB();
        tBatchTaskInfo.setBatchNo(tCurBatchNo);
        if (!tBatchTaskInfo.getInfo())
        {
            buildError("dealTask", "未找到批次任务信息。");
            return false;
        }

        // 如果导入标志不是00-待处理或03-异常终止状态时，报错返回
        String tImportState = tBatchTaskInfo.getImportState();
        if (!"00".equals(tImportState) && !"03".equals(tImportState))
        {
            if ("02".equals(tImportState))
            {
                buildError("dealTask", "该批次已经成功导入，不允许重复再次导入。");
                return false;
            }
            else
            {
                buildError("dealTask", "该批次正在导入处理中，请稍候。");
                return false;
            }
        }
        // --------------------

        // 对批次加锁，导入状态为：01-处理中
        tBatchTaskInfo.setImportState("01");
        tBatchTaskInfo.setImportStartDate(PubFun.getCurrentDate());
        if (!tBatchTaskInfo.update()) // 及时提交数据库。锁定生效。
        {
            buildError("dealTask", "更新批次导入状态时，出现异常。请稍候重试。");
            return false;
        }
        // --------------------

        // 创建日志记录器
        CertifyDiskImportLog tImportLog = null;

        try
        {
            tImportLog = new CertifyDiskImportLog(mGlobalInput, mBatchNo);
        }
        catch (Exception e)
        {
            String tStrErr = "创建日志失败。";
            buildError("getInputData", tStrErr);
            e.printStackTrace();
            return false;
        }
        // --------------------

        // 获取明细任务清单
        String tStrSql = "select * from LCBriGrpImportDetail where BatchNo = '" + tCurBatchNo + "'";
        LCBriGrpImportDetailSet tSubTaskSet = null;
        tSubTaskSet = new LCBriGrpImportDetailDB().executeQuery(tStrSql);

        for (int i = 1; i <= tSubTaskSet.size(); i++)
        {
            LCBriGrpImportDetailSchema tSubTaskInfo = tSubTaskSet.get(i);

            String tSubImportState = tSubTaskInfo.getImportState();
            if (!"00".equals(tSubImportState)) // 只处理00-未处理的任务
            {
                mLogger.info("[" + tSubTaskInfo.getBatchNo() + "-" + tSubTaskInfo.getBussNo() + "]任务已经导入成功，不需重复处理。");
                continue;
            }

            if (!dealSubTask(tSubTaskInfo, cMsgCols, tImportLog))
            {
                continue;
            }
        }
        // --------------------

        // 所有子任务执行完毕，设置批次任务状态
        tBatchTaskInfo.setImportState("02");
        tBatchTaskInfo.setImportEndDate(PubFun.getCurrentDate());
        if (!tBatchTaskInfo.update()) // 及时提交数据库。锁定生效。
        {
            buildError("dealTask", "更新批次导入完成状态时，出现异常。请稍候重试。");
            return false;
        }
        // --------------------

        // 关闭日志记录器
        if (!tImportLog.logEnd())
        {
            buildError("dealTask", "关闭日志记录器时，出现异常。");
            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealSubTask(LCBriGrpImportDetailSchema cSubTaskInfo, IXmlMsgColls cMsgCols,
            CertifyDiskImportLog cImportLog)
    {
        MMap tMMap = null;
        MMap tTmpMap = null;

        String tBussNo = cSubTaskInfo.getBussNo();

        IXmlMsgColls[] tMsgBodyDatas = cMsgCols.getBodyByFlag("PolicyInfo");

        for (int i = 0; i < tMsgBodyDatas.length; i++)
        {
            IXmlMsgColls tSubMsgBodyData = tMsgBodyDatas[i];
            String tPrtNo = tSubMsgBodyData.getTextByFlag("PrtNo");
            String tOperator = "";
            if("WIIPI00001".equals(mMsgType)){
            	tOperator = "Create";
            }else if("WIIPI00002".equals(mMsgType)){
            	tOperator = "Update";
            }
            if (!tBussNo.equals(tPrtNo))
                continue;
            
            //检验印刷号是否符合规则
            if (!(tPrtNo.matches("[A-Za-z0-9]*")))
            {
            	String tStrErr = "印刷号格式有误，必须为字母或数字！";
                if (!cImportLog.errLog(tPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(cImportLog.mErrors);
                    return false;
                }

                return false;
            }
            
            // 对待处理保单进行校验
            if(checkSign(tPrtNo)){
            	String tStrErr = "印刷号为【"+tPrtNo+"】的保单已签单成功，不可再次导入！";
            	if (!cImportLog.errLog(tPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(cImportLog.mErrors);
                    return false;
                }
            	cSubTaskInfo.setImportState("00"); // 设置为未导入
                cSubTaskInfo.setEnableFlag("02"); // 设置任务失效 00 默认导入 01 有效 02失效
                cSubTaskInfo.setImportDate(PubFun.getCurrentDate());

                tMMap = new MMap();
                tMMap.put(cSubTaskInfo.getSchema(), SysConst.UPDATE);
                
                if (!submit(tMMap))
                {
                    return false;
                }
                tMMap = null;
                return false;
            }
            
            if(checkTempfee(tPrtNo)){
            	String tStrErr = "印刷号为【"+tPrtNo+"】的保单已有财务确认数据，不可再次导入！";
            	if (!cImportLog.errLog(tPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(cImportLog.mErrors);
                    return false;
                }
            	cSubTaskInfo.setImportState("00"); // 设置为未导入
                cSubTaskInfo.setEnableFlag("02"); // 设置任务失效 00 默认导入 01 有效 02失效
                cSubTaskInfo.setImportDate(PubFun.getCurrentDate());

                tMMap = new MMap();
                tMMap.put(cSubTaskInfo.getSchema(), SysConst.UPDATE);
                
                if (!submit(tMMap))
                {
                    return false;
                }
                tMMap = null;
            	return false;
            }
            
            String tBatchNo = getBatchNo(tPrtNo,mBatchNo);//获取该印刷号对应的有效未确认的保单对应的批次号
            if(tBatchNo != null && !"".equals(tBatchNo) ){
        		String tStrErr = "印刷号为【"+tPrtNo+"】的保单在批次【"+tBatchNo+"】中未处理完成！";
        		if (!cImportLog.errLog(tPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(cImportLog.mErrors);
                    return false;
                }
        		cSubTaskInfo.setImportState("00"); // 设置为未导入
                cSubTaskInfo.setEnableFlag("02"); // 设置任务失效 00 默认导入 01 有效 02失效
                cSubTaskInfo.setImportDate(PubFun.getCurrentDate());

                tMMap = new MMap();
                tMMap.put(cSubTaskInfo.getSchema(), SysConst.UPDATE);
                
                if (!submit(tMMap))
                {
                    return false;
                }
                tMMap = null;
        		return false;
        	}

            VData tVData = new VData();

            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("BatchNo", mBatchNo);
            tTransferData.setNameAndValue("PrtNo", tPrtNo);
            tTransferData.setNameAndValue("XmlMsgDatas", tSubMsgBodyData);
            tTransferData.setNameAndValue("ImportLog", cImportLog);
            tTransferData.setNameAndValue("MsgType", mMsgType);

            tVData.add(mGlobalInput);
            tVData.add(tTransferData);

            ParseBriWiiGrpContBL tParseBriWiiGrpContBL = new ParseBriWiiGrpContBL();
            tTmpMap = tParseBriWiiGrpContBL.getSubmitMap(tVData, tOperator);
            
            //江苏中介校验
            if(tTmpMap != null){
            	/* 获取销售渠道 */
            	String jSaleChnl = new ExeSQL().getOneValue("select salechnl from lcgrpcont where prtno = '"+tPrtNo+"'");
            	/* 获取保单号 */
            	String jGrpContNo = new ExeSQL().getOneValue("select grpcontno from lcgrpcont where prtno = '"+tPrtNo+"'");
            	/* 获取管理机构 */
            	String jManageCom = new ExeSQL().getOneValue("select managecom from lcgrpcont where prtno = '"+tPrtNo+"'");
            	/* 截取管理机构前四位 */
            	String jSubManageCom = jManageCom.substring(0,4);
            	
            	String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
        			  	    + "and code = '"+jSaleChnl+"'";
                SSRS tSSRS = new ExeSQL().execSQL(tSql);            	         	
            	if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(jSubManageCom)){
            		ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
          	  	  	TransferData jTransferData = new TransferData();
          	  	  	jTransferData.setNameAndValue("ContNo",jGrpContNo);
          	  	  	VData jVData = new VData();
          	  	  	jVData.add(jTransferData);
          	  	  	if(!tDealQueryAgentInfoBL.submitData(jVData, "check")){
          	  	  		String tStrErr = "印刷号为【" + tPrtNo + "】的保单，导入数据时发生错误：" + tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
          	  	  		if (!cImportLog.errLog(tPrtNo, tStrErr)){
          	  	  			mErrors.copyAllErrors(cImportLog.mErrors);
          	  	  			return false;
          	  	  		}
    	            return false;
          	  	  	}
            	}
            }//End JS check
            
            if (tTmpMap == null)
            {
                cSubTaskInfo.setImportState("00"); // 设置为未导入
                cSubTaskInfo.setEnableFlag("02"); // 设置任务失效 00 默认导入 01 有效 02失效
                cSubTaskInfo.setImportDate(PubFun.getCurrentDate());

                tMMap = new MMap();
                tMMap.put(cSubTaskInfo.getSchema(), SysConst.UPDATE);
                
                if (!submit(tMMap))
                {
                    return false;
                }
                tMMap = null;
                return false;
            }
            
            tMMap = new MMap();
            tMMap.add(tTmpMap);
            tTmpMap = null;

            // 修改任务状态
            cSubTaskInfo.setImportState("02"); // 设置为已导入
            cSubTaskInfo.setEnableFlag("01"); // 设置任务成功
            cSubTaskInfo.setImportDate(PubFun.getCurrentDate());
            tMMap.put(cSubTaskInfo.getSchema(), SysConst.UPDATE);
            // --------------------
 
            if (!submit(tMMap))
            {
            	
                return false;
            }
            tMMap = null;

            break; // 相同印刷号，只处理一次
        }

        return true;
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap cMMap)
    {
        VData data = new VData();
        data.add(cMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("系统超时，请稍后尝试重新提交，谢谢。");
            buildError("submitData", "系统超时，请稍后尝试重新提交，谢谢。");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
    
    //是否已有签单数据
    private boolean checkSign(String aPrtNo){
    	String tSignSql = "select appflag,signdate from lcgrpcont where prtno = '"+aPrtNo+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(tSignSql);
    	if(tSSRS != null && tSSRS.MaxRow == 1){
    		if("1".equals(tSSRS.GetText(1, 1)) || (tSSRS.GetText(1, 2) != null && !"".equals(tSSRS.GetText(1, 2)))){//已签单
    			return true;
    		}
    	}
    	return false;
    }
    /**
     * 校验是否存在银行数据
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkTempfee(String aPrtNo){
    	String tSql = "select 1 from ljtempfee where otherno = '"+aPrtNo+"' and enteraccdate is not null and confdate is null ";
    	String tBankFlag = new ExeSQL().getOneValue(tSql);
    	if(!"1".equals(tBankFlag)){
			return false;
    	}
    	return true;
    }
    
//  校验保单是否处理完成
    private static String getBatchNo(String aPrtNo,String aBatchNo){
    	String tSql = "select batchno from lcbrigrpimportdetail where bussno = '"+aPrtNo+"' and enableflag  != '02' and confstate != '02' and batchno !='"+aBatchNo+"'";//有效未确认的保单
    	ExeSQL tExeSQL = new ExeSQL();
    	String tBatchNo = tExeSQL.getOneValue(tSql);
    	return tBatchNo;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        mLogger.error(szFunc + ":" + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "BriGrpBatchImportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
