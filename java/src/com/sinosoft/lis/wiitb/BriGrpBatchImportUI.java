/**
 * 2011-8-9
 */
package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BriGrpBatchImportUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public BriGrpBatchImportUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            BriGrpBatchImportBL tBusLogic = new BriGrpBatchImportBL();
            if (!tBusLogic.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tBusLogic.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
