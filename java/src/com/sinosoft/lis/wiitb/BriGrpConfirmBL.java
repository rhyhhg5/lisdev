package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.db.LCBriGrpImportBatchInfoDB;
import com.sinosoft.lis.db.LCBriGrpImportDetailDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCBriGrpImportBatchInfoSchema;
import com.sinosoft.lis.schema.LCBriGrpImportDetailSchema;
import com.sinosoft.lis.vschema.LCBriGrpImportBatchInfoSet;
import com.sinosoft.lis.vschema.LCBriGrpImportDetailSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BriGrpConfirmBL {
	
	/** 错误处理类 */
    public CErrors mErrors = new CErrors();
	
    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;
    
    private String mBatchNo = null;
    
//    private String mPayMode = null;
//    
//    private String mBankCode = null;
    
    private static final String Confirm_Succ = "处理成功";
    
    private LCBriGrpImportBatchInfoSchema mLCBriGrpImportBatchInfoSchema = new LCBriGrpImportBatchInfoSchema();
    
    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
	
	public boolean submitData(VData cInputData, String cOperate)
    {
    	if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }
    	
        return true;
    }
	private MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }
	private boolean getInputData(VData cInputData, String cOperate)
    {

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }
        
        mBatchNo = (String)mTransferData.getValueByName("BatchNo"); 
        if(mBatchNo == null || "".equals(mBatchNo)){
        	buildError("getInputData", "获取批次号码失败。");
            return false;
        }
        
        return true;
    }
    private boolean checkData()
    {
        if(!checkBatchInfo()){
        	return false;
        }
    	return true;
    }
    private boolean dealData()
    {
    	
    	boolean AllFlag = true;
    	LCBriGrpImportDetailDB tLCBriGrpImportDetailDB = new LCBriGrpImportDetailDB();
    	tLCBriGrpImportDetailDB.setBatchNo(mBatchNo);
    	tLCBriGrpImportDetailDB.setConfState("00");
    	LCBriGrpImportDetailSet tLCBriGrpImportDetailSet = tLCBriGrpImportDetailDB.query();
    	if(tLCBriGrpImportDetailSet==null || tLCBriGrpImportDetailSet.size()<=0){
    		buildError("dealData", "获取该批次明细失败。");
            return false;
    	}
    	for(int i=1;i<=tLCBriGrpImportDetailSet.size();i++){
    		MMap tDetailMMap = new MMap();
    		LCBriGrpImportDetailSchema tLCBriGrpImportDetailSchema = tLCBriGrpImportDetailSet.get(i);
    		String tPrtNo = tLCBriGrpImportDetailSchema.getBussNo();
    		if("00".equals(tLCBriGrpImportDetailSchema.getEnableFlag())){
    			System.out.println("批次【"+mBatchNo+"】的保单【印刷号："+tLCBriGrpImportDetailSchema.getBussNo()+"】为无效保单。");
    			buildError("dealData", "批次【"+mBatchNo+"】的保单【印刷号："+tLCBriGrpImportDetailSchema.getBussNo()+"】为无效保单。");
    			tLCBriGrpImportDetailSchema.setDealRemark(mErrors.getLastError());
    			MMap tempMMap = new MMap();
    			tempMMap.put(tLCBriGrpImportDetailSchema, SysConst.UPDATE);
    			submit(tempMMap);
    			continue;
    		}
    		if(!"02".equals(tLCBriGrpImportDetailSchema.getImportState())){
    			buildError("dealData", "该批次下保单【印刷号："+tLCBriGrpImportDetailSchema.getBussNo()+"】未导入成功，不能执行确认操作。");
    			tLCBriGrpImportDetailSchema.setDealRemark(mErrors.getLastError());
    			MMap tempMMap = new MMap();
    			tempMMap.put(tLCBriGrpImportDetailSchema, SysConst.UPDATE);
    			submit(tempMMap);
    			continue;
    		}
    		if(checkSign(tPrtNo)){//已签单
    			SSRS ContDateSSRS = getSignDates(tPrtNo);
    			if(ContDateSSRS == null || ContDateSSRS.MaxRow != 1){
    				buildError("dealData", "获取该批次下保单【印刷号："+tLCBriGrpImportDetailSchema.getBussNo()+"】保单数据失败。");
    				tLCBriGrpImportDetailSchema.setDealRemark(mErrors.getLastError());
        			MMap tempMMap = new MMap();
        			tempMMap.put(tLCBriGrpImportDetailSchema, SysConst.UPDATE);
        			submit(tempMMap);
        			return false;
    			}
        		tLCBriGrpImportDetailSchema.setGrpContNo(ContDateSSRS.GetText(1, 1));
    			tLCBriGrpImportDetailSchema.setGrpName(ContDateSSRS.GetText(1, 2));
    			tLCBriGrpImportDetailSchema.setOrgancomCode(ContDateSSRS.GetText(1, 3));
    			tLCBriGrpImportDetailSchema.setOtherCertificates(ContDateSSRS.GetText(1, 4));
    			tLCBriGrpImportDetailSchema.setCValiDate(ContDateSSRS.GetText(1, 5));
    			tLCBriGrpImportDetailSchema.setCInValidate(ContDateSSRS.GetText(1, 6));
    			tLCBriGrpImportDetailSchema.setPolApplyDate(ContDateSSRS.GetText(1, 7));
    			tLCBriGrpImportDetailSchema.setSignDate(ContDateSSRS.GetText(1, 8));
    			tLCBriGrpImportDetailSchema.setConfMakeDate(ContDateSSRS.GetText(1, 9));
    			tLCBriGrpImportDetailSchema.setPrem(ContDateSSRS.GetText(1, 10));
    			tLCBriGrpImportDetailSchema.setPayMode(ContDateSSRS.GetText(1, 11));
    			tLCBriGrpImportDetailSchema.setDealFlag("01");//将状态变为已承保
    			tLCBriGrpImportDetailSchema.setConfState("02");//将状态变为已确认
    			tLCBriGrpImportDetailSchema.setDealRemark(Confirm_Succ);
    			tLCBriGrpImportDetailSchema.setConfDate(PubFun.getCurrentDate());
    		}else{//未签单
    			String tSignSql = "select paymode,bankcode from lcgrpcont where prtno = '"+tPrtNo+"'";
    	    	SSRS tSSRS = new ExeSQL().execSQL(tSignSql);
    	    	if(tSSRS == null || tSSRS.MaxRow<=0){
    	    		buildError("dealData", "获取该批次下保单【印刷号："+tLCBriGrpImportDetailSchema.getBussNo()+"】缴费方式及银行账号失败。");
    	    		tLCBriGrpImportDetailSchema.setDealRemark(mErrors.getLastError());
        			MMap tempMMap = new MMap();
        			tempMMap.put(tLCBriGrpImportDetailSchema, SysConst.UPDATE);
        			submit(tempMMap);
        			return false;
    	    	}
    	    	String tPayMode = tSSRS.GetText(1, 1);
    	    	String tBankCode = tSSRS.GetText(1, 2);
    			if(!checkDetails(tLCBriGrpImportDetailSchema,tPayMode)){//不能确认
        			tLCBriGrpImportDetailSchema.setDealRemark(mErrors.getLastError());
        			AllFlag = false;
        		}else{
        			if("4".equals(tPayMode)){//银行转账
        				if(!checkTempfee(tLCBriGrpImportDetailSchema.getBussNo())){//无暂收数据
        					tLCBriGrpImportDetailSchema.setDealRemark("财务已缴费成功，暂未签单，请稍后再试!");
//        					tLCBriGrpImportDetailSchema.setConfState("02");//将状态变为已确认
//        					tLCBriGrpImportDetailSchema.setConfDate(PubFun.getCurrentDate());
        					AllFlag = false;
        				}else{
	        				String tBankSuccFlag = getBankSuccFlag(tPrtNo);//获取该单回盘状态。
	        				String tAgentGetSuccFlag = getAgentGetSuccFlag(tBankCode);
	        				if(tAgentGetSuccFlag.equals(tBankSuccFlag+";")){//回盘完成并成功，但未签单，不可确认。经确认财务定义会多个";"号。
	        					tLCBriGrpImportDetailSchema.setDealRemark("财务已缴费成功，暂未签单，请稍后再试。");
	        					AllFlag = false;
	        				}else{
	        					String tBankError = getBankError(tBankCode,tBankSuccFlag);
	        					tLCBriGrpImportDetailSchema.setDealRemark(tBankError);
	        					tLCBriGrpImportDetailSchema.setConfState("02");//将状态变为已确认
	        					tLCBriGrpImportDetailSchema.setConfDate(PubFun.getCurrentDate());
	        				}
        				}
        			}else{//现金
        				if(!checkTempfee(tLCBriGrpImportDetailSchema.getBussNo())){//无暂收数据
        					tLCBriGrpImportDetailSchema.setDealRemark("未缴费!");
        					tLCBriGrpImportDetailSchema.setConfState("02");//将状态变为已确认
        					tLCBriGrpImportDetailSchema.setConfDate(PubFun.getCurrentDate());
        				}else{
        					tLCBriGrpImportDetailSchema.setDealRemark("财务已缴费成功，暂未签单，请稍后再试。");
        					AllFlag = false;
        				}
        			}
        		}
    			
    			SSRS ContDateSSRS = getContDates(tPrtNo);
    			if(ContDateSSRS == null || ContDateSSRS.MaxRow != 1){
    				buildError("dealData", "获取该批次下保单【印刷号："+tLCBriGrpImportDetailSchema.getBussNo()+"】保单数据失败。");
    				tLCBriGrpImportDetailSchema.setDealRemark(mErrors.getLastError());
        			MMap tempMMap = new MMap();
        			tempMMap.put(tLCBriGrpImportDetailSchema, SysConst.UPDATE);
        			submit(tempMMap);
        			return false;
    			}
    			tLCBriGrpImportDetailSchema.setGrpName(ContDateSSRS.GetText(1, 1));
    			tLCBriGrpImportDetailSchema.setOrgancomCode(ContDateSSRS.GetText(1, 2));
    			tLCBriGrpImportDetailSchema.setOtherCertificates(ContDateSSRS.GetText(1, 3));
    			tLCBriGrpImportDetailSchema.setCValiDate(ContDateSSRS.GetText(1, 4));
    			tLCBriGrpImportDetailSchema.setCInValidate(ContDateSSRS.GetText(1, 5));
    			tLCBriGrpImportDetailSchema.setPolApplyDate(ContDateSSRS.GetText(1, 6));
    			tLCBriGrpImportDetailSchema.setPrem(ContDateSSRS.GetText(1, 7));
    			tLCBriGrpImportDetailSchema.setPayMode(ContDateSSRS.GetText(1, 8));
    			
    			tLCBriGrpImportDetailSchema.setDealFlag("00");//未承保
    		}
    		
    		tLCBriGrpImportDetailSchema.setModifyDate(PubFun.getCurrentDate());
			tLCBriGrpImportDetailSchema.setModifyTime(PubFun.getCurrentTime());
    		tDetailMMap.put(tLCBriGrpImportDetailSchema, SysConst.DELETE_AND_INSERT);
    		submit(tDetailMMap);
    	}
        if(AllFlag){
        	MMap BatchMMap = new MMap();
        	mLCBriGrpImportBatchInfoSchema.setOperator(mGlobalInput.Operator);
        	mLCBriGrpImportBatchInfoSchema.setModifyDate(PubFun.getCurrentDate());
        	mLCBriGrpImportBatchInfoSchema.setModifyTime(PubFun.getCurrentTime());
        	mLCBriGrpImportBatchInfoSchema.setBatchConfState("02");
        	mLCBriGrpImportBatchInfoSchema.setBatchConfDate(PubFun.getCurrentDate());
        	BatchMMap.put(mLCBriGrpImportBatchInfoSchema, SysConst.DELETE_AND_INSERT);
        	submit(BatchMMap);
        }
    	
    	return AllFlag;
    }
    
    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriGrpTempFeeFromBank";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public MMap getMMap(){
    	return mMap;
    }
    
    public CErrors getCErrors(){
    	return mErrors;
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap aMMap)
    {
        VData data = new VData();
        data.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
    private boolean checkBatchInfo(){
    	LCBriGrpImportBatchInfoDB tLCBriGrpImportBatchInfoDB = new LCBriGrpImportBatchInfoDB();
    	tLCBriGrpImportBatchInfoDB.setBatchNo(mBatchNo);
    	LCBriGrpImportBatchInfoSet tLCBriGrpImportBatchInfoSet = new LCBriGrpImportBatchInfoSet();
    	tLCBriGrpImportBatchInfoSet = tLCBriGrpImportBatchInfoDB.query();
    	if(tLCBriGrpImportBatchInfoSet == null || tLCBriGrpImportBatchInfoSet.size() != 1){
    		buildError("checkBatchInfo", "获取批次信息失败。");
            return false;
    	}
    	mLCBriGrpImportBatchInfoSchema = tLCBriGrpImportBatchInfoSet.get(1);
    	if(!"02".equals(mLCBriGrpImportBatchInfoSchema.getImportState())){
    		buildError("checkBatchInfo", "该批次导入未成功！。");
            return false;
    	}
    	return true;
    }
    /**
     * 校验明细数据
     * @param LCBriGrpImportDetailSchema
     * @return boolean
     */
    private boolean checkDetails(LCBriGrpImportDetailSchema aLCBriGrpImportDetailSchema,String tPayMode){
    	
    	String tPrtNo = aLCBriGrpImportDetailSchema.getBussNo();
    	if(!checkGrpContInfo(tPrtNo)){//校验数据是否完整
			return false;
		}
		if("4".equals(tPayMode)){//银行转账，需校验是否发盘，是否在途
			
			if(!checkBankOnTheWay(tPrtNo)){
				buildError("dealData", "该批次下保单【印刷号："+tPrtNo+"】处于银行在途状态，该单确认未成功。");
				return false;
			}
			if(!checkBankOutFile(tPrtNo)){
				buildError("dealData", "该批次下保单【印刷号："+tPrtNo+"】未发盘，该单确认未成功。");
				return false;
			}
		}
		
		return true;
    }
    /**
     * 校验是否银行在途,当银行在途时，不可确认
     * @param LCBriGrpImportDetailSchema
     * @return boolean
     */
    private boolean checkBankOnTheWay(String aPrtNo){
    	String tSql = "select bankonthewayflag from ljspay where otherno = '"+aPrtNo+"'";
    	String tBankFlag = new ExeSQL().getOneValue(tSql);
    	if("1".equals(tBankFlag)){
			return false;
    	}
    	return true;
    }
    /**
     * 校验是否银行未发盘
     * @param LCBriGrpImportDetailSchema
     * @param prtno  财务中 polno存的prtno
     * @return boolean
     */
    private boolean checkBankOutFile(String aPrtNo){
    	String tSql = "select 1 from lyreturnfrombankb where polno = '"+aPrtNo+"'";
    	String tBankFlag = new ExeSQL().getOneValue(tSql);
    	if(!"1".equals(tBankFlag)){
			return false;
    	}
    	return true;
    }
    /**
     * 校验是否存在银行数据
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkTempfee(String aPrtNo){
    	
    	String tGrpContNoSql = "select grpcontno from lcgrpcont where prtno = '"+aPrtNo+"' ";
    	String tGrpContNo = new ExeSQL().getOneValue(tGrpContNoSql);
    	
    	String tSql = "select 1 from ljtempfee where otherno = '"+aPrtNo+"' and confdate is null "
    	            + "union all "
    	            + "select 1 from ljtempfee where otherno = '"+tGrpContNo+"' and othernotype = '7'";
    	String tBankFlag = new ExeSQL().getOneValue(tSql);
    	if(!"1".equals(tBankFlag)){
			return false;
    	}
    	return true;
    }
    /**
     * 校验保单数据是否完整
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkGrpContInfo(String aPrtNo){
    	String tSql = "select uwflag from lcgrpcont where prtno = '"+aPrtNo+"'";
    	String uwflag = new ExeSQL().getOneValue(tSql);
    	if("a".equals(uwflag)){
    		buildError("checkGrpContInfo", "该批次下保单【印刷号："+aPrtNo+"】已撤单。");
			return false;
    	}
    	if(!"9".equals(uwflag)){
    		String tBSql = "select 1 from lbgrpcont where prtno = '"+aPrtNo+"'";
    		String tTB = new ExeSQL().getOneValue(tBSql);//查看是否退保
    		if(!"1".equals(tTB)){
    			buildError("checkGrpContInfo", "该批次下保单【印刷号："+aPrtNo+"】数据异常。");
    			return false;
    		}else{
    			buildError("checkGrpContInfo", "该批次下保单【印刷号："+aPrtNo+"】已退保。");
    			return false;
    		}
    		
    	}
    	return true;
    }
    
    private String getBankSuccFlag(String aPrtNo){
    	String tSql = "select BankSuccFlag from lyreturnfrombankb where polno = '"+aPrtNo+"' order by serialno desc";
    	String tBankSuccFlag = new ExeSQL().getOneValue(tSql);
    	return tBankSuccFlag;
    }
    
    private boolean checkSign(String aPrtNo){
    	String tSignSql = "select appflag,signdate from lcgrpcont where prtno = '"+aPrtNo+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(tSignSql);
    	if(tSSRS != null && tSSRS.MaxRow == 1){
    		if("1".equals(tSSRS.GetText(1, 1)) && tSSRS.GetText(1, 2) != null && !"".equals(tSSRS.GetText(1, 2))){//已签单
    			return true;
    		}
    	}
    	return false;
    }
    
    
    private String getAgentGetSuccFlag(String aBankCode){
    	String tAgentGetSuccFlagSql = "select AgentGetSuccFlag from LDBank where BankCode = '"+aBankCode+"' ";
    	String tAgentGetSuccFlag = new ExeSQL().getOneValue(tAgentGetSuccFlagSql);
    	return tAgentGetSuccFlag;
    }
    
    private String getBankError(String aBankCode,String aBankSuccFlag){
    	String tBankErrorSql = "select codename from LDCode1 where CodeType = 'bankerror' and Code = '"+aBankCode+"' and code1 = '"+aBankSuccFlag+"'";
    	String tBankError = new ExeSQL().getOneValue(tBankErrorSql);
    	return tBankError;
    }
    //签单数据
    private SSRS getSignDates(String aPrtNo){
    	StringBuffer tSqlBuffer = new  StringBuffer();
    	tSqlBuffer.append("select lgc.grpcontno,lga.name,lga.OrganComCode,lga.othercertificates, ");
    	tSqlBuffer.append("lgc.cvalidate,lgc.cinvalidate,lgc.polapplydate,lgc.signdate,lja.confdate,lgc.prem,lgc.paymode ");
    	tSqlBuffer.append("from lcgrpcont lgc ");
    	tSqlBuffer.append("inner join lcgrpappnt lga ");
    	tSqlBuffer.append("on lgc.appntno = lga.customerno ");
    	tSqlBuffer.append("and lgc.addressno = lga.addressno ");
    	tSqlBuffer.append("inner join ljapay lja ");
    	tSqlBuffer.append("on lja.incomeno = lgc.grpcontno ");
    	tSqlBuffer.append("and duefeetype = '0' ");
    	tSqlBuffer.append("where lgc.prtno = '"+aPrtNo+"' ");
		SSRS tSSRS = new ExeSQL().execSQL(tSqlBuffer.toString());
		return tSSRS;
    }
    //未签单数据
    private SSRS getContDates(String aPrtNo){
    	StringBuffer tSqlBuffer = new  StringBuffer();
    	tSqlBuffer.append("select lga.name,lga.OrganComCode,lga.othercertificates, ");
    	tSqlBuffer.append("lgc.cvalidate,lgc.cinvalidate,lgc.polapplydate,lgc.prem,lgc.paymode ");
    	tSqlBuffer.append("from lcgrpcont lgc ");
    	tSqlBuffer.append("inner join lcgrpappnt lga ");
    	tSqlBuffer.append("on lgc.appntno = lga.customerno ");
    	tSqlBuffer.append("and lgc.addressno = lga.addressno ");
    	tSqlBuffer.append("where lgc.prtno = '"+aPrtNo+"' ");
		SSRS tSSRS = new ExeSQL().execSQL(tSqlBuffer.toString());
		return tSSRS;
    }
    
    public static void main(String[] args){
    	BriGrpConfirmBL tBriGrpConfirmBL = new BriGrpConfirmBL();
    	VData tVData = new VData();
    	
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.Operator = "001";
    	
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("BatchNo", "");
    	
    	tVData.add(tGlobalInput);
    	tVData.add(tTransferData);
    	
    	tBriGrpConfirmBL.submitData(tVData, "test");
    }
}
