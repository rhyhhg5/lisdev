/**
 * 2011-3-21
 */
package com.sinosoft.lis.wiitb;

import java.math.BigDecimal;

import com.sinosoft.lis.db.LCContPlanDB;
import com.sinosoft.lis.db.LCContPlanDutyParamDB;
import com.sinosoft.lis.db.LCContPlanRiskDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LDRiskWrapDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDRiskWrapSchema;
import com.sinosoft.lis.schema.LMRiskAppSchema;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCContPlanSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class BriGrpWrapPlanBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mPrtNo = null;

    private String mGrpContNo = null;

    private LCRiskDutyWrapSet mInpWrapDutyInfos = null;

    private LCGrpContSchema mGrpContInfo = null;

    private LDRiskWrapSet mRiskWrapDes = null;

    private LCGrpPolSet mGrpPolInfo = new LCGrpPolSet();

    private LCContPlanDutyParamSet mGrpPlanDutyParams = new LCContPlanDutyParamSet();

    private LCContPlanSet mGrpPlanInfo = new LCContPlanSet();

    private LCContPlanRiskSet mGrpPlanRiskInfo = new LCContPlanRiskSet();

    public BriGrpWrapPlanBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        if (mPrtNo == null || mPrtNo.equals(""))
        {
            buildError("getInputData", "未获取到保单印刷号。");
            return false;
        }

        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        if (mGrpContNo == null || mGrpContNo.equals(""))
        {
            buildError("getInputData", "未获取到保单合同号。");
            return false;
        }

        mInpWrapDutyInfos = (LCRiskDutyWrapSet) cInputData.getObjectByObjectName("LCRiskDutyWrapSet", 0);
        if (mInpWrapDutyInfos == null || mInpWrapDutyInfos.size() == 0)
        {
            buildError("getInputData", "未获取到保单保障信息。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Create".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = createGrpWrapInfo();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;
        }

        return true;
    }

    private MMap createGrpWrapInfo()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        // 获取保单信息
        mGrpContInfo = loadGrpPolicy();
        if (mGrpContInfo == null)
        {
            return null;
        }
        // --------------------

        // 校验是否信息可以变更
        if (!chkChgKeyDatas(mGrpContInfo))
        {
            return null;
        }
        // --------------------

        // 处理已存在保单保障信息
        String tGrpContNo = mGrpContInfo.getGrpContNo();
        tTmpMap = cleanGrpPlanInfo(tGrpContNo);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        // 处理团体保障计划信息。
        if (!dealGrpPlanInfo())
        {
            return null;
        }
        // ---------------------

        // 同步数据
        tMMap.put(mGrpPolInfo, SysConst.DELETE_AND_INSERT);
        tMMap.put(mGrpPlanInfo, SysConst.DELETE_AND_INSERT);
        tMMap.put(mGrpPlanRiskInfo, SysConst.DELETE_AND_INSERT);
        tMMap.put(mGrpPlanDutyParams, SysConst.DELETE_AND_INSERT);
        // --------------------

        return tMMap;
    }

    private MMap cleanGrpPlanInfo(String cGrpContNo)
    {
        MMap tMMap = new MMap();
        String tStrSql = null;

        // 团体险种信息 LCGrpPol
        tStrSql = "select * from LCGrpPol where GrpContNo = '" + cGrpContNo + "'";
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        LCGrpPolSet tGrpPolInfos = tLCGrpPolDB.executeQuery(tStrSql);
        if (tGrpPolInfos != null && tGrpPolInfos.size() > 0)
        {
            tMMap.put(tGrpPolInfos, SysConst.DELETE);
        }
        tStrSql = null;
        // --------------------

        // 保障计划主表 LCContPlan
        tStrSql = "select * from LCContPlan where GrpContNo = '" + cGrpContNo + "'";
        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        LCContPlanSet tGrpPlanInfos = tLCContPlanDB.executeQuery(tStrSql);
        if (tGrpPlanInfos != null && tGrpPlanInfos.size() > 0)
        {
            tMMap.put(tGrpPlanInfos, SysConst.DELETE);
        }
        tStrSql = null;
        // --------------------

        // 保障计划险种表 LCContPlanRisk
        tStrSql = "select * from LCContPlanRisk where GrpContNo = '" + cGrpContNo + "'";
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
        LCContPlanRiskSet tGrpPlanRiskInfos = tLCContPlanRiskDB.executeQuery(tStrSql);
        if (tGrpPlanRiskInfos != null && tGrpPlanRiskInfos.size() > 0)
        {
            tMMap.put(tGrpPlanRiskInfos, SysConst.DELETE);
        }
        tStrSql = null;
        // --------------------

        // 保障计划责任要素表 LCContPlanDutyParam
        tStrSql = "select * from LCContPlanDutyParam where GrpContNo = '" + cGrpContNo + "'";
        LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
        LCContPlanDutyParamSet tGrpPlanDutyParamInfos = tLCContPlanDutyParamDB.executeQuery(tStrSql);
        if (tGrpPlanDutyParamInfos != null && tGrpPlanDutyParamInfos.size() > 0)
        {
            tMMap.put(tGrpPlanDutyParamInfos, SysConst.DELETE);
        }
        tStrSql = null;
        // --------------------

        return tMMap;
    }

    private LCGrpContSchema loadGrpPolicy()
    {
        String tStrSql = "select * from LCGrpCont where GrpContNo = '" + mGrpContNo + "' and PrtNo = '" + mPrtNo + "'";
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tGrpContInfoSet = tLCGrpContDB.executeQuery(tStrSql);
        if (tGrpContInfoSet == null || tGrpContInfoSet.size() > 1)
        {
            buildError("loadGrpPolicy", "保单信息不存在。");
            return null;
        }
        return tGrpContInfoSet.get(1);
    }

    private boolean chkChgKeyDatas(LCGrpContSchema cGrpContInfo)
    {
        String tGrpContNo = cGrpContInfo.getGrpContNo();

        String tStrSql = "select 1 from LCCont where GrpContNo = '" + tGrpContNo + "'";
        String tResult = new ExeSQL().getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            buildError("chkChgValidateDatas", "已存在被保人，不能调整套餐信息，如需调整，请先删除被保人后，重试修改操作。");
            return false;
        }

        return true;
    }

    /**
     * 处理套餐关联险种信息
     * @return
     */
    private boolean dealGrpPlanInfo()
    {
        // 获取对应产品险种信息。 
        if (!loadRiskInfo())
        {
            return false;
        }
        // -----------------------------

        // 处理团单险种信息。
        if (!dealGrpPolInfo())
        {
            return false;
        }
        // -----------------------------

        return true;
    }

    private boolean loadRiskInfo()
    {
        String tStrSql = null;

        // 根据单证类别获取套餐险种名称。
        String tWrapCode = getWrapCode(mInpWrapDutyInfos);
        // -----------------------------

        // 获取套餐险种信息。
        tStrSql = null;
        tStrSql = " select * from LDRiskWrap where 1 = 1 " + " and RiskWrapCode = '" + tWrapCode + "' "
                + " order by RiskWrapCode, RiskCode ";
        mRiskWrapDes = new LDRiskWrapDB().executeQuery(tStrSql);
        tStrSql = null;
        if (mRiskWrapDes == null || mRiskWrapDes.size() == 0)
        {
            String tStrErr = "获取套餐险种信息失败。";
            buildError("loadRiskInfoByCertifyCode", tStrErr);
            return false;
        }
        // -----------------------------

        return true;
    }

    private String getWrapCode(LCRiskDutyWrapSet cInpWrapDutyInfos)
    {
        String tWrapCode = null;
        int tWrapCount = 0;

        for (int i = 1; i <= cInpWrapDutyInfos.size(); i++)
        {
            LCRiskDutyWrapSchema tWrapDutyParamInfo = cInpWrapDutyInfos.get(i);
            String tTmpWrapCode = tWrapDutyParamInfo.getRiskWrapCode();
            if (tTmpWrapCode == null)
            {
                buildError("getWrapCode", "套餐代码获取失败。");
                return null;
            }
            if (!tTmpWrapCode.equals(tWrapCode))
            {
                tWrapCode = tTmpWrapCode;
                tWrapCount++;
            }
        }

        if (tWrapCount > 1)
        {
            buildError("getWrapCode", "套餐代码不唯一。");
            return null;
        }

        return tWrapCode;
    }

    private boolean dealGrpPolInfo()
    {
        String tDefContPlanCode = "A";
        String tDefContPlanName = "全体人员";

        for (int i = 1; i <= mRiskWrapDes.size(); i++)
        {
            // 
            LDRiskWrapSchema tLDRiskWrapSchema = null;
            tLDRiskWrapSchema = mRiskWrapDes.get(i);

            // 获取险种描述信息。
            LMRiskAppSchema tLMRiskApp = getRiskAppDes(tLDRiskWrapSchema.getRiskCode());
            if (tLMRiskApp == null)
            {
                String tStrErr = "获取套餐中险种描述信息失败。";
                buildError("dealGrpPolInfo", tStrErr);
                return false;
            }
            // ----------------------------

            String tLimit = null;
            String tPolSeqNo = null;

            try
            {
                tLimit = PubFun.getNoLimit(mGrpContInfo.getManageCom());
                tPolSeqNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);

                if (tPolSeqNo == null || tPolSeqNo.equals(""))
                {
                    String tStrErr = "团体险种流水号生成失败。";
                    buildError("dealGrpPolInfo", tStrErr);
                    return false;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                String tStrErr = "团体险种流水号生成失败。";
                buildError("dealGrpPolInfo", tStrErr);
                return false;
            }

            LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();

            tLCGrpPolSchema.setRiskCode(tLMRiskApp.getRiskCode());
            tLCGrpPolSchema.setComFeeRate(tLMRiskApp.getAppInterest());
            tLCGrpPolSchema.setBranchFeeRate(tLMRiskApp.getAppPremRate());

            tLCGrpPolSchema.setGrpPolNo(tPolSeqNo);
            tLCGrpPolSchema.setGrpProposalNo(tPolSeqNo);

            tLCGrpPolSchema.setGrpContNo(mGrpContInfo.getGrpContNo());
            tLCGrpPolSchema.setPrtNo(mGrpContInfo.getPrtNo());

            tLCGrpPolSchema.setManageCom(mGrpContInfo.getManageCom());
            tLCGrpPolSchema.setCValiDate(mGrpContInfo.getCValiDate());

            String tRiskWrapFlag = "Y";
            tLCGrpPolSchema.setRiskWrapFlag(tRiskWrapFlag);

            tLCGrpPolSchema.setPayMode(mGrpContInfo.getPayMode());
            tLCGrpPolSchema.setPayIntv(mGrpContInfo.getPayIntv());

            tLCGrpPolSchema.setSaleChnl(mGrpContInfo.getSaleChnl());
            tLCGrpPolSchema.setAgentCom(mGrpContInfo.getAgentCom());
            tLCGrpPolSchema.setAgentType(mGrpContInfo.getAgentType());
            tLCGrpPolSchema.setAgentCode(mGrpContInfo.getAgentCode());
            tLCGrpPolSchema.setAgentGroup(mGrpContInfo.getAgentGroup());

            tLCGrpPolSchema.setCustomerNo(mGrpContInfo.getAppntNo());
            tLCGrpPolSchema.setAddressNo(mGrpContInfo.getAddressNo());
            tLCGrpPolSchema.setGrpName(mGrpContInfo.getGrpName());

            tLCGrpPolSchema.setAppFlag(mGrpContInfo.getAppFlag());
            tLCGrpPolSchema.setState(mGrpContInfo.getState());
            tLCGrpPolSchema.setStateFlag(mGrpContInfo.getStateFlag());

            tLCGrpPolSchema.setUWFlag(mGrpContInfo.getUWFlag());
            tLCGrpPolSchema.setUWOperator(mGrpContInfo.getUWOperator());
            tLCGrpPolSchema.setUWDate(mGrpContInfo.getUWDate());
            tLCGrpPolSchema.setUWTime(mGrpContInfo.getUWTime());

            tLCGrpPolSchema.setApproveFlag(mGrpContInfo.getApproveFlag());
            tLCGrpPolSchema.setApproveCode(mGrpContInfo.getApproveCode());
            tLCGrpPolSchema.setApproveDate(mGrpContInfo.getApproveDate());
            tLCGrpPolSchema.setApproveTime(mGrpContInfo.getApproveTime());

            tLCGrpPolSchema.setOperator(mGlobalInput.Operator);
            tLCGrpPolSchema.setMakeDate(PubFun.getCurrentDate());
            tLCGrpPolSchema.setMakeTime(PubFun.getCurrentTime());
            tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
            tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
            // -------------------------------

            mGrpPolInfo.add(tLCGrpPolSchema);
            // -------------------------------

            // 
            LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();

            tLCContPlanRiskSchema.setGrpContNo(mGrpContInfo.getGrpContNo());
            tLCContPlanRiskSchema.setProposalGrpContNo(mGrpContInfo.getProposalGrpContNo());

            tLCContPlanRiskSchema.setRiskCode(tLMRiskApp.getRiskCode());
            tLCContPlanRiskSchema.setMainRiskCode(tLDRiskWrapSchema.getMainRiskCode());
            tLCContPlanRiskSchema.setMainRiskVersion(tLMRiskApp.getRiskVer());
            tLCContPlanRiskSchema.setRiskVersion(tLMRiskApp.getRiskVer());

            tLCContPlanRiskSchema.setContPlanCode(tDefContPlanCode);
            tLCContPlanRiskSchema.setContPlanName(tDefContPlanName);

            tLCContPlanRiskSchema.setPlanType("0");

            tLCContPlanRiskSchema.setRiskWrapFlag("Y");
            tLCContPlanRiskSchema.setRiskWrapCode(tLDRiskWrapSchema.getRiskWrapCode());

            tLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);
            tLCContPlanRiskSchema.setMakeDate(PubFun.getCurrentDate());
            tLCContPlanRiskSchema.setMakeTime(PubFun.getCurrentTime());
            tLCContPlanRiskSchema.setModifyDate(PubFun.getCurrentDate());
            tLCContPlanRiskSchema.setModifyTime(PubFun.getCurrentTime());

            // --------------------------------

            // 获取套餐险种要素信息。
            LDRiskDutyWrapSet tLDRiskDutyWrapSet = null;

            String tStrSql = " select * from LDRiskDutyWrap where 1 = 1 " + " and RiskWrapCode = '"
                    + tLDRiskWrapSchema.getRiskWrapCode() + "' " + " and RiskCode = '"
                    + tLDRiskWrapSchema.getRiskCode() + "' " + " order by RiskWrapCode, RiskCode, DutyCode, CalFactor ";
            tLDRiskDutyWrapSet = new LDRiskDutyWrapDB().executeQuery(tStrSql);
            tStrSql = null;
            if (tLDRiskDutyWrapSet == null || tLDRiskDutyWrapSet.size() == 0)
            {
                String tStrErr = "获取套餐险种要素信息失败。";
                buildError("dealGrpPolInfo", tStrErr);
                return false;
            }

            double tSumRiskPrem = 0;

            for (int j = 1; j <= tLDRiskDutyWrapSet.size(); j++)
            {
                LDRiskDutyWrapSchema tRiskDutyWrap = tLDRiskDutyWrapSet.get(j);
                String tRiskCode = tRiskDutyWrap.getRiskCode();
                String tDutyCode = tRiskDutyWrap.getDutyCode();
                String tCalFactor = tRiskDutyWrap.getCalFactor();
                String tCalFactorType = tRiskDutyWrap.getCalFactorType();
                for (int k = 1; k <= mInpWrapDutyInfos.size(); k++)
                {
                    LCRiskDutyWrapSchema tInpRiskDutyWrap = mInpWrapDutyInfos.get(k);

                    String tInpRiskCode = tInpRiskDutyWrap.getRiskCode();
                    String tInpDutyCode = tInpRiskDutyWrap.getDutyCode();
                    String tInpCalFactor = tInpRiskDutyWrap.getCalFactor();
                    String tInpCalFactorValue = tInpRiskDutyWrap.getCalFactorValue();

                    if (tRiskCode.equals(tInpRiskCode) && tDutyCode.equals(tInpDutyCode))
                    {
                        if ("2".equals(tCalFactorType) && tCalFactor.equals(tInpCalFactor))
                        {
                            tRiskDutyWrap.setCalFactorValue(tInpCalFactorValue);
                            break;
                        }
                    }
                }
            }

            for (int j = 1; j <= tLDRiskDutyWrapSet.size(); j++)
            {
                LDRiskDutyWrapSchema tLDRiskDutyWrapSchema = tLDRiskDutyWrapSet.get(j);

                LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();

                tLCContPlanDutyParamSchema.setGrpContNo(mGrpContInfo.getGrpContNo());
                tLCContPlanDutyParamSchema.setProposalGrpContNo(mGrpContInfo.getProposalGrpContNo());

                tLCContPlanDutyParamSchema.setContPlanCode(tDefContPlanCode);
                tLCContPlanDutyParamSchema.setContPlanName(tDefContPlanName);

                tLCContPlanDutyParamSchema.setRiskCode(tLDRiskDutyWrapSchema.getRiskCode());
                tLCContPlanDutyParamSchema.setDutyCode(tLDRiskDutyWrapSchema.getDutyCode());

                tLCContPlanDutyParamSchema.setRiskVersion(tLMRiskApp.getRiskVer());

                tLCContPlanDutyParamSchema.setGrpPolNo(tPolSeqNo);

                tLCContPlanDutyParamSchema.setMainRiskCode(tLDRiskWrapSchema.getMainRiskCode());
                tLCContPlanDutyParamSchema.setMainRiskVersion(tLMRiskApp.getRiskVer());

                tLCContPlanDutyParamSchema.setCalFactor(tLDRiskDutyWrapSchema.getCalFactor());

                String tCalFactorType = tLDRiskDutyWrapSchema.getCalFactorType();
                tLCContPlanDutyParamSchema.setCalFactorType(tCalFactorType);

                if ("2".equals(tCalFactorType))
                {
                    /** 准备要素 calbase */
                    PubCalculator tCal = prepCalBase(tLDRiskDutyWrapSchema, mInpWrapDutyInfos);

                    /** 计算 */
                    tCal.setCalSql(tLDRiskDutyWrapSchema.getCalSql());
                    System.out.println(tLDRiskDutyWrapSchema.getCalSql());

                    String tResult = tCal.calculate();
                    String tInpCalFactorValue = tLDRiskDutyWrapSchema.getCalFactorValue();
                    if (tResult.equals("0") && !tResult.equals(tInpCalFactorValue))
                    {
                        tLCContPlanDutyParamSchema.setCalFactorValue(tInpCalFactorValue);
                    }
                    else
                    {
                        tLCContPlanDutyParamSchema.setCalFactorValue(tResult);
                    }
                    System.out.println(tResult);
                }
                else
                {
                    tLCContPlanDutyParamSchema.setCalFactorValue(tLDRiskDutyWrapSchema.getCalFactorValue());
                }

                if ("Prem".equals(tLDRiskDutyWrapSchema.getCalFactor()))
                {
                    try
                    {
                        String tResult = tLCContPlanDutyParamSchema.getCalFactorValue();
                        tSumRiskPrem = Arith.add(tSumRiskPrem, new BigDecimal(tResult).doubleValue());
                    }
                    catch (Exception e)
                    {
                        String tStrErr = "套餐要素中保费出现非数值型字符串。";
                        buildError("dealGrpPolInfo", tStrErr);
                        return false;
                    }
                }

                tLCContPlanDutyParamSchema.setPlanType("0");
                tLCContPlanDutyParamSchema.setPayPlanCode("000000");
                tLCContPlanDutyParamSchema.setGetDutyCode("000000");
                tLCContPlanDutyParamSchema.setInsuAccNo("000000");

                mGrpPlanDutyParams.add(tLCContPlanDutyParamSchema);

            }
            // -----------------------------

            int tSumInsuCount = mGrpContInfo.getPeoples3();
            if (tSumInsuCount <= 0)
            {
                buildError("dealGrpPolInfo", "保单被保人数不能小于或等于0。");
                return false;
            }
            tLCContPlanRiskSchema.setRiskPrem(tSumInsuCount * tSumRiskPrem);
            mGrpPlanRiskInfo.add(tLCContPlanRiskSchema);
        }

        // 创建保障计划
        LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();

        tLCContPlanSchema.setGrpContNo(mGrpContInfo.getGrpContNo());
        tLCContPlanSchema.setProposalGrpContNo(mGrpContInfo.getProposalGrpContNo());

        tLCContPlanSchema.setContPlanCode(tDefContPlanCode);
        tLCContPlanSchema.setContPlanName(tDefContPlanName);

        tLCContPlanSchema.setPlanType("0");

        tLCContPlanSchema.setOperator(mGlobalInput.Operator);
        tLCContPlanSchema.setMakeDate(PubFun.getCurrentDate());
        tLCContPlanSchema.setMakeTime(PubFun.getCurrentTime());
        tLCContPlanSchema.setModifyDate(PubFun.getCurrentDate());
        tLCContPlanSchema.setModifyTime(PubFun.getCurrentTime());

        int tSumInsuCount = mGrpContInfo.getPeoples3();
        tLCContPlanSchema.setPeoples2(tSumInsuCount);
        tLCContPlanSchema.setPeoples3(tSumInsuCount);

        mGrpPlanInfo.add(tLCContPlanSchema);
        // ------------------------

        return true;
    }

    /**
     * 装载界面传入的要素信息
     * @param cRiskCode
     * @param cInpWrapDutyInfos
     * @return
     */
    private PubCalculator prepCalBase(LDRiskDutyWrapSchema cWrapDutyParamInfo, LCRiskDutyWrapSet cInpWrapDutyInfos)
    {
        PubCalculator tCalBase = new PubCalculator();

        String tRiskCode = cWrapDutyParamInfo.getRiskCode();
        tCalBase.addBasicFactor("RiskCode", tRiskCode);

        String tDutyCode = cWrapDutyParamInfo.getDutyCode();

        for (int i = 1; i <= cInpWrapDutyInfos.size(); i++)
        {
            LCRiskDutyWrapSchema tTmpWrapDutyInfo = cInpWrapDutyInfos.get(i);
            String tTmpRiskCode = tTmpWrapDutyInfo.getRiskCode();
            String tTmpDutyCode = tTmpWrapDutyInfo.getDutyCode();
            if (!tRiskCode.equals(tTmpRiskCode) || !tDutyCode.equals(tTmpDutyCode))
            {
                continue;
            }

            String tCalFactorType = tTmpWrapDutyInfo.getCalFactorType();
            String tCalFactor = tTmpWrapDutyInfo.getCalFactor();
            String tCalFactorValue = tTmpWrapDutyInfo.getCalFactorValue();
            if ("2".equals(tCalFactorType))
            {
                tCalBase.addBasicFactor(tCalFactor, tCalFactorValue);
            }
        }

        return tCalBase;
    }

    /**
     * 获取险种产品描述。
     * @param cRiskCode
     * @return
     */
    private LMRiskAppSchema getRiskAppDes(String cRiskCode)
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(cRiskCode);
        if (!tLMRiskAppDB.getInfo())
        {
            String tStrErr = "获取套餐中险种描述信息失败。";
            buildError("dealGrpPolInfo", tStrErr);
            return null;
        }
        return tLMRiskAppDB.getSchema();
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + " : " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
}
