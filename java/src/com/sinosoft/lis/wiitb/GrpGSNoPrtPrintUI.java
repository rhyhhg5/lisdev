/**
 * 2012-3-23
 */
package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class GrpGSNoPrtPrintUI
{
    /** �������� */
    public CErrors mErrors = new CErrors();

    public GrpGSNoPrtPrintUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            GrpGSNoPrtPrintBL tBusLogicBL = new GrpGSNoPrtPrintBL();
            if (!tBusLogicBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tBusLogicBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
