/**
 * 2011-8-10
 */
package com.sinosoft.lis.wiitb;

import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.collections.XmlMsgColls;

/**
 * @author LY
 *
 */
public class MsgXmlParse
{
    private final static Logger mLogger = Logger.getLogger(MsgXmlParse.class);

    private String mRow_Flag_Title = new String("Item");

    private StringBuffer mErrMsg = null;

    public IXmlMsgColls deal(Document cInXmlDoc)
    {
        if (cInXmlDoc == null)
        {
            errLog("报文信息为空。");
            return null;
        }

        IXmlMsgColls tMsgCols = new XmlMsgColls();

        Document tXmlDoc = (Document) cInXmlDoc.clone();
        Element tEleRoot = tXmlDoc.getRootElement();

        // 处理消息体，不做阻断，找不到类属于正常情况，仅输出进行提示。
        if (!loadBodyNodes(tEleRoot, tMsgCols))
        {
            errLog("部分节点不存在对应对象信息，或Xsch自动加载失败。");
            return null;
        }
        // --------------------

        return tMsgCols;
    }

    private boolean loadBodyNodes(Element cEleRoot, IXmlMsgColls cMsgInfo)
    {
        List tFieldList = cEleRoot.getChildren();
        for (int idx = 0; idx < tFieldList.size(); idx++)
        {
            Element tTmpElmNode = (Element) tFieldList.get(idx);
            List tTmpLSubNodes = tTmpElmNode.getChildren();
            if (tTmpLSubNodes.size() == 0)
            {
                /// 处理数据子节点 ///

                String tFieldName = tTmpElmNode.getName();
                String tFieldValue = tTmpElmNode.getTextTrim();

                if (!cMsgInfo.setBodyByFlag(tFieldName, tFieldValue))
                {
                    errLog("[" + tFieldName + "]节点在同级中出现多次。");
                    return false;
                }
            }
            else if (tTmpLSubNodes.size() >= 1)
            {
                /// 处理数据集合子节点 ///

                String tFieldName = tTmpElmNode.getName();

                // 循环数据集合节点下要求必须仅含有行标节点
                List tSubElmNodeList = tTmpElmNode.getChildren(mRow_Flag_Title);
                if (tSubElmNodeList.size() == 0)
                {
                    IXmlMsgColls tSubMsgInfo = new XmlMsgColls();
                    if (!loadBodyNodes(tTmpElmNode, tSubMsgInfo))
                    {
                        return false;
                    }

                    if (!cMsgInfo.setBodyByFlag(tFieldName, tSubMsgInfo))
                    {
                        return false;
                    }
                }
                else
                {
                    List tTmpSubFiled = tTmpElmNode.getChildren();
                    for (int sIdx = 0; sIdx < tTmpSubFiled.size(); sIdx++)
                    {
                        Element tTmpChkElm = (Element) tTmpSubFiled.get(sIdx);

                        String tTmpChkFiledName = tTmpChkElm.getName();

                        if (!mRow_Flag_Title.equals(tTmpChkFiledName))
                        {
                            errLog("[" + tTmpElmNode.getName() + "]数据集合节点不符合约定格式规范。");
                            return false;
                        }
                    }

                    for (int sIdx = 0; sIdx < tSubElmNodeList.size(); sIdx++)
                    {
                        Element tTmpSubElmNode = (Element) tSubElmNodeList.get(sIdx);

                        IXmlMsgColls tSubMsgInfo = new XmlMsgColls();
                        if (!loadBodyNodes(tTmpSubElmNode, tSubMsgInfo))
                        {
                            return false;
                        }

                        if (!cMsgInfo.setBodyByFlag(tFieldName, tSubMsgInfo))
                        {
                            return false;
                        }
                    }
                }
                // --------------------
            }
        }

        return true;
    }

    public String getErrInfo()
    {
        if (mErrMsg == null)
            return null;

        return mErrMsg.toString();
    }

    private void errLog(String cErrInfo)
    {
        mLogger.error("ErrInfo:" + cErrInfo);

        if (mErrMsg == null)
        {
            mErrMsg = new StringBuffer();
            mErrMsg.append(cErrInfo);
        }
        else
        {
            mErrMsg.append("|");
            mErrMsg.append(cErrInfo);
        }

    }
}
