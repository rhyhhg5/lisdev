package com.sinosoft.lis.wiitb;

import java.util.List;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.collections.XmlMsgColls;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;

public class BriGrpCommonCheckBL
{
    private StringBuffer mErrInfo = null;

    /**
     * 报文头结点数据质检。
     * @param mRoot
     */
    public boolean checkMsgHead(IXmlMsgColls mRoot)
    {
        mErrInfo = null;
        boolean flag = true;

        if (mRoot == null)
        {
            String tStrErr = "获取报文MsgHead节点数据为空，请检查上传数据！";
            errLog(tStrErr);
            flag = false;
        }

        //    	校验报文类型
        String tMsgType = mRoot.getTextByFlag("MsgType");
        if (!isNULL(tMsgType))
        {
            String tStrErr = "上传数据中报文类型为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!checkMsgType(tMsgType))
        {
            String tStrErr = "上传报文类型与约定不符，请核实！";
            errLog(tStrErr);
            flag = false;
        }
        //    	校验批次号
        String tBatchNo = mRoot.getTextByFlag("BatchNo");
        if (!isNULL(tBatchNo))
        {
            String tStrErr = "上传数据中批次号为空！";
            errLog(tStrErr);
            flag = false;
        }

        if (!"WIIPI99999".equals(tMsgType))
        {
            if (!checkBatchNo(tBatchNo))
            {
                String tStrErr = "上传数据中批次号不符合约定格式，请核查！";
                errLog(tStrErr);
                flag = false;
            }
            //    	批次是否唯一
            if (!isValidBatchNo(tBatchNo))
            {
                String tStrErr = "已存在该批次数据，请核查！";
                errLog(tStrErr);
                flag = false;
            }
        }

        //    	报文发送日期
        String tSendDate = mRoot.getTextByFlag("SendDate");
        if (!isNULL(tSendDate))
        {
            String tStrErr = "上传数据中报文发送日期为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!PubFun.checkDateForm(tSendDate))
        {
            String tStrErr = "上传数据中报文发送日期格式与约定格式不符，请核查！";
            errLog(tStrErr);
            flag = false;
        }
        //    	报文发送时间
        String tSendTime = mRoot.getTextByFlag("SendTime");
        if (!isNULL(tSendTime))
        {
            String tStrErr = "上传数据中报文发送时间为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	报送单位
        String tBranchCode = mRoot.getTextByFlag("BranchCode");
        if (!isNULL(tBranchCode))
        {
            String tStrErr = "上传数据中报送单位为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	操作员
        String tSendOperator = mRoot.getTextByFlag("SendOperator");
        if (!isNULL(tSendOperator))
        {
            String tStrErr = "上传数据中操作员为空！";
            errLog(tStrErr);
            flag = false;
        }

        return flag;
    }

    /**
     * 报文保单数据质检。
     * @param mRoot
     */
    public boolean checkPolicyInfo(IXmlMsgColls mRoot, String aBatchNo, String aMsgType,String aComcode)
    {
        mErrInfo = null;
        boolean flag = true;

        //    	印刷号
        String tPrtNo = mRoot.getTextByFlag("PrtNo");
        if (!isNULL(tPrtNo))
        {
            String tStrErr = "保单数据的印刷号为空！";
            errLog(tStrErr);
            flag = false;
        }
        if ("WIIPI00001".equals(aMsgType))
        {
            String tBatchNo = getBatchNo(tPrtNo, aBatchNo);//获取该印刷号对应的有效未确认的保单对应的批次号
            if (isNULL(tBatchNo))
            {
                String tStrErr = "印刷号为【" + tPrtNo + "】的保单在批次【" + tBatchNo + "】中未处理完成！";
                errLog(tStrErr);
                flag = false;
            }
        }
        if (checkSign(tPrtNo))
        {//已签单
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单已签单成功，不可再次导入！";
            errLog(tStrErr);
            flag = false;
        }
        if (checkTempfee(tPrtNo))
        {
            String tStrErr = "印刷号为【" + tPrtNo + "】的保单已有财务确认数据，不可再次导入！";
            errLog(tStrErr);
            flag = false;
        }
        //    	管理机构
        String tManageCom = mRoot.getTextByFlag("ManageCom");
        if (!isNULL(tManageCom))
        {
            String tStrErr = "保单数据的管理机构为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	销售渠道
        String tSaleChnl = mRoot.getTextByFlag("SaleChnl");
        if (!isNULL(tSaleChnl))
        {
            String tStrErr = "保单数据的销售渠道为空！";
            errLog(tStrErr);
            flag = false;
        }
        //       代理机构
        String tAgentCom = mRoot.getTextByFlag("AgentCom");
        if (("03".equals(tSaleChnl) || "04".equals(tSaleChnl) || "10".equals(tSaleChnl) || "15".equals(tSaleChnl) || "20".equals(tSaleChnl)) && !isNULL(tAgentCom))
        {
            String tStrErr = "保单数据的代理机构为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (isNULL(tAgentCom) && !checkAgentCom(tAgentCom))
        {
            String tStrErr = "不存在代码为【" + tAgentCom + "】代理机构或该机构已停业！";
            errLog(tStrErr);
            flag = false;
        }

        //        代理人编码  modify bi zjd 集团统一工号修改
        String tAgentCode = new ExeSQL().getOneValue(" select getAgentCode('"+mRoot.getTextByFlag("AgentCode")+"') from dual ");
        if("".equals(tAgentCode) || tAgentCode==null){
        	tAgentCode=mRoot.getTextByFlag("AgentCode");
        }
        if (!isNULL(tAgentCode))
        {
            String tStrErr = "保单数据的销售人员为空！";
            errLog(tStrErr);
            flag = false;
        }
        else
        //      核心是否存在该代理人编码  
        if (!checkAgentCode(tAgentCode))
        {
            String tStrErr = "不存在代码为【" + tAgentCode + "】的业务员或该业务员已离职！";
            errLog(tStrErr);
            flag = false;
        }
        else
        {
            //      校验业务员名称是否与核心一致 
            String tAgentName = mRoot.getTextByFlag("AgentName");
            if (isNULL(tAgentName))
            {
                if (!checkAgentName(tAgentCode, tAgentName))
                {
                    String tStrErr = "业务员姓名与业务员编码不符，请核查！";
                    errLog(tStrErr);
                    flag = false;
                }
            }
        }
        String tSaleChnlCheck = checkSaleChnlInfo(tManageCom,tSaleChnl,tAgentCom,tAgentCode);
        if(!"".equals(tSaleChnlCheck)){
        	String tStrErr = tSaleChnlCheck;
            errLog(tStrErr);
            flag = false;
        }
        //    	市场类型
        String tMarketType = mRoot.getTextByFlag("MarketType");
        if (!isNULL(tMarketType))
        {
            String tStrErr = "保单数据的市场类型为空！";
            errLog(tStrErr);
            flag = false;
        }
        
        String checkMarkettypeSalechnlFlag = checkMarkettypeSalechnl(tMarketType,tSaleChnl);
        if(!"".equals(checkMarkettypeSalechnlFlag)){
        	String tStrErr = checkMarkettypeSalechnlFlag;
            errLog(tStrErr);
            flag = false;
        }
        
        //    	投保单申请日期
        String tPolApplyDate = mRoot.getTextByFlag("PolApplyDate");
        if (!isNULL(tPolApplyDate))
        {
            String tStrErr = "保单数据的投保单申请日期为空！";
            errLog(tStrErr);
            flag = false;
        }
        else if (!PubFun.checkDateForm(tPolApplyDate))
        {
            String tStrErr = "保单数据的投保单申请日期格式与约定格式不符，请核查！";
            errLog(tStrErr);
            flag = false;
        }
        //    	业务经办人
        String tHandlerName = mRoot.getTextByFlag("HandlerName");
        if (!isNULL(tHandlerName))
        {
            String tStrErr = "保单数据的业务经办人为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	初审人员
        String tFirstTrialOperator = mRoot.getTextByFlag("FirstTrialOperator");
        if (!isNULL(tFirstTrialOperator))
        {
            String tStrErr = "保单数据的初审人员为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	缴费方式
        String tPayMode = mRoot.getTextByFlag("PayMode");
        if (!isNULL(tPayMode))
        {
            String tStrErr = "保单数据的缴费方式为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!PubFun.isNumeric(tPayMode))
        {
            String tStrErr = "保单数据的缴费方式不是数字类型，请核查！";
            errLog(tStrErr);
            flag = false;
        }
        if ("4".equals(tPayMode))
        {
            String BankCode = mRoot.getTextByFlag("BankCode");
            if (!isNULL(BankCode))
            {
                String tStrErr = "保单数据的银行代码为空！";
                errLog(tStrErr);
                flag = false;
            }
            String BankAccNo = mRoot.getTextByFlag("BankAccNo");
            if (!isNULL(BankAccNo))
            {
                String tStrErr = "保单数据的银行账户号码为空！";
                errLog(tStrErr);
                flag = false;
            }
            String AccName = mRoot.getTextByFlag("AccName");
            if (!isNULL(AccName))
            {
                String tStrErr = "保单数据的账户名称为空！";
                errLog(tStrErr);
                flag = false;
            }
        }
        //    	缴费频次
        String tPayIntv = mRoot.getTextByFlag("PayIntv");
        if (!isNULL(tPayIntv))
        {
            String tStrErr = "保单数据的缴费频次为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!PubFun.isNumeric(tPayIntv))
        {
            String tStrErr = "保单数据的缴费频次不是数字类型，请核查！";
            errLog(tStrErr);
            flag = false;
        }
        //    	保费合计
        String tPremScope = mRoot.getTextByFlag("PremScope");
        if (!isNULL(tPremScope))
        {
            String tStrErr = "保单数据的保费合计为空！";
            errLog(tStrErr);
            flag = false;
        }

        if (!PubFun.isNumeric(tPremScope))
        {
            String tStrErr = "保单数据的保费合计不是数字类型，请核查！";
            errLog(tStrErr);
            flag = false;
        }
        //    	保险责任生效日期
        boolean dateflag = true;
        String tCValiDate = mRoot.getTextByFlag("CValiDate");
        if (!isNULL(tCValiDate))
        {
            String tStrErr = "保单数据的保险责任生效日期为空！";
            errLog(tStrErr);
            flag = false;
            dateflag = false;
        }
        else if (!PubFun.checkDateForm(tCValiDate))
        {
            String tStrErr = "保单数据的保险责任生效日期格式与约定格式不符，请核查！";
            errLog(tStrErr);
            flag = false;
            dateflag = false;
        }
        //		保险责任终止日期
        String tCInValiDate = mRoot.getTextByFlag("CInValiDate");
        if (!isNULL(tCInValiDate))
        {
            String tStrErr = "保单数据的保险责任终止日期为空！";
            errLog(tStrErr);
            flag = false;
            dateflag = false;
        }
        else if (!PubFun.checkDateForm(tCInValiDate))
        {
            String tStrErr = "保单数据的保险责任终止日期格式与约定格式不符，请核查！";
            errLog(tStrErr);
            flag = false;
            dateflag = false;
        }
        //生效日期不可以小于终止日期
        if (dateflag)
        {
            FDate tFDate = new FDate();
            if (tFDate.getDate(tCValiDate).after(tFDate.getDate(tCInValiDate)))
            {
                String tStrErr = "保单数据的保险责任终止日期不应小于保险责任起始日期，请核查！";
                errLog(tStrErr);
                flag = false;
            }
        }
        //		被保险人数（成员）
        String tPeoples3 = mRoot.getTextByFlag("Peoples3");
        if (!isNULL(tPeoples3))
        {
            String tStrErr = "保单数据的被保险人数（成员）为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!PubFun.isNumeric(tPeoples3))
        {
            String tStrErr = "保单数据的被保险人数（成员）不是数字类型，请核查！";
            errLog(tStrErr);
            flag = false;
        }
        //校验共保信息
        String CoInsuranceFlag=mRoot.getTextByFlag("CoInsuranceFlag");
        if(isNULL(CoInsuranceFlag) && "1".equals(CoInsuranceFlag)){
        	String ok="";
        	String CoAgentCom=mRoot.getTextByFlag("CoAgentCom");
        	String CoAgentComName=mRoot.getTextByFlag("CoAgentComName");
        	String CoAgentCom1=mRoot.getTextByFlag("CoAgentCom1");
        	String CoAgentComName1=mRoot.getTextByFlag("CoAgentComName1");
        	if(!isNULL(CoAgentCom)){
        		String tStrErr = "共保机构代码为空，请核实！";
                errLog(tStrErr);
                flag = false;
                ok="1";
        	}
        	if(!isNULL(CoAgentCom1)){
        		String tStrErr = "共保机构代码1为空，请核实！";
                errLog(tStrErr);
                flag = false;
                ok="1";
        	}
        	if(!isNULL(CoAgentComName)){
        		String tStrErr = "共保机构名称为空，请核实！";
                errLog(tStrErr);
                flag = false;
                ok="1";
        	}
        	if(!isNULL(CoAgentComName1)){
        		String tStrErr = "共保机构名称1为空，请核实！";
                errLog(tStrErr);
                flag = false;
                ok="1";
        	}
        	if(CoAgentCom.equals(CoAgentCom1)){
        		String tStrErr = "两个共保机构为同一共保机构，请核实！";
                errLog(tStrErr);
                flag = false;
                ok="1";
        	}
        	if("".equals(ok) && !checkCoCom(CoAgentCom,CoAgentComName,tManageCom,aComcode)){
        		String tStrErr = "第一个共保机构不存在，请核实！";
                errLog(tStrErr);
                flag = false;
        	}
        	if("".equals(ok) && !checkCoCom(CoAgentCom1,CoAgentComName1,tManageCom,aComcode)){
        		String tStrErr = "第二个共保机构不存在，请核实！";
                errLog(tStrErr);
                flag = false;
        	}
        	String Rate=mRoot.getTextByFlag("Rate");
        	String Rate1=mRoot.getTextByFlag("Rate1");
        	if(isNULL(Rate) && isNULL(Rate1)){
	        	double a=Double.parseDouble(Rate);
	        	double b=Double.parseDouble(Rate1);
	        	if(a<0){
	        		String tStrErr = "共保比例不能小于0，请核实！";
	                errLog(tStrErr);
	                flag = false;
	        	}
	        	if(b<0){
	        		String tStrErr = "共保比例1不能小于0，请核实！";
	                errLog(tStrErr);
	                flag = false;
	        	}
	        	if(a>1){
	        		String tStrErr = "共保比例不能大于1，请核实！";
	                errLog(tStrErr);
	                flag = false;
	        	}
	        	if(b>1){
	        		String tStrErr = "共保比例1不能大于1，请核实！";
	                errLog(tStrErr);
	                flag = false;
	        	}
	        	double SumRate=a+b;
	        	if(SumRate>1.0){
	        		String tStrErr = "共保比例之和不能大于1，请核实！";
	                errLog(tStrErr);
	                flag = false;
	        	}
        	}else{
        		if(!isNULL(Rate)){
	        		String tStrErr = "共保比例为空，请核实！";
	                errLog(tStrErr);
	                flag = false;
        		}
        		if(!isNULL(Rate1)){
        			String tStrErr = "共保比例1为空，请核实！";
	                errLog(tStrErr);
	                flag = false;
        		}
        	}
        }
        //    	投保单位信息
        IXmlMsgColls[] iresGrpAppntInfos = mRoot.getBodyByFlag("GrpAppntInfo");
        if (iresGrpAppntInfos == null)
        {
            String tStrErr = "保单数据的投保单位为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (iresGrpAppntInfos.length != 1)
        {
            String tStrErr = "保单数据的存在多个投保单位，请核查！";
            errLog(tStrErr);
            flag = false;
        }
        IXmlMsgColls iresGrpAppntInfo = iresGrpAppntInfos[0];
        //    	投保单位名称
        String tGrpName = iresGrpAppntInfo.getTextByFlag("GrpName");
        if (!isNULL(tGrpName))
        {
            String tStrErr = "保单数据的投保单位名称为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	税务登记证号码(原来为组织机构代码）
        String tOrgancomCode = iresGrpAppntInfo.getTextByFlag("OrgancomCode");
        if (!isNULL(tOrgancomCode))
        {
            String tStrErr = "保单数据的税务登记证号码为空！";
            errLog(tStrErr);
            flag = false;
        }else{
        	//正则表达式验证税务登记证号码是否为全数字
        	Pattern pattern = Pattern.compile("[A-Za-z0-9]+");   //字母和数字的混合
        	Pattern pattern2 = Pattern.compile("/^[1-9]+[0-9]*]*$/");  //全是数字 (不包含小数) 
        	Pattern pattern3 = Pattern.compile("[A-Za-z]+");  //全是字母 
        	if (pattern.matcher(tOrgancomCode).matches() || pattern2.matcher(tOrgancomCode).matches() || pattern3.matcher(tOrgancomCode).matches()) {
        		if (tOrgancomCode.trim().length() != 15 && tOrgancomCode.trim().length() != 18 && tOrgancomCode.trim().length() != 20) {
        			String tStrErr = "保单数据的税务登记证号码应该为15,18或20位，请核实！"; 
                    errLog(tStrErr);
           		 	flag = false;
        		}
			}else{
				String tStrErr = "保单数据的税务登记证号码应该为数字或字母，请核实！";
	            errLog(tStrErr);
	            flag = false;
			}
        	
        }
        //    	保险证编码/社保登记编号
        String tOtherCertificates = iresGrpAppntInfo.getTextByFlag("OtherCertificates");
        if (!isNULL(tOtherCertificates))
        {
            String tStrErr = "保单数据的保险证编码/社保登记编号为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	投保单位地址
        String tGrpAddress = iresGrpAppntInfo.getTextByFlag("GrpAddress");
        if (!isNULL(tGrpAddress))
        {
            String tStrErr = "保单数据的投保单位地址为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	邮政编码
        String tGrpZipCode = iresGrpAppntInfo.getTextByFlag("GrpZipCode");
        if (!isNULL(tGrpZipCode))
        {
            String tStrErr = "保单数据的邮政编码为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	联系人
        String tLinkMan1 = iresGrpAppntInfo.getTextByFlag("LinkMan1");
        if (!isNULL(tLinkMan1))
        {
            String tStrErr = "保单数据的联系人为空！";
            errLog(tStrErr);
            flag = false;
        }
        //    	联系电话
        String tPhone1 = iresGrpAppntInfo.getTextByFlag("Phone1");
        if (!isNULL(tPhone1))
        {
            String tStrErr = "保单数据的联系电话为空！";
            errLog(tStrErr);
            flag = false;
        }
//        if (!checkPhone(tPhone1)){
//        	String tStrErr = "保单联系电话(移动电话)若为手机号，则必须为11位数字；若为固定电话，则仅允许包含数字、括号和“-”！";
//            errLog(tStrErr);
//            flag = false;
//        }
        //    	投保单位级别
        String tGrpLevel = iresGrpAppntInfo.getTextByFlag("GrpLevel");
        if (!isNULL(tGrpLevel))
        {
            String tStrErr = "保单数据的投保单位级别为空！";
            errLog(tStrErr);
            flag = false;
        }
        if (!PubFun.isNumeric(tGrpLevel))
        {
            String tStrErr = "保单数据的投保单位级别不是数字类型，请核查！";
            errLog(tStrErr);
            flag = false;
        }
//    	投保单位级别
        String tWrapFlag = iresGrpAppntInfo.getTextByFlag("WrapFlag");
        if (tWrapFlag == null || "".equals(tWrapFlag))
        {
            tWrapFlag = tGrpLevel; // 为和之前兼容，增加套餐代码后，如果未找到套餐代码信息则自动用单位级别进行查找
        }
        String checkRiskSalechnlFlag = checkRiskSalechnl(tManageCom,tWrapFlag,tSaleChnl);
        if(!"".equals(checkRiskSalechnlFlag)){
        	String tStrErr = checkRiskSalechnlFlag;
            errLog(tStrErr);
            flag = false;
        }
        return flag;
    }

    //	获取节点数量
    //    private static int arrLnegth(IXmlMsgColls[] aIXmlMsgColls)
    //    {
    //        return aIXmlMsgColls.length;
    //    }

    //	校验节点值是否为空
    private static boolean isNULL(String aFlagValue)
    {
        if (aFlagValue == null || "".equals(aFlagValue))
        {
            return false;
        }
        return true;
    }

    //	校验报文类型
    private static boolean checkMsgType(String aMsgType)
    {
        if (!"WIIPI00001".equals(aMsgType) && !"WIIPI00002".equals(aMsgType) && !"WIIPI99999".equals(aMsgType))
        {
            return false;
        }
        return true;
    }

    //	校验批次号格式
    private static boolean checkBatchNo(String aBatchNo)
    {

        //		String tPatMsgHeadFormat = "(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))0229)";
        String tPatMsgHeadFormat = "LISWIIDLGSSys([1-3])\\d{3}(0?[1-9]{1}|1[0-2])(0?[1-9]{1}|[1-2][0-9]|3[0-1])(([0-1]?[0-9]|2[0-3])[0-5]?[0-9][0-5]?[0-9])\\d{5}";
        //    	String tPatMsgHeadFormat ="LISWIIDLGSSys(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))0229)(0?[1-9]{1}|1[0-2])(0?[1-9]{1}|[1-2][0-9]|3[0-1])(([0-1]?[0-9]|2[0-3])[0-5]?[0-9][0-5]?[0-9])\\d{5}";
        StringBuffer tPatMsgFormat = new StringBuffer();
        tPatMsgFormat.append("^");
        tPatMsgFormat.append(tPatMsgHeadFormat);
        tPatMsgFormat.append("$");

        Pattern pattern = Pattern.compile(tPatMsgFormat.toString());

        Matcher tMatcher = null;

        // 校验消息
        tMatcher = pattern.matcher(aBatchNo);
        if (!tMatcher.matches())
        {
            return false;
        }
        return true;
    }

    // 校验批次是否已经存在
    private static boolean isValidBatchNo(String aBatchNo)
    {
        String sql = "select count(*) from LCBriGrpImportBatchInfo where BatchNo='" + aBatchNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        int count = Integer.parseInt(tExeSQL.getOneValue(sql));
        if (count > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // 校验保单是否处理完成
    private static String getBatchNo(String aPrtNo, String aBatchNo)
    {
        String tSql = "select batchno from lcbrigrpimportdetail where bussno = '" + aPrtNo
                + "' and enableflag  != '02' and confstate != '02' and batchno !='" + aBatchNo + "'";//有效未确认的保单
        ExeSQL tExeSQL = new ExeSQL();
        String tBatchNo = tExeSQL.getOneValue(tSql);
        return tBatchNo;
    }

    /**
     * 生成错误信息。
     * @param cErrInfo
     */
    private void errLog(String cErrInfo)
    {
        System.out.println("工伤险数据质检错误：" + cErrInfo);
        if (mErrInfo == null)
        {
            mErrInfo = new StringBuffer();
            mErrInfo.append(cErrInfo);
        }
        else
        {
            mErrInfo.append("&");
            mErrInfo.append(cErrInfo);
        }
    }

    //是否已有签单数据
    private boolean checkSign(String aPrtNo)
    {
        String tSignSql = "select appflag,signdate from lcgrpcont where prtno = '" + aPrtNo + "'";
        SSRS tSSRS = new ExeSQL().execSQL(tSignSql);
        if (tSSRS != null && tSSRS.MaxRow == 1)
        {
            if ("1".equals(tSSRS.GetText(1, 1)) && tSSRS.GetText(1, 2) != null && !"".equals(tSSRS.GetText(1, 2)))
            {//已签单
                return true;
            }
        }
        return false;
    }

    /**
     * 校验是否存在银行数据
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkTempfee(String aPrtNo)
    {
        String tSql = "select 1 from ljtempfee where otherno = '" + aPrtNo
                + "' and enteraccdate is not null and confdate is null ";
        String tBankFlag = new ExeSQL().getOneValue(tSql);
        if (!"1".equals(tBankFlag))
        {
            return false;
        }
        return true;
    }

    /**
     * 校验代理机构是否正确
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkAgentCom(String aAgentCom)
    {
        String tSql = "select 1 from lacom where agentcom = '" + aAgentCom + "' and endflag = 'N' ";
        String tAgentComFlag = new ExeSQL().getOneValue(tSql);
        if (!"1".equals(tAgentComFlag))
        {
            return false;
        }
        return true;
    }

    /**
     * 校验代理人编码是否正确
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkAgentCode(String aAgentCode)
    {
        String tSql = "select 1 from laagent where agentcode = '" + aAgentCode + "' and agentstate <= '02' ";
        String tAgentCodeFlag = new ExeSQL().getOneValue(tSql);
        if (!"1".equals(tAgentCodeFlag))
        {
            return false;
        }
        return true;
    }

    /**
     * 校验代理人名称是否正确
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkAgentName(String aAgentCode, String aAgentName)
    {
        String tSql = "select 1 from laagent where agentcode = '" + aAgentCode + "' and name = '" + aAgentName + "' ";
        String tAgentCodeFlag = new ExeSQL().getOneValue(tSql);
        if (!"1".equals(tAgentCodeFlag))
        {
            return false;
        }
        return true;
    }

    public String getErrInfo()
    {
        return mErrInfo.toString();
    }

    public static void main(String[] args)
    {
        //        XmlMsgColls xml = new XmlMsgColls();
        //        xml.setBodyByFlag("BatchNo", "LISWIIDLGSSys2011072609150000001");
        //        xml.setBodyByFlag("SendDate", "2011-7-13");
        //        xml.setBodyByFlag("SendTime", "15:45:36");
        //
        //        XmlMsgColls xml1 = new XmlMsgColls();
        //        xml1.setBodyByFlag("BatchNo", "LISWIIDLGSSys2011072609150000002");
        //        xml1.setBodyByFlag("SendDate", "2011-7-13");
        //        xml1.setBodyByFlag("SendTime", "15:45:36");
        //
        //        XmlMsgColls root = new XmlMsgColls();
        //        root.setBodyByFlag("MsgHead", xml);
        //        root.setBodyByFlag("MsgHead", xml1);
        //
        //        IXmlMsgColls[] ires = root.getBodyByFlag("MsgHead");
        //        System.out.println(ires.length);
        //        for (int i = 0; i < ires.length; i++)
        //        {
        //            List list = ires[i].getBodyIdxList();
        //            for (int j = 0; j < list.size(); j++)
        //            {
        //                String tmpTitle = (String) list.get(j);
        //                System.out.println(tmpTitle + ":" + ires[i].getTextByFlag(tmpTitle));
        //            }
        //        }
//        String tBatchNo = "LISWIIDLGSSys2011021609155900001";
//        //String tBatchNo = "20110816" ;
//        if (!checkBatchNo(tBatchNo))
//        {
//            System.out.print("失败");
//        }
//        else
//        {
//            System.out.print("成功");
//        }
    	
//    	String s = "12a3b456789136f";
//    	System.out.println(s.length());
    	
    }
    
    private String checkSaleChnlInfo(String aManageCom,String aSaleChnl,String aAgentCom,String aAgentCode){
    	String tError = "";
    	
    	String tSQLCode = "select 1 from laagent where agentcode = '"+aAgentCode+"' and managecom = '"+aManageCom+"' ";
    	String arrCode = new ExeSQL().getOneValue(tSQLCode);
    	if(!"1".equals(arrCode)){
    		tError = "业务员与管理机构不匹配！";
    		return tError;
    	}
		String agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ aAgentCode+ "'" 
	                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                     + " and b.CodeType = 'salechnl' and b.Code = '"+ aSaleChnl + "' ";
	    String arrAgentCode = new ExeSQL().getOneValue(agentCodeSql);
	    if(!"1".equals(arrAgentCode)){
	    	tError = "业务员和销售渠道不匹配！";
			return tError;
	    }
    	if(!"".equals(aAgentCom) && aAgentCom != null){
    		String tSQLCom = "select 1 from lacom where agentcom = '"+aAgentCom+"' and managecom = '"+aManageCom+"' ";
    		String arrCom = new ExeSQL().getOneValue(tSQLCom);
    		if(!"1".equals(arrCom)){
    			tError = "中介机构与管理机构不匹配！";
    			return tError;
    		}
    		String tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+aAgentCode+"' and agentcom = '"+aAgentCom+"' ";
    		String arrComCode = new ExeSQL().getOneValue(tSQLComCode);
    		if(!"1".equals(arrComCode)){
    			tError = "业务员与中介机构不匹配！";
    			return tError;
    		}
    	}
    	if(!"03".equals(aSaleChnl) && !"10".equals(aSaleChnl) && !"04".equals(aSaleChnl) && !"15".equals(aSaleChnl) && !"20".equals(aSaleChnl) && !"".equals(StrTool.cTrim(aAgentCom))){
    		tError = "销售渠道非中介渠道，不应填写中介机构！";
			return tError;
    	}
    	if("16".equals(aSaleChnl)){
    		tError = "销售渠道不支持社保直销！";
			return tError;
    	}
    	return tError;
    }
    
    private String checkMarkettypeSalechnl(String tMarketType,String tSaleChnl){
    	String tError = "";
    	
    	String tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+tMarketType+"' and code1 = '"+tSaleChnl+"' ";
    	String arrCheck = new ExeSQL().getOneValue(tcheckSQL);
    	if(!"1".equals(arrCheck)){
    		tError = "市场类型和销售渠道不匹配，请核查！";
    		return tError;
    	}
    	return tError;
    }
    
    private String checkRiskSalechnl(String tManageCom,String tWrapFlag,String tSaleChnl){
    	String tError = "";
    	String tSQL = " select codename from ldcode1 where codetype = 'bcgs' " + " and Code = '" + tManageCom + "' and code1 = '" + tWrapFlag + "' ";
    	String tWrapCode = new ExeSQL().getOneValue(tSQL);
        if (tWrapCode == null || "".equals(tWrapCode))
        {
        	tError = "投保单位级别信息有误，未找到[" + tManageCom + "]机构下[" + tWrapFlag + "]套餐代码所对应产品信息。";
            return tError;
        }
        String tSQLRiskCode = "select riskcode from ldriskwrap where riskwrapcode = '"+tWrapCode+"' ";
    	SSRS arr = new ExeSQL().execSQL(tSQLRiskCode);
    	if(arr == null || arr.MaxRow < 1){
    		tError = "获取保单套餐下险种数据失败！";
    		return tError;
    	}
    	for(int i=1;i<=arr.MaxRow;i++){
    		String tRiskCode = arr.GetText(i, 1);
    		String tcheckSQL = "select codename from ldcode where codetype = 'unitesalechnlrisk' and code = '"+tRiskCode+"' ";
    		String arrCheck = new ExeSQL().getOneValue(tcheckSQL);
    		if("".equals(arrCheck)){
    			if(tSaleChnl == "16"){
    				tError = "险种"+tRiskCode+"为非社保险种，与销售渠道"+tSaleChnl+"不匹配！";
    				return tError;
    			}
    		}else{
    			if( !"all".equals(arrCheck) && tSaleChnl.equals(arrCheck)){
    				tError = "险种"+tRiskCode+"与销售渠道"+tSaleChnl+"不匹配！";
    				return tError;
    			}
    		}
    	}
    	return tError;
    }
    
    //校验联系电话
    private boolean checkPhone(String pho){
    	String tPho = pho.substring(0,1);
    	Pattern pattern1 = Pattern.compile("^[1][0-9]{10}$");
    	Pattern pattern2 = Pattern.compile("^[0-9\\-\\(\\)（）]*$");
    	if("1".equals(tPho)){
    		return pattern1.matcher(pho).matches();
    	}else{
    		return pattern2.matcher(pho).matches();
    	}
    }
    //校验共保机构
    public boolean checkCoCom(String CoAgentCom,String CoAgentComName,String ManageCom,String Comcode){
    	String Sql="select 1 from LACom lac "
    			+ "where 1=1 "
    			+ "and lac.agentcom='"+CoAgentCom+"' "
    			+ "and lac.name='"+CoAgentComName+"' "
    			+ "and lac.ManageCom like '"+ManageCom+"%' "
    			+ "and lac.ManageCom like '"+Comcode+"%' "
    			+ "and ((lac.BranchType = '2' and lac.BranchType2 = '02')  or lac.BranchType = '5') "
    			+ "and lac.AcType = '05' "
    			+ "and lac.SellFlag = 'Y' "
    			+ "and (lac.EndFlag='N' or lac.EndFlag is null) ";
    	String SqlResult=new ExeSQL().getOneValue(Sql);
	    if(SqlResult==null || !"1".equals(SqlResult)){
	    	return false;
	    }
    	return true;
    }
}
