/**
 * 2011-8-11
 */
package com.sinosoft.lis.wiitb;

import com.sinosoft.collections.IXmlMsgColls;
import com.sinosoft.lis.certifybusiness.CertifyDiskImportLog;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDRiskDutyWrapDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCCoInsuranceParamSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpContSubSchema;
import com.sinosoft.lis.schema.LCRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LDRiskDutyWrapSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.tb.CoInsuranceGrpContUL;
import com.sinosoft.lis.tb.NoNameContUI;
import com.sinosoft.lis.tb.ParseGuideIn;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LCCoInsuranceParamSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LDRiskDutyWrapSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class ParseBriWiiGrpContBL
{
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    private GlobalInput mGlobalInput = null;

    private TransferData mTransferData = null;

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();

    /** 数据操作字符串 */
    private String mOperate = "";

    private String mCurDate = PubFun.getCurrentDate();

    private String mCurTime = PubFun.getCurrentTime();

    private String mBatchNo = null;

    private String mMsgType = null;

    private String mPrtNo = null;

    private LCGrpContSchema mGrpContInfo = null;

    private IXmlMsgColls mMsgData = null;

    private CertifyDiskImportLog mImportLog = null;

    private String mWrapCode = null;

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        if (!submit())
        {
            return false;
        }

        return true;
    }

    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }

    private boolean getInputData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "处理超时，请重新登录。");
            return false;
        }

        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        if (mTransferData == null)
        {
            buildError("getInputData", "所需参数不完整。");
            return false;
        }

        mMsgData = (IXmlMsgColls) mTransferData.getValueByName("XmlMsgDatas");
        if (mMsgData == null)
        {
            buildError("getInputData", "未获取到保单数据信息。");
            return false;
        }

        mPrtNo = (String) mTransferData.getValueByName("PrtNo");
        if (mPrtNo == null)
        {
            buildError("getInputData", "未获取到保单印刷号信息。");
            return false;
        }

        mBatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (mBatchNo == null || mBatchNo.equals(""))
        {
            buildError("getInputData", "获取批次信息失败，请检查文件名是否符合批次命名规则。");
            return false;
        }

        mImportLog = (CertifyDiskImportLog) mTransferData.getValueByName("ImportLog");
        if (mImportLog == null || mImportLog.equals(""))
        {
            buildError("getInputData", "未获取到日志记录器。");
            return false;
        }

        mMsgType = (String) mTransferData.getValueByName("MsgType");
        if (mMsgType == null || "".equals(mMsgType))
        {
            buildError("getInputData", "获取报文类型失败，请检查报文类型是否符合约定命名规则。");
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    private boolean dealData()
    {
        MMap tTmpMap = null;

        if ("Create".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = createBriGrpCont();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;

            //            if (!createBriGrpCont())
            //            {
            //                return false;
            //            }
        }
        else if ("Update".equals(mOperate))
        {
            tTmpMap = null;
            tTmpMap = updateBriGrpCont();
            if (tTmpMap == null)
            {
                return false;
            }
            mMap.add(tTmpMap);
            tTmpMap = null;

            //            if (!createBriGrpCont())
            //            {
            //                return false;
            //            }
        }

        return true;
    }

    private MMap createBriGrpCont()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        IXmlMsgColls tPolicyDatas = mMsgData;

        String tStrSql = null;
        String tStrResult = null;
        String ComCode=mGlobalInput.ComCode;

        // 数据质检
        BriGrpCommonCheckBL tChkCommTool = new BriGrpCommonCheckBL();
        if (!tChkCommTool.checkPolicyInfo(tPolicyDatas, mBatchNo, mMsgType,ComCode))
        {
            String tStrErr = "[" + mPrtNo + "]：" + tChkCommTool.getErrInfo();
            buildError("createBriGrpCont", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return null;
            }

            return null;
        }
        // --------------------

        // 创建保单基本信息
        tStrSql = "select 1 from LCGrpCont where PrtNo = '" + mPrtNo + "'";
        tStrResult = new ExeSQL().getOneValue(tStrSql);
        if (!"1".equals(tStrResult))
        {
            if (!dealGrpContInfo(tPolicyDatas))
            {
                return null;
            }
        }
        else
        {
            tTmpMap = updateAccountInfo(tPolicyDatas);
            if (tTmpMap == null)
            {
                return null;
            }
            else
            {

                tMMap.add(tTmpMap);
                tTmpMap = null;
                MMap tSuccMMap = mImportLog.infoLog4Map(mPrtNo, "导入成功");
                tMMap.add(tSuccMMap);
                return tMMap; // 更新帐户时
            }
        }
        tStrSql = null;
        // --------------------

        //创建共保信息
        String tCoInsuranceFlag=tPolicyDatas.getTextByFlag("CoInsuranceFlag");
        if(tCoInsuranceFlag!=null && "1".equals(tCoInsuranceFlag)){
		    if(!dealCoInsurenceInfo(tPolicyDatas)){
		    	return null;
		    }
        }
        // 创建保单保障信息
        if (!dealGrpWrapInfo())
        {
            rollBackData();//回滚数据
            return null;
        }
        // --------------------

        // 处理整单相关状态，如果为银行转帐缴费的保单，同时生成待发盘数据
        tTmpMap = dealGrpContEndInfo(mGrpContInfo);
        if (tTmpMap == null)
        {
            rollBackData();//回滚数据
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    private MMap updateBriGrpCont()
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        IXmlMsgColls tPolicyDatas = mMsgData;

        String tStrSql = null;

        String ComCode = mGlobalInput.ComCode;
        // 数据质检
        BriGrpCommonCheckBL tChkCommTool = new BriGrpCommonCheckBL();
        if (!tChkCommTool.checkPolicyInfo(tPolicyDatas, mBatchNo, mMsgType,ComCode))
        {
            String tStrErr = "[" + mPrtNo + "]：" + tChkCommTool.getErrInfo();
            buildError("createBriGrpCont", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return null;
            }

            return null;
        }
        // --------------------

        // 更新时先删除保单
        tStrSql = "select * from LCGrpCont where PrtNo = '" + mPrtNo + "'";
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(tStrSql);
        if (tLCGrpContSet != null && tLCGrpContSet.size() > 0)
        {
            LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);
            VData tVData = new VData();
            tVData.add(tLCGrpContSchema);
            tVData.add(mGlobalInput);
            BriGrpContDeleteBL tBriGrpContDeleteBL = new BriGrpContDeleteBL();

            tTmpMap = tBriGrpContDeleteBL.getSubmitMap(tVData, "UpGrpDelete");
            if (tTmpMap == null)
            {
                return null;
            }

            // 创建保单基本信息时，即时提交保单数据，因此需先删除保单数据
            if (!submit(tTmpMap))
            {
                return null;
            }
            tTmpMap = null;
        }

        // 创建保单基本信息
        if (!dealGrpContInfo(tPolicyDatas))
        {
            return null;
        }
        tStrSql = null;
        // --------------------

        // 创建保单保障信息
        if (!dealGrpWrapInfo())
        {
            return null;
        }
        // --------------------

        // 处理整单相关状态，如果为银行转帐缴费的保单，同时生成待发盘数据
        tTmpMap = dealGrpContEndInfo(mGrpContInfo);
        if (tTmpMap == null)
        {
            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------

        return tMMap;
    }

    private boolean dealGrpContInfo(IXmlMsgColls cMsgDatas)
    {
        // 保单层信息
        String tPrtNo = cMsgDatas.getTextByFlag("PrtNo");
        String tManageCom = cMsgDatas.getTextByFlag("ManageCom");
        String tSaleChnl = cMsgDatas.getTextByFlag("SaleChnl");
        String tMarketType = cMsgDatas.getTextByFlag("MarketType");
        String tAgentCom = cMsgDatas.getTextByFlag("AgentCom");
        String magentcode=new ExeSQL().getOneValue("select getAgentCode('"+cMsgDatas.getTextByFlag("AgentCode")+"') from dual");
        String tAgentCode ="" ;
        if(!"".equals(magentcode) && magentcode!=null){
        	tAgentCode=magentcode;
        }else{
        	tAgentCode=cMsgDatas.getTextByFlag("AgentCode");
        }
        String tPolApplyDate = cMsgDatas.getTextByFlag("PolApplyDate");
        String tHandlerName = cMsgDatas.getTextByFlag("HandlerName");
        String tFirstTrialOperator = cMsgDatas.getTextByFlag("FirstTrialOperator");
        String tPeoples3 = cMsgDatas.getTextByFlag("Peoples3");
        String tOnWorkPeoples = cMsgDatas.getTextByFlag("OnWorkPeoples");
        String tOffWorkPeoples = cMsgDatas.getTextByFlag("OffWorkPeoples");
        String tOtherPeoples = cMsgDatas.getTextByFlag("OtherPeoples");
        String tPayMode = cMsgDatas.getTextByFlag("PayMode");
        String tPayIntv = cMsgDatas.getTextByFlag("PayIntv");
        String tPremScope = cMsgDatas.getTextByFlag("PremScope");
        String tBankCode = cMsgDatas.getTextByFlag("BankCode");
        String tBankAccNo = cMsgDatas.getTextByFlag("BankAccNo");
        String tBankAccName = cMsgDatas.getTextByFlag("AccName");
        String tCValiDate = cMsgDatas.getTextByFlag("CValiDate");
        String tCInValiDate = cMsgDatas.getTextByFlag("CInValiDate");
        String tRemark = cMsgDatas.getTextByFlag("Remark");
       
        String tCounty = cMsgDatas.getTextByFlag("County");
        String tCustomerBankCode = cMsgDatas.getTextByFlag("CustomerBankCode");
        String tAuthorizeCode = cMsgDatas.getTextByFlag("AuthorizeCode");
        String tCoInsuranceFlag=cMsgDatas.getTextByFlag("CoInsuranceFlag");

        LCGrpContSchema tGrpContInfo = new LCGrpContSchema();

        tGrpContInfo.setPrtNo(tPrtNo);
        tGrpContInfo.setManageCom(tManageCom);
        tGrpContInfo.setSaleChnl(tSaleChnl);
        tGrpContInfo.setMarketType(tMarketType);
        tGrpContInfo.setAgentCom(tAgentCom);
        tGrpContInfo.setAgentCode(tAgentCode);

        tGrpContInfo.setPolApplyDate(tPolApplyDate);
        tGrpContInfo.setHandlerName(tHandlerName);
        tGrpContInfo.setFirstTrialOperator(tFirstTrialOperator);

        tGrpContInfo.setPeoples3(tPeoples3);
        tGrpContInfo.setOnWorkPeoples(tOnWorkPeoples);
        tGrpContInfo.setOffWorkPeoples(tOffWorkPeoples);
        tGrpContInfo.setOtherPeoples(tOtherPeoples);

        tGrpContInfo.setPayMode(tPayMode);
        tGrpContInfo.setPayIntv(tPayIntv);
        tGrpContInfo.setPremScope(tPremScope);

        tGrpContInfo.setBankCode(tBankCode);
        tGrpContInfo.setBankAccNo(tBankAccNo);
        tGrpContInfo.setAccName(tBankAccName);

        tGrpContInfo.setCValiDate(tCValiDate);
        tGrpContInfo.setCInValiDate(tCInValiDate);

        tGrpContInfo.setRemark(tRemark);
        tGrpContInfo.setCoInsuranceFlag(tCoInsuranceFlag);
        // --------------------
        
        LCGrpContSubSchema tGrpContSub = new LCGrpContSubSchema();
        tGrpContSub.setPrtNo(tPrtNo);
        tGrpContSub.setCounty(tCounty);
        tGrpContSub.setCustomerBankCode(tCustomerBankCode);
        tGrpContSub.setAuthorizeCode(tAuthorizeCode);
        tGrpContSub.setManageCom(tManageCom);
        tGrpContSub.setOperator(mGlobalInput.Operator);
        tGrpContSub.setMakeDate(PubFun.getCurrentDate());
        tGrpContSub.setMakeTime(PubFun.getCurrentTime());
        tGrpContSub.setModifyDate(PubFun.getCurrentDate());
        tGrpContSub.setModifyTime(PubFun.getCurrentTime());

        // 投保单位信息  
        IXmlMsgColls[] tGrpDatas = cMsgDatas.getBodyByFlag("GrpAppntInfo");
        if (tGrpDatas == null || tGrpDatas.length != 1)
        {
            String tStrErr = "[" + mPrtNo + "]：投保单位信息未找到。";
            buildError("createBriGrpCont", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }

        IXmlMsgColls tGrpInfoData = tGrpDatas[0];

        String tGrpLevel = tGrpInfoData.getTextByFlag("GrpLevel");

        String tWrapFlag = tGrpInfoData.getTextByFlag("WrapFlag");
        if (tWrapFlag == null || tWrapFlag.equals(""))
        {
            tWrapFlag = tGrpLevel; // 为和之前兼容，增加套餐代码后，如果未找到套餐代码信息则自动用单位级别进行查找
        }

        String tSQL = " select codename from ldcode1 where codetype = 'bcgs' " + " and Code = '" + tManageCom + "' "
                + " and code1 = '" + tWrapFlag + "' ";

        mWrapCode = new ExeSQL().getOneValue(tSQL);
        if (mWrapCode == null || "".equals(mWrapCode))
        {
            String tStrErr = "[" + mPrtNo + "]：投保单位级别信息有误，未找到[" + tManageCom + "]机构下[" + tGrpLevel + "]套餐代码所对应产品信息。";
            buildError("createBriGrpCont", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }

        //        String tCustomerNo = cMsgDatas.getTextByFlag("CustomerNo");
        String tGrpName = tGrpInfoData.getTextByFlag("GrpName");
        String tOrgancomCode = tGrpInfoData.getTextByFlag("OrgancomCode");
        String tOtherCertificates = tGrpInfoData.getTextByFlag("OtherCertificates");

        LCGrpAppntSchema tGrpAppntInfo = new LCGrpAppntSchema();

        //tGrpAppntInfo.setCustomerNo(tCustomerNo);
        tGrpAppntInfo.setName(tGrpName);
        tGrpAppntInfo.setTaxNo(tOrgancomCode);
        tGrpAppntInfo.setOtherCertificates(tOtherCertificates);
        tGrpAppntInfo.setGrpLevel(tGrpLevel);
        // --------------------

        // 投保单位地址&联系方式相关信息
        String tGrpAddress = tGrpInfoData.getTextByFlag("GrpAddress");
        String tGrpZipCode = tGrpInfoData.getTextByFlag("GrpZipCode");
        String tLinkMan1 = tGrpInfoData.getTextByFlag("LinkMan1");
        String tPhone1 = tGrpInfoData.getTextByFlag("Phone1");

        LCGrpAddressSchema tGrpAddrInfo = new LCGrpAddressSchema();

        tGrpAddrInfo.setGrpAddress(tGrpAddress);
        tGrpAddrInfo.setGrpZipCode(tGrpZipCode);
        tGrpAddrInfo.setLinkMan1(tLinkMan1);
        tGrpAddrInfo.setPhone1(tPhone1);
        // --------------------

        // 创建保单基本信息
        VData tVData = new VData();

        TransferData tTransferData = new TransferData();
        tVData.add(tTransferData);

        tVData.add(mGlobalInput);

        tVData.add(tGrpContInfo);
        tVData.add(tGrpAppntInfo);
        tVData.add(tGrpAddrInfo);
        tVData.add(tGrpContSub);

        BriGrpContUI tBriGrpContUI = new BriGrpContUI();
        if (!tBriGrpContUI.submitData(tVData, "Create"))
        {
            String tStrErr = "[" + mPrtNo + "]" + tBriGrpContUI.mErrors.getFirstError();
            buildError("createBriGrpCont", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }
        // --------------------

        return true;
    }

    private MMap updateAccountInfo(IXmlMsgColls cMsgDatas)
    {
        MMap tMMap = new MMap();

        StringBuffer tBStrSql = null;

        // 判断保单缴费方式是否为：4-银行转帐，且尚未签单
        String tStrSql = null;
        String tStrResult = null;
        tStrSql = " select 1 from LCGrpCont where PayMode in ('4') and AppFlag != '1' and PrtNo = '" + mPrtNo + "' ";
        tStrResult = new ExeSQL().getOneValue(tStrSql);
        if (!"1".equals(tStrResult))
        {
            System.out.println("非银行转帐方式缴费的保单，不用更新银行帐户。");
            return new MMap();
        }
        tStrSql = null;
        tStrResult = null;
        // --------------------

        String tGetNoticeNo = null;

        // 判断转帐方式缴费的保单，待收费信息是否在途（？？！！..）
        tStrSql = " select * from LJSPay where OtherNo = '" + mPrtNo + "'";
        LJSPaySet tLJSPaySet = new LJSPayDB().executeQuery(tStrSql);
        if (tLJSPaySet == null || tLJSPaySet.size() != 1)
        {
            TransferData tTransData = new TransferData();
            tTransData.setNameAndValue("PrtNo", mPrtNo);

            VData tVData = new VData();
            tVData.add(mGlobalInput);
            tVData.add(tTransData);

            BriGrpTempFeeFromBankBL tBriGrpTempFeeFromBankBL = new BriGrpTempFeeFromBankBL();

            MMap tTmpMap = tBriGrpTempFeeFromBankBL.getSubmitMap(tVData, null);
            if (tTmpMap == null)
            {
                String tStrErr = "[" + mPrtNo + "]" + tBriGrpTempFeeFromBankBL.mErrors.getFirstError();
                buildError("dealGrpContEndInfo", tStrErr);
                if (!mImportLog.errLog(mPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return null;
                }

                return null;
            }
            tMMap.add(tTmpMap);
            return tMMap;
        }
        else
        {
            LJSPaySchema tSPayInfo = tLJSPaySet.get(1);

            String tOnTheWayFlag = tSPayInfo.getBankOnTheWayFlag();
            if ("1".equals(tOnTheWayFlag))
            {
                String tStrErr = "[" + mPrtNo + "]：发盘数据已在途，不允许重复处理。";
                buildError("updateAccountInfo", tStrErr);
                if (!mImportLog.errLog(mPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return null;
                }

                return null;
            }

            tGetNoticeNo = tSPayInfo.getGetNoticeNo();

            LJSPayDB tUpSPayDB = new LJSPayDB();
            tUpSPayDB.setGetNoticeNo(tGetNoticeNo);
            if (!tUpSPayDB.getInfo())
            {
                String tStrErr = "[" + mPrtNo + "]：待发盘数据异常。";
                buildError("updateAccountInfo", tStrErr);
                if (!mImportLog.errLog(mPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return null;
                }

                return null;
            }

            tUpSPayDB.setCanSendBank("1");
            tUpSPayDB.update(); // 即时锁定待发盘数据

            tStrSql = null;
            tStrResult = null;
            // --------------------

            String tBankCode = cMsgDatas.getTextByFlag("BankCode");
            String tBankAccNo = cMsgDatas.getTextByFlag("BankAccNo");

            if (tBankCode == null || tBankCode.equals(""))
            {
                String tStrErr = "[" + mPrtNo + "]：银行代码信息为空。";
                buildError("updateAccountInfo", tStrErr);
                if (!mImportLog.errLog(mPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return null;
                }

                return null;
            }

            if (tBankAccNo == null || tBankAccNo.equals(""))
            {
                String tStrErr = "[" + mPrtNo + "]：银行帐号信息为空。";
                buildError("updateAccountInfo", tStrErr);
                if (!mImportLog.errLog(mPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return null;
                }

                return null;
            }

            // 更新保单主表帐户信息
            tBStrSql = new StringBuffer();
            tBStrSql.append(" update LCGrpCont Set ");
            tBStrSql.append(" BankCode = '" + tBankCode + "', ");
            tBStrSql.append(" BankAccNo = '" + tBankAccNo + "', ");
            tBStrSql.append(" ModifyDate = '" + mCurDate + "', ");
            tBStrSql.append(" ModifyTime = '" + mCurTime + "' ");
            tBStrSql.append(" where PrtNo = '" + mPrtNo + "' ");

            //tMMap.put(tBStrSql.toString(), SysConst.UPDATE);
            tBStrSql = null;
            // --------------------

            // 更新保单待发盘转帐信息
            tBStrSql = new StringBuffer();
            tBStrSql.append(" update LJSPay Set ");
            tBStrSql.append(" CanSendBank = '0', ");
            //            tBStrSql.append(" BankCode = '" + tBankCode + "', ");
            //            tBStrSql.append(" BankAccNo = '" + tBankAccNo + "', ");
            tBStrSql.append(" ModifyDate = '" + mCurDate + "', ");
            tBStrSql.append(" ModifyTime = '" + mCurTime + "' ");
            tBStrSql.append(" where GetNoticeNo = '" + tGetNoticeNo + "' ");

            tMMap.put(tBStrSql.toString(), SysConst.UPDATE);
            tBStrSql = null;

            tBStrSql = new StringBuffer();
            tBStrSql.append(" update LJTempFeeClass Set ");
            tBStrSql.append(" BankCode = '" + tBankCode + "', ");
            tBStrSql.append(" BankAccNo = '" + tBankAccNo + "', ");
            tBStrSql.append(" ModifyDate = '" + mCurDate + "', ");
            tBStrSql.append(" ModifyTime = '" + mCurTime + "' ");
            tBStrSql.append(" where TempFeeNo = '" + tGetNoticeNo + "' ");

            //tMMap.put(tBStrSql.toString(), SysConst.UPDATE);
            tBStrSql = null;
            return tMMap;
        }
        // --------------------
    }

    private boolean dealGrpWrapInfo()
    {
        String tStrSql = null;

        tStrSql = " select * from LCGrpCont where PrtNo = '" + mPrtNo + "' ";
        LCGrpContSet tGrpContInfoSet = new LCGrpContDB().executeQuery(tStrSql);

        if (tGrpContInfoSet == null || tGrpContInfoSet.size() != 1)
        {
            String tStrErr = "[" + mPrtNo + "]：对应保单信息未找到或出现冗余。";
            buildError("dealGrpWrapInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }
        tStrSql = null;

        LCGrpContSchema tGrpContInfo = tGrpContInfoSet.get(1);

        // 获取对应产品套餐信息
        tStrSql = " select * from LDRiskDutyWrap where RiskWrapCode = '" + mWrapCode + "' ";
        LDRiskDutyWrapSet tDWrapDutyInfos = null;
        tDWrapDutyInfos = new LDRiskDutyWrapDB().executeQuery(tStrSql);
        if (tDWrapDutyInfos == null || tDWrapDutyInfos.size() == 0)
        {
            String tStrErr = "[" + mPrtNo + "]：对应套餐[" + mWrapCode + "]信息异常。";
            buildError("dealGrpWrapInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }

        LCRiskDutyWrapSet tLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
        for (int idx = 1; idx <= tDWrapDutyInfos.size(); idx++)
        {
            LDRiskDutyWrapSchema tDWrapDutyParamInfo = tDWrapDutyInfos.get(idx);

            LCRiskDutyWrapSchema tWrapDutyParam = new LCRiskDutyWrapSchema();

            if (tDWrapDutyParamInfo.getCalFactorType() == null || !tDWrapDutyParamInfo.getCalFactorType().equals("1"))
            {
                String tStrErr = "[" + mPrtNo + "]：对应套餐[" + mWrapCode + "]要素信息并非全部指定。";
                buildError("dealGrpWrapInfo", tStrErr);
                if (!mImportLog.errLog(mPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }

                return false;
            }
            tWrapDutyParam.setRiskWrapCode(tDWrapDutyParamInfo.getRiskWrapCode());
            tWrapDutyParam.setRiskCode(tDWrapDutyParamInfo.getRiskCode());
            tWrapDutyParam.setDutyCode(tDWrapDutyParamInfo.getDutyCode());
            tWrapDutyParam.setCalFactor(tDWrapDutyParamInfo.getCalFactor());
            tWrapDutyParam.setCalFactorType(tDWrapDutyParamInfo.getCalFactorType());
            tWrapDutyParam.setCalFactorValue(tDWrapDutyParamInfo.getCalFactorValue());

            tLCRiskDutyWrapSet.add(tWrapDutyParam);
        }
        // --------------------

        // 创建保单套餐信息
        TransferData tTransData = new TransferData();
        tTransData.setNameAndValue("PrtNo", mPrtNo);
        tTransData.setNameAndValue("GrpContNo", tGrpContInfo.getGrpContNo());

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransData);

        tVData.add(tLCRiskDutyWrapSet);

        BriGrpWrapPlanUI tBriGrpWrapPlanUI = new BriGrpWrapPlanUI();
        if (!tBriGrpWrapPlanUI.submitData(tVData, "Create"))
        {
            String tStrErr = "[" + mPrtNo + "]" + tBriGrpWrapPlanUI.mErrors.getFirstError();
            buildError("dealGrpWrapInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }
        // --------------------

        // 创建无名单数据
        if (!dealNoNameInfo(tGrpContInfo.getGrpContNo()))
        {
            return false;
        }
        // --------------------

        // 计算保费
        if (!calGrpContPolInfo(tGrpContInfo.getGrpContNo()))
        {
            return false;
        }
        // --------------------

        // 更新GrpCont最新数据
        LCGrpContDB tTmpGrpContInfo = new LCGrpContDB();
        tTmpGrpContInfo.setGrpContNo(tGrpContInfo.getGrpContNo());
        if (!tTmpGrpContInfo.getInfo())
        {
            String tStrErr = "[" + mPrtNo + "]：获取最新保单信息失败。";
            buildError("dealGrpWrapInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }

        mGrpContInfo = tTmpGrpContInfo.getSchema();
        // --------------------

        // 校验系统计算保费与传入保费是否一致，不一致则阻断报错
        double tCalPremScope = tTmpGrpContInfo.getPrem();
        double tInputPremScope = tTmpGrpContInfo.getPremScope();
        if (tCalPremScope != tInputPremScope)
        {
            String tStrErr = "[" + mPrtNo + "]：填写整单保费为[" + tInputPremScope + "]，与系统计算整单保费[" + tCalPremScope + "]不一致。";
            buildError("dealGrpWrapInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }
        // --------------------

        return true;
    }

    private boolean dealNoNameInfo(String cGrpContNo)
    {
        TransferData tTransData = new TransferData();
        tTransData.setNameAndValue("GrpContNo", cGrpContNo);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransData);

        NoNameContUI tNoNameContUI = new NoNameContUI();
        if (!tNoNameContUI.submitData(tVData, "Create"))
        {
            String tStrErr = "[" + mPrtNo + "]" + tNoNameContUI.mErrors.getFirstError();
            buildError("dealNoNameInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }

        return true;
    }

    private boolean calGrpContPolInfo(String cGrpContNo)
    {
        TransferData tTransData = new TransferData();
        tTransData.setNameAndValue("GrpContNo", cGrpContNo);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransData);

        ParseGuideIn tParseGuideIn = new ParseGuideIn();
        if (!tParseGuideIn.submitData(tVData, "INSERT||DATABASE"))
        {
            String tStrErr = "[" + mPrtNo + "]" + tParseGuideIn.mErrors.getFirstError();
            buildError("calGrpContPolInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }

            return false;
        }

        return true;
    }

    private MMap dealGrpContEndInfo(LCGrpContSchema cGrpContInfo)
    {
        MMap tMMap = new MMap();
        MMap tTmpMap = null;

        String tOperator = mGlobalInput.Operator;
        String tGrpContNo = cGrpContInfo.getGrpContNo();
        String tPrtNo = cGrpContInfo.getPrtNo();
        String tCurDate = PubFun.getCurrentDate();
        String tCurTime = PubFun.getCurrentTime();

        StringBuffer tStrSql = null;

        // LCCont
        tStrSql = new StringBuffer();
        tStrSql.append(" update LCCont set ");

        tStrSql.append(" UWFlag = '9', ");
        tStrSql.append(" UWDate = '" + tCurDate + "', ");
        tStrSql.append(" UWOperator = '" + tOperator + "', ");

        tStrSql.append(" ApproveFlag = '9', ");
        tStrSql.append(" ApproveDate = '" + tCurDate + "', ");
        tStrSql.append(" ApproveTime = '" + tCurTime + "', ");
        tStrSql.append(" ApproveCode = '" + tOperator + "' ");

        tStrSql.append(" where 1 = 1 ");
        tStrSql.append(" and GrpContNo = '" + tGrpContNo + "' ");
        System.out.println(tStrSql.toString());
        tMMap.put(tStrSql.toString(), SysConst.UPDATE);
        // --------------------

        // LCPol
        tStrSql = new StringBuffer();
        tStrSql.append(" update LCPol set ");

        tStrSql.append(" UWFlag = '9', ");
        tStrSql.append(" UWDate = '" + tCurDate + "', ");
        tStrSql.append(" UWCode = '" + tOperator + "', ");

        tStrSql.append(" ApproveFlag = '9', ");
        tStrSql.append(" ApproveDate = '" + tCurDate + "', ");
        tStrSql.append(" ApproveTime = '" + tCurTime + "', ");
        tStrSql.append(" ApproveCode = '" + tOperator + "' ");

        tStrSql.append(" where 1 = 1 ");
        tStrSql.append(" and GrpContNo = '" + tGrpContNo + "' ");
        System.out.println(tStrSql.toString());
        tMMap.put(tStrSql.toString(), SysConst.UPDATE);
        // --------------------

        // LCGrpCont
        tStrSql = new StringBuffer();
        tStrSql.append(" update LCGrpCont set ");

        tStrSql.append(" UWFlag = '9', ");
        tStrSql.append(" UWDate = '" + tCurDate + "', ");
        tStrSql.append(" UWOperator = '" + tOperator + "', ");

        tStrSql.append(" ApproveFlag = '9', ");
        tStrSql.append(" ApproveDate = '" + tCurDate + "', ");
        tStrSql.append(" ApproveTime = '" + tCurTime + "', ");
        tStrSql.append(" ApproveCode = '" + tOperator + "' ");

        tStrSql.append(" where 1 = 1 ");
        tStrSql.append(" and GrpContNo = '" + tGrpContNo + "' ");
        System.out.println(tStrSql.toString());
        tMMap.put(tStrSql.toString(), SysConst.UPDATE);
        // --------------------

        // LCGrpPol
        tStrSql = new StringBuffer();
        tStrSql.append(" update LCGrpPol set ");

        tStrSql.append(" UWFlag = '9', ");
        tStrSql.append(" UWDate = '" + tCurDate + "', ");
        tStrSql.append(" UWOperator = '" + tOperator + "', ");

        tStrSql.append(" ApproveFlag = '9', ");
        tStrSql.append(" ApproveDate = '" + tCurDate + "', ");
        tStrSql.append(" ApproveTime = '" + tCurTime + "', ");
        tStrSql.append(" ApproveCode = '" + tOperator + "' ");

        tStrSql.append(" where 1 = 1 ");
        tStrSql.append(" and GrpContNo = '" + tGrpContNo + "' ");
        System.out.println(tStrSql.toString());
        tMMap.put(tStrSql.toString(), SysConst.UPDATE);
        // --------------------

        // 银行转帐方式缴费保单，生成待发盘数据
        TransferData tTransData = new TransferData();
        tTransData.setNameAndValue("PrtNo", tPrtNo);

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tTransData);

        BriGrpTempFeeFromBankBL tBriGrpTempFeeFromBankBL = new BriGrpTempFeeFromBankBL();
        tTmpMap = tBriGrpTempFeeFromBankBL.getSubmitMap(tVData, null);
        if (tTmpMap == null)
        {
            String tStrErr = "[" + mPrtNo + "]" + tBriGrpTempFeeFromBankBL.mErrors.getFirstError();
            buildError("dealGrpContEndInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return null;
            }

            return null;
        }
        tMMap.add(tTmpMap);
        tTmpMap = null;
        // --------------------
        MMap tSuccMMap = mImportLog.infoLog4Map(mPrtNo, "导入成功");

        tMMap.add(tSuccMMap);
        // 发送缴费通知书，是否需要发送？待定
        //        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        //        tLOPRTManagerSchema.setOtherNo(mGrpContNo);
        //        tLOPRTManagerSchema.setOtherNoType("01");
        //        tLOPRTManagerSchema.setCode("57");
        //        //校验是否发过首期交费通知书 ，如果发过则不再重发
        //        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        //        tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
        //        if (tLOPRTManagerDB.query().size() <= 0)
        //        {
        //            VData tempVData = new VData();
        //            tempVData.add(tLOPRTManagerSchema);
        //            tempVData.add(mGlobalInput);
        //
        //            GrpUWSendPrintUI tGrpUWSendPrintUI = new GrpUWSendPrintUI();
        //            if (!tGrpUWSendPrintUI.submitData(tempVData, "INSERT"))
        //            {
        //                this.mErrors.copyAllErrors(tGrpUWSendPrintUI.mErrors);
        //                return false;
        //            }
        //        }
        // --------------------

        return tMMap;
    }

    /**
     * 创建错误日志。
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        System.out.println(szFunc + " : " + szErrMsg);

        CError cError = new CError();
        cError.moduleName = "CertifyGrpContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("系统超时，请稍后尝试重新提交，谢谢。");
            buildError("submitData", "系统超时，请稍后尝试重新提交，谢谢。");
            return false;
        }

        return true;
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap aMMap)
    {
        VData data = new VData();
        data.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("系统超时，请稍后尝试重新提交，谢谢。");
            buildError("submitData", "系统超时，请稍后尝试重新提交，谢谢。");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    // 回滚数据
    private boolean rollBackData()
    {
        //    	 回滚数据
        String tStrSql = "select * from LCGrpCont where PrtNo = '" + mPrtNo + "'";
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(tStrSql);
        if (tLCGrpContSet != null && tLCGrpContSet.size() == 1)
        {
            LCGrpContSchema tLCGrpContSchema = tLCGrpContSet.get(1);
            VData ttVData = new VData();
            ttVData.add(tLCGrpContSchema);
            ttVData.add(mGlobalInput);
            BriGrpContDeleteBL tBriGrpContDeleteBL = new BriGrpContDeleteBL();

            MMap tTmpMap = tBriGrpContDeleteBL.getSubmitMap(ttVData, "UpGrpDelete");
            if (tTmpMap == null)
            {
                return false;
            }
            if (!submit(tTmpMap))
            {
                return false;
            }
            return true;
        }
        return true;
    }
    
    /**
     * 创建共保信息
     * @param cMsgDatas
     * @return
     */
    public boolean dealCoInsurenceInfo(IXmlMsgColls cMsgDatas){
    	LCCoInsuranceParamSet tLCCoInsuranceParamSet = new LCCoInsuranceParamSet();
    	LCCoInsuranceParamSchema tTmpLCCoInsuranceParamSchema = new LCCoInsuranceParamSchema();
    	LCCoInsuranceParamSchema tTmpLCCoInsuranceParamSchema1 = new LCCoInsuranceParamSchema();
    	String tPrtNo = cMsgDatas.getTextByFlag("PrtNo");
    	String Sql="select GrpContNo,ProposalGrpContNo from LCGrpCont "
    			+ "where PrtNo ='" + tPrtNo + "'";
    	SSRS tSSRS = new SSRS();
    	tSSRS=new ExeSQL().execSQL(Sql);
    	if(tSSRS==null || tSSRS.getMaxRow()==0){
    		String tStrErr = "未查询到保单信息！";
            buildError("dealCoInsurenceInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
    	}
    	String tGrpContNo=tSSRS.GetText(1, 1);
    	String tProposalGrpContNo=tSSRS.GetText(1, 2);
    	//处理共保机构
    	String tConAgentCom=cMsgDatas.getTextByFlag("CoAgentCom");
    	String tConAgentName=cMsgDatas.getTextByFlag("CoAgentComName");
    	String tRate=cMsgDatas.getTextByFlag("Rate");
    	tTmpLCCoInsuranceParamSchema.setGrpContNo(tGrpContNo);
        tTmpLCCoInsuranceParamSchema.setProposalGrpContNo(tProposalGrpContNo);
        tTmpLCCoInsuranceParamSchema.setPrtNo(tPrtNo);
        tTmpLCCoInsuranceParamSchema.setAgentCom(tConAgentCom);
        tTmpLCCoInsuranceParamSchema.setAgentComName(tConAgentName);
        tTmpLCCoInsuranceParamSchema.setRate(tRate);
        
        //处理共保机构1
        String tConAgentCom1=cMsgDatas.getTextByFlag("CoAgentCom1");
    	String tConAgentName1=cMsgDatas.getTextByFlag("CoAgentComName1");
    	String tRate1=cMsgDatas.getTextByFlag("Rate1");
    	tTmpLCCoInsuranceParamSchema1.setGrpContNo(tGrpContNo);
        tTmpLCCoInsuranceParamSchema1.setProposalGrpContNo(tProposalGrpContNo);
        tTmpLCCoInsuranceParamSchema1.setPrtNo(tPrtNo);
        tTmpLCCoInsuranceParamSchema1.setAgentCom(tConAgentCom1);
        tTmpLCCoInsuranceParamSchema1.setAgentComName(tConAgentName1);
        tTmpLCCoInsuranceParamSchema1.setRate(tRate1);
        
        tLCCoInsuranceParamSet.add(tTmpLCCoInsuranceParamSchema);
        tLCCoInsuranceParamSet.add(tTmpLCCoInsuranceParamSchema1);
        
    	TransferData tTransferData = new TransferData();
    	tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
    	VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(tLCCoInsuranceParamSet);
        tVData.add(tTransferData);
        
        CoInsuranceGrpContUL tCoInsuranceGrpContUL = new CoInsuranceGrpContUL();
        try{
        	if (!tCoInsuranceGrpContUL.submitData(tVData, "Create"))
            {
        		String tStrErr = " 处理失败! 原因是: " + tCoInsuranceGrpContUL.mErrors.getLastError();
        		buildError("dealCoInsurenceInfo", tStrErr);
                if (!mImportLog.errLog(mPrtNo, tStrErr))
                {
                    mErrors.copyAllErrors(mImportLog.mErrors);
                    return false;
                }
            }
        }catch(Exception ex)
        {
        	String tStrErr = "共保信息插入失败！";
            buildError("dealCoInsurenceInfo", tStrErr);
            if (!mImportLog.errLog(mPrtNo, tStrErr))
            {
                mErrors.copyAllErrors(mImportLog.mErrors);
                return false;
            }
        }
        
    	return true;
    }
}
