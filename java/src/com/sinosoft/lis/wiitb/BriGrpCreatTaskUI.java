package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BriGrpCreatTaskUI {
	/** �������� */
    public CErrors mErrors = new CErrors();

    public BriGrpCreatTaskUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	BriGrpCreatTaskBL tBriGrpCreatTaskBL = new BriGrpCreatTaskBL();
            if (!tBriGrpCreatTaskBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tBriGrpCreatTaskBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
