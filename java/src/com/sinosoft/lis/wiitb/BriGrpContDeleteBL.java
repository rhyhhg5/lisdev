package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title:团单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class BriGrpContDeleteBL {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
    
    private String mOperate = "";

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    public BriGrpContDeleteBL() {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }
    	
    	if (!submit())
        {
            return false;
        }

        return true;
    }
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }
    
    private boolean getInputData(VData cInputData, String cOperate)
    {
    	mInputData = (VData) cInputData.clone(); 
    	
    	mOperate = cOperate;
    	//全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null) {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        //团体保单实例
        mLCGrpContSchema.setSchema((LCGrpContSchema) mInputData.
                                   getObjectByObjectName("LCGrpContSchema", 0));

        if (mLCGrpContSchema == null) {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));

            return false;
        }

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = null;
        if (!"".equals(StrTool.cTrim(mLCGrpContSchema.getGrpContNo()))) {
            tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLCGrpContSet = tLCGrpContDB.query();
        } else{
        	this.mErrors.addOneError(new CError("传入的团单保单号码为空！"));
            return false;
        }

        if (tLCGrpContSet != null && tLCGrpContSet.size() > 0) {
            mLCGrpContSchema.setSchema(tLCGrpContSet.get(1));
        }
        System.out.println("完成数据解析 结果为 " + mLCGrpContSchema.getPrtNo() + "  " +
                           mLCGrpContSchema.getGrpContNo());
        return true;
    }
    private boolean checkData()
    {
//    	是否签单
    	if(!checkSign(mLCGrpContSchema.getPrtNo())){
    		buildError("checkData","保单已签单，不能删除！");
    		return false;
    	}
//    	财务数据
    	if(!checkTempfee(mLCGrpContSchema.getPrtNo())){//已生成财务数据
    		buildError("checkData","该保单已存在财务数据，不能删除！");
        	return false;
        }
//    	锁定发盘
//    	if(!updateAccountInfo()){
//        	return false;
//        }
    	return true;
    }
    private boolean dealData()
    {
//    	获取删除保单数据
    	BriGrpContDeleteData tBriGrpContDeleteData = new BriGrpContDeleteData();
    	if(!tBriGrpContDeleteData.submitData(mInputData, mOperate)){
    		this.mErrors.copyAllErrors(tBriGrpContDeleteData.mErrors);
			return false;
    	}
//    	保单数据
    	MMap delGrpContMMap = tBriGrpContDeleteData.getMMap();
    	if(delGrpContMMap == null){
    		buildError("dealData","获取待删除保单数据失败！");
    		return false;
    	}
    	mMap.add(delGrpContMMap);
//    	财务数据
    	MMap delLJFeeMMap = getLJData();
    	mMap.add(delLJFeeMMap);
//    	更新任务
    	MMap BatchTaskMMap = updateBatchTask();
    	if(BatchTaskMMap != null){
    		mMap.add(BatchTaskMMap);
    	}
    	
    	mResult.clear();
        mResult.add(mMap);
        return true;
    }

    private MMap updateBatchTask(){
    	MMap BatchMMap = new MMap();
    	String tPrtNo = mLCGrpContSchema.getPrtNo();
    	LCBriGrpImportDetailDB tLCBriGrpImportDetailDB = new LCBriGrpImportDetailDB();
//    	tLCBriGrpImportDetailDB.setBussNo(tPrtNo);
    	String tSql = "select * from LCBriGrpImportDetail where bussno = '"+tPrtNo+"' and confState !='02' and (enableFlag ='00' or enableFlag = '01') ";
    	LCBriGrpImportDetailSet tLCBriGrpImportDetailSet = tLCBriGrpImportDetailDB.executeQuery(tSql);
    	if(tLCBriGrpImportDetailSet != null && tLCBriGrpImportDetailSet.size()>0){
    		for(int i=1;i<=tLCBriGrpImportDetailSet.size();i++){
    			LCBriGrpImportDetailSchema tLCBriGrpImportDetailSchema = tLCBriGrpImportDetailSet.get(i);
				tLCBriGrpImportDetailSchema.setEnableFlag("02");
				tLCBriGrpImportDetailSchema.setModifyDate(PubFun.getCurrentDate());
				tLCBriGrpImportDetailSchema.setModifyTime(PubFun.getCurrentTime());
				BatchMMap.put(tLCBriGrpImportDetailSchema, SysConst.UPDATE);
    		}
    	}
    	return BatchMMap;
    }
//    财务数据
    private MMap getLJData(){

		MMap LJDataMMap = new MMap();
		String tPrtNo = mLCGrpContSchema.getPrtNo();
		String tSql = "";
		Reflections tReflections = new Reflections();

		LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
		// tLJTempFeeDB.setOtherNo(tPrtNo);
		tSql = "select * from LJTempFee where OtherNo = '" + tPrtNo + "' ";
		LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.executeQuery(tSql);
		if (tLJTempFeeSet != null && tLJTempFeeSet.size() > 0) {
			String tTempfeeNo = tLJTempFeeSet.get(1).getTempFeeNo();
			LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
			// tLJTempFeeClassDB.setTempFeeNo(tTempfeeNo);
			tSql = "select * from LJTempFeeClass where TempFeeNo = '"
					+ tTempfeeNo + "' ";
			LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB
					.executeQuery(tSql);
			if (tLJTempFeeClassSet != null && tLJTempFeeClassSet.size() > 0) {
				LJTempFeeClassBSet tLJTempFeeClassBSet = new LJTempFeeClassBSet();
				LJTempFeeClassBSchema tLJTempFeeClassBSchema = new LJTempFeeClassBSchema();
				tLJTempFeeClassBSet.add(tLJTempFeeClassBSchema);
				tReflections
						.transFields(tLJTempFeeClassBSet, tLJTempFeeClassSet);
				for (int i = 1; i <= tLJTempFeeClassBSet.size(); i++) {
					String aSeqNo = PubFun1.CreateMaxNo("TEMPFEESEQNO", "");
					tLJTempFeeClassBSet.get(i).setSeqNo(aSeqNo);
				}
				LJDataMMap.put(tLJTempFeeClassBSet, SysConst.DELETE_AND_INSERT);
				LJDataMMap.put(tLJTempFeeClassSet, SysConst.DELETE);
			}
			LBTempFeeSet tLBTempFeeSet = new LBTempFeeSet();
			LBTempFeeSchema tLBTempFeeSchema = new LBTempFeeSchema();
			tLBTempFeeSet.add(tLBTempFeeSchema);
			tReflections.transFields(tLBTempFeeSet, tLJTempFeeSet);
			String aBackUpSerialNo = PubFun1.CreateMaxNo("LBTempFee", 20);
			for (int i = 1; i <= tLBTempFeeSet.size(); i++) {
				tLBTempFeeSet.get(i).setBackUpSerialNo(aBackUpSerialNo);
			}
			LJDataMMap.put(tLBTempFeeSet, SysConst.DELETE_AND_INSERT);
			LJDataMMap.put(tLJTempFeeSet, SysConst.DELETE);
		}

		LJSPayDB tLJSPayDB = new LJSPayDB();
		// tLJSPayDB.setOtherNo(tPrtNo);
		tSql = "select * from LJSPay where OtherNo = '" + tPrtNo + "' ";
		LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSql);
		if(tLJSPaySet != null && tLJSPaySet.size()>0){
			LJSPayBSet tLJSPayBSet = new LJSPayBSet();
			LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
			tLJSPayBSet.add(tLJSPayBSchema);
			tReflections.transFields(tLJSPayBSet, tLJSPaySet);
			LJDataMMap.put(tLJSPayBSet, SysConst.DELETE_AND_INSERT);
			LJDataMMap.put(tLJSPaySet, SysConst.DELETE);
		}
		
		return LJDataMMap;
	}
    
    /**
	 * 操作结果
	 * 
	 * @return VData
	 */
    public VData getResult() {
        return mResult;
    }
    
    /**
	 * 操作结果
	 * 
	 * @return VData
	 */
    public MMap getMMap() {
        return mMap;
    }
    /**
	 * 出错处理
	 * 
	 * @param szFunc
	 *            String
	 * @param szErrMsg
	 *            String
	 */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GroupContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    
    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit()
    {
        VData data = new VData();
        data.add(mMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
//    缴费方式为银行转账时（paymode = 4），校验账户信息，及时锁定待发盘数据。
    private boolean updateAccountInfo()
    {
        String tPrtNo = mLCGrpContSchema.getPrtNo();

        // 判断保单缴费方式是否为：4-银行转帐，且尚未签单
        String tStrSql = null;
        String tStrResult = null;
        tStrSql = " select 1 from LCGrpCont where PayMode in ('4') and AppFlag != '1' and PrtNo = '" +tPrtNo+ "' ";
        tStrResult = new ExeSQL().getOneValue(tStrSql);
        if (!"1".equals(tStrResult))
        {
            System.out.println("非银行转帐方式缴费的保单，不用更新银行帐户。");
            return true;
        }
        tStrSql = null;
        tStrResult = null;
        // --------------------
        
        String tGetNoticeNo = null;

        // 判断转帐方式缴费的保单，待收费信息是否在途（？？！！..）
        tStrSql = " select * from LJSPay where OtherNo = '" + tPrtNo + "'";
        LJSPaySet tLJSPaySet = new LJSPayDB().executeQuery(tStrSql);
        if (tLJSPaySet == null || tLJSPaySet.size() != 1)
        {
            String tStrErr = "[" + tPrtNo + "]：待发盘数据异常。";
            buildError("updateAccountInfo", tStrErr);
            return false;
        }

        LJSPaySchema tSPayInfo = tLJSPaySet.get(1);

        String tOnTheWayFlag = tSPayInfo.getBankOnTheWayFlag();
        if ("1".equals(tOnTheWayFlag))
        {
            String tStrErr = "[" + tPrtNo + "]：发盘数据已在途，不允许重复处理。";
            buildError("updateAccountInfo", tStrErr);
            return false;
        }

        tGetNoticeNo = tSPayInfo.getGetNoticeNo();

        LJSPayDB tUpSPayDB = new LJSPayDB();
        tUpSPayDB.setGetNoticeNo(tGetNoticeNo);
        if (!tUpSPayDB.getInfo())
        {
            String tStrErr = "[" + tPrtNo + "]：待发盘数据异常。";
            buildError("updateAccountInfo", tStrErr);
            return false;
        }

        tUpSPayDB.setCanSendBank("1");
        tUpSPayDB.update(); // 即时锁定待发盘数据

        tStrSql = null;
        tStrResult = null;
        // --------------------
        return true;

    }
//    是否签单
    private boolean checkSign(String aPrtNo){
    	String tSignSql = "select appflag,signdate from lcgrpcont where prtno = '"+aPrtNo+"'";
    	SSRS tSSRS = new ExeSQL().execSQL(tSignSql);
    	if(tSSRS != null && tSSRS.MaxRow >0){
    		if("1".equals(tSSRS.GetText(1, 1)) || (tSSRS.GetText(1, 2) != null && !"".equals(tSSRS.GetText(1, 2)))){//已签单
    			return false;
    		}
    	}
    	return true;
    }
    
    /**
     * 校验是否存在银行数据
     * @param LCBriGrpImportDetailSchema
     * @param prtno
     * @return boolean
     */
    private boolean checkTempfee(String aPrtNo){
    	String tSql = "select 1 from ljtempfee where otherno = '"+aPrtNo+"' and enteraccdate is not null and confdate is null ";
    	String tBankFlag = new ExeSQL().getOneValue(tSql);
    	if("1".equals(tBankFlag)){
			return false;
    	}
    	return true;
    }

    
    public static void main(String[] args){
    	BriGrpContDeleteBL tBriGrpContDeleteBL = new BriGrpContDeleteBL();
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.ManageCom = "86110000";
    	tGlobalInput.Operator = "ly";
    	
    	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    	tLCGrpContDB.setPrtNo("99000400001");
    	LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
    	
    	VData tVData = new VData();
    	tVData.add(tGlobalInput);
    	tVData.add(tLCGrpContSet.getObj(1));
    	tBriGrpContDeleteBL.submitData(tVData, "test");
    }
}
