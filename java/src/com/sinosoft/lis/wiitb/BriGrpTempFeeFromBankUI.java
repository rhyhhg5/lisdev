package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BriGrpTempFeeFromBankUI {
	/** �������� */
    public CErrors mErrors = new CErrors();

    public BriGrpTempFeeFromBankUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	BriGrpTempFeeFromBankBL tBriGrpTempFeeFromBankBL = new BriGrpTempFeeFromBankBL();
            if (!tBriGrpTempFeeFromBankBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tBriGrpTempFeeFromBankBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
