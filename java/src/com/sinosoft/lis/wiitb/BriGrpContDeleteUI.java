package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title:团单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class BriGrpContDeleteUI
{

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	public BriGrpContDeleteUI()
	{
	}

	/**
	 * 不执行任何操作，只传递数据给下一层
	 * @param cInputData VData
	 * @param cOperate String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;

		BriGrpContDeleteBL tBriGrpContDeleteBL = new BriGrpContDeleteBL();

		if (tBriGrpContDeleteBL.submitData(cInputData, mOperate) == false)
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tBriGrpContDeleteBL.mErrors);

			return false;
		}
		else
		{
			mResult = tBriGrpContDeleteBL.getResult();
		}

		return true;
	}

	/**
	 * 获取从BL层取得的结果
	 * @return VData
	 */
	public VData getResult()
	{
		return mResult;
	}

	public static void main(String[] agrs)
	{
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();    //集体保单
		GlobalInput mGlobalInput = new GlobalInput();
		mGlobalInput.ManageCom = "86";
		mGlobalInput.Operator = "UW0003";

		tLCGrpContSchema.setGrpContNo("1400006631");
		tLCGrpContSchema.setManageCom("86440000");

		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(mGlobalInput);

		BriGrpContDeleteUI tgrlbl = new BriGrpContDeleteUI();
		tgrlbl.submitData(tVData, "DELETE");

		if (tgrlbl.mErrors.needDealError())
		{
			System.out.println(tgrlbl.mErrors.getFirstError());
		}
	}
}
