package com.sinosoft.lis.wiitb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BriGrpDownLoadUI {
	/** �������� */
    public CErrors mErrors = new CErrors();

    public BriGrpDownLoadUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
        	BriGrpDownLoadBL tBriGrpDownLoadBL = new BriGrpDownLoadBL();
            if (!tBriGrpDownLoadBL.submitData(cInputData, cOperate))
            {
                this.mErrors.copyAllErrors(tBriGrpDownLoadBL.mErrors);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            this.mErrors.addOneError(e.getMessage());
            return false;
        }
    }
}
